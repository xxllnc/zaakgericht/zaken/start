.. SPDX-FileCopyrightText: Mintlab B.V.
..
.. SPDX-License-Identifier: EUPL-1.2


Contributing
============

Thank you for your support and effort in helping this project grow. We welcome
all contributors to our codebase. And by reading the following guidelines, we
are hoping you get al the information you need to write beautiful and working
code.

The Minty namespace is for all packages related to the Minty framework, which
provides a file layout based on Domain Driven Design. It allows you to quickly
setup a Python module in this structure, and with the provided cookiecutter,
it will allow you to quickly setup a rmq worker or Pyramid HTTP api.

TL;DR
-----

Setup your workspace, by running the following commands. After that, continue
reading your section for your editor

::

  git config core.hooksPath bin/git-hooks
  git config commit.template .git-commit-template


What's in a name
----------------

The name Minty Python is chosen because of the reference to both our main
company name and Monty Python. But don't let the name scare you: this project
is released in Open Source, so we will allow you to do with it whatever you
want ;)

Code structure
--------------

This project uses a lot of DDD (Domain Driven Design) patterns, which is a
great method for writing complex applications. It gives us a method to write
sustainable code, and prevents us from the pitfall of tighly coupling too
much concepts of our application.

**Repository structure**

minty
  Base of this application

minty.app
  [application] Application layer, here is where the http daemon or
  messagebus listener would reside.

minty.app.service
  Application services

minty.app.pyramid
  Pyramid HTTP Daemon

minty.app.http.views
  API Views

minty.domain
  [domain] Domain layer, here is where all the bussiness logic resides

minty.infrastructure
  Infrastructure layer

Cookiecutter
------------

We use cookiecutter to generate our code structure. This way we make sure we
use our CI tooling, documentation and test coverage tools properly. Starting
a new repository derived from this template is as simple as running the
following command.

  pip3 install cookiecutter
  cookiecutter https://gitlab.com/minty-python/minty-template.git

Please follow the questions, and a new repository will be generated for you.
If you select `use_hooks`, it will load the mentioned "TL;DR" git config
commands.

Docker
------

A generated project contains a docker-compose.yml and Dockerfile, which means
you can startup your project by running the known docker-compose commands. In
short:

::

  docker-compose up -d PROJECT_NAME

Documentation
-------------

Documentation for your module is generated by SPHINX, and can be regenerated
via the docker-compose command:

::

  docker-compose run --rm PROJECT_NAME bin/generate_documentation.sh

Your README.rst will be copied to the docs/generated directory, before
generating the new documentation. You can view the documentation by opening
`docs/output/index.html`

Bumping a version
-----------------

We provide bumpversion as tooling for bumping the version and setting a tag
in git. Usage:

::

  # Remove dry-run, when you actually want to bump it (patch release)
  bumpversion --dry-run patch

  # Be more verbose (minor release):
  bumpversion --verbose --dry-run minor

  # Major release
  bumpversion major
  

Code location
-------------

This code is hosted on gitlab, please see the README.md for the exact
location of this specific codebase. We use `git` as our repository software.

Code quality
------------

Along with readable code, we like extensible documentation, unit tests and
a solid understanding of the location of our code. We use the following tools
to make sure we do not forget something (PEP8):

::

    isort   # Makes sure all those "import" lines are sorted
    black   # Makes sure we use the proper indentation, line width, etc.
    ruff  # Code quality and linting

In short:

- Use spaces instead of tabs
- Make sure your maximum line width does not exceed 79 characters
- Write a test for every function
- Write your documentation

Or, just make it easey for yourself, and use the proper tools for the job:

Git setup
---------

We provided some git-hooks, which can check your code for proper indentation
and style.

On the commandline, run:

::

  git config core.hooksPath bin/git-hooks


Editor setup
------------

- Plase use the editoconfiguration from .editorconfig, instructions:
  https://editorconfig.org/
- For VIM: https://realpython.com/vim-and-python-a-match-made-in-heaven/

Sublime
^^^^^^^

**Package Control: Install packages**

**sublack**

::

  {
    "black_line_length": 79,
    "black_on_save": true,
  }

**Python Flake8 Lint** 

::

  {
    "ignore": ["E203", "E266", "E501", "W503"],
    "complexity": 10,
  }


Committing code
---------------

After you followed our code quality guidelines above, you are ready for a code
review from someone else. We assume you already forked the project to your
own private repository.

Commit message
^^^^^^^^^^^^^^

Please rebase your changes to a single commit describing your change. Make
sure we know whether it is a feature or a bugfix, or simply, follow the given
commit message template `.git-commit-template`. You can set it up as a default
by running:

::

  git config commit.template .git-commit-template

Merge request
^^^^^^^^^^^^^

Now that you have a solid commmit message describing the change you've made
to the software, it is time to create a merge request, so other people can
review your changes. Use the "Merge request" button from within gitlab to do
just that.

People will think something about your implementation, but they will allways
make sure you know that to do with it. The rules below describe the checks
a developer does when reviewing your code.

Code review rules
^^^^^^^^^^^^^^^^^

**Code layout**:

- Every class and function contains a valid docstring
- Code coverage is at the least higher than before the commit, or reason is
  given.
- README.rst is updated, when usage changes
- Code contains proper Python type hinting

**Functionality**:

Code review template
^^^^^^^^^^^^^^^^^^^^

When giving feedback to fellow developers, using more words is always better
than none. In addition to it, we like to make sure you know what to do with
the information by using the following emoji's ;)

- \:bulb: 💡 Code is okay, but by using this suggestion, it will be even better
- \:+1: 👍 Code is ok, just some feedback!
- \:-1: 👎Code is not ok, it does not pass the code review rules above

Please make sure you do not only use the emoji's above, but also tell the
developer what is wrong and what he could do to fix it.

**Examples:**

- \:bulb: I like your solution, but instead of ` a = a + 1 ` you could also
  increment the variable without extra code by doing: ` a += 1 ` .
- \:+1: Nice work! I really like what you did by refactoring this piece of code
- \:-1: Missing documentation: at function `addProject` you forgot to mention
  the required parameter `project`

Please make sure you approve the merge request at the top when you did not
find any showstoppers (thumbs down), this way the other party can decide what
to do next: leave it this way, or improve and process the given suggestions
and showstoppers.
