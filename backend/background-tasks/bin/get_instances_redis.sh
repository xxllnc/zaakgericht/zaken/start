function get_hosts {
    local REDIS=${1:-redis:6379}

    local REDISHOST=${REDIS%:*}
    local REDISPORT=${REDIS#*:}

    hosts=$(redis-cli -h "${REDISHOST}" -p "${REDISPORT}" --tls --raw smembers saas:instances);
}
