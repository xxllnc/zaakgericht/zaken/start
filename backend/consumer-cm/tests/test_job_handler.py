# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
import minty.cqrs
import minty.exceptions
import random
from datetime import datetime, timezone
from minty.cqrs.events import Event
from unittest import mock
from uuid import uuid4
from zsnl_case_events_consumer.handlers import CaseManagementJobHandler


class TestJobsHandler:
    def setup_method(self):
        self.mock_cqrs = mock.Mock(name="cqrs")
        self.mock_repo_factory = mock.Mock(name="repo_factory")

        self.mock_cqrs.domains = {
            "zsnl_domains.jobs": {
                "repository_factory": self.mock_repo_factory,
            }
        }
        self.handler = CaseManagementJobHandler(self.mock_cqrs)

    def test_handle_delete_case(self):
        """
        Test handling of the common case for the job handler: perform some work
        """
        job_uuid = uuid4()
        user_uuid = uuid4()
        case_uuid = str(uuid4())
        case_id = random.randint(1, 100)
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_job = mock.Mock(name="job")
        mock_job.configure_mock(
            status="running",
            job_description=mock.Mock(job_type="delete_case"),
        )
        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.return_value = mock_job
        mock_repo.get_item_from_batch.return_value = json.dumps(
            [case_uuid, case_id]
        )
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_any_call(
            event.correlation_id,
            "zsnl_domains.case_management",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_called_with(
            event.correlation_id,
            "zsnl_domains.jobs",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance().progress_job.assert_called_once_with(
            job_uuid=event.entity_id
        )

        self.mock_cqrs.get_command_instance().delete_case.assert_called_with(
            case_uuid=case_uuid
        )

    def test_handle_simulate_delete_case(self):
        """
        Test handling of the common case for the job handler: perform some work
        """
        job_uuid = uuid4()
        user_uuid = uuid4()
        case_uuid = str(uuid4())
        case_id = random.randint(1, 100)
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_job = mock.Mock(name="job")
        mock_job.configure_mock(
            status="running",
            job_description=mock.Mock(job_type="simulate_delete_case"),
        )
        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.return_value = mock_job
        mock_repo.get_item_from_batch.return_value = json.dumps(
            [case_uuid, case_id]
        )
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_any_call(
            event.correlation_id,
            "zsnl_domains.case_management",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_called_with(
            event.correlation_id,
            "zsnl_domains.jobs",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance().progress_job.assert_called_once_with(
            job_uuid=event.entity_id
        )

        self.mock_cqrs.get_query_instance().delete_case_check.assert_called_with(
            case_uuid=case_uuid
        )

    def test_handle_done(self):
        """
        Test handling of the common case for the job handler: perform some work
        """
        job_uuid = uuid4()
        user_uuid = uuid4()
        case_uuid1 = str(uuid4())
        case_id1 = random.randint(1, 100)
        case_uuid2 = str(uuid4())
        case_id2 = random.randint(1, 100)
        case_uuid3 = str(uuid4())
        case_id3 = random.randint(1, 100)

        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_job = mock.Mock(name="job")
        mock_job.configure_mock(
            status="running",
            job_description=mock.Mock(job_type="delete_case"),
        )
        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.return_value = mock_job
        mock_repo.get_item_from_batch.side_effect = [
            json.dumps([case_uuid1, case_id1]),
            json.dumps([case_uuid2, case_id2]),
            json.dumps([case_uuid3, case_id3]),
            None,
        ]
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.mock_cqrs.get_command_instance().delete_case.side_effect = [
            None,
            AssertionError("Just testing"),
            minty.exceptions.Conflict("It broke", "no/really"),
        ]

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_any_call(
            event.correlation_id,
            "zsnl_domains.case_management",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_called_with(
            event.correlation_id,
            "zsnl_domains.jobs",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        # Because the repository returns `None` when retrieving the next
        # job: job done!
        self.mock_cqrs.get_command_instance().job_processed.assert_called_once_with(
            job_uuid=event.entity_id
        )

        delete_calls = (
            self.mock_cqrs.get_command_instance().delete_case.call_args_list
        )
        assert len(delete_calls) == 3

        assert delete_calls[0] == mock.call(case_uuid=case_uuid1)
        assert delete_calls[1] == mock.call(case_uuid=case_uuid2)
        assert delete_calls[2] == mock.call(case_uuid=case_uuid3)

        finish_item_calls = mock_repo.finish_item.call_args_list
        assert len(finish_item_calls) == 3

        assert finish_item_calls[0] == mock.call(
            job=mock_job,
            batch_item=json.dumps([case_uuid1, case_id1]),
            result=(("deleted",), None),
        )
        assert finish_item_calls[1] == mock.call(
            job=mock_job,
            batch_item=json.dumps([case_uuid2, case_id2]),
            result=(None, "Just testing"),
        )
        assert finish_item_calls[2] == mock.call(
            job=mock_job,
            batch_item=json.dumps([case_uuid3, case_id3]),
            result=(None, "It broke"),
        )

    def test_no_user_info(self, caplog):
        """
        Test the log message when the event has no user_info
        (which should never happen "for real")
        """

        job_uuid = uuid4()
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=uuid4(),
            user_info=None,
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        self.handler.handle(event)

        assert (
            "Got JobPrepared event without user_info, cannot handle"
            in caplog.text
        )
        # No userinfo (should never happen in real life)

    def test_no_job_found(self, caplog):
        """
        Test the situation where the job is not found (it got deleted
        after cancelling)
        """
        caplog.set_level(logging.INFO)

        job_uuid = uuid4()
        user_uuid = uuid4()
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.side_effect = minty.exceptions.NotFound(
            "Test", "test/test"
        )
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_not_called()

        assert (
            f"Got an event of type {event.event_name} for non-existant job {event.entity_id}"
            in caplog.text
        )

    def test_not_running(self, caplog):
        """
        Check how jobs in the wrong state are handled (not "running")
        """
        caplog.set_level(logging.INFO)

        job_uuid = uuid4()
        user_uuid = uuid4()
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_job = mock.Mock(name="job")
        mock_job.configure_mock(
            status="cancelled",
            job_description=mock.Mock(job_type="delete_case"),
        )

        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.return_value = mock_job
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_not_called()

        assert (
            f"Got an event of type {event.event_name} for non-running job {event.entity_id}: cancelled"
            in caplog.text
        )

    def test_non_cm_type(self, caplog):
        """
        Tests handling of jobs for other domains
        """
        caplog.set_level(logging.DEBUG)

        job_uuid = uuid4()
        user_uuid = uuid4()
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_job = mock.Mock(name="job")
        mock_job.configure_mock(
            status="running",
            job_description=mock.Mock(job_type="something_not_cm_related"),
        )

        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.return_value = mock_job
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_not_called()

        assert "Received event for non-cm job" in caplog.text

    def test_handle_run_archive_export(self):
        """
        Test handling of the common case for the job handler: perform some work
        """
        job_uuid = uuid4()
        user_uuid = uuid4()
        case_uuid = str(uuid4())
        case_id = random.randint(1, 100)
        event = Event(
            uuid=uuid4(),
            created_date=datetime.now(timezone.utc),
            correlation_id=uuid4(),
            domain="zsnl_domains.jobs",
            context="test",
            user_uuid=user_uuid,
            user_info=minty.cqrs.UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="Job",
            entity_id=job_uuid,
            event_name="JobPrepared",
            changes=[],
            entity_data={},
        )

        mock_job = mock.Mock(name="job")
        mock_job.configure_mock(
            status="running",
            job_description=mock.Mock(job_type="run_archive_export"),
        )
        mock_repo = mock.Mock(name="repo")
        mock_repo.get_job.return_value = mock_job
        mock_repo.get_item_from_batch.return_value = json.dumps(
            [case_uuid, case_id]
        )
        self.mock_repo_factory.get_repository.return_value = mock_repo

        self.handler.handle(event)

        mock_repo.get_job.assert_called_once_with(
            job_uuid=event.entity_id,
            user_info=event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_any_call(
            event.correlation_id,
            "zsnl_domains.case_management",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance.assert_called_with(
            event.correlation_id,
            "zsnl_domains.jobs",
            event.context,
            event.user_uuid,
            event.user_info,
        )

        self.mock_cqrs.get_command_instance().progress_job.assert_called_once_with(
            job_uuid=event.entity_id
        )

        self.mock_cqrs.get_query_instance().run_archive_export.assert_called()
