# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty
import minty.logging.mdc
import os
from .consumers import CaseEventsConsumer
from logging.config import fileConfig
from minty.cqrs import CQRS
from minty.infrastructure import InfrastructureFactory
from minty.middleware import AmqpPublisherMiddleware
from minty_amqp.client import AMQPClient
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import case_management, jobs
from zsnl_perl_migration import (
    LegacyEventBroadcastMiddleware,
    LegacyEventQueueMiddleware,
)

ZS_COMPONENT = "zsnl_consumer_cm"


subject_relation_events = [
    ("zsnl_domains.case_management", "SubjectRelationCreated"),
    ("zsnl_domains.case_management", "SubjectRelationUpdated"),
    ("zsnl_domains.case_management", "SubjectRelationDeleted"),
]

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    record.req = minty.logging.mdc.get_mdc()  # type: ignore
    return record


def main():
    fileConfig("logging.conf")

    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    infra_factory = InfrastructureFactory(config_file="config.conf")
    cqrs = CQRS(
        domains=[case_management, jobs],
        infrastructure_factory=infra_factory,
        command_wrapper_middleware=[
            LegacyEventQueueMiddleware(
                database_infra_name="database",
                interesting_events=[],
                interesting_case_relation_events=[],
                interesting_subject_relation_events=subject_relation_events,
            ),
            DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="case_management", infrastructure_name="amqp"
            ),
            LegacyEventBroadcastMiddleware(
                amqp_infra_name="amqp",
                interesting_events=[],
                interesting_case_relation_events=[],
                interesting_subject_relation_events=subject_relation_events,
            ),
        ],
    )

    config = cqrs.infrastructure_factory.get_config(context=None)
    amqp_client = AMQPClient(config, cqrs=cqrs)
    amqp_client.register_consumers([CaseEventsConsumer])
    amqp_client.start()


def init():
    if __name__ == "__main__":
        main()


init()
