# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .handlers import CaseManagementBaseHandler
from minty_amqp.consumer import BaseConsumer, BaseHandler


class CaseEventsConsumer(BaseConsumer):
    def _register_routing(self):
        assert self.cqrs

        self._known_handlers: list[BaseHandler] = [
            handler(self.cqrs)
            for handler in CaseManagementBaseHandler.__subclasses__()
        ]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
