# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.cqrs
from .constants import (
    ARCHIVE_EXPORT_PREFIX,
    CASE_LEGACY_PREFIX,
    CASE_PREFIX,
    EXPORT_FILE_PREFIX,
    JOBS_PREFIX,
    SUBJECT_RELATION_PREFIX,
    TIMELINE_EXPORT_PREFIX,
)
from minty.exceptions import CQRSException, NotFound
from minty_amqp.consumer import BaseHandler
from typing import Any, Literal, cast
from uuid import UUID, uuid4
from zsnl_domains.jobs.entities.job import JobStatus
from zsnl_domains.jobs.repositories.job import (
    JobRepository,
    OkResult,
    Result,
)


class CaseManagementBaseHandler(BaseHandler):
    @property
    def domain(self) -> Literal["zsnl_domains.case_management"]:
        return "zsnl_domains.case_management"


class CaseCreatedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.CaseCreated"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        command_instance.enqueue_subcases(case_uuid=event.entity_id)
        command_instance.enqueue_documents(case_uuid=event.entity_id)
        command_instance.enqueue_subjects(case_uuid=event.entity_id)
        command_instance.enqueue_emails(case_uuid=event.entity_id)
        command_instance.transition_case(case_uuid=event.entity_id)


class CaseAttributesChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [
            "zsnl.v2.legacy.Case.CaseCustomFieldsUpdated",
            CASE_PREFIX + "CustomFieldSync",
        ]

    def handle(self, event):
        changes = event.format_changes()
        custom_fields = changes["custom_fields"]
        command_instance = self.get_command_instance(event)

        if event.event_name == "CustomFieldSync":
            custom_fields_updated = {}
            for cf in custom_fields:
                custom_fields_updated[cf] = []
                custom_fields_updated[cf].append(
                    custom_fields[cf].get("value")
                )
            custom_fields = custom_fields_updated

        try:
            command_instance.synchronize_relations_for_case(
                case_uuid=str(event.entity_id), custom_fields=custom_fields
            )
        except CQRSException as e:
            self.logger.warning(
                f"Error during relation sync; ignored: {e}", exc_info=True
            )


class CaseV2AttributeChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_PREFIX + "CustomFieldUpdated"]

    def _remove_relationship_type_wrapper(self, value):
        # when retrieving the case custom_fields, a wrapper is added around
        # the field value. This does not match with the input format of
        # synchronize_relations_for_case cmd. So it is removed here to match
        # the expected input.
        result = value
        if (
            "type" in value
            and value["type"] == "relationship"
            and isinstance(value["value"], dict)
        ):
            result = value["value"]
        return result

    def handle(self, event):
        changes = event.format_changes()

        # Need to apply transformation of the value to a json dump array
        # so it matches the exepected input for synchronize_relations_for_case
        custom_fields = {
            key: [json.dumps(self._remove_relationship_type_wrapper(value))]
            for key, value in changes["custom_fields"].items()
        }
        command_instance = self.get_command_instance(event)

        try:
            command_instance.synchronize_relations_for_case(
                case_uuid=str(event.entity_id), custom_fields=custom_fields
            )
        except CQRSException as e:
            self.logger.warning(
                f"Error during relation sync; ignored: {e}", exc_info=True
            )


class SubcasesEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.SubcasesEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.create_subcases(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_subcases_data"]).keys()
            ),
        )


class DocumentsEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.DocumentsEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.generate_documents(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_documents_data"]).keys()
            ),
        )


class EmailsEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.EmailsEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.generate_emails(
            case_uuid=event.entity_id,
            queue_ids=list(json.loads(changes["enqueued_emails_data"]).keys()),
        )


class SubjectsEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.SubjectsEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.generate_subjects(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_subjects_data"]).keys()
            ),
        )


class SubjectRelationCreatedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [SUBJECT_RELATION_PREFIX + "SubjectRelationCreated"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        if changes.get("send_confirmation_email"):
            # Send notification for employee/person/organization
            # when send_confirmation_email value is True
            # in the changes of SubjectRelationCreated event
            command_instance.enqueue_subject_relation_email(
                subject_relation_uuid=event.entity_id
            )
        # Sync subject and subject extern for the case
        if changes.get("case"):
            command_instance.synchronize_subject(
                case_uuid=changes["case"]["id"]
            )


class SubjectRelationEmailEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [SUBJECT_RELATION_PREFIX + "SubjectRelationEmailEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        command_instance.send_subject_relation_email(
            subject_relation_uuid=event.entity_id
        )


class CaseAssigneeEmailEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_PREFIX + "CaseAssigneeEmailEnqueued"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        queue_id = changes["assignee_email_queue_id"]
        command_instance = self.get_command_instance(event)
        command_instance.send_case_assignee_email(
            case_uuid=event.entity_id, queue_id=queue_id
        )


class TimelineExportRequestedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [TIMELINE_EXPORT_PREFIX + "TimelineExportRequested"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance = self.get_command_instance(event)
        command_instance.run_export(
            uuid=changes["uuid"],
            type=changes["type"],
            period_start=changes["period_start"],
            period_end=changes["period_end"],
        )


class ObjectActionTriggeredRequestedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_LEGACY_PREFIX + "ObjectActionTriggered"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        active_status: bool = (
            changes["system_attribute_changes"].get("status", "active")
            == "active"
        )
        self.get_command_instance(event).custom_object_action_trigger(
            case_uuid=event.entity_id,
            action_type=changes["action_type"],
            relation_field_magic_string=changes["attribute_magic_string"],
            custom_object_fields=changes["attribute_changes"],
            status=active_status,
        )


class CaseCreatedRelateCustomObjectHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_LEGACY_PREFIX + "CaseCreated"]

    def handle(self, event):
        changes = event.format_changes()
        command_instance = self.get_command_instance(event)

        for custom_object_uuid in changes.get("custom_objects", []):
            command_instance.relate_custom_object_to(
                custom_object_uuid=custom_object_uuid,
                cases=[event.entity_id],
            )


class SubjectRelationChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [
            SUBJECT_RELATION_PREFIX + "SubjectRelationDeleted",
            SUBJECT_RELATION_PREFIX + "SubjectRelationUpdated",
        ]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        entity_data = event.entity_data

        # Sync subject and subject extern for the case
        if entity_data.get("case"):
            command_instance.synchronize_subject(
                case_uuid=entity_data["case"]["id"]
            )


class ExportFileDownloadedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [EXPORT_FILE_PREFIX + "ExportFileDownloaded"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)

        command_instance.export_file_increase_download_counter(
            export_file_uuid=event.entity_id
        )


class ArchiveExportRequestedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [ARCHIVE_EXPORT_PREFIX + "ArchiveExportRequested"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        case_uuids = changes.get("case_uuids", None)
        export_file_uuid = changes.get("export_file_uuid", None)

        command_instance = self.get_command_instance(event)

        command_instance.run_archive_export(
            case_uuids=case_uuids,
            export_file_uuid=export_file_uuid,
        )


class CaseManagementJobHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [
            JOBS_PREFIX + "JobPrepared",
            JOBS_PREFIX + "JobProgressed",
        ]

    def get_query_instance(
        self, event: minty.cqrs.Event, domain: str | None = None
    ):
        """
        Retrieve the query instance for the handler's domain,
        preconfigured with the event data (correlation id, context, user id)
        """
        if not self.cqrs:
            # This is impossible to trigger, because `get_command_instance` is
            # called before this and it has the same check.
            # However, because this is an assertion/guard to ensure things are
            # called in the correct way, it should be kept even if untestable.
            raise AssertionError(
                "CQRS instance not available"
            )  # pragma: no cover

        return self.cqrs.get_query_instance(
            event.correlation_id,
            domain or self.domain,
            event.context,
            event.user_uuid,
            event.user_info,
        )

    def get_jobs_repository(self, context: str) -> JobRepository:
        """
        Retrieve a "jobs" repository

        This is a special case used to work around the restrictions in queries
        (read-only) and commands (no returns).
        """
        repo_factory = self.cqrs.domains["zsnl_domains.jobs"][
            "repository_factory"
        ]

        return cast(
            JobRepository,
            repo_factory.get_repository(
                "jobs", read_only=True, context=context
            ),
        )

    def _get_handler(self, job):
        self.logger.debug(f"Got a {job.job_description.job_type} job")
        match job.job_description.job_type:
            case "delete_case":
                return self._handle_delete_case
            case "simulate_delete_case":
                return self._handle_simulate_delete_case
            case "run_archive_export":
                return self._handle_run_archive_export
            case _:
                self.logger.debug("Received event for non-cm job")
                return

    def handle(self, event) -> None:
        if not event.user_info:
            self.logger.error(
                f"Got {event.event_name} event without user_info, cannot handle"
            )
            return

        jobs_repository = self.get_jobs_repository(event.context)

        try:
            entity_id = (
                event.entity_id
                if isinstance(event.entity_id, UUID)
                else UUID(event.entity_id)
            )
            job = jobs_repository.get_job(
                job_uuid=entity_id, user_info=event.user_info
            )
        except NotFound:
            self.logger.info(
                f"Got an event of type {event.event_name} for non-existant job {event.entity_id}"
            )
            return

        if job.status != JobStatus.running:
            self.logger.info(
                f"Got an event of type {event.event_name} for non-running job {event.entity_id}: {job.status}"
            )
            return

        handler = self._get_handler(job)
        if handler is None:
            # Job is not handled by `cm` domain
            return

        cm_command_instance = self.get_command_instance(event)
        cm_query_instance = self.get_query_instance(event)

        done = False
        for _ in range(10):
            item = jobs_repository.get_item_from_batch(job=job)

            if not item:
                # No more items: job done!
                done = True
                break

            result: Result
            try:
                result = handler(
                    event,
                    item,
                    command_instance=cm_command_instance,
                    query_instance=cm_query_instance,
                )
            except CQRSException as e:
                result = (None, e.args[0])
            except Exception as e:
                result = (None, str(e))

            # Write to OK / Error destination
            # Mark as done in Redis
            jobs_repository.finish_item(
                job=job, batch_item=item, result=result
            )

        command_instance_jobs = self.get_command_instance(
            event, "zsnl_domains.jobs"
        )

        if done:
            command_instance_jobs.job_processed(job_uuid=entity_id)
        else:
            command_instance_jobs.progress_job(job_uuid=entity_id)

    def _handle_delete_case(
        self,
        event: minty.cqrs.Event,
        batch_item: str,
        command_instance: Any,
        query_instance: Any,
    ) -> OkResult:
        self.logger.debug(f"Deleting case {batch_item}")

        item_parts: list[str] = json.loads(batch_item)

        command_instance.delete_case(case_uuid=item_parts[0])

        # Success responses are always a list -- even if it's only one!
        return (("deleted",), None)

    def _handle_simulate_delete_case(
        self,
        event: minty.cqrs.Event,
        batch_item: str,
        command_instance: Any,
        query_instance: Any,
    ) -> OkResult:
        self.logger.debug(f"Checking if case {batch_item} is deletable")

        item_parts: list[str] = json.loads(batch_item)

        response = query_instance.delete_case_check(case_uuid=item_parts[0])

        return ((response.check_result, response.warnings), None)

    def _handle_run_archive_export(
        self,
        event: minty.cqrs.Event,
        batch_item: str,
        command_instance: Any,
        query_instance: Any,
    ) -> OkResult:
        self.logger.debug(f"Creating archive export for case {batch_item}")

        item_parts: list[str] = json.loads(batch_item)
        export_file_uuid = uuid4()

        result = query_instance.run_archive_export(
            case_uuid=item_parts[0], export_file_uuid=export_file_uuid
        )

        return ((result,), None)
