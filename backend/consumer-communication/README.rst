.. _readme:

Please read [README.md](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/README.md)
for details.

.. SPDX-FileCopyrightText: 2020 Mintlab B.V.
..
.. SPDX-License-Identifier: EUPL-1.2
