# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

stages:
    - Tag
    - QA
    - Build
    - Verify
    - Release

.consumer_communication_job_variables:
  variables:
    BACKEND_PATH: "backend/consumer-communication"
    ADD_JOB_ON_CHANGES_OF_1: "backend/domains/**/*"
    ADD_JOB_ON_CHANGES_OF_2: "backend/minty-amqp/**/*"
    ADD_JOB_ON_CHANGES_OF_3: "backend/minty/**/*"
    ADD_JOB_ON_CHANGES_OF_4: "backend/minty-infra-*/**/*"

consumer-communication:Tag latest container when there are no changes:
  extends: 
    - .consumer_communication_job_variables
    - .tag_latest_backend_container_template
  # only is used to add this job only when the app or ci template is NOT changed
  # This is not supported yet by rules
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^release\//
      - $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
  except:
    refs:
      - schedule
      - web
    changes:
      - "backend/consumer-communication/**/*"
      - "template.gitlab-ci.yml"
      - "backend/domains/**/*"
      - "backend/minty/**/*"
      - "backend/minty-amqp/**/*"
      - "backend/minty-infra-*/**/*"

consumer-communication:REUSE Compliance:
  extends: 
    - .consumer_communication_job_variables
    - .reuse_compliance_template

consumer-communication:Run Python tests:
  extends: 
    - .consumer_communication_job_variables
    - .python_tests_template

.build_container_image_consumer_communication:
  extends:
    - .consumer_communication_job_variables
    - .build_and_push_container_image_template
  needs:
    - "set-version-number-and-tag-commit"
    - "consumer-communication:REUSE Compliance"
    - "consumer-communication:Run Python tests"
    - job: "minty-amqp:REUSE Compliance"
      optional: true
    - job: "minty-amqp:Run Python tests"
      optional: true
    - job: "domains:REUSE Compliance"
      optional: true
    - job: "domains:Run Python tests"
      optional: true

consumer-communication:Build container image (x86_64):
  extends: .build_container_image_consumer_communication
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  variables:
    BUILD_ARCH: "amd64"

consumer-communication:Build container image (arm64):
  extends: .build_container_image_consumer_communication
  tags: [ "xxllnc-shared", "arch:arm64" ]
  variables:
    BUILD_ARCH: "arm64"

consumer-communication:Make multiarch manifest:
  extends:
    - .consumer_communication_job_variables
    - .make_multiarch_manifest_template
  variables:
    BUILD_ARCHS: "amd64 arm64"
  needs:
    - "set-version-number-and-tag-commit"
    - "consumer-communication:Build container image (x86_64)"
    - "consumer-communication:Build container image (arm64)"

consumer-communication:Create SBOM:
    extends:
      - .consumer_communication_job_variables
      - .create_sbom_template
    needs: ["consumer-communication:Make multiarch manifest"]

#
# Release targets
#
consumer-communication:Tag xcp release container:
  extends:
    - .consumer_communication_job_variables    
    - .release_on_xcp
