# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from unittest import mock
from zsnl_communication_consumer.consumers import CommunicationConsumer


class TestConsumerCommunication(unittest.TestCase):
    def test_consumer_routing(self):
        consumer = CommunicationConsumer.__new__(CommunicationConsumer)
        consumer.cqrs = mock.Mock()

        consumer._register_routing()

        self.assertEqual(len(consumer._known_handlers), 4)
