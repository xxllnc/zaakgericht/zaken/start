# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from minty.exceptions import NotFound
from minty_amqp.consumer import BaseHandler


class CommunicationBaseHandler(BaseHandler):
    """
    Base class for all event handlers in the communication domain. Defines the
    "domain" property, required by `BaseHandler`.
    """

    @property
    def domain(self) -> str:
        return "zsnl_domains.communication"


class EmailImportHandler(CommunicationBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return ["zsnl.v2.communication.IncomingEmail.EmailReceived"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.import_email(file_uuid=changes["file"])


class EmailSendHandler(CommunicationBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return [
            "zsnl.v2.zsnl_domains_communication.ExternalMessage.ExternalMessageCreated",
            "zsnl.v2.zsnl_communication_http_domains_communication.ExternalMessage.ExternalMessageCreated",
        ]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }

        if changes["external_message_type"] != "email":
            self.logger.debug(
                f"Ignoring event: message type is {changes['external_message_type']}, not email"
            )
            return

        if changes["direction"] != "outgoing":
            self.logger.debug(
                f"Ignoring event: direction is {changes['direction']}, not outgoing"
            )
            return

        command_instance = self.get_command_instance(event)

        command_instance.send_email(message_uuid=event.entity_id)


class AttachedToMessageHandler(CommunicationBaseHandler):
    """As a user I want to see preview for an attachment.
    When an attachment is linked to message, a pdf derivative for attachment is generated in background.
    """

    @property
    def routing_keys(self) -> list[str]:
        return [
            "zsnl.v2.zsnl_domains_communication.MessageAttachment.AttachedToMessage",
            "zsnl.v2.zsnl_communication_http_domains_communication.MessageAttachment.AttachedToMessage",
        ]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }

        try:
            command_instance.generate_pdf_derivative(
                attachment_uuid=changes["attachment_uuid"]
            )
        except KeyError:
            self.logger.exception(
                "Got an 'AttachedToMessage' event without an attachment uuid"
            )
        except NotFound:
            self.logger.exception(
                "Got a 'Not Found' exception while trying to generate PDF derivative"
            )


class CaseAssigneeNotificationHandler(CommunicationBaseHandler):
    """
    Handler for the "Case.AssigneeNotified" event, which is sent when a
    notification email needs to be sent to an assignee.
    """

    @property
    def routing_keys(self) -> list[str]:
        return ["zsnl.v2.zsnl_domains_case_management.Case.AssigneeNotified"]

    def handle(self, event: minty.cqrs.Event):
        command_instance = self.get_command_instance(event)
        changes = event.format_changes()
        command_instance.send_notification(
            case_uuid=event.entity_id,
            sender_name=changes["notification"]["sender_name"],
            sender_address=changes["notification"]["sender_address"],
            subject=changes["notification"]["subject"],
            body=changes["notification"]["body"],
            recipient_address=changes["notification"]["recipient_address"],
        )
