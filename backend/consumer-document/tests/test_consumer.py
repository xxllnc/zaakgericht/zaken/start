# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import TestCase, mock
from zsnl_documents_events_consumer import __main__
from zsnl_documents_events_consumer.consumers import DocumentsEventsConsumer


class TestConsumer(TestCase):
    def test_consumer(self):
        document_consumer = DocumentsEventsConsumer(
            queue="fake_queue_name",
            exchange="fake_exchange",
            cqrs=mock.Mock(),
            qos_prefetch_count=2,
            dead_letter_config={},
        )

        rk = document_consumer.routing_keys

        assert set(rk) == {
            "zsnl.v2.legacy.Case.CaseCreated",
            "zsnl.v2.legacy.Document.DocumentCreated",
            "zsnl.v2.legacy.Document.PreviewRequested",
            "zsnl.v2.legacy.Document.ThumbnailRequested",
            "zsnl.v2.legacy.File.VirusScanFinished",
            "zsnl.v2.zsnl_domains_document.DocumentArchive.DocumentArchiveRequested",
            "zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
            "zsnl.v2.zsnl_domains_document.Document.DocumentFromAttachmentCreated",
            "zsnl.v2.zsnl_domains_document.Document.PreviewCreated",
            "zsnl.v2.zsnl_domains_document.Document.PreviewRequested",
            "zsnl.v2.zsnl_domains_document.Document.ThumbnailRequested",
        }


class TestMain:
    @mock.patch("zsnl_documents_events_consumer.__main__.CQRS")
    @mock.patch(
        "zsnl_documents_events_consumer.__main__.InfrastructureFactory"
    )
    @mock.patch("zsnl_documents_events_consumer.__main__.AMQPClient")
    def test_main(self, mock_client, mock_infra, mock_cqrs):
        with mock.patch.object(__main__, "__name__", "__main__"):
            __main__.init()
            mock_client.assert_called()
            mock_client().register_consumers.assert_called_with(
                [DocumentsEventsConsumer]
            )
            mock_client().start.assert_called()

    @mock.patch("zsnl_documents_events_consumer.__main__.old_factory")
    def test_log_record_factory(self, old_factory):
        record = __main__.log_record_factory("a", test="b")

        old_factory.assert_called_once_with("a", test="b")
        assert record.zs_component == "zsnl_consumer_document"  # type: ignore
        assert record == old_factory()
