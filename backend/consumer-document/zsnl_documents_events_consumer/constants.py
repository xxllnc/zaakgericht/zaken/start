# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Final

DOCUMENT_ARCHIVE_REQUESTED_PREFIX: Final[str] = (
    "zsnl.v2.zsnl_domains_document.DocumentArchive."
)
DOCUMENT_PREFIX: Final[str] = "zsnl.v2.zsnl_domains_document.Document."
LEGACY_FILE_PREFIX: Final[str] = "zsnl.v2.legacy.File."
LEGACY_DOCUMENT_PREFIX: Final[str] = "zsnl.v2.legacy.Document."
LEGACY_CASE_PREFIX: Final[str] = "zsnl.v2.legacy.Case."
