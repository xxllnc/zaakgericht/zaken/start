# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import constants
from minty.cqrs.events import Event
from minty_amqp.consumer import BaseHandler


class DocumentBaseHandler(BaseHandler):
    @property
    def domain(self):
        return "zsnl_domains.document"


class CreateDocumentArchiveRequestedHandler(DocumentBaseHandler):
    """
    Create zipfile for downloading multiple documents at once.
    """

    @property
    def routing_keys(self) -> list[str]:
        return [
            constants.DOCUMENT_ARCHIVE_REQUESTED_PREFIX
            + "DocumentArchiveRequested"
        ]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)

        changes = event.format_changes()

        case_uuid = changes.get("case_uuid", None)
        zip_all = changes.get("zip_all", None)
        directory_uuids = changes.get("directory_uuids", None)
        document_uuids = changes.get("document_uuids", None)
        export_file_uuid = changes.get("export_file_uuid", None)

        command_instance.create_document_archive(
            case_uuid=case_uuid,
            zip_all=zip_all,
            directory_uuids=directory_uuids,
            document_uuids=document_uuids,
            export_file_uuid=export_file_uuid,
        )
        return


class SetDocumentSearchTermsOnCreateHandler(DocumentBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return [
            constants.DOCUMENT_PREFIX + "DocumentCreated",
            constants.DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
            constants.LEGACY_DOCUMENT_PREFIX + "DocumentCreated",
        ]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)
        command_instance.set_search_terms_for_document(
            document_uuid=event.entity_id
        )
        return


class SetDocumentSearchTermsOnVirusscanHandler(DocumentBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return [constants.LEGACY_FILE_PREFIX + "VirusScanFinished"]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)
        command_instance.set_search_terms_for_documents_for_file(
            file_uuid=event.entity_id
        )
        return


class CreateDocumentPreviewOnDocumentEventHandler(DocumentBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return [
            constants.DOCUMENT_PREFIX + "DocumentCreated",
            constants.DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
            constants.DOCUMENT_PREFIX + "PreviewRequested",
            constants.LEGACY_DOCUMENT_PREFIX + "DocumentCreated",
            constants.LEGACY_DOCUMENT_PREFIX + "PreviewRequested",
        ]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)

        is_manual_request = event.routing_key in (
            constants.DOCUMENT_PREFIX + "PreviewRequested",
            constants.LEGACY_DOCUMENT_PREFIX + "PreviewRequested",
        )

        command_instance.create_preview_for_document(
            document_uuid=event.entity_id, is_manual_request=is_manual_request
        )
        return


class CreateDocumentPreviewOnVirusscanHandler(DocumentBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return [constants.LEGACY_FILE_PREFIX + "VirusScanFinished"]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)
        command_instance.create_preview_for_documents_for_file(
            file_uuid=event.entity_id
        )
        return


class CreateDocumentThumbnailOnDocumentEventHandler(DocumentBaseHandler):
    """
    Create thumbnails for documents once they're created, or once the
    preview has been created.

    The create_thumbnail_for_document call does a "quick exit" if no
    thumbnail can be (or needs to be) made.
    """

    @property
    def routing_keys(self) -> list[str]:
        return [
            constants.DOCUMENT_PREFIX + "DocumentCreated",
            constants.DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
            constants.DOCUMENT_PREFIX + "PreviewCreated",
            constants.DOCUMENT_PREFIX + "ThumbnailRequested",
            constants.LEGACY_DOCUMENT_PREFIX + "DocumentCreated",
            constants.LEGACY_DOCUMENT_PREFIX + "ThumbnailRequested",
        ]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)

        is_manual_request = event.routing_key in (
            constants.DOCUMENT_PREFIX + "ThumbnailRequested",
            constants.LEGACY_DOCUMENT_PREFIX + "ThumbnailRequested",
        )

        command_instance.create_thumbnail_for_document(
            document_uuid=event.entity_id,
            is_manual_request=is_manual_request,
        )
        return


class CreateDocumentThumbnailOnVirusscanHandler(DocumentBaseHandler):
    """
    Create thumbnails for documents that can be thumbnailed directly
    """

    @property
    def routing_keys(self) -> list[str]:
        return [constants.LEGACY_FILE_PREFIX + "VirusScanFinished"]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)
        command_instance.create_thumbnail_for_documents_for_file(
            file_uuid=event.entity_id
        )
        return


class CreatePDFForCaseRegistration(DocumentBaseHandler):
    """
    Create PDF for created cases if specified in the casetype

    Puts PDF in document-tab with a specific label ("PDF-webformulier")
    """

    @property
    def routing_keys(self) -> list[str]:
        return [constants.LEGACY_CASE_PREFIX + "CaseCreated"]

    def handle(self, event: Event):
        fields = event.format_changes()
        if "hostname" not in fields or "origin" not in fields:
            self.logger.info(
                f"CaseCreated Event for case {fields['case_number']} missing hostname or origin, skipping..."
            )
            return
        command_instance = self.get_command_instance(event)
        command_instance.create_pdf_for_case_registration(
            case_uuid=event.entity_id,
            fields=fields,
        )
        return
