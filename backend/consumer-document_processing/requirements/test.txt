pytest~=8.1
pytest-spec~=3.0
pytest-cov~=5.0
pytest-mock~=3.3
pip-audit
webtest
liccheck~=0.1

## Code style tools
ruff~=0.3.0
