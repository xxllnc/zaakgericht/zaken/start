# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.events import Event
from minty_amqp.consumer import BaseHandler
from typing import Final

DOCUMENT_PREFIX: Final[str] = "zsnl.v2.zsnl_domains_document.Document."


class DocumentProcessingBaseHandler(BaseHandler):
    """
    Base class for all event handlers in the document processing domain.
    Defines the "domain" property, required by `BaseHandler`.
    """

    @property
    def domain(self) -> str:
        return "zsnl_domains.document"


class SetDocumentSearchTermsRateLimited(DocumentProcessingBaseHandler):
    @property
    def routing_keys(self) -> list[str]:
        return [DOCUMENT_PREFIX + "SearchIndexSetDelayed"]

    def handle(self, event: Event):
        command_instance = self.get_command_instance(event)
        command_instance.set_search_terms_for_document_delayed(
            document_uuid=event.entity_id
        )
        return
