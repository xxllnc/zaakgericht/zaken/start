# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import TestCase, mock
from zsnl_jobs_consumer.consumers import JobsConsumer


class TestJobsConsumer(TestCase):
    def test_consumer_routing(self):
        consumer = JobsConsumer.__new__(JobsConsumer)
        consumer.cqrs = mock.Mock()

        consumer._register_routing()

        self.assertEqual(len(consumer._known_handlers), 2)
