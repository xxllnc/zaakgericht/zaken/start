# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.events import Event
from minty_amqp.consumer import BaseHandler

JOBS_ROUTING_KEY_PREFIX = "zsnl.v2.zsnl_domains_jobs.Job"


class JobsBaseHandler(BaseHandler):
    """
    Base class for all event handlers in the jobs domain. Defines the "domain"
    property, required by `BaseHandler`.
    """

    @property
    def domain(self) -> str:
        return "zsnl_domains.jobs"


class JobCreatedHandler(JobsBaseHandler):
    """
    Handler for "JobCreated" events
    """

    @property
    def routing_keys(self):
        return [
            f"{JOBS_ROUTING_KEY_PREFIX}.JobCreated",
        ]

    def handle(self, event: Event):
        # Process "job description" and store a list of objects to work on
        # - leads to JobPrepared event
        command_instance = self.get_command_instance(event)

        command_instance.prepare_job(job_uuid=event.entity_id)

        return


class JobProcessedHandler(JobsBaseHandler):
    """
    Handler for JobProcessed events

    Runs the "Job Finished" command, which collects results from the different
    batches and make output files.
    """

    @property
    def routing_keys(self) -> list[str]:
        return [f"{JOBS_ROUTING_KEY_PREFIX}.JobProcessed"]

    def handle(self, event: Event) -> None:
        command_instance = self.get_command_instance(event)

        command_instance.finish_job(job_uuid=event.entity_id)

        return
