# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import requests
from collections import namedtuple
from json.decoder import JSONDecodeError
from minty.exceptions import ConfigurationConflict
from minty_amqp.consumer import BaseConsumer
from unittest import mock
from uuid import uuid4
from zsnl_amqp_consumers.notifications.consumer import (
    EmailNotificationConsumer,
)
from zsnl_amqp_consumers.repository import CaseInformation

SubjectInformation = namedtuple("SubjectInformation", "id display_name uuid")
COMMUNICATION_PREFIX = "zsnl.v2.zsnl_domains_communication."
DOCUMENT_PREFIX = "zsnl.v2.zsnl_domains_document.Document."
SUBJECT_RELATION_PREFIX = (
    "zsnl.v2.zsnl_domains_case_management.SubjectRelation."
)
TASK_PREFIX = "zsnl.v2.zsnl_domains_case_management.Task."
CASE_PREFIX = "zsnl.v2.zsnl_domains_case_management.Case."


class TestEmailNotificationConsumer:
    def setup_method(self):
        with mock.patch.object(BaseConsumer, "__init__", lambda x: None):
            mock_cqrs = mock.MagicMock()
            self.email_notification_cons = EmailNotificationConsumer()

            self.email_notification_cons.cqrs = mock_cqrs

    def test_consumer(self):
        email_events_consumer = EmailNotificationConsumer(
            queue="fake_queue_name",
            exchange="fake_exchange",
            cqrs=mock.MagicMock(),
            qos_prefetch_count=2,
            dead_letter_config={},
        )

        rk = email_events_consumer.routing_keys
        assert (
            set(rk)
            - {
                CASE_PREFIX + "CaseAssigneeChanged",
                COMMUNICATION_PREFIX + "ContactMoment.ContactMomentCreated",
                COMMUNICATION_PREFIX
                + "ExternalMessage.ExternalMessageCreated",
                DOCUMENT_PREFIX + "DocumentAddedToCase",
                DOCUMENT_PREFIX + "DocumentCreated",
                DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
                DOCUMENT_PREFIX + "LabelsApplied",
                DOCUMENT_PREFIX + "LabelsRemoved",
                SUBJECT_RELATION_PREFIX + "SubjectRelationCreated",
                SUBJECT_RELATION_PREFIX + "SubjectRelationDeleted",
                SUBJECT_RELATION_PREFIX + "SubjectRelationUpdated",
                TASK_PREFIX + "TaskCompletionSet",
                TASK_PREFIX + "TaskCreated",
                TASK_PREFIX + "TaskDeleted",
                TASK_PREFIX + "TaskUpdated",
            }
            == set()
        )

    def test_email_notification_cons_call_configuration_conflict(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "content",
                    "new_value": "some content",
                    "old_value": None,
                }
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons.cqrs.infrastructure_factory.get_infrastructure.side_effect = ConfigurationConflict
        self.email_notification_cons(message=mock_message)

        mock_message.reject.assert_called_once()

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call_document_accepted(self, get_case, call_handler):
        case_uuid = uuid4()
        get_case.return_value = CaseInformation(
            id=123, confidentiality=False, uuid=case_uuid
        )
        user_uuid = uuid4()

        call_handler.return_value = True
        mock_message = mock.MagicMock()

        mock_message.json.return_value = {
            "changes": [
                {"key": "accepted", "new_value": True, "old_value": False},
                {
                    "key": "case_uuid",
                    "new_value": str(case_uuid),
                    "old_value": None,
                },
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "DocumentAccepted",
            "context": "dev.zaaksysteem.dev",
        }
        event = {
            "changes": [
                {"key": "accepted", "new_value": True, "old_value": False},
                {
                    "key": "case_uuid",
                    "new_value": str(case_uuid),
                    "old_value": None,
                },
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "DocumentAccepted",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message["event_name"] = "DocumentAccepted"
        call_handler.assert_called_with(
            event=event, case_uuid=str(case_uuid), context=event["context"]
        )

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call_task_created(self, get_case, call_handler):
        case_uuid = uuid4()
        get_case.return_value = CaseInformation(
            id=123, confidentiality=False, uuid=case_uuid
        )
        user_uuid = uuid4()

        call_handler.return_value = True
        mock_message = mock.MagicMock()

        mock_message.json.return_value = {
            "changes": [
                {"key": "accepted", "new_value": True, "old_value": False},
            ],
            "entity_data": {
                "case": {
                    "id": str(case_uuid),
                    "type": "case",
                },
            },
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "TaskCreated",
            "context": "dev.zaaksysteem.dev",
        }
        event = {
            "changes": [
                {"key": "accepted", "new_value": True, "old_value": False},
            ],
            "entity_data": {
                "case": {
                    "id": str(case_uuid),
                    "type": "case",
                },
            },
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "TaskCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message["event_name"] = "TaskCreated"
        call_handler.assert_called_with(
            event=event, case_uuid=str(case_uuid), context=event["context"]
        )

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call(self, get_case, call_handler):
        get_case.return_value = (
            CaseInformation(id=123, confidentiality=False, uuid=uuid4()),
            None,
        )
        user_uuid = uuid4()
        call_handler.return_value = True
        mock_message = mock.MagicMock()
        uuid = uuid4()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "new_value": str(uuid),
                    "old_value": None,
                }
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

        mock_message.reset_mock()
        mock_message.json.return_value = {
            "changes": [
                {"key": "case_uuid", "new_value": str(uuid), "old_value": None}
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "DocumentCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    def test_consumer_call_no_case(self, call_handler):
        user_uuid = uuid4()
        call_handler.return_value = True
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._discard_message"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call_request_failed(
        self, get_case, call_handler, discard_message
    ):
        get_case.return_value = (
            CaseInformation(id=123, confidentiality=False, uuid=uuid4()),
            None,
        )
        call_handler.return_value = False
        mock_message = mock.MagicMock()
        uuid = uuid4()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "new_value": str(uuid),
                    "old_value": None,
                }
            ],
            "entity_data": {},
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        discard_message.assert_called_once()

    def test_email_notification_cons_corrupted_message(self):
        mock_message = mock.MagicMock()
        mock_message.json.side_effect = JSONDecodeError(
            msg="tes", doc="", pos=123
        )

        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    def test_email_notification_cons_corrupted_message_no_context(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "name": "CaseTargetCompletionDateSet"
        }

        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    def test_reject_message(self):
        mock_message = mock.MagicMock()
        self.email_notification_cons._reject_and_retry_message(
            message=mock_message, logline="error"
        )
        mock_message.reject.assert_called()

    def test_check_message(self):
        mock_message = mock.MagicMock()
        uuid = uuid4()
        mock_event = {
            "event_name": "mock_message",
            "context": "message_context",
            "user_uuid": uuid,
        }
        mock_message.json.return_value = mock_event
        event = self.email_notification_cons._check_message(mock_message)

        assert mock_event == event

        # missing name
        mock_message = mock.MagicMock()
        mock_event_no_name = {"context": "message_context"}
        mock_message.json.return_value = mock_event_no_name
        event = self.email_notification_cons._check_message(mock_message)
        assert event is None
        mock_message.ack.assert_called()

        # corrupted json
        mock_message = mock.MagicMock()
        mock_message.json.side_effect = JSONDecodeError(
            msg="tes", doc="", pos=123
        )
        event = self.email_notification_cons._check_message(mock_message)
        assert event is None
        mock_message.ack.assert_called()

    def test_get_database_session(self):
        mock_message = mock.MagicMock()
        context = "dev.zaaksysteem.nl"
        self.email_notification_cons.cqrs.infrastructure_factory.get_infrastructure.return_value = "database_session"

        session = self.email_notification_cons._get_database_session(
            context=context, message=mock_message
        )
        assert session == "database_session"

    def test_get_database_session_config_conflict(self):
        mock_message = mock.MagicMock()
        context = "dev.zaaksysteem.nl"
        self.email_notification_cons.cqrs.infrastructure_factory.get_infrastructure.side_effect = ConfigurationConflict

        session = self.email_notification_cons._get_database_session(
            context=context, message=mock_message
        )
        assert session is None
        mock_message.reject.asser_called()

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_succesfull(self, mock_requests):
        mock_requests().status_code = 200
        mock_requests().json.return_value = {"response": "from_request"}
        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{case_uuid!s}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
                "Host": context,
            },
            timeout=15,
        )
        assert success is True

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_not_succesfull(
        self, mock_requests
    ):
        mock_requests().status_code = 400
        mock_requests().json.return_value = {"response": "from_request"}

        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{case_uuid!s}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
                "Host": context,
            },
            timeout=15,
        )
        assert success is False

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_exception(self, mock_requests):
        mock_requests.side_effect = requests.exceptions.RequestException

        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{case_uuid!s}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
                "Host": context,
            },
            timeout=15,
        )
        assert success is False

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_not_succesfull_invalid_json(
        self, mock_requests
    ):
        mock_requests().status_code = 400
        mock_requests().json.side_effect = ValueError
        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{case_uuid!s}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
                "Host": context,
            },
            timeout=15,
        )
        assert success is False

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    def test_consumer_call_case_assignee_changed(self, call_handler):
        case_uuid = uuid4()
        user_uuid = uuid4()

        call_handler.return_value = True
        mock_message = mock.MagicMock()

        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "assignee",
                    "new_value": {
                        "correspondence_street": None,
                        "correspondence_zipcode": None,
                        "department": "Backoffice",
                        "email": "devnull@zaaksysteem.nl",
                        "family_name": "Mïn",
                        "first_names": "Ad",
                        "full_name": "Ad Mïn",
                        "house_number": None,
                        "id": "1",
                        "initials": "A.",
                        "last_name": "Mïn",
                        "magic_string_prefix": None,
                        "name": "Ad Mïn",
                        "phone_number": "0612345678",
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "residence_house_number": None,
                        "residence_street": None,
                        "residence_zipcode": None,
                        "role": None,
                        "status": "Actief",
                        "street": None,
                        "subject_type": "medewerker",
                        "surname": "Mïn",
                        "title": None,
                        "type": "employee",
                        "unique_name": "betrokkene-medewerker-1",
                        "uuid": "bb402e57-bd86-4fe0-b2e2-21f2a552f1c9",
                        "zipcode": None,
                    },
                    "old_value": None,
                }
            ],
            "entity_data": {"uuid": str(case_uuid)},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "CaseAssigneeChanged",
            "entity_type": "Case",
            "entity_id": str(case_uuid),
            "context": "dev.zaaksysteem.dev",
        }
        event = {
            "changes": [
                {
                    "key": "assignee",
                    "new_value": {
                        "correspondence_street": None,
                        "correspondence_zipcode": None,
                        "department": "Backoffice",
                        "email": "devnull@zaaksysteem.nl",
                        "family_name": "Mïn",
                        "first_names": "Ad",
                        "full_name": "Ad Mïn",
                        "house_number": None,
                        "id": "1",
                        "initials": "A.",
                        "last_name": "Mïn",
                        "magic_string_prefix": None,
                        "name": "Ad Mïn",
                        "phone_number": "0612345678",
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "residence_house_number": None,
                        "residence_street": None,
                        "residence_zipcode": None,
                        "role": None,
                        "status": "Actief",
                        "street": None,
                        "subject_type": "medewerker",
                        "surname": "Mïn",
                        "title": None,
                        "type": "employee",
                        "unique_name": "betrokkene-medewerker-1",
                        "uuid": "bb402e57-bd86-4fe0-b2e2-21f2a552f1c9",
                        "zipcode": None,
                    },
                    "old_value": None,
                }
            ],
            "entity_data": {"uuid": str(case_uuid)},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "CaseAssigneeChanged",
            "entity_id": str(case_uuid),
            "entity_type": "Case",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message["event_name"] = "CaseAssigneeChanged"
        call_handler.assert_called_with(
            event=event, case_uuid=str(case_uuid), context=event["context"]
        )
