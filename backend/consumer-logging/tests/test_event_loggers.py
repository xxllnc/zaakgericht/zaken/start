# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import random
from minty.exceptions import NotFound
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_amqp_consumers.event_logger_admin_catalog import (
    AdminCatalogBase,
    CaseTypeDeleted,
    CaseTypeOnlineStatusChanged,
    CaseTypeVersionUpdated,
    DocumentTemplateLogging,
    EmailTemplateLogging,
    FolderCreated,
    FolderDeleted,
    FolderEntryMoved,
)
from zsnl_amqp_consumers.event_logger_archive_request import ArchiveRequest
from zsnl_amqp_consumers.event_logger_attribute import (
    AttributeBase,
    AttributeCreated,
    AttributeDeleted,
    AttributeEdited,
)
from zsnl_amqp_consumers.event_logger_auth0 import (
    Auth0Base,
    Auth0InterfaceNotActive,
    Auth0UserCreated,
    Auth0UserMarkedAsDeleted,
    Auth0UserNotActive,
)
from zsnl_amqp_consumers.event_logger_case import (
    AddValidsignTimelineEntry,
    CaseAllocationSet,
    CaseAssigneeChanged,
    CaseAttributeUpdated,
    CaseCompletionDateSet,
    CaseCoordinatorSet,
    CaseCreated,
    CaseDeleted,
    CaseDestructionDateCleared,
    CaseDestructionDateRecalculated,
    CaseDestructionDateSet,
    CaseManagementBase,
    CasePaused,
    CaseRegistrationDateSet,
    CaseResultSet,
    CaseResumed,
    CaseStatusSet,
    CaseTargetCompletionDateSet,
)
from zsnl_amqp_consumers.event_logger_case_relation import (
    CaseRelationBase,
    CaseRelationCreated,
    CaseRelationDeleted,
)
from zsnl_amqp_consumers.event_logger_communication import (
    CommunicationBase,
    ContactMomentCreated,
    ExternalMessageCreated,
    MessageDeleted,
    NoteCreated,
    ThreadToCaseLinked,
)
from zsnl_amqp_consumers.event_logger_custom_object import (
    CustomObjectBase,
    CustomObjectCreated,
    CustomObjectDeleted,
    CustomObjectRelatedTo,
    CustomObjectRetrieved,
    CustomObjectUnrelatedFrom,
    CustomObjectUpdated,
)
from zsnl_amqp_consumers.event_logger_custom_object_type import (
    CustomObjectTypeBase,
    CustomObjectTypeCreated,
    CustomObjectTypeDeleted,
    CustomObjectTypeUpdated,
)
from zsnl_amqp_consumers.event_logger_document import (
    DocumentAddedToCase,
    DocumentAssignedToRole,
    DocumentAssignedToUser,
    DocumentAssignmentRejected,
    DocumentBase,
    DocumentCreated,
    DocumentDeleted,
    DocumentDownloaded,
    DocumentUpdated,
)
from zsnl_amqp_consumers.event_logger_jobs import (
    JobCancelled,
    JobCreated,
    JobDeleted,
    JobErrorsDownloaded,
    JobResultsDownloaded,
)
from zsnl_amqp_consumers.event_logger_label import (
    LabelApplied,
    LabelBase,
    LabelDeleted,
)
from zsnl_amqp_consumers.event_logger_objecttype import (
    ObjectTypeBase,
    ObjectTypeDeleted,
)
from zsnl_amqp_consumers.event_logger_subject import (
    BsnRetrieved,
    ContactInformationSaved,
    ContactViewed,
    NonAuthenticBsnUpdated,
    NonAuthenticSedulaUpdated,
    PersonUpdated,
    SubjectBase,
)
from zsnl_amqp_consumers.event_logger_subject_relation import (
    SubjectRelationBase,
    SubjectRelationCreated,
    SubjectRelationDeleted,
    SubjectRelationUpdated,
)
from zsnl_amqp_consumers.repository import (
    AttributeInformation,
    CaseInformation,
    CaseTypeInformation,
    CaseTypeVersionInformation,
    CatalogFolderInformation,
    Contact,
    CustomObjectInformation,
    CustomObjectTypeInformation,
    DepartmentInformation,
    DocumentInformation,
    DocumentTemplateInformation,
    EmailTemplateInformation,
    EmployeeInformation,
    FileInformation,
    ObjectTypeInformation,
    OrganistionInformation,
    PersonInformation,
    RoleInformation,
    SubjectRelationInformation,
    UserInformation,
)
from zsnl_domains.database.schema import Logging


class TestEventLoggerCase:
    def test_case_base_generate_event_parameters(self):
        base = CaseManagementBase()
        with pytest.raises(NotImplementedError):
            session = mock.MagicMock()
            base.generate_event_parameters(
                session=session, entity_data={}, case_id=12345
            )

    def test_casetargetcompletiondateset_generate_event_parameters(self):
        logger = CaseTargetCompletionDateSet()

        case_id = 1234
        entity_data = {"target_completion_date": "2019-12-23"}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "target_date": "23-12-2019",
            "case_id": case_id,
        }

    def test_caseregistrationdateset_generate_event_parameters(self):
        logger = CaseRegistrationDateSet()

        case_id = 1234
        entity_data = {"registration_date": "2019-12-23"}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "registration_date": "23-12-2019",
            "case_id": case_id,
        }

    def test_casecompletiondateset_generate_event_parameters(self):
        logger = CaseCompletionDateSet()
        case_id = 1234
        entity_data = {"completion_date": "2019-12-23"}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "target_date": "23-12-2019",
            "case_id": case_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_assignee_set(self, mock_get_case, mock_get_user):
        case_event = CaseAssigneeChanged()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "assignee",
                    "new_value": {
                        "department": {
                            "description": "",
                            "name": "Backoffice",
                            "parent": None,
                            "uuid": str(uuid4()),
                        },
                        "group_ids": [2],
                        "id": 1,
                        "last_modified": "2021-03-09",
                        "nobody": False,
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [1, 12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": True,
                        "username": "admin",
                        "uuid": str(uuid4()),
                    },
                    "old_value": {
                        "department": None,
                        "group_ids": [2],
                        "id": 1,
                        "last_modified": "2021-03-09",
                        "nobody": False,
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [1, 12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": True,
                        "username": "admin",
                        "uuid": str(uuid4()),
                    },
                },
                {
                    "key": "send_email_to_assignee",
                    "new_value": False,
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseAssigneeChanged",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Zaak 1234 geaccepteerd door name"
        assert log_record.event_type == "case/accept"

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_case.get_attribute_by_magic_string"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_attribute_updated(self, mock_get_case, mock_get_attribute):
        attribute_updated_handler = CaseAttributeUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "custom_fields",
                    "new_value": {
                        "relation_field": {
                            "specifics": None,
                            "type": "relationship",
                            "value": [
                                {
                                    "specifics": {
                                        "metadata": {
                                            "description": "subtitel : ",
                                            "summary": "titel:  112233",
                                        },
                                        "relationship_type": "custom_object",
                                    },
                                    "type": "relationship",
                                    "value": "5ee8658f-ae4a-4759-9abf-d67bba3e8b96",
                                }
                            ],
                        }
                    },
                    "old_value": {},
                }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": "1846262d-1499-4015-92d9-f7f2d20f0389",
            "created_date": "2022-02-25T08:17:52.685723",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": "39d1b4b6-e9ad-4e78-a9e0-114ccb335931",
            "entity_type": "Case",
            "event_name": "CustomFieldUpdated",
            "id": "a1d4389f-201c-468d-a9ec-1ff4c73cddf9",
            "user_uuid": "7145766b-472c-4316-9634-6470ea51c4c9",
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_attribute.return_value = AttributeInformation(
            id=4433, name="attr_name", magic_string="magic_string"
        )
        log_record = attribute_updated_handler(session=session, event=event)
        assert log_record.component == "kenmerk"
        assert log_record.onderwerp == 'Kenmerk "relation_field" gewijzigd'
        assert log_record.event_type == "case/attribute/update"
        assert log_record.event_data == {
            "case_id": 1234,
            "attribute_id": 4433,
            "attribute_name": "relation_field",
            "attribute_value": {
                "specifics": None,
                "type": "relationship",
                "value": [
                    {
                        "specifics": {
                            "metadata": {
                                "description": "subtitel : ",
                                "summary": "titel:  112233",
                            },
                            "relationship_type": "custom_object",
                        },
                        "type": "relationship",
                        "value": "5ee8658f-ae4a-4759-9abf-d67bba3e8b96",
                    }
                ],
            },
            "correlation_id": "1846262d-1499-4015-92d9-f7f2d20f0389",
        }

    def test_case_attribute_updated_invalid_number_of_fields(self):
        attribute_updated_handler = CaseAttributeUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "custom_fields",
                    "new_value": {
                        "the_first_field": {"value": "abc"},
                        "the_second_field": {"value": "textvalue"},
                    },
                    "old_value": {},
                }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": "1846262d-1499-4015-92d9-f7f2d20f0389",
            "created_date": "2022-02-25T08:17:52.685723",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": "39d1b4b6-e9ad-4e78-a9e0-114ccb335931",
            "entity_type": "Case",
            "event_name": "CustomFieldUpdated",
            "id": "a1d4389f-201c-468d-a9ec-1ff4c73cddf9",
            "user_uuid": "7145766b-472c-4316-9634-6470ea51c4c9",
        }
        with pytest.raises(AttributeError) as error:
            attribute_updated_handler(session=session, event=event)
        assert error.value.args == (
            "Only one attribute should be changed for each CaseAttributeUpdated event",
        )

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_coordinator_set(self, mock_get_case, mock_get_user):
        case_event = CaseCoordinatorSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "coordinator",
                    "new_value": {
                        "department": {
                            "description": "",
                            "name": "Backoffice",
                            "parent": None,
                            "uuid": str(uuid4()),
                        },
                        "group_ids": [2],
                        "id": 34,
                        "last_modified": "2021-03-15",
                        "nobody": False,
                        "properties": {
                            "displayname": "auser",
                            "givenname": "auser",
                            "initials": "a",
                            "mail": "auser@mintlab.nl",
                            "sn": "testname",
                            "title": "Administrator",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": False,
                        "username": "auser",
                        "uuid": str(uuid4()),
                    },
                    "old_value": {
                        "department": None,
                        "group_ids": [2],
                        "id": 1,
                        "last_modified": "2021-03-09",
                        "nobody": False,
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [1, 12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": True,
                        "username": "admin",
                        "uuid": "bb402e57-bd86-4fe0-b2e2-21f2a552f1c9",
                    },
                }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:10:51.085405",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseCoordinatorSet",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == 'Coordinator ingesteld op "name"'
        assert log_record.event_type == "case/relation/update"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_status_set(self, mock_get_case, mock_get_user):
        case_event = CaseStatusSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "status", "old_value": "new", "new_value": "open"}
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseStatusSet",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Status voor zaak: 1234 gewijzigd naar: open"
        )
        assert log_record.event_type == "case/update/status"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_paused(self, mock_get_case, mock_get_user):
        case_event = CasePaused()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "suspension_reason",
                    "new_value": "a reason for suspension",
                    "old_value": "",
                },
                {
                    "key": "stalled_since_date",
                    "new_value": "2019-02-01",
                    "old_value": "",
                },
                {
                    "key": "stalled_until_date",
                    "new_value": "2019-03-02",
                    "old_value": "",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CasePaused",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Zaak 1234 opgeschort tot 2019-03-02: a reason for suspension"
        )
        assert log_record.event_type == "case/suspend"

    def test_generate_subject_case_paused(self):
        case = CasePaused()
        event_parameters = {
            "case_id": 1234,
            "reason": "for a reason",
            "stalled_since": "20-01-2019",
            "stalled_until": "23-01-2019",
        }
        expected = "Zaak 1234 opgeschort tot 23-01-2019: for a reason"
        result = case._generate_subject(event_parameters=event_parameters)
        assert result == expected

    def test_generate_subject_case_paused_indefinite_date(self):
        case = CasePaused()
        event_parameters = {
            "case_id": 1234,
            "reason": "for a reason",
            "stalled_since": "20-01-2019",
        }
        expected = "Zaak 1234 opgeschort voor onbepaalde tijd: for a reason"
        result = case._generate_subject(event_parameters=event_parameters)
        assert result == expected

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_resumed(self, mock_get_case, mock_get_user):
        case_event = CaseResumed()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "resume_reason",
                    "new_value": "a reason for resuming",
                    "old_value": "",
                },
                {
                    "key": "stalled_since_date",
                    "new_value": "2019-02-01",
                    "old_value": "",
                },
                {
                    "key": "stalled_until_date",
                    "new_value": "2019-03-02",
                    "old_value": "",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseResumed",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp == "Zaak 1234 hervat: a reason for resuming"
        )
        assert log_record.event_type == "case/resume"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_management_base_call(self, mock_get_case, mock_get_user):
        session = mock.MagicMock()
        case = CaseRegistrationDateSet()

        event = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "context": "localhost:6543",
            "correlation_id": "1322e1d8-7741-4f7f-9f4f-2cc95ecca010",
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "domain": "zsnl_domains.case_management",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "entity_type": "Case",
            "event_name": "CaseRegistrationDateSet",
            "id": "2f971350-3db9-431c-b724-2d1bc8279a72",
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case(session=session, event=event)
        assert log_record.zaak_id == 1234
        assert log_record.created_by_name_cache == "name"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_management_base_call_no_data(
        self, mock_get_case, mock_get_user
    ):
        session = mock.MagicMock()
        case = CaseRegistrationDateSet()
        event = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "context": "localhost:6543",
            "correlation_id": "1322e1d8-7741-4f7f-9f4f-2cc95ecca010",
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "domain": "zsnl_domains.case_management",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "entity_type": "Case",
            "event_name": "CaseRegistrationDateSet",
            "id": "2f971350-3db9-431c-b724-2d1bc8279a72",
            "entity_data": {},
        }

        mock_get_case.side_effect = NoResultFound
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        with pytest.raises(NoResultFound):
            case(session=session, event=event)

    def test_generate_subject(self):
        case = CaseRegistrationDateSet()
        event = {
            "parameters": {"case_id": 1234, "registration_date": "23-01-2019"}
        }
        expected = (
            "Registratiedatum voor zaak: 1234 gewijzigd naar: 23-01-2019"
        )
        result = case._generate_subject(event_parameters=event["parameters"])
        assert result == expected

    def test_create_logging_record(self):
        case = CaseRegistrationDateSet()
        user_info = UserInformation(
            id=444, display_name="name", type="medewerker"
        )

        log_record = case._create_logging_record(
            user_info=user_info,
            event_type="case/publish",
            subject="always something to log",
            component="zaak",
            component_id=1234,
            case_id=4567,
            created_date="2019-12-21",
            event_data={"event": "data"},
        )
        assert isinstance(log_record, Logging)
        assert log_record.event_data == {"event": "data"}
        assert log_record.created_by_name_cache == "name"
        assert log_record.created == "2019-12-21"
        assert log_record.last_modified == "2019-12-21"
        assert log_record.created_by == "betrokkene-medewerker-444"
        assert log_record.zaak_id == 4567

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_role")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_department")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_allocation_set(
        self, mock_get_case, mock_get_user, mock_get_department, mock_get_role
    ):
        case_event = CaseAllocationSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "department",
                    "old_value": None,
                    "new_value": {
                        "entity_id": str(uuid4()),
                        "type": "Department",
                    },
                },
                {
                    "key": "role",
                    "old_value": None,
                    "new_value": {"entity_id": str(uuid4()), "type": "Role"},
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseAssigneeChanged",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_department.return_value = DepartmentInformation(
            id=3, name="Frontoffice"
        )
        mock_get_role.return_value = RoleInformation(
            id=1, name="Administrator"
        )
        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Toewijzing gewijzigd naar afdeling 'Frontoffice' en rol 'Administrator'"
        )
        assert log_record.event_type == "case/update/allocation"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_role")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_department")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_allocation_set_uuid_dept_and_rol(
        self, mock_get_case, mock_get_user, mock_get_department, mock_get_role
    ):
        case_event = CaseAllocationSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "department",
                    "old_value": None,
                    "new_value": {"uuid": str(uuid4()), "type": "Department"},
                },
                {
                    "key": "role",
                    "old_value": None,
                    "new_value": {"uuid": str(uuid4()), "type": "Role"},
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseAssigneeChanged",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_department.return_value = DepartmentInformation(
            id=3, name="Frontoffice"
        )
        mock_get_role.return_value = RoleInformation(
            id=1, name="Administrator"
        )
        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Toewijzing gewijzigd naar afdeling 'Frontoffice' en rol 'Administrator'"
        )
        assert log_record.event_type == "case/update/allocation"

    def test_casedestructiondatecleared_generate_event_parameters(self):
        logger = CaseDestructionDateCleared()

        case_id = 1234
        entity_data = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": None,
                    "old_value": None,
                }
            ]
        }
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": case_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_casedestructiondatecleared_with_reason_generate_event_parameters(
        self, mock_get_case
    ):
        logger = CaseDestructionDateCleared()
        event = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": None,
                    "old_value": "2022-08-23",
                },
                {
                    "key": "destruction_reason",
                    "new_value": {
                        "label": None,
                        "reason": "Reason for destruction",
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateCleared",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Vernietigingsdatum voor zaak: 1234 gewist: Reason for destruction"
        )
        assert log_record.event_type == "case/update/purge_date"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_casedestructiondaterecalculated_with_reason_generate_event_parameters(
        self, mock_get_case
    ):
        logger = CaseDestructionDateRecalculated()
        event = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": "2022-08-23",
                    "old_value": "2022-08-23",
                },
                {
                    "key": "destruction_reason",
                    "new_value": {
                        "label": "4 weken",
                        "reason": "Reason for destruction",
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateRecalculated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Vernietigingsdatum voor zaak 1234 herberekend naar 4 weken: Reason for destruction"
        )
        assert log_record.event_type == "case/update/purge_date"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_casedestructiondateset_with_reason_generate_event_parameters(
        self, mock_get_case
    ):
        logger = CaseDestructionDateSet()
        event = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": "2022-10-20",
                    "old_value": "2022-08-23",
                },
                {
                    "key": "destruction_reason",
                    "new_value": {
                        "label": None,
                        "reason": "Reason for destruction",
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateSet",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Vernietigingsdatum voor zaak: 1234 gewijzigd naar 20-10-2022 : Reason for destruction"
        )
        assert log_record.event_type == "case/update/completion_date"

    def test_casedestructiondateset_generate_event_parameters(self):
        logger = CaseDestructionDateSet()

        case_id = 1234
        entity_data = {
            "destruction_date": "2022-05-16",
        }
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": case_id,
            "destruction_date": "16-05-2022",
        }

    def test_case_update_result_event_parameters(self):
        logger = CaseResultSet()

        case_id = 1234
        entity_data = {
            "archival_state": "vernietigen",
            "archival_state_old": "vernietigen",
            "result": {
                "archival_attributes": None,
                "result": "aangegaan",
                "result_id": 113,
            },
            "result_old": {
                "archival_attributes": {
                    "selection_list": "",
                    "state": "vernietigen",
                },
                "result": "aangegaan",
                "result_id": 113,
            },
        }
        session = mock.MagicMock()
        result_mock = mock.MagicMock()
        result_mock.configure_mock(label="label_value")
        session.execute().fetchone.return_value = result_mock

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": 1234,
            "result_new": "aangegaan",
            "result_new_label": "label_value",
            "result_old": "aangegaan",
        }

    def test_case_set_result_event_parameters(self):
        logger = CaseResultSet()

        case_id = 1234
        entity_data = {
            "archival_state": "vernietigen",
            "archival_state_old": "vernietigen",
            "result": {
                "archival_attributes": None,
                "result": "aangegaan",
                "result_id": 113,
            },
        }
        session = mock.MagicMock()
        result_mock = mock.MagicMock()
        result_mock.configure_mock(label="label_value")
        session.execute().fetchone.return_value = result_mock

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": 1234,
            "result_new": "aangegaan",
            "result_new_label": "label_value",
        }


class TestEventLoggerCaseType:
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type")
    def test_case_type_online_status_changed_activated(
        self, mock_get_case_type, mock_get_user
    ):
        session = mock.MagicMock()
        case_type_event = CaseTypeOnlineStatusChanged()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {"key": "active", "old_value": False, "new_value": True},
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for activating",
                },
            ],
            "entity_data": {},
        }
        mock_get_case_type.return_value = CaseTypeInformation(
            id=1234, title="case_type_title"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" geactiveerd: reason for activating'
        )
        assert log_record.event_type == "casetype/publish"

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type")
    def test_case_type_online_status_changed_deactivated(
        self, mock_get_case_type, mock_get_user
    ):
        session = mock.MagicMock()
        case_type_event = CaseTypeOnlineStatusChanged()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {"key": "active", "old_value": True, "new_value": False},
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for de-activating",
                },
            ],
            "entity_data": {},
        }
        mock_get_case_type.return_value = CaseTypeInformation(
            id=1234, title="case_type_title"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" gedeactiveerd: reason for de-activating'
        )
        assert log_record.event_type == "casetype/unpublish"

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type")
    def test_case_type_deleted(self, mock_get_case_type, mock_get_user):
        session = mock.MagicMock()
        case_type_event = CaseTypeDeleted()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for de-activating",
                }
            ],
            "entity_data": {},
        }
        mock_get_case_type.return_value = CaseTypeInformation(
            id=1234, title="case_type_title"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" verwijderd: reason for de-activating'
        )
        assert log_record.event_type == "casetype/remove"

    def test_generate_event_parameters_not_implemented(self):
        logger = AdminCatalogBase()
        with pytest.raises(NotImplementedError):
            logger.generate_event_parameters(entity_data={}, case_type_info={})

    def test_get_event_type_format_not_implemented(self):
        logger = AdminCatalogBase()
        with pytest.raises(NotImplementedError):
            logger.get_event_type_format(event_params={})

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_catalog_folder"
    )
    def test_folder_entry_moved(self, mock_get_catalog_folder, mock_get_user):
        session = mock.MagicMock()
        moved_event = FolderEntryMoved()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_folder = CatalogFolderInformation(
            id=10, uuid=str(uuid4()), name="Test Folder"
        )

        # Move single item to a folder

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [{"key": "folder_id", "new_value": 2, "old_value": 2}],
            "entity_data": {"entry_type": "folder", "name": "test5"},
        }

        mock_get_catalog_folder.return_value = mock_folder

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert (
            log_record.onderwerp == "Map 'test5' verplaatst naar 'Test Folder'"
        )
        assert log_record.component_id == mock_folder.id
        assert log_record.zaak_id is None

        assert log_record.event_type == "admin/catalog/folder_entries_moved"
        assert log_record.event_data == {
            "destination_folder_id": mock_folder.id,
            "destination_folder_uuid": mock_folder.uuid,
            "destination_folder_name": f"'{mock_folder.name}'",
            "entry_type": "folder",
            "name": "test5",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_catalog_folder"
    )
    def test_something_unknown_moved(
        self, mock_get_catalog_folder, mock_get_user
    ):
        session = mock.MagicMock()
        moved_event = FolderEntryMoved()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_folder = CatalogFolderInformation(
            id=10, uuid=str(uuid4()), name="Test Folder"
        )

        # Move single item to a folder

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [{"key": "folder_id", "new_value": 2, "old_value": 2}],
            "entity_data": {
                "entry_type": "something_unknown",
                "name": "test5",
            },
        }

        mock_get_catalog_folder.return_value = mock_folder

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert (
            log_record.onderwerp
            == "something_unknown 'test5' verplaatst naar 'Test Folder'"
        )
        assert log_record.component_id == mock_folder.id
        assert log_record.zaak_id is None

        assert log_record.event_type == "admin/catalog/folder_entries_moved"
        assert log_record.event_data == {
            "destination_folder_id": mock_folder.id,
            "destination_folder_uuid": mock_folder.uuid,
            "destination_folder_name": f"'{mock_folder.name}'",
            "entry_type": "something_unknown",
            "name": "test5",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    def test_folder_entry_deleted(self, mock_get_user):
        session = mock.MagicMock()
        moved_event = FolderDeleted()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        entity_id = str(uuid4())
        user_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": user_uuid,
            "entity_id": entity_id,
            "changes": [
                {
                    "key": "commit_message",
                    "new_value": "test delete",
                    "old_value": "delete",
                }
            ],
            "entity_data": {"entry_type": "folder", "name": "test5"},
        }

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert log_record.onderwerp == "Folder 'test5' verwijderd: test delete"
        assert log_record.component_id is None
        assert log_record.zaak_id is None

        assert log_record.event_type == "folder/remove"
        assert log_record.event_data == {
            "name": "test5",
            "reason": "test delete",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    def test_folder_created(self, mock_get_user):
        session = mock.MagicMock()
        moved_event = FolderCreated()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        entity_id = str(uuid4())
        user_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": user_uuid,
            "entity_id": entity_id,
            "changes": [
                {"key": "parent_uuid", "old_value": None, "new_value": None},
                {"key": "name", "old_value": None, "new_value": "test5"},
            ],
            "entity_data": {},
        }

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert log_record.onderwerp == "Folder 'test5' toegevoegd"
        assert log_record.component_id is None
        assert log_record.zaak_id is None

        assert log_record.event_type == "folder/create"
        assert log_record.event_data == {"name": "test5"}

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_catalog_folder"
    )
    def test_folder_entry_moved_to_root_folder(
        self, mock_get_catalog_folder, mock_get_user
    ):
        session = mock.MagicMock()
        moved_event = FolderEntryMoved()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        # Move single item to a folder

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {"key": "folder_id", "new_value": None, "old_value": 2}
            ],
            "entity_data": {"entry_type": "folder", "name": "test5"},
        }

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert log_record.onderwerp == "Map 'test5' verplaatst naar 'hoofdmap'"
        assert log_record.component_id is None
        assert log_record.zaak_id is None

        assert log_record.event_type == "admin/catalog/folder_entries_moved"
        assert log_record.event_data == {
            "destination_folder_id": None,
            "destination_folder_uuid": None,
            "destination_folder_name": "'hoofdmap'",
            "entry_type": "folder",
            "name": "test5",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type_version"
    )
    def test_case_type_active_version_changed(
        self, mock_get_case_type_version, mock_get_user
    ):
        session = mock.MagicMock()
        case_type_event = CaseTypeVersionUpdated()
        old_version_uuid = str(uuid4())
        new_version_uuid = str(uuid4())

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {
                    "key": "current_version_uuid",
                    "old_value": old_version_uuid,
                    "new_value": new_version_uuid,
                },
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for changing",
                },
            ],
            "entity_data": {},
        }
        mock_get_case_type_version.return_value = CaseTypeVersionInformation(
            case_type_id=1234, title="case_type_title", version=6
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" gewijzigd naar versie 6: reason for changing'
        )
        assert log_record.event_type == "casetype/update/version"


class TestEventLoggerAttribute:
    def test_attribute_base_generate_event_parameters(self):
        base = AttributeBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={}, attribute_info=None, commit_message=""
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_attribute")
    def test_attribute_edited(self, mock_get_attribute, mock_get_user):
        session = mock.MagicMock()
        attribute_event = AttributeEdited()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "public_name",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "type_multiple",
                    "old_value": False,
                    "new_value": True,
                },
                {
                    "key": "attribute_values",
                    "old_value": [],
                    "new_value": [
                        {"value": "a", "active": True, "sort_order": 0},
                        {"value": "b", "active": False, "sort_order": 1},
                    ],
                },
            ],
            "entity_data": {"commit_message": "commit_message"},
        }
        mock_get_attribute.return_value = AttributeInformation(
            id=1234, name="attr_name", magic_string="magic_string"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = attribute_event(session=session, event=event)

        mock_get_attribute.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )
        assert log_record.component == "kenmerk"
        assert (
            log_record.onderwerp
            == "Kenmerk 'magic_string' opgeslagen (commit_message), Opties: a, b(inactief)"
        )
        assert log_record.event_type == "attribute/update"
        assert log_record.created_by_name_cache == "name"

    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_attribute")
    def test_attribute_created(self, mock_get_attribute, mock_get_user):
        session = mock.MagicMock()
        attribute_event = AttributeCreated()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "public_name",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "type_multiple",
                    "old_value": False,
                    "new_value": True,
                },
            ],
            "entity_data": {"commit_message": "commit_message"},
        }
        mock_get_attribute.return_value = AttributeInformation(
            id=1234, name="attr_name", magic_string="magic_string"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = attribute_event(session=session, event=event)

        mock_get_attribute.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )
        assert log_record.component == "kenmerk"
        assert (
            log_record.onderwerp
            == "Kenmerk 'magic_string' toegevoegd: commit_message"
        )
        assert log_record.event_type == "attribute/create"
        assert log_record.created_by_name_cache == "name"

    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_attribute")
    def test_attribute_deleted(self, mock_get_attribute, mock_get_user):
        session = mock.MagicMock()
        attribute_event = AttributeDeleted()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "public_name",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "type_multiple",
                    "old_value": False,
                    "new_value": True,
                },
            ],
            "entity_data": {"commit_message": "commit_message"},
        }
        mock_get_attribute.return_value = AttributeInformation(
            id=1234, name="attr_name", magic_string="magic_string"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = attribute_event(session=session, event=event)

        mock_get_attribute.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )
        assert log_record.component == "kenmerk"
        assert (
            log_record.onderwerp
            == "Kenmerk attr_name (magic string magic_string) verwijderd: commit_message"
        )
        assert log_record.event_type == "attribute/remove"
        assert log_record.created_by_name_cache == "name"


class TestEmailTemplateInformationLogger:
    def setup_method(self):
        self.logger = EmailTemplateLogging()

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_email_template"
    )
    def test_call(self, mock_get_email_template, mock_get_user):
        session = mock.MagicMock()
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_template.return_value = EmailTemplateInformation(
            id=100, label="label1"
        )

        event = {
            "event_name": "EmailTemplateCreated",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "label",
                    "old_value": "template1",
                    "new_value": "template2",
                },
            ],
            "entity_data": {},
        }
        log_record = self.logger(session, event)
        assert log_record.component == "notificatie"
        assert log_record.event_type == "template/email/create"
        assert log_record.component_id == 100
        assert log_record.event_data == {
            "reason": "new_name",
            "template_id": 100,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_email_template"
    )
    def test_call_email_template_deleted(
        self, mock_get_email_template, mock_get_user
    ):
        session = mock.MagicMock()
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_template.return_value = EmailTemplateInformation(
            id=100, label="label1"
        )

        event = {
            "event_name": "EmailTemplateDeleted",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "label",
                    "old_value": "template1",
                    "new_value": "template2",
                },
            ],
            "entity_data": {},
        }
        log_record = self.logger(session, event)
        assert log_record.component == "notificatie"
        assert log_record.event_type == "template/email/remove"
        assert log_record.component_id == 100
        assert log_record.event_data == {
            "reason": "new_name",
            "template_id": 100,
        }

    def test_generate_subject(self):
        subject = self.logger._generate_subject(
            label="template 1",
            reason="new_template",
            event_name="EmailTemplateCreated",
        )
        assert (
            subject == 'E-mailsjabloon "template 1" toegevoegd: new_template'
        )

        subject2 = self.logger._generate_subject(
            label="template 1",
            reason="template edited",
            event_name="EmailTemplateEdited",
        )
        assert (
            subject2
            == 'E-mailsjabloon "template 1" opgeslagen: template edited'
        )

    def test_generate_event_paremeters(self):
        ent_data = {"commit_message": "just because"}
        email_t_info = EmailTemplateInformation(id=100, label="label1")
        event_params = self.logger.generate_event_parameters(
            entity_data=ent_data, email_template_info=email_t_info
        )
        assert event_params == {"reason": "just because", "template_id": 100}


class TestEventLoggerObjectType:
    def test_attribute_base_generate_event_parameters(self):
        base = ObjectTypeBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={}, object_type_info=None, commit_message=""
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_objecttype.get_user")
    def test_call_object_type_deleted(self, mock_get_user):
        session = mock.MagicMock()

        object_type_event = ObjectTypeDeleted()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        event = {
            "event_name": "ObjectTypeDeleted",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "name",
                    "old_value": "objectype1",
                    "new_value": "objectype2",
                },
            ],
            "entity_data": {},
        }
        log_record = object_type_event(session, event)
        assert log_record.component == "object_type"
        assert log_record.event_type == "object/delete"
        assert log_record.component_id is None
        assert log_record.event_data == {
            "object_type": "ObjectType",
            "object_label": "objectype2",
            "reason": "new_name",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_objecttype.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_objecttype.get_object_type")
    def test_call_object_type_other(self, mock_get_object_type, mock_get_user):
        session = mock.MagicMock()

        object_type_event = ObjectTypeDeleted()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_object_type.return_value = ObjectTypeInformation(
            id=100, title="label1"
        )

        event = {
            "event_name": "ObjectTypeCreated",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "new_name",
                },
                {"key": "name", "old_value": None, "new_value": "objectype2"},
            ],
            "entity_data": {},
        }
        object_type_event(session, event)
        mock_get_object_type.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

    def test_generate_subject(self):
        event_params = {
            "object_type": "ObjectType",
            "object_label": "object type 1",
            "reason": "deleted object type",
        }
        object_type_event = ObjectTypeDeleted()
        subject = object_type_event._generate_subject(
            event_parameters=event_params
        )
        assert (
            subject
            == 'ObjectType "object type 1" verwijderd: deleted object type'
        )

        event_params1 = {
            "object_type": "ObjectType",
            "object_label": "object type 2",
            "reason": "object type deleted",
        }
        subject2 = object_type_event._generate_subject(
            event_parameters=event_params1
        )
        assert (
            subject2
            == 'ObjectType "object type 2" verwijderd: object type deleted'
        )

    def test_generate_event_paremeters(self):
        ent_data = {"commit_message": "just because", "name": "test obj"}
        object_type_event = ObjectTypeDeleted()
        event_params = object_type_event.generate_event_parameters(
            entity_data=ent_data, object_type_info=None, commit_message="str"
        )
        assert event_params == {
            "object_type": "ObjectType",
            "object_label": "test obj",
            "reason": "str",
        }


class TestDocumentTemplateInformationLogger:
    def setup_method(self):
        self.logger = DocumentTemplateLogging()

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_document_template"
    )
    def test_call(self, mock_get_document_template, mock_get_user):
        session = mock.MagicMock()
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_document_template.return_value = DocumentTemplateInformation(
            id=100, name="document"
        )

        event = {
            "event_name": "DocumentTemplateCreated",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "new template",
                },
                {"key": "name", "old_value": None, "new_value": "document"},
            ],
            "entity_data": {},
        }
        log_record = self.logger(session, event)
        assert log_record.component == "sjabloon"
        assert log_record.event_type == "template/create"
        assert log_record.component_id == 100
        assert log_record.event_data == {
            "reason": "new template",
            "template_id": 100,
        }

    def test_generate_subject(self):
        subject = self.logger._generate_subject(
            name="document",
            reason="template created",
            event_name="DocumentTemplateCreated",
        )
        assert subject == 'Sjabloon "document" toegevoegd: template created'

        subject2 = self.logger._generate_subject(
            name="document 1",
            reason="template edited",
            event_name="DocumentTemplateEdited",
        )
        assert subject2 == 'Sjabloon "document 1" opgeslagen: template edited'

    def test_generate_event_paremeters(self):
        entity_data = {"commit_message": "just because"}
        document_template_info = DocumentTemplateInformation(
            id=100, name="document"
        )
        event_params = self.logger.generate_event_parameters(
            entity_data=entity_data,
            document_template_info=document_template_info,
        )
        assert event_params == {"reason": "just because", "template_id": 100}

    def test_casecreated(self):
        logger = CaseCreated()
        case_id = 1234
        entity_data = {}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {"case_id": case_id}


class TestEventLoggerCommunication:
    def test_communication_base_generate_event_parameters(self):
        base = CommunicationBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(entity_data={})

    def test_communication_base_generate_component_attributes(self):
        base = CommunicationBase()
        with pytest.raises(NotImplementedError):
            base.generate_component_attributes()

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_thread_to_case_linked(
        self,
        mock_get_email_configuration,
        mock_get_case_from_thread,
        mock_get_user,
    ):
        session = mock.MagicMock()
        contact_moment_event = ThreadToCaseLinked()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": "a072d0cd-a261-477d-8aba-1971a486e6a5",
                },
                {
                    "key": "last_modified",
                    "new_value": "2024-12-11T13:03:37.876322+00:00",
                    "old_value": "2024-12-11T12:22:39.287880+00:00",
                },
                {
                    "key": "external_message_subject",
                    "new_value": "Message subject",
                    "old_value": None,
                },
                {
                    "key": "external_message_message_date",
                    "new_value": "2024-11-22T06:53:20.717479+00:00",
                    "old_value": None,
                },
                {
                    "key": "external_message_participants",
                    "new_value": [
                        {
                            "address": "to@example.com",
                            "display_name": "Testing to",
                            "role": "to",
                        },
                        {
                            "address": "cc@example.com",
                            "display_name": "Testing cc",
                            "role": "cc",
                        },
                        {
                            "address": "bcc@example.com",
                            "display_name": "Testing bcc",
                            "role": "bcc",
                        },
                        {
                            "address": "from@example.com",
                            "display_name": "Testing from",
                            "role": "from",
                        },
                        {
                            "address": "from2@example.com",
                            "display_name": "",
                            "role": "from",
                        },
                    ],
                    "old_value": None,
                },
                {
                    "key": "external_message_direction",
                    "new_value": "outgoing",
                    "old_value": None,
                },
            ],
            "entity_data": {"message_type": "note"},
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }
        mock_get_case_from_thread.return_value = (
            CaseInformation(id=3, confidentiality="public", uuid=uuid4()),
            None,
        )
        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        mock_get_case_from_thread.assert_called_once_with(
            session=session, thread_uuid="a072d0cd-a261-477d-8aba-1971a486e6a5"
        )

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Bericht toegevoegd aan zaak '3'"
        assert log_record.event_type == "case/thread/link"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.zaak_id == 3
        assert log_record.event_data == {
            "case_id": 3,
            "message_type": "note",
            "subject": "Message subject",
            "recipient": "to@example.com",
            "from": "Testing from <from@example.com>, from2@example.com",
            "cc": "cc@example.com",
            "bcc": "bcc@example.com",
            "created_date": "",
            "message_date": "2024-11-22T06:53:20.717479+00:00",
            "direction": "outgoing",
            "username": "name",
            "external_message_type": "",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_thread_to_case_linked_email_config(
        self,
        mock_get_email_configuration,
        mock_get_case_from_thread,
        mock_get_user,
    ):
        session = mock.MagicMock()
        contact_moment_event = ThreadToCaseLinked()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": "a072d0cd-a261-477d-8aba-1971a486e6a5",
                },
                {
                    "key": "last_modified",
                    "new_value": "2024-12-11T13:03:37.876322+00:00",
                    "old_value": "2024-12-11T12:22:39.287880+00:00",
                },
                {
                    "key": "external_message_subject",
                    "new_value": "Message subject",
                    "old_value": None,
                },
                {
                    "key": "external_message_message_date",
                    "new_value": "2024-11-22T06:53:20.717479+00:00",
                    "old_value": None,
                },
                {
                    "key": "external_message_participants",
                    "new_value": [
                        {
                            "address": "to@example.com",
                            "display_name": "Testing to",
                            "role": "to",
                        },
                        {
                            "address": "cc@example.com",
                            "display_name": "Testing cc",
                            "role": "cc",
                        },
                        {
                            "address": "bcc@example.com",
                            "display_name": "Testing bcc",
                            "role": "bcc",
                        },
                    ],
                    "old_value": None,
                },
                {
                    "key": "external_message_direction",
                    "new_value": "outgoing",
                    "old_value": None,
                },
            ],
            "entity_data": {"message_type": "note"},
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }
        mock_get_case_from_thread.return_value = (
            CaseInformation(id=3, confidentiality="public", uuid=uuid4()),
            None,
        )
        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        mock_get_case_from_thread.assert_called_once_with(
            session=session, thread_uuid="a072d0cd-a261-477d-8aba-1971a486e6a5"
        )

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Bericht toegevoegd aan zaak '3'"
        assert log_record.event_type == "case/thread/link"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.zaak_id == 3
        assert log_record.event_data == {
            "case_id": 3,
            "message_type": "note",
            "subject": "Message subject",
            "recipient": "to@example.com",
            "from": "SenderName <sender@example.com>",
            "cc": "cc@example.com",
            "bcc": "bcc@example.com",
            "created_date": "",
            "message_date": "2024-11-22T06:53:20.717479+00:00",
            "direction": "outgoing",
            "username": "name",
            "external_message_type": "",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_thread_to_case_linked_no_participants(
        self,
        mock_get_email_configuration,
        mock_get_case_from_thread,
        mock_get_user,
    ):
        session = mock.MagicMock()
        contact_moment_event = ThreadToCaseLinked()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": "a072d0cd-a261-477d-8aba-1971a486e6a5",
                },
                {
                    "key": "last_modified",
                    "new_value": "2024-12-11T13:03:37.876322+00:00",
                    "old_value": "2024-12-11T12:22:39.287880+00:00",
                },
                {
                    "key": "external_message_subject",
                    "new_value": "Message subject",
                    "old_value": None,
                },
                {
                    "key": "external_message_message_date",
                    "new_value": "2024-11-22T06:53:20.717479+00:00",
                    "old_value": None,
                },
                {
                    "key": "external_message_participants",
                    "new_value": [],
                    "old_value": None,
                },
                {
                    "key": "external_message_direction",
                    "new_value": "outgoing",
                    "old_value": None,
                },
            ],
            "entity_data": {"message_type": "note"},
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }
        mock_get_case_from_thread.return_value = (
            CaseInformation(id=3, confidentiality="public", uuid=uuid4()),
            None,
        )
        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        mock_get_case_from_thread.assert_called_once_with(
            session=session, thread_uuid="a072d0cd-a261-477d-8aba-1971a486e6a5"
        )

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Bericht toegevoegd aan zaak '3'"
        assert log_record.event_type == "case/thread/link"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.zaak_id == 3
        assert log_record.event_data == {
            "case_id": 3,
            "message_type": "note",
            "subject": "Message subject",
            "recipient": "",
            "from": "",
            "cc": "",
            "bcc": "",
            "created_date": "",
            "message_date": "2024-11-22T06:53:20.717479+00:00",
            "direction": "outgoing",
            "username": "name",
            "external_message_type": "",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_thread_to_case_linked_no_integration_config(
        self,
        mock_get_email_configuration,
        mock_get_case_from_thread,
        mock_get_user,
    ):
        session = mock.MagicMock()
        contact_moment_event = ThreadToCaseLinked()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": "a072d0cd-a261-477d-8aba-1971a486e6a5",
                },
                {
                    "key": "last_modified",
                    "new_value": "2024-12-11T13:03:37.876322+00:00",
                    "old_value": "2024-12-11T12:22:39.287880+00:00",
                },
                {
                    "key": "external_message_subject",
                    "new_value": "Message subject",
                    "old_value": None,
                },
                {
                    "key": "external_message_message_date",
                    "new_value": "2024-11-22T06:53:20.717479+00:00",
                    "old_value": None,
                },
                {
                    "key": "external_message_participants",
                    "new_value": [
                        {
                            "address": "to@example.com",
                            "display_name": "Testing to",
                            "role": "to",
                        },
                        {
                            "address": "cc@example.com",
                            "display_name": "Testing cc",
                            "role": "cc",
                        },
                        {
                            "address": "bcc@example.com",
                            "display_name": "Testing bcc",
                            "role": "bcc",
                        },
                        {
                            "address": "from@example.com",
                            "display_name": "Testing from",
                            "role": "from",
                        },
                        {
                            "address": "from2@example.com",
                            "display_name": "",
                            "role": "from",
                        },
                    ],
                    "old_value": None,
                },
                {
                    "key": "external_message_direction",
                    "new_value": "outgoing",
                    "old_value": None,
                },
            ],
            "entity_data": {"message_type": "note"},
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_email_configuration.side_effect = NotFound("")

        mock_get_case_from_thread.return_value = (
            CaseInformation(id=3, confidentiality="public", uuid=uuid4()),
            None,
        )

        with pytest.raises(NotFound):
            contact_moment_event(session=session, event=event)

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_message_deleted(self, mock_get_user):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "note",
                "case_uuid": case_uuid,
                "thread_uuid": thread_uuid,
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "name heeft notitie verwijderd."
        assert log_record.event_type == "subject/note/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.event_data == {
            "case_id": log_record.zaak_id,
            "message_type": "notitie",
            "username": "name",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_message_deleted_note(self, mock_get_user):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "note",
                "case_uuid": case_uuid,
                "thread_uuid": thread_uuid,
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "name heeft notitie verwijderd."
        assert log_record.event_type == "subject/note/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.event_data == {
            "case_id": log_record.zaak_id,
            "message_type": "notitie",
            "username": "name",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_message_deleted_contact_moment(self, mock_get_user):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "contact_moment",
                "case_uuid": case_uuid,
                "thread_uuid": thread_uuid,
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "name heeft contactmoment verwijderd."
        assert log_record.event_type == "subject/contact_moment/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.event_data == {
            "case_id": log_record.zaak_id,
            "message_type": "contactmoment",
            "username": "name",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_message_deleted_external_message(
        self, mock_get_email_configuration, mock_get_user
    ):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        participants: list[dict] = [
            {
                "role": "from",
                "display_name": "Testing from 1",
                "address": "from1@example.com",
            },
            {
                "role": "from",
                "display_name": "",
                "address": "from2@example.com",
            },
            {
                "role": "to",
                "display_name": "Testing to",
                "address": "to@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 1",
                "address": "cc1@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 2",
                "address": "cc2@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 1",
                "address": "bcc1@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 2",
                "address": "bcc2@example.com",
            },
        ]

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "external",
                "case_uuid": case_uuid,
                "external_message_type": "pip",
                "thread_uuid": thread_uuid,
                "participants": participants,
                "direction": "outgoing",
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Uitgaande pip verwijderd door medewerker name."
        )
        assert log_record.event_type == "subject/message/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.event_data == {
            "case_id": log_record.zaak_id,
            "message_type": "pip",
            "subject": "",
            "recipient": "to@example.com",
            "from": "Testing from 1 <from1@example.com>, from2@example.com",
            "cc": "cc1@example.com, cc2@example.com",
            "bcc": "bcc1@example.com, bcc2@example.com",
            "created_date": "",
            "message_date": "",
            "direction": "Uitgaande",
            "username": "name",
            "external_message_type": "pip",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_message_deleted_external_message_email_config(
        self, mock_get_email_configuration, mock_get_user
    ):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())

        participants: list[dict] = [
            {
                "role": "to",
                "display_name": "Testing to",
                "address": "to@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 1",
                "address": "cc1@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 2",
                "address": "cc2@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 1",
                "address": "bcc1@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 2",
                "address": "bcc2@example.com",
            },
        ]

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "external",
                "case_uuid": case_uuid,
                "external_message_type": "pip",
                "thread_uuid": thread_uuid,
                "participants": participants,
                "direction": "incoming",
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Inkomende pip verwijderd door medewerker name."
        )
        assert log_record.event_type == "subject/message/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.event_data == {
            "case_id": log_record.zaak_id,
            "message_type": "pip",
            "subject": "",
            "recipient": "to@example.com",
            "from": "SenderName <sender@example.com>",
            "cc": "cc1@example.com, cc2@example.com",
            "bcc": "bcc1@example.com, bcc2@example.com",
            "created_date": "",
            "message_date": "",
            "direction": "Inkomende",
            "username": "name",
            "external_message_type": "pip",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_message_deleted_external_message_no_participants(
        self, mock_get_email_configuration, mock_get_user
    ):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        participants: list[dict] = []

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "external",
                "case_uuid": case_uuid,
                "external_message_type": "pip",
                "thread_uuid": thread_uuid,
                "participants": participants,
                "direction": "outgoing",
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Uitgaande pip verwijderd door medewerker name."
        )
        assert log_record.event_type == "subject/message/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.event_data == {
            "case_id": log_record.zaak_id,
            "message_type": "pip",
            "subject": "",
            "recipient": "",
            "from": "SenderName <sender@example.com>",
            "cc": "",
            "bcc": "",
            "created_date": "",
            "message_date": "",
            "direction": "Uitgaande",
            "username": "name",
            "external_message_type": "pip",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_external_message_created_event(
        self,
        mock_get_email_configuration,
        mock_get_user,
        mock_get_contact,
        mock_get_case,
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        participants: list[dict] = [
            {
                "role": "to",
                "display_name": "Testing to",
                "address": "to@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 1",
                "address": "cc1@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 2",
                "address": "cc2@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 1",
                "address": "bcc1@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 2",
                "address": "bcc2@example.com",
            },
        ]

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "pip",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {
                "participants": participants,
            },
        }

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }
        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "PIP-bericht toegevoegd"
        assert log_record.event_type == "case/pip/feedback"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "message_type": "PIP-bericht",
            "subject": "",
            "recipient": "to@example.com",
            "from": "SenderName <sender@example.com>",
            "cc": "cc1@example.com, cc2@example.com",
            "bcc": "bcc1@example.com, bcc2@example.com",
            "message_date": "",
            "content": "",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_external_message_created_event_from(
        self,
        mock_get_email_configuration,
        mock_get_user,
        mock_get_contact,
        mock_get_case,
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        participants: list[dict] = [
            {
                "role": "from",
                "display_name": "Testing from 1",
                "address": "from1@example.com",
            },
            {
                "role": "from",
                "display_name": "",
                "address": "from2@example.com",
            },
            {
                "role": "to",
                "display_name": "Testing to",
                "address": "to@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 1",
                "address": "cc1@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 2",
                "address": "cc2@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 1",
                "address": "bcc1@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 2",
                "address": "bcc2@example.com",
            },
        ]

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "pip",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {
                "participants": participants,
            },
        }

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }
        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "PIP-bericht toegevoegd"
        assert log_record.event_type == "case/pip/feedback"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "message_type": "PIP-bericht",
            "subject": "",
            "recipient": "to@example.com",
            "from": "Testing from 1 <from1@example.com>, from2@example.com",
            "cc": "cc1@example.com, cc2@example.com",
            "bcc": "bcc1@example.com, bcc2@example.com",
            "message_date": "",
            "content": "",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_external_message_created_event_no_participants(
        self,
        mock_get_email_configuration,
        mock_get_user,
        mock_get_contact,
        mock_get_case,
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "pip",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }
        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "PIP-bericht toegevoegd"
        assert log_record.event_type == "case/pip/feedback"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "message_type": "PIP-bericht",
            "subject": "",
            "recipient": "",
            "from": "SenderName <sender@example.com>",
            "cc": "",
            "bcc": "",
            "message_date": "",
            "content": "",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_message_deleted_note_message_key_error(
        self,
        mock_get_email_configuration,
        mock_get_user,
        mock_get_contact,
        mock_get_case,
    ):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        thread_uuid = str(uuid4())
        participants: list[dict] = []

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "external",
                "case_uuid": case_uuid,
                "external_message_type": "note",
                "thread_uuid": thread_uuid,
                "participants": participants,
                "direction": "outgoing",
            },
        }

        event_2 = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "note",
                "case_uuid": case_uuid,
                "thread_uuid": thread_uuid,
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }

        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )

        cme = MessageDeleted()
        cme_2 = MessageDeleted()
        cme(session=session, event=event)

        # Original problem repro
        # Update class variable, because it is shared across all instances.
        # In Python all instances share the exact copy of the class variable.
        # Thus, if one of the instances modifies the value of a class variable,
        # then all instances start referring to the fresh copy.
        # MessageDeleted.variables.append("direction")
        # cme_2(session=session, event=event_2)

        # Repro to catch the key error
        # MessageDeleted.variables.append("direction")
        # with pytest.raises(KeyError):
        #     cme_2(session=session, event=event_2)

        cme_2(session=session, event=event_2)

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_note_created_event(
        self, mock_get_user, mock_get_contact, mock_get_case_from_thread
    ):
        communication_event = NoteCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "Note",
            "event_name": "NoteCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case_from_thread.return_value = (
            CaseInformation(id=13, confidentiality="public", uuid=case_uuid),
            None,
        )
        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Notitie toegevoegd"
        assert log_record.event_type == "case/note/created"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "Notitie",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_contact_moment_created_event(
        self, mock_get_user, mock_get_contact, mock_get_case_from_thread
    ):
        communication_event = ContactMomentCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ContactMoment",
            "event_name": "ContactMomentCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case_from_thread.return_value = (
            CaseInformation(id=13, confidentiality="public", uuid=case_uuid),
            None,
        )
        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Contactmoment toegevoegd"
        assert log_record.event_type == "case/contact_moment/created"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "Contactmoment",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_file")
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_import_email_event(
        self,
        mock_get_email_configuration,
        mock_get_user,
        mock_get_contact,
        mock_get_case,
        mock_get_file,
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {"key": "is_imported", "old_value": None, "new_value": True},
                {
                    "key": "original_message_file",
                    "old_value": None,
                    "new_value": uuid4(),
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "email",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )
        mock_get_file.return_value = FileInformation(id=13, name="test.msg")

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "E-mail 'test.msg' als bericht geïmporteerd."
        )
        assert log_record.event_type == "case/email/created"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "E-mail",
            "filename": "test.msg",
            "subject": "",
            "recipient": "",
            "from": "SenderName <sender@example.com>",
            "cc": "",
            "bcc": "",
            "message_date": "",
        }

        # When case id cannot be found.
        mock_get_case.return_value = CaseInformation(
            id=None, confidentiality="public", uuid=None
        )
        log_record = communication_event(session=session, event=event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_file")
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_email_configuration"
    )
    def test_email_received_event(
        self,
        mock_get_email_configuration,
        mock_get_user,
        mock_get_contact,
        mock_get_case,
        mock_get_file,
    ):
        communication_event = ExternalMessageCreated()
        communication_event.variables = ["message_type"]

        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {"key": "is_imported", "old_value": None, "new_value": None},
                {
                    "key": "original_message_file",
                    "old_value": None,
                    "new_value": uuid4(),
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "email",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": "None",
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_email_configuration.return_value = {
            "sender_name": "SenderName",
            "sender_address": "sender@example.com",
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )
        mock_get_file.return_value = FileInformation(id=13, name="test.msg")

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "E-mail toegevoegd"
        assert log_record.event_type == "case/email/created"
        assert log_record.created_by_name_cache == "None"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "E-mail",
            "subject": "",
            "recipient": "",
            "from": "SenderName <sender@example.com>",
            "cc": "",
            "bcc": "",
            "message_date": "",
        }

        # When user with uuid cannot be found.
        event["user_uuid"] = uuid4()
        mock_get_user.side_effect = NotFound
        log_record = communication_event(session=session, event=event)
        assert log_record is None


class TestEventLoggerSubjectRelation:
    def test_case_base_generate_event_parameters(self):
        base = SubjectRelationBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(entity_data={}, case_id=12345)

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_created_for_employee(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationCreated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": None, "new_value": "Advocaat"},
                {
                    "key": "subject",
                    "old_value": None,
                    "new_value": {
                        "type": "employee",
                        "id": str(uuid4()),
                        "name": "beheerder",
                    },
                },
                {"key": "permission", "old_value": None, "new_value": "write"},
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationCreated",
            "id": str(uuid4()),
            "entity_data": {"case": {"id": str(uuid4()), "type": "case"}},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=654, subject_type="medewerker", subject_id=2
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Betrokkene 'beheerder' toegevoegd aan zaak 1234 als Advocaat"
        )
        assert log_record.event_type == "case/subject/add"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 654,
            "params": {
                "magic_string_prefix": "advocaat",
                "rol": "Advocaat",
                "employee_authorisation": "write",
                "betrokkene_identifier": "betrokkene-medewerker-2",
            },
            "subject_name": "beheerder",
            "role": "Advocaat",
            "subject_id": 654,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_created_for_organization(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationCreated()
        session = mock.MagicMock()

        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": None, "new_value": "Advocaat"},
                {"key": "authorised", "old_value": None, "new_value": True},
                {
                    "key": "subject",
                    "old_value": None,
                    "new_value": {
                        "type": "organization",
                        "id": str(uuid4()),
                        "name": "test_org",
                    },
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationCreated",
            "id": str(uuid4()),
            "entity_data": {"case": {"id": str(uuid4()), "type": "case"}},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=800, subject_type="bedrijf", subject_id=5
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Betrokkene 'test_org' toegevoegd aan zaak 1234 als Advocaat"
        )
        assert log_record.event_type == "case/subject/add"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 800,
            "params": {
                "magic_string_prefix": "advocaat",
                "pip_authorized": "0",
                "rol": "Advocaat",
                "betrokkene_identifier": "betrokkene-bedrijf-5",
            },
            "subject_name": "test_org",
            "role": "Advocaat",
            "subject_id": 800,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_updated_for_employee(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": "Advocaat", "new_value": "Role"},
                {
                    "key": "magic_string_prefix",
                    "old_value": "advocaat",
                    "new_value": "role",
                },
                {
                    "key": "permission",
                    "old_value": "write",
                    "new_value": "write",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationUpdated",
            "id": str(uuid4()),
            "entity_data": {
                "case": {"id": str(uuid4()), "type": "case"},
                "subject": {
                    "id": str(uuid4()),
                    "type": "employee",
                    "name": "beheerder",
                },
            },
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=654, subject_type="medewerker", subject_id=2
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.zaak_id == 1234
        assert (
            log_record.onderwerp
            == "Betrokkene 'beheerder' van zaak 1234 bijgewerkt"
        )
        assert log_record.event_type == "case/subject/update"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 654,
            "params": {
                "magic_string_prefix": "role",
                "rol": "Role",
                "employee_authorisation": "write",
                "betrokkene_identifier": "betrokkene-medewerker-2",
            },
            "subject_name": "beheerder",
            "role": "Role",
            "subject_id": 654,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_updated_for_organization(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": "Advocaat", "new_value": "Role"},
                {
                    "key": "magic_string_prefix",
                    "old_value": "advocaat",
                    "new_value": "role",
                },
                {"key": "authorized", "old_value": False, "new_value": True},
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationUpdated",
            "id": str(uuid4()),
            "entity_data": {
                "case": {"id": str(uuid4()), "type": "case"},
                "subject": {
                    "id": str(uuid4()),
                    "type": "organization",
                    "name": "test_org",
                },
            },
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=800, subject_type="bedrijf", subject_id=7
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.zaak_id == 1234
        assert (
            log_record.onderwerp
            == "Betrokkene 'test_org' van zaak 1234 bijgewerkt"
        )
        assert log_record.event_type == "case/subject/update"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 800,
            "params": {
                "magic_string_prefix": "role",
                "pip_authorized": "1",
                "rol": "Role",
                "betrokkene_identifier": "betrokkene-bedrijf-7",
            },
            "subject_name": "test_org",
            "role": "Role",
            "subject_id": 800,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_deleted(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationDeleted()
        session = mock.MagicMock()
        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationDeleted",
            "id": str(uuid4()),
            "entity_data": {
                "case": {"id": str(uuid4()), "type": "case"},
                "subject": {
                    "id": str(uuid4()),
                    "type": "employee",
                    "name": "beheerder",
                },
                "role": "Advocaat",
            },
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=654, subject_type="medewerker", subject_id=2
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.zaak_id == 1234
        assert (
            log_record.onderwerp
            == "Betrokkene 'beheerder' verwijderd van zaak 1234 als Advocaat"
        )
        assert log_record.event_type == "case/subject/remove"
        assert log_record.event_data == {
            "case_id": 1234,
            "role": "Advocaat",
            "subject_name": "beheerder",
            "correlation_id": "req-1234",
        }


class TestEventLoggerCaseRelation:
    def test_case_base_generate_event_parameters(self):
        base = CaseRelationBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                session=mock.Mock(),
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_case_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case_relation.get_case")
    def test_case_relation_deleted(self, mock_get_case, mock_get_user):
        case_relation_event = CaseRelationDeleted()
        session = mock.MagicMock()
        case_uuid = uuid4()
        other_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "current_case_uuid",
                    "old_value": None,
                    "new_value": str(case_uuid),
                },
                {
                    "key": "object1_uuid",
                    "old_value": None,
                    "new_value": str(case_uuid),
                },
                {
                    "key": "object2_uuid",
                    "old_value": None,
                    "new_value": str(other_uuid),
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "CaseRelation",
            "event_name": "CaseRelationDeleted",
            "id": str(uuid4()),
            "entity_data": {"case_id_a": 10, "case_id_b": 12},
        }

        mock_get_case.side_effect = [
            CaseInformation(id=12, confidentiality="public", uuid=uuid4()),
            CaseInformation(id=22, confidentiality="public", uuid=uuid4()),
        ]
        mock_get_user.return_value = UserInformation(
            id=15, display_name="name", type="medewerker"
        )

        log_records = case_relation_event(session=session, event=event)

        assert len(log_records) == 2

        assert log_records[0].component == "zaak"
        assert log_records[1].component == "zaak"
        assert (
            log_records[0].onderwerp
            == "Relatie met zaak 22 (Test Case Type) verwijderd"
        )
        assert (
            log_records[1].onderwerp
            == "Relatie met zaak 12 (Test Case Type) verwijderd"
        )
        assert log_records[0].event_type == "case/relation/remove"
        assert log_records[1].event_type == "case/relation/remove"
        assert log_records[0].event_data == {
            "linked_case_id": 22,
            "linked_case_type_name": "Test Case Type",
            "correlation_id": "req-1234",
        }
        assert log_records[1].event_data == {
            "linked_case_id": 12,
            "linked_case_type_name": "Test Case Type",
            "correlation_id": "req-1234",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case_relation.get_case")
    def test_case_relation_created(self, mock_get_case):
        case_relation_event = CaseRelationCreated()
        session = mock.MagicMock()
        case_uuid = uuid4()
        related_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "object1_uuid",
                    "old_value": None,
                    "new_value": str(case_uuid),
                },
                {
                    "key": "object2_uuid",
                    "old_value": None,
                    "new_value": str(related_uuid),
                },
                {
                    "key": "current_case_uuid",
                    "old_value": None,
                    "new_value": str(case_uuid),
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "CaseRelation",
            "event_name": "CaseRelationCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.side_effect = [
            CaseInformation(
                id=12,
                confidentiality="public",
                uuid=uuid4(),
                casetype_name="Test Casetype",
            ),
            CaseInformation(
                id=123,
                confidentiality="public",
                uuid=uuid4(),
                casetype_name="Test Related Casetype",
            ),
        ]

        log_records = case_relation_event(session=session, event=event)

        assert log_records[0].component == "zaak"
        assert log_records[1].component == "zaak"
        assert (
            log_records[0].onderwerp
            == "Zaak 123 (Test Related Casetype) gerelateerd"
        )
        assert (
            log_records[1].onderwerp == "Zaak 12 (Test Casetype) gerelateerd"
        )
        assert log_records[0].event_type == "case/update/relation"
        assert log_records[1].event_type == "case/update/relation"
        assert log_records[0].event_data == {
            "correlation_id": "req-1234",
            "linked_case_id": 123,
            "linked_case_type_name": "Test Related Casetype",
        }
        assert log_records[1].event_data == {
            "correlation_id": "req-1234",
            "linked_case_id": 12,
            "linked_case_type_name": "Test Casetype",
        }


class TestEventLoggerDocument:
    def test_document_base_generate_event_parameters(self):
        base = DocumentBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                case_id=None,
                entity_data={},
                document_info=DocumentInformation(
                    12, "test_doc", "application/pdf", 1
                ),
                user_info=UserInformation(
                    id=12, display_name="test_user", type="employee"
                ),
                intake_owner_info=None,
                intake_group_info=None,
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_base_user_not_found(self, mock_get_user):
        base = DocumentBase()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "origin", "old_value": None, "new_value": "Intern"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.side_effect = NotFound

        log_record = base(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_base_document_not_found(self, mock_get_document):
        base = DocumentBase()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "origin", "old_value": None, "new_value": "Intern"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.side_effect = NotFound

        log_record = base(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_updated_event(
        self, mock_get_document, mock_get_user, mock_get_case
    ):
        document_event = DocumentUpdated()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "description",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "origin", "old_value": None, "new_value": "Intern"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
                {
                    "key": "basename",
                    "old_value": None,
                    "new_value": "test_doc",
                },
                {
                    "key": "confidentiality",
                    "old_value": None,
                    "new_value": "Intern",
                },
                {
                    "key": "document_category",
                    "old_value": None,
                    "new_value": "Plan",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_case.return_value = CaseInformation(
            id=17, confidentiality="Public", uuid=case_uuid
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "Documenteigenschappen van 'test_doc.doc | 12' gewijzigd door 'test_user'."
        )
        assert log_record.event_type == "document/metadata/update"
        assert log_record.event_data == {
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "metadata": {
                "description": "test_desc",
                "document_category": "Plan",
                "origin": "Intern",
                "origin_date": "2020-03-15",
                "trust_level": "Intern",
            },
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )

        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )

        log_record = document_event(session=session, event=event)

        assert log_record.event_type == "case/document/metadata/update"
        assert log_record.event_data["case_id"] == 17

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_deleted_event(
        self, mock_get_document, mock_get_user, mock_get_case
    ):
        document_event = DocumentDeleted()
        session = mock.MagicMock()

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "domain": "zsnl_domains.document",
            "entity_data": {
                "destroy_reason": "Document verwijderd uit intake"
            },
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentDeleted",
            "id": "becf2afb-348c-44be-a507-68dbdd22614b",
            "user_uuid": str(uuid4()),
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        case_uuid = uuid4()
        mock_get_case.return_value = CaseInformation(
            id=20, confidentiality="public", uuid=case_uuid
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' verwijderd uit Documentintake door 'test_user'"
        )
        assert log_record.event_type == "document/delete_document"
        assert log_record.event_data == {
            "destroy_reason": "Document verwijderd uit intake",
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )
        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )

        log_record = document_event(session=session, event=event)

        assert log_record.event_type == "case/document/delete_document"
        assert log_record.event_data["case_id"] == 20

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_deleted_event_no_reason_given_falls_back_to_default(
        self, mock_get_document, mock_get_user
    ):
        document_event = DocumentDeleted()
        session = mock.MagicMock()

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "domain": "zsnl_domains.document",
            "entity_data": {"destroy_reason": ""},
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentDeleted",
            "id": "becf2afb-348c-44be-a507-68dbdd22614b",
            "user_uuid": str(uuid4()),
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' verwijderd uit Documentintake door 'test_user'"
        )
        assert log_record.event_type == "document/delete_document"
        assert log_record.event_data == {
            "destroy_reason": "Niet opgegeven.",
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_added_to_case_event(
        self, mock_get_user, mock_get_document, mock_get_case
    ):
        document_event = DocumentAddedToCase()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "description",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "origin", "old_value": None, "new_value": "Inkomend"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAddedToCase",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_case.return_value = CaseInformation(
            id=15, uuid=case_uuid, confidentiality="Intern"
        )

        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )
        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' toegevoegd aan zaak 15 door 'test_user'"
        )
        assert log_record.event_type == "case/document/assign"
        assert log_record.event_data == {
            "case_id": 15,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "metadata": {
                "description": "test_desc",
                "origin": "Inkomend",
                "origin_date": "2020-03-15",
            },
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

        mock_get_case.side_effect = NotFound
        log_record = document_event(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_created_event(
        self, mock_get_user, mock_get_document, mock_get_case
    ):
        document_event = DocumentCreated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "basename",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "extension", "old_value": None, "new_value": ".pdf"},
                {
                    "key": "store_uuid",
                    "old_value": None,
                    "new_value": uuid4(),
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="name", type="medewerker"
        )

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )

        case_uuid = uuid4()
        mock_get_case.return_value = CaseInformation(
            id=20, uuid=case_uuid, confidentiality="Intern"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert log_record.onderwerp == "Document 'test_doc' toegevoegd"
        assert log_record.event_type == "document/create"
        assert log_record.event_data == {
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc",
            "version": 1,
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
        }

        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )
        log_record = document_event(session=session, event=event)
        assert log_record.event_type == "case/document/create"
        assert log_record.event_data["case_id"] == 20

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_department")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_assigned_to_role_event(
        self,
        mock_get_document,
        mock_get_user,
        mock_get_department,
    ):
        document_event = DocumentAssignedToRole()
        session = mock.MagicMock()
        intake_group_uuid = uuid4()
        intake_role_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "intake_group_uuid",
                    "old_value": None,
                    "new_value": intake_group_uuid,
                },
                {
                    "key": "intake_role_uuid",
                    "old_value": None,
                    "new_value": intake_role_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToRole",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_department.return_value = DepartmentInformation(
            id=17, name="development"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' toegewezen aan Afdeling 'development' door 'test_user'."
        )
        assert log_record.event_type == "document/assign"
        assert log_record.event_data == {
            "case_id": None,
            "file_id": 12,
            "correlation_id": "req-1234",
            "file_name": "test_doc.doc",
            "intake_group_name": "development",
            "user_name": "test_user",
        }

        mock_get_department.side_effect = NotFound
        log_record = document_event(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_assigned_to_user_event(
        self, mock_get_document, mock_get_user
    ):
        document_event = DocumentAssignedToUser()
        session = mock.MagicMock()
        intake_owner_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": intake_owner_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToUser",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' toegewezen aan 'test_user' door 'test_user'."
        )
        assert log_record.event_type == "document/assign"
        assert log_record.event_data == {
            "case_id": None,
            "correlation_id": "req-1234",
            "file_id": 12,
            "file_name": "test_doc.doc",
            "intake_owner_name": "test_user",
            "user_name": "test_user",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_rejected_by_user_event(
        self, mock_get_document, mock_get_user
    ):
        document_event = DocumentAssignmentRejected()
        session = mock.MagicMock()
        intake_owner_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": intake_owner_uuid,
                },
                {
                    "key": "rejection_reason",
                    "new_value": "Rejecting this document as it is not required.",
                    "old_value": None,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignmentRejected",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc' afgewezen in de documentintake door 'test_user'."
        )
        assert log_record.event_type == "document/assignment/reject"
        assert log_record.event_data == {
            "correlation_id": "req-1234",
            "file_name": "test_doc.doc",
            "user_name": "test_user",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_downloaded_event(
        self, mock_get_user, mock_get_document, mock_get_case
    ):
        document_event = DocumentDownloaded()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAddedToCase",
            "id": str(uuid4()),
            "entity_data": {"case_uuid": case_uuid},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_case.return_value = CaseInformation(
            id=15, uuid=case_uuid, confidentiality="Intern"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert log_record.onderwerp == "Document 'test_doc.doc' gedownload"
        assert log_record.event_type == "case/document/download"
        assert log_record.event_data == {
            "file_name": "test_doc.doc",
            "correlation_id": "req-1234",
        }

        mock_get_case.side_effect = NotFound
        log_record = document_event(session, event)
        assert log_record is None


class TestEventLoggerCustomObject:
    def test_case_base_generate_event_parameters(self):
        base = CustomObjectBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                session=None,
                custom_object_info=CustomObjectInformation(
                    title="",
                    custom_field_definition=[],
                    custom_object_type="",
                    custom_object_uuid="",
                    version=0,
                ),
                user_info=UserInformation(
                    id="", display_name="", type="employee"
                ),
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_created_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectCreated()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {"key": "name", "new_value": "CustObj1", "old_value": None},
                {"key": "title", "new_value": "CustObj1", "old_value": None},
                {"key": "uuid", "new_value": uuid4(), "old_value": None},
                {"key": "status", "new_value": "active", "old_value": None},
                {"key": "version", "new_value": 1, "old_value": None},
                {
                    "key": "date_created",
                    "new_value": "2020-10-12T09:02:21.597690+00:00",
                    "old_value": None,
                },
                {
                    "key": "last_modified",
                    "new_value": "2020-10-12T09:02:21.597690+00:00",
                    "old_value": None,
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=random.randint(1, 100),
            custom_field_definition=[],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object 'CustObj1' van objecttype 'CustObjType1' is aangemaakt."
        )
        assert log_record.event_type == "custom_object/created"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_updated_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUpdated()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {"key": "cases", "new_value": uuid4(), "old_value": uuid4()},
                {"key": "uuid", "new_value": uuid4(), "old_value": uuid4()},
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        version = random.randint(1, 100)

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=version,
            custom_field_definition=[],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=version,
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_version": version,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object 'CustObj1' van objecttype 'CustObjType1' is aangepast."
        )
        assert log_record.event_type == "custom_object/updated"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_version": version,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_updated_event_status(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUpdated()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {
                    "key": "status",
                    "old_value": "inactive",
                    "new_value": "active",
                },
                {
                    "key": "custom_fields",
                    # Somehow, the old_values contain the type but the new_values don't
                    "old_value": {
                        "magic_string": {"type": "text", "value": "something"},
                        "magic_string_invalid": None,
                        "checkbox_field": {"value": ["a", "b"]},
                        "checkbox_field_changed": {"value": ["a", "b"]},
                        "magic_string_here_2": {
                            "type": "text",
                            "value": "unchanged",
                        },
                        "relation_field": {
                            "type": "relationship",
                            "value": [
                                {
                                    "label": "xxx",
                                    "value": "a2ab1f47-90de-4ca4-8af7-212a165ceaff",
                                },
                                {
                                    "label": "yyy",
                                    "value": "518b03d5-fdc3-459a-b73a-13baa81de463",
                                },
                            ],
                        },
                        "relation_field_changed": {
                            "type": "relationship",
                            "value": [
                                {
                                    "label": "xxx",
                                    "value": "a2ab1f47-90de-4ca4-8af7-212a165ceaff",
                                },
                                {
                                    "label": "yyy",
                                    "value": "518b03d5-fdc3-459a-b73a-13baa81de463",
                                },
                            ],
                        },
                    },
                    "new_value": {
                        "magic_string": {"value": "not the same"},
                        "magic_string_invalid": None,
                        "magic_string_here_2": {"value": "unchanged"},
                        "checkbox_field": {"value": ["a", "b"]},
                        "checkbox_field_changed": {"value": ["a", "c"]},
                        "relation_field": {
                            "type": "text",
                            "value": [
                                {
                                    "label": "xxx",
                                    "value": "a2ab1f47-90de-4ca4-8af7-212a165ceaff",
                                },
                                {
                                    "label": "yyy",
                                    "value": "518b03d5-fdc3-459a-b73a-13baa81de463",
                                },
                            ],
                        },
                        "relation_field_changed": {
                            "type": "text",
                            "value": [
                                {
                                    "label": "xxx",
                                    "value": "a2ab1f47-90de-4ca4-8af7-212a165ceaff",
                                },
                                {
                                    "label": "zzz",
                                    "value": "f443d088-826f-4395-8d2d-64d37e9a784e",
                                },
                            ],
                        },
                    },
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=random.randint(1, 100),
            custom_field_definition=[
                {
                    "name": "Name of attribute in catalog",
                    "label": "Name of attribute in custom object type",
                    "options": [],
                    "description": "Longer description in object type",
                    "is_required": True,
                    "magic_string": "magic_string",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
                {
                    "name": "Second Attribute",
                    "label": "Second",
                    "options": [],
                    "description": "Longer description in object type",
                    "is_required": True,
                    "magic_string": "magic_string_here_2",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
                {
                    "name": "Relation Field",
                    "label": "Relation Unchanged",
                    "options": [],
                    "description": "Longer description in object type",
                    "is_required": True,
                    "magic_string": "relation_field",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
                {
                    "name": "Relation Field (Changed)",
                    "label": "Relation Changed",
                    "options": [],
                    "description": "Longer description in object type",
                    "is_required": True,
                    "magic_string": "relation_field_changed",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
                {
                    "name": "Checkbox Field",
                    "label": "Checkbox Unchanged",
                    "options": [],
                    "description": "Longer description in object type",
                    "is_required": True,
                    "magic_string": "checkbox_field",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
                {
                    "name": "Checkbox Field (Changed)",
                    "label": "Checkbox Changed",
                    "options": [],
                    "description": "Longer description in object type",
                    "is_required": True,
                    "magic_string": "checkbox_field_changed",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
                {
                    "name": "Name of attribute in catalog",
                    "label": "Invalid",
                    "options": [],
                    "description": "Invalid",
                    "is_required": True,
                    "magic_string": "magic_string_invalid",
                    "attribute_uuid": str(uuid4()),
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "custom_field_type": "text",
                    "external_description": "",
                    "custom_field_specification": None,
                },
            ],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "De status van object 'CustObj1' van objecttype 'CustObjType1' is aangepast naar actief.\n\n"
            "Aangepaste kenmerken: Name of attribute in custom object type, Relation Changed, Checkbox Changed"
        )

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_case")
    def test_custom_object_related_to_event(
        self, mock_get_case, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectRelatedTo()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())
        case_id = random.randint(1, 1000)
        case_uuid = str(uuid4())
        case_existing_uuid = str(uuid4())

        event = {
            "changes": [
                {
                    "key": "cases",
                    "new_value": [
                        {"uuid": case_uuid},
                        {"uuid": case_existing_uuid},
                    ],
                    "old_value": [
                        {"uuid": case_existing_uuid},
                    ],
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectRelatedTo",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=case_id,
            uuid=case_uuid,
            confidentiality="public",
        )

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=random.randint(1, 100),
            custom_field_definition=[],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={
                "cases": [
                    {"uuid": case_uuid},
                    {"uuid": case_existing_uuid},
                ],
                "cases_old": [
                    {"uuid": case_existing_uuid},
                ],
            },
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )

        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "case_id": case_id,
            "custom_object_values_copied": "",
            "custom_object_related_items": f"zaak {case_id}",
            "related_uuids": [case_uuid],
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == f"Object 'CustObj1' van objecttype 'CustObjType1' is gerelateerd aan zaak {case_id}."
        )
        assert log_record.event_type == "custom_object/relatedTo"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_values_copied": "",
            "case_id": case_id,
            "custom_object_related_items": f"zaak {case_id}",
            "related_uuids": [case_uuid],
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_case")
    def test_custom_object_related_to_event_copy_values(
        self, mock_get_case, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectRelatedTo()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())
        case_id = random.randint(1, 1000)
        case_uuid = str(uuid4())
        case_existing_uuid = str(uuid4())

        event = {
            "changes": [
                {
                    "key": "cases",
                    "new_value": [
                        {"uuid": case_uuid},
                        {"uuid": case_existing_uuid},
                    ],
                    "old_value": [
                        {"uuid": case_existing_uuid},
                    ],
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectRelatedTo",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=case_id,
            uuid=case_uuid,
            confidentiality="public",
        )

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=random.randint(1, 100),
            custom_field_definition=[],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={
                "cases": [
                    {
                        "uuid": case_uuid,
                        "copy_values_to_case": "True",
                    },
                    {
                        "uuid": case_existing_uuid,
                    },
                ],
                "cases_old": [
                    {
                        "uuid": case_existing_uuid,
                    },
                ],
            },
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )

        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_values_copied": " Kenmerken uit het object zijn gekopieerd naar de zaak.",
            "case_id": case_id,
            "custom_object_related_items": f"zaak {case_id}",
            "related_uuids": [case_uuid],
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == f"Object 'CustObj1' van objecttype 'CustObjType1' is gerelateerd aan zaak {case_id}."
        )
        assert log_record.event_type == "custom_object/relatedTo"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_values_copied": "",
            "case_id": case_id,
            "custom_object_related_items": f"zaak {case_id}",
            "related_uuids": [case_uuid],
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_related_to_custom_object_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectRelatedTo()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        related_custom_object_uuid = str(uuid4())
        related_custom_object_uuid_existing = str(uuid4())

        event = {
            "changes": [
                {
                    "key": "custom_objects",
                    "new_value": [
                        {"uuid": related_custom_object_uuid},
                        {"uuid": related_custom_object_uuid_existing},
                    ],
                    "old_value": [
                        {"uuid": related_custom_object_uuid_existing},
                    ],
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectRelatedTo",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.side_effect = [
            CustomObjectInformation(
                title="CustObjRel",
                custom_object_type="CustObjType1",
                custom_object_uuid=related_custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
        ]

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={
                "custom_objects": [
                    {"uuid": related_custom_object_uuid},
                    {"uuid": related_custom_object_uuid_existing},
                ],
                "custom_objects_old": [
                    {"uuid": related_custom_object_uuid_existing},
                ],
            },
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_values_copied": "",
            "custom_object_related_items": "object 'CustObjRel'",
            "related_uuids": [related_custom_object_uuid],
        }

        mock_get_custom_object.side_effect = [
            CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            CustomObjectInformation(
                title="CustObjRel",
                custom_object_type="CustObjType1",
                custom_object_uuid=related_custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
        ]

        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object 'CustObj1' van objecttype 'CustObjType1' is gerelateerd aan object 'CustObjRel'."
        )
        assert log_record.event_type == "custom_object/relatedTo"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_values_copied": "",
            "custom_object_related_items": "object 'CustObjRel'",
            "related_uuids": [related_custom_object_uuid],
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_retrieved_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectRetrieved()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains_case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectRetrieved",
            "id": str(uuid4()),
            "entity_data": {},
        }

        version = random.randint(1, 100)
        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=version,
            custom_field_definition=[],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=version,
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_version": version,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object 'CustObj1' van objecttype 'CustObjType1' is opgevraagd."
        )
        assert log_record.event_type == "custom_object/retrieved"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "custom_object_version": version,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_case")
    def test_custom_object_unrelated_from_event(
        self, mock_get_case, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUnrelatedFrom()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())
        case_uuid_existing = str(uuid4())
        case_uuid = str(uuid4())
        case_id = random.randint(1, 1000)

        event = {
            "changes": [
                {
                    "key": "cases",
                    "new_value": [
                        {"uuid": case_uuid_existing},
                    ],
                    "old_value": [
                        {"uuid": case_uuid_existing},
                        {"uuid": case_uuid},
                    ],
                }
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUnrelatedFrom",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=case_id,
            uuid=case_uuid,
            confidentiality="public",
        )

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
            version=random.randint(1, 100),
            custom_field_definition=[],
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={
                "cases": [
                    {"uuid": case_uuid_existing},
                ],
                "cases_old": [
                    {"uuid": case_uuid_existing},
                    {"uuid": case_uuid},
                ],
            },
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_related_items": f"zaak {case_id}",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "related_uuids": [case_uuid],
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == f"Relatie verwijderd vanuit object 'CustObj1' van objecttype 'CustObjType1' naar zaak {case_id}."
        )
        assert log_record.event_type == "custom_object/unrelatedFrom"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_related_items": f"zaak {case_id}",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "related_uuids": [case_uuid],
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_unrelated_from_custom_object_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUnrelatedFrom()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        related_custom_object_uuid = str(uuid4())
        related_custom_object_uuid_existing = str(uuid4())

        event = {
            "changes": [
                {
                    "key": "custom_objects",
                    "new_value": [
                        {"uuid": related_custom_object_uuid_existing},
                    ],
                    "old_value": [
                        {"uuid": related_custom_object_uuid},
                        {"uuid": related_custom_object_uuid_existing},
                    ],
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUnrelatedFrom",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.side_effect = [
            CustomObjectInformation(
                title="CustObjRel",
                custom_object_type="CustObjType1",
                custom_object_uuid=related_custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
        ]

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={
                "custom_objects": [
                    {"uuid": related_custom_object_uuid_existing},
                ],
                "custom_objects_old": [
                    {"uuid": related_custom_object_uuid},
                    {"uuid": related_custom_object_uuid_existing},
                ],
            },
            session=None,
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_related_items": "object 'CustObjRel'",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "related_uuids": [related_custom_object_uuid],
        }

        mock_get_custom_object.side_effect = [
            CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            CustomObjectInformation(
                title="CustObjRel",
                custom_object_type="CustObjType1",
                custom_object_uuid=related_custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
        ]

        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Relatie verwijderd vanuit object 'CustObj1' van objecttype 'CustObjType1' naar object 'CustObjRel'."
        )
        assert log_record.event_type == "custom_object/unrelatedFrom"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_related_items": "object 'CustObjRel'",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
            "related_uuids": [related_custom_object_uuid],
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    def test_custom_object_deleted_from_event(self, mock_get_user):
        custom_object_event = CustomObjectDeleted()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "title",
                    "new_value": "custom_object_1",
                    "old_value": "custom_object_1",
                },
                {
                    "key": "version_independent_uuid",
                    "new_value": None,
                    "old_value": custom_object_uuid,
                },
                {
                    "key": "delete_reason",
                    "new_value": "Reason required",
                    "old_value": None,
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "CustomObject",
            "event_name": "CustomObjectDeleted",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={"delete_reason": "A reason"},
            session=None,
            custom_object_info=CustomObjectInformation(
                title="custom_object_1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "custom_object_1",
            "username": "test_user",
            "custom_object_uuid": custom_object_uuid,
            "delete_reason": "A reason",
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object 'custom_object_1' verwijderd door 'test_user'. Reden van verwijderen: 'Reason required'"
        )
        assert log_record.event_type == "custom_object/deleted"
        assert log_record.event_data == {
            "custom_object_name": "custom_object_1",
            "username": "test_user",
            "custom_object_uuid": custom_object_uuid,
            "delete_reason": "Reason required",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    def test_custom_object_deleted_from_event_no_reason(self, mock_get_user):
        custom_object_event = CustomObjectDeleted()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "title",
                    "new_value": "custom_object_1",
                    "old_value": "custom_object_1",
                },
                {
                    "key": "version_independent_uuid",
                    "new_value": None,
                    "old_value": custom_object_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "CustomObject",
            "event_name": "CustomObjectDeleted",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            session=None,
            custom_object_info=CustomObjectInformation(
                title="custom_object_1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
                version=random.randint(1, 100),
                custom_field_definition=[],
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "custom_object_1",
            "username": "test_user",
            "custom_object_uuid": custom_object_uuid,
            "delete_reason": "Geen reden opgegeven",
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object 'custom_object_1' verwijderd door 'test_user'. Reden van verwijderen: 'Geen reden opgegeven'"
        )
        assert log_record.event_type == "custom_object/deleted"
        assert log_record.event_data == {
            "custom_object_name": "custom_object_1",
            "username": "test_user",
            "custom_object_uuid": custom_object_uuid,
            "delete_reason": "Geen reden opgegeven",
        }


class TestEventLoggerCustomObjectType:
    def test_case_base_generate_event_parameters(self):
        base = CustomObjectTypeBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                custom_object_type_info=CustomObjectTypeInformation(
                    id=101, name="CustObjType1"
                ),
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object_type.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object_type.get_custom_object_type"
    )
    def test_custom_object_type_created_event(
        self, mock_get_custom_object_type, mock_get_user
    ):
        custom_object_type_event = CustomObjectTypeCreated()
        session = mock.MagicMock()

        event = {
            "changes": [
                {
                    "key": "name",
                    "new_value": "CustObjType1",
                    "old_value": None,
                },
                {"key": "uuid", "new_value": uuid4(), "old_value": None},
                {
                    "key": "title",
                    "new_value": "CustObjType1",
                    "old_value": None,
                },
                {"key": "status", "new_value": "active", "old_value": None},
                {"key": "version", "new_value": 1, "old_value": None},
                {
                    "key": "date_created",
                    "new_value": "2020-11-05T14:24:46.321472+00:00",
                    "old_value": None,
                },
                {
                    "key": "audit_log",
                    "new_value": {
                        "description": "test",
                        "updated_components": [
                            "custom_fields",
                            "authorizations",
                        ],
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2020-11-05T14:24:46.322418",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "CustomObjectType",
            "event_name": "CustomObjectTypeCreated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_custom_object_type.return_value = CustomObjectTypeInformation(
            id=101, name="CustObjType1"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_type_event.generate_event_parameters(
            entity_data={},
            custom_object_type_info=CustomObjectTypeInformation(
                id=101, name="CustObjType1"
            ),
        )
        assert event_params == {
            "custom_object_type": "CustObjType1",
        }
        log_record = custom_object_type_event(session=session, event=event)

        assert log_record.component == "custom_object_type"
        assert log_record.onderwerp == "Objecttype 'CustObjType1' aangemaakt."
        assert log_record.event_type == "custom_object_type/created"
        assert log_record.event_data == {
            "custom_object_type": "CustObjType1",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object_type.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object_type.get_custom_object_type"
    )
    def test_custom_object_type_updated_event(
        self, mock_get_custom_object_type, mock_get_user
    ):
        custom_object_type_event = CustomObjectTypeUpdated()
        session = mock.MagicMock()

        event = {
            "changes": [
                {
                    "key": "uuid",
                    "new_value": str(uuid4()),
                    "old_value": str(uuid4()),
                },
                {
                    "key": "name",
                    "new_value": "CustObjType1",
                    "old_value": "CustObjType",
                },
                {
                    "key": "audit_log",
                    "new_value": {
                        "description": "Attributes Updated",
                        "updated_components": [
                            "custom_fields",
                            "authorizations",
                        ],
                    },
                    "old_value": {
                        "description": "Attributes Set",
                        "updated_components": [
                            "custom_fields",
                            "authorizations",
                        ],
                    },
                },
            ],
            "context": "context",
            "correlation_id": str(uuid4()),
            "created_date": "2020-11-05T14:27:08.977004",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "CustomObjectType",
            "event_name": "CustomObjectTypeUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_custom_object_type.return_value = CustomObjectTypeInformation(
            id=101, name="CustObjType1"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        entity_data = {
            "audit_log": {
                "description": "Updating custom fields",
                "updated_components": ["custom_fields", "authorizations"],
            }
        }
        event_params = custom_object_type_event.generate_event_parameters(
            entity_data=entity_data,
            custom_object_type_info=CustomObjectTypeInformation(
                id=101, name="CustObjType1"
            ),
        )
        assert event_params == {
            "custom_object_type": "CustObjType1",
            "updated_component_list": "Kenmerken,Rechten",
        }
        log_record = custom_object_type_event(session=session, event=event)

        assert log_record.component == "custom_object_type"
        assert (
            log_record.onderwerp
            == "Objecttype 'CustObjType1' gewijzigd (Kenmerken,Rechten)."
        )
        assert log_record.event_type == "custom_object_type/updated"
        assert log_record.event_data == {
            "custom_object_type": "CustObjType1",
            "updated_component_list": "Kenmerken,Rechten",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object_type.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object_type.get_custom_object_type"
    )
    def test_custom_object_type_deleted_event(
        self, mock_get_custom_object_type, mock_get_user
    ):
        custom_object_type_event = CustomObjectTypeDeleted()
        session = mock.MagicMock()

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": str(uuid4()),
            "created_date": "2020-11-05T21:32:08.482378",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "CustomObjectType",
            "event_name": "CustomObjectTypeDeleted",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_custom_object_type.return_value = CustomObjectTypeInformation(
            id=101, name="CustObjType1"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_type_event.generate_event_parameters(
            entity_data={},
            custom_object_type_info=CustomObjectTypeInformation(
                id=101, name="CustObjType1"
            ),
        )
        assert event_params == {
            "custom_object_type": "CustObjType1",
        }
        log_record = custom_object_type_event(session=session, event=event)

        assert log_record.component == "custom_object_type"
        assert log_record.onderwerp == "Objecttype 'CustObjType1' verwijderd."
        assert log_record.event_type == "custom_object_type/deleted"
        assert log_record.event_data == {
            "custom_object_type": "CustObjType1",
        }


class TestEventLoggerSubject:
    def test_case_base_generate_event_parameters(self):
        base = SubjectBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                subject_info=PersonInformation(
                    id=random.randint(1, 1000), type="person"
                ),
                uuid=uuid4(),
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_bsn_retrieved_event(self, mock_get_person, mock_get_user):
        person_event = BsnRetrieved()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "BsnRetrieved",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Inzage verleend op betrokkene 'person_a', veld 'bsn'."
        )
        assert log_record.event_type == "subject/inspect"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_bsn_updated_non_authentic_contact_event(
        self, mock_get_person, mock_get_user
    ):
        person_event = NonAuthenticBsnUpdated()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "bsn",
                    "new_value": "111",
                    "old_value": "",
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "NonAuthenticBsnUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "BSN van 'person_a' gewijzigd."
        assert log_record.event_type == "subject/update"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_non_authentic_contact_updated_event(
        self, mock_get_person, mock_get_user
    ):
        person_event = PersonUpdated()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "first_names",
                    "new_value": "name",
                    "old_value": "",
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "PersonUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        event2 = {
            "changes": [],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "PersonUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        event3 = {
            "changes": [
                {
                    "key": "residence_address",
                    "new_value": {
                        "city": "Haarlem",
                        "country": "Nederland",
                        "country_code": 6030,
                        "geo_lat_long": None,
                        "is_foreign": False,
                        "street": "ellermanstraat",
                        "street_number": 23,
                        "street_number_letter": "A",
                        "street_number_suffix": "F",
                        "zipcode": "1041AK",
                    },
                    "old_value": {
                        "city": "Amsterdam",
                        "country": "Nederland",
                        "country_code": 6030,
                        "geo_lat_long": None,
                        "is_foreign": False,
                        "street": "ellermanstraat",
                        "street_number": 23,
                        "street_number_letter": "A",
                        "street_number_suffix": "F",
                        "zipcode": "1041AK",
                    },
                }
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "PersonUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "changes": "",
        }
        log_record: PersonUpdated = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp == "Info van 'person_a' gewijzigd: voornamen."
        )
        assert log_record.event_type == "subject/update"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "changes": "voornamen",
        }

        log_record2: PersonUpdated = person_event(
            session=session, event=event2
        )

        assert log_record2.component == "betrokkene"
        assert log_record2.onderwerp == "Info van 'person_a' gewijzigd: ."
        assert log_record2.event_type == "subject/update"

        assert log_record2.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "changes": "",
        }

        log_record3: PersonUpdated = person_event(
            session=session, event=event3
        )

        assert log_record3.component == "betrokkene"
        assert log_record3.onderwerp == "Info van 'person_a' gewijzigd: stad."
        assert log_record3.event_type == "subject/update"

        assert log_record3.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "changes": "stad",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_status_set_stalled(self, mock_get_case, mock_get_user):
        case_event = CaseStatusSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "status", "old_value": "open", "new_value": "stalled"}
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseStatusSet",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Status voor zaak: 1234 gewijzigd naar: opgeschort"
        )
        assert log_record.event_type == "case/update/status"

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_contact_info_update_event(self, mock_get_person, mock_get_user):
        person_event = ContactInformationSaved()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "contact_information",
                    "new_value": {
                        "email": "person_a@gmail.com",
                        "internal_note": "phone",
                        "is_anonymous_contact": False,
                        "mobile_number": "0652387284",
                        "phone_number": "0624844247",
                        "preferred_contact_channel": "email",
                    },
                    "old_value": {
                        "email": "person_a_1@gmail.com",
                        "internal_note": "phone",
                        "is_anonymous_contact": False,
                        "mobile_number": "0632387284",
                        "phone_number": "0624844247",
                        "preferred_contact_channel": "email",
                    },
                }
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "ContactInformationSaved",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={
                "name": "person_a",
                "contact_information": {
                    "email": "person_a@gmail.com",
                    "internal_note": "phone",
                    "is_anonymous_contact": False,
                    "mobile_number": "0652387284",
                    "phone_number": "0624844247",
                    "preferred_contact_channel": "email",
                },
                "contact_information_old": {
                    "email": "person_a_1@gmail.com",
                    "internal_note": "phone",
                    "is_anonymous_contact": False,
                    "mobile_number": "0632387284",
                    "phone_number": "0624844247",
                    "preferred_contact_channel": "email",
                },
            },
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "updated_fields": "e-mailadres, telefoonnummer (mobiel)",
        }
        log_record = person_event(session=session, event=event)
        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Plusgegevens voor betrokkene 'person_a' gewijzigd: e-mailadres, telefoonnummer (mobiel)"
        )
        assert log_record.event_type == "subject/update_contact_data"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "updated_fields": "e-mailadres, telefoonnummer (mobiel)",
        }

    def test_add_validsign_participants_generate_event_parameters(self):
        logger = AddValidsignTimelineEntry()
        case_id = 1234
        entity_data = {
            "document_sign_participants": [
                {"name": "ivo", "lastName": "ten Tije"},
                {"name": "person", "lastName": "achter"},
            ]
        }
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "document_sign_participants": " naar ivo ten Tije, person achter",
            "case_id": case_id,
        }

    def test_add_validsign_no_participants_generate_event_parameters(self):
        logger = AddValidsignTimelineEntry()
        case_id = 1234
        entity_data = {"document_sign_participants": []}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "document_sign_participants": "",
            "case_id": case_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_sedula_updated_non_authentic_contact_event(
        self, mock_get_person, mock_get_user
    ):
        person_event = NonAuthenticSedulaUpdated()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "sedula_number",
                    "new_value": "111",
                    "old_value": "",
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "NonAuthenticSedulaUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "Sedula van 'person_a' gewijzigd."
        assert log_record.event_type == "subject/update"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_event_logger_contact_viewed_person(
        self, mock_get_person, mock_get_user
    ):
        person_event = ContactViewed()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "ContactViewed",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp == "Contact geopend op  betrokkene 'person_a'"
        )
        assert log_record.event_type == "subject/contact_viewed"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_organization")
    def test_event_logger_contact_viewed_organization(
        self, mock_get_organization, mock_get_user
    ):
        person_event = ContactViewed()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "company_a"},
            "entity_id": entity_id,
            "entity_type": "Organization",
            "event_name": "ContactViewed",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_organization.return_value = OrganistionInformation(
            id=101, type="Organization"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "company_a"},
            subject_info=PersonInformation(id=101, type="Organization"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "company_a",
            "betrokkene_identifier": "betrokkene-Organization-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Contact geopend op  betrokkene 'company_a'"
        )
        assert log_record.event_type == "subject/contact_viewed"

        assert log_record.event_data == {
            "name": "company_a",
            "betrokkene_identifier": "betrokkene-Organization-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_employee")
    def test_event_logger_contact_viewed_empoyee(
        self, mock_get_employee, mock_get_user
    ):
        person_event = ContactViewed()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "employee_a"},
            "entity_id": entity_id,
            "entity_type": "Employee",
            "event_name": "ContactViewed",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_employee.return_value = EmployeeInformation(
            id=101, type="employee"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "employee_a"},
            subject_info=PersonInformation(id=101, type="Employee"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "employee_a",
            "betrokkene_identifier": "betrokkene-Employee-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Contact geopend op  betrokkene 'employee_a'"
        )
        assert log_record.event_type == "subject/contact_viewed"

        assert log_record.event_data == {
            "name": "employee_a",
            "betrokkene_identifier": "betrokkene-medewerker-101",
            "uuid": entity_id,
        }


class TestEventLoggerArchiveRequest:
    def test_archive_request_generate_event_parameters(self):
        logger = ArchiveRequest()
        case_id = 127
        entity_data = {}
        document_string = "Zaak: 127 Archief verzoek voor de documenten lorem_ipsum_repeated.txt Versie: 1, 4.txt Versie: 1"
        event_params = logger.generate_event_parameters(
            entity_data=entity_data,
            case_id=case_id,
            document_string=document_string,
        )
        assert event_params == {
            "case_id": case_id,
            "document_string": document_string,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_archive_request.get_document"
    )
    def test_request_archive_event(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        archive_event = ArchiveRequest()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "case_uuid",
                    "new_value": "efaea64-9ff4-4c31-a70a-77e247c83643",
                    "old_value": "",
                },
                {
                    "key": "directory_uuids",
                    "new_value": "[]",
                    "old_value": "",
                },
                {
                    "key": "document_uuids",
                    "new_value": '["efaea64-9ff4-4c31-a70a-77e247c83643"]',
                    "old_value": "",
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {},
            "entity_id": entity_id,
            "entity_type": "DocumentArchive",
            "event_name": "DocumentArchiveRequested",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        mock_get_document.return_value = DocumentInformation(
            id=101, name="Test", mimetype="TESt", version=1
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = archive_event.generate_event_parameters(
            case_id=1234,
            document_string="Test",
            entity_data={},
        )

        assert event_params == {
            "case_id": 1234,
            "document_string": "Test",
        }
        log_record = archive_event(session=session, event=event)

        assert log_record.component == "archief vezoek"
        assert log_record.event_type == "archive_request/create"


class TestEventLoggerLabel:
    def setup_method(self):
        self.case_information = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        self.document_information = DocumentInformation(
            id=101, name="Test", mimetype="TESt", version=1
        )

        self.user_information = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        self.entity_id = str(uuid4())

        self.event_add_one = {
            "changes": [
                {
                    "key": "case_uuid",
                    "new_value": "efaea64-9ff4-4c31-a70a-77e247c83643",
                    "old_value": "",
                },
                {
                    "key": "labels",
                    "new_value": [
                        {
                            "attribute_id": "e0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc",
                            "name": "doc",
                            "public_name": "doc",
                            "uuid": "5933f36e-b5b7-4151-8f83-0b2062705c53",
                        }
                    ],
                    "old_value": [],
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {},
            "entity_id": self.entity_id,
            "entity_type": "Document",
            "event_name": "LabelsApplied",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        self.event_remove_one = {
            "changes": [
                {
                    "key": "case_uuid",
                    "new_value": "efaea64-9ff4-4c31-a70a-77e247c83643",
                    "old_value": "",
                },
                {
                    "key": "labels",
                    "old_value": [
                        {
                            "attribute_id": "e0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc",
                            "name": "doc",
                            "public_name": "doc",
                            "uuid": "5933f36e-b5b7-4151-8f83-0b2062705c53",
                        }
                    ],
                    "new_value": [],
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {},
            "entity_id": self.entity_id,
            "entity_type": "Document",
            "event_name": "LabelsRemoved",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        self.event_add_two = {
            "changes": [
                {
                    "key": "case_uuid",
                    "new_value": "efaea64-9ff4-4c31-a70a-77e247c83643",
                    "old_value": "",
                },
                {
                    "key": "labels",
                    "new_value": [
                        {
                            "attribute_id": "e0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc",
                            "name": "doc",
                            "public_name": "doc",
                            "uuid": "5933f36e-b5b7-4151-8f83-0b2062705c53",
                        },
                        {
                            "attribute_id": "f0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc2",
                            "name": "doc2",
                            "public_name": "doc",
                            "uuid": "2933f36e-b5b7-4151-8f83-0b2062705c53",
                        },
                    ],
                    "old_value": [],
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {},
            "entity_id": self.entity_id,
            "entity_type": "Document",
            "event_name": "LabelsApplied",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        self.event_no_change = {
            "changes": [
                {
                    "key": "case_uuid",
                    "new_value": "efaea64-9ff4-4c31-a70a-77e247c83643",
                    "old_value": "",
                },
                {
                    "key": "labels",
                    "new_value": [
                        {
                            "attribute_id": "e0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc",
                            "name": "doc",
                            "public_name": "doc",
                            "uuid": "5933f36e-b5b7-4151-8f83-0b2062705c53",
                        },
                    ],
                    "old_value": [
                        {
                            "attribute_id": "e0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc",
                            "name": "doc",
                            "public_name": "doc",
                            "uuid": "5933f36e-b5b7-4151-8f83-0b2062705c53",
                        },
                    ],
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {},
            "entity_id": self.entity_id,
            "entity_type": "Document",
            "event_name": "LabelsApplied",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        self.event_remove_two = {
            "changes": [
                {
                    "key": "case_uuid",
                    "new_value": "efaea64-9ff4-4c31-a70a-77e247c83643",
                    "old_value": "",
                },
                {
                    "key": "labels",
                    "old_value": [
                        {
                            "attribute_id": "e0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc",
                            "name": "doc",
                            "public_name": "doc",
                            "uuid": "5933f36e-b5b7-4151-8f83-0b2062705c53",
                        },
                        {
                            "attribute_id": "f0beed3d-bcbc-4308-9a7f-d44e9e2cd2ca",
                            "magic_string": "doc2",
                            "name": "doc2",
                            "public_name": "doc2",
                            "uuid": "2933f36e-b5b7-4151-8f83-0b2062705c53",
                        },
                    ],
                    "new_value": [],
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {},
            "entity_id": self.entity_id,
            "entity_type": "Document",
            "event_name": "LabelsRemoved",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_archive_request.get_document"
    )
    def test_request_label_add(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        label_event = LabelApplied()
        session = mock.MagicMock()

        mock_get_case.return_value = self.case_information
        mock_get_document.return_value = self.document_information
        mock_get_user.return_value = self.user_information

        formatted_entity_data = label_event.format_entity_data(
            event=self.event_add_one
        )
        event_params = label_event.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_info=self.document_information,
        )

        assert event_params == {
            "documentlabel": " 'doc",
            "document": "Test",
        }
        log_record = label_event(session=session, event=self.event_add_one)

        assert log_record.component == "document"
        assert log_record.event_type == "document/label/apply"

    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_archive_request.get_document"
    )
    def test_request_label_remove(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        label_event = LabelDeleted()
        session = mock.MagicMock()

        mock_get_case.return_value = self.case_information
        mock_get_document.return_value = self.document_information
        mock_get_user.return_value = self.user_information

        formatted_entity_data = label_event.format_entity_data(
            event=self.event_remove_one
        )
        event_params = label_event.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_info=self.document_information,
        )

        assert event_params == {
            "documentlabel": " 'doc",
            "document": "Test",
        }
        log_record = label_event(session=session, event=self.event_remove_one)

        assert log_record.component == "document"
        assert log_record.event_type == "document/label/delete"

    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_archive_request.get_document"
    )
    def test_request_label_add_multiple(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        label_event = LabelApplied()
        session = mock.MagicMock()

        mock_get_case.return_value = self.case_information
        mock_get_document.return_value = self.document_information
        mock_get_user.return_value = self.user_information

        formatted_entity_data = label_event.format_entity_data(
            event=self.event_add_two
        )
        event_params = label_event.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_info=self.document_information,
        )

        assert event_params == {
            "documentlabel": "s 'doc, doc2",
            "document": "Test",
        }
        log_record = label_event(session=session, event=self.event_add_two)

        assert log_record.component == "document"
        assert log_record.event_type == "document/label/apply"

    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_archive_request.get_document"
    )
    def test_request_label_add_None(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        label_event = LabelApplied()
        session = mock.MagicMock()

        mock_get_case.return_value = self.case_information
        mock_get_document.return_value = self.document_information
        mock_get_user.return_value = self.user_information

        formatted_entity_data = label_event.format_entity_data(
            event=self.event_no_change
        )
        event_params = label_event.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_info=self.document_information,
        )

        assert event_params == {
            "documentlabel": " '",
            "document": "Test",
        }
        log_record = label_event(session=session, event=self.event_no_change)

        assert log_record.component == "document"
        assert log_record.event_type == "document/label/apply"

    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_archive_request.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_archive_request.get_document"
    )
    def test_request_label_remove_multiple(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        label_event = LabelDeleted()
        session = mock.MagicMock()

        mock_get_case.return_value = self.case_information
        mock_get_document.return_value = self.document_information
        mock_get_user.return_value = self.user_information

        formatted_entity_data = label_event.format_entity_data(
            event=self.event_remove_two
        )
        event_params = label_event.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_info=self.document_information,
        )

        assert event_params == {
            "documentlabel": "s 'doc, doc2",
            "document": "Test",
        }
        log_record = label_event(session=session, event=self.event_remove_two)

        assert log_record.component == "document"
        assert log_record.event_type == "document/label/delete"

    @mock.patch("zsnl_amqp_consumers.event_logger_label.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_label.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_label.get_document")
    def test_request_label_remove_not_found(
        self,
        mock_get_document,
        mock_get_case,
        mock_get_user,
    ):
        label_event = LabelDeleted()
        session = mock.MagicMock()

        mock_get_case.return_value = self.case_information
        mock_get_document.return_value = self.document_information
        mock_get_user.return_value = self.user_information

        mock_get_case.side_effect = NotFound
        log_record = label_event(session=session, event=self.event_remove_two)
        assert log_record is None

        mock_get_document.side_effect = NotFound
        log_record = label_event(session=session, event=self.event_remove_two)
        assert log_record is None

        mock_get_user.side_effect = NotFound
        log_record = label_event(session=session, event=self.event_remove_two)
        assert log_record is None

    def test_label_base_generate_event_parameters(self):
        base = LabelBase()

        formatted_entity_data = base.format_entity_data(
            event=self.event_remove_two
        )
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data=formatted_entity_data,
                document_info=self.document_information,
            )


class TestEventLoggerAuth0Base:
    def setup_method(self):
        self.user_information: UserInformation = UserInformation(
            None, None, None
        )
        self.entity_id: str = str(uuid4())
        self.correlation_id: str = str(uuid4())
        self.user_uuid: str | None = str(uuid4())

        self.event: dict = {
            "changes": [
                {
                    "key": "username",
                    "new_value": "test@example.com",
                    "old_value": "",
                },
                {
                    "key": "interface_name",
                    "new_value": "authldap",
                    "old_value": "",
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": self.correlation_id,
            "created_date": "2024-10-02T11:53:53.88248",
            "domain": "zsnl_domains.auth",
            "entity_data": {},
            "entity_id": None,
            "entity_type": "Timeline",
            "event_name": "",
            "id": str(uuid4()),
            "user_uuid": self.user_uuid,
            "processed": False,
            "user_info": None,
        }

    def test_case_base_generate_event_parameters(self):
        base = Auth0Base()
        with pytest.raises(NotImplementedError):
            session = mock.MagicMock()
            base.generate_event_parameters(
                session=session,
                entity_data={},
                user_info=None,
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_auth0.get_user")
    def test_auth0_interface_not_active_event(self, mock_get_user):
        auth0_event = Auth0InterfaceNotActive()
        session = mock.MagicMock()

        self.event["event_name"] = "Auth0InterfaceNotActive"
        mock_get_user.return_value = self.user_information

        formatted_entity_data = auth0_event.format_entity_data(
            event=self.event
        )

        event_params = auth0_event.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=mock_get_user,
        )
        assert event_params == {
            "username": "test@example.com",
            "interface_name": "authldap",
        }

        log_record = auth0_event(session=session, event=self.event)
        assert log_record.component == "user"
        assert log_record.onderwerp == (
            "Inlogproces (auth0) voor gebruiker 'test@example.com' is mislukt,"
            " omdat de interface 'authldap' niet bestaat of is gedeactiveerd."
        )
        assert log_record.event_type == "auth/auth0/interface/status/inactive"
        assert log_record.event_data == {
            "correlation_id": self.correlation_id,
            "interface_name": "authldap",
            "username": "test@example.com",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_auth0.get_user")
    def test_auth0_user_not_active_event(self, mock_get_user):
        auth0_event = Auth0UserNotActive()
        session = mock.MagicMock()

        self.event["event_name"] = "Auth0UserNotActive"
        mock_get_user.return_value = self.user_information

        formatted_entity_data = auth0_event.format_entity_data(
            event=self.event
        )

        event_params = auth0_event.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=mock_get_user,
        )
        assert event_params == {
            "username": "test@example.com",
            "interface_name": "authldap",
        }

        log_record = auth0_event(session=session, event=self.event)
        assert log_record.component == "user"
        assert log_record.onderwerp == (
            "Inlogproces (auth0) voor gebruiker 'test@example.com' is mislukt,"
            " omdat de gebruiker is gedeactiveerd. Interface is 'authldap'."
        )
        assert log_record.event_type == "auth/auth0/user/status/inactive"
        assert log_record.event_data == {
            "correlation_id": self.correlation_id,
            "interface_name": "authldap",
            "username": "test@example.com",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_auth0.get_user")
    def test_auth0_user_marked_as_deleted_event(self, mock_get_user):
        auth0_event = Auth0UserMarkedAsDeleted()
        session = mock.MagicMock()

        self.event["event_name"] = "Auth0UserMarkedAsDeleted"
        mock_get_user.return_value = self.user_information

        formatted_entity_data = auth0_event.format_entity_data(
            event=self.event
        )

        event_params = auth0_event.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=mock_get_user,
        )
        assert event_params == {
            "username": "test@example.com",
            "interface_name": "authldap",
        }

        log_record = auth0_event(session=session, event=self.event)
        assert log_record.component == "user"
        assert log_record.onderwerp == (
            "Inlogproces (auth0) voor gebruiker 'test@example.com' is mislukt,"
            " omdat de gebruiker als verwijderd is gemarkeerd. "
            "Interface is 'authldap'."
        )
        assert log_record.event_type == "auth/auth0/user/status/deleted"
        assert log_record.event_data == {
            "correlation_id": self.correlation_id,
            "interface_name": "authldap",
            "username": "test@example.com",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_auth0.get_user")
    def test_auth0_user_created(self, mock_get_user):
        auth0_event = Auth0UserCreated()
        session = mock.MagicMock()

        self.event["event_name"] = "Auth0UserCreated"
        mock_get_user.return_value = self.user_information

        formatted_entity_data = auth0_event.format_entity_data(
            event=self.event
        )

        event_params = auth0_event.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=mock_get_user,
        )
        assert event_params == {
            "username": "test@example.com",
            "interface_name": "authldap",
        }

        log_record = auth0_event(session=session, event=self.event)
        assert log_record.component == "user"
        assert log_record.onderwerp == (
            "Inlogproces auth0. Gebruiker 'test@example.com' aangemaakt, "
            "interface is 'authldap'."
        )
        assert log_record.event_type == "auth/auth0/user/created"
        assert log_record.event_data == {
            "correlation_id": self.correlation_id,
            "interface_name": "authldap",
            "username": "test@example.com",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_auth0.get_user")
    def test_auth0_user_uuid_none(self, mock_get_user):
        auth0_event = Auth0UserCreated()
        session = mock.MagicMock()

        self.event["event_name"] = "Auth0UserCreated"
        self.event["user_uuid"] = None
        mock_get_user.return_value = self.user_information

        formatted_entity_data = auth0_event.format_entity_data(
            event=self.event
        )

        event_params = auth0_event.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=mock_get_user,
        )
        assert event_params == {
            "username": "test@example.com",
            "interface_name": "authldap",
        }

        log_record = auth0_event(session=session, event=self.event)
        assert log_record.component == "user"
        assert log_record.onderwerp == (
            "Inlogproces auth0. Gebruiker 'test@example.com' aangemaakt, "
            "interface is 'authldap'."
        )
        assert log_record.event_type == "auth/auth0/user/created"
        assert log_record.event_data == {
            "correlation_id": self.correlation_id,
            "interface_name": "authldap",
            "username": "test@example.com",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_auth0.get_user")
    def test_auth0_user_not_found_exception(self, mock_get_user):
        auth0_event = Auth0UserCreated()
        session = mock.MagicMock()

        self.event["event_name"] = "Auth0UserCreated"
        mock_get_user.side_effect = NotFound

        formatted_entity_data = auth0_event.format_entity_data(
            event=self.event
        )

        event_params = auth0_event.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=mock_get_user,
        )
        assert event_params == {
            "username": "test@example.com",
            "interface_name": "authldap",
        }

        log_record = auth0_event(session=session, event=self.event)
        assert log_record.component == "user"
        assert log_record.onderwerp == (
            "Inlogproces auth0. Gebruiker 'test@example.com' aangemaakt, "
            "interface is 'authldap'."
        )
        assert log_record.event_type == "auth/auth0/user/created"
        assert log_record.event_data == {
            "correlation_id": self.correlation_id,
            "interface_name": "authldap",
            "username": "test@example.com",
        }


class TestEventLoggerJobs:
    def setup_method(self):
        self.user_information = UserInformation(
            id=random.randint(1, 100),
            display_name="test_user",
            type="medewerker",
        )

    @mock.patch("zsnl_amqp_consumers.event_logger_jobs.get_user")
    def test_job_created(self, mock_get_user):
        job_logger = JobCreated()
        session = mock.MagicMock()
        mock_get_user.return_value = self.user_information

        session = mock.Mock()
        event = {
            "changes": [
                {
                    "key": "status",
                    "new_value": "pending",
                    "old_value": None,
                },
                {
                    "key": "job_description",
                    "new_value": {
                        "job_type": "delete_case",
                        "parameter1": "value1",
                        "parameter2": ["value2.1", "value2.2"],
                    },
                    "old_value": None,
                },
                {
                    "key": "friendly_name",
                    "new_value": "foobar",
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.jobs",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Job",
            "event_name": "JobCreated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        log_record = job_logger(
            session=session,
            event=event,
        )

        assert (
            log_record.onderwerp
            == f"Job 'foobar' ({event['entity_id']}) van type 'Zaken vernietigen' aangemaakt."
        )
        assert log_record.event_type == "job/created"

    @mock.patch("zsnl_amqp_consumers.event_logger_jobs.get_user")
    def test_job_created_user_error(self, mock_get_user):
        job_logger = JobCreated()
        session = mock.MagicMock()
        mock_get_user.side_effect = NotFound

        session = mock.Mock()
        event = {
            "user_uuid": str(uuid4()),
        }

        log_record = job_logger(
            session=session,
            event=event,
        )
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_jobs.get_user")
    def test_job_cancelled(self, mock_get_user):
        j = JobCancelled()
        session = mock.MagicMock()
        mock_get_user.return_value = self.user_information

        session = mock.Mock()
        event = {
            "changes": [
                {
                    "key": "status",
                    "new_value": "cancelled",
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.jobs",
            "entity_data": {"friendly_name": "foobar"},
            "entity_id": str(uuid4()),
            "entity_type": "Job",
            "event_name": "JobCancelled",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        log_record = j(
            session=session,
            event=event,
        )

        assert (
            log_record.onderwerp
            == f"Job 'foobar' ({event['entity_id']}) geannuleerd."
        )
        assert log_record.event_type == "job/cancelled"

    @mock.patch("zsnl_amqp_consumers.event_logger_jobs.get_user")
    def test_job_deleted(self, mock_get_user):
        j = JobDeleted()
        session = mock.MagicMock()
        mock_get_user.return_value = self.user_information

        session = mock.Mock()
        event = {
            "changes": [
                {
                    "key": "status",
                    "new_value": "deleted",
                    "old_value": "finished",
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.jobs",
            "entity_data": {"friendly_name": "foobar"},
            "entity_id": str(uuid4()),
            "entity_type": "Job",
            "event_name": "JobDeleted",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        log_record = j(
            session=session,
            event=event,
        )

        assert (
            log_record.onderwerp
            == f"Job 'foobar' ({event['entity_id']}) verwijderd."
        )
        assert log_record.event_type == "job/deleted"

    @mock.patch("zsnl_amqp_consumers.event_logger_jobs.get_user")
    def test_job_results_downloaded(self, mock_get_user):
        j = JobResultsDownloaded()
        session = mock.MagicMock()
        mock_get_user.return_value = self.user_information

        session = mock.Mock()
        event = {
            "changes": [],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.jobs",
            "entity_data": {"friendly_name": "foobar"},
            "entity_id": str(uuid4()),
            "entity_type": "Job",
            "event_name": "JobResultsDownloaded",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        log_record = j(
            session=session,
            event=event,
        )

        assert (
            log_record.onderwerp
            == f"Resultaat van Job 'foobar' ({event['entity_id']}) gedownload."
        )
        assert log_record.event_type == "job/download_result"

    @mock.patch("zsnl_amqp_consumers.event_logger_jobs.get_user")
    def test_job_errors_downloaded(self, mock_get_user):
        j = JobErrorsDownloaded()
        session = mock.MagicMock()
        mock_get_user.return_value = self.user_information

        session = mock.Mock()
        event = {
            "changes": [],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.jobs",
            "entity_data": {"friendly_name": "foobar"},
            "entity_id": str(uuid4()),
            "entity_type": "Job",
            "event_name": "JobErrorsDownloaded",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        log_record = j(
            session=session,
            event=event,
        )

        assert (
            log_record.onderwerp
            == f"Foutrapportage van Job 'foobar' ({event['entity_id']}) gedownload."
        )
        assert log_record.event_type == "job/download_error"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    def test_case_delete(self, mock_get_user):
        logger = CaseDeleted()
        event = {
            "changes": [
                {
                    "key": "status",
                    "new_value": "deleted",
                    "old_value": "reso;ved",
                },
                {
                    "key": "deleted",
                    "new_value": "2025-02-12T10:52:30.430658",
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {"id": 1, "confidentiality": "public"},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateCleared",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_user.return_value = self.user_information

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Zaak 1 vernietigd door test_user"
        assert log_record.event_type == "case/delete"
