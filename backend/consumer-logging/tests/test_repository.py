# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import random
from collections import namedtuple
from minty.exceptions import NotFound
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_amqp_consumers import repository
from zsnl_domains.database.schema import (
    BibliotheekCategorie,
    BibliotheekKenmerk,
    BibliotheekNotificatie,
    BibliotheekSjabloon,
    Group,
    ObjectBibliotheekEntry,
    Role,
    ZaakBetrokkenen,
    Zaaktype,
    ZaaktypeNode,
)


class TestRepository:
    def test_format_date_from_iso(self):
        date_input = "2019-01-21"
        expected_result = "21-01-2019"
        result = repository.format_date_from_iso(date_input)
        assert result == expected_result

    def test_get_case(self):
        mock_case_row = mock.Mock(name="case_row")
        mock_case_row.configure_mock(
            id=223_344,
            confidentiality="public",
            case_type_name="Test Casetype",
        )

        mock_session = mock.MagicMock(name="session")
        mock_session.execute().one.side_effect = [mock_case_row]

        res = repository.get_case(
            session=mock_session, uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9"
        )
        assert res.id == 223_344
        assert isinstance(res, repository.CaseInformation)

    def test_get_case_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.execute().one.side_effect = NoResultFound
        with pytest.raises(NotFound) as error:
            repository.get_case(
                session=mock_session,
                uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
            )
            assert (
                error
                == "No case was found with UUID: 'c6a3a0de-39b5-11e9-a248-338f209d44e9'"
            )

    def test_get_user(self):
        mock_session = mock.MagicMock()
        user_result = namedtuple("Resultproxy", ["id", "display_name", "type"])

        mock_session.execute().fetchone.return_value = user_result(
            id=223_344, display_name="test_employee", type="medewerker"
        )
        res = repository.get_user(
            session=mock_session, uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9"
        )
        assert res.id == 223_344
        assert res.display_name == "test_employee"
        assert isinstance(res, repository.UserInformation)

        mock_session.execute().fetchone.return_value = user_result(
            id=223_344, display_name="test_company", type="bedrijf"
        )
        res = repository.get_user(
            session=mock_session, uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9"
        )
        assert res.id == 223_344
        assert res.display_name == "test_company"
        assert isinstance(res, repository.UserInformation)

        mock_session.execute().fetchone.return_value = user_result(
            id=223_344, display_name="test_person", type="natuurlijk_persoon"
        )
        res = repository.get_user(
            session=mock_session, uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9"
        )
        assert res.id == 223_344
        assert res.display_name == "test_person"
        assert isinstance(res, repository.UserInformation)

    def test_get_user_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as excinfo:
            repository.get_user(
                session=mock_session,
                uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
            )
        assert excinfo.value.args == (
            "No user was found with UUID: 'c6a3a0de-39b5-11e9-a248-338f209d44e9'",
            "user/not_found",
        )

    def test_get_contact_from_thread(self):
        mock_session = mock.MagicMock()

        mock_session.execute().fetchone.return_value = repository.Contact(
            id=1, type="employee"
        )
        res = repository.get_contact_from_thread(
            session=mock_session,
            thread_uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
        )
        assert res.id == 1
        assert res.type == "employee"

    def test_get_contact_from_thread_no_result(self):
        mock_session = mock.MagicMock()

        mock_session.execute().fetchone.return_value = None

        res = repository.get_contact_from_thread(
            session=mock_session,
            thread_uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
        )
        assert res.id is None
        assert res.type is None

    def test_get_contact_from_thread_no_contact(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.side_effect = NoResultFound

        return_value = repository.get_contact_from_thread(
            session=mock_session,
            thread_uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
        )

        assert isinstance(return_value, repository.Contact)
        assert return_value.id is None
        assert return_value.type is None

    def test_get_case_from_thread(self):
        mock_session = mock.Mock()
        mock_row = mock.Mock()
        mock_row.configure_mock(
            id=1,
            confidentiality="public",
            uuid=uuid4(),
            thread_is_notification=False,
        )
        mock_session.execute().one.return_value = mock_row
        return_value = repository.get_case_from_thread(
            session=mock_session,
            thread_uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
        )
        assert isinstance(return_value[0], repository.CaseInformation)
        assert isinstance(return_value[1], repository.ThreadInformation)
        assert return_value[0].id == 1
        assert return_value[0].confidentiality == "public"
        assert return_value[1].is_notification is False

    def test_get_case_from_thread_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.execute().one.side_effect = NoResultFound
        return_value = repository.get_case_from_thread(
            session=mock_session,
            thread_uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
        )
        assert isinstance(return_value[0], repository.CaseInformation)
        assert return_value[0].id is None
        assert return_value[0].confidentiality == "public"
        assert return_value[1].is_notification is False

    def test_get_case_type(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.return_value = Zaaktype(
            id=223_344,
            current_zaaktype_node=ZaaktypeNode(titel="title_for_casetype"),
        )
        res = repository.get_case_type(
            session=mock_session, uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9"
        )
        assert res.id == 223_344
        assert res.title == "title_for_casetype"
        assert isinstance(res, repository.CaseTypeInformation)

    def test_get_case_type_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.side_effect = NoResultFound

        res = repository.get_case_type(
            session=mock_session,
            uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
        )
        assert res.id == 0
        assert res.title == "Onbekend zaaktype"
        assert isinstance(res, repository.CaseTypeInformation)

    def test_get_case_type_version(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.return_value = ZaaktypeNode(
            zaaktype_id=223_344, titel="title_for_casetype", version=7
        )
        res = repository.get_case_type_version(
            session=mock_session, uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9"
        )
        assert res.case_type_id == 223_344
        assert res.title == "title_for_casetype"
        assert res.version == 7
        assert isinstance(res, repository.CaseTypeVersionInformation)

    def test_get_case_type_version_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.side_effect = NoResultFound

        with pytest.raises(NotFound) as error:
            repository.get_case_type_version(
                session=mock_session,
                uuid="c6a3a0de-39b5-11e9-a248-338f209d44e9",
            )
        assert (
            str(error.value)
            == "No case_type_version found with UUID: 'c6a3a0de-39b5-11e9-a248-338f209d44e9'"
        )

    def test_get_catalog_folder(self):
        mock_session = mock.MagicMock()

        folder_info = BibliotheekCategorie(
            id=100, uuid=str(uuid4()), naam="Folder Name Here"
        )
        mock_session.query().filter().one.return_value = folder_info
        res = repository.get_catalog_folder(
            session=mock_session, id=folder_info.id
        )
        assert res.id == folder_info.id
        assert res.uuid == folder_info.uuid
        assert res.name == folder_info.naam

    def test_get_catalog_folder_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.side_effect = NoResultFound

        folder_id = 12
        with pytest.raises(NotFound) as error:
            repository.get_catalog_folder(session=mock_session, id=folder_id)
        assert (
            str(error.value)
            == f"No catalog folder found with id: '{folder_id}'"
        )

    def test_get_object_type(self):
        mock_session = mock.MagicMock()

        object_type = ObjectBibliotheekEntry(
            id=100,
            object_uuid=str(uuid4()),
            bibliotheek_categorie_id=12,
            name="Folder Name Here",
        )
        mock_session.query().filter().one.return_value = object_type
        res = repository.get_object_type(
            session=mock_session, uuid=object_type.object_uuid
        )
        assert res.id == object_type.id
        assert res.title == object_type.name

    def test_get_object_type_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.query().filter().one.side_effect = NoResultFound

        object_type_uuid = uuid4()
        with pytest.raises(NotFound) as error:
            repository.get_object_type(
                session=mock_session, uuid=object_type_uuid
            )
        assert (
            str(error.value)
            == f"No object type found with uuid: '{object_type_uuid}'"
        )

    def test_get_attribute(self):
        mock_session = mock.MagicMock()

        attribute_info = BibliotheekKenmerk(
            id=100, uuid=str(uuid4()), magic_string="magic_string"
        )
        mock_session.query().filter().one.return_value = attribute_info
        res = repository.get_attribute_by_magic_string(
            session=mock_session, magic_string=attribute_info.magic_string
        )
        assert isinstance(res, repository.AttributeInformation)
        assert res.id == attribute_info.id
        assert res.magic_string == attribute_info.magic_string

        mock_session.query().filter().one.side_effect = NoResultFound
        with pytest.raises(NotFound):
            res = repository.get_attribute_by_magic_string(
                session=mock_session, magic_string=attribute_info.magic_string
            )

    def test_get_attribute_by_magic_string_not_found(self):
        mock_session = mock.MagicMock()
        magic_string = "magic_string_name"
        mock_session.query().filter().one.side_effect = NoResultFound

        with pytest.raises(NotFound) as excinfo:
            repository.get_attribute_by_magic_string(
                session=mock_session, magic_string=magic_string
            )

        assert excinfo.value.args == (
            f"Attribute with magic_string {magic_string} not found",
            "attribute/not_found",
        )

    def test_get_attribute_by_magic_string(self):
        mock_session = mock.MagicMock()

        attribute_info = BibliotheekKenmerk(
            id=100, uuid=str(uuid4()), magic_string="magic_string"
        )
        mock_session.query().filter().one.return_value = attribute_info
        res = repository.get_attribute(
            session=mock_session, uuid=attribute_info.id
        )
        assert isinstance(res, repository.AttributeInformation)
        assert res.id == attribute_info.id
        assert res.magic_string == attribute_info.magic_string

        mock_session.query().filter().one.side_effect = NoResultFound
        with pytest.raises(NotFound):
            res = repository.get_attribute(
                session=mock_session, uuid=attribute_info.id
            )

    def test_get_attribute_not_found(self):
        mock_session = mock.MagicMock()
        uuid = uuid4()
        mock_session.query().filter().one.side_effect = NoResultFound

        with pytest.raises(NotFound) as excinfo:
            repository.get_attribute(session=mock_session, uuid=uuid)

        assert excinfo.value.args == (
            f"Attribute with uuid {uuid} not found",
            "attribute/not_found",
        )

    def test_get_email_template_not_found(self):
        mock_session = mock.MagicMock()
        uuid = uuid4()
        mock_session.query().filter().one.side_effect = NoResultFound

        assert (
            repository.get_email_template(session=mock_session, uuid=uuid)
            is None
        )

    def test_get_email_template(self):
        mock_session = mock.MagicMock()

        email_template = BibliotheekNotificatie(
            id=100, uuid=str(uuid4()), label="label"
        )
        mock_session.query().filter().one.return_value = email_template
        res = repository.get_email_template(
            session=mock_session, uuid=email_template.uuid
        )
        assert isinstance(res, repository.EmailTemplateInformation)
        assert res.id == email_template.id
        assert res.label == email_template.label

    def test_get_document_template_not_found(self):
        mock_session = mock.MagicMock()
        uuid = uuid4()
        mock_session.query().filter().one.side_effect = NoResultFound

        with pytest.raises(NotFound) as excinfo:
            repository.get_document_template(session=mock_session, uuid=uuid)

        assert excinfo.value.args == (
            f"Document template with uuid: {uuid} not found",
            "document_template/not_found",
        )

    def test_get_document_template(self):
        mock_session = mock.MagicMock()

        document_template = BibliotheekSjabloon(
            id=100, uuid=str(uuid4()), naam="document"
        )
        mock_session.query().filter().one.return_value = document_template
        res = repository.get_document_template(
            session=mock_session, uuid=document_template.uuid
        )
        assert isinstance(res, repository.DocumentTemplateInformation)
        assert res.id == document_template.id
        assert res.name == document_template.naam

    def test_get_subject_relation(self):
        mock_session = mock.MagicMock()
        uuid = uuid4()
        subject_relation = ZaakBetrokkenen(
            id=100,
            uuid=str(uuid),
            betrokkene_type="medewerker",
            gegevens_magazijn_id=2,
        )
        mock_session.query().filter().one.return_value = subject_relation
        res = repository.get_subject_relation(
            session=mock_session, uuid=subject_relation.uuid
        )
        assert isinstance(res, repository.SubjectRelationInformation)
        assert res.id == subject_relation.id
        assert res.subject_type == subject_relation.betrokkene_type
        assert res.subject_id == subject_relation.gegevens_magazijn_id

        with pytest.raises(NotFound) as excinfo:
            mock_session.query().filter().one.side_effect = NoResultFound
            repository.get_subject_relation(session=mock_session, uuid=uuid)

        assert excinfo.value.args == (
            f"Subject relation with uuid: {uuid} not found",
            "subject_relation/not_found",
        )

    def test_get_department(self):
        mock_session = mock.MagicMock()
        department_uuid = uuid4()
        department = Group(id=100, uuid=department_uuid, name="Frontofiice")

        mock_session.query().filter().one.return_value = department
        res = repository.get_department(
            session=mock_session, uuid=department.uuid
        )
        assert isinstance(res, repository.DepartmentInformation)
        assert res.id == department.id
        assert res.name == department.name

        with pytest.raises(NotFound) as excinfo:
            mock_session.query().filter().one.side_effect = NoResultFound
            repository.get_department(
                session=mock_session, uuid=department_uuid
            )

        assert excinfo.value.args == (
            f"Department with uuid: {department_uuid} not found",
            "department/not_found",
        )

    def test_get_role(self):
        mock_session = mock.MagicMock()
        role_uuid = uuid4()
        role = Role(id=100, uuid=role_uuid, name="Administrator")

        mock_session.query().filter().one.return_value = role
        res = repository.get_role(session=mock_session, uuid=role.uuid)
        assert isinstance(res, repository.RoleInformation)
        assert res.id == role.id
        assert res.name == role.name

        with pytest.raises(NotFound) as excinfo:
            mock_session.query().filter().one.side_effect = NoResultFound
            repository.get_role(session=mock_session, uuid=role_uuid)

        assert excinfo.value.args == (
            f"Role with uuid: {role_uuid} not found",
            "role/not_found",
        )

    def test_get_document(self):
        mock_session = mock.MagicMock()
        document_uuid = uuid4()
        document = namedtuple(
            "Document",
            ["id", "name", "extension", "mimetype", "version"],
        )(
            id=100,
            name="doc",
            extension=".doc",
            version=1,
            mimetype="application/pdf",
        )

        mock_session.execute().fetchone.return_value = document
        res = repository.get_document(session=mock_session, uuid=document_uuid)
        assert isinstance(res, repository.DocumentInformation)
        assert res.id == document.id
        assert res.name == document.name + document.extension
        assert res.mimetype == document.mimetype
        assert res.version == document.version

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_document(session=mock_session, uuid=document_uuid)

        assert excinfo.value.args == (
            f"Document with uuid: {document_uuid} not found",
            "document/not_found",
        )

    def test_get_case_assignee(self):
        mock_session = mock.MagicMock()
        case_uuid = uuid4()
        assignee_uuid = uuid4()
        subject = namedtuple("Subject", ["id", "properties", "uuid"])(
            id=12, properties={"displayname": "test admin"}, uuid=assignee_uuid
        )

        mock_session.execute().fetchone.return_value = subject
        res = repository.get_case_assignee(
            session=mock_session, case_uuid=case_uuid
        )
        assert isinstance(res, repository.SubjectInformation)
        assert res.id == subject.id
        assert res.display_name == "test admin"
        assert res.uuid == assignee_uuid

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_case_assignee(
                session=mock_session, case_uuid=case_uuid
            )

        assert excinfo.value.args == (
            f"Case with uuid: {case_uuid} is not assigned",
            "case/not_assigned",
        )

    def test_file(self):
        mock_session = mock.MagicMock()
        filestore_uuid = uuid4()
        file = namedtuple("Filestore", ["id", "original_name"])(
            id=2, original_name="test.eml"
        )

        mock_session.execute().fetchone.return_value = file
        res = repository.get_file(session=mock_session, uuid=filestore_uuid)
        assert isinstance(res, repository.FileInformation)
        assert res.id == file.id
        assert res.name == file.original_name

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_file(session=mock_session, uuid=filestore_uuid)

        assert excinfo.value.args == (
            f"File with uuid: {filestore_uuid} not found",
            "file/not_found",
        )

    def test_get_custom_object(self):
        mock_session = mock.MagicMock()
        custom_object_uuid = uuid4()
        version_independent_uuid = uuid4()
        custom_object = namedtuple(
            "CustomObjectVersion",
            [
                "title",
                "custom_object_type_version_id",
                "custom_object_type",
                "custom_object_uuid",
                "version",
                "custom_field_definition",
            ],
        )(
            title="CustObj1",
            custom_object_type_version_id=1,
            custom_object_type="CustObjType1",
            custom_object_uuid=version_independent_uuid,
            version=random.randint(1, 100),
            custom_field_definition={
                "custom_fields": [
                    {
                        "name": "Name of attribute in catalog",
                        "label": "Name of attribute in custom object type",
                        "options": [],
                        "description": "Longer description in object type",
                        "is_required": True,
                        "magic_string": "magic_string_here",
                        "attribute_uuid": str(uuid4()),
                        "is_hidden_field": False,
                        "multiple_values": False,
                        "custom_field_type": "text",
                        "external_description": "",
                        "custom_field_specification": None,
                    },
                    {
                        "name": "Second Attribute",
                        "label": "Second",
                        "options": [],
                        "description": "Longer description in object type",
                        "is_required": True,
                        "magic_string": "magic_string_here_2",
                        "attribute_uuid": str(uuid4()),
                        "is_hidden_field": False,
                        "multiple_values": False,
                        "custom_field_type": "text",
                        "external_description": "",
                        "custom_field_specification": None,
                    },
                ]
            },
        )

        mock_session.execute().fetchone.return_value = custom_object
        res = repository.get_custom_object(
            session=mock_session, uuid=custom_object_uuid
        )
        assert isinstance(res, repository.CustomObjectInformation)
        assert res.title == custom_object.title
        assert res.custom_object_type == custom_object.custom_object_type

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_custom_object(
                session=mock_session, uuid=custom_object_uuid
            )

        assert excinfo.value.args == (
            f"Custom Object with uuid: {custom_object_uuid} not found",
            "custom_object/not_found",
        )

    def test_get_custom_object_type(self):
        mock_session = mock.MagicMock()
        custom_object_type_uuid = uuid4()
        custom_object_type = namedtuple(
            "CustomObjectTypeVersion", ["id", "name"]
        )(
            id=101,
            name="CustObjType1",
        )

        mock_session.execute().fetchone.return_value = custom_object_type
        res = repository.get_custom_object_type(
            session=mock_session, uuid=custom_object_type_uuid
        )
        assert isinstance(res, repository.CustomObjectTypeInformation)
        assert res.name == custom_object_type.name
        assert res.id == custom_object_type.id

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_custom_object_type(
                session=mock_session, uuid=custom_object_type_uuid
            )

        assert excinfo.value.args == (
            f"Custom object type with uuid: {custom_object_type_uuid} not found",
            "custom_object_type/not_found",
        )

    def test_get_person(self):
        mock_session = mock.MagicMock()
        person_uuid = uuid4()
        person = namedtuple("PersonInformation", ["id", "object_type"])(
            id=101,
            object_type="natuurlijk_persoon",
        )

        mock_session.execute().fetchone.return_value = person
        res = repository.get_person(session=mock_session, uuid=person_uuid)
        assert isinstance(res, repository.PersonInformation)
        assert res.type == person.object_type
        assert res.id == person.id

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_person(session=mock_session, uuid=person_uuid)

        assert excinfo.value.args == (
            f"Person with uuid: {person_uuid} not found",
            "person/not_found",
        )

    def test_get_organization(self):
        mock_session = mock.MagicMock()
        organization_uuid = uuid4()
        organization = namedtuple(
            "OrganizationInformation", ["id", "object_type"]
        )(
            id=101,
            object_type="organization",
        )

        mock_session.execute().fetchone.return_value = organization
        res = repository.get_organization(
            session=mock_session, uuid=organization_uuid
        )
        assert isinstance(res, repository.OrganistionInformation)
        assert res.type == organization.object_type
        assert res.id == organization.id

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_organization(
                session=mock_session, uuid=organization_uuid
            )

        assert excinfo.value.args == (
            f"Bedrijf with uuid: {organization_uuid} not found",
            "bedrijf/not_found",
        )

    def test_get_employee(self):
        mock_session = mock.MagicMock()
        employee_uuid = uuid4()
        employee = namedtuple("EmployeeInformation", ["id", "subject_type"])(
            id=101,
            subject_type="employee",
        )

        mock_session.execute().fetchone.return_value = employee
        res = repository.get_employee(session=mock_session, uuid=employee_uuid)
        assert isinstance(res, repository.EmployeeInformation)
        assert res.type == employee.subject_type
        assert res.id == employee.id

        with pytest.raises(NotFound) as excinfo:
            mock_session.execute().fetchone.return_value = None
            repository.get_employee(session=mock_session, uuid=employee_uuid)

        assert excinfo.value.args == (
            f"Employee with uuid: {employee_uuid} not found",
            "employee/not_found",
        )

    def test_get_email_configuration(self):
        mock_session = mock.MagicMock()

        db_row = mock.Mock()
        db_row.configure_mock(
            interface_config={
                "sender_name": "sender",
                "api_user": "sender@example.com",
            },
        )
        mock_session.execute().fetchone.return_value = db_row

        res: dict = repository.get_email_configuration(session=mock_session)
        assert isinstance(res, dict)
        assert res.get("sender_name") == "sender"
        assert res.get("sender_address") == "sender@example.com"

    def test_get_email_configuration_no_result(self):
        mock_session = mock.MagicMock()
        mock_session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as excinfo:
            repository.get_email_configuration(session=mock_session)

        assert excinfo.value.args == (
            "No outgoing email configuration found",
            "communication/email/no_configuration",
        )
