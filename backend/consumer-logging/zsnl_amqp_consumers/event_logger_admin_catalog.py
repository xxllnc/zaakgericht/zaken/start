# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    DocumentTemplateInformation,
    EmailTemplateInformation,
    get_case_type,
    get_case_type_version,
    get_catalog_folder,
    get_document_template,
    get_email_template,
    get_user,
)
from zsnl_domains.database.schema import Logging


class AdminCatalogBase(BaseLogger):
    def __init__(self):
        self.component = None

    def __call__(self, session, event) -> Logging:
        """Collect necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case foundW
        :return: logging record
        :rtype: Logging
        """
        user_info = get_user(session=session, uuid=event["user_uuid"])

        case_type_info = get_case_type(
            session=session, uuid=event["entity_id"]
        )

        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data, case_type_info=case_type_info
        )

        event_type_format = self.get_event_type_format(
            event_params=event_params
        )

        subject = self._generate_subject(
            subject_template=event_type_format["subject_template"],
            event_params=event_params,
        )
        return self._create_logging_record(
            user_info=user_info,
            event_type=event_type_format["event_type"],
            subject=subject,
            component=self.component,
            component_id=case_type_info.id,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def _generate_subject(
        self, subject_template: str, event_params: dict
    ) -> str:
        """format subject from event_params.

        :param subject: un-formatted subject
        :type subject: str
        :param event_params: event_parameters
        :type event_params: dict
        :return: formatted_subject
        :rtype: str
        """
        formatted_subject = subject_template.format(
            event_params["casetype_title"], event_params["reason"]
        )
        return formatted_subject

    def generate_event_parameters(self, entity_data: dict, case_type_info):
        raise NotImplementedError

    def get_event_type_format(self, event_params):
        raise NotImplementedError


class CaseTypeOnlineStatusChanged(AdminCatalogBase):
    def __init__(self):
        super().__init__()
        self.component = "zaaktype"
        self.activated = {
            "subject_template": 'Zaaktype "{}" geactiveerd: {}',
            "event_type": "casetype/publish",
        }
        self.deactivated = {
            "subject_template": 'Zaaktype "{}" gedeactiveerd: {}',
            "event_type": "casetype/unpublish",
        }

    def generate_event_parameters(self, entity_data: dict, case_type_info):
        """Generate subject from event parameters.

        :param entity_data: entity data
        :type entity_data: dict
        :param case_type_info: case type info
        :type case_type_info: CaseTypeInfo
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "case_type_id": case_type_info.id,
            "casetype_title": case_type_info.title,
            "reason": entity_data["reason"],
            "active": entity_data["active"],
        }
        return event_parameters

    def get_event_type_format(self, event_params: dict):
        """Select and return format for activated or deactivated event type.

        :param event_params: event parameters
        :type event_params: dict
        :return: format for logline
        :rtype: dict
        """
        if event_params["active"] is True:
            event_type_format = self.activated
        else:
            event_type_format = self.deactivated
        return event_type_format


class CaseTypeDeleted(AdminCatalogBase):
    def __init__(self):
        super().__init__()
        self.component = "zaaktype"
        self.event_data = {
            "subject_template": 'Zaaktype "{}" verwijderd: {}',
            "event_type": "casetype/remove",
        }

    def generate_event_parameters(self, entity_data: dict, case_type_info):
        """Generate subject from event parameters.

        :param entity_data: entity data
        :type entity_data: dict
        :param case_type_info: case type info
        :type case_type_info: CaseTypeInformation
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "casetype_id": case_type_info.id,
            "title": case_type_info.title,
            "reason": entity_data["reason"],
        }
        return event_parameters

    def _generate_subject(
        self, subject_template: str, event_params: dict
    ) -> str:
        """format subject from event_params.

        :param subject: un-formatted subject
        :type subject: str
        :param event_params: event_parameters
        :type event_params: dict
        :return: formatted_subject
        :rtype: str
        """
        formatted_subject = subject_template.format(
            event_params["title"], event_params["reason"]
        )
        return formatted_subject

    def get_event_type_format(self, event_params: dict):
        """Select and return format for CaseTypeDeleted event type.

        :param event_params: event parameters
        :type event_params: dict
        :return: format for logline
        :rtype: dict
        """
        return self.event_data


class CaseTypeVersionUpdated(BaseLogger):
    def __init__(self):
        super().__init__()
        self.component = "zaaktype"
        self.version_activated = {
            "subject_template": 'Zaaktype "{}" gewijzigd naar versie {}: {}',
            "event_type": "casetype/update/version",
        }

    def __call__(self, session, event) -> Logging:
        """Collect necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case foundW
        :return: logging record
        :rtype: Logging
        """
        user_info = get_user(session=session, uuid=event["user_uuid"])

        case_type_version_info = get_case_type_version(
            session=session, uuid=self.get_version_uuid(event)
        )

        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            case_type_version_info=case_type_version_info,
        )

        event_type_format = self.get_event_type_format(
            event_params=event_params
        )

        subject = self._generate_subject(
            subject_template=event_type_format["subject_template"],
            event_params=event_params,
        )
        return self._create_logging_record(
            user_info=user_info,
            event_type=event_type_format["event_type"],
            subject=subject,
            component=self.component,
            component_id=case_type_version_info.case_type_id,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def get_version_uuid(self, event):
        """Get the version uuid from the changes in the event.

        :param event:
        :return:
        """
        for change in event["changes"]:
            if change["key"] == "current_version_uuid":
                return change["new_value"]

    def generate_event_parameters(
        self, entity_data: dict, case_type_version_info
    ):
        """Generate subject from event parameters.

        :param entity_data: entity data
        :type entity_data: dict
        :param case_type_version_info: case type info
        :type case_type_version_info: CaseTypeInfo
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "casetype_id": case_type_version_info.case_type_id,
            "casetype_version_title": case_type_version_info.title,
            "reason": entity_data["reason"],
            "version": case_type_version_info.version,
        }
        return event_parameters

    def get_event_type_format(self, event_params: dict):
        """Select and return format for version activated event type.

        :param event_params: event parameters
        :type event_params: dict
        :return: format for logline
        :rtype: dict
        """
        event_type_format = self.version_activated
        return event_type_format

    def _generate_subject(
        self, subject_template: str, event_params: dict
    ) -> str:
        """format subject from event_params.

        :param subject_template: un-formatted subject
        :type subject_template: str
        :param event_params: event_parameters
        :type event_params: dict
        :return: formatted_subject
        :rtype: str
        """
        formatted_subject = subject_template.format(
            event_params["casetype_version_title"],
            event_params["version"],
            event_params["reason"],
        )
        return formatted_subject


class FolderEntryMoved(BaseLogger):
    def __init__(self):
        super().__init__()
        self.mapping = {
            "attribute": "kenmerk",
            "case_type": "zaaktype",
            "custom_object_type": "objecttype (beta)",
            "document_template": "document sjabloon",
            "email_template": "email sjabloon",
            "folder": "map",
            "object_type": "objecttype",
        }

    def __call__(self, session, event) -> Logging:
        user_info = get_user(session=session, uuid=event["user_uuid"])
        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data
        )
        folder_info = None
        if event_params["destination_folder_id"] is not None:
            folder_info = get_catalog_folder(
                session, event_params["destination_folder_id"]
            )
            event_params["destination_folder_id"] = folder_info.id
            event_params["destination_folder_uuid"] = str(folder_info.uuid)
            event_params["destination_folder_name"] = f"'{folder_info.name}'"
        else:
            event_params["destination_folder_id"] = None
            event_params["destination_folder_uuid"] = None
            event_params["destination_folder_name"] = "'hoofdmap'"

        return self._create_logging_record(
            user_info=user_info,
            event_type="admin/catalog/folder_entries_moved",
            subject=self._generate_subject(event_params=event_params),
            component="folder",
            component_id=event_params["destination_folder_id"],
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def _generate_subject(self, event_params: dict):
        """Generate subject from event parameters.

        :param event_params: event parameters
        :type event_params: dict
        :return: subject
        :rtype: str
        """

        try:
            object_type = self.mapping[event_params["entry_type"]].capitalize()
        except KeyError:
            object_type = event_params["entry_type"]

        formatted_subject = "{} '{}' verplaatst naar {}".format(
            object_type,
            event_params["name"],
            event_params["destination_folder_name"],
        )
        return formatted_subject

    def generate_event_parameters(self, entity_data: dict):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "destination_folder_id": entity_data["folder_id"],
            "entry_type": entity_data["entry_type"],
            "name": entity_data["name"],
        }
        return event_parameters


class FolderDeleted(BaseLogger):
    def __init__(self):
        super().__init__()
        pass

    def __call__(self, session, event) -> Logging:
        user_info = get_user(session=session, uuid=event["user_uuid"])
        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type="folder/remove",
            subject=self._generate_subject(event_params=event_params),
            component="folder",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def _generate_subject(self, event_params: dict):
        """Generate subject from event parameters.

        :param event_params: event parameters
        :type event_params: dict
        :return: subject
        :rtype: str
        """
        formatted_subject = "Folder '{}' verwijderd: {}".format(
            event_params["name"], event_params["reason"]
        )
        return formatted_subject

    def generate_event_parameters(self, entity_data: dict):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "reason": entity_data["commit_message"],
            "name": entity_data["name"],
        }
        return event_parameters


class FolderCreated(BaseLogger):
    def __init__(self):
        super().__init__()
        pass

    def __call__(self, session, event) -> Logging:
        user_info = get_user(session=session, uuid=event["user_uuid"])
        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type="folder/create",
            subject=self._generate_subject(event_params=event_params),
            component="folder",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def _generate_subject(self, event_params: dict):
        """Generate subject from event parameters.

        :param event_params: event parameters
        :type event_params: dict
        :return: subject
        :rtype: str
        """
        formatted_subject = "Folder '{}' toegevoegd".format(
            event_params["name"]
        )
        return formatted_subject

    def generate_event_parameters(self, entity_data: dict):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {"name": entity_data["name"]}
        return event_parameters


class EmailTemplateLogging(BaseLogger):
    def __init__(self):
        super().__init__()
        pass

    def __call__(self, session, event) -> Logging:
        user_info = get_user(session=session, uuid=event["user_uuid"])
        emailtemplate_info = get_email_template(
            session=session, uuid=event["entity_id"]
        )

        if emailtemplate_info is None:
            return

        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            email_template_info=emailtemplate_info,
        )

        event_type = {
            "EmailTemplateCreated": "template/email/create",
            "EmailTemplateEdited": "template/email/update",
            "EmailTemplateDeleted": "template/email/remove",
        }
        label = formatted_entity_data.get("label", emailtemplate_info.label)

        return self._create_logging_record(
            user_info=user_info,
            event_type=event_type[event["event_name"]],
            subject=self._generate_subject(
                reason=formatted_entity_data["commit_message"],
                label=label,
                event_name=event["event_name"],
            ),
            component="notificatie",
            component_id=emailtemplate_info.id,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def _generate_subject(self, reason: str, label: str, event_name: str):
        """Generate subject from event parameters.

        :param event_params: event parameters
        :type event_params: dict
        :return: subject
        :rtype: str
        """
        event_type = {
            "EmailTemplateCreated": "toegevoegd",
            "EmailTemplateEdited": "opgeslagen",
            "EmailTemplateDeleted": "verwijderd",
        }

        formatted_subject = 'E-mailsjabloon "{}" {}: {}'.format(
            label, event_type[event_name], reason
        )
        return formatted_subject

    def generate_event_parameters(
        self, entity_data: dict, email_template_info: EmailTemplateInformation
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "template_id": email_template_info.id,
            "reason": entity_data["commit_message"],
        }
        return event_parameters


class DocumentTemplateLogging(BaseLogger):
    def __init__(self):
        super().__init__()
        pass

    def __call__(self, session, event) -> Logging:
        user_info = get_user(session=session, uuid=event["user_uuid"])
        document_template_info = get_document_template(
            session=session, uuid=event["entity_id"]
        )
        formatted_entity_data = self.format_entity_data(event=event)

        event_params = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_template_info=document_template_info,
        )

        event_type = {
            "DocumentTemplateCreated": "template/create",
            "DocumentTemplateEdited": "template/update",
            "DocumentTemplateDeleted": "template/remove",
        }
        name = formatted_entity_data.get("name", document_template_info.name)

        return self._create_logging_record(
            user_info=user_info,
            event_type=event_type[event["event_name"]],
            subject=self._generate_subject(
                reason=formatted_entity_data["commit_message"],
                name=name,
                event_name=event["event_name"],
            ),
            component="sjabloon",
            component_id=document_template_info.id,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_params,
        )

    def _generate_subject(self, reason: str, name: str, event_name: str):
        """Generate subject from event parameters.

        :param event_params: event parameters
        :type event_params: dict
        :return: subject
        :rtype: str
        """
        event_type = {
            "DocumentTemplateCreated": "toegevoegd",
            "DocumentTemplateEdited": "opgeslagen",
            "DocumentTemplateDeleted": "verwijderd",
        }

        formatted_subject = 'Sjabloon "{}" {}: {}'.format(
            name, event_type[event_name], reason
        )
        return formatted_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        document_template_info: DocumentTemplateInformation,
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "template_id": document_template_info.id,
            "reason": entity_data["commit_message"],
        }
        return event_parameters
