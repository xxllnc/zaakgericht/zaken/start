# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import get_case, get_document, get_user
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Logging


class ArchiveRequestBase(BaseLogger):
    def __init__(self):
        pass

    def __call__(self, session, event: dict) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """

        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)

        case_info = get_case(
            session=session, uuid=formatted_entity_data["case_uuid"]
        )

        document_string: str = ""
        first: bool = True
        for document_uuid in formatted_entity_data["document_uuids"]:
            try:
                document_info = get_document(
                    session=session, uuid=document_uuid
                )
            except NotFound:
                # Document not found.
                return

            if not first:
                document_string = document_string + ", "
            first = False
            document_string = (
                document_string
                + str(document_info.name)
                + " Versie: "
                + str(document_info.version)
            )

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            case_id=case_info.id,
            document_string=document_string,
        )

        event_parameters["correlation_id"] = event["correlation_id"]
        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=case_info.id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=(case_info.confidentiality != "public"),
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self, entity_data: dict, case_id: int, document_string: str
    ):
        raise NotImplementedError


class ArchiveRequest(ArchiveRequestBase):
    def __init__(self):
        super().__init__()
        self.component = "archief vezoek"
        self.subject = "Zaak: {} Archiefverzoek voor de documenten {}"
        self.variables = ["case_id", "document_string"]
        self.event_type = "archive_request/create"

    def generate_event_parameters(
        self, entity_data: dict, case_id: int, document_string: str
    ):
        event_parameters = {
            "case_id": case_id,
            "document_string": document_string,
        }
        return event_parameters
