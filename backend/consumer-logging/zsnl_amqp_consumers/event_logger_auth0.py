# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import UserInformation, get_user
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Logging


class Auth0Base(BaseLogger):
    def __init__(self):
        self.component = "user"
        self.variables = None
        self.event_type = None
        self.subject = None

    def __call__(self, session, event: dict) -> Logging:
        """Collect all necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: dict
        :return: logging record
        :rtype: Logging
        """

        # In the logic of exchaning an Auth0 authorization code for a token,
        # it may happen that no user can be found.
        if event["user_uuid"] in ["None", None]:
            user_info = UserInformation(None, None, None)
        else:
            try:
                user_info = get_user(session=session, uuid=event["user_uuid"])
            except NotFound:
                user_info = UserInformation(None, None, None)

        formatted_entity_data = self.format_entity_data(event=event)

        event_parameters = self.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            user_info=user_info,
        )

        event_parameters["correlation_id"] = event["correlation_id"]

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded
        message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        user_info: UserInformation,
    ):
        raise NotImplementedError


class Auth0InterfaceNotActive(Auth0Base):
    def __init__(self):
        super().__init__()
        self.subject = (
            "Inlogproces (auth0) voor gebruiker '{}' is mislukt, omdat de "
            "interface '{}' niet bestaat of is gedeactiveerd."
        )
        self.variables = ["username", "interface_name"]
        self.event_type = "auth/auth0/interface/status/inactive"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        user_info: UserInformation,
    ) -> dict:
        """Generate event parameters from entity data."""

        event_params = {
            "username": entity_data["username"],
            "interface_name": entity_data["interface_name"],
        }

        return event_params


class Auth0UserNotActive(Auth0Base):
    def __init__(self):
        super().__init__()
        self.subject = (
            "Inlogproces (auth0) voor gebruiker '{}' is mislukt, omdat de "
            "gebruiker is gedeactiveerd. Interface is '{}'."
        )
        self.variables = ["username", "interface_name"]
        self.event_type = "auth/auth0/user/status/inactive"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        user_info: UserInformation,
    ) -> dict:
        """Generate event parameters from entity data."""

        event_params = {
            "username": entity_data["username"],
            "interface_name": entity_data["interface_name"],
        }

        return event_params


class Auth0UserMarkedAsDeleted(Auth0Base):
    def __init__(self):
        super().__init__()
        self.subject = (
            "Inlogproces (auth0) voor gebruiker '{}' is mislukt, omdat de "
            "gebruiker als verwijderd is gemarkeerd. Interface is '{}'."
        )
        self.variables = ["username", "interface_name"]
        self.event_type = "auth/auth0/user/status/deleted"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        user_info: UserInformation,
    ) -> dict:
        """Generate event parameters from entity data."""

        event_params = {
            "username": entity_data["username"],
            "interface_name": entity_data["interface_name"],
        }

        return event_params


class Auth0UserCreated(Auth0Base):
    def __init__(self):
        super().__init__()
        self.subject = (
            "Inlogproces auth0. Gebruiker '{}' aangemaakt, interface is '{}'."
        )
        self.variables = ["username", "interface_name"]
        self.event_type = "auth/auth0/user/created"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        user_info: UserInformation,
    ) -> dict:
        """Generate event parameters from entity data."""

        event_params = {
            "username": entity_data["username"],
            "interface_name": "authldap",
        }

        return event_params
