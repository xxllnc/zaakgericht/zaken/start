# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import repository
from .event_logger_base import BaseLogger
from collections import namedtuple
from zsnl_domains.database.schema import Logging

EventData = namedtuple("EventData", "subject type")
TaskInformation = namedtuple(
    "TaslInformation",
    "uuid title related_case_id related_case_confidentiality",
)


class TaskLogger(BaseLogger):
    def __init__(self):
        pass

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """
        user_info = repository.get_user(
            session=session, uuid=event["user_uuid"]
        )

        case_uuid = event["entity_data"]["case"]["uuid"]
        case = repository.get_case(session, case_uuid)
        task = TaskInformation(
            uuid=event["entity_id"],
            title=event["entity_data"]["title"],
            related_case_confidentiality=case.confidentiality,
            related_case_id=case.id,
        )

        event_parameters = {
            "uuid": str(task.uuid),
            "title": task.title,
            "correlation_id": event["correlation_id"],
        }

        event_data = self.generate_task_info(
            title=task.title, event_name=event["event_name"]
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=event_data.type,
            subject=event_data.subject,
            component="zaak",
            component_id=None,
            case_id=task.related_case_id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=(task.related_case_confidentiality != "public"),
        )

    def generate_task_info(self, title, event_name) -> EventData:
        """Generate Task information."""
        events = {
            "TaskCreated": EventData(
                subject=f"Taak '{title}' aangemaakt.", type="case/task/created"
            ),
            "TaskDeleted": EventData(
                subject=f"Taak '{title}' verwijderd.", type="case/task/deleted"
            ),
            "TaskUpdated": EventData(
                subject=f"Taak '{title}' gewijzigd.", type="case/task/updated"
            ),
        }
        return events[event_name]
