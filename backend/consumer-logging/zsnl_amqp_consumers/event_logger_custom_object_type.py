# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    CustomObjectTypeInformation,
    get_custom_object_type,
    get_user,
)
from zsnl_domains.database.schema import Logging


class CustomObjectTypeBase(BaseLogger):
    def __init__(self):
        self.variables = None
        self.event_type = None
        self.subject = None
        self.component_mapping = {
            "attributes": "Algemeen",
            "custom_fields": "Kenmerken",
            "relationships": "Relaties",
            "authorizations": "Rechten",
        }

    def __call__(self, session, event) -> Logging:
        "Collect and necessary information before creating log record."

        custom_object_type_info = get_custom_object_type(
            session=session, uuid=event["entity_id"]
        )

        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            custom_object_type_info=custom_object_type_info,
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="custom_object_type",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        "Generate subject line formatted with parameters from decoded message."

        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_type_info: CustomObjectTypeInformation,
    ):
        raise NotImplementedError


class CustomObjectTypeCreated(CustomObjectTypeBase):
    def __init__(self):
        super().__init__()
        self.subject = "Objecttype '{}' aangemaakt."
        self.variables = ["custom_object_type"]
        self.event_type = "custom_object_type/created"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_type_info: CustomObjectTypeInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_type": custom_object_type_info.name,
        }

        return event_params


class CustomObjectTypeUpdated(CustomObjectTypeBase):
    def __init__(self):
        super().__init__()
        self.subject = "Objecttype '{}' gewijzigd ({})."
        self.variables = ["custom_object_type", "updated_component_list"]
        self.event_type = "custom_object_type/updated"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_type_info: CustomObjectTypeInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        component_list = entity_data["audit_log"]["updated_components"]
        updated_component_list = ",".join(
            self.component_mapping[component] for component in component_list
        )

        event_params = {
            "custom_object_type": custom_object_type_info.name,
            "updated_component_list": updated_component_list,
        }

        return event_params


class CustomObjectTypeDeleted(CustomObjectTypeBase):
    def __init__(self):
        super().__init__()
        self.subject = "Objecttype '{}' verwijderd."
        self.variables = ["custom_object_type"]
        self.event_type = "custom_object_type/deleted"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_type_info: CustomObjectTypeInformation,
    ) -> dict:
        "Generate event parameters from entity data."
        event_params = {
            "custom_object_type": custom_object_type_info.name,
        }

        return event_params
