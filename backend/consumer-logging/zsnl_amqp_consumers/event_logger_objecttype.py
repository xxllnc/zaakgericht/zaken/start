# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import ObjectTypeInformation, get_object_type, get_user
from zsnl_domains.database.schema import Logging


class ObjectTypeBase(BaseLogger):
    def __init__(self):
        self.variables = None
        self.event_type = None
        self.subject = None

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """
        object_type_info = (
            get_object_type(session=session, uuid=event["entity_id"])
            if event["event_name"] != "ObjectTypeDeleted"
            else None
        )

        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)
        commit_message = formatted_entity_data["commit_message"]

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            object_type_info=object_type_info,
            commit_message=commit_message,
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="object_type",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        object_type_info: ObjectTypeInformation,
        commit_message: str,
    ):
        raise NotImplementedError


class ObjectTypeDeleted(ObjectTypeBase):
    def __init__(self):
        super().__init__()
        self.subject = '{} "{}" verwijderd: {}'
        self.variables = ["object_type", "object_label", "reason"]
        self.event_type = "object/delete"

    def generate_event_parameters(
        self,
        entity_data: dict,
        object_type_info: ObjectTypeInformation,
        commit_message: str,
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :param object_type_info: tuple with the object type data
        :type object_type_info: ObjectTypeInformation
        :param commit_message: the reason for deletion
        :type commit_message: str
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "object_type": "ObjectType",
            "object_label": entity_data["name"],
            "reason": commit_message,
        }
        return event_parameters
