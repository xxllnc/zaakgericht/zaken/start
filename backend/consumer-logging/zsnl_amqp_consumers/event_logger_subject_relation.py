# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    SubjectRelationInformation,
    get_case,
    get_subject_relation,
    get_user,
)
from zsnl_domains.database.schema import Logging


class SubjectRelationBase(BaseLogger):
    def __init__(self):
        pass

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """

        case_uuid = event["entity_data"]["case"]["id"]
        case_info = get_case(session=session, uuid=case_uuid)

        user_info = get_user(session=session, uuid=event["user_uuid"])

        subject_relation_info = get_subject_relation(
            session=session, uuid=event["entity_id"]
        )

        formatted_entity_data = self.format_entity_data(event=event)

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            case_id=case_info.id,
            subject_relation_info=subject_relation_info,
        )
        event_parameters["correlation_id"] = event["correlation_id"]
        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=case_info.id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=(case_info.confidentiality != "public"),
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(self, entity_data: dict, case_id: int):
        raise NotImplementedError


class SubjectRelationCreated(SubjectRelationBase):
    def __init__(self):
        super().__init__()
        self.component = "zaak"
        self.subject = "Betrokkene '{}' toegevoegd aan zaak {} als {}"
        self.variables = ["subject_name", "case_id", "role"]
        self.event_type = "case/subject/add"

    def generate_event_parameters(
        self,
        entity_data: dict,
        case_id: int,
        subject_relation_info: SubjectRelationInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "case_subject_id": subject_relation_info.id,
            "params": self._generate_subject_relation_parameters(
                entity_data, subject_relation_info
            ),
            "subject_name": entity_data["subject"]["name"],
            "role": entity_data["role"],
            "subject_id": subject_relation_info.id,
        }
        return event_parameters

    def _generate_subject_relation_parameters(
        self,
        entity_data: dict,
        subject_relation_info: SubjectRelationInformation,
    ):
        params = {
            "magic_string_prefix": entity_data["magic_string_prefix"],
            "rol": entity_data["role"],
            "betrokkene_identifier": f"betrokkene-{subject_relation_info.subject_type}-{subject_relation_info.subject_id}",
        }

        if entity_data["subject"]["type"] == "employee":
            params.update(
                {"employee_authorisation": entity_data.get("permission")}
            )
        else:
            params.update(
                {
                    "pip_authorized": "1"
                    if entity_data.get("authorized")
                    else "0"
                }
            )
        return params


class SubjectRelationUpdated(SubjectRelationBase):
    def __init__(self):
        super().__init__()
        self.component = "zaak"
        self.subject = "Betrokkene '{}' van zaak {} bijgewerkt"
        self.variables = ["subject_name", "case_id"]
        self.event_type = "case/subject/update"

    def generate_event_parameters(
        self,
        entity_data: dict,
        case_id: int,
        subject_relation_info: SubjectRelationInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "case_subject_id": subject_relation_info.id,
            "params": self._generate_subject_relation_parameters(
                entity_data, subject_relation_info
            ),
            "subject_name": entity_data["subject"]["name"],
            "role": entity_data.get("role"),
            "subject_id": subject_relation_info.id,
        }
        return event_parameters

    def _generate_subject_relation_parameters(
        self,
        entity_data: dict,
        subject_relation_info: SubjectRelationInformation,
    ):
        params = {
            "magic_string_prefix": entity_data.get("magic_string_prefix"),
            "rol": entity_data.get("role"),
            "betrokkene_identifier": f"betrokkene-{subject_relation_info.subject_type}-{subject_relation_info.subject_id}",
        }

        if entity_data["subject"]["type"] == "employee":
            params.update(
                {"employee_authorisation": entity_data.get("permission")}
            )
        else:
            params.update(
                {
                    "pip_authorized": "1"
                    if entity_data.get("authorized")
                    else "0"
                }
            )
        return params


class SubjectRelationDeleted(SubjectRelationBase):
    def __init__(self):
        super().__init__()
        self.component = "zaak"
        self.subject = "Betrokkene '{}' verwijderd van zaak {} als {}"
        self.variables = ["subject_name", "case_id", "role"]
        self.event_type = "case/subject/remove"

    def generate_event_parameters(
        self,
        entity_data: dict,
        case_id: int,
        subject_relation_info: SubjectRelationInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "role": entity_data.get("role"),
            "subject_name": entity_data.get("subject")["name"],
        }
        return event_parameters
