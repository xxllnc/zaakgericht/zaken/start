# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_notifier_base import BaseNotifier
from .repository import (
    DocumentInformation,
    get_case_assignee,
    get_document,
    get_user,
)
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Message


class DocumentBase(BaseNotifier):
    def __call__(self, session, event, logging_id) -> Message:
        """Collect and necessary information before creating log record.

        :param BaseMessage: base class for message
        :type BaseMessage: BaseMessage
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :param logging_id: id of entry in logging table
        :type event: int
        :raises error: no document found or no case_assignee found
        :return: logging record
        :rtype: Logging
        """

        formatted_entity_data = self.format_entity_data(event=event)
        case_uuid = formatted_entity_data.get("case_uuid")

        if case_uuid:
            # If the case has no assignee, no message notification
            try:
                assignee_info = get_case_assignee(
                    session=session, case_uuid=case_uuid
                )
            except NotFound:
                return

            # If the assignee of case is the logged in user, no message notification
            if str(assignee_info.uuid) == event["user_uuid"]:
                return

        else:
            # If document has no case that means document is in document_intake
            assignee_uuid = formatted_entity_data.get("intake_owner_uuid")

            # If the intake document has no assignee, or if the assignee of document in intake is the logged in user,
            # no message notification
            if not assignee_uuid or str(assignee_uuid) == event["user_uuid"]:
                return

            # If the assignee of intake document cannot be found, no message notification
            try:
                assignee_info = get_user(session=session, uuid=assignee_uuid)
            except NotFound:
                return

        try:
            document_info = get_document(
                session=session, uuid=event["entity_id"]
            )
        except NotFound:
            # Document not found.
            return

        return self._create_message_notifier_record(
            assignee_info=assignee_info,
            subject=self._generate_subject(document_info),
            logging_id=logging_id,
            is_read=False,
            is_archived=False,
        )

    def _generate_subject(self, document_info: DocumentInformation) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param document_info: info of document
        :type event_parameters: DocumentInformation
        :return: message line to log
        :rtype: str
        """
        raise NotImplementedError


class DocumentAddedToCase(DocumentBase):
    def _generate_subject(self, document_info: DocumentInformation):
        return f"Document '{document_info.name}' assign"


class DocumentCreated(DocumentBase):
    def _generate_subject(self, document_info: DocumentInformation):
        return f"Document '{document_info.name}' aangemaakt"


class DocumentAssignedToUser(DocumentBase):
    def _generate_subject(self, document_info: DocumentInformation):
        return f"Document '{document_info.name}' is aan u toegewezen"
