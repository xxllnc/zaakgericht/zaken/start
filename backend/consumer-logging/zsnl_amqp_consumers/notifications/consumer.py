# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import os
import requests
from ..repository import get_case_from_thread
from amqpstorm import Message
from json.decoder import JSONDecodeError
from minty.exceptions import ConfigurationConflict
from minty_amqp.consumer import BaseConsumer

COMMUNICATION_PREFIX = "zsnl.v2.zsnl_domains_communication."
DOCUMENT_PREFIX = "zsnl.v2.zsnl_domains_document.Document."
SUBJECT_RELATION_PREFIX = (
    "zsnl.v2.zsnl_domains_case_management.SubjectRelation."
)
TASK_PREFIX = "zsnl.v2.zsnl_domains_case_management.Task."
CASE_PREFIX = "zsnl.v2.zsnl_domains_case_management.Case."

API_HOSTNAME = os.environ.get("API_HOSTNAME", None)


class EmailNotificationConsumer(BaseConsumer):
    @classmethod
    def get_config(cls, config):
        # This class uses old-style configuration for now

        settings = []
        if "consumer_settings" in config:
            if isinstance(config["consumer_settings"], list):
                settings.extend(config["consumer_settings"])
            else:
                settings.append(config["consumer_settings"])

        for s in settings:
            if (
                s["consumer_class"]
                == "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer"
            ):
                return s

        raise ConfigurationConflict("No configuration found")

    def _register_routing(self):
        self.routing_keys += [
            CASE_PREFIX + "CaseAssigneeChanged",
            COMMUNICATION_PREFIX + "ContactMoment.ContactMomentCreated",
            COMMUNICATION_PREFIX + "ExternalMessage.ExternalMessageCreated",
            DOCUMENT_PREFIX + "DocumentAddedToCase",
            DOCUMENT_PREFIX + "DocumentCreated",
            DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
            DOCUMENT_PREFIX + "LabelsApplied",
            DOCUMENT_PREFIX + "LabelsRemoved",
            SUBJECT_RELATION_PREFIX + "SubjectRelationCreated",
            SUBJECT_RELATION_PREFIX + "SubjectRelationDeleted",
            SUBJECT_RELATION_PREFIX + "SubjectRelationUpdated",
            TASK_PREFIX + "TaskCompletionSet",
            TASK_PREFIX + "TaskCreated",
            TASK_PREFIX + "TaskDeleted",
            TASK_PREFIX + "TaskUpdated",
        ]

    def __call__(self, message: Message):
        """Handle message to create email notifications

        Log records are formatted to be consistent with the Perl system.

        :param BaseConsumer: base consumer
        :type BaseConsumer: BaseConsumer
        :param message: message from amqp broker
        :type message: Message
        """
        self.logger.debug("Start handing event/message")

        event = self._check_message(message=message)
        if not event:
            # _check_message handles AMQP discard
            return

        session = self._get_database_session(
            context=event["context"], message=message
        )
        if not session:
            # _get_database_session handles AMQP reject
            return

        event_data = self._format_event_data(event)

        case_uuid = None
        if event.get("entity_type") == "Case":
            # Case.CaseAssigneeChanged events - the entity_id is always the
            # case UUID
            case_uuid = event["entity_id"]
        elif event_data.get("case_uuid"):
            case_uuid = event_data["case_uuid"]
        elif case := event_data.get("case", None):
            # Some entities have case["uuid"], others have case["id"]
            case_uuid = case.get("uuid", case.get("id"))
        elif "thread_uuid" in event_data:
            case, _ = get_case_from_thread(
                session=session, thread_uuid=event_data["thread_uuid"]
            )
            case_uuid = case.uuid

        self._process_event(event, message, case_uuid)

    def _process_event(self, event, message, case_uuid):
        processed = False
        if case_uuid:
            self.logger.info(
                f"Processing event {event['event_name']} for case {case_uuid}"
            )

            processed = self._call_email_notification_handler(
                event=event,
                case_uuid=case_uuid,
                context=event["context"],
            )
        else:
            self.logger.info(
                f"No case uuid? Ignoring event {event['event_name']}"
            )

        if not processed:
            return self._discard_message(
                message,
                logline="Event not processed",
            )
        else:
            message.ack()
            self.cqrs.infrastructure_factory.flush_local_storage()
            self.logger.info("Handled message successfully.")

    def _call_email_notification_handler(
        self, event, case_uuid, context
    ) -> bool:
        platform_key = self.cqrs.infrastructure_factory.get_config(context)[
            "zs_platform_key"
        ]

        if API_HOSTNAME:
            url_base = f"http://{API_HOSTNAME}"
        else:
            url_base = f"https://{context}"

        url = url_base + f"/api/v1/case/{case_uuid}/process_event"

        headers = {
            "Content-Type": "application/json",
            "ZS-Platform-Key": platform_key,
            "Host": context,
        }

        self.logger.info(f"Calling {url}")

        try:
            r = requests.post(url=url, json=event, headers=headers, timeout=15)
        except requests.exceptions.RequestException as e:
            self.logger.error(f"Error while doing the request {e}")
            return False

        try:
            response = r.json()
            self.logger.debug(f"request_response: {response}")
        except ValueError:
            self.logger.error("No valid json response.")
            return False

        if r.status_code == requests.codes.ok:
            self.logger.debug("Response status code OK")
            return True
        else:
            self.logger.error("Response status code not OK")
            return False

    def _discard_message(self, message: Message, logline: str):
        """Discard message and remove from queue.

        :param message: amqp message
        :type message: Message
        :param logline: error message
        :type logline: str
        """
        self.logger.debug({"msg_body": message.body})
        self.logger.info(f"Message discarded: {logline}")

        message.ack()

        self.cqrs.infrastructure_factory.flush_local_storage()

    def _reject_and_retry_message(self, message: Message, logline: str):
        """Reject and retry message.

        :param message: amqp message
        :type message: Message
        :param logline: error message
        :type logline: str
        """
        self.logger.warning(logline)
        message.reject(requeue=False)
        self.cqrs.infrastructure_factory.flush_local_storage()

    def _check_message(self, message):
        """Check message for errors and discard if corrupted.

        :param message: message
        :type message: Message
        :return: event
        :rtype: dict
        """

        try:
            event = message.json()
        except JSONDecodeError:
            self._discard_message(
                message=message,
                logline="Corrupted message, not able to decode JSON.",
            )
            return

        try:
            event["context"]
            event["event_name"]
            event["user_uuid"]
        except KeyError as key:
            self._discard_message(
                message=message, logline=f"Incomplete event: missing {key}."
            )
            return

        return event

    def _get_database_session(self, context, message):
        """Get session for database

        :param context: context
        :type context: str
        :param message: message
        :type message: Message
        :return: session
        :rtype: Session
        """
        try:
            name = "database"
            session = self.cqrs.infrastructure_factory.get_infrastructure(
                context=context, infrastructure_name=name
            )
            return session
        except ConfigurationConflict as why:
            logline = (
                f"Configuration conflict for context: '{context}'"
                + f" and infrastructure_name: '{name}' conflict_error: {why}"
            )
            self._reject_and_retry_message(message=message, logline=logline)
            return

    def _format_event_data(self, event) -> dict:
        """Create formatted dict from event.changes & event.entity_data.

        :param event: event
        :type event: event
        :return: dict with entity_data
        :rtype: dict
        """
        event_data = dict(event["entity_data"])
        for change in event["changes"]:
            event_data[change["key"]] = change["new_value"]
        return event_data
