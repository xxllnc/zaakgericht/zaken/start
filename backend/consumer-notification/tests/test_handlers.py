# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty.cqrs.events
import minty.exceptions as exceptions
import pytest
import unittest
from datetime import datetime, timezone
from unittest import mock
from uuid import uuid4
from zsnl_consumer_notification import handlers


class TestDocumentAddedHandler(unittest.TestCase):
    def test_document_added_handler(self):
        now = datetime.now(tz=timezone.utc)

        case_uuid = uuid4()

        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={},
        )

        mock_cqrs = mock.Mock()
        handler = handlers.DocumentAddedHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.case_management")

        self.assertEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_document.Document.DocumentAddedToCase",
                "zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
                "zsnl.v2.zsnl_domains_document.Document.DocumentFromAttachmentCreated",
                "zsnl.v2.legacy.Document.CopiedToCase",
            ],
        )

        event = minty.cqrs.events.Event(
            uuid=uuid4(),
            correlation_id=uuid4(),
            created_date=now,
            context="no context",
            user_uuid=user_info.user_uuid,
            user_info=user_info,
            domain="zsnl_domains_document",
            entity_type="Document",
            entity_id=uuid4(),
            event_name="DocumentCreated",
            changes=[
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": str(case_uuid),
                }
            ],
            entity_data={},
        )

        handler.handle(event)

        mock_cqrs.get_command_instance().notify_case_assignee.assert_called_once_with(
            case_uuid=str(case_uuid),
            event_actor=user_info.user_uuid,
            notification_type="new_document",
        )

    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_document_added_handler_case_not_found(self):
        now = datetime.now(tz=timezone.utc)

        case_uuid = uuid4()

        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={},
        )

        mock_cqrs = mock.Mock()
        handler = handlers.DocumentAddedHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.case_management")

        self.assertEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_document.Document.DocumentAddedToCase",
                "zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
                "zsnl.v2.zsnl_domains_document.Document.DocumentFromAttachmentCreated",
                "zsnl.v2.legacy.Document.CopiedToCase",
            ],
        )

        event = minty.cqrs.events.Event(
            uuid=uuid4(),
            correlation_id=uuid4(),
            created_date=now,
            context="no context",
            user_uuid=user_info.user_uuid,
            user_info=user_info,
            domain="zsnl_domains_document",
            entity_type="Document",
            entity_id=uuid4(),
            event_name="DocumentCreated",
            changes=[
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": str(case_uuid),
                }
            ],
            entity_data={},
        )

        mock_cqrs.get_command_instance().notify_case_assignee.side_effect = [
            exceptions.NotFound
        ]

        with self._caplog.at_level(logging.INFO):
            handler.handle(event=event)

        mock_cqrs.get_command_instance().notify_case_assignee.assert_called_once_with(
            case_uuid=str(case_uuid),
            event_actor=user_info.user_uuid,
            notification_type="new_document",
        )

        assert "Case not found" in self._caplog.text

    def test_document_added_handler_no_case(self):
        now = datetime.now(tz=timezone.utc)

        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={},
        )

        mock_cqrs = mock.Mock()
        handler = handlers.DocumentAddedHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.case_management")

        self.assertEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_document.Document.DocumentAddedToCase",
                "zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
                "zsnl.v2.zsnl_domains_document.Document.DocumentFromAttachmentCreated",
                "zsnl.v2.legacy.Document.CopiedToCase",
            ],
        )

        event = minty.cqrs.events.Event(
            uuid=uuid4(),
            correlation_id=uuid4(),
            created_date=now,
            context="no context",
            user_uuid=user_info.user_uuid,
            user_info=user_info,
            domain="zsnl_domains_document",
            entity_type="Document",
            entity_id=uuid4(),
            event_name="DocumentCreated",
            changes=[
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": None,
                }
            ],
            entity_data={},
        )

        handler.handle(event)

        # If the event doesn't contain a case UUID, the command is not called
        mock_cqrs.get_command_instance().notify_case_assignee.assert_not_called()


class TestCaseAssignedHandler(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_case_assigned_handler(self):
        now = datetime.now(tz=timezone.utc)

        case_uuid = uuid4()

        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={},
        )

        mock_cqrs = mock.Mock()
        handler = handlers.CaseAssignedHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.case_management")

        self.assertEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_case_management.Case.CaseAssigneeChanged",
            ],
        )

        event = minty.cqrs.events.Event(
            uuid=uuid4(),
            correlation_id=uuid4(),
            created_date=now,
            context="no context",
            user_uuid=user_info.user_uuid,
            user_info=user_info,
            domain="zsnl_domains_case_management",
            entity_type="Case",
            entity_id=str(case_uuid),
            event_name="CaseAssigneeChanged",
            changes=[
                {
                    "key": "assignee",
                    "old_value": None,
                    "new_value": {"email": "email"},
                }
            ],
            entity_data={},
        )

        handler.handle(event)

        mock_cqrs.get_command_instance().notify_case_assignee.assert_called_once_with(
            case_uuid=str(case_uuid),
            event_actor=user_info.user_uuid,
            notification_type="send_case_assignee_email",
        )

    def test_case_assigned_handler_case_not_found(self):
        now = datetime.now(tz=timezone.utc)

        case_uuid = uuid4()

        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={},
        )

        mock_cqrs = mock.Mock()
        handler = handlers.CaseAssignedHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.case_management")

        self.assertEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_case_management.Case.CaseAssigneeChanged",
            ],
        )

        event = minty.cqrs.events.Event(
            uuid=uuid4(),
            correlation_id=uuid4(),
            created_date=now,
            context="no context",
            user_uuid=user_info.user_uuid,
            user_info=user_info,
            domain="zsnl_domains_case_management",
            entity_type="Case",
            entity_id=str(case_uuid),
            event_name="CaseAssigneeChanged",
            changes=[
                {
                    "key": "assignee",
                    "old_value": None,
                    "new_value": {"email": "email"},
                }
            ],
            entity_data={},
        )

        mock_cqrs.get_command_instance().notify_case_assignee.side_effect = [
            exceptions.NotFound
        ]

        with self._caplog.at_level(logging.INFO):
            handler.handle(event=event)

        mock_cqrs.get_command_instance().notify_case_assignee.assert_called_once_with(
            case_uuid=str(case_uuid),
            event_actor=user_info.user_uuid,
            notification_type="send_case_assignee_email",
        )
        assert "Case not found" in self._caplog.text
