# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.events import Event
from minty.exceptions import NotFound
from minty_amqp.consumer import BaseHandler


class NotificationBaseHandler(BaseHandler):
    """
    Base class for all event handlers in the notification consumer.

    Defines the "domain" property to use the "case_management" domain,
    for use by BaseHandler.
    """

    @property
    def domain(self):
        return "zsnl_domains.case_management"


class DocumentAddedHandler(NotificationBaseHandler):
    """
    Handler for changes to case custom fields: notify the case assignee.
    """

    @property
    def routing_keys(self) -> list[str]:
        return [
            "zsnl.v2.zsnl_domains_document.Document.DocumentAddedToCase",
            "zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
            "zsnl.v2.zsnl_domains_document.Document.DocumentFromAttachmentCreated",
            "zsnl.v2.legacy.Document.CopiedToCase",
        ]

    def handle(self, event: Event) -> None:
        changes = event.format_changes()
        event_actor = event.user_info.user_uuid if event.user_info else None
        case_uuid = changes.get("case_uuid")

        if not case_uuid:
            self.logger.debug(
                "Document created in intake = no case, so no assignee to notify"
            )
            return

        command_instance = self.get_command_instance(event)

        try:
            command_instance.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor,
                notification_type="new_document",
            )
        except NotFound as e:
            self.logger.info(f"Case not found: {e}")

        return


class CaseAssignedHandler(NotificationBaseHandler):
    """
    Handler for changing case assignee: notify the new case assignee.
    """

    @property
    def routing_keys(self) -> list[str]:
        return [
            "zsnl.v2.zsnl_domains_case_management.Case.CaseAssigneeChanged",
        ]

    def handle(self, event: Event) -> None:
        event_actor = event.user_info.user_uuid if event.user_info else None
        case_uuid = event.entity_id

        command_instance = self.get_command_instance(event)

        try:
            command_instance.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor,
                notification_type="send_case_assignee_email",
            )
        except NotFound as e:
            self.logger.info(f"Case not found: {e}")

        return
