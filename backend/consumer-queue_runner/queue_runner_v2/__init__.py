# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.0.1"

import logging
from .utils import CustomJsonFormatter
from statsd import StatsClient

logger = logging.getLogger()
logHandler = logging.StreamHandler()
formatter = CustomJsonFormatter(
    "%(timestamp) %(level) %(zs_component) %(message)"
)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)


class StatsDMock:
    """Mocks statsd so things don't break when initialization fails."""

    def incr(self, message: str):
        logger.info(f"statsd not initialized - incr call: {message}")

    def timer(self, message: str):
        logger.warning(f"statsd not initialized - timer call: {message}")

        class Timer:
            def start(self):
                pass

            def stop(self):
                pass

        return Timer()


STATSD = StatsDMock()


def init_statsd(host, port):
    """Initialize statsd client using the `host` and `port` params.

    :param host: statsd host address
    :type host: str
    :param port: statsd port
    :type port: int
    """
    global STATSD
    STATSD = StatsClient(host, port, prefix="zaaksysteem.queue.statsd")


def get_statsd_client():
    """Return statsd or statsdmock client, depending on initialization.

    :return: statsd global var
    :rtype: StatsClient or StatsDMock
    """
    global STATSD
    return STATSD
