#! /usr/local/bin/python3 -u

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import sys
from . import init_statsd, queue_consumer
from .handlers.config import handler_config
from .utils import parse_arguments


def main(arguments=None):
    """Initialize QueueConsumer, Statsd & logging - start consuming.

    :param arguments: configuration argmuments to parse, defaults to None
    :param arguments: None
    """
    args = parse_arguments(arguments)
    # Initialize logger & statsd
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG) if args.debug else logger.setLevel(
        logging.INFO
    )

    logger.info(
        "arguments received: debug: {} heartbeats: {} number_of_threasds: {}".format(
            args.debug, args.heartbeats, args.number_of_threads
        )
    )

    try:
        init_statsd(host=args.statsd_host, port=args.statsd_port)
    except OSError as e:
        logger.info("StatsD logging disabled: " + e.strerror)

    # initialize Handlers and QueueConsumer
    handlers = handler_config()
    consumer = queue_consumer.queue_consumer_factory(
        url=args.rabbitmq_url,
        handlers=handlers,
        number_of_threads=int(args.number_of_threads),
        heartbeats=int(args.heartbeats),
    )
    # start consuming messages
    consumer.start_consuming()


def init():
    """Start `main` function, with arguments from command line."""
    if __name__ == "__main__":
        sys.exit(main(arguments=sys.argv[1:]))


init()
