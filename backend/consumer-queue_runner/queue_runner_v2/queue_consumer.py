# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import functools
import json
import logging
import pika
import threading
from . import get_statsd_client


class QueueConsumer:
    """RabbitMQ consumer - dispatch messages to their associated handlers.

    To keep the connection alive between RabbitMQ and consumer, messages are
    handled in a seperate thread. Handler(s) and with which queue(s), exchange(s)
    and routing key(s) they are associated, are initialized by calling the
    `self.connect_to_queue` method.
    """

    def __init__(self, connection: pika.BlockingConnection):
        """Initialize the QueueConsumer class.

        :param connection: connection to RabbitMQ
        :type connection: pika.BlockingConnection
        """
        self._thread_store = []
        self._connection = connection
        self._channel = None
        self._logger = logging.getLogger()
        self._statsd = get_statsd_client()

    def start_consuming(self):
        """Start consuming on the `self._channel` channel."""
        self._channel.start_consuming()

    def create_channel(self, prefetch_count):
        """Set up channel and set it in the `self._channel` param.

        :param prefetch_count: number of messages to handle (equals number of
            threads handling messages)
        :type prefetch_count: int
        """
        channel = self._connection.channel()
        channel.basic_qos(prefetch_count=prefetch_count)
        self._channel = channel

    def connect_to_queue(self, queue, exchange, routing_key, handler_class):
        """Declare queue and bind handler to queueu, exchange and routing key.

        :param queue: queue to subscribe to
        :type queue: str
        :param exchange: exchange to subscribe to
        :type exchange: str
        :param routing_key: routing key to subscribe to
        :type routing_key: str
        :param handler_class: class to handle the message
        :type handler_class: object
        """
        ch = self._channel
        queue_args = {
            "x-queue-type": "quorum"
        }  # default create a quorum queue
        ch.queue_declare(
            queue=queue, auto_delete=False, durable=True, arguments=queue_args
        )
        ch.queue_bind(queue=queue, exchange=exchange, routing_key=routing_key)

        message_callback = functools.partial(
            self.on_message, args=(handler_class)
        )
        ch.basic_consume(
            on_message_callback=message_callback, queue=queue, auto_ack=False
        )
        self._logger.info(
            f"Connected to queue: '{queue}' routing_key: '{routing_key}' exchange: '{exchange}'"
        )

    def parse_json__message(self, body: str):
        """Parse message from rabbitMQ into dict.

        :param body: RabbitMQ message
        :type body: str
        :raises Exception: if message can't be parsed raise Exception
        :return: json decoded message
        :rtype: dict
        """
        try:
            message = json.loads(body)
            return message
        except Exception as e:
            self._logger.warning(f"Could not parse message: {e}")
            raise

    def execute_handler(
        self,
        channel: object,
        delivery_tag: str,
        body: object,
        handler_class: object,
        routing_key: str,
    ):
        """Call execute method on the handler class.

        Will call `self.ack_message` if the handler.execute method is called
        succesfull. if an exception is raised in the handler.execute method
        `self.nack_message` will be called.

        :param channel: channel param
        :type channel: object
        :param delivery_tag: identifies the message to and from RabbitMQ
        :type delivery_tag: str
        :param body: message received from rabbitMQ
        :type body: object
        :param handler_class: class to handle the message
        :type handler_class: object
        :param routing_key: Identifies type of message - used for logging.
        :type routing_key: str
        """
        try:
            message = self.parse_json__message(body)
            handler_class.execute(message=message, routing_key=routing_key)
            self.ack_message(channel, delivery_tag)
        except Exception:
            self.nack_message(channel, delivery_tag)

    def on_message(
        self,
        channel: object,
        method_frame: object,
        header_frame: object,
        body: str,
        args: dict,
    ):
        """On receiving a new message, spin up a new thread to do the work.

        Calls `self.clean_thread_store()` for cleanup, after thread is started

        :param channel: channel object
        :type channel: object
        :param method_frame: method_frame received from RabbitMQ
        :type method_frame: object
        :param header_frame: header_frame received from RabbitMQ
        :type header_frame: object
        :param body: message body received from RabbitMQ
        :type body: str
        :param args: args, holds the handler class
        :type args: dict
        """
        (handler_class) = args
        delivery_tag = method_frame.delivery_tag
        routing_key = method_frame.routing_key

        try:
            _, _, instance_ref, event_type = routing_key.split(".")
        except ValueError:
            self._logger.info(
                f"Received message with invalid routing key '{routing_key}', dropping message."
            )

            return self.nack_message(channel, delivery_tag)

        self._logger.info(
            f"Received message, routing key: '{routing_key}'",
            extra={"instance_ref": instance_ref, "event_type": event_type},
        )

        self._statsd.incr(
            f"zs.v0.instance.{instance_ref}.{event_type}"
        )  # new metric name: amqp_read_number

        t = threading.Thread(
            target=self.execute_handler,
            args=(channel, delivery_tag, body, handler_class, routing_key),
        )
        t.start()
        self._thread_store.append(t)
        self.clean_thread_store()

    def clean_thread_store(self):
        """Remove completed threads from the thread store."""
        self._thread_store = [t for t in self._thread_store if t.is_alive()]

    def nack_message(self, channel: object, delivery_tag: str):
        """Signal to RabbitMQ that work on message was not done succesfully.

        :param channel: channel instance
        :type channel: object
        :param delivery_tag: identifies the message to and from RabbitMQ
        :type delivery_tag: str
        """

        if channel.is_open:
            callback = functools.partial(
                channel.basic_reject, delivery_tag, False
            )
            self._connection.add_callback_threadsafe(callback)
        else:
            self._logger.warning(
                f"Channel is closed. failed to NACK message with delivery tag '{delivery_tag}'"
            )

    def ack_message(self, channel: object, delivery_tag: str):
        """Signal to RabbitMQ that work on message was done succesfully.

        :param channel: channel instance
        :type channel: object
        :param delivery_tag: identifies the message to and from RabbitMQ
        :type delivery_tag: str
        """
        if channel.is_open:
            callback = functools.partial(
                channel.basic_ack, delivery_tag, False
            )
            self._connection.add_callback_threadsafe(callback)
        else:
            self._logger.warning(
                f"Channel is closed. failed to ACK message with delivery tag: '{delivery_tag}'"
            )


def queue_consumer_factory(
    url: str,
    handlers: object,
    number_of_threads: int,
    heartbeats: int,
    connection_module=pika,
):
    """Initialize the QueueConsumer object with the specified params.

    :param url: name of rabbitmq url to connect to
    :type url: str
    :param config: config to bind handler to which queue, exchange & routing key
    :type config: object
    :param number_of_threads: total number of threads handling messages
    :type number_of_threads: int
    :param heartbeats: interval in seconds in which heartbeats are sent from
        the rabbitmq server. 2 missed heartbeats & the connection will be closed
    :type heartbeats: int
    :param connection_module: module to connect to the rabbitmq, defaults to pika
    :param connection_module: pika module, optional
    :return: Initialized QueConsumer and handlers bound to queues, exchanges &
        routing keys
    :rtype: QueueConsumer
    """
    url_with_params = connection_module.URLParameters(
        f"{url}?heartbeat={heartbeats}"
    )
    queue_consumer = QueueConsumer(
        connection=connection_module.BlockingConnection(url_with_params)
    )
    queue_consumer.create_channel(prefetch_count=number_of_threads)
    for h in handlers:
        queue_consumer.connect_to_queue(
            queue=h["queue"],
            exchange=h["exchange"],
            routing_key=h["routing_key"],
            handler_class=h["handler_class"],
        )
    return queue_consumer
