# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import os
import pytest
from queue_runner_v2.handlers import api_request
from requests.exceptions import HTTPError
from unittest import mock


class MyError(Exception):
    pass


class RequestsMock:
    """request class to mock the requests lib and mock api calls."""

    def __init__(
        self, request_timeout=None, response_mock=None, requests_exception=None
    ):
        if response_mock is None:
            self.response_mock = ResponseMock(
                text="request successful", status_code=200, headers="request"
            )
        else:
            self.response_mock = response_mock

        if requests_exception is None:
            self.requests_exception = False
        else:
            self.requests_exception = requests_exception

        self._request_timeout = request_timeout

    def post(self, url: str, json, verify: bool, timeout, headers=None):
        if self.requests_exception:
            raise self.requests_exception()
        else:
            return self.response_mock


class ResponseMock:
    __slots__ = [
        "headers",
        "json_response",
        "reason",
        "status_code",
        "text",
        "url",
    ]

    def __init__(self, text, status_code, headers, json_response=None, url=""):
        self.reason = "reason why request failed"
        self.text = text
        self.status_code = status_code
        self.headers = {"zs-req-id": headers}
        self.url = url
        if json_response is None:
            self.json_response = None
        else:
            self.json_response = json_response

    def json(self):
        if self.json_response:
            return self.json_response
        else:
            raise ValueError


class TestApiHandler:
    def test_init_values(self):
        cert = "/tls_certifate.crt"
        handler = api_request.HandlerApi(
            request_module=RequestsMock(),
            request_timeout=437,
            certificate=cert,
        )
        assert handler._certificate is cert
        assert handler._timeout == 437

    def test_empty_url(self):
        """Check no url in message raises a KeyError."""
        msg = {"body": "body msg"}
        with pytest.raises(KeyError):
            api_request.HandlerApi(request_timeout=60).execute(
                msg, routing_key="test"
            )

    def test_successful_request(self, caplog):
        """Test successful Api call from beginning till end."""
        caplog.set_level(logging.DEBUG)
        handler = api_request.HandlerApi(
            request_module=RequestsMock(
                response_mock=ResponseMock(
                    text="request successful",
                    status_code=200,
                    headers="request",
                    json_response={
                        "result": [
                            {"type": "msg_type", "messages": ["msg1", "msg2"]}
                        ]
                    },
                )
            ),
            request_timeout=600,
        )
        msg = {
            "instance_hostname": "foobar.com",
            "url": "https://foobar.com/test",
            "msg": "msg as expected",
        }

        handler.execute(msg, routing_key="test")
        assert (
            "Dispatching message to 'https://foobar.com/test'"
            in caplog.records[0].message
        )
        assert "Queue item handled successfully." in caplog.records[1].message

    @mock.patch.dict(os.environ, {"API_HOSTNAME": "test-api-host"}, clear=True)
    def test_successful_request_with_api_hostname_set(self, caplog):
        """Test successful Api call from beginning till end."""
        caplog.set_level(logging.DEBUG)

        handler = api_request.HandlerApi(
            request_module=RequestsMock(
                response_mock=ResponseMock(
                    text="request successful",
                    status_code=200,
                    headers="request",
                    json_response={
                        "result": [
                            {"type": "msg_type", "messages": ["msg1", "msg2"]}
                        ]
                    },
                )
            ),
            request_timeout=600,
        )
        msg = {
            "instance_hostname": "foobar.com",
            "url": "https://foobar.com/test",
            "msg": "msg as expected",
        }

        handler.execute(msg, routing_key="test")
        assert (
            "Dispatching message to 'https://foobar.com/test'"
            in caplog.records[0].message
        )
        assert "Queue item handled successfully." in caplog.records[1].message

    def test_successful_request_corrupt_item(self, caplog):
        caplog.set_level(logging.DEBUG)
        handler = api_request.HandlerApi(
            request_module=RequestsMock(
                response_mock=ResponseMock(
                    text="request successful",
                    status_code=200,
                    headers="request",
                )
            ),
            request_timeout=600,
        )
        msg = {
            "instance_hostname": "extra",
            "url": "https://extra/special_url",
            "msg": "msg as expected",
        }
        handler.execute(msg, routing_key="test")
        assert "https://extra/special_url" in caplog.records[0].message
        assert "Corrupt response body" in caplog.records[2].message

    def test_failed_request(self, caplog):
        handler = api_request.HandlerApi(
            request_timeout=600,
            request_module=RequestsMock(requests_exception=MyError),
        )
        msg = {
            "url": "https://localhost",
            "msg": "msg as expected",
            "instance_hostname": "foobar.com",
        }
        with pytest.raises(MyError):
            handler.execute(msg, routing_key="test")

        assert "Error during http(s) request" in caplog.records[0].message

    def test_http_error_code_failed_parsing_json(self, caplog):
        json_response = None
        response = ResponseMock(
            text="TEXTLOGGING TEST",
            status_code=400,
            headers="request_id",
            json_response=json_response,
        )
        handler = api_request.HandlerApi(
            request_timeout=600,
            request_module=RequestsMock(response_mock=response),
        )
        msg = {
            "url": "https://localhost",
            "msg": "msg as expected",
            "instance_hostname": "localhost",
        }

        with pytest.raises(HTTPError):
            handler.execute(msg, routing_key="test")

        assert response.text in caplog.records[0].message

    def test_http_error_code_with_good_json(self, caplog):
        json_response = {
            "result": [{"type": "msg_type", "messages": ["msg1", "msg2"]}]
        }
        headers = "request_id"
        response = ResponseMock(
            text="TEXTLOGGING TEST",
            status_code=400,
            headers=headers,
            json_response=json_response,
        )
        handler = api_request.HandlerApi(
            request_timeout=600,
            request_module=RequestsMock(response_mock=response),
        )
        msg = {
            "url": "test.localhost",
            "msg": "msg as expected",
            "instance_hostname": "foobar.com",
        }

        with pytest.raises(HTTPError):
            handler.execute(msg, routing_key="test")

        status_code = "400"

        assert status_code in caplog.records[0].message
        for msg in json_response["result"][0]["messages"]:
            assert msg in caplog.records[0].message

    def test_request_timeout(self, caplog):
        import requests

        handler = api_request.HandlerApi(
            request_timeout=60,
            request_module=RequestsMock(
                requests_exception=requests.exceptions.ConnectTimeout
            ),
        )
        msg = {
            "url": "test.localhost",
            "msg": "msg as expected",
            "instance_hostname": "foobar.com",
        }

        with pytest.raises(requests.exceptions.ConnectTimeout):
            handler.execute(msg, routing_key="test")

        assert (
            "HTTP timeout after exceeding the time limit:"
            in caplog.records[0].message
        )

    def test_successful_response_wrong_json_format(self, caplog):
        """Test successful Api call but returning json in the wrong format."""
        caplog.set_level(logging.DEBUG)
        # init handler
        handler = api_request.HandlerApi(
            request_module=RequestsMock(), request_timeout=600
        )
        # response
        response_mock = ResponseMock(
            text="request successful",
            status_code=200,
            headers="request",
            json_response={
                "no_results": [
                    {"type": "msg_type", "messages": ["msg1", "msg2"]}
                ]
            },
            url="extra_special_url",
        )
        handler._successful_response(
            response=response_mock,
            request_id="request_id",
            routing_key="routing.key",
        )
        assert "Queue item handled successfully." in caplog.records[0].message
        assert "Corrupt response body:" in caplog.records[1].message

    def test_unsuccessful_response_wrong_json_format(self, caplog):
        """Test successful Api call but returning json in the wrong format."""
        caplog.set_level(logging.DEBUG)
        # init handler
        handler = api_request.HandlerApi(
            request_module=RequestsMock(), request_timeout=600
        )
        # response
        response_mock = ResponseMock(
            text="showsup_in_log?",
            status_code=400,
            headers="request",
            json_response={
                "no_results": [
                    {"type": "msg_type", "messages": ["msg1", "msg2"]}
                ]
            },
            url="extra_special_url",
        )
        handler._unsuccessful_response(
            response=response_mock, request_id="request_id"
        )
        assert "Error while running action" in caplog.records[0].message
        assert "showsup_in_log?" in caplog.records[0].message
