# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from queue_runner_v2.handlers.config import handler_config


class TestConfig:
    def test_config(self):
        handlers = handler_config()
        for h in handlers:
            assert h["queue"]
            assert h["exchange"]
            assert h["exchange_type"]
            assert h["routing_key"]
            assert hasattr(h["handler_class"], "execute")
