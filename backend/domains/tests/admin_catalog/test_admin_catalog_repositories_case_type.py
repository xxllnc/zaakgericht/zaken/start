# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.cqrs.events import Event, EventService
from minty.exceptions import Conflict, NotFound
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.repositories.case_type import (
    CaseTypeRepository,
)


class TestCaseTypeRepository:
    def setup_method(self):
        self.mock_infra = mock.MagicMock()
        self.mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        self.case_type_repo = CaseTypeRepository(
            infrastructure_factory=self.mock_infra,
            infrastructure_factory_ro=self.mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )

    def test_get_session(self):
        assert (
            self.case_type_repo.session is self.mock_infra.get_infrastructure()
        )

    def test_get_case_type_no_result(self):
        mock_ses = self.case_type_repo.session

        mock_ses.query().join().filter().filter().filter().one.side_effect = (
            NoResultFound
        )
        uuid = str(uuid4())
        with pytest.raises(NotFound) as excinfo:
            self.case_type_repo.get_case_type(uuid=uuid)
        assert excinfo.value.args == (
            f"Case type with uuid: '{uuid}' not found.",
            "case_type/not_found",
        )

    def test_get_case(self):
        uuid = str(uuid4())
        mock_ses = self.case_type_repo.session
        mock_result = [mock.MagicMock()] * 8

        mock_result[0].configure_mock(
            id=1,
            uuid=uuid,
            name="test_name",
            active=True,
            identification=123,
            current_version=10,
            last_modified="2019-12-21 23:50:43",
            current_version_uuid=uuid4(),
            current_node_initiator_source="internextern",
        )
        mock_result[1] = mock.MagicMock()
        mock_result[4] = "internextern"
        mock_ses.query().join().filter().filter().filter().one.return_value = (
            mock_result
        )
        self.case_type_repo.get_case_type(uuid=uuid)

    def test_save(self):
        uuid = str(uuid4())
        self.case_type_repo.cache[uuid] = mock.MagicMock()
        event_service = mock.MagicMock()
        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="CaseTypeEntity",
            entity_id=uuid,
            event_name="CaseTypeOnlineStatusChanged",
            changes=[{"key": "active", "old_value": False, "new_value": True}],
            entity_data={},
        )
        event_service.event_list = [event]
        self.case_type_repo.event_service = event_service
        self.case_type_repo.save()

        old_version_uuid = uuid4()
        new_version_uuid = uuid4()
        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="CaseTypeEntity",
            entity_id=uuid,
            event_name="CaseTypeVersionUpdated",
            changes=[
                {
                    "key": "current_version_uuid",
                    "old_value": old_version_uuid,
                    "new_value": new_version_uuid,
                }
            ],
            entity_data={},
        )
        event_service.event_list = [event]
        self.case_type_repo.event_service = event_service
        self.case_type_repo.save()

        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="CaseTypeEntity",
            entity_id=uuid,
            event_name="UnkownEvent",
            changes=[{"key": "active", "old_value": False, "new_value": True}],
            entity_data={},
        )
        event_service.event_list = [event]
        self.case_type_repo.event_service = event_service
        with pytest.raises(NotImplementedError) as excinfo:
            self.case_type_repo.save()
        assert excinfo.value.args == (
            f"Event {event.event_name} for CaseType is unknown and cannot be saved",
        )

        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="CaseTypeVersion",
            entity_id=uuid,
            event_name="UnkownEvent",
            changes=[{"key": "active", "old_value": False, "new_value": True}],
            entity_data={},
        )
        event_service.event_list = [event]
        self.case_type_repo.event_service = event_service
        self.case_type_repo.save()

    def test_get_related_case_types(self):
        mock_ses = self.case_type_repo.session
        mock_entity = mock.MagicMock()
        mock_ses.query().filter().filter().one.return_value = mock_entity

        uuid = str(uuid4())

        mock_sqla_related_case_types = mock.MagicMock()
        mock_sqla_related_case_types.uuid = uuid
        mock_sqla_related_case_types.name = "test"
        mock_sqla_related_case_types.active = True
        mock_sqla_related_case_types.version = 5
        mock_sqla_related_case_types.is_current_version = True

        mock_ses.execute().fetchall.return_value = [
            mock_sqla_related_case_types
        ]

        result = self.case_type_repo.get_related_case_types(case_type_id=1200)

        assert result[0].uuid == uuid
        assert result[0].name == "test"
        assert result[0].active is True
        assert result[0].version == 5
        assert result[0].is_current_version is True

    def test_delete_case_type(self):
        uuid = str(uuid4())
        reason = "no more used"

        res = self.case_type_repo.delete_case_type(uuid=uuid, reason=reason)

        assert res.uuid == uuid
        assert res.reason == reason
        assert isinstance(res.deleted, datetime)

    def test__get_case_types_related_to_case_type(self):
        mock_ses = self.case_type_repo.session
        mock_ses.execute().fetchone.return_value = namedtuple(
            "RowProxy", "cnt"
        )(cnt=10)

        uuid = str(uuid4())

        res = self.case_type_repo._get_case_types_related_to_case_type(
            uuid=uuid
        )

        assert res == 10

    def test__get_cases_related_to_case_type(self):
        mock_ses = self.case_type_repo.session
        mock_ses.execute().fetchone.return_value = namedtuple(
            "RowProxy", "cnt"
        )(cnt=3)

        uuid = str(uuid4())

        res = self.case_type_repo._get_cases_related_to_case_type(uuid=uuid)

        assert res == 3

    @mock.patch.object(
        CaseTypeRepository, "_get_case_types_related_to_case_type"
    )
    @mock.patch.object(CaseTypeRepository, "_get_cases_related_to_case_type")
    def test__delete_case_type(self, get_cases_mock, get_case_type_mock):
        uuid = uuid4()
        event_service = mock.MagicMock()
        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="CaseTypeEntity",
            entity_id=uuid,
            event_name="CaseTypeDeleted",
            changes=[
                {
                    "key": "deleted",
                    "old_value": None,
                    "new_value": "2019-02-23",
                },
                {
                    "key": "reason",
                    "old_value": None,
                    "new_value": "no more used",
                },
            ],
            entity_data={},
        )
        event_service.event_list = [event]
        get_case_type_mock.return_value = 0
        get_cases_mock.return_value = 0

        self.case_type_repo.event_service = event_service

        self.case_type_repo.save()

        with pytest.raises(Conflict) as excinfo:
            get_case_type_mock.return_value = 3
            self.case_type_repo.save()
        assert excinfo.value.args == (
            "Case type is used in case types",
            "case_type/used_in_case_types",
        )

        with pytest.raises(Conflict) as excinfo:
            get_case_type_mock.return_value = 0
            get_cases_mock.return_value = 2
            self.case_type_repo.save()
        assert excinfo.value.args == (
            "Case type is used in cases",
            "case_type/used_in_cases",
        )
