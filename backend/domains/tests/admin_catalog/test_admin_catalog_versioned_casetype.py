# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pydantic.v1
import pytest
from decimal import Decimal
from minty.cqrs.events import EventService
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.admin import catalog
from zsnl_domains.admin.catalog import entities
from zsnl_domains.admin.catalog.entities.versioned_casetype import (
    EmployeeRelation,
    SubjectType,
)
from zsnl_domains.admin.catalog.repositories.versioned_casetype_commands import (
    util,
)


@pytest.fixture
def versioned_casetype():
    versioned_casetype = mock.Mock()
    versioned_casetype.configure_mock(
        uuid=uuid4(),
        casetype_uuid=uuid4(),
        catalog_folder={"uuid": uuid4(), "name": "name of folder"},
        active=True,
        general_attributes={
            "name": "MINTY-11111",
            "identification": "identificatie 11111",
            "tags": "trefwoorden 11111",
            "description": "omschrijving of toelichting 11111",
            "case_summary": "extra informatie 11111",
            "case_public_summary": "extra informatie extern 11111",
            "legal_period": {"value": "2023-09-06", "type": "einddatum"},
            "service_period": {"value": 11113, "type": "werkdagen"},
        },
        documentation={
            "process_description": "processbeschrijving 11111",
            "initiator_type": "aangaan",
            "motivation": "aanleiding 11111",
            "purpose": "doel 11111",
            "archive_classification_code": "archiefclassificatiecode 11111",
            "designation_of_confidentiality": "Openbaar",
            "responsible_subject": "verantwoordelijke 11111",
            "responsible_relationship": "verantwoordingsrelatie 11111",
            "possibility_for_objection_and_appeal": False,
            "publication": False,
            "publication_text": "publicatietekst 11111",
            "bag": False,
            "lex_silencio_positivo": False,
            "may_postpone": False,
            "may_extend": False,
            "extension_period": None,
            "adjourn_period": 11111,
            "penalty_law": False,
            "wkpb_applies": False,
            "e_webform": "eformulier 11111",
            "legal_basis": "wettelijke grondslag 11111",
            "local_basis": "lokale grondslag 11111",
            "gdpr": {
                "enabled": False,
                "kind": {
                    "basic_details": True,
                    "personal_id_number": True,
                    "income": True,
                    "race_or_ethniticy": True,
                    "political_views": True,
                    "religion": True,
                    "membership_union": True,
                    "genetic_or_biometric_data": True,
                    "health": False,
                    "sexual_identity": True,
                    "criminal_record": True,
                    "offspring": True,
                },
                "source": {
                    "public_source": False,
                    "registration": True,
                    "partner": False,
                    "sender": False,
                },
                "processing_type": "Delen",
                "process_foreign_country": False,
                "process_foreign_country_reason": "toelichting doorgifte buitenland 11111",
                "processing_legal": "Wettelijke verplichting",
                "processing_legal_reason": "toelichting verwerking 11111",
            },
        },
        relations={
            "trigger": "internextern",
            "allowed_requestor_types": [
                "natuurlijk_persoon_na",
                "natuurlijk_persoon",
                "medewerker",
                "preset_client",
                "niet_natuurlijk_persoon",
            ],
            "preset_requestor": {
                "uuid": "3b4730a2-e50b-4d15-893e-525dd27f1c2c",
                "name": "T. Test achternaam",
                "type": "person",
            },
            "api_preset_assignee": {
                "uuid": "27c02737-024c-48d0-81ab-53c3dc77f59b",
                "name": "Rudie Schonenberg",
                "type": "employee",
            },
            "address_requestor_use_as_correspondence": True,
            "address_requestor_use_as_case_address": True,
            "address_requestor_show_on_map": True,
        },
        webform={
            "public_confirmation_title": "Uw zaak is geregistreerd",
            "public_confirmation_message": "Bedankt voor het [[case.casetype.initiator_type]] van een <strong>[[case.casetype.name]]</strong>. Uw registratie is bij ons bekend onder <strong>zaaknummer [[case.number]]</strong>. Wij verzoeken u om bij verdere communicatie dit zaaknummer te gebruiken. De behandeling van deze zaak zal spoedig plaatsvinden.",
            "case_location_message": "tekst bij adrescontrole-11111",
            "pip_view_message": "Ook kunt u op elk moment van de dag de voortgang en inhoud inzien via de persoonlijke internetpagina.",
            "actions": {
                "enable_webform": True,
                "create_delayed": True,
                "address_check": True,
                "reuse_casedata": True,
                "enable_online_payment": True,
                "enable_manual_payment": True,
                "email_required": True,
                "phone_required": True,
                "mobile_required": True,
                "disable_captcha": True,
                "generate_pdf_end_webform": True,
            },
            "price": {
                "web": 23,
                "frontdesk": 24.01,
                "phone": 25.99,
                "email": 26.00,
                "assignee": 27,
                "post": 28,
                "chat": 1,
                "other": 15.9,
            },
        },
        registrationform={
            "allow_assigning_to_self": True,
            "allow_assigning": True,
            "show_confidentionality": True,
            "show_contact_details": True,
            "allow_add_relations": True,
        },
        case_dossier={
            "disable_pip_for_requestor": True,
            "lock_registration_phase": True,
            "queue_coworker_changes": True,
            "allow_external_task_assignment": True,
            "default_document_folders": [
                "documentmap-11111",
                "documentmap 2 default",
            ],
        },
        api={
            "api_can_transition": True,
            "is_public": True,
            "notifications": {
                "external_notify_on_new_case": True,
                "external_notify_on_new_document": True,
                "external_notify_on_new_message": True,
                "external_notify_on_exceed_term": True,
                "external_notify_on_allocate_case": True,
                "external_notify_on_phase_transition": True,
                "external_notify_on_task_change": True,
                "external_notify_on_label_change": True,
                "external_notify_on_subject_change": True,
            },
        },
        authorization=[
            {
                "role_uuid": "79a38581-9648-469f-8ea3-d34a99f9e158",
                "department_uuid": "e7a6b730-b98a-4757-bbfd-07c2edda5180",
                "confidential": False,
                "rights": [
                    "zaak_beheer",
                    "zaak_edit",
                    "zaak_read",
                    "zaak_search",
                ],
            },
            {
                "role_uuid": "e63eea98-1862-401d-bdfa-643f91102688",
                "department_uuid": "923996fc-f8f8-4745-9886-a8b00bce1bbe",
                "confidential": False,
                "rights": ["zaak_search"],
            },
            {
                "role_uuid": "a95e4082-67ab-4969-be66-63c5b27bfb79",
                "department_uuid": "923996fc-f8f8-4745-9886-a8b00bce1bbe",
                "confidential": True,
                "rights": ["zaak_read", "zaak_search"],
            },
            {
                "role_uuid": "a95e4082-67ab-4969-be66-63c5b27bfb79",
                "department_uuid": "923996fc-f8f8-4745-9886-a8b00bce1bbe",
                "confidential": True,
                "rights": ["zaak_read", "zaak_search", "zaak_edit"],
            },
        ],
        child_casetype_settings={
            "enabled": True,
            "child_casetypes": [
                {
                    "enabled": "true",
                    "casetype": {
                        "name": "Name of casetype with id 1",
                        "uuid": "2c40ea6c-a3d3-4685-bfc9-cba7d1eab7a1",
                    },
                    "settings": {
                        "api": "false",
                        "case": "false",
                        "webform": "false",
                        "registrationform": "false",
                        "results": "true",
                        "relations": "false",
                        "last_phase": "false",
                        "allocations": "false",
                        "first_phase": "false",
                        "middle_phases": "[2]",
                        "notifications": "false",
                        "authorizations": "false",
                        "middle_phases2": "false",
                    },
                },
                {
                    "enabled": "true",
                    "casetype": {
                        "name": "Name of casetype with id 3",
                        "uuid": "a486807c-0e76-4145-807c-c794501f622b",
                    },
                    "settings": {
                        "api": "false",
                        "case": "false",
                        "webform": "false",
                        "registrationform": "false",
                        "results": "false",
                        "relations": "true",
                        "last_phase": "false",
                        "allocations": "false",
                        "first_phase": "false",
                        "middle_phases": "[2]",
                        "notifications": "false",
                        "authorizations": "false",
                        "middle_phases2": "false",
                    },
                },
            ],
        },
        change_log={
            "update_components": [
                "basisattributen",
                "zaakacties",
                "fasering en toewijzing",
                "kenmerken",
                "kinderen",
            ],
            "update_description": "Wijziging die zijn doorgevoerd",
        },
        phases=[
            {
                "phase_id": 1,
                "milestone_name": "Geregistreerd",
                "phase_name": "Registreren",
                "created": "2024-01-22T11:52:41.444146",
                "last_modified": "2024-01-22T11:52:41.444146",
                "term_in_days": 0,
                "assignment": {
                    "enabled": False,
                    "department_uuid": "432555e5-2a37-4380-8571-5f51df8c07ab",
                    "department_name": "Development",
                    "role_uuid": "8b07fe5d-7685-4a58-b851-3d9692f90c62",
                    "role_name": "Behandelaar",
                },
                "tasks": None,
                "templates": [
                    {
                        "catalog_template_uuid": "3cc1a024-5f3b-4ae6-96c0-2c5a1b83ad4c",
                        "catalog_template_name": "default_document_sjabloon",
                        "label": "label fase 1 odt",
                        "description": "toelichting fase 1 odt",
                        "auto_generate": True,
                        "allow_edit": True,
                        "custom_filename": "bestandsnaam.odt",
                        "target_format": "odt",
                        "document_attribute_uuid": "45b84d9f-5d8c-453c-b250-3a350ef67da3",
                        "document_attribute_name": "kenmerk_document",
                    }
                ],
                "custom_fields": [
                    {
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "Benodigde gegevens",
                        "help_intern": "Vul de benodigde velden in voor uw zaak",
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": None,
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "group",
                        "permissions": None,
                    },
                    {
                        "uuid": "800b63f6-aa9a-4df7-9968-2cc036923dd8",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "kenmerk tekstfield title",
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_tekstfield",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "text",
                        "permissions": None,
                    },
                    {
                        "uuid": "40a72131-193d-4ace-804a-9b8de9c4c868",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "kenmerk datum title",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_datum",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": {
                            "value": "2",
                            "active": False,
                            "term": "days",
                            "during": "post",
                            "reference": "addaff98-a91e-4674-b993-60bce9dfb093",
                        },
                        "end_date_limitation": {
                            "value": "3",
                            "active": False,
                            "term": "weeks",
                            "during": "pre",
                            "reference": "addaff98-a91e-4674-b993-60bce9dfb093",
                        },
                        "attribute_type": "date",
                        "permissions": None,
                    },
                    {
                        "uuid": "addaff98-a91e-4674-b993-60bce9dfb093",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "kenmerk datum2 title",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_datum2",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": {
                            "value": "1",
                            "active": False,
                            "term": "days",
                            "during": "post",
                            "reference": "currentDate",
                        },
                        "end_date_limitation": {
                            "value": "1",
                            "active": False,
                            "term": "days",
                            "during": "pre",
                            "reference": "currentDate",
                        },
                        "attribute_type": "date",
                        "permissions": None,
                    },
                ],
                "cases": [
                    {
                        "related_casetype": "aaef48d3-98f4-4e66-ade8-af0409137e2f",
                        "requestor_type": "aanvrager",
                        "requestor": {
                            "type": "person",
                            "uuid": "46190248-f0a9-4a9b-9351-9929b0a770c7",
                            "name": "test person",
                        },
                        "kind": "deelzaak",
                        "start_after_fixeddate": None,
                        "start_after": None,
                        "automatic": False,
                        "allocation": {
                            "department_uuid": "432555e5-2a37-4380-8571-5f51df8c07ab",
                            "department_name": "Development",
                            "role_uuid": "8b07fe5d-7685-4a58-b851-3d9692f90c62",
                            "role_name": "Behandelaar",
                        },
                        "handle_in_phase": 2,
                        "assign_automatic": True,
                        "copy_subject_roles_enabled": True,
                        "copy_subject_roles": [
                            "Advocaat",
                            "Aannemer",
                            "Gemachtigde",
                            "Mantelzorger",
                        ],
                        "copy_all_attributes": False,
                        "copy_attributes_mapping": {
                            "800b63f6-aa9a-4df7-9968-2cc036923dd8": "800b63f6-aa9a-4df7-9968-2cc036923dd8",  # tekstfield
                            "40a72131-193d-4ace-804a-9b8de9c4c868": "40a72131-193d-4ace-804a-9b8de9c4c868",  # date field
                        },
                        "copy_case_relations": True,
                        "copy_object_relations": True,
                        "show_on_pip": True,
                        "pip_label": "label op pip",
                        "add_subjects_involved": True,
                        "subjects_involved_authorized_for_case": True,
                        "subjects_involved_email_notifcation": False,
                        "subjects_involved_role": "Advocaat",
                        "subject_involved": {
                            "uuid": "16af9921-8683-4640-bd80-56d077f1a382",
                            "name": "Name of subject",
                            "type": "person",
                        },
                        "subject_involved_magic_string": "advocaat",
                        "creation_style": "background",
                    }
                ],
                "involved_subjects": [
                    {
                        "involved_subject": {
                            "uuid": "3b4730a2-e50b-4d15-893e-525dd27f1c2c",
                            "name": "k. a Test achternaama",
                            "type": "person",
                        },
                        "role": "Auditor",
                        "magic_string": "auditor",
                        "authorized_for_case": True,
                        "notify_by_email": True,
                    },
                    {
                        "involved_subject": {
                            "uuid": "18471e4f-e807-4431-83f6-28a41d7dd92c",
                            "name": "test organisatie",
                            "type": "organization",
                        },
                        "role": "Auditor",
                        "magic_string": "auditor",
                        "authorized_for_case": False,
                        "notify_by_email": False,
                    },
                ],
            },
            {
                "phase_id": 2,
                "milestone_name": "mijlpaal naam van middelste fase",
                "phase_name": "Middelste fase",
                "created": "2024-01-22T11:52:41.444146",
                "last_modified": "2024-01-22T11:52:41.444146",
                "term_in_days": 9,
                "assignment": {
                    "enabled": False,
                    "department_uuid": "432555e5-2a37-4380-8571-5f51df8c07ab",
                    "department_name": "Development",
                    "role_uuid": "8b07fe5d-7685-4a58-b851-3d9692f90c62",
                    "role_name": "Behandelaar",
                },
                "tasks": ["task_a", "task_b", "task_c"],
                "templates": [
                    {
                        "catalog_template_uuid": "9a278be5-4717-4e91-884b-7295fc7136d5",
                        "catalog_template_name": "default_document_sjabloon",
                        "label": "label odt",
                        "description": "toelichting odt",
                        "auto_generate": True,
                        "allow_edit": True,
                        "custom_filename": "bestandsnaam.odt",
                        "target_format": "odt",
                        "document_attribute_uuid": "45b84d9f-5d8c-453c-b250-3a350ef67da3",
                        "document_attribute_name": "kenmerk_document",
                    },
                    {
                        "catalog_template_uuid": "9a278be5-4717-4e91-884b-7295fc7136d5",
                        "catalog_template_name": "default_document_sjabloon",
                        "label": "label pdf",
                        "description": "toelichting pdf",
                        "auto_generate": False,
                        "allow_edit": False,
                        "custom_filename": "file",
                        "target_format": "pdf",
                        "document_attribute_uuid": None,
                        "document_attribute_name": None,
                    },
                    {
                        "catalog_template_uuid": "adf99a19-2348-4af6-907b-38bf9b16bf84",
                        "catalog_template_name": "default_document_sjabloon",
                        "label": "label docx",
                        "description": "toelichting abc",
                        "auto_generate": True,
                        "allow_edit": False,
                        "custom_filename": "work_document",
                        "target_format": "docx",
                        "document_attribute_uuid": None,
                        "document_attribute_name": None,
                    },
                ],
                "email_templates": [
                    {
                        "receiver": "aanvrager",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "cc@test.nl",
                        "bcc": "bcc@test.nl",
                        "send_automatic": "True",
                    },
                    {
                        "receiver": "zaak_behandelaar",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "",
                        "bcc": "",
                        "send_automatic": "True",
                    },
                    {
                        "receiver": "behandelaar",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "cc@test",
                        "bcc": "bcc@test.nl",
                        "send_automatic": "False",
                        "subject": {
                            "uuid": "16af9921-8683-4640-bd80-56d077f1a382",
                            "name": "Rudie dev",
                            "type": "employee",
                        },
                    },
                    {
                        "receiver": "coordinator",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "",
                        "bcc": "",
                        "send_automatic": "False",
                    },
                    {
                        "receiver": "gemachtigde",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "",
                        "bcc": "",
                        "send_automatic": "True",
                    },
                    {
                        "receiver": "betrokkene",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "",
                        "bcc": "",
                        "send_automatic": "True",
                        "person_involved_role": "Ouder",
                    },
                    {
                        "receiver": "overig",
                        "catalog_message_uuid": "4e060f1a-2770-43cd-a30f-afe25836d77a",
                        "catalog_message_label": "default_email_sjabloon",
                        "cc": "",
                        "bcc": "",
                        "send_automatic": "False",
                        "to": "email@test.nl",
                    },
                ],
                "custom_fields": [
                    {
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "Benodigde gegevens",
                        "help_intern": "<p>toelichting <strong><em>voor </em></strong>benodigde gegevens</p>",
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": None,
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "group",
                        "permissions": None,
                    },
                    {
                        "uuid": "b9d34db3-df3f-493e-87db-fd6f3824c978",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "adres_dmv_postcode label",
                        "help_intern": "<p>adres_dmv_postcode toelichting</p>",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": 1,
                        "attribute_name": "kenmerk_adres_dmv_postcode",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_adres",
                        "permissions": None,
                    },
                    {
                        "uuid": "fb903f6c-c15b-421d-aec5-1ab9e853bb61",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "kenmerk_relatie_object_v2_multi title",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_relatie_object_v2_multi",
                        "relationship_type": "custom_object",
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": "Aanmaken tekst object v2 relatie",
                        "create_custom_object_attribute_mapping": {
                            "kenmerk_datum": "[[kenmerk_datum]]",
                            "kenmerk_relatie_object_v2_simple": "",
                            "kenmerk_relatie_object_simple_single": "",
                            "kenmerk_keuzelijst": "",
                            "kenmerk_tekstveld": "[[kenmerk_tekst]]",
                        },
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "relationship",
                        "permissions": None,
                    },
                    {
                        "uuid": "54eb06bb-fb9b-4e03-a210-bfe513bae340",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_relatie_subject",
                        "relationship_type": "subject",
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": "Auditor",
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "relationship",
                        "permissions": None,
                    },
                    {
                        "uuid": "41b4d2a8-a700-4baf-a29d-407268d1aae4",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "adres_dmv_straatnaam titel",
                        "help_intern": "<p>adres_dmv_straatnaam toelichting</p>",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_adres_dmv_straatnaam",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_straat_adres",
                        "permissions": [
                            {
                                "department_uuid": "e7a6b730-b98a-4757-bbfd-07c2edda5180",
                                "department_name": "Frontoffice",
                                "role_uuid": "79a38581-9648-469f-8ea3-d34a99f9e158",
                                "role_name": "Afdelingshoofd",
                            },
                            {
                                "department_uuid": "923996fc-f8f8-4745-9886-a8b00bce1bbe",
                                "department_name": "Backoffice",
                                "role_uuid": "a95e4082-67ab-4969-be66-63c5b27bfb79",
                                "role_name": "Persoonsverwerker",
                            },
                            {
                                "department_uuid": "923996fc-f8f8-4745-9886-a8b00bce1bbe",
                                "department_name": "Backoffice",
                                "role_uuid": "e878ae93-b62e-4673-b105-c08e898e3428",
                                "role_name": "BRP externe bevrager",
                            },
                        ],
                    },
                    {
                        "uuid": "d7e3eec0-b473-46f6-a519-84701f4de00b",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_adres_google_maps",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "googlemaps",
                        "permissions": None,
                    },
                    {
                        "uuid": "237cd3af-6bd5-4110-9584-59edadb0e179",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "adressen_dmv_postcode titel",
                        "help_intern": "<p>adressen_dmv_postcode toelichting</p>",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_adressen_dmv_postcode",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_adressen",
                        "permissions": None,
                    },
                    {
                        "uuid": "69d00149-61c0-4110-979d-dd02019ed1d0",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "adressen_dmv_straatnamen titel",
                        "help_intern": "<p>adressen_dmv_straatnamen toelichting</p>",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": 1,
                        "attribute_name": "kenmerk_adresssen_dmv_straatnaam",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_straat_adressen",
                        "permissions": None,
                    },
                    {
                        "uuid": "a1c4e340-544c-4f78-8042-b7e84a6104bf",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "Groep naam 2",
                        "help_intern": "<p>Toelichting groep naam 2</p>",
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": None,
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "group",
                        "permissions": None,
                    },
                    {
                        "uuid": "25bb050f-5879-4c88-a87d-70c7ba2e28fe",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_adressv2",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": "test  aabb",
                        "map_wms_feature_attribute_label": "WMS-Kaartlaag kenmerk waarde",
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "address_v2",
                        "permissions": [
                            {
                                "department_uuid": "923996fc-f8f8-4745-9886-a8b00bce1bbe",
                                "department_name": "Backoffice",
                                "role_uuid": "e63eea98-1862-401d-bdfa-643f91102688",
                                "role_name": "Administrator",
                            }
                        ],
                    },
                    {
                        "uuid": "12dfb985-fcb4-4a1b-af0f-6e09de4ed046",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "titel van tekstblok",
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": None,
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "textblock",
                        "permissions": None,
                    },
                    {
                        "uuid": "21bea749-2146-4e16-9c07-4d5c00b95acb",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_afbeelding",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "image_from_url",
                        "permissions": None,
                    },
                    {
                        "uuid": "0f9a004f-c71f-4404-b9a1-23fee2aaa3d8",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_betrokkene",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "subject",
                        "permissions": None,
                    },
                    {
                        "uuid": "e14baae6-08ee-4ab2-ae05-c0b126fa1c71",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_datum",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "date",
                        "permissions": None,
                    },
                    {
                        "uuid": "45b84d9f-5d8c-453c-b250-3a350ef67da3",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_document",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "file",
                        "permissions": None,
                    },
                    {
                        "uuid": "3a88effa-ca66-4733-b2da-24e667f8c9df",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_email",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "email",
                        "permissions": None,
                    },
                    {
                        "uuid": "2215818d-371d-4fe5-b176-b8a0612724a4",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_enkelvoudigekeuze",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "option",
                        "permissions": None,
                    },
                    {
                        "uuid": "c6ef9383-d9db-47db-bf38-5f435b335118",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_geolocatie",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "geojson",
                        "permissions": None,
                    },
                    {
                        "uuid": "3e89513a-7821-4881-bd21-fee7daace5ff",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_groot_tekstveld",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "textarea",
                        "permissions": None,
                    },
                    {
                        "uuid": "063935d4-daf7-4bc9-be99-95b79d0c0930",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_kalenderafspraak_qmatic",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "calendar",
                        "permissions": None,
                    },
                    {
                        "uuid": "aec79dab-6cd7-4ce0-800e-eeb00aa4b625",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_kalenderafspraak_supersaas",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "calendar_supersaas",
                        "permissions": None,
                    },
                    {
                        "uuid": "72676f0b-9f36-4c4d-abbf-9eedcb38810f",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_keuzelijst",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "select",
                        "permissions": None,
                    },
                    {
                        "uuid": "84e424a3-8b67-4961-8943-7b593072ec0d",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_locatiemetkaart",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": "test  aabb",
                        "map_wms_feature_attribute_label": "Enkelvoudige keuze kenmerk label",
                        "map_wms_feature_attribute_id": "2215818d-371d-4fe5-b176-b8a0612724a4",
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "geolatlon",
                        "permissions": None,
                    },
                    {
                        "uuid": "244295bc-03ef-43a4-a657-b622ebd7434d",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_meervoudigekeuze",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "checkbox",
                        "permissions": None,
                    },
                    {
                        "uuid": "e6395d63-e252-4931-a375-b4582c3c4af2",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_numeriek",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "numeric",
                        "permissions": None,
                    },
                    {
                        "uuid": "f707cf6c-f40a-478b-b34b-55bbcefd2c80",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_rekeningnummer",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bankaccount",
                        "permissions": None,
                    },
                    {
                        "uuid": "105f857b-5da1-4943-94b6-eaba4e58740b",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "",
                        "help_intern": "",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_relatie_object_simple_single",
                        "relationship_type": "custom_object",
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": "tekst object single aanmaken",
                        "create_custom_object_attribute_mapping": {
                            "kenmerk_relatie_object_v2_simple": "",
                            "kenmerk_datum": "[[kenmerk_datum]]",
                            "kenmerk_tekstveld": "[[kenmerk_tekst]]",
                            "kenmerk_keuzelijst": "",
                            "kenmerk_relatie_object_simple_single": "",
                        },
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "relationship",
                        "permissions": None,
                    },
                    {
                        "uuid": "22dbb824-081b-43ab-ab88-bffbe3882a24",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_richtext",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "richtext",
                        "permissions": None,
                    },
                    {
                        "uuid": "e321d1b6-2d9a-428f-85ab-99a7824340ab",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_straat",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_openbareruimte",
                        "permissions": None,
                    },
                    {
                        "uuid": "4e044345-009a-45ed-a062-a9968a7f53a9",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_straten",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_openbareruimtes",
                        "permissions": None,
                    },
                    {
                        "uuid": "fb42f797-194e-455e-8333-f467b687e2f6",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_tekstveld",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "text",
                        "permissions": None,
                    },
                    {
                        "uuid": "0b079cf9-f765-4f9f-ba93-21c914e96680",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_tekstveld_hoofdletters",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "text_uc",
                        "permissions": None,
                    },
                    {
                        "uuid": "afff2101-0bc2-4607-a3b5-a14844c6cb13",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_valuta",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valuta",
                        "permissions": None,
                    },
                    {
                        "uuid": "90a70e9c-ae6b-4e5e-8c5b-4f95460bc13b",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_valuta_incl_btw",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valutain",
                        "permissions": None,
                    },
                    {
                        "uuid": "40407a46-a9c9-4681-b898-815dd4fcd16c",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_valuta_incl_btw_6",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valutain6",
                        "permissions": None,
                    },
                    {
                        "uuid": "35d8363e-0806-4fe7-b59a-9cd749abd610",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_valuta_incl_btw_21",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valutain21",
                        "permissions": None,
                    },
                    {
                        "uuid": "2077c5e1-f058-4c9e-8c44-2831a7e36cc9",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_webadres",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "url",
                        "permissions": None,
                    },
                    {
                        "uuid": "581902a2-8564-4d85-ab09-0ee6188d7883",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "valuta_ex_btw",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valutaex",
                        "permissions": None,
                    },
                    {
                        "uuid": "30d979de-a635-42a1-979d-895db95ea149",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "valuta_ex_btw_21",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valutaex21",
                        "permissions": None,
                    },
                    {
                        "uuid": "1f5f3328-b7f1-4334-b49c-135d59793fac",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "valuta_ex_btw_6",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "valutaex6",
                        "permissions": None,
                    },
                    {
                        "uuid": "40bf5174-6ce7-4488-9cba-023abe71b227",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "straat bag titel",
                        "help_intern": "<p>straat bag toelichting</p>",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_straat",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_openbareruimte",
                        "permissions": None,
                    },
                    {
                        "uuid": "1cea466e-5683-47d4-9bd2-fba5a169b094",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": "straten bag titel",
                        "help_intern": "<p>straten bag toelichting</p>",
                        "help_extern": "",
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_straten",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "bag_openbareruimtes",
                        "permissions": None,
                    },
                    {
                        "uuid": "0dfb94f7-b527-4693-9c4d-a314de8bfa04",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_appointment",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "appointment",
                        "permissions": None,
                    },
                    {
                        "uuid": "3db55b26-5211-4e2b-a90d-783807b35f3a",
                        "is_group": False,
                        "referential": False,
                        "mandatory": False,
                        "system_attribute": False,
                        "title": None,
                        "help_intern": None,
                        "help_extern": None,
                        "is_multiple": False,
                        "label_multiple": None,
                        "use_as_case_address": False,
                        "attribute_name": "kenmerk_appointmentv2",
                        "relationship_type": None,
                        "publish_pip": False,
                        "pip_can_change": False,
                        "publish_api": False,
                        "skip_change_approval": False,
                        "create_custom_object_enabled": False,
                        "create_custom_object_label": None,
                        "create_custom_object_attribute_mapping": None,
                        "relationship_subject_role": None,
                        "show_on_map": False,
                        "map_wms_layer_id": None,
                        "map_wms_feature_attribute_label": None,
                        "map_wms_feature_attribute_id": None,
                        "map_case_location": False,
                        "start_date_limitation": None,
                        "end_date_limitation": None,
                        "attribute_type": "appointment_v2",
                        "permissions": None,
                    },
                ],
            },
            {
                "phase_id": 3,
                "milestone_name": "Afgehandeld",
                "phase_name": "Afhandelen",
                "created": "2024-01-22T11:52:41.444146",
                "last_modified": "2024-01-22T11:52:41.444146",
                "term_in_days": 0,
                "assignment": None,
                "custom_fields": None,
                "cases": [
                    {
                        "related_casetype": "aaef48d3-98f4-4e66-ade8-af0409137e2f",
                        "requestor_type": "aanvrager",
                        "requestor": None,
                        "kind": "vervolgzaak_datum",
                        "start_after_fixeddate": "2025-01-31",
                        "start_after": None,
                        "automatic": False,
                        "allocation": {
                            "department_uuid": "432555e5-2a37-4380-8571-5f51df8c07ab",
                            "department_name": "Development",
                            "role_uuid": "8b07fe5d-7685-4a58-b851-3d9692f90c62",
                            "role_name": "Behandelaar",
                        },
                        "handle_in_phase": 3,
                        "assign_automatic": False,
                        "copy_subject_roles_enabled": False,
                        "copy_subject_roles": None,
                        "copy_all_attributes": False,
                        "copy_attributes_mapping": None,
                        "copy_case_relations": False,
                        "copy_object_relations": False,
                        "show_on_pip": False,
                        "pip_label": None,
                        "add_subjects_involved": False,
                        "subjects_involved_authorized_for_case": False,
                        "subjects_involved_email_notifcation": False,
                        "subjects_involved_role": "Advocaat",
                        "subject_involved": None,
                        "subject_involved_magic_string": "",
                        "creation_style": "background",
                        "owner_role": None,
                    },
                    {
                        "related_casetype": "2c40ea6c-a3d3-4685-bfc9-cba7d1eab7a1",
                        "requestor_type": "aanvrager",
                        "requestor": None,
                        "kind": "vervolgzaak",
                        "start_after_fixeddate": None,
                        "start_after": 12,
                        "automatic": False,
                        "allocation": {
                            "department_uuid": "432555e5-2a37-4380-8571-5f51df8c07ab",
                            "department_name": "Development",
                            "role_uuid": "8b07fe5d-7685-4a58-b851-3d9692f90c62",
                            "role_name": "Behandelaar",
                        },
                        "handle_in_phase": 3,
                        "assign_automatic": False,
                        "copy_subject_roles_enabled": False,
                        "copy_subject_roles": None,
                        "copy_all_attributes": False,
                        "copy_attributes_mapping": None,
                        "copy_case_relations": False,
                        "copy_object_relations": False,
                        "show_on_pip": False,
                        "pip_label": None,
                        "add_subjects_involved": False,
                        "subjects_involved_authorized_for_case": False,
                        "subjects_involved_email_notifcation": False,
                        "subjects_involved_role": "Advocaat",
                        "subject_involved": None,
                        "subject_involved_magic_string": "",
                        "creation_style": "background",
                        "owner_role": None,
                    },
                ],
            },
        ],
        results=[
            {
                "name": "name for result",
                "result_type": "aangegaan",
                "is_default": True,
                "selection_list": "selectielijst x",
                "selection_list_number": "123",
                "selection_list_source_date": "2024-10-10",
                "selection_list_end_date": "2024-10-11",
                "archival_trigger": True,
                "archival_nomination": "Bewaren (B)",
                "retention_period": 28,
                "archival_nomination_valuation": "vervallen",
                "comments": "comments for result",
                "processtype_name": "processtype name for result",
                "processtype_number": "123",
                "processtype_description": "processtype description",
                "processtype_explanation": "processtype explanation",
                "processtype_object": "processtype object",
                "procestype_generic": "Specifiek",
                "origin": "Trendanalyse",
                "process_period": "D",
            }
        ],
    )

    return versioned_casetype


@pytest.fixture
def department_rows_mock():
    department1 = mock.MagicMock()
    department1.configure_mock(
        id=1, name="department1", uuid="432555e5-2a37-4380-8571-5f51df8c07ab"
    )
    department2 = mock.MagicMock()
    department2.configure_mock(
        id=1, name="department2", uuid="923996fc-f8f8-4745-9886-a8b00bce1bbe"
    )
    department3 = mock.MagicMock()
    department3.configure_mock(
        id=3, name="department3", uuid="e7a6b730-b98a-4757-bbfd-07c2edda5180"
    )
    return [
        department1,
        department2,
        department3,
    ]


@pytest.fixture
def role_rows_mock():
    role1 = mock.MagicMock()
    role1.configure_mock(
        id=1,
        uuid="79a38581-9648-469f-8ea3-d34a99f9e158",
        name="Afdelingshoofd",
    )
    role2 = mock.MagicMock()
    role2.configure_mock(
        id=2,
        uuid="e878ae93-b62e-4673-b105-c08e898e3428",
        name="BRP externe bevrager",
    )
    role3 = mock.MagicMock()
    role3.configure_mock(
        id=3,
        uuid="a95e4082-67ab-4969-be66-63c5b27bfb79",
        name="Persoonsverwerker",
    )
    role4 = mock.MagicMock()
    role4.configure_mock(
        id=4,
        uuid="e63eea98-1862-401d-bdfa-643f91102688",
        name="Administrator",
    )
    role5 = mock.MagicMock()
    role5.configure_mock(
        id=5,
        uuid="8b07fe5d-7685-4a58-b851-3d9692f90c62",
        name="Behandelaar",
    )
    return [role1, role2, role3, role4, role5]


@pytest.fixture()
def zaaktype_kenmerken_rows_mock(versioned_casetype, attributes_rows_mock):
    def _find_bibliotheek_kenmerk_id(uuid: str | None) -> int | None:
        for attribute in attributes_rows_mock:
            if attribute.uuid == uuid:
                return attribute.id
        return None

    result_rows = []
    count = 2
    for phase in versioned_casetype.phases:
        phase_result_rows = []
        for cf in phase["custom_fields"] or []:
            properties = {}
            if cf["attribute_type"] == "date":
                properties = {
                    "date_limit": {
                        "start": {
                            "active": 1,
                            "num": 2,
                            "term": "days",
                            "during": "post",
                        },
                        "end": {
                            "active": 1,
                            "num": 3,
                            "term": "weeks",
                            "during": "pre",
                        },
                    }
                }
            phase_result_rows.append(
                mock.Mock(
                    id=count,
                    bibliotheek_kenmerken_id=_find_bibliotheek_kenmerk_id(
                        uuid=cf.get("uuid")
                    ),
                    properties=properties,
                )
            )
            count = count + 1
        result_rows.append(phase_result_rows)
    return result_rows


@pytest.fixture()
def attributes_rows_mock(versioned_casetype):
    result_rows = {}
    for phase in versioned_casetype.phases:
        for cf in phase["custom_fields"] or []:
            if cf.get("uuid"):
                result_rows[cf["uuid"]] = cf

    result = [
        mock.Mock(
            uuid=uuid,
            id=index,
            naam_public=attribute["attribute_name"],
            magic_string=attribute["attribute_name"],
        )
        for index, (uuid, attribute) in enumerate(result_rows.items(), 1)
    ]
    # add an attribute which is in the catalog, but is not in on of the fields of the versioned_casetype.
    # (it's used in the case mapping as a target attribute in another casetype)
    result.append(
        mock.Mock(
            uuid="4bd1a943-3ce7-446f-bf73-b81a5ecb3fff",
            id=12345,
            naam_public="attribute_from_catalog_not_in_casetype",
            magic_string="attribute_from_catalog_not_in_casetype",
        ),
    )
    return result


@pytest.fixture()
def target_attribute_mappings_cases(versioned_casetype):
    # get the target attribute uuids configures when copy attributes in cases
    result = set()
    for phase in versioned_casetype.phases:
        for case in phase.get("cases", []):
            result.update(
                [
                    mock.Mock(uuid=uuid)
                    for uuid in (
                        case.get("copy_attributes_mapping", {}) or {}
                    ).values()
                ]
            )

            # result.update((case.get("copy_attributes_mapping", {}) or {}).values())
    return result


@pytest.fixture()
def template_rows_mock(versioned_casetype):
    # prepare the result rows mock for templates (sjablonen), which is the result needed in AssertTemplatesExist
    result_rows = {}
    for phase in versioned_casetype.phases:
        for template in phase.get("templates", []) or []:
            if template.get("catalog_template_uuid"):
                result_rows[template["catalog_template_uuid"]] = template

    return [
        mock.Mock(
            uuid=uuid,
            id=index,
            naam_public=template["catalog_template_name"],
        )
        for index, (uuid, template) in enumerate(result_rows.items(), 1)
    ]


@pytest.fixture()
def email_template_rows_mock(versioned_casetype):
    # prepare the result rows mock for templates (sjablonen), which is the result needed in AssertTemplatesExist
    result_rows = {}
    for phase in versioned_casetype.phases:
        for template in phase.get("email_templates", []) or []:
            if template.get("catalog_message_uuid"):
                result_rows[template["catalog_message_uuid"]] = template

    return [
        mock.Mock(
            uuid=uuid,
            id=index,
            naam_public=template["catalog_message_label"],
        )
        for index, (uuid, template) in enumerate(result_rows.items(), 1)
    ]


@pytest.fixture()
def casetype_rows_mock(versioned_casetype):
    # prepare the result rows mock for casetypes (cases), which is the result needed in AssertCasetypeExist
    result_rows = {}
    for phase in versioned_casetype.phases:
        for case in phase.get("cases", []) or []:
            if case.get("related_casetype"):
                result_rows[case["related_casetype"]] = case

    return [
        mock.Mock(
            uuid=uuid,
            id=index,
        )
        for index, (uuid, case) in enumerate(result_rows.items(), 20)
    ]


def mock_to_entity(
    versioned_casetype_mock: mock.Mock,
) -> entities.VersionedCaseType:
    return entities.VersionedCaseType.parse_obj(
        {
            "uuid": versioned_casetype_mock.uuid,
            "casetype_uuid": versioned_casetype_mock.casetype_uuid,
            "active": versioned_casetype_mock.active,
            "catalog_folder": versioned_casetype_mock.catalog_folder,
            "general_attributes": versioned_casetype_mock.general_attributes,
            "documentation": versioned_casetype_mock.documentation,
            "relations": versioned_casetype_mock.relations,
            "webform": versioned_casetype_mock.webform,
            "registrationform": versioned_casetype_mock.registrationform,
            "case_dossier": versioned_casetype_mock.case_dossier,
            "api": versioned_casetype_mock.api,
            "authorization": versioned_casetype_mock.authorization,
            "child_casetype_settings": versioned_casetype_mock.child_casetype_settings,
            "change_log": None,
            "phases": versioned_casetype_mock.phases,
            "results": versioned_casetype_mock.results,
        }
    )


class TestAs_A_User_I_Want_To_Get_VersionedCaseType(TestBase):
    def setup_method(self):
        self.load_query_instance(catalog)
        self.mock_infra = mock.MagicMock()
        self.event_service = EventService(
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

    def test_get_versioned_casetype_required_attributes_none(
        self, versioned_casetype
    ):
        with pytest.raises(Conflict) as excinfo:
            self.qry.get_versioned_casetype_with_uuid(
                uuid=None, version_uuid=None
            )
        assert excinfo.value.args[0] == (
            "One of the required attributes uuid / version_uuid is not present."
        )

    def test_get_versioned_casetype_required_attributes_both_filled(
        self, versioned_casetype
    ):
        with pytest.raises(Conflict) as excinfo:
            self.qry.get_versioned_casetype_with_uuid(
                uuid=uuid4(), version_uuid=uuid4()
            )
        assert excinfo.value.args[0] == (
            "Only one of the required attributes uuid or version_uuid is allowed."
        )

    def test_get_versioned_casetype(self, versioned_casetype):
        case_type_version_uuid = uuid4()
        self.session.execute().fetchone.return_value = versioned_casetype

        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=postgresql.dialect())
        assert (
            str(query)
            == 'SELECT view_case_type_version_v2.uuid, view_case_type_version_v2.casetype_uuid, view_case_type_version_v2.active, view_case_type_version_v2.catalog_folder, view_case_type_version_v2.general_attributes, view_case_type_version_v2.documentation, view_case_type_version_v2.relations, view_case_type_version_v2.webform, view_case_type_version_v2.registrationform, view_case_type_version_v2.case_dossier, view_case_type_version_v2.api, view_case_type_version_v2."authorization", view_case_type_version_v2.child_casetype_settings, view_case_type_version_v2.phases, view_case_type_version_v2.results \nFROM view_case_type_version_v2 \nWHERE view_case_type_version_v2.uuid = %(uuid_1)s::UUID'
        )
        assert result == mock_to_entity(
            versioned_casetype
        )  # check if the mocked db_row is transformed to the entity correctly

    def test_get_versioned_casetype_invalid_company_data(
        self, versioned_casetype
    ):
        case_type_version_uuid = uuid4()
        self.session.execute().fetchone.return_value = versioned_casetype
        versioned_casetype.relations["preset_requestor"] = None

        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=postgresql.dialect())
        assert (
            str(query)
            == 'SELECT view_case_type_version_v2.uuid, view_case_type_version_v2.casetype_uuid, view_case_type_version_v2.active, view_case_type_version_v2.catalog_folder, view_case_type_version_v2.general_attributes, view_case_type_version_v2.documentation, view_case_type_version_v2.relations, view_case_type_version_v2.webform, view_case_type_version_v2.registrationform, view_case_type_version_v2.case_dossier, view_case_type_version_v2.api, view_case_type_version_v2."authorization", view_case_type_version_v2.child_casetype_settings, view_case_type_version_v2.phases, view_case_type_version_v2.results \n'
            "FROM view_case_type_version_v2 \n"
            "WHERE view_case_type_version_v2.uuid = %(uuid_1)s::UUID"
        )
        assert result == mock_to_entity(
            versioned_casetype
        )  # check if the mocked db_row is transformed to the entity correctly

    def test_get_versioned_casetype_not_found(self):
        case_type_version_uuid = uuid4()
        self.session.execute().fetchone.return_value = None  # no result

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_versioned_casetype_with_uuid(
                version_uuid=case_type_version_uuid
            )
        assert excinfo.value.args[0] == (
            f"VersionedCaseType with uuid '{case_type_version_uuid}' not found."
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=postgresql.dialect())
        assert (
            str(query)
            == 'SELECT view_case_type_version_v2.uuid, view_case_type_version_v2.casetype_uuid, view_case_type_version_v2.active, view_case_type_version_v2.catalog_folder, view_case_type_version_v2.general_attributes, view_case_type_version_v2.documentation, view_case_type_version_v2.relations, view_case_type_version_v2.webform, view_case_type_version_v2.registrationform, view_case_type_version_v2.case_dossier, view_case_type_version_v2.api, view_case_type_version_v2."authorization", view_case_type_version_v2.child_casetype_settings, view_case_type_version_v2.phases, view_case_type_version_v2.results \n'
            "FROM view_case_type_version_v2 \n"
            "WHERE view_case_type_version_v2.uuid = %(uuid_1)s::UUID"
        )

    def test_search_with_casetype_uuid(self, versioned_casetype):
        case_type_uuid = uuid4()
        case_type_uuid_result = mock.Mock()
        case_type_uuid_result.configure_mock(
            uuid=uuid4(),
        )

        self.session.execute().fetchone.side_effect = [
            case_type_uuid_result,
            versioned_casetype,
        ]

        result = self.qry.get_versioned_casetype_with_uuid(uuid=case_type_uuid)

        args_list = self.session.execute.call_args_list
        # test if the query for retrieving the casetype_version uuid with a casetype uuid is exectuted
        assert (
            str(args_list[1].args[0].compile(dialect=postgresql.dialect()))
            == "SELECT zaaktype_node.uuid \nFROM zaaktype_node \nWHERE zaaktype_node.id = (SELECT zaaktype.zaaktype_node_id \nFROM zaaktype \nWHERE zaaktype.uuid = %(uuid_1)s::UUID) \n LIMIT %(param_1)s"
        )

        # test if the query for retrieving the VersionedCaseType is exectute
        assert (
            str(args_list[2].args[0].compile(dialect=postgresql.dialect()))
            == 'SELECT view_case_type_version_v2.uuid, view_case_type_version_v2.casetype_uuid, view_case_type_version_v2.active, view_case_type_version_v2.catalog_folder, view_case_type_version_v2.general_attributes, view_case_type_version_v2.documentation, view_case_type_version_v2.relations, view_case_type_version_v2.webform, view_case_type_version_v2.registrationform, view_case_type_version_v2.case_dossier, view_case_type_version_v2.api, view_case_type_version_v2."authorization", view_case_type_version_v2.child_casetype_settings, view_case_type_version_v2.phases, view_case_type_version_v2.results \n'
            "FROM view_case_type_version_v2 \n"
            "WHERE view_case_type_version_v2.uuid = %(uuid_1)s::UUID"
        )
        assert result == mock_to_entity(
            versioned_casetype
        )  # check if the mocked db_row is transformed to the entity correctly

    def test_search_casetype_uuid_not_found(self):
        case_type_uuid = uuid4()

        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_versioned_casetype_with_uuid(uuid=case_type_uuid)
        assert excinfo.value.args[0] == (
            f"Version dependent uuid for casetype with uuid '{case_type_uuid}' not found."
        )

    def test_get_versioned_casetype_invalid_monitoring_period(
        self, versioned_casetype
    ):
        case_type_version_uuid = uuid4()
        self.session.execute().fetchone.return_value = versioned_casetype

        # check for MonitoringPeriod validation and fallback on default values
        versioned_casetype.general_attributes["legal_period"]["type"] = (
            "kalenderdagen"
        )
        versioned_casetype.general_attributes["legal_period"]["value"] = (
            "invalid value"
        )
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.qry.get_versioned_casetype_with_uuid(
                version_uuid=case_type_version_uuid
            )
        assert (
            "2 validation errors for VersionedCaseType\ngeneral_attributes -> legal_period -> value\n  value is not a valid integer (type=type_error.integer)\ngeneral_attributes -> legal_period -> value\n  invalid date format (type=value_error.date)"
            in str(excinfo.value)
        )

        versioned_casetype.general_attributes["legal_period"]["type"] = (
            "einddatum"
        )
        versioned_casetype.general_attributes["legal_period"]["value"] = (
            "invalid value"
        )
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert result.general_attributes.legal_period.value is None

        # test for numeric value < 1
        versioned_casetype.general_attributes["legal_period"]["type"] = (
            "kalenderdagen"
        )
        versioned_casetype.general_attributes["legal_period"]["value"] = "0"
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.qry.get_versioned_casetype_with_uuid(
                version_uuid=case_type_version_uuid
            )
        assert (
            "Value '0' of kalenderdagen' for monitoring must be 1 or higher (type=value_error)"
            in str(excinfo.value)
        )

    def test_get_versioned_casetype_invalid_pricing_decimals(
        self, versioned_casetype
    ):
        case_type_version_uuid = uuid4()
        self.session.execute().fetchone.return_value = versioned_casetype

        # test price input beeing rouded to 2 decimals
        versioned_casetype.webform["price"]["web"] = "12.123"
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert result.webform.price.web == Decimal("12.12")

        versioned_casetype.webform["price"]["web"] = "99"
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert result.webform.price.web == Decimal("99")

        versioned_casetype.webform["price"]["web"] = "15.106"
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert result.webform.price.web == Decimal("15.11")  # round up

        versioned_casetype.webform["price"]["web"] = "12,34"
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert result.webform.price.web is None  # , is an invalid seperator.

        versioned_casetype.webform["price"]["web"] = "100.000.00"
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert result.webform.price.web is None  # 2 . chars are invalid.

        versioned_casetype.webform["price"]["web"] = None
        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )
        assert (
            result.webform.price.web is None
        )  # Deal with None value properly

    def test_get_versioned_casetype_start_after_fixeddate(
        self, versioned_casetype
    ):
        # db records contain date format dd-mm-YYYY for start_after_fixeddate field. test convertion
        case_type_version_uuid = uuid4()
        self.session.execute().fetchone.return_value = versioned_casetype
        versioned_casetype.phases[2]["cases"][0]["start_after_fixeddate"] = (
            "27-02-2025"  # dd-mm-yyyy format
        )

        result = self.qry.get_versioned_casetype_with_uuid(
            version_uuid=case_type_version_uuid
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=postgresql.dialect())
        assert (
            str(query)
            == 'SELECT view_case_type_version_v2.uuid, view_case_type_version_v2.casetype_uuid, view_case_type_version_v2.active, view_case_type_version_v2.catalog_folder, view_case_type_version_v2.general_attributes, view_case_type_version_v2.documentation, view_case_type_version_v2.relations, view_case_type_version_v2.webform, view_case_type_version_v2.registrationform, view_case_type_version_v2.case_dossier, view_case_type_version_v2.api, view_case_type_version_v2."authorization", view_case_type_version_v2.child_casetype_settings, view_case_type_version_v2.phases, view_case_type_version_v2.results \nFROM view_case_type_version_v2 \nWHERE view_case_type_version_v2.uuid = %(uuid_1)s::UUID'
        )
        assert result == mock_to_entity(
            versioned_casetype
        )  # check if the mocked db_row is transformed to the entity correctly


class TestAs_A_User_I_Want_To_Create_A_VersionedCaseType(TestBase):
    @pytest.fixture(autouse=True)
    def setup_method(
        self,
        department_rows_mock,
        role_rows_mock,
        attributes_rows_mock,
        zaaktype_kenmerken_rows_mock,
        template_rows_mock,
        email_template_rows_mock,
        casetype_rows_mock,
        target_attribute_mappings_cases,
    ):
        self.load_command_instance(catalog)
        self.mock_infra = mock.MagicMock()
        self.event_service = EventService(
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        folder_row = mock.MagicMock()
        folder_row.configure_mock(id=1, uuid=uuid4(), name="folder name")

        self.session.execute().fetchall.side_effect = [
            department_rows_mock,
            role_rows_mock,
            attributes_rows_mock,
            template_rows_mock,
            email_template_rows_mock,
            casetype_rows_mock,
            target_attribute_mappings_cases,
            *zaaktype_kenmerken_rows_mock,
            [],
        ]

        self.session.execute().fetchone.side_effect = (
            folder_row,
            None,  # assert_casetype_not_exists
            None,  # assert casetype unique
            mock.MagicMock(id=11111),  # zaaktype id
            mock.MagicMock(id=1324),  # subject row preset_client
            mock.MagicMock(id=11111),  # zaaktype definitie id
            mock.MagicMock(max_version=25),  # max version zaaktype_node
            mock.MagicMock(),  # a legacy subject
            mock.MagicMock(id=11112),  # zaaktype node id
            mock.MagicMock(id=11113),
            mock.MagicMock(id=11114),  # logging id
            mock.MagicMock(id=11115),  # zaaktype_status result phase 1
            mock.MagicMock(id=11116),  # zaaktype_status result phase 2
            mock.MagicMock(id=11117),  # zaaktype_status result phase 3
            mock.MagicMock(
                id=34
            ),  # legacy subject id (for zaaktype_notificatie)
            mock.MagicMock(id=5678),  # subject row preset_client
            mock.MagicMock(id=20000),  # legacy subject_id in zaken config
            mock.MagicMock(
                id=20001
            ),  # legacy subject_id in default subjects (natuurlijk persoon)
            mock.MagicMock(
                id=20002
            ),  # legacy subject_id in default subjects (bedrijf)
        )
        self.session.execute.reset_mock()

    def test_create_versioned_casetype(self, versioned_casetype):
        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        queries = {
            0: "SELECT bibliotheek_categorie.id, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, bibliotheek_categorie.last_modified, bibliotheek_categorie_1.uuid AS parent_uuid, bibliotheek_categorie_1.naam AS parent_name, bibliotheek_categorie_1.id AS parent_id \nFROM bibliotheek_categorie LEFT OUTER JOIN bibliotheek_categorie AS bibliotheek_categorie_1 ON bibliotheek_categorie.pid = bibliotheek_categorie_1.id \nWHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID",
            1: "SELECT zaaktype.uuid \nFROM zaaktype \nWHERE zaaktype.uuid = %(uuid_1)s::UUID",
            2: "SELECT zaaktype_node.uuid, zaaktype_node.titel \nFROM zaaktype_node JOIN zaaktype ON zaaktype_node.zaaktype_id = zaaktype.id \nWHERE zaaktype_node.titel = %(titel_1)s AND zaaktype.uuid != %(uuid_1)s::UUID \n LIMIT %(param_1)s",
            3: "SELECT groups.id, groups.uuid, groups.name \nFROM groups \nWHERE groups.uuid IN (__[POSTCOMPILE_uuid_1])",
            4: "SELECT roles.id, roles.uuid, roles.name \nFROM roles \nWHERE roles.uuid IN (__[POSTCOMPILE_uuid_1])",
            5: "SELECT bibliotheek_kenmerken.id, bibliotheek_kenmerken.naam_public, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.uuid \nFROM bibliotheek_kenmerken \nWHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.uuid IN (__[POSTCOMPILE_uuid_1])",
            6: "SELECT bibliotheek_sjablonen.id, bibliotheek_sjablonen.naam, bibliotheek_sjablonen.uuid \nFROM bibliotheek_sjablonen \nWHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.uuid IN (__[POSTCOMPILE_uuid_1])",
            7: "SELECT bibliotheek_notificaties.id, bibliotheek_notificaties.label, bibliotheek_notificaties.uuid \nFROM bibliotheek_notificaties \nWHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.uuid IN (__[POSTCOMPILE_uuid_1])",
            8: "SELECT zaaktype.id, zaaktype.uuid \nFROM zaaktype \nWHERE zaaktype.deleted IS NULL AND zaaktype.uuid IN (__[POSTCOMPILE_uuid_1])",
            9: "SELECT bibliotheek_kenmerken.uuid \nFROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON bibliotheek_kenmerken.id = zaaktype_kenmerken.bibliotheek_kenmerken_id JOIN zaaktype ON zaaktype.zaaktype_node_id = zaaktype_kenmerken.zaaktype_node_id \nWHERE bibliotheek_kenmerken.uuid IN (__[POSTCOMPILE_uuid_1]) AND zaaktype.uuid = %(uuid_2)s::UUID",
            10: "INSERT INTO zaaktype (uuid, bibliotheek_categorie_id, active) VALUES (%(uuid)s::UUID, (SELECT bibliotheek_categorie.id \nFROM bibliotheek_categorie \nWHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID), %(active)s) RETURNING zaaktype.id",
            11: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            12: "INSERT INTO zaaktype_definitie (handelingsinitiator, grondslag, procesbeschrijving, afhandeltermijn, afhandeltermijn_type, servicenorm, servicenorm_type, pdc_tarief, omschrijving_upl, aard, preset_client, extra_informatie, extra_informatie_extern) VALUES (%(handelingsinitiator)s, %(grondslag)s, %(procesbeschrijving)s, %(afhandeltermijn)s, %(afhandeltermijn_type)s, %(servicenorm)s, %(servicenorm_type)s, %(pdc_tarief)s, %(omschrijving_upl)s, %(aard)s, %(preset_client)s, %(extra_informatie)s, %(extra_informatie_extern)s) RETURNING zaaktype_definitie.id",
            13: "SELECT max(zaaktype_node.version) AS max_version \nFROM zaaktype_node \nWHERE zaaktype_node.zaaktype_id = %(zaaktype_id_1)s",
            14: "SELECT subject.id AS id \nFROM subject \nWHERE subject.uuid = %(uuid_1)s::UUID",
            15: "INSERT INTO zaaktype_node (zaaktype_id, zaaktype_definitie_id, titel, code, trigger, version, properties, is_public, zaaktype_trefwoorden, zaaktype_omschrijving, webform_toegang, aanvrager_hergebruik, automatisch_behandelen, toewijzing_zaakintake, online_betaling, adres_andere_locatie, adres_aanvrager, adres_geojson, extra_relaties_in_aanvraag, contact_info_intake, prevent_pip, contact_info_email_required, contact_info_phone_required, contact_info_mobile_phone_required) VALUES (%(zaaktype_id)s, %(zaaktype_definitie_id)s, %(titel)s, %(code)s, %(trigger)s, %(version)s, %(properties)s, %(is_public)s, %(zaaktype_trefwoorden)s, %(zaaktype_omschrijving)s, %(webform_toegang)s, %(aanvrager_hergebruik)s, %(automatisch_behandelen)s, %(toewijzing_zaakintake)s, %(online_betaling)s, %(adres_andere_locatie)s, %(adres_aanvrager)s, %(adres_geojson)s, %(extra_relaties_in_aanvraag)s, %(contact_info_intake)s, %(prevent_pip)s, %(contact_info_email_required)s, %(contact_info_phone_required)s, %(contact_info_mobile_phone_required)s) RETURNING zaaktype_node.id",
            16: "DELETE FROM zaaktype_authorisation WHERE zaaktype_authorisation.zaaktype_id = %(zaaktype_id_1)s",
            17: "INSERT INTO zaaktype_authorisation (zaaktype_node_id, recht, role_id, ou_id, zaaktype_id, confidential) VALUES (%(zaaktype_node_id_m0)s, %(recht_m0)s, %(role_id_m0)s, %(ou_id_m0)s, %(zaaktype_id_m0)s, %(confidential_m0)s), (%(zaaktype_node_id_m1)s, %(recht_m1)s, %(role_id_m1)s, %(ou_id_m1)s, %(zaaktype_id_m1)s, %(confidential_m1)s), (%(zaaktype_node_id_m2)s, %(recht_m2)s, %(role_id_m2)s, %(ou_id_m2)s, %(zaaktype_id_m2)s, %(confidential_m2)s), (%(zaaktype_node_id_m3)s, %(recht_m3)s, %(role_id_m3)s, %(ou_id_m3)s, %(zaaktype_id_m3)s, %(confidential_m3)s), (%(zaaktype_node_id_m4)s, %(recht_m4)s, %(role_id_m4)s, %(ou_id_m4)s, %(zaaktype_id_m4)s, %(confidential_m4)s), (%(zaaktype_node_id_m5)s, %(recht_m5)s, %(role_id_m5)s, %(ou_id_m5)s, %(zaaktype_id_m5)s, %(confidential_m5)s), (%(zaaktype_node_id_m6)s, %(recht_m6)s, %(role_id_m6)s, %(ou_id_m6)s, %(zaaktype_id_m6)s, %(confidential_m6)s), (%(zaaktype_node_id_m7)s, %(recht_m7)s, %(role_id_m7)s, %(ou_id_m7)s, %(zaaktype_id_m7)s, %(confidential_m7)s), (%(zaaktype_node_id_m8)s, %(recht_m8)s, %(role_id_m8)s, %(ou_id_m8)s, %(zaaktype_id_m8)s, %(confidential_m8)s), (%(zaaktype_node_id_m9)s, %(recht_m9)s, %(role_id_m9)s, %(ou_id_m9)s, %(zaaktype_id_m9)s, %(confidential_m9)s)",
            18: "INSERT INTO zaaktype_betrokkenen (zaaktype_node_id, betrokkene_type) VALUES (%(zaaktype_node_id_m0)s, %(betrokkene_type_m0)s), (%(zaaktype_node_id_m1)s, %(betrokkene_type_m1)s), (%(zaaktype_node_id_m2)s, %(betrokkene_type_m2)s), (%(zaaktype_node_id_m3)s, %(betrokkene_type_m3)s), (%(zaaktype_node_id_m4)s, %(betrokkene_type_m4)s)",
            19: "UPDATE zaaktype SET zaaktype_node_id=%(zaaktype_node_id)s WHERE zaaktype.id = %(id_1)s",
            20: "SELECT subject.id AS id \nFROM subject \nWHERE subject.uuid = %(uuid_1)s::UUID",
            21: "INSERT INTO logging (component, component_id, onderwerp, event_type, event_data, created_by, created_by_name_cache, restricted) VALUES (%(component)s, %(component_id)s, %(onderwerp)s, %(event_type)s, %(event_data)s, %(created_by)s, %(created_by_name_cache)s, %(restricted)s) RETURNING logging.id",
            22: "UPDATE zaaktype_node SET logging_id=%(logging_id)s WHERE zaaktype_node.id = %(id_1)s",
            23: "INSERT INTO zaaktype_status (zaaktype_node_id, status, termijn, naam, fase, ou_id, role_id, role_set) VALUES (%(zaaktype_node_id)s, %(status)s, %(termijn)s, %(naam)s, %(fase)s, %(ou_id)s, %(role_id)s, %(role_set)s) RETURNING zaaktype_status.id",
            24: "INSERT INTO zaaktype_status (zaaktype_node_id, status, termijn, naam, fase, ou_id, role_id, role_set) VALUES (%(zaaktype_node_id)s, %(status)s, %(termijn)s, %(naam)s, %(fase)s, %(ou_id)s, %(role_id)s, %(role_set)s) RETURNING zaaktype_status.id",
            25: "INSERT INTO zaaktype_status (zaaktype_node_id, status, termijn, naam, fase, ou_id, role_id, role_set) VALUES (%(zaaktype_node_id)s, %(status)s, %(termijn)s, %(naam)s, %(fase)s, %(ou_id)s, %(role_id)s, %(role_set)s) RETURNING zaaktype_status.id",
            26: "INSERT INTO zaaktype_kenmerken (zaaktype_node_id, zaak_status_id, bibliotheek_kenmerken_id, value_mandatory, label, help, pip, zaakinformatie_view, bag_zaakadres, pip_can_change, referential, is_systeemkenmerk, required_permissions, help_extern, object_metadata, label_multiple, properties, is_group) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(value_mandatory_m0)s, %(label_m0)s, %(help_m0)s, %(pip_m0)s, %(zaakinformatie_view)s, %(bag_zaakadres_m0)s, %(pip_can_change_m0)s, %(referential_m0)s, %(is_systeemkenmerk_m0)s, %(required_permissions_m0)s, %(help_extern_m0)s, %(object_metadata)s, %(label_multiple_m0)s, %(properties_m0)s, %(is_group_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_kenmerken_id_m1)s, %(value_mandatory_m1)s, %(label_m1)s, %(help_m1)s, %(pip_m1)s, %(zaakinformatie_view_m1)s, %(bag_zaakadres_m1)s, %(pip_can_change_m1)s, %(referential_m1)s, %(is_systeemkenmerk_m1)s, %(required_permissions_m1)s, %(help_extern_m1)s, %(object_metadata_m1)s, %(label_multiple_m1)s, %(properties_m1)s, %(is_group_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_kenmerken_id_m2)s, %(value_mandatory_m2)s, %(label_m2)s, %(help_m2)s, %(pip_m2)s, %(zaakinformatie_view_m2)s, %(bag_zaakadres_m2)s, %(pip_can_change_m2)s, %(referential_m2)s, %(is_systeemkenmerk_m2)s, %(required_permissions_m2)s, %(help_extern_m2)s, %(object_metadata_m2)s, %(label_multiple_m2)s, %(properties_m2)s, %(is_group_m2)s), (%(zaaktype_node_id_m3)s, %(zaak_status_id_m3)s, %(bibliotheek_kenmerken_id_m3)s, %(value_mandatory_m3)s, %(label_m3)s, %(help_m3)s, %(pip_m3)s, %(zaakinformatie_view_m3)s, %(bag_zaakadres_m3)s, %(pip_can_change_m3)s, %(referential_m3)s, %(is_systeemkenmerk_m3)s, %(required_permissions_m3)s, %(help_extern_m3)s, %(object_metadata_m3)s, %(label_multiple_m3)s, %(properties_m3)s, %(is_group_m3)s) RETURNING zaaktype_kenmerken.id, zaaktype_kenmerken.bibliotheek_kenmerken_id, zaaktype_kenmerken.properties",
            27: "INSERT INTO zaaktype_kenmerken (zaaktype_node_id, zaak_status_id, bibliotheek_kenmerken_id, value_mandatory, label, help, pip, zaakinformatie_view, bag_zaakadres, pip_can_change, referential, is_systeemkenmerk, required_permissions, help_extern, object_metadata, label_multiple, properties, is_group) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(value_mandatory_m0)s, %(label_m0)s, %(help_m0)s, %(pip_m0)s, %(zaakinformatie_view)s, %(bag_zaakadres_m0)s, %(pip_can_change_m0)s, %(referential_m0)s, %(is_systeemkenmerk_m0)s, %(required_permissions_m0)s, %(help_extern_m0)s, %(object_metadata)s, %(label_multiple_m0)s, %(properties_m0)s, %(is_group_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_kenmerken_id_m1)s, %(value_mandatory_m1)s, %(label_m1)s, %(help_m1)s, %(pip_m1)s, %(zaakinformatie_view_m1)s, %(bag_zaakadres_m1)s, %(pip_can_change_m1)s, %(referential_m1)s, %(is_systeemkenmerk_m1)s, %(required_permissions_m1)s, %(help_extern_m1)s, %(object_metadata_m1)s, %(label_multiple_m1)s, %(properties_m1)s, %(is_group_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_kenmerken_id_m2)s, %(value_mandatory_m2)s, %(label_m2)s, %(help_m2)s, %(pip_m2)s, %(zaakinformatie_view_m2)s, %(bag_zaakadres_m2)s, %(pip_can_change_m2)s, %(referential_m2)s, %(is_systeemkenmerk_m2)s, %(required_permissions_m2)s, %(help_extern_m2)s, %(object_metadata_m2)s, %(label_multiple_m2)s, %(properties_m2)s, %(is_group_m2)s), (%(zaaktype_node_id_m3)s, %(zaak_status_id_m3)s, %(bibliotheek_kenmerken_id_m3)s, %(value_mandatory_m3)s, %(label_m3)s, %(help_m3)s, %(pip_m3)s, %(zaakinformatie_view_m3)s, %(bag_zaakadres_m3)s, %(pip_can_change_m3)s, %(referential_m3)s, %(is_systeemkenmerk_m3)s, %(required_permissions_m3)s, %(help_extern_m3)s, %(object_metadata_m3)s, %(label_multiple_m3)s, %(properties_m3)s, %(is_group_m3)s), (%(zaaktype_node_id_m4)s, %(zaak_status_id_m4)s, %(bibliotheek_kenmerken_id_m4)s, %(value_mandatory_m4)s, %(label_m4)s, %(help_m4)s, %(pip_m4)s, %(zaakinformatie_view_m4)s, %(bag_zaakadres_m4)s, %(pip_can_change_m4)s, %(referential_m4)s, %(is_systeemkenmerk_m4)s, %(required_permissions_m4)s, %(help_extern_m4)s, %(object_metadata_m4)s, %(label_multiple_m4)s, %(properties_m4)s, %(is_group_m4)s), (%(zaaktype_node_id_m5)s, %(zaak_status_id_m5)s, %(bibliotheek_kenmerken_id_m5)s, %(value_mandatory_m5)s, %(label_m5)s, %(help_m5)s, %(pip_m5)s, %(zaakinformatie_view_m5)s, %(bag_zaakadres_m5)s, %(pip_can_change_m5)s, %(referential_m5)s, %(is_systeemkenmerk_m5)s, %(required_permissions_m5)s, %(help_extern_m5)s, %(object_metadata_m5)s, %(label_multiple_m5)s, %(properties_m5)s, %(is_group_m5)s), (%(zaaktype_node_id_m6)s, %(zaak_status_id_m6)s, %(bibliotheek_kenmerken_id_m6)s, %(value_mandatory_m6)s, %(label_m6)s, %(help_m6)s, %(pip_m6)s, %(zaakinformatie_view_m6)s, %(bag_zaakadres_m6)s, %(pip_can_change_m6)s, %(referential_m6)s, %(is_systeemkenmerk_m6)s, %(required_permissions_m6)s, %(help_extern_m6)s, %(object_metadata_m6)s, %(label_multiple_m6)s, %(properties_m6)s, %(is_group_m6)s), (%(zaaktype_node_id_m7)s, %(zaak_status_id_m7)s, %(bibliotheek_kenmerken_id_m7)s, %(value_mandatory_m7)s, %(label_m7)s, %(help_m7)s, %(pip_m7)s, %(zaakinformatie_view_m7)s, %(bag_zaakadres_m7)s, %(pip_can_change_m7)s, %(referential_m7)s, %(is_systeemkenmerk_m7)s, %(required_permissions_m7)s, %(help_extern_m7)s, %(object_metadata_m7)s, %(label_multiple_m7)s, %(properties_m7)s, %(is_group_m7)s), (%(zaaktype_node_id_m8)s, %(zaak_status_id_m8)s, %(bibliotheek_kenmerken_id_m8)s, %(value_mandatory_m8)s, %(label_m8)s, %(help_m8)s, %(pip_m8)s, %(zaakinformatie_view_m8)s, %(bag_zaakadres_m8)s, %(pip_can_change_m8)s, %(referential_m8)s, %(is_systeemkenmerk_m8)s, %(required_permissions_m8)s, %(help_extern_m8)s, %(object_metadata_m8)s, %(label_multiple_m8)s, %(properties_m8)s, %(is_group_m8)s), (%(zaaktype_node_id_m9)s, %(zaak_status_id_m9)s, %(bibliotheek_kenmerken_id_m9)s, %(value_mandatory_m9)s, %(label_m9)s, %(help_m9)s, %(pip_m9)s, %(zaakinformatie_view_m9)s, %(bag_zaakadres_m9)s, %(pip_can_change_m9)s, %(referential_m9)s, %(is_systeemkenmerk_m9)s, %(required_permissions_m9)s, %(help_extern_m9)s, %(object_metadata_m9)s, %(label_multiple_m9)s, %(properties_m9)s, %(is_group_m9)s), (%(zaaktype_node_id_m10)s, %(zaak_status_id_m10)s, %(bibliotheek_kenmerken_id_m10)s, %(value_mandatory_m10)s, %(label_m10)s, %(help_m10)s, %(pip_m10)s, %(zaakinformatie_view_m10)s, %(bag_zaakadres_m10)s, %(pip_can_change_m10)s, %(referential_m10)s, %(is_systeemkenmerk_m10)s, %(required_permissions_m10)s, %(help_extern_m10)s, %(object_metadata_m10)s, %(label_multiple_m10)s, %(properties_m10)s, %(is_group_m10)s), (%(zaaktype_node_id_m11)s, %(zaak_status_id_m11)s, %(bibliotheek_kenmerken_id_m11)s, %(value_mandatory_m11)s, %(label_m11)s, %(help_m11)s, %(pip_m11)s, %(zaakinformatie_view_m11)s, %(bag_zaakadres_m11)s, %(pip_can_change_m11)s, %(referential_m11)s, %(is_systeemkenmerk_m11)s, %(required_permissions_m11)s, %(help_extern_m11)s, %(object_metadata_m11)s, %(label_multiple_m11)s, %(properties_m11)s, %(is_group_m11)s), (%(zaaktype_node_id_m12)s, %(zaak_status_id_m12)s, %(bibliotheek_kenmerken_id_m12)s, %(value_mandatory_m12)s, %(label_m12)s, %(help_m12)s, %(pip_m12)s, %(zaakinformatie_view_m12)s, %(bag_zaakadres_m12)s, %(pip_can_change_m12)s, %(referential_m12)s, %(is_systeemkenmerk_m12)s, %(required_permissions_m12)s, %(help_extern_m12)s, %(object_metadata_m12)s, %(label_multiple_m12)s, %(properties_m12)s, %(is_group_m12)s), (%(zaaktype_node_id_m13)s, %(zaak_status_id_m13)s, %(bibliotheek_kenmerken_id_m13)s, %(value_mandatory_m13)s, %(label_m13)s, %(help_m13)s, %(pip_m13)s, %(zaakinformatie_view_m13)s, %(bag_zaakadres_m13)s, %(pip_can_change_m13)s, %(referential_m13)s, %(is_systeemkenmerk_m13)s, %(required_permissions_m13)s, %(help_extern_m13)s, %(object_metadata_m13)s, %(label_multiple_m13)s, %(properties_m13)s, %(is_group_m13)s), (%(zaaktype_node_id_m14)s, %(zaak_status_id_m14)s, %(bibliotheek_kenmerken_id_m14)s, %(value_mandatory_m14)s, %(label_m14)s, %(help_m14)s, %(pip_m14)s, %(zaakinformatie_view_m14)s, %(bag_zaakadres_m14)s, %(pip_can_change_m14)s, %(referential_m14)s, %(is_systeemkenmerk_m14)s, %(required_permissions_m14)s, %(help_extern_m14)s, %(object_metadata_m14)s, %(label_multiple_m14)s, %(properties_m14)s, %(is_group_m14)s), (%(zaaktype_node_id_m15)s, %(zaak_status_id_m15)s, %(bibliotheek_kenmerken_id_m15)s, %(value_mandatory_m15)s, %(label_m15)s, %(help_m15)s, %(pip_m15)s, %(zaakinformatie_view_m15)s, %(bag_zaakadres_m15)s, %(pip_can_change_m15)s, %(referential_m15)s, %(is_systeemkenmerk_m15)s, %(required_permissions_m15)s, %(help_extern_m15)s, %(object_metadata_m15)s, %(label_multiple_m15)s, %(properties_m15)s, %(is_group_m15)s), (%(zaaktype_node_id_m16)s, %(zaak_status_id_m16)s, %(bibliotheek_kenmerken_id_m16)s, %(value_mandatory_m16)s, %(label_m16)s, %(help_m16)s, %(pip_m16)s, %(zaakinformatie_view_m16)s, %(bag_zaakadres_m16)s, %(pip_can_change_m16)s, %(referential_m16)s, %(is_systeemkenmerk_m16)s, %(required_permissions_m16)s, %(help_extern_m16)s, %(object_metadata_m16)s, %(label_multiple_m16)s, %(properties_m16)s, %(is_group_m16)s), (%(zaaktype_node_id_m17)s, %(zaak_status_id_m17)s, %(bibliotheek_kenmerken_id_m17)s, %(value_mandatory_m17)s, %(label_m17)s, %(help_m17)s, %(pip_m17)s, %(zaakinformatie_view_m17)s, %(bag_zaakadres_m17)s, %(pip_can_change_m17)s, %(referential_m17)s, %(is_systeemkenmerk_m17)s, %(required_permissions_m17)s, %(help_extern_m17)s, %(object_metadata_m17)s, %(label_multiple_m17)s, %(properties_m17)s, %(is_group_m17)s), (%(zaaktype_node_id_m18)s, %(zaak_status_id_m18)s, %(bibliotheek_kenmerken_id_m18)s, %(value_mandatory_m18)s, %(label_m18)s, %(help_m18)s, %(pip_m18)s, %(zaakinformatie_view_m18)s, %(bag_zaakadres_m18)s, %(pip_can_change_m18)s, %(referential_m18)s, %(is_systeemkenmerk_m18)s, %(required_permissions_m18)s, %(help_extern_m18)s, %(object_metadata_m18)s, %(label_multiple_m18)s, %(properties_m18)s, %(is_group_m18)s), (%(zaaktype_node_id_m19)s, %(zaak_status_id_m19)s, %(bibliotheek_kenmerken_id_m19)s, %(value_mandatory_m19)s, %(label_m19)s, %(help_m19)s, %(pip_m19)s, %(zaakinformatie_view_m19)s, %(bag_zaakadres_m19)s, %(pip_can_change_m19)s, %(referential_m19)s, %(is_systeemkenmerk_m19)s, %(required_permissions_m19)s, %(help_extern_m19)s, %(object_metadata_m19)s, %(label_multiple_m19)s, %(properties_m19)s, %(is_group_m19)s), (%(zaaktype_node_id_m20)s, %(zaak_status_id_m20)s, %(bibliotheek_kenmerken_id_m20)s, %(value_mandatory_m20)s, %(label_m20)s, %(help_m20)s, %(pip_m20)s, %(zaakinformatie_view_m20)s, %(bag_zaakadres_m20)s, %(pip_can_change_m20)s, %(referential_m20)s, %(is_systeemkenmerk_m20)s, %(required_permissions_m20)s, %(help_extern_m20)s, %(object_metadata_m20)s, %(label_multiple_m20)s, %(properties_m20)s, %(is_group_m20)s), (%(zaaktype_node_id_m21)s, %(zaak_status_id_m21)s, %(bibliotheek_kenmerken_id_m21)s, %(value_mandatory_m21)s, %(label_m21)s, %(help_m21)s, %(pip_m21)s, %(zaakinformatie_view_m21)s, %(bag_zaakadres_m21)s, %(pip_can_change_m21)s, %(referential_m21)s, %(is_systeemkenmerk_m21)s, %(required_permissions_m21)s, %(help_extern_m21)s, %(object_metadata_m21)s, %(label_multiple_m21)s, %(properties_m21)s, %(is_group_m21)s), (%(zaaktype_node_id_m22)s, %(zaak_status_id_m22)s, %(bibliotheek_kenmerken_id_m22)s, %(value_mandatory_m22)s, %(label_m22)s, %(help_m22)s, %(pip_m22)s, %(zaakinformatie_view_m22)s, %(bag_zaakadres_m22)s, %(pip_can_change_m22)s, %(referential_m22)s, %(is_systeemkenmerk_m22)s, %(required_permissions_m22)s, %(help_extern_m22)s, %(object_metadata_m22)s, %(label_multiple_m22)s, %(properties_m22)s, %(is_group_m22)s), (%(zaaktype_node_id_m23)s, %(zaak_status_id_m23)s, %(bibliotheek_kenmerken_id_m23)s, %(value_mandatory_m23)s, %(label_m23)s, %(help_m23)s, %(pip_m23)s, %(zaakinformatie_view_m23)s, %(bag_zaakadres_m23)s, %(pip_can_change_m23)s, %(referential_m23)s, %(is_systeemkenmerk_m23)s, %(required_permissions_m23)s, %(help_extern_m23)s, %(object_metadata_m23)s, %(label_multiple_m23)s, %(properties_m23)s, %(is_group_m23)s), (%(zaaktype_node_id_m24)s, %(zaak_status_id_m24)s, %(bibliotheek_kenmerken_id_m24)s, %(value_mandatory_m24)s, %(label_m24)s, %(help_m24)s, %(pip_m24)s, %(zaakinformatie_view_m24)s, %(bag_zaakadres_m24)s, %(pip_can_change_m24)s, %(referential_m24)s, %(is_systeemkenmerk_m24)s, %(required_permissions_m24)s, %(help_extern_m24)s, %(object_metadata_m24)s, %(label_multiple_m24)s, %(properties_m24)s, %(is_group_m24)s), (%(zaaktype_node_id_m25)s, %(zaak_status_id_m25)s, %(bibliotheek_kenmerken_id_m25)s, %(value_mandatory_m25)s, %(label_m25)s, %(help_m25)s, %(pip_m25)s, %(zaakinformatie_view_m25)s, %(bag_zaakadres_m25)s, %(pip_can_change_m25)s, %(referential_m25)s, %(is_systeemkenmerk_m25)s, %(required_permissions_m25)s, %(help_extern_m25)s, %(object_metadata_m25)s, %(label_multiple_m25)s, %(properties_m25)s, %(is_group_m25)s), (%(zaaktype_node_id_m26)s, %(zaak_status_id_m26)s, %(bibliotheek_kenmerken_id_m26)s, %(value_mandatory_m26)s, %(label_m26)s, %(help_m26)s, %(pip_m26)s, %(zaakinformatie_view_m26)s, %(bag_zaakadres_m26)s, %(pip_can_change_m26)s, %(referential_m26)s, %(is_systeemkenmerk_m26)s, %(required_permissions_m26)s, %(help_extern_m26)s, %(object_metadata_m26)s, %(label_multiple_m26)s, %(properties_m26)s, %(is_group_m26)s), (%(zaaktype_node_id_m27)s, %(zaak_status_id_m27)s, %(bibliotheek_kenmerken_id_m27)s, %(value_mandatory_m27)s, %(label_m27)s, %(help_m27)s, %(pip_m27)s, %(zaakinformatie_view_m27)s, %(bag_zaakadres_m27)s, %(pip_can_change_m27)s, %(referential_m27)s, %(is_systeemkenmerk_m27)s, %(required_permissions_m27)s, %(help_extern_m27)s, %(object_metadata_m27)s, %(label_multiple_m27)s, %(properties_m27)s, %(is_group_m27)s), (%(zaaktype_node_id_m28)s, %(zaak_status_id_m28)s, %(bibliotheek_kenmerken_id_m28)s, %(value_mandatory_m28)s, %(label_m28)s, %(help_m28)s, %(pip_m28)s, %(zaakinformatie_view_m28)s, %(bag_zaakadres_m28)s, %(pip_can_change_m28)s, %(referential_m28)s, %(is_systeemkenmerk_m28)s, %(required_permissions_m28)s, %(help_extern_m28)s, %(object_metadata_m28)s, %(label_multiple_m28)s, %(properties_m28)s, %(is_group_m28)s), (%(zaaktype_node_id_m29)s, %(zaak_status_id_m29)s, %(bibliotheek_kenmerken_id_m29)s, %(value_mandatory_m29)s, %(label_m29)s, %(help_m29)s, %(pip_m29)s, %(zaakinformatie_view_m29)s, %(bag_zaakadres_m29)s, %(pip_can_change_m29)s, %(referential_m29)s, %(is_systeemkenmerk_m29)s, %(required_permissions_m29)s, %(help_extern_m29)s, %(object_metadata_m29)s, %(label_multiple_m29)s, %(properties_m29)s, %(is_group_m29)s), (%(zaaktype_node_id_m30)s, %(zaak_status_id_m30)s, %(bibliotheek_kenmerken_id_m30)s, %(value_mandatory_m30)s, %(label_m30)s, %(help_m30)s, %(pip_m30)s, %(zaakinformatie_view_m30)s, %(bag_zaakadres_m30)s, %(pip_can_change_m30)s, %(referential_m30)s, %(is_systeemkenmerk_m30)s, %(required_permissions_m30)s, %(help_extern_m30)s, %(object_metadata_m30)s, %(label_multiple_m30)s, %(properties_m30)s, %(is_group_m30)s), (%(zaaktype_node_id_m31)s, %(zaak_status_id_m31)s, %(bibliotheek_kenmerken_id_m31)s, %(value_mandatory_m31)s, %(label_m31)s, %(help_m31)s, %(pip_m31)s, %(zaakinformatie_view_m31)s, %(bag_zaakadres_m31)s, %(pip_can_change_m31)s, %(referential_m31)s, %(is_systeemkenmerk_m31)s, %(required_permissions_m31)s, %(help_extern_m31)s, %(object_metadata_m31)s, %(label_multiple_m31)s, %(properties_m31)s, %(is_group_m31)s), (%(zaaktype_node_id_m32)s, %(zaak_status_id_m32)s, %(bibliotheek_kenmerken_id_m32)s, %(value_mandatory_m32)s, %(label_m32)s, %(help_m32)s, %(pip_m32)s, %(zaakinformatie_view_m32)s, %(bag_zaakadres_m32)s, %(pip_can_change_m32)s, %(referential_m32)s, %(is_systeemkenmerk_m32)s, %(required_permissions_m32)s, %(help_extern_m32)s, %(object_metadata_m32)s, %(label_multiple_m32)s, %(properties_m32)s, %(is_group_m32)s), (%(zaaktype_node_id_m33)s, %(zaak_status_id_m33)s, %(bibliotheek_kenmerken_id_m33)s, %(value_mandatory_m33)s, %(label_m33)s, %(help_m33)s, %(pip_m33)s, %(zaakinformatie_view_m33)s, %(bag_zaakadres_m33)s, %(pip_can_change_m33)s, %(referential_m33)s, %(is_systeemkenmerk_m33)s, %(required_permissions_m33)s, %(help_extern_m33)s, %(object_metadata_m33)s, %(label_multiple_m33)s, %(properties_m33)s, %(is_group_m33)s), (%(zaaktype_node_id_m34)s, %(zaak_status_id_m34)s, %(bibliotheek_kenmerken_id_m34)s, %(value_mandatory_m34)s, %(label_m34)s, %(help_m34)s, %(pip_m34)s, %(zaakinformatie_view_m34)s, %(bag_zaakadres_m34)s, %(pip_can_change_m34)s, %(referential_m34)s, %(is_systeemkenmerk_m34)s, %(required_permissions_m34)s, %(help_extern_m34)s, %(object_metadata_m34)s, %(label_multiple_m34)s, %(properties_m34)s, %(is_group_m34)s), (%(zaaktype_node_id_m35)s, %(zaak_status_id_m35)s, %(bibliotheek_kenmerken_id_m35)s, %(value_mandatory_m35)s, %(label_m35)s, %(help_m35)s, %(pip_m35)s, %(zaakinformatie_view_m35)s, %(bag_zaakadres_m35)s, %(pip_can_change_m35)s, %(referential_m35)s, %(is_systeemkenmerk_m35)s, %(required_permissions_m35)s, %(help_extern_m35)s, %(object_metadata_m35)s, %(label_multiple_m35)s, %(properties_m35)s, %(is_group_m35)s), (%(zaaktype_node_id_m36)s, %(zaak_status_id_m36)s, %(bibliotheek_kenmerken_id_m36)s, %(value_mandatory_m36)s, %(label_m36)s, %(help_m36)s, %(pip_m36)s, %(zaakinformatie_view_m36)s, %(bag_zaakadres_m36)s, %(pip_can_change_m36)s, %(referential_m36)s, %(is_systeemkenmerk_m36)s, %(required_permissions_m36)s, %(help_extern_m36)s, %(object_metadata_m36)s, %(label_multiple_m36)s, %(properties_m36)s, %(is_group_m36)s), (%(zaaktype_node_id_m37)s, %(zaak_status_id_m37)s, %(bibliotheek_kenmerken_id_m37)s, %(value_mandatory_m37)s, %(label_m37)s, %(help_m37)s, %(pip_m37)s, %(zaakinformatie_view_m37)s, %(bag_zaakadres_m37)s, %(pip_can_change_m37)s, %(referential_m37)s, %(is_systeemkenmerk_m37)s, %(required_permissions_m37)s, %(help_extern_m37)s, %(object_metadata_m37)s, %(label_multiple_m37)s, %(properties_m37)s, %(is_group_m37)s), (%(zaaktype_node_id_m38)s, %(zaak_status_id_m38)s, %(bibliotheek_kenmerken_id_m38)s, %(value_mandatory_m38)s, %(label_m38)s, %(help_m38)s, %(pip_m38)s, %(zaakinformatie_view_m38)s, %(bag_zaakadres_m38)s, %(pip_can_change_m38)s, %(referential_m38)s, %(is_systeemkenmerk_m38)s, %(required_permissions_m38)s, %(help_extern_m38)s, %(object_metadata_m38)s, %(label_multiple_m38)s, %(properties_m38)s, %(is_group_m38)s), (%(zaaktype_node_id_m39)s, %(zaak_status_id_m39)s, %(bibliotheek_kenmerken_id_m39)s, %(value_mandatory_m39)s, %(label_m39)s, %(help_m39)s, %(pip_m39)s, %(zaakinformatie_view_m39)s, %(bag_zaakadres_m39)s, %(pip_can_change_m39)s, %(referential_m39)s, %(is_systeemkenmerk_m39)s, %(required_permissions_m39)s, %(help_extern_m39)s, %(object_metadata_m39)s, %(label_multiple_m39)s, %(properties_m39)s, %(is_group_m39)s), (%(zaaktype_node_id_m40)s, %(zaak_status_id_m40)s, %(bibliotheek_kenmerken_id_m40)s, %(value_mandatory_m40)s, %(label_m40)s, %(help_m40)s, %(pip_m40)s, %(zaakinformatie_view_m40)s, %(bag_zaakadres_m40)s, %(pip_can_change_m40)s, %(referential_m40)s, %(is_systeemkenmerk_m40)s, %(required_permissions_m40)s, %(help_extern_m40)s, %(object_metadata_m40)s, %(label_multiple_m40)s, %(properties_m40)s, %(is_group_m40)s), (%(zaaktype_node_id_m41)s, %(zaak_status_id_m41)s, %(bibliotheek_kenmerken_id_m41)s, %(value_mandatory_m41)s, %(label_m41)s, %(help_m41)s, %(pip_m41)s, %(zaakinformatie_view_m41)s, %(bag_zaakadres_m41)s, %(pip_can_change_m41)s, %(referential_m41)s, %(is_systeemkenmerk_m41)s, %(required_permissions_m41)s, %(help_extern_m41)s, %(object_metadata_m41)s, %(label_multiple_m41)s, %(properties_m41)s, %(is_group_m41)s), (%(zaaktype_node_id_m42)s, %(zaak_status_id_m42)s, %(bibliotheek_kenmerken_id_m42)s, %(value_mandatory_m42)s, %(label_m42)s, %(help_m42)s, %(pip_m42)s, %(zaakinformatie_view_m42)s, %(bag_zaakadres_m42)s, %(pip_can_change_m42)s, %(referential_m42)s, %(is_systeemkenmerk_m42)s, %(required_permissions_m42)s, %(help_extern_m42)s, %(object_metadata_m42)s, %(label_multiple_m42)s, %(properties_m42)s, %(is_group_m42)s), (%(zaaktype_node_id_m43)s, %(zaak_status_id_m43)s, %(bibliotheek_kenmerken_id_m43)s, %(value_mandatory_m43)s, %(label_m43)s, %(help_m43)s, %(pip_m43)s, %(zaakinformatie_view_m43)s, %(bag_zaakadres_m43)s, %(pip_can_change_m43)s, %(referential_m43)s, %(is_systeemkenmerk_m43)s, %(required_permissions_m43)s, %(help_extern_m43)s, %(object_metadata_m43)s, %(label_multiple_m43)s, %(properties_m43)s, %(is_group_m43)s) RETURNING zaaktype_kenmerken.id, zaaktype_kenmerken.bibliotheek_kenmerken_id, zaaktype_kenmerken.properties",
            28: "UPDATE zaaktype_kenmerken SET properties=%(properties)s WHERE zaaktype_kenmerken.id = %(id_1)s",
            29: "UPDATE zaaktype_kenmerken SET properties=%(properties)s WHERE zaaktype_kenmerken.id = %(id_1)s",
            30: "INSERT INTO zaaktype_resultaten (zaaktype_node_id, zaaktype_status_id, resultaat, ingang, bewaartermijn, label, selectielijst, archiefnominatie, comments, trigger_archival, selectielijst_brondatum, selectielijst_einddatum, properties, standaard_keuze) VALUES (%(zaaktype_node_id_m0)s, %(zaaktype_status_id_m0)s, %(resultaat_m0)s, %(ingang_m0)s, %(bewaartermijn_m0)s, %(label_m0)s, %(selectielijst_m0)s, %(archiefnominatie_m0)s, %(comments_m0)s, %(trigger_archival_m0)s, %(selectielijst_brondatum_m0)s, %(selectielijst_einddatum_m0)s, %(properties_m0)s, %(standaard_keuze_m0)s)",
            31: "INSERT INTO zaaktype_status_checklist_item (casetype_status_id, label) VALUES (%(casetype_status_id_m0)s, %(label_m0)s), (%(casetype_status_id_m1)s, %(label_m1)s), (%(casetype_status_id_m2)s, %(label_m2)s)",
            32: "INSERT INTO zaaktype_sjablonen (zaaktype_node_id, zaak_status_id, bibliotheek_sjablonen_id, bibliotheek_kenmerken_id, automatisch_genereren, target_format, help, custom_filename, label, allow_edit) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_sjablonen_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(automatisch_genereren_m0)s, %(target_format_m0)s, %(help_m0)s, %(custom_filename_m0)s, %(label_m0)s, %(allow_edit_m0)s)",
            33: "INSERT INTO zaaktype_sjablonen (zaaktype_node_id, zaak_status_id, bibliotheek_sjablonen_id, bibliotheek_kenmerken_id, automatisch_genereren, target_format, help, custom_filename, label, allow_edit) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_sjablonen_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(automatisch_genereren_m0)s, %(target_format_m0)s, %(help_m0)s, %(custom_filename_m0)s, %(label_m0)s, %(allow_edit_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_sjablonen_id_m1)s, %(bibliotheek_kenmerken_id_m1)s, %(automatisch_genereren_m1)s, %(target_format_m1)s, %(help_m1)s, %(custom_filename_m1)s, %(label_m1)s, %(allow_edit_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_sjablonen_id_m2)s, %(bibliotheek_kenmerken_id_m2)s, %(automatisch_genereren_m2)s, %(target_format_m2)s, %(help_m2)s, %(custom_filename_m2)s, %(label_m2)s, %(allow_edit_m2)s)",
            34: "SELECT subject.id AS id \nFROM subject \nWHERE subject.uuid = %(uuid_1)s::UUID",
            35: "INSERT INTO zaaktype_notificatie (zaaktype_node_id, zaak_status_id, bibliotheek_notificaties_id, rcpt, email, behandelaar, automatic, cc, bcc, betrokkene_role) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_notificaties_id_m0)s, %(rcpt_m0)s, %(email_m0)s, %(behandelaar_m0)s, %(automatic_m0)s, %(cc_m0)s, %(bcc_m0)s, %(betrokkene_role_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_notificaties_id_m1)s, %(rcpt_m1)s, %(email_m1)s, %(behandelaar_m1)s, %(automatic_m1)s, %(cc_m1)s, %(bcc_m1)s, %(betrokkene_role_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_notificaties_id_m2)s, %(rcpt_m2)s, %(email_m2)s, %(behandelaar_m2)s, %(automatic_m2)s, %(cc_m2)s, %(bcc_m2)s, %(betrokkene_role_m2)s), (%(zaaktype_node_id_m3)s, %(zaak_status_id_m3)s, %(bibliotheek_notificaties_id_m3)s, %(rcpt_m3)s, %(email_m3)s, %(behandelaar_m3)s, %(automatic_m3)s, %(cc_m3)s, %(bcc_m3)s, %(betrokkene_role_m3)s), (%(zaaktype_node_id_m4)s, %(zaak_status_id_m4)s, %(bibliotheek_notificaties_id_m4)s, %(rcpt_m4)s, %(email_m4)s, %(behandelaar_m4)s, %(automatic_m4)s, %(cc_m4)s, %(bcc_m4)s, %(betrokkene_role_m4)s), (%(zaaktype_node_id_m5)s, %(zaak_status_id_m5)s, %(bibliotheek_notificaties_id_m5)s, %(rcpt_m5)s, %(email_m5)s, %(behandelaar_m5)s, %(automatic_m5)s, %(cc_m5)s, %(bcc_m5)s, %(betrokkene_role_m5)s), (%(zaaktype_node_id_m6)s, %(zaak_status_id_m6)s, %(bibliotheek_notificaties_id_m6)s, %(rcpt_m6)s, %(email_m6)s, %(behandelaar_m6)s, %(automatic_m6)s, %(cc_m6)s, %(bcc_m6)s, %(betrokkene_role_m6)s)",
            36: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            37: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            38: "INSERT INTO zaaktype_relatie (zaaktype_node_id, zaaktype_status_id, relatie_zaaktype_id, relatie_type, eigenaar_type, start_delay, status, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, subject_role, copy_subject_role, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_role, eigenaar_id, show_in_pip, pip_label, copy_related_cases, copy_related_objects, copy_selected_attributes, creation_style) VALUES (%(zaaktype_node_id_m0)s, %(zaaktype_status_id_m0)s, %(relatie_zaaktype_id_m0)s, %(relatie_type_m0)s, %(eigenaar_type_m0)s, %(start_delay_m0)s, %(status_m0)s, %(kopieren_kenmerken_m0)s, %(ou_id_m0)s, %(role_id_m0)s, %(automatisch_behandelen_m0)s, %(required_m0)s, %(subject_role_m0)s::VARCHAR[], %(copy_subject_role_m0)s, %(betrokkene_authorized_m0)s, %(betrokkene_notify_m0)s, %(betrokkene_id_m0)s, %(betrokkene_role_m0)s, %(betrokkene_role_set_m0)s, %(betrokkene_prefix_m0)s, %(eigenaar_role_m0)s, %(eigenaar_id_m0)s, %(show_in_pip_m0)s, %(pip_label_m0)s, %(copy_related_cases_m0)s, %(copy_related_objects_m0)s, %(copy_selected_attributes_m0)s, %(creation_style_m0)s)",
            39: "INSERT INTO zaaktype_relatie (zaaktype_node_id, zaaktype_status_id, relatie_zaaktype_id, relatie_type, eigenaar_type, start_delay, status, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, subject_role, copy_subject_role, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_role, eigenaar_id, show_in_pip, pip_label, copy_related_cases, copy_related_objects, copy_selected_attributes, creation_style) VALUES (%(zaaktype_node_id_m0)s, %(zaaktype_status_id_m0)s, %(relatie_zaaktype_id_m0)s, %(relatie_type_m0)s, %(eigenaar_type_m0)s, %(start_delay_m0)s, %(status_m0)s, %(kopieren_kenmerken_m0)s, %(ou_id_m0)s, %(role_id_m0)s, %(automatisch_behandelen_m0)s, %(required_m0)s, %(subject_role_m0)s::VARCHAR[], %(copy_subject_role_m0)s, %(betrokkene_authorized_m0)s, %(betrokkene_notify_m0)s, %(betrokkene_id_m0)s, %(betrokkene_role_m0)s, %(betrokkene_role_set_m0)s, %(betrokkene_prefix_m0)s, %(eigenaar_role_m0)s, %(eigenaar_id_m0)s, %(show_in_pip_m0)s, %(pip_label_m0)s, %(copy_related_cases_m0)s, %(copy_related_objects_m0)s, %(copy_selected_attributes_m0)s, %(creation_style_m0)s), (%(zaaktype_node_id_m1)s, %(zaaktype_status_id_m1)s, %(relatie_zaaktype_id_m1)s, %(relatie_type_m1)s, %(eigenaar_type_m1)s, %(start_delay_m1)s, %(status_m1)s, %(kopieren_kenmerken_m1)s, %(ou_id_m1)s, %(role_id_m1)s, %(automatisch_behandelen_m1)s, %(required_m1)s, %(subject_role_m1)s::VARCHAR[], %(copy_subject_role_m1)s, %(betrokkene_authorized_m1)s, %(betrokkene_notify_m1)s, %(betrokkene_id_m1)s, %(betrokkene_role_m1)s, %(betrokkene_role_set_m1)s, %(betrokkene_prefix_m1)s, %(eigenaar_role_m1)s, %(eigenaar_id_m1)s, %(show_in_pip_m1)s, %(pip_label_m1)s, %(copy_related_cases_m1)s, %(copy_related_objects_m1)s, %(copy_selected_attributes_m1)s, %(creation_style_m1)s)",
            40: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            41: "SELECT bedrijf.id AS id \nFROM bedrijf \nWHERE bedrijf.uuid = %(uuid_1)s::UUID",
            42: "INSERT INTO zaaktype_standaard_betrokkenen (zaaktype_node_id, zaak_status_id, betrokkene_type, betrokkene_identifier, naam, rol, magic_string_prefix, gemachtigd, notify) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(betrokkene_type_m0)s, %(betrokkene_identifier_m0)s, %(naam_m0)s, %(rol_m0)s, %(magic_string_prefix_m0)s, %(gemachtigd_m0)s, %(notify_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(betrokkene_type_m1)s, %(betrokkene_identifier_m1)s, %(naam_m1)s, %(rol_m1)s, %(magic_string_prefix_m1)s, %(gemachtigd_m1)s, %(notify_m1)s)",
            43: "UPDATE zaaktype_node SET id=%(id)s WHERE zaaktype_node.id = %(id_1)s",
        }

        assert len(call_list) == 44
        for query_index in range(44):
            compiled_query = str(
                object=call_list[query_index][0][0].compile(
                    dialect=postgresql.dialect()
                )
            )
            assert compiled_query == queries[query_index]

        # assert tempaltes
        assert_templates_exist = call_list[6][0][0].compile().params
        assert set(assert_templates_exist["uuid_1"]) == set(
            [
                UUID("9a278be5-4717-4e91-884b-7295fc7136d5"),
                UUID("adf99a19-2348-4af6-907b-38bf9b16bf84"),
                UUID("3cc1a024-5f3b-4ae6-96c0-2c5a1b83ad4c"),
            ]
        )

        # assert casetype_definitie
        insert_casetype_definitie = call_list[12][0][0].compile().params
        assert insert_casetype_definitie["servicenorm"] == 11113
        assert insert_casetype_definitie["afhandeltermijn"] == "06-09-2023"

        # assert casetype_node_properties
        insert_casetype_node = call_list[15][0][0].compile().params
        assert insert_casetype_node["properties"] == {
            "aanleiding": "aanleiding 11111",
            "doel": "doel 11111",
            "archiefclassicatiecode": "archiefclassificatiecode 11111",
            "vertrouwelijkheidsaanduiding": "Openbaar",
            "verantwoordelijke": "verantwoordelijke 11111",
            "verantwoordingsrelatie": "verantwoordingsrelatie 11111",
            "beroep_mogelijk": "Nee",
            "publicatie": "Nee",
            "publicatietekst": "publicatietekst 11111",
            "bag": "Nee",
            "lex_silencio_positivo": "Nee",
            "opschorten_mogelijk": "Nee",
            "verlenging_mogelijk": "Nee",
            "verdagingstermijn": 11111,
            "wet_dwangsom": "Nee",
            "wkpb": "Nee",
            "e_formulier": "eformulier 11111",
            "lokale_grondslag": "lokale grondslag 11111",
            "gdpr": {
                "enabled": "Nee",
                "processing_type": "Delen",
                "process_foreign_country": "Nee",
                "process_foreign_country_reason": "toelichting doorgifte buitenland 11111",
                "processing_legal": "Wettelijke verplichting",
                "processing_legal_reason": "toelichting verwerking 11111",
                "personal": {
                    "basic_details": "on",
                    "personal_id_number": "on",
                    "income": "on",
                    "race_or_ethniticy": "on",
                    "political_views": "on",
                    "religion": "on",
                    "membership_union": "on",
                    "genetic_or_biometric_data": "on",
                    "sexual_identity": "on",
                    "criminal_record": "on",
                    "offspring": "on",
                },
                "source_personal": {"registration": "on"},
            },
            "preset_owner_identifier": mock.ANY,
            "public_confirmation_message": "Bedankt voor het [[case.casetype.initiator_type]] van een <strong>[[case.casetype.name]]</strong>. Uw registratie is bij ons bekend onder <strong>zaaknummer [[case.number]]</strong>. Wij verzoeken u om bij verdere communicatie dit zaaknummer te gebruiken. De behandeling van deze zaak zal spoedig plaatsvinden.",
            "public_confirmation_title": "Uw zaak is geregistreerd",
            "case_location_message": "tekst bij adrescontrole-11111",
            "pip_view_message": "Ook kunt u op elk moment van de dag de voortgang en inhoud inzien via de persoonlijke internetpagina.",
            "delayed": True,
            "case_location_check": True,
            "offline_betaling": True,
            "no_captcha": True,
            "pdf_registration_form": True,
            "pdc_tarief_balie": "24.01",
            "pdc_tarief_telefoon": "25.99",
            "pdc_tarief_email": "26.0",
            "pdc_tarief_behandelaar": "27",
            "pdc_tarief_post": "28",
            "pdc_tarief_chat": "1",
            "pdc_tarief_overige": "15.9",
            "confidentiality": True,
            "lock_registration_phase": True,
            "queue_coworker_changes": True,
            "allow_external_task_assignment": True,
            "default_directories": [
                "documentmap-11111",
                "documentmap 2 default",
            ],
            "api_can_transition": True,
            "notify_on_new_case": True,
            "notify_on_new_document": True,
            "notify_on_new_message": True,
            "notify_on_exceed_term": True,
            "notify_on_allocate_case": True,
            "notify_on_phase_transition": True,
            "notify_on_task_change": True,
            "notify_on_label_change": True,
            "notify_on_subject_change": True,
            "child_casetype_settings": {
                "enabled": True,
                "child_casetypes": [
                    {
                        "enabled": True,
                        "casetype": {
                            "uuid": "2c40ea6c-a3d3-4685-bfc9-cba7d1eab7a1",
                            "name": "Name of casetype with id 1",
                        },
                        "settings": {
                            "api": "false",
                            "case": "false",
                            "webform": "false",
                            "registrationform": "false",
                            "results": "true",
                            "relations": "false",
                            "last_phase": "false",
                            "allocations": "false",
                            "first_phase": "false",
                            "middle_phases": "[2]",
                            "notifications": "false",
                            "authorizations": "false",
                            "middle_phases2": "false",
                        },
                    },
                    {
                        "enabled": True,
                        "casetype": {
                            "uuid": "a486807c-0e76-4145-807c-c794501f622b",
                            "name": "Name of casetype with id 3",
                        },
                        "settings": {
                            "api": "false",
                            "case": "false",
                            "webform": "false",
                            "registrationform": "false",
                            "results": "false",
                            "relations": "true",
                            "last_phase": "false",
                            "allocations": "false",
                            "first_phase": "false",
                            "middle_phases": "[2]",
                            "notifications": "false",
                            "authorizations": "false",
                            "middle_phases2": "false",
                        },
                    },
                ],
            },
        }

        # assert properties inserted are correct, not checking all types of
        # properties, they all work kind of the same
        insert_kenmerken_params_phase_1 = call_list[26][0][0].compile().params
        assert insert_kenmerken_params_phase_1["zaaktype_node_id_m2"] == 11112
        assert insert_kenmerken_params_phase_1["zaak_status_id_m2"] == 11115

        assert insert_kenmerken_params_phase_1["properties_m2"] == {
            "skip_change_approval": 0,
            "custom_object": {
                "create": {"enabled": 0, "label": None},
                "attributes": None,
                "edit": {"enabled": 0},
                "delete": {"enabled": 0},
            },
            "relationship_subject_role": None,
            "show_on_map": 0,
            "map_wms_layer_id": None,
            "map_wms_feature_attribute_label": None,
            "map_wms_feature_attribute_id": None,
            "map_case_location": 0,
            "date_limit": {
                "start": {
                    "active": 0,
                    "num": 2,
                    "term": "days",
                    "during": "post",
                },
                "end": {
                    "active": 0,
                    "num": 3,
                    "term": "weeks",
                    "during": "pre",
                },
            },
        }

        insert_kenmerken_params_phase_2 = call_list[27][0][0].compile().params
        assert insert_kenmerken_params_phase_2["required_permissions_m4"] == {
            "selectedUnits": [
                {
                    "role_id": "1",
                    "role_name": "Afdelingshoofd",
                    "org_unit_id": "3",
                    "org_unit_name": "department3",
                },
                {
                    "role_id": "3",
                    "role_name": "Persoonsverwerker",
                    "org_unit_id": "1",
                    "org_unit_name": "department2",
                },
                {
                    "role_id": "2",
                    "role_name": "BRP externe bevrager",
                    "org_unit_id": "1",
                    "org_unit_name": "department2",
                },
            ]
        }

        # test update of properties for date attribute related to reference.
        assert call_list[28][0][0].compile().params["properties"] == {
            "date_limit": {
                "start": {
                    "active": 1,
                    "num": 2,
                    "term": "days",
                    "during": "post",
                    "reference": 5,
                },
                "end": {
                    "active": 1,
                    "num": 3,
                    "term": "weeks",
                    "during": "pre",
                    "reference": 5,
                },
            }
        }

        insert_kenmerken_params_phase2 = call_list[27][0][0].compile().params
        locatie_met_kaart_props = insert_kenmerken_params_phase2[
            "properties_m22"
        ]
        assert locatie_met_kaart_props["map_wms_feature_attribute_id"] == 19
        assert (
            locatie_met_kaart_props["map_wms_feature_attribute_label"]
            == "kenmerk_enkelvoudigekeuze"
        )

        # assert insert result
        casetype_tasks = call_list[30][0][0].compile().params
        assert casetype_tasks["zaaktype_status_id_m0"] == 11117
        assert casetype_tasks["zaaktype_node_id_m0"] == 11112
        assert casetype_tasks["resultaat_m0"] == "aangegaan"
        assert casetype_tasks["label_m0"] == "name for result"
        assert casetype_tasks["properties_m0"] == {
            "selectielijst_nummer": "123",
            "procestype_nummer": "123",
            "procestype_naam": "processtype name for result",
            "procestype_omschrijving": "processtype description",
            "procestype_toelichting": "processtype explanation",
            "procestype_object": "processtype object",
            "procestype_generiek": "Specifiek",
            "herkomst": "Trendanalyse",
            "procestermijn": "D",
        }
        # assert insert tasks
        casetype_tasks = call_list[31][0][0].compile().params
        assert casetype_tasks["casetype_status_id_m0"] == 11116
        assert casetype_tasks["label_m0"] == "task_a"
        assert casetype_tasks["label_m1"] == "task_b"
        assert casetype_tasks["label_m2"] == "task_c"

        # assert insert templates
        templates_phase1 = call_list[32][0][0].compile().params
        assert templates_phase1["zaaktype_node_id_m0"] == 11112
        assert templates_phase1["zaak_status_id_m0"] == 11115
        assert templates_phase1["bibliotheek_sjablonen_id_m0"] == 1
        assert templates_phase1["bibliotheek_kenmerken_id_m0"] == 17
        assert templates_phase1["automatisch_genereren_m0"] == 1
        assert templates_phase1["target_format_m0"] == "odt"
        assert templates_phase1["help_m0"] == "toelichting fase 1 odt"
        assert templates_phase1["custom_filename_m0"] == "bestandsnaam.odt"
        assert templates_phase1["label_m0"] == "label fase 1 odt"
        assert templates_phase1["allow_edit_m0"]

        templates_phase2 = call_list[33][0][0].compile().params
        assert templates_phase2["zaaktype_node_id_m0"] == 11112
        assert templates_phase2["zaak_status_id_m0"] == 11116
        assert templates_phase2["bibliotheek_sjablonen_id_m0"] == 2
        assert templates_phase2["bibliotheek_kenmerken_id_m0"] == 17
        assert templates_phase2["automatisch_genereren_m0"] == 1
        assert templates_phase2["target_format_m0"] == "odt"
        assert templates_phase2["help_m0"] == "toelichting odt"
        assert templates_phase2["custom_filename_m0"] == "bestandsnaam.odt"
        assert templates_phase2["label_m0"] == "label odt"
        assert templates_phase2["allow_edit_m0"]

        assert templates_phase2["zaaktype_node_id_m1"] == 11112
        assert templates_phase2["zaak_status_id_m1"] == 11116
        assert templates_phase2["bibliotheek_sjablonen_id_m1"] == 2
        assert templates_phase2["bibliotheek_kenmerken_id_m1"] is None
        assert templates_phase2["automatisch_genereren_m1"] is None
        assert templates_phase2["target_format_m1"] == "pdf"
        assert templates_phase2["help_m1"] == "toelichting pdf"
        assert templates_phase2["custom_filename_m1"] == "file"
        assert templates_phase2["label_m1"] == "label pdf"
        assert not templates_phase2["allow_edit_m1"]

        assert templates_phase2["zaaktype_node_id_m2"] == 11112
        assert templates_phase2["zaak_status_id_m2"] == 11116
        assert templates_phase2["bibliotheek_sjablonen_id_m2"] == 3
        assert templates_phase2["bibliotheek_kenmerken_id_m2"] is None
        assert templates_phase2["automatisch_genereren_m2"] == 1
        assert templates_phase2["target_format_m2"] == "docx"
        assert templates_phase2["help_m2"] == "toelichting abc"
        assert templates_phase2["custom_filename_m2"] == "work_document"
        assert templates_phase2["label_m2"] == "label docx"
        assert not templates_phase2["allow_edit_m2"]

        email_templates_phase2 = call_list[35][0][0].compile().params
        key_values = {
            "zaaktype_node_id_m0": 11112,
            "zaak_status_id_m0": 11116,
            "bibliotheek_notificaties_id_m0": 1,
            "rcpt_m0": "aanvrager",
            "email_m0": "",
            "behandelaar_m0": None,
            "automatic_m0": 0,
            "cc_m0": "cc@test.nl",
            "bcc_m0": "bcc@test.nl",
            "betrokkene_role_m0": None,
            "zaaktype_node_id_m1": 11112,
            "zaak_status_id_m1": 11116,
            "bibliotheek_notificaties_id_m1": 1,
            "rcpt_m1": "zaak_behandelaar",
            "email_m1": "",
            "behandelaar_m1": None,
            "automatic_m1": 0,
            "cc_m1": "",
            "bcc_m1": "",
            "betrokkene_role_m1": None,
            "zaaktype_node_id_m2": 11112,
            "zaak_status_id_m2": 11116,
            "bibliotheek_notificaties_id_m2": 1,
            "rcpt_m2": "behandelaar",
            "email_m2": "",
            "behandelaar_m2": "betrokkene-medewerker-34",
            "automatic_m2": 0,
            "cc_m2": "cc@test",
            "bcc_m2": "bcc@test.nl",
            "betrokkene_role_m2": None,
            "zaaktype_node_id_m3": 11112,
            "zaak_status_id_m3": 11116,
            "bibliotheek_notificaties_id_m3": 1,
            "rcpt_m3": "coordinator",
            "email_m3": "",
            "behandelaar_m3": None,
            "automatic_m3": 0,
            "cc_m3": "",
            "bcc_m3": "",
            "betrokkene_role_m3": None,
            "zaaktype_node_id_m4": 11112,
            "zaak_status_id_m4": 11116,
            "bibliotheek_notificaties_id_m4": 1,
            "rcpt_m4": "gemachtigde",
            "email_m4": "",
            "behandelaar_m4": None,
            "automatic_m4": 0,
            "cc_m4": "",
            "bcc_m4": "",
            "betrokkene_role_m4": None,
            "zaaktype_node_id_m5": 11112,
            "zaak_status_id_m5": 11116,
            "bibliotheek_notificaties_id_m5": 1,
            "rcpt_m5": "betrokkene",
            "email_m5": "",
            "behandelaar_m5": None,
            "automatic_m5": 0,
            "cc_m5": "",
            "bcc_m5": "",
            "betrokkene_role_m5": "Ouder",
            "zaaktype_node_id_m6": 11112,
            "zaak_status_id_m6": 11116,
            "bibliotheek_notificaties_id_m6": 1,
            "rcpt_m6": "overig",
            "email_m6": "email@test.nl",
            "behandelaar_m6": None,
            "automatic_m6": 0,
            "cc_m6": "",
            "bcc_m6": "",
            "betrokkene_role_m6": None,
        }
        for key, value in key_values.items():
            assert email_templates_phase2[key] == value

        zaaktype_relatie_phase1_params = call_list[38][0][0].compile().params
        zaaktype_relatie_phase1_params_expected = {
            "zaaktype_node_id_m0": 11112,
            "zaaktype_status_id_m0": 11115,
            "relatie_zaaktype_id_m0": 20,
            "relatie_type_m0": "deelzaak",
            "eigenaar_type_m0": "aanvrager",
            "start_delay_m0": None,
            "status_m0": False,
            "kopieren_kenmerken_m0": 0,
            "ou_id_m0": 1,
            "role_id_m0": 5,
            "automatisch_behandelen_m0": True,
            "required_m0": 2,
            "subject_role_m0": [
                "Advocaat",
                "Aannemer",
                "Gemachtigde",
                "Mantelzorger",
            ],
            "copy_subject_role_m0": 1,
            "betrokkene_authorized_m0": True,
            "betrokkene_notify_m0": 0,
            "betrokkene_id_m0": "betrokkene-natuurlijk_persoon-20000",
            "betrokkene_role_m0": "Advocaat",
            "betrokkene_role_set_m0": 1,
            "betrokkene_prefix_m0": "advocaat",
            "eigenaar_role_m0": None,
            "eigenaar_id_m0": "betrokkene-natuurlijk_persoon-5678",
            "show_in_pip_m0": True,
            "pip_label_m0": "label op pip",
            "copy_related_cases_m0": True,
            "copy_related_objects_m0": True,
            "copy_selected_attributes_m0": {
                "kenmerk_datum": "kenmerk_datum",
                "kenmerk_tekstfield": "kenmerk_tekstfield",
            },
            "creation_style_m0": "background",
        }
        assert (
            zaaktype_relatie_phase1_params
            == zaaktype_relatie_phase1_params_expected
        )

        zaaktype_relatie_phase3_params = call_list[39][0][0].compile().params
        zaaktype_relatie_phase3_params_expected = {
            "zaaktype_node_id_m0": 11112,
            "zaaktype_status_id_m0": 11117,
            "relatie_zaaktype_id_m0": 20,
            "relatie_type_m0": "vervolgzaak_datum",
            "eigenaar_type_m0": "aanvrager",
            "start_delay_m0": "31-01-2025",
            "status_m0": False,
            "kopieren_kenmerken_m0": 0,
            "ou_id_m0": 1,
            "role_id_m0": 5,
            "automatisch_behandelen_m0": False,
            "required_m0": 3,
            "subject_role_m0": None,
            "copy_subject_role_m0": 0,
            "betrokkene_authorized_m0": False,
            "betrokkene_notify_m0": 0,
            "betrokkene_id_m0": None,
            "betrokkene_role_m0": "Advocaat",
            "betrokkene_role_set_m0": 0,
            "betrokkene_prefix_m0": "",
            "eigenaar_role_m0": None,
            "eigenaar_id_m0": None,
            "show_in_pip_m0": False,
            "pip_label_m0": None,
            "copy_related_cases_m0": False,
            "copy_related_objects_m0": False,
            "copy_selected_attributes_m0": {},
            "creation_style_m0": "background",
            "zaaktype_node_id_m1": 11112,
            "zaaktype_status_id_m1": 11117,
            "relatie_zaaktype_id_m1": 21,
            "relatie_type_m1": "vervolgzaak",
            "eigenaar_type_m1": "aanvrager",
            "start_delay_m1": 12,
            "status_m1": False,
            "kopieren_kenmerken_m1": 0,
            "ou_id_m1": 1,
            "role_id_m1": 5,
            "automatisch_behandelen_m1": False,
            "required_m1": 3,
            "subject_role_m1": None,
            "copy_subject_role_m1": 0,
            "betrokkene_authorized_m1": False,
            "betrokkene_notify_m1": 0,
            "betrokkene_id_m1": None,
            "betrokkene_role_m1": "Advocaat",
            "betrokkene_role_set_m1": 0,
            "betrokkene_prefix_m1": "",
            "eigenaar_role_m1": None,
            "eigenaar_id_m1": None,
            "show_in_pip_m1": False,
            "pip_label_m1": None,
            "copy_related_cases_m1": False,
            "copy_related_objects_m1": False,
            "copy_selected_attributes_m1": {},
            "creation_style_m1": "background",
        }
        assert (
            zaaktype_relatie_phase3_params
            == zaaktype_relatie_phase3_params_expected
        )

        zaaktype_betrokkenen_phase1_params = (
            call_list[42][0][0].compile().params
        )
        zaaktype_betrokkenen_phase1_params_expected = {
            "zaaktype_node_id_m0": 11112,
            "zaak_status_id_m0": 11115,
            "betrokkene_type_m0": "person",
            "betrokkene_identifier_m0": "betrokkene-natuurlijk_persoon-20001",
            "naam_m0": "k. a Test achternaama",
            "rol_m0": "Auditor",
            "magic_string_prefix_m0": "auditor",
            "gemachtigd_m0": True,
            "notify_m0": True,
            "zaaktype_node_id_m1": 11112,
            "zaak_status_id_m1": 11115,
            "betrokkene_type_m1": "organization",
            "betrokkene_identifier_m1": "betrokkene-bedrijf-20002",
            "naam_m1": "test organisatie",
            "rol_m1": "Auditor",
            "magic_string_prefix_m1": "auditor",
            "gemachtigd_m1": False,
            "notify_m1": False,
        }
        assert (
            zaaktype_betrokkenen_phase1_params
            == zaaktype_betrokkenen_phase1_params_expected
        )

    def test_create_versioned_casetype_attribute_not_found(
        self, versioned_casetype
    ):
        fetch_all_results = list(self.session.execute().fetchall.side_effect)
        fetch_all_results[7][
            3
        ].bibliotheek_kenmerken_id = (
            99999  # Override id to enforce NotFound error
        )
        self.session.execute().fetchall.side_effect = fetch_all_results
        self.session.execute.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "Attribute with uuid addaff98-a91e-4674-b993-60bce9dfb093 not found in casetype"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_no_trigger_default_intern(
        self,
        versioned_casetype,
    ):
        versioned_casetype.relations["trigger"] = None
        versioned_casetype.relations["allowed_requestor_types"] = [
            "medewerker"
        ]
        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        # only check for trigger parameter
        assert (
            call_list[15][0][0]
            .compile(dialect=postgresql.dialect())
            .params["trigger"]
            == "intern"
        )

    def test_create_versioned_casetype_no_trigger_default_extern(
        self,
        versioned_casetype,
    ):
        versioned_casetype.relations["trigger"] = None
        versioned_casetype.relations["allowed_requestor_types"] = ["bedrijf"]
        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        # only check for trigger parameter
        assert (
            call_list[15][0][0]
            .compile(dialect=postgresql.dialect())
            .params["trigger"]
            == "extern"
        )

    def test_create_versioned_casetype_no_trigger_default_intern_extern(
        self,
        versioned_casetype,
    ):
        versioned_casetype.relations["trigger"] = None
        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        # only check for trigger parameter
        assert (
            call_list[15][0][0]
            .compile(dialect=postgresql.dialect())
            .params["trigger"]
            == "internextern"
        )

    def test_create_versioned_casetype_with_organisation_preset_requestor(
        self,
        versioned_casetype,
    ):
        # overwrite the type of the preset_requestor to test the behaviour
        versioned_casetype.relations["preset_requestor"]["type"] = (
            "organization"
        )

        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        assert (
            str(
                self.session.execute.call_args_list[11][0][0].compile(
                    dialect=postgresql.dialect()
                )
            )
            == "SELECT bedrijf.id AS id \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_1)s::UUID"
        )

    def test_create_versioned_casetype_change_log_one_updated_component(
        self,
        versioned_casetype,
    ):
        insert_casetype_row = mock.MagicMock(id=11111)

        logging_legacy_subject_row = mock.MagicMock(id=1234)

        fetchone_sideeffect = list(self.session.execute().fetchone.side_effect)
        fetchone_sideeffect[3] = insert_casetype_row
        fetchone_sideeffect[9] = logging_legacy_subject_row
        self.session.execute().fetchone.side_effect = tuple(
            fetchone_sideeffect
        )

        self.session.execute.reset_mock()

        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log={
                "update_components": [
                    "basisattributen",
                ],
                "update_description": "Wijziging een component",
            },
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        self.assert_has_event_name("VersionedCasetypeCreated")

        component: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["component"]
        )
        onderwerp: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["onderwerp"]
        )
        event_type: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["event_type"]
        )
        event_data: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["event_data"]
        )
        created_by: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["created_by"]
        )

        assert component == "zaaktype"
        assert (
            onderwerp
            == f"Zaaktype MINTY-11111 ({insert_casetype_row.id}) is opgeslagen. Aangepast component is: basisattributen. Opmerkingen: Wijziging een component"
        )
        assert event_type == "casetype/mutation"
        assert (
            event_data
            == "{'case_type': "
            + str(insert_casetype_row.id)
            + ", 'commit_message': 'Wijziging een component', 'components': ['basisattributen'], 'title': 'MINTY-11111'}"
        )

        assert created_by == "betrokkene-medewerker-1234"

    def test_create_versioned_casetype_change_log_multiple_updated_components(
        self, versioned_casetype
    ):
        insert_casetype_row = mock.MagicMock(id=11111)

        logging_legacy_subject_row = mock.MagicMock()
        logging_legacy_subject_row.__getitem__.side_effect = [1234]

        fetchone_sideeffect = list(self.session.execute().fetchone.side_effect)
        fetchone_sideeffect[4] = insert_casetype_row
        fetchone_sideeffect[9] = logging_legacy_subject_row
        self.session.execute().fetchone.side_effect = tuple(
            fetchone_sideeffect
        )
        self.session.execute.reset_mock()

        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log={
                "update_components": [
                    "basisattributen",
                    "zaakacties",
                    "kenmerken",
                    "basisattributen",  # ignore this duplicate
                    "regels",
                    "kinderen",
                ],
                "update_description": "Wijziging meerdere componenten",
            },
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        component: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["component"]
        )
        onderwerp: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["onderwerp"]
        )
        event_type: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["event_type"]
        )
        event_data: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["event_data"]
        )

        assert component == "zaaktype"

        assert (
            onderwerp
            == f"Zaaktype MINTY-11111 ({insert_casetype_row.id}) is opgeslagen. Aangepaste componenten zijn: basisattributen, kenmerken, kinderen, regels, zaakacties. Opmerkingen: Wijziging meerdere componenten"
        )

        assert event_type == "casetype/mutation"

        assert (
            event_data
            == "{'case_type': "
            + str(insert_casetype_row.id)
            + ", 'commit_message': 'Wijziging meerdere componenten', 'components': ['basisattributen', 'kenmerken', 'kinderen', 'regels', 'zaakacties'], 'title': 'MINTY-11111'}"
        )

    def test_create_versioned_casetype_change_log_no_update_components(
        self, versioned_casetype
    ):
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log={
                    "update_description": "Wijziging in componenten",
                },
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "1 validation error for CreateVersionedCasetype\nchange_log -> update_components\n  field required (type=value_error.missing)"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_change_log_update_description_none(
        self, versioned_casetype
    ):
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log={
                    "update_components": None,
                    "update_description": "Wijziging in componenten",
                },
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "1 validation error for CreateVersionedCasetype\nchange_log -> update_components\n  none is not an allowed value (type=type_error.none.not_allowed)"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_change_log_only_update_components(
        self, versioned_casetype
    ):
        insert_casetype_row = mock.MagicMock(id=11111)
        logging_legacy_subject_row = mock.MagicMock()
        logging_legacy_subject_row.__getitem__.side_effect = [1234]

        fetchone_sideeffect = list(self.session.execute().fetchone.side_effect)
        fetchone_sideeffect[3] = insert_casetype_row
        fetchone_sideeffect[9] = logging_legacy_subject_row
        self.session.execute().fetchone.side_effect = tuple(
            fetchone_sideeffect
        )
        self.session.execute.reset_mock()

        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log={
                "update_components": ["basisattributen"],
                "update_description": "",
            },
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeCreated")
        call_list = self.session.execute.call_args_list

        component: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["component"]
        )
        onderwerp: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["onderwerp"]
        )
        event_type: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["event_type"]
        )
        event_data: str = str(
            call_list[21][0][0]
            .compile(dialect=postgresql.dialect())
            .params["event_data"]
        )

        assert component == "zaaktype"

        assert (
            onderwerp
            == f"Zaaktype MINTY-11111 ({insert_casetype_row.id}) is opgeslagen. Aangepast component is: basisattributen"
        )

        assert event_type == "casetype/mutation"

        assert (
            event_data
            == "{'case_type': "
            + str(insert_casetype_row.id)
            + ", 'commit_message': '', 'components': ['basisattributen'], 'title': 'MINTY-11111'}"
        )

    def test_create_versioned_casetype_empty_change_log(
        self, versioned_casetype
    ):
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log={},
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "1 validation error for CreateVersionedCasetype\nchange_log -> update_components\n  field required (type=value_error.missing)"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_no_change_log(self, versioned_casetype):
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "1 validation error for ChangeLog\nupdate_components\n  field required (type=value_error.missing)"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_already_exists(
        self, versioned_casetype
    ):
        folder_row = mock.MagicMock()
        folder_row.configure_mock(id=1, uuid=uuid4(), name="folder name")

        self.session.execute().fetchone.side_effect = (
            folder_row,
            uuid4(),  # assert_casetype_not_exists
        )
        self.session.execute.reset_mock()

        casetype_uuid = uuid4()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=casetype_uuid,
                casetype_version_uuid=uuid4(),
                active=False,
                catalog_folder_uuid=uuid4(),
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert excinfo.value.args == (
            f"Casetype with uuid {casetype_uuid} already exists",
        )

    def test_create_versioned_casetype_not_unique(self, versioned_casetype):
        folder_row = mock.MagicMock()
        folder_row.configure_mock(id=1, uuid=uuid4(), name="folder name")

        casetype_unique_row = mock.MagicMock()
        casetype_unique_row.configure_mock(
            uuid=uuid4(), titel="Duplicate title", code="Duplicate code"
        )

        self.session.execute().fetchone.side_effect = (
            folder_row,
            None,
            casetype_unique_row,  # casetype not unique
        )
        self.session.execute.reset_mock()

        casetype_uuid = uuid4()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=casetype_uuid,
                casetype_version_uuid=uuid4(),
                active=False,
                catalog_folder_uuid=uuid4(),
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert excinfo.value.args == (
            f"Casetype not unique. Castype node with uuid {casetype_unique_row.uuid} has duplicate name",
        )

    def test_create_versioned_casetype_folder_does_not_exist(
        self, versioned_casetype
    ):
        folder_row = mock.MagicMock()
        folder_row.configure_mock(id=1, uuid=uuid4(), name="folder name")

        next_casetype_version_row = mock.MagicMock()
        next_casetype_version_row.configure_mock(max_version=3)

        self.session.execute().fetchone.side_effect = [
            None  # get_folder result
        ]
        self.session.execute.reset_mock()

        folder_uuid = uuid4()
        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                active=False,
                catalog_folder_uuid=folder_uuid,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert excinfo.value.args == (
            f"Folder with uuid: {folder_uuid} not found.",
            "folder/not_found",
        )

    def test_create_legacy_subject_id(self):
        subject_relation = EmployeeRelation(
            uuid=uuid4(),
            name="employee",
            type=SubjectType.Employee,
        )

        logging_legacy_subject_row = mock.MagicMock(id=1234)

        self.session.execute().fetchone.side_effect = (
            logging_legacy_subject_row,
        )

        legacy_subject_id: str | None = util.create_legacy_subject_id(
            session=self.session,
            subject_uuid=subject_relation.uuid,
            subject_type=subject_relation.type,
        )

        assert legacy_subject_id == "betrokkene-medewerker-1234"

    def test_create_legacy_subject_id_no_relation(self):
        legacy_subject_id: str | None = util.create_legacy_subject_id(
            session=self.session, subject_uuid=None, subject_type=None
        )

        assert legacy_subject_id is None

    def test_create_versioned_casetype_roles_notfound(
        self, versioned_casetype
    ):
        fetch_all_results = list(self.session.execute().fetchall.side_effect)
        fetch_all_results[1] = []
        self.session.execute().fetchall.side_effect = fetch_all_results
        self.session.execute.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )

    def test_create_versioned_casetype_groups_notfound(
        self, versioned_casetype
    ):
        fetch_all_results = list(self.session.execute().fetchall.side_effect)
        fetch_all_results[0] = []
        self.session.execute().fetchall.side_effect = fetch_all_results
        self.session.execute.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )

    def test_create_versioned_casetype_attributes_notfound(
        self, versioned_casetype
    ):
        fetch_all_results = list(self.session.execute().fetchall.side_effect)
        fetch_all_results[2] = []
        self.session.execute().fetchall.side_effect = fetch_all_results
        self.session.execute.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )

    def test_create_versioned_casetype_tasks_in_first_phase(
        self, versioned_casetype
    ):
        # add a task to first phase
        versioned_casetype.phases[0]["tasks"] = ["task 1", "task 2"]

        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "1 validation error for VersionedCaseType\nphases\n  Tasks are not allowed in the first phase (type=value_error)"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_invalid_price_format(
        self, versioned_casetype
    ):
        versioned_casetype.webform["price"]["post"] = "aa"
        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert "value is not a valid decimal" in str(excinfo.value)

        versioned_casetype.webform["price"]["post"] = ""
        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )
        assert 1 == 1

    def test_create_versioned_casetype_monitoring_period(
        self, versioned_casetype
    ):
        versioned_casetype.general_attributes["legal_period"]["type"] = (
            "einddatum"
        )
        versioned_casetype.general_attributes["legal_period"]["value"] = (
            "2030-12-31"
        )

        versioned_casetype.general_attributes["service_period"]["type"] = (
            "einddatum"
        )
        versioned_casetype.general_attributes["service_period"]["value"] = (
            "2030-12-30"
        )

        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        call_list = self.session.execute.call_args_list
        # assert casetype_definitie
        insert_casetype_definitie = call_list[12][0][0].compile().params
        assert insert_casetype_definitie["servicenorm"] == "30-12-2030"
        assert insert_casetype_definitie["afhandeltermijn"] == "31-12-2030"

    def test_create_versioned_casetype_template_not_found(
        self, versioned_casetype
    ):
        fetch_all_results = list(self.session.execute().fetchall.side_effect)
        # Override uuid to enforce NotFound error
        fetch_all_results[3][1].uuid = uuid4()

        self.session.execute().fetchall.side_effect = fetch_all_results
        self.session.execute.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "Template(s) with uuid 9a278be5-4717-4e91-884b-7295fc7136d5 not found."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_duplicate_cases_in_phase(
        self, versioned_casetype
    ):
        case_settings = versioned_casetype.phases[0]["cases"][0]
        versioned_casetype.phases[0]["cases"].append(
            case_settings
        )  # copy the casetype settings so it is a duplicate

        with pytest.raises(ValueError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "A casetype can only be added once per phase. Duplicate casetypes found with uuid aaef48d3-98f4-4e66-ade8-af0409137e2f."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_kind_invalid(self, versioned_casetype):
        versioned_casetype.phases[0]["cases"][0]["kind"] = "vervolgzaak"

        with pytest.raises(ValueError) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "The value vervolgzaak in attribute cases.kind is only allowed in the last phase."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_casetype_not_found(
        self, versioned_casetype
    ):
        versioned_casetype.phases[0]["cases"][0]["related_casetype"] = (
            "4bd1a943-3ce7-446f-bf73-b81a5ecb3fdc"  # unknown uuid
        )

        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "Casetypes(s) with uuid 4bd1a943-3ce7-446f-bf73-b81a5ecb3fdc not found."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_source_attribute_not_found_for_case(
        self, versioned_casetype
    ):
        unknown_source_attribute_uuid = "4bd1a943-3ce7-446f-bf73-b81a5ecb3fdc"
        versioned_casetype.phases[0]["cases"][0]["copy_attributes_mapping"] = {
            "800b63f6-aa9a-4df7-9968-2cc036923dd8": "800b63f6-aa9a-4df7-9968-2cc036923dd8",  # tekstfield
            unknown_source_attribute_uuid: "40a72131-193d-4ace-804a-9b8de9c4c868",  # date field
        }

        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            f"Casetype configuration for cases invalid. The supplied source attribute uuid(s) ['{unknown_source_attribute_uuid}'] are not present in this casetype."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_target_attribute_not_found_for_case(
        self, versioned_casetype
    ):
        unknown_target_attribute_uuid = "4bd1a943-3ce7-446f-bf73-b81a5ecb3fff"
        versioned_casetype.phases[0]["cases"][0]["copy_attributes_mapping"] = {
            "800b63f6-aa9a-4df7-9968-2cc036923dd8": "800b63f6-aa9a-4df7-9968-2cc036923dd8",
            "40a72131-193d-4ace-804a-9b8de9c4c868": unknown_target_attribute_uuid,
        }

        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "Casetype configuration for cases with casetype uuid aaef48d3-98f4-4e66-ade8-af0409137e2f invalid. The provided attribute uuid's {'4bd1a943-3ce7-446f-bf73-b81a5ecb3fff'} are not present in the target casetype."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_target_attribute_not_of_type_uuid(
        self, versioned_casetype
    ):
        versioned_casetype.phases[0]["cases"][0]["copy_attributes_mapping"] = {
            "magic_string_not_supported": "800b63f6-aa9a-4df7-9968-2cc036923dd8",
        }

        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "Key magic_string_not_supported in copy_attribute_mapping is not of type UUID."
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_subject_not_found(
        self, versioned_casetype
    ):
        fetchone_sideeffect = list(self.session.execute().fetchone.side_effect)
        fetchone_sideeffect[15] = None
        self.session.execute().fetchone.side_effect = tuple(
            fetchone_sideeffect
        )
        self.session.execute.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "Subject person with uuid 46190248-f0a9-4a9b-9351-9929b0a770c7 not found"
            in str(excinfo.value)
        )

    def test_create_versioned_casetype_involved_subject_role_not_found(
        self, versioned_casetype
    ):
        # supply a custom role, which is not present in config
        versioned_casetype.phases[0]["involved_subjects"][0]["role"] = (
            "custom_role"
        )
        fetchone_sideeffect = list(self.session.execute().fetchone.side_effect)
        fetchone_sideeffect.insert(
            3, mock.MagicMock(value="another_costum_role")
        )
        self.session.execute().fetchone.side_effect = tuple(
            fetchone_sideeffect
        )
        self.session.execute.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_versioned_casetype(
                casetype_uuid=uuid4(),
                casetype_version_uuid=uuid4(),
                catalog_folder_uuid=uuid4(),
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                change_log=versioned_casetype.change_log,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert "Config role(s) with name custom_role not found." in str(
            excinfo.value
        )

    def test_create_versioned_casetype_involved_subject_role_found(
        self, versioned_casetype
    ):
        # supply a custom role, which is present in config
        versioned_casetype.phases[0]["involved_subjects"][0]["role"] = (
            "custom_role"
        )
        fetchone_sideeffect = list(self.session.execute().fetchone.side_effect)
        fetchone_sideeffect.insert(3, mock.MagicMock(value="custom_role"))
        self.session.execute().fetchone.side_effect = tuple(
            fetchone_sideeffect
        )
        self.session.execute.reset_mock()

        self.cmd.create_versioned_casetype(
            casetype_uuid=uuid4(),
            casetype_version_uuid=uuid4(),
            catalog_folder_uuid=uuid4(),
            active=versioned_casetype.active,
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )


class TestAs_A_User_I_Want_To_Update_A_VersionedCaseType(TestBase):
    @pytest.fixture(autouse=True)
    def setup_method(
        self,
        department_rows_mock,
        role_rows_mock,
        attributes_rows_mock,
        template_rows_mock,
        email_template_rows_mock,
        casetype_rows_mock,
        zaaktype_kenmerken_rows_mock,
        target_attribute_mappings_cases,
    ):
        self.load_command_instance(catalog)
        self.mock_infra = mock.MagicMock()
        self.event_service = EventService(
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        folder_row = mock.MagicMock()
        folder_row.configure_mock(id=1, uuid=uuid4(), name="folder name")
        casetype_uuid = uuid4()
        casetype_mock = mock.MagicMock(uuid=casetype_uuid)

        self.session.execute().fetchall.side_effect = [
            department_rows_mock,
            role_rows_mock,
            attributes_rows_mock,
            template_rows_mock,
            email_template_rows_mock,
            casetype_rows_mock,
            target_attribute_mappings_cases,
            *zaaktype_kenmerken_rows_mock,
            [],
        ]

        self.session.execute().fetchone.side_effect = (
            folder_row,
            casetype_mock,  # assert_casetype_exists
            None,  # assert casetype unique
            mock.MagicMock(id=11111),  # zaaktype id
            mock.MagicMock(id=1324),  # subject row preset_client
            mock.MagicMock(id=11111),  # zaaktype definitie id
            mock.MagicMock(max_version=25),  # max version zaaktype_node
            mock.MagicMock(),  # a legacy subject
            mock.MagicMock(id=11112),  # zaaktype node id
            mock.MagicMock(id=11113),
            mock.MagicMock(id=11114),  # logging id
            mock.MagicMock(id=11115),  # zaaktype_status result phase 1
            mock.MagicMock(id=11116),  # zaaktype_status result phase 2
            mock.MagicMock(id=11117),  # zaaktype_status result phase 3
            mock.MagicMock(id=5678),  # subject row zaken config
            mock.MagicMock(id=20000),  # legacy subject_id in zaken config
            mock.MagicMock(
                id=20001
            ),  # legacy subject_id in default subjects (natuurlijk persoon)
            mock.MagicMock(
                id=20002
            ),  # legacy subject_id in default subjects (bedrijf)
            mock.MagicMock(
                id=34
            ),  # legacy subject id (for zaaktype_notificatie)
        )
        self.session.execute.reset_mock()

    def test_update_versioned_casetype_casetype_does_not_exists(
        self, versioned_casetype
    ):
        casetype_uuid = uuid4()
        casetype_version_uuid = uuid4()

        self.session.execute().fetchone.side_effect = (
            None,  # result for check if casetype exists
        )
        self.session.execute.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.update_versioned_casetype(
                casetype_uuid=casetype_uuid,
                casetype_version_uuid=casetype_version_uuid,
                active=False,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                phases=versioned_casetype.phases,
                change_log=versioned_casetype.change_log,
                results=versioned_casetype.results,
            )
        assert excinfo.value.args == (
            f"Casetype with uuid {casetype_uuid} does not exist",
        )

    def test_update_versioned_casetype_casetype_uuid_not_unique(
        self, versioned_casetype
    ):
        casetype_uuid = uuid4()
        casetype_version_uuid = uuid4()

        casetype_mock = mock.MagicMock()
        casetype_mock.configure_mock(uuid=casetype_uuid)

        casetype_node_mock = mock.MagicMock()
        casetype_node_mock.configure_mock(uuid=casetype_version_uuid)

        self.session.execute().fetchone.side_effect = (
            casetype_mock,
            casetype_node_mock,  # if casetype_node unique
        )
        self.session.execute.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.update_versioned_casetype(
                casetype_uuid=casetype_uuid,
                casetype_version_uuid=casetype_version_uuid,
                active=False,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                phases=versioned_casetype.phases,
                change_log=versioned_casetype.change_log,
                results=versioned_casetype.results,
            )
        assert excinfo.value.args == (
            f"Casetypeversion not unique. Casetypeversion with uuid {casetype_version_uuid} already exists",
        )

    def test_update_versioned_casetype(self, versioned_casetype):
        casetype_uuid = uuid4()
        casetype_version_uuid = uuid4()

        self.cmd.update_versioned_casetype(
            casetype_uuid=casetype_uuid,
            casetype_version_uuid=casetype_version_uuid,
            active=versioned_casetype.active,
            catalog_folder_uuid=uuid4(),
            general_attributes=versioned_casetype.general_attributes,
            documentation=versioned_casetype.documentation,
            relations=versioned_casetype.relations,
            webform=versioned_casetype.webform,
            registrationform=versioned_casetype.registrationform,
            case_dossier=versioned_casetype.case_dossier,
            api=versioned_casetype.api,
            authorization=versioned_casetype.authorization,
            child_casetype_settings=versioned_casetype.child_casetype_settings,
            change_log=versioned_casetype.change_log,
            phases=versioned_casetype.phases,
            results=versioned_casetype.results,
        )

        self.assert_has_event_name("VersionedCasetypeUpdated")
        call_list = self.session.execute.call_args_list

        queries = {
            0: "SELECT bibliotheek_categorie.id, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, bibliotheek_categorie.last_modified, bibliotheek_categorie_1.uuid AS parent_uuid, bibliotheek_categorie_1.naam AS parent_name, bibliotheek_categorie_1.id AS parent_id \nFROM bibliotheek_categorie LEFT OUTER JOIN bibliotheek_categorie AS bibliotheek_categorie_1 ON bibliotheek_categorie.pid = bibliotheek_categorie_1.id \nWHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID",
            1: "SELECT zaaktype.id \nFROM zaaktype \nWHERE zaaktype.uuid = %(uuid_1)s::UUID",
            2: "SELECT zaaktype_node.uuid \nFROM zaaktype_node \nWHERE zaaktype_node.uuid = %(uuid_1)s::UUID",
            3: "SELECT groups.id, groups.uuid, groups.name \nFROM groups \nWHERE groups.uuid IN (__[POSTCOMPILE_uuid_1])",
            4: "SELECT roles.id, roles.uuid, roles.name \nFROM roles \nWHERE roles.uuid IN (__[POSTCOMPILE_uuid_1])",
            5: "SELECT bibliotheek_kenmerken.id, bibliotheek_kenmerken.naam_public, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.uuid \nFROM bibliotheek_kenmerken \nWHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.uuid IN (__[POSTCOMPILE_uuid_1])",
            6: "SELECT bibliotheek_sjablonen.id, bibliotheek_sjablonen.naam, bibliotheek_sjablonen.uuid \nFROM bibliotheek_sjablonen \nWHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.uuid IN (__[POSTCOMPILE_uuid_1])",
            7: "SELECT bibliotheek_notificaties.id, bibliotheek_notificaties.label, bibliotheek_notificaties.uuid \nFROM bibliotheek_notificaties \nWHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.uuid IN (__[POSTCOMPILE_uuid_1])",
            8: "SELECT zaaktype.id, zaaktype.uuid \nFROM zaaktype \nWHERE zaaktype.deleted IS NULL AND zaaktype.uuid IN (__[POSTCOMPILE_uuid_1])",
            9: "SELECT bibliotheek_kenmerken.uuid \nFROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON bibliotheek_kenmerken.id = zaaktype_kenmerken.bibliotheek_kenmerken_id JOIN zaaktype ON zaaktype.zaaktype_node_id = zaaktype_kenmerken.zaaktype_node_id \nWHERE bibliotheek_kenmerken.uuid IN (__[POSTCOMPILE_uuid_1]) AND zaaktype.uuid = %(uuid_2)s::UUID",
            10: "UPDATE zaaktype SET bibliotheek_categorie_id=(SELECT bibliotheek_categorie.id \nFROM bibliotheek_categorie \nWHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID), active=%(active)s WHERE zaaktype.uuid = %(uuid_2)s::UUID",
            11: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            12: "INSERT INTO zaaktype_definitie (handelingsinitiator, grondslag, procesbeschrijving, afhandeltermijn, afhandeltermijn_type, servicenorm, servicenorm_type, pdc_tarief, omschrijving_upl, aard, preset_client, extra_informatie, extra_informatie_extern) VALUES (%(handelingsinitiator)s, %(grondslag)s, %(procesbeschrijving)s, %(afhandeltermijn)s, %(afhandeltermijn_type)s, %(servicenorm)s, %(servicenorm_type)s, %(pdc_tarief)s, %(omschrijving_upl)s, %(aard)s, %(preset_client)s, %(extra_informatie)s, %(extra_informatie_extern)s) RETURNING zaaktype_definitie.id",
            13: "SELECT max(zaaktype_node.version) AS max_version \nFROM zaaktype_node \nWHERE zaaktype_node.zaaktype_id = %(zaaktype_id_1)s",
            14: "SELECT subject.id AS id \nFROM subject \nWHERE subject.uuid = %(uuid_1)s::UUID",
            15: "INSERT INTO zaaktype_node (zaaktype_id, zaaktype_definitie_id, titel, code, trigger, version, properties, is_public, zaaktype_trefwoorden, zaaktype_omschrijving, webform_toegang, aanvrager_hergebruik, automatisch_behandelen, toewijzing_zaakintake, online_betaling, adres_andere_locatie, adres_aanvrager, adres_geojson, extra_relaties_in_aanvraag, contact_info_intake, prevent_pip, contact_info_email_required, contact_info_phone_required, contact_info_mobile_phone_required) VALUES (%(zaaktype_id)s, %(zaaktype_definitie_id)s, %(titel)s, %(code)s, %(trigger)s, %(version)s, %(properties)s, %(is_public)s, %(zaaktype_trefwoorden)s, %(zaaktype_omschrijving)s, %(webform_toegang)s, %(aanvrager_hergebruik)s, %(automatisch_behandelen)s, %(toewijzing_zaakintake)s, %(online_betaling)s, %(adres_andere_locatie)s, %(adres_aanvrager)s, %(adres_geojson)s, %(extra_relaties_in_aanvraag)s, %(contact_info_intake)s, %(prevent_pip)s, %(contact_info_email_required)s, %(contact_info_phone_required)s, %(contact_info_mobile_phone_required)s) RETURNING zaaktype_node.id",
            16: "DELETE FROM zaaktype_authorisation WHERE zaaktype_authorisation.zaaktype_id = %(zaaktype_id_1)s",
            17: "INSERT INTO zaaktype_authorisation (zaaktype_node_id, recht, role_id, ou_id, zaaktype_id, confidential) VALUES (%(zaaktype_node_id_m0)s, %(recht_m0)s, %(role_id_m0)s, %(ou_id_m0)s, %(zaaktype_id_m0)s, %(confidential_m0)s), (%(zaaktype_node_id_m1)s, %(recht_m1)s, %(role_id_m1)s, %(ou_id_m1)s, %(zaaktype_id_m1)s, %(confidential_m1)s), (%(zaaktype_node_id_m2)s, %(recht_m2)s, %(role_id_m2)s, %(ou_id_m2)s, %(zaaktype_id_m2)s, %(confidential_m2)s), (%(zaaktype_node_id_m3)s, %(recht_m3)s, %(role_id_m3)s, %(ou_id_m3)s, %(zaaktype_id_m3)s, %(confidential_m3)s), (%(zaaktype_node_id_m4)s, %(recht_m4)s, %(role_id_m4)s, %(ou_id_m4)s, %(zaaktype_id_m4)s, %(confidential_m4)s), (%(zaaktype_node_id_m5)s, %(recht_m5)s, %(role_id_m5)s, %(ou_id_m5)s, %(zaaktype_id_m5)s, %(confidential_m5)s), (%(zaaktype_node_id_m6)s, %(recht_m6)s, %(role_id_m6)s, %(ou_id_m6)s, %(zaaktype_id_m6)s, %(confidential_m6)s), (%(zaaktype_node_id_m7)s, %(recht_m7)s, %(role_id_m7)s, %(ou_id_m7)s, %(zaaktype_id_m7)s, %(confidential_m7)s), (%(zaaktype_node_id_m8)s, %(recht_m8)s, %(role_id_m8)s, %(ou_id_m8)s, %(zaaktype_id_m8)s, %(confidential_m8)s), (%(zaaktype_node_id_m9)s, %(recht_m9)s, %(role_id_m9)s, %(ou_id_m9)s, %(zaaktype_id_m9)s, %(confidential_m9)s)",
            18: "INSERT INTO zaaktype_betrokkenen (zaaktype_node_id, betrokkene_type) VALUES (%(zaaktype_node_id_m0)s, %(betrokkene_type_m0)s), (%(zaaktype_node_id_m1)s, %(betrokkene_type_m1)s), (%(zaaktype_node_id_m2)s, %(betrokkene_type_m2)s), (%(zaaktype_node_id_m3)s, %(betrokkene_type_m3)s), (%(zaaktype_node_id_m4)s, %(betrokkene_type_m4)s)",
            19: "UPDATE zaaktype SET zaaktype_node_id=%(zaaktype_node_id)s WHERE zaaktype.id = %(id_1)s",
            20: "SELECT subject.id AS id \nFROM subject \nWHERE subject.uuid = %(uuid_1)s::UUID",
            21: "INSERT INTO logging (component, component_id, onderwerp, event_type, event_data, created_by, created_by_name_cache, restricted) VALUES (%(component)s, %(component_id)s, %(onderwerp)s, %(event_type)s, %(event_data)s, %(created_by)s, %(created_by_name_cache)s, %(restricted)s) RETURNING logging.id",
            22: "UPDATE zaaktype_node SET logging_id=%(logging_id)s WHERE zaaktype_node.id = %(id_1)s",
            23: "INSERT INTO zaaktype_status (zaaktype_node_id, status, termijn, naam, fase, ou_id, role_id, role_set) VALUES (%(zaaktype_node_id)s, %(status)s, %(termijn)s, %(naam)s, %(fase)s, %(ou_id)s, %(role_id)s, %(role_set)s) RETURNING zaaktype_status.id",
            24: "INSERT INTO zaaktype_status (zaaktype_node_id, status, termijn, naam, fase, ou_id, role_id, role_set) VALUES (%(zaaktype_node_id)s, %(status)s, %(termijn)s, %(naam)s, %(fase)s, %(ou_id)s, %(role_id)s, %(role_set)s) RETURNING zaaktype_status.id",
            25: "INSERT INTO zaaktype_status (zaaktype_node_id, status, termijn, naam, fase, ou_id, role_id, role_set) VALUES (%(zaaktype_node_id)s, %(status)s, %(termijn)s, %(naam)s, %(fase)s, %(ou_id)s, %(role_id)s, %(role_set)s) RETURNING zaaktype_status.id",
            26: "INSERT INTO zaaktype_kenmerken (zaaktype_node_id, zaak_status_id, bibliotheek_kenmerken_id, value_mandatory, label, help, pip, zaakinformatie_view, bag_zaakadres, pip_can_change, referential, is_systeemkenmerk, required_permissions, help_extern, object_metadata, label_multiple, properties, is_group) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(value_mandatory_m0)s, %(label_m0)s, %(help_m0)s, %(pip_m0)s, %(zaakinformatie_view)s, %(bag_zaakadres_m0)s, %(pip_can_change_m0)s, %(referential_m0)s, %(is_systeemkenmerk_m0)s, %(required_permissions_m0)s, %(help_extern_m0)s, %(object_metadata)s, %(label_multiple_m0)s, %(properties_m0)s, %(is_group_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_kenmerken_id_m1)s, %(value_mandatory_m1)s, %(label_m1)s, %(help_m1)s, %(pip_m1)s, %(zaakinformatie_view_m1)s, %(bag_zaakadres_m1)s, %(pip_can_change_m1)s, %(referential_m1)s, %(is_systeemkenmerk_m1)s, %(required_permissions_m1)s, %(help_extern_m1)s, %(object_metadata_m1)s, %(label_multiple_m1)s, %(properties_m1)s, %(is_group_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_kenmerken_id_m2)s, %(value_mandatory_m2)s, %(label_m2)s, %(help_m2)s, %(pip_m2)s, %(zaakinformatie_view_m2)s, %(bag_zaakadres_m2)s, %(pip_can_change_m2)s, %(referential_m2)s, %(is_systeemkenmerk_m2)s, %(required_permissions_m2)s, %(help_extern_m2)s, %(object_metadata_m2)s, %(label_multiple_m2)s, %(properties_m2)s, %(is_group_m2)s), (%(zaaktype_node_id_m3)s, %(zaak_status_id_m3)s, %(bibliotheek_kenmerken_id_m3)s, %(value_mandatory_m3)s, %(label_m3)s, %(help_m3)s, %(pip_m3)s, %(zaakinformatie_view_m3)s, %(bag_zaakadres_m3)s, %(pip_can_change_m3)s, %(referential_m3)s, %(is_systeemkenmerk_m3)s, %(required_permissions_m3)s, %(help_extern_m3)s, %(object_metadata_m3)s, %(label_multiple_m3)s, %(properties_m3)s, %(is_group_m3)s) RETURNING zaaktype_kenmerken.id, zaaktype_kenmerken.bibliotheek_kenmerken_id, zaaktype_kenmerken.properties",
            27: "INSERT INTO zaaktype_kenmerken (zaaktype_node_id, zaak_status_id, bibliotheek_kenmerken_id, value_mandatory, label, help, pip, zaakinformatie_view, bag_zaakadres, pip_can_change, referential, is_systeemkenmerk, required_permissions, help_extern, object_metadata, label_multiple, properties, is_group) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(value_mandatory_m0)s, %(label_m0)s, %(help_m0)s, %(pip_m0)s, %(zaakinformatie_view)s, %(bag_zaakadres_m0)s, %(pip_can_change_m0)s, %(referential_m0)s, %(is_systeemkenmerk_m0)s, %(required_permissions_m0)s, %(help_extern_m0)s, %(object_metadata)s, %(label_multiple_m0)s, %(properties_m0)s, %(is_group_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_kenmerken_id_m1)s, %(value_mandatory_m1)s, %(label_m1)s, %(help_m1)s, %(pip_m1)s, %(zaakinformatie_view_m1)s, %(bag_zaakadres_m1)s, %(pip_can_change_m1)s, %(referential_m1)s, %(is_systeemkenmerk_m1)s, %(required_permissions_m1)s, %(help_extern_m1)s, %(object_metadata_m1)s, %(label_multiple_m1)s, %(properties_m1)s, %(is_group_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_kenmerken_id_m2)s, %(value_mandatory_m2)s, %(label_m2)s, %(help_m2)s, %(pip_m2)s, %(zaakinformatie_view_m2)s, %(bag_zaakadres_m2)s, %(pip_can_change_m2)s, %(referential_m2)s, %(is_systeemkenmerk_m2)s, %(required_permissions_m2)s, %(help_extern_m2)s, %(object_metadata_m2)s, %(label_multiple_m2)s, %(properties_m2)s, %(is_group_m2)s), (%(zaaktype_node_id_m3)s, %(zaak_status_id_m3)s, %(bibliotheek_kenmerken_id_m3)s, %(value_mandatory_m3)s, %(label_m3)s, %(help_m3)s, %(pip_m3)s, %(zaakinformatie_view_m3)s, %(bag_zaakadres_m3)s, %(pip_can_change_m3)s, %(referential_m3)s, %(is_systeemkenmerk_m3)s, %(required_permissions_m3)s, %(help_extern_m3)s, %(object_metadata_m3)s, %(label_multiple_m3)s, %(properties_m3)s, %(is_group_m3)s), (%(zaaktype_node_id_m4)s, %(zaak_status_id_m4)s, %(bibliotheek_kenmerken_id_m4)s, %(value_mandatory_m4)s, %(label_m4)s, %(help_m4)s, %(pip_m4)s, %(zaakinformatie_view_m4)s, %(bag_zaakadres_m4)s, %(pip_can_change_m4)s, %(referential_m4)s, %(is_systeemkenmerk_m4)s, %(required_permissions_m4)s, %(help_extern_m4)s, %(object_metadata_m4)s, %(label_multiple_m4)s, %(properties_m4)s, %(is_group_m4)s), (%(zaaktype_node_id_m5)s, %(zaak_status_id_m5)s, %(bibliotheek_kenmerken_id_m5)s, %(value_mandatory_m5)s, %(label_m5)s, %(help_m5)s, %(pip_m5)s, %(zaakinformatie_view_m5)s, %(bag_zaakadres_m5)s, %(pip_can_change_m5)s, %(referential_m5)s, %(is_systeemkenmerk_m5)s, %(required_permissions_m5)s, %(help_extern_m5)s, %(object_metadata_m5)s, %(label_multiple_m5)s, %(properties_m5)s, %(is_group_m5)s), (%(zaaktype_node_id_m6)s, %(zaak_status_id_m6)s, %(bibliotheek_kenmerken_id_m6)s, %(value_mandatory_m6)s, %(label_m6)s, %(help_m6)s, %(pip_m6)s, %(zaakinformatie_view_m6)s, %(bag_zaakadres_m6)s, %(pip_can_change_m6)s, %(referential_m6)s, %(is_systeemkenmerk_m6)s, %(required_permissions_m6)s, %(help_extern_m6)s, %(object_metadata_m6)s, %(label_multiple_m6)s, %(properties_m6)s, %(is_group_m6)s), (%(zaaktype_node_id_m7)s, %(zaak_status_id_m7)s, %(bibliotheek_kenmerken_id_m7)s, %(value_mandatory_m7)s, %(label_m7)s, %(help_m7)s, %(pip_m7)s, %(zaakinformatie_view_m7)s, %(bag_zaakadres_m7)s, %(pip_can_change_m7)s, %(referential_m7)s, %(is_systeemkenmerk_m7)s, %(required_permissions_m7)s, %(help_extern_m7)s, %(object_metadata_m7)s, %(label_multiple_m7)s, %(properties_m7)s, %(is_group_m7)s), (%(zaaktype_node_id_m8)s, %(zaak_status_id_m8)s, %(bibliotheek_kenmerken_id_m8)s, %(value_mandatory_m8)s, %(label_m8)s, %(help_m8)s, %(pip_m8)s, %(zaakinformatie_view_m8)s, %(bag_zaakadres_m8)s, %(pip_can_change_m8)s, %(referential_m8)s, %(is_systeemkenmerk_m8)s, %(required_permissions_m8)s, %(help_extern_m8)s, %(object_metadata_m8)s, %(label_multiple_m8)s, %(properties_m8)s, %(is_group_m8)s), (%(zaaktype_node_id_m9)s, %(zaak_status_id_m9)s, %(bibliotheek_kenmerken_id_m9)s, %(value_mandatory_m9)s, %(label_m9)s, %(help_m9)s, %(pip_m9)s, %(zaakinformatie_view_m9)s, %(bag_zaakadres_m9)s, %(pip_can_change_m9)s, %(referential_m9)s, %(is_systeemkenmerk_m9)s, %(required_permissions_m9)s, %(help_extern_m9)s, %(object_metadata_m9)s, %(label_multiple_m9)s, %(properties_m9)s, %(is_group_m9)s), (%(zaaktype_node_id_m10)s, %(zaak_status_id_m10)s, %(bibliotheek_kenmerken_id_m10)s, %(value_mandatory_m10)s, %(label_m10)s, %(help_m10)s, %(pip_m10)s, %(zaakinformatie_view_m10)s, %(bag_zaakadres_m10)s, %(pip_can_change_m10)s, %(referential_m10)s, %(is_systeemkenmerk_m10)s, %(required_permissions_m10)s, %(help_extern_m10)s, %(object_metadata_m10)s, %(label_multiple_m10)s, %(properties_m10)s, %(is_group_m10)s), (%(zaaktype_node_id_m11)s, %(zaak_status_id_m11)s, %(bibliotheek_kenmerken_id_m11)s, %(value_mandatory_m11)s, %(label_m11)s, %(help_m11)s, %(pip_m11)s, %(zaakinformatie_view_m11)s, %(bag_zaakadres_m11)s, %(pip_can_change_m11)s, %(referential_m11)s, %(is_systeemkenmerk_m11)s, %(required_permissions_m11)s, %(help_extern_m11)s, %(object_metadata_m11)s, %(label_multiple_m11)s, %(properties_m11)s, %(is_group_m11)s), (%(zaaktype_node_id_m12)s, %(zaak_status_id_m12)s, %(bibliotheek_kenmerken_id_m12)s, %(value_mandatory_m12)s, %(label_m12)s, %(help_m12)s, %(pip_m12)s, %(zaakinformatie_view_m12)s, %(bag_zaakadres_m12)s, %(pip_can_change_m12)s, %(referential_m12)s, %(is_systeemkenmerk_m12)s, %(required_permissions_m12)s, %(help_extern_m12)s, %(object_metadata_m12)s, %(label_multiple_m12)s, %(properties_m12)s, %(is_group_m12)s), (%(zaaktype_node_id_m13)s, %(zaak_status_id_m13)s, %(bibliotheek_kenmerken_id_m13)s, %(value_mandatory_m13)s, %(label_m13)s, %(help_m13)s, %(pip_m13)s, %(zaakinformatie_view_m13)s, %(bag_zaakadres_m13)s, %(pip_can_change_m13)s, %(referential_m13)s, %(is_systeemkenmerk_m13)s, %(required_permissions_m13)s, %(help_extern_m13)s, %(object_metadata_m13)s, %(label_multiple_m13)s, %(properties_m13)s, %(is_group_m13)s), (%(zaaktype_node_id_m14)s, %(zaak_status_id_m14)s, %(bibliotheek_kenmerken_id_m14)s, %(value_mandatory_m14)s, %(label_m14)s, %(help_m14)s, %(pip_m14)s, %(zaakinformatie_view_m14)s, %(bag_zaakadres_m14)s, %(pip_can_change_m14)s, %(referential_m14)s, %(is_systeemkenmerk_m14)s, %(required_permissions_m14)s, %(help_extern_m14)s, %(object_metadata_m14)s, %(label_multiple_m14)s, %(properties_m14)s, %(is_group_m14)s), (%(zaaktype_node_id_m15)s, %(zaak_status_id_m15)s, %(bibliotheek_kenmerken_id_m15)s, %(value_mandatory_m15)s, %(label_m15)s, %(help_m15)s, %(pip_m15)s, %(zaakinformatie_view_m15)s, %(bag_zaakadres_m15)s, %(pip_can_change_m15)s, %(referential_m15)s, %(is_systeemkenmerk_m15)s, %(required_permissions_m15)s, %(help_extern_m15)s, %(object_metadata_m15)s, %(label_multiple_m15)s, %(properties_m15)s, %(is_group_m15)s), (%(zaaktype_node_id_m16)s, %(zaak_status_id_m16)s, %(bibliotheek_kenmerken_id_m16)s, %(value_mandatory_m16)s, %(label_m16)s, %(help_m16)s, %(pip_m16)s, %(zaakinformatie_view_m16)s, %(bag_zaakadres_m16)s, %(pip_can_change_m16)s, %(referential_m16)s, %(is_systeemkenmerk_m16)s, %(required_permissions_m16)s, %(help_extern_m16)s, %(object_metadata_m16)s, %(label_multiple_m16)s, %(properties_m16)s, %(is_group_m16)s), (%(zaaktype_node_id_m17)s, %(zaak_status_id_m17)s, %(bibliotheek_kenmerken_id_m17)s, %(value_mandatory_m17)s, %(label_m17)s, %(help_m17)s, %(pip_m17)s, %(zaakinformatie_view_m17)s, %(bag_zaakadres_m17)s, %(pip_can_change_m17)s, %(referential_m17)s, %(is_systeemkenmerk_m17)s, %(required_permissions_m17)s, %(help_extern_m17)s, %(object_metadata_m17)s, %(label_multiple_m17)s, %(properties_m17)s, %(is_group_m17)s), (%(zaaktype_node_id_m18)s, %(zaak_status_id_m18)s, %(bibliotheek_kenmerken_id_m18)s, %(value_mandatory_m18)s, %(label_m18)s, %(help_m18)s, %(pip_m18)s, %(zaakinformatie_view_m18)s, %(bag_zaakadres_m18)s, %(pip_can_change_m18)s, %(referential_m18)s, %(is_systeemkenmerk_m18)s, %(required_permissions_m18)s, %(help_extern_m18)s, %(object_metadata_m18)s, %(label_multiple_m18)s, %(properties_m18)s, %(is_group_m18)s), (%(zaaktype_node_id_m19)s, %(zaak_status_id_m19)s, %(bibliotheek_kenmerken_id_m19)s, %(value_mandatory_m19)s, %(label_m19)s, %(help_m19)s, %(pip_m19)s, %(zaakinformatie_view_m19)s, %(bag_zaakadres_m19)s, %(pip_can_change_m19)s, %(referential_m19)s, %(is_systeemkenmerk_m19)s, %(required_permissions_m19)s, %(help_extern_m19)s, %(object_metadata_m19)s, %(label_multiple_m19)s, %(properties_m19)s, %(is_group_m19)s), (%(zaaktype_node_id_m20)s, %(zaak_status_id_m20)s, %(bibliotheek_kenmerken_id_m20)s, %(value_mandatory_m20)s, %(label_m20)s, %(help_m20)s, %(pip_m20)s, %(zaakinformatie_view_m20)s, %(bag_zaakadres_m20)s, %(pip_can_change_m20)s, %(referential_m20)s, %(is_systeemkenmerk_m20)s, %(required_permissions_m20)s, %(help_extern_m20)s, %(object_metadata_m20)s, %(label_multiple_m20)s, %(properties_m20)s, %(is_group_m20)s), (%(zaaktype_node_id_m21)s, %(zaak_status_id_m21)s, %(bibliotheek_kenmerken_id_m21)s, %(value_mandatory_m21)s, %(label_m21)s, %(help_m21)s, %(pip_m21)s, %(zaakinformatie_view_m21)s, %(bag_zaakadres_m21)s, %(pip_can_change_m21)s, %(referential_m21)s, %(is_systeemkenmerk_m21)s, %(required_permissions_m21)s, %(help_extern_m21)s, %(object_metadata_m21)s, %(label_multiple_m21)s, %(properties_m21)s, %(is_group_m21)s), (%(zaaktype_node_id_m22)s, %(zaak_status_id_m22)s, %(bibliotheek_kenmerken_id_m22)s, %(value_mandatory_m22)s, %(label_m22)s, %(help_m22)s, %(pip_m22)s, %(zaakinformatie_view_m22)s, %(bag_zaakadres_m22)s, %(pip_can_change_m22)s, %(referential_m22)s, %(is_systeemkenmerk_m22)s, %(required_permissions_m22)s, %(help_extern_m22)s, %(object_metadata_m22)s, %(label_multiple_m22)s, %(properties_m22)s, %(is_group_m22)s), (%(zaaktype_node_id_m23)s, %(zaak_status_id_m23)s, %(bibliotheek_kenmerken_id_m23)s, %(value_mandatory_m23)s, %(label_m23)s, %(help_m23)s, %(pip_m23)s, %(zaakinformatie_view_m23)s, %(bag_zaakadres_m23)s, %(pip_can_change_m23)s, %(referential_m23)s, %(is_systeemkenmerk_m23)s, %(required_permissions_m23)s, %(help_extern_m23)s, %(object_metadata_m23)s, %(label_multiple_m23)s, %(properties_m23)s, %(is_group_m23)s), (%(zaaktype_node_id_m24)s, %(zaak_status_id_m24)s, %(bibliotheek_kenmerken_id_m24)s, %(value_mandatory_m24)s, %(label_m24)s, %(help_m24)s, %(pip_m24)s, %(zaakinformatie_view_m24)s, %(bag_zaakadres_m24)s, %(pip_can_change_m24)s, %(referential_m24)s, %(is_systeemkenmerk_m24)s, %(required_permissions_m24)s, %(help_extern_m24)s, %(object_metadata_m24)s, %(label_multiple_m24)s, %(properties_m24)s, %(is_group_m24)s), (%(zaaktype_node_id_m25)s, %(zaak_status_id_m25)s, %(bibliotheek_kenmerken_id_m25)s, %(value_mandatory_m25)s, %(label_m25)s, %(help_m25)s, %(pip_m25)s, %(zaakinformatie_view_m25)s, %(bag_zaakadres_m25)s, %(pip_can_change_m25)s, %(referential_m25)s, %(is_systeemkenmerk_m25)s, %(required_permissions_m25)s, %(help_extern_m25)s, %(object_metadata_m25)s, %(label_multiple_m25)s, %(properties_m25)s, %(is_group_m25)s), (%(zaaktype_node_id_m26)s, %(zaak_status_id_m26)s, %(bibliotheek_kenmerken_id_m26)s, %(value_mandatory_m26)s, %(label_m26)s, %(help_m26)s, %(pip_m26)s, %(zaakinformatie_view_m26)s, %(bag_zaakadres_m26)s, %(pip_can_change_m26)s, %(referential_m26)s, %(is_systeemkenmerk_m26)s, %(required_permissions_m26)s, %(help_extern_m26)s, %(object_metadata_m26)s, %(label_multiple_m26)s, %(properties_m26)s, %(is_group_m26)s), (%(zaaktype_node_id_m27)s, %(zaak_status_id_m27)s, %(bibliotheek_kenmerken_id_m27)s, %(value_mandatory_m27)s, %(label_m27)s, %(help_m27)s, %(pip_m27)s, %(zaakinformatie_view_m27)s, %(bag_zaakadres_m27)s, %(pip_can_change_m27)s, %(referential_m27)s, %(is_systeemkenmerk_m27)s, %(required_permissions_m27)s, %(help_extern_m27)s, %(object_metadata_m27)s, %(label_multiple_m27)s, %(properties_m27)s, %(is_group_m27)s), (%(zaaktype_node_id_m28)s, %(zaak_status_id_m28)s, %(bibliotheek_kenmerken_id_m28)s, %(value_mandatory_m28)s, %(label_m28)s, %(help_m28)s, %(pip_m28)s, %(zaakinformatie_view_m28)s, %(bag_zaakadres_m28)s, %(pip_can_change_m28)s, %(referential_m28)s, %(is_systeemkenmerk_m28)s, %(required_permissions_m28)s, %(help_extern_m28)s, %(object_metadata_m28)s, %(label_multiple_m28)s, %(properties_m28)s, %(is_group_m28)s), (%(zaaktype_node_id_m29)s, %(zaak_status_id_m29)s, %(bibliotheek_kenmerken_id_m29)s, %(value_mandatory_m29)s, %(label_m29)s, %(help_m29)s, %(pip_m29)s, %(zaakinformatie_view_m29)s, %(bag_zaakadres_m29)s, %(pip_can_change_m29)s, %(referential_m29)s, %(is_systeemkenmerk_m29)s, %(required_permissions_m29)s, %(help_extern_m29)s, %(object_metadata_m29)s, %(label_multiple_m29)s, %(properties_m29)s, %(is_group_m29)s), (%(zaaktype_node_id_m30)s, %(zaak_status_id_m30)s, %(bibliotheek_kenmerken_id_m30)s, %(value_mandatory_m30)s, %(label_m30)s, %(help_m30)s, %(pip_m30)s, %(zaakinformatie_view_m30)s, %(bag_zaakadres_m30)s, %(pip_can_change_m30)s, %(referential_m30)s, %(is_systeemkenmerk_m30)s, %(required_permissions_m30)s, %(help_extern_m30)s, %(object_metadata_m30)s, %(label_multiple_m30)s, %(properties_m30)s, %(is_group_m30)s), (%(zaaktype_node_id_m31)s, %(zaak_status_id_m31)s, %(bibliotheek_kenmerken_id_m31)s, %(value_mandatory_m31)s, %(label_m31)s, %(help_m31)s, %(pip_m31)s, %(zaakinformatie_view_m31)s, %(bag_zaakadres_m31)s, %(pip_can_change_m31)s, %(referential_m31)s, %(is_systeemkenmerk_m31)s, %(required_permissions_m31)s, %(help_extern_m31)s, %(object_metadata_m31)s, %(label_multiple_m31)s, %(properties_m31)s, %(is_group_m31)s), (%(zaaktype_node_id_m32)s, %(zaak_status_id_m32)s, %(bibliotheek_kenmerken_id_m32)s, %(value_mandatory_m32)s, %(label_m32)s, %(help_m32)s, %(pip_m32)s, %(zaakinformatie_view_m32)s, %(bag_zaakadres_m32)s, %(pip_can_change_m32)s, %(referential_m32)s, %(is_systeemkenmerk_m32)s, %(required_permissions_m32)s, %(help_extern_m32)s, %(object_metadata_m32)s, %(label_multiple_m32)s, %(properties_m32)s, %(is_group_m32)s), (%(zaaktype_node_id_m33)s, %(zaak_status_id_m33)s, %(bibliotheek_kenmerken_id_m33)s, %(value_mandatory_m33)s, %(label_m33)s, %(help_m33)s, %(pip_m33)s, %(zaakinformatie_view_m33)s, %(bag_zaakadres_m33)s, %(pip_can_change_m33)s, %(referential_m33)s, %(is_systeemkenmerk_m33)s, %(required_permissions_m33)s, %(help_extern_m33)s, %(object_metadata_m33)s, %(label_multiple_m33)s, %(properties_m33)s, %(is_group_m33)s), (%(zaaktype_node_id_m34)s, %(zaak_status_id_m34)s, %(bibliotheek_kenmerken_id_m34)s, %(value_mandatory_m34)s, %(label_m34)s, %(help_m34)s, %(pip_m34)s, %(zaakinformatie_view_m34)s, %(bag_zaakadres_m34)s, %(pip_can_change_m34)s, %(referential_m34)s, %(is_systeemkenmerk_m34)s, %(required_permissions_m34)s, %(help_extern_m34)s, %(object_metadata_m34)s, %(label_multiple_m34)s, %(properties_m34)s, %(is_group_m34)s), (%(zaaktype_node_id_m35)s, %(zaak_status_id_m35)s, %(bibliotheek_kenmerken_id_m35)s, %(value_mandatory_m35)s, %(label_m35)s, %(help_m35)s, %(pip_m35)s, %(zaakinformatie_view_m35)s, %(bag_zaakadres_m35)s, %(pip_can_change_m35)s, %(referential_m35)s, %(is_systeemkenmerk_m35)s, %(required_permissions_m35)s, %(help_extern_m35)s, %(object_metadata_m35)s, %(label_multiple_m35)s, %(properties_m35)s, %(is_group_m35)s), (%(zaaktype_node_id_m36)s, %(zaak_status_id_m36)s, %(bibliotheek_kenmerken_id_m36)s, %(value_mandatory_m36)s, %(label_m36)s, %(help_m36)s, %(pip_m36)s, %(zaakinformatie_view_m36)s, %(bag_zaakadres_m36)s, %(pip_can_change_m36)s, %(referential_m36)s, %(is_systeemkenmerk_m36)s, %(required_permissions_m36)s, %(help_extern_m36)s, %(object_metadata_m36)s, %(label_multiple_m36)s, %(properties_m36)s, %(is_group_m36)s), (%(zaaktype_node_id_m37)s, %(zaak_status_id_m37)s, %(bibliotheek_kenmerken_id_m37)s, %(value_mandatory_m37)s, %(label_m37)s, %(help_m37)s, %(pip_m37)s, %(zaakinformatie_view_m37)s, %(bag_zaakadres_m37)s, %(pip_can_change_m37)s, %(referential_m37)s, %(is_systeemkenmerk_m37)s, %(required_permissions_m37)s, %(help_extern_m37)s, %(object_metadata_m37)s, %(label_multiple_m37)s, %(properties_m37)s, %(is_group_m37)s), (%(zaaktype_node_id_m38)s, %(zaak_status_id_m38)s, %(bibliotheek_kenmerken_id_m38)s, %(value_mandatory_m38)s, %(label_m38)s, %(help_m38)s, %(pip_m38)s, %(zaakinformatie_view_m38)s, %(bag_zaakadres_m38)s, %(pip_can_change_m38)s, %(referential_m38)s, %(is_systeemkenmerk_m38)s, %(required_permissions_m38)s, %(help_extern_m38)s, %(object_metadata_m38)s, %(label_multiple_m38)s, %(properties_m38)s, %(is_group_m38)s), (%(zaaktype_node_id_m39)s, %(zaak_status_id_m39)s, %(bibliotheek_kenmerken_id_m39)s, %(value_mandatory_m39)s, %(label_m39)s, %(help_m39)s, %(pip_m39)s, %(zaakinformatie_view_m39)s, %(bag_zaakadres_m39)s, %(pip_can_change_m39)s, %(referential_m39)s, %(is_systeemkenmerk_m39)s, %(required_permissions_m39)s, %(help_extern_m39)s, %(object_metadata_m39)s, %(label_multiple_m39)s, %(properties_m39)s, %(is_group_m39)s), (%(zaaktype_node_id_m40)s, %(zaak_status_id_m40)s, %(bibliotheek_kenmerken_id_m40)s, %(value_mandatory_m40)s, %(label_m40)s, %(help_m40)s, %(pip_m40)s, %(zaakinformatie_view_m40)s, %(bag_zaakadres_m40)s, %(pip_can_change_m40)s, %(referential_m40)s, %(is_systeemkenmerk_m40)s, %(required_permissions_m40)s, %(help_extern_m40)s, %(object_metadata_m40)s, %(label_multiple_m40)s, %(properties_m40)s, %(is_group_m40)s), (%(zaaktype_node_id_m41)s, %(zaak_status_id_m41)s, %(bibliotheek_kenmerken_id_m41)s, %(value_mandatory_m41)s, %(label_m41)s, %(help_m41)s, %(pip_m41)s, %(zaakinformatie_view_m41)s, %(bag_zaakadres_m41)s, %(pip_can_change_m41)s, %(referential_m41)s, %(is_systeemkenmerk_m41)s, %(required_permissions_m41)s, %(help_extern_m41)s, %(object_metadata_m41)s, %(label_multiple_m41)s, %(properties_m41)s, %(is_group_m41)s), (%(zaaktype_node_id_m42)s, %(zaak_status_id_m42)s, %(bibliotheek_kenmerken_id_m42)s, %(value_mandatory_m42)s, %(label_m42)s, %(help_m42)s, %(pip_m42)s, %(zaakinformatie_view_m42)s, %(bag_zaakadres_m42)s, %(pip_can_change_m42)s, %(referential_m42)s, %(is_systeemkenmerk_m42)s, %(required_permissions_m42)s, %(help_extern_m42)s, %(object_metadata_m42)s, %(label_multiple_m42)s, %(properties_m42)s, %(is_group_m42)s), (%(zaaktype_node_id_m43)s, %(zaak_status_id_m43)s, %(bibliotheek_kenmerken_id_m43)s, %(value_mandatory_m43)s, %(label_m43)s, %(help_m43)s, %(pip_m43)s, %(zaakinformatie_view_m43)s, %(bag_zaakadres_m43)s, %(pip_can_change_m43)s, %(referential_m43)s, %(is_systeemkenmerk_m43)s, %(required_permissions_m43)s, %(help_extern_m43)s, %(object_metadata_m43)s, %(label_multiple_m43)s, %(properties_m43)s, %(is_group_m43)s) RETURNING zaaktype_kenmerken.id, zaaktype_kenmerken.bibliotheek_kenmerken_id, zaaktype_kenmerken.properties",
            28: "UPDATE zaaktype_kenmerken SET properties=%(properties)s WHERE zaaktype_kenmerken.id = %(id_1)s",
            29: "UPDATE zaaktype_kenmerken SET properties=%(properties)s WHERE zaaktype_kenmerken.id = %(id_1)s",
            30: "INSERT INTO zaaktype_resultaten (zaaktype_node_id, zaaktype_status_id, resultaat, ingang, bewaartermijn, label, selectielijst, archiefnominatie, comments, trigger_archival, selectielijst_brondatum, selectielijst_einddatum, properties, standaard_keuze) VALUES (%(zaaktype_node_id_m0)s, %(zaaktype_status_id_m0)s, %(resultaat_m0)s, %(ingang_m0)s, %(bewaartermijn_m0)s, %(label_m0)s, %(selectielijst_m0)s, %(archiefnominatie_m0)s, %(comments_m0)s, %(trigger_archival_m0)s, %(selectielijst_brondatum_m0)s, %(selectielijst_einddatum_m0)s, %(properties_m0)s, %(standaard_keuze_m0)s)",
            31: "INSERT INTO zaaktype_status_checklist_item (casetype_status_id, label) VALUES (%(casetype_status_id_m0)s, %(label_m0)s), (%(casetype_status_id_m1)s, %(label_m1)s), (%(casetype_status_id_m2)s, %(label_m2)s)",
            32: "INSERT INTO zaaktype_sjablonen (zaaktype_node_id, zaak_status_id, bibliotheek_sjablonen_id, bibliotheek_kenmerken_id, automatisch_genereren, target_format, help, custom_filename, label, allow_edit) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_sjablonen_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(automatisch_genereren_m0)s, %(target_format_m0)s, %(help_m0)s, %(custom_filename_m0)s, %(label_m0)s, %(allow_edit_m0)s)",
            33: "INSERT INTO zaaktype_sjablonen (zaaktype_node_id, zaak_status_id, bibliotheek_sjablonen_id, bibliotheek_kenmerken_id, automatisch_genereren, target_format, help, custom_filename, label, allow_edit) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_sjablonen_id_m0)s, %(bibliotheek_kenmerken_id_m0)s, %(automatisch_genereren_m0)s, %(target_format_m0)s, %(help_m0)s, %(custom_filename_m0)s, %(label_m0)s, %(allow_edit_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_sjablonen_id_m1)s, %(bibliotheek_kenmerken_id_m1)s, %(automatisch_genereren_m1)s, %(target_format_m1)s, %(help_m1)s, %(custom_filename_m1)s, %(label_m1)s, %(allow_edit_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_sjablonen_id_m2)s, %(bibliotheek_kenmerken_id_m2)s, %(automatisch_genereren_m2)s, %(target_format_m2)s, %(help_m2)s, %(custom_filename_m2)s, %(label_m2)s, %(allow_edit_m2)s)",
            34: "SELECT subject.id AS id \nFROM subject \nWHERE subject.uuid = %(uuid_1)s::UUID",
            35: "INSERT INTO zaaktype_notificatie (zaaktype_node_id, zaak_status_id, bibliotheek_notificaties_id, rcpt, email, behandelaar, automatic, cc, bcc, betrokkene_role) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(bibliotheek_notificaties_id_m0)s, %(rcpt_m0)s, %(email_m0)s, %(behandelaar_m0)s, %(automatic_m0)s, %(cc_m0)s, %(bcc_m0)s, %(betrokkene_role_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(bibliotheek_notificaties_id_m1)s, %(rcpt_m1)s, %(email_m1)s, %(behandelaar_m1)s, %(automatic_m1)s, %(cc_m1)s, %(bcc_m1)s, %(betrokkene_role_m1)s), (%(zaaktype_node_id_m2)s, %(zaak_status_id_m2)s, %(bibliotheek_notificaties_id_m2)s, %(rcpt_m2)s, %(email_m2)s, %(behandelaar_m2)s, %(automatic_m2)s, %(cc_m2)s, %(bcc_m2)s, %(betrokkene_role_m2)s), (%(zaaktype_node_id_m3)s, %(zaak_status_id_m3)s, %(bibliotheek_notificaties_id_m3)s, %(rcpt_m3)s, %(email_m3)s, %(behandelaar_m3)s, %(automatic_m3)s, %(cc_m3)s, %(bcc_m3)s, %(betrokkene_role_m3)s), (%(zaaktype_node_id_m4)s, %(zaak_status_id_m4)s, %(bibliotheek_notificaties_id_m4)s, %(rcpt_m4)s, %(email_m4)s, %(behandelaar_m4)s, %(automatic_m4)s, %(cc_m4)s, %(bcc_m4)s, %(betrokkene_role_m4)s), (%(zaaktype_node_id_m5)s, %(zaak_status_id_m5)s, %(bibliotheek_notificaties_id_m5)s, %(rcpt_m5)s, %(email_m5)s, %(behandelaar_m5)s, %(automatic_m5)s, %(cc_m5)s, %(bcc_m5)s, %(betrokkene_role_m5)s), (%(zaaktype_node_id_m6)s, %(zaak_status_id_m6)s, %(bibliotheek_notificaties_id_m6)s, %(rcpt_m6)s, %(email_m6)s, %(behandelaar_m6)s, %(automatic_m6)s, %(cc_m6)s, %(bcc_m6)s, %(betrokkene_role_m6)s)",
            36: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            37: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            38: "INSERT INTO zaaktype_relatie (zaaktype_node_id, zaaktype_status_id, relatie_zaaktype_id, relatie_type, eigenaar_type, start_delay, status, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, subject_role, copy_subject_role, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_role, eigenaar_id, show_in_pip, pip_label, copy_related_cases, copy_related_objects, copy_selected_attributes, creation_style) VALUES (%(zaaktype_node_id_m0)s, %(zaaktype_status_id_m0)s, %(relatie_zaaktype_id_m0)s, %(relatie_type_m0)s, %(eigenaar_type_m0)s, %(start_delay_m0)s, %(status_m0)s, %(kopieren_kenmerken_m0)s, %(ou_id_m0)s, %(role_id_m0)s, %(automatisch_behandelen_m0)s, %(required_m0)s, %(subject_role_m0)s::VARCHAR[], %(copy_subject_role_m0)s, %(betrokkene_authorized_m0)s, %(betrokkene_notify_m0)s, %(betrokkene_id_m0)s, %(betrokkene_role_m0)s, %(betrokkene_role_set_m0)s, %(betrokkene_prefix_m0)s, %(eigenaar_role_m0)s, %(eigenaar_id_m0)s, %(show_in_pip_m0)s, %(pip_label_m0)s, %(copy_related_cases_m0)s, %(copy_related_objects_m0)s, %(copy_selected_attributes_m0)s, %(creation_style_m0)s)",
            39: "INSERT INTO zaaktype_relatie (zaaktype_node_id, zaaktype_status_id, relatie_zaaktype_id, relatie_type, eigenaar_type, start_delay, status, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, subject_role, copy_subject_role, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_role, eigenaar_id, show_in_pip, pip_label, copy_related_cases, copy_related_objects, copy_selected_attributes, creation_style) VALUES (%(zaaktype_node_id_m0)s, %(zaaktype_status_id_m0)s, %(relatie_zaaktype_id_m0)s, %(relatie_type_m0)s, %(eigenaar_type_m0)s, %(start_delay_m0)s, %(status_m0)s, %(kopieren_kenmerken_m0)s, %(ou_id_m0)s, %(role_id_m0)s, %(automatisch_behandelen_m0)s, %(required_m0)s, %(subject_role_m0)s::VARCHAR[], %(copy_subject_role_m0)s, %(betrokkene_authorized_m0)s, %(betrokkene_notify_m0)s, %(betrokkene_id_m0)s, %(betrokkene_role_m0)s, %(betrokkene_role_set_m0)s, %(betrokkene_prefix_m0)s, %(eigenaar_role_m0)s, %(eigenaar_id_m0)s, %(show_in_pip_m0)s, %(pip_label_m0)s, %(copy_related_cases_m0)s, %(copy_related_objects_m0)s, %(copy_selected_attributes_m0)s, %(creation_style_m0)s), (%(zaaktype_node_id_m1)s, %(zaaktype_status_id_m1)s, %(relatie_zaaktype_id_m1)s, %(relatie_type_m1)s, %(eigenaar_type_m1)s, %(start_delay_m1)s, %(status_m1)s, %(kopieren_kenmerken_m1)s, %(ou_id_m1)s, %(role_id_m1)s, %(automatisch_behandelen_m1)s, %(required_m1)s, %(subject_role_m1)s::VARCHAR[], %(copy_subject_role_m1)s, %(betrokkene_authorized_m1)s, %(betrokkene_notify_m1)s, %(betrokkene_id_m1)s, %(betrokkene_role_m1)s, %(betrokkene_role_set_m1)s, %(betrokkene_prefix_m1)s, %(eigenaar_role_m1)s, %(eigenaar_id_m1)s, %(show_in_pip_m1)s, %(pip_label_m1)s, %(copy_related_cases_m1)s, %(copy_related_objects_m1)s, %(copy_selected_attributes_m1)s, %(creation_style_m1)s)",
            40: "SELECT natuurlijk_persoon.id AS id \nFROM natuurlijk_persoon \nWHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID",
            41: "SELECT bedrijf.id AS id \nFROM bedrijf \nWHERE bedrijf.uuid = %(uuid_1)s::UUID",
            42: "INSERT INTO zaaktype_standaard_betrokkenen (zaaktype_node_id, zaak_status_id, betrokkene_type, betrokkene_identifier, naam, rol, magic_string_prefix, gemachtigd, notify) VALUES (%(zaaktype_node_id_m0)s, %(zaak_status_id_m0)s, %(betrokkene_type_m0)s, %(betrokkene_identifier_m0)s, %(naam_m0)s, %(rol_m0)s, %(magic_string_prefix_m0)s, %(gemachtigd_m0)s, %(notify_m0)s), (%(zaaktype_node_id_m1)s, %(zaak_status_id_m1)s, %(betrokkene_type_m1)s, %(betrokkene_identifier_m1)s, %(naam_m1)s, %(rol_m1)s, %(magic_string_prefix_m1)s, %(gemachtigd_m1)s, %(notify_m1)s)",
            43: "UPDATE zaaktype_node SET id=%(id)s WHERE zaaktype_node.id = %(id_1)s",
        }
        assert len(call_list) == 44
        for query_index in range(44):
            compiled_query = str(
                call_list[query_index][0][0].compile(
                    dialect=postgresql.dialect()
                )
            )
            assert compiled_query == queries[query_index]

    def test_update_versioned_casetype_no_change_log(self, versioned_casetype):
        casetype_uuid = uuid4()
        casetype_version_uuid = uuid4()

        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.update_versioned_casetype(
                casetype_uuid=casetype_uuid,
                casetype_version_uuid=casetype_version_uuid,
                active=versioned_casetype.active,
                general_attributes=versioned_casetype.general_attributes,
                documentation=versioned_casetype.documentation,
                relations=versioned_casetype.relations,
                webform=versioned_casetype.webform,
                registrationform=versioned_casetype.registrationform,
                case_dossier=versioned_casetype.case_dossier,
                api=versioned_casetype.api,
                authorization=versioned_casetype.authorization,
                child_casetype_settings=versioned_casetype.child_casetype_settings,
                phases=versioned_casetype.phases,
                results=versioned_casetype.results,
            )
        assert (
            "1 validation error for ChangeLog\nupdate_components\n  field required (type=value_error.missing)"
            in str(excinfo.value)
        )
