# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import catalog


class TestAs_A_User_I_Want_To_Change_Version_of_Case_type(TestBase):
    def setup_method(self):
        self.load_command_instance(catalog)

        # mocking get case_type_node_id
        mock_get_case_type_node_id = mock.MagicMock()
        mock_get_case_type_node_id.fetchone.return_value = (2,)

        # mocking get and insert case_type_defintion
        mock_get_case_type_definition = mock.MagicMock()
        mock_case_type_definition = mock.MagicMock()
        mock_case_type_definition.configure_mock(
            openbaarheid="openbaarheid",
            handelingsinitiator="handelingsinitiator",
            grondslag="grondslag",
            procesbeschrijving="procesbeschrijving",
            afhandeltermijn="afhandeltermijn",
            afhandeltermijn_type="afhandeltermijn_type",
            selectielijst="selectielijst",
            servicenorm="servicenorm",
            servicenorm_type="servicenorm_type",
            pdc_voorwaarden="pdc_voorwaarden",
            pdc_meenemen="pdc_meenemen",
            pdc_description="pdc_description",
            pdc_tarief="pdc_tarief",
            omschrijving_upl="omschrijving_upl",
            aard="aard",
            preset_client="preset_client",
            extra_informatie="extra_informatie",
            extra_informatie_extern="extra_informatie_extern",
        )
        mock_get_case_type_definition.fetchone.return_value = (
            mock_case_type_definition
        )
        mock_insert_case_type_defintion = mock.MagicMock()

        # mocking get case_type_latest_version
        mock_get_case_type_latest_version = mock.MagicMock()
        mock_get_case_type_latest_version.fetchone.return_value = (5,)

        # mocking get and insert case_type_node
        mock_get_case_type_node = mock.MagicMock()
        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(
            zaaktype_id="zaaktype_id",
            created="created",
            last_modified="last_modified",
            zaaktype_definitie_id="zaaktype_definitie_id",
            titel="titel",
            code="code",
            trigger="trigger",
            version="version",
            active="active",
            properties="properties",
            is_public="is_public",
            zaaktype_trefwoorden="zaaktype_trefwoorden",
            zaaktype_omschrijving="zaaktype_omschrijving",
            webform_toegang="webform_toegang",
            webform_authenticatie="webform_authenticatie",
            adres_relatie="adres_relatie",
            aanvrager_hergebruik="aanvrager_hergebruik",
            automatisch_behandelen="automatisch_behandelen",
            toewijzing_zaakintake="toewijzing_zaakintake",
            toelichting="toelichting",
            online_betaling="online_betaling",
            adres_andere_locatie="adres_andere_locatie",
            adres_aanvrager="adres_aanvrager",
            bedrijfid_wijzigen="bedrijfid_wijzigen",
            zaaktype_vertrouwelijk="zaaktype_vertrouwelijk",
            extra_relaties_in_aanvraag="extra_relaties_in_aanvraag",
            contact_info_intake="contact_info_intake",
            prevent_pip="prevent_pip",
            contact_info_email_required="contact_info_email_required",
            contact_info_phone_required="contact_info_phone_required",
            contact_info_mobile_phone_required="contact_info_mobile_phone_required",
            moeder_zaaktype_id="moeder_zaaktype_id",
            logging_id="logging_id",
        )
        mock_get_case_type_node.fetchone.return_value = mock_case_type_node
        mock_insert_case_type_node = mock.MagicMock()

        # mocking get and insert case_type_subjects
        mock_get_case_type_subjects = mock.MagicMock()
        mock_case_type_subject = mock.MagicMock()
        mock_case_type_subject.configure_mock(
            zaaktype_node_id="zaaktype_node_id",
            betrokkene_type="betrokkene_type",
            created="created",
            last_modified="last_modified",
        )
        mock_get_case_type_subjects.fetchall.return_value = [
            mock_case_type_subject
        ]
        mock_insert_case_type_subject = mock.MagicMock()

        # mocking get and insert case_type_statuses
        mock_get_case_type_statuses = mock.MagicMock()
        mock_case_type_status = mock.MagicMock()
        mock_case_type_status.configure_mock(
            created="created",
            last_modified="last_modified",
            zaaktype_node_id="zaaktype_node_id",
            status="status",
            status_type="status_type",
            naam="naam",
            fase="fase",
            ou_id="ou_id",
            role_id="role_id",
            checklist="checklist",
            role_set="role_set",
        )
        mock_get_case_type_statuses.fetchall.return_value = [
            mock_case_type_status
        ]
        mock_insert_case_type_status = mock.MagicMock()
        # mocking get and insert case_type_checklist_item
        mock_get_case_type_checklist_items = mock.MagicMock()
        mock_case_type_checklist_item = mock.MagicMock()
        mock_case_type_checklist_item.configure_mock(
            created="created",
            last_modified="last_modified",
            zaaktype_node_id="zaaktype_node_id",
            status="status",
            status_type="status_type",
            naam="naam",
            fase="fase",
            ou_id="ou_id",
            role_id="role_id",
            checklist="checklist",
            role_set="role_set",
        )
        mock_get_case_type_checklist_items.fetchall.return_value = [
            mock_case_type_checklist_item
        ]
        mock_insert_case_type_checklist_item = mock.MagicMock()

        # mocking get and insert case_type_attributes
        mock_get_case_type_attributes = mock.MagicMock()
        mock_case_type_attribute = mock.MagicMock()
        mock_case_type_attribute.configure_mock(
            created="created",
            last_modified="last_modified",
            zaaktype_node_id="zaaktype_node_id",
            zaak_status_id="zaak_status_id",
            bibliotheek_kenmerken_id="bibliotheek_kenmerken_id",
            object_id="object_id",
            value_mandatory="value_mandatory",
            label="label",
            help="help",
            pip="pip",
            zaakinformatie_view="zaakinformatie_view",
            bag_zaakadres="bag_zaakadres",
            value_default="value_default",
            pip_can_change="pip_can_change",
            publish_public="publish_public",
            referential="referential",
            is_systeemkenmerk="is_systeemkenmerk",
            required_permissions="required_permissions",
            label_multiple="label_multiple",
            version="version",
            help_extern="help_extern",
            object_metadata="object_metadata",
            properties="properties",
            is_group="is_group",
        )
        mock_get_case_type_attributes.fetchall.return_value = [
            mock_case_type_attribute
        ]
        mock_insert_case_type_attribute = mock.MagicMock()

        # mocking get and insert case_type_notifications
        mock_get_case_type_notifications = mock.MagicMock()
        mock_case_type_notification = mock.MagicMock()
        mock_case_type_notification.configure_mock(
            created="created",
            last_modified="last_modified",
            zaaktype_node_id="zaaktype_node_id",
            zaak_status_id="zaak_status_id",
            bibliotheek_notificaties_id="bibliotheek_notificaties_id",
            label="label",
            rcpt="rcpt",
            onderwerp="onderwerp",
            bericht="bericht",
            intern_block="intern_block",
            email="email",
            behandelaar="behandelaar",
            automatic="automatic",
            cc="cc",
            bcc="bcc",
            betrokkene_role="betrokkene_role",
        )
        mock_get_case_type_notifications.fetchall.return_value = [
            mock_case_type_notification
        ]
        mock_insert_case_type_notification = mock.MagicMock()

        # mocking get and insert case_type_templates
        mock_get_case_type_templates = mock.MagicMock()
        mock_case_type_template = mock.MagicMock()
        mock_case_type_template.configure_mock(
            created="created",
            last_modified="last_modified",
            zaaktype_node_id="zaaktype_node_id",
            zaak_status_id="zaak_status_id",
            bibliotheek_sjablonen_id="bibliotheek_sjablonen_id",
            bibliotheek_kenmerken_id="bibliotheek_kenmerken_id",
            automatisch_genereren="automatisch_genereren",
            target_format="target_format",
            help="help",
            custom_filename="test",
            label="test_label",
        )
        mock_get_case_type_templates.fetchall.return_value = [
            mock_case_type_template
        ]
        mock_insert_case_type_template = mock.MagicMock()

        # mocking get and insert case_type_results
        mock_get_case_type_results = mock.MagicMock()
        mock_case_type_result = mock.MagicMock()
        mock_case_type_result.configure_mock(
            zaaktype_node_id="zaaktype_node_id",
            zaaktype_status_id="zaaktype_status_id",
            resultaat="resultaat",
            ingang="ingang",
            bewaartermijn="bewaartermijn",
            created="created",
            last_modified="last_modified",
            dossiertype="dossiertype",
            label="label",
            selectielijst="selectielijst",
            archiefnominatie="archiefnominatie",
            comments="comments",
            external_reference="external_reference",
            trigger_archival="trigger_archival",
            selectielijst_brondatum="selectielijst_brondatum",
            selectielijst_einddatum="selectielijst_einddatum",
            properties="properties",
            standaard_keuze="standaard_keuze",
        )
        mock_get_case_type_results.fetchall.return_value = [
            mock_case_type_result
        ]
        mock_insert_case_type_result = mock.MagicMock()

        # mocking get and insert case_type_rules
        mock_get_case_type_rules = mock.MagicMock()
        mock_case_type_rule = mock.MagicMock()
        mock_case_type_rule.configure_mock(
            zaaktype_node_id="zaaktype_node_id",
            zaak_status_id="zaak_status_id",
            naam="naam",
            created="created",
            last_modified="last_modified",
            settings="settings",
            active="active",
            is_group="is_group",
        )
        mock_get_case_type_rules.fetchall.return_value = [
            mock_case_type_attribute
        ]
        mock_insert_case_type_rule = mock.MagicMock()

        # mocking get and insert case_type_relations
        mock_get_case_type_relations = mock.MagicMock()
        mock_case_type_relation = mock.MagicMock()
        mock_case_type_relation.configure_mock(
            created="created",
            last_modified="last_modified",
            zaaktype_node_id="zaaktype_node_id",
            zaaktype_status_id="zaaktype_status_id",
            relatie_zaaktype_id="relatie_zaaktype_id",
            relatie_type="relatie_type",
            start_delay="start_delay",
            status="status",
            kopieren_kenmerken="kopieren_kenmerken",
            ou_id="ou_id",
            role_id="role_id",
            automatisch_behandelen="automatisch_behandelen",
            required="required",
            subject_role="subject_role",
            copy_subject_role="copy_subject_role",
            betrokkene_authorized="betrokkene_authorized",
            betrokkene_notify="betrokkene_notify",
            betrokkene_id="betrokkene_id",
            betrokkene_role="betrokkene_role",
            betrokkene_role_set="betrokkene_role_set",
            betrokkene_prefix="betrokkene_prefix",
            eigenaar_type="eigenaar_type",
            eigenaar_role="eigenaar_role",
            eigenaar_id="eigenaar_id",
            show_in_pip="show_in_pip",
            pip_label="pip_label",
        )
        mock_get_case_type_relations.fetchall.return_value = [
            mock_case_type_attribute
        ]
        mock_insert_case_type_relation = mock.MagicMock()

        # mocking get and insert mock_case_type_standard_subjects
        mock_get_case_type_standard_subjects = mock.MagicMock()
        mock_case_type_standard_subject = mock.MagicMock()
        mock_case_type_standard_subject.configure_mock(
            zaaktype_node_id="zaaktype_node_id",
            zaak_status_id="zaak_status_id",
            betrokkene_type="betrokkene_type",
            betrokkene_identifier="betrokkene_identifier",
            naam="naam",
            rol="rol",
            magic_string_prefix="magic_string_prefix",
            gemachtigd="gemachtigd",
            notify="notify",
        )
        mock_get_case_type_standard_subjects.fetchall.return_value = [
            mock_case_type_standard_subject
        ]
        mock_insert_case_type_standard_subject = mock.MagicMock()

        # mocking get and insert mock_case_type_standard_subjects
        mock_get_case_type_object_mutations = mock.MagicMock()
        mock_case_type_object_mutation = mock.MagicMock()
        mock_case_type_object_mutation.configure_mock(
            zaaktype_node_id="zaaktype_node_id",
            zaaktype_status_id="zaaktype_status_id",
            attribute_uuid="attribute_uuid",
            mutation_type="create",
            object_title="test_object",
            phase_transition=False,
            system_attribute_changes={},
            attribute_mapping='{"subject": "", "basic_date"": "", "read_list_single_value": ""}',
        )
        mock_get_case_type_object_mutations.fetchall.return_value = [
            mock_case_type_object_mutation
        ]
        mock_insert_case_type_object_mutation = mock.MagicMock()

        # mocking get_new_case_type_node
        mock_get_new_case_type_node = mock.MagicMock()
        mock_new_case_type_node = mock.MagicMock()
        mock_new_case_type_node.configure_mock(
            id=123,
            zaaktype_id="zaaktype_id",
        )
        mock_get_new_case_type_node.return_value = mock_new_case_type_node

        # mocking update case_type
        mock_update_case_type = mock.MagicMock()

        mock_rows = [
            mock_get_case_type_node_id,
            mock_get_case_type_definition,
            mock_insert_case_type_defintion,
            mock_get_case_type_node,
            mock_get_case_type_latest_version,
            mock_insert_case_type_node,
            mock_get_case_type_subjects,
            mock_insert_case_type_subject,
            mock_get_case_type_statuses,
            mock_insert_case_type_status,
            mock_get_case_type_checklist_items,
            mock_insert_case_type_checklist_item,
            mock_get_case_type_attributes,
            mock_insert_case_type_attribute,
            mock_get_case_type_notifications,
            mock_insert_case_type_notification,
            mock_get_case_type_templates,
            mock_insert_case_type_template,
            mock_get_case_type_results,
            mock_insert_case_type_result,
            mock_get_case_type_rules,
            mock_insert_case_type_rule,
            mock_get_case_type_relations,
            mock_insert_case_type_relation,
            mock_get_case_type_standard_subjects,
            mock_insert_case_type_standard_subject,
            mock_get_case_type_object_mutations,
            mock_insert_case_type_object_mutation,
            mock_get_new_case_type_node,
            mock_update_case_type,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

    def test_change_version_of_case_type(self):
        case_type_uuid = uuid4()
        current_version_uuid = uuid4()
        mock_result = [mock.MagicMock()] * 8
        mock_result[4] = "internextern"
        self.session.query().join().filter().filter().filter().one.return_value = mock_result

        mock_result[0].configure_mock(
            id=1,
            uuid=case_type_uuid,
            name="test_name",
            active=True,
            identification=123,
            current_version=10,
            last_modified="2019-12-21 23:50:43",
            current_version_uuid=current_version_uuid,
        )
        self.cmd.update_case_type_version(
            case_type_uuid=str(case_type_uuid),
            version_uuid=str(current_version_uuid),
            reason="for testing",
        )
        self.assert_has_event_name("CaseTypeVersionUpdated")

        assert len(self.execute_queries) == 30

        select_case_type_node_id_query = self.execute_queries[0]
        compiled = select_case_type_node_id_query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(compiled) == "SELECT zaaktype_node.id \n"
            "FROM zaaktype_node \n"
            "WHERE zaaktype_node.uuid = %(uuid_1)s::UUID"
        )

        select_case_type_definition_query = self.execute_queries[1]
        assert (
            str(select_case_type_definition_query)
            == "SELECT zaaktype_definitie.openbaarheid, zaaktype_definitie.handelingsinitiator, zaaktype_definitie.grondslag, zaaktype_definitie.procesbeschrijving, zaaktype_definitie.afhandeltermijn, zaaktype_definitie.afhandeltermijn_type, zaaktype_definitie.selectielijst, zaaktype_definitie.servicenorm, zaaktype_definitie.servicenorm_type, zaaktype_definitie.pdc_voorwaarden, zaaktype_definitie.pdc_meenemen, zaaktype_definitie.pdc_description, zaaktype_definitie.pdc_tarief, zaaktype_definitie.omschrijving_upl, zaaktype_definitie.aard, zaaktype_definitie.preset_client, zaaktype_definitie.extra_informatie, zaaktype_definitie.extra_informatie_extern \n"
            "FROM zaaktype_definitie, zaaktype_node \n"
            "WHERE zaaktype_definitie.id = zaaktype_node.zaaktype_definitie_id AND zaaktype_node.id = :id_1"
        )

        insert_case_type_definition_command = self.execute_queries[2]
        assert (
            str(insert_case_type_definition_command)
            == "INSERT INTO zaaktype_definitie (openbaarheid, handelingsinitiator, grondslag, procesbeschrijving, afhandeltermijn, afhandeltermijn_type, selectielijst, servicenorm, servicenorm_type, pdc_voorwaarden, pdc_description, pdc_meenemen, pdc_tarief, omschrijving_upl, aard, preset_client, extra_informatie, extra_informatie_extern) VALUES (:openbaarheid, :handelingsinitiator, :grondslag, :procesbeschrijving, :afhandeltermijn, :afhandeltermijn_type, :selectielijst, :servicenorm, :servicenorm_type, :pdc_voorwaarden, :pdc_description, :pdc_meenemen, :pdc_tarief, :omschrijving_upl, :aard, :preset_client, :extra_informatie, :extra_informatie_extern)"
        )

        select_case_type_node_query = self.execute_queries[3]
        assert (
            str(select_case_type_node_query)
            == "SELECT zaaktype_node.zaaktype_id, zaaktype_node.titel, zaaktype_node.code, zaaktype_node.trigger, zaaktype_node.active, zaaktype_node.properties, zaaktype_node.is_public, zaaktype_node.zaaktype_trefwoorden, zaaktype_node.zaaktype_omschrijving, zaaktype_node.webform_toegang, zaaktype_node.webform_authenticatie, zaaktype_node.adres_relatie, zaaktype_node.aanvrager_hergebruik, zaaktype_node.automatisch_behandelen, zaaktype_node.toewijzing_zaakintake, zaaktype_node.toelichting, zaaktype_node.online_betaling, zaaktype_node.adres_andere_locatie, zaaktype_node.adres_aanvrager, zaaktype_node.bedrijfid_wijzigen, zaaktype_node.zaaktype_vertrouwelijk, zaaktype_node.extra_relaties_in_aanvraag, zaaktype_node.contact_info_intake, zaaktype_node.prevent_pip, zaaktype_node.contact_info_email_required, zaaktype_node.contact_info_phone_required, zaaktype_node.contact_info_mobile_phone_required, zaaktype_node.moeder_zaaktype_id, zaaktype_node.logging_id \n"
            "FROM zaaktype_node \n"
            "WHERE zaaktype_node.id = :id_1"
        )

        select_case_type_latest_version_query = self.execute_queries[4]
        assert (
            str(select_case_type_latest_version_query)
            == "SELECT zaaktype_node.version \n"
            "FROM zaaktype_node \n"
            "WHERE zaaktype_node.zaaktype_id = :zaaktype_id_1 ORDER BY zaaktype_node.version DESC"
        )

        insert_case_type_node_command = self.execute_queries[5]
        assert (
            str(insert_case_type_node_command)
            == "INSERT INTO zaaktype_node (uuid, zaaktype_id, zaaktype_definitie_id, created, last_modified, titel, code, trigger, version, active, properties, is_public, zaaktype_trefwoorden, zaaktype_omschrijving, webform_toegang, webform_authenticatie, adres_relatie, aanvrager_hergebruik, automatisch_behandelen, toewijzing_zaakintake, toelichting, online_betaling, adres_andere_locatie, adres_aanvrager, adres_geojson, bedrijfid_wijzigen, zaaktype_vertrouwelijk, extra_relaties_in_aanvraag, contact_info_intake, prevent_pip, contact_info_email_required, contact_info_phone_required, contact_info_mobile_phone_required, moeder_zaaktype_id, logging_id) VALUES (:uuid, :zaaktype_id, :zaaktype_definitie_id, :created, :last_modified, :titel, :code, :trigger, :version, :active, :properties, :is_public, :zaaktype_trefwoorden, :zaaktype_omschrijving, :webform_toegang, :webform_authenticatie, :adres_relatie, :aanvrager_hergebruik, :automatisch_behandelen, :toewijzing_zaakintake, :toelichting, :online_betaling, :adres_andere_locatie, :adres_aanvrager, :adres_geojson, :bedrijfid_wijzigen, :zaaktype_vertrouwelijk, :extra_relaties_in_aanvraag, :contact_info_intake, :prevent_pip, :contact_info_email_required, :contact_info_phone_required, :contact_info_mobile_phone_required, :moeder_zaaktype_id, :logging_id)"
        )

        select_case_type_subjects_query = self.execute_queries[6]
        assert (
            str(select_case_type_subjects_query)
            == "SELECT zaaktype_betrokkenen.betrokkene_type \n"
            "FROM zaaktype_betrokkenen \n"
            "WHERE zaaktype_betrokkenen.zaaktype_node_id = :zaaktype_node_id_1"
        )

        insert_case_type_subject_command = self.execute_queries[7]
        assert (
            str(insert_case_type_subject_command)
            == "INSERT INTO zaaktype_betrokkenen (zaaktype_node_id, betrokkene_type, created, last_modified) VALUES (:zaaktype_node_id, :betrokkene_type, :created, :last_modified)"
        )

        select_case_type_statuses_query = self.execute_queries[8]
        assert (
            str(select_case_type_statuses_query)
            == "SELECT zaaktype_status.id, zaaktype_status.status, zaaktype_status.status_type, zaaktype_status.naam, zaaktype_status.fase, zaaktype_status.ou_id, zaaktype_status.role_id, zaaktype_status.checklist, zaaktype_status.role_set, zaaktype_status.termijn \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = :zaaktype_node_id_1"
        )

        insert_case_status_command = self.execute_queries[9]
        assert (
            str(insert_case_status_command)
            == "INSERT INTO zaaktype_status (created, last_modified, zaaktype_node_id, status, status_type, termijn, naam, fase, ou_id, role_id, checklist, role_set) VALUES (:created, :last_modified, :zaaktype_node_id, :status, :status_type, :termijn, :naam, :fase, :ou_id, :role_id, :checklist, :role_set)"
        )
        select_case_type_checklist_item_query = self.execute_queries[10]
        assert (
            str(select_case_type_checklist_item_query)
            == "SELECT zaaktype_status_checklist_item.label, zaaktype_status_checklist_item.external_reference \n"
            "FROM zaaktype_status_checklist_item \n"
            "WHERE zaaktype_status_checklist_item.casetype_status_id = :casetype_status_id_1 ORDER BY zaaktype_status_checklist_item.id"
        )

        insert_case_type_checklist_item_query = self.execute_queries[11]
        assert (
            str(insert_case_type_checklist_item_query)
            == "INSERT INTO zaaktype_status_checklist_item (casetype_status_id, label, external_reference) VALUES (:casetype_status_id, :label, :external_reference)"
        )

        select_case_type_attributes_query = self.execute_queries[12]
        assert (
            str(select_case_type_attributes_query)
            == "SELECT zaaktype_kenmerken.bibliotheek_kenmerken_id, zaaktype_kenmerken.object_id, zaaktype_kenmerken.value_mandatory, zaaktype_kenmerken.label, zaaktype_kenmerken.help, zaaktype_kenmerken.pip, zaaktype_kenmerken.zaakinformatie_view, zaaktype_kenmerken.bag_zaakadres, zaaktype_kenmerken.value_default, zaaktype_kenmerken.pip_can_change, zaaktype_kenmerken.publish_public, zaaktype_kenmerken.referential, zaaktype_kenmerken.is_systeemkenmerk, zaaktype_kenmerken.required_permissions, zaaktype_kenmerken.version, zaaktype_kenmerken.help_extern, zaaktype_kenmerken.object_metadata, zaaktype_kenmerken.label_multiple, zaaktype_kenmerken.properties, zaaktype_kenmerken.is_group \n"
            "FROM zaaktype_kenmerken \n"
            "WHERE zaaktype_kenmerken.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_kenmerken.zaak_status_id = :zaak_status_id_1 ORDER BY zaaktype_kenmerken.id"
        )

        insert_case_attribute_command = self.execute_queries[13]
        assert (
            str(insert_case_attribute_command)
            == "INSERT INTO zaaktype_kenmerken (uuid, created, last_modified, zaaktype_node_id, zaak_status_id, bibliotheek_kenmerken_id, object_id, value_mandatory, label, help, pip, zaakinformatie_view, bag_zaakadres, value_default, pip_can_change, publish_public, referential, is_systeemkenmerk, required_permissions, version, help_extern, object_metadata, label_multiple, properties, is_group) VALUES (:uuid, :created, :last_modified, :zaaktype_node_id, :zaak_status_id, :bibliotheek_kenmerken_id, :object_id, :value_mandatory, :label, :help, :pip, :zaakinformatie_view, :bag_zaakadres, :value_default, :pip_can_change, :publish_public, :referential, :is_systeemkenmerk, :required_permissions, :version, :help_extern, :object_metadata, :label_multiple, :properties, :is_group)"
        )

        select_case_type_notifications_query = self.execute_queries[14]
        assert (
            str(select_case_type_notifications_query)
            == "SELECT zaaktype_notificatie.bibliotheek_notificaties_id, zaaktype_notificatie.label, zaaktype_notificatie.rcpt, zaaktype_notificatie.onderwerp, zaaktype_notificatie.bericht, zaaktype_notificatie.intern_block, zaaktype_notificatie.email, zaaktype_notificatie.behandelaar, zaaktype_notificatie.automatic, zaaktype_notificatie.cc, zaaktype_notificatie.bcc, zaaktype_notificatie.betrokkene_role \n"
            "FROM zaaktype_notificatie \n"
            "WHERE zaaktype_notificatie.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_notificatie.zaak_status_id = :zaak_status_id_1 ORDER BY zaaktype_notificatie.id ASC"
        )

        insert_case_type_notification_command = self.execute_queries[15]
        assert (
            str(insert_case_type_notification_command)
            == "INSERT INTO zaaktype_notificatie (created, last_modified, zaaktype_node_id, zaak_status_id, bibliotheek_notificaties_id, label, rcpt, onderwerp, bericht, intern_block, email, behandelaar, automatic, cc, bcc, betrokkene_role) VALUES (:created, :last_modified, :zaaktype_node_id, :zaak_status_id, :bibliotheek_notificaties_id, :label, :rcpt, :onderwerp, :bericht, :intern_block, :email, :behandelaar, :automatic, :cc, :bcc, :betrokkene_role)"
        )

        select_case_type_results_query = self.execute_queries[16]
        assert (
            str(select_case_type_results_query)
            == "SELECT zaaktype_resultaten.resultaat, zaaktype_resultaten.ingang, zaaktype_resultaten.bewaartermijn, zaaktype_resultaten.dossiertype, zaaktype_resultaten.label, zaaktype_resultaten.selectielijst, zaaktype_resultaten.archiefnominatie, zaaktype_resultaten.comments, zaaktype_resultaten.external_reference, zaaktype_resultaten.trigger_archival, zaaktype_resultaten.selectielijst_brondatum, zaaktype_resultaten.selectielijst_einddatum, zaaktype_resultaten.properties, zaaktype_resultaten.standaard_keuze \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_resultaten.zaaktype_status_id = :zaaktype_status_id_1 ORDER BY zaaktype_resultaten.id ASC"
        )

        insert_case_type_result_command = self.execute_queries[17]
        assert (
            str(insert_case_type_result_command)
            == "INSERT INTO zaaktype_resultaten (zaaktype_node_id, zaaktype_status_id, resultaat, ingang, bewaartermijn, created, last_modified, dossiertype, label, selectielijst, archiefnominatie, comments, external_reference, trigger_archival, selectielijst_brondatum, selectielijst_einddatum, properties, standaard_keuze) VALUES (:zaaktype_node_id, :zaaktype_status_id, :resultaat, :ingang, :bewaartermijn, :created, :last_modified, :dossiertype, :label, :selectielijst, :archiefnominatie, :comments, :external_reference, :trigger_archival, :selectielijst_brondatum, :selectielijst_einddatum, :properties, :standaard_keuze)"
        )

        select_case_type_rules_query = self.execute_queries[18]
        assert (
            str(select_case_type_rules_query)
            == "SELECT zaaktype_regel.naam, zaaktype_regel.settings, zaaktype_regel.active, zaaktype_regel.is_group \n"
            "FROM zaaktype_regel \n"
            "WHERE zaaktype_regel.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_regel.zaak_status_id = :zaak_status_id_1 ORDER BY zaaktype_regel.id ASC"
        )

        insert_case_type_rule_command = self.execute_queries[19]
        assert (
            str(insert_case_type_rule_command)
            == "INSERT INTO zaaktype_regel (zaaktype_node_id, zaak_status_id, naam, created, last_modified, settings, active, is_group) VALUES (:zaaktype_node_id, :zaak_status_id, :naam, :created, :last_modified, :settings, :active, :is_group)"
        )

        select_case_type_relations_query = self.execute_queries[20]

        assert (
            str(select_case_type_relations_query)
            == "SELECT zaaktype_relatie.relatie_zaaktype_id, zaaktype_relatie.relatie_type, zaaktype_relatie.start_delay, zaaktype_relatie.status, zaaktype_relatie.kopieren_kenmerken, zaaktype_relatie.ou_id, zaaktype_relatie.role_id, zaaktype_relatie.automatisch_behandelen, zaaktype_relatie.required, zaaktype_relatie.subject_role, zaaktype_relatie.copy_subject_role, zaaktype_relatie.betrokkene_authorized, zaaktype_relatie.betrokkene_notify, zaaktype_relatie.betrokkene_id, zaaktype_relatie.betrokkene_role, zaaktype_relatie.betrokkene_role_set, zaaktype_relatie.betrokkene_prefix, zaaktype_relatie.eigenaar_type, zaaktype_relatie.eigenaar_role, zaaktype_relatie.eigenaar_id, zaaktype_relatie.show_in_pip, zaaktype_relatie.pip_label, zaaktype_relatie.copy_related_cases, zaaktype_relatie.copy_related_objects, zaaktype_relatie.copy_selected_attributes, zaaktype_relatie.creation_style \n"
            "FROM zaaktype_relatie \n"
            "WHERE zaaktype_relatie.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_relatie.zaaktype_status_id = :zaaktype_status_id_1 ORDER BY zaaktype_relatie.id ASC"
        )

        insert_case_type_relation_command = self.execute_queries[21]
        assert (
            str(insert_case_type_relation_command)
            == "INSERT INTO zaaktype_relatie (created, last_modified, zaaktype_node_id, zaaktype_status_id, relatie_zaaktype_id, relatie_type, eigenaar_type, start_delay, status, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, subject_role, copy_subject_role, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_role, eigenaar_id, show_in_pip, pip_label, copy_related_cases, copy_related_objects, copy_selected_attributes, creation_style) VALUES (:created, :last_modified, :zaaktype_node_id, :zaaktype_status_id, :relatie_zaaktype_id, :relatie_type, :eigenaar_type, :start_delay, :status, :kopieren_kenmerken, :ou_id, :role_id, :automatisch_behandelen, :required, :subject_role, :copy_subject_role, :betrokkene_authorized, :betrokkene_notify, :betrokkene_id, :betrokkene_role, :betrokkene_role_set, :betrokkene_prefix, :eigenaar_role, :eigenaar_id, :show_in_pip, :pip_label, :copy_related_cases, :copy_related_objects, :copy_selected_attributes, :creation_style)"
        )

        select_case_type_templates_query = self.execute_queries[22]
        assert (
            str(select_case_type_templates_query)
            == "SELECT zaaktype_sjablonen.bibliotheek_sjablonen_id, zaaktype_sjablonen.bibliotheek_kenmerken_id, zaaktype_sjablonen.automatisch_genereren, zaaktype_sjablonen.target_format, zaaktype_sjablonen.help, zaaktype_sjablonen.zaak_status_id, zaaktype_sjablonen.custom_filename, zaaktype_sjablonen.label \n"
            "FROM zaaktype_sjablonen \n"
            "WHERE zaaktype_sjablonen.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_sjablonen.zaak_status_id = :zaak_status_id_1 ORDER BY zaaktype_sjablonen.id ASC"
        )

        insert_case_type_template_command = self.execute_queries[23]
        assert (
            str(insert_case_type_template_command)
            == "INSERT INTO zaaktype_sjablonen (created, last_modified, zaaktype_node_id, zaak_status_id, bibliotheek_sjablonen_id, bibliotheek_kenmerken_id, automatisch_genereren, target_format, help, custom_filename, label) VALUES (:created, :last_modified, :zaaktype_node_id, :zaak_status_id, :bibliotheek_sjablonen_id, :bibliotheek_kenmerken_id, :automatisch_genereren, :target_format, :help, :custom_filename, :label)"
        )

        select_case_type_standard_subjects_query = self.execute_queries[24]
        assert (
            str(select_case_type_standard_subjects_query)
            == "SELECT zaaktype_standaard_betrokkenen.betrokkene_type, zaaktype_standaard_betrokkenen.betrokkene_identifier, zaaktype_standaard_betrokkenen.naam, zaaktype_standaard_betrokkenen.rol, zaaktype_standaard_betrokkenen.magic_string_prefix, zaaktype_standaard_betrokkenen.gemachtigd, zaaktype_standaard_betrokkenen.notify \n"
            "FROM zaaktype_standaard_betrokkenen \n"
            "WHERE zaaktype_standaard_betrokkenen.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_standaard_betrokkenen.zaak_status_id = :zaak_status_id_1 ORDER BY zaaktype_standaard_betrokkenen.id ASC"
        )

        insert_case_type_standard_subject_command = self.execute_queries[25]
        assert (
            str(insert_case_type_standard_subject_command)
            == "INSERT INTO zaaktype_standaard_betrokkenen (uuid, zaaktype_node_id, zaak_status_id, betrokkene_type, betrokkene_identifier, naam, rol, magic_string_prefix, gemachtigd, notify) VALUES (:uuid, :zaaktype_node_id, :zaak_status_id, :betrokkene_type, :betrokkene_identifier, :naam, :rol, :magic_string_prefix, :gemachtigd, :notify)"
        )
        select_case_type_standard_subjects_query = self.execute_queries[26]
        assert (
            str(select_case_type_standard_subjects_query)
            == "SELECT zaaktype_object_mutation.attribute_uuid, zaaktype_object_mutation.mutation_type, zaaktype_object_mutation.object_title, zaaktype_object_mutation.phase_transition, zaaktype_object_mutation.system_attribute_changes, zaaktype_object_mutation.attribute_mapping \n"
            "FROM zaaktype_object_mutation \n"
            "WHERE zaaktype_object_mutation.zaaktype_node_id = :zaaktype_node_id_1 AND zaaktype_object_mutation.zaaktype_status_id = :zaaktype_status_id_1 ORDER BY zaaktype_object_mutation.id"
        )

        insert_case_type_standard_subject_command = self.execute_queries[27]
        assert (
            str(insert_case_type_standard_subject_command)
            == "INSERT INTO zaaktype_object_mutation (zaaktype_node_id, zaaktype_status_id, attribute_uuid, mutation_type, object_title, phase_transition, system_attribute_changes, attribute_mapping) VALUES (:zaaktype_node_id, :zaaktype_status_id, :attribute_uuid, :mutation_type, :object_title, :phase_transition, :system_attribute_changes, :attribute_mapping)"
        )

        get_new_case_type_node = self.execute_queries[28]
        assert (
            str(get_new_case_type_node)
            == "SELECT zaaktype_node.zaaktype_id, zaaktype_node.id, zaaktype_node.version \n"
            "FROM zaaktype_node \n"
            "WHERE zaaktype_node.uuid = :uuid_1"
        )

        update_case_type_version_command = self.execute_queries[29]
        assert (
            str(update_case_type_version_command)
            == "UPDATE zaaktype SET zaaktype_node_id=:zaaktype_node_id, version=:version WHERE zaaktype.id = :id_1"
        )
