# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.case_type_version import (
    CaseTypeVersionEntity,
)


class TestCaseTypeEntity:
    def setup_method(self):
        self.version_uuid = str(uuid4())
        self.case_type_uuid = str(uuid4())
        self.case_type_version = CaseTypeVersionEntity(
            id=1,
            uuid=self.version_uuid,
            case_type_uuid=self.case_type_uuid,
            active=True,
            last_modified="2019-12-21 23:50:43",
            name="version_final",
            username="administrator",
            display_name="ad min",
            version=4,
            created="2019-12-21 23:50:43",
            reason="Reason for a case type version",
        )
        self.case_type_version.event_service = mock.MagicMock()

    def test_set_status(self):
        assert self.case_type_version.entity_id == self.version_uuid
        assert self.case_type_version.case_type_uuid == self.case_type_uuid
        assert self.case_type_version.active is True
        assert (
            self.case_type_version.reason == "Reason for a case type version"
        )
