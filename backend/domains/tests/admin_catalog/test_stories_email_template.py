# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import pytest
from collections import namedtuple
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import IntegrityError
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.admin import catalog


class Test_AsUser_Manipute_Email_Template(TestBase):
    def setup_method(self):
        self.load_command_instance(catalog)

    def _get_fields(self, attachment_uuid: UUID | None = None):
        if attachment_uuid is None:
            attachment_uuid = uuid4()
        return {
            "commit_message": "commit message",
            "label": "label",
            "sender": "John",
            "sender_address": "sender_address@sender.nl",
            "subject": "subject",
            "message": "message",
            "category_uuid": "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1",
            "attachments": [
                {
                    "name": "attachment",
                    "uuid": str(attachment_uuid),
                }
            ],
        }

    def test_create_email_template(self):
        entity_uuid = str(uuid4())
        fields = self._get_fields()

        self.cmd.create_email_template(uuid=entity_uuid, fields=fields)
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        self.assert_has_event_name("EmailTemplateCreated")
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "INSERT INTO bibliotheek_notificaties (uuid, bibliotheek_categorie_id, label, subject, message, sender, sender_address, search_term) VALUES (%(uuid)s::UUID, (SELECT bibliotheek_categorie.id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID), %(label)s, %(subject)s, %(message)s, %(sender)s, %(sender_address)s, %(search_term)s) RETURNING bibliotheek_notificaties.id"
        )

    def test_create_email_template_existing(self):
        entity_uuid = str(uuid4())
        fields = self._get_fields()

        with pytest.raises(Conflict) as excinfo:
            self.session.execute.side_effect = IntegrityError(
                statement=None, params={}, orig=Exception
            )

            self.cmd.create_email_template(uuid=entity_uuid, fields=fields)

        assert excinfo.value.args == (
            f"Email template with uuid {entity_uuid} already exists",
            "email_template/already_exists_with_uuid",
        )

    def test_edit_email_template(self):
        entity_uuid = str(uuid4())
        fields = self._get_fields()

        attachments = [mock.MagicMock()]
        attachments[0].configure_mock(
            uuid=uuid4(),
            naam="attachment",
        )
        nt = namedtuple(
            "query_result",
            "uuid id label sender sender_address subject message category_uuid",
        )
        mock_email_template = nt(
            uuid=uuid4(),
            id=1,
            label="label",
            sender="sender",
            sender_address="sender_address",
            subject="subject",
            message="message",
            category_uuid=uuid4(),
        )
        self.session.execute().fetchone.return_value = mock_email_template
        self.session.execute().fetchall.return_value = attachments
        self.session.reset_mock()
        self.cmd.edit_email_template(uuid=entity_uuid, fields=fields)
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6
        self.assert_has_event_name("EmailTemplateEdited")
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_notificaties.uuid, bibliotheek_notificaties.id, bibliotheek_notificaties.label, bibliotheek_notificaties.sender, bibliotheek_notificaties.sender_address, bibliotheek_notificaties.message, bibliotheek_notificaties.subject, bibliotheek_categorie.uuid AS category_uuid \n"
            "FROM bibliotheek_notificaties LEFT OUTER JOIN bibliotheek_categorie ON bibliotheek_notificaties.bibliotheek_categorie_id = bibliotheek_categorie.id \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID AND bibliotheek_notificaties.deleted IS NULL"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.naam \n"
            "FROM bibliotheek_kenmerken, bibliotheek_notificatie_kenmerk \n"
            "WHERE bibliotheek_notificatie_kenmerk.bibliotheek_notificatie_id = %(bibliotheek_notificatie_id_1)s AND bibliotheek_kenmerken.id = bibliotheek_notificatie_kenmerk.bibliotheek_kenmerken_id"
        )
        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "DELETE FROM bibliotheek_notificatie_kenmerk WHERE bibliotheek_notificatie_kenmerk.bibliotheek_notificatie_id = (SELECT bibliotheek_notificaties.id \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID) AND bibliotheek_notificatie_kenmerk.bibliotheek_kenmerken_id IN (SELECT bibliotheek_kenmerken.id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.uuid IN (%(uuid_2_1)s::UUID))"
        )

        select_statement = call_list[3][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_notificaties SET uuid=%(uuid)s::UUID, bibliotheek_categorie_id=(SELECT bibliotheek_categorie.id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID), label=%(label)s, subject=%(subject)s, message=%(message)s, sender=%(sender)s, sender_address=%(sender_address)s, search_term=%(search_term)s WHERE bibliotheek_notificaties.uuid = %(uuid_2)s::UUID"
        )

        select_statement = call_list[4][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_notificaties.label, bibliotheek_notificaties.message, bibliotheek_notificaties.subject \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[4][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_notificaties.label, bibliotheek_notificaties.message, bibliotheek_notificaties.subject \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[5][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_notificaties SET search_term=%(search_term)s WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

    def test_edit_email_template_unchanged_attachment(self):
        entity_uuid = str(uuid4())
        attachment_uuid = uuid4()

        fields = self._get_fields(attachment_uuid=attachment_uuid)

        attachments = [mock.Mock()]
        attachments[0].configure_mock(
            uuid=attachment_uuid,
            naam="attachment",
        )

        nt = namedtuple(
            "query_result",
            "uuid id label sender sender_address subject message category_uuid",
        )
        mock_email_template = nt(
            uuid=uuid4(),
            id=1,
            label="label",
            sender="sender",
            sender_address="sender_address",
            subject="subject",
            message="message",
            category_uuid=uuid4(),
        )
        self.session.execute().fetchone.return_value = mock_email_template
        self.session.execute().fetchall.return_value = attachments
        self.session.reset_mock()

        # Run the command:
        self.cmd.edit_email_template(uuid=entity_uuid, fields=fields)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 5
        self.assert_has_event_name("EmailTemplateEdited")
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_notificaties.uuid, bibliotheek_notificaties.id, bibliotheek_notificaties.label, bibliotheek_notificaties.sender, bibliotheek_notificaties.sender_address, bibliotheek_notificaties.message, bibliotheek_notificaties.subject, bibliotheek_categorie.uuid AS category_uuid \n"
            "FROM bibliotheek_notificaties LEFT OUTER JOIN bibliotheek_categorie ON bibliotheek_notificaties.bibliotheek_categorie_id = bibliotheek_categorie.id \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID AND bibliotheek_notificaties.deleted IS NULL"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.naam \n"
            "FROM bibliotheek_kenmerken, bibliotheek_notificatie_kenmerk \n"
            "WHERE bibliotheek_notificatie_kenmerk.bibliotheek_notificatie_id = %(bibliotheek_notificatie_id_1)s AND bibliotheek_kenmerken.id = bibliotheek_notificatie_kenmerk.bibliotheek_kenmerken_id"
        )
        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_notificaties SET uuid=%(uuid)s::UUID, bibliotheek_categorie_id=(SELECT bibliotheek_categorie.id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID), label=%(label)s, subject=%(subject)s, message=%(message)s, sender=%(sender)s, sender_address=%(sender_address)s, search_term=%(search_term)s WHERE bibliotheek_notificaties.uuid = %(uuid_2)s::UUID"
        )

        select_statement = call_list[3][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_notificaties.label, bibliotheek_notificaties.message, bibliotheek_notificaties.subject \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[4][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_notificaties SET search_term=%(search_term)s WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

    def test_edit_email_template_details_not_found(self):
        entity_uuid = str(uuid4())
        fields = self._get_fields()

        self.session.execute().fetchone.return_value = None
        with pytest.raises(NotFound) as excinfo:
            self.cmd.edit_email_template(uuid=entity_uuid, fields=fields)

        assert excinfo.value.args == (
            f"Email template with uuid {entity_uuid} not found",
            "email_template/not_found",
        )

    def test_edit_email_template_info_not_found(self):
        entity_uuid = str(uuid4())
        fields = self._get_fields()
        nt = namedtuple(
            "query_result",
            "uuid id label sender sender_address subject message category_uuid",
        )
        mock_email_template = nt(
            uuid=entity_uuid,
            id=1,
            label="label",
            sender="sender",
            sender_address="sender_address",
            subject="subject",
            message="message",
            category_uuid=uuid4(),
        )
        self.session.execute().fetchone.side_effect = (
            mock_email_template,
            NotFound,
        )
        with pytest.raises(NotFound) as excinfo:
            self.session.execute().fetchone.side_effect = (
                mock_email_template,
                None,
            )
            self.cmd.edit_email_template(uuid=entity_uuid, fields=fields)

        assert excinfo.value.args == (
            f"Email template with uuid '{entity_uuid}' not found.",
            "email_template/not_found",
        )

    def test_delete_email_template(self):
        email_template_uuid = str(uuid4())
        self.session.execute().fetchone.return_value = [0]

        self.session.reset_mock()
        self.cmd.delete_email_template(
            uuid=email_template_uuid, reason="No more used"
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        self.assert_has_event_name("EmailTemplateDeleted")
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT count(*) AS count_1 \n"
            "FROM zaaktype_notificatie JOIN zaaktype_node ON zaaktype_notificatie.zaaktype_node_id = zaaktype_node.id JOIN zaaktype ON zaaktype_node.zaaktype_id = zaaktype.id \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype_notificatie.bibliotheek_notificaties_id = (SELECT bibliotheek_notificaties.id \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID)"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "UPDATE bibliotheek_notificaties SET uuid=%(uuid)s::UUID, bibliotheek_categorie_id=%(bibliotheek_categorie_id)s, deleted=%(deleted)s, search_term=%(search_term)s WHERE bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

    def test_delete_email_template_in_use(self):
        email_template_uuid = str(uuid4())
        with pytest.raises(Conflict) as excinfo:
            self.session.execute().fetchone.return_value = [4]

            self.cmd.delete_email_template(
                uuid=email_template_uuid, reason="No more used"
            )

        assert excinfo.value.args == (
            "Email template is used in case types",
            "email_template/used_in_case_types",
        )
