# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import pytest
from collections import namedtuple
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import IntegrityError
from uuid import uuid4
from zsnl_domains.admin import catalog


class Test_AsUser_ManipulateFolder(TestBase):
    def setup_method(self):
        self.load_command_instance(catalog)

    def test_create_folder(self):
        folder_uuid = str(uuid4())
        parent_uuid = str(uuid4())
        name = "testFolder"

        self.cmd.create_folder(
            folder_uuid=folder_uuid, parent_uuid=parent_uuid, name=name
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        self.assert_has_event_name("FolderCreated")
        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "INSERT INTO bibliotheek_categorie (uuid, pid, naam) VALUES (%(uuid)s::UUID, %(pid)s, %(naam)s) RETURNING bibliotheek_categorie.id"
        )

    def test_create_folder_invalid_parent(self):
        folder_uuid = str(uuid4())
        parent_uuid = uuid4()
        name = "test_folder"

        with pytest.raises(NotFound):
            self.session.execute().scalar.side_effect = [None]
            self.cmd.create_folder(
                folder_uuid=folder_uuid, parent_uuid=parent_uuid, name=name
            )

    def test_create_folder_same_as_parent(self):
        folder_uuid = str(uuid4())
        parent_uuid = None
        name = "test_folder"

        with pytest.raises(Conflict):
            self.session.execute.side_effect = IntegrityError(None, None, None)
            self.cmd.create_folder(
                folder_uuid=folder_uuid, parent_uuid=parent_uuid, name=name
            )

    def test_delete_folder(self):
        folder_uuid = str(uuid4())
        reason = "not needed"

        self.cmd.delete_folder(uuid=folder_uuid, reason=reason)
        self.assert_has_event_name("FolderDeleted")
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_categorie.naam \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT count(*) AS count_1 \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.pid = %(pid_1)s UNION ALL SELECT count(*) AS count_2 \n"
            "FROM zaaktype \n"
            "WHERE zaaktype.bibliotheek_categorie_id = %(bibliotheek_categorie_id_1)s UNION ALL SELECT count(*) AS count_3 \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.bibliotheek_categorie_id = %(bibliotheek_categorie_id_2)s UNION ALL SELECT count(*) AS count_4 \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.bibliotheek_categorie_id = %(bibliotheek_categorie_id_3)s UNION ALL SELECT count(*) AS count_5 \n"
            "FROM bibliotheek_sjablonen \n"
            "WHERE bibliotheek_sjablonen.bibliotheek_categorie_id = %(bibliotheek_categorie_id_4)s UNION ALL SELECT count(*) AS count_6 \n"
            "FROM object_bibliotheek_entry \n"
            "WHERE object_bibliotheek_entry.bibliotheek_categorie_id = %(bibliotheek_categorie_id_5)s"
        )

        delete_statement = call_list[3][0][0]
        compiled_select = delete_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "DELETE FROM bibliotheek_categorie WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

    def test__delete_non_empty_folder(self):
        folder_uuid = str(uuid4())
        reason = "not needed"
        with pytest.raises(Conflict) as excinfo:
            # folder_contents_mock.return_value = 50
            self.session.execute().fetchall.return_value = [[10], [10], [10]]
            self.session.reset_mock()
            self.cmd.delete_folder(uuid=folder_uuid, reason=reason)

        assert excinfo.value.args == (
            "Folder is not empty.",
            "folder/not_empty",
        )

    def test_delete_non_existant_folder(self):
        folder_uuid = str(uuid4())
        reason = "not needed"
        self.session.execute().scalar.return_value = None

        with pytest.raises(NotFound) as excinfo:
            self.cmd.delete_folder(uuid=folder_uuid, reason=reason)
        assert excinfo.value.args == (
            f"Folder with uuid: {folder_uuid} not found.",
            "folder/not_found",
        )

    def test_rename_folder(self):
        folder_uuid = uuid4()
        name = "new_folder"
        query_result_obj = namedtuple(
            "query_result",
            "id uuid name parent_uuid parent_name last_modified ",
        )
        query_res = query_result_obj(
            id=1,
            uuid=folder_uuid,
            name="old_folder",
            parent_uuid=uuid4(),
            parent_name="test_folder",
            last_modified="2019-07-28",
        )
        self.session.execute().fetchone.return_value = query_res
        self.session.reset_mock()
        self.cmd.rename_folder(folder_uuid=str(folder_uuid), name=name)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_categorie.id, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, bibliotheek_categorie.last_modified, bibliotheek_categorie_1.uuid AS parent_uuid, bibliotheek_categorie_1.naam AS parent_name, bibliotheek_categorie_1.id AS parent_id \n"
            "FROM bibliotheek_categorie LEFT OUTER JOIN bibliotheek_categorie AS bibliotheek_categorie_1 ON bibliotheek_categorie.pid = bibliotheek_categorie_1.id \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[1][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_categorie SET naam=%(naam)s WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

    def test_rename_non_existant_folder(self):
        folder_uuid = str(uuid4())
        name = "new_folder"
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as excinfo:
            self.cmd.rename_folder(folder_uuid=str(folder_uuid), name=name)
        assert excinfo.value.args == (
            f"Folder with uuid: {folder_uuid} not found.",
            "folder/not_found",
        )
