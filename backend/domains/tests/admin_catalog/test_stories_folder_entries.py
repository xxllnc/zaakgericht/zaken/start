# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import pytest
from collections import namedtuple
from minty.cqrs.test import TestBase
from minty.exceptions import NotFound
from sqlalchemy.dialects import postgresql
from uuid import uuid4
from zsnl_domains.admin import catalog


class Test_AsUser_Move_Folder_Entries(TestBase):
    def setup_method(self):
        self.load_command_instance(catalog)

    def test_move_folder_entries(self):
        folder_uuid = uuid4()
        dest_uuid = str(uuid4())
        fold_entries = [{"type": "case_type", "id": str(uuid4())}]
        folder_entry_row = namedtuple(
            "query_res", "object_id type uuid name active parent_folder_id"
        )(
            object_id=1223,
            type="folder",
            uuid=folder_uuid,
            name="name_of_folder",
            active=True,
            parent_folder_id=456,
        )
        folder_row = namedtuple(
            "query_result",
            "id uuid name parent_uuid parent_name last_modified parent_id",
        )(
            id=1,
            uuid=folder_uuid,
            name="old_folder",
            parent_uuid=uuid4(),
            parent_name="test_folder",
            last_modified="2019-07-28",
            parent_id=10,
        )

        parent_ids_row = namedtuple("parent_response", "pid")
        parent_ids = [parent_ids_row(10)]
        self.session.execute().fetchone.side_effect = [
            folder_entry_row,
            folder_row,
        ]
        self.session.execute().fetchall.return_value = parent_ids
        self.session.reset_mock()
        self.cmd.move_folder_entries(
            destination_folder_id=dest_uuid, folder_entries=fold_entries
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT %(param_1)s AS type, zaaktype.uuid, zaaktype_node.titel AS name, zaaktype.active, zaaktype.bibliotheek_categorie_id AS parent_folder_id, zaaktype.id AS object_id \n"
            "FROM zaaktype, zaaktype_node \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.zaaktype_node_id = zaaktype_node.id AND zaaktype.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_categorie.id, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, bibliotheek_categorie.last_modified, bibliotheek_categorie_1.uuid AS parent_uuid, bibliotheek_categorie_1.naam AS parent_name, bibliotheek_categorie_1.id AS parent_id \n"
            "FROM bibliotheek_categorie LEFT OUTER JOIN bibliotheek_categorie AS bibliotheek_categorie_1 ON bibliotheek_categorie.pid = bibliotheek_categorie_1.id \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[2][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

    def test_move_folder_entry_no_results(self):
        self.session.execute().fetchone.return_value = None
        id = str(uuid4())
        type = "folder"
        dest_uuid = str(uuid4())
        fold_entries = [{"type": "folder", "id": id}]
        with pytest.raises(NotFound) as excinfo:
            self.cmd.move_folder_entries(
                destination_folder_id=dest_uuid, folder_entries=fold_entries
            )
        assert excinfo.value.args == (
            f"Folder entry of type: '{type}'"
            + f" with uuid: '{id}' not found.",
            "folder_entry/not_found",
        )

    def test_move_folder_entries_root(self):
        folder_uuid = uuid4()
        dest_uuid = None
        fold_entries = [
            {"type": "folder", "id": str(uuid4())},
        ]
        folder_entry_row = namedtuple(
            "query_res", "object_id type uuid name active parent_folder_id"
        )
        folder_entry_row_1 = folder_entry_row(
            object_id=1223,
            type="folder",
            uuid=folder_uuid,
            name="name_of_folder",
            active=True,
            parent_folder_id=10,
        )

        folder_row = namedtuple(
            "query_result",
            "id uuid name parent_uuid parent_name last_modified parent_id",
        )(
            id=1,
            uuid=folder_uuid,
            name="old_folder",
            parent_uuid=uuid4(),
            parent_name="test_folder",
            last_modified="2019-07-28",
            parent_id=10,
        )

        parent_ids_row = namedtuple("parent_response", "pid")
        parent_ids = [parent_ids_row(10)]
        self.session.execute().fetchone.side_effect = [
            folder_entry_row_1,
            folder_row,
        ]
        self.session.execute().fetchall.return_value = parent_ids
        self.session.reset_mock()
        self.cmd.move_folder_entries(
            destination_folder_id=dest_uuid, folder_entries=fold_entries
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT %(param_1)s AS type, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, true AS active, bibliotheek_categorie.pid AS parent_folder_id, bibliotheek_categorie.id AS object_id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[1][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_categorie SET pid=%(pid)s WHERE bibliotheek_categorie.uuid = %(uuid_1)s::UUID"
        )

    def get_folder_entry_row(self, id, type):
        folder_entry_row = namedtuple(
            "query_res", "object_id type uuid name active parent_folder_id"
        )
        return folder_entry_row(
            object_id=id,
            type=type,
            uuid=uuid4(),
            name="name_of_folder",
            active=True,
            parent_folder_id=10,
        )

    def test_folder_entries_types(self):
        folder_uuid = uuid4()
        dest_uuid = None
        fold_entries = [
            {"type": "case_type", "id": str(uuid4())},
            {"type": "attribute", "id": str(uuid4())},
            {"type": "object_type", "id": str(uuid4())},
            {"type": "email_template", "id": str(uuid4())},
            {"type": "document_template", "id": str(uuid4())},
            {"type": "custom_object_type", "id": str(uuid4())},
        ]

        folder_row = namedtuple(
            "query_result",
            "id uuid name parent_uuid parent_name last_modified parent_id",
        )(
            id=1,
            uuid=folder_uuid,
            name="old_folder",
            parent_uuid=uuid4(),
            parent_name="test_folder",
            last_modified="2019-07-28",
            parent_id=10,
        )

        parent_ids_row = namedtuple("parent_response", "pid")
        parent_ids = [parent_ids_row(10)]
        self.session.execute().fetchone.side_effect = [
            self.get_folder_entry_row(1, "case_type"),
            self.get_folder_entry_row(2, "attribute"),
            self.get_folder_entry_row(3, "email_template"),
            self.get_folder_entry_row(4, "document_template"),
            self.get_folder_entry_row(5, "object_type"),
            self.get_folder_entry_row(6, "custom_object_type"),
            folder_row,
        ]
        self.session.execute().fetchall.return_value = parent_ids
        self.session.reset_mock()
        self.cmd.move_folder_entries(
            destination_folder_id=dest_uuid, folder_entries=fold_entries
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 12

        update_statement = call_list[6][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE zaaktype SET bibliotheek_categorie_id=%(bibliotheek_categorie_id)s FROM zaaktype_node WHERE zaaktype.deleted IS NULL AND zaaktype.zaaktype_node_id = zaaktype_node.id AND zaaktype.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[7][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_kenmerken SET bibliotheek_categorie_id=%(bibliotheek_categorie_id)s WHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[8][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_notificaties SET bibliotheek_categorie_id=%(bibliotheek_categorie_id)s WHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[9][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE bibliotheek_sjablonen SET bibliotheek_categorie_id=%(bibliotheek_categorie_id)s WHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.uuid = %(uuid_1)s::UUID"
        )

        update_statement = call_list[10][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE object_bibliotheek_entry SET bibliotheek_categorie_id=%(bibliotheek_categorie_id)s WHERE object_bibliotheek_entry.object_uuid = %(object_uuid_1)s::UUID"
        )

        update_statement = call_list[11][0][0]
        compiled_select = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE custom_object_type SET catalog_folder_id=%(catalog_folder_id)s WHERE custom_object_type.uuid = %(uuid_1)s::UUID"
        )
