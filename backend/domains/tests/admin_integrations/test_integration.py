# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import random
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.admin import integrations
from zsnl_domains.admin.integrations.entities import (
    Integration,
)
from zsnl_domains.admin.integrations.repositories.integration import (
    INTEGRATION_QUERY,
)


def mock_integration_row(integration_uuid: UUID):
    row = mock.Mock(id="transaction")
    row.configure_mock(
        uuid=integration_uuid,
        name="Some Integration",
        active=True,
        module="stufnps",
        id=random.randint(1, 100),
    )
    return row


class TestGetAllIntegrations(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

    def test_get_integrations(self):
        integration_uuid = uuid4()

        integration_row = mock_integration_row(integration_uuid)

        self.session.execute().fetchall.return_value = [integration_row]
        self.session.execute.reset_mock()
        integrations = self.qry.get_integrations()

        assert len(integrations) == 1
        assert isinstance(integrations[0], Integration)
        assert integrations[0].uuid == integration_uuid
        assert integrations[0].manual_type == {"file", "text"}
        assert 1 <= integrations[0].legacy_id <= 100

        expected_integration_query = INTEGRATION_QUERY

        execute_calls = self.session.execute.call_args_list
        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )
