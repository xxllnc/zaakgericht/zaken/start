# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
import minty.exceptions
import pytest
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import integrations


class Test_SaveEmailIntegrationLoginState(TestBase):
    def setup_method(self):
        self.load_command_instance(integrations)

    def test_save_email_integration_login_state(self):
        integration_uuid = uuid4()

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={"interface_config": 1},
            internal_config={"internal_config": 1},
        )
        self.session.execute().fetchone.return_value = db_row
        self.session.execute.reset_mock()

        self.cmd.save_email_integration_login_state(
            integration_uuid=integration_uuid,
            code_verifier=b"verifier",
            state=b"state",
        )

        assert len(self.session.execute.call_args_list) == 2
        update_query = self.session.execute.call_args_list[1][0][0]
        compiled_update = update_query.compile(dialect=postgresql.dialect())

        assert (
            str(compiled_update)
            == "UPDATE interface SET internal_config=(interface.internal_config || %(internal_config_1)s::JSONB) WHERE interface.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_update.params == {
            "uuid_1": str(integration_uuid),
            "internal_config_1": {
                "internal_config": 1,
                "login_code_verifier": "dmVyaWZpZXI=",
                "login_state": "c3RhdGU=",
            },
        }


class Test_ExchangeCodeForRefreshToken(TestBase):
    def setup_method(self):
        self.load_command_instance(integrations)

    def test_exchange_code_for_refresh_token(self):
        integration_uuid = uuid4()
        tenant_id = uuid4()
        client_id = uuid4()
        client_secret = uuid4()
        state = base64.urlsafe_b64encode(b"state").decode("ascii")
        verifier = base64.urlsafe_b64encode(b"verifier").decode("ascii")

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={
                "ms_tenant_id": str(tenant_id),
                "ms_client_id": str(client_id),
                "ms_client_secret": str(client_secret),
            },
            internal_config={
                "login_state": state,
                "login_code_verifier": verifier,
            },
        )
        self.session.execute().fetchone.return_value = db_row
        self.session.execute.reset_mock()

        with mock.patch("requests.post") as requests_post:
            requests_post().json.return_value = {"refresh_token": "minty"}
            requests_post().status_code = 200

            full_state = base64.urlsafe_b64encode(
                json.dumps(
                    {"state": "state", "uuid": str(integration_uuid)}
                ).encode("ascii")
            ).decode("ascii")

            self.cmd.exchange_code_for_refresh_token(
                redirect_uri="https://example.com/",
                code="code1",
                state=full_state,
            )

        assert len(self.session.execute.call_args_list) == 2
        update_query = self.session.execute.call_args_list[1][0][0]
        compiled_update = update_query.compile(dialect=postgresql.dialect())

        assert (
            str(compiled_update)
            == "UPDATE interface SET internal_config=(interface.internal_config || %(internal_config_1)s::JSONB) WHERE interface.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_update.params == {
            "uuid_1": str(integration_uuid),
            "internal_config_1": {
                "access_token_failed": None,
                "refresh_token": "minty",
                "login_state": None,
                "login_code_verifier": verifier,
            },
        }

    def test_exchange_code_for_refresh_token_error_response(self):
        integration_uuid = uuid4()
        tenant_id = uuid4()
        client_id = uuid4()
        client_secret = uuid4()
        state = base64.urlsafe_b64encode(b"state").decode("ascii")
        verifier = base64.urlsafe_b64encode(b"verifier").decode("ascii")

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={
                "ms_tenant_id": str(tenant_id),
                "ms_client_id": str(client_id),
                "ms_client_secret": str(client_secret),
            },
            internal_config={
                "login_state": state,
                "login_code_verifier": verifier,
            },
        )
        self.session.execute().fetchone.return_value = db_row
        self.session.execute.reset_mock()

        with (
            pytest.raises(
                minty.exceptions.CQRSException,
                match="error='error': error_description=\"something's wrong\"",
            ),
            mock.patch("requests.post") as requests_post,
        ):
            requests_post().json.return_value = {
                "error": "error",
                "error_description": "something's wrong",
            }
            requests_post().status_code = 200

            full_state = base64.urlsafe_b64encode(
                json.dumps(
                    {"state": "state", "uuid": str(integration_uuid)}
                ).encode("ascii")
            ).decode("ascii")

            self.cmd.exchange_code_for_refresh_token(
                redirect_uri="https://example.com/",
                code="code1",
                state=full_state,
            )

    def test_exchange_code_for_refresh_token_bad_state(self):
        integration_uuid = uuid4()

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={"interface_config": 1},
            internal_config={"login_state": ""},
        )
        self.session.execute().fetchone.return_value = db_row
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            state = base64.urlsafe_b64encode(
                json.dumps(
                    {"state": "state", "uuid": str(integration_uuid)}
                ).encode("ascii")
            ).decode("ascii")

            self.cmd.exchange_code_for_refresh_token(
                redirect_uri="https://example.com/", code="code1", state=state
            )
