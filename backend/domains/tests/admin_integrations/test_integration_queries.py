# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
import minty.exceptions
import pytest
import urllib.parse
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import integrations
from zsnl_domains.admin.integrations import entities


class Test_ASUser_Get_EmailIntegration(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

    def test_get_email_integration(self):
        integration_uuid = uuid4()

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={"interface_config": 1},
            internal_config={"internal_config": 1},
        )
        self.session.execute().fetchone.return_value = db_row

        integration = self.qry.get_email_integration(
            integration_uuid=integration_uuid
        )

        assert isinstance(integration, entities.EmailIntegration)
        assert integration.entity_id == integration_uuid
        assert integration.name == "Some Integration"
        assert integration.module == "pop3client"
        assert integration.user_config == {"interface_config": 1}
        assert integration.internal_config == {"internal_config": 1}

    def test_get_email_integration_no_result(self):
        integration_uuid = uuid4()

        db_row = None
        self.session.execute().fetchone.return_value = db_row

        with pytest.raises(minty.exceptions.NotFound):
            _ = self.qry.get_email_integration(
                integration_uuid=integration_uuid
            )

    def test_email_integration_entity(self):
        integration_uuid = uuid4()
        tenant_id = uuid4()
        client_id = uuid4()
        client_secret = uuid4()

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={
                "kind": "microsoft",
                "ms_tenant_id": str(tenant_id),
                "ms_client_id": str(client_id),
                "ms_client_secret": str(client_secret),
            },
            internal_config={"login_code_verifier": base64.b64encode(b"test")},
        )
        self.session.execute().fetchone.return_value = db_row

        integration = self.qry.get_email_integration(
            integration_uuid=integration_uuid
        )

        assert isinstance(integration, entities.EmailIntegration)

        with mock.patch("secrets.token_bytes") as token_bytes:
            token_bytes.side_effect = [b"secret1", b"secret2"]
            (login_url, code_verifier, state) = integration.generate_login_url(
                redirect_uri="https://example.com"
            )

        assert state == base64.urlsafe_b64encode(b"secret1").rstrip(b"=")
        assert code_verifier == base64.urlsafe_b64encode(b"secret2").rstrip(
            b"="
        )

        augmented_state = base64.urlsafe_b64encode(
            json.dumps(
                {
                    "state": state.decode("ascii"),
                    "uuid": str(integration_uuid),
                }
            ).encode("ascii")
        ).decode("ascii")

        parameters = urllib.parse.urlencode(
            {
                "client_id": client_id,
                "code_challenge_method": "S256",
                # urlsafe_base64 of SHA256 of the code_verifier
                "code_challenge": "CMmBizA-EDijZ1hqq3b_r8TxJf0LZ9lFihx5FigPGuA",
                "prompt": "select_account",
                "redirect_uri": "https://example.com",
                "response_mode": "query",
                "response_type": "code",
                "scope": "offline_access https://outlook.office.com/POP.AccessAsUser.All",
                "state": augmented_state,
            }
        )

        assert login_url == (
            f"https://login.microsoftonline.com/{tenant_id}"
            f"/oauth2/v2.0/authorize?{parameters}"
        )

        (token_url, token_params) = integration.get_token_request()
        assert (
            token_url
            == f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token"
        )
        assert token_params == {
            "client_id": str(client_id),
            "client_secret": str(client_secret),
            "code_verifier": "test",
            "grant_type": "authorization_code",
        }

    def test_email_integration_entity_bad_kind(self):
        integration_uuid = uuid4()
        tenant_id = uuid4()
        client_id = uuid4()
        client_secret = uuid4()

        db_row = mock.Mock()
        db_row.configure_mock(
            uuid=integration_uuid,
            name="Some Integration",
            module="pop3client",
            interface_config={
                "kind": "unknown",
                "ms_tenant_id": str(tenant_id),
                "ms_client_id": str(client_id),
                "ms_client_secret": str(client_secret),
            },
            internal_config={"login_code_verifier": base64.b64encode(b"test")},
        )
        self.session.execute().fetchone.return_value = db_row

        integration = self.qry.get_email_integration(
            integration_uuid=integration_uuid
        )

        assert isinstance(integration, entities.EmailIntegration)

        with pytest.raises(minty.exceptions.Conflict):
            (_, _, _) = integration.generate_login_url(
                redirect_uri="https://example.com"
            )


class Test_ASUser_Get_Active_Appointment_Integrations(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

    def test_get_active_appointment_integrations(self):
        integration_uuids = [uuid4(), uuid4()]
        appointment_integrations = [mock.MagicMock(), mock.MagicMock()]
        appointment_integrations[0].configure_mock(
            id=1,
            uuid=integration_uuids[0],
            name="Appointment Integration-1",
            active=True,
            module="appointment",
        )
        appointment_integrations[1].configure_mock(
            id=2,
            uuid=integration_uuids[1],
            name="Appointment Integration-2",
            active=True,
            module="appointment",
        )
        self.session.execute().fetchall.return_value = appointment_integrations
        self.session.execute.reset_mock()
        result = self.qry.get_active_appointment_integrations()

        assert len(result) == 2
        assert result[0].entity_id == integration_uuids[0]
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT interface.uuid, interface.id, interface.name, interface.active, interface.module \n"
            "FROM interface \n"
            "WHERE interface.date_deleted IS NULL AND interface.active IS true AND interface.module IN (%(module_1_1)s) ORDER BY interface.active DESC, interface.name ASC"
        )


class Test_ASUser_Get_Active_Appointment_v2_Integrations(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

    def test_get_active_appointment_v2_integrations(self):
        integration_uuids = [uuid4(), uuid4()]
        appointment_integrations = [mock.MagicMock(), mock.MagicMock()]
        appointment_integrations[0].configure_mock(
            id=1,
            uuid=integration_uuids[0],
            name="Appointment Integration-1",
            active=True,
            module="appointment",
        )
        appointment_integrations[1].configure_mock(
            id=2,
            uuid=integration_uuids[1],
            name="Appointment Integration-2",
            active=True,
            module="appointment",
        )
        self.session.execute().fetchall.return_value = appointment_integrations
        self.session.execute.reset_mock()
        result = self.qry.get_active_appointment_v2_integrations()

        assert len(result) == 2
        assert result[0].entity_id == integration_uuids[0]
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT interface.uuid, interface.id, interface.name, interface.active, interface.module \n"
            "FROM interface \n"
            "WHERE interface.date_deleted IS NULL AND interface.active IS true AND interface.module IN (%(module_1_1)s) ORDER BY interface.active DESC, interface.name ASC"
        )


class Test_ASUser_Get_Active_Document_Integrations(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

    def test_get_active_document_integrations(self):
        integration_uuids = [uuid4(), uuid4()]
        document_integrations = [mock.MagicMock(), mock.MagicMock()]
        document_integrations[0].configure_mock(
            id=1,
            uuid=integration_uuids[0],
            name="Document Integration-1",
            active=True,
            module="xential",
        )
        document_integrations[1].configure_mock(
            id=2,
            uuid=integration_uuids[1],
            name="Document Integration-2",
            active=True,
            module="stuf_dcr",
        )
        self.session.execute().fetchall.return_value = document_integrations
        self.session.execute.reset_mock()
        result = self.qry.get_active_integrations_for_document()

        assert len(result) == 2
        assert result[0].entity_id == integration_uuids[0]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT interface.uuid, interface.id, interface.name, interface.active, interface.module \n"
            "FROM interface \n"
            "WHERE interface.date_deleted IS NULL AND interface.active IS true AND interface.module IN (%(module_1_1)s, %(module_1_2)s) ORDER BY interface.active DESC, interface.name ASC"
        )
