# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from collections import namedtuple
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import users


class Test_AsAdmin_rename_users(TestBase):
    def setup_method(self):
        self.load_command_instance(users)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )
        self.cmd.user_info = self.user_info

    def test_rename_users(self):
        integration_uuid = uuid4()
        dry_run = True
        old_user_1 = mock.Mock()
        old_user_1.configure_mock(
            uuid=uuid4(),
            name="old_user_1",
            integration_uuid=integration_uuid,
        )
        new_user_1 = mock.Mock()
        new_user_1.configure_mock(
            uuid=uuid4(),
            name="new_user_1",
            integration_uuid=integration_uuid,
        )
        self.session.execute().fetchone.side_effect = [
            old_user_1,
            None,
            namedtuple("transaction", "id")(id=1),
        ]
        users = [
            {
                "old_username": "old_user_1",
                "new_username": "old_user_1@example.com",
            }
        ]
        self.session.reset_mock()
        self.cmd.rename_user(
            users=users,
            integration_uuid=str(integration_uuid),
            dry_run=dry_run,
        )

        call_list = self.session.execute.call_args_list
        assert self.session.rollback().called_once()
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT user_entity.uuid, user_entity.source_identifier AS name, interface.uuid AS integration_uuid \n"
            "FROM user_entity JOIN interface ON user_entity.source_interface_id = interface.id \n"
            "WHERE user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.uuid = %(uuid_1)s::UUID) AND user_entity.source_identifier = %(source_identifier_1)s"
        )

        update_statement = call_list[2][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE user_entity SET source_identifier=%(source_identifier)s WHERE user_entity.uuid = %(uuid_1)s::UUID"
        )

        insert_statement = call_list[3][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO transaction (uuid, interface_id, external_transaction_id, input_data, date_created, date_last_retry, processed, error_count, direction, success_count, total_count, error_message, text_vector) VALUES (%(uuid)s::UUID, %(interface_id)s, %(external_transaction_id)s, %(input_data)s, %(date_created)s, %(date_last_retry)s, %(processed)s, %(error_count)s, %(direction)s, %(success_count)s, %(total_count)s, %(error_message)s, %(text_vector)s) RETURNING transaction.id"
        )

    def test_rename_users_without_dry_run(self):
        integration_uuid = uuid4()
        dry_run = False
        old_user_1 = mock.Mock()
        old_user_1.configure_mock(
            uuid=uuid4(),
            name="old_user_1",
            integration_uuid=integration_uuid,
        )
        new_user_1 = mock.Mock()
        new_user_1.configure_mock(
            uuid=uuid4(),
            name="new_user_1",
            integration_uuid=integration_uuid,
        )
        self.session.execute().fetchone.side_effect = [
            old_user_1,
            None,
            namedtuple("transaction", "id")(id=1),
        ]
        users = [
            {
                "old_username": "old_user_1",
                "new_username": "old_user_1@example.com",
            }
        ]
        self.session.reset_mock()
        self.cmd.rename_user(
            users=users,
            integration_uuid=str(integration_uuid),
            dry_run=dry_run,
        )

        call_list = self.session.execute.call_args_list

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT user_entity.uuid, user_entity.source_identifier AS name, interface.uuid AS integration_uuid \n"
            "FROM user_entity JOIN interface ON user_entity.source_interface_id = interface.id \n"
            "WHERE user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.uuid = %(uuid_1)s::UUID) AND user_entity.source_identifier = %(source_identifier_1)s"
        )

        update_statement = call_list[2][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE user_entity SET source_identifier=%(source_identifier)s WHERE user_entity.uuid = %(uuid_1)s::UUID"
        )

        insert_statement = call_list[3][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO transaction (uuid, interface_id, external_transaction_id, input_data, date_created, date_last_retry, processed, error_count, direction, success_count, total_count, error_message, text_vector) VALUES (%(uuid)s::UUID, %(interface_id)s, %(external_transaction_id)s, %(input_data)s, %(date_created)s, %(date_last_retry)s, %(processed)s, %(error_count)s, %(direction)s, %(success_count)s, %(total_count)s, %(error_message)s, %(text_vector)s) RETURNING transaction.id"
        )
