# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from unittest import mock
from zsnl_domains.auth.entities import Event


class Test_entity_event(TestBase):
    def setup_method(self) -> None:
        self.event_service = mock.MagicMock()

        username: str = "username"
        interface_name: str = "interface_name"

        self.event: Event = Event(
            entity_type="event",
            username=username,
            interface_name=interface_name,
            _event_service=self.event_service,
        )

    def test_add_event_auth0_interface_not_active(self) -> None:
        self.event.add_event_auth0_interface_not_active(
            username=None, interface_name=None
        )

    def test_add_event_auth0_user_not_active(self) -> None:
        self.event.add_event_auth0_user_not_active(
            username=None, interface_name=None
        )

    def test_add_event_auth0_user_marked_as_deleted(self) -> None:
        self.event.add_event_auth0_user_marked_as_deleted(
            username=None, interface_name=None
        )
