# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from zsnl_domains.auth.entities import Role


class Test_entity_role(TestBase):
    def test_create_role(self) -> None:
        permissions: list[str] = [
            "permission_1",
            "permission_2",
            "permission_3",
        ]

        test_role: Role = Role.create(permissions=permissions)

        assert isinstance(test_role.permissions, list)
        assert len(test_role.permissions) == 3
        assert "permission_1" in test_role.permissions
        assert "permission_2" in test_role.permissions
        assert "permission_3" in test_role.permissions
        assert test_role.entity_type == "role"

    def test_create_role_permissions_empty_list(self) -> None:
        permissions: list[str] = []

        test_role: Role = Role.create(permissions=permissions)

        assert isinstance(test_role.permissions, list)
        assert len(test_role.permissions) == 0
        assert test_role.entity_type == "role"

    def test_create_role_permissions_empty(self) -> None:
        permissions: None = None

        test_role: Role = Role.create(permissions=permissions)

        assert isinstance(test_role.permissions, type(None))
        assert test_role.entity_type == "role"

    def test_create_role_permissions_is_none(self) -> None:
        test_role: Role = Role.create()

        assert isinstance(test_role.permissions, type(None))
        assert test_role.entity_type == "role"
