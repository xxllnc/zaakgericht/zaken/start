# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime, timezone
from minty.cqrs.test import TestBase
from minty.entity import Entity
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.auth.entities import Subject


class Test_entity_subject(TestBase):
    def setup_method(self) -> None:
        self.event_service = mock.MagicMock()

        username: str = "username"
        subject_uuid: UUID = uuid4()
        last_modified: datetime = datetime.now(timezone.utc)

        self.subject: Subject = Subject(
            entity_type="subject",
            id=1,
            settings={},
            properties={},
            uuid=subject_uuid,
            username=username,
            subject_type="employee",
            last_modified=last_modified,
            group_ids=[],
            role_ids=[],
            nobody=False,
            system=False,
            related_custom_object_uuid=None,
            _event_service=self.event_service,
        )

    def test_create_subject(self) -> None:
        new_username: str = "username1"
        new_subject_uuid: UUID = uuid4()
        new_last_modified: datetime = datetime.now(timezone.utc)

        new_subject: Entity = self.subject.create(
            uuid=new_subject_uuid,
            username=new_username,
            subject_type="employee",
            last_modified=new_last_modified,
            group_ids=[],
            role_ids=[],
            nobody=False,
            system=False,
            _event_service=self.event_service,
        )

        assert isinstance(new_subject, Subject)
        assert new_subject.entity_type == "subject"
        assert new_subject.uuid == new_subject_uuid
        assert new_subject.username == new_username
        assert new_subject.subject_type == "employee"
