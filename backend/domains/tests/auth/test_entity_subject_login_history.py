# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime, timezone
from minty.cqrs.test import TestBase
from minty.entity import Entity
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.auth.entities import SubjectLoginHistory


class Test_entity_subject_login_history(TestBase):
    def setup_method(self) -> None:
        self.event_service = mock.MagicMock()

        subject_id: int = 100
        subject_username: str = "test user"
        subject_uuid: UUID = uuid4()
        date_attempt: datetime = datetime.now(timezone.utc)
        ip: str = "127.0.0.1"
        success: bool = True
        method: str = "AUTH0"

        self.subject_login_history: SubjectLoginHistory = SubjectLoginHistory(
            entity_type="subject_login_history",
            id=1,
            ip=ip,
            subject_username=subject_username,
            subject_id=subject_id,
            subject_uuid=subject_uuid,
            success=success,
            method=method,
            date_attempt=date_attempt,
            _event_service=self.event_service,
        )

    def test_create_subject_login_history(self) -> None:
        new_id: int = 2
        new_subject_id: int = 200
        new_subject_username: str = "new test user"
        new_subject_uuid: UUID = uuid4()
        new_date_attempt: datetime = datetime.now(timezone.utc)
        new_ip: str = "127.0.0.2"
        new_success: bool = False
        new_method: str = "default"

        new_slh: Entity = self.subject_login_history.create(
            id=new_id,
            ip=new_ip,
            subject_username=new_subject_username,
            subject_id=new_subject_id,
            subject_uuid=new_subject_uuid,
            success=new_success,
            method=new_method,
            date_attempt=new_date_attempt,
            _event_service=self.event_service,
        )

        assert isinstance(new_slh, SubjectLoginHistory)
        assert new_slh.entity_type == "subject_login_history"
        assert new_slh.id == new_id
        assert new_slh.subject_id == new_subject_id
        assert new_slh.subject_username == new_subject_username
        assert new_slh.subject_uuid == new_subject_uuid
        assert new_slh.success == new_success
        assert new_slh.method == new_method
        assert new_slh.date_attempt == new_date_attempt
