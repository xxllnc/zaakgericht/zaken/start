# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import re
from minty.cqrs.test import TestBase
from minty.entity import EntityCollection
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from unittest import mock
from zsnl_domains import auth
from zsnl_domains.auth.exceptions import (
    FindRolesError,
    RolesNotFoundError,
)


class Test_Role_queries(TestBase):
    def setup_method(self) -> None:
        self.load_query_instance(domain=auth)

    def test_get_permissions_for_roles_no_role_ids(self) -> None:
        self.session.execute().fetchall.return_value = None
        self.session.reset_mock()

        result = self.qry.get_permissions_for_roles(role_ids=[])
        assert isinstance(result, type(None))
        assert result is None

    def test_get_permissions_for_roles(self) -> None:
        mock_roles: list[mock.Mock] = [mock.Mock()]
        mock_roles[0].configure_mock(
            permissions=["permission1_for_role_0"],
        )

        self.session.execute().fetchall.return_value = mock_roles
        self.session.reset_mock()

        result = self.qry.get_permissions_for_roles(role_ids=["0"])
        assert isinstance(result, EntityCollection)

        roles = list(result)
        assert len(roles) == 1
        assert isinstance(roles[0].permissions, list)
        assert len(roles[0].permissions) == 1
        assert "permission1_for_role_0" in roles[0].permissions

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT roles.permissions \nFROM roles \nWHERE roles.id IN ('0')"
        )

    def test_get_permissions_for_roles_test_2(self) -> None:
        mock_roles: list[mock.Mock] = [mock.Mock(), mock.Mock(), mock.Mock()]
        mock_roles[0].configure_mock(
            permissions=["permission1_for_role_0"],
        )
        mock_roles[1].configure_mock(
            permissions=["permission1_for_role_1", "permission2_for_role_1"],
        )
        mock_roles[2].configure_mock(
            permissions=["permission1_for_role_2", "permission2_for_role_2"],
        )

        self.session.execute().fetchall.return_value = mock_roles
        self.session.reset_mock()

        result = self.qry.get_permissions_for_roles(role_ids=["0,1,2"])
        assert isinstance(result, EntityCollection)

        roles = list(result)
        assert len(roles) == 3

        assert isinstance(roles[0].permissions, list)
        assert len(roles[0].permissions) == 1
        assert "permission1_for_role_0" in roles[0].permissions

        assert isinstance(roles[1].permissions, list)
        assert len(roles[1].permissions) == 2
        assert "permission1_for_role_1" in roles[1].permissions
        assert "permission2_for_role_1" in roles[1].permissions

        assert isinstance(roles[2].permissions, list)
        assert len(roles[2].permissions) == 2
        assert "permission1_for_role_2" in roles[2].permissions
        assert "permission1_for_role_2" in roles[2].permissions

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT roles.permissions \n"
            "FROM roles \n"
            "WHERE roles.id IN ('0,1,2')"
        )

    def test_get_permissions_for_roles_no_permissions(self) -> None:
        mock_roles: list[mock.Mock] = [mock.Mock()]
        mock_roles[0].configure_mock(
            permissions=None,
        )

        self.session.execute().fetchall.return_value = mock_roles
        self.session.reset_mock()

        result = self.qry.get_permissions_for_roles(role_ids=["0"])
        assert isinstance(result, EntityCollection)

        roles = list(result)
        assert len(roles) == 1
        assert isinstance(roles[0].permissions, type(None))

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT roles.permissions \nFROM roles \nWHERE roles.id IN ('0')"
        )

    def test_get_permissions_for_roles_empty_permissions_list(self) -> None:
        mock_roles: list[mock.Mock] = [mock.Mock()]
        mock_roles[0].configure_mock(
            permissions=[],
        )

        self.session.execute().fetchall.return_value = mock_roles
        self.session.reset_mock()

        result = self.qry.get_permissions_for_roles(role_ids=["0"])
        assert isinstance(result, EntityCollection)

        roles = list(result)
        assert len(roles) == 1
        assert isinstance(roles[0].permissions, list)
        assert len(roles[0].permissions) == 0

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT roles.permissions \nFROM roles \nWHERE roles.id IN ('0')"
        )

    def test_get_permissions_for_roles_no_roles(self) -> None:
        self.session.execute().fetchall.return_value = None
        self.session.reset_mock()

        ids: list[str] = ["1"]
        with pytest.raises(
            RolesNotFoundError,
            match=re.escape(f"No roles found based on ids {ids}."),
        ):
            result = self.qry.get_permissions_for_roles(role_ids=ids)
            assert isinstance(result, type(None))

            (args, kwargs) = self.session.execute.call_args_list[0]
            query = args[0].compile(
                dialect=postgresql.dialect(),
                compile_kwargs={"literal_binds": True},
            )

            assert str(query) == (
                "SELECT roles.permissions \n"
                "FROM roles \n"
                "WHERE roles.id IN ('1')"
            )

    def test_get_permissions_for_roles_error_sqlalchemy(self) -> None:
        ids: list[str] = ["1"]

        self.session.execute().fetchall.side_effect = SQLAlchemyError()
        self.session.reset_mock()

        with pytest.raises(
            FindRolesError,
            match=re.escape(f"Find roles by role ids '{ids}' failed."),
        ):
            self.qry.get_permissions_for_roles(role_ids=ids)

    def test_get_permissions_for_roles_error_dbapi(self) -> None:
        ids: list[str] = ["1"]

        self.session.execute().fetchall.side_effect = DBAPIError("", "", "")
        self.session.reset_mock()

        with pytest.raises(
            FindRolesError,
            match=re.escape(f"Find roles by role ids '{ids}' failed."),
        ):
            self.qry.get_permissions_for_roles(role_ids=ids)
