# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from zsnl_domains import auth
from zsnl_domains.auth.exceptions import CreateUserByNameError


class Test_Subject_commands(TestBase):
    def setup_method(self):
        self.load_command_instance(auth)
        self.username: str = "username"
        self.interface_name: str = "authldap"

    def test_create_user(self):
        self.cmd.create_user(username=self.username)
        self.assert_has_event_name("Auth0UserCreated")

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 4

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = %(module_1)s AND interface.active = "
            "%(active_1)s AND interface.date_deleted IS NULL"
        )

        assert compiled_select.params == {
            "module_1": "authldap",
            "active_1": "true",
        }

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT roles.id \nFROM roles \nWHERE roles.name = %(name_1)s"
        )

        assert compiled_select.params == {
            "name_1": "Behandelaar",
        }

        insert_statement = call_list[2][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO subject (uuid, subject_type, username, last_modified,"
            " role_ids, group_ids, nobody, system) VALUES (%(uuid)s::UUID, "
            "%(subject_type)s, %(username)s, %(last_modified)s, "
            "%(role_ids)s::INTEGER[], %(group_ids)s::INTEGER[], %(nobody)s, "
            "%(system)s) RETURNING subject.id"
        )

        assert len(compiled_insert.params) == 8
        assert compiled_insert.params["uuid"] is not None
        assert compiled_insert.params["subject_type"] == "employee"
        assert compiled_insert.params["username"] == self.username
        assert compiled_insert.params[
            "last_modified"
        ] == datetime.today().strftime("%Y-%m-%d")
        assert compiled_insert.params["role_ids"] is not None
        assert compiled_insert.params["group_ids"] == []
        assert not compiled_insert.params["nobody"]
        assert not compiled_insert.params["system"]

        insert_statement = call_list[3][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO user_entity (uuid, source_interface_id, "
            "source_identifier, subject_id, date_created, properties, active) "
            "VALUES (%(uuid)s::UUID, %(source_interface_id)s, "
            "%(source_identifier)s, %(subject_id)s, %(date_created)s, "
            "%(properties)s, %(active)s) RETURNING user_entity.id"
        )

        assert len(compiled_insert.params) == 7
        assert compiled_insert.params["uuid"] is not None
        assert compiled_insert.params["source_interface_id"] is not None
        assert compiled_insert.params["source_identifier"] == self.username
        assert compiled_insert.params["subject_id"] is not None
        assert compiled_insert.params["date_created"] is not None
        assert compiled_insert.params["properties"] == "{}"
        assert compiled_insert.params["active"]

    def test_create_usererror_sqlalchemy(self):
        self.session.execute().fetchone.side_effect = SQLAlchemyError()
        self.session.reset_mock()

        with pytest.raises(
            CreateUserByNameError,
            match="Create user by name 'username' failed.",
        ):
            self.cmd.create_user(username=self.username)

    def test_create_usererror_dbapi(self):
        self.session.execute().fetchone.side_effect = DBAPIError("", "", "")
        self.session.reset_mock()

        with pytest.raises(
            CreateUserByNameError,
            match="Create user by name 'username' failed.",
        ):
            self.cmd.create_user(username=self.username)
