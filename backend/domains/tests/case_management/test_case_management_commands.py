# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import minty.cqrs
import pytest
from minty.cqrs import UserInfo
from minty.exceptions import Conflict, Forbidden, NotFound, ValidationError
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.case_management import get_command_instance
from zsnl_domains.case_management.entities._shared import RelatedCustomObject
from zsnl_domains.case_management.entities.employee import Employee
from zsnl_domains.case_management.entities.organization import Organization
from zsnl_domains.case_management.entities.person import Person


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context, event_service, read_only):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCaseDomainCommands:
    def setup_method(self):
        with (
            mock.patch(
                "zsnl_domains.case_management.repositories.employee.EmployeeRepository",
                autospec=True,
            ) as employeeRepoMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.organization.OrganizationRepository",
                autospec=True,
            ) as organizationRepoMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.person.PersonRepository",
                autospec=True,
            ) as personRepoMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.subject_relation.SubjectRelationRepository",
                autospec=True,
            ) as relatedSubjectRepoMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.case_relation.CaseRelationRepository",
                autospec=True,
            ) as case_relation_repo_mock,
            mock.patch(
                "zsnl_domains.case_management.repositories.department.DepartmentRepository",
                autospec=True,
            ) as departmentRepoMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.role.RoleRepository",
                autospec=True,
            ) as roleRepoMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.case_messages.CaseMessageListRepository",
                autospec=True,
            ) as CaseMessageListRepoMock,
            mock.patch(
                "zsnl_domains.case_management.entities.subject.Subject",
                autospec=True,
            ) as subjectEntityMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.subject.SubjectRepository",
                autospec=True,
            ) as subjectRepoMock,
            mock.patch(
                "zsnl_domains.case_management.entities.case.Case",
                autospec=True,
            ) as caseEntityMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.case.CaseRepository",
                autospec=True,
            ) as caseRepoMock,
            mock.patch(
                "zsnl_domains.case_management.entities.case_type_version.CaseTypeVersionEntity",
                autospec=True,
            ) as caseTypeVersionEntityMock,
            mock.patch(
                "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository",
                autospec=True,
            ) as caseTypeRepoMock,
        ):
            repo_factory = MockRepositoryFactory(infra_factory={})
            repo_factory.repositories["case_relation"] = (
                case_relation_repo_mock
            )
            repo_factory.repositories["case"] = caseRepoMock
            repo_factory.repositories["subject"] = subjectRepoMock
            repo_factory.repositories["case_message_list"] = (
                CaseMessageListRepoMock
            )
            repo_factory.repositories["case_type"] = caseTypeRepoMock
            repo_factory.repositories["role"] = roleRepoMock
            repo_factory.repositories["department"] = departmentRepoMock

            repo_factory.repositories["subject_relation"] = (
                relatedSubjectRepoMock
            )
            repo_factory.repositories["person"] = personRepoMock
            repo_factory.repositories["organization"] = organizationRepoMock
            repo_factory.repositories["employee"] = employeeRepoMock

            user_uuid = uuid4()
            self.commandmock = get_command_instance(
                repository_factory=repo_factory,
                context=None,
                user_uuid=str(user_uuid),
                event_service={},
            )
            self.commandmock.user_info = minty.cqrs.UserInfo(
                user_uuid=user_uuid,
                permissions={"admin": False, "behandelaar": True},
            )
            self.case_entity = caseEntityMock
            self.subject_entity = subjectEntityMock
            caseRepoMock.find_case_by_uuid.return_value = caseEntityMock
            subjectRepoMock.find_subject_by_uuid.return_value = (
                subjectEntityMock
            )
            caseTypeRepoMock.find_case_type_version_by_uuid.return_value = (
                caseTypeVersionEntityMock
            )
            self.user_uuid = uuid4()
            self.user_info = UserInfo(
                user_uuid=self.user_uuid, permissions={"admin": True}
            )

    def test_set_case_target_completion_date(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.set_case_target_completion_date(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            target_date="2019-01-30",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.set_target_completion_date.call_args
        assert kwargs["target_completion_date"] == "2019-01-30"
        repo.save.assert_called_once()

    def test_change_case_set_case_completion_date(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.set_case_completion_date(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            target_date="2019-01-29",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.set_completion_date.call_args
        assert kwargs["completion_date"] == "2019-01-29"
        repo.save.assert_called_once()

    def test_pause_case(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.pause_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            suspension_reason="suspension",
            suspension_term_value=6,
            suspension_term_type="weeks",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.pause.call_args
        assert kwargs["suspension_reason"] == "suspension"
        assert kwargs["suspension_term_value"] == 6
        assert kwargs["suspension_term_type"] == "weeks"
        repo.save.assert_called_once()

    def test_pause_case_indefinite_date(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.pause_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            suspension_reason="suspension",
            suspension_term_type="indefinite",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.pause.call_args
        assert kwargs["suspension_reason"] == "suspension"
        assert kwargs["suspension_term_value"] is None
        assert kwargs["suspension_term_type"] == "indefinite"
        repo.save.assert_called_once()

    def test_pause_case_non_existing_interval(self):
        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                suspension_reason="suspension",
                suspension_term_type="Test non existing term type",
            )

    def test_resume_case(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.resume_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            stalled_since_date="2019-01-31",
            stalled_until_date="2019-02-15",
            resume_reason="resume_reason",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.resume.call_args
        assert kwargs["stalled_since_date"] == "2019-01-31"
        assert kwargs["stalled_until_date"] == "2019-02-15"
        assert kwargs["resume_reason"] == "resume_reason"
        repo.save.assert_called_once()

    def test_resume_case_validation(self):
        with pytest.raises(ValidationError):
            self.commandmock.resume_case(
                case_uuid=1,
                stalled_until_date="2019-01-31",
                stalled_since_date="2019-01-01",
            )

        with pytest.raises(ValidationError):
            self.commandmock.resume_case(
                case_uuid=str(uuid4()),
                stalled_since_date="31-01-2019",
                stalled_until_date="2019-01-22",
            )

        with pytest.raises(ValidationError):
            self.commandmock.resume_case(
                case_uuid=str(uuid4()),
                stalled_until_date="2019-01-01",
                stalled_since_date="23-02-2019",
            )

    def test_pause_case_validation(self):
        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid=1,
                stalled_until_date="2019-01-31",
                stalled_since_date="2019-01-31",
                suspension_reason="reason",
            )

        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                stalled_until_date="01-01-2019",
                stalled_since_date="2019-01-31",
                suspension_reason="reason",
            )

        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                stalled_until_date="2019-01-31",
                stalled_since_date="2019-01-31",
                suspension_reason=1,
            )

    def test_assign_case_to_self(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]

        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info
        self.commandmock.assign_case_to_self(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2"
        )
        case_repo.find_case_by_uuid.assert_any_call(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        # Called twice -- once in "reassign_case_messages"
        assert case_repo.get_contact_employee.call_args_list == [
            mock.call(uuid=UUID(self.commandmock.user_uuid))
        ]
        self.case_entity.set_assignee.assert_called_once()
        self.case_entity.set_coordinator.assert_called_once()
        self.case_entity.set_status.assert_called_once()
        case_repo.save.assert_called_once()

    def test_assign_case_to_department(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        department_repo = self.commandmock.repository_factory.repositories[
            "department"
        ]
        role_repo = self.commandmock.repository_factory.repositories["role"]

        self.case_entity.department = None
        self.case_entity.role = None
        self.commandmock.user_info = self.user_info
        self.commandmock.assign_case_to_department(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            department_uuid="a40577cd-21c9-4caa-8d5f-972b8a67c633",
            role_uuid="bffaab57-26fc-4c6b-b321-5f6b6f352b2c",
        )

        case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )
        department_repo.find.assert_called_once_with(
            UUID("a40577cd-21c9-4caa-8d5f-972b8a67c633")
        )
        role_repo.find.assert_called_once_with(
            UUID("bffaab57-26fc-4c6b-b321-5f6b6f352b2c")
        )
        self.case_entity.clear_assignee.assert_called_once()
        self.case_entity.set_allocation.assert_called_once()
        case_repo.save.assert_called_once()

    def test_assign_case_to_user(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info
        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )
        case_repo.find_case_by_uuid.assert_any_call(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )
        # Called twice -- once in "reassign_case_messages"
        assert case_repo.get_contact_employee.call_args_list == [
            mock.call(uuid=UUID("8ee6f9de-d93b-453d-9246-536c83a27318"))
        ]
        self.case_entity.set_assignee.assert_called_once()
        self.case_entity.set_coordinator.assert_called_once()
        self.case_entity.set_status.assert_called_once()
        case_repo.save.assert_called_once()

    def test_assign_case_to_user_no_rights(self):
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info

        repo_factory = self.commandmock.repository_factory
        caseRepoMock = repo_factory.repositories["case"]
        caseRepoMock.find_case_by_uuid.side_effect = [
            self.case_entity,
            NotFound,
        ]
        repo_factory.repositories["case"] = caseRepoMock

        with pytest.raises(Forbidden):
            self.commandmock.assign_case_to_user(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
            )

    def test_assign_case_to_user_with_role_administrator(self):
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info

        repo_factory = self.commandmock.repository_factory
        employeeRepoMock = repo_factory.repositories["employee"]
        employeeRepoMock.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid="58347e47-9b26-4e31-a7b3-ace95bad91b1",
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[
                {
                    "entity_meta_summary": "Administrator",
                    "parent": {"uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b4"},
                    "uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b3",
                }
            ],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )
        repo_factory.repositories["employee"] = employeeRepoMock

        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )

    def test_assign_case_to_user_with_role_zaaksysteembeheerder(self):
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info

        repo_factory = self.commandmock.repository_factory
        employeeRepoMock = repo_factory.repositories["employee"]
        employeeRepoMock.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid="58347e47-9b26-4e31-a7b3-ace95bad91b1",
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[
                {
                    "entity_meta_summary": "Zaaksysteembeheerder",
                    "parent": {"uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b4"},
                    "uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b3",
                }
            ],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )
        repo_factory.repositories["employee"] = employeeRepoMock

        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )

    def test_assign_case_to_user_with_role_behandelaar(self):
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info

        repo_factory = self.commandmock.repository_factory
        employeeRepoMock = repo_factory.repositories["employee"]
        employeeRepoMock.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid="58347e47-9b26-4e31-a7b3-ace95bad91b1",
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[
                {
                    "entity_meta_summary": "Behandelaar",
                    "parent": {"uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b4"},
                    "uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b3",
                }
            ],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )
        repo_factory.repositories["employee"] = employeeRepoMock

        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )

    def test_assign_case_to_user_no_admin(self):
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info

        repo_factory = self.commandmock.repository_factory
        employeeRepoMock = repo_factory.repositories["employee"]
        employeeRepoMock.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid="58347e47-9b26-4e31-a7b3-ace95bad91b1",
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[
                {
                    "entity_meta_summary": "beheerder",
                    "parent": {"uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b4"},
                    "uuid": "58347e47-9b26-4e31-a7b3-ace95bad91b3",
                }
            ],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )
        repo_factory.repositories["employee"] = employeeRepoMock

        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )

    def test_assign_case_to_user_same_uuid(self):
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info

        repo_factory = self.commandmock.repository_factory
        caseRepoMock = repo_factory.repositories["case"]
        caseRepoMock.find_case_by_uuid.side_effect = [
            self.case_entity,
        ]
        repo_factory.repositories["case"] = caseRepoMock

        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid=self.user_info.user_uuid,
        )

    def test_change_case_coordinator(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info
        self.commandmock.change_case_coordinator(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            coordinator_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )

        case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )
        case_repo.get_contact_employee.assert_called_once_with(
            uuid=UUID("8ee6f9de-d93b-453d-9246-536c83a27318")
        )
        self.case_entity.set_coordinator.assert_called_once()
        case_repo.save.assert_called_once()

    def test_reassign_case_messages(self):
        cml_repo = self.commandmock.repository_factory.repositories[
            "case_message_list"
        ]
        subject_repo = self.commandmock.repository_factory.repositories[
            "subject"
        ]

        self.commandmock.reassign_case_messages(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            subject_uuid="59068c82-7194-11e9-b1c4-d761253cfb1c",
        )
        cml_repo.get_messages_for_case.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2")
        )
        subject_repo.find_subject_by_uuid.assert_called_once_with(
            subject_uuid=UUID("59068c82-7194-11e9-b1c4-d761253cfb1c")
        )

        cml_repo.save.assert_called_once()

    def test_transition_case(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        self.commandmock.transition_case(case_uuid=case_uuid)
        case_repo.unsafe_find_case_by_uuid.assert_called_once()
        case_repo.save.assert_called_once()

    def test_set_case_parent(self):
        self.commandmock.user_info = self.user_info
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())
        case_1_uuid = str(uuid4())

        self.commandmock.set_case_parent(
            case_uuid=case_uuid, parent_uuid=case_1_uuid
        )
        case_repo.find_case_by_uuid.assert_called_once()
        case_repo.save.assert_called_once()

    def test_create_subject_relation(self):
        case_uuid = uuid4()
        subject_uuid = uuid4()
        subject = {"type": "person", "id": str(subject_uuid)}
        person_repo = self.commandmock.repository_factory.repositories[
            "person"
        ]
        self.commandmock.user_info = self.user_info
        person_repo.find_person_by_uuid.return_value = Person(
            type="person",
            uuid=subject_uuid,
            name="beheerder",
            first_names="beheerder",
            surname="beheerder",
            contact_information={},
            authenticated=True,
            family_name=None,
            noble_title=None,
            date_of_birth=None,
            date_of_death=None,
            surname_prefix=None,
            gender="M",
            inside_municipality=False,
            foreign_address=False,
            residence_address=None,
            correspondence_address=None,
            related_custom_object_uuid=str(uuid4()),
            has_valid_address=True,
        )
        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission=None,
        )

        subject = {"type": "organization", "id": str(subject_uuid)}
        organization_repo = self.commandmock.repository_factory.repositories[
            "organization"
        ]

        related_custom_object_uuid = uuid4()
        organization_repo.find_organization_by_uuid.return_value = (
            Organization(
                entity_id=subject_uuid,
                uuid=subject_uuid,
                name="beheerder",
                source="source",
                coc_number="12345678",
                coc_location_number="012345678912",
                date_founded=None,
                date_registered=None,
                date_ceased=None,
                rsin=None,
                oin=None,
                main_activity=None,
                secondary_activities=[],
                organization_type=None,
                location_address=None,
                correspondence_address=None,
                contact_information={},
                related_custom_object=RelatedCustomObject(
                    entity_id=related_custom_object_uuid,
                    uuid=related_custom_object_uuid,
                ),
                has_valid_address=True,
                authenticated=True,
            )
        )
        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission=None,
        )

        subject = {"type": "employee", "id": str(subject_uuid)}
        employee_repo = self.commandmock.repository_factory.repositories[
            "employee"
        ]

        employee_repo.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid=subject_uuid,
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )
        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission="read",
        )

        # test create subjectrelation without magic_string
        subject = {"type": "employee", "id": str(subject_uuid)}
        employee_repo = self.commandmock.repository_factory.repositories[
            "employee"
        ]

        employee_repo.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid=subject_uuid,
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )

        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        subject_relation_repo.find_subject_relations_for_case.return_value = [
            mock.MagicMock(
                uuid=uuid4(), role="Advocaat", magic_string_prefix="advocaat1"
            )
        ]

        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission="read",
            magic_string_prefix=None,
        )
        # assert magic_string advocaat1 is generated
        assert (
            subject_relation_repo.create_subject_relation.call_args.args[3]
            == "advocaat2"
        )

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_enqueue_subject_relation_email(self, mock_subject_relation):
        subject_relation_uuid = uuid4()
        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.enqueue_subject_relation_email(
            subject_relation_uuid=str(subject_relation_uuid)
        )
        mock_subject_relation.enqueue_email.assert_called_once()
        subject_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_send_subject_relation_email(self, mock_subject_relation):
        subject_relation_uuid = uuid4()
        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.send_subject_relation_email(
            subject_relation_uuid=str(subject_relation_uuid)
        )
        mock_subject_relation.send_email.assert_called_once()
        subject_relation_repo.save.assert_called_once()

    def test_create_case_relation(self):
        case_relation_repo = self.commandmock.repository_factory.repositories[
            "case_relation"
        ]
        case_relation_repo.create_case_relation = mock.MagicMock()
        uuid1 = str(uuid4())
        uuid2 = str(uuid4())
        user_info = mock.MagicMock()

        user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True},
        )
        self.commandmock.user_info = user_info

        self.commandmock.create_case_relation(uuid1=uuid1, uuid2=uuid2)
        case_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_enqueue_case_assignee_email(self, mock_case):
        case_uuid = str(uuid4())
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        mock_case = mock.MagicMock()
        case_repo.find_case_by_uuid.return_value = mock_case

        self.commandmock.enqueue_case_assignee_email(case_uuid=case_uuid)
        mock_case.enqueue_assignee_email.assert_called_once()
        case_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_send_case_assignee_email(self, mock_subject_relation):
        case_uuid = str(uuid4())
        queue_id = str(uuid4())
        case_repo = self.commandmock.repository_factory.repositories["case"]
        user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True},
        )
        self.commandmock.user_info = user_info
        mock_case = mock.MagicMock()
        case_repo.find_case_by_uuid.return_value = mock_case

        self.commandmock.send_case_assignee_email(
            case_uuid=case_uuid, queue_id=queue_id
        )
        mock_case.send_assignee_email.assert_called_once_with(
            queue_id=queue_id
        )
        case_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_update_subject_relation(self, mock_subject_relation):
        relation_uuid = str(uuid4())

        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        self.commandmock.user_info = self.user_info
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.update_subject_relation(
            relation_uuid=relation_uuid,
            role="Role",
            magic_string_prefix="role",
            authorized=None,
            permission="write",
        )
        mock_subject_relation.update.assert_called_once_with(
            role="Role",
            magic_string_prefix="role",
            authorized=None,
            permission="write",
        )
        subject_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_delete_subject_relation(self, mock_subject_relation):
        relation_uuid = str(uuid4())

        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        mock_subject_relation.source_custom_field_type_id = None
        self.commandmock.user_info = self.user_info
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.delete_subject_relation(relation_uuid=relation_uuid)

        mock_subject_relation.delete.assert_called_once_with()
        subject_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_delete_subject_relation_not_allowed(self, mock_subject_relation):
        relation_uuid = str(uuid4())

        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        mock_subject_relation.source_custom_field_type_id = uuid4()
        mock_subject_relation.source_custom_field_type_name = (
            "Name of relation attribute"
        )

        self.commandmock.user_info = self.user_info
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        with pytest.raises(Conflict) as excinfo:
            self.commandmock.delete_subject_relation(
                relation_uuid=relation_uuid
            )
        assert excinfo.value.args == (
            "Cannot delete subject relation. It is in use in relation attribute 'Name of relation attribute' and must be deleted from this attribute.",
        )

        assert not subject_relation_repo.save.called
