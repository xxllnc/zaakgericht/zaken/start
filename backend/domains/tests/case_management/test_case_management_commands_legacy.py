# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import defaultdict, namedtuple
from datetime import date, datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_create_Case(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid):
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["id"] = "1"
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None
        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": [
                    {
                        "uuid": uuid4(),
                        "name": "custom_field_1",
                        "field_magic_string": "test_subject",
                        "field_type": "text",
                    },
                    {
                        "uuid": type_uuid_1,
                        "name": "custom_field_2",
                        "field_magic_string": "object_relation_1",
                        "field_type": "relationship",
                        "relationship_type": "custom_object",
                    },
                    {
                        "uuid": type_uuid_2,
                        "name": "custom_field_3",
                        "field_magic_string": "object_relation_2",
                        "field_type": "relationship",
                        "relationship_type": "custom_object",
                    },
                ],
                "checklist_items": [],
                "rules": [],
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"
        mock_case_type["results"] = [
            {
                "id": 156,
                "uuid": "a04d7629-ca9a-4246-8e9c-cc38b327e904",
                "name": "Bewaren",
                "result": "behaald",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
            {
                "id": 157,
                "uuid": "604138d9-e14f-4944-812c-12a098a68092",
                "name": "Nog een 2de resultaat",
                "result": "afgebroken",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
        ]

        return mock_case_type

    def _mock_subject(self, uuid, type, role, magic_string_prefix):
        mock_subject = mock.MagicMock()
        mock_subject.configure_mock(
            uuid=uuid,
            type=type,
            role=role,
            magic_string_prefix=magic_string_prefix,
        )
        return mock_subject

    def _mock_file(self):
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        return mock_file

    def _mock_case(self, case_uuid, case_type_uuid):
        mock_case = mock.MagicMock(name="mock_case")
        mock_case.configure_mock(
            id=16,
            uuid=case_uuid,
            case_department={
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            case_role={
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            case_subjects=None,
            vernietigingsdatum=None,
            archival_state=None,
            status="new",
            afhandeldatum=None,
            stalled_until=None,
            milestone=1,
            suspension_reason=None,
            completion=None,
            last_modified=date(2019, 3, 30),
            coordinator=None,
            assignee=None,
            requestor=None,
            subject="Parking permit",
            subject_extern="Parking permit amsterdam",
            case_type_uuid=case_type_uuid,
            case_type_version_uuid=uuid4(),
            aanvraag_trigger="extern",
            contactkanaal="post",
            resume_reason=None,
            summary="Perking permit Amsterdam",
            payment_amount="100",
            payment_status="success",
            confidentiality="public",
            attributes="{}",
            prefix="1",
            route_ou="1",
            route_role="role_id",
            behandelaar=100,
            behandelaar_gm_id=1,
            coordinator_gm_id=1,
            created=date(2020, 1, 1),
            registratiedatum=date(2020, 1, 1),
            streefafhandeldatum=date(2020, 1, 5),
            urgency="No",
            preset_client="No",
            onderwerp="test",
            onderwerp_extern="test summary",
            resultaat="test",
            resultaat_id=1,
            result_uuid=uuid4(),
            aanvrager=200,
            aanvrager_gm_id=1,
            selectielijst=None,
            case_status={
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            case_meta={
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            case_actions=[
                {
                    "status": 2,
                    "type": "allocation",
                    "data": '{"original_ou_id":1,"description":"Toewijzing","role_set":1,"original_role_id":12,"ou_id":1,"role_id":12}',
                    "automatic": True,
                },
                {
                    "status": 1,
                    "type": "case",
                    "data": '{"betrokkene_authorized":null,"eigenaar_role":null,"betrokkene_id":"","automatisch_behandelen":1,"last_modified":"2022-05-18 09:50:39.261554","copy_related_cases":0,"pip_label":null,"id":184,"eigenaar_type":"aanvrager","betrokkene_role":"Advocaat","zaaktype_status_id":496,"copy_subject_role":0,"copy_related_objects":0,"betrokkene_notify":null,"relatie_type":"deelzaak","required":"3","betrokkene_prefix":"","created":"2022-05-18 09:50:39.261554","relatie_zaaktype_id":1,"status":1,"start_delay":"","ou_id":1,"role_id":12,"show_in_pip":0,"betrokkene_role_set":null,"subject_role":null,"eigenaar_id":"","zaaktype_node_id":216,"related_casetype_uuid":"84f238fd-68c7-4022-8ce1-e8d08cb8f611","kopieren_kenmerken":1,"creation_style":"background","copy_selected_attributes":null}',
                    "automatic": True,
                },
                {
                    "status": 1,
                    "type": "documents",
                    "data": '{"id": 4}',
                    "automatic": True,
                },
                {
                    "status": 1,
                    "type": "subjects",
                    "data": '{"id": 5}',
                    "automatic": True,
                },
                {
                    "status": 1,
                    "type": "allocation",
                    "data": '{"original_role_id":12,"ou_id":1,"role_id":12,"description":"Toewijzing","original_ou_id":1,"role_set":null}',
                    "automatic": None,
                },
                {
                    "status": 1,
                    "type": "email",
                    "data": '{"cc":"sharda@mintlab.nl","email":"","body":"test doc added","betrokkene_naam":null,"sender_address":"sharda@mintlab.nl","automatic_phase":1,"bcc":"","description":"E-mail","bibliotheek_notificaties_id":1,"case_document_attachments":[],"zaaktype_notificatie_id":102,"subject":"Test doc added","behandelaar":"","rcpt":"aanvrager","intern_block":null,"betrokkene_role":"Advocaat","sender":"sharda@mintlab.nl"}',
                    "automatic": True,
                },
            ],
            custom_fields=[
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                    "is_multiple": False,
                },
            ],
            file_custom_fields=None,
            destructable=False,
            aggregation_scope="Dossier",
            number_parent=None,
            number_master=737,
            relations=",738",
            number_previous=None,
            premature_completion_rationale=None,
            price=100,
            type_of_archiving="Bewaren (B)",
            period_of_preservation="4 weken",
            suspension_rationale=None,
            result_description="closing the case",
            result_explanation="closing the case",
            result_selection_list_number="10",
            result_process_type_number="11",
            result_process_type_name="test-process-type",
            result_process_type_description="This is test process type",
            result_process_type_explanation="This is test process type",
            result_process_type_object="Procestype-object",
            result_process_type_generic="Generiek",
            result_origin="Systeemanalyse",
            result_process_term="A",
            active_selection_list="test list",
            days_left=10,
            lead_time_real=None,
            progress_days=10,
            case_location="Test locataion",
            user_is_admin=False,
            user_is_requestor=False,
            user_is_assignee=False,
            user_is_coordinator=False,
            authorizations=["read", "write"],
        )
        return mock_case

    @mock.patch("urllib.request.urlopen")
    def test_create_subcases(self, mock_urlopen):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_case_type = self.__mock_case_type(case_type_uuid)

        self.session.query().filter().one.side_effect = [uuid4()]
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)
        #   case_type_info.ZaaktypeNode.uuid = str(case_type_uuid)

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()
        queue_id = str(uuid4())

        self.cmd.repository_factory.repositories["case"].context = "dev"
        self.cmd.create_subcases(
            case_uuid=case_uuid,
            queue_ids=[queue_id],
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

    def test_create_subcases_case_not_found(
        self,
    ):
        case_uuid = str(uuid4())
        self.session.execute().fetchone.side_effect = [
            None,
        ]

        self.session.reset_mock()
        queue_id = str(uuid4())
        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_subcases(
                case_uuid=case_uuid,
                queue_ids=[queue_id],
            )
        assert excinfo.value.args == (
            f"Case with uuid '{case_uuid}' not found.",
        )

    def test_enqueue_subcases(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 11
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.repository_factory.repositories["case"].context = "dev"
        self.cmd.enqueue_subcases(case_uuid=case_uuid)
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 12

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

        insert_statement = call_list[11][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s::UUID, %(object_id)s::UUID, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s::UUID, %(priority)s, %(metadata)s)"
        )

    def test_enqueue_documents(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 12
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()
        self.cmd.enqueue_documents(case_uuid=case_uuid)
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 12

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

        insert_statement = call_list[11][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s::UUID, %(object_id)s::UUID, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s::UUID, %(priority)s, %(metadata)s)"
        )

    @mock.patch("urllib.request.urlopen")
    def test_enqueue_email(self, mock_urlopen):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.repository_factory.repositories["case"].context = "dev"
        self.cmd.enqueue_emails(case_uuid=case_uuid)
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 12

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

        insert_statement = call_list[11][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s::UUID, %(object_id)s::UUID, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s::UUID, %(priority)s, %(metadata)s)"
        )

    @mock.patch("urllib.request.urlopen")
    def test_enqueue_subjects(self, mock_urlopen):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)
        #  self.session.query().filter().one.side_effect = [uuid4()]
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.repository_factory.repositories["case"].context = "dev"
        self.cmd.enqueue_subjects(case_uuid=case_uuid)
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 12

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

        insert_statement = call_list[11][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert) == (
            "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s::UUID, %(object_id)s::UUID, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s::UUID, %(priority)s, %(metadata)s)"
        )

    @mock.patch("urllib.request.urlopen")
    def test_generate_documents(self, mock_urlopen):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.repository_factory.repositories["case"].context = "dev"
        queue_id = str(uuid4())
        self.cmd.generate_documents(case_uuid=case_uuid, queue_ids=[queue_id])
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

    @mock.patch("urllib.request.urlopen")
    def test_generate_emails(self, mock_urlopen):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)

        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )

        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.repository_factory.repositories["case"].context = "dev"
        queue_id = str(uuid4())
        self.cmd.generate_emails(case_uuid=case_uuid, queue_ids=[queue_id])
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )

    @mock.patch("urllib.request.urlopen")
    def test_generate_subjects(self, mock_urlopen):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(case_type_uuid)

        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = self._mock_subject(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = self._mock_subject(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_case = self._mock_case(case_uuid, case_type_uuid)
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1],
            [mock_related_case_1],
            [self._mock_file()],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.repository_factory.repositories["case"].context = "dev"
        queue_id = str(uuid4())
        self.cmd.generate_subjects(case_uuid=case_uuid, queue_ids=[queue_id])

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, %(param_2)s AS authorizations, %(param_3)s AS user_is_requestor, %(param_4)s AS user_is_assignee, %(param_5)s AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_1)s::UUID AND view_case_v2.status != %(status_1)s"
        )
