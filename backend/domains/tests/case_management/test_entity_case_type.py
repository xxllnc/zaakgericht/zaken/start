# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from unittest import mock
from uuid import uuid4
from zsnl_domains.case_management.entities import CaseTypeVersionEntity


class TestCaseTypeVersion:
    def test_case_type_entity(self):
        event_service = mock.MagicMock()
        case_type_uuid = uuid4()
        case_type = CaseTypeVersionEntity(
            uuid=case_type_uuid,
            id=1,
            case_type_uuid=uuid4(),
            version=2,
            name="Declaratie",
            active=False,
            description="description",
            identification="identification",
            tags="tags",
            case_summary="Declaratie",
            case_public_summary="Declaratie",
            requestor={},
            catalog_folder=None,
            terms={
                "lead_time_legal": {"type": "kalenderdagen", "value": 56},
                "lead_time_service": {"type": "kalenderdagen", "value": 123},
            },
            initiator_source="external",
            initiator_type="aangaan",
            metadata={"process_description": "", "legal_basis": ""},
            phases=[
                {
                    "name": "phase_1",
                    "milestone": 1,
                    "phase": "Regeisteren",
                    "department": {
                        "uuid": uuid4(),
                        "table_id": 3,
                        "name": "dev",
                    },
                    "role": {"uuid": uuid4(), "table_id": 5, "name": "admin"},
                }
            ],
            case_type_results=[],
            is_eligible_for_case_creation=True,
            preset_assignee=None,
            created=datetime(2019, 4, 3),
            last_modified=datetime(2019, 4, 5),
            settings={"payment": {"amount": "123"}},
            event_service=event_service,
        )
        assert case_type.uuid == case_type_uuid
