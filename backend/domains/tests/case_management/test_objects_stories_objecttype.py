# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import copy
import pydantic.v1
import pytest
from copy import deepcopy
from datetime import datetime, timedelta, timezone
from minty import exceptions
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from time import process_time
from unittest import mock
from unittest.mock import MagicMock, patch
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.repositories import custom_object_type
from zsnl_domains.shared.custom_field import (
    CustomFieldRelationshipTypes,
    CustomFieldTypes,
)


class ObjectTypeTestBase(TestBase):
    _fetchone_cache = {}

    def _get_db_params(
        self,
        objecttype_uuid=None,
        version_independent_uuid=None,
        relationship_object_type_uuid=None,
        relationship_case_type_uuid=None,
    ):
        if objecttype_uuid is None:
            objecttype_uuid = uuid4()
        if version_independent_uuid is None:
            version_independent_uuid = uuid4()
        if relationship_object_type_uuid is None:
            relationship_object_type_uuid = uuid4()
        if relationship_case_type_uuid is None:
            relationship_case_type_uuid = uuid4()

        return {
            "version_independent_uuid": version_independent_uuid,
            "authorization_definition": {
                "authorizations": [
                    {
                        "authorization": "admin",
                        "role": {"name": "Beheerder", "uuid": uuid4()},
                        "department": {"name": "Mintlab", "uuid": uuid4()},
                    },
                    {
                        "authorization": "read",
                        "role": {"name": "Behandelaar", "uuid": uuid4()},
                        "department": {"name": "Mintlab", "uuid": uuid4()},
                    },
                ]
            },
            "v_uuid": objecttype_uuid,
            "v_name": "parking_permit",
            "v_title": "[[ object.name ]]",
            "v_subtitle": "[[ object.subtitle ]]",
            "v_external_reference": "External Reference",
            "v_status": "active",
            "v_is_active_version": 0,
            "v_custom_field_definition": {
                "custom_fields": [
                    {
                        "attribute_uuid": UUID(
                            "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f"
                        ),
                        "custom_field_type": "text",
                        "description": None,
                        "external_description": None,
                        "is_hidden_field": False,
                        "is_required": False,
                        "label": "Date the problem started",
                        "magic_string": None,
                        "options": None,
                        "name": "ZTC Subject",
                        "custom_field_specification": None,
                    },
                    {
                        "attribute_uuid": UUID(
                            "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7e"
                        ),
                        "custom_field_type": "relationship",
                        "description": None,
                        "external_description": None,
                        "is_hidden_field": False,
                        "is_required": False,
                        "label": "Object Relation",
                        "magic_string": "ztc_object_relation",
                        "options": None,
                        "name": "ZTC Object Relation",
                        "custom_field_specification": {
                            "use_on_map": True,
                            "uuid": UUID(
                                "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7d"
                            ),
                            "type": "custom_object",
                            "name": "Custom Object 123",
                        },
                    },
                ]
            },
            "v_relationship_definition": {
                "relationships": [
                    {
                        "custom_object_type_uuid": relationship_object_type_uuid,
                        "is_required": True,
                        "description": "Description",
                        "external_description": "External description",
                        "name": "Contract",
                    },
                    {
                        "case_type_uuid": relationship_case_type_uuid,
                        "name": "Melding",
                    },
                ]
            },
            "v_audit_log": {
                "updated_components": ["attributes", "authorizations"],
                "description": "Revoked access from Frits and added name field",
            },
            "lv_name": "parking_permit_old_version",
            "lv_title": "[[ object.old_version_name ]]",
            "lv_subtitle": "[[ object.subtitle_name ]]",
            "lv_external_reference": "External Reference Previous",
            "lv_uuid": uuid4(),
            "lv_status": "active",
            "lv_custom_field_definition": {
                "custom_fields": [
                    {
                        "attribute_uuid": UUID(
                            "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f"
                        ),
                        "custom_field_type": "text",
                        "description": None,
                        "external_description": None,
                        "is_hidden_field": False,
                        "is_required": False,
                        "label": "Date the problem was last version",
                        "magic_string": None,
                        "type_multiple": False,
                        "options": None,
                        "name": "ZTC Subject",
                        "custom_field_specification": None,
                    },
                    {
                        "attribute_uuid": UUID(
                            "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7e"
                        ),
                        "custom_field_type": "relationship",
                        "description": None,
                        "external_description": None,
                        "is_hidden_field": False,
                        "is_required": False,
                        "label": "Object Relation",
                        "magic_string": "ztc_object_relation",
                        "type_multiple": True,
                        "options": None,
                        "name": "ZTC Object Relation",
                        "custom_field_specification": {
                            "use_on_map": True,
                            "uuid": UUID(
                                "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7d"
                            ),
                            "type": "custom_object",
                            "name": "Custom Object 123",
                        },
                    },
                ]
            },
            "lv_relationship_definition": {
                "relationships": [
                    {
                        "custom_object_type_uuid": relationship_object_type_uuid,
                        "name": "Old Contract",
                    },
                    {
                        "case_type_uuid": relationship_case_type_uuid,
                        "name": "Melding Old",
                    },
                ]
            },
            "lv_audit_log": {
                "updated_components": ["attributes"],
                "description": "Added access to Frits and added status field",
            },
            "authorizations": ["admin", "readwrite"],
        }

    def load_row_into_fetchone(
        self,
        objecttype_uuid=None,
        version_independent_uuid=None,
        skip_authorization_definition=False,
        relationship_object_type_uuid=None,
        relationship_case_type_uuid=None,
        custom_field_type="text",
    ):
        dbparams = self._get_db_params(
            objecttype_uuid=objecttype_uuid,
            version_independent_uuid=version_independent_uuid,
            relationship_object_type_uuid=relationship_object_type_uuid,
            relationship_case_type_uuid=relationship_case_type_uuid,
        )

        if skip_authorization_definition:
            self._fetchone_cache = {}
            dbparams["authorization_definition"]["authorizations"] = None

        self.mock_fetchall(
            objecttype_uuid=objecttype_uuid,
            version_independent_uuid=version_independent_uuid,
            dbparams=dbparams,
            custom_field_type=custom_field_type,
        )

        try:
            self.session.execute().fetchone.return_value = (
                self._fetchone_cache[objecttype_uuid][
                    version_independent_uuid
                ][0]
            )
        except Exception:
            rv = MagicMock(**dbparams)
            self.session.execute().fetchone.return_value = rv
            self._fetchone_cache[objecttype_uuid] = {}
            self._fetchone_cache[objecttype_uuid][version_independent_uuid] = [
                rv,
                dbparams,
            ]

        return self._fetchone_cache[objecttype_uuid][version_independent_uuid][
            1
        ]

    def _mock_relationships(self, relationships):
        magic_ships = []
        for relationship in relationships:
            if "custom_object_type_uuid" in relationship.keys():
                magic_ships.append(
                    MagicMock(
                        uuid=relationship["custom_object_type_uuid"],
                        type="custom_object_type",
                    )
                )

            if "case_type_uuid" in relationship.keys():
                magic_ships.append(
                    MagicMock(
                        uuid=relationship["case_type_uuid"], type="case_type"
                    )
                )

        return magic_ships

    def mock_fetchall(
        self,
        objecttype_uuid=None,
        version_independent_uuid=None,
        dbparams=None,
        no_existing_authorizations=False,
        use_authorizations=None,
        relationship_object_type_uuid=None,
        relationship_case_type_uuid=None,
        custom_field_type="text",
    ):
        if not dbparams:
            dbparams = self._get_db_params(
                objecttype_uuid=objecttype_uuid or uuid4(),
                version_independent_uuid=version_independent_uuid or uuid4(),
                relationship_object_type_uuid=relationship_object_type_uuid
                or uuid4(),
                relationship_case_type_uuid=relationship_case_type_uuid
                or uuid4(),
            )

        authorizations = dbparams["authorization_definition"]["authorizations"]
        if use_authorizations:
            authorizations = use_authorizations

        def load_fetchall():
            latest_query = str(self.session.execute.call_args_list[-1][0][0])

            if (
                "UNION ALL SELECT custom_object_type.uuid AS uuid"
                in latest_query
            ):
                return self._mock_relationships(
                    dbparams["v_relationship_definition"]["relationships"]
                )

            if "bibliotheek_kenmerken.uuid" in latest_query:
                return [
                    MagicMock(
                        uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                        naam="ZTC Subject",
                        magic_string="ztc_subject",
                        value_type=custom_field_type,
                        value_default=None,
                        type_multiple=True,
                        options=["step1"],
                    ),
                    MagicMock(
                        uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7e",
                        naam="ZTC Object Relation",
                        magic_string="ztc_object_relation",
                        value_type="relationship",
                        value_default=None,
                        type_multiple=False,
                        options=[],
                        relationship_type="custom_object",
                        relationship_name="Custom Object 123",
                        relationship_uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7d",
                    ),
                ]

            if not no_existing_authorizations:
                if "groups.uuid" in latest_query:
                    dep1 = MagicMock(**authorizations[0]["department"])
                    type(dep1).name = authorizations[0]["department"]["name"]
                    dep2 = MagicMock(**authorizations[1]["department"])
                    type(dep2).name = authorizations[1]["department"]["name"]
                    return [dep1, dep2]

                if "roles.uuid" in latest_query:
                    role1 = MagicMock(**authorizations[0]["role"])
                    type(role1).name = authorizations[0]["role"]["name"]
                    role2 = MagicMock(**authorizations[1]["role"])
                    type(role2).name = authorizations[1]["role"]["name"]
                    return [role1, role2]

            return []

        self.session.execute().fetchall.side_effect = load_fetchall


class TestAs_An_Administrator_I_Want_To_Create_An_ObjectType(
    ObjectTypeTestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_object_has_valid_name_title_and_status(self):
        objecttype_uuid = str(uuid4())

        self.cmd.create_custom_object_type(
            uuid=objecttype_uuid,
            catalog_folder_id=1,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        # Expect two table inserts and an update
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Insert"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Update"

        # Compile first insert query
        query = call_list[0][0][0].compile()

        assert query.params["date_created"]
        assert query.params["last_modified"]
        assert query.params["name"] == "parking_permit"
        assert query.params["status"] == "active"
        assert query.params["title"] == "[[ object.name ]]"
        assert query.params["external_reference"] == "External Reference"
        assert query.params["uuid"] == objecttype_uuid
        assert query.params["version"] == 1
        assert query.params["audit_log"] == {
            "description": "bla",
            "updated_components": ["attributes", "authorizations"],
        }

        # Compile second insert query
        query = call_list[1][0][0].compile()
        assert query.params["uuid"]
        assert query.params["custom_object_type_version_id"]
        assert query.params["catalog_folder_id"] == 1

        # Check update query
        query = call_list[2][0][0].compile()
        assert query.params["custom_object_type_id"]

    @patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository._get_catalog_folder_id"
    )
    def test_create_object_type_with_catalog_folder_uuid(
        self, mock_get_folder_id
    ):
        objecttype_uuid = str(uuid4())

        catalog_folder_uuid = str(uuid4())
        mock_get_folder_id.return_value = 33

        self.cmd.create_custom_object_type(
            uuid=objecttype_uuid,
            catalog_folder_uuid=catalog_folder_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        # Expect two table inserts and an update
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Insert"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Update"

        # Compile first insert query
        query = call_list[0][0][0].compile()

        assert query.params["date_created"]
        assert query.params["last_modified"]
        assert query.params["name"] == "parking_permit"
        assert query.params["status"] == "active"
        assert query.params["title"] == "[[ object.name ]]"
        assert query.params["external_reference"] == "External Reference"
        assert query.params["uuid"] == objecttype_uuid
        assert query.params["version"] == 1
        assert query.params["audit_log"] == {
            "description": "bla",
            "updated_components": ["attributes", "authorizations"],
        }

        # Compile second insert query
        query = call_list[1][0][0].compile()
        assert query.params["uuid"]
        assert query.params["custom_object_type_version_id"]
        assert query.params["catalog_folder_id"] == 33

        # Check update query
        query = call_list[2][0][0].compile()
        assert query.params["custom_object_type_id"]

    def test_object_requires_a_uuid(self):
        with pytest.raises(exceptions.ValidationError) as excinfo:
            self.cmd.create_custom_object_type(
                name="parking_permit",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        assert "'uuid' is a required property" in str(excinfo.value)

    def test_object_only_accepts_valid_status(self):
        with pytest.raises(exceptions.ValidationError) as excinfo:
            objecttype_uuid = str(uuid4())

            self.cmd.create_custom_object_type(
                uuid=objecttype_uuid,
                name="parking_permit",
                title="[[ object.name ]]",
                status="activee",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        assert "active" in str(excinfo.value)

    def test_object_fails_on_missing_title_and_name(self):
        with pytest.raises(exceptions.ValidationError) as excinfo:
            objecttype_uuid = str(uuid4())

            self.cmd.create_custom_object_type(
                uuid=objecttype_uuid, status="active"
            )

        assert "'name' is a required property" in str(excinfo.value)

    def test_create_objecttype_with_customfields_exceptions(self):
        objecttype_uuid = str(uuid4())

        with pytest.raises(exceptions.NotFound) as excinfo:
            self.get_cmd().create_custom_object_type(
                custom_field_definition={
                    "custom_fields": [{"label": "Date the problem started"}]
                },
                uuid=objecttype_uuid,
                name="parking_permit",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        assert (
            "Could not find attribute in library with missing 'attribute_uuid'"
            in str(excinfo.value)
        )

        with pytest.raises(exceptions.NotFound) as excinfo:
            self.get_cmd().create_custom_object_type(
                custom_field_definition={
                    "custom_fields": [
                        {
                            "label": "Date the problem started",
                            "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                        }
                    ]
                },
                uuid=objecttype_uuid,
                name="parking_permit",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        assert "Could not find attribute in library with UUID" in str(
            excinfo.value
        )

    def test_loading_of_attributes_check_missing_custom_field(self):
        objecttype_uuid = str(uuid4())

        # Check if
        self.get_cmd().create_custom_object_type(
            custom_field_definition={},
            uuid=objecttype_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

    def test_loading_of_relationships_check_missing_relationships(self):
        objecttype_uuid = str(uuid4())

        # Check if
        self.get_cmd().create_custom_object_type(
            relationship_definition={},
            uuid=objecttype_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

    def test_create_objecttype_with_customfields(self, mocker):
        objecttype_uuid = str(uuid4())

        self.session.execute().fetchall.return_value = [
            MagicMock(
                uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                naam="ZTC Subject",
                magic_string="ztc_subject",
                value_type="text",
                value_default=None,
                options=None,
                external_reference="External Reference",
                type_multiple=True,
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )
        ]

        self.get_cmd().create_custom_object_type(
            custom_field_definition={
                "custom_fields": [
                    {
                        "name": "Startdate",
                        "label": "Date the problem started",
                        "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                    }
                ]
            },
            uuid=objecttype_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        call_list = self.session.execute.call_args_list
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Insert"
        assert call_list[5][0][0].__class__.__name__ == "Update"

        query = call_list[3][0][0].compile()

        assert {
            "custom_fields": [
                {
                    "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                    "custom_field_type": "text",
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Date the problem started",
                    "magic_string": "ztc_subject",
                    "options": None,
                    "name": "ZTC Subject",
                    "custom_field_specification": None,
                    "multiple_values": True,
                }
            ]
        } == query.params["custom_field_definition"]

    def test_create_objecttype_with_geojson_field_has_use_on_map(self, mocker):
        objecttype_uuid = str(uuid4())

        self.session.execute().fetchall.return_value = [
            MagicMock(
                uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                naam="GeoLocation1",
                magic_string="geolocation1",
                value_type="geojson",
                value_default=None,
                options=None,
                external_reference="External Reference",
                custom_field_specification={"use_on_map": True},
                type_multiple=False,
            )
        ]

        self.get_cmd().create_custom_object_type(
            custom_field_definition={
                "custom_fields": [
                    {
                        "custom_field_specification": {"use_on_map": True},
                        "name": "Home Location",
                        "label": "GeoLocation of your home",
                        "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                    }
                ]
            },
            uuid=objecttype_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        call_list = self.session.execute.call_args_list
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Insert"
        assert call_list[5][0][0].__class__.__name__ == "Update"

        query = call_list[3][0][0].compile()

        assert {
            "custom_fields": [
                {
                    "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                    "custom_field_type": "geojson",
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "GeoLocation of your home",
                    "magic_string": "geolocation1",
                    "options": None,
                    "name": "GeoLocation1",
                    "custom_field_specification": {"use_on_map": True},
                    "multiple_values": False,
                }
            ]
        } == query.params["custom_field_definition"]

    def test_create_objecttype_with_acl(self):
        objecttype_uuid = str(uuid4())

        with pytest.raises(KeyError):
            self.cmd.create_custom_object_type(
                authorization_definition={
                    "authorizations": [{"authorization": "unexisting"}]
                },
                uuid=objecttype_uuid,
                name="parking_permit",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        with pytest.raises(KeyError):
            self.cmd.create_custom_object_type(
                authorization_definition={},
                uuid=objecttype_uuid,
                name="parking_permit",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        auth_definition = {
            "authorizations": [
                {
                    "authorization": "admin",
                    "role": {"name": "Beheerder", "uuid": str(uuid4())},
                    "department": {"name": "Mintlab", "uuid": str(uuid4())},
                },
                {
                    "authorization": "readwrite",
                    "role": {"name": "Behandelaar", "uuid": str(uuid4())},
                    "department": {"name": "Mintlab", "uuid": str(uuid4())},
                },
            ]
        }

        auth_params = deepcopy(auth_definition)
        del auth_params["authorizations"][0]["role"]["name"]
        del auth_params["authorizations"][0]["department"]["name"]
        del auth_params["authorizations"][1]["role"]["name"]
        del auth_params["authorizations"][1]["department"]["name"]

        self.mock_fetchall(
            objecttype_uuid=objecttype_uuid,
            use_authorizations=auth_definition["authorizations"],
        )

        self.get_cmd().create_custom_object_type(
            authorization_definition=auth_params,
            uuid=objecttype_uuid,
            name="important_object",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        call_list = self.session.execute.call_args_list
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Insert"
        assert call_list[5][0][0].__class__.__name__ == "Update"

        # Compile first insert query
        query = call_list[3][0][0].compile()

        assert query.params["date_created"]
        assert query.params["last_modified"]
        assert query.params["name"] == "important_object"
        assert query.params["status"] == "active"
        assert query.params["title"] == "[[ object.name ]]"
        assert query.params["uuid"] == objecttype_uuid
        assert query.params["version"] == 1
        assert query.params["audit_log"] == {
            "description": "bla",
            "updated_components": ["attributes", "authorizations"],
        }
        assert query.params["external_reference"] == "External Reference"

        # Compile second insert query
        query = call_list[4][0][0].compile()
        assert query.params["uuid"]
        assert query.params["custom_object_type_version_id"]
        assert auth_definition == query.params["authorization_definition"]

        with pytest.raises(exceptions.NotFound):
            new_definition = copy.deepcopy(auth_definition)
            new_definition["authorizations"][0]["role"]["uuid"] = uuid4()
            self.get_cmd().create_custom_object_type(
                authorization_definition=new_definition,
                uuid=objecttype_uuid,
                name="important_object",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        with pytest.raises(exceptions.NotFound):
            new_definition = copy.deepcopy(auth_definition)
            new_definition["authorizations"][0]["department"]["uuid"] = uuid4()
            self.get_cmd().create_custom_object_type(
                authorization_definition=new_definition,
                uuid=objecttype_uuid,
                name="important_object",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

    def test_create_objecttype_with_nonexisting_relationships(self):
        objecttype_uuid = str(uuid4())

        with pytest.raises(exceptions.NotFound) as excinfo:
            self.cmd.create_custom_object_type(
                relationship_definition={
                    "relationships": [{"custom_object_type_uuid": uuid4()}]
                },
                uuid=objecttype_uuid,
                name="important_object",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        assert "Could not find relationship" in str(excinfo.value)

    def test_create_objecttype_with_duplicate_relationships(self):
        objecttype_uuid = str(uuid4())
        relationship_uuid = uuid4()
        case_type_uuid = uuid4()

        self.mock_fetchall(
            objecttype_uuid=objecttype_uuid,
            relationship_object_type_uuid=relationship_uuid,
            relationship_case_type_uuid=case_type_uuid,
        )

        with pytest.raises(pydantic.v1.ValidationError) as excinfo:
            self.cmd.create_custom_object_type(
                relationship_definition={
                    "relationships": [
                        {
                            "custom_object_type_uuid": relationship_uuid,
                            "case_type_uuid": case_type_uuid,
                        }
                    ]
                },
                uuid=objecttype_uuid,
                name="important_object",
                title="[[ object.name ]]",
                status="active",
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )

        assert "is already set" in str(excinfo.value)

    def test_create_objecttype_with_relationships(self):
        objecttype_uuid = str(uuid4())
        relationship_uuid = uuid4()

        self.mock_fetchall(
            objecttype_uuid=objecttype_uuid,
            relationship_object_type_uuid=relationship_uuid,
        )

        self.cmd.create_custom_object_type(
            relationship_definition={
                "relationships": [
                    {"custom_object_type_uuid": relationship_uuid}
                ]
            },
            uuid=objecttype_uuid,
            name="important_object",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        call_list = self.session.execute.call_args_list

        # Compile first insert query
        query = call_list[1][0][0].compile()
        assert query.params["uuid_1"] == [relationship_uuid]

        query = call_list[3][0][0].compile()

        assert query.params["name"] == "important_object"
        assert query.params["uuid"] == objecttype_uuid
        assert query.params["relationship_definition"]
        assert (
            query.params["relationship_definition"]["relationships"][0][
                "case_type_uuid"
            ]
            is None
        )
        assert query.params["relationship_definition"]["relationships"][0][
            "custom_object_type_uuid"
        ] == str(relationship_uuid)

    def test_create_objecttype_with_customfields_relationship(self, mocker):
        objecttype_uuid = str(uuid4())

        self.session.execute().fetchall.return_value = [
            MagicMock(
                uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                naam="ZTC Object Relation",
                magic_string="ztc_object_relation",
                value_type="relationship",
                value_default=None,
                options=None,
                external_reference="External Reference",
                relationship_type="custom_object",
                relationship_uuid="ec5cfb98-2ed6-45cc-a382-d47bfbba3e7c",
                relationship_name="Custom object 123",
                type_multiple=True,
            )
        ]

        self.get_cmd().create_custom_object_type(
            custom_field_definition={
                "custom_fields": [
                    {
                        "name": "ZTC Object Relation",
                        "label": "Custom object relation",
                        "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                        "custom_field_specification": {"use_on_map": True},
                    }
                ]
            },
            uuid=objecttype_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        call_list = self.session.execute.call_args_list
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Insert"
        assert call_list[5][0][0].__class__.__name__ == "Update"

        query = call_list[3][0][0].compile()

        assert {
            "custom_fields": [
                {
                    "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                    "custom_field_type": "relationship",
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Custom object relation",
                    "magic_string": "ztc_object_relation",
                    "options": None,
                    "name": "ZTC Object Relation",
                    "multiple_values": True,
                    "custom_field_specification": {
                        "use_on_map": True,
                        "type": "custom_object",
                        "uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7c",
                        "name": "Custom object 123",
                    },
                }
            ]
        } == query.params["custom_field_definition"]

    def test_create_objecttype_with_document_attribute(self, mocker):
        objecttype_uuid = str(uuid4())

        self.session.execute().fetchall.return_value = [
            MagicMock(
                uuid="7ef5bb43-f476-4201-a666-84f9f9de9a34",
                naam="Document attribute",
                magic_string="document_attribute",
                value_type="file",
                value_default=None,
                options=None,
                type_multiple=False,
            )
        ]

        self.get_cmd().create_custom_object_type(
            custom_field_definition={
                "custom_fields": [
                    {
                        "name": "Document attribute",
                        "label": "Document",
                        "attribute_uuid": "7ef5bb43-f476-4201-a666-84f9f9de9a34",
                    }
                ]
            },
            uuid=objecttype_uuid,
            name="parking_permit",
            title="[[ object.name ]]",
            status="active",
            external_reference="External Reference",
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "bla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeCreated")

        call_list = self.session.execute.call_args_list
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Insert"
        assert call_list[5][0][0].__class__.__name__ == "Update"

        query = call_list[3][0][0].compile()

        assert {
            "custom_fields": [
                {
                    "label": "Document",
                    "name": "Document attribute",
                    "description": None,
                    "external_description": None,
                    "is_required": False,
                    "is_hidden_field": False,
                    "multiple_values": False,
                    "attribute_uuid": "7ef5bb43-f476-4201-a666-84f9f9de9a34",
                    "options": None,
                    "custom_field_type": "file",
                    "magic_string": "document_attribute",
                    "custom_field_specification": None,
                }
            ]
        } == query.params["custom_field_definition"]


class TestAs_An_Administrator_I_Want_To_Get_An_ObjectType(ObjectTypeTestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_name_title_status_and_dates_are_correctly_queried(self):
        objecttype_uuid = "c7d81da3-c28d-4910-a427-48dfa0e6e3e8"
        self.session.execute().fetchone.return_value = None

        objecttype = None
        with pytest.raises(exceptions.NotFound):
            objecttype = self.qry.get_custom_object_type_by_uuid(
                uuid=objecttype_uuid
            )

        assert objecttype is None

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert query.params["uuid_1"] == self.qry.user_info.user_uuid
        assert query.params["uuid_2"] == UUID(
            "c7d81da3-c28d-4910-a427-48dfa0e6e3e8"
        )
        assert query.params["date_deleted_1"] < datetime.now(timezone.utc)
        assert query.params["date_deleted_1"] > (
            datetime.now(timezone.utc) - timedelta(minutes=1)
        )

    # @pytest.mark.xfail(strict=True)
    def test_got_objecttype_by_uuid(self):
        objecttype_uuid = uuid4()
        version_independent_uuid = uuid4()
        dbparams = self.load_row_into_fetchone(
            objecttype_uuid, version_independent_uuid
        )

        objecttype = self.qry.get_custom_object_type_by_uuid(
            uuid=str(objecttype_uuid)
        )

        assert objecttype.uuid == objecttype_uuid

        assert objecttype.uuid == dbparams["v_uuid"]
        assert objecttype.name == dbparams["v_name"]
        assert objecttype.title == dbparams["v_title"]
        assert objecttype.status == dbparams["v_status"]

        assert objecttype.entity_meta_authorizations == {"admin", "readwrite"}
        assert (
            objecttype.authorization_definition.dict()
            == dbparams["authorization_definition"]
        )
        assert objecttype.custom_field_definition.dict() == {
            "custom_fields": [
                {
                    "attribute_uuid": UUID(
                        "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f"
                    ),
                    "custom_field_specification": None,
                    "custom_field_type": CustomFieldTypes.text,
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Date the problem started",
                    "magic_string": None,
                    "multiple_values": False,
                    "name": "ZTC Subject",
                    "options": None,
                },
                {
                    "attribute_uuid": UUID(
                        "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7e"
                    ),
                    "custom_field_specification": {
                        "name": "Custom Object 123",
                        "type": CustomFieldRelationshipTypes.custom_object,
                        "use_on_map": True,
                        "uuid": UUID("ec5cfb98-2ed6-45cc-a382-d47bfbba3e7d"),
                    },
                    "custom_field_type": CustomFieldTypes.relationship,
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Object Relation",
                    "magic_string": "ztc_object_relation",
                    "multiple_values": False,
                    "name": "ZTC Object Relation",
                    "options": None,
                },
            ]
        }

        relationships = objecttype.relationship_definition.relationships
        assert relationships[0].custom_object_type_uuid
        assert len(relationships) == 2

        assert relationships[0].description == "Description"
        assert relationships[0].external_description == "External description"
        assert relationships[0].is_required is True

    def test_got_objecttype_by_independent_uuid(self):
        objecttype_uuid = uuid4()
        version_independent_uuid = uuid4()
        dbparams = self.load_row_into_fetchone(
            objecttype_uuid, version_independent_uuid
        )

        objecttype = (
            self.qry.get_custom_object_type_by_version_independent_uuid(
                uuid=str(objecttype_uuid)
            )
        )

        assert objecttype.uuid == objecttype_uuid

        assert objecttype.uuid == dbparams["v_uuid"]
        assert objecttype.name == dbparams["v_name"]
        assert objecttype.title == dbparams["v_title"]
        assert objecttype.status == dbparams["v_status"]

    @pytest.mark.skip(reason="Stress run for ocassionally running")
    def test_got_objecttype_by_uuid_a_100_times(self):
        objecttype_uuid = uuid4()
        version_independent_uuid = uuid4()
        dbparams = self.load_row_into_fetchone(
            objecttype_uuid, version_independent_uuid
        )
        t1_start = process_time()
        for _ in range(100):
            objecttype = (
                self.qry.get_custom_object_type_by_version_independent_uuid(
                    uuid=str(objecttype_uuid)
                )
            )

            assert objecttype.uuid == dbparams["v_uuid"]
            assert objecttype.name == dbparams["v_name"]
            assert objecttype.title == dbparams["v_title"]
            assert objecttype.status == dbparams["v_status"]

        t1_stop = process_time()
        elapsed = t1_stop - t1_start

        assert elapsed < 1, f"Time elapsed {round(elapsed, 2)} > 1 seconds"

    def test_object_has_a_version_and_version_independent_uuid(self):
        objecttype_uuid = uuid4()
        version_independent_uuid = uuid4()
        dbparams = self.load_row_into_fetchone(
            objecttype_uuid, version_independent_uuid
        )

        objecttype = self.qry.get_custom_object_type_by_uuid(
            uuid=str(objecttype_uuid)
        )

        assert objecttype.version_independent_uuid == version_independent_uuid
        assert objecttype.is_active_version is False
        assert objecttype.latest_version.uuid == dbparams["lv_uuid"]

        assert (
            objecttype.latest_version.authorization_definition.dict()
            == dbparams["authorization_definition"]
        )
        assert objecttype.latest_version.custom_field_definition.dict() == {
            "custom_fields": [
                {
                    "attribute_uuid": UUID(
                        "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f"
                    ),
                    "custom_field_specification": None,
                    "custom_field_type": CustomFieldTypes.text,
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Date the problem was last version",
                    "magic_string": None,
                    "multiple_values": False,
                    "name": "ZTC Subject",
                    "options": None,
                },
                {
                    "attribute_uuid": UUID(
                        "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7e"
                    ),
                    "custom_field_specification": {
                        "name": "Custom Object 123",
                        "type": CustomFieldRelationshipTypes.custom_object,
                        "use_on_map": True,
                        "uuid": UUID("ec5cfb98-2ed6-45cc-a382-d47bfbba3e7d"),
                    },
                    "custom_field_type": CustomFieldTypes.relationship,
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Object Relation",
                    "magic_string": "ztc_object_relation",
                    "multiple_values": False,
                    "name": "ZTC Object Relation",
                    "options": None,
                },
            ]
        }

        relationships = (
            objecttype.latest_version.relationship_definition.relationships
        )
        assert relationships[1].case_type_uuid
        assert len(relationships) == 2

    def test_object_has_an_audit_log(self):
        objecttype_uuid = uuid4()
        version_independent_uuid = uuid4()
        self.load_row_into_fetchone(objecttype_uuid, version_independent_uuid)

        objecttype = self.qry.get_custom_object_type_by_uuid(
            uuid=str(objecttype_uuid)
        )

        assert objecttype.audit_log.updated_components == [
            "attributes",
            "authorizations",
        ]

        assert (
            objecttype.audit_log.description
            == "Revoked access from Frits and added name field"
        )

    def test_object_has_a_reference(self):
        objecttype_uuid = uuid4()
        version_independent_uuid = uuid4()
        self.load_row_into_fetchone(objecttype_uuid, version_independent_uuid)

        objecttype = self.qry.get_custom_object_type_by_uuid(
            uuid=str(objecttype_uuid)
        )

        assert objecttype.external_reference == "External Reference"

    @patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_version_independent_uuid"
    )
    def test_update_with_not_found_objecttype_uuid(
        self, find_by_version_independent_uuid
    ):
        find_by_version_independent_uuid.return_value = None

        with pytest.raises(exceptions.NotFound):
            self.qry.get_custom_object_type_by_version_independent_uuid(
                uuid=str(uuid4())
            )

    def test_objecttype_by_independent_uuid_when_use_is_invalid(self):
        with pytest.raises(exceptions.Conflict) as excinfo:
            self.qry.get_custom_object_type_by_version_independent_uuid(
                uuid=str(uuid4()), use="update"
            )
        assert excinfo.value.args == (
            "Invalid parameter 'use=update' to retrieve custom_object_type",
            "case_management/custom_object/object_type/invalid_use",
        )


class TestAs_An_Administrator_I_Want_To_Update_An_ObjectType(
    ObjectTypeTestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_update_name_status_and_title(self):
        objecttype_uuid = str(uuid4())
        case_type_uuid = uuid4()

        self.mock_fetchall(
            objecttype_uuid=objecttype_uuid,
            relationship_case_type_uuid=case_type_uuid,
        )

        self.load_row_into_fetchone(
            objecttype_uuid,
            relationship_case_type_uuid=case_type_uuid,
            custom_field_type="option",
        )

        self.get_cmd().update_custom_object_type(
            existing_uuid=uuid4(),
            uuid=objecttype_uuid,
            name="parking_permit_updated",
            catalog_folder_id=0,
            title="[[ object.name2 ]]",
            external_reference="External Reference",
            status="active",
            custom_field_definition={
                "custom_fields": [
                    {
                        "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                        "label": "Date the problem started",
                    }
                ]
            },
            relationship_definition={
                "relationships": [{"case_type_uuid": case_type_uuid}]
            },
            audit_log={
                "updated_components": ["attributes", "authorizations"],
                "description": "blabla",
            },
        )

        self.assert_has_event_name("CustomObjectTypeUpdated")

        # Expect a select and a update
        call_list = self.session.execute.call_args_list

        assert call_list[10][0][0].__class__.__name__ == "Insert"
        assert call_list[11][0][0].__class__.__name__ == "Select"
        assert call_list[12][0][0].__class__.__name__ == "Update"
        assert call_list[13][0][0].__class__.__name__ == "Update"

        # Compile insert query
        query = call_list[10][0][0].compile()

        assert isinstance(query.params["custom_field_definition"], dict)
        assert (
            query.params["custom_field_definition"]["custom_fields"][0][
                "label"
            ]
            == "Date the problem started"
        )
        assert query.params["name"] == "parking_permit_updated"
        assert query.params["title"] == "[[ object.name2 ]]"
        assert query.params["subtitle"] == "[[ object.subtitle ]]"
        assert query.params["status"] == "active"
        assert query.params["uuid"] == objecttype_uuid
        assert query.params["audit_log"] == {
            "description": "blabla",
            "updated_components": ["attributes", "authorizations"],
        }
        assert query.params["external_reference"] == "External Reference"
        assert {
            "custom_fields": [
                {
                    "attribute_uuid": "ec5cfb98-2ed6-45cc-a382-d47bfbba3e7f",
                    "custom_field_type": "option",
                    "description": None,
                    "external_description": None,
                    "is_hidden_field": False,
                    "is_required": False,
                    "label": "Date the problem started",
                    "magic_string": "ztc_subject",
                    "multiple_values": True,
                    "options": ["step1"],
                    "name": "ZTC Subject",
                    "custom_field_specification": None,
                }
            ]
        } == query.params["custom_field_definition"]

        assert query.params["relationship_definition"]["relationships"][0][
            "case_type_uuid"
        ]

        query = call_list[12][0][0].compile()
        assert (
            query.params["authorization_definition"]["authorizations"][0][
                "authorization"
            ]
            == "admin"
        )
        assert query.params["catalog_folder_id"] is None
        assert query.params["id_1"]

        query = call_list[13][0][0].compile()
        assert query.params["custom_object_type_id"]

    def test_updating_of_empty_authorizations(self):
        """Fixes bug that we could not update an authorization when no
        authorizations were set previously"""

        objecttype_uuid = str(uuid4())

        dbparams = self.load_row_into_fetchone(
            objecttype_uuid, skip_authorization_definition=True
        )

        auth_definition = {
            "authorizations": [
                {
                    "authorization": "admin",
                    "role": {"name": "Beheerder", "uuid": str(uuid4())},
                    "department": {"name": "Mintlab", "uuid": str(uuid4())},
                },
                {
                    "authorization": "readwrite",
                    "role": {"name": "Behandelaar", "uuid": str(uuid4())},
                    "department": {"name": "Mintlab", "uuid": str(uuid4())},
                },
            ]
        }

        self.mock_fetchall(
            dbparams=dbparams,
            objecttype_uuid=objecttype_uuid,
            use_authorizations=auth_definition["authorizations"],
        )

        self.cmd.update_custom_object_type(
            existing_uuid=uuid4(),
            uuid=objecttype_uuid,
            name="parking_permit_updated",
            catalog_folder_id=1,
            title="[[ object.name2 ]]",
            status="active",
            authorization_definition=auth_definition,
            audit_log={
                "updated_components": ["authorizations"],
                "description": "Added authorizations",
            },
        )

    @patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_update_with_not_found_objecttype_uuid(self, find_by_uuid):
        find_by_uuid.return_value = None
        objecttype_uuid = str(uuid4())

        with pytest.raises(exceptions.NotFound):
            self.cmd.update_custom_object_type(
                existing_uuid=uuid4(),
                uuid=objecttype_uuid,
                name="parking_permit_updated",
                title="[[ object.name2 ]]",
                status="active",
                custom_field_definition={"custom_fields": []},
                external_reference="External Reference",
                audit_log={
                    "updated_components": ["attributes", "authorizations"],
                    "description": "bla",
                },
            )


class TestAs_An_Administrator_I_Want_To_Delete_An_ObjectType(
    ObjectTypeTestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_delete_objecttype_completely(self):
        objecttype_uuid = str(uuid4())

        self.session.execute().fetchone.return_value = None

        with pytest.raises(exceptions.NotFound):
            self.cmd.delete_custom_object_type(uuid=str(uuid4()))
        mock_object_type = mock.MagicMock()
        mock_object_type.configure_mock(
            **self.load_row_into_fetchone(
                objecttype_uuid, version_independent_uuid=str(uuid4())
            )
        )
        self.session.execute().fetchone.side_effect = [
            mock_object_type,
            mock.MagicMock(),
            None,
        ]
        self.session.reset_mock()
        self.cmd.delete_custom_object_type(uuid=objecttype_uuid)

        self.assert_has_event_name("CustomObjectTypeDeleted")

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[1][0][0].__class__.__name__ == "Select"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Update"

        # Compile first delete query
        query = call_list[3][0][0].compile()

        assert query.params["custom_object_type_id_1"]
        assert query.params["date_deleted"]

    def test_delete_objecttype_completely_by_version_independent_uuid(self):
        objecttype_uuid = str(uuid4())

        self.session.execute().fetchone.return_value = None

        with pytest.raises(exceptions.NotFound):
            self.cmd.delete_custom_object_type(
                version_independent_uuid=str(uuid4())
            )

        self.load_row_into_fetchone(objecttype_uuid)
        mock_object_type = mock.MagicMock()
        mock_object_type.configure_mock(
            **self.load_row_into_fetchone(objecttype_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_object_type,
            mock.MagicMock(),
            None,
        ]
        self.session.reset_mock()
        self.cmd.delete_custom_object_type(
            version_independent_uuid=objecttype_uuid
        )

        self.assert_has_event_name("CustomObjectTypeDeleted")

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[1][0][0].__class__.__name__ == "Select"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Update"

        # Compile first delete query
        query = call_list[3][0][0].compile()

        assert query.params["custom_object_type_id_1"]
        assert query.params["date_deleted"]

    def test_delete_objecttype_with_existing_relation(self):
        objecttype_uuid = str(uuid4())

        mock_object_type = mock.MagicMock()
        mock_object_type.configure_mock(
            **self.load_row_into_fetchone(
                objecttype_uuid, version_independent_uuid=str(uuid4())
            )
        )
        self.session.execute().fetchone.side_effect = [
            mock_object_type,
            mock.MagicMock(),
            uuid4(),
        ]
        self.session.reset_mock()
        with pytest.raises(exceptions.Conflict) as excinfo:
            self.cmd.delete_custom_object_type(
                version_independent_uuid=str(uuid4())
            )
        assert excinfo.value.args == (
            "Could not delete custom object type, as it is used in object relation",
            "case_management/custom_object_type/can_not_delete",
        )


class TestIndividualRepositoryFunctions:
    def setup_method(self):
        event_service = MagicMock
        otr = custom_object_type.CustomObjectTypeRepository(
            infrastructure_factory=MagicMock(),
            infrastructure_factory_ro=MagicMock(),
            context="no context",
            event_service=event_service,
            read_only=False,
        )
        type(otr).session = MagicMock()

        self.repo = otr

    def test__save_delete_object_type(self):
        self.repo.session.execute().fetchone.return_value = MagicMock(
            custom_object_type_id=None
        )

        with pytest.raises(exceptions.NotFound):
            self.repo._save_delete_object_type(MagicMock())

    def test___entity_from_row(self):
        row = MagicMock(spec=["uuid", "name"])

        with pytest.raises(pydantic.v1.ValidationError):
            self.repo._entity_from_row(row)

    def test__none_return_values(self):
        with pytest.raises(ValueError):
            assert self.repo._load_from_stmt(None, None, None) is None

        assert self.repo._select_stmt_objecttype() is None
