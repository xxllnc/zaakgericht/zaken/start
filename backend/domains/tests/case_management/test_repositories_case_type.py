# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.case_management.entities import (
    CaseTypeVersionEntity,
    CaseTypeVersionSearchResultEntity,
    Department,
    Role,
)
from zsnl_domains.case_management.repositories.case_type import (
    CaseTypeRepository,
)


class TestCaseTypeRepository:
    def setup_method(self):
        with mock.patch("sqlalchemy.orm.Session"):
            self.mock_infra = mock.MagicMock()
            self.mock_infra_ro = mock.MagicMock()
            self.case_type_repo = CaseTypeRepository(
                infrastructure_factory=self.mock_infra,
                infrastructure_factory_ro=self.mock_infra_ro,
                context="no context",
                event_service=mock.MagicMock(),
                read_only=False,
            )

    def test_get_session(self):
        assert (
            self.case_type_repo.session is self.mock_infra.get_infrastructure()
        )

    def test__get_case_type_node_id(self):
        mock_ses = self.case_type_repo.session

        mock_res = mock.MagicMock()
        mock_res.id = 42

        mock_ses.execute().fetchone.return_value = mock_res

        res = self.case_type_repo._get_case_type_node_id("fake_uuid")
        assert res == 42

        mock_ses.execute().fetchone.return_value = None
        with pytest.raises(NotFound):
            self.case_type_repo._get_case_type_node_id("fake_uuid")

    @mock.patch.object(CaseTypeRepository, "logger")
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository._get_case_type_node_id"
    )
    @mock.patch.object(CaseTypeRepository, "is_simple_case_type")
    def test_find_case_type_version_by_uuid(
        self,
        mock_is_simple_case_type,
        mock_get_node_id,
        mock_case_type_repo_logger,
    ):
        mock_ses = self.case_type_repo.session
        version_uuid = uuid4()
        department_id_1 = uuid4()
        role_id_1 = uuid4()
        department_id_2 = uuid4()
        role_id_2 = uuid4()
        case_type_uuid = uuid4()
        subcase_id_1 = uuid4()
        subcase_id_2 = uuid4()
        mock_get_node_id.return_value = 42

        properties = {
            "allow_external_task_assignment": False,
            "offline_betaling": 1,
            "no_captcha": 1,
            "confidentiality": 1,
            "lock_registration_phase": 1,
            "queue_coworker_changes": 1,
            "check_permissions_for_assignee": 1,
            "api_can_transition": 1,
            "default_directories": ["doc1"],
            "public_confirmation_title": "case type has been registered",
            "public_confirmation_message": "casetype 'casetype 3' has been registered by user admin",
            "pdc_tarief_behandelaar": "100",
            "pdc_tarief_balie": "100",
            "pdc_tarief_telefoon": "110",
            "pdc_tarief_email": "110",
            "pdc_tarief_post": "120",
        }

        query_result = [
            {
                "uuid": version_uuid,
                "id": 1,
                "case_type_uuid": case_type_uuid,
                "version": 3,
                "description": "test description",
                "identification": "test identification",
                "tags": "casetype_3",
                "case_summary": "test summary",
                "webform_amount": "350",
                "initiator_type": "ingaan",
                "initiator_source": "intern",
                "case_public_summary": "test public_summary",
                "name": "casetype 3",
                "active": True,
                "use_requestor_address_for_correspondence": False,
                "allow_reuse_casedata": True,
                "enable_webform": True,
                "enable_online_payment": True,
                "properties": properties,
                "require_email_on_webform": True,
                "require_phonenumber_on_webform": True,
                "require_mobilenumber_on_webform": True,
                "show_contact_info": True,
                "enable_subject_relations_on_form": True,
                "open_case_on_create": True,
                "enable_allocation_on_form": True,
                "disable_pip_for_requestor": True,
                "is_public": True,
                "legal_basis": "test",
                "process_description": "test process decsription",
                "catalog_folder": {"uuid": uuid4(), "name": "test folder"},
                "terms": {
                    "lead_time_legal": {"type": "kalenderdagen", "value": 120},
                    "lead_time_service": {
                        "type": "kalenderdagen",
                        "value": 30,
                    },
                },
                "requestor": {
                    "type_of_requestors": [
                        "person",
                        "employee",
                        "organization",
                        "non-person",
                    ],
                    "use_for_correspondence": False,
                },
                "phases": [
                    {
                        "name": "Registration",
                        "milestone": 1,
                        "phase": "Registering",
                        "allocation": {
                            "department": {
                                "uuid": department_id_1,
                                "name": "development",
                                "table_id": 1,
                            },
                            "role": {
                                "uuid": role_id_1,
                                "name": "admin",
                                "table_id": 2,
                            },
                        },
                        "cases": [
                            {
                                "related_casetype_element": subcase_id_1,
                                "type_of_relation": "deelzaak",
                                "copy_custom_fields_from_parent": True,
                                "open_case_on_create": True,
                                "requestor": {
                                    "related_role": "Advocaat",
                                    "requestor_type": "anders",
                                },
                                "start_on_transition": True,
                                "resolve_before_phase": 2,
                                "show_in_pip": True,
                                "label_in_pip": "test",
                                "allocation": {
                                    "department": {
                                        "uuid": department_id_1,
                                        "name": "Development",
                                    },
                                    "role": {
                                        "uuid": role_id_1,
                                        "name": "Behandelaar",
                                    },
                                },
                            }
                        ],
                        "custom_fields": [
                            {
                                "uuid": uuid4(),
                                "default_value": None,
                                "description": "<p>toelichting intern</p>",
                                "edit_authorizations": [
                                    {
                                        "department": {
                                            "uuid": "2d1c7cb7-9352-4cf1-bd9f-4fe1532a7155",
                                            "name": "Backoffice",
                                        },
                                        "role": {
                                            "uuid": "c20efa09-6486-48da-bae3-f834645c9c51",
                                            "name": "App gebruiker",
                                        },
                                    },
                                    {
                                        "department": {
                                            "uuid": "2d1c7cb7-9352-4cf1-bd9f-4fe1532a7155",
                                            "name": "Backoffice",
                                        },
                                        "role": {
                                            "uuid": "bca1c47e-d799-4f21-96b5-fec5567d2388",
                                            "name": "Afdelingshoofd",
                                        },
                                    },
                                ],
                                "enable_skip_of_queue": True,
                                "external_description": '<p><span style="color: rgb(85, 85, 85);">toelichting extern</span></p>',
                                "field_magic_string": "att1",
                                "field_options": [],
                                "field_type": "text",
                                "is_hidden_field": True,
                                "is_multiple": False,
                                "is_required": True,
                                "name": "att1_modified",
                                "public_name": "att1",
                                "publish_on": [
                                    {"name": "pip"},
                                    {"name": "web"},
                                ],
                                "requestor_can_change_from_pip": True,
                                "sensitive_data": True,
                                "titel_multiple": None,
                                "title": "titel",
                            },
                            {
                                "uuid": uuid4(),
                                "default_value": None,
                                "description": "<p>c</p>",
                                "edit_authorizations": [],
                                "enable_skip_of_queue": True,
                                "external_description": "<p>c</p>",
                                "field_magic_string": "test_kenmerk",
                                "field_options": [],
                                "field_type": "text",
                                "is_hidden_field": True,
                                "is_multiple": True,
                                "is_required": None,
                                "name": "test kenmerk",
                                "public_name": "public name",
                                "publish_on": [
                                    {"name": "pip"},
                                    {"name": "web"},
                                ],
                                "requestor_can_change_from_pip": True,
                                "sensitive_data": True,
                                "titel_multiple": "c",
                                "title": "c",
                            },
                        ],
                    },
                    {
                        "name": "Handle",
                        "milestone": 2,
                        "phase": "Handling",
                        "allocation": {
                            "department": {
                                "uuid": department_id_2,
                                "name": "front office",
                                "table_id": 3,
                            },
                            "role": {
                                "uuid": role_id_2,
                                "name": "admin",
                                "table_id": 4,
                            },
                        },
                        "cases": [
                            {
                                "related_casetype_element": subcase_id_2,
                                "type_of_relation": "deelzaak",
                                "copy_custom_fields_from_parent": False,
                                "open_case_on_create": False,
                                "start_on_transition": False,
                                "resolve_before_phase": 2,
                                "show_in_pip": False,
                                "label_in_pip": "test",
                                "requestor": {
                                    "related_role": "Advocaat",
                                    "requestor_type": "anders",
                                },
                                "allocation": {
                                    "department": {
                                        "uuid": department_id_2,
                                        "name": "Frontdesk",
                                    },
                                    "role": {
                                        "uuid": role_id_2,
                                        "name": "Contactbeheerder",
                                    },
                                },
                            }
                        ],
                    },
                ],
                "results": [],
                "preset_requestor": None,
                "preset_assignee": None,
                "created": datetime(2019, 4, 5),
                "last_modified": datetime(2019, 4, 8),
                "deleted": None,
            }
        ]

        mock_is_simple_case_type.return_value = True
        mock_ses.execute().fetchone.return_value = query_result
        res = self.case_type_repo.find_case_type_version_by_uuid(
            version_uuid=version_uuid
        )
        assert isinstance(res, CaseTypeVersionEntity)
        assert res.uuid == version_uuid
        assert res.name == "casetype 3"
        assert res.active is True
        assert res.case_type_uuid == case_type_uuid
        assert res.description == "test description"
        assert res.identification == "test identification"
        assert res.tags == "casetype_3"
        assert res.case_summary == "test summary"
        assert res.initiator_type == "ingaan"
        assert res.initiator_source == "intern"
        assert res.case_public_summary == "test public_summary"
        assert res.settings.dict() == {
            "allow_reuse_casedata": True,
            "allow_external_task_assignment": False,
            "enable_webform": True,
            "enable_online_payment": True,
            "enable_manual_payment": True,
            "require_email_on_webform": True,
            "require_phonenumber_on_webform": True,
            "require_mobilenumber_on_webform": True,
            "disable_captcha_for_predefined_requestor": True,
            "show_confidentiality": True,
            "show_contact_info": True,
            "enable_subject_relations_on_form": True,
            "open_case_on_create": True,
            "enable_allocation_on_form": True,
            "disable_pip_for_requestor": True,
            "lock_registration_phase": True,
            "enable_queue_for_changes_from_other_subjects": True,
            "check_acl_on_allocation": True,
            "api_can_transition": True,
            "is_public": True,
            "list_of_default_folders": [{"name": "doc1"}],
            "text_confirmation_message_title": "case type has been registered",
            "text_public_confirmation_message": "casetype 'casetype 3' has been registered by user admin",
            "payment": {
                "assignee": {"amount": "100"},
                "frontdesk": {"amount": "100"},
                "phone": {"amount": "110"},
                "mail": {"amount": "110"},
                "webform": {"amount": "350"},
                "post": {"amount": "120"},
            },
        }
        assert res.phases[0].name == "Registration"
        assert isinstance(res.phases[0].allocation.department, Department)
        assert (
            res.phases[0].allocation.department.dict().items()
            > {"uuid": department_id_1, "name": "development"}.items()
        )
        assert isinstance(res.phases[0].allocation.role, Role)
        assert (
            res.phases[0].allocation.role.dict().items()
            > {"uuid": role_id_1, "name": "admin"}.items()
        )

        assert (
            res.phases[0].cases[0].dict().items()
            > {
                "related_casetype_element": subcase_id_1,
                "type_of_relation": "deelzaak",
                "copy_custom_fields_from_parent": True,
                "open_case_on_create": True,
                "start_on_transition": True,
                "resolve_before_phase": 2,
                "show_in_pip": True,
                "label_in_pip": "test",
            }.items()
        )

        assert res.phases[0].cases[0].requestor.dict() == {
            "requestor_type": "anders",
            "related_role": "Advocaat",
        }
        assert (
            res.phases[0].custom_fields[0].dict().items()
            > {
                "default_value": None,
                "description": "<p>toelichting intern</p>",
                "enable_skip_of_queue": True,
                "external_description": '<p><span style="color: rgb(85, 85, 85);">toelichting extern</span></p>',
                "field_magic_string": "att1",
                "field_options": [],
                "field_type": "text",
                "is_hidden_field": True,
                "is_multiple": False,
                "is_required": True,
                "name": "att1_modified",
                "public_name": "att1",
                "requestor_can_change_from_pip": True,
                "sensitive_data": True,
                "title_multiple": None,
                "title": "titel",
            }.items()
        )

        assert res.requestor == {
            "type_of_requestors": [
                "person",
                "employee",
                "organization",
                "non-person",
            ],
            "use_for_correspondence": False,
        }

        with pytest.raises(NotFound) as excinfo:
            mock_ses.execute().fetchone.return_value = None
            self.case_type_repo.find_case_type_version_by_uuid(
                version_uuid=version_uuid
            )

        assert excinfo.value.args == (
            f"Case type version with version_uuid: '{version_uuid}' not found.",
            "case_type/version_not_found",
        )

        with pytest.raises(NotFound) as excinfo:
            # When case_type_version_by_id_query gives NotFound Error
            mock_ses.execute().fetchone.return_value = None
            self.case_type_repo.find_case_type_version_by_uuid(
                version_uuid=version_uuid
            )

        assert excinfo.value.args == (
            f"Case type version with version_uuid: '{version_uuid}' not found.",
            "case_type/version_not_found",
        )

    def test_is_simple_case_type(self):
        mock_ses = self.case_type_repo.session
        uuid = str(uuid4())

        mock_ses.execute().fetchone.return_value = None
        res = self.case_type_repo.is_simple_case_type(uuid=uuid)
        assert res is False

        mock_ses.execute().fetchone.return_value = 1
        res = self.case_type_repo.is_simple_case_type(uuid=uuid)
        assert res is True

    @mock.patch.object(CaseTypeRepository, "find_case_type_version_by_uuid")
    def test_find_active_version_by_uuid(self, mock_find_case_type_version):
        mock_ses = self.case_type_repo.session

        case_type_uuid = uuid4()
        case_type_version_uuid = uuid4()

        mock_ses.execute().fetchone.return_value = namedtuple(
            "QueryResult", "uuid"
        )(uuid=case_type_version_uuid)
        mock_find_case_type_version.return_value = "test"

        res = self.case_type_repo.find_active_version_by_uuid(
            case_type_uuid=case_type_uuid
        )
        assert res == "test"
        self.case_type_repo.find_case_type_version_by_uuid.assert_called_once_with(
            version_uuid=case_type_version_uuid
        )

        with pytest.raises(NotFound) as excinfo:
            mock_ses.execute().fetchone.return_value = None
            self.case_type_repo.find_active_version_by_uuid(
                case_type_uuid=case_type_uuid
            )
        assert excinfo.value.args == (
            f"Case type with uuid '{case_type_uuid}' not found.",
            "case_type/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository._get_case_type_node_id"
    )
    @mock.patch.object(CaseTypeRepository, "is_simple_case_type")
    @mock.patch.object(CaseTypeRepository, "_generate_metadata_for_case_type")
    def test_find_case_types_by_list_uuids(
        self,
        mock_generate_metadata_for_case_type,
        mock_is_simple_case_type,
        mock_get_node_id,
    ):
        mock_ses = self.case_type_repo.session
        version_uuid = uuid4()
        department_id_1 = uuid4()
        role_id_1 = uuid4()
        department_id_2 = uuid4()
        role_id_2 = uuid4()
        case_type_uuid = uuid4()
        subcase_id_1 = uuid4()
        subcase_id_2 = uuid4()
        mock_get_node_id.return_value = 42

        properties = {
            "allow_external_task_assignment": False,
            "offline_betaling": 1,
            "no_captcha": 1,
            "confidentiality": 1,
            "lock_registration_phase": 1,
            "queue_coworker_changes": 1,
            "check_permissions_for_assignee": 1,
            "api_can_transition": 1,
            "default_directories": ["doc1"],
            "public_confirmation_title": "case type has been registered",
            "public_confirmation_message": "casetype 'casetype 3' has been registered by user admin",
            "pdc_tarief_behandelaar": "100",
            "pdc_tarief_balie": "100",
            "pdc_tarief_telefoon": "110",
            "pdc_tarief_email": "110",
            "pdc_tarief_post": "120",
        }

        query_result = [mock.Mock()]
        query_result[0].configure_mock(
            uuid=version_uuid,
            id=1,
            case_type_uuid=case_type_uuid,
            version=3,
            description="description",
            identification="identification",
            tags="tags",
            case_summary="case_summary",
            properties=properties,
            name="casetype 3",
            created=datetime(2019, 4, 5),
            last_modified=datetime(2019, 4, 8),
            deleted=None,
            initiator_type="ingaan",
            initiator_source="intern",
            payment={
                "assignee": {"amount": "100"},
                "frontdesk": {"amount": "100"},
                "phone": {"amount": "110"},
                "mail": {"amount": "110"},
                "webform": {"amount": "350"},
                "post": {"amount": "120"},
            },
            webform_amount=350,
            case_public_summary="test public_summary",
            terms={
                "lead_time_legal": {"type": "kalenderdagen", "value": 120},
                "lead_time_service": {
                    "type": "kalenderdagen",
                    "value": 30,
                },
            },
            active=True,
            legal_basis="test",
            process_description="process_description",
            phases=[
                {
                    "name": "Registration",
                    "milestone": 1,
                    "phase": "Registering",
                    "allocation": {
                        "department": {
                            "uuid": department_id_1,
                            "name": "development",
                            "table_id": 1,
                        },
                        "role": {
                            "uuid": role_id_1,
                            "name": "admin",
                            "table_id": 2,
                        },
                    },
                    "cases": [
                        {
                            "related_casetype_element": subcase_id_1,
                            "type_of_relation": "deelzaak",
                            "copy_custom_fields_from_parent": True,
                            "open_case_on_create": True,
                            "requestor": {
                                "related_role": "Advocaat",
                                "requestor_type": "anders",
                            },
                            "start_on_transition": True,
                            "resolve_before_phase": 2,
                            "show_in_pip": True,
                            "label_in_pip": "test",
                            "allocation": {
                                "department": {
                                    "uuid": department_id_1,
                                    "name": "Development",
                                },
                                "role": {
                                    "uuid": role_id_1,
                                    "name": "Behandelaar",
                                },
                            },
                        }
                    ],
                    "custom_fields": [
                        {
                            "uuid": uuid4(),
                            "default_value": None,
                            "description": "<p>toelichting intern</p>",
                            "edit_authorizations": [
                                {
                                    "department": {
                                        "uuid": "2d1c7cb7-9352-4cf1-bd9f-4fe1532a7155",
                                        "name": "Backoffice",
                                    },
                                    "role": {
                                        "uuid": "c20efa09-6486-48da-bae3-f834645c9c51",
                                        "name": "App gebruiker",
                                    },
                                },
                                {
                                    "department": {
                                        "uuid": "2d1c7cb7-9352-4cf1-bd9f-4fe1532a7155",
                                        "name": "Backoffice",
                                    },
                                    "role": {
                                        "uuid": "bca1c47e-d799-4f21-96b5-fec5567d2388",
                                        "name": "Afdelingshoofd",
                                    },
                                },
                            ],
                            "enable_skip_of_queue": True,
                            "external_description": '<p><span style="color: rgb(85, 85, 85);">toelichting extern</span></p>',
                            "field_magic_string": "att1",
                            "field_options": [],
                            "field_type": "text",
                            "is_hidden_field": True,
                            "is_multiple": False,
                            "is_required": True,
                            "name": "att1_modified",
                            "public_name": "att1",
                            "publish_on": [
                                {"name": "pip"},
                                {"name": "web"},
                            ],
                            "requestor_can_change_from_pip": True,
                            "sensitive_data": True,
                            "titel_multiple": None,
                            "title": "titel",
                        },
                        {
                            "uuid": uuid4(),
                            "default_value": None,
                            "description": "<p>c</p>",
                            "edit_authorizations": [],
                            "enable_skip_of_queue": True,
                            "external_description": "<p>c</p>",
                            "field_magic_string": "test_kenmerk",
                            "field_options": [],
                            "field_type": "text",
                            "is_hidden_field": True,
                            "is_multiple": True,
                            "is_required": None,
                            "name": "test kenmerk",
                            "public_name": "public name",
                            "publish_on": [
                                {"name": "pip"},
                                {"name": "web"},
                            ],
                            "requestor_can_change_from_pip": True,
                            "sensitive_data": True,
                            "titel_multiple": "c",
                            "title": "c",
                        },
                    ],
                },
                {
                    "name": "Handle",
                    "milestone": 2,
                    "phase": "Handling",
                    "allocation": {
                        "department": {
                            "uuid": department_id_2,
                            "name": "front office",
                            "table_id": 3,
                        },
                        "role": {
                            "uuid": role_id_2,
                            "name": "admin",
                            "table_id": 4,
                        },
                    },
                    "cases": [
                        {
                            "related_casetype_element": subcase_id_2,
                            "type_of_relation": "deelzaak",
                            "copy_custom_fields_from_parent": False,
                            "open_case_on_create": False,
                            "start_on_transition": False,
                            "resolve_before_phase": 2,
                            "show_in_pip": False,
                            "label_in_pip": "test",
                            "requestor": {
                                "related_role": "Advocaat",
                                "requestor_type": "anders",
                            },
                            "allocation": {
                                "department": {
                                    "uuid": department_id_2,
                                    "name": "Frontdesk",
                                },
                                "role": {
                                    "uuid": role_id_2,
                                    "name": "Contactbeheerder",
                                },
                            },
                        }
                    ],
                },
            ],
        )
        mock_is_simple_case_type.return_value = True

        mock_generate_metadata_for_case_type.return_value = {
            "legal_basis": "legal_basis",
            "process_description": "process_description",
        }
        mock_ses.execute().fetchall.return_value = query_result
        res = self.case_type_repo.find_case_types_by_list_uuids(
            [str(version_uuid)]
        )
        assert isinstance(res[0], CaseTypeVersionSearchResultEntity)
