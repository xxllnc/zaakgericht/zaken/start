# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.cqrs import UserInfo, test
from minty.exceptions import NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class TestAs_An_admin_or_zaakbeheerder_I_want_to_run_tmlo_archive_export(
    test.TestBase
):
    def setup_method(self):
        mock_s3_infra = mock.MagicMock()
        mock_s3_infra.upload.return_value = {
            "mime_type": "application/json",
            "md5": "123456",
            "size": "32",
            "storage_location": "minio",
        }
        self.load_query_instance(case_management)

        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_run_tmlo_archive_export(self):
        case_uuid = str(uuid4())
        mock_config = mock.MagicMock()
        mock_config.configure_mock(
            export_id="NL-9999-0000",
            is_multi_tenant=1,
            notes=None,
            multi_tenant_host="sprint.zaaksysteem.nl",
            export_target="TEST",
            export_type="TMLO",
            uuid=None,
        )
        case_uuid = str(uuid4())
        file_custom_filed_uuid = uuid4()
        thumbnail_uuid = uuid4()

        mock_documents = [mock.MagicMock()]
        mock_documents = [
            {
                "id": 1,
                "uuid": uuid4(),
                "extension": ".odt",
                "filename": "testfile",
                "mimetype": "application/pdf",
                "size": 1,
                "store_uuid": uuid4(),
                "md5": "md5",
                "date_created": "2024-11-21T12:40:33.166416+00:00",
                "confidentiality": "confidential",
                "storage_location": ["minio"],
                "case_id": 3,
            }
        ]
        mock_cases = [mock.MagicMock()]
        mock_cases[0].configure_mock(
            id=3,
            uuid=case_uuid,
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "basic_name",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "ztc external",
                    "magic_string": "ztc_extern_kenmerk",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_4",
                    "magic_string": "basic_date",
                    "type": "date",
                    "value": ["2024-11-21T12:40:33.166416+00:00"],
                    "is_multiple": False,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": file_custom_filed_uuid,
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": thumbnail_uuid,
                        },
                    ],
                }
            ],
            destruction_date=None,
            confidentiality="public",
            result="afgehandeld",
            result_uuid=uuid4(),
            result_description="Afgehandeld!",
            archival_state="vernietigen",
            active_selection_list="12",
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            assignee_obj=None,
            case_type_identification="10",
            case_type_version={
                "uuid": uuid4(),
                "name": "My First Case Type",
            },
            authorizations=["read"],
            type_of_archiving="Bewaren (B)",
            result_explanation="result explanation",
            archive_settings={
                "archive_type": "tmlo",
                "name": [
                    "this [[basic_name]]",
                    "This is plain text",
                ],
                "classification_code": ["[[basic_name]]"],
                "classification_description": ["[[basic_name]]"],
                "classification_source_tmlo": ["[[basic_name]]"],
                "classification_date": ["[[basic_date]]"],
                "description_tmlo": ["[[basic_date]]"],
                "location": ["[[basic_date]]"],
                "coverage_in_time_tmlo": None,
                "coverage_in_geo_tmlo": None,
                "language_tmlo": ["[[basic_date]]"],
                "user_rights": ["[[basic_date]]"],
                "user_rights_description": ["[[basic_date]]"],
                "user_rights_date_period": ["[[basic_date]]"],
                "confidentiality": ["[[basic_date]]"],
                "confidentiality_description": ["[[basic_date]]"],
                "confidentiality_date_period": ["[[basic_date]]"],
                "form_genre": ["[[basic_date]]"],
                "form_publication": ["[[basic_date]]"],
                "structure": ["[[basic_date]]"],
                "generic_metadata_tmlo": [
                    "this [[basic_name]]",
                    "This is plain text",
                ],
            },
            zaaktype_node_id=10,
            documents=mock_documents,
        )

        mock_event_logs = [
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
        ]
        mock_event_logs[0].configure_mock(
            event_type="case/document/publish",
            created_by="admin",
            onderwerp='Document "test_doc" gepubliceerd op de PIP',
        )
        mock_event_logs[1].configure_mock(
            event_type="case/document/copy_case_into",
            created_by="admin",
            onderwerp="Document 'test_doc.odt' (versie 1) gekopieerd naar zaak '4' (Bewaartermijn niet geactiveerd), als document 'test_doc.odt'",
        )
        mock_event_logs[2].configure_mock(
            event_type="case/update/completion_date",
            created_by="admin",
            onderwerp="Afhandeldatum voor zaak: 3 gewijzigd naar: 06-06-2024",
        )
        mock_event_logs[3].configure_mock(
            event_type="case/document/replace",
            created_by="admin",
            onderwerp="Document 'test_doc.odt' (versie 1) vervangen met document 'test_doc.odt' (versie 2)",
        )
        uuid1 = uuid4()
        uuid3 = uuid4()

        related_case_row_1 = mock.Mock()
        related_case_row_1.configure_mock(
            relation_type="related_case",
            case_a_uuid=uuid1,
            case_b_uuid=uuid3,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )
        related_case_row_2 = mock.Mock()
        related_case_row_2.configure_mock(
            relation_type="related_case",
            case_a_uuid=uuid1,
            case_b_uuid=uuid3,
            case_a_onderwerp="case 2",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )
        self.session.execute().fetchall.side_effect = [
            mock_cases,
            mock_event_logs,
            [related_case_row_1],
            [related_case_row_2],
        ]
        mock_realtion_case = mock.MagicMock()
        mock_realtion_case.configure_mock(uuid=case_uuid, id=3)
        self.session.execute().fetchone.side_effect = [
            mock_config,
            mock_realtion_case,
        ]
        self.session.execute.reset_mock()

        self.qry.run_archive_export(
            case_uuid=case_uuid, export_file_uuid=uuid4()
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

        select_statement = call_list[0][0][0]

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT interface.interface_config \n"
            "FROM interface \n"
            "WHERE interface.module = %(module_1)s AND interface.date_deleted IS NULL AND interface.active"
        )

        select_statement = call_list[1][0][0]

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.registratiedatum AS registration_date, view_case_v2.type_of_archiving, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.confidentiality, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.onderwerp AS summary, view_case_v2.onderwerp_extern AS public_summary, view_case_v2.case_type_version, view_case_v2.vernietigingsdatum AS destruction_date, view_case_v2.assignee_obj, %(param_1)s AS user_is_admin, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, json_build_object(%(json_build_object_1)s, zaaktype_archive_settings.archive_type, %(json_build_object_2)s, zaaktype_archive_settings.id, %(json_build_object_3)s, zaaktype_archive_settings.uuid, %(json_build_object_4)s, zaaktype_archive_settings.casetype_node_id, %(json_build_object_5)s, zaaktype_archive_settings.name, %(json_build_object_6)s, zaaktype_archive_settings.classification_code, %(json_build_object_7)s, zaaktype_archive_settings.classification_description, %(json_build_object_8)s, zaaktype_archive_settings.classification_source_tmlo, %(json_build_object_9)s, zaaktype_archive_settings.classification_date, %(json_build_object_10)s, zaaktype_archive_settings.description_tmlo, %(json_build_object_11)s, zaaktype_archive_settings.location, %(json_build_object_12)s, zaaktype_archive_settings.coverage_in_time_tmlo, %(json_build_object_13)s, zaaktype_archive_settings.coverage_in_geo_tmlo, %(json_build_object_14)s, zaaktype_archive_settings.language_tmlo, %(json_build_object_15)s, zaaktype_archive_settings.user_rights, %(json_build_object_16)s, zaaktype_archive_settings.user_rights_description, %(json_build_object_17)s, zaaktype_archive_settings.user_rights_date_period, %(json_build_object_18)s, zaaktype_archive_settings.confidentiality, %(json_build_object_19)s, zaaktype_archive_settings.confidentiality_description, %(json_build_object_20)s, zaaktype_archive_settings.confidentiality_date_period, %(json_build_object_21)s, zaaktype_archive_settings.form_genre, %(json_build_object_22)s, zaaktype_archive_settings.form_publication, %(json_build_object_23)s, zaaktype_archive_settings.structure, %(json_build_object_24)s, zaaktype_archive_settings.generic_metadata_tmlo) AS archive_settings, zaaktype_node.code AS case_type_identification, array((SELECT json_build_object(%(json_build_object_26)s, file.id, %(json_build_object_27)s, file.uuid, %(json_build_object_28)s, file.extension, %(json_build_object_29)s, file.name, %(json_build_object_30)s, filestore.mimetype, %(json_build_object_31)s, filestore.size, %(json_build_object_32)s, filestore.uuid, %(json_build_object_33)s, filestore.storage_location, %(json_build_object_34)s, filestore.md5, %(json_build_object_35)s, file.date_created, %(json_build_object_36)s, file_metadata.trust_level, %(json_build_object_37)s, file.case_id) AS json_build_object_25 \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id \n"
            "WHERE file.case_id = view_case_v2.id AND file.date_deleted IS NULL AND file.destroyed IS false)) AS documents \n"
            "FROM view_case_v2 LEFT OUTER JOIN zaaktype_archive_settings ON view_case_v2.zaaktype_node_id = zaaktype_archive_settings.casetype_node_id LEFT OUTER JOIN zaaktype_node ON view_case_v2.zaaktype_node_id = zaaktype_node.id \n"
            "WHERE view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_1)s AND (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND view_case_v2.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.casetype_id = view_case_v2.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) AND view_case_v2.uuid = %(uuid_2)s::UUID"
        )

    def test_run_tmlo_archive_export_missing_mapping(self):
        case_uuid = str(uuid4())
        mock_config = mock.MagicMock()
        mock_config.configure_mock(
            export_id="NL-9999-0000",
            is_multi_tenant=1,
            notes=None,
            multi_tenant_host="sprint.zaaksysteem.nl",
            export_target="TEST",
            export_type="TMLO",
            uuid=None,
        )
        mock_documents = [mock.MagicMock()]
        mock_documents = [
            {
                "id": 1,
                "uuid": uuid4(),
                "extension": ".odt",
                "filename": "testfile",
                "mimetype": "application/pdf",
                "size": 1,
                "store_uuid": uuid4(),
                "md5": "md5",
                "date_created": "2019-11-21T12:40:33.166416+00:00",
                "confidentiality": "confidential",
                "storage_location": ["minio"],
                "case_id": 3,
            }
        ]
        case_uuid = str(uuid4())
        file_custom_filed_uuid = uuid4()
        thumbnail_uuid = uuid4()
        mock_cases = [mock.MagicMock()]
        mock_cases[0].configure_mock(
            id=3,
            uuid=case_uuid,
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "basic_name",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "ztc external",
                    "magic_string": "ztc_extern_kenmerk",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_4",
                    "magic_string": "basic_date",
                    "type": "date",
                    "value": ["2024-01-01T12:40:33.166416+00:00"],
                    "is_multiple": False,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": file_custom_filed_uuid,
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": thumbnail_uuid,
                        },
                    ],
                }
            ],
            destruction_date=None,
            confidentiality="public",
            result="afgehandeld",
            result_uuid=uuid4(),
            result_description="Afgehandeld!",
            archival_state="vernietigen",
            active_selection_list="12",
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            assignee_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Assign Ee",
            },
            case_type_identification="10",
            case_type_version={
                "uuid": uuid4(),
                "name": "My First Case Type",
            },
            authorizations=["read"],
            type_of_archiving="Bewaren (B)",
            result_explanation="result explanation",
            archive_settings={
                "archive_type": "tmlo",
                "name": ["[[basic_date]]"],
                "classification_code": None,
                "classification_description": ["[[basic_date]]"],
                "classification_source_tmlo": ["[[basic_date]]"],
                "classification_date": ["[[basic_date]]"],
                "description_tmlo": ["[[basic_date]]"],
                "location": ["[[basic_date]]"],
                "coverage_in_time_tmlo": ["[[basic_date]]"],
                "coverage_in_geo_tmlo": ["[[basic_date]]"],
                "language_tmlo": ["[[basic_date]]"],
                "user_rights": ["[[basic_date]]"],
                "user_rights_description": ["[[basic_date]]"],
                "user_rights_date_period": ["[[basic_date]]"],
                "confidentiality": ["[[basic_date]]"],
                "confidentiality_description": None,
                "confidentiality_date_period": ["[[basic_date]]"],
                "form_genre": None,
                "form_publication": ["[[basic_date]]"],
                "structure": ["[[basic_date]]"],
                "generic_metadata_tmlo": [
                    "this [[basic_name]]",
                    "This is plain text",
                ],
            },
            zaaktype_node_id=10,
            documents=mock_documents,
        )

        mock_event_logs = [
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
        ]
        mock_event_logs[0].configure_mock(
            event_type="document/create",
            created_by="admin",
            onderwerp="Document 'test_doc' toegevoegd",
        )
        mock_event_logs[1].configure_mock(
            event_type="case/update/result",
            created_by="admin",
            onderwerp="Zaakresultaat ingesteld op aangegaan (Close the case)",
        )
        mock_event_logs[2].configure_mock(
            event_type="case/view",
            created_by="admin",
            onderwerp="Zaak opgevraagd.",
        )
        uuid1 = uuid4()
        uuid3 = uuid4()

        related_case_row_1 = mock.Mock()
        related_case_row_1.configure_mock(
            relation_type="related_case",
            case_a_uuid=uuid1,
            case_b_uuid=uuid3,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )
        related_case_row_2 = mock.Mock()
        related_case_row_2.configure_mock(
            relation_type="related_case",
            case_a_uuid=uuid1,
            case_b_uuid=uuid3,
            case_a_onderwerp="case 2",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )
        self.session.execute().fetchall.side_effect = [
            mock_cases,
            mock_event_logs,
            [related_case_row_1],
            [related_case_row_2],
        ]
        mock_realtion_case = mock.MagicMock()
        mock_realtion_case.configure_mock(uuid=case_uuid, id=3)
        self.session.execute().fetchone.side_effect = [
            mock_config,
            mock_realtion_case,
        ]
        self.session.execute.reset_mock()

        self.qry.run_archive_export(
            case_uuid=case_uuid, export_file_uuid=uuid4()
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

        select_statement = call_list[0][0][0]

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT interface.interface_config \n"
            "FROM interface \n"
            "WHERE interface.module = %(module_1)s AND interface.date_deleted IS NULL AND interface.active"
        )

        select_statement = call_list[1][0][0]

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.registratiedatum AS registration_date, view_case_v2.type_of_archiving, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.confidentiality, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.onderwerp AS summary, view_case_v2.onderwerp_extern AS public_summary, view_case_v2.case_type_version, view_case_v2.vernietigingsdatum AS destruction_date, view_case_v2.assignee_obj, %(param_1)s AS user_is_admin, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, json_build_object(%(json_build_object_1)s, zaaktype_archive_settings.archive_type, %(json_build_object_2)s, zaaktype_archive_settings.id, %(json_build_object_3)s, zaaktype_archive_settings.uuid, %(json_build_object_4)s, zaaktype_archive_settings.casetype_node_id, %(json_build_object_5)s, zaaktype_archive_settings.name, %(json_build_object_6)s, zaaktype_archive_settings.classification_code, %(json_build_object_7)s, zaaktype_archive_settings.classification_description, %(json_build_object_8)s, zaaktype_archive_settings.classification_source_tmlo, %(json_build_object_9)s, zaaktype_archive_settings.classification_date, %(json_build_object_10)s, zaaktype_archive_settings.description_tmlo, %(json_build_object_11)s, zaaktype_archive_settings.location, %(json_build_object_12)s, zaaktype_archive_settings.coverage_in_time_tmlo, %(json_build_object_13)s, zaaktype_archive_settings.coverage_in_geo_tmlo, %(json_build_object_14)s, zaaktype_archive_settings.language_tmlo, %(json_build_object_15)s, zaaktype_archive_settings.user_rights, %(json_build_object_16)s, zaaktype_archive_settings.user_rights_description, %(json_build_object_17)s, zaaktype_archive_settings.user_rights_date_period, %(json_build_object_18)s, zaaktype_archive_settings.confidentiality, %(json_build_object_19)s, zaaktype_archive_settings.confidentiality_description, %(json_build_object_20)s, zaaktype_archive_settings.confidentiality_date_period, %(json_build_object_21)s, zaaktype_archive_settings.form_genre, %(json_build_object_22)s, zaaktype_archive_settings.form_publication, %(json_build_object_23)s, zaaktype_archive_settings.structure, %(json_build_object_24)s, zaaktype_archive_settings.generic_metadata_tmlo) AS archive_settings, zaaktype_node.code AS case_type_identification, array((SELECT json_build_object(%(json_build_object_26)s, file.id, %(json_build_object_27)s, file.uuid, %(json_build_object_28)s, file.extension, %(json_build_object_29)s, file.name, %(json_build_object_30)s, filestore.mimetype, %(json_build_object_31)s, filestore.size, %(json_build_object_32)s, filestore.uuid, %(json_build_object_33)s, filestore.storage_location, %(json_build_object_34)s, filestore.md5, %(json_build_object_35)s, file.date_created, %(json_build_object_36)s, file_metadata.trust_level, %(json_build_object_37)s, file.case_id) AS json_build_object_25 \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id \n"
            "WHERE file.case_id = view_case_v2.id AND file.date_deleted IS NULL AND file.destroyed IS false)) AS documents \n"
            "FROM view_case_v2 LEFT OUTER JOIN zaaktype_archive_settings ON view_case_v2.zaaktype_node_id = zaaktype_archive_settings.casetype_node_id LEFT OUTER JOIN zaaktype_node ON view_case_v2.zaaktype_node_id = zaaktype_node.id \n"
            "WHERE view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_1)s AND (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND view_case_v2.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.casetype_id = view_case_v2.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) AND view_case_v2.uuid = %(uuid_2)s::UUID"
        )

    def test_run_tmlo_archive_export_missing_mapping_classification(
        self,
    ):
        case_uuid = str(uuid4())
        mock_config = mock.MagicMock()
        mock_config.configure_mock(
            export_id="NL-9999-0000",
            is_multi_tenant=1,
            notes=None,
            multi_tenant_host="sprint.zaaksysteem.nl",
            export_target="TEST",
            export_type="TMLO",
            uuid=None,
        )
        mock_documents = [mock.MagicMock()]
        mock_documents = [
            {
                "id": 1,
                "uuid": uuid4(),
                "extension": ".odt",
                "filename": "testfile",
                "mimetype": "application/pdf",
                "size": 1,
                "store_uuid": uuid4(),
                "md5": "md5",
                "date_created": "2019-11-21T12:40:33.166416+00:00",
                "confidentiality": "confidential",
                "storage_location": ["minio"],
                "case_id": 3,
            }
        ]
        case_uuid = str(uuid4())
        file_custom_filed_uuid = uuid4()
        thumbnail_uuid = uuid4()
        mock_cases = [mock.MagicMock()]
        mock_cases[0].configure_mock(
            id=3,
            uuid=case_uuid,
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "basic_name",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "ztc_extern_kenmerk",
                    "magic_string": "ztc_extern_kenmerk",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_4",
                    "magic_string": "basic_date",
                    "type": "date",
                    "value": ["2019-11-21T12:40:33.166416+00:00"],
                    "is_multiple": False,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": file_custom_filed_uuid,
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": thumbnail_uuid,
                        },
                    ],
                }
            ],
            destruction_date=None,
            confidentiality="public",
            result="afgehandeld",
            result_uuid=uuid4(),
            result_description="Afgehandeld!",
            archival_state="vernietigen",
            active_selection_list="12",
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            assignee_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Assign Ee",
            },
            case_type_identification="10",
            case_type_version={
                "uuid": uuid4(),
                "name": "My First Case Type",
            },
            authorizations=["read"],
            type_of_archiving="Bewaren (B)",
            result_explanation="result explanation",
            archive_settings={
                "archive_type": "tmlo",
                "name": ["[[basic_date]]"],
                "classification_code": ["[[basic_date]]"],
                "classification_description": None,
                "classification_source_tmlo": None,
                "classification_date": None,
                "description_tmlo": ["[[basic_date]]"],
                "location": ["[[basic_date]]"],
                "coverage_in_time_tmlo": ["[[basic_date]]"],
                "coverage_in_geo_tmlo": ["[[basic_date]]"],
                "language_tmlo": ["[[basic_date]]"],
                "user_rights": ["[[basic_date]]"],
                "user_rights_description": None,
                "user_rights_date_period": ["[[basic_date]]"],
                "confidentiality": ["[[basic_date]]"],
                "confidentiality_description": None,
                "confidentiality_date_period": ["[[basic_date]]"],
                "form_genre": ["[[basic_date]]"],
                "form_publication": None,
                "structure": ["[[basic_date]]"],
                "generic_metadata_tmlo": [
                    "this [[basic_name]]",
                    "This is plain text",
                ],
            },
            zaaktype_node_id=10,
            documents=mock_documents,
        )

        mock_event_logs = [mock.MagicMock()]
        mock_event_logs[0].configure_mock(
            event_type="document/create",
            created_by="admin",
            onderwerp="Document 'test_doc' toegevoegd",
        )

        self.session.execute().fetchall.side_effect = [
            mock_cases,
            mock_event_logs,
            [],
            [],
        ]
        mock_realtion_case = mock.MagicMock()
        mock_realtion_case.configure_mock(uuid=case_uuid, id=3)
        self.session.execute().fetchone.side_effect = [
            mock_config,
            mock_realtion_case,
        ]
        self.session.execute.reset_mock()

        self.qry.run_archive_export(
            case_uuid=case_uuid, export_file_uuid=uuid4()
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

    def test_run_tmlo_archive_export_missing_mapping_form(
        self,
    ):
        case_uuid = str(uuid4())
        mock_config = mock.MagicMock()
        mock_config.configure_mock(
            export_id="NL-9999-0000",
            is_multi_tenant=1,
            notes=None,
            multi_tenant_host="sprint.zaaksysteem.nl",
            export_target="TEST",
            export_type="TMLO",
            uuid=None,
        )
        mock_documents = [mock.MagicMock()]
        mock_documents = [
            {
                "id": 1,
                "uuid": uuid4(),
                "extension": ".odt",
                "filename": "testfile",
                "mimetype": "application/pdf",
                "size": 1,
                "store_uuid": uuid4(),
                "md5": "md5",
                "date_created": "2019-11-21T12:40:33.166416+00:00",
                "confidentiality": "confidential",
                "storage_location": ["minio"],
                "case_id": 3,
            }
        ]
        case_uuid = str(uuid4())
        file_custom_filed_uuid = uuid4()
        thumbnail_uuid = uuid4()
        mock_cases = [mock.MagicMock()]
        mock_cases[0].configure_mock(
            id=3,
            uuid=case_uuid,
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "basic_name",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_4",
                    "magic_string": "basic_date",
                    "type": "date",
                    "value": ["2019-11-21T12:40:33.166416+00:00"],
                    "is_multiple": False,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": file_custom_filed_uuid,
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": thumbnail_uuid,
                        },
                    ],
                }
            ],
            destruction_date=None,
            confidentiality="public",
            result="afgehandeld",
            result_uuid=uuid4(),
            result_description="Afgehandeld!",
            archival_state="vernietigen",
            active_selection_list="12",
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            assignee_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Assign Ee",
            },
            case_type_identification="10",
            case_type_version={
                "uuid": uuid4(),
                "name": "My First Case Type",
            },
            authorizations=["read"],
            type_of_archiving="Bewaren (B)",
            result_explanation="result explanation",
            archive_settings={
                "archive_type": "tmlo",
                "name": ["[[basic_date]]"],
                "classification_code": ["[[basic_date]]"],
                "classification_description": ["[[basic_date]]"],
                "classification_source_tmlo": ["[[basic_date]]"],
                "classification_date": None,
                "description_tmlo": [10],
                "location": ["[[basic_date]]"],
                "coverage_in_time_tmlo": ["[[basic_date]]"],
                "coverage_in_geo_tmlo": ["[[basic_date]]"],
                "language_tmlo": "",
                "user_rights": ["[[basic_date]]"],
                "user_rights_description": ["[[basic_date]]"],
                "user_rights_date_period": ["[[basic_date]]"],
                "confidentiality": ["[[basic_date]]"],
                "confidentiality_description": None,
                "confidentiality_date_period": ["[[basic_date]]"],
                "form_genre": ["[[basic_date]]"],
                "form_publication": ["[[basic_date]]"],
                "structure": None,
                "generic_metadata_tmlo": [
                    "this [[basic_name]]",
                    "This is plain text",
                ],
            },
            zaaktype_node_id=10,
            documents=mock_documents,
        )

        mock_event_logs = [mock.MagicMock()]
        mock_event_logs[0].configure_mock(
            event_type="document/create",
            created_by="admin",
            onderwerp="Document 'test_doc' toegevoegd",
        )

        self.session.execute().fetchall.side_effect = [
            mock_cases,
            mock_event_logs,
            [],
            [],
        ]
        mock_realtion_case = mock.MagicMock()
        mock_realtion_case.configure_mock(uuid=case_uuid, id=3)
        self.session.execute().fetchone.side_effect = [
            mock_config,
            mock_realtion_case,
        ]
        self.session.execute.reset_mock()

        self.qry.run_archive_export(
            case_uuid=case_uuid, export_file_uuid=uuid4()
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

    def test_run_tmlo_archive_export_no_cases(self):
        case_uuid = str(uuid4())
        mock_config = mock.MagicMock()
        mock_config.configure_mock(
            export_id="NL-9999-0000",
            is_multi_tenant=1,
            notes=None,
            multi_tenant_host="sprint.zaaksysteem.nl",
            export_target="TEST",
            export_type="TMLO",
            uuid=None,
        )
        self.session.execute().fetchall.side_effect = [None]
        self.session.execute().fetchone.side_effect = [mock_config]
        self.session.execute.reset_mock()
        with pytest.raises(ValueError) as excinfo:
            self.qry.run_archive_export(
                case_uuid=case_uuid, export_file_uuid=uuid4()
            )
        assert excinfo.value.args == (
            "Archive export is not generated as there are no TMLO cases",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        select_statement = call_list[0][0][0]

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT interface.interface_config \n"
            "FROM interface \n"
            "WHERE interface.module = %(module_1)s AND interface.date_deleted IS NULL AND interface.active"
        )

    def test_run_tmlo_archive_export_no_integration_config(self):
        case_uuid = str(uuid4())
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as excinfo:
            self.qry.run_archive_export(
                case_uuid=case_uuid, export_file_uuid=uuid4()
            )

        assert excinfo.value.args == (
            "No configuration found for archive export",
        )
