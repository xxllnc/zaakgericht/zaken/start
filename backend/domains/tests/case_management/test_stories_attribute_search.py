# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.queries.attribute_search import (
    AttributeSearchFilter,
)


class Test_Attribute_Search_Queries(TestBase):
    mock_db_rows = [mock.Mock(), mock.Mock()]
    attribute_uuid_1 = uuid4()
    attribute_uuid_2 = uuid4()
    mock_db_rows[0].configure_mock(
        uuid=attribute_uuid_1,
        magic_string="test_address",
        label="test address",
        value_type="bag_straat_adressen",
        description="Test street address",
    )
    mock_db_rows[1].configure_mock(
        uuid=attribute_uuid_2,
        magic_string="test_doc_label",
        label="Test document label",
        value_type="file",
        description="Test document label",
    )

    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True},
        )
        self.qry.user_info = self.user_info

    def test_search_attribute(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        attribute_search_result = self.qry.search_attribute(
            page=1, page_size=10
        )

        assert len(attribute_search_result.entities) == 2
        # verify 1st search result
        search_result1 = attribute_search_result.entities[0]
        assert search_result1.label == "test address"
        assert search_result1.magic_string == "test_address"
        assert search_result1.description == "Test street address"
        assert search_result1.type == "bag_straat_adressen"

        # verify 2nd search result
        search_result2 = attribute_search_result.entities[1]
        assert search_result2.label == "Test document label"
        assert search_result2.magic_string == "test_doc_label"
        assert search_result2.description == "Test document label"
        assert search_result2.type == "file"

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.description, bibliotheek_kenmerken.naam AS label, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.value_type \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

    def test_search_attribute_with_keyword_filter(self):
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = AttributeSearchFilter()
        filters.filter_keyword = "test"
        self.qry.search_attribute(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.description, bibliotheek_kenmerken.naam AS label, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.value_type \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL AND ((CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_1)s) IS NULL OR (CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_2)s) != %(param_3)s) AND bibliotheek_kenmerken.naam ILIKE %(naam_1)s ESCAPE '~' \n"
            " LIMIT %(param_4)s OFFSET %(param_5)s"
        )

    def test_search_attribute_restrict_sensitive_data(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        self.qry.search_attribute(page=1, page_size=10)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.description, bibliotheek_kenmerken.naam AS label, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.value_type \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
