# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.exceptions
import pytest
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import entities
from zsnl_domains.case_management.entities import _shared


class Test_Case_Basic_Queries(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_get_case_basic(self):
        case_uuid = str(uuid4())
        file_custom_filed_uuid = uuid4()
        thumbnail_uuid = uuid4()
        mock_db_row = mock.Mock(
            id=3,
            uuid=uuid4(),
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring1",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "some json type",
                    "magic_string": "magicstring3",
                    "value": ['{"json": "JSON!"}'],
                    "type": "geojson",
                    "is_multiple": False,
                },
                {
                    "name": "some bad json",
                    "magic_string": "magicstring4",
                    "value": ["definitely not JSON"],
                    "type": "geojson",
                    "is_multiple": False,
                },
                {
                    "naam": "address v2",
                    "magic_string": "address_v2",
                    "value": [
                        '{"geojson":{"type":"FeatureCollection","features":[{"properties":{},"type":"Feature","geometry":{"coordinates":[4.6417655,52.37360117],"type":"Point"}}]},"bag":{"type":"nummeraanduiding","id":"nummeraanduiding-0392200000093761"},"address":{"full":"Klaprooshof 1, Haarlem"}}'
                    ],
                    "type": "address_v2",
                    "is_multiple": False,
                },
                {
                    "naam": "Address straat",
                    "magic_string": "address_straat",
                    "value": ["openbareruimte-0362300000027499"],
                    "type": "bag_openbareruimte",
                    "is_multiple": True,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": file_custom_filed_uuid,
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": thumbnail_uuid,
                        },
                    ],
                }
            ],
            registration_date=datetime.datetime.now(tz=datetime.UTC),
            target_completion_date=datetime.datetime.now(tz=datetime.UTC),
            completion_date=None,
            destruction_date=None,
            case_meta={
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
                "unaccepted_files_count": 5,
                "unaccepted_attribute_update_count": 10,
                "unread_communication_count": 100,
            },
            stalled_until=None,
            confidentiality="public",
            contact_channel="web",
            milestone=3,
            progress_status=50.3,
            result="afgehandeld",
            result_uuid=uuid4(),
            result_description="Afgehandeld!",
            archival_state="vernietigen",
            active_selection_list="12",
            preset_client=False,
            payment_amount=100.00,
            payment_status="pending",
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            requestor_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Re Questor",
            },
            assignee_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Assign Ee",
            },
            coordinator_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Co Ordinator",
            },
            case_department={"uuid": uuid4(), "name": "some department"},
            case_role={"uuid": uuid4(), "name": "some role"},
            html_email_template="html_tpl1",
            case_type={"uuid": uuid4()},
            case_type_version={"uuid": uuid4(), "name": "My First Case Type"},
            authorizations=["read"],
            user_is_requestor=True,
            user_is_assignee=True,
            user_is_coordinator=True,
            user_is_admin=False,
            case_subjects=[
                {
                    "betrokkene_type": "medewerker",
                    "type": "employee",
                    "subject_id": str(uuid4()),
                    "rol": "Ontvanger",
                    "magic_string_prefix": "ontvanger",
                    "display_name": "R.E. Cipient",
                }
            ],
        )
        mock.seal(mock_db_row)

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        case_basic = self.qry.get_case_basic_by_uuid(case_uuid=case_uuid)

        assert case_basic.entity_id == case_basic.uuid == mock_db_row.uuid
        assert case_basic.number == 3
        assert case_basic.case_type.uuid == mock_db_row.case_type["uuid"]
        assert (
            case_basic.case_type_version.uuid
            == mock_db_row.case_type_version["uuid"]
        )
        assert case_basic.custom_fields == {
            "magicstring1": {"type": "text", "value": "some value"},
            "magicstring3": {"type": "geojson", "value": {"json": "JSON!"}},
            "magicstring4": {"type": "geojson", "value": {}},
            "address_v2": {
                "type": "address_v2",
                "value": {
                    "geojson": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "properties": {},
                                "type": "Feature",
                                "geometry": {
                                    "coordinates": [4.6417655, 52.37360117],
                                    "type": "Point",
                                },
                            }
                        ],
                    },
                    "bag": {
                        "type": "nummeraanduiding",
                        "id": "0392200000093761",
                    },
                    "address": {"full": "Klaprooshof 1, Haarlem"},
                },
            },
            "address_straat": {
                "type": "bag_openbareruimte",
                "value": ["0362300000027499"],
            },
            "magicstring2": {
                "type": "file",
                "value": [
                    {
                        "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                        "size": 3,
                        "uuid": file_custom_filed_uuid,
                        "filename": "hello.txt",
                        "mimetype": "text/plain",
                        "is_archivable": True,
                        "original_name": "hello.txt",
                        "thumbnail_uuid": thumbnail_uuid,
                    }
                ],
            },
        }

        assert case_basic.recipient.uuid == UUID(
            mock_db_row.case_subjects[0]["subject_id"]
        )
        assert case_basic.num_unaccepted_files == 5
        assert case_basic.num_unaccepted_updates == 10
        assert case_basic.num_unread_communication == 100

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.registratiedatum AS registration_date, view_case_v2.streefafhandeldatum AS target_completion_date, view_case_v2.afhandeldatum AS completion_date, view_case_v2.vernietigingsdatum AS destruction_date, view_case_v2.case_meta, view_case_v2.stalled_until, view_case_v2.confidentiality, view_case_v2.contactkanaal AS contact_channel, view_case_v2.milestone, view_case_v2.progress_days AS progress_percentage, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.preset_client, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.status, view_case_v2.onderwerp AS summary, view_case_v2.onderwerp_extern AS public_summary, view_case_v2.requestor_obj, view_case_v2.assignee_obj, view_case_v2.coordinator_obj, view_case_v2.case_department, view_case_v2.case_role, view_case_v2.html_email_template, view_case_v2.case_type, view_case_v2.case_type_version, view_case_v2.case_subjects, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_1)s ELSE %(param_2)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_3)s ELSE %(param_4)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_coordinator, %(param_7)s AS user_is_admin, CAST(view_case_v2.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress_status \n"
            "FROM view_case_v2 JOIN zaaktype_node ON zaaktype_node.id = view_case_v2.zaaktype_node_id \n"
            "WHERE view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_1)s AND (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND view_case_v2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.casetype_id = view_case_v2.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) AND view_case_v2.uuid = %(uuid_3)s::UUID"
        )

    def test_get_case_basic_without_optional_parts(self):
        case_uuid = str(uuid4())

        mock_db_row = mock.Mock(
            id=3,
            uuid=uuid4(),
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring1",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "some json type",
                    "magic_string": "magicstring3",
                    "value": ['{"json": "JSON!"}'],
                    "type": "geojson",
                    "is_multiple": False,
                },
                {
                    "name": "some bad json",
                    "magic_string": "magicstring4",
                    "value": ["definitely not JSON"],
                    "type": "geojson",
                    "is_multiple": False,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": uuid4(),
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": uuid4(),
                        },
                    ],
                }
            ],
            registration_date=datetime.datetime.now(tz=datetime.UTC),
            target_completion_date=datetime.datetime.now(tz=datetime.UTC),
            completion_date=None,
            destruction_date=None,
            case_meta={
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
                "unaccepted_files_count": 0,
                "unaccepted_attribute_update_count": 0,
                "unread_communication_count": 100,
            },
            stalled_until=None,
            confidentiality="public",
            contact_channel="web",
            milestone=3,
            progress_status="",
            result=None,
            result_uuid=None,
            result_description=None,
            archival_state="vernietigen",
            active_selection_list=None,
            preset_client=False,
            payment_amount=None,
            payment_status=None,
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            requestor_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Re Questor",
            },
            assignee_obj=None,
            coordinator_obj=None,
            case_department={"uuid": uuid4(), "name": "some department"},
            case_role={"uuid": None, "name": None},
            html_email_template="html_tpl1",
            case_type={"uuid": uuid4()},
            case_type_version={"uuid": uuid4(), "name": "My First Case Type"},
            authorizations=["read"],
            user_is_requestor=False,
            user_is_assignee=False,
            user_is_coordinator=False,
            user_is_admin=True,
            case_subjects=[],
        )
        mock.seal(mock_db_row)

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        case_basic: entities.CaseBasic = self.qry.get_case_basic_by_uuid(
            case_uuid=case_uuid
        )

        assert case_basic.role is None
        assert case_basic.assignee is None
        assert case_basic.coordinator is None
        assert case_basic.payment is None
        assert case_basic.result is None
        assert case_basic.recipient is None
        assert case_basic.progress_status is None

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.registratiedatum AS registration_date, view_case_v2.streefafhandeldatum AS target_completion_date, view_case_v2.afhandeldatum AS completion_date, view_case_v2.vernietigingsdatum AS destruction_date, view_case_v2.case_meta, view_case_v2.stalled_until, view_case_v2.confidentiality, view_case_v2.contactkanaal AS contact_channel, view_case_v2.milestone, view_case_v2.progress_days AS progress_percentage, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.preset_client, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.status, view_case_v2.onderwerp AS summary, view_case_v2.onderwerp_extern AS public_summary, view_case_v2.requestor_obj, view_case_v2.assignee_obj, view_case_v2.coordinator_obj, view_case_v2.case_department, view_case_v2.case_role, view_case_v2.html_email_template, view_case_v2.case_type, view_case_v2.case_type_version, view_case_v2.case_subjects, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_1)s ELSE %(param_2)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_3)s ELSE %(param_4)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_coordinator, %(param_7)s AS user_is_admin, CAST(view_case_v2.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress_status \n"
            "FROM view_case_v2 JOIN zaaktype_node ON zaaktype_node.id = view_case_v2.zaaktype_node_id \n"
            "WHERE view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_1)s AND (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND view_case_v2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.casetype_id = view_case_v2.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) AND view_case_v2.uuid = %(uuid_3)s::UUID"
        )

    def test_get_case_basic_minimal_authorization(self):
        case_uuid = str(uuid4())

        mock_db_row = mock.Mock(
            id=3,
            uuid=uuid4(),
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring1",
                    "value": ["some value"],
                    "type": "text",
                    "is_multiple": False,
                },
                {
                    "name": "some json type",
                    "magic_string": "magicstring3",
                    "value": ['{"json": "JSON!"}'],
                    "type": "geojson",
                    "is_multiple": False,
                },
                {
                    "name": "some bad json",
                    "magic_string": "magicstring4",
                    "value": ["definitely not JSON"],
                    "type": "geojson",
                    "is_multiple": False,
                },
                {
                    "name": "custom_field_4",
                    "magic_string": "object_relation_4",
                    "type": "date",
                    "value": ["2019-05-09T00:00:00Z"],
                    "is_multiple": False,
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "is_multiple": False,
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": uuid4(),
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": uuid4(),
                        },
                    ],
                }
            ],
            registration_date=datetime.datetime.now(tz=datetime.UTC),
            target_completion_date=datetime.datetime.now(tz=datetime.UTC),
            completion_date=None,
            destruction_date=None,
            case_meta={
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
                "unaccepted_files_count": 0,
                "unaccepted_attribute_update_count": 0,
                "unread_communication_count": 1,
            },
            stalled_until=None,
            confidentiality="public",
            contact_channel="web",
            milestone=3,
            progress_status=50.3,
            result=None,
            result_uuid=None,
            result_description=None,
            archival_state="vernietigen",
            active_selection_list=None,
            preset_client=False,
            payment_amount=None,
            payment_status=None,
            status="open",
            summary="Some summary words",
            public_summary="Other summary words",
            requestor_obj={
                "uuid": uuid4(),
                "type": "employee",
                "display_name": "Re Questor",
            },
            assignee_obj=None,
            coordinator_obj=None,
            case_department={"uuid": uuid4(), "name": "some department"},
            case_role={"uuid": None, "name": None},
            html_email_template="html_tpl1",
            case_type={"uuid": uuid4()},
            case_type_version={"uuid": uuid4(), "name": "My First Case Type"},
            authorizations=["read"],
            user_is_requestor=False,
            user_is_assignee=False,
            user_is_coordinator=False,
            user_is_admin=False,
            case_subjects=[],
        )
        mock.seal(mock_db_row)

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        case_basic: entities.CaseBasic = self.qry.get_case_basic_by_uuid(
            case_uuid=case_uuid
        )

        assert case_basic.entity_meta_authorizations == {
            _shared.CaseAuthorizationLevel.read,
        }
        assert case_basic.role is None
        assert case_basic.assignee is None
        assert case_basic.coordinator is None
        assert case_basic.payment is None
        assert case_basic.result is None

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.registratiedatum AS registration_date, view_case_v2.streefafhandeldatum AS target_completion_date, view_case_v2.afhandeldatum AS completion_date, view_case_v2.vernietigingsdatum AS destruction_date, view_case_v2.case_meta, view_case_v2.stalled_until, view_case_v2.confidentiality, view_case_v2.contactkanaal AS contact_channel, view_case_v2.milestone, view_case_v2.progress_days AS progress_percentage, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.preset_client, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.status, view_case_v2.onderwerp AS summary, view_case_v2.onderwerp_extern AS public_summary, view_case_v2.requestor_obj, view_case_v2.assignee_obj, view_case_v2.coordinator_obj, view_case_v2.case_department, view_case_v2.case_role, view_case_v2.html_email_template, view_case_v2.case_type, view_case_v2.case_type_version, view_case_v2.case_subjects, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_1)s ELSE %(param_2)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_3)s ELSE %(param_4)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_coordinator, %(param_7)s AS user_is_admin, CAST(view_case_v2.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress_status \n"
            "FROM view_case_v2 JOIN zaaktype_node ON zaaktype_node.id = view_case_v2.zaaktype_node_id \n"
            "WHERE view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_1)s AND (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND view_case_v2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.casetype_id = view_case_v2.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) AND view_case_v2.uuid = %(uuid_3)s::UUID"
        )

    def test_get_case_basic_notfound(self):
        case_uuid = str(uuid4())

        mock_db_row = None

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.get_case_basic_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.registratiedatum AS registration_date, view_case_v2.streefafhandeldatum AS target_completion_date, view_case_v2.afhandeldatum AS completion_date, view_case_v2.vernietigingsdatum AS destruction_date, view_case_v2.case_meta, view_case_v2.stalled_until, view_case_v2.confidentiality, view_case_v2.contactkanaal AS contact_channel, view_case_v2.milestone, view_case_v2.progress_days AS progress_percentage, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.preset_client, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.status, view_case_v2.onderwerp AS summary, view_case_v2.onderwerp_extern AS public_summary, view_case_v2.requestor_obj, view_case_v2.assignee_obj, view_case_v2.coordinator_obj, view_case_v2.case_department, view_case_v2.case_role, view_case_v2.html_email_template, view_case_v2.case_type, view_case_v2.case_type_version, view_case_v2.case_subjects, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_1)s ELSE %(param_2)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_3)s ELSE %(param_4)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_coordinator, %(param_7)s AS user_is_admin, CAST(view_case_v2.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress_status \n"
            "FROM view_case_v2 JOIN zaaktype_node ON zaaktype_node.id = view_case_v2.zaaktype_node_id \n"
            "WHERE view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_1)s AND (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND view_case_v2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.casetype_id = view_case_v2.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) AND view_case_v2.uuid = %(uuid_3)s::UUID"
        )
