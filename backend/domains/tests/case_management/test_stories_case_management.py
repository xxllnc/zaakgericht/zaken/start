# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pytest
import random
import sqlalchemy.exc
from collections import defaultdict, namedtuple
from datetime import date, datetime, timedelta
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import entities
from zsnl_domains.case_management.entities.case import CaseContactEmployee
from zsnl_domains.case_management.entities.case_type_version import (
    CaseTypePayment,
    CaseTypePaymentDict,
    CustomFieldDateLimit,
)


class Test_AsUser_create_Case(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, custom_fields, rules=None):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["id"] = "1"
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules or [],
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"
        mock_case_type["results"] = [
            {
                "id": 156,
                "uuid": "a04d7629-ca9a-4246-8e9c-cc38b327e904",
                "name": "Bewaren",
                "result": "behaald",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
            {
                "id": 157,
                "uuid": "604138d9-e14f-4944-812c-12a098a68092",
                "name": "Nog een 2de resultaat",
                "result": "afgebroken",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
        ]

        return mock_case_type

    def __mock_casemeta_current_deadline(self) -> dict:
        return {
            "start": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
            "days": 0,
            "current": 0,
            "phase_id": 1,
            "phase_no": 1,
        }

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Checkbox",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": False,
                "field_options": ["test", "test1"],
                "date_field_limit": {},
            },
            {
                "id": 334,
                "uuid": uuid4(),
                "is_required": False,
                "field_magic_string": "test_option",
                "field_type": "option",
                "label": "Custom label for option",
                "field_options": ["step1", "step2"],
                "name": "Option name",
            },
            {
                "id": 10,
                "uuid": uuid4(),
                "is_required": False,
                "field_magic_string": "basic_date",
                "field_type": "date",
                "label": "Basic date",
                "field_options": [],
                "name": "Basic date",
                "date_field_limit": CustomFieldDateLimit(start=None, end=None),
            },
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        requestor_uuid = uuid4()
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = dict(
            id="1",
            uuid=requestor_uuid,
            type="employee",
            properties=properties,
            settings="{}",
            username="Subject",
            last_modified=None,
            role_ids=[],
            group_ids=[1, 2],
            nobody=False,
            system=True,
            department_uuid=department_uuid,
            department="depatment",
            employee_address=None,
            Group=[],
        )

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            None,
            None,
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="email",
            requestor={"type": "employee", "id": str(requestor_uuid)},
            custom_fields={
                "test_checkbox": [["test", "test1"]],
                "test_option": ["step1", "step2"],
                "basic_date": ["2022-12-07"],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={"mobile_number": "+3106123456789"},
            options={"allow_missing_required_fields": True},
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query) == "UPDATE zaak SET route_ou=(SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_1), route_role=(SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2), last_modified=:last_modified, onderwerp=:onderwerp, onderwerp_extern=:onderwerp_extern WHERE zaak.uuid = :uuid_3"
        )

        db_execute_calls = self.session.execute.call_args_list
        select_current_timeline = db_execute_calls[15][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(select_current_timeline)
            == "SELECT json_build_object(%(json_build_object_2)s, %(json_build_object_3)s, %(json_build_object_4)s, zaaktype_status.termijn, %(json_build_object_5)s, %(json_build_object_6)s, %(json_build_object_7)s, zaaktype_status.id, %(json_build_object_8)s, zaaktype_status.status) AS json_build_object_1 \nFROM zaak JOIN zaaktype_status ON zaak.zaaktype_node_id = zaaktype_status.zaaktype_node_id AND zaak.milestone + %(milestone_1)s = zaaktype_status.status \nWHERE zaak.id = %(id_1)s"
        )
        insert_case_meta = db_execute_calls[16][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_case_meta)
            == "INSERT INTO zaak_meta (zaak_id, current_deadline, deadline_timeline, last_modified, unread_communication_count, unaccepted_attribute_update_count, unaccepted_files_count, pending_changes) VALUES (%(zaak_id)s, %(current_deadline)s::JSONB, %(deadline_timeline)s::JSONB, %(last_modified)s, %(unread_communication_count)s, %(unaccepted_attribute_update_count)s, %(unaccepted_files_count)s, %(pending_changes)s::JSONB) RETURNING zaak_meta.id"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_duplicate_uuid(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Checkbox",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": False,
                "field_options": ["test", "test1"],
                "date_field_limit": {},
            },
            {
                "id": 334,
                "uuid": uuid4(),
                "is_required": False,
                "field_magic_string": "test_option",
                "field_type": "option",
                "label": "Custom label for option",
                "field_options": ["step1", "step2"],
                "name": "Option name",
            },
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        requestor_uuid = uuid4()
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": requestor_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            None,
            None,
        ]
        x = mock.MagicMock()
        x.__getitem__.side_effect = [
            100,  # Case contact (requestor?)
            sqlalchemy.exc.IntegrityError(None, None, None),
        ]
        self.session.execute().inserted_primary_key = x

        case_uuid = str(uuid4())

        with pytest.raises(Conflict):
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(requestor_uuid)},
                custom_fields={},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": False,
                    }
                },
                contact_information={"mobile_number": "+3106123456789"},
                options={"allow_missing_required_fields": True},
            )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_incorrect_custom_fields(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject_field",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            },
            {
                "name": "Test checkbox",
                "uuid": field_type_uuids[1],
                "field_magic_string": "att_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": True,
                "is_required": True,
                "field_options": ["option_a", "option_b"],
                "date_field_limit": {},
            },
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        department_uuid = uuid4()
        subject_uuid = str(self.cmd.user_uuid)

        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        val = [
            {
                "case_id": 247,
                "case_type_id": 1,
                "case_type_node_id": 83,
                "case_cordinator_id": 1,
                "case_assignee_id": 1,
                "department_id": 1,
                "role_id": 1,
            }
        ]

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            val,
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            None,
            None,
        ]

        case_uuid = str(uuid4())
        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="internal",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": subject_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            "Custom fields 'test_subject' is/are not allowed",
            "case/custom_fields_not_allowed",
        )

        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            case_type_custom_fields = [
                {
                    "name": "Test Subject",
                    "uuid": field_type_uuids[0],
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_hidden_field": False,
                    "field_options": [],
                    "date_field_limit": {},
                    "is_required": True,
                },
                {
                    "name": "Test Name",
                    "uuid": field_type_uuids[0],
                    "field_magic_string": "test_name",
                    "field_type": "text",
                    "is_hidden_field": False,
                    "field_options": [],
                    "date_field_limit": {},
                    "is_required": True,
                },
            ]
            mock_case_type = self.__mock_case_type(
                case_type_uuid, case_type_custom_fields
            )
            self.session.query().filter().one.side_effect = [mock_subject]

            self.session.execute().fetchone.side_effect = [
                mock_case_type_node,
                [mock_case_type],
                True,
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                parent_department,
                mock_role,
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                val,
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                None,
                None,
            ]
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="internal",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": subject_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": False},
            )
        assert excinfo.value.args == (
            "Custom fields 'test_name' is/are required",
            "case/required_custom_fields_missing",
        )

        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.session.query().filter().one.side_effect = [mock_subject]

            self.session.execute().fetchone.side_effect = [
                mock_case_type_node,
                [mock_case_type],
                True,
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                parent_department,
                mock_role,
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                val,
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                None,
                None,
            ]
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": "test"},
                confidentiality="internal",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": subject_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            "Custom field values 'test' for 'test_subject' is not a list",
            "case/custom_field_values_not_list",
        )

        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            mock_case_type["phases"] = []
            mock_role = mock.Mock()
            mock_role.configure_mock(
                uuid=str(uuid4()),
                name="Role name",
                description="A role",
                parent_uuid=uuid4(),
                parent_name="Parent group",
            )
            self.session.execute().fetchone.side_effect = [
                mock_case_type_node,
                [mock_case_type],
                True,
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                parent_department,
                mock_role,
                val,
            ]
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": "test"},
                confidentiality="internal",
                assignment={
                    "role": {"id": str(uuid4()), "type": "role"},
                    "department": {
                        "id": str(department_uuid),
                        "type": "department",
                    },
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            f"Case type '{mock_case_type['uuid']}' has no phases in it.",
            "case_type/no_phases",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_rules(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": " contact channel rule",
                "created": "2022-07-01T11:13:54.80033",
                "last_modified": "2022-07-01T11:13:54.80033",
                "settings": '{"voorwaarde_1_kenmerk":"contactchannel","mijlsort":"1","active":"1","naam":" contact channel rule","acties":"1","bibliotheek_kenmerken_id":null,"actie_1_value":"internal","ander_2":"change_confidentiality","voorwaarden":"1","ander_2_attribute_type":"none","voorwaarde_1_value_checkbox":"1","condition_type":"and","ander_2_value":"public","actie_1":"change_confidentiality","actie_1_attribute_type":"none","properties":{},"is_group":0,"anders":"2","voorwaarde_1_value":"behandelaar"}',
                "active": True,
                "is_group": False,
            }
        ]

        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "internal"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_rules_with_or_condition(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"or","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"contactchannel","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"change_confidentiality","ander_2":"change_confidentiality","actie_1_value":"internal","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"none","anders":"2"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="post",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_nomatching_rules(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"and","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"contactchannel","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"change_confidentiality","ander_2":"change_confidentiality","actie_1_value":"internal","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"none","anders":"2"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="post",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_set_value_ruleaction(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"and","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"contactchannel","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"set_value","ander_2":"change_confidentiality","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"valuta","anders":"2","actie_1_value":"230.0","actie_1_kenmerk":"333","actie_1_can_change":"0"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "id value_type")(
                id=1, value_type="numeric"
            ),
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        compiled_insert_update = db_execute_calls[25][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_insert_update) == (
            "INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, value, magic_string) VALUES (%(zaak_id)s, %(bibliotheek_kenmerken_id)s, %(value)s::TEXT[], %(magic_string)s) ON CONFLICT (zaak_id, bibliotheek_kenmerken_id) DO UPDATE SET value = %(param_1)s::TEXT[] RETURNING zaak_kenmerk.id"
        )
        assert compiled_insert_update.params["value"] == ["230.0"]
        assert compiled_insert_update.params["magic_string"] == "test_subject"
        assert compiled_insert_update.params["bibliotheek_kenmerken_id"] == 1

        # test with action_kenmerk as an int
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "valuta",
                        "anders": "2",
                        "actie_1_value": "230.0",
                        "actie_1_kenmerk": 333,
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "id value_type")(
                id=1, value_type="numeric"
            ),
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        compiled_insert_update = db_execute_calls[25][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert_update) == (
            "INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, value, magic_string) VALUES (%(zaak_id)s, %(bibliotheek_kenmerken_id)s, %(value)s::TEXT[], %(magic_string)s) ON CONFLICT (zaak_id, bibliotheek_kenmerken_id) DO UPDATE SET value = %(param_1)s::TEXT[] RETURNING zaak_kenmerk.id"
        )
        assert compiled_insert_update.params["value"] == ["230.0"]
        assert compiled_insert_update.params["magic_string"] == "test_subject"
        assert compiled_insert_update.params["bibliotheek_kenmerken_id"] == 1

        # test for invalid kenmerk setting. the action should be skipped
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "valuta",
                        "anders": "2",
                        "actie_1_value": "230.0",
                        # "actie_1_kenmerk": "333",  # this setting is removed for test
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_set_value_price(self, mock_ids):
        # test for rule action set_value with bedrag web
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "result",
                        "anders": "2",
                        "actie_1_value": "1",
                        "actie_1_kenmerk": "case_result",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        # test for set_value price web
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "price",
                        "anders": "2",
                        "actie_1_value": "1",
                        "actie_1_kenmerk": "price",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        update_case_stmt = db_execute_calls[23][0][0].compile()
        assert (
            str(update_case_stmt)
            == "UPDATE zaak SET last_modified=:last_modified, payment_amount=:payment_amount WHERE zaak.uuid = :uuid_1"
        )

        # action value setting not available
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "price",
                        "anders": "2",
                        # "actie_1_value": "1",
                        "actie_1_kenmerk": "price",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

        # action value no valid float
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "price",
                        "anders": "2",
                        "actie_1_value": "abcd",  # this is the invalid float
                        "actie_1_kenmerk": "price",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_set_value_result(self, mock_ids):
        # test for rule action set_value with result.
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "result",
                        "anders": "2",
                        "actie_1_value": "1",
                        "actie_1_kenmerk": "case_result",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        # test if case_type_node query is performed correct with case type results ordered by id
        compiled_select_casetype_node = db_execute_calls[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select_casetype_node) == (
            "SELECT json_build_object(%(json_build_object_2)s, zaaktype_node.uuid, %(json_build_object_3)s, zaaktype_node.uuid, %(json_build_object_4)s, zaaktype.uuid, %(json_build_object_5)s, zaaktype_node.version, %(json_build_object_6)s, zaaktype.active, %(json_build_object_7)s, zaaktype_node.titel, %(json_build_object_8)s, zaaktype_node.created, %(json_build_object_9)s, zaaktype_node.deleted, %(json_build_object_10)s, zaaktype_node.last_modified, %(json_build_object_11)s, zaaktype_node.trigger, %(json_build_object_12)s, zaaktype_node.code, %(json_build_object_13)s, zaaktype_node.is_public, %(json_build_object_14)s, zaaktype_node.zaaktype_trefwoorden, %(json_build_object_15)s, zaaktype_node.zaaktype_omschrijving, %(json_build_object_16)s, zaaktype_node.toewijzing_zaakintake, %(json_build_object_17)s, zaaktype_node.aanvrager_hergebruik, %(json_build_object_18)s, zaaktype_node.extra_relaties_in_aanvraag, %(json_build_object_19)s, zaaktype_node.automatisch_behandelen, %(json_build_object_20)s, zaaktype_node.contact_info_email_required, %(json_build_object_21)s, zaaktype_node.contact_info_mobile_phone_required, %(json_build_object_22)s, zaaktype_node.contact_info_phone_required, %(json_build_object_23)s, zaaktype_node.contact_info_intake, %(json_build_object_24)s, zaaktype_node.online_betaling, %(json_build_object_25)s, zaaktype_node.webform_toegang, %(json_build_object_26)s, zaaktype_node.prevent_pip, %(json_build_object_27)s, zaaktype_node.adres_aanvrager, %(json_build_object_28)s, CAST(zaaktype_node.properties AS JSON), %(json_build_object_29)s, zaaktype_definitie.handelingsinitiator, %(json_build_object_30)s, zaaktype_definitie.pdc_tarief, %(json_build_object_31)s, zaaktype_definitie.grondslag, %(json_build_object_32)s, zaaktype_definitie.procesbeschrijving, %(json_build_object_33)s, coalesce(zaaktype_definitie.extra_informatie, %(coalesce_1)s), %(json_build_object_34)s, coalesce(zaaktype_definitie.extra_informatie_extern, %(coalesce_2)s), %(json_build_object_35)s, json_build_object(%(json_build_object_36)s, json_build_object(%(json_build_object_37)s, zaaktype_definitie.afhandeltermijn_type, %(json_build_object_38)s, zaaktype_definitie.afhandeltermijn), %(json_build_object_39)s, json_build_object(%(json_build_object_40)s, zaaktype_definitie.servicenorm_type, %(json_build_object_41)s, zaaktype_definitie.servicenorm)), %(json_build_object_42)s, json_build_object(%(json_build_object_43)s, bibliotheek_categorie.uuid, %(json_build_object_44)s, bibliotheek_categorie.naam), %(json_build_object_45)s, array((SELECT json_build_object(%(json_build_object_47)s, zaaktype_status.naam, %(json_build_object_48)s, zaaktype_status.status, %(json_build_object_49)s, zaaktype_status.fase, %(json_build_object_50)s, (SELECT json_build_object(%(json_build_object_52)s, json_build_object(%(json_build_object_53)s, groups.uuid, %(json_build_object_54)s, groups.name), %(json_build_object_55)s, json_build_object(%(json_build_object_56)s, roles.uuid, %(json_build_object_57)s, roles.name), %(json_build_object_58)s, zaaktype_status.role_set) AS json_build_object_51 \n"
            "FROM groups, roles \n"
            "WHERE groups.id = zaaktype_status.ou_id AND roles.id = zaaktype_status.role_id), %(json_build_object_59)s, array((SELECT json_build_object(%(json_build_object_61)s, zaaktype_relatie.relatie_type, %(json_build_object_62)s, zaaktype_node_1.uuid, %(json_build_object_63)s, CAST(zaaktype_relatie.kopieren_kenmerken AS BOOLEAN), %(json_build_object_64)s, CAST(zaaktype_relatie.status IS NOT NULL AS BOOLEAN), %(json_build_object_65)s, zaaktype_relatie.automatisch_behandelen, %(json_build_object_66)s, zaaktype_relatie.required, %(json_build_object_67)s, zaaktype_relatie.show_in_pip, %(json_build_object_68)s, zaaktype_relatie.pip_label, %(json_build_object_69)s, json_build_object(%(json_build_object_70)s, zaaktype_relatie.eigenaar_type, %(json_build_object_71)s, zaaktype_relatie.eigenaar_role), %(json_build_object_72)s, json_build_object(%(json_build_object_73)s, json_build_object(%(json_build_object_74)s, groups.uuid, %(json_build_object_75)s, groups.name), %(json_build_object_76)s, json_build_object(%(json_build_object_77)s, roles.uuid, %(json_build_object_78)s, roles.name))) AS json_build_object_60 \n"
            "FROM zaaktype_relatie, zaaktype_node AS zaaktype_node_1, groups, roles, zaaktype_node, zaaktype AS zaaktype_1 \n"
            "WHERE zaaktype_node.id = %(id_1)s AND zaaktype_relatie.zaaktype_node_id = zaaktype_node.id AND zaaktype_relatie.zaaktype_status_id = zaaktype_status.id AND groups.id = zaaktype_relatie.ou_id AND roles.id = zaaktype_relatie.role_id AND zaaktype_relatie.relatie_zaaktype_id = zaaktype_1.id AND zaaktype_node_1.id = zaaktype_1.zaaktype_node_id)), %(json_build_object_79)s, array((SELECT json_build_object(%(json_build_object_81)s, bibliotheek_kenmerken.id, %(json_build_object_82)s, bibliotheek_kenmerken.naam, %(json_build_object_83)s, bibliotheek_kenmerken.uuid, %(json_build_object_84)s, bibliotheek_kenmerken.naam_public, %(json_build_object_85)s, zaaktype_kenmerken.label, %(json_build_object_86)s, bibliotheek_kenmerken.type_multiple, %(json_build_object_87)s, zaaktype_kenmerken.label_multiple, %(json_build_object_88)s, zaaktype_kenmerken.help, %(json_build_object_89)s, zaaktype_kenmerken.help_extern, %(json_build_object_90)s, zaaktype_kenmerken.value_mandatory, %(json_build_object_91)s, coalesce(zaaktype_kenmerken.pip_can_change, %(coalesce_3)s), %(json_build_object_92)s, coalesce(zaaktype_kenmerken.is_systeemkenmerk, %(coalesce_4)s), %(json_build_object_93)s, coalesce(CAST(zaaktype_kenmerken.properties AS JSON) ->> %(param_1)s, %(coalesce_5)s) = %(coalesce_6)s, %(json_build_object_94)s, coalesce(CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_2)s, %(coalesce_7)s) = %(coalesce_8)s, %(json_build_object_95)s, bibliotheek_kenmerken.magic_string, %(json_build_object_96)s, bibliotheek_kenmerken.value_type, %(json_build_object_97)s, bibliotheek_kenmerken.value_default, %(json_build_object_98)s, CAST(coalesce(zaaktype_kenmerken.referential, %(coalesce_9)s) IS true AS BOOLEAN), %(json_build_object_99)s, %(json_build_object_100)s, %(json_build_object_101)s, array((SELECT json_build_object(%(json_build_object_103)s, json_build_object(%(json_build_object_104)s, groups.uuid, %(json_build_object_105)s, groups.name), %(json_build_object_106)s, json_build_object(%(json_build_object_107)s, roles.uuid, %(json_build_object_108)s, roles.name)) AS json_build_object_102 \n"
            "FROM zaaktype_kenmerken AS zaaktype_kenmerken_1 JOIN LATERAL json_array_elements(CAST(zaaktype_kenmerken.required_permissions AS JSON) -> %(param_3)s) AS units ON zaaktype_kenmerken_1.id = zaaktype_kenmerken.id JOIN groups ON groups.id = CAST(units ->> %(units_1)s AS INTEGER) JOIN roles ON roles.id = CAST(units ->> %(units_2)s AS INTEGER))), %(json_build_object_109)s, array((SELECT bibliotheek_kenmerken_values.value \n"
            "FROM bibliotheek_kenmerken_values \n"
            "WHERE bibliotheek_kenmerken_values.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id)), %(json_build_object_110)s, CASE WHEN ((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_4)s) IS NULL) THEN CAST(%(param_5)s::JSON AS JSON) ELSE CAST(json_build_object(%(json_build_object_111)s, json_build_object(%(json_build_object_112)s, CAST(coalesce(CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_7)s) AS JSON) ->> %(param_8)s, %(coalesce_10)s) = %(coalesce_11)s AS BOOLEAN), %(json_build_object_113)s, CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_7)s) AS JSON) -> %(param_9)s, %(json_build_object_114)s, CAST(coalesce(nullif(CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_7)s) AS JSON) ->> %(param_10)s, %(nullif_1)s), %(coalesce_12)s) AS INTEGER), %(json_build_object_115)s, CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_7)s) AS JSON) -> %(param_11)s, %(json_build_object_116)s, CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_7)s) AS JSON) -> %(param_12)s), %(json_build_object_117)s, json_build_object(%(json_build_object_118)s, CAST(coalesce(CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_13)s) AS JSON) ->> %(param_14)s, %(coalesce_13)s) = %(coalesce_14)s AS BOOLEAN), %(json_build_object_119)s, CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_13)s) AS JSON) -> %(param_15)s, %(json_build_object_120)s, CAST(coalesce(nullif(CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_13)s) AS JSON) ->> %(param_16)s, %(nullif_2)s), %(coalesce_15)s) AS INTEGER), %(json_build_object_121)s, CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_13)s) AS JSON) -> %(param_17)s, %(json_build_object_122)s, CAST(((CAST(zaaktype_kenmerken.properties AS JSON) -> %(param_6)s) -> %(param_13)s) AS JSON) -> %(param_18)s)) AS JSON) END, %(json_build_object_123)s, bibliotheek_kenmerken.relationship_type, %(json_build_object_124)s, bibliotheek_kenmerken.relationship_name, %(json_build_object_125)s, bibliotheek_kenmerken.relationship_uuid, %(json_build_object_126)s, coalesce(CAST(zaaktype_kenmerken.properties AS JSON) ->> %(param_19)s, %(coalesce_16)s)) AS json_build_object_80 \n"
            "FROM bibliotheek_kenmerken, zaaktype_kenmerken, zaaktype_node \n"
            "WHERE zaaktype_kenmerken.zaak_status_id = zaaktype_status.id AND zaaktype_kenmerken.zaaktype_node_id = zaaktype_node.id AND bibliotheek_kenmerken.id = zaaktype_kenmerken.bibliotheek_kenmerken_id AND zaaktype_node.id = %(id_2)s)), %(json_build_object_127)s, array((SELECT json_build_object(%(json_build_object_129)s, coalesce(CAST(zaaktype_sjablonen.automatisch_genereren AS BOOLEAN), %(coalesce_17)s), %(json_build_object_130)s, bibliotheek_sjablonen.naam, %(json_build_object_131)s, json_build_object(%(json_build_object_132)s, %(json_build_object_133)s, %(json_build_object_134)s, zaaktype_sjablonen.bibliotheek_sjablonen_id, %(json_build_object_135)s, zaaktype_sjablonen.bibliotheek_kenmerken_id, %(json_build_object_136)s, filestore.original_name, %(json_build_object_137)s, coalesce(zaaktype_sjablonen.target_format, %(coalesce_18)s), %(json_build_object_138)s, coalesce(CAST(zaaktype_sjablonen.automatisch_genereren AS BOOLEAN), %(coalesce_19)s))) AS json_build_object_128 \n"
            "FROM bibliotheek_sjablonen, zaaktype_sjablonen, filestore, zaaktype \n"
            "WHERE filestore.id = bibliotheek_sjablonen.filestore_id AND zaaktype_sjablonen.bibliotheek_sjablonen_id = bibliotheek_sjablonen.id AND zaaktype.zaaktype_node_id = zaaktype_sjablonen.zaaktype_node_id AND zaaktype.zaaktype_node_id = %(zaaktype_node_id_1)s AND zaaktype_sjablonen.zaak_status_id = zaaktype_status.id)), %(json_build_object_139)s, array((SELECT json_build_object(%(json_build_object_141)s, coalesce(zaaktype_notificatie.intern_block, %(coalesce_20)s) = %(coalesce_21)s, %(json_build_object_142)s, bibliotheek_notificaties.label, %(json_build_object_143)s, json_build_object(%(json_build_object_144)s, %(json_build_object_145)s, %(json_build_object_146)s, zaaktype_notificatie.rcpt, %(json_build_object_147)s, bibliotheek_notificaties.subject, %(json_build_object_148)s, bibliotheek_notificaties.message, %(json_build_object_149)s, bibliotheek_notificaties.sender_address, %(json_build_object_150)s, bibliotheek_notificaties.sender, %(json_build_object_151)s, zaaktype_notificatie.cc, %(json_build_object_152)s, zaaktype_notificatie.bcc, %(json_build_object_153)s, zaaktype_notificatie.betrokkene_role, %(json_build_object_154)s, zaaktype_notificatie.email, %(json_build_object_155)s, zaaktype_notificatie.behandelaar, %(json_build_object_156)s, zaaktype_notificatie.intern_block, %(json_build_object_157)s, array((SELECT json_build_object(%(json_build_object_159)s, %(json_build_object_160)s, %(json_build_object_161)s, bibliotheek_kenmerken.naam, %(json_build_object_162)s, zaaktype_kenmerken.id) AS json_build_object_158 \n"
            "FROM bibliotheek_kenmerken, zaaktype_kenmerken, bibliotheek_notificatie_kenmerk, zaaktype_node \n"
            "WHERE bibliotheek_notificaties.id = bibliotheek_notificatie_kenmerk.bibliotheek_notificatie_id AND zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_notificatie_kenmerk.bibliotheek_kenmerken_id AND bibliotheek_notificatie_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND zaaktype_node.id = %(id_3)s AND zaaktype_kenmerken.zaaktype_node_id = zaaktype_node.id)), %(json_build_object_163)s, coalesce(CAST(zaaktype_notificatie.automatic AS BOOLEAN), %(coalesce_22)s), %(json_build_object_164)s, zaaktype_notificatie.id, %(json_build_object_165)s, bibliotheek_notificaties.uuid, %(json_build_object_166)s, zaaktype_notificatie.bibliotheek_notificaties_id)) AS json_build_object_140 \n"
            "FROM bibliotheek_notificaties, zaaktype_notificatie \n"
            "WHERE zaaktype_notificatie.zaaktype_node_id = %(zaaktype_node_id_2)s AND bibliotheek_notificaties.id = zaaktype_notificatie.bibliotheek_notificaties_id AND zaaktype_notificatie.zaak_status_id = zaaktype_status.id)), %(json_build_object_167)s, array((SELECT json_build_object(%(json_build_object_169)s, zaaktype_standaard_betrokkenen.naam, %(json_build_object_170)s, %(json_build_object_171)s, %(json_build_object_172)s, json_build_object(%(json_build_object_173)s, zaaktype_standaard_betrokkenen.betrokkene_identifier, %(json_build_object_174)s, zaaktype_standaard_betrokkenen.naam, %(json_build_object_175)s, zaaktype_standaard_betrokkenen.rol, %(json_build_object_176)s, zaaktype_standaard_betrokkenen.magic_string_prefix, %(json_build_object_177)s, zaaktype_standaard_betrokkenen.gemachtigd, %(json_build_object_178)s, zaaktype_standaard_betrokkenen.notify, %(json_build_object_179)s, zaaktype_standaard_betrokkenen.uuid, %(json_build_object_180)s, zaaktype_standaard_betrokkenen.betrokkene_type)) AS json_build_object_168 \n"
            "FROM zaaktype_standaard_betrokkenen, zaaktype_node \n"
            "WHERE zaaktype_standaard_betrokkenen.zaaktype_node_id = zaaktype_node.id AND zaaktype_standaard_betrokkenen.zaak_status_id = zaaktype_status.id AND zaaktype_node.id = %(id_4)s)), %(json_build_object_181)s, array((SELECT zaaktype_status_checklist_item.label \n"
            "FROM zaaktype_status_checklist_item \n"
            "WHERE zaaktype_status_checklist_item.casetype_status_id = zaaktype_status.id ORDER BY zaaktype_status_checklist_item.id)), %(json_build_object_182)s, array((SELECT json_build_object(%(json_build_object_184)s, zaaktype_regel.naam, %(json_build_object_185)s, zaaktype_regel.created, %(json_build_object_186)s, zaaktype_regel.last_modified, %(json_build_object_187)s, zaaktype_regel.settings, %(json_build_object_188)s, zaaktype_regel.active, %(json_build_object_189)s, zaaktype_regel.is_group) AS json_build_object_183 \n"
            "FROM zaaktype_regel \n"
            "WHERE zaaktype_regel.zaaktype_node_id = %(zaaktype_node_id_3)s AND zaaktype_regel.zaak_status_id = zaaktype_status.id))) AS json_build_object_46 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id AND zaaktype_node.id = %(id_5)s ORDER BY zaaktype_status.status)), %(json_build_object_190)s, json_build_object(%(json_build_object_191)s, array((SELECT zaaktype_betrokkenen.betrokkene_type \n"
            "FROM zaaktype_betrokkenen \n"
            "WHERE zaaktype_betrokkenen.zaaktype_node_id = zaaktype_node.id)), %(json_build_object_192)s, CAST(coalesce(zaaktype_node.adres_aanvrager, %(coalesce_23)s) = %(coalesce_24)s AS BOOLEAN)), %(json_build_object_193)s, CASE WHEN (zaaktype_definitie.preset_client IS NULL OR zaaktype_definitie.preset_client = %(preset_client_1)s) THEN NULL ELSE (SELECT jsonb_build_object(%(jsonb_build_object_2)s, %(jsonb_build_object_3)s, %(jsonb_build_object_4)s, natuurlijk_persoon.uuid, %(jsonb_build_object_5)s, get_display_name_for_person(hstore(ARRAY[%(param_20)s, %(param_21)s, %(param_22)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam]))) AS jsonb_build_object_1 \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = get_subject_by_legacy_id(zaaktype_definitie.preset_client) UNION ALL SELECT jsonb_build_object(%(jsonb_build_object_7)s, %(jsonb_build_object_8)s, %(jsonb_build_object_9)s, bedrijf.uuid, %(jsonb_build_object_10)s, get_display_name_for_company(hstore(ARRAY[%(param_23)s], ARRAY[bedrijf.handelsnaam]))) AS jsonb_build_object_6 \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = get_subject_by_legacy_id(zaaktype_definitie.preset_client) \n"
            " LIMIT %(param_24)s) END, %(json_build_object_194)s, CASE WHEN ((CAST(zaaktype_node.properties AS JSON) ->> %(param_25)s) = %(param_26)s OR (CAST(zaaktype_node.properties AS JSON) ->> %(param_25)s) IS NULL) THEN CAST(%(param_27)s::JSON AS JSON) ELSE CAST((SELECT json_build_object(%(json_build_object_196)s, subject.uuid, %(json_build_object_197)s, subject.username) AS json_build_object_195 \n"
            "FROM subject \n"
            "WHERE subject.id = CAST(SUBSTRING(CAST(zaaktype_node.properties AS JSON) ->> %(param_25)s FROM %(substring_1)s) AS INTEGER)) AS JSON) END, %(json_build_object_198)s, array((SELECT json_build_object(%(json_build_object_200)s, zaaktype_resultaten.uuid, %(json_build_object_201)s, coalesce(zaaktype_resultaten.label, zaaktype_resultaten.resultaat), %(json_build_object_202)s, zaaktype_resultaten.resultaat, %(json_build_object_203)s, zaaktype_resultaten.trigger_archival, %(json_build_object_204)s, result_preservation_terms.label, %(json_build_object_205)s, result_preservation_terms.unit, %(json_build_object_206)s, result_preservation_terms.unit_amount) AS json_build_object_199 \n"
            "FROM zaaktype_resultaten JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten.bewaartermijn \n"
            "WHERE zaaktype_resultaten.zaaktype_node_id = zaaktype_node.id ORDER BY zaaktype_resultaten.id))) AS json_build_object_1 \n"
            "FROM zaaktype_node, zaaktype, zaaktype_definitie, bibliotheek_categorie \n"
            "WHERE zaaktype_node.id = %(id_6)s AND zaaktype.id = zaaktype_node.zaaktype_id AND zaaktype.deleted IS NULL AND zaaktype_definitie.id = zaaktype_node.zaaktype_definitie_id AND bibliotheek_categorie.id = zaaktype.bibliotheek_categorie_id"
        )

        # test if result update query is performed correct
        compiled_update_resultaat = db_execute_calls[24][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_update_resultaat) == (
            "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=(SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s::UUID) WHERE zaak.uuid = %(uuid_2)s::UUID"
        )
        assert compiled_update_resultaat.params["resultaat"] == "afgebroken"
        assert (
            compiled_update_resultaat.params["uuid_1"]
            == mock_case_type["results"][1]["uuid"]
        )
        assert compiled_update_resultaat.params["uuid_2"] == case_uuid

        # test no result value in settings
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "result",
                        "anders": "2",
                        # "actie_1_value": "1",  # test for this value not available
                        "actie_1_kenmerk": "case_result",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

        # test invalid result value in settings
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "result",
                        "anders": "2",
                        "actie_1_value": "not a numeric value",  # test for this value not numeric
                        "actie_1_kenmerk": "case_result",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

        # test invalid result value index is too high (it should be in the range of the results in the casetype node)
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": "set_value",
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "result",
                        "anders": "2",
                        "actie_1_value": "999",
                        "actie_1_kenmerk": "case_result",
                        "actie_1_can_change": "0",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_set_registrationdate_ruleaction(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 334,
                "name": "Date field for registration date",
                "uuid": field_type_uuids[1],
                "field_magic_string": "date_field_magicstr",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "2",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "active": "1",
                        "actie_1": "set_value",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "actie_1_attribute_type": "valuta",
                        "actie_1_value": "230.0",
                        "actie_1_kenmerk": "333",
                        "actie_1_can_change": "0",
                        "actie_2": "wijzig_registratiedatum",
                        "actie_2_recalculate": "on",
                        "actie_2_datum_bibliotheek_kenmerken_id": "334",
                        "actie_2_attribute_type": "none",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "id value_type")(
                id=1, value_type="numeric"
            ),
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        date_now = datetime.now()
        streefafhandeldatum = date_now + timedelta(days=234)
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={
                "test_subject": ["test"],
                "date_field_magicstr": [date_now.strftime("%Y-%m-%d")],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        compiled_update = db_execute_calls[26][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET last_modified=now(), registratiedatum=%(registratiedatum)s, streefafhandeldatum=%(streefafhandeldatum)s, onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert compiled_update.params["registratiedatum"] == date_now.strftime(
            "%Y-%m-%d"
        )
        assert compiled_update.params[
            "streefafhandeldatum"
        ] == streefafhandeldatum.strftime(
            "%Y-%m-%d"
        )  # 234 calander days (based on term_legal)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_set_registrationdate_no_recalculate_target_competion_dateruleaction(
        self, mock_ids
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 334,
                "name": "Date field for registration date",
                "uuid": field_type_uuids[1],
                "field_magic_string": "date_field_magicstr",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "2",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "active": "1",
                        "actie_1": "set_value",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "actie_1_attribute_type": "valuta",
                        "actie_1_value": "230.0",
                        "actie_1_kenmerk": "333",
                        "actie_1_can_change": "0",
                        "actie_2": "wijzig_registratiedatum",
                        "actie_2_recalculate": "off",  # this is the prop that's tested
                        "actie_2_datum_bibliotheek_kenmerken_id": "334",
                        "actie_2_attribute_type": "none",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "id value_type")(
                id=1, value_type="numeric"
            ),
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={
                "test_subject": ["test"],
                "date_field_magicstr": ["2022-07-29"],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        compiled_update = db_execute_calls[26][0][0].compile(
            dialect=postgresql.dialect()
        )
        # assert streefafhandeldatum isn't updated now, but registration date is
        assert str(compiled_update) == (
            "UPDATE zaak SET last_modified=now(), registratiedatum=%(registratiedatum)s, onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_update.params["registratiedatum"] == "2022-07-29"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_set_registrationdate_invalid_date_kenmerk(
        self, mock_ids
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 334,
                "name": "Date field for registration date",
                "uuid": field_type_uuids[1],
                "field_magic_string": "date_field_magicstr",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "2",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "active": "1",
                        "actie_1": "set_value",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "actie_1_attribute_type": "valuta",
                        "actie_1_value": "230.0",
                        "actie_1_kenmerk": "333",
                        "actie_1_can_change": "0",
                        "actie_2": "wijzig_registratiedatum",
                        "actie_2_recalculate": "on",
                        "actie_2_datum_bibliotheek_kenmerken_id": "11111111",  # this is the prop that's tested
                        "actie_2_attribute_type": "none",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "id value_type")(
                id=1, value_type="numeric"
            ),
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={
                "test_subject": ["test"],
                "date_field_magicstr": ["2022-07-29"],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        # verify the case is created, even with an invalid kenmerkref in ruleaction
        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    # Date calulation test all have "2022-01-29" as the start date.
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    @pytest.mark.parametrize(
        "day_amount, day_action, weeks_amount, weeks_action, month_amount, month_action, year_amount, year_action, expected_date",
        [
            (0, "add", 0, "add", 1, "add", 0, "add", ["2022-02-28"]),
            (0, "add", 0, "add", 2, "add", 0, "add", ["2022-03-29"]),
            (5, "add", 0, "add", 2, "add", 0, "add", ["2022-04-03"]),
            (0, "add", 0, "add", 12, "add", 0, "add", ["2023-01-29"]),
            (0, "add", 1, "subtract", 0, "add", 0, "add", ["2022-01-22"]),
            (5, "subtract", 0, "add", 0, "add", 0, "add", ["2022-01-24"]),
            (-1, "subtract", 0, "add", 0, "add", 0, "add", ["2022-01-30"]),
            (0, "add", 0, "add", -1, "add", 0, "add", ["2021-12-29"]),
            (0, "subtract", 1, "subtract", 0, "add", 0, "add", ["2022-01-22"]),
            (
                5,
                "subtract",
                1,
                "subtract",
                0,
                "add",
                0,
                "subtract",
                ["2022-01-17"],
            ),
            (
                5,
                "subtract",
                1,
                "subtract",
                3,
                "add",
                0,
                "subtract",
                ["2022-04-17"],
            ),
            (
                5,
                "subtract",
                1,
                "subtract",
                3,
                "add",
                1,
                "subtract",
                ["2021-04-17"],
            ),
        ],
    )
    def test_create_case_with_calculate_ruleaction(
        self,
        mock_ids,
        day_amount,
        day_action,
        weeks_amount,
        weeks_action,
        month_amount,
        month_action,
        year_amount,
        year_action,
        expected_date,
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 334,
                "name": "Date field source calculations",
                "uuid": field_type_uuids[1],
                "field_magic_string": "date_field_magicstr_src",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 335,
                "name": "Date field destination calculations",
                "uuid": field_type_uuids[1],
                "field_magic_string": "date_field_magicstr_dst",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "2",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "active": "1",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "actie_1_weeks_action": weeks_action,
                        "actie_1_attribute_type": "none",
                        "actie_1_src_attr_id": "334",
                        "actie_1": "date_calculations",
                        "actie_1_weeks_amount": weeks_amount,
                        "actie_1_years_action": year_action,
                        "actie_1_days_action": day_action,
                        "actie_1_months_action": month_action,
                        "actie_1_months_amount": month_amount,
                        "actie_1_dst_attr_id": "335",
                        "actie_1_days_amount": day_amount,
                        "actie_1_years_amount": year_amount,
                        "actie_2_weeks_action": weeks_action,
                        "actie_2_attribute_type": "none",
                        "actie_2_src_attr_id": "334",
                        "actie_2": "date_calculations",
                        "actie_2_weeks_amount": weeks_amount,
                        "actie_2_years_action": year_action,
                        "actie_2_days_action": day_action,
                        "actie_2_months_action": month_action,
                        "actie_2_months_amount": month_amount,
                        "actie_2_dst_attr_id": "335",
                        "actie_2_days_amount": day_amount,
                        "actie_2_years_amount": year_amount,
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "id value_type")(
                id=1, value_type="numeric"
            ),
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={
                "test_subject": ["test"],
                "date_field_magicstr_src": ["2022-01-29"],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        compiled_update = db_execute_calls[25][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, value, magic_string) VALUES (%(zaak_id)s, %(bibliotheek_kenmerken_id)s, %(value)s::TEXT[], %(magic_string)s) ON CONFLICT (zaak_id, bibliotheek_kenmerken_id) DO UPDATE SET value = %(param_1)s::TEXT[] RETURNING zaak_kenmerk.id"
        )

        assert compiled_update.params["value"] == expected_date
        assert (
            compiled_update.params["magic_string"] == "date_field_magicstr_dst"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_calculate_ruleaction_letters(
        self,
        mock_ids,
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 334,
                "name": "Date field source calculations",
                "uuid": field_type_uuids[1],
                "field_magic_string": "date_field_magicstr_src",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
            {
                "id": 335,
                "name": "Date field destination calculations",
                "uuid": field_type_uuids[2],
                "field_magic_string": "date_field_magicstr_dst",
                "field_type": "date",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
                "value_type": "numeric",
            },
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "2",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "active": "1",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "actie_1_weeks_action": "add",
                        "actie_1_attribute_type": "none",
                        "actie_1_src_attr_id": "334",
                        "actie_1": "date_calculations",
                        "actie_1_weeks_amount": "1",
                        "actie_1_years_action": "add",
                        "actie_1_days_action": "add",
                        "actie_1_months_action": "add",
                        "actie_1_months_amount": "1",
                        "actie_1_dst_attr_id": "335",
                        "actie_1_days_amount": "a",
                        "actie_1_years_amount": "1",
                        "actie_2_weeks_action": "add",
                        "actie_2_attribute_type": "none",
                        "actie_2_src_attr_id": "334",
                        "actie_2": "date_calculations",
                        "actie_2_weeks_amount": "1",
                        "actie_2_years_action": "add",
                        "actie_2_days_action": "add",
                        "actie_2_months_action": "1",
                        "actie_2_months_amount": "add",
                        "actie_2_dst_attr_id": "335",
                        "actie_2_days_amount": "a",
                        "actie_2_years_amount": "a",
                    }
                ),
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            [mock_subject],
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={
                "test_subject": ["test"],
                "date_field_magicstr_src": ["2022-01-29"],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        compiled_update = db_execute_calls[25][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak_betrokkenen SET deleted=now() FROM zaak WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak_betrokkenen.id = zaak.behandelaar"
        )

        assert compiled_update.params["uuid_1"] == case_uuid

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_errors(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "id": 333,
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"mijlsort":"6","condition_type":"and","active":"1","actie_1_attribute_type":"none","actie_1":"change_confidentiality","acties":"1","naam":"payment status rule","voorwaarde_1_value":"offline","voorwaarde_1_value_checkbox":"1","bibliotheek_kenmerken_id":null,"actie_1_value":"public","properties":{},"voorwaarden":"1","voorwaarde_1_kenmerk":"payment_status","is_group":0}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        with pytest.raises(NotImplementedError) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="post",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": False,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )

        assert excinfo.value.args == (
            "Condition 'payment_status' is not implemented in v2 rule_engine",
            "rule_engine/condition_not_implemented",
        )

        rules = [
            {
                "name": "Preset Client Rule",
                "created": "2022-05-11T07:57:23.045075",
                "last_modified": "2022-05-11T07:57:23.045075",
                "settings": '{"actie_1":"change_confidentiality","actie_1_attribute_type":"none","acties":"1","condition_type":"all","active":"1","mijlsort":"7","actie_1_value":"public","is_group":0,"voorwaarde_1_kenmerk":"preset_client","properties":{},"voorwaarden":"1","naam":"Preset Client Rule","bibliotheek_kenmerken_id":null,"voorwaarde_1_value":"Ja","voorwaarde_1_value_checkbox":"1"}',
                "active": True,
                "is_group": False,
            },
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        with pytest.raises(NotFound) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="behandelaar",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": False,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )

        assert excinfo.value.args == (
            "Condition type all condition is not allowed in v2 rule_engine",
            "rule_engine/condition_type_not_allowed",
        )

        # test for raising a NotImplementedError when a action is not implemented
        un_implemented_action = "not_implementend_action"
        rules = [
            {
                "name": "Preset Client Rule",
                "created": "2022-05-11T07:57:23.045075",
                "last_modified": "2022-05-11T07:57:23.045075",
                "settings": json.dumps(
                    {
                        "voorwaarde_1_value_checkbox": "1",
                        "condition_type": "and",
                        "acties": "1",
                        "is_group": 0,
                        "properties": {
                            "map_wms_feature_attribute_label": None,
                            "map_case_location": None,
                            "text_content": None,
                            "relationship_subject_role": None,
                            "map_wms_feature_attribute_id": None,
                            "map_wms_layer_id": None,
                            "skip_change_approval": None,
                            "show_on_map": None,
                        },
                        "voorwaarde_1_kenmerk": "contactchannel",
                        "bibliotheek_kenmerken_id": None,
                        "mijlsort": "1",
                        "ander_2_value": "public",
                        "active": "1",
                        "actie_1": un_implemented_action,
                        "ander_2": "change_confidentiality",
                        "voorwaarden": "1",
                        "voorwaarde_1_value": "behandelaar",
                        "naam": "condition contact channel",
                        "ander_2_attribute_type": "none",
                        "actie_1_attribute_type": "none",
                        "anders": "2",
                    }
                ),
                "active": True,
                "is_group": False,
            },
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        with pytest.raises(NotImplementedError) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="behandelaar",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": False,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            f"Action '{un_implemented_action}' is not implemented in v2 rule_engine",
            "rule_engine/action_not_implemented",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_case_summaries_containing_magic_strings(
        self, mock_ids
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["case_summary"] = (
            "Case is [[j('$.attributes.status')]] with confidentiality '[[j('$.attributes.confidentiality')]]' and case_type is '[[j('$.relationships.case_type.name')]]'"
        )
        mock_case_type["case_public_summary"] = (
            "Value of 'test_subject' using j style magic string is '[[j('$.attributes.custom_fields.test_subject.value')]]' and with legacy magic string is '[[test_subject]]'"
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="internal",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert (
            insert_case_stmt.params["onderwerp"]
            == "Case is new with confidentiality 'internal' and case_type is 'test_case_type '"
        )
        assert (
            insert_case_stmt.params["onderwerp_extern"]
            == "Value of 'test_subject' using j style magic string is 'test' and with legacy magic string is 'test'"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_case_summaries_containing_user_name_magic_string(
        self, mock_ids
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["case_summary"] = "Case is created by '[[user.name]]'"
        mock_case_type["case_public_summary"] = (
            "The username is '[[user.name]]'"
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        user_name_subject_dict = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "settings": {},
            "username": "Ferry Brak",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "department_name": "a department name",
            "subject_type": "medewerker",
            "properties": {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            },
            "employee_address": None,
            "Group": [],
        }
        user_name_subject_mock = mock.MagicMock()
        user_name_subject_mock.configure_mock(**user_name_subject_dict)
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,  # is_simple_case_type
            [mock_subject],  # get_contact_employee
            # Department for generate_entity_from_employee
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            user_name_subject_mock,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="internal",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert (
            insert_case_stmt.params["onderwerp"]
            == "Case is created by 'Ferry Brak'"
        )
        assert (
            insert_case_stmt.params["onderwerp_extern"]
            == "The username is 'Ferry Brak'"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_case_summaries_containing_user_name_not_found_magic_string(
        self, mock_ids
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["case_summary"] = "Case is created by '[[user.name]]'"
        mock_case_type["case_public_summary"] = (
            "The username is '[[user.name]]'"
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            None,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="internal",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert insert_case_stmt.params["onderwerp"] == "Case is created by ''"
        assert (
            insert_case_stmt.params["onderwerp_extern"] == "The username is ''"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_empty_case_summaries(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        (
            mock_case_type["case_summary"],
            mock_case_type["case_public_summary"],
        ) = (None, None)

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.query().filter().one.side_effect = [mock_subject]
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="internal",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[14][0][0].compile()
        assert insert_case_stmt.params["onderwerp"] == ""
        assert insert_case_stmt.params["onderwerp_extern"] == ""

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_department(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.MagicMock()
        department_uuid = uuid4()
        assignee_uuid = str(uuid4())
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [self.__mock_casemeta_current_deadline()],
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("case_id", "id")(id=247),
            namedtuple("group_id", "id")(id=1),
        ]

        case_uuid = str(uuid4())
        role_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="email",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "role": {"id": str(role_uuid), "type": "role"},
                "department": {
                    "id": str(department_uuid),
                    "type": "department",
                },
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query) == "UPDATE zaak SET route_ou=(SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_1), route_role=(SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2), last_modified=:last_modified, onderwerp=:onderwerp, onderwerp_extern=:onderwerp_extern WHERE zaak.uuid = :uuid_3"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_conflict_not_active(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["active"] = False
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
        ]

        case_uuid = str(uuid4())
        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )

        assert excinfo.value.args == (
            "Case type is not active.",
            "case_type/not_active",
        )

    def test_set_registration_date(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2019-01-31"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "id": 10,
                    "uuid": uuid4(),
                    "is_required": False,
                    "field_magic_string": "basic_date",
                    "field_type": "date",
                    "label": "Basic date",
                    "field_options": [],
                    "name": "Basic date",
                    "date_field_limit": CustomFieldDateLimit(
                        start=None, end=None
                    ),
                },
            ],
        )
        mock_case_type["case_summary"] = "Dat is  '[[basic_date | isodate]]'"
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        department_uuid = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )

        mock_requestor = mock.MagicMock()
        mock_requestor = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": None,
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": True,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": 1,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }

        self.session.query().filter().one.side_effect = [
            uuid4(),
            namedtuple("Department", "department")(department="Development"),
        ]

        subject_id = str(uuid4())

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": [
                    {
                        "betrokkene_type": "medewerker",
                        "rol": "advocaat",
                        "subject_id": subject_id,
                    }
                ],
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2020, 1, 1),
                    "last_modified": date(2019, 3, 30),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "basic_date",
                        "magic_string": "basic_date",
                        "type": "date",
                        "value": ["2022-12-07"],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": [
                    {
                        "name": "some name",
                        "magic_string": "magicstring2",
                        "type": "file",
                        "is_multiple": False,
                        "value": [
                            {
                                "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                                "size": 3,
                                "uuid": uuid4(),
                                "filename": "hello.txt",
                                "mimetype": "text/plain",
                                "is_archivable": True,
                                "original_name": "hello.txt",
                                "thumbnail_uuid": uuid4(),
                            },
                        ],
                    }
                ],
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "manage"],
            }
        )

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()
        self.cmd.set_case_registration_date(
            case_uuid=case_uuid, target_date=target_date
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 19

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

        update_statement = call_list[18][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_update) == (
            "UPDATE zaak SET last_modified=now(), registratiedatum=%(registratiedatum)s, onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_update.params["registratiedatum"] == "2019-01-31"

    def test_set_registration_date_conflict(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2020-01-31"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        self.session.query().filter().one.side_effect = [uuid4()]

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 10),
                "streefafhandeldatum": date(2020, 1, 5),
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2020, 1, 1),
                    "last_modified": date(2019, 3, 30),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = mock.MagicMock()
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_subject = mock.Mock()

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee]],
            [mock_subject],
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        with pytest.raises(Conflict) as excinfo:
            self.cmd.set_case_registration_date(
                case_uuid=case_uuid, target_date=target_date
            )

        assert excinfo.value.args == (
            "Not allowed to set 'registration_date' after 'target_completion_date'.",
        )

    def test_set_registration_date_conflict_target_completion_date(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2020-10-20"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        self.session.query().filter().one.side_effect = [uuid4()]
        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": date(2019, 1, 31),
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 10),
                "streefafhandeldatum": date(2019, 1, 5),
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2020, 1, 1),
                    "last_modified": date(2019, 3, 30),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        mock_requestor = mock.MagicMock()
        mock_assignee = mock.MagicMock()
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee]],
            [mock.MagicMock()],
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.set_case_registration_date(
                case_uuid=case_uuid, target_date=target_date
            )

        assert excinfo.value.args == (
            "Not allowed to set 'registration_date' after 'completion_date'.",
        )

    def test_assign_case_to_user_not_found(self):
        case_uuid = str(uuid4())
        user_uuid = str(uuid4())
        self.session.execute().fetchone.side_effect = [None]
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.assign_case_to_user(
                case_uuid=case_uuid, user_uuid=user_uuid
            )

        assert excinfo.value.args == (
            f"Employee with uuid '{user_uuid}' not found.",
            "employee/not_found",
        )

    def test_set_case_target_completion_date(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2019-01-31"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        self.session.query().filter().one.side_effect = [uuid4()]

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": date(2020, 1, 5),
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2020, 1, 1),
                    "last_modified": date(2019, 3, 30),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        mock_requestor = mock.MagicMock()
        mock_assignee = mock.MagicMock()

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee]],
            [mock.MagicMock()],
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]

        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.cmd.set_case_registration_date(
            case_uuid=case_uuid, target_date=target_date
        )

        assert mock_case.registratiedatum == date(2020, 1, 1)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 14

        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Function"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"

    def test_enqueue_case_assignee_email(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = mock.MagicMock()
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        self.session.query().filter().one.side_effect = [uuid4()]

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )
        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": date(2020, 1, 5),
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2020, 1, 1),
                    "last_modified": date(2019, 3, 30),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()
        self.cmd.enqueue_case_assignee_email(case_uuid=case_uuid)
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 12

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"
        insert_statement = call_list[11][0][0]
        compiled_select = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s::UUID, %(object_id)s::UUID, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s::UUID, %(priority)s, %(metadata)s)"
        )

    def test_reassign_case_messages(self):
        case_uuid = str(uuid4())
        case_id = 1
        subject_uuid = str(uuid4())

        mock_message = mock.Mock()
        mock_message.configure_mock(
            case_id=case_id,
            case_uuid=case_uuid,
            user=None,
        )

        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(uuid4())
        mock_subject.configure_mock(
            id=1,
            uuid=assignee_uuid,
            subject_type="employee",
            properties={},
            settings={},
            username="Subject",
            last_modified=None,
            role_ids=[],
            group_ids=[],
            nobody=False,
            system=True,
            department_uuid=department_uuid,
            department_name="department",
        )
        self.session.execute().fetchone.side_effect = [
            mock_subject,
            mock_message,
        ]
        self.session.reset_mock()

        self.cmd.reassign_case_messages(
            case_uuid=case_uuid, subject_uuid=subject_uuid
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT subject.uuid, subject.id, subject.subject_type, subject.properties, subject.settings, subject.username, subject.last_modified, subject.role_ids, subject.group_ids, subject.nobody, subject.system, groups.uuid AS department_uuid, groups.name AS department_name \n"
            "FROM subject, groups \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID AND groups.id = subject.group_ids[%(group_ids_1)s]"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE message SET subject_id=concat(%(concat_1)s, (SELECT id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) WHERE message.logging_id IN (SELECT logging.id \n"
            "FROM logging \n"
            "WHERE logging.zaak_id = %(zaak_id_1)s)"
        )

    def test_pause_case(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        department_uuid = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )

        mock_requestor = mock.MagicMock()
        mock_requestor = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": None,
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": True,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": 1,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }

        self.session.query().filter().one.side_effect = [
            uuid4(),
            namedtuple("Department", "department")(department="Development"),
        ]

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "open",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "extension_reason": None,
                "suspension_reason": None,
                "related_to": "{}",
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2020, 1, 1),
                    "last_modified": date(2019, 3, 30),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": date(2020, 1, 1),
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": [
                    {
                        "name": "some name",
                        "magic_string": "magicstring2",
                        "type": "file",
                        "value": [
                            {
                                "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                                "size": 3,
                                "uuid": uuid4(),
                                "filename": "hello.txt",
                                "mimetype": "text/plain",
                                "is_archivable": True,
                                "original_name": "hello.txt",
                                "thumbnail_uuid": uuid4(),
                            },
                        ],
                    }
                ],
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.pause_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            suspension_reason="suspension",
            suspension_term_value=6,
            suspension_term_type="weeks",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 21

        update_statement = call_list[19][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET status=%(status)s, last_modified=now(), stalled_until=%(stalled_until)s, onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_update.params["status"] == "stalled"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_clear_destruction_date(self, mock_find_case, test_case):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = date(2022, 3, 1)
        case.status = "resolved"
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_destruction_date(
            case_uuid=case_uuid,
            reason="reason for destruction",
            new_destruction_date={"type": "clear"},
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        update_statement = call_list[0][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET last_modified=now(), vernietigingsdatum=%(vernietigingsdatum)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert case.destruction_date is None

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_destruction_date(self, mock_find_case, test_case):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = date(2022, 3, 1)
        case.status = "resolved"
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_destruction_date(
            case_uuid=case_uuid,
            reason="reason for destruction",
            new_destruction_date={
                "type": "set",
                "destruction_date": "2024-07-22",
            },
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        update_statement = call_list[0][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET last_modified=now(), vernietigingsdatum=%(vernietigingsdatum)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert case.destruction_date == date(2024, 7, 22)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_recalculate_destruction_date(self, mock_find_case, test_case):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = None
        case.status = "resolved"
        case.completion_date = date(2022, 3, 1)
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_destruction_date(
            case_uuid=case_uuid,
            reason="reason for destruction",
            new_destruction_date={
                "type": "recalculate",
                "period": "28",
            },
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        update_statement = call_list[2][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert case.destruction_date == date(2022, 3, 29)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_recalculate_destruction_date_by_month(
        self, mock_find_case, test_case
    ):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = None
        case.status = "resolved"
        case.completion_date = date(2022, 3, 1)
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="4 weken",
            preservation_term_unit="month",
            preservation_term_unit_amount=6,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_destruction_date(
            case_uuid=case_uuid,
            reason="reason for destruction",
            new_destruction_date={
                "type": "recalculate",
                "period": "186",
            },
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        update_statement = call_list[2][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert case.destruction_date == date(2022, 9, 1)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_recalculate_destruction_date_by_year(
        self, mock_find_case, test_case
    ):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = None
        case.status = "resolved"
        case.completion_date = date(2022, 3, 1)
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="1 jaar",
            preservation_term_unit="year",
            preservation_term_unit_amount=1,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_destruction_date(
            case_uuid=case_uuid,
            reason="reason for destruction",
            new_destruction_date={
                "type": "recalculate",
                "period": "365",
            },
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        update_statement = call_list[2][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert case.destruction_date == date(2023, 3, 1)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_recalculate_destruction_date_bewaren(
        self, mock_find_case, test_case
    ):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = None
        case.status = "resolved"
        case.completion_date = date(2022, 3, 1)
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="Bewaren",
            preservation_term_unit="day",
            preservation_term_unit_amount=99999,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_destruction_date(
            case_uuid=case_uuid,
            reason="reason for destruction",
            new_destruction_date={
                "type": "recalculate",
                "period": "99999",
            },
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        update_statement = call_list[2][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert case.destruction_date is None

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_recalculate_destruction_date_invalid_period(
        self, mock_find_case, test_case
    ):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        code = 12
        case.destruction_date = None
        case.status = "resolved"
        case.completion_date = date(2022, 3, 1)
        mock_find_case.return_value = case

        self.session.execute().fetchone.return_value = None
        self.session.reset_mock()
        with pytest.raises(NotFound) as excinfo:
            self.cmd.set_destruction_date(
                case_uuid=case_uuid,
                reason="reason for destruction",
                new_destruction_date={
                    "type": "recalculate",
                    "period": "12",
                },
            )
        assert excinfo.value.args == (
            f"Time period information for code {code} not found",
            "time_period_info/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_clear_destruction_date_invalid_case_status(
        self, mock_find_case, test_case
    ):
        case_uuid = uuid4()
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = date(2022, 3, 1)
        mock_find_case.return_value = case
        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.set_destruction_date(
                case_uuid=case_uuid,
                reason="reason for destruction",
                new_destruction_date={"type": "clear"},
            )
        assert excinfo.value.args == (
            "Destruction date can only be updated for resolved cases.",
            "case/change_destruction_date/not_allowed",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_subject(self, mock_find_case, test_case):
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        custom_fields = [
            {
                "uuid": uuid4(),
                "id": 123,
                "name": "custom_field_1",
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "is_multiple": False,
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["version"] = mock.MagicMock()
        mock_case_type["requestor"] = mock.MagicMock()
        mock_case_type["case_type_results"] = []
        mock_case_type["metadata"] = {
            "process_description": "",
            "legal_basis": "",
        }
        mock_case_type["settings"] = {
            "allow_reuse_casedata": True,
            "enable_webform": True,
            "enable_online_payment": True,
            "enable_manual_payment": None,
            "require_email_on_webform": False,
            "require_phonenumber_on_webform": False,
            "require_mobilenumber_on_webform": False,
            "disable_captcha_for_predefined_requestor": None,
            "show_confidentiality": True,
            "show_contact_info": True,
            "enable_subject_relations_on_form": True,
            "open_case_on_create": True,
            "enable_allocation_on_form": True,
            "disable_pip_for_requestor": False,
            "list_of_default_folders": [],
            "lock_registration_phase": None,
            "enable_queue_for_changes_from_other_subjects": None,
            "check_acl_on_allocation": None,
            "api_can_transition": None,
            "is_public": True,
            "text_confirmation_message_title": 'Uw"zaak"is"geregistreerd',
            "text_public_confirmation_message": 'Bedankt"voor"het"[[case.casetype.initiator_type]]"van"een"<strong>[[case.casetype.name]]</strong>."Uw"registratie"is"bij"ons"bekend"onder"<strong>zaaknummer"[[case.number]]</strong>."Wij"verzoeken"u"om"bij"verdere"communicatie"dit"zaaknummer"te"gebruiken."De"behandeling"van"deze"zaak"zal"spoedig"plaatsvinden.',
            "payment": CaseTypePayment(
                assignee=CaseTypePaymentDict(amount="500"),
                frontdesk=CaseTypePaymentDict(amount="200"),
                phone=CaseTypePaymentDict(amount="300"),
                mail=CaseTypePaymentDict(amount="400"),
                webform=CaseTypePaymentDict(amount="150"),
                post=CaseTypePaymentDict(amount="600"),
            ),
        }

        case.destruction_date = None
        case.status = "open"
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type

        case.related_contacts = [
            CaseContactEmployee(
                entity_type="employee",
                entity_id=UUID("fa81a55c-c590-47f5-a89a-2337a52aab52"),
                entity_meta_summary=None,
                entity_relationships=[],
                entity_meta__fields=[
                    "entity_meta_summary",
                    "magic_string_prefix",
                    "role",
                ],
                entity_id__fields=["uuid"],
                entity_changelog=[],
                entity_data={},
                id=random.randint(1, 1000),
                uuid=str(uuid4()),
                magic_string_prefix="advocaat",
                role="Advocaat",
            )
        ]
        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case
        self.session.reset_mock()

        self.cmd.synchronize_subject(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        update_statement = call_list[0][0][0]

        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET last_modified=now(), onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_delete_case(self, mock_find_case, test_case):
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        custom_fields = [
            {
                "uuid": uuid4(),
                "id": 123,
                "name": "custom_field_1",
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "is_multiple": False,
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["version"] = mock.MagicMock()
        mock_case_type["requestor"] = mock.MagicMock()
        mock_case_type["case_type_results"] = []
        mock_case_type["metadata"] = {
            "process_description": "",
            "legal_basis": "",
        }
        mock_case_type["settings"] = {
            "allow_reuse_casedata": True,
            "enable_webform": True,
            "enable_online_payment": True,
            "enable_manual_payment": None,
            "require_email_on_webform": False,
            "require_phonenumber_on_webform": False,
            "require_mobilenumber_on_webform": False,
            "disable_captcha_for_predefined_requestor": None,
            "show_confidentiality": True,
            "show_contact_info": True,
            "enable_subject_relations_on_form": True,
            "open_case_on_create": True,
            "enable_allocation_on_form": True,
            "disable_pip_for_requestor": False,
            "list_of_default_folders": [],
            "lock_registration_phase": None,
            "enable_queue_for_changes_from_other_subjects": None,
            "check_acl_on_allocation": None,
            "api_can_transition": None,
            "is_public": True,
            "text_confirmation_message_title": 'Uw"zaak"is"geregistreerd',
            "text_public_confirmation_message": 'Bedankt"voor"het"[[case.casetype.initiator_type]]"van"een"<strong>[[case.casetype.name]]</strong>."Uw"registratie"is"bij"ons"bekend"onder"<strong>zaaknummer"[[case.number]]</strong>."Wij"verzoeken"u"om"bij"verdere"communicatie"dit"zaaknummer"te"gebruiken."De"behandeling"van"deze"zaak"zal"spoedig"plaatsvinden.',
            "payment": CaseTypePayment(
                assignee=CaseTypePaymentDict(amount="500"),
                frontdesk=CaseTypePaymentDict(amount="200"),
                phone=CaseTypePaymentDict(amount="300"),
                mail=CaseTypePaymentDict(amount="400"),
                webform=CaseTypePaymentDict(amount="150"),
                post=CaseTypePaymentDict(amount="600"),
            ),
        }

        case.destruction_date = None
        case.status = "open"
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type

        case.related_contacts = [
            CaseContactEmployee(
                entity_type="employee",
                entity_id=UUID("fa81a55c-c590-47f5-a89a-2337a52aab52"),
                entity_meta_summary=None,
                entity_relationships=[],
                entity_meta__fields=[
                    "entity_meta_summary",
                    "magic_string_prefix",
                    "role",
                ],
                entity_id__fields=["uuid"],
                entity_changelog=[],
                entity_data={},
                id=random.randint(1, 1000),
                uuid=str(uuid4()),
                magic_string_prefix="advocaat",
                role="Advocaat",
            )
        ]
        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.delete_case(case_uuid=case_uuid)

        assert excinfo.value.args == (
            "Only resolved cases can be deleted (but status=open).",
            "case/delete_case/status_not_allowed",
        )

        case.status = "resolved"
        with pytest.raises(Conflict) as excinfo:
            self.cmd.delete_case(case_uuid=case_uuid)

        assert excinfo.value.args == (
            "Only cases with archival status 'vernietigen' can be deleted (but archival status=None).",
            "case/delete_case/archival_status_not_allowed",
        )

        case.archival_state = "vernietigen"
        with pytest.raises(Conflict) as excinfo:
            self.cmd.delete_case(case_uuid=case_uuid)

        assert excinfo.value.args == (
            "Destruction date has not passed",
            "case/delete_case/destruction_date_not_passed",
        )

        case.destruction_date = "2020-01-01"
        self.cmd.delete_case(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 7
        update_statement = call_list[0][0][0]

        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET status=%(status)s, last_modified=now(), deleted=%(deleted)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        delete_relations = call_list[1][0][0]

        compiled_delete_relations = delete_relations.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_delete_relations) == (
            "DELETE FROM case_relation WHERE case_relation.case_id_a = %(case_id_a_1)s OR case_relation.case_id_b = %(case_id_b_1)s"
        )

        delete_messages = call_list[2][0][0]

        compiled_delete_messages = delete_messages.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_delete_messages) == (
            "DELETE FROM message WHERE message.logging_id IN (SELECT logging.id \n"
            "FROM logging \n"
            "WHERE logging.zaak_id = %(zaak_id_1)s)"
        )
        assert compiled_delete_messages.params == {"zaak_id_1": case.number}

        delete_logging = call_list[3][0][0]

        compiled_delete_logging = delete_logging.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_delete_logging) == (
            "DELETE FROM logging WHERE logging.zaak_id = %(zaak_id_1)s"
        )

        delete_cor = call_list[4][0][0]

        compiled_delete_cor = delete_cor.compile(dialect=postgresql.dialect())
        assert str(compiled_delete_cor) == (
            "DELETE FROM custom_object_relationship WHERE custom_object_relationship.related_case_id = %(related_case_id_1)s"
        )

        delete_object_data = call_list[5][0][0]

        compiled_delete_object_data = delete_object_data.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_delete_object_data) == (
            "DELETE FROM object_data WHERE object_data.object_class = %(object_class_1)s AND object_data.object_id = %(object_id_1)s"
        )

        delete_object_data = call_list[6][0][0]

        compiled_delete_object_data = delete_object_data.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_delete_object_data) == (
            "UPDATE zaak SET pid=%(pid)s WHERE zaak.pid = %(pid_1)s"
        )


class TestAs_A_User_I_Want_To_Get_case(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.qry.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, custom_fields, rules=None):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["id"] = "1"
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules or [],
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"
        mock_case_type["results"] = [
            {
                "id": 156,
                "uuid": "a04d7629-ca9a-4246-8e9c-cc38b327e904",
                "name": "Bewaren",
                "result": "behaald",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
            {
                "id": 157,
                "uuid": "604138d9-e14f-4944-812c-12a098a68092",
                "name": "Nog een 2de resultaat",
                "result": "afgebroken",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
        ]

        return mock_case_type

    def test_get_case(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        self.session.query().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_requestor = mock.Mock()
        mock_requestor = {
            "uuid": uuid4(),
            "type": "bedrijf",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": "M",
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": None,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": None,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        correspondence_info = {
            "correspondentie_woonplaats": "Amsterdam",
            "correspondentie_straatnaam": "Overamstel",
            "correspondentie_postcode": "1234ER",
            "correspondentie_adres_buitenland1": None,
            "correspondentie_adres_buitenland2": None,
            "correspondentie_adres_buitenland3": None,
            "correspondentie_landcode": 6030,
            "correspondentie_huisnummer": 50,
            "correspondentie_huisletter": "M",
            "correspondentie_huisnummertoevoeging": "SS",
        }
        org = mock.MagicMock()
        org = {
            "uuid": uuid4(),
            "id": "3",
            "object_type": None,
            "handelsnaam": "test company",
            "email": "test@test.nl",
            "telefoonnummer": "1234456",
            "werkzamepersonen": None,
            "dossiernummer": "1234",
            "subdossiernummer": None,
            "hoofdvestiging_dossiernummer": None,
            "hoofdvestiging_subdossiernummer": None,
            "vorig_dossiernummer": None,
            "vorig_subdossiernummer": None,
            "rechtsvorm": "102",
            "kamernummer": None,
            "faillisement": None,
            "surseance": None,
            "contact_naam": None,
            "contact_aanspreektitel": None,
            "contact_voorvoegsel": None,
            "contact_voorletters": None,
            "contact_geslachtsnaam": None,
            "contact_geslachtsaanduiding": None,
            "vestiging_adres": None,
            "vestiging_straatnaam": None,
            "vestiging_huisnummer": None,
            "vestiging_huisnummertoevoeging": None,
            "vestiging_postcodewoonplaats": None,
            "vestiging_postcode": None,
            "vestiging_woonplaats": None,
            "hoofdactiviteitencode": None,
            "nevenactiviteitencode1": None,
            "nevenactiviteitencode2": None,
            "authenticated": None,
            "authenticatedby": None,
            "fulldossiernummer": None,
            "import_datum": None,
            "deleted_on": None,
            "verblijfsobject_id": None,
            "system_of_record": None,
            "system_of_record_id": None,
            "vestigingsnummer": None,
            "vestiging_huisletter": None,
            "vestiging_adres_buitenland1": None,
            "vestiging_adres_buitenland2": None,
            "vestiging_adres_buitenland3": None,
            "vestiging_landcode": None,
            "correspondence_info": correspondence_info,
            "contact_data": contact_data,
        }

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        case_actions = {
            "status": 1,
            "type": "case",
            "data": json.dumps(
                {
                    "id": 4,
                    "zaaktype_node_id": 6,
                    "zaaktype_status_id": 11,
                    "relatie_zaaktype_id": 2,
                    "relatie_type": "deelzaak",
                    "start_delay": "0",
                    "status": 1,
                    "kopieren_kenmerken": None,
                    "ou_id": 1,
                    "role_id": 12,
                    "automatisch_behandelen": None,
                    "required": "2",
                    "betrokkene_authorized": None,
                    "betrokkene_notify": None,
                    "betrokkene_id": "",
                    "betrokkene_role": None,
                    "betrokkene_role_set": "Advocaat",
                    "betrokkene_prefix": "",
                    "eigenaar_type": "aanvrager",
                    "eigenaar_role": "Advocaat",
                    "eigenaar_id": "",
                    "show_in_pip": False,
                    "pip_label": None,
                    "created": "2019-09-16T13:23:59.745873 +00:00",
                    "last_modified": "2019-09-16T13:23:59.745873 +00:00",
                }
            ),
            "automatic": True,
        }

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2019, 2, 1),
                    "last_modified": date(2020, 1, 1),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": date(2020, 1, 1),
                },
                "case_actions": [case_actions],
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": True,
                "user_is_assignee": True,
                "user_is_coordinator": True,
                "authorizations": ["read"],
            }
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [org],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        result = self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert isinstance(result, entities.Case)
        assert result.entity_meta_authorizations == {"search", "read", "write"}

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 17

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_2)s ELSE %(param_3)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_4)s ELSE %(param_5)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_6)s ELSE %(param_7)s END AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_2)s::UUID AND view_case_v2.status != %(status_1)s AND view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_2)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "array((SELECT json_build_object(%(json_build_object_2)s, zaak_betrokkenen.subject_id, %(json_build_object_3)s, zaak_betrokkenen.betrokkene_type, %(json_build_object_4)s, coalesce(zaak_betrokkenen.rol, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_1)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_2)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_3)s END), %(json_build_object_5)s, coalesce(zaak_betrokkenen.magic_string_prefix, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_4)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_5)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_6)s END)) AS json_build_object_1 \n"
            "FROM zaak JOIN zaak_betrokkenen ON zaak.id = zaak_betrokkenen.zaak_id AND zaak_betrokkenen.deleted IS NULL \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID))"
        )

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT json_build_object(%(json_build_object_2)s, bedrijf.id, %(json_build_object_3)s, bedrijf.uuid, %(json_build_object_4)s, bedrijf.dossiernummer, %(json_build_object_5)s, bedrijf.handelsnaam, %(json_build_object_6)s, bedrijf.rechtsvorm, %(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestigingsnummer, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_11)s, bedrijf.vestiging_postcode, %(json_build_object_12)s, json_build_object(%(json_build_object_13)s, bedrijf.correspondentie_straatnaam, %(json_build_object_14)s, bedrijf.correspondentie_huisnummer, %(json_build_object_15)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_16)s, bedrijf.correspondentie_postcode, %(json_build_object_17)s, bedrijf.correspondentie_woonplaats, %(json_build_object_18)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_19)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_20)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_21)s, bedrijf.correspondentie_adres, %(json_build_object_22)s, bedrijf.correspondentie_postcodewoonplaats, %(json_build_object_23)s, bedrijf.correspondentie_huisletter, %(json_build_object_24)s, bedrijf.correspondentie_landcode), %(json_build_object_25)s, bedrijf.vestiging_huisletter, %(json_build_object_26)s, bedrijf.vestiging_landcode, %(json_build_object_27)s, bedrijf.subdossiernummer, %(json_build_object_28)s, bedrijf.hoofdvestiging_dossiernummer, %(json_build_object_29)s, bedrijf.hoofdvestiging_subdossiernummer, %(json_build_object_30)s, bedrijf.vorig_dossiernummer, %(json_build_object_31)s, bedrijf.vorig_subdossiernummer, %(json_build_object_32)s, bedrijf.kamernummer, %(json_build_object_33)s, bedrijf.faillisement, %(json_build_object_34)s, bedrijf.surseance, %(json_build_object_35)s, bedrijf.telefoonnummer, %(json_build_object_36)s, bedrijf.email, %(json_build_object_37)s, bedrijf.vestiging_adres, %(json_build_object_38)s, bedrijf.vestiging_postcodewoonplaats, %(json_build_object_39)s, bedrijf.vestiging_woonplaats, %(json_build_object_40)s, bedrijf.hoofdactiviteitencode, %(json_build_object_41)s, bedrijf.nevenactiviteitencode1, %(json_build_object_42)s, bedrijf.nevenactiviteitencode2, %(json_build_object_43)s, bedrijf.nevenactiviteitencode2, %(json_build_object_44)s, bedrijf.contact_naam, %(json_build_object_45)s, bedrijf.contact_aanspreektitel, %(json_build_object_46)s, bedrijf.contact_voorletters, %(json_build_object_47)s, bedrijf.contact_voorvoegsel, %(json_build_object_48)s, bedrijf.contact_geslachtsnaam, %(json_build_object_49)s, bedrijf.contact_geslachtsaanduiding, %(json_build_object_50)s, bedrijf.authenticated, %(json_build_object_51)s, bedrijf.authenticatedby, %(json_build_object_52)s, bedrijf.import_datum, %(json_build_object_53)s, bedrijf.verblijfsobject_id, %(json_build_object_54)s, bedrijf.vestigingsnummer, %(json_build_object_55)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_56)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_57)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_58)s, json_build_object(%(json_build_object_59)s, contact_data.mobiel, %(json_build_object_60)s, contact_data.telefoonnummer, %(json_build_object_61)s, contact_data.email, %(json_build_object_62)s, contact_data.note)) AS json_build_object_1 \n"
            "FROM bedrijf LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s \n"
            "WHERE bedrijf.uuid = %(uuid_1)s::UUID"
        )

    def test_get_case_with_address_custom_fields(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        self.session.query().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_requestor = mock.Mock()
        mock_requestor = {
            "uuid": uuid4(),
            "type": "bedrijf",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": "{}",
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": "M",
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": None,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": None,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        correspondence_info = {
            "correspondentie_woonplaats": "Amsterdam",
            "correspondentie_straatnaam": "Overamstel",
            "correspondentie_postcode": "1234ER",
            "correspondentie_adres_buitenland1": None,
            "correspondentie_adres_buitenland2": None,
            "correspondentie_adres_buitenland3": None,
            "correspondentie_landcode": 6030,
            "correspondentie_huisnummer": 50,
            "correspondentie_huisletter": "M",
            "correspondentie_huisnummertoevoeging": "SS",
        }
        org = mock.MagicMock()
        org = {
            "uuid": uuid4(),
            "id": "3",
            "object_type": None,
            "handelsnaam": "test company",
            "email": "test@test.nl",
            "telefoonnummer": "1234456",
            "werkzamepersonen": None,
            "dossiernummer": "1234",
            "subdossiernummer": None,
            "hoofdvestiging_dossiernummer": None,
            "hoofdvestiging_subdossiernummer": None,
            "vorig_dossiernummer": None,
            "vorig_subdossiernummer": None,
            "rechtsvorm": "102",
            "kamernummer": None,
            "faillisement": None,
            "surseance": None,
            "contact_naam": None,
            "contact_aanspreektitel": None,
            "contact_voorvoegsel": None,
            "contact_voorletters": None,
            "contact_geslachtsnaam": None,
            "contact_geslachtsaanduiding": None,
            "vestiging_adres": None,
            "vestiging_straatnaam": None,
            "vestiging_huisnummer": None,
            "vestiging_huisnummertoevoeging": None,
            "vestiging_postcodewoonplaats": None,
            "vestiging_postcode": None,
            "vestiging_woonplaats": None,
            "hoofdactiviteitencode": None,
            "nevenactiviteitencode1": None,
            "nevenactiviteitencode2": None,
            "authenticated": None,
            "authenticatedby": None,
            "fulldossiernummer": None,
            "import_datum": None,
            "deleted_on": None,
            "verblijfsobject_id": None,
            "system_of_record": None,
            "system_of_record_id": None,
            "vestigingsnummer": None,
            "vestiging_huisletter": None,
            "vestiging_adres_buitenland1": None,
            "vestiging_adres_buitenland2": None,
            "vestiging_adres_buitenland3": None,
            "vestiging_landcode": None,
            "correspondence_info": correspondence_info,
            "contact_data": contact_data,
        }

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        case_actions = {
            "status": 1,
            "type": "case",
            "data": json.dumps(
                {
                    "id": 4,
                    "zaaktype_node_id": 6,
                    "zaaktype_status_id": 11,
                    "relatie_zaaktype_id": 2,
                    "relatie_type": "deelzaak",
                    "start_delay": "0",
                    "status": 1,
                    "kopieren_kenmerken": None,
                    "ou_id": 1,
                    "role_id": 12,
                    "automatisch_behandelen": None,
                    "required": "2",
                    "betrokkene_authorized": None,
                    "betrokkene_notify": None,
                    "betrokkene_id": "",
                    "betrokkene_role": None,
                    "betrokkene_role_set": "Advocaat",
                    "betrokkene_prefix": "",
                    "eigenaar_type": "aanvrager",
                    "eigenaar_role": "Advocaat",
                    "eigenaar_id": "",
                    "show_in_pip": False,
                    "pip_label": None,
                    "created": "2019-09-16T13:23:59.745873 +00:00",
                    "last_modified": "2019-09-16T13:23:59.745873 +00:00",
                }
            ),
            "automatic": True,
        }

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": "{}",
                "prefix": "1",
                "route_ou": "1",
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": date(2019, 2, 1),
                    "last_modified": date(2020, 1, 1),
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": date(2020, 1, 1),
                },
                "case_actions": [case_actions],
                "custom_fields": [
                    {
                        "naam": "geo-1",
                        "magic_string": "geo1",
                        "value": [
                            '{"features":[{"properties":{},"geometry":{"coordinates":[4.810304,52.298721],"type":"Point"},"type":"Feature"}],"type":"FeatureCollection"}'
                        ],
                        "type": "geojson",
                        "is_multiple": False,
                    },
                    {
                        "naam": "address v2",
                        "magic_string": "address_v2",
                        "value": [
                            '{"geojson":{"type":"FeatureCollection","features":[{"properties":{},"type":"Feature","geometry":{"coordinates":[4.6417655,52.37360117],"type":"Point"}}]},"bag":{"type":"nummeraanduiding","id":"nummeraanduiding-0392200000093761"},"address":{"full":"Klaprooshof 1, Haarlem"}}'
                        ],
                        "type": "address_v2",
                        "is_multiple": False,
                    },
                    {
                        "naam": "numeric field",
                        "magic_string": "numeric_field",
                        "value": ["10"],
                        "type": "numeric",
                        "is_multiple": False,
                    },
                    {
                        "naam": "location address",
                        "magic_string": "location_address",
                        "value": [
                            "nummeraanduiding-0160200000254729",
                            "nummeraanduiding-0575200000050889",
                        ],
                        "type": "bag_straat_adressen",
                        "is_multiple": True,
                    },
                    {
                        "naam": "test address postcode",
                        "magic_string": "test_address1",
                        "value": ["nummeraanduiding-0160200000254729"],
                        "type": "bag_adressen",
                        "is_multiple": True,
                    },
                    {
                        "naam": "address google map",
                        "magic_string": "address_google_map",
                        "value": [
                            "Amsterdam Rijnkanaalkade 55, 1019VA Amsterdam"
                        ],
                        "type": "googlemaps",
                        "is_multiple": False,
                    },
                    {
                        "naam": "Address straat",
                        "magic_string": "address_straat",
                        "value": ["openbareruimte-0362300000027499"],
                        "type": "bag_openbareruimte",
                        "is_multiple": True,
                    },
                    {
                        "naam": "location on map",
                        "magic_string": "location_on_map",
                        "value": ["52.32464203,5.07427267"],
                        "type": "geolatlon",
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": True,
                "user_is_assignee": True,
                "user_is_coordinator": True,
                "authorizations": ["read"],
            }
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [org],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        address_1 = namedtuple("query_result", "address")(
            address="msterdam Rijnkanaalkade 1, 1019VA Amsterdam"
        )
        address_2 = namedtuple("query_result", "address")(
            address="H.H. Schefferlaan 1, 7771CW Hardenberg"
        )
        address_list_1 = [address_1, address_2]
        self.session.execute().fetchall.side_effect = [
            [mock_file],
            address_list_1,
            [address_1],
            [address_2],
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [subject],
        ]

        self.session.reset_mock()

        result = self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert isinstance(result, entities.Case)
        assert result.entity_meta_authorizations == {"search", "read", "write"}

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 20

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.result_uuid, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_2)s ELSE %(param_3)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_4)s ELSE %(param_5)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID)) THEN %(param_6)s ELSE %(param_7)s END AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_2)s::UUID AND view_case_v2.status != %(status_1)s AND view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_2)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "array((SELECT json_build_object(%(json_build_object_2)s, zaak_betrokkenen.subject_id, %(json_build_object_3)s, zaak_betrokkenen.betrokkene_type, %(json_build_object_4)s, coalesce(zaak_betrokkenen.rol, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_1)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_2)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_3)s END), %(json_build_object_5)s, coalesce(zaak_betrokkenen.magic_string_prefix, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_4)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_5)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_6)s END)) AS json_build_object_1 \n"
            "FROM zaak JOIN zaak_betrokkenen ON zaak.id = zaak_betrokkenen.zaak_id AND zaak_betrokkenen.deleted IS NULL \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID))"
        )

        select_statement = call_list[13][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT CAST(bag_cache.bag_data AS JSON) ->> %(param_1)s AS address \n"
            "FROM bag_cache \n"
            "WHERE bag_cache.bag_id IN (__[POSTCOMPILE_bag_id_1])"
        )

    def test_get_case_for_secret_person_as_requestor(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        self.session.query().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )

        address = mock.MagicMock()
        address.configure_mock(
            straatnaam="Vrijzicht",
            huisnummer=50,
            huisnummertoevoeging="ABC",
            woonplaats="Amsterdam",
            postcode="1234ER",
            adres_buitenland1=None,
            adres_buitenland2=None,
            adres_buitenland3=None,
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        person = mock.MagicMock()
        person.configure_mock(
            uuid=str(uuid4()),
            id=1,
            voornamen="Edwin",
            voorvoegsel="T",
            geslachtsnaam="Jaison",
            naamgebruik="T Jaison",
            geslachtsaanduiding="M",
            burgerservicenummer="1234567",
            geboorteplaats="Amsterdam",
            geboorteland="Amsterdam",
            geboortedatum=None,
            landcode=6030,
            active=True,
            adellijke_titel="Mr.",
            datum_huwelijk_ontbinding=None,
            onderzoek_persoon=None,
            datum_huwelijk=None,
            a_nummer=None,
            indicatie_geheim="7",
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id

        self.session.query().outerjoin().outerjoin().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": True,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "manage"],
            }
        )

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            None,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]

        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        self.session.reset_mock()

        case = self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert case.entity_meta_authorizations == {
            "search",
            "read",
            "write",
            "manage",
        }

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    def test_get_case_no_department(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )
        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        address = mock.MagicMock()
        address.configure_mock(
            straatnaam="Vrijzicht",
            huisnummer=50,
            huisnummertoevoeging="ABC",
            woonplaats="Amsterdam",
            postcode="1234ER",
            adres_buitenland1=None,
            adres_buitenland2=None,
            adres_buitenland3=None,
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        person = mock.MagicMock()
        person.configure_mock(
            uuid=str(uuid4()),
            id=1,
            voornamen="Edwin",
            voorvoegsel="T",
            geslachtsnaam="Jaison",
            naamgebruik="T Jaison",
            geslachtsaanduiding="M",
            burgerservicenummer="1234567",
            geboorteplaats="Amsterdam",
            geboorteland="Amsterdam",
            geboortedatum=None,
            landcode=6030,
            active=True,
            adellijke_titel="Mr.",
            datum_huwelijk_ontbinding=None,
            onderzoek_persoon=None,
            datum_huwelijk=None,
            a_nummer=None,
            indicatie_geheim=None,
            address=address,
            contact_data=contact_data,
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        self.session.query().outerjoin().outerjoin().filter().one.side_effect = [
            namedtuple("query_result", "NatuurlijkPersoon Adres ContactData")(
                person, address, contact_data
            ),
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        self.session.reset_mock()
        self.qry.get_case_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    def test_get_case_for_person_as_requestor(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )
        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        address = mock.MagicMock()
        address.configure_mock(
            straatnaam="Vrijzicht",
            huisnummer=50,
            huisnummertoevoeging="ABC",
            woonplaats="Amsterdam",
            postcode="1234ER",
            adres_buitenland1=None,
            adres_buitenland2=None,
            adres_buitenland3=None,
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        person = mock.MagicMock()
        person.configure_mock(
            uuid=str(uuid4()),
            id=1,
            voornamen="Edwin",
            voorvoegsel="T",
            geslachtsnaam="Jaison",
            naamgebruik="T Jaison",
            geslachtsaanduiding="M",
            burgerservicenummer="1234567",
            geboorteplaats="Amsterdam",
            geboorteland="Amsterdam",
            geboortedatum=None,
            landcode=6030,
            active=True,
            adellijke_titel="Mr.",
            datum_huwelijk_ontbinding=None,
            onderzoek_persoon=None,
            datum_huwelijk=None,
            a_nummer=None,
            indicatie_geheim=None,
            address=address,
            contact_data=contact_data,
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        self.session.query().outerjoin().outerjoin().filter().one.side_effect = [
            namedtuple("query_result", "NatuurlijkPersoon Adres ContactData")(
                person, address, contact_data
            ),
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": None,
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        self.session.reset_mock()
        self.qry.get_case_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    def test_get_case_for_organization_as_requestor(
        self,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        org_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=org_uuid,
            type="bedrijf",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        org = mock.MagicMock()
        org.configure_mock(
            uuid=org_uuid,
            id=1,
            handelsnaam="Mintlab",
            correspondentie_woonplaats="Amsterdam",
            correspondentie_straatnaam="Overamstel",
            correspondentie_postcode="1234ER",
            vestiging_straatnaam="Overamstel",
            correspondentie_huisnummer=50,
            correspondentie_huisletter="M",
            correspondentie_huisnummertoevoeging="SS",
            vestiging_huisnummer=23,
            vestiging_huisletter="P",
            vestiging_huisnummertoevoeging="MM",
            vestiging_postcode="1234ER",
            correspondentie_adres_buitenland1=None,
            correspondentie_adres_buitenland2=None,
            correspondentie_adres_buitenland3=None,
            dossiernummer=50,
            rechtsvorm="Internet banking",
            vestigingsnummer=3,
            correspondentie_landcode=6030,
            vestiging_landcode=6030,
            contact_data=contact_data,
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id

        self.session.query().filter().outerjoin().one.side_effect = [
            namedtuple("query_result", "Bedrijf ContactData")(
                org, contact_data
            ),
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "case_department": {
                    "uuid": str(uuid4()),
                    "name": "Backoffice",
                    "description": "Default backoffice",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_role": {
                    "uuid": str(uuid4()),
                    "name": "Behandelaar",
                    "description": "Systeemrol: Behandelaar",
                    "parent_uuid": str(uuid4()),
                    "parent_name": "Development",
                },
                "case_subjects": None,
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": "role_id",
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            True,
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        self.qry.get_case_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    def test_get_case_requestor_employee_not_found(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        role_id = 10
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        subject_uuid = str(uuid4())
        subject = [
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Aanvrager",
                "magic_string_prefix": "aanvrager",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Behandelaar",
                "magic_string_prefix": "behandelaar",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Coordinator",
                "magic_string_prefix": "coordinator",
            },
        ]

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "department": None,
                "role": "mock.Mock()",
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": role_id,
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [subject],
            None,
            None,
            None,
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert excinfo.value.args == (
            f"Employee with uuid '{subject_uuid}' not found.",
            "employee/not_found",
        )

    def test_get_case_requestor_person_not_found(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        role_id = 10
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        subject_uuid = str(uuid4())
        subject = [
            {
                "uuid": subject_uuid,
                "type": "natuurlijk_persoon",
                "role": "Aanvrager",
                "magic_string_prefix": "natuurlijk_persoon",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Behandelaar",
                "magic_string_prefix": "behandelaar",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Coordinator",
                "magic_string_prefix": "coordinator",
            },
        ]

        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "department": None,
                "role": "mock.Mock()",
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": role_id,
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [subject],
            None,
            None,
            None,
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
        ]

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert excinfo.value.args == (
            f"Person with uuid '{subject_uuid}' not found.",
            "natuurlijk_persoon/not_found",
        )

    def test_get_case_requestor_organization_not_found(self):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        role_id = 10
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                    "is_multiple": False,
                },
            ],
        )

        subject_uuid = str(uuid4())
        subject = [
            {
                "uuid": subject_uuid,
                "type": "bedrijf",
                "role": "Aanvrager",
                "magic_string_prefix": "bedrijf",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Behandelaar",
                "magic_string_prefix": "behandelaar",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Coordinator",
                "magic_string_prefix": "coordinator",
            },
        ]
        mock_case = mock.Mock(
            **{
                "id": 16,
                "uuid": case_uuid,
                "department": None,
                "role": "mock.Mock()",
                "vernietigingsdatum": None,
                "archival_state": None,
                "status": "new",
                "afhandeldatum": None,
                "stalled_until": None,
                "milestone": 1,
                "suspension_reason": None,
                "completion": None,
                "last_modified": date(2019, 3, 30),
                "coordinator": None,
                "assignee": None,
                "requestor": None,
                "subject": "Parking permit",
                "subject_extern": "Parking permit amsterdam",
                "case_type_uuid": "case_type_uuid",
                "case_type_version_uuid": uuid4(),
                "aanvraag_trigger": "extern",
                "contactkanaal": "post",
                "resume_reason": None,
                "summary": "Perking permit Amsterdam",
                "payment_amount": "100",
                "payment_status": "success",
                "confidentiality": "public",
                "attributes": {},
                "prefix": "1",
                "route_ou": 1,
                "route_role": role_id,
                "behandelaar": 100,
                "behandelaar_gm_id": 1,
                "coordinator_gm_id": 1,
                "created": date(2020, 1, 1),
                "registratiedatum": date(2020, 1, 1),
                "streefafhandeldatum": None,
                "urgency": "No",
                "preset_client": "No",
                "onderwerp": "test",
                "onderwerp_extern": "test summary",
                "resultaat": "test",
                "resultaat_id": 1,
                "result_uuid": uuid4(),
                "aanvrager": 200,
                "aanvrager_gm_id": 1,
                "selectielijst": None,
                "case_status": {
                    "id": 348,
                    "zaaktype_node_id": 147,
                    "status": 1,
                    "status_type": None,
                    "naam": "Geregistreerd",
                    "created": "2021-10-26T09:24:06.494135",
                    "last_modified": "2021-10-26T09:24:06.494135",
                    "ou_id": 1,
                    "role_id": 12,
                    "checklist": None,
                    "fase": "Registreren",
                    "role_set": None,
                },
                "case_meta": {
                    "opschorten": None,
                    "afhandeling": None,
                    "stalled_since": None,
                },
                "case_actions": {},
                "custom_fields": [
                    {
                        "name": "custom_field_1",
                        "magic_string": "test_subject",
                        "type": "text",
                        "value": ["Test Subject"],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_2",
                        "magic_string": "object_relation_1",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                    {
                        "name": "custom_field_3",
                        "magic_string": "object_relation_2",
                        "type": "relationship",
                        "value": [
                            '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                        ],
                        "is_multiple": False,
                    },
                ],
                "file_custom_fields": None,
                "destructable": False,
                "aggregation_scope": "Dossier",
                "number_parent": None,
                "number_master": 737,
                "relations": ",738",
                "number_previous": None,
                "premature_completion_rationale": None,
                "price": 100,
                "type_of_archiving": "Bewaren (B)",
                "period_of_preservation": "4 weken",
                "suspension_rationale": None,
                "result_description": "closing the case",
                "result_explanation": "closing the case",
                "result_selection_list_number": "10",
                "result_process_type_number": "11",
                "result_process_type_name": "test-process-type",
                "result_process_type_description": "This is test process type",
                "result_process_type_explanation": "This is test process type",
                "result_process_type_object": "Procestype-object",
                "result_process_type_generic": "Generiek",
                "result_origin": "Systeemanalyse",
                "result_process_term": "A",
                "active_selection_list": "test list",
                "days_left": 10,
                "lead_time_real": None,
                "progress_days": 10,
                "case_location": "Test locataion",
                "user_is_admin": False,
                "user_is_requestor": False,
                "user_is_assignee": False,
                "user_is_coordinator": False,
                "authorizations": ["read", "write"],
            }
        )
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [subject],
            None,
            None,
            None,
            namedtuple("uuid", "uuid")(uuid=case_type_uuid),
            mock_case_type_node,
            [mock_case_type],
        ]

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert excinfo.value.args == (
            f"Organization with uuid '{subject_uuid}' not found.",
            "organization/not_found",
        )


class Test_AsUser_transition_Case(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, custom_fields, rules=None):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["id"] = "1"
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules or None,
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"

        return mock_case_type


class Test_AsUser_Subject_relation_stories(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

    def _compile_query(self, query) -> str:
        return str(query.compile(dialect=postgresql.dialect()))

    @mock.patch("urllib.request.urlopen")
    def test_send_subject_relation_email(self, mock_urlopen):
        subject_relation_uuid = uuid4()
        subject_relation = mock.MagicMock()
        case = mock.MagicMock()
        case.configure_mock(id=uuid4(), type="case")
        subject_relation.configure_mock(
            uuid=subject_relation_uuid,
            role="Auditor",
            magic_string_prefix="auditor1",
            pip_authorized=False,
            subject_uuid=uuid4(),
            subject_type="medewerker",
            name="testuser",
            case_uuid=uuid4(),
            send_confirmation_email=None,
            enqueued_email_data=None,
            permission="read",
            is_preset_client=True,
            source_custom_field_type_id=None,
            source_custom_field_type_name=None,
            source_custom_field_magic_string=None,
            type="subject_relation",
        )
        user_info = mock.MagicMock()
        user_info.configure_mock(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )

        self.session.execute().fetchone.side_effect = [subject_relation]
        self.session.execute().fetchall.side_effect = [
            namedtuple("uuid", "uuid")(uuid=uuid4()),
            user_info,
        ]
        self.session.reset_mock()
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'

        self.cmd.send_subject_relation_email(str(subject_relation_uuid))
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaak_betrokkenen.uuid AS uuid, zaak_betrokkenen.betrokkene_type AS subject_type, CASE WHEN (zaak_betrokkenen.betrokkene_type = %(betrokkene_type_1)s) THEN CAST(subject.properties AS JSON) ->> %(param_1)s WHEN (zaak_betrokkenen.betrokkene_type = %(betrokkene_type_2)s) THEN concat_ws(%(concat_ws_1)s, natuurlijk_persoon.voorletters, natuurlijk_persoon.voorvoegsel, natuurlijk_persoon.geslachtsnaam) WHEN (zaak_betrokkenen.betrokkene_type = %(betrokkene_type_3)s) THEN bedrijf.handelsnaam ELSE zaak_betrokkenen.naam END AS name, coalesce(zaak_betrokkenen.magic_string_prefix, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_2)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_3)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_4)s END) AS magic_string_prefix, CASE WHEN (zaak_betrokkenen.rol IS NOT NULL) THEN zaak_betrokkenen.pip_authorized WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_5)s ELSE %(param_6)s END AS pip_authorized, zaak.uuid AS case_uuid, zaak_betrokkenen.subject_id AS subject_uuid, zaak_betrokkenen.authorisation AS permission, coalesce(zaak_betrokkenen.rol, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_7)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_8)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_9)s END) AS role, zaak.preset_client AS is_preset_client, bibliotheek_kenmerken.uuid AS source_custom_field_type_id, bibliotheek_kenmerken.naam AS source_custom_field_type_name, bibliotheek_kenmerken.magic_string AS source_custom_field_magic_string \n"
            "FROM zaak JOIN zaak_betrokkenen ON zaak_betrokkenen.zaak_id = zaak.id AND zaak_betrokkenen.deleted IS NULL AND zaak.deleted IS NULL AND zaak.status != %(status_1)s LEFT OUTER JOIN bibliotheek_kenmerken ON zaak_betrokkenen.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id LEFT OUTER JOIN subject ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_4)s AND subject.subject_type = %(subject_type_1)s AND subject.uuid = zaak_betrokkenen.subject_id LEFT OUTER JOIN natuurlijk_persoon ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_5)s AND natuurlijk_persoon.uuid = zaak_betrokkenen.subject_id LEFT OUTER JOIN bedrijf ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_6)s AND bedrijf.uuid = zaak_betrokkenen.subject_id \n"
            "WHERE zaak_betrokkenen.uuid = %(uuid_1)s::UUID AND zaak_betrokkenen.deleted IS NULL"
        )


class Test_AsUser_set_result(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )
        self.cmd.user_info = self.user_info

    def _compile_query(self, query) -> str:
        return str(query.compile(dialect=postgresql.dialect()))

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_not_found(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())

        mock_find_case.side_effect = NotFound("")

        with pytest.raises(NotFound):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_type_result_uuid_not_found(
        self, mock_find_case, test_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        mock_find_case.return_value = case
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as exception:
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )
        assert (
            exception.value.args[0]
            == f"case_type_result not found {case_type_result_uuid} for case_uuid {case_uuid}"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger(self, mock_find_case, test_case):
        case_uuid = uuid4()
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = date(2019, 10, 25)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=str(case_uuid),
            case_type_result_uuid=case_type_result_uuid,
        )

        assert case.destruction_date == date(2019, 11, 22)
        assert case.result == entities.CaseResult(
            result="aangegaan",
            result_uuid=UUID(case_type_result_uuid),
            archival_attributes=None,
        )
        assert case.archival_state == "vernietigen"

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s::UUID AND zaak_1.status != %(status_1)s AND zaaktype_resultaten_1.uuid = %(uuid_2)s::UUID AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s"
        )

        assert (
            self._compile_query(call_list[1][0][0])
            == "UPDATE zaak SET last_modified=now(), vernietigingsdatum=%(vernietigingsdatum)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert (
            self._compile_query(call_list[2][0][0])
            == "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert (
            self._compile_query(call_list[3][0][0])
            == "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=(SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s::UUID) WHERE zaak.uuid = %(uuid_2)s::UUID"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_destruction_date_month(
        self, mock_find_case, test_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = date(2019, 10, 25)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 maanden",
            preservation_term_unit="month",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date == date(2020, 2, 25)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_destruction_date_year(
        self, mock_find_case, test_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = date(2019, 10, 25)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 jaar",
            preservation_term_unit="year",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date == date(2023, 10, 25)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_destruction_invalid_unit(
        self, mock_find_case, test_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = date(2019, 10, 25)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 decade",
            preservation_term_unit="decade",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        with pytest.raises(NotImplementedError):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_trigger_active(self, mock_find_case, test_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = date(2019, 10, 25)
        case.destruction_date = date(2022, 3, 1)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=True,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date is None
        assert case.result == entities.CaseResult(
            result="aangegaan",
            result_uuid=UUID(case_type_result_uuid),
            archival_attributes=None,
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s::UUID AND zaak_1.status != %(status_1)s AND zaaktype_resultaten_1.uuid = %(uuid_2)s::UUID AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s"
        )

        assert (
            self._compile_query(call_list[1][0][0])
            == "UPDATE zaak SET last_modified=now(), vernietigingsdatum=%(vernietigingsdatum)s WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert (
            self._compile_query(call_list[2][0][0])
            == "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=(SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s::UUID) WHERE zaak.uuid = %(uuid_2)s::UUID"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_no_completion_date(
        self, mock_find_case, test_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = None
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.result == entities.CaseResult(
            result="aangegaan",
            result_uuid=UUID(case_type_result_uuid),
            archival_attributes=None,
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s::UUID AND zaak_1.status != %(status_1)s AND zaaktype_resultaten_1.uuid = %(uuid_2)s::UUID AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s"
        )

        assert (
            self._compile_query(call_list[1][0][0])
            == "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s::UUID"
        )

        assert (
            self._compile_query(call_list[2][0][0])
            == "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=(SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s::UUID) WHERE zaak.uuid = %(uuid_2)s::UUID"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_status_invalid(self, mock_find_case, test_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.completion_date = date(2019, 10, 25)
        case.status = "resolved"
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        with pytest.raises(Conflict):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_invalid_unit(
        self, mock_find_case, test_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        case.destruction_date = date(2019, 1, 1)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="Bewaren",
            preservation_term_unit=None,
            preservation_term_unit_amount=None,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid,
            case_type_result_uuid=case_type_result_uuid,
        )

        # assert no changes on desctruction_date due to invalue perservation_term_unit
        assert case.destruction_date == date(2019, 1, 1)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_add_validsignentry_to_timeline(self, mock_find_case, test_case):
        case_uuid = "3fa85f64-5717-4562-b3fc-2c963f66afa6"
        case: entities.Case = test_case(
            case_uuid=case_uuid, event_service=self.cmd.event_service
        )
        mock_find_case.return_value = case

        participants = [
            {
                "name": "name1",
                "lastName": "lastname1",
                "email": "string",
                "phone": "string",
                "order": 0,
            },
            {
                "name": "name2",
                "lastName": "lastname2",
                "email": "string",
                "phone": "string",
                "order": 0,
            },
        ]

        self.cmd.add_validsign_timeline_entry(
            case_uuid=case_uuid,
            participants=participants,
        )

        assert (
            case.document_sign_participants[0].name == participants[0]["name"]
        )
        assert (
            case.document_sign_participants[0].lastName
            == participants[0]["lastName"]
        )
        assert (
            case.document_sign_participants[0].email
            == participants[0]["email"]
        )
        assert (
            case.document_sign_participants[1].name == participants[1]["name"]
        )
