# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import pytest
from collections import defaultdict, namedtuple
from datetime import date, datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_Case_Relation_Queries(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_get_case_relation(self):
        case_uuid = str(uuid4())
        self.qry.get_case_relations(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak.uuid, zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID)))"
        )
        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT %(param_1)s AS relation_type, zaak_1.uuid AS case_a_uuid, zaak_2.uuid AS case_b_uuid, zaak_1.onderwerp AS case_a_onderwerp, zaak_2.onderwerp AS case_b_onderwerp, case_relation.uuid AS relation_uuid, case_relation.case_id_a, case_relation.case_id_b, case_relation.type_a, case_relation.type_b, case_relation.order_seq_a, case_relation.order_seq_b \n"
            "FROM zaak AS zaak_1 JOIN case_relation ON zaak_1.id = case_relation.case_id_a JOIN zaak AS zaak_2 ON zaak_2.id = case_relation.case_id_b \n"
            "WHERE (case_relation.case_id_a = %(case_id_a_1)s OR case_relation.case_id_b = %(case_id_b_1)s) AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_2.deleted IS NULL AND zaak_2.status != %(status_2)s AND (zaak_2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak_2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_2.id AND case_acl.casetype_id = zaak_2.zaaktype_id AND case_acl.permission = %(permission_2)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID)))"
        )
        assert compiled_select.params["permission_1"] == "search"
        assert compiled_select.params["permission_2"] == "search"

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT %(param_1)s AS relation_type, zaak_1.uuid AS case_a_uuid, zaak.uuid AS case_b_uuid, zaak_1.onderwerp AS case_a_onderwerp, zaak.onderwerp AS case_b_onderwerp, zaak_1.uuid AS relation_uuid, zaak_1.id AS case_id_a, zaak.id AS case_id_b, %(param_2)s AS type_a, %(param_3)s AS type_b, %(param_4)s AS order_seq_a, %(param_5)s AS order_seq_b \n"
            "FROM zaak AS zaak_1 JOIN zaak ON zaak_1.id = zaak.pid \n"
            "WHERE zaak.id = %(id_1)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) UNION SELECT %(param_6)s AS relation_type, zaak_2.uuid AS case_a_uuid, zaak.uuid AS case_b_uuid, zaak_2.onderwerp AS case_a_onderwerp, zaak.onderwerp AS case_b_onderwerp, zaak_2.uuid AS relation_uuid, zaak_2.id AS case_id_a, zaak.id AS case_id_b, %(param_7)s AS type_a, %(param_8)s AS type_b, %(param_9)s AS order_seq_a, %(param_10)s AS order_seq_b \n"
            "FROM zaak AS zaak_2 JOIN zaak ON zaak_2.pid = zaak.id \n"
            "WHERE zaak.id = %(id_2)s AND zaak_2.deleted IS NULL AND zaak_2.status != %(status_2)s AND (zaak_2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak_2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_2.id AND case_acl.casetype_id = zaak_2.zaaktype_id AND case_acl.permission = %(permission_2)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID)))"
        )
        assert compiled_select.params["permission_1"] == "search"
        assert compiled_select.params["permission_2"] == "search"

    def test_get_case_relation_not_found(self):
        case_uuid = str(uuid4())

        with pytest.raises(NotFound) as excinfo:
            self.session.execute().fetchone.side_effect = [None]
            self.qry.get_case_relations(case_uuid=case_uuid)

        assert excinfo.value.args == (
            "Case not found.",
            "case_relation/case/not_found",
        )


class Test_Case_Relation_Commands(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.cmd.user_info = self.user_info

    def _mock_case(self, id, case_uuid):
        mock_case = mock.MagicMock()
        mock_case = {
            "id": id,
            "uuid": case_uuid,
            "department": None,
            "role": "mock.Mock()",
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "case_status_details": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
        }

        return mock_case

    def _mock_case_type(self, case_type_uuid, custom_fields, rules=None):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules or [],
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"

        return mock_case_type

    def _mock_department(self, department_uuid):
        # department entity
        department = mock.Mock()
        department.configure_mock(
            uuid=department_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )
        return department

    def _mock_system_attributes(self):
        system_attribute_values = mock.MagicMock()
        system_attribute_values = {
            "case.type_of_archiving": "Bewaren (B)",
            "case.period_of_preservation": "4 weken",
            "case.suspension_rationale": None,
            "case.result_description": "closing the case",
            "case.result_explanation": "closing the case",
            "case.result_selection_list_number": "10",
            "case.result_process_type_number": "11",
            "case.result_process_type_name": "test-process-type",
            "case.result_process_type_description": "This is test process type",
            "case.result_process_type_explanation": "This is test process type",
            "case.result_process_type_object": "Procestype-object",
            "case.result_process_type_generic": "Generiek",
            "case.result_origin": "Systeemanalyse",
            "case.result_process_term": "A",
            "case.active_selection_list": "test list",
            "case.case_location.nummeraanduiding": {
                "human_identifier": "H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            },
        }
        system_attributes = mock.MagicMock()
        system_attributes = {
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "values": system_attribute_values,
        }
        return system_attributes

    def _mock_role(self, department_uuid):
        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )
        return mock_role

    def _mock_requestor(self, subject_uuid):
        mock_requestor = mock.MagicMock()
        mock_requestor = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        return mock_requestor

    def _mock_assignee(self, subject_uuid):
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        return mock_assignee

    def _mock_subject(self, subject_uuid):
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": mock.MagicMock(),
            "Group": [],
        }
        return mock_subject

    def test_create_case_relation(self):
        uuid1 = str(uuid4())
        uuid2 = str(uuid4())
        uuid3 = str(uuid4())
        mock_allowed_case_1 = mock.MagicMock()
        mock_allowed_case_1.case_id = 1
        mock_allowed_case_2 = mock.MagicMock()
        mock_allowed_case_2.case_id = 2
        mock_case_1 = mock.MagicMock()
        mock_case_1.id = 1
        mock_case_1.uuid = uuid1
        mock_case_2 = mock.MagicMock()
        mock_case_2.id = 2
        mock_case_2.uuid = uuid2
        related_case_row_1 = mock.Mock()
        related_case_row_1.configure_mock(
            relation_type="related_case",
            case_a_uuid=uuid1,
            case_b_uuid=uuid3,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )
        related_case_row_2 = mock.Mock()
        related_case_row_2.configure_mock(
            relation_type="related_case",
            case_a_uuid=uuid1,
            case_b_uuid=uuid3,
            case_a_onderwerp="case 2",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_1,
            mock_case_2,
            None,
            namedtuple("case_id", "id")(id=1),
            namedtuple("case_id", "id")(id=2),
            namedtuple("relation_count", "count")(count=1),
            namedtuple("relation_count", "count")(count=3),
        ]
        self.session.execute().fetchall.side_effect = [
            [mock_allowed_case_1, mock_allowed_case_2],
            [related_case_row_1],
            [related_case_row_2],
            [],
        ]
        self.session.reset_mock()
        self.cmd.create_case_relation(uuid1=uuid1, uuid2=uuid2)
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 8

        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Select"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"
        assert call_list[7][0][0].__class__.__name__ == "Select"

        insert_statement = call_list[3][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO case_relation (case_id_a, case_id_b, order_seq_a, order_seq_b, type_a, type_b) VALUES (%(case_id_a)s, %(case_id_b)s, (SELECT coalesce(max(anon_1.count) + %(max_1)s, %(coalesce_2)s) AS coalesce_1 \n"
            "FROM (SELECT case_relation.order_seq_a AS count \n"
            "FROM case_relation \n"
            "WHERE case_relation.case_id_b = %(case_id_b_1)s UNION ALL SELECT case_relation.order_seq_b AS count \n"
            "FROM case_relation \n"
            "WHERE case_relation.case_id_a = %(case_id_a_1)s) AS anon_1), (SELECT coalesce(max(anon_2.count) + %(max_2)s, %(coalesce_4)s) AS coalesce_3 \n"
            "FROM (SELECT case_relation.order_seq_a AS count \n"
            "FROM case_relation \n"
            "WHERE case_relation.case_id_b = %(case_id_b_2)s UNION ALL SELECT case_relation.order_seq_b AS count \n"
            "FROM case_relation \n"
            "WHERE case_relation.case_id_a = %(case_id_a_2)s) AS anon_2), %(type_a)s, %(type_b)s) RETURNING case_relation.id"
        )

    def test_create_case_relation_case_not_found(self):
        uuid1 = str(uuid4())
        uuid2 = str(uuid4())

        with pytest.raises(NotFound) as excinfo:
            self.session.execute().fetchone.side_effect = [None, None]

            self.cmd.create_case_relation(uuid1=uuid1, uuid2=uuid2)

        assert excinfo.value.args == (
            "Case not found.",
            "case_relation/case/not_found",
        )

        with pytest.raises(Conflict) as excinfo:
            mock_case_1 = mock.MagicMock()
            mock_case_1.id = 1
            mock_case_1.uuid = uuid1
            mock_case_2 = mock.MagicMock()
            mock_case_2.id = 2
            mock_case_2.uuid = uuid2
            self.session.execute().fetchone.side_effect = [
                mock_case_1,
                mock_case_2,
                uuid4(),
            ]
            self.cmd.create_case_relation(uuid1=uuid1, uuid2=uuid2)

        assert excinfo.value.args == (
            "Relationship already exists",
            "case_relation/already_exists",
        )

    def test_create_case_relation_with_self(self):
        uuid1 = str(uuid4())
        mock_case_1 = mock.MagicMock()
        mock_case_1.id = 1
        mock_case_1.uuid = uuid1

        with pytest.raises(Conflict) as excinfo:
            case_type_uuid = uuid4()
            type_uuid_1 = uuid4()
            type_uuid_2 = uuid4()

            mock_case_type_node = mock.Mock()
            mock_case_type_node.configure_mock(id=42, uuid=uuid4())
            mock_case_type = self._mock_case_type(
                case_type_uuid,
                custom_fields=[
                    {
                        "uuid": uuid4(),
                        "name": "custom_field_1",
                        "field_magic_string": "test_subject",
                        "field_type": "text",
                    },
                    {
                        "uuid": type_uuid_1,
                        "name": "custom_field_2",
                        "field_magic_string": "object_relation_1",
                        "field_type": "relationship",
                        "relationship_type": "custom_object",
                    },
                    {
                        "uuid": type_uuid_2,
                        "name": "custom_field_3",
                        "field_magic_string": "object_relation_2",
                        "field_type": "relationship",
                        "relationship_type": "custom_object",
                    },
                ],
            )

            subject_id = str(uuid4())
            subject = mock.MagicMock()
            subject.betrokkene_type = "medewerker"
            subject.rol = "advocaat"
            subject.subject_id = subject_id

            mock_case_1 = mock.MagicMock()
            mock_case_1 = self._mock_case(id=1, case_uuid=uuid1)
            department_uuid = uuid4()
            subject_uuid = uuid4()
            self.session.execute().fetchone.side_effect = [
                [mock_case_1],
                [
                    [
                        self._mock_requestor(subject_uuid),
                        self._mock_assignee(subject_uuid),
                    ]
                ],
                [self._mock_subject(subject_uuid)],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                [self._mock_subject(subject_uuid)],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                self._mock_system_attributes(),
                mock.MagicMock(),
                [mock_case_type],
                None,
                self._mock_department(department_uuid),
                self._mock_role(department_uuid),
                [mock_case_1],
                [
                    [
                        self._mock_requestor(subject_uuid),
                        self._mock_assignee(subject_uuid),
                    ]
                ],
                [self._mock_subject(subject_uuid)],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                [self._mock_subject(subject_uuid)],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                self._mock_system_attributes(),
                mock_case_type_node,
                [mock_case_type],
                None,
                self._mock_department(department_uuid),
                self._mock_role(department_uuid),
                uuid4(),
            ]

            mock_related_case_1 = mock.MagicMock()
            mock_related_case_1.case_a_id = 20
            mock_related_case_2 = mock.MagicMock()
            mock_related_case_2.case_a_id = 12

            self.session.execute().fetchall.side_effect = [
                [mock_related_case_1, mock_related_case_2],
                [mock_related_case_1],
                [mock_related_case_2],
                [],
                [subject],
                [mock.MagicMock()],
                [mock_related_case_1, mock_related_case_2],
                [mock_related_case_1],
                [mock_related_case_2],
                [subject],
                [mock.MagicMock()],
                [],
                None,
                str(self.user_uuid),
            ]

            self.cmd.create_case_relation(uuid1=uuid1, uuid2=uuid1)

        assert excinfo.value.args == (
            "Can not relate case to itself",
            "case_relation/self_relation/not_allowed",
        )

    def test_delete_case_relation(self):
        relation_uuid = uuid4()
        case_uuid_1 = uuid4()
        case_uuid_2 = uuid4()
        case_uuid_3 = uuid4()

        mock_case_1 = mock.MagicMock()
        mock_case_1 = self._mock_case(id=1, case_uuid=case_uuid_1)
        mock_case_2 = mock.MagicMock()
        mock_case_2 = self._mock_case(id=1, case_uuid=case_uuid_2)

        case_relation_row = mock.Mock()
        case_relation_row.configure_mock(
            relation_type="related_case",
            case_a_uuid=case_uuid_1,
            case_b_uuid=case_uuid_2,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 2",
            relation_uuid=relation_uuid,
            case_id_a=1,
            case_id_b=2,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=1,
        )

        related_case_row = mock.Mock()
        related_case_row.configure_mock(
            relation_type="related_case",
            case_a_uuid=case_uuid_1,
            case_b_uuid=case_uuid_3,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 3",
            relation_uuid=uuid4(),
            case_id_a=1,
            case_id_b=3,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=2,
        )

        self.session.execute().fetchone.side_effect = [
            case_relation_row,
            [mock_case_1],
            [mock_case_2],
            namedtuple("case_id", "id")(id=1),
            namedtuple("case_id", "id")(id=2),
            namedtuple("case_id", "id")(id=1),
            namedtuple("case_id", "id")(id=2),
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 20
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 12
        self.session.execute().fetchall.side_effect = [
            [related_case_row],
            [],
            [],
            [],
        ]
        self.session.reset_mock()
        self.cmd.delete_case_relation(
            relation_uuid=str(relation_uuid), case_uuid=str(case_uuid_1)
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        delete_statement = call_list[3][0][0]
        compiled_delete = delete_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_delete) == (
            "DELETE FROM case_relation WHERE case_relation.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[5][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT %(param_1)s AS relation_type, zaak_1.uuid AS case_a_uuid, zaak_2.uuid AS case_b_uuid, zaak_1.onderwerp AS case_a_onderwerp, zaak_2.onderwerp AS case_b_onderwerp, case_relation.uuid AS relation_uuid, case_relation.case_id_a, case_relation.case_id_b, case_relation.type_a, case_relation.type_b, case_relation.order_seq_a, case_relation.order_seq_b \n"
            "FROM zaak AS zaak_1 JOIN case_relation ON zaak_1.id = case_relation.case_id_a JOIN zaak AS zaak_2 ON zaak_2.id = case_relation.case_id_b \n"
            "WHERE (case_relation.case_id_a = %(case_id_a_1)s OR case_relation.case_id_b = %(case_id_b_1)s) AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_2.deleted IS NULL AND zaak_2.status != %(status_2)s AND (zaak_2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak_2.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_2.id AND case_acl.casetype_id = zaak_2.zaaktype_id AND case_acl.permission = %(permission_2)s AND case_acl.subject_uuid = %(subject_uuid_2)s::UUID))) FOR NO KEY UPDATE"
        )
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["permission_2"] == "read"

    def test_delete_case_relation_case_not_found(self):
        with pytest.raises(NotFound) as excinfo:
            self.session.execute().fetchone.side_effect = [None]
            self.cmd.delete_case_relation(
                relation_uuid=str(uuid4()), case_uuid=str(uuid4())
            )

        assert excinfo.value.args == (
            "Case relation not found.",
            "case_relation/not_found",
        )

        with pytest.raises(NotFound) as excinfo:
            self.session.execute().fetchone.side_effect = [
                mock.MagicMock(),
                None,
                None,
            ]

            self.cmd.delete_case_relation(
                relation_uuid=str(uuid4()), case_uuid=str(uuid4())
            )

        assert excinfo.value.args == (
            "Case not found.",
            "case_relation/case/not_found",
        )

    def test_reorder_case_relation_second_case(self):
        relation_uuid_1 = uuid4()
        relation_uuid_2 = uuid4()
        case_uuid_1 = uuid4()
        case_uuid_2 = uuid4()
        case_uuid_3 = uuid4()
        mock_case_1 = mock.MagicMock()
        mock_case_1.id = 30
        mock_case_1.uuid = case_uuid_1
        mock_case_2 = mock.MagicMock()
        mock_case_2.id = 28
        mock_case_2.uuid = case_uuid_2
        case_relation_row = mock.Mock()
        case_relation_row.configure_mock(
            relation_type="related_case",
            relation_uuid=relation_uuid_1,
            case_a_uuid=case_uuid_1,
            case_b_uuid=case_uuid_3,
            type_a="plain",
            type_b="plain",
            owner_uuid=None,
            blocks_deletion=None,
            case_a_onderwerp=None,
            case_b_onderwerp=None,
            case_id_a=30,
            case_id_b=28,
            order_seq_a=1,
            order_seq_b=2,
            current_case_uuid=None,
        )

        related_case_row = mock.Mock()
        related_case_row.configure_mock(
            relation_type="related_case",
            case_a_uuid=case_uuid_1,
            case_b_uuid=case_uuid_2,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 3",
            relation_uuid=relation_uuid_2,
            case_id_a=30,
            case_id_b=29,
            type_a="plain",
            type_b="plain",
            order_seq_a=1,
            order_seq_b=1,
        )

        self.session.execute().fetchone.side_effect = [
            case_relation_row,
            mock_case_1,
            mock_case_2,
            namedtuple("case_id", "id")(id=30),
            namedtuple("case_id", "id")(id=29),
            mock_case_1,
            mock_case_2,
            namedtuple("case_id", "id")(id=30),
            namedtuple("case_id", "id")(id=28),
        ]
        self.session.execute().fetchall.side_effect = [
            [],
            [],
            [case_relation_row, related_case_row],
            [case_relation_row, related_case_row],
        ]

        self.session.reset_mock()
        self.cmd.reorder_case_relation(
            relation_uuid=str(relation_uuid_1),
            case_uuid=str(case_uuid_1),
            new_index=1,
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 7
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Select"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"
        assert call_list[6][0][0].__class__.__name__ == "Update"

        update_statement = call_list[6][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE case_relation SET order_seq_b=%(order_seq_b)s WHERE case_relation.uuid = %(uuid_1)s::UUID"
        )

    def test_reorder_case_relation_first_case(self):
        relation_uuid_1 = uuid4()
        relation_uuid_2 = uuid4()
        case_uuid_1 = uuid4()
        case_uuid_2 = uuid4()
        case_uuid_3 = uuid4()
        mock_case_1 = mock.MagicMock()
        mock_case_1.id = 28
        mock_case_1.uuid = case_uuid_1
        mock_case_2 = mock.MagicMock()
        mock_case_2.id = 30
        mock_case_2.uuid = case_uuid_2
        case_relation_row = mock.Mock()
        case_relation_row.configure_mock(
            relation_type="related_case",
            relation_uuid=relation_uuid_1,
            case_a_uuid=case_uuid_1,
            case_b_uuid=case_uuid_3,
            type_a="plain",
            type_b="plain",
            owner_uuid=None,
            blocks_deletion=None,
            case_a_onderwerp=None,
            case_b_onderwerp=None,
            case_id_a=30,
            case_id_b=28,
            order_seq_a=3,
            order_seq_b=2,
            current_case_uuid=None,
        )

        related_case_row = mock.Mock()
        related_case_row.configure_mock(
            relation_type="related_case",
            case_a_uuid=case_uuid_1,
            case_b_uuid=case_uuid_2,
            case_a_onderwerp="case 1",
            case_b_onderwerp="case 3",
            relation_uuid=relation_uuid_2,
            case_id_a=30,
            case_id_b=29,
            type_a="plain",
            type_b="plain",
            order_seq_a=2,
            order_seq_b=2,
        )
        self.session.execute().fetchone.side_effect = [
            case_relation_row,
            mock_case_1,
            mock_case_2,
            namedtuple("case_id", "id")(id=29),
            namedtuple("case_id", "id")(id=30),
            mock_case_1,
            mock_case_2,
            namedtuple("case_id", "id")(id=30),
            namedtuple("case_id", "id")(id=28),
        ]
        self.session.execute().fetchall.side_effect = [
            [],
            [],
            [related_case_row, case_relation_row],
            [related_case_row, case_relation_row],
        ]

        self.session.reset_mock()
        self.cmd.reorder_case_relation(
            relation_uuid=str(relation_uuid_2),
            case_uuid=str(case_uuid_2),
            new_index=5,
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 7
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Select"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"
        assert call_list[6][0][0].__class__.__name__ == "Update"

        update_statement = call_list[6][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE case_relation SET order_seq_a=%(order_seq_a)s WHERE case_relation.uuid = %(uuid_1)s::UUID"
        )

    def test_reorder_case_relation_case_not_found(self):
        relation_uuid_1 = uuid4()
        case_uuid_1 = uuid4()
        case_relation_row = mock.Mock()
        case_relation_row.configure_mock(
            relation_type="related_case",
            relation_uuid=relation_uuid_1,
            case_a_uuid=case_uuid_1,
            case_b_uuid=uuid4(),
            type_a="plain",
            type_b="plain",
            owner_uuid=None,
            blocks_deletion=None,
            case_a_onderwerp=None,
            case_b_onderwerp=None,
            case_id_a=30,
            case_id_b=28,
            order_seq_a=1,
            order_seq_b=2,
            current_case_uuid=None,
        )
        with pytest.raises(NotFound) as excinfo:
            self.session.execute().fetchone.side_effect = [
                case_relation_row,
                mock.MagicMock(),
                mock.MagicMock(),
                namedtuple("case_id", "id")(id=30),
                None,
            ]
            self.cmd.reorder_case_relation(
                relation_uuid=str(relation_uuid_1),
                case_uuid=str(case_uuid_1),
                new_index=1,
            )

        assert excinfo.value.args == (
            "Case not found.",
            "case_relation/case/not_found",
        )

    def test_reorder_case_relation_invalid_min_index(self):
        relation_uuid_1 = uuid4()
        case_uuid_1 = uuid4()
        case_relation_row = mock.Mock()
        case_relation_row.configure_mock(
            relation_type="related_case",
            relation_uuid=relation_uuid_1,
            case_a_uuid=case_uuid_1,
            case_b_uuid=uuid4(),
            type_a="plain",
            type_b="plain",
            owner_uuid=None,
            blocks_deletion=None,
            case_a_onderwerp=None,
            case_b_onderwerp=None,
            case_id_a=30,
            case_id_b=28,
            order_seq_a=1,
            order_seq_b=2,
            current_case_uuid=None,
        )

        with pytest.raises(Conflict) as excinfo:
            self.session.execute().fetchone.side_effect = [
                case_relation_row,
                mock.MagicMock(),
                mock.MagicMock(),
                namedtuple("case_id_count", "id")(id=3),
                mock.MagicMock(),
            ]
            self.cmd.reorder_case_relation(
                relation_uuid=str(relation_uuid_1),
                case_uuid=str(case_uuid_1),
                new_index=5,
            )

        assert excinfo.value.args == (
            "Maximum value for index is 3",
            "case_relation/reorder_index_greater_than_max",
        )

    def test_reorder_case_relation_invalid_max_index(self):
        relation_uuid_1 = uuid4()
        case_uuid_1 = uuid4()
        case_relation_row = mock.Mock()
        case_relation_row.configure_mock(
            relation_type="related_case",
            relation_uuid=relation_uuid_1,
            case_a_uuid=case_uuid_1,
            case_b_uuid=uuid4(),
            type_a="plain",
            type_b="plain",
            owner_uuid=None,
            blocks_deletion=None,
            case_a_onderwerp=None,
            case_b_onderwerp=None,
            case_id_a=30,
            case_id_b=28,
            order_seq_a=1,
            order_seq_b=2,
            current_case_uuid=None,
        )
        with pytest.raises(Conflict) as excinfo:
            self.session.execute().fetchone.side_effect = [
                case_relation_row,
                mock.MagicMock(),
                mock.MagicMock(),
                namedtuple("case_id", "id")(id=30),
                NoResultFound,
            ]
            self.cmd.reorder_case_relation(
                relation_uuid=str(relation_uuid_1),
                case_uuid=str(case_uuid_1),
                new_index=0,
            )

        assert excinfo.value.args == (
            "Minimum value for index is 1",
            "case_relation/reorder_index_less_than_one",
        )
