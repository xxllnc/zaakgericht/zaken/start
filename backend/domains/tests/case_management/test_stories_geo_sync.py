# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.cqrs.test
import pytest
from .test_objects_stories_custom_object import CustomObjectTestBase
from datetime import date, datetime
from minty.cqrs.events import EventService
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import entities
from zsnl_domains.case_management.entities._shared import (
    ContactInformation,
    InternationalAddress,
    RelatedCustomObject,
)
from zsnl_domains.case_management.repositories.case import CaseRepository
from zsnl_domains.case_management.repositories.custom_object import (
    CustomObjectRepository,
)
from zsnl_domains.case_management.repositories.organization import (
    OrganizationRepository,
)
from zsnl_domains.case_management.repositories.person import PersonRepository


class TestSyncGeo(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_uuid = uuid4()
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True, "contact_nieuw": True},
        )
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=self.user_uuid,
        )
        self.user = entities.Person(
            uuid=self.user_uuid,
            name="Name",
            surname="Surname",
            gender="F",
            has_valid_address=True,
            is_active=True,
            contact_information=ContactInformation(is_anonymous_contact=False),
            _event_service=event_service,
            authenticated=False,
            sedula_number=None,
        )

        related_custom_object_uuid = uuid4()
        self.organization = entities.Organization(
            entity_id=uuid4(),
            uuid=uuid4(),
            name="beheerder",
            source="source",
            coc_number="12345678",
            coc_location_number="012345678912",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            organization_type=None,
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object=RelatedCustomObject(
                entity_id=related_custom_object_uuid,
                uuid=related_custom_object_uuid,
            ),
            has_valid_address=True,
            authenticated=False,
            _event_service=event_service,
        )

        self.case_uuid = uuid4()
        self.case = entities.Case.parse_obj(
            {
                "id": 123,
                "number": 123,
                "uuid": self.case_uuid,
                "department": entities.Department.parse_obj(
                    {
                        "uuid": uuid4(),
                        "name": "Department of Mysteries",
                        "description": "Who knows!",
                    }
                ),
                "role": entities.Role.parse_obj(
                    {"uuid": uuid4(), "name": "Administrator"}
                ),
                "destruction_date": date(2019, 1, 1),
                "archival_state": "2019-23-23",
                "status": "open",
                "created_date": date(2019, 10, 22),
                "registration_date": date(2019, 10, 23),
                "target_completion_date": date(2019, 10, 24),
                "completion_date": date(2019, 10, 25),
                "stalled_until_date": date(2019, 10, 22),
                "milestone": 1,
                "extension_reason": "no reason",
                "suspension_reason": "no reason",
                "related_to": {},
                "completion": "yes",
                "stalled_since_date": date(2019, 12, 22),
                "last_modified_date": date(2019, 4, 19),
                "coordinator": None,
                "assignee": None,
                "case_type_version": {
                    "id": 123,
                    "uuid": uuid4(),
                    "case_type_uuid": uuid4(),
                    "version": 100,
                    "name": "Case Type Name",
                    "active": True,
                    "phases": [],
                    "requestor": {
                        "type_of_requestors": ["employee"],
                        "use_for_correspondence": False,
                    },
                    "settings": {"payment": {}},
                    "terms": {
                        "lead_time_legal": {"type": "days", "value": 100},
                        "lead_time_service": {"type": "days", "value": 100},
                    },
                    "case_type_results": [],
                    "is_eligible_for_case_creation": True,
                    "created": datetime(2022, 8, 9, 13, 19, 12),
                    "last_modified": datetime(2022, 8, 9, 13, 19, 12),
                    "metadata": {
                        "legal_basis": "None",
                        "process_description": "None",
                    },
                },
                "_event_service": self.cmd.event_service,
            }
        )

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_sync_geo_person(self, mock_find_person_by_uuid):
        mock_find_person_by_uuid.return_value = self.user
        self.session.execute.reset_mock()
        residence_address = InternationalAddress(
            address_line_1="line",
            country="Zwitserland",
            geo_lat_long="(52.02479, 5.17425)",
        )
        self.user.residence_address = residence_address
        self.cmd.sync_geo_values(
            custom_object_uuid=None,
            case_uuid=None,
            contact_uuid=uuid4(),
            type="person",
        )

    @mock.patch.object(OrganizationRepository, "find_organization_by_uuid")
    def test_sync_geo_Organization(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.organization
        self.session.execute.reset_mock()
        location_address = InternationalAddress(
            address_line_1="line",
            country="Zwitserland",
            geo_lat_long="(52.02479, 5.17425)",
        )
        self.organization.location_address = location_address
        self.cmd.sync_geo_values(
            custom_object_uuid=None,
            case_uuid=None,
            contact_uuid=self.organization.uuid,
            type="organization",
        )

    @mock.patch.object(CaseRepository, "find_case_by_uuid")
    def test_sync_geo_case(self, mock_find_case):
        mock_find_case.return_value = self.case
        self.session.execute.reset_mock()
        self.cmd.sync_geo_values(
            custom_object_uuid=None,
            case_uuid=self.case.uuid,
            contact_uuid=None,
            type=None,
        )

    @mock.patch.object(CaseRepository, "find_case_by_uuid")
    def test_sync_geo_case_not_found(self, mock_find_case):
        mock_find_case.side_effect = NotFound
        self.session.execute.reset_mock()
        with pytest.raises(minty.exceptions.Forbidden):
            self.cmd.sync_geo_values(
                custom_object_uuid=None,
                case_uuid=self.case.uuid,
                contact_uuid=None,
                type=None,
            )

    @mock.patch.object(CustomObjectRepository, "find_by_uuid")
    @mock.patch(
        "zsnl_domains.case_management.entities.custom_object_type.CustomObjectType",
        autospec=True,
    )
    def test_sync_custom_object_geo(
        self, mock_find_by_uuid, customObjectTypeMock
    ):
        fielddata = {
            "name": "Animal",
            "entity_id": uuid4(),
            "entity_meta_summary": "Cat",
            "title": "Cat",
            "subtitle": "Cow",
            "uuid": uuid4(),
            "status": "active",
            "version": 1,
            "date_created": datetime.now(),
            "last_modified": datetime.now(),
            "version_independent_uuid": uuid4(),
            "is_active_version": True,
            "custom_fields": {
                "kenmerk_locatiemetkaart": {
                    "type": "geolatlon",
                    "value": "52.326964736309215,4.929905533654112",
                },
            },
            "archive_metadata": {
                "status": "archived",
                "ground": "Custom text",
                "retention": 5,
            },
            "custom_object_type": CustomObjectTestBase().get_object_type_params(),
        }

        co = entities.custom_object.CustomObject.parse_obj(fielddata)
        mock_find_by_uuid.return_value = co
        self.session.execute.reset_mock()
        self.cmd.sync_geo_values(
            custom_object_uuid=co.uuid,
            case_uuid=None,
            contact_uuid=None,
            type=None,
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.filter"
    )
    @mock.patch(
        "zsnl_domains.case_management.entities.custom_object_type.CustomObjectType",
        autospec=True,
    )
    def test_sync_geo_case_with_custom_object_related(
        self, customObjectTypeMock, mock_filter, mock_find_case
    ):
        fielddata = {
            "name": "Animal",
            "entity_id": uuid4(),
            "entity_meta_summary": "Cat",
            "title": "Cat",
            "subtitle": "Cow",
            "uuid": uuid4(),
            "status": "active",
            "version": 1,
            "date_created": datetime.now(),
            "last_modified": datetime.now(),
            "version_independent_uuid": uuid4(),
            "is_active_version": True,
            "custom_fields": {
                "kenmerk_locatiemetkaart": {
                    "type": "geolatlon",
                    "value": "52.326964736309215,4.929905533654112",
                },
            },
            "archive_metadata": {
                "status": "archived",
                "ground": "Custom text",
                "retention": 5,
            },
            "custom_object_type": CustomObjectTestBase().get_object_type_params(),
            "_event_service": self.cmd.event_service,
        }

        co = entities.custom_object.CustomObject.parse_obj(fielddata)
        mock_find_case.return_value = self.case
        mock_filter.return_value = [co]
        self.session.execute.reset_mock()
        self.cmd.sync_geo_values(
            custom_object_uuid=None,
            case_uuid=self.case.uuid,
            contact_uuid=None,
            type=None,
        )
