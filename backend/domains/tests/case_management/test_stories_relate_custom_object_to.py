# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
import pytest
import random
from ..mocks import MockRedis
from datetime import datetime, timezone
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management

now = datetime.now(tz=timezone.utc)


def mock_custom_object_row(
    custom_object_uuid: UUID,
    custom_fields=None,
    custom_objects=None,
    custom_field_definition=None,
):
    if custom_fields is None:
        custom_fields = {}
    if custom_objects is None:
        custom_objects = []
    if custom_field_definition is None:
        custom_field_definition = {"custom_fields": []}

    return mock.Mock(
        version_independent_uuid=uuid4(),
        is_active_version="1",
        object_type_uuid=uuid4(),
        object_type_name="Test object type",
        uuid=custom_object_uuid,
        title="Test object title",
        subtitle="Test object subtitle",
        external_reference="Test reference",
        status="active",
        version=random.randint(1, 1000),
        date_created=now,
        last_modified=now,
        date_deleted=None,
        authorizations=[],
        cases=[],
        custom_objects=custom_objects,
        documents=[],
        subjects=[],
        custom_fields=custom_fields,
        archive_status="to preserve",
        archive_ground=None,
        archive_retention=0,
        ot_is_active_version=1,
        ot_version_independent_uuid=uuid4(),
        ot_uuid=uuid4(),
        ot_name="Object Type Name",
        ot_title="Object Type Title",
        ot_subtitle="Object Type Subtitle",
        ot_status="active",
        ot_version=random.randint(1, 1000),
        ot_external_reference="Object Type External Reference",
        ot_custom_field_definition=custom_field_definition,
        ot_relationship_definition={"relationships": None},
        ot_audit_log={
            "description": "f",
            "updated_components": ["attributes"],
        },
        ot_date_created=now,
        ot_last_modified=now,
        ot_date_deleted=None,
    )


class TestRelateTo(TestBase):
    def setup_method(self):
        self.redis = MockRedis()

        self.load_command_instance(
            case_management,
            inframocks={
                "redis": self.redis,
            },
        )

        self.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        self.cmd.user_info = self.user_info

    def test_relate_custom_object_to_other_object(self):
        custom_object_uuid = uuid4()
        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid
        )

        custom_object_uuid_to_relate = uuid4()
        custom_object_id_to_relate = random.randint(1, 1000)

        mock_db_entry = mock.Mock()
        mock_db_entry.configure_mock(
            id=custom_object_id_to_relate,
            uuid=custom_object_uuid_to_relate,
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,  # find_by_uuid from command
            mock_custom_object,  # find_by_uuid during save
            mock_cotv,  # custom_object_type_version
            mock_cov,  # get id of custom_object_version
            mock_co,  # get id of custom_object
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = [
            [],  # id+uuid of custom objects linked from custom fields
            [
                mock_db_entry,
            ],  # id+uuid of custom objects linked "directly"
        ]
        self.session.query().filter().all.side_effect = [
            [],  # Currently existing CustomObjectRelationships
            [],  # id+uuid of documents that are linked from custom fields
            [],  # id+uuid of cases that are linked from custom fields
            [],  # id+uuid of cases that are linked "directly"
        ]

        self.cmd.relate_custom_object_to(
            custom_object_uuid=custom_object_uuid,
            cases=[],
            custom_objects=[custom_object_uuid_to_relate],
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 7

        insert_statement = calls[6][0][0]
        insert_compiled = insert_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled) == (
            "INSERT INTO custom_object_relationship "
            "(custom_object_id, custom_object_version_id, relationship_type, relationship_magic_string_prefix, related_custom_object_id, related_uuid, source_custom_field_type_id) "
            "VALUES "
            "(%(custom_object_id)s, "
            "%(custom_object_version_id)s, "
            "%(relationship_type)s, "
            "%(relationship_magic_string_prefix)s, "
            "%(related_custom_object_id)s, "
            "%(related_uuid)s::UUID, "
            "%(source_custom_field_type_id)s) "
            "RETURNING custom_object_relationship.id"
        )
        assert insert_compiled.params == {
            "custom_object_id": mock_co.id,
            "custom_object_version_id": mock_cov.id,
            "related_custom_object_id": custom_object_id_to_relate,
            "related_uuid": custom_object_uuid_to_relate,
            "relationship_magic_string_prefix": None,
            "relationship_type": "custom_object",
            "source_custom_field_type_id": None,
        }

    def test_relate_custom_object_to_other_object_deduplicate(self):
        custom_object_uuid = uuid4()
        to_dedup = uuid4()
        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid,
            custom_objects=[
                {
                    "uuid": str(to_dedup),
                    "magic_string_prefix": None,
                },
                {
                    "uuid": str(to_dedup),
                    "magic_string_prefix": None,
                },
            ],
        )

        custom_object_uuid_to_relate = uuid4()
        custom_object_id_to_relate = random.randint(1, 1000)

        mock_db_entry = mock.Mock(name="db_entry")
        mock_db_entry.configure_mock(
            id=custom_object_id_to_relate,
            uuid=custom_object_uuid_to_relate,
        )

        to_dedup_id = random.randint(1, 1000)
        mock_db_entry_to_dedup = mock.Mock(name="db_entry_to_dedup")
        mock_db_entry_to_dedup.configure_mock(
            id=to_dedup_id,
            uuid=to_dedup,
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,  # find_by_uuid from command
            mock_custom_object,  # find_by_uuid during save
            mock_cotv,  # custom_object_type_version
            mock_cov,  # get id of custom_object_version
            mock_co,  # get id of custom_object
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = [
            [],  # id+uuid of custom objects linked from custom fields
            [
                mock_db_entry,
                mock_db_entry_to_dedup,
            ],  # id+uuid of custom objects linked "directly"
        ]
        self.session.query().filter().all.side_effect = [
            [],  # Currently existing CustomObjectRelationships
            [],  # id+uuid of documents that are linked from custom fields
            [],  # id+uuid of cases that are linked from custom fields
            [],  # id+uuid of cases that are linked "directly"
        ]

        self.cmd.relate_custom_object_to(
            custom_object_uuid=custom_object_uuid,
            cases=[],
            custom_objects=[custom_object_uuid_to_relate],
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 8

        insert_statement = calls[6][0][0]
        insert_compiled = insert_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled) == (
            "INSERT INTO custom_object_relationship "
            "(custom_object_id, custom_object_version_id, relationship_type, relationship_magic_string_prefix, related_custom_object_id, related_uuid, source_custom_field_type_id) "
            "VALUES "
            "(%(custom_object_id)s, "
            "%(custom_object_version_id)s, "
            "%(relationship_type)s, "
            "%(relationship_magic_string_prefix)s, "
            "%(related_custom_object_id)s, "
            "%(related_uuid)s::UUID, "
            "%(source_custom_field_type_id)s) "
            "RETURNING custom_object_relationship.id"
        )
        assert insert_compiled.params == {
            "custom_object_id": mock_co.id,
            "custom_object_version_id": mock_cov.id,
            "related_custom_object_id": custom_object_id_to_relate,
            "related_uuid": custom_object_uuid_to_relate,
            "relationship_magic_string_prefix": None,
            "relationship_type": "custom_object",
            "source_custom_field_type_id": None,
        }
        insert_statement = calls[7][0][0]
        insert_compiled = insert_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled) == (
            "INSERT INTO custom_object_relationship "
            "(custom_object_id, custom_object_version_id, relationship_type, relationship_magic_string_prefix, related_custom_object_id, related_uuid, source_custom_field_type_id) "
            "VALUES "
            "(%(custom_object_id)s, "
            "%(custom_object_version_id)s, "
            "%(relationship_type)s, "
            "%(relationship_magic_string_prefix)s, "
            "%(related_custom_object_id)s, "
            "%(related_uuid)s::UUID, "
            "%(source_custom_field_type_id)s) "
            "RETURNING custom_object_relationship.id"
        )
        assert insert_compiled.params == {
            "custom_object_id": mock_co.id,
            "custom_object_version_id": mock_cov.id,
            "related_custom_object_id": to_dedup_id,
            "related_uuid": to_dedup,
            "relationship_magic_string_prefix": None,
            "relationship_type": "custom_object",
            "source_custom_field_type_id": None,
        }

    def test_relate_custom_object_to_other_object_also_in_field(self):
        custom_object_uuid_to_relate = uuid4()
        custom_object_id_to_relate = random.randint(1, 1000)

        custom_object_uuid = uuid4()
        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid,
            custom_fields={
                "objectrelatie": {
                    "type": "relationship",
                    "value": [
                        {
                            "type": "relationship",
                            "value": str(custom_object_uuid_to_relate),
                            "specifics": {
                                "metadata": {
                                    "summary": "Some related object",
                                    "description": None,
                                },
                                "relationship_type": "custom_object",
                            },
                        }
                    ],
                }
            },
            custom_field_definition={
                "custom_fields": [
                    {
                        "label": "Object-relatie",
                        "name": "Objectrelatie",
                        "description": "sdf",
                        "external_description": "",
                        "is_required": False,
                        "is_hidden_field": False,
                        "multiple_values": True,
                        "attribute_uuid": str(uuid4()),
                        "options": [],
                        "custom_field_type": "relationship",
                        "magic_string": "objectrelatie",
                        "custom_field_specification": {
                            "name": "Objecttype v2",
                            "type": "custom_object",
                            "uuid": str(uuid4()),
                            "use_on_map": False,
                        },
                    }
                ]
            },
        )

        mock_db_entry = mock.Mock()
        mock_db_entry.configure_mock(
            id=custom_object_id_to_relate,
            uuid=custom_object_uuid_to_relate,
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,  # find_by_uuid from command
            mock_custom_object,  # find_by_uuid during save
            mock_cotv,  # custom_object_type_version
            mock_cov,  # get id of custom_object_version
            mock_co,  # get id of custom_object
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = [
            [
                mock_db_entry,
            ],  # id+uuid of custom objects linked from custom fields
            [
                mock_db_entry,
            ],  # id+uuid of custom objects linked "directly"
        ]
        self.session.query().filter().all.side_effect = [
            [],  # Currently existing CustomObjectRelationships
            [],  # id+uuid of documents that are linked from custom fields
            [],  # id+uuid of cases that are linked from custom fields
            [],  # id+uuid of cases that are linked "directly"
        ]

        self.cmd.relate_custom_object_to(
            custom_object_uuid=custom_object_uuid,
            cases=[],
            custom_objects=[custom_object_uuid_to_relate],
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 8

        insert_statement1 = calls[6][0][0]
        insert_compiled1 = insert_statement1.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled1) == (
            "INSERT INTO custom_object_relationship "
            "(custom_object_id, custom_object_version_id, relationship_type, relationship_magic_string_prefix, related_custom_object_id, related_uuid, source_custom_field_type_id) "
            "VALUES "
            "(%(custom_object_id)s, "
            "%(custom_object_version_id)s, "
            "%(relationship_type)s, "
            "%(relationship_magic_string_prefix)s, "
            "%(related_custom_object_id)s, "
            "%(related_uuid)s::UUID, "
            "%(source_custom_field_type_id)s) "
            "RETURNING custom_object_relationship.id"
        )
        assert insert_compiled1.params == {
            "custom_object_id": mock_co.id,
            "custom_object_version_id": mock_cov.id,
            "related_custom_object_id": custom_object_id_to_relate,
            "related_uuid": custom_object_uuid_to_relate,
            "relationship_magic_string_prefix": "objectrelatie",
            "relationship_type": "custom_object",
            "source_custom_field_type_id": None,
        }

        insert_statement2 = calls[7][0][0]
        insert_compiled2 = insert_statement2.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled2) == (
            "INSERT INTO custom_object_relationship "
            "(custom_object_id, custom_object_version_id, relationship_type, relationship_magic_string_prefix, related_custom_object_id, related_uuid, source_custom_field_type_id) "
            "VALUES "
            "(%(custom_object_id)s, "
            "%(custom_object_version_id)s, "
            "%(relationship_type)s, "
            "%(relationship_magic_string_prefix)s, "
            "%(related_custom_object_id)s, "
            "%(related_uuid)s::UUID, "
            "%(source_custom_field_type_id)s) "
            "RETURNING custom_object_relationship.id"
        )
        assert insert_compiled2.params == {
            "custom_object_id": mock_co.id,
            "custom_object_version_id": mock_cov.id,
            "related_custom_object_id": custom_object_id_to_relate,
            "related_uuid": custom_object_uuid_to_relate,
            "relationship_magic_string_prefix": None,
            "relationship_type": "custom_object",
            "source_custom_field_type_id": None,
        }

    def test_relate_custom_object_to_self(self):
        custom_object_uuid = uuid4()
        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = []
        self.session.query().filter().all.side_effect = []

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.relate_custom_object_to(
                custom_object_uuid=custom_object_uuid,
                cases=[],
                custom_objects=[custom_object_uuid],
            )


class TestUnrelateFrom(TestBase):
    def setup_method(self):
        self.redis = MockRedis()

        self.load_command_instance(
            case_management,
            inframocks={
                "redis": self.redis,
            },
        )

        self.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        self.cmd.user_info = self.user_info

    def test_unrelate_custom_object_to_other_object(self):
        custom_object_uuid = uuid4()
        custom_object_uuid_to_unrelate = uuid4()

        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid,
            custom_objects=[
                {
                    "uuid": str(custom_object_uuid_to_unrelate),
                    "magic_string_prefix": None,
                }
            ],
        )

        mock_custom_object_to_unrelate = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid_to_unrelate
        )
        custom_object_id_to_unrelate = random.randint(1, 1000)

        mock_db_entry = mock.Mock()
        mock_db_entry.configure_mock(
            id=custom_object_id_to_unrelate,
            uuid=custom_object_uuid_to_unrelate,
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,
            mock_custom_object_to_unrelate,
            mock_cotv,  # custom_object_type_version
            mock_cov,  # get id of custom_object_version
            mock_co,  # get id of custom_object
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = [
            [],  # id+uuid of custom objects linked from custom fields
            [],  # id+uuid of custom objects linked "directly"
        ]
        self.session.query().filter().all.side_effect = [
            [],  # Currently existing CustomObjectRelationships
            [],  # id+uuid of documents that are linked from custom fields
            [],  # id+uuid of cases that are linked from custom fields
            [],  # id+uuid of cases that are linked "directly"
        ]

        self.cmd.unrelate_custom_object_from(
            custom_object_uuid=custom_object_uuid,
            cases=[],
            custom_objects=[custom_object_uuid_to_unrelate],
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 6

        statement = calls[5][0][0]
        compiled = statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled).startswith("SELECT ")

        # INSERTs are done last in the "save" code

    def test_unrelate_custom_object_to_other_object_not_related(self):
        custom_object_uuid = uuid4()
        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid
        )

        custom_object_uuid_to_unrelate = uuid4()
        mock_custom_object_to_unrelate = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid_to_unrelate
        )
        custom_object_id_to_unrelate = random.randint(1, 1000)

        mock_db_entry = mock.Mock()
        mock_db_entry.configure_mock(
            id=custom_object_id_to_unrelate,
            uuid=custom_object_uuid_to_unrelate,
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,
            mock_custom_object_to_unrelate,
            mock_cotv,  # custom_object_type_version
            mock_cov,  # get id of custom_object_version
            mock_co,  # get id of custom_object
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = [
            [],  # id+uuid of custom objects linked from custom fields
            [
                mock_db_entry,
            ],  # id+uuid of custom objects linked "directly"
        ]
        self.session.query().filter().all.side_effect = [
            [],  # Currently existing CustomObjectRelationships
            [],  # id+uuid of documents that are linked from custom fields
            [],  # id+uuid of cases that are linked from custom fields
            [],  # id+uuid of cases that are linked "directly"
        ]

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.unrelate_custom_object_from(
                custom_object_uuid=custom_object_uuid,
                cases=[],
                custom_objects=[custom_object_uuid_to_unrelate],
            )

    def test_unrelate_custom_object_to_other_object_also_in_field(self):
        custom_object_uuid_to_unrelate = uuid4()
        custom_object_id_to_unrelate = random.randint(1, 1000)

        custom_object_uuid = uuid4()
        mock_custom_object = mock_custom_object_row(
            custom_object_uuid=custom_object_uuid,
            custom_objects=[
                {
                    "uuid": str(custom_object_uuid_to_unrelate),
                    "magic_string_prefix": None,
                }
            ],
            custom_fields={
                "objectrelatie": {
                    "type": "relationship",
                    "value": [
                        {
                            "type": "relationship",
                            "value": str(custom_object_uuid_to_unrelate),
                            "specifics": {
                                "metadata": {
                                    "summary": "Some related object",
                                    "description": None,
                                },
                                "relationship_type": "custom_object",
                            },
                        }
                    ],
                }
            },
            custom_field_definition={
                "custom_fields": [
                    {
                        "label": "Object-relatie",
                        "name": "Objectrelatie",
                        "description": "sdf",
                        "external_description": "",
                        "is_required": False,
                        "is_hidden_field": False,
                        "multiple_values": True,
                        "attribute_uuid": str(uuid4()),
                        "options": [],
                        "custom_field_type": "relationship",
                        "magic_string": "objectrelatie",
                        "custom_field_specification": {
                            "name": "Objecttype v2",
                            "type": "custom_object",
                            "uuid": str(uuid4()),
                            "use_on_map": False,
                        },
                    }
                ]
            },
        )

        mock_db_entry = mock.Mock()
        mock_db_entry.configure_mock(
            id=custom_object_id_to_unrelate,
            uuid=custom_object_uuid_to_unrelate,
        )

        mock_cotv = mock.Mock()
        mock_cotv.id = random.randint(1, 1000)
        mock_cov = mock.Mock()
        mock_cov.id = random.randint(1, 1000)
        mock_co = mock.Mock()
        mock_co.id = random.randint(1, 1000)

        self.session.execute().fetchall.side_effect = [
            [],  # related subjects/contacts
        ]
        self.session.execute().fetchone.side_effect = [
            mock_custom_object,  # find_by_uuid from command
            mock_custom_object,  # find_by_uuid during save
            mock_cotv,  # custom_object_type_version
            mock_cov,  # get id of custom_object_version
            mock_co,  # get id of custom_object
        ]
        self.session.execute.reset_mock()

        self.session.query().join().filter().all.side_effect = [
            [
                mock_db_entry,
            ],  # id+uuid of custom objects linked from custom fields
            [],  # id+uuid of custom objects linked "directly"
        ]
        self.session.query().filter().all.side_effect = [
            [],  # Currently existing CustomObjectRelationships
            [],  # id+uuid of documents that are linked from custom fields
            [],  # id+uuid of cases that are linked from custom fields
            [],  # id+uuid of cases that are linked "directly"
        ]

        self.cmd.unrelate_custom_object_from(
            custom_object_uuid=custom_object_uuid,
            cases=[],
            custom_objects=[custom_object_uuid_to_unrelate],
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 7

        # (Re-)insert of the relationship from the custom field
        insert_statement1 = calls[6][0][0]
        insert_compiled1 = insert_statement1.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled1) == (
            "INSERT INTO custom_object_relationship "
            "(custom_object_id, custom_object_version_id, relationship_type, relationship_magic_string_prefix, related_custom_object_id, related_uuid, source_custom_field_type_id) "
            "VALUES "
            "(%(custom_object_id)s, "
            "%(custom_object_version_id)s, "
            "%(relationship_type)s, "
            "%(relationship_magic_string_prefix)s, "
            "%(related_custom_object_id)s, "
            "%(related_uuid)s::UUID, "
            "%(source_custom_field_type_id)s) "
            "RETURNING custom_object_relationship.id"
        )
        assert insert_compiled1.params == {
            "custom_object_id": mock_co.id,
            "custom_object_version_id": mock_cov.id,
            "related_custom_object_id": custom_object_id_to_unrelate,
            "related_uuid": custom_object_uuid_to_unrelate,
            "relationship_magic_string_prefix": "objectrelatie",
            "relationship_type": "custom_object",
            "source_custom_field_type_id": None,
        }
