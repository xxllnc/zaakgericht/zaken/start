# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetRelatedObjectsForCustomObject(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_related_objects_for_custom_object(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]
        object_type_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            uuid=object_uuids[0],
            title="Object1",
            object_type_uuid=object_type_uuids[0],
            object_type_name="ObjectType1",
            magic_strings=[],
            magic_strings_inbound=[],
        )
        mock_objects[1].configure_mock(
            uuid=object_uuids[1],
            title="Object2",
            object_type_uuid=object_type_uuids[1],
            object_type_name="ObjectType2",
            magic_strings=["a", None, None],
            magic_strings_inbound=[None, None, "b"],
            relation_source=[None, object_uuids[1], None],
        )

        self.session.execute().fetchall.return_value = mock_objects

        objects = self.qry.get_related_objects_for_custom_object(
            object_uuid=str(uuid4())
        )
        assert isinstance(objects, list)
        assert len(objects) == 2

        assert objects[0].uuid == object_uuids[0]
        assert objects[0].entity_id == mock_objects[0].uuid
        assert objects[0].object_type.entity_id == object_type_uuids[0]

        assert objects[1].uuid == object_uuids[1]
        assert objects[1].entity_id == mock_objects[1].uuid
        assert objects[1].object_type.entity_id == object_type_uuids[1]

        assert objects[1].relation_source == [object_uuids[1]]

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "WITH anon_1 AS \n"
            "(SELECT custom_object.uuid AS uuid, custom_object_relationship.related_custom_object_id AS custom_object_id, custom_object_version.title AS title, custom_object_type.uuid AS object_type_uuid, custom_object_type_version.name AS object_type_name, custom_object_relationship.relationship_magic_string_prefix AS magic_string, :param_1 AS magic_string_inbound, CASE WHEN (custom_object_relationship.relationship_magic_string_prefix IS NULL) THEN CAST(:param_2 AS UUID) END AS relation_source \n"
            "FROM custom_object_relationship JOIN custom_object ON custom_object.id = custom_object_relationship.related_custom_object_id AND custom_object_relationship.relationship_type = :relationship_type_1 JOIN custom_object_version ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id \n"
            "WHERE custom_object_relationship.custom_object_id = (SELECT custom_object.id \n"
            "FROM custom_object \n"
            "WHERE custom_object.uuid = :uuid_1) UNION ALL SELECT custom_object.uuid AS uuid, custom_object_relationship.custom_object_id AS custom_object_id, custom_object_version.title AS title, custom_object_type.uuid AS object_type_uuid, custom_object_type_version.name AS object_type_name, :param_3 AS magic_string, custom_object_relationship.relationship_magic_string_prefix AS magic_string_inbound, CASE WHEN (custom_object_relationship.relationship_magic_string_prefix IS NULL) THEN custom_object.uuid END AS relation_source \n"
            "FROM custom_object_relationship JOIN custom_object ON custom_object.id = custom_object_relationship.custom_object_id JOIN custom_object_version ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id \n"
            "WHERE custom_object_relationship.relationship_type = :relationship_type_2 AND custom_object_relationship.related_custom_object_id = (SELECT custom_object.id \n"
            "FROM custom_object \n"
            "WHERE custom_object.uuid = :uuid_2))\n"
            " SELECT anon_1.uuid, anon_1.title, anon_1.object_type_uuid, anon_1.object_type_name, array_agg(anon_1.magic_string) AS magic_strings, array_agg(anon_1.magic_string_inbound) AS magic_strings_inbound, array_agg(anon_1.relation_source) AS relation_source \n"
            "FROM anon_1 GROUP BY anon_1.uuid, anon_1.title, anon_1.object_type_uuid, anon_1.object_type_name"
        )

    def test_get_related_objects_for_subject(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]
        object_type_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            uuid=object_uuids[0],
            title="Object1",
            object_type_uuid=object_type_uuids[0],
            object_type_name="ObjectType1",
            magic_strings=None,
            magic_strings_inbound=None,
        )
        mock_objects[1].configure_mock(
            uuid=object_uuids[1],
            title="Object2",
            object_type_uuid=object_type_uuids[1],
            object_type_name="ObjectType2",
            magic_strings=None,
            magic_strings_inbound=None,
        )

        self.session.execute().fetchall.return_value = mock_objects

        objects = self.qry.get_related_custom_objects_for_subject(
            subject_uuid=str(uuid4())
        )
        assert isinstance(objects, list)
        assert len(objects) == 2

        assert objects[0].uuid == object_uuids[0]
        assert objects[0].entity_id == mock_objects[0].uuid
        assert objects[0].object_type.entity_id == object_type_uuids[0]

        assert objects[1].uuid == object_uuids[1]
        assert objects[1].entity_id == mock_objects[1].uuid
        assert objects[1].object_type.entity_id == object_type_uuids[1]

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT custom_object.uuid, custom_object_version.title, custom_object_type.uuid AS object_type_uuid, custom_object_type_version.name AS object_type_name, :param_1 AS magic_strings, :param_2 AS magic_strings_inbound, ARRAY[custom_object.uuid] AS relation_source \n"
            "FROM custom_object JOIN custom_object_version ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id \n"
            "WHERE custom_object_version.id IN (SELECT custom_object_relationship.custom_object_version_id \n"
            "FROM custom_object_relationship \n"
            "WHERE custom_object_relationship.relationship_type = :relationship_type_1 AND custom_object_relationship.related_uuid = :related_uuid_1)"
        )
        assert query.params["param_1"] is None
        assert query.params["param_2"] is None
