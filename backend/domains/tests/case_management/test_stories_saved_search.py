# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import minty.exceptions
import pytest
import random
from minty.cqrs.test import TestBase
from sqlalchemy import exc as sqlalchemy_exc
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import saved_search as entity
from zsnl_domains.case_management.entities.saved_search_label import (
    SavedSearchLabel,
)


class TestGetSavedSearch(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_get_saved_search(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_uuid = uuid4()
        label_uuid_1 = uuid4()
        label_uuid_2 = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "keyword",
                        "values": [{"label": "label", "value": "test"}],
                    },
                    {
                        "type": "attributes.value",
                        "values": {
                            "label": "straat_kenmerk",
                            "attributeUuid": "e059983b-ab2c-49f0-8bd2-e14cb98a3d2e",
                            "type": "bag_openbareruimte",
                            "magic_string": "straat_kenmerk",
                            "values": [
                                {
                                    "label": "'t Straatje, Batenburg",
                                    "value": "weg-7f422d3a51b1d601a844713b257eb517",
                                    "type": "openbareruimte",
                                    "id": "openbareruimte-0296300000000345",
                                }
                            ],
                        },
                    },
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {"label": "label", "value": custom_object_uuid}
                        ],
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": False,
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                    "sort_order": 1,
                },
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "write",
                    "sort_order": 1,
                },
            ],
            labels=[
                {
                    "name": "label2",
                    "uuid": label_uuid_1,
                },
                {
                    "name": "label1",
                    "uuid": label_uuid_2,
                },
            ],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        saved_search = self.qry.get_saved_search(saved_search_uuid)

        assert saved_search == entity.SavedSearch.parse_obj(
            {
                "entity_id": saved_search_uuid,
                "uuid": saved_search_uuid,
                "name": "Test Name",
                "kind": "custom_object",
                "owner": {
                    "entity_id": owner_uuid,
                    "uuid": owner_uuid,
                    "entity_meta_summary": "Owner Name",
                },
                "permissions": [
                    {
                        "group_id": group_uuid,
                        "role_id": role_uuid,
                        "permission": [
                            entity.SavedSearchPermission.read,
                            entity.SavedSearchPermission.write,
                        ],
                        "sort_order": 1,
                    },
                ],
                "filters": {
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "keyword",
                            "values": [{"label": "label", "value": "test"}],
                        },
                        {
                            "type": "attributes.value",
                            "values": {
                                "label": "straat_kenmerk",
                                "attributeUuid": "e059983b-ab2c-49f0-8bd2-e14cb98a3d2e",
                                "type": "bag_openbareruimte",
                                "magic_string": "straat_kenmerk",
                                "values": [
                                    {
                                        "label": "'t Straatje, Batenburg",
                                        "value": "weg-7f422d3a51b1d601a844713b257eb517",
                                        "type": "openbareruimte",
                                        "id": "openbareruimte-0296300000000345",
                                    }
                                ],
                            },
                        },
                        {
                            "type": "relationship.custom_object_type",
                            "values": [
                                {"label": "label", "value": custom_object_uuid}
                            ],
                        },
                    ],
                },
                "columns": [
                    {
                        "source": ["attributes", "a"],
                        "label": "a",
                        "type": "string",
                        "visible": True,
                    },
                    {
                        "source": ["attributes", "b"],
                        "label": "b",
                        "type": "datetime",
                        "visible": False,
                    },
                ],
                "sort_column": "a",
                "sort_order": "asc",
                "entity_meta_authorizations": {
                    entity.AuthorizationLevel.read,
                    entity.AuthorizationLevel.readwrite,
                    entity.AuthorizationLevel.admin,
                },
                "labels": [
                    SavedSearchLabel(
                        entity_type="saved_seach_label",
                        entity_id=label_uuid_1,
                        entity_meta_summary=None,
                        entity_relationships=[],
                        entity_meta__fields=["entity_meta_summary"],
                        entity_id__fields=["uuid"],
                        entity_changelog=[],
                        entity_data={},
                        uuid=label_uuid_1,
                        name="label2",
                    ),
                    SavedSearchLabel(
                        entity_type="saved_seach_label",
                        entity_id=label_uuid_2,
                        entity_meta_summary=None,
                        entity_relationships=[],
                        entity_meta__fields=["entity_meta_summary"],
                        entity_id__fields=["uuid"],
                        entity_changelog=[],
                        entity_data={},
                        uuid=label_uuid_2,
                        name="label1",
                    ),
                ],
                "_event_service": None,
                "date_created": "2023-01-15 13:50:43",
                "date_updated": "2023-01-15 13:50:43",
                "updated_by": {
                    "entity_id": owner_uuid,
                    "uuid": owner_uuid,
                    "summary": "Owner Name",
                },
                "template": "standard",
            }
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject_1.uuid, %(json_build_object_2)s, CAST(subject_1.properties AS JSON) ->> %(param_1)s) AS owner, json_build_object(%(json_build_object_3)s, subject_2.uuid, %(json_build_object_4)s, CAST(subject_2.properties AS JSON) ->> %(param_2)s) AS updated_by, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, saved_search.date_created, saved_search.date_updated, saved_search.template, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, anon_2.labels AS labels, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_3)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_4)s END AS anon_3 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_owner, anon_2.labels AS labels \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, groups.uuid, %(json_build_object_6)s, roles.uuid, %(json_build_object_7)s, saved_search_permission.permission, %(json_build_object_8)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_9)s, saved_search_labels.uuid, %(json_build_object_10)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_4 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))) AND saved_search.uuid = %(uuid_2)s::UUID"
        )

        assert saved_search_query.params == {
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "param_1": "displayname",
            "json_build_object_3": "uuid",
            "json_build_object_4": "summary",
            "param_2": "displayname",
            "permission_1": "read",
            "param_3": "read",
            "permission_2": "write",
            "param_4": "readwrite",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "param_5": True,
            "param_6": False,
            "json_build_object_5": "group_id",
            "json_build_object_6": "role_id",
            "json_build_object_7": "permission",
            "json_build_object_8": "sort_order",
            "json_build_object_9": "uuid",
            "json_build_object_10": "name",
            "position_1": "|read",
            "uuid_2": saved_search_uuid,
        }

    def test_get_saved_search_notfound(self):
        self.session.execute().fetchone.return_value = None
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.get_saved_search(uuid4())


class TestListSavedSearch(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_list_saved_search(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        object_type_uuid = uuid4()
        label_uuid_1 = uuid4()
        label_uuid_2 = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {"label": "label", "value": object_type_uuid}
                        ],
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
            ],
            authorizations=["read", "readwrite"],
            user_is_owner=False,
            labels=[
                {
                    "name": "label2",
                    "uuid": label_uuid_1,
                },
                {
                    "name": "label1",
                    "uuid": label_uuid_2,
                },
            ],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        self.session.execute().fetchall.return_value = [mock_db_row]
        self.session.execute().scalar.return_value = 31337
        self.session.execute.reset_mock()

        result = self.qry.list_saved_search(page=5, page_size=10)

        assert isinstance(result, minty.entity.EntityCollection)
        assert result.total_results == 31337
        assert result.entities == [
            entity.SavedSearch.parse_obj(
                {
                    "entity_id": saved_search_uuid,
                    "uuid": saved_search_uuid,
                    "name": "Test Name",
                    "kind": "custom_object",
                    "owner": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "entity_meta_summary": "Owner Name",
                    },
                    "permissions": [
                        {
                            "group_id": group_uuid,
                            "role_id": role_uuid,
                            "permission": ["read"],
                        },
                    ],
                    "filters": {
                        "kind_type": "custom_object",
                        "filters": [
                            {
                                "type": "relationship.custom_object_type",
                                "values": [
                                    {
                                        "label": "label",
                                        "value": object_type_uuid,
                                    }
                                ],
                            },
                        ],
                    },
                    "columns": [
                        {
                            "source": ["attributes", "a"],
                            "label": "a",
                            "type": "string",
                        },
                        {
                            "source": ["attributes", "b"],
                            "label": "b",
                            "type": "datetime",
                        },
                    ],
                    "labels": [
                        SavedSearchLabel(
                            entity_type="saved_seach_label",
                            entity_id=label_uuid_1,
                            entity_meta_summary=None,
                            entity_relationships=[],
                            entity_meta__fields=["entity_meta_summary"],
                            entity_id__fields=["uuid"],
                            entity_changelog=[],
                            entity_data={},
                            uuid=label_uuid_1,
                            name="label2",
                        ),
                        SavedSearchLabel(
                            entity_type="saved_seach_label",
                            entity_id=label_uuid_2,
                            entity_meta_summary=None,
                            entity_relationships=[],
                            entity_meta__fields=["entity_meta_summary"],
                            entity_id__fields=["uuid"],
                            entity_changelog=[],
                            entity_data={},
                            uuid=label_uuid_2,
                            name="label1",
                        ),
                    ],
                    "sort_column": "a",
                    "sort_order": "asc",
                    "entity_meta_authorizations": {
                        entity.AuthorizationLevel.read,
                        entity.AuthorizationLevel.readwrite,
                    },
                    "_event_service": None,
                    "date_created": "2023-01-15 13:50:43",
                    "date_updated": "2023-01-15 13:50:43",
                    "updated_by": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "summary": "Owner Name",
                    },
                    "template": "standard",
                }
            )
        ]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject_1.uuid, %(json_build_object_2)s, CAST(subject_1.properties AS JSON) ->> %(param_1)s) AS owner, json_build_object(%(json_build_object_3)s, subject_2.uuid, %(json_build_object_4)s, CAST(subject_2.properties AS JSON) ->> %(param_2)s) AS updated_by, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, saved_search.date_created, saved_search.date_updated, saved_search.template, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, anon_2.labels AS labels, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_3)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_4)s END AS anon_3 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_owner, anon_2.labels AS labels \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, groups.uuid, %(json_build_object_6)s, roles.uuid, %(json_build_object_7)s, saved_search_permission.permission, %(json_build_object_8)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_9)s, saved_search_labels.uuid, %(json_build_object_10)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_4 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID))) ORDER BY saved_search.name \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )
        assert saved_search_query.params == {
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "param_1": "displayname",
            "json_build_object_3": "uuid",
            "json_build_object_4": "summary",
            "param_2": "displayname",
            "permission_1": "read",
            "param_3": "read",
            "permission_2": "write",
            "param_4": "readwrite",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "param_5": True,
            "param_6": False,
            "json_build_object_5": "group_id",
            "json_build_object_6": "role_id",
            "json_build_object_7": "permission",
            "json_build_object_8": "sort_order",
            "json_build_object_9": "uuid",
            "json_build_object_10": "name",
            "position_1": "|read",
            "param_7": 10,
            "param_8": 40,
        }
        saved_search_query = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_1)s, groups.uuid, %(json_build_object_2)s, roles.uuid, %(json_build_object_3)s, saved_search_permission.permission, %(json_build_object_4)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, saved_search_labels.uuid, %(json_build_object_6)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_3 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))"
        )
        assert saved_search_query.params == {
            "param_1": 1,
            "json_build_object_1": "group_id",
            "json_build_object_2": "role_id",
            "json_build_object_3": "permission",
            "json_build_object_4": "sort_order",
            "json_build_object_5": "uuid",
            "json_build_object_6": "name",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "position_1": "|read",
        }

    def test_list_saved_search_with_kind_filter(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "write",
                },
            ],
            labels=[],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Updated Name"},
            template="standard",
        )

        self.session.execute().fetchall.return_value = [mock_db_row]
        self.session.execute().scalar.return_value = 31337
        self.session.execute.reset_mock()

        result = self.qry.list_saved_search(
            filter={"kind": "custom_object"}, page=5, page_size=10
        )

        assert isinstance(result, minty.entity.EntityCollection)
        assert result.total_results == 31337
        assert result.entities == [
            entity.SavedSearch.parse_obj(
                {
                    "entity_id": saved_search_uuid,
                    "uuid": saved_search_uuid,
                    "name": "Test Name",
                    "kind": "custom_object",
                    "owner": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "entity_meta_summary": "Owner Name",
                        "entity_type": "employee",
                    },
                    "permissions": [
                        {
                            "group_id": group_uuid,
                            "role_id": role_uuid,
                            "permission": ["read", "write"],
                        },
                    ],
                    "filters": {
                        "kind_type": "custom_object",
                        "filters": [
                            {
                                "type": "relationship.custom_object_type",
                                "values": [
                                    {
                                        "label": "label",
                                        "value": custom_object_type_uuid,
                                    }
                                ],
                            },
                        ],
                    },
                    "columns": [
                        {
                            "source": ["attributes", "a"],
                            "label": "a",
                            "type": "string",
                        },
                        {
                            "source": ["attributes", "b"],
                            "label": "b",
                            "type": "datetime",
                        },
                    ],
                    "labels": [],
                    "sort_column": "a",
                    "sort_order": "asc",
                    "entity_meta_authorizations": {
                        entity.AuthorizationLevel.read,
                        entity.AuthorizationLevel.readwrite,
                        entity.AuthorizationLevel.admin,
                    },
                    "_event_service": None,
                    "date_created": "2023-01-15 13:50:43",
                    "date_updated": "2023-01-15 13:50:43",
                    "updated_by": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "summary": "Updated Name",
                        "entity_type": "employee",
                    },
                }
            )
        ]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject_1.uuid, %(json_build_object_2)s, CAST(subject_1.properties AS JSON) ->> %(param_1)s) AS owner, json_build_object(%(json_build_object_3)s, subject_2.uuid, %(json_build_object_4)s, CAST(subject_2.properties AS JSON) ->> %(param_2)s) AS updated_by, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, saved_search.date_created, saved_search.date_updated, saved_search.template, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, anon_2.labels AS labels, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_3)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_4)s END AS anon_3 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_owner, anon_2.labels AS labels \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, groups.uuid, %(json_build_object_6)s, roles.uuid, %(json_build_object_7)s, saved_search_permission.permission, %(json_build_object_8)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_9)s, saved_search_labels.uuid, %(json_build_object_10)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_4 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))) AND saved_search.kind = %(kind_1)s ORDER BY saved_search.name \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )
        assert saved_search_query.params == {
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "param_1": "displayname",
            "json_build_object_3": "uuid",
            "json_build_object_4": "summary",
            "param_2": "displayname",
            "permission_1": "read",
            "param_3": "read",
            "permission_2": "write",
            "param_4": "readwrite",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "param_5": True,
            "param_6": False,
            "json_build_object_5": "group_id",
            "json_build_object_6": "role_id",
            "json_build_object_7": "permission",
            "json_build_object_8": "sort_order",
            "json_build_object_9": "uuid",
            "json_build_object_10": "name",
            "position_1": "|read",
            "kind_1": entity.SavedSearchKind.custom_object,
            "param_7": 10,
            "param_8": 40,
        }
        saved_search_query = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_1)s, groups.uuid, %(json_build_object_2)s, roles.uuid, %(json_build_object_3)s, saved_search_permission.permission, %(json_build_object_4)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, saved_search_labels.uuid, %(json_build_object_6)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_3 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))) AND saved_search.kind = %(kind_1)s"
        )
        assert saved_search_query.params == {
            "param_1": 1,
            "json_build_object_1": "group_id",
            "json_build_object_2": "role_id",
            "json_build_object_3": "permission",
            "json_build_object_4": "sort_order",
            "json_build_object_5": "uuid",
            "json_build_object_6": "name",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "position_1": "|read",
            "kind_1": "custom_object",
        }

    def test_list_saved_search_with_label_filter(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()
        label_uuid_1 = uuid4()
        label_uuid_2 = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "write",
                },
            ],
            labels=[
                {
                    "name": "label1",
                    "uuid": label_uuid_1,
                },
                {
                    "name": "label2",
                    "uuid": label_uuid_2,
                },
            ],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Updated Name"},
            template="standard",
        )

        self.session.execute().fetchall.return_value = [mock_db_row]
        self.session.execute().scalar.return_value = 31337
        self.session.execute.reset_mock()

        result = self.qry.list_saved_search(
            filter={"labels": "label1,label2"}, page=5, page_size=10
        )

        assert isinstance(result, minty.entity.EntityCollection)
        assert result.total_results == 31337

        assert result.entities == [
            entity.SavedSearch.parse_obj(
                {
                    "entity_id": saved_search_uuid,
                    "uuid": saved_search_uuid,
                    "name": "Test Name",
                    "kind": "custom_object",
                    "owner": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "entity_meta_summary": "Owner Name",
                    },
                    "permissions": [
                        {
                            "group_id": group_uuid,
                            "role_id": role_uuid,
                            "permission": ["read", "write"],
                        },
                    ],
                    "filters": {
                        "kind_type": "custom_object",
                        "filters": [
                            {
                                "type": "relationship.custom_object_type",
                                "values": [
                                    {
                                        "label": "label",
                                        "value": custom_object_type_uuid,
                                    }
                                ],
                            },
                        ],
                    },
                    "columns": [
                        {
                            "source": ["attributes", "a"],
                            "label": "a",
                            "type": "string",
                        },
                        {
                            "source": ["attributes", "b"],
                            "label": "b",
                            "type": "datetime",
                        },
                    ],
                    "labels": [
                        SavedSearchLabel(
                            entity_type="saved_seach_label",
                            entity_id=label_uuid_1,
                            entity_meta_summary=None,
                            entity_relationships=[],
                            entity_meta__fields=["entity_meta_summary"],
                            entity_id__fields=["uuid"],
                            entity_changelog=[],
                            entity_data={},
                            uuid=label_uuid_1,
                            name="label1",
                        ),
                        SavedSearchLabel(
                            entity_type="saved_seach_label",
                            entity_id=label_uuid_2,
                            entity_meta_summary=None,
                            entity_relationships=[],
                            entity_meta__fields=["entity_meta_summary"],
                            entity_id__fields=["uuid"],
                            entity_changelog=[],
                            entity_data={},
                            uuid=label_uuid_2,
                            name="label2",
                        ),
                    ],
                    "sort_column": "a",
                    "sort_order": "asc",
                    "entity_meta_authorizations": {
                        entity.AuthorizationLevel.read,
                        entity.AuthorizationLevel.readwrite,
                        entity.AuthorizationLevel.admin,
                    },
                    "date_created": "2023-01-15 13:50:43",
                    "date_updated": "2023-01-15 13:50:43",
                    "updated_by": {
                        "uuid": owner_uuid,
                        "entity_id": owner_uuid,
                        "summary": "Updated Name",
                    },
                    "_event_service": None,
                }
            )
        ]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject_1.uuid, %(json_build_object_2)s, CAST(subject_1.properties AS JSON) ->> %(param_1)s) AS owner, json_build_object(%(json_build_object_3)s, subject_2.uuid, %(json_build_object_4)s, CAST(subject_2.properties AS JSON) ->> %(param_2)s) AS updated_by, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, saved_search.date_created, saved_search.date_updated, saved_search.template, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, anon_2.labels AS labels, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_3)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_4)s END AS anon_3 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_owner, anon_2.labels AS labels \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, groups.uuid, %(json_build_object_6)s, roles.uuid, %(json_build_object_7)s, saved_search_permission.permission, %(json_build_object_8)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_9)s, saved_search_labels.uuid, %(json_build_object_10)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_4 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))) AND (EXISTS (SELECT %(param_7)s AS anon_5 \n"
            "FROM saved_search_label_mapping JOIN saved_search_labels ON saved_search_label_mapping.saved_search_id = saved_search.id AND saved_search_label_mapping.label_id = saved_search_labels.id \n"
            "WHERE saved_search_labels.label IN (__[POSTCOMPILE_label_1]))) ORDER BY saved_search.name \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )

        assert saved_search_query.params == {
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "param_1": "displayname",
            "json_build_object_3": "uuid",
            "json_build_object_4": "summary",
            "param_2": "displayname",
            "permission_1": "read",
            "param_3": "read",
            "permission_2": "write",
            "param_4": "readwrite",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "param_5": True,
            "param_6": False,
            "json_build_object_5": "group_id",
            "json_build_object_6": "role_id",
            "json_build_object_7": "permission",
            "json_build_object_8": "sort_order",
            "json_build_object_9": "uuid",
            "json_build_object_10": "name",
            "position_1": "|read",
            "param_7": 1,
            "label_1": ["label1", "label2"],
            "param_8": 10,
            "param_9": 40,
        }
        saved_search_query = call_list[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(saved_search_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_1)s, groups.uuid, %(json_build_object_2)s, roles.uuid, %(json_build_object_3)s, saved_search_permission.permission, %(json_build_object_4)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, saved_search_labels.uuid, %(json_build_object_6)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_3 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))) AND (EXISTS (SELECT %(param_2)s AS anon_4 \n"
            "FROM saved_search_label_mapping JOIN saved_search_labels ON saved_search_label_mapping.saved_search_id = saved_search.id AND saved_search_label_mapping.label_id = saved_search_labels.id \n"
            "WHERE saved_search_labels.label IN (%(label_1_1)s, %(label_1_2)s)))"
        )

        assert saved_search_query.params == {
            "param_1": 1,
            "json_build_object_1": "group_id",
            "json_build_object_2": "role_id",
            "json_build_object_3": "permission",
            "json_build_object_4": "sort_order",
            "json_build_object_5": "uuid",
            "json_build_object_6": "name",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "position_1": "|read",
            "param_2": 1,
            "label_1_1": "label1",
            "label_1_2": "label2",
        }


class TestCreateSavedSearch(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_create_saved_search(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        object_type_uuid = uuid4()

        self.cmd.create_saved_search(
            uuid=saved_search_uuid,
            name="Test Search",
            kind="custom_object",
            filters={
                "kind_type": "custom_object",
                "operator": "and",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {"label": "label", "value": object_type_uuid}
                        ],
                    },
                ],
            },
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read"],
                }
            ],
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": False,
                },
            ],
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        # Query the owner's full name
        owner_name_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(owner_name_query)
            == "SELECT CAST(subject.properties AS JSON) ->> %(param_1)s AS summary \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID"
        )
        assert owner_name_query.params == {
            "param_1": "displayname",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_info.user_uuid,
        }

        # Insert the saved search
        insert_saved_search = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_saved_search) == (
            "INSERT INTO saved_search (uuid, name, kind, owner_id, filters, columns, sort_column, sort_order, date_created, template) VALUES (%(uuid)s::UUID, %(name)s, %(kind)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID), %(filters)s::JSONB, %(columns)s::JSONB, %(sort_column)s, %(sort_order)s, %(date_created)s, %(template)s) RETURNING saved_search.id"
        )

        date_created = insert_saved_search.params.get("date_created")
        assert insert_saved_search.params == {
            "uuid": str(saved_search_uuid),
            "name": "Test Search",
            "kind": "custom_object",
            "subject_type_1": "employee",
            "uuid_1": str(self.cmd.user_info.user_uuid),
            "filters": {
                "kind_type": "custom_object",
                "operator": "and",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {"label": "label", "value": str(object_type_uuid)}
                        ],
                    },
                ],
            },
            "columns": [
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": False,
                },
            ],
            "sort_column": None,
            "sort_order": "desc",
            "date_created": date_created,
            "template": "standard",
        }

        # Insert permissions
        insert_permissions = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_permissions) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission, sort_order) VALUES (%(saved_search_id_m0)s, (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_1)s::UUID), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_2)s::UUID), %(permission_m0)s, %(sort_order_m0)s)"
        )
        assert insert_permissions.params == {
            "saved_search_id_m0": saved_search_db_id,
            "uuid_1": str(group_uuid),
            "uuid_2": str(role_uuid),
            "permission_m0": "read",
            "sort_order_m0": None,
        }

        # Call procedure to update permissions
        procedure_call = call_list[3][0][0]

        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_create_saved_search_with_multiple_filters(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        object_type_uuid = uuid4()

        self.cmd.create_saved_search(
            uuid=saved_search_uuid,
            name="Test Search",
            kind="custom_object",
            filters={
                "kind_type": "custom_object",
                "operator": "or",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {"label": "label", "value": object_type_uuid}
                        ],
                    },
                    {
                        "type": "attributes.last_modified",
                        "values": [
                            {
                                "type": "absolute",
                                "value": "2022-01-01T00:00:00Z",
                                "operator": "lt",
                            },
                            {
                                "type": "relative",
                                "value": "+P0Y0M0DT3H0M0S",
                                "operator": "lt",
                            },
                            {
                                "type": "range",
                                "start_value": "2022-08-04T11:00:00.000Z",
                                "end_value": "2022-08-04T11:00:00.000Z",
                                "time_set_by_user": True,
                            },
                        ],
                    },
                ],
            },
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read"],
                }
            ],
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": True,
                },
            ],
            sort_column="random",
            sort_order="asc",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        # Query the owner's full name
        owner_name_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(owner_name_query)
            == "SELECT CAST(subject.properties AS JSON) ->> %(param_1)s AS summary \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID"
        )
        assert owner_name_query.params == {
            "param_1": "displayname",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_info.user_uuid,
        }

        # Insert the saved search
        insert_saved_search = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_saved_search) == (
            "INSERT INTO saved_search (uuid, name, kind, owner_id, filters, columns, sort_column, sort_order, date_created, template) VALUES (%(uuid)s::UUID, %(name)s, %(kind)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID), %(filters)s::JSONB, %(columns)s::JSONB, %(sort_column)s, %(sort_order)s, %(date_created)s, %(template)s) RETURNING saved_search.id"
        )
        date_created = insert_saved_search.params.get("date_created")
        assert insert_saved_search.params == {
            "uuid": str(saved_search_uuid),
            "name": "Test Search",
            "kind": "custom_object",
            "subject_type_1": "employee",
            "uuid_1": str(self.cmd.user_info.user_uuid),
            "filters": {
                "kind_type": "custom_object",
                "operator": "or",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {"label": "label", "value": str(object_type_uuid)}
                        ],
                    },
                    {
                        "type": "attributes.last_modified",
                        "values": [
                            {
                                "type": "absolute",
                                "value": "2022-01-01T00:00:00+00:00",
                                "operator": "lt",
                            },
                            {
                                "type": "relative",
                                "value": "+P0Y0M0DT3H0M0S",
                                "operator": "lt",
                            },
                            {
                                "type": "range",
                                "start_value": "2022-08-04T11:00:00+00:00",
                                "end_value": "2022-08-04T11:00:00+00:00",
                                "time_set_by_user": True,
                            },
                        ],
                    },
                ],
            },
            "columns": [
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": True,
                },
            ],
            "sort_column": "random",
            "sort_order": "asc",
            "date_created": date_created,
            "template": "standard",
        }

        # Insert permissions
        insert_permissions = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_permissions) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission, sort_order) VALUES (%(saved_search_id_m0)s, (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_1)s::UUID), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_2)s::UUID), %(permission_m0)s, %(sort_order_m0)s)"
        )
        assert insert_permissions.params == {
            "saved_search_id_m0": saved_search_db_id,
            "uuid_1": str(group_uuid),
            "uuid_2": str(role_uuid),
            "permission_m0": "read",
            "sort_order_m0": None,
        }

        # Call procedure to update permissions
        procedure_call = call_list[3][0][0]

        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_create_saved_search_dupe(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.side_effect = [
            fetchone_rv_mock,
            sqlalchemy_exc.IntegrityError("X", {}, "X"),
        ]
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_saved_search(
                uuid=saved_search_uuid,
                name="Test Search",
                kind="case",
                filters={
                    "kind_type": "case",
                    "filters": [
                        {
                            "type": "attributes.confidentiality",
                            "values": ["public"],
                            "options": {"options": "value"},
                        },
                    ],
                },
                permissions=[
                    {
                        "group_id": group_uuid,
                        "role_id": role_uuid,
                        "permission": ["read"],
                    }
                ],
                columns=[
                    {
                        "source": ["attributes", "a"],
                        "label": "a",
                        "type": "string",
                    },
                    {
                        "source": ["attributes", "b"],
                        "label": "b",
                        "type": "datetime",
                    },
                ],
                sort_column="random",
                sort_order="asc",
            )

    def test_create_saved_search_bad_permission(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_saved_search(
                uuid=saved_search_uuid,
                name="Test Search",
                kind="case",
                filters={
                    "kind_type": "case",
                    "filters": [
                        {
                            "type": "attributes.confidentiality",
                            "values": ["public"],
                        },
                    ],
                },
                permissions=[
                    {
                        "group_id": group_uuid,
                        "role_id": role_uuid,
                        "permission": ["write"],
                    }
                ],
                columns=[
                    {
                        "source": ["attributes", "a"],
                        "label": "a",
                        "type": "string",
                    },
                    {
                        "source": ["attributes", "b"],
                        "label": "b",
                        "type": "datetime",
                    },
                ],
                sort_column="random",
                sort_order="asc",
            )

    def test_create_saved_search_case(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        case_type_uuid = uuid4()
        department_uuid = uuid4()

        self.cmd.create_saved_search(
            uuid=saved_search_uuid,
            name="Test Search",
            kind="case",
            filters={
                "kind_type": "case",
                "filters": [
                    {
                        "type": "relationship.case_type.id",
                        "values": [
                            {"label": "label", "value": case_type_uuid}
                        ],
                        "operator": "or",
                        "options": {"option": "value"},
                    },
                    {
                        "type": "attributes.department_role",
                        "values": [
                            {
                                "department": {
                                    "value": str(department_uuid),
                                    "label": "department_label",
                                },
                                "role": {
                                    "value": str(role_uuid),
                                    "label": "role_label",
                                },
                            }
                        ],
                    },
                ],
            },
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read"],
                }
            ],
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": True,
                },
            ],
            sort_column="random",
            sort_order="asc",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        # Insert the saved search
        insert_saved_search = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_saved_search) == (
            "INSERT INTO saved_search (uuid, name, kind, owner_id, filters, columns, sort_column, sort_order, date_created, template) VALUES (%(uuid)s::UUID, %(name)s, %(kind)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID), %(filters)s::JSONB, %(columns)s::JSONB, %(sort_column)s, %(sort_order)s, %(date_created)s, %(template)s) RETURNING saved_search.id"
        )
        date_created = insert_saved_search.params.get("date_created")
        assert insert_saved_search.params == {
            "uuid": str(saved_search_uuid),
            "name": "Test Search",
            "kind": "case",
            "subject_type_1": "employee",
            "uuid_1": str(self.cmd.user_info.user_uuid),
            "filters": {
                "kind_type": "case",
                "filters": [
                    {
                        "options": {"option": "value"},
                        "type": "relationship.case_type.id",
                        "values": [
                            {"label": "label", "value": str(case_type_uuid)}
                        ],
                        "operator": "or",
                    },
                    {
                        "type": "attributes.department_role",
                        "values": [
                            {
                                "department": {
                                    "value": str(department_uuid),
                                    "label": "department_label",
                                },
                                "role": {
                                    "value": str(role_uuid),
                                    "label": "role_label",
                                },
                            }
                        ],
                    },
                ],
            },
            "columns": [
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": True,
                },
            ],
            "sort_column": "random",
            "sort_order": "asc",
            "date_created": date_created,
            "template": "standard",
        }

        # Insert permissions
        insert_permissions = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_permissions) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission, sort_order) VALUES (%(saved_search_id_m0)s, (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_1)s::UUID), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_2)s::UUID), %(permission_m0)s, %(sort_order_m0)s)"
        )
        assert insert_permissions.params == {
            "saved_search_id_m0": saved_search_db_id,
            "uuid_1": str(group_uuid),
            "uuid_2": str(role_uuid),
            "permission_m0": "read",
            "sort_order_m0": None,
        }

        # Call procedure to update permissions
        procedure_call = call_list[3][0][0]

        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_create_saved_search_case_with_multiple_filters(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()

        self.cmd.create_saved_search(
            uuid=saved_search_uuid,
            name="Test Search",
            kind="case",
            filters={
                "kind_type": "case",
                "filters": [
                    {
                        "type": "attributes.payment_status",
                        "values": ["pending", "success"],
                        "options": {"option": "value"},
                    },
                    {
                        "type": "attributes.result",
                        "values": ["aangegaan", "aangehouden"],
                    },
                    {
                        "type": "attributes.channel_of_contact",
                        "values": ["email"],
                    },
                    {
                        "type": "attributes.case_location",
                        "values": [
                            {
                                "label": "P40 straat 1, Schiphol",
                                "value": "adr-af21f072d6e6b41c3c01d56a5c73b036",
                                "type": "nummeraanduiding",
                                "id": "nummeraanduiding-0394200001003719",
                            }
                        ],
                    },
                    {
                        "type": "attributes.registration_date",
                        "values": [
                            {
                                "operator": "lt",
                                "type": "absolute",
                                "value": "2023-09-13T09:01:09.235Z",
                            }
                        ],
                    },
                    {
                        "type": "attributes.completion_date",
                        "values": [
                            {
                                "operator": "gt",
                                "type": "absolute",
                                "value": "2023-09-13T09:01:09.235Z",
                            }
                        ],
                    },
                ],
            },
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read"],
                }
            ],
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": True,
                },
            ],
            sort_column="random",
            sort_order="asc",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        # Query the owner's full name
        owner_name_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(owner_name_query)
            == "SELECT CAST(subject.properties AS JSON) ->> %(param_1)s AS summary \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID"
        )
        assert owner_name_query.params == {
            "param_1": "displayname",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_info.user_uuid,
        }

        # Insert the saved search
        insert_saved_search = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_saved_search) == (
            "INSERT INTO saved_search (uuid, name, kind, owner_id, filters, columns, sort_column, sort_order, date_created, template) VALUES (%(uuid)s::UUID, %(name)s, %(kind)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID), %(filters)s::JSONB, %(columns)s::JSONB, %(sort_column)s, %(sort_order)s, %(date_created)s, %(template)s) RETURNING saved_search.id"
        )
        date_created = insert_saved_search.params.get("date_created")
        assert insert_saved_search.params == {
            "uuid": str(saved_search_uuid),
            "name": "Test Search",
            "kind": "case",
            "subject_type_1": "employee",
            "uuid_1": str(self.cmd.user_info.user_uuid),
            "filters": {
                "kind_type": "case",
                "filters": [
                    {
                        "options": {"option": "value"},
                        "type": "attributes.payment_status",
                        "values": ["pending", "success"],
                    },
                    {
                        "type": "attributes.result",
                        "values": ["aangegaan", "aangehouden"],
                    },
                    {
                        "type": "attributes.channel_of_contact",
                        "values": ["email"],
                    },
                    {
                        "type": "attributes.case_location",
                        "values": [
                            {
                                "label": "P40 straat 1, Schiphol",
                                "value": "adr-af21f072d6e6b41c3c01d56a5c73b036",
                                "type": "nummeraanduiding",
                                "id": "nummeraanduiding-0394200001003719",
                            }
                        ],
                    },
                    {
                        "type": "attributes.registration_date",
                        "values": [
                            {
                                "operator": "lt",
                                "type": "absolute",
                                "value": "2023-09-13T09:01:09.235000+00:00",
                            }
                        ],
                    },
                    {
                        "type": "attributes.completion_date",
                        "values": [
                            {
                                "type": "absolute",
                                "value": "2023-09-13T09:01:09.235000+00:00",
                                "operator": "gt",
                            }
                        ],
                    },
                ],
            },
            "columns": [
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                    "visible": True,
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "visible": True,
                },
            ],
            "sort_column": "random",
            "sort_order": "asc",
            "date_created": date_created,
            "template": "standard",
        }

        # Insert permissions
        insert_permissions = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_permissions) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission, sort_order) VALUES (%(saved_search_id_m0)s, (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_1)s::UUID), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_2)s::UUID), %(permission_m0)s, %(sort_order_m0)s)"
        )
        assert insert_permissions.params == {
            "saved_search_id_m0": saved_search_db_id,
            "uuid_1": str(group_uuid),
            "uuid_2": str(role_uuid),
            "permission_m0": "read",
            "sort_order_m0": None,
        }

        # Call procedure to update permissions
        procedure_call = call_list[3][0][0]

        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )


class TestDeleteSavedSearch(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_delete_saved_search(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        label_id_1 = 1
        label_uuid_1 = uuid4()
        label_uuid_2 = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "case",
                "filters": [
                    {
                        "type": "attributes.confidentiality",
                        "values": ["public"],
                    },
                    {
                        "type": "relationship.requestor.id",
                        "values": [
                            {
                                "label": "relationshipid",
                                "value": str(uuid4()),
                                "type": "person",
                            }
                        ],
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "magic_string": "b_magic_string",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[],
            labels=[
                {
                    "name": "label1",
                    "uuid": label_uuid_1,
                },
                {
                    "name": "label2",
                    "uuid": label_uuid_2,
                },
            ],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        existing_labels = [mock.MagicMock()]
        existing_labels[0].configure_mock(
            id=label_id_1, uuid=label_uuid_1, label="label1"
        )
        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute().fetchall.return_value = existing_labels
        self.session.execute.reset_mock()

        self.cmd.delete_saved_search(uuid=saved_search_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 6

        # Query the owner's full name
        delete_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert (
            str(delete_query)
            == "DELETE FROM saved_search WHERE saved_search.uuid = %(uuid_1)s::UUID"
        )
        assert delete_query.params == {"uuid_1": str(saved_search_uuid)}

        unused_label_query = call_list[4][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(unused_label_query)
            == "SELECT saved_search_labels.uuid, saved_search_labels.label \n"
            "FROM saved_search_labels \n"
            "WHERE (saved_search_labels.id NOT IN (SELECT saved_search_label_mapping.label_id \n"
            "FROM saved_search_label_mapping))"
        )

        unused_label_delete_query = call_list[5][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(unused_label_delete_query)
            == "DELETE FROM saved_search_labels WHERE saved_search_labels.uuid = %(uuid_1)s::UUID"
        )


class TestUpdateSavedSearch(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_update_saved_search_only_content(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column=None,
            sort_order="asc",
            permission_rows=[],
            user_is_owner=False,
            authorizations=["read", "readwrite"],
            labels=[],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        self.cmd.update_saved_search(
            uuid=saved_search_uuid,
            name="New Test Name",
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "c"],
                    "label": "c",
                    "type": "string",
                    "visible": False,
                },
            ],
            sort_column=None,
            sort_order="desc",
            template="standard",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        # Query the owner's full name
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert (
            str(update_query)
            == "UPDATE saved_search SET name=%(name)s, filters=%(filters)s::JSONB, columns=%(columns)s::JSONB, sort_column=%(sort_column)s, sort_order=%(sort_order)s, date_updated=%(date_updated)s, updated_by=(SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID), template=%(template)s WHERE saved_search.uuid = %(uuid_2)s::UUID"
        )
        date_updated = update_query.params.get("date_updated")
        uuid_1 = update_query.params.get("uuid_1")
        uuid2 = update_query.params.get("uuid_2")
        assert update_query.params == {
            "columns": [
                {
                    "label": "c",
                    "source": ["attributes", "c"],
                    "type": "string",
                    "visible": False,
                }
            ],
            "filters": {
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": str(custom_object_type_uuid),
                            }
                        ],
                    }
                ],
            },
            "name": "New Test Name",
            "sort_column": None,
            "sort_order": "desc",
            "date_updated": date_updated,
            "uuid_1": str(uuid_1),
            "subject_type_1": "employee",
            "uuid_2": uuid2,
            "template": "standard",
        }

        with pytest.raises(minty.exceptions.Forbidden):
            # Cannot update permissions if user does not have admin rights
            self.cmd.update_saved_search(
                uuid=saved_search_uuid,
                name="New Test Name",
                filters={
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "relationship.custom_object_type",
                            "values": [
                                {
                                    "label": "label",
                                    "value": str(custom_object_type_uuid),
                                }
                            ],
                        }
                    ],
                },
                columns=[
                    {
                        "source": ["attributes", "c"],
                        "label": "c",
                        "type": "string",
                    },
                ],
                sort_column="c",
                sort_order="desc",
                permissions=[],
            )

    def test_update_saved_search_content_and_permission(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()

        group2_uuid = uuid4()
        role2_uuid = uuid4()
        custom_object_type_uuid = uuid4()
        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group2_uuid,
                    "role_id": role2_uuid,
                    "permission": "read",
                },
            ],
            labels=[],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        self.cmd.update_saved_search(
            uuid=saved_search_uuid,
            name="New Test Name",
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "c"],
                    "label": "c",
                    "type": "string",
                },
            ],
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read", "write"],
                    "order": 1,
                },
            ],
            template="archive_export",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 7

        # Query the owner's full name
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert (
            str(update_query)
            == "UPDATE saved_search SET name=%(name)s, filters=%(filters)s::JSONB, columns=%(columns)s::JSONB, sort_column=%(sort_column)s, sort_order=%(sort_order)s, date_updated=%(date_updated)s, updated_by=(SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID), template=%(template)s WHERE saved_search.uuid = %(uuid_2)s::UUID"
        )
        date_updated = update_query.params.get("date_updated")
        uuid1 = update_query.params.get("uuid_1")
        uuid2 = update_query.params.get("uuid_2")
        assert update_query.params == {
            "name": "New Test Name",
            "filters": {
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": str(custom_object_type_uuid),
                            }
                        ],
                    }
                ],
            },
            "columns": [
                {"source": ["attributes", "c"], "label": "c", "type": "string"}
            ],
            "sort_column": None,
            "sort_order": "desc",
            "date_updated": date_updated,
            "subject_type_1": "employee",
            "uuid_1": str(uuid1),
            "uuid_2": uuid2,
            "template": "archive_export",
        }
        # Delete permission
        insert_query = call_list[2][0][0].compile(dialect=postgresql.dialect())
        assert str(insert_query) == (
            "DELETE FROM saved_search_permission WHERE saved_search_permission.saved_search_id = (SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_1)s::UUID) AND saved_search_permission.group_id = (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_2)s::UUID) AND saved_search_permission.role_id = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_3)s::UUID) AND saved_search_permission.permission = %(permission_1)s AND saved_search_permission.sort_order IS NULL"
        )

        # Insert new permission
        insert_query = call_list[4][0][0].compile(dialect=postgresql.dialect())
        assert str(insert_query) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission, sort_order) VALUES ((SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_1)s::UUID), (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_2)s::UUID), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_3)s::UUID), %(permission_m0)s, %(sort_order_m0)s)"
        )
        assert insert_query.params == {
            "permission_m0": "write",
            "uuid_1": str(saved_search_uuid),
            "uuid_2": str(group_uuid),
            "uuid_3": str(role_uuid),
            "sort_order_m0": None,
        }

        # Call procedure to update permissions
        id_lookup_select = call_list[5][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(id_lookup_select) == (
            "SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_1)s::UUID"
        )
        assert id_lookup_select.params == {"uuid_1": str(saved_search_uuid)}

        procedure_call = call_list[6][0][0]
        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_update_saved_search_readonly_forbidden(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
            ],
            user_is_owner=False,
            authorizations=["read"],
            labels=[],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.Forbidden):
            self.cmd.update_saved_search(
                uuid=saved_search_uuid,
                name="New Test Name",
                filters={
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "relationship.custom_object_type",
                            "values": [
                                {
                                    "label": "label",
                                    "value": custom_object_type_uuid,
                                }
                            ],
                        }
                    ],
                },
                columns=[
                    {
                        "source": ["attributes", "c"],
                        "label": "c",
                        "type": "string",
                    },
                ],
                sort_column="c",
                sort_order="desc",
            )

    def test_update_saved_search_dupe_name(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
            ],
            user_is_owner=False,
            authorizations=["read", "readwrite", "admin"],
            labels=[],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        mock_execute = mock.Mock()
        mock_execute.fetchone.return_value = mock_db_row
        self.session.execute.side_effect = [
            mock_execute,
            sqlalchemy_exc.IntegrityError("X", {}, "X"),
        ]
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.update_saved_search(
                uuid=saved_search_uuid,
                name="New Test Name",
                filters={
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "relationship.custom_object_type",
                            "values": [
                                {
                                    "label": "label",
                                    "value": custom_object_type_uuid,
                                }
                            ],
                        }
                    ],
                },
                columns=[
                    {
                        "source": ["attributes", "c"],
                        "label": "c",
                        "type": "string",
                    },
                ],
                sort_column="c",
                sort_order="desc",
            )

    def test_set_labels_for_saved_search(self):
        saved_search_uuid = uuid4()
        label_id_1 = 1
        label_uuid_1 = uuid4()
        label_uuid_2 = uuid4()

        existing_labels = [mock.MagicMock()]
        existing_labels[0].configure_mock(
            id=label_id_1, uuid=label_uuid_1, label="label1"
        )

        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()

        group2_uuid = uuid4()
        role2_uuid = uuid4()
        custom_object_type_uuid = uuid4()
        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "values": [
                            {
                                "label": "label",
                                "value": custom_object_type_uuid,
                            }
                        ],
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group2_uuid,
                    "role_id": role2_uuid,
                    "permission": "read",
                },
            ],
            labels=[
                {
                    "name": "label1",
                    "uuid": label_uuid_1,
                },
                {
                    "name": "label2",
                    "uuid": label_uuid_2,
                },
            ],
            date_created="2023-01-15 13:50:43",
            date_updated="2023-01-15 13:50:43",
            updated_by={"uuid": owner_uuid, "summary": "Owner Name"},
            template="standard",
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute().fetchall.return_value = existing_labels
        self.session.execute.reset_mock()

        self.cmd.set_labels_for_saved_search(
            saved_search_uuids=[saved_search_uuid],
            saved_search_labels=["label1", "label3"],
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 7

        select_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(select_query) == (
            "SELECT saved_search_labels.uuid, saved_search_labels.label \n"
            "FROM saved_search_labels \n"
            "WHERE saved_search_labels.label IN (%(label_1_1)s, %(label_1_2)s)"
        )
        insert_query = call_list[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_query) == (
            "INSERT INTO saved_search_labels (uuid, label) VALUES (%(uuid)s::UUID, %(label)s) RETURNING saved_search_labels.id"
        )

        select_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(select_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject_1.uuid, %(json_build_object_2)s, CAST(subject_1.properties AS JSON) ->> %(param_1)s) AS owner, json_build_object(%(json_build_object_3)s, subject_2.uuid, %(json_build_object_4)s, CAST(subject_2.properties AS JSON) ->> %(param_2)s) AS updated_by, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, saved_search.date_created, saved_search.date_updated, saved_search.template, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, anon_2.labels AS labels, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_3)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_4)s END AS anon_3 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)) THEN %(param_5)s ELSE %(param_6)s END AS user_is_owner, anon_2.labels AS labels \n"
            "FROM saved_search JOIN subject AS subject_1 ON saved_search.owner_id = subject_1.id LEFT OUTER JOIN subject AS subject_2 ON saved_search.updated_by = subject_2.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_5)s, groups.uuid, %(json_build_object_6)s, roles.uuid, %(json_build_object_7)s, saved_search_permission.permission, %(json_build_object_8)s, saved_search_permission.sort_order)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id LEFT OUTER JOIN LATERAL (SELECT saved_search_label_mapping.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_9)s, saved_search_labels.uuid, %(json_build_object_10)s, saved_search_labels.label)) AS labels \n"
            "FROM saved_search_labels JOIN saved_search_label_mapping ON saved_search_labels.id = saved_search_label_mapping.label_id \n"
            "WHERE saved_search_label_mapping.saved_search_id = saved_search.id GROUP BY saved_search_label_mapping.saved_search_id) AS anon_2 ON anon_2.saved_search_id = saved_search.id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_4 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s::UUID)))) AND saved_search.uuid = %(uuid_2)s::UUID"
        )

        insert_query = call_list[3][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(insert_query) == (
            "INSERT INTO saved_search_label_mapping (label_id, saved_search_id) VALUES ((SELECT saved_search_labels.id \n"
            "FROM saved_search_labels \n"
            "WHERE saved_search_labels.uuid = %(uuid_1)s::UUID), (SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_2)s::UUID)) RETURNING saved_search_label_mapping.id"
        )

        delete_query = call_list[4][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(delete_query) == (
            "DELETE FROM saved_search_label_mapping WHERE saved_search_label_mapping.label_id = (SELECT saved_search_labels.id \n"
            "FROM saved_search_labels \n"
            "WHERE saved_search_labels.uuid = %(uuid_1)s::UUID) AND saved_search_label_mapping.saved_search_id = (SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_2)s::UUID)"
        )

        select_query = call_list[5][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(select_query) == (
            "SELECT saved_search_labels.uuid, saved_search_labels.label \n"
            "FROM saved_search_labels \n"
            "WHERE (saved_search_labels.id NOT IN (SELECT saved_search_label_mapping.label_id \n"
            "FROM saved_search_label_mapping))"
        )
        select_query = call_list[6][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(select_query) == (
            "DELETE FROM saved_search_labels WHERE saved_search_labels.uuid = %(uuid_1)s::UUID"
        )


class TestListSavedSearchLables(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_get_all_lables(self):
        label_uuid_1 = uuid4()
        label_uuid_2 = uuid4()

        existing_labels = [mock.MagicMock(), mock.MagicMock()]
        existing_labels[0].configure_mock(uuid=label_uuid_1, label="label1")
        existing_labels[1].configure_mock(uuid=label_uuid_2, label="label2")

        self.session.execute().fetchall.return_value = existing_labels
        self.session.execute.reset_mock()

        result = self.qry.get_saved_search_labels()
        assert result == [
            SavedSearchLabel(
                entity_type="saved_seach_label",
                entity_id=label_uuid_1,
                entity_meta_summary=None,
                entity_relationships=[],
                entity_meta__fields=["entity_meta_summary"],
                entity_id__fields=["uuid"],
                entity_changelog=[],
                entity_data={},
                uuid=label_uuid_1,
                name="label1",
                _event_service=None,
            ),
            SavedSearchLabel(
                entity_type="saved_seach_label",
                entity_id=label_uuid_2,
                entity_meta_summary=None,
                entity_relationships=[],
                entity_meta__fields=["entity_meta_summary"],
                entity_id__fields=["uuid"],
                entity_changelog=[],
                entity_data={},
                uuid=label_uuid_2,
                name="label2",
                _event_service=None,
            ),
        ]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_query = call_list[0][0][0].compile(dialect=postgresql.dialect())

        assert str(select_query) == (
            "SELECT saved_search_labels.uuid, saved_search_labels.label \n"
            "FROM saved_search_labels"
        )
