# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.exceptions
import pytest
from collections import defaultdict, namedtuple
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from unittest import mock
from unittest.mock import patch
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import Person


class TestAs_A_User_I_Want_To_CreateSubjectRelation(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    @patch(
        "zsnl_domains.case_management.repositories.person.PersonRepository.find_person_by_uuid"
    )
    @patch(
        "zsnl_domains.case_management.repositories.subject_relation.SubjectRelationRepository._assert_valid_magic_string_prefix"
    )
    @patch(
        "zsnl_domains.case_management.repositories.subject_relation.SubjectRelationRepository._assert_valid_role"
    )
    def test_create_subject_relation(
        self, mock_role, mock_magic_string, mock_person
    ):
        case_uuid = uuid4()
        subject_uuid = uuid4()
        subject = {"type": "person", "id": str(subject_uuid)}
        mock_person.return_value = Person(
            uuid=subject_uuid,
            name="beheerder",
            first_names="beheerder",
            surname="",
            contact_information={},
            authenticated=True,
            family_name=None,
            noble_title=None,
            date_of_birth=None,
            date_of_death=None,
            gender="F",
            inside_municipality=False,
            residence_address=None,
            correspondence_address=None,
            related_custom_object=None,
            country_code="6030",
            has_valid_address=True,
        )

        mock_role.return_value = mock.Mock()
        mock_magic_string.return_value = mock.Mock()

        self.session.execute().fetchone.side_effect = [
            namedtuple("case", "uuid type")(uuid=case_uuid, type="case"),
            namedtuple("id", "id")(id=123),
            mock.MagicMock(),
            namedtuple("id", "id")(id=123),
        ]
        self.session.reset_mock()

        self.cmd.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Ouder",
            magic_string_prefix="ouder",
            authorized=True,
            send_confirmation_email=True,
            permission=None,
        )

        call_list = self.session.execute.call_args_list
        insert_call_1 = call_list[1]
        insert_statement_1 = insert_call_1[0][0]

        assert isinstance(insert_statement_1, sql.dml.Insert)
        query = insert_statement_1.compile(dialect=postgresql.dialect())
        assert query.params["uuid_1"] == str(subject_uuid)
        assert (
            str(query)
            == "INSERT INTO gm_natuurlijk_persoon (gegevens_magazijn_id, burgerservicenummer, a_nummer, voorletters, voorvoegsel, voornamen, geslachtsnaam, geslachtsaanduiding, nationaliteitscode1, nationaliteitscode2, nationaliteitscode3, geboorteplaats, geboorteland, geboortedatum, aanhef_aanschrijving, voorletters_aanschrijving, voornamen_aanschrijving, naam_aanschrijving, voorvoegsel_aanschrijving, burgerlijke_staat, indicatie_geheim, import_datum, authenticatedby, verblijfsobject_id, datum_overlijden, aanduiding_naamgebruik, onderzoek_persoon, onderzoek_huwelijk, onderzoek_overlijden, onderzoek_verblijfplaats, partner_a_nummer, partner_burgerservicenummer, partner_voorvoegsel, partner_geslachtsnaam, datum_huwelijk, datum_huwelijk_ontbinding, landcode, naamgebruik, adellijke_titel) SELECT natuurlijk_persoon.id, natuurlijk_persoon.burgerservicenummer, natuurlijk_persoon.a_nummer, natuurlijk_persoon.voorletters, natuurlijk_persoon.voorvoegsel, natuurlijk_persoon.voornamen, natuurlijk_persoon.geslachtsnaam, natuurlijk_persoon.geslachtsaanduiding, natuurlijk_persoon.nationaliteitscode1, natuurlijk_persoon.nationaliteitscode2, natuurlijk_persoon.nationaliteitscode3, natuurlijk_persoon.geboorteplaats, natuurlijk_persoon.geboorteland, natuurlijk_persoon.geboortedatum, natuurlijk_persoon.aanhef_aanschrijving, natuurlijk_persoon.voorletters_aanschrijving, natuurlijk_persoon.voornamen_aanschrijving, natuurlijk_persoon.naam_aanschrijving, natuurlijk_persoon.voorvoegsel_aanschrijving, natuurlijk_persoon.burgerlijke_staat, natuurlijk_persoon.indicatie_geheim, natuurlijk_persoon.import_datum, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.verblijfsobject_id, natuurlijk_persoon.datum_overlijden, natuurlijk_persoon.aanduiding_naamgebruik, natuurlijk_persoon.onderzoek_persoon, natuurlijk_persoon.onderzoek_huwelijk, natuurlijk_persoon.onderzoek_overlijden, natuurlijk_persoon.onderzoek_verblijfplaats, natuurlijk_persoon.partner_a_nummer, natuurlijk_persoon.partner_burgerservicenummer, natuurlijk_persoon.partner_voorvoegsel, natuurlijk_persoon.partner_geslachtsnaam, natuurlijk_persoon.datum_huwelijk, natuurlijk_persoon.datum_huwelijk_ontbinding, natuurlijk_persoon.landcode, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.adellijke_titel \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID RETURNING gm_natuurlijk_persoon.id"
        )

        insert_call_2 = call_list[2]
        insert_statement_2 = insert_call_2[0][0]

        assert isinstance(insert_statement_2, sql.dml.Insert)
        query = insert_statement_2.compile(dialect=postgresql.dialect())

        assert (
            str(query)
            == "INSERT INTO gm_adres (straatnaam, huisnummer, huisnummertoevoeging, nadere_aanduiding, postcode, woonplaats, functie_adres, landcode, gemeentedeel, datum_aanvang_bewoning, woonplaats_id, gemeente_code, hash, import_datum, adres_buitenland1, adres_buitenland2, adres_buitenland3, natuurlijk_persoon_id) SELECT adres.straatnaam, adres.huisnummer, adres.huisnummertoevoeging, adres.nadere_aanduiding, adres.postcode, adres.woonplaats, adres.functie_adres, adres.landcode, adres.gemeentedeel, adres.datum_aanvang_bewoning, adres.woonplaats_id, adres.gemeente_code, adres.hash, adres.import_datum, adres.adres_buitenland1, adres.adres_buitenland2, adres.adres_buitenland3, 123 \n"
            "FROM adres, natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID AND adres.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres.functie_adres = %(functie_adres_1)s RETURNING gm_adres.id"
        )

    def test_update_subject_relation(self):
        relation_uuid = str(uuid4())
        mock_subject_relation_row1 = mock.MagicMock()
        mock_subject_relation_row1.configure_mock(
            uuid=relation_uuid,
            role="Familielid",
            magic_string_prefix="subject_relation",
            pip_authorized=False,
            permission=None,
            case_uuid=uuid4(),
            subject_uuid=uuid4(),
            subject_type="natuurlijk_persoon",
            name="Piet Friet",
            is_preset_client=False,
            source_custom_field_type_id=uuid4(),
            source_custom_field_type_name="Relation attribute name",
            source_custom_field_magic_string="relation_attribute_magic_string",
        )

        self.session.execute().fetchone.side_effect = [
            mock_subject_relation_row1,
            namedtuple("config", "value")(["Familielid"]),
            None,
            None,
        ]
        self.session.reset_mock()
        self.cmd.update_subject_relation(
            relation_uuid=relation_uuid,
            role="Ouder",
            magic_string_prefix="role",
            authorized=None,
            permission="write",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 5

        update_statement = call_list[4][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_update) == (
            "UPDATE zaak_betrokkenen SET uuid=%(uuid)s::UUID, rol=%(rol)s, magic_string_prefix=%(magic_string_prefix)s WHERE zaak_betrokkenen.uuid = %(uuid_1)s::UUID"
        )


class TestAs_A_User_I_Want_To_See_Related_Subjects_From_Fields(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, phases):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["uuid"] = case_type_uuid
        mock_case_type["id"] = "1"
        mock_case_type["case_type_uuid"] = uuid4()
        mock_case_type["name"] = "mock_case_type"
        mock_case_type["description"] = ""
        mock_case_type["identification"] = ""
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = "summary"
        mock_case_type["case_public_summary"] = "public_summary"
        mock_case_type["active"] = True
        mock_case_type["requestor"] = {
            "type_of_requestors": ["medewerker", "pip"],
            "use_for_correspondence": True,
        }
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "Name"}
        mock_case_type["phases"] = phases

        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"
        mock_case_type["results"] = [
            {
                "id": 156,
                "uuid": "a04d7629-ca9a-4246-8e9c-cc38b327e904",
                "name": "Bewaren",
                "result": "behaald",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
            {
                "id": 157,
                "uuid": "604138d9-e14f-4944-812c-12a098a68092",
                "name": "Nog een 2de resultaat",
                "result": "afgebroken",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
        ]

        return mock_case_type

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_relations_for_case_single_add(
        self,
        mock_find_case,
        mock_objects,
    ):
        case_uuid = uuid4()
        person_uuid = uuid4()
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps(
                    {
                        "value": str(person_uuid),
                        "type": "relationship",
                        "specifics": {
                            "relationship_type": "person",
                            "metadata": {
                                "summary": "contact summary",
                                "description": "description of the contact",
                            },
                        },
                    }
                )
            ],
            "subject_relation_invalid": [json.dumps({"id": str(person_uuid)})],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": field_type_uuids[0],
                            "field_magic_string": "not_a_relation",
                            "field_type": "text",
                            "name": "custom_field_1",
                            "public_name": "custom_field_1",
                        },
                        {
                            "uuid": field_type_uuids[1],
                            "field_magic_string": "not_a_subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "object",
                            "name": "custom_field_2",
                            "public_name": "custom_field_2",
                        },
                        {
                            "uuid": field_type_uuids[2],
                            "field_magic_string": "subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Aannemer",
                            "name": "custom_field_3",
                            "public_name": "custom_field_3",
                        },
                        {
                            "uuid": field_type_uuids[3],
                            "field_magic_string": "subject_relation_invalid",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Advocaat",
                            "name": "custom_field_4",
                            "public_name": "custom_field_4",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_person = mock.MagicMock()
        mock_person.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="test user",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="test user",
            external_identifier=None,
        )
        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity
        mock_subject_relation_rows = []

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            mock_subject_relation_rows,
            [],
        ]

        mock_case = mock.MagicMock()
        mock_case.uuid = case_uuid
        mock_case.id = 31337
        mock_case.name = "test"
        mock_case.meta = 3
        mock_find_case.return_value = mock_case
        self.session.reset_mock()

        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid), custom_fields=custom_fields
        )

        # Get a list of all inserts, updates and deletes
        dialect_pg = postgresql.dialect()
        mutations = [
            call[0][0].compile(dialect=dialect_pg)
            for call in self.session.execute.call_args_list
            if not isinstance(call[0][0], sql.expression.SelectBase)
        ]

        assert len(mutations) == 5

        # The other 4 are "housekeeping" (inserting into gm_natuurlijk_persoon etc.)
        assert str(mutations[4]) == (
            "INSERT INTO zaak_betrokkenen (zaak_id, betrokkene_type, betrokkene_id, gegevens_magazijn_id, uuid, subject_id, naam, rol, magic_string_prefix, pip_authorized, bibliotheek_kenmerken_id) VALUES ((SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID), %(betrokkene_type)s, %(betrokkene_id)s, (SELECT natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_2)s::UUID), %(uuid)s::UUID, %(subject_id)s::UUID, %(naam)s, %(rol)s, %(magic_string_prefix)s, %(pip_authorized)s, (SELECT bibliotheek_kenmerken.id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.uuid = %(uuid_3)s::UUID)) RETURNING zaak_betrokkenen.id"
        )

        assert mutations[4].params["betrokkene_id"] == 123
        assert mutations[4].params["betrokkene_type"] == "natuurlijk_persoon"
        assert mutations[4].params["uuid_2"] == str(person_uuid)
        assert mutations[4].params["rol"] == "Aannemer"
        assert mutations[4].params["magic_string_prefix"] == "subject_relation"
        assert mutations[4].params["pip_authorized"] is False

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_relations_for_case_single_add_subject(
        self,
        mock_find_case,
        mock_objects,
    ):
        case_uuid = uuid4()
        person_uuid = uuid4()
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4())]
        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps(
                    {
                        "value": str(person_uuid),
                        "type": "relationship",
                        "specifics": {
                            "relationship_type": "subject",
                            "metadata": {
                                "summary": "contact summary",
                                "description": "description of the contact",
                            },
                        },
                    },
                ),
            ],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": field_type_uuids[0],
                            "field_magic_string": "subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Aannemer",
                            "name": "custom_field_3",
                            "public_name": "custom_field_3",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_person = mock.MagicMock()
        mock_person.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="test user",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="test user",
            external_identifier=None,
        )
        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity
        mock_subject_relation_rows = []

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            # mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            mock_subject_relation_rows,
            [mock_person],  # get_persons_by_uuid
            [],  # get_organizations_by_uuid
            [],  # get_employees_by_uuid
        ]

        mock_case = mock.MagicMock()
        mock_case.uuid = case_uuid
        mock_case.id = 31337
        mock_case.name = "test"
        mock_case.meta = 3
        mock_find_case.return_value = mock_case
        self.session.reset_mock()

        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid), custom_fields=custom_fields
        )

        # Get a list of all inserts, updates and deletes
        dialect_pg = postgresql.dialect()
        mutations = [
            call[0][0].compile(dialect=dialect_pg)
            for call in self.session.execute.call_args_list
            if not isinstance(call[0][0], sql.expression.SelectBase)
        ]

        assert len(mutations) == 5

        # The other 4 are "housekeeping" (inserting into gm_natuurlijk_persoon etc.)
        assert str(mutations[4]) == (
            "INSERT INTO zaak_betrokkenen (zaak_id, betrokkene_type, betrokkene_id, gegevens_magazijn_id, uuid, subject_id, naam, rol, magic_string_prefix, pip_authorized, bibliotheek_kenmerken_id) VALUES ((SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID), %(betrokkene_type)s, %(betrokkene_id)s, (SELECT natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_2)s::UUID), %(uuid)s::UUID, %(subject_id)s::UUID, %(naam)s, %(rol)s, %(magic_string_prefix)s, %(pip_authorized)s, (SELECT bibliotheek_kenmerken.id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.uuid = %(uuid_3)s::UUID)) RETURNING zaak_betrokkenen.id"
        )

        assert mutations[4].params["betrokkene_id"] == 123
        assert mutations[4].params["betrokkene_type"] == "natuurlijk_persoon"
        assert mutations[4].params["uuid_2"] == str(person_uuid)
        assert mutations[4].params["rol"] == "Aannemer"
        assert mutations[4].params["magic_string_prefix"] == "subject_relation"
        assert mutations[4].params["pip_authorized"] is False

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_relations_for_case_single_add_subject_notfound(
        self,
        mock_find_case,
        mock_objects,
    ):
        case_uuid = uuid4()
        person_uuid = uuid4()
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4())]
        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps(
                    {
                        "value": str(person_uuid),
                        "type": "relationship",
                        "specifics": {
                            "relationship_type": "subject",
                            "metadata": {
                                "summary": "contact summary",
                                "description": "description of the contact",
                            },
                        },
                    },
                ),
            ],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": field_type_uuids[0],
                            "field_magic_string": "subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Aannemer",
                            "name": "custom_field_3",
                            "public_name": "custom_field_3",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity
        mock_subject_relation_rows = []

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            mock_subject_relation_rows,
            [],  # get_persons_by_uuid
            [],  # get_organizations_by_uuid
            [],  # get_employees_by_uuid
        ]

        mock_case = mock.MagicMock()
        mock_case.uuid = case_uuid
        mock_case.id = 31337
        mock_case.name = "test"
        mock_case.meta = 3
        mock_find_case.return_value = mock_case
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            self.cmd.synchronize_relations_for_case(
                case_uuid=str(case_uuid), custom_fields=custom_fields
            )

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_relations_for_case_make_field_empty(
        self, mock_find_case, mock_objects
    ):
        case_uuid = str(uuid4())
        person_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [],
        }
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": field_type_uuids[0],
                            "name": "custom_field_1",
                            "field_magic_string": "not_a_relation",
                            "field_type": "text",
                        },
                        {
                            "uuid": field_type_uuids[1],
                            "name": "custom_field_2",
                            "field_magic_string": "not_a_subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "object",
                        },
                        {
                            "uuid": field_type_uuids[2],
                            "name": "custom_field_3",
                            "field_magic_string": "subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Aannemer",
                        },
                        {  # To test the "Relation field not in custom field" path
                            "uuid": field_type_uuids[3],
                            "name": "custom_field_4",
                            "field_magic_string": "subject_relation_2",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Advocaat",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_person = mock.MagicMock()
        mock_person.uuid = person_uuid
        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity
        mock_subject_relation_row1 = mock.MagicMock()
        mock_subject_relation_row1.configure_mock(
            uuid=str(uuid4()),
            role="Familielid",
            magic_string_prefix="subject_relation",
            pip_authorized=False,
            permission=None,
            case_uuid=case_uuid,
            subject_uuid=person_uuid,
            subject_type="natuurlijk_persoon",
            name="Piet Friet",
            is_preset_client=False,
            source_custom_field_type_id=field_type_uuids[2],
            source_custom_field_type_name="relation attribute name",
            source_custom_field_magic_string="relation_attribute_magic_string",
        )

        mock_subject_relation_rows = [mock_subject_relation_row1]

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            mock_subject_relation_rows,
            [],
        ]
        mock_case = mock.Mock()
        mock_case.configure_mock(meta=3, prefix="", id=31337, uuid=case_uuid)
        mock_find_case.return_value = mock_case

        self.session.reset_mock()

        self.cmd.synchronize_relations_for_case(
            case_uuid=case_uuid, custom_fields=custom_fields
        )

        # Get a list of all inserts, updates and deletes
        dialect_pg = postgresql.dialect()
        mutations = [
            call[0][0].compile(dialect=dialect_pg)
            for call in self.session.execute.call_args_list
            if not isinstance(call[0][0], sql.expression.SelectBase)
        ]
        assert len(mutations) == 1

        assert str(mutations[0]) == (
            "UPDATE zaak_betrokkenen SET deleted=now() WHERE zaak_betrokkenen.uuid = %(uuid_1)s::UUID"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_relations_for_case_replace_field_value(
        self,
        mock_find_case,
        mock_objects,
    ):
        case_uuid = uuid4()
        person_uuid = uuid4()
        old_person_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps(
                    {
                        "value": str(person_uuid),
                        "type": "relationship",
                        "specifics": {
                            "relationship_type": "person",
                            "metadata": {
                                "summary": "contact summary",
                                "description": "description of the contact",
                            },
                        },
                    }
                )
            ],
        }
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": field_type_uuids[0],
                            "name": "custom_field_1",
                            "field_magic_string": "not_a_relation",
                            "field_type": "text",
                        },
                        {
                            "uuid": field_type_uuids[1],
                            "name": "custom_field_2",
                            "field_magic_string": "not_a_subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "object",
                        },
                        {
                            "uuid": field_type_uuids[2],
                            "name": "custom_field_3",
                            "field_magic_string": "subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Aannemer",
                        },
                        {  # To test the "Relation field not in custom field" path
                            "uuid": field_type_uuids[3],
                            "name": "custom_field_4",
                            "field_magic_string": "subject_relation_2",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Advocaat",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_person = mock.MagicMock()
        mock_person.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="test user",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            preferred_contact_channel="pip",
            summary="test user",
            external_identifier="IN_PROGRESS",
        )

        mock_person.related_custom_object_uuid = None

        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity

        mock_subject_relation_row1 = mock.MagicMock()
        mock_subject_relation_row1.configure_mock(
            uuid=uuid4(),
            role="Familielid",
            magic_string_prefix="subject_relation",
            pip_authorized=False,
            permission=None,
            case_uuid=case_uuid,
            subject_uuid=old_person_uuid,
            subject_type="natuurlijk_persoon",
            name="Piet Friet",
            is_preset_client=False,
            source_custom_field_type_id=field_type_uuids[2],
            source_custom_field_type_name="relation attribute name",
            source_custom_field_magic_string="relation_attribute_magic_string",
        )

        mock_subject_relation_rows = [mock_subject_relation_row1]

        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            mock_subject_relation_rows,
            [],
        ]

        mock_case = mock.MagicMock()
        mock_case.uuid = case_uuid
        mock_case.id = 31337
        mock_case.meta = 3
        mock_case.name = "testcase"
        mock_find_case.return_value = mock_case

        self.session.reset_mock()
        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid), custom_fields=custom_fields
        )

        # Get a list of all inserts, updates and deletes
        dialect_pg = postgresql.dialect()
        mutations = [
            call[0][0].compile(dialect=dialect_pg)
            for call in self.session.execute.call_args_list
            if not isinstance(call[0][0], sql.expression.SelectBase)
        ]

        assert len(mutations) == 6

        # The other 4 are "housekeeping" (inserting into gm_natuurlijk_persoon etc.)
        assert str(mutations[0]) == (
            "UPDATE zaak_betrokkenen SET deleted=now() WHERE zaak_betrokkenen.uuid = %(uuid_1)s::UUID"
        )
        assert mutations[0].params == {
            "uuid_1": str(mock_subject_relation_row1.uuid)
        }

        assert str(mutations[5]) == (
            "INSERT INTO zaak_betrokkenen (zaak_id, betrokkene_type, betrokkene_id, gegevens_magazijn_id, uuid, subject_id, naam, rol, magic_string_prefix, pip_authorized, bibliotheek_kenmerken_id) VALUES ((SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID), %(betrokkene_type)s, %(betrokkene_id)s, (SELECT natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_2)s::UUID), %(uuid)s::UUID, %(subject_id)s::UUID, %(naam)s, %(rol)s, %(magic_string_prefix)s, %(pip_authorized)s, (SELECT bibliotheek_kenmerken.id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.uuid = %(uuid_3)s::UUID)) RETURNING zaak_betrokkenen.id"
        )

        assert mutations[5].params["betrokkene_id"] == 123
        assert mutations[5].params["betrokkene_type"] == "natuurlijk_persoon"
        assert mutations[5].params["uuid_2"] == str(person_uuid)
        assert mutations[5].params["rol"] == "Aannemer"
        assert mutations[5].params["magic_string_prefix"] == "subject_relation"
        assert mutations[5].params["pip_authorized"] is False

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_synchronize_relations_for_case_replace_field_value_no_change(
        self,
        mock_find_case,
        mock_objects,
    ):
        case_uuid = uuid4()
        person_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps({"id": person_uuid, "type": "person"})
            ],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": field_type_uuids[0],
                            "field_magic_string": "not_a_relation",
                            "field_type": "text",
                            "name": "custom_field_1",
                            "public_name": "custom_field_1",
                        },
                        {
                            "uuid": field_type_uuids[1],
                            "field_magic_string": "not_a_subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "object",
                            "name": "custom_field_2",
                            "public_name": "custom_field_2",
                        },
                        {
                            "uuid": field_type_uuids[2],
                            "field_magic_string": "subject_relation",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Aannemer",
                            "name": "custom_field_3",
                            "public_name": "custom_field_3",
                        },
                        {  # To test the "Relation field not in custom field" path
                            "uuid": field_type_uuids[3],
                            "field_magic_string": "subject_relation_2",
                            "field_type": "relationship",
                            "relationship_type": "subject",
                            "relationship_subject_role": "Advocaat",
                            "name": "custom_field_4",
                            "public_name": "custom_field_4",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_person = mock.MagicMock()
        mock_person.configure_mock(
            uuid=person_uuid,
            authenticated=False,
            authenticatedby="zaaksysyteem",
            initials="",
            first_names="B",
            insertions="M",
            family_name="family_name",
            surname="surname",
            noble_title="Mrs",
            inside_municipality=True,
            address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
        )

        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity

        mock_subject_relation_row1 = mock.MagicMock()
        mock_subject_relation_row1.configure_mock(
            uuid=str(uuid4()),
            role="Familielid",
            magic_string_prefix="subject_relation",
            pip_authorized=False,
            permission=None,
            case_uuid=case_uuid,
            subject_uuid=person_uuid,
            subject_type="natuurlijk_persoon",
            name="Piet Friet",
            is_preset_client=False,
            source_custom_field_type_id=field_type_uuids[2],
            source_custom_field_type_name="relation attribute name",
            source_custom_field_magic_string="relation_attribute_magic_string",
        )

        mock_subject_relation_rows = [mock_subject_relation_row1]

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            mock_subject_relation_rows,
            [],
        ]

        mock_case = mock.Mock()
        mock_case.uuid = case_uuid
        mock_case.name = "testcase"
        mock_case.meta = 3
        mock_case.id = 31337

        mock_find_case.return_value = mock_case
        self.session.reset_mock()
        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid), custom_fields=custom_fields
        )

        #     # Get a list of all inserts, updates and deletes
        dialect_pg = postgresql.dialect()
        mutations = [
            call[0][0].compile(dialect=dialect_pg)
            for call in self.session.execute.call_args_list
            if not isinstance(call[0][0], sql.selectable.Select)
        ]

        assert len(mutations) == 0

    @mock.patch.object(
        case_management.repositories.case_type.CaseTypeRepository, "logger"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    def test_case_version_not_found_error(
        self,
        mock_objects,
        mock_case_type_repo_logger,
    ):
        case_uuid = str(uuid4())
        person_uuid = uuid4()

        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps({"id": str(person_uuid), "type": "person"})
            ],
            "subject_relation_invalid": [json.dumps({"id": str(person_uuid)})],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_person = mock.MagicMock()
        mock_subject_relation_rows = []

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            None,
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            [1],  # is_admin (case ACL)
            mock_subject_relation_rows,
            [],
        ]

        self.session.reset_mock()

        with pytest.raises(minty.exceptions.NotFound) as excinfo:
            self.cmd.synchronize_relations_for_case(
                case_uuid=case_uuid, custom_fields=custom_fields
            )
        assert excinfo.value.args == (
            f"Case type version for case with uuid: '{case_uuid}' not found.",
            "case_type/version_not_found",
        )

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            minty.exceptions.NotFound(
                "Case type not found"
            ),  # When find_case_type_version_for_case query throws NotFound Error
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            [1],  # is_admin (case ACL)
            mock_subject_relation_rows,
            [],
        ]

        with pytest.raises(minty.exceptions.NotFound) as excinfo:
            self.cmd.synchronize_relations_for_case(
                case_uuid=case_uuid, custom_fields=custom_fields
            )
        assert excinfo.value.args == (
            f"Case type version for case with uuid: '{case_uuid}' not found.",
            "case_type/version_not_found",
        )
        mock_case_type_repo_logger.warning.assert_called_once_with(
            "Caught exception during case_type_version query: Case type not found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.object_relation.ObjectRelationRepository.find_object_relations_for_case"
    )
    def test_case_type_node_not_found_error(
        self,
        mock_objects,
    ):
        case_uuid = str(uuid4())
        person_uuid = uuid4()

        custom_fields = {
            "not_a_relation": ["value"],
            "not_a_subject_relation": ["value"],
            "subject_relation": [
                json.dumps({"id": str(person_uuid), "type": "person"})
            ],
            "subject_relation_invalid": [json.dumps({"id": str(person_uuid)})],
        }

        mock_person = mock.MagicMock()

        mock_objects_entity = mock.Mock()
        mock_objects.return_value = mock_objects_entity
        mock_subject_relation_rows = []

        self.session.execute().fetchone.side_effect = [
            None,
            None,
            None,  # "is_simple_case_type"
            mock_person,
            [123],  # person_snapshot_id
            [234],  # residence_address_snapshot_id
            None,  # correspondence_address_snapshot_id
            [["CustomRole", "CustomRole2"]],  # Custom relation roles
            None,  # Role + subject combination exists for case?
            None,  # Magic string prefix is already in existing ZaakBetrokkenen?
        ]
        self.session.execute().fetchall.side_effect = [
            [1],  # is_admin (case ACL)
            mock_subject_relation_rows,
            [],
        ]

        self.session.reset_mock()

        with pytest.raises(minty.exceptions.NotFound) as excinfo:
            self.cmd.synchronize_relations_for_case(
                case_uuid=case_uuid, custom_fields=custom_fields
            )
        assert excinfo.value.args == (
            f"Case type version for case: '{case_uuid}' not found.",
            "case_type/version_not_found",
        )


class TestEnqueueSubjectRelationEmail(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )
        self.cmd.user_info = self.user_info

    def test_enqueue_subject_relation_email(self):
        relation_uuid = uuid4()

        mock_subject_relation_row = mock.Mock()
        mock_subject_relation_row.configure_mock(
            uuid=relation_uuid,
            role="Familielid",
            magic_string_prefix="subject_relation",
            pip_authorized=False,
            permission=None,
            case_uuid=uuid4(),
            subject_uuid=uuid4(),
            subject_type="natuurlijk_persoon",
            name="Piet Friet",
            is_preset_client=False,
            source_custom_field_type_id=uuid4(),
            source_custom_field_type_name="Relation attribute name",
            source_custom_field_magic_string="relation_attribute_magic_string",
        )

        self.session.execute().fetchone.side_effect = [
            mock_subject_relation_row
        ]
        self.session.reset_mock()

        self.cmd.enqueue_subject_relation_email(
            subject_relation_uuid=str(relation_uuid)
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2

        compiled_insert = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_insert)
            == "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s::UUID, %(object_id)s::UUID, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s::UUID, %(priority)s, %(metadata)s)"
        )
