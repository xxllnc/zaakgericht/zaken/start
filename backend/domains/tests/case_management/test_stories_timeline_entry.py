# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from collections import namedtuple
from datetime import UTC, datetime, timezone
from dateutil.tz import tzoffset, tzutc
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import TimelineEntry
from zsnl_domains.case_management.repositories.timeline_entry import (
    _check_none,
    _generate_content_for_mail,
    _get_entity_data,
)


class TestHelpers:
    def test__check_none(self) -> None:
        items1: set[str] = set()
        entity_data1: dict = {}
        assert _check_none(items=items1, entity_data=entity_data1)

        items2: set[str] = {"bcc"}
        entity_data2: dict = {}
        assert _check_none(items=items2, entity_data=entity_data2)

        items3: set[str] = {"bcc", "cc"}
        entity_data3: dict = {"bcc": None, "cc": None}
        assert _check_none(items=items3, entity_data=entity_data3)

        items4: set[str] = {"bcc", "cc"}
        entity_data4: dict = {"bcc": "bcc@example.com", "cc": None}
        assert not _check_none(items=items4, entity_data=entity_data4)

        items5: set[str] = {"bcc", "cc"}
        entity_data5: dict = {"bcc": "bcc@example.com", "cc": "cc@example.com"}
        assert not _check_none(items=items5, entity_data=entity_data5)

    def test__get_entity_data(self) -> None:
        item1: str = ""
        entity_data1: dict = {}
        assert _get_entity_data(item=item1, entity_data=entity_data1) == ""

        item2: str = "bcc"
        entity_data2: dict = {}
        assert _get_entity_data(item=item2, entity_data=entity_data2) == ""

        item3: str = "bcc"
        entity_data3: dict = {"bcc": None, "cc": None}
        assert _get_entity_data(item=item3, entity_data=entity_data3) == ""

        item4: str = "bcc"
        entity_data4: dict = {"bcc": "bcc@example.com", "cc": None}
        assert (
            _get_entity_data(item=item4, entity_data=entity_data4)
            == "bcc@example.com"
        )

        item5: str = "bcc"
        entity_data5: dict = {"bcc": "bcc@example.com", "cc": "cc@example.com"}
        assert (
            _get_entity_data(item=item5, entity_data=entity_data5)
            == "bcc@example.com"
        )

    def test__generate_content_for_mail(self) -> None:
        entity_data1: dict = {}
        result1: list[str] = []
        assert _generate_content_for_mail(entity_data=entity_data1) == result1

        entity_data2: dict = {"bcc": "bcc@example.com", "cc": "cc@example.com"}
        result2: list[str] = [
            "Onderwerp: ",
            "Berichtdatum: ",
            "Aan: ",
            "Afzender: ",
            "CC: cc@example.com",
            "BCC: bcc@example.com",
        ]
        assert _generate_content_for_mail(entity_data=entity_data2) == result2

        entity_data3: dict = {
            "bcc": "bcc@example.com",
            "cc": "cc@example.com",
            "from": "from@example.com",
            "message_date": "2024-11-22T06:53:20.717479+00:00",
            "recipient": "to@example.com",
            "subject": "Onderwerp test",
        }
        result3: list[str] = [
            "Onderwerp: Onderwerp test",
            "Berichtdatum: 22-11-2024, 07:53",
            "Aan: to@example.com",
            "Afzender: from@example.com",
            "CC: cc@example.com",
            "BCC: bcc@example.com",
        ]
        assert _generate_content_for_mail(entity_data=entity_data3) == result3

        entity_data4: dict = {
            "bcc": None,
            "cc": None,
            "from": None,
            "message_date": None,
            "recipient": None,
            "subject": "Onderwerp test",
        }
        result4: list[str] = [
            "Onderwerp: Onderwerp test",
            "Berichtdatum: ",
            "Aan: ",
            "Afzender: ",
            "CC: ",
            "BCC: ",
        ]
        assert _generate_content_for_mail(entity_data=entity_data4) == result4

        entity_data5: dict = {
            "bcc": None,
            "cc": None,
            "from": None,
            "message_date": None,
            "recipient": None,
            "subject": None,
        }
        result5: list[str] = []
        assert _generate_content_for_mail(entity_data=entity_data5) == result5

        message_date6: datetime = datetime(
            year=2024,
            month=11,
            day=30,
            hour=19,
            minute=28,
            second=29,
            microsecond=0,
            tzinfo=UTC,
        )
        entity_data6: dict = {
            "bcc": "bcc@example.com",
            "cc": "cc@example.com",
            "from": "from@example.com",
            "message_date": message_date6,
            "recipient": "to@example.com",
            "subject": "Onderwerp test",
        }
        result6: list[str] = [
            "Onderwerp: Onderwerp test",
            "Berichtdatum: 2024-11-30 19:28:29+00:00",
            "Aan: to@example.com",
            "Afzender: from@example.com",
            "CC: cc@example.com",
            "BCC: bcc@example.com",
        ]
        assert _generate_content_for_mail(entity_data=entity_data6) == result6

        message_date7: str = "2024-11-22T06:53:20.717479+00:00"
        entity_data7: dict = {
            "bcc": "bcc@example.com",
            "cc": "cc@example.com",
            "from": "from@example.com",
            "message_date": message_date7,
            "recipient": "to@example.com",
            "subject": "Onderwerp test",
        }
        result7: list[str] = [
            "Onderwerp: Onderwerp test",
            "Berichtdatum: 22-11-2024, 07:53",
            "Aan: to@example.com",
            "Afzender: from@example.com",
            "CC: cc@example.com",
            "BCC: bcc@example.com",
        ]
        assert _generate_content_for_mail(entity_data=entity_data7) == result7


class Test_AsUser_GetListOfCustomObjectEventLogs(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_custom_object_event_logs(self):
        mock_custom_object_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_events[0].configure_mock(
            id=10,
            type="custom_object/created",
            created=created1,
            description="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            user="auser",
            uuid=uuid1,
            exception="someting terribly went wrong",
            entity_meta_summary="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            entity_id=uuid1,
            case_uuid=uuid4(),
            zaak_id=10,
            metadata="metadata",
        )
        mock_custom_object_events[1].configure_mock(
            id=11,
            type="custom_object/updated",
            created=created2,
            description="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            user="buser",
            uuid=uuid2,
            exception="someting terribly went wrong2",
            entity_meta_summary="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            entity_id=uuid2,
            case_uuid=uuid4(),
            zaak_id=1,
            metadata="metadata",
        )

        self.session.execute().fetchall.return_value = (
            mock_custom_object_events
        )

        self.session.reset_mock()

        custom_object_uuid = uuid4()

        mock_custom_object_event_list = self.qry.get_custom_object_event_logs(
            page=page,
            page_size=page_size,
            custom_object_uuid=str(custom_object_uuid),
        )

        assert isinstance(mock_custom_object_event_list[0], TimelineEntry)

        mock_custom_object_events = list(mock_custom_object_event_list)
        assert len(mock_custom_object_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert (
            str(query)
            == 'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, CAST(logging.event_data AS JSON) ->> %(param_1)s AS exception, logging.zaak_id, logging.component, CAST(logging.event_data AS JSON) ->> %(param_2)s AS metadata \n'
            "FROM logging \n"
            "WHERE logging.component = %(component_1)s AND ((CAST(logging.event_data AS JSON) ->> %(param_3)s) = %(param_4)s OR ((CAST(logging.event_data AS JSONB) -> %(param_5)s) ? %(param_6)s)) ORDER BY logging.created DESC \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )
        assert query.params["param_7"] == 10
        assert query.params["param_8"] == 10

    def test_get_custom_object_event_logs_with_period(self):
        mock_custom_object_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_events[0].configure_mock(
            id=10,
            type="custom_object/created",
            created=created1,
            description="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            user="auser",
            exception="someting terribly went wrong",
            uuid=uuid1,
            entity_meta_summary="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            entity_id=uuid1,
            case_uuid=uuid4(),
            zaak_id=10,
            metadata="metadata",
        )
        mock_custom_object_events[1].configure_mock(
            id=11,
            type="custom_object/updated",
            created=created2,
            description="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            user="buser",
            exception="someting terribly went wrong2",
            uuid=uuid2,
            entity_meta_summary="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            entity_id=uuid2,
            case_uuid=uuid4(),
            zaak_id=1,
            metadata="metadata",
        )

        self.session.execute().fetchall.return_value = (
            mock_custom_object_events
        )

        self.session.reset_mock()

        custom_object_uuid = uuid4()

        mock_custom_object_event_list = self.qry.get_custom_object_event_logs(
            page=page,
            page_size=page_size,
            custom_object_uuid=str(custom_object_uuid),
            period_start="2021-02-03T12:53:00+01:00",
            period_end="2021-02-04T12:53:00Z",
        )

        assert isinstance(mock_custom_object_event_list[0], TimelineEntry)

        mock_custom_object_events = list(mock_custom_object_event_list)
        assert len(mock_custom_object_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(query)
            == 'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, CAST(logging.event_data AS JSON) ->> %(param_1)s AS exception, logging.zaak_id, logging.component, CAST(logging.event_data AS JSON) ->> %(param_2)s AS metadata \n'
            "FROM logging \n"
            "WHERE logging.component = %(component_1)s AND ((CAST(logging.event_data AS JSON) ->> %(param_3)s) = %(param_4)s OR ((CAST(logging.event_data AS JSONB) -> %(param_5)s) ? %(param_6)s)) AND logging.created >= %(created_1)s AND logging.created <= %(created_2)s ORDER BY logging.created DESC \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )
        assert query.params["created_1"] == datetime(
            2021, 2, 3, 12, 53, tzinfo=tzoffset(None, 3600)
        )
        assert query.params["created_2"] == datetime(
            2021, 2, 4, 12, 53, tzinfo=tzutc()
        )
        assert query.params["param_7"] == 10
        assert query.params["param_8"] == 10


class Test_AsUser_GetListOfContactEventLogs(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_contact_event_logs(self):
        mock_contact_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )

        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )

        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"

        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        mock_contact_events[0].configure_mock(
            id=10,
            component="zaak",
            type="case/attribute/update",
            created=created1,
            description='Kenmerk "numeric field" gewijzigd',
            user="auser",
            exception="someting terribly went wrong",
            uuid=uuid1,
            entity_meta_summary='Kenmerk "numeric field" gewijzigd',
            entity_id=uuid1,
            case_uuid=uuid4(),
            zaak_id=11,
            attribute_value="20",
            metadata="metadata",
        )

        mock_contact_events[1].configure_mock(
            id=11,
            type="case/document/create",
            created=created2,
            description='Document "testdoc-1" toegevoegd',
            user="auser",
            exception="someting terribly went wrong2",
            uuid=uuid2,
            entity_meta_summary='Document "testdoc-1" toegevoegd',
            entity_id=uuid2,
            case_uuid=uuid4(),
            zaak_id=1,
            metadata="metadata",
            component="document",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )
        self.session.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "type id"
        )(type="medewerker", id=1)

        self.session.execute().fetchall.return_value = mock_contact_events
        contact_uuid = uuid4()
        self.session.reset_mock()
        mock_contact_event_list = self.qry.get_contact_event_logs(
            page=page,
            page_size=page_size,
            contact_type="medewerker",
            contact_uuid=str(contact_uuid),
            period_start="2021-02-03T12:53:00Z",
            period_end="2021-02-03T12:53:00+01:00",
        )

        assert isinstance(mock_contact_event_list[0], TimelineEntry)

        mock_contact_events = list(mock_contact_event_list)
        assert len(mock_contact_events) == 2

        call_list = self.session.execute.call_args_list

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        # Time line events query
        assert (
            str(compiled_select) == "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, %(param_3)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_4)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_5)s AS metadata, logging.component AS component, CAST(logging.event_data AS JSON) ->> %(param_6)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_7)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_8)s)) ->> %(param_9)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_10)s)) ->> %(param_11)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_12)s)) ->> %(param_13)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_14)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_15)s AS body \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s, %(event_type_1_40)s, %(event_type_1_41)s, %(event_type_1_42)s, %(event_type_1_43)s, %(event_type_1_44)s, %(event_type_1_45)s, %(event_type_1_46)s, %(event_type_1_47)s, %(event_type_1_48)s, %(event_type_1_49)s, %(event_type_1_50)s, %(event_type_1_51)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_16)s) = %(param_17)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_19)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_20)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_21)s AS metadata, logging.component AS component, CAST(logging.event_data AS JSON) ->> %(param_22)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_23)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_24)s)) ->> %(param_25)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_26)s)) ->> %(param_27)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_28)s)) ->> %(param_29)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_30)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_31)s AS body \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id AND zaak_betrokkenen.subject_id = %(subject_id_1)s::UUID \n"
            "WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s, %(event_type_2_40)s, %(event_type_2_41)s, %(event_type_2_42)s, %(event_type_2_43)s, %(event_type_2_44)s, %(event_type_2_45)s, %(event_type_2_46)s, %(event_type_2_47)s, %(event_type_2_48)s, %(event_type_2_49)s, %(event_type_2_50)s, %(event_type_2_51)s))\n"
            ' SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.exception, anon_1.case_uuid, anon_1.attribute_value, anon_1.metadata, anon_1.component, anon_1.interface_name, anon_1.document_name, anon_1.pkg_name, anon_1.recipient_display_name, anon_1.recipient_address, anon_1.subject, anon_1.body \n'
            "FROM anon_1 \n"
            "WHERE created >= %(created_1)s AND created <= %(created_2)s ORDER BY created DESC \n"
            " LIMIT %(param_32)s OFFSET %(param_33)s"
        )
        assert compiled_select.params["created_1"].tzinfo == timezone.utc
        assert compiled_select.params["created_2"].tzinfo == timezone.utc

        # Timeline events with only period start filter
        mock_contact_event_list = self.qry.get_contact_event_logs(
            page=page,
            page_size=page_size,
            contact_type="medewerker",
            contact_uuid=str(contact_uuid),
            period_start="2021-02-03T12:53:00Z",
        )

        assert isinstance(mock_contact_event_list[0], TimelineEntry)

        mock_contact_events = list(mock_contact_event_list)
        assert len(mock_contact_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        # Time line events query
        assert (
            str(query) == "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, %(param_3)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_4)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_5)s AS metadata, logging.component AS component, CAST(logging.event_data AS JSON) ->> %(param_6)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_7)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_8)s)) ->> %(param_9)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_10)s)) ->> %(param_11)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_12)s)) ->> %(param_13)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_14)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_15)s AS body \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s, %(event_type_1_40)s, %(event_type_1_41)s, %(event_type_1_42)s, %(event_type_1_43)s, %(event_type_1_44)s, %(event_type_1_45)s, %(event_type_1_46)s, %(event_type_1_47)s, %(event_type_1_48)s, %(event_type_1_49)s, %(event_type_1_50)s, %(event_type_1_51)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_16)s) = %(param_17)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_19)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_20)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_21)s AS metadata, logging.component AS component, CAST(logging.event_data AS JSON) ->> %(param_22)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_23)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_24)s)) ->> %(param_25)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_26)s)) ->> %(param_27)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_28)s)) ->> %(param_29)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_30)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_31)s AS body \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id AND zaak_betrokkenen.subject_id = %(subject_id_1)s::UUID \n"
            "WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s, %(event_type_2_40)s, %(event_type_2_41)s, %(event_type_2_42)s, %(event_type_2_43)s, %(event_type_2_44)s, %(event_type_2_45)s, %(event_type_2_46)s, %(event_type_2_47)s, %(event_type_2_48)s, %(event_type_2_49)s, %(event_type_2_50)s, %(event_type_2_51)s))\n"
            ' SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.exception, anon_1.case_uuid, anon_1.attribute_value, anon_1.metadata, anon_1.component, anon_1.interface_name, anon_1.document_name, anon_1.pkg_name, anon_1.recipient_display_name, anon_1.recipient_address, anon_1.subject, anon_1.body \n'
            "FROM anon_1 \n"
            "WHERE created >= %(created_1)s ORDER BY created DESC \n"
            " LIMIT %(param_32)s OFFSET %(param_33)s"
        )

        # Timeline events with only period end filter
        mock_contact_event_list = self.qry.get_contact_event_logs(
            page=page,
            page_size=page_size,
            contact_type="medewerker",
            contact_uuid=str(contact_uuid),
            period_end="2021-02-03T12:53:00+01:00",
        )

        assert isinstance(mock_contact_event_list[0], TimelineEntry)

        mock_contact_events = list(mock_contact_event_list)
        assert len(mock_contact_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        # Time line events query
        assert (
            str(query) == "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, %(param_3)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_4)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_5)s AS metadata, logging.component AS component, CAST(logging.event_data AS JSON) ->> %(param_6)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_7)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_8)s)) ->> %(param_9)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_10)s)) ->> %(param_11)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_12)s)) ->> %(param_13)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_14)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_15)s AS body \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s, %(event_type_1_40)s, %(event_type_1_41)s, %(event_type_1_42)s, %(event_type_1_43)s, %(event_type_1_44)s, %(event_type_1_45)s, %(event_type_1_46)s, %(event_type_1_47)s, %(event_type_1_48)s, %(event_type_1_49)s, %(event_type_1_50)s, %(event_type_1_51)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_16)s) = %(param_17)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_19)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_20)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_21)s AS metadata, logging.component AS component, CAST(logging.event_data AS JSON) ->> %(param_22)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_23)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_24)s)) ->> %(param_25)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_26)s)) ->> %(param_27)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_28)s)) ->> %(param_29)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_30)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_31)s AS body \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id AND zaak_betrokkenen.subject_id = %(subject_id_1)s::UUID \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s, %(event_type_2_40)s, %(event_type_2_41)s, %(event_type_2_42)s, %(event_type_2_43)s, %(event_type_2_44)s, %(event_type_2_45)s, %(event_type_2_46)s, %(event_type_2_47)s, %(event_type_2_48)s, %(event_type_2_49)s, %(event_type_2_50)s, %(event_type_2_51)s))\n SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.exception, anon_1.case_uuid, anon_1.attribute_value, anon_1.metadata, anon_1.component, anon_1.interface_name, anon_1.document_name, anon_1.pkg_name, anon_1.recipient_display_name, anon_1.recipient_address, anon_1.subject, anon_1.body \n'
            "FROM anon_1 \n"
            "WHERE created <= %(created_1)s ORDER BY created DESC \n"
            " LIMIT %(param_32)s OFFSET %(param_33)s"
        )


class Test_GetCaseEventLogs(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_case_event_logs(self):
        mock_case_events = [mock.Mock(), mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2021, month=10, day=1, hour=13, minute=44, second=27
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="zaak-component",
            type="zaak/created",
            created=created1,
            description="Descripition",
            user="auser",
            exception="someting terribly went wrong",
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata="metadata",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="zaak",
            type="zaak/updated",
            created=created2,
            description="Descripition2",
            user="buser",
            exception="someting terribly went wrong2",
            uuid=uuid2,
            entity_meta_summary="Summary2",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata="metadata",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )

        mock_case_events[2].configure_mock(
            id=12,
            component="zaak",
            type="zaak/updated",
            created=created2,
            description="Document 'samengevoegd_test.pdf' samengevoegd vanuit &#39;test_document.pdf&#39; en &#39;test_document2.pdf&#39;",
            comment="This is a comment",
            user="buser",
            exception="someting terribly went wrong2",
            uuid=uuid2,
            entity_meta_summary="Document 'samengevoegd_test.pdf' samengevoegd vanuit &#39;test_document.pdf&#39; en &#39;test_document2.pdf&#39;",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata="metadata",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )

        self.session.execute().fetchall.return_value = mock_case_events

        mock_case_event_list = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )
        assert isinstance(mock_case_event_list[0], TimelineEntry)
        assert (
            mock_case_event_list[2].description
            == "Document 'samengevoegd_test.pdf' samengevoegd vanuit 'test_document.pdf' en 'test_document2.pdf', reden: This is a comment"
        )
        assert (
            mock_case_event_list[2].entity_meta_summary
            == "Document 'samengevoegd_test.pdf' samengevoegd vanuit 'test_document.pdf' en 'test_document2.pdf'"
        )

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 3
        execute_calls = self.session.execute.call_args_list

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None

    def test_get_case_event_logs_for_doc_info_change_events(self):
        mock_case_events = [mock.Mock(), mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2021, month=10, day=1, hour=13, minute=44, second=27
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="document",
            type="case/document/metadata/update",
            created=created1,
            description="Document-metadata aangepast voor 'New 2.odt' versie 1 (ID 717)",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="zaak",
            type="case/document/sign",
            created=created2,
            description="Ondertekend document toegevoegd aan zaak",
            user="buser",
            exception=None,
            uuid=uuid2,
            entity_meta_summary="Summary2",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata="metadata",
            document_name="Testdoc",
            interface_name="ValidSign Electronische Handtekening",
            pkg_name="Zaaksysteem zaak 3118 - file Testdoc gerelateerde zaak aanmaken via de PIP",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )

        mock_case_events[2].configure_mock(
            id=12,
            component="zaak",
            type="case/postex/send",
            created=created2,
            description='<geen onderwerp opgegeven>","zaak_id":12}',
            comment="This is a comment",
            user="buser",
            exception=None,
            uuid=uuid2,
            entity_meta_summary='<geen onderwerp opgegeven>","zaak_id":12}',
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata="metadata",
            address="test address",
            display_name="testuser",
            subject="<geen onderwerp opgegeven>",
            body="<geen inhoud opgegeven>",
        )

        self.session.execute().fetchall.return_value = mock_case_events

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 3
        execute_calls = self.session.execute.call_args_list
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None

    def test_get_case_event_logs_with_period(self):
        mock_case_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=2, day=3, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2021, month=2, day=3, hour=13, minute=44, second=27
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="zaak",
            type="case/attribute/update",
            created=created1,
            description='Kenmerk "numeric field" gewijzigd',
            user="auser",
            exception="someting terribly went wrong",
            uuid=uuid1,
            entity_meta_summary='Kenmerk "numeric field" gewijzigd',
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            event_data={"comment": "reason"},
            attribute_value="20",
            metadata="metadata",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="zaak",
            type="custom_object/updated",
            created=created2,
            description="Descripition",
            user="buser",
            exception="someting terribly went wrong 2",
            uuid=uuid2,
            entity_meta_summary="Descripition",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=1,
            attribute_value="20",
            metadata="metadata",
            address=None,
            display_name=None,
            subject=None,
            body=None,
        )

        self.session.execute().fetchall.return_value = mock_case_events

        mock_case_event_list = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
            period_start="2021-02-03T12:53:00Z",
            period_end="2021-02-04T12:53:00Z",
        )
        assert isinstance(mock_case_event_list[0], TimelineEntry)
        assert "reden" in mock_case_event_list[0].description

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
            period_start="2021-02-03T12:53:00Z",
            period_end="2021-02-04T12:53:00Z",
        )

        assert len(case_event_logs) == 2
        execute_calls = self.session.execute.call_args_list

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND logging.created >= %(created_1)s AND logging.created <= %(created_2)s ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 33

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None
        assert compiled_select.params["created_1"] == datetime(
            2021, 2, 3, 12, 53, tzinfo=timezone.utc
        )
        assert compiled_select.params["created_2"] == datetime(
            2021, 2, 4, 12, 53, tzinfo=timezone.utc
        )

    def test_get_case_event_logs_for_case_email_created_events(self):
        mock_case_events = [mock.Mock(), mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        uuid3 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="case",
            type="case/email/created",
            created=created1,
            description="Bericht verstuurd via e-mail.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test",
            body=None,
            attachments=None,
            bcc="bcc@example.com",
            case_id=10,
            cc="cc@example.com",
            message_type="E-mail",
            recipient="to@example.com",
            message_date="not a valid date",
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="case",
            type="case/email/created",
            created=created1,
            description="Bericht verstuurd via e-mail.",
            user="auser",
            exception=None,
            uuid=uuid2,
            entity_meta_summary="Summary",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 2",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test 2",
            body=None,
            attachments=None,
            bcc="bcc2@example.com",
            case_id=10,
            cc="cc2@example.com",
            message_type="E-mail",
            recipient="to2@example.com",
            message_date=None,
        )
        mock_case_events[2].configure_mock(
            id=12,
            component="case",
            type="case/email/created",
            created=created1,
            description="Bericht verstuurd via e-mail.",
            user="auser",
            exception=None,
            uuid=uuid3,
            entity_meta_summary="Summary",
            entity_id=uuid3,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 3",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test 3",
            body=None,
            attachments=None,
            bcc="bcc3@example.com",
            case_id=10,
            cc="cc3@example.com",
            message_type="E-mail",
            recipient="to3@example.com",
            message_date="2024-11-28T10:24:46.310185+00:00",
        )

        mock_case_events[0].configure_mock(**{"from": "from@example.com"})
        mock_case_events[1].configure_mock(**{"from": "from2@example.com"})
        mock_case_events[2].configure_mock(**{"from": "from3@example.com"})

        self.session.execute().fetchall.return_value = mock_case_events

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 3
        assert isinstance(case_event_logs[0], TimelineEntry)
        assert isinstance(case_event_logs[1], TimelineEntry)
        assert isinstance(case_event_logs[2], TimelineEntry)

        assert (
            case_event_logs[0].description
            == "Bericht verstuurd via e-mail., reden: This is a comment"
        )
        assert case_event_logs[0].content == (
            "Onderwerp: Onderwerp test\n"
            "Berichtdatum: not a valid date\n"
            "Aan: to@example.com\n"
            "Afzender: from@example.com\n"
            "CC: cc@example.com\n"
            "BCC: bcc@example.com"
        )

        assert (
            case_event_logs[1].description
            == "Bericht verstuurd via e-mail., reden: This is a comment 2"
        )
        assert case_event_logs[1].content == (
            "Onderwerp: Onderwerp test 2\n"
            "Berichtdatum: \n"
            "Aan: to2@example.com\n"
            "Afzender: from2@example.com\n"
            "CC: cc2@example.com\n"
            "BCC: bcc2@example.com"
        )

        assert (
            case_event_logs[2].description
            == "Bericht verstuurd via e-mail., reden: This is a comment 3"
        )
        assert case_event_logs[2].content == (
            "Onderwerp: Onderwerp test 3\n"
            "Berichtdatum: 28-11-2024, 11:24\n"
            "Aan: to3@example.com\n"
            "Afzender: from3@example.com\n"
            "CC: cc3@example.com\n"
            "BCC: bcc3@example.com"
        )

        execute_calls = self.session.execute.call_args_list
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None

    def test_get_case_event_logs_for_case_pip_feedback(self):
        mock_case_events = [mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="case",
            type="case/pip/feedback",
            created=created1,
            description="Pip feedback.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test",
            body=None,
            attachments=None,
            bcc=None,
            case_id=10,
            cc=None,
            message_type="Pip",
            recipient="to@example.com",
            message_date=None,
        )
        self.session.execute().fetchall.return_value = mock_case_events

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 1
        execute_calls = self.session.execute.call_args_list
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None

    def test_get_case_event_logs_for_case_thread_link_events(self) -> None:
        mock_case_events: list[mock.MagicMock] = [
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
        ]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid_1 = uuid4()
        uuid_2 = uuid4()
        uuid_3 = uuid4()
        uuid_4 = uuid4()

        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="case",
            type="case/thread/link",
            created=created1,
            description="Bericht toegevoegd",
            user="auser",
            exception=None,
            uuid=uuid_1,
            entity_meta_summary="Summary",
            entity_id=uuid_1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test",
            body=None,
            attachments=None,
            bcc="bcc@example.com",
            case_id=10,
            cc="cc@example.com",
            message_type="E-mail",
            recipient="to@example.com",
            message_date="not a valid date",
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="case",
            type="case/thread/link",
            created=created1,
            description="Bericht toegevoegd",
            user="auser",
            exception=None,
            uuid=uuid_2,
            entity_meta_summary="Summary",
            entity_id=uuid_2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 2",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test 2",
            body=None,
            attachments=None,
            bcc="bcc2@example.com",
            case_id=10,
            cc="cc2@example.com",
            message_type="E-mail",
            recipient="to2@example.com",
            message_date=None,
        )
        mock_case_events[2].configure_mock(
            id=12,
            component="case",
            type="case/thread/link",
            created=created1,
            description="Bericht toegevoegd",
            user="auser",
            exception=None,
            uuid=uuid_3,
            entity_meta_summary="Summary",
            entity_id=uuid_3,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 3",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test 3",
            body=None,
            attachments=None,
            bcc="bcc3@example.com",
            case_id=10,
            cc="cc3@example.com",
            message_type="E-mail",
            recipient="to3@example.com",
            message_date="2024-11-28T10:24:46.310185+00:00",
        )
        mock_case_events[3].configure_mock(
            id=13,
            component="case",
            type="case/thread/link",
            created=created1,
            description="Bericht toegevoegd",
            user="auser",
            exception=None,
            uuid=uuid_4,
            entity_meta_summary="Summary",
            entity_id=uuid_4,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 4",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject=None,
            body=None,
            attachments=None,
            bcc=None,
            case_id=10,
            cc=None,
            message_type="E-mail",
            recipient=None,
            message_date=None,
        )

        mock_case_events[0].configure_mock(**{"from": "from@example.com"})
        mock_case_events[1].configure_mock(**{"from": "from2@example.com"})
        mock_case_events[2].configure_mock(**{"from": "from3@example.com"})
        mock_case_events[3].configure_mock(**{"from": None})

        self.session.execute().fetchall.return_value = mock_case_events

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 4
        assert isinstance(case_event_logs[0], TimelineEntry)
        assert isinstance(case_event_logs[1], TimelineEntry)
        assert isinstance(case_event_logs[2], TimelineEntry)
        assert isinstance(case_event_logs[3], TimelineEntry)

        assert (
            case_event_logs[0].description
            == "Bericht toegevoegd, reden: This is a comment"
        )
        assert case_event_logs[0].content == (
            "Onderwerp: Onderwerp test\n"
            "Berichtdatum: not a valid date\n"
            "Aan: to@example.com\n"
            "Afzender: from@example.com\n"
            "CC: cc@example.com\n"
            "BCC: bcc@example.com"
        )

        assert (
            case_event_logs[1].description
            == "Bericht toegevoegd, reden: This is a comment 2"
        )
        assert case_event_logs[1].content == (
            "Onderwerp: Onderwerp test 2\n"
            "Berichtdatum: \n"
            "Aan: to2@example.com\n"
            "Afzender: from2@example.com\n"
            "CC: cc2@example.com\n"
            "BCC: bcc2@example.com"
        )

        assert (
            case_event_logs[2].description
            == "Bericht toegevoegd, reden: This is a comment 3"
        )
        assert case_event_logs[2].content == (
            "Onderwerp: Onderwerp test 3\n"
            "Berichtdatum: 28-11-2024, 11:24\n"
            "Aan: to3@example.com\n"
            "Afzender: from3@example.com\n"
            "CC: cc3@example.com\n"
            "BCC: bcc3@example.com"
        )

        assert (
            case_event_logs[3].description
            == "Bericht toegevoegd, reden: This is a comment 4"
        )
        assert case_event_logs[3].content == ""

        execute_calls = self.session.execute.call_args_list
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None


class Test_SubjectEventLogs(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_subject_message_delete_log(self):
        mock_events = [mock.Mock(), mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_events[0].configure_mock(
            id=10,
            component="betrokkene",
            type="subject/note/delete",
            created=created1,
            description="name heeft notitie verwijderd.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test",
            body=None,
            attachments=None,
            bcc=None,
            case_id=10,
            cc=None,
            message_type="note",
            recipient="to@example.com",
            message_date="not a valid date",
        )
        mock_events[1].configure_mock(
            id=11,
            component="betrokkene",
            type="subject/note/delete",
            created=created1,
            description="name heeft notitie verwijderd.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 2",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test 2",
            body=None,
            attachments=None,
            bcc=None,
            case_id=10,
            cc=None,
            message_type="note",
            recipient="to2@example.com",
            message_date=None,
        )
        mock_events[2].configure_mock(
            id=12,
            component="betrokkene",
            type="subject/note/delete",
            created=created1,
            description="name heeft notitie verwijderd.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment 3",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test 3",
            body=None,
            attachments=None,
            bcc="bcc3@example.com",
            case_id=10,
            cc="cc3@example.com",
            message_type="note",
            recipient="to3@example.com",
            message_date="2024-11-28T10:24:46.310185+00:00",
        )

        mock_events[0].configure_mock(**{"from": "from@example.com"})
        mock_events[1].configure_mock(**{"from": "from2@example.com"})
        mock_events[2].configure_mock(**{"from": "from3@example.com"})

        self.session.execute().fetchall.return_value = mock_events

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 3
        assert isinstance(case_event_logs[0], TimelineEntry)
        assert isinstance(case_event_logs[1], TimelineEntry)
        assert isinstance(case_event_logs[2], TimelineEntry)

        assert (
            case_event_logs[0].description
            == "name heeft notitie verwijderd., reden: This is a comment"
        )
        assert case_event_logs[0].content == (
            "Onderwerp: Onderwerp test\n"
            "Berichtdatum: not a valid date\n"
            "Aan: to@example.com\n"
            "Afzender: from@example.com\n"
            "CC: \n"
            "BCC: "
        )

        assert (
            case_event_logs[1].description
            == "name heeft notitie verwijderd., reden: This is a comment 2"
        )
        assert case_event_logs[1].content == (
            "Onderwerp: Onderwerp test 2\n"
            "Berichtdatum: \n"
            "Aan: to2@example.com\n"
            "Afzender: from2@example.com\n"
            "CC: \n"
            "BCC: "
        )

        assert (
            case_event_logs[2].description
            == "name heeft notitie verwijderd., reden: This is a comment 3"
        )
        assert case_event_logs[2].content == (
            "Onderwerp: Onderwerp test 3\n"
            "Berichtdatum: 28-11-2024, 11:24\n"
            "Aan: to3@example.com\n"
            "Afzender: from3@example.com\n"
            "CC: cc3@example.com\n"
            "BCC: bcc3@example.com"
        )

        execute_calls = self.session.execute.call_args_list
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None

    def test_subject_message_delete_log_invalid_message_date(self):
        mock_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_events[0].configure_mock(
            id=10,
            component="betrokkene",
            type="subject/note/delete",
            created=created1,
            description="name heeft notitie verwijderd.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test",
            body=None,
            attachments=None,
            bcc=None,
            case_id=10,
            cc=None,
            message_type="note",
            recipient="to@example.com",
            message_date=None,
        )
        mock_events[1].configure_mock(
            id=11,
            component="betrokkene",
            type="subject/note/delete",
            created=created1,
            description="name heeft notitie verwijderd.",
            user="auser",
            exception=None,
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="20",
            metadata='{"creation_date":"2021-02-03T12:53:00Z","description":"test"}',
            address=None,
            display_name=None,
            comment="This is a comment",
            interface_name=None,
            document_name=None,
            pkg_name=None,
            recipient_display_name=None,
            recipient_address=None,
            subject="Onderwerp test",
            body=None,
            attachments=None,
            bcc=None,
            case_id=10,
            cc=None,
            message_type="note",
            recipient="to@example.com",
            message_date="2024-11-28T10:24:46.310185+00:00",
        )

        self.session.execute().fetchall.return_value = mock_events

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 2
        execute_calls = self.session.execute.call_args_list
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value, CAST(logging.event_data AS JSON) ->> %(param_4)s AS metadata, logging.component, CAST(logging.event_data AS JSON) ->> %(param_5)s AS interface_name, CAST(logging.event_data AS JSON) ->> %(param_6)s AS document_name, ((CAST(logging.event_data AS JSON) -> %(param_7)s)) ->> %(param_8)s AS pkg_name, ((CAST(logging.event_data AS JSON) -> %(param_9)s)) ->> %(param_10)s AS recipient_display_name, ((CAST(logging.event_data AS JSON) -> %(param_11)s)) ->> %(param_12)s AS recipient_address, CAST(logging.event_data AS JSON) ->> %(param_13)s AS subject, CAST(logging.event_data AS JSON) ->> %(param_14)s AS body, CAST(logging.event_data AS JSON) ->> %(param_15)s AS attachments, CAST(logging.event_data AS JSON) ->> %(param_16)s AS bcc, CAST(logging.event_data AS JSON) ->> %(param_17)s AS case_id, CAST(logging.event_data AS JSON) ->> %(param_18)s AS cc, CAST(logging.event_data AS JSON) ->> %(param_19)s AS "from", CAST(logging.event_data AS JSON) ->> %(param_20)s AS message_type, CAST(logging.event_data AS JSON) ->> %(param_21)s AS recipient, coalesce(CAST(logging.event_data AS JSON) ->> %(param_22)s, CAST(logging.event_data AS JSON) ->> %(param_23)s) AS message_date \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_24)s OFFSET %(param_25)s"
        )

        assert len(compiled_select.params) == 31

        assert compiled_select.params["param_1"] == "comment"
        assert compiled_select.params["param_2"] == "exception"
        assert compiled_select.params["param_3"] == "attribute_value"
        assert compiled_select.params["param_4"] == "metadata"
        assert compiled_select.params["param_5"] == "interface_name"
        assert compiled_select.params["param_6"] == "document_name"
        assert compiled_select.params["param_7"] == "pkg"
        assert compiled_select.params["param_8"] == "name"
        assert compiled_select.params["param_9"] == "recipient"
        assert compiled_select.params["param_10"] == "display_name"
        assert compiled_select.params["param_11"] == "recipient"
        assert compiled_select.params["param_12"] == "address"
        assert compiled_select.params["param_13"] == "subject"
        assert compiled_select.params["param_14"] == "body"
        assert compiled_select.params["param_15"] == "attachments"
        assert compiled_select.params["param_16"] == "bcc"
        assert compiled_select.params["param_17"] == "case_id"
        assert compiled_select.params["param_18"] == "cc"
        assert compiled_select.params["param_19"] == "from"
        assert compiled_select.params["param_20"] == "message_type"
        assert compiled_select.params["param_21"] == "recipient"
        assert compiled_select.params["param_22"] == "message_date"
        assert compiled_select.params["param_23"] == "created_date"
        assert compiled_select.params["param_24"] is not None
        assert compiled_select.params["param_25"] is not None

        assert compiled_select.params["uuid_1"] is not None
        assert compiled_select.params["status_1"] == "deleted"
        assert compiled_select.params["uuid_2"] is not None
        assert compiled_select.params["aanvrager_type_1"] == "medewerker"
        assert compiled_select.params["permission_1"] == "read"
        assert compiled_select.params["subject_uuid_1"] is not None
