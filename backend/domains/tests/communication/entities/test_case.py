# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from uuid import uuid4
from zsnl_domains.communication.entities import Case


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCaseEntity:
    case_id = 123
    case_uuid = uuid4()
    case_description = "onderwerp"
    case_type_name = "case type name"

    def setup_method(self):
        self.case = Case(
            id=self.case_id,
            uuid=self.case_uuid,
            description=None,
            description_public=None,
            case_type_name=None,
            status="open",
        )

    def test_case_initialisation(self):
        assert self.case.id == self.case_id
        assert self.case.uuid == self.case_uuid
        assert self.case.entity_id == self.case_uuid

    def test_case_init_for_search_results(self):
        case = Case(
            id=self.case_id,
            uuid=self.case_uuid,
            description=self.case_description,
            description_public=None,
            case_type_name=self.case_type_name,
            status="open",
        )

        assert case.id == self.case_id
        assert case.uuid == self.case_uuid
        assert case.description == self.case_description
        assert case.case_type_name == self.case_type_name
        assert case.status == "open"
