# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from minty.exceptions import NotFound
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Contact
from zsnl_domains.communication.repositories import ContactRepository


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestContactRepository:
    def setup_method(self):
        with mock.patch("sqlalchemy.orm.Session") as mock_session:
            self.infra = InfraFactoryMock(infra={"database": mock_session})
            self.infra_ro = InfraFactoryMock(infra={"database": mock_session})
            self.contact_repo = ContactRepository(
                infrastructure_factory=self.infra,
                infrastructure_factory_ro=self.infra_ro,
                context=None,
                event_service=mock.MagicMock(),
                read_only=False,
            )
            self.mock_session = mock_session
            self.contact_uuid = str(uuid4())
            event = mock.MagicMock
            event.entity_type = "Contact"
            event.entity_id = self.contact_uuid
            event.event_name = "ContactCreated"
            event.changes = {}
            event.entity_data = {}
            self.event = event

            self.db = self.contact_repo._get_infrastructure("database")
            self.contact_repo.event_service.event_list = [event]
            self.event_list = self.contact_repo.event_service.event_list

    def test_get_session(self):
        assert self.contact_repo.session is self.infra.get_infrastructure(
            context=None, infrastructure_name="database"
        )

    def test_search_contact(self):
        contacts = self.contact_repo.search_contact(
            keyword="test search",
            type_filter={"employee", "organization", "person"},
        )
        assert isinstance(contacts, list)

        for contact in contacts:
            assert isinstance(contact, Contact)

    def test_search_contact_no_result(self):
        self.contact_repo._get_infrastructure(
            "database"
        ).query.side_effect = NoResultFound

        keyword = "test search"
        result = self.contact_repo.search_contact(
            keyword=keyword, type_filter={"employee", "organization", "person"}
        )

        assert result == []

    def test_transform_to_entity(self):
        query_res = mock.MagicMock()
        query_res.uuid = uuid4()
        query_res.id = 2
        query_res.type = "employee"
        query_res.name = "name of the contact"
        query_res.address = "a address string"

        contact = self.contact_repo._transform_to_entity(query_res)
        assert isinstance(contact, Contact)
        assert contact.id == query_res.id
        assert contact.uuid == query_res.uuid
        assert contact.type == query_res.type
        assert contact.name == query_res.name

    def test_transform_to_entity_no_address(self):
        query_res = mock.MagicMock()
        query_res.uuid = uuid4()
        query_res.id = 2
        query_res.type = "employee"
        query_res.name = "name of the contact"
        query_res.street_name = None
        query_res.house_number = None
        query_res.house_letter = None
        query_res.addition = None
        query_res.place = None
        query_res.abroad_address = "abroad address"

        contact = self.contact_repo._transform_to_entity(query_res)
        assert isinstance(contact, Contact)
        assert contact.id == query_res.id
        assert contact.uuid == query_res.uuid
        assert contact.type == query_res.type
        assert contact.name == query_res.name
        assert contact.address == "abroad address"

    def test_get_contact_by_uuid(self):
        contact_uuid = uuid4()
        ContactRow = namedtuple("contact_row", "id uuid type name")
        contact_row = ContactRow(
            id=2, uuid=contact_uuid, type="employee", name="admin"
        )

        self.contact_repo.session.execute().fetchone.return_value = contact_row

        contact = self.contact_repo.get_contact_by_uuid(
            contact_uuid=contact_uuid
        )
        assert isinstance(contact, Contact)
        assert contact.id == contact_row.id
        assert contact.uuid == contact_row.uuid
        assert contact.name == "admin"
        assert contact.type == contact_row.type

        contact_row2 = ContactRow(
            id=2, uuid=contact_uuid, type="person", name="test person"
        )

        self.contact_repo.session.execute().fetchone.return_value = (
            contact_row2
        )

        contact = self.contact_repo.get_contact_by_uuid(
            contact_uuid=contact_uuid
        )

        assert contact.name == "test person"

    def test_get_contact_by_uuid_no_result(self):
        self.contact_repo.session.execute().fetchone.return_value = None

        contact_uuid = uuid4()

        with pytest.raises(NotFound) as excinfo:
            self.contact_repo.get_contact_by_uuid(contact_uuid=contact_uuid)

        assert excinfo.value.args == (
            f"Contact with uuid '{contact_uuid}' not found.",
            "contact/not_found",
        )

    def test__prepare_contact_address(self):
        query_res = mock.MagicMock()
        query_res.street_name = "straatname"
        query_res.house_number = 2
        query_res.house_letter = "A"
        query_res.addition = 3
        query_res.place = "amsterdam"
        query_res.postcode = "1122AA"

        address = self.contact_repo._prepare_contact_address(query_res)
        assert isinstance(address, str)
        assert address == "straatname 2A-3, 1122AA, amsterdam"

    def test__prepare_contact_address_no_streetname(self):
        query_res = mock.MagicMock()
        query_res.street_name = None
        query_res.house_number = 2
        query_res.house_letter = "A"
        query_res.addition = 3
        query_res.place = "amsterdam"
        query_res.postcode = "1122AA"

        address = self.contact_repo._prepare_contact_address(query_res)
        assert isinstance(address, str)
        assert address == "amsterdam"

    def test__prepare_contact_address_no_address(self):
        query_res = mock.MagicMock()
        query_res.street_name = None
        query_res.house_number = 2
        query_res.house_letter = "A"
        query_res.addition = 3
        query_res.place = None

        address = self.contact_repo._prepare_contact_address(query_res)
        assert address is None

    def test_get_requestor_contact_from_case(self):
        case_uuid = uuid4()
        contact_uuid = uuid4()
        ContactRow = namedtuple("contact_row", "id uuid type name")
        contact_row = ContactRow(
            id=2, uuid=contact_uuid, type="employee", name="admin"
        )

        self.contact_repo.session.execute().fetchone.return_value = contact_row

        contact = self.contact_repo.get_requestor_contact_from_case(
            case_uuid=case_uuid
        )
        assert isinstance(contact, Contact)
        assert contact.id == contact_row.id
        assert contact.uuid == contact_row.uuid
        assert contact.name == "admin"
        assert contact.type == contact_row.type

        contact_row2 = ContactRow(
            id=2, uuid=contact_uuid, type="person", name="test person"
        )

        self.contact_repo.session.execute().fetchone.return_value = (
            contact_row2
        )

        contact = self.contact_repo.get_requestor_contact_from_case(
            case_uuid=case_uuid
        )

        assert contact.name == "test person"

    def test_get_requestor_contact_from_case_no_result(self):
        self.contact_repo.session.execute().fetchone.return_value = None
        case_uuid = uuid4()

        with pytest.raises(NotFound) as excinfo:
            self.contact_repo.get_requestor_contact_from_case(
                case_uuid=case_uuid
            )

        assert excinfo.value.args == (
            "Contact not found.",
            "contact/not_found",
        )

    def test_get_subject_relation_contacts_from_case(self):
        case_uuid = uuid4()
        contact_uuid = uuid4()
        ContactRow = namedtuple("contact_row", "id uuid type name")
        contact_row_1 = ContactRow(
            id=2, uuid=contact_uuid, type="employee", name="admin"
        )
        contact_row_2 = ContactRow(
            id=2, uuid=contact_uuid, type="person", name="test person"
        )

        self.contact_repo.session.execute().fetchall.return_value = [
            contact_row_1,
            contact_row_2,
        ]

        contact_rows = (
            self.contact_repo.get_subject_relation_contacts_from_case(
                case_uuid=case_uuid
            )
        )
        res_1 = contact_rows[0]
        assert isinstance(res_1, Contact)
        assert res_1.id == contact_row_1.id
        assert res_1.uuid == contact_row_1.uuid
        assert res_1.name == contact_row_1.name
        assert res_1.type == contact_row_1.type

        res_2 = contact_rows[1]
        assert res_2.id == contact_row_2.id
        assert res_2.uuid == contact_row_2.uuid
        assert res_2.name == contact_row_2.name
        assert res_2.type == contact_row_2.type
