# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import sqlalchemy
from sqlalchemy.dialects import postgresql
from sqlalchemy.sql import and_, cast
from sqlalchemy.sql.dml import Delete
from sqlalchemy.sql.elements import BooleanClauseList
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.repositories.database_queries import (
    contact_query,
    contacts_query,
    delete_message,
    delete_thread,
    get_file_by_attachment,
    get_thread_list_query,
    list_cases_query,
    search_cases_query,
    search_contact_query,
    thread_by_partial_uuid_statement,
    thread_by_uuid_statement,
)
from zsnl_domains.communication.repositories.util import base62_decode
from zsnl_domains.database import schema


class TestDataBaseQueries:
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.natural_person_query"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.organization_query"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.employee_query"
    )
    @mock.patch("sqlalchemy.sql.union_all")
    def test_search_contact_query(
        self,
        mock_union,
        mock_employee_query,
        mock_organization_query,
        mock_natural_person_query,
    ):
        mock_union.return_value = "test union"

        # Code under test:
        res = search_contact_query(
            keyword="testing",
            type_filter={"employee", "organization", "person"},
        )

        assert res == "test union"

        args, kwargs = mock_natural_person_query.where.call_args
        assert isinstance(args[0], BooleanClauseList)
        assert (
            str(args[0])
            == "lower(natuurlijk_persoon.search_term) LIKE lower(:search_term_1) AND natuurlijk_persoon.active IS true AND natuurlijk_persoon.deleted_on IS NULL"
        )

        args, kwargs = mock_organization_query.where.call_args
        assert isinstance(args[0], BooleanClauseList)
        assert (
            str(args[0])
            == "lower(bedrijf.search_term) LIKE lower(:search_term_1) AND bedrijf.deleted_on IS NULL"
        )

        args, kwargs = mock_employee_query.where.call_args
        assert isinstance(args[0], BooleanClauseList)
        assert (
            str(args[0])
            == """(lower(subject.username) LIKE lower(:username_1) OR lower((CAST(subject.properties AS JSON) ->> :param_1)) LIKE lower(:param_2) OR lower((CAST(subject.properties AS JSON) ->> :param_3)) LIKE lower(:param_4) OR lower((CAST(subject.properties AS JSON) ->> :param_5)) LIKE lower(:param_6)) AND array_length(subject.role_ids, :array_length_1) > :array_length_2"""
        )

        mock_union.assert_called_once_with(
            mock_natural_person_query.where().order_by().limit(),
            mock_organization_query.where().order_by().limit(),
            mock_employee_query.where().order_by().limit(),
        )

        mock_natural_person_query.reset_mock()
        mock_organization_query.reset_mock()
        mock_employee_query.reset_mock()
        mock_union.reset_mock()

        # Code under test:
        res = search_contact_query(keyword="testing", type_filter=set())

        assert res == "test union"

        mock_natural_person_query.where.assert_not_called()
        mock_organization_query.where.assert_not_called()
        mock_employee_query.where.assert_not_called()

        mock_union.assert_called_once_with()

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.union_all"
    )
    def test_get_person_or_company_by_uuid(self, mock_union):
        contact_uuid = uuid4()
        union_all = mock.MagicMock()
        union_all.order_by.return_value = "ordered_result"
        mock_union.return_value = union_all
        res = contact_query(contact_uuid=contact_uuid)

        compiled = res.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            "SELECT natuurlijk_persoon.id, natuurlijk_persoon.uuid, concat(natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS name, 'person' AS type \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID UNION ALL SELECT bedrijf.id, bedrijf.uuid, bedrijf.handelsnaam AS name, 'organization' AS type \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_2)s::UUID UNION ALL SELECT subject.id, subject.uuid, CAST(subject.properties AS JSON) ->> %(param_1)s AS name, 'employee' AS type \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )

    @mock.patch("sqlalchemy.orm.Session")
    def test_get_thread_list_query(self, mock_session):
        res = get_thread_list_query(
            case_uuid=uuid4(),
            contact_uuid=None,
            message_types={"note"},
            db=mock_session,
            user_uuid=uuid4(),
            permission="read",
        )

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)

    @mock.patch("sqlalchemy.orm.Session")
    def test_search_case_query(self, mock_session):
        res = search_cases_query(
            search_term="test | test",
            db=mock_session,
            user_uuid=uuid4(),
            permission="read",
        )

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)

    @mock.patch(
        "zsnl_domains.shared.repositories.case_acl.allowed_cases_subquery"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.pip_acl_query"
    )
    def test_list_cases_query(self, mock_pip_query, mock_acl_subquery):
        user_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        mock_acl_subquery.return_value = True
        mock_pip_query.return_value = []

        res = list_cases_query(
            db="fake_db",
            is_pip_user=False,
            user_uuid=user_uuid,
            contact_uuid=contact_uuid,
        )

        mock_acl_subquery.assert_called_once_with(
            db="fake_db", user_uuid=user_uuid, permission="read"
        )
        mock_pip_query.assert_called_once_with(contact_uuid)

        compiled = res.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            "SELECT zaak.id AS case_id, zaak.uuid AS case_uuid, zaak.status AS case_status, zaak.onderwerp AS case_description, zaak.onderwerp_extern AS case_description_public, zaaktype_node.titel AS case_type_name \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_meta.zaak_id = zaak.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND zaak.uuid IN (NULL) AND (1 != 1)"
        )

    @mock.patch(
        "zsnl_domains.shared.repositories.case_acl.allowed_cases_subquery"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.pip_acl_query"
    )
    def test_list_cases_query_pip(self, mock_pip_query, mock_acl_subquery):
        user_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        mock_acl_subquery.return_value = []
        mock_pip_query.return_value = []

        res = list_cases_query(
            db="fake_db",
            is_pip_user=True,
            user_uuid=user_uuid,
            contact_uuid=contact_uuid,
        )

        mock_acl_subquery.assert_not_called()
        mock_pip_query.assert_called_once_with(contact_uuid)

        compiled = res.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            "SELECT zaak.id AS case_id, zaak.uuid AS case_uuid, zaak.status AS case_status, zaak.onderwerp AS case_description, zaak.onderwerp_extern AS case_description_public, zaaktype_node.titel AS case_type_name \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_meta.zaak_id = zaak.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND zaak.uuid IN (NULL) AND (1 != 1)"
        )

    @mock.patch(
        "zsnl_domains.shared.repositories.case_acl.allowed_cases_subquery"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.pip_acl_query"
    )
    def test_get_file_by_attachment(self, mock_pip_query, mock_acl_subquery):
        db = mock.MagicMock()
        attach_uuid = uuid4()
        user_uuid = uuid4()
        is_pip = False

        mock_acl_subquery.return_value = True

        get_file_by_attachment(
            db=db,
            attachment_uuid=attach_uuid,
            user_uuid=user_uuid,
            is_pip=is_pip,
        )
        mock_acl_subquery.assert_called()
        mock_pip_query.assert_not_called()

    @mock.patch(
        "zsnl_domains.shared.repositories.case_acl.allowed_cases_subquery"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.pip_acl_query"
    )
    def test_get_file_by_attachment_pip_user(
        self, mock_pip_query, mock_acl_subquery
    ):
        db = mock.MagicMock()
        attach_uuid = uuid4()
        user_uuid = uuid4()
        is_pip = True
        get_file_by_attachment(
            db=db,
            attachment_uuid=attach_uuid,
            user_uuid=user_uuid,
            is_pip=is_pip,
        )
        mock_acl_subquery.assert_not_called()
        mock_pip_query.assert_called()

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries._case_acls_query"
    )
    def test_thread_by_uuid_statement(self, acl_func):
        session = "Session"
        tr_uuid = uuid4()
        check_acl = True
        permission = "write"
        user_uuid = uuid4()
        acl_func.return_value = None
        thread_by_uuid_statement(
            thread_uuid=tr_uuid,
            check_acl=check_acl,
            permission=permission,
            session=session,
            user_uuid=user_uuid,
        )
        acl_func.assert_called_with(
            db=session, user_uuid=user_uuid, permission=permission
        )

    def test_delete_message(self):
        session = mock.MagicMock()
        uuid = uuid4()
        ent_type = "ContactMoment"
        delete_message(session=session, uuid=uuid, entity_type=ent_type)

        ent_type = "ExternalMessage"
        delete_message(session=session, uuid=uuid, entity_type=ent_type)

    def test_delete_thread(self):
        uuid = uuid4()
        ret = delete_thread(uuid=uuid)

        assert isinstance(ret, Delete)

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.thread_list_query"
    )
    def test_thread_by_partial_uuid_statement(self, tlq):
        ret = thread_by_partial_uuid_statement("abc")

        assert str(tlq.where.call_args[0][0]) == str(
            and_(
                ~schema.Case.status.in_(["deleted", "resolved"]),
                schema.Case.deleted.is_(None),
                base62_decode("abc")
                == cast(
                    cast(
                        "x"
                        + sqlalchemy.func.translate(
                            sqlalchemy.func.right(
                                cast(
                                    schema.Thread.uuid, sqlalchemy.types.Text
                                ),
                                12,
                            ),
                            "-",
                            "",
                        ),
                        postgresql.BIT(48),
                    ),
                    sqlalchemy.types.BigInteger,
                ),
            )
        )
        assert ret == tlq.where()

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.union_all"
    )
    def test_contacts_query(self, mock_union):
        contact_uuid = uuid4()
        union_all = mock.MagicMock()
        union_all.order_by.return_value = "ordered_result"
        mock_union.return_value = union_all
        res = contacts_query(contact_uuids=[contact_uuid])

        compiled = res.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert compiled.params == {
            "param_1": "displayname",
            "subject_type_1": "employee",
            "uuid_1_1": contact_uuid,
            "uuid_2_1": contact_uuid,
            "uuid_3_1": contact_uuid,
            "voorletters_1": " ",
        }
        assert str(compiled) == (
            "SELECT natuurlijk_persoon.id, natuurlijk_persoon.uuid, concat(natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS name, 'person' AS type \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid IN (%(uuid_1_1)s::UUID) UNION ALL SELECT bedrijf.id, bedrijf.uuid, bedrijf.handelsnaam AS name, 'organization' AS type \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid IN (%(uuid_2_1)s::UUID) UNION ALL SELECT subject.id, subject.uuid, CAST(subject.properties AS JSON) ->> %(param_1)s AS name, 'employee' AS type \n"
            "FROM subject \n"
            "WHERE subject.uuid IN (%(uuid_3_1)s::UUID) AND subject.subject_type = %(subject_type_1)s"
        )
