# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Case, Contact, Thread
from zsnl_domains.communication.repositories import ThreadRepository


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestThreadRepository:
    def setup_method(self):
        with mock.patch("sqlalchemy.orm.Session") as mock_session:
            self.mock_infra = mock.MagicMock()
            self.infra = InfraFactoryMock(infra={"database": mock_session})
            self.infra_ro = InfraFactoryMock(infra={"database": mock_session})
            self.event_service = mock.MagicMock()

            self.thread_repo = ThreadRepository(
                infrastructure_factory=self.infra,  # type: ignore
                infrastructure_factory_ro=self.infra_ro,  # type: ignore
                context="no context",
                event_service=self.event_service,
                read_only=False,
            )
            self.thread_uuid = str(uuid4())

            self.mock_session = mock_session

            self.db = self.thread_repo._get_infrastructure("database")

    def test_get_session(self):
        assert self.thread_repo.session is self.infra.get_infrastructure(
            context=None, infrastructure_name="database"
        )

    def test_get_thread_list_for_case(self):
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid=uuid4(),
            contact_uuid=None,
            message_types="note",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

        threads = self.thread_repo.get_thread_list(
            case_uuid="\0",
            contact_uuid=None,
            message_types="note",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_for_contact(self):
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid=None,
            contact_uuid=uuid4(),
            message_types=None,
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_for_contact_null(self):
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid=None,
            contact_uuid="\0",
            message_types="note",
            user_uuid=uuid4(),
            permission="read",
            user_info=user_info,
            keyword=None,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_for_contact_and_case(self):
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid=uuid4(),
            contact_uuid=uuid4(),
            message_types="email,contact_moment,note",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list(self):
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid=None,
            contact_uuid=None,
            message_types="note",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_public(self):
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid="\0",
            contact_uuid="\0",
            message_types="note",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_for_pip_user(self):
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            case_uuid=uuid4(),
            contact_uuid=None,
            message_types=None,
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_for_pip_user_contact(self):
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            contact_uuid=uuid4(),
            case_uuid=None,
            message_types="email",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_for_pip_user_no_query_executed(self):
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        page = 1
        page_size = 10
        threads = self.thread_repo.get_thread_list(
            contact_uuid=None,
            case_uuid=None,
            message_types="email",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )
        assert isinstance(threads, list)

        for thread in threads:
            assert isinstance(thread, Thread)

    def test_get_thread_list_no_result(self):
        self.thread_repo._get_infrastructure(
            "database"
        ).query.side_effect = NoResultFound
        page = 1
        page_size = 10
        user_info = mock.MagicMock()
        user_info.permissions = {}
        result = self.thread_repo.get_thread_list(
            case_uuid=uuid4(),
            contact_uuid=uuid4(),
            message_types="email,note,contact_moment",
            user_uuid=uuid4(),
            permission="read",
            keyword=None,
            user_info=user_info,
            page=page,
            page_size=page_size,
        )

        assert result == []

    def test_create_thread(self):
        new_thread_uuid = uuid4()
        thread_type = "type"
        case = Case(
            uuid=uuid4(),
            id=6,
            description=None,
            description_public=None,
            case_type_name=None,
            status="open",
        )
        contact = Contact(uuid=uuid4(), name="test contact")

        self.thread_repo.create(new_thread_uuid, thread_type, contact, case)

    def test_transform_to_entity(self):
        contact_uuid = uuid4()
        case_uuid = uuid4()
        created = last_modified = datetime.now()
        last_message_cache = {"slug": "note cache", "message_type": "note"}
        number_of_messages = 12

        query_res = mock.MagicMock()
        query_res.uuid = uuid4()
        query_res.id = 2
        query_res.thread_type = "note"
        query_res.case_id = 1
        query_res.contact_uuid = contact_uuid
        query_res.contact_displayname = "admin"
        query_res.case_uuid = case_uuid
        query_res.created = created
        query_res.last_modified = last_modified
        query_res.last_message_cache = last_message_cache
        query_res.message_count = number_of_messages

        thread = self.thread_repo._transform_to_entity(query_res)

        assert isinstance(thread, Thread)
        assert thread.id == query_res.id
        assert thread.uuid == query_res.uuid
        assert thread.thread_type == query_res.thread_type

        assert thread.case.id == query_res.case_id
        assert thread.case.uuid == query_res.case_uuid

        assert thread.contact.uuid == query_res.contact_uuid
        assert thread.contact.name == query_res.contact_displayname

        assert thread.created == query_res.created
        assert thread.last_modified == query_res.last_modified
        assert thread.last_message_cache == query_res.last_message_cache
        assert thread.number_of_messages == query_res.message_count

        assert isinstance(thread.case, Case)
        assert isinstance(thread.contact, Contact)

    def test_transform_to_entity_no_case(self):
        contact_uuid = uuid4()
        created = last_modified = datetime.now()
        last_message_cache = {"slug": "note cache", "message_type": "note"}
        number_of_messages = 12

        query_res = mock.MagicMock()
        query_res.uuid = uuid4()
        query_res.id = 2
        query_res.thread_type = "note"
        query_res.case_id = None
        query_res.contact_uuid = contact_uuid
        query_res.contact_displayname = "admin"
        query_res.created = created
        query_res.last_modified = last_modified
        query_res.last_message_cache = last_message_cache
        query_res.message_count = number_of_messages

        thread = self.thread_repo._transform_to_entity(query_res)

        assert isinstance(thread, Thread)
        assert thread.id == query_res.id
        assert thread.uuid == query_res.uuid
        assert thread.thread_type == query_res.thread_type

        assert thread.contact.uuid == query_res.contact_uuid
        assert thread.contact.name == query_res.contact_displayname

        assert thread.created == query_res.created
        assert thread.last_modified == query_res.last_modified
        assert thread.last_message_cache == query_res.last_message_cache
        assert thread.number_of_messages == query_res.message_count

        assert isinstance(thread.contact, Contact)
        assert thread.case is None

    def test_save(self):
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        event_changes = [
            {
                "key": "uuid",
                "old_value": None,
                "new_value": "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2",
            },
            {
                "key": "thread_type",
                "old_value": None,
                "new_value": "contactmoment",
            },
            {
                "key": "contact",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "f6985c75-8824-4594-930c-b68fa4228179",
                },
            },
            {
                "key": "last_message_cache",
                "old_value": None,
                "new_value": {
                    "message_type": "contactmoment",
                    "slug": "test content test content",
                    "direction": "incoming",
                    "channel": "email",
                    "created_name": "admin",
                    "recipient_name": "test display name",
                    "created": "2019-08-14T19:33:14.754457",
                },
            },
            {
                "key": "created",
                "old_value": None,
                "new_value": "2019-08-14T19:33:14.754457",
            },
            {
                "key": "case",
                "old_value": None,
                "new_value": {
                    "type": "Case",
                    "entity_id": "f6985c75-8824-4594-930c-b68fa4228179",
                },
            },
        ]

        event = mock.MagicMock

        event.entity_type = "Thread"
        event.entity_id = self.thread_uuid
        event.event_name = "ThreadCreated"
        event.changes = event_changes
        event.entity_data = {}

        self.event_service.event_list = [event]
        exec_result = mock.MagicMock()
        exec_result.inserted_primary_key = [1, 2]
        self.mock_session.execute.return_value = exec_result

        self.thread_repo.save()
        self.mock_session.execute.assert_called()

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._delete_thread"
    )
    def test_save_deleted(self, delete_thread):
        event = mock.MagicMock()
        event.event_name = "ThreadDeleted"
        self.event_service.event_list = [event]
        self.thread_repo.save()
        delete_thread.assert_called_once_with(event)

    def test_save_thread_linked_to_case_event(self):
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        event_changes = [
            {
                "key": "last_message_cache",
                "old_value": None,
                "new_value": {"message_type": "email"},
            },
            {
                "key": "last_modified",
                "old_value": None,
                "new_value": "2019-08-14T19:33:14.754457",
            },
            {
                "key": "case",
                "old_value": None,
                "new_value": {
                    "type": "Case",
                    "entity_id": "f6985c75-8824-4594-930c-b68fa4228179",
                },
            },
        ]

        event = mock.MagicMock

        event.entity_type = "Thread"
        event.entity_id = self.thread_uuid
        event.event_name = "ThreadToCaseLinked"
        event.changes = event_changes
        event.entity_data = {}

        self.event_service.event_list = [event]

        exec_result = mock.MagicMock()
        exec_result.inserted_primary_key = [1, 2]
        self.mock_session.execute.return_value = exec_result

        self.thread_repo.save()
        self.mock_session.execute.assert_called()

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._generate_database_values"
    )
    def test_save_link_thread_integrity_violation(
        self, mock_generate_database_values
    ):
        event = mock.MagicMock

        event.entity_type = "Thread"
        event.entity_id = self.thread_uuid
        event.event_name = "ThreadToCaseLinked"
        event.entity_data = {}

        mock_generate_database_values.return_value = {
            "case_id": 1,
            "last_message_cache": {"message_type": "test"},
            "last_modified": "2019-08-14T19:33:14.754457",
        }
        self.mock_session.execute.side_effect = IntegrityError(
            statement=None, params={}, orig=Exception
        )
        with pytest.raises(Conflict) as excinfo:
            self.thread_repo._link_thread_to_case(event=event)

        assert excinfo.value.args == (
            f"Thread with uuid {self.thread_uuid} could not be linked to case number {1}",
            "communication/thread/not_linked",
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._generate_database_values"
    )
    def test_save_insert_thread_integrity_violation(
        self, mock_generate_database_values
    ):
        mock_generate_database_values.return_value = {
            "uuid": "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2"
        }
        self.mock_session.execute.side_effect = IntegrityError(
            statement=None, params={}, orig=Exception
        )
        with pytest.raises(Conflict) as excinfo:
            self.thread_repo.insert_thread(event="event")

        assert excinfo.value.args == (
            "Thread with uuid a5da1df2-7f50-4c4e-b46e-6fd7baabaed2 could not be created",
            "communication/thread/not_created",
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._transform_to_entity"
    )
    def test_get_thread_by_uuid(self, transform):
        thread_uuid = uuid4()
        user_uuid = uuid4()
        ThreadRow = namedtuple(
            "thread_row", "id uuid case_id last_message_cache "
        )
        thread_row = ThreadRow(
            id=2,
            uuid=thread_uuid,
            case_id=2,
            last_message_cache={"message_type": "pip"},
        )

        self.thread_repo.session.execute().fetchone.return_value = thread_row

        self.thread_repo.get_thread_by_uuid(
            thread_uuid=thread_uuid,
            check_acl=False,
            user_uuid=user_uuid,
            permission="view",
        )
        transform.assert_called_with(thread_row)

    def test_get_thread_by_uuid_no_result(self):
        self.thread_repo.session.execute().fetchone.return_value = None
        user_uuid = uuid4()

        thread_uuid = uuid4()
        res = self.thread_repo.get_thread_by_uuid(
            thread_uuid=thread_uuid,
            check_acl=False,
            user_uuid=user_uuid,
            permission="view",
        )
        assert res is None

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.delete_thread"
    )
    def test__delete_thread(self, del_thread):
        ent_uuid = str(uuid4())
        event = mock.MagicMock()
        event.entity_id = ent_uuid
        self.thread_repo._delete_thread(event)
        self.db.execute.assert_called()
        del_thread.assert_called_with(ent_uuid)

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._transform_to_entity"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.thread.database_queries"
    )
    def test_get_thread_by_partial_uuid(self, mock_queries, mock_xform):
        partial_uuid = "abcdefg"
        ThreadRow = namedtuple(
            "thread_row", "id uuid case_id last_message_cache "
        )
        thread_row = ThreadRow(
            id=2,
            uuid=str(uuid4()),
            case_id=2,
            last_message_cache={"message_type": "pip"},
        )

        self.thread_repo.session.execute().fetchall.return_value = [thread_row]

        self.thread_repo.get_thread_by_partial_uuid(partial_uuid=partial_uuid)

        mock_xform.assert_called_with(thread_row)

        mock_queries.thread_by_partial_uuid_statement.assert_called_once_with(
            partial_uuid=partial_uuid
        )
        self.thread_repo.session.execute.assert_called_with(
            mock_queries.thread_by_partial_uuid_statement()
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.database_queries"
    )
    def test_get_thread_by_partial_uuid_none(self, mock_queries):
        partial_uuid = "abcdefg"

        self.thread_repo.session.execute().fetchall.return_value = []

        with pytest.raises(NotFound):
            self.thread_repo.get_thread_by_partial_uuid(
                partial_uuid=partial_uuid
            )

        mock_queries.thread_by_partial_uuid_statement.assert_called_once_with(
            partial_uuid=partial_uuid
        )
        self.thread_repo.session.execute.assert_called_with(
            mock_queries.thread_by_partial_uuid_statement()
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.database_queries"
    )
    def test_get_thread_by_partial_uuid_multiple(self, mock_queries):
        partial_uuid = "abcdefg"

        self.thread_repo.session.execute().fetchall.return_value = ["a", "b"]

        with pytest.raises(Conflict):
            self.thread_repo.get_thread_by_partial_uuid(
                partial_uuid=partial_uuid
            )

        mock_queries.thread_by_partial_uuid_statement.assert_called_once_with(
            partial_uuid=partial_uuid
        )
        self.thread_repo.session.execute.assert_called_with(
            mock_queries.thread_by_partial_uuid_statement()
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._transform_to_entity"
    )
    def test_get_thread_for_pip_user_by_uuid(self, transform):
        thread_uuid = uuid4()
        user_uuid = uuid4()
        ThreadRow = namedtuple(
            "thread_row", "id uuid case_id last_message_cache "
        )
        thread_row = ThreadRow(
            id=2,
            uuid=thread_uuid,
            case_id=2,
            last_message_cache={"message_type": "pip"},
        )

        self.thread_repo.session.execute().fetchone.return_value = thread_row

        self.thread_repo.get_thread_for_pip_by_uuid(
            thread_uuid=thread_uuid, user_uuid=user_uuid
        )
        transform.assert_called_with(thread_row)

        self.thread_repo.session.execute().fetchone.return_value = None
        self.thread_repo.get_thread_for_pip_by_uuid(
            thread_uuid=thread_uuid, user_uuid=user_uuid
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._generate_database_values"
    )
    def test__update_thread_unread_count_updated(
        self, mock_generate_database_values
    ):
        event = mock.MagicMock

        event.entity_type = "Thread"
        event.entity_id = self.thread_uuid
        event.event_name = "ThreadUnReadCountIncremented"
        event.entity_data = {}

        mock_generate_database_values.return_value = {"unread_pip_count": 1}

        self.thread_repo._update_thread_unread_count(event=event)
        mock_generate_database_values.assert_called_once_with(event=event)

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._update_thread_unread_count"
    )
    def test_save_thread_unread_count_decremeneted(
        self, update_thread_unread_count
    ):
        event = mock.MagicMock()
        event.event_name = "ThreadUnReadCountDecremented"
        self.event_service.event_list = [event]
        self.thread_repo.save()
        update_thread_unread_count.assert_called_once_with(event)

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._generate_database_values"
    )
    def test__thread_attachment_count_updated(
        self, mock_generate_database_values
    ):
        event = mock.MagicMock

        event.entity_type = "Thread"
        event.entity_id = self.thread_uuid
        event.event_name = "ThreadAttachmentCountIncremented"
        event.entity_data = {}

        mock_generate_database_values.return_value = {"attachment_count": 3}

        self.thread_repo._update_thread_attachment_count(event=event)
        mock_generate_database_values.assert_called_once_with(event=event)

    @mock.patch(
        "zsnl_domains.communication.repositories.thread.ThreadRepository._update_thread_attachment_count"
    )
    def test_save_thread_attachment_count_incremeneted(
        self, update_thread_attachment_count
    ):
        event = mock.MagicMock()
        event.event_name = "ThreadAttachmentCountIncremented"
        self.event_service.event_list = [event]
        self.thread_repo.save()
        update_thread_attachment_count.assert_called_once_with(event)

    def test__save_mark_message_unread(self):
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        event_changes = [
            {"key": "unread_employee_count", "old_value": 1, "new_value": 0}
        ]
        event = mock.MagicMock

        event.entity_type = "Thread"
        event.entity_id = self.thread_uuid
        event.event_name = "ThreadUnReadCountDecremented"
        event.changes = event_changes
        event.entity_data = {}

        self.event_service.event_list = [event]
        self.mock_session.execute().fetchone.side_effect = [
            namedtuple("RowProxy", "unread_employee_count unread_pip_count")(
                unread_employee_count=1, unread_pip_count=1
            ),
            None,
        ]
        self.thread_repo.save()
        self.mock_session.execute.assert_called()

    def test__validate_unread_messages_count(self):
        event_name = "ThreadUnReadCountDecremented"
        thread_uuid = uuid4()
        type = "unread_employee_count"
        thread_count = 0

        self.mock_session.execute().fetchone.return_value = namedtuple(
            "RowProxy", "unread_employee_count unread_pip_count"
        )(unread_employee_count=0, unread_pip_count=1)

        result = self.thread_repo._validate_unread_messages_count(
            event_name, thread_uuid, type, thread_count
        )
        assert result == 0

        event_name = "ThreadUnReadCountIncremented"
        thread_count = 1

        self.mock_session.execute().fetchone.return_value = namedtuple(
            "RowProxy", "unread_employee_count unread_pip_count"
        )(unread_employee_count=0, unread_pip_count=1)

        result = self.thread_repo._validate_unread_messages_count(
            event_name, thread_uuid, type, thread_count
        )
        assert result == 1

        event_name = "ThreadUnReadCountDecremented"
        thread_uuid = uuid4()
        type = "unread_pip_count"
        thread_count = 0

        self.mock_session.execute().fetchone.return_value = namedtuple(
            "RowProxy", "unread_employee_count unread_pip_count"
        )(unread_employee_count=0, unread_pip_count=1)

        result = self.thread_repo._validate_unread_messages_count(
            event_name, thread_uuid, type, thread_count
        )
        assert result == 0

        event_name = "ThreadUnReadCountIncremented"
        thread_count = 1

        self.mock_session.execute().fetchone.return_value = namedtuple(
            "RowProxy", "unread_employee_count unread_pip_count"
        )(unread_employee_count=0, unread_pip_count=0)

        result = self.thread_repo._validate_unread_messages_count(
            event_name, thread_uuid, type, thread_count
        )
        assert result == 1
