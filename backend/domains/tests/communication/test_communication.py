# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from uuid import uuid4
from zsnl_domains import communication
from zsnl_domains.communication import Commands, Queries


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCommunicationGeneral:
    user_uuid = uuid4

    def test_get_query_instance(self):
        repo = "repository_factory"
        context = None
        qry = communication.get_query_instance(
            repository_factory=repo, context=context, user_uuid=self.user_uuid
        )
        assert isinstance(qry, Queries)
        assert qry.repository_factory == "repository_factory"
        assert qry.context is None
        assert qry.user_uuid == self.user_uuid

    def test_get_command_instance(self):
        repo = "repository_factory"
        context = None
        cmd = communication.get_command_instance(
            repository_factory=repo,
            context=context,
            user_uuid=self.user_uuid,
            event_service="event_service",
        )
        assert isinstance(cmd, Commands)
        assert cmd.repository_factory == "repository_factory"
        assert cmd.context is None
        assert cmd.user_uuid == self.user_uuid
