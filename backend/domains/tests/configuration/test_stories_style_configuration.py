# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import io
import pytest
import zipfile
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import NotFound, ValidationError
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import configuration
from zsnl_domains.configuration.entities.tenant import Tenant


@pytest.fixture
def valid_zip_file():
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "w") as zip_file:
        zip_file.writestr("path/config.json", "valid content")
    zip_buffer.seek(0)
    return zip_buffer


@pytest.fixture
def oversized_zip_file():
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "w") as zip_file:
        for no in range(11):
            zip_file.writestr(f"config_{no}.json", "a" * 1000000)
    zip_buffer.seek(0)
    return zip_buffer


class Test_Style_Configuration_Commands(TestBase):
    def setup_method(self):
        self.load_command_instance(configuration)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.cmd.user_info = self.user_info

    def test_create_style(self, valid_zip_file):
        tenant = "test.zaaksysteem.nl"
        self.cmd.repository_factory.repositories[
            "style_configuration"
        ].context = "dev.zaaksysteem.nl"
        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "VirtualHosts": {
                    "template1.dev.zaaksysteem.app": {"customer_id": "xxllnc"},
                    "test.zaaksysteem.nl": {"customer_id": "test"},
                },
            }
        )

        self.cmd.upload_template(
            tenant=tenant, file=valid_zip_file, file_name="test.zss"
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3
        update = self.session.execute.call_args_list[1][0][0]
        update_compiled = update.compile(dialect=postgresql.dialect())

        assert str(update_compiled) == (
            "UPDATE style_configuration SET uuid=%(uuid)s::UUID, tenant=%(tenant)s, name=%(name)s, extension=%(extension)s, content=%(content)s, last_modified=%(last_modified)s, mimetype=%(mimetype)s WHERE style_configuration.tenant = %(tenant_1)s AND style_configuration.extension = %(extension_1)s"
        )

    def test_create_style_default_tenant(self, valid_zip_file):
        tenant = None
        self.cmd.repository_factory.repositories[
            "style_configuration"
        ].context = "dev.zaaksysteem.nl"
        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "VirtualHosts": {
                    "template1.dev.zaaksysteem.app": {"customer_id": "xxllnc"},
                    "test.zaaksysteem.nl": {"customer_id": "test"},
                },
            }
        )
        self.cmd.context = "dev.zaaksysteem.nl"
        self.session.execute().fetchone.return_value = None
        self.session.execute.reset_mock()
        self.cmd.upload_template(
            tenant=tenant, file=valid_zip_file, file_name="test.zss"
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3
        insert = self.session.execute.call_args_list[1][0][0]
        insert_compiled = insert.compile(dialect=postgresql.dialect())

        assert str(insert_compiled) == (
            "INSERT INTO style_configuration (uuid, tenant, name, extension, content, last_modified, mimetype) VALUES (%(uuid)s::UUID, %(tenant)s, %(name)s, %(extension)s, %(content)s, %(last_modified)s, %(mimetype)s) RETURNING style_configuration.id"
        )

    def test_create_style_invalid_size(self, oversized_zip_file):
        tenant = "test.zaaksysteem.nl"
        self.cmd.repository_factory.repositories[
            "style_configuration"
        ].context = "dev.zaaksysteem.nl"
        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "VirtualHosts": {
                    "template1.dev.zaaksysteem.app": {"customer_id": "xxllnc"},
                    "test.zaaksysteem.nl": {"customer_id": "test"},
                },
            }
        )

        with pytest.raises(ValidationError) as excinfo:
            self.cmd.upload_template(
                tenant=tenant, file=oversized_zip_file, file_name="test.zss"
            )
        assert excinfo.value.args == ("Maximum allowed file size is 10MB.",)

    def test_create_style_invalid_tenant(self):
        tenant = "test.invalid.zaaksysteem.nl"

        self.cmd.repository_factory.repositories[
            "style_configuration"
        ].context = "dev.zaaksysteem.nl"
        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "VirtualHosts": {
                    "template1.dev.zaaksysteem.app": {"customer_id": "xxllnc"},
                    "test.zaaksysteem.nl": {"customer_id": "test"},
                },
            }
        )
        with pytest.raises(ValidationError) as excinfo:
            file_content = b"test zip file"
            self.cmd.upload_template(
                tenant=tenant, file=file_content, file_name="test.zss"
            )
            assert excinfo.value.args[0][0]["message"] == (
                "Invalid tenant.Valid tenants are template1.dev.zaaksysteem.app,test.zaaksysteem.nl",
                "tenant/invalid",
            )


class Test_Style_Configuration_queries(TestBase):
    def setup_method(self):
        self.load_query_instance(configuration)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_get_tenants(self):
        tenant_list = [mock.Mock(), mock.Mock()]
        self.qry.repository_factory.repositories[
            "style_configuration"
        ].context = "dev.zaaksysteem.nl"
        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "VirtualHosts": {
                    "template1.dev.zaaksysteem.app": {"customer_id": "xxllnc"},
                    "test.zaaksysteem.nl": {"customer_id": "test"},
                },
            }
        )
        tenant_list[0].configure_mock(name="template1.dev.zaaksysteem.app")
        tenant_list[1].configure_mock(name="test.zaaksysteem.nl")
        style_config_list = mock.Mock()
        style_config_list.configure_mock(
            tenant="dev.zaaksysteem.nl",
            name="test",
            last_modified="2023-12-05T12:39:29.266635+00:00",
            content="eyJzaXRlSW1wcm92SWQiOiIxMjM0In0=",
            extension="zss",
            uuid=uuid4(),
            mimetype="application/zip",
        )

        self.session.execute().fetchall.return_value = [style_config_list]
        self.session.execute.reset_mock()
        tenants = self.qry.get_tenants()

        assert isinstance(tenants[0], Tenant)

        assert len(tenants) == 3

        assert tenants[0].name == "dev.zaaksysteem.nl"
        assert tenants[1].name == "template1.dev.zaaksysteem.app"
        assert tenants[2].name == "test.zaaksysteem.nl"

    def test_get_style_config(self):
        style_config_list = mock.Mock()
        uuid_1 = uuid4()
        tenant = "test.zaaksysteem.nl"
        style_config_list.configure_mock(
            tenant="dev.zaaksysteem.nl",
            name="config",
            last_modified="2023-12-05T12:39:29.266635+00:00",
            content="eyJzaXRlSW1wcm92SWQiOiIxMjM0In0=",
            extension="json",
            uuid=uuid_1,
            mimetype="application/json",
        )

        self.session.execute().fetchone.return_value = style_config_list
        self.session.execute.reset_mock()

        self.qry.get_content(name="config", tenant=tenant)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        style_content_query = self.session.execute.call_args_list[0][0][0]
        qry_compiled = style_content_query.compile(
            dialect=postgresql.dialect()
        )

        assert str(qry_compiled) == (
            "SELECT style_configuration.content, style_configuration.last_modified, style_configuration.name, style_configuration.tenant, style_configuration.uuid, style_configuration.extension, style_configuration.mimetype \n"
            "FROM style_configuration \n"
            "WHERE style_configuration.name = %(name_1)s AND style_configuration.tenant = %(tenant_1)s"
        )

    def test_get_style_config_default_tenant(self):
        style_config_list = mock.Mock()
        uuid_1 = uuid4()
        style_config_list.configure_mock(
            tenant="dev.zaaksysteem.nl",
            name="config",
            last_modified="2023-12-05T12:39:29.266635+00:00",
            content="eyJzaXRlSW1wcm92SWQiOiIxMjM0In0=",
            extension="json",
            uuid=uuid_1,
            mimetype="application/json",
        )

        self.qry.context = "dev.zaaksysteem.nl"

        self.session.execute().fetchone.return_value = style_config_list
        self.session.execute.reset_mock()
        self.qry.get_content(name="config")

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        style_content_query = self.session.execute.call_args_list[0][0][0]
        qry_compiled = style_content_query.compile(
            dialect=postgresql.dialect()
        )

        assert str(qry_compiled) == (
            "SELECT style_configuration.content, style_configuration.last_modified, style_configuration.name, style_configuration.tenant, style_configuration.uuid, style_configuration.extension, style_configuration.mimetype \n"
            "FROM style_configuration \n"
            "WHERE style_configuration.name = %(name_1)s AND style_configuration.tenant = %(tenant_1)s"
        )

    def test_get_style_config_error(self):
        self.qry.context = "dev.zaaksysteem.nl"

        self.session.execute().fetchone.return_value = None
        self.session.execute.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_content(name="config")
            assert excinfo.value.message == ("No config found for config.")
