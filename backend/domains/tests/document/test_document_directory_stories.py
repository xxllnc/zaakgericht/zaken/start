# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from dataclasses import dataclass
from minty.cqrs import EventService
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from minty.repository import RepositoryFactory
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import document
from zsnl_domains.case_management import CaseRepository
from zsnl_domains.document import DirectoryRepository
from zsnl_domains.document.entities import Directory


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


@dataclass
class DirectoryRow:
    id: int
    name: str
    case_id: int
    original_name: str
    path: list
    uuid: UUID
    parent: int


@dataclass
class CaseRow:
    id: int
    uuid: UUID


class TestDirectory(TestBase):
    def setup_method(self):
        # Setup user_info mock
        self.user_uuid = uuid4()
        self.is_pip_user = False
        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(self.user_uuid)
        self.user_info.permissions = {"pip_user": self.is_pip_user}

        # Setup the mock infrastructures
        self.mock_infrastructures = {"database": mock.MagicMock()}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )
        self.mock_infra_ro = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        # Setup and register the mock Document repository
        repo_factory = RepositoryFactory(
            infrastructure_factory=self.mock_infra,
            infrastructure_factory_ro=self.mock_infra_ro,
        )
        repo_factory.register_repository("case", CaseRepository)
        repo_factory.register_repository("directory", DirectoryRepository)

        # Create an EventService
        self.event_service = EventService(
            correlation_id=str(uuid4()),
            domain="test_domain",
            context="test_context",
            user_uuid=self.user_uuid,
        )

        self.directory_repo = DirectoryRepository(
            infrastructure_factory=self.mock_infra,
            infrastructure_factory_ro=self.mock_infra_ro,
            context="test_context",
            event_service=self.event_service,
            read_only=False,
        )

        self.case_repository = CaseRepository(
            infrastructure_factory=self.mock_infra,
            infrastructure_factory_ro=self.mock_infra_ro,
            context="test_context",
            event_service=self.event_service,
            read_only=False,
        )

        # sets self.cmd to a Command instance based on the mock infra
        self.load_command_instance(document, self.mock_infrastructures)
        self.cmd.user_info = self.user_info

        self.new_directory_uuid = uuid4()
        self.directory_name = "my_new_directory"
        self.case_uuid = uuid4()
        self.case_id = 22
        self.parent = 567

        self.directory_row = DirectoryRow(
            id=1,
            name=self.directory_name,
            case_id=self.case_id,
            original_name=self.directory_name,
            path=[],
            uuid=self.new_directory_uuid,
            parent=self.parent,
        )

        self.case_row = CaseRow(id=self.case_id, uuid=self.case_uuid)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_create_new_root_directory(self, mock_find_case_by_uuid):
        self.session.reset_mock()

        directory_name = "my_new_root_directory"
        self.directory_row.name = self.directory_row.original_name = (
            directory_name
        )
        self.cmd.user_info = self.user_info
        mock_find_case_by_uuid.return_value = self.case_row

        self.cmd.create_directory(
            directory_uuid=str(self.new_directory_uuid),
            case_uuid=str(self.case_uuid),
            name=self.directory_name,
            parent_uuid="",
        )

        # func_name, args, kwargs = mock_create_create_directory.mock_calls[0]
        # assert args[0] == str(self.new_directory_uuid)
        # assert args[1] == self.directory_name
        # assert args[2] == self.case_id
        # assert args[3] == []

    def test_create_new_root_directory_with_non_existing_case_raises_conflict(
        self,
    ):
        invalid_case_uuid = "12345678-1234-1234-1234-123456789012"

        with pytest.raises(NotFound) as exception_info:
            self.cmd.user_info = self.user_info
            self.case_repository.session.execute().fetchone.side_effect = [
                None
            ]
            self.cmd.create_directory(
                directory_uuid=str(self.new_directory_uuid),
                case_uuid=invalid_case_uuid,
                name=self.directory_name,
                parent_uuid="",
            )

        assert (
            exception_info.value.args[0]
            == f"Case with uuid '{invalid_case_uuid}' not found."
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.document.repositories.directory.DirectoryRepository.get_directory_by_uuid"
    )
    def test_create_subdirectory(
        self, mock_get_directory, mock_find_case_by_uuid
    ):
        parent_directory = DirectoryRow(
            id=88,
            name="Another Name",
            case_id=self.case_id,
            original_name="Another Name",
            path=[48],
            uuid=self.new_directory_uuid,
            parent=48,
        )

        self.session.reset_mock()
        self.cmd.user_info = self.user_info
        mock_find_case_by_uuid.return_value = self.case_row
        mock_get_directory.return_value = parent_directory

        self.directory_repo.session.execute().fetchone.side_effect = [
            parent_directory
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.directory.DirectoryRepository.create_directory"
        ) as mock_create_directory:
            self.cmd.create_directory(
                directory_uuid=str(self.new_directory_uuid),
                case_uuid=str(self.case_uuid),
                name=self.directory_name,
                parent_uuid=str(uuid4()),
            )

            func_name, args, kwargs = mock_create_directory.mock_calls[0]
            # Assert that the Directory-entry has gotten the right path.
            assert args[3] == [48, 88]

    def test_get_directory_by_uuid_raises_conflict_if_not_found(self):
        invalid_directory_uuid = "12345678-1234-1234-1234-123456789012"
        self.directory_repo.session.execute().fetchone.side_effect = [None]
        self.cmd.user_info = self.user_info
        with pytest.raises(Conflict) as exception_info:
            self.directory_repo.get_directory_by_uuid(invalid_directory_uuid)

        assert (
            exception_info.value.args[0]
            == f"Could not find directory with uuid '{invalid_directory_uuid}'"
        )
        assert (
            exception_info.value.args[1]
            == "domains/document/repositories/get_directory_by_uuid"
        )

    def test_get_directory_by_uuid(self):
        self.directory_repo.session.execute().fetchone.side_effect = [
            self.directory_row
        ]

        result = self.directory_repo.get_directory_by_uuid(uuid4())

        assert isinstance(result, Directory)

        assert result.uuid == self.new_directory_uuid
        assert result.name == self.directory_name
        assert result.parent == self.parent
        assert result.case_id == self.case_id
        assert result.path == []

    def test_get_directory_id_by_uuid_raised_conflict_if_not_found(self):
        invalid_directory_uuid = "12345678-1234-1234-1234-123456789012"
        self.directory_repo.session.execute().fetchone.side_effect = [None]

        with pytest.raises(Conflict) as exception_info:
            self.directory_repo.get_directory_id_by_uuid(
                invalid_directory_uuid
            )

        assert (
            exception_info.value.args[0]
            == f"Could not find directory with uuid '{invalid_directory_uuid}'"
        )
        assert (
            exception_info.value.args[1]
            == "domains/document/repositories/get_directory_id_by_uuid"
        )

    def test_create_subdirectory_without_matching_case_raises_conflict(self):
        # self.session.reset_mock()
        # self.case_repository.session.execute().side_effect = [
        #     self.case_row
        # ]
        #
        # self.cmd.create_directory(
        #     directory_uuid=str(self.new_directory_uuid),
        #     case_uuid=str(self.case_uuid),
        #     name=self.directory_name,
        #     parent_uuid=str(uuid4()),
        # )
        pass

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_create_new_sub_directory_creates_path_array(
        self, mock_find_case_by_uuid
    ):
        self.session.reset_mock()

        directory_uuid_1 = uuid4()
        directory_row_1 = DirectoryRow(
            id=88,
            name="Name",
            case_id=self.case_id,
            original_name="Name",
            path=[12, 13],
            uuid=directory_uuid_1,
            parent=10,
        )

        mock_find_case_by_uuid.return_value = self.case_row

        with mock.patch(
            "zsnl_domains.document.repositories.directory.DirectoryRepository.get_directory_by_uuid"
        ) as mock_get_dir:
            with mock.patch(
                "zsnl_domains.document.repositories.directory.DirectoryRepository.get_directory_id_by_uuid"
            ) as mock_get_id:
                mock_get_dir.return_value = directory_row_1
                mock_get_id.return_value = 10

                self.cmd.create_directory(
                    directory_uuid=str(self.new_directory_uuid),
                    case_uuid=str(self.case_uuid),
                    name=self.directory_name,
                    parent_uuid=str(uuid4()),
                )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_create_new_root_directory_with_same_case_id_and_name_raises_conflict(
        self, mock_find_case_by_uuid
    ):
        self.session.reset_mock()
        directory_name = "my_new_root_directory"
        self.directory_row.name = self.directory_row.original_name = (
            directory_name
        )
        self.cmd.user_info = self.user_info
        mock_find_case_by_uuid.return_value = self.case_row

        self.cmd.create_directory(
            directory_uuid=str(self.new_directory_uuid),
            case_uuid=str(self.case_uuid),
            name=self.directory_name,
            parent_uuid="",
        )

        self.cmd.create_directory(
            directory_uuid=str(self.new_directory_uuid),
            case_uuid=str(self.case_uuid),
            name=self.directory_name,
            parent_uuid="",
        )
