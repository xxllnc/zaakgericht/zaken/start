# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import Conflict, Forbidden
from unittest import mock
from uuid import uuid4
from zsnl_domains.document.entities.document import Document, File


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestDocumentEntity:
    def test_create_document(self):
        valid_document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()
        document = Document(valid_document_uuid)
        document.event_service = mock.MagicMock()

        create_arguments = {
            "basename": "fake_basename",
            "extension": ".pdf",
            "accepted": True,
            "store_uuid": store_uuid,
            "directory_uuid": directory_uuid,
            "case_uuid": case_uuid,
            "mimetype": "application/pdf",
            "size": 4,
            "storage_location": "fake_storage_location",
            "md5": "md5",
        }
        # Basic creation - happy path
        document.create(**create_arguments)
        assert document.storage_location == "fake_storage_location"
        assert document.accepted is True
        assert document.basename == "fake_basename"
        assert document.extension == ".pdf"
        assert document.store_uuid == store_uuid
        assert document.directory_uuid == directory_uuid
        assert document.case_uuid == case_uuid
        assert document.mimetype == "application/pdf"
        assert document.size == 4
        assert document.storage_location == "fake_storage_location"

        # Store UUID already set
        with pytest.raises(Conflict):
            document.create(**create_arguments)

        with pytest.raises(Forbidden):
            document = Document(uuid4())
            create_arguments["basename"] = ".test"
            document.create(**create_arguments)

        with pytest.raises(Forbidden):
            document = Document(uuid4())
            create_arguments["basename"] = "example"
            create_arguments["extension"] = ".txt"
            create_arguments["mimetype"] = "image/jpeg"
            document.create(**create_arguments)

    def test_create_document_from_attachment(self):
        attachment_uuid = uuid4()
        document = Document(document_uuid=uuid4())
        document.event_service = mock.MagicMock()

        create_arguments = {"attachment_uuid": attachment_uuid}
        # Basic creation
        document.create_document_from_attachment(**create_arguments)
        assert document.attachment_uuid == attachment_uuid

    def test_create_document_extended_mimetype(self):
        valid_document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()
        document = Document(valid_document_uuid)
        document.event_service = mock.MagicMock()

        create_arguments = {
            "basename": "sample",
            "extension": ".m4a",
            "accepted": True,
            "store_uuid": store_uuid,
            "directory_uuid": directory_uuid,
            "case_uuid": case_uuid,
            "mimetype": "audio/mp4",
            "size": 4,
            "storage_location": "fake_storage_location",
            "md5": "md5",
        }
        with pytest.raises(Forbidden) as exception_info:
            document.create(**create_arguments)
        assert exception_info.value.args == (
            "Unknown file extension or file type (extension='.m4a', mimetype='audio/mp4').",
            "file/type_not_allowed",
        )

        # add accept_extended_mimetypes argument
        document = Document(valid_document_uuid)
        document.event_service = mock.MagicMock()
        create_arguments["accept_extended_mimetypes"] = True
        document.create(**create_arguments)

        assert document.storage_location == "fake_storage_location"
        assert document.accepted is True
        assert document.basename == "sample"
        assert document.extension == ".m4a"
        assert document.store_uuid == store_uuid
        assert document.directory_uuid == directory_uuid
        assert document.case_uuid == case_uuid
        assert document.mimetype == "audio/mp4"
        assert document.size == 4
        assert document.storage_location == "fake_storage_location"


class TestFileEntity:
    def test_create_file(self):
        file_uuid = uuid4()
        file_entity = File(file_uuid)
        file_entity.event_service = mock.MagicMock()

        create_arguments = {
            "basename": "fake_basename",
            "extension": ".txt",
            "mimetype": "text/plain",
            "size": 4,
            "storage_location": "fake_storage_location",
            "md5": "md5",
        }
        # Basic creation - happy path
        file_entity.create(**create_arguments)
        assert file_entity.storage_location == "fake_storage_location"
        assert file_entity.basename == "fake_basename"
        assert file_entity.extension == ".txt"
        assert file_entity.mimetype == "text/plain"
        assert file_entity.size == 4
        assert file_entity.storage_location == "fake_storage_location"
        assert (
            file_entity.filename
            == file_entity.basename + file_entity.extension
        )

    def test_create_file_fail(self):
        file_uuid = uuid4()
        file_entity = File(file_uuid)
        file_entity.event_service = mock.MagicMock()

        create_arguments = {
            "basename": "",
            "extension": ".txt",
            "mimetype": "text/plain",
            "size": 4,
            "storage_location": "fake_storage_location",
            "md5": "md5",
        }

        with pytest.raises(Forbidden):
            file_entity.create(**create_arguments)
