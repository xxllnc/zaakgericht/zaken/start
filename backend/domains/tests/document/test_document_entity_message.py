# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime, timezone
from uuid import uuid4
from zsnl_domains.document.entities.message import Message


class TestMessageEntity:
    def test_message_entity_has_required_attributes(self):
        now = datetime.now(timezone.utc)
        params = {
            "uuid": uuid4(),
            "case_uuid": uuid4(),
            "created_by_displayname": "Admin",
            "created": now,
            "last_modified": now,
            "message_date": now,
            "message_type": "external",
        }

        message = Message(**params)

        assert message.uuid == params["uuid"]
        assert message.case_uuid == params["case_uuid"]
        assert message.message_type == params["message_type"]
        assert (
            message.created_by_displayname == params["created_by_displayname"]
        )
        assert message.created == params["created"]
        assert message.last_modified == params["last_modified"]
        assert message.message_date == params["message_date"]
