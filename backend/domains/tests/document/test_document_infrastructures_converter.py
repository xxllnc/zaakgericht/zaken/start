# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
import pytest
import urllib
from minty import exceptions
from minty.cqrs.test import TestBase
from unittest import mock
from zsnl_domains.document.infrastructures import ConverterInfrastructure


class Test_ConverterInfrastructure(TestBase):
    def setup_method(self):
        config = {"converter_base_url": "http://converter"}
        self.converter = ConverterInfrastructure()(config=config)

    @mock.patch(
        "zsnl_domains.document.infrastructures.converter.urllib.request"
    )
    def test_convert(self, mock_urllib_request):
        response = b"I am a converted string"
        converter_service_return_value = json.dumps(
            {"content": base64.b64encode(response).decode("utf-8")}
        )
        mock_urllib_request.urlopen().read.return_value = (
            converter_service_return_value
        )

        content = b"I am a python string"
        res = self.converter.convert(
            to_type="application/pdf", content=content
        )
        assert res == response

        mock_urllib_request.urlopen.side_effect = urllib.error.HTTPError(
            url="http://converter",
            code="converter/server_error",
            msg="Cannot convert to mimetype 'pdf'",
            hdrs=None,
            fp=None,
        )

        with pytest.raises(exceptions.Conflict) as excinfo:
            self.converter.convert(to_type="pdf", content=content)

        assert excinfo.value.args == ("Error during document conversion",)
