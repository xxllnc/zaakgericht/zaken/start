# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
import pytest
import random
from minty.cqrs.test import TestBase
from sqlalchemy import exc as sql_exc
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import document
from zsnl_domains.document.entities.document import MAX_SIZE_FOR_PREVIEW


def _make_document_row(
    id: str,
    mimetype="text/plain",
    extension=".txt",
    size: int | None = 100,
    virus_scan_status="ok",
    has_preview=False,
    has_thumbnail=False,
    document_uuid=None,
    store_uuid=None,
):
    document_row = mock.Mock(name=f"document_row_{id}")
    document_row.configure_mock(
        document_uuid=document_uuid if document_uuid else uuid4(),
        store_uuid=store_uuid if store_uuid else uuid4(),
        mimetype=mimetype,
        size=size,
        storage_location=["the moon"],
        md5="",
        is_archivable=True,
        virus_scan_status=virus_scan_status,
        directory_uuid=None,
        case_uuid=uuid4(),
        case_display_number=random.randint(1, 1_000_000),
        filename="example",
        extension=extension,
        accepted=True,
        version=random.randint(1, 100),
        intake_group_uuid=None,
        intake_role_uuid=None,
        intake_owner_uuid=None,
        creator_uuid=uuid4(),
        creator_displayname="Piet Friet",
        date_modified="",
        id=random.randint(1, 1_000_000),
        preview_uuid=uuid4() if has_preview else None,
        preview_storage_location=["here"] if has_preview else None,
        preview_mimetype="application/pdf" if has_preview else None,
        description="Some text",
        origin=None,
        origin_date=None,
        confidentiality="Openbaar",
        thumbnail_uuid=uuid4() if has_thumbnail else None,
        thumbnail_storage_location=["there"] if has_thumbnail else None,
        thumbnail_mimetype="image/png" if has_thumbnail else None,
        labels=[],
        document_source="test_source",
        document_status="copy",
    )

    return document_row


class PreviewBase(TestBase):
    def setup_method(self) -> None:
        # This sets `self.session`
        self.mock_converter = mock.Mock()
        self.mock_s3 = mock.Mock()
        self.mock_redis = mock.Mock()

        self.mock_redis.get.return_value = None

        mock_infra = {
            "converter": self.mock_converter,
            "s3": self.mock_s3,
            "redis": self.mock_redis,
        }

        self.load_command_instance(document, inframocks=mock_infra)

        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
                "rate_limits": {
                    "command:create_preview": 5,
                    "command:create_thumbnail": 5,
                },
            }
        )

        self.cmd.user_info.permissions = {"admin": True}

        self.executed_queries = []
        self.mock_rows = []

        def mock_execute(query):
            self.executed_queries.append(query)

            try:
                rv = self.mock_rows.pop(0)
            except IndexError as e:
                raise IndexError(query) from e

            if isinstance(rv, Exception):
                raise rv

            return rv

        self.session.execute = mock_execute

    def load_database_rows(self, mock_rows: list):
        self.mock_rows = mock_rows
        self.executed_queries = []


class TestCreatePreviewForDocumentsForFile(PreviewBase):
    def setup_method(self) -> None:
        return super().setup_method()

    def test_preview_for_documents_for_file(self):
        file_uuid = uuid4()
        new_filestore_id = random.randint(1, 1_000_000)

        mock_result = mock.Mock(name="result")
        mock_result.fetchone.side_effect = [(new_filestore_id,)]
        preview_rows = [
            # The documents found for the file_uuid
            [_make_document_row(id="simple_preview", store_uuid=file_uuid)],
            # "Inserted primary key" of the new filestore row
            mock_result,
            # INSERT of file_derivative
            None,
            # UPDATE of filestore row with upload data
            None,
        ]
        document_uuid = preview_rows[0][0].document_uuid

        self.load_database_rows(preview_rows)

        s3_upload_result = {
            "md5": "md5",
            "size": random.randint(1, 1_000_000),
            "storage_location": "cloud",
        }

        self.mock_s3.upload.side_effect = [s3_upload_result]

        self.cmd.create_preview_for_documents_for_file(file_uuid=file_uuid)

        self.mock_s3.download_file.assert_called_once_with(
            destination=mock.ANY,
            file_uuid=str(file_uuid),
            storage_location="the moon",
        )
        self.mock_converter.convert_file_handle.assert_called_once_with(
            to_type="application/pdf",
            options={},
            input_handle=mock.ANY,
            output_handle=mock.ANY,
        )
        self.mock_s3.upload.assert_called_once_with(
            uuid=mock.ANY,
            file_handle=mock.ANY,
        )
        assert isinstance(self.mock_s3.upload.call_args[1]["uuid"], UUID)

        assert len(self.executed_queries) == 4

        select_documents_query = self.executed_queries[0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(select_documents_query)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, file.lock_subject_id AS lock_user_uuid, file.lock_subject_name AS lock_user_display_name, file.lock_timestamp AS lock_timestamp, file.shared AS lock_shared, file.publish_pip AS publish_pip, file.publish_website AS publish_website, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file.document_status, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, file_metadata.document_source, file_metadata.pronom_format, file_metadata.appearance, file_metadata.structure, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels, CASE WHEN (file.search_index IS NULL) THEN %(param_7)s ELSE %(param_8)s END AS has_search_index \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.destroyed IS false AND filestore.uuid = %(uuid_1)s::UUID"
        )
        assert select_documents_query.params == {
            "json_build_object_2": "uuid",
            "json_build_object_3": "name",
            "json_build_object_4": "public_name",
            "json_build_object_5": "magic_string",
            "mimetype_1": "application/pdf",
            "param_1": 1,
            "param_2": 1,
            "param_3": 1,
            "param_4": 1,
            "param_5": 1,
            "param_6": 1,
            "param_7": False,
            "param_8": True,
            "type_1": "pdf",
            "type_2": "thumbnail",
            "uuid_1": file_uuid,
        }

        insert_filestore_query = self.executed_queries[1].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_filestore_query)
            == "INSERT INTO filestore (uuid, original_name, size, mimetype, md5, storage_location, is_archivable, virus_scan_status) VALUES (%(uuid)s::UUID, %(original_name)s, %(size)s, %(mimetype)s, %(md5)s, %(storage_location)s::VARCHAR[], %(is_archivable)s, %(virus_scan_status)s) RETURNING filestore.id"
        )
        assert insert_filestore_query.params == {
            "is_archivable": True,
            "md5": "",
            "mimetype": "application/pdf",
            "original_name": f"preview_{document_uuid}.pdf",
            "size": 0,
            "storage_location": [],
            "uuid": mock.ANY,
            "virus_scan_status": "ok",
        }
        insert_derivative_query = self.executed_queries[2].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_derivative_query)
            == "INSERT INTO file_derivative (file_id, filestore_id, max_width, max_height, type) VALUES ((SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true), %(filestore_id)s, %(max_width)s, %(max_height)s, %(type)s) RETURNING file_derivative.id"
        )
        assert insert_derivative_query.params == {
            "uuid_1": document_uuid,
            "filestore_id": new_filestore_id,
            "max_height": 0,
            "max_width": 0,
            "type": "pdf",
        }

        update_filestore_query = self.executed_queries[3].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(update_filestore_query)
            == "UPDATE filestore SET size=%(size)s, md5=%(md5)s, storage_location=%(storage_location)s::VARCHAR[] WHERE filestore.id = %(id_1)s"
        )
        assert update_filestore_query.params == {
            "size": s3_upload_result["size"],
            "md5": s3_upload_result["md5"],
            "storage_location": [s3_upload_result["storage_location"]],
            "id_1": new_filestore_id,
        }

    def test_preview_for_documents_for_file_ratelimited(self):
        self.mock_redis.get.return_value = 100

        file_uuid = uuid4()
        self.cmd.create_preview_for_documents_for_file(file_uuid=file_uuid)

        assert len(self.executed_queries) == 0


class TestCreatePreviewForDocument(PreviewBase):
    def setup_method(self) -> None:
        super().setup_method()
        self.cmd.user_info.permissions = {"gebruiker": True}
        return

    def test_create_preview_for_document(self):
        document_uuid = uuid4()
        file_uuid = uuid4()
        new_filestore_id = random.randint(1, 1_000_000)

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="simple_preview",
                document_uuid=document_uuid,
                store_uuid=file_uuid,
            )
        ]
        mock_fetchone = mock.Mock()
        mock_fetchone.fetchone.side_effect = [(new_filestore_id,)]

        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
            # "Inserted primary key" of the new filestore row
            mock_fetchone,
            # INSERT of file_derivative
            None,
            # UPDATE of filestore row with upload data
            None,
        ]

        self.load_database_rows(preview_rows)

        s3_upload_result = {
            "md5": "md5",
            "size": random.randint(1, 1_000_000),
            "storage_location": "cloud",
        }

        self.mock_s3.upload.side_effect = [s3_upload_result]

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        self.mock_s3.download_file.assert_called_once_with(
            destination=mock.ANY,
            file_uuid=str(file_uuid),
            storage_location="the moon",
        )
        self.mock_converter.convert_file_handle.assert_called_once_with(
            to_type="application/pdf",
            options={},
            input_handle=mock.ANY,
            output_handle=mock.ANY,
        )
        self.mock_s3.upload.assert_called_once_with(
            uuid=mock.ANY,
            file_handle=mock.ANY,
        )
        assert isinstance(self.mock_s3.upload.call_args[1]["uuid"], UUID)

        assert len(self.executed_queries) == 4

        select_documents_query = self.executed_queries[0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(select_documents_query)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, file.lock_subject_id AS lock_user_uuid, file.lock_subject_name AS lock_user_display_name, file.lock_timestamp AS lock_timestamp, file.shared AS lock_shared, file.publish_pip AS publish_pip, file.publish_website AS publish_website, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file.document_status, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, file_metadata.document_source, file_metadata.pronom_format, file_metadata.appearance, file_metadata.structure, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels, CASE WHEN (file.search_index IS NULL) THEN %(param_7)s ELSE %(param_8)s END AS has_search_index \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.destroyed IS false AND file.uuid = %(uuid_1)s::UUID"
        )
        assert select_documents_query.params == {
            "json_build_object_2": "uuid",
            "json_build_object_3": "name",
            "json_build_object_4": "public_name",
            "json_build_object_5": "magic_string",
            "mimetype_1": "application/pdf",
            "param_1": 1,
            "param_2": 1,
            "param_3": 1,
            "param_4": 1,
            "param_5": 1,
            "param_6": 1,
            "param_7": False,
            "param_8": True,
            "type_1": "pdf",
            "type_2": "thumbnail",
            "uuid_1": document_uuid,
        }

        insert_filestore_query = self.executed_queries[1].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_filestore_query)
            == "INSERT INTO filestore (uuid, original_name, size, mimetype, md5, storage_location, is_archivable, virus_scan_status) VALUES (%(uuid)s::UUID, %(original_name)s, %(size)s, %(mimetype)s, %(md5)s, %(storage_location)s::VARCHAR[], %(is_archivable)s, %(virus_scan_status)s) RETURNING filestore.id"
        )
        assert insert_filestore_query.params == {
            "is_archivable": True,
            "md5": "",
            "mimetype": "application/pdf",
            "original_name": f"preview_{document_uuid}.pdf",
            "size": 0,
            "storage_location": [],
            "uuid": mock.ANY,
            "virus_scan_status": "ok",
        }
        insert_derivative_query = self.executed_queries[2].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_derivative_query)
            == "INSERT INTO file_derivative (file_id, filestore_id, max_width, max_height, type) VALUES ((SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true), %(filestore_id)s, %(max_width)s, %(max_height)s, %(type)s) RETURNING file_derivative.id"
        )
        assert insert_derivative_query.params == {
            "uuid_1": document_uuid,
            "filestore_id": new_filestore_id,
            "max_height": 0,
            "max_width": 0,
            "type": "pdf",
        }

        update_filestore_query = self.executed_queries[3].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(update_filestore_query)
            == "UPDATE filestore SET size=%(size)s, md5=%(md5)s, storage_location=%(storage_location)s::VARCHAR[] WHERE filestore.id = %(id_1)s"
        )
        assert update_filestore_query.params == {
            "size": s3_upload_result["size"],
            "md5": s3_upload_result["md5"],
            "storage_location": [s3_upload_result["storage_location"]],
            "id_1": new_filestore_id,
        }

    def test_create_preview_for_document_capitalized_extension(self):
        document_uuid = uuid4()
        file_uuid = uuid4()
        new_filestore_id = random.randint(1, 1_000_000)

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="simple_preview",
                extension=".TXT",
                document_uuid=document_uuid,
                store_uuid=file_uuid,
            )
        ]
        mock_fetchone = mock.Mock()
        mock_fetchone.fetchone.side_effect = [(new_filestore_id,)]

        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
            # "Inserted primary key" of the new filestore row
            mock_fetchone,
            # INSERT of file_derivative
            None,
            # UPDATE of filestore row with upload data
            None,
        ]

        self.load_database_rows(preview_rows)

        s3_upload_result = {
            "md5": "md5",
            "size": random.randint(1, 1_000_000),
            "storage_location": "cloud",
        }

        self.mock_s3.upload.side_effect = [s3_upload_result]

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        self.mock_s3.download_file.assert_called_once_with(
            destination=mock.ANY,
            file_uuid=str(file_uuid),
            storage_location="the moon",
        )
        self.mock_converter.convert_file_handle.assert_called_once_with(
            to_type="application/pdf",
            options={},
            input_handle=mock.ANY,
            output_handle=mock.ANY,
        )
        self.mock_s3.upload.assert_called_once_with(
            uuid=mock.ANY,
            file_handle=mock.ANY,
        )
        assert isinstance(self.mock_s3.upload.call_args[1]["uuid"], UUID)

        assert len(self.executed_queries) == 4

        select_documents_query = self.executed_queries[0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(select_documents_query)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, file.lock_subject_id AS lock_user_uuid, file.lock_subject_name AS lock_user_display_name, file.lock_timestamp AS lock_timestamp, file.shared AS lock_shared, file.publish_pip AS publish_pip, file.publish_website AS publish_website, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file.document_status, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, file_metadata.document_source, file_metadata.pronom_format, file_metadata.appearance, file_metadata.structure, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels, CASE WHEN (file.search_index IS NULL) THEN %(param_7)s ELSE %(param_8)s END AS has_search_index \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.destroyed IS false AND file.uuid = %(uuid_1)s::UUID"
        )
        assert select_documents_query.params == {
            "json_build_object_2": "uuid",
            "json_build_object_3": "name",
            "json_build_object_4": "public_name",
            "json_build_object_5": "magic_string",
            "mimetype_1": "application/pdf",
            "param_1": 1,
            "param_2": 1,
            "param_3": 1,
            "param_4": 1,
            "param_5": 1,
            "param_6": 1,
            "param_7": False,
            "param_8": True,
            "type_1": "pdf",
            "type_2": "thumbnail",
            "uuid_1": document_uuid,
        }

        insert_filestore_query = self.executed_queries[1].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_filestore_query)
            == "INSERT INTO filestore (uuid, original_name, size, mimetype, md5, storage_location, is_archivable, virus_scan_status) VALUES (%(uuid)s::UUID, %(original_name)s, %(size)s, %(mimetype)s, %(md5)s, %(storage_location)s::VARCHAR[], %(is_archivable)s, %(virus_scan_status)s) RETURNING filestore.id"
        )
        assert insert_filestore_query.params == {
            "is_archivable": True,
            "md5": "",
            "mimetype": "application/pdf",
            "original_name": f"preview_{document_uuid}.pdf",
            "size": 0,
            "storage_location": [],
            "uuid": mock.ANY,
            "virus_scan_status": "ok",
        }
        insert_derivative_query = self.executed_queries[2].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_derivative_query)
            == "INSERT INTO file_derivative (file_id, filestore_id, max_width, max_height, type) VALUES ((SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true), %(filestore_id)s, %(max_width)s, %(max_height)s, %(type)s) RETURNING file_derivative.id"
        )
        assert insert_derivative_query.params == {
            "uuid_1": document_uuid,
            "filestore_id": new_filestore_id,
            "max_height": 0,
            "max_width": 0,
            "type": "pdf",
        }

        update_filestore_query = self.executed_queries[3].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(update_filestore_query)
            == "UPDATE filestore SET size=%(size)s, md5=%(md5)s, storage_location=%(storage_location)s::VARCHAR[] WHERE filestore.id = %(id_1)s"
        )
        assert update_filestore_query.params == {
            "size": s3_upload_result["size"],
            "md5": s3_upload_result["md5"],
            "storage_location": [s3_upload_result["storage_location"]],
            "id_1": new_filestore_id,
        }

    def test_create_preview_for_document_already_has_preview(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="has_preview",
                document_uuid=document_uuid,
                has_preview=True,
            )
        ]
        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
        ]

        self.load_database_rows(preview_rows)

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()

    def test_create_preview_for_document_ratelimited(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()
        self.mock_redis.reset_mock()

        self.mock_redis.get.return_value = 100

        document_uuid = uuid4()

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 0
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()

    def test_create_preview_for_document_not_previewable(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="simple_preview",
                document_uuid=document_uuid,
                mimetype="application/octet-stream",
            )
        ]
        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
        ]

        self.load_database_rows(preview_rows)

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()

    def test_create_preview_for_document_undefined_size(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="undefined_size", document_uuid=document_uuid, size=None
            )
        ]
        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
        ]

        self.load_database_rows(preview_rows)

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()

    def test_create_preview_for_document_too_big_to_preview(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="too_big",
                document_uuid=document_uuid,
                size=MAX_SIZE_FOR_PREVIEW + 1,
            )
        ]
        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
        ]

        self.load_database_rows(preview_rows)

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()

    def test_create_preview_for_document_virus_scan_not_ok(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="virusscan_pending",
                document_uuid=document_uuid,
                virus_scan_status="pending",
            )
        ]
        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
        ]

        self.load_database_rows(preview_rows)

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()

    def test_create_preview_for_document_database_conflict(self):
        self.mock_s3.reset_mock()
        self.mock_converter.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="db_conflict",
                document_uuid=document_uuid,
            )
        ]

        new_filestore_id = random.randint(1, 1_000_000)
        mock_fetchone = mock.Mock()
        mock_fetchone.fetchone.side_effect = [(new_filestore_id,)]

        preview_rows = [
            # The documents found for the file_uuid
            mock_result,
            # "Inserted primary key" of the new filestore row
            mock_fetchone,
            # INSERT of file_derivative
            sql_exc.IntegrityError("test", {}, "test"),
        ]

        self.load_database_rows(preview_rows)

        self.cmd.create_preview_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 3
        self.mock_converter.assert_not_called()
        self.mock_s3.assert_not_called()
