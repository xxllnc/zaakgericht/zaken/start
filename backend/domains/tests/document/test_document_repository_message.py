# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from dataclasses import dataclass
from datetime import datetime, timezone
from minty.cqrs import EventService
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.document.entities import Message, MessageExternal
from zsnl_domains.document.repositories.message import MessageRepository


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


@dataclass
class ContactRow:
    """
    Dataclass used to represent the SQL result of contacts
    """

    type: str


@dataclass
class MessageRow:
    """
    Dataclass used to represent the SQL result of messages
    """

    uuid: UUID
    type: str
    case_uuid: UUID
    created_by_displayname: str
    created: datetime
    last_modified: datetime
    message_date: datetime
    created_by_uuid: UUID
    attachment_count: int
    external_message_id: int
    external_message_subject: str
    external_message_type: str
    participants: list
    external_message_content: str
    external_message_source_file_uuid: UUID
    case_id: int
    attachments: list


class TestMessageRepository(TestBase):
    def setup_method(self):
        self.valid_user_uuid = str(uuid4())

        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "converter": mock.MagicMock(),
            "s3": mock.MagicMock(),
        }

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )
        self.mock_infra_ro = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        self.message_uuid = uuid4()
        self.message_type = "external"
        self.contact_uuid = uuid4()
        self.case_uuid = uuid4()
        self.now = datetime.now(timezone.utc)
        self.contact_uuid = uuid4()
        self.contact_id = 1
        self.contact_type = "employee"
        self.contact_name = "Ad Min"
        self.contact_row = ContactRow(type=self.contact_type)
        self.message_row = MessageRow(
            message_date=self.now,
            created=self.now,
            external_message_subject="subject",
            type=self.message_type,
            participants=[
                {
                    "role": "from",
                    "address": "from@address.com",
                    "display_name": "FromDisplayName",
                },
                {
                    "role": "to",
                    "address": "to@address.com",
                    "display_name": "ToDisplayName",
                },
            ],
            external_message_source_file_uuid=None,
            external_message_content="content",
            external_message_type="email",
            external_message_id=1,
            attachments=[{"filename": "testfile.txt"}],
            uuid=self.message_uuid,
            case_uuid=self.case_uuid,
            created_by_displayname="Ad Min",
            created_by_uuid=self.contact_uuid,
            last_modified=self.now,
            case_id=1,
            attachment_count=1,
        )

        self.message_repo = MessageRepository(
            infrastructure_factory=self.mock_infra,
            infrastructure_factory_ro=self.mock_infra_ro,
            context=None,
            event_service=EventService(
                correlation_id=uuid4(),
                domain="domain",
                context="context",
                user_uuid=self.valid_user_uuid,
            ),
            read_only=False,
        )

    def test_get_message_by_uuid(self):
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
        ]
        res = self.message_repo.get_message_by_uuid(
            message_uuid=self.message_uuid
        )

        assert res.uuid == self.message_uuid
        assert res.case_uuid == self.case_uuid

    def test_get_message_by_uuid_no_message_date(self):
        self.message_row.message_date = None

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
        ]

        res = self.message_repo.get_message_by_uuid(
            message_uuid=self.message_uuid
        )

        assert res.uuid == self.message_uuid
        assert res.case_uuid == self.case_uuid

    def test_get_source_file_by_message_uuid_not_found(self):
        message_uuid = uuid4()

        with pytest.raises(NotFound):
            self.message_repo.session.execute().fetchone.return_value = None
            self.message_repo.get_message_by_uuid(message_uuid=message_uuid)

    def test_only_valid_message_types_can_be_converted_to_pdf(self):
        now = datetime.now(timezone.utc)

        external_params = {
            "id": 1,
            "case_id": 1,
            "message_date": now,
            "created": now,
            "subject": "subject",
            "participants": [],
            "type": "invalid_type",
            "content": "content",
            "attachments": None,
            "source_file_uuid": None,
        }

        message_external = MessageExternal(**external_params)

        params = {
            "uuid": uuid4(),
            "case_uuid": uuid4(),
            "created_by_displayname": "Admin",
            "created": now,
            "last_modified": now,
            "message_date": now,
            "message_type": "external",
            "message_external": message_external,
        }

        message = Message(**params)

        with pytest.raises(Conflict):
            self.message_repo._create_pdf_from_message(message)
