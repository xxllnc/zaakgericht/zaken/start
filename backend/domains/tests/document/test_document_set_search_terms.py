# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty.exceptions
import pytest
import random
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import document
from zsnl_domains.document.entities.document import MAX_SIZE_FOR_SEARCH_TERMS


def _make_document_row(
    id: str,
    mimetype="image/png",
    extension=".png",
    size: int | None = 100,
    virus_scan_status="ok",
    has_preview=None,
    has_thumbnail=False,
    document_uuid=None,
    store_uuid=None,
    has_search_index=False,
):
    document_row = mock.Mock(name=f"document_row_{id}")
    document_row.configure_mock(
        document_uuid=document_uuid if document_uuid else uuid4(),
        store_uuid=store_uuid if store_uuid else uuid4(),
        mimetype=mimetype,
        size=size,
        storage_location=["the moon"],
        md5="",
        is_archivable=True,
        virus_scan_status=virus_scan_status,
        directory_uuid=None,
        case_uuid=uuid4(),
        case_display_number=random.randint(1, 1_000_000),
        filename="example",
        extension=extension,
        accepted=True,
        version=random.randint(1, 100),
        intake_group_uuid=None,
        intake_role_uuid=None,
        intake_owner_uuid=None,
        creator_uuid=uuid4(),
        creator_displayname="Piet Friet",
        date_modified="",
        id=random.randint(1, 1_000_000),
        preview_uuid=has_preview if has_preview else None,
        preview_storage_location=["here"] if has_preview else None,
        preview_mimetype="application/pdf" if has_preview else None,
        description="Some text",
        origin=None,
        origin_date=None,
        confidentiality="Openbaar",
        thumbnail_uuid=uuid4() if has_thumbnail else None,
        thumbnail_storage_location=["there"] if has_thumbnail else None,
        thumbnail_mimetype="image/png" if has_thumbnail else None,
        labels=[],
        has_search_index=has_search_index,
        document_source="test_source",
        document_status="original",
        pronom_format="pronom",
        appearance="appearance",
        structure="structure",
    )

    return document_row


class SetSearchTermsBase(TestBase):
    def setup_method(self) -> None:
        # This sets `self.session`
        self.mock_s3 = mock.Mock()
        self.mock_tika = mock.Mock()
        self.mock_redis = mock.Mock()

        self.mock_redis.get.return_value = None

        mock_infra = {
            "s3": self.mock_s3,
            "tika": self.mock_tika,
            "redis": self.mock_redis,
        }

        self.load_command_instance(document, inframocks=mock_infra)

        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
                "rate_limits": {
                    "command:set_search_index": 5,
                },
            }
        )

        self.executed_queries = []
        self.mock_rows = []

        def mock_execute(query):
            self.executed_queries.append(query)

            try:
                rv = self.mock_rows.pop(0)
                if isinstance(rv, Exception):
                    raise rv
            except IndexError as e:
                raise IndexError(query) from e

            if isinstance(rv, Exception):
                raise rv

            return rv

        self.session.execute = mock_execute

    def load_database_rows(self, mock_rows: list):
        self.mock_rows = mock_rows
        self.executed_queries = []


class TestSetSearchTermsForDocumentsForFile(SetSearchTermsBase):
    def setup_method(self) -> None:
        return super().setup_method()

    def test_search_terms_for_documents_for_file(self):
        file_uuid = uuid4()

        mock_document = _make_document_row(
            id="simple_preview_direct",
            store_uuid=file_uuid,
        )

        set_search_terms_rows = [
            # The documents found for the file_uuid
            [mock_document],
            # "SELECT FOR UPDATE" to lock the `file` row
            None,
            # UPDATE of `file` with the search terms
            None,
        ]
        document_uuid = set_search_terms_rows[0][0].document_uuid

        self.load_database_rows(set_search_terms_rows)

        self.mock_tika.get_text.return_value = "De documenttekst komt hier."

        self.cmd.set_search_terms_for_documents_for_file(file_uuid=file_uuid)

        self.mock_s3.download_file.assert_has_calls(
            [
                mock.call(
                    destination=mock.ANY,
                    file_uuid=str(file_uuid),
                    storage_location="the moon",
                ),
            ]
        )

        # 3 queries:
        # - SELECT
        # - SELECT FOR UPDATE
        # - UPDATE
        assert len(self.executed_queries) == 2

        select_documents_query = self.executed_queries[0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(select_documents_query)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, file.lock_subject_id AS lock_user_uuid, file.lock_subject_name AS lock_user_display_name, file.lock_timestamp AS lock_timestamp, file.shared AS lock_shared, file.publish_pip AS publish_pip, file.publish_website AS publish_website, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file.document_status, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, file_metadata.document_source, file_metadata.pronom_format, file_metadata.appearance, file_metadata.structure, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels, CASE WHEN (file.search_index IS NULL) THEN %(param_7)s ELSE %(param_8)s END AS has_search_index \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.destroyed IS false AND filestore.uuid = %(uuid_1)s::UUID"
        )
        assert select_documents_query.params == {
            "json_build_object_2": "uuid",
            "json_build_object_3": "name",
            "json_build_object_4": "public_name",
            "json_build_object_5": "magic_string",
            "mimetype_1": "application/pdf",
            "param_1": 1,
            "param_2": 1,
            "param_3": 1,
            "param_4": 1,
            "param_5": 1,
            "param_6": 1,
            "param_7": False,
            "param_8": True,
            "type_1": "pdf",
            "type_2": "thumbnail",
            "uuid_1": file_uuid,
        }

        update_file_query = self.executed_queries[1].compile(
            dialect=postgresql.dialect()
        )
        assert str(update_file_query) == (
            "UPDATE file SET search_term=concat_ws(%(concat_ws_1)s, file.name, (SELECT file_metadata.description \n"
            "FROM file_metadata \n"
            "WHERE file.metadata_id = file_metadata.id)), search_index=to_tsvector(%(to_tsvector_1)s, %(to_tsvector_2)s) WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true"
        )
        assert update_file_query.params == {
            "concat_ws_1": " ",
            "to_tsvector_1": "dutch",
            "to_tsvector_2": "De documenttekst komt hier.",
            "uuid_1": document_uuid,
        }


class TestSetSearchTermsForDocument(SetSearchTermsBase):
    def setup_method(self) -> None:
        return super().setup_method()

    def test_set_search_terms_for_document(self):
        document_uuid = uuid4()
        file_uuid = uuid4()

        mock_document_row = _make_document_row(
            id="simple_search_terms_direct",
            document_uuid=document_uuid,
            store_uuid=file_uuid,
        )

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [mock_document_row]

        new_filestore_id = random.randint(1, 1_000_000)
        mock_fetchone = mock.Mock()
        mock_fetchone.fetchone.side_effect = [
            (new_filestore_id,),
        ]

        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # "SELECT FOR UPDATE" to lock the `file` row
            None,
            # UPDATE of `file` with the search terms
            None,
        ]

        self.load_database_rows(set_search_terms_rows)
        self.mock_tika.get_text.return_value = "De documenttekst komt hier"

        self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 2

        self.mock_s3.download_file.assert_called_once_with(
            destination=mock.ANY,
            file_uuid=str(file_uuid),
            storage_location="the moon",
        )

        select_documents_query = self.executed_queries[0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(select_documents_query)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, file.lock_subject_id AS lock_user_uuid, file.lock_subject_name AS lock_user_display_name, file.lock_timestamp AS lock_timestamp, file.shared AS lock_shared, file.publish_pip AS publish_pip, file.publish_website AS publish_website, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file.document_status, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, file_metadata.document_source, file_metadata.pronom_format, file_metadata.appearance, file_metadata.structure, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels, CASE WHEN (file.search_index IS NULL) THEN %(param_7)s ELSE %(param_8)s END AS has_search_index \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.destroyed IS false AND file.uuid = %(uuid_1)s::UUID"
        )
        assert select_documents_query.params == {
            "json_build_object_2": "uuid",
            "json_build_object_3": "name",
            "json_build_object_4": "public_name",
            "json_build_object_5": "magic_string",
            "mimetype_1": "application/pdf",
            "param_1": 1,
            "param_2": 1,
            "param_3": 1,
            "param_4": 1,
            "param_5": 1,
            "param_6": 1,
            "param_7": False,
            "param_8": True,
            "type_1": "pdf",
            "type_2": "thumbnail",
            "uuid_1": document_uuid,
        }

        update_file_query = self.executed_queries[1].compile(
            dialect=postgresql.dialect()
        )
        assert str(update_file_query) == (
            "UPDATE file SET search_term=concat_ws(%(concat_ws_1)s, file.name, (SELECT file_metadata.description \n"
            "FROM file_metadata \n"
            "WHERE file.metadata_id = file_metadata.id)), search_index=to_tsvector(%(to_tsvector_1)s, %(to_tsvector_2)s) WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true"
        )
        assert update_file_query.params == {
            "concat_ws_1": " ",
            "to_tsvector_1": "dutch",
            "to_tsvector_2": "De documenttekst komt hier",
            "uuid_1": document_uuid,
        }

    def test_set_search_terms_for_document_already_has_search_index(self):
        self.mock_s3.reset_mock()
        self.mock_tika.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="has_search_index",
                document_uuid=document_uuid,
                has_search_index=True,
            )
        ]
        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # SELECT .. FOR UPDATE
            None,
        ]

        self.load_database_rows(set_search_terms_rows)

        self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_s3.assert_not_called()
        self.mock_tika.assert_not_called()

    def test_set_search_terms_for_document_undefined_size(self):
        self.mock_s3.reset_mock()
        self.mock_tika.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="undefined_size", document_uuid=document_uuid, size=None
            )
        ]
        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # SELECT .. FOR UPDATE
            None,
        ]

        self.load_database_rows(set_search_terms_rows)

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_s3.assert_not_called()
        self.mock_tika.assert_not_called()

    def test_set_search_terms_for_document_too_big(self):
        self.mock_s3.reset_mock()
        self.mock_tika.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="too_big",
                document_uuid=document_uuid,
                size=MAX_SIZE_FOR_SEARCH_TERMS + 1,
            )
        ]
        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # SELECT .. FOR UPDATE
            None,
        ]

        self.load_database_rows(set_search_terms_rows)

        self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_s3.assert_not_called()
        self.mock_tika.assert_not_called()

    def test_set_search_terms_for_document_virus_scan_not_ok(self):
        self.mock_s3.reset_mock()
        self.mock_tika.reset_mock()

        document_uuid = uuid4()

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="virus_pending",
                document_uuid=document_uuid,
                virus_scan_status="pending",
            )
        ]
        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # SELECT .. FOR UPDATE
            None,
        ]

        self.load_database_rows(set_search_terms_rows)

        self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_s3.assert_not_called()
        self.mock_tika.assert_not_called()

    def test_set_search_terms_for_document_rate_limited(self):
        self.mock_s3.reset_mock()
        self.mock_tika.reset_mock()

        document_uuid = uuid4()
        file_uuid = uuid4()

        self.mock_redis.get.return_value = 100

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [
            _make_document_row(
                id="simple_search_terms_direct",
                document_uuid=document_uuid,
                store_uuid=file_uuid,
            )
        ]
        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # SELECT .. FOR UPDATE
            None,
        ]

        self.load_database_rows(set_search_terms_rows)

        self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert len(self.executed_queries) == 1
        self.mock_s3.assert_not_called()
        self.mock_tika.assert_not_called()

    def test_set_search_terms_for_document_delayed(self):
        document_uuid = uuid4()
        file_uuid = uuid4()

        mock_document_row = _make_document_row(
            id="simple_search_terms_direct",
            document_uuid=document_uuid,
            store_uuid=file_uuid,
        )

        mock_result = mock.Mock()
        mock_result.fetchone.side_effect = [mock_document_row]

        new_filestore_id = random.randint(1, 1_000_000)
        mock_fetchone = mock.Mock()
        mock_fetchone.fetchone.side_effect = [
            (new_filestore_id,),
        ]

        set_search_terms_rows = [
            # The documents found for the file_uuid
            mock_result,
            # "SELECT FOR UPDATE" to lock the `file` row
            None,
            # UPDATE of `file` with the search terms
            None,
        ]

        self.load_database_rows(set_search_terms_rows)
        self.mock_tika.get_text.return_value = "De documenttekst komt hier"

        self.cmd.set_search_terms_for_document_delayed(
            document_uuid=document_uuid
        )

        assert len(self.executed_queries) == 2

        self.mock_s3.download_file.assert_called_once_with(
            destination=mock.ANY,
            file_uuid=str(file_uuid),
            storage_location="the moon",
        )

        select_documents_query = self.executed_queries[0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(select_documents_query)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, file.lock_subject_id AS lock_user_uuid, file.lock_subject_name AS lock_user_display_name, file.lock_timestamp AS lock_timestamp, file.shared AS lock_shared, file.publish_pip AS publish_pip, file.publish_website AS publish_website, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file.document_status, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, file_metadata.document_source, file_metadata.pronom_format, file_metadata.appearance, file_metadata.structure, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels, CASE WHEN (file.search_index IS NULL) THEN %(param_7)s ELSE %(param_8)s END AS has_search_index \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.destroyed IS false AND file.uuid = %(uuid_1)s::UUID"
        )
        assert select_documents_query.params == {
            "json_build_object_2": "uuid",
            "json_build_object_3": "name",
            "json_build_object_4": "public_name",
            "json_build_object_5": "magic_string",
            "mimetype_1": "application/pdf",
            "param_1": 1,
            "param_2": 1,
            "param_3": 1,
            "param_4": 1,
            "param_5": 1,
            "param_6": 1,
            "param_7": False,
            "param_8": True,
            "type_1": "pdf",
            "type_2": "thumbnail",
            "uuid_1": document_uuid,
        }

        update_file_query = self.executed_queries[1].compile(
            dialect=postgresql.dialect()
        )
        assert str(update_file_query) == (
            "UPDATE file SET search_term=concat_ws(%(concat_ws_1)s, file.name, (SELECT file_metadata.description \n"
            "FROM file_metadata \n"
            "WHERE file.metadata_id = file_metadata.id)), search_index=to_tsvector(%(to_tsvector_1)s, %(to_tsvector_2)s) WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true"
        )
        assert update_file_query.params == {
            "concat_ws_1": " ",
            "to_tsvector_1": "dutch",
            "to_tsvector_2": "De documenttekst komt hier",
            "uuid_1": document_uuid,
        }

    def test_set_search_terms_for_document_delayed_no_user_info(self, caplog):
        document_uuid = uuid4()
        self.cmd.user_info = None
        caplog.set_level(logging.DEBUG)

        self.cmd.set_search_terms_for_document_delayed(
            document_uuid=document_uuid
        )

        assert "No user info, cannot set search terms" in caplog.text
        # No userinfo (should never happen in real life)

    def test_set_search_terms_for_document_no_user_info(self, caplog):
        document_uuid = uuid4()
        self.cmd.user_info = None
        caplog.set_level(logging.DEBUG)

        self.cmd.set_search_terms_for_document(document_uuid=document_uuid)

        assert "No user info, cannot set search terms" in caplog.text
        # No userinfo (should never happen in real life)

    def test_set_search_terms_for_documents_for_file_no_user_info(
        self, caplog
    ):
        file_uuid = uuid4()
        self.cmd.user_info = None
        caplog.set_level(logging.DEBUG)

        self.cmd.set_search_terms_for_documents_for_file(file_uuid=file_uuid)

        assert "No user info, cannot set search terms" in caplog.text
        # No userinfo (should never happen in real life)
