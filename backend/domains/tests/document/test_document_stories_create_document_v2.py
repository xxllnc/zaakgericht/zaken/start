# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import hashlib
import minty.exceptions
import pytest
import random
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from tempfile import NamedTemporaryFile
from typing import IO
from typing_extensions import TypedDict
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import document


class MockS3:
    class S3Upload(TypedDict):
        content: bytes
        uuid: UUID

    uploads: list[S3Upload]
    mime_types: list[str]

    def __init__(self, mime_types: list[str]):
        self.uploads = []
        self.mime_types = mime_types

    def upload(self, file_handle: IO[bytes], uuid: UUID):
        content = file_handle.read()
        self.uploads.append({"uuid": uuid, "content": content})

        hash = hashlib.md5(content, usedforsecurity=False).hexdigest

        return {
            "mime_type": self.mime_types.pop(),
            "size": len(content),
            "storage_location": "attic",
            "md5": hash,
        }


class TestUploadRegistrationForm(TestBase):
    def setup_method(self):
        self.mock_s3 = MockS3(mime_types=["application/pdf"])
        self.load_command_instance(document, inframocks={"s3": self.mock_s3})

        # remove standard admin role for mock command instance
        self.cmd.user_info.permissions.pop("admin")
        # insert system role, because normally this is done in the
        # create_document_with_token call
        self.cmd.user_info.permissions["system"] = True

    def test_upload_registration_form(self):
        case_uuid = uuid4()
        num_unaccepted_files = random.randint(1, 1000)
        person_id = random.randint(1, 1000)

        mock_case_id_assignee_uuid = mock.Mock(behandelaar_gm_id=None)
        mock_filestore_row = minty.exceptions.NotFound("Not found")
        mock_user_result = mock.Mock()
        mock_user_result.configure_mock(
            subject_type="natuurlijk_persoon", id=person_id
        )
        mock_document_label_row = mock.Mock()
        mock_document_label_row.configure_mock(
            uuid=uuid4(),
            name="PDF Formulier",
            public_name="PDF Formulier",
            magic_string="pdf_webformulier",
            attribute_id=uuid4(),
        )

        self.session.execute().scalar_one.side_effect = [
            # Twice, once for "create document" and once for "accept document"
            num_unaccepted_files,
            1,  # number of identical files (same name, current version etc.)
            num_unaccepted_files,
        ]
        self.session.execute().one.side_effect = [
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case_id_assignee_uuid,
            mock_filestore_row,
            mock_case_id_assignee_uuid,
            mock_user_result,  # get legacy contact identifier
            None,  # get_file_metadata
            None,  # get_file_publishing_setting
            None,  # _apply_label_defaults (metadata)
        ]
        self.session.execute().fetchall.side_effect = [
            [mock_document_label_row],
            [mock.Mock(uuid=case_uuid)],  # get_all_case_uuids_for_label
        ]

        self.session.execute().inserted_primary_key = [random.randint(1, 1000)]
        self.session.reset_mock()

        with NamedTemporaryFile() as temp_file:
            temp_file.write(b"abcdefg")
            temp_file.flush()

            self.cmd.upload_registration_form(
                local_filename=temp_file.name,
                name="something-something.pdf",
                case_uuid=case_uuid,
                magic_string="its_a_kind_of_magic",
            )

        filestore_insert = self.session.execute.call_args_list[4][0][0]
        filestore_insert_compiled = filestore_insert.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(filestore_insert_compiled)
            == "INSERT INTO filestore (uuid, original_name, size, mimetype, md5, storage_location, is_archivable, virus_scan_status) VALUES (%(uuid)s::UUID, %(original_name)s, %(size)s, %(mimetype)s, %(md5)s, %(storage_location)s::VARCHAR[], %(is_archivable)s, %(virus_scan_status)s) RETURNING filestore.id"
        )

        file_insert = self.session.execute.call_args_list[10][0][0]
        file_insert_compiled = file_insert.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(file_insert_compiled)
            == "INSERT INTO file (filestore_id, uuid, root_file_id, name, extension, version, case_id, directory_id, creation_reason, accepted, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, destroyed, active_version, is_duplicate_of, queue, document_status, confidential, search_term, skip_intake) VALUES (%(filestore_id)s, %(uuid)s::UUID, %(root_file_id)s, %(name)s, %(extension)s, %(version)s, %(case_id)s, %(directory_id)s, %(creation_reason)s, %(accepted)s, %(reject_to_queue)s, %(is_duplicate_name)s, %(publish_pip)s, %(publish_website)s, %(date_created)s, %(created_by)s, %(date_modified)s, %(modified_by)s, %(destroyed)s, %(active_version)s, %(is_duplicate_of)s, %(queue)s, %(document_status)s, %(confidential)s, %(search_term)s, %(skip_intake)s) RETURNING file.id"
        )

        update_case_meta = self.session.execute.call_args_list[12][0][0]
        update_case_meta_compiled = update_case_meta.compile(
            dialect=postgresql.dialect()
        )
        assert update_case_meta_compiled.params == {
            "unaccepted_files_count": num_unaccepted_files,
            "uuid_1": str(case_uuid),
        }

        insert_label = self.session.execute.call_args_list[21][0][0]
        insert_label_compiled = insert_label.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_label_compiled) == (
            "INSERT INTO file_case_document (file_id, magic_string, bibliotheek_kenmerken_id, case_id) VALUES ((SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true), (SELECT zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_1)s::UUID), (SELECT zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_2)s::UUID), (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_2)s::UUID)) RETURNING file_case_document.id"
        )
        assert insert_label_compiled.params == {
            "case_document_uuid_1": str(mock_document_label_row.uuid),
            "case_document_uuid_2": str(mock_document_label_row.uuid),
            "uuid_1": mock.ANY,  # Generated document UUID
            "uuid_2": case_uuid,
        }

    def test_upload_registration_form_as_pip_user(self):
        case_uuid = uuid4()
        # inject pip_user role, strip out system
        self.cmd.user_info.permissions = {"pip_user": True}

        person_id = random.randint(1, 1000)
        mock_user_result = mock.Mock()
        mock_user_result.configure_mock(
            subject_type="natuurlijk_persoon", id=person_id
        )

        self.session.execute().fetchone.side_effect = [
            mock_user_result,  # get legacy contact identifier
            None,  # get_file_metadata
            None,  # get_file_publishing_setting
            None,  # _apply_label_defaults (metadata)
        ]

        with pytest.raises(minty.exceptions.NotFound):
            with NamedTemporaryFile() as temp_file:
                temp_file.write(b"abcdefg")
                temp_file.flush()

                self.cmd.upload_registration_form(
                    local_filename=temp_file.name,
                    name="something-something.pdf",
                    case_uuid=case_uuid,
                    magic_string="its_a_kind_of_magic",
                )
