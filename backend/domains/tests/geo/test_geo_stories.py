# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import json
import pytest
import zoneinfo
from copy import deepcopy
from minty.cqrs.test import TestBase
from minty.exceptions import NotFound, ValidationError
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import geo as geo_domain

GEO_JSON = {
    "type": "FeatureCollection",
    "features": [],
}


def make_geo_feature(identifier: str | None = None):
    properties = (
        {"properties": {"identifier": identifier}} if identifier else {}
    )

    return {
        "type": "Feature",
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [4.72412109375, 52.25470880113083],
                    [5.1251220703125, 52.25470880113083],
                    [5.1251220703125, 52.48612543090344],
                    [4.72412109375, 52.48612543090344],
                    [4.72412109375, 52.25470880113083],
                ]
            ],
        },
        **properties,
    }


def get_custom_object_geojson(object_uuid: UUID):
    return {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [4.72412109375, 52.25470880113083],
                            [5.1251220703125, 52.25470880113083],
                            [5.1251220703125, 52.48612543090344],
                            [4.72412109375, 52.48612543090344],
                            [4.72412109375, 52.25470880113083],
                        ]
                    ],
                },
                "properties": {
                    "zaaksysteem": {
                        "origin": {
                            "identifier": str(object_uuid),
                            "title": "Custom Object",
                            "subtitle": "Custom Object subtitle",
                            "status": "active",
                            "family": "object",
                            "type": "Custom Object Type",
                            "custom_field_magicstring": "alakazam",
                            "custom_field_label": "geo_location",
                        }
                    },
                },
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [4.72412109375, 52.25470880113083],
                            [5.1251220703125, 52.25470880113083],
                            [5.1251220703125, 52.48612543090344],
                            [4.72412109375, 52.48612543090344],
                            [4.72412109375, 52.25470880113083],
                        ]
                    ],
                },
                "properties": {
                    "identifier": str(object_uuid),
                    "zaaksysteem": {
                        "origin": {
                            "identifier": str(object_uuid),
                            "title": "Custom Object",
                            "subtitle": "Custom Object subtitle",
                            "status": "active",
                            "family": "object",
                            "type": "Custom Object Type",
                            "custom_field_magicstring": "hocus-pocus",
                            "custom_field_label": "geo_location",
                        }
                    },
                },
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [4.72412109375, 52.25470880113083],
                            [5.1251220703125, 52.25470880113083],
                            [5.1251220703125, 52.48612543090344],
                            [4.72412109375, 52.48612543090344],
                            [4.72412109375, 52.25470880113083],
                        ]
                    ],
                },
                "properties": {
                    "zaaksysteem": {
                        "origin": {
                            "identifier": str(object_uuid),
                            "title": "Custom Object",
                            "subtitle": "Custom Object subtitle",
                            "status": "active",
                            "family": "object",
                            "type": "Custom Object Type",
                            "custom_field_magicstring": "hocus-pocus2",
                            "custom_field_label": "geo_location",
                        }
                    },
                },
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [4.72412109375, 52.25470880113083],
                            [5.1251220703125, 52.25470880113083],
                            [5.1251220703125, 52.48612543090344],
                            [4.72412109375, 52.48612543090344],
                            [4.72412109375, 52.25470880113083],
                        ]
                    ],
                },
                "properties": {
                    "zaaksysteem": {
                        "origin": {
                            "identifier": str(object_uuid),
                            "title": "Custom Object",
                            "subtitle": "Custom Object subtitle",
                            "status": "active",
                            "family": "object",
                            "type": "Custom Object Type",
                            "custom_field_magicstring": "magic_address_v2",
                            "custom_field_label": "geo_location",
                            "location_bagid": 123456,
                            "location_description": "sample address",
                        }
                    },
                },
            },
        ],
    }


class TestAs_A_User_I_Want_To_Add_GeoJSON_Features(TestBase):
    def setup_method(self):
        self.load_command_instance(geo_domain)

    def test_create_geo_feature_success(self):
        geo_uuid = uuid4()

        self.cmd.create_geo_feature(
            uuid=str(geo_uuid),
            geojson={
                "type": "FeatureCollection",
                "features": [make_geo_feature("x")],
            },
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )
        self.assert_has_event_name("GeoFeatureCreated")

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        insert_call = call_list[0]
        select_statement = insert_call[0][0]

        assert isinstance(select_statement, sql.dml.Insert)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_select)
            == "INSERT INTO service_geojson (uuid, geo_json, last_modified) VALUES (%(uuid)s::UUID, %(geo_json)s::JSONB, %(last_modified)s) ON CONFLICT (uuid) DO UPDATE SET geo_json = excluded.geo_json, last_modified = excluded.last_modified WHERE service_geojson.last_modified < %(last_modified_1)s RETURNING service_geojson.id"
        )
        assert compiled_select.params["uuid"] == str(geo_uuid)
        assert compiled_select.params["geo_json"] == {
            "type": "FeatureCollection",
            "features": [make_geo_feature("x")],
        }
        assert (
            compiled_select.params["last_modified"]
            == "2022-10-05T10:10:10+00:00"
        )


class TestAs_A_User_I_Want_To_Get_GeoJSON_Features(TestBase):
    def setup_method(self):
        self.load_query_instance(geo_domain)

    def test_get_geo_feature_success(self):
        geo_uuid = uuid4()
        related_uuid = uuid4()
        mock_geo = mock.Mock(
            uuid=geo_uuid,
            geo_json={
                "type": "FeatureCollection",
                "features": [make_geo_feature(str(geo_uuid))],
            },
            # Lightning hits clock tower in Hill Valley
            last_modified=datetime.datetime(
                1955,
                11,
                12,
                22,
                4,
                0,
                tzinfo=zoneinfo.ZoneInfo("America/Los_Angeles"),
            ),
            related_uuids=[str(related_uuid)],
        )

        self.session.execute().fetchall.return_value = [mock_geo]
        self.session.reset_mock()
        geo_features = self.qry.get_geo_features(uuid=str(geo_uuid))

        assert len(geo_features) == 1
        execute_calls = self.session.execute.call_args_list

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT anon_1.uuid, anon_1.geo_json, anon_1.last_modified, array_agg(anon_1.related_uuids) AS related_uuids \n"
            "FROM (SELECT service_geojson.uuid AS uuid, service_geojson.geo_json AS geo_json, service_geojson.last_modified AS last_modified, service_geojson.uuid AS related_uuids \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid IN (%(uuid_1_1)s::UUID) UNION SELECT service_geojson.uuid AS uuid, service_geojson.geo_json AS geo_json, service_geojson.last_modified AS last_modified, service_geojson_relationship.related_uuid AS related_uuids \n"
            "FROM service_geojson_relationship JOIN service_geojson ON service_geojson.id = service_geojson_relationship.service_geojson_id \n"
            "WHERE service_geojson_relationship.related_uuid IN (%(related_uuid_1_1)s::UUID)) AS anon_1 GROUP BY anon_1.uuid, anon_1.geo_json, anon_1.last_modified"
        )

    def test_get_geo_feature_success_for_multiple_things(self):
        geo_uuids = [uuid4(), uuid4()]
        related_uuid = uuid4()

        mock_geo_features = [mock.Mock(), mock.Mock()]
        mock_geo_features[0].configure_mock(
            uuid=geo_uuids[0],
            geo_json={
                "type": "FeatureCollection",
                "features": [make_geo_feature(str(geo_uuids[0]))],
            },
            last_modified=datetime.datetime(
                2022, 1, 1, 0, 0, 0, tzinfo=datetime.UTC
            ),
            related_uuids=[related_uuid],
        )

        mock_geo_features[1].configure_mock(
            uuid=geo_uuids[1],
            geo_json={
                "type": "FeatureCollection",
                "features": [make_geo_feature(str(geo_uuids[1]))],
            },
            last_modified=datetime.datetime(
                2022, 1, 2, 0, 0, 0, tzinfo=datetime.UTC
            ),
            related_uuids=[],
        )

        self.session.execute().fetchall.return_value = mock_geo_features
        self.session.reset_mock()
        uuid = ",".join(str(uuid) for uuid in geo_uuids)
        geo_features = self.qry.get_geo_features(uuid=uuid)

        assert len(geo_features) == 2

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 1

        compiled_select = execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT anon_1.uuid, anon_1.geo_json, anon_1.last_modified, array_agg(anon_1.related_uuids) AS related_uuids \n"
            "FROM (SELECT service_geojson.uuid AS uuid, service_geojson.geo_json AS geo_json, service_geojson.last_modified AS last_modified, service_geojson.uuid AS related_uuids \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid IN (%(uuid_1_1)s::UUID, %(uuid_1_2)s::UUID) UNION SELECT service_geojson.uuid AS uuid, service_geojson.geo_json AS geo_json, service_geojson.last_modified AS last_modified, service_geojson_relationship.related_uuid AS related_uuids \n"
            "FROM service_geojson_relationship JOIN service_geojson ON service_geojson.id = service_geojson_relationship.service_geojson_id \n"
            "WHERE service_geojson_relationship.related_uuid IN (%(related_uuid_1_1)s::UUID, %(related_uuid_1_2)s::UUID)) AS anon_1 GROUP BY anon_1.uuid, anon_1.geo_json, anon_1.last_modified"
        )
        assert {
            compiled_select.params["uuid_1_1"],
            compiled_select.params["uuid_1_2"],
        } == {geo_uuids[0], geo_uuids[1]}
        assert {
            compiled_select.params["related_uuid_1_1"],
            compiled_select.params["related_uuid_1_2"],
        } == {geo_uuids[0], geo_uuids[1]}

    def test_get_geo_feature_success_for_too_many(self):
        geo_uuids = [uuid4() for _ in range(51)]
        uuids = ",".join(str(uuid) for uuid in geo_uuids)

        with pytest.raises(ValidationError):
            self.qry.get_geo_features(uuid=uuids)


class TestAs_A_User_I_Want_To_Delete_GeoJSON_Feature_on_Object_Delete(
    TestBase
):
    def setup_method(self):
        self.load_command_instance(geo_domain)

    def test_delete_geo_feature_success(self):
        geo_uuid = uuid4()
        mock_geo = mock.Mock()
        mock_geo.configure_mock(
            uuid=geo_uuid,
            geo_json={
                "type": "FeatureCollection",
                "features": [make_geo_feature(str(geo_uuid))],
            },
            last_modified=datetime.datetime(
                2022, 1, 1, 0, 0, 0, tzinfo=datetime.UTC
            ),
            related_uuids=[],
        )

        self.session.execute().fetchone.side_effect = [mock_geo]
        self.session.reset_mock()
        self.cmd.delete_geo_feature(object_uuid=str(geo_uuid))
        execute_calls = self.session.execute.call_args_list

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT service_geojson.uuid, service_geojson.geo_json, service_geojson.last_modified, service_geojson.uuid AS related_uuids \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_select.params["uuid_1"] == geo_uuid

        delete_statement = execute_calls[1][0][0]
        compiled_delete = delete_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_delete) == (
            "DELETE FROM service_geojson WHERE service_geojson.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_delete.params["uuid_1"] == str(geo_uuid)


class TestAs_A_User_I_Want_To_Link_GeoFeatures(TestBase):
    def setup_method(self):
        self.load_command_instance(geo_domain)

    def test_set_geo_feature_relationships(self):
        geo_uuid = uuid4()

        existing1 = mock.Mock()
        existing2 = mock.Mock()

        existing1.configure_mock(
            entity_id=str(uuid4()),
            related_uuid=str(uuid4()),
            origin_uuid=str(uuid4()),
            last_modified=datetime.datetime(
                2000, 10, 3, 16, 20, 11, tzinfo=datetime.UTC
            ),
        )
        existing2.configure_mock(
            entity_id=str(uuid4()),
            related_uuid=str(uuid4()),
            origin_uuid=str(uuid4()),
            last_modified=datetime.datetime(
                2000, 10, 3, 16, 20, 11, tzinfo=datetime.UTC
            ),
        )

        self.session.execute().fetchall.side_effect = [[existing1, existing2]]
        self.session.reset_mock()

        new_relation_uuid = str(uuid4())
        self.cmd.set_geo_feature_relationships(
            origin_uuid=str(geo_uuid),
            related_uuids=[existing1.related_uuid, new_relation_uuid],
            source_event_timestamp=datetime.datetime(
                2000, 10, 3, 16, 23, 11, tzinfo=datetime.UTC
            ),
        )

        # Assert there was a select, then a delete and a create.
        execute_calls = self.session.execute.call_args_list

        assert len(execute_calls) == 4

        # Call number, args, first arg
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT service_geojson_relationship.uuid AS entity_id, service_geojson.uuid AS related_uuid, service_geojson_relationship.related_uuid AS origin_uuid, service_geojson_relationship.last_modified \n"
            "FROM service_geojson_relationship JOIN service_geojson ON service_geojson.id = service_geojson_relationship.service_geojson_id \n"
            "WHERE service_geojson_relationship.related_uuid = %(related_uuid_1)s::UUID"
        )
        assert compiled_select.params["related_uuid_1"] == geo_uuid

        # Call number, args, first arg
        delete_statement = execute_calls[1][0][0]
        compiled_delete = delete_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_delete) == (
            "DELETE FROM service_geojson_relationship WHERE service_geojson_relationship.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_delete.params["uuid_1"] == existing2.entity_id

        # Call number, args, first arg
        insert_statement = execute_calls[2][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO service_geojson (uuid, geo_json, last_modified) VALUES (%(uuid)s::UUID, %(geo_json)s::JSONB, %(last_modified)s) ON CONFLICT DO NOTHING RETURNING service_geojson.id"
        )

        # Call number, args, first arg
        insert_statement = execute_calls[3][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO service_geojson_relationship (uuid, service_geojson_id, related_uuid, last_modified) SELECT %(param_1)s AS anon_1, service_geojson.id, %(param_2)s AS anon_2, %(param_3)s AS anon_3 \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid = %(uuid_1)s::UUID ON CONFLICT DO NOTHING"
        )
        assert compiled_insert.params["param_2"] == str(geo_uuid)
        assert compiled_insert.params["uuid_1"] == new_relation_uuid

    def test_update_geo_feature_relationships(self):
        geo_uuid = uuid4()

        existing1 = mock.Mock()
        existing2 = mock.Mock()

        existing1.configure_mock(
            entity_id=str(uuid4()),
            related_uuid=str(uuid4()),
            origin_uuid=str(uuid4()),
            last_modified=datetime.datetime.now(tz=datetime.UTC),
        )
        existing2.configure_mock(
            entity_id=str(uuid4()),
            related_uuid=str(uuid4()),
            origin_uuid=str(uuid4()),
            last_modified=datetime.datetime.now(tz=datetime.UTC),
        )

        self.session.execute().fetchall.side_effect = [[existing1, existing2]]
        self.session.reset_mock()

        new_relation_uuid = str(uuid4())
        self.cmd.update_geo_feature_relationships(
            origin_uuid=str(geo_uuid),
            added=[new_relation_uuid, existing1.related_uuid],
            removed=[existing2.related_uuid],
            source_event_timestamp=datetime.datetime(
                2000, 10, 3, 16, 23, 11, tzinfo=datetime.UTC
            ),
        )

        # Assert there was a select, then a delete and a create.
        execute_calls = self.session.execute.call_args_list

        assert len(execute_calls) == 4

        # Call number, args, first arg
        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT service_geojson_relationship.uuid AS entity_id, service_geojson.uuid AS related_uuid, service_geojson_relationship.related_uuid AS origin_uuid, service_geojson_relationship.last_modified \n"
            "FROM service_geojson_relationship JOIN service_geojson ON service_geojson.id = service_geojson_relationship.service_geojson_id \n"
            "WHERE service_geojson_relationship.related_uuid = %(related_uuid_1)s::UUID"
        )
        assert compiled_select.params["related_uuid_1"] == geo_uuid

        # Call number, args, first arg
        delete_statement = execute_calls[1][0][0]
        compiled_delete = delete_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_delete) == (
            "DELETE FROM service_geojson_relationship WHERE service_geojson_relationship.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_delete.params["uuid_1"] == existing2.entity_id

        # Call number, args, first arg
        insert_statement = execute_calls[2][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_insert)
            == "INSERT INTO service_geojson (uuid, geo_json, last_modified) VALUES (%(uuid)s::UUID, %(geo_json)s::JSONB, %(last_modified)s) ON CONFLICT DO NOTHING RETURNING service_geojson.id"
        )

        # Call number, args, first arg
        insert_statement = execute_calls[3][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO service_geojson_relationship (uuid, service_geojson_id, related_uuid, last_modified) SELECT %(param_1)s AS anon_1, service_geojson.id, %(param_2)s AS anon_2, %(param_3)s AS anon_3 \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid = %(uuid_1)s::UUID ON CONFLICT DO NOTHING"
        )
        assert compiled_insert.params["param_2"] == str(geo_uuid)
        assert compiled_insert.params["uuid_1"] == new_relation_uuid


class TestAs_A_User_I_Want_To_Update_GeoJSON_Features_On_Object_Change(
    TestBase
):
    def setup_method(self):
        self.load_command_instance(geo_domain)

    def _make_mock_relation_rows(self, uuids):
        relations = []
        now = datetime.datetime.now(tz=datetime.UTC)
        for related_uuid in uuids:
            relation = mock.Mock()
            relation.entity_id = uuid4()
            relation.related_uuid = related_uuid
            relation.origin_uuid = uuid4()
            relation.last_modified = now

            relations.append(relation)
        return relations

    def test_update_custom_object_geo_success(self):
        object_uuid = uuid4()
        other_object_uuid = uuid4()
        deleted_relationship_uuid = uuid4()
        added_relationship_uuid = uuid4()
        case_uuid = uuid4()

        custom_fields = {
            "abracadabra": {},
            "alakazam": {
                "value": {
                    "type": "FeatureCollection",
                    "features": [make_geo_feature()],
                }
            },
            "hocus-pocus": {"value": make_geo_feature(str(object_uuid))},
            "hocus-pocus2": {"value": make_geo_feature()},
            "shazam": json.dumps(
                {
                    "value": [
                        {
                            "specifics": {
                                "metadata": {
                                    "summary": "Geo object titel arnhem",
                                    "description": "Geo object subtitel",
                                },
                                "relationship_type": "custom_object",
                            },
                            "type": "relationship",
                            "value": str(other_object_uuid),
                        }
                    ]
                }
            ),
            "shazam2": json.dumps(
                {
                    "value": [
                        {
                            "type": "relationship",
                            "value": str(added_relationship_uuid),
                            "specifics": {
                                "metadata": {"summary": "other_object_uuid"},
                                "relationship_type": "custom_object",
                            },
                        }
                    ]
                }
            ),
            "magic_bad_geojson": {"value": {"not": "geojson"}},
            "magic_bad_geojson2": {"value": "not even a dict"},
            "magic_address_v2": {
                "value": {
                    "geojson": {
                        "type": "FeatureCollection",
                        "features": [make_geo_feature()],
                    },
                    "bag": {"id": 123456},
                    "address": {"full": "sample address"},
                }
            },
            "magic_bad2_address_v2": {"value": None},
            "magic_bad_address_v2": {"value": {"not_geojson": True}},
        }

        mock_db_row = mock.Mock()
        mock_db_row.uuid = object_uuid
        mock_db_row.family = "object"
        mock_db_row.title = "Custom Object"
        mock_db_row.subtitle = "Custom Object subtitle"
        mock_db_row.status = "active"
        mock_db_row.type = "Custom Object Type"

        mock_db_row.custom_field_definition = {
            "custom_fields": [
                {},
                {"custom_field_specification": None},
                {"custom_field_specification": {}},
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "abracadabra",
                    "custom_field_type": "geojson",
                    "label": "geo_location",
                    "type": "Custom Object Type",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "alakazam",
                    "custom_field_type": "geojson",
                    "label": "geo_location",
                    "type": "Custom Object Type",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "hocus-pocus",
                    "custom_field_type": "geojson",
                    "label": "geo_location",
                    "type": "Custom Object Type",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "hocus-pocus2",
                    "custom_field_type": "geojson",
                    "label": "geo_location",
                    "type": "Custom Object Type",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "shazam",
                    "custom_field_type": "relationship",
                    "label": "geo_location",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "shazam2",
                    "custom_field_type": "relationship",
                    "label": "geo_location",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "magic_bad_geojson",
                    "custom_field_type": "geojson",
                    "label": "geo_location",
                    "type": "Custom Object Type",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "magic_bad_geojson2",
                    "custom_field_type": "geojson",
                    "label": "geo_location",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "magic_address_v2",
                    "custom_field_type": "address_v2",
                    "label": "geo_location",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "magic_bad_address_v2",
                    "custom_field_type": "address_v2",
                    "label": "geo_location",
                },
                {
                    "custom_field_specification": {"use_on_map": True},
                    "magic_string": "magic_bad2_address_v2",
                    "custom_field_type": "address_v2",
                    "label": "geo_location",
                },
            ]
        }

        self.session.execute().fetchone.return_value = mock_db_row

        mock_relation_uuids = [
            # Stays the same
            other_object_uuid,
            # Not in custom fields anymore
            deleted_relationship_uuid,
        ]
        mock_relation_rows = self._make_mock_relation_rows(
            uuids=mock_relation_uuids
        )
        self.session.execute().fetchall.return_value = mock_relation_rows
        self.session.execute.reset_mock()
        self.cmd.create_geo_feature = mock.Mock()

        # This is what we're testing:
        self.cmd.update_custom_object_geo(
            object_uuid=str(object_uuid),
            custom_fields=custom_fields,
            cases=[str(case_uuid)],
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.create_geo_feature.assert_called_once_with(
            uuid=str(object_uuid),
            geojson=get_custom_object_geojson(object_uuid),
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 5
        # repo(custom_object).get_by_object_uuid
        # [call number][args=0, kwargs=1][argument nr]

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT custom_object.uuid, custom_object_type_version.custom_field_definition, custom_object_version.title, custom_object_version.subtitle, custom_object_type_version.name AS type, custom_object_version.status, %(param_1)s AS family \n"
            "FROM custom_object_version JOIN custom_object ON custom_object_version.custom_object_id = custom_object.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id \n"
            "WHERE custom_object_version.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_select.params["uuid_1"] == object_uuid

        # create_geo_feature is mocked
        # repo(geo_feature_relationship).get_relationships_for_object
        select_statement = call_list[1][0][0]
        assert isinstance(select_statement, sql.selectable.Select)

    def test_update_custom_object_geo_unknown_object(self):
        object_uuid = uuid4()

        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound):
            self.cmd.update_custom_object_geo(
                object_uuid=str(object_uuid),
                custom_fields={},
                cases=[],
                source_event_timestamp=datetime.datetime(
                    2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
                ),
            )


class TestAs_A_User_I_Want_To_Update_GeoJSON_Features_On_Case_Change(TestBase):
    def setup_method(self):
        self.load_command_instance(geo_domain)

    def test_update_case_geo_success(self):
        case_uuid = uuid4()
        related_uuid = uuid4()
        related_uuid2 = uuid4()

        custom_fields = {
            "magic_geo": [
                json.dumps(
                    {
                        "type": "FeatureCollection",
                        "features": [make_geo_feature("from_geojson")],
                    }
                )
            ],
            "magic_empty": [],
            "magic_not_in_case_type": ["ignored"],
            "magic_no_map": ["ignored"],
            "magic_no_geo": ["ignored"],
            "magic_invalid_json": ["{]"],
            "magic_no_properties": ["ignored"],
            "magic_address_v2": [
                json.dumps(
                    {
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [make_geo_feature("from_address_v2")],
                        },
                        "bag": {"id": 123456},
                        "address": {"full": "sample address"},
                    }
                )
            ],
            "magic_invalid_address_v2": [
                json.dumps(
                    {
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [make_geo_feature("from_address_v2")],
                        },
                    }
                )
            ],
            "magic_relationship_single": [
                json.dumps(
                    {
                        "label": "Test object abcdefg",
                        "type": "custom_object",
                        "name": "Some name",
                        "description": "Some Object Type",
                        "value": str(related_uuid),
                    }
                )
            ],
            "magic_relationship_multi": [
                json.dumps(
                    {
                        "value": [
                            {
                                "value": str(related_uuid2),
                                "specifics": {
                                    "metadata": {
                                        "summary": "Test object abcdefg",
                                        "description": "Some Object Type",
                                    },
                                    "relationship_type": "custom_object",
                                },
                                "type": "relationship",
                            }
                        ],
                        "specifics": None,
                        "type": "relationship",
                    }
                )
            ],
        }

        mock_db_row = mock.Mock()

        mock_db_row.uuid = case_uuid
        mock_db_row.title = "10"
        mock_db_row.family = "case"
        mock_db_row.status = "open"
        mock_db_row.assignee = "auser"
        mock_db_row.requestor = "buser"
        mock_db_row.requestor_uuid = uuid4()
        mock_db_row.type = "case-type"
        mock_db_row.subtitle = "case information"
        mock_db_row.use_geojson_address = False
        mock_db_row.custom_fields = [
            {
                "magic_string": "magic_geo",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_empty",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_no_map",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "0"},
            },
            {
                "magic_string": "magic_no_geo",
                "field_type": "not_geojson",
                "label": "geo_location",
                "properties": {},
            },
            {
                "magic_string": "magic_invalid_json",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_no_properties",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": None,
            },
            {
                "magic_string": "magic_address_v2",
                "field_type": "address_v2",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_invalid_address_v2",
                "field_type": "address_v2",
                "label": "geo_location_2",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_relationship_single",
                "field_type": "relationship",
                "label": "relationship_single",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_relationship_multi",
                "field_type": "relationship",
                "label": "relationship_multi",
                "properties": {"show_on_map": "1"},
            },
        ]

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        saved_now = datetime.datetime.utcnow()
        with mock.patch("minty.cqrs.events.datetime") as mocked_datetime:
            mocked_datetime.utcnow.return_value = saved_now

            # The code to test:
            self.cmd.update_case_geo(
                case_uuid=str(case_uuid),
                custom_fields=custom_fields,
                source_event_timestamp=datetime.datetime(
                    2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
                ),
            )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 7

        # First: get case
        select_case_statement = call_list[0][0][0]

        compiled_select = select_case_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT zaak.uuid, CAST(zaak.id AS VARCHAR) AS title, %(param_1)s AS family, CAST(subject.properties AS JSON) ->> %(param_2)s AS assignee, zaak.onderwerp AS subtitle, zaak.status, zaaktype_node.titel AS type, coalesce(zaaktype_node.adres_geojson, %(coalesce_1)s) AS use_geojson_address, coalesce(zaak_betrokkenen.naam, %(coalesce_2)s) AS requestor, coalesce(bedrijf_1.uuid, natuurlijk_persoon_1.uuid, subject_1.uuid, NULL) AS requestor_uuid, json_agg(json_build_object(%(json_build_object_1)s, bibliotheek_kenmerken.magic_string, %(json_build_object_2)s, bibliotheek_kenmerken.value_type, %(json_build_object_3)s, bibliotheek_kenmerken.naam, %(json_build_object_4)s, CAST(zaaktype_kenmerken.properties AS JSON))) AS custom_fields \n"
            "FROM zaak JOIN zaaktype_kenmerken ON zaaktype_kenmerken.zaaktype_node_id = zaak.zaaktype_node_id JOIN bibliotheek_kenmerken ON bibliotheek_kenmerken.id = zaaktype_kenmerken.bibliotheek_kenmerken_id JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id LEFT OUTER JOIN (zaak_betrokkenen LEFT OUTER JOIN bedrijf AS bedrijf_1 ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_1)s AND zaak_betrokkenen.gegevens_magazijn_id = bedrijf_1.id LEFT OUTER JOIN natuurlijk_persoon AS natuurlijk_persoon_1 ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_2)s AND zaak_betrokkenen.gegevens_magazijn_id = natuurlijk_persoon_1.id LEFT OUTER JOIN subject AS subject_1 ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_3)s AND zaak_betrokkenen.gegevens_magazijn_id = subject_1.id) ON zaak.aanvrager = zaak_betrokkenen.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID GROUP BY zaak.uuid, zaak.id, zaak.onderwerp, subject.properties, zaaktype_node.titel, zaaktype_node.adres_geojson, zaak_betrokkenen.naam, zaak.status, bedrijf_1.uuid, natuurlijk_persoon_1.uuid, subject_1.uuid"
        )
        assert compiled_select.params["uuid_1"] == case_uuid

        # Second: insert new geo data
        insert_statement = call_list[1][0][0]
        assert isinstance(insert_statement, postgresql.dml.Insert)
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO service_geojson (uuid, geo_json, last_modified) VALUES (%(uuid)s::UUID, %(geo_json)s::JSONB, %(last_modified)s) ON CONFLICT (uuid) DO UPDATE SET geo_json = excluded.geo_json, last_modified = excluded.last_modified WHERE service_geojson.last_modified < %(last_modified_1)s RETURNING service_geojson.id"
        )
        assert (
            compiled_insert.params["last_modified"]
            == "2022-10-05T10:10:10+00:00"
        )
        assert compiled_insert.params["uuid"] == str(case_uuid)
        assert compiled_insert.params["geo_json"] == {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {
                        "identifier": "from_geojson",
                        "zaaksysteem": {
                            "origin": {
                                "identifier": str(case_uuid),
                                "title": "10",
                                "subtitle": "case information",
                                "status": "open",
                                "family": "case",
                                "type": "case-type",
                                "custom_field_magicstring": "magic_geo",
                                "custom_field_label": "geo_location",
                                "assignee": "auser",
                                "requestor": "buser",
                            }
                        },
                    },
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [
                                [4.72412109375, 52.25470880113083],
                                [5.1251220703125, 52.25470880113083],
                                [5.1251220703125, 52.48612543090344],
                                [4.72412109375, 52.48612543090344],
                                [4.72412109375, 52.25470880113083],
                            ]
                        ],
                    },
                },
                {
                    "type": "Feature",
                    "geometry": {
                        "coordinates": [
                            [
                                [4.72412109375, 52.25470880113083],
                                [5.1251220703125, 52.25470880113083],
                                [5.1251220703125, 52.48612543090344],
                                [4.72412109375, 52.48612543090344],
                                [4.72412109375, 52.25470880113083],
                            ]
                        ],
                        "type": "Polygon",
                    },
                    "properties": {
                        "identifier": "from_address_v2",
                        "zaaksysteem": {
                            "origin": {
                                "assignee": "auser",
                                "custom_field_label": "geo_location",
                                "custom_field_magicstring": "magic_address_v2",
                                "family": "case",
                                "identifier": str(case_uuid),
                                "location_bagid": 123456,
                                "location_description": "sample address",
                                "requestor": "buser",
                                "status": "open",
                                "subtitle": "case information",
                                "title": "10",
                                "type": "case-type",
                            }
                        },
                    },
                },
                {
                    "type": "Feature",
                    "properties": {"identifier": "from_address_v2"},
                    "geometry": {
                        "coordinates": [
                            [
                                [4.72412109375, 52.25470880113083],
                                [5.1251220703125, 52.25470880113083],
                                [5.1251220703125, 52.48612543090344],
                                [4.72412109375, 52.48612543090344],
                                [4.72412109375, 52.25470880113083],
                            ]
                        ],
                        "type": "Polygon",
                    },
                },
            ],
        }

        # Third: get list of feature relationships
        select_case_statement = call_list[2][0][0]

        compiled_select = select_case_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT service_geojson_relationship.uuid AS entity_id, service_geojson.uuid AS related_uuid, service_geojson_relationship.related_uuid AS origin_uuid, service_geojson_relationship.last_modified \n"
            "FROM service_geojson_relationship JOIN service_geojson ON service_geojson.id = service_geojson_relationship.service_geojson_id \n"
            "WHERE service_geojson_relationship.related_uuid = %(related_uuid_1)s::UUID"
        )
        assert compiled_select.params["related_uuid_1"] == case_uuid

        # Fourth: insert service_geojson record if not present
        upsert_relation_statement = call_list[3][0][0]
        compiled_query = upsert_relation_statement.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_query)
            == "INSERT INTO service_geojson (uuid, geo_json, last_modified) VALUES (%(uuid)s::UUID, %(geo_json)s::JSONB, %(last_modified)s) ON CONFLICT DO NOTHING RETURNING service_geojson.id"
        )
        assert compiled_query.params == {
            "geo_json": {"features": [], "type": "FeatureCollection"},
            "last_modified": datetime.datetime(
                1970, 1, 1, 0, 0, tzinfo=datetime.UTC
            ),
            "uuid": str(related_uuid),
        }

        # Fifth: insert service_geojson_relationship
        upsert_relation_statement = call_list[4][0][0]
        compiled_query = upsert_relation_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_query) == (
            "INSERT INTO service_geojson_relationship (uuid, service_geojson_id, related_uuid, last_modified) SELECT %(param_1)s AS anon_1, service_geojson.id, %(param_2)s AS anon_2, %(param_3)s AS anon_3 \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid = %(uuid_1)s::UUID ON CONFLICT DO NOTHING"
        )

        assert compiled_query.params["param_2"] == str(case_uuid)
        assert compiled_query.params["uuid_1"] == str(related_uuid)

        # Sixth: insert service_geojson record if not present
        upsert_relation_statement = call_list[5][0][0]
        compiled_query = upsert_relation_statement.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_query)
            == "INSERT INTO service_geojson (uuid, geo_json, last_modified) VALUES (%(uuid)s::UUID, %(geo_json)s::JSONB, %(last_modified)s) ON CONFLICT DO NOTHING RETURNING service_geojson.id"
        )

        # Seventh: get list of feature relationships
        upsert_relation_statement = call_list[6][0][0]
        compiled_query = upsert_relation_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_query) == (
            "INSERT INTO service_geojson_relationship (uuid, service_geojson_id, related_uuid, last_modified) SELECT %(param_1)s AS anon_1, service_geojson.id, %(param_2)s AS anon_2, %(param_3)s AS anon_3 \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid = %(uuid_1)s::UUID ON CONFLICT DO NOTHING"
        )

        assert compiled_query.params["param_2"] == str(case_uuid)
        assert compiled_query.params["uuid_1"] == str(related_uuid2)

    def test_update_case_geo_with_use_requestor_geojson_success(self):
        case_uuid = uuid4()

        custom_fields = {
            "magic_geo": [
                json.dumps(
                    {
                        "type": "FeatureCollection",
                        "features": [make_geo_feature("from_geojson")],
                    }
                )
            ],
            "magic_empty": [],
            "magic_not_in_case_type": ["ignored"],
            "magic_no_map": ["ignored"],
            "magic_no_geo": ["ignored"],
            "magic_invalid_json": ["{]"],
            "magic_no_properties": ["ignored"],
            "magic_address_v2": [
                json.dumps(
                    {
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [make_geo_feature("from_address_v2")],
                        },
                        "bag": {"id": 123456},
                        "address": {"full": "sample address"},
                    }
                )
            ],
            "magic_invalid_address_v2": [
                json.dumps(
                    {
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [make_geo_feature("from_address_v2")],
                        },
                    }
                )
            ],
        }

        mock_db_row = mock.Mock()

        mock_db_row.uuid = case_uuid
        mock_db_row.title = "10"
        mock_db_row.family = "case"
        mock_db_row.status = "open"
        mock_db_row.assignee = "auser"
        mock_db_row.requestor = "buser"
        mock_db_row.requestor_uuid = uuid4()
        mock_db_row.type = "case-type"
        mock_db_row.subtitle = "case information"
        mock_db_row.use_geojson_address = True
        mock_db_row.custom_fields = [
            {
                "magic_string": "magic_geo",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_empty",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_no_map",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "0"},
            },
            {
                "magic_string": "magic_no_geo",
                "field_type": "not_geojson",
                "label": "geo_location",
                "properties": {},
            },
            {
                "magic_string": "magic_invalid_json",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_no_properties",
                "field_type": "geojson",
                "label": "geo_location",
                "properties": None,
            },
            {
                "magic_string": "magic_address_v2",
                "field_type": "address_v2",
                "label": "geo_location",
                "properties": {"show_on_map": "1"},
            },
            {
                "magic_string": "magic_invalid_address_v2",
                "field_type": "address_v2",
                "label": "geo_location_2",
                "properties": {"show_on_map": "1"},
            },
        ]

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        self.cmd.update_case_geo(
            case_uuid=str(case_uuid),
            custom_fields=custom_fields,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 5

        # Last: insert geo relationship (rest is tested above)
        insert_case_statement = call_list[4][0][0]
        assert isinstance(insert_case_statement, postgresql.dml.Insert)

        compiled_insert = insert_case_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO service_geojson_relationship (uuid, service_geojson_id, related_uuid, last_modified) SELECT %(param_1)s AS anon_1, service_geojson.id, %(param_2)s AS anon_2, %(param_3)s AS anon_3 \n"
            "FROM service_geojson \n"
            "WHERE service_geojson.uuid = %(uuid_1)s::UUID ON CONFLICT DO NOTHING"
        )
        assert compiled_insert.params["uuid_1"] == str(
            mock_db_row.requestor_uuid
        )

    def test_update_case_geo_case_not_found(self):
        case_uuid = uuid4()

        self.session.execute().fetchone.return_value = None
        self.session.reset_mock()

        self.cmd.update_case_geo(
            case_uuid=str(case_uuid),
            custom_fields={},
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )


class TestGeoFeaturesForContacts(TestBase):
    def setup_method(self):
        self.load_command_instance(geo_domain)

    def test_update_contact_geo(self):
        self.cmd.create_geo_feature = mock.Mock()

        contact_uuid = uuid4()
        geojson = deepcopy(GEO_JSON)
        geojson["features"].append(make_geo_feature("contact"))

        geojson_full = deepcopy(geojson)
        geojson_full["features"][0]["properties"]["zaaksysteem"] = {
            "origin": {
                "identifier": str(contact_uuid),
                "title": "Test Contact",
                "location_description": "Some Fake Address",
                "family": "organization",
                "status": "active",
            }
        }

        mock_contact = mock.Mock()
        mock_contact.configure_mock(
            uuid=contact_uuid,
            name="Test Contact",
            address="Some Fake Address",
            type="organization",
            status="active",
        )

        self.session.execute().fetchone.side_effect = [mock_contact]
        self.session.reset_mock()

        self.cmd.update_contact_geo(
            contact_uuid=str(contact_uuid),
            geojson=geojson,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.create_geo_feature.assert_called_once_with(
            uuid=contact_uuid,
            geojson=geojson_full,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        # First "arg" of "args" of the first call
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT bedrijf.uuid, bedrijf.handelsnaam AS name, CASE WHEN (bedrijf.vestiging_straatnaam IS NOT NULL) THEN concat(bedrijf.vestiging_straatnaam, %(param_1)s || CAST(bedrijf.vestiging_huisnummer AS VARCHAR), bedrijf.vestiging_huisletter, %(nullif_1)s || nullif(bedrijf.vestiging_huisnummertoevoeging, %(nullif_2)s), %(concat_1)s, %(vestiging_postcode_1)s || bedrijf.vestiging_postcode, %(vestiging_woonplaats_1)s || bedrijf.vestiging_woonplaats) WHEN (bedrijf.vestiging_adres_buitenland1 IS NOT NULL) THEN concat(bedrijf.vestiging_adres_buitenland1, %(vestiging_adres_buitenland2_1)s || bedrijf.vestiging_adres_buitenland2, %(vestiging_adres_buitenland3_1)s || bedrijf.vestiging_adres_buitenland3) WHEN (bedrijf.correspondentie_straatnaam IS NOT NULL) THEN concat(bedrijf.correspondentie_straatnaam, %(param_2)s || CAST(bedrijf.correspondentie_huisnummer AS VARCHAR), bedrijf.correspondentie_huisletter, %(nullif_3)s || nullif(bedrijf.correspondentie_huisnummertoevoeging, %(nullif_4)s), %(concat_2)s, %(correspondentie_postcode_1)s || bedrijf.correspondentie_postcode, %(correspondentie_woonplaats_1)s || bedrijf.correspondentie_woonplaats) WHEN (bedrijf.correspondentie_adres_buitenland1 IS NOT NULL) THEN concat(bedrijf.correspondentie_adres_buitenland1, %(correspondentie_adres_buitenland2_1)s || bedrijf.correspondentie_adres_buitenland2, %(correspondentie_adres_buitenland3_1)s || bedrijf.correspondentie_adres_buitenland3) ELSE %(param_3)s END AS address, %(param_4)s AS type, CASE WHEN (bedrijf.date_ceased IS NULL AND bedrijf.date_founded IS NULL) THEN NULL WHEN (bedrijf.date_ceased <= CURRENT_DATE) THEN %(param_5)s ELSE %(param_6)s END AS status \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.deleted_on IS NULL AND uuid = %(uuid_1)s::UUID UNION ALL SELECT natuurlijk_persoon.uuid, get_display_name_for_person(hstore(ARRAY[%(param_7)s, %(param_8)s, %(param_9)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS name, coalesce(concat(adres.straatnaam || %(straatnaam_1)s || CAST(adres.huisnummer AS VARCHAR), adres.huisletter, %(huisnummertoevoeging_1)s || adres.huisnummertoevoeging, %(concat_3)s, adres.postcode || %(postcode_1)s, adres.woonplaats), %(coalesce_1)s) AS address, %(param_10)s AS type, CASE WHEN (natuurlijk_persoon.datum_overlijden IS NOT NULL) THEN %(param_11)s ELSE %(param_12)s END AS status \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN adres ON natuurlijk_persoon.adres_id = adres.id \n"
            "WHERE natuurlijk_persoon.active IS true AND natuurlijk_persoon.deleted_on IS NULL AND uuid = %(uuid_2)s::UUID"
        )
        assert compiled_select.params["uuid_1"] == contact_uuid
        assert compiled_select.params["uuid_2"] == contact_uuid

    def test_update_contact_geo_notfound(self):
        self.cmd.create_geo_feature = mock.Mock()

        contact_uuid = uuid4()
        geojson = deepcopy(GEO_JSON)
        geojson["features"].append(make_geo_feature("contact"))

        self.session.execute().fetchone.return_value = None
        self.session.reset_mock()

        self.cmd.update_contact_geo(
            contact_uuid=str(contact_uuid),
            geojson=geojson,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.create_geo_feature.assert_not_called()

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        # First "arg" of "args" of the first call
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT bedrijf.uuid, bedrijf.handelsnaam AS name, CASE WHEN (bedrijf.vestiging_straatnaam IS NOT NULL) THEN concat(bedrijf.vestiging_straatnaam, %(param_1)s || CAST(bedrijf.vestiging_huisnummer AS VARCHAR), bedrijf.vestiging_huisletter, %(nullif_1)s || nullif(bedrijf.vestiging_huisnummertoevoeging, %(nullif_2)s), %(concat_1)s, %(vestiging_postcode_1)s || bedrijf.vestiging_postcode, %(vestiging_woonplaats_1)s || bedrijf.vestiging_woonplaats) WHEN (bedrijf.vestiging_adres_buitenland1 IS NOT NULL) THEN concat(bedrijf.vestiging_adres_buitenland1, %(vestiging_adres_buitenland2_1)s || bedrijf.vestiging_adres_buitenland2, %(vestiging_adres_buitenland3_1)s || bedrijf.vestiging_adres_buitenland3) WHEN (bedrijf.correspondentie_straatnaam IS NOT NULL) THEN concat(bedrijf.correspondentie_straatnaam, %(param_2)s || CAST(bedrijf.correspondentie_huisnummer AS VARCHAR), bedrijf.correspondentie_huisletter, %(nullif_3)s || nullif(bedrijf.correspondentie_huisnummertoevoeging, %(nullif_4)s), %(concat_2)s, %(correspondentie_postcode_1)s || bedrijf.correspondentie_postcode, %(correspondentie_woonplaats_1)s || bedrijf.correspondentie_woonplaats) WHEN (bedrijf.correspondentie_adres_buitenland1 IS NOT NULL) THEN concat(bedrijf.correspondentie_adres_buitenland1, %(correspondentie_adres_buitenland2_1)s || bedrijf.correspondentie_adres_buitenland2, %(correspondentie_adres_buitenland3_1)s || bedrijf.correspondentie_adres_buitenland3) ELSE %(param_3)s END AS address, %(param_4)s AS type, CASE WHEN (bedrijf.date_ceased IS NULL AND bedrijf.date_founded IS NULL) THEN NULL WHEN (bedrijf.date_ceased <= CURRENT_DATE) THEN %(param_5)s ELSE %(param_6)s END AS status \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.deleted_on IS NULL AND uuid = %(uuid_1)s::UUID UNION ALL SELECT natuurlijk_persoon.uuid, get_display_name_for_person(hstore(ARRAY[%(param_7)s, %(param_8)s, %(param_9)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS name, coalesce(concat(adres.straatnaam || %(straatnaam_1)s || CAST(adres.huisnummer AS VARCHAR), adres.huisletter, %(huisnummertoevoeging_1)s || adres.huisnummertoevoeging, %(concat_3)s, adres.postcode || %(postcode_1)s, adres.woonplaats), %(coalesce_1)s) AS address, %(param_10)s AS type, CASE WHEN (natuurlijk_persoon.datum_overlijden IS NOT NULL) THEN %(param_11)s ELSE %(param_12)s END AS status \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN adres ON natuurlijk_persoon.adres_id = adres.id \n"
            "WHERE natuurlijk_persoon.active IS true AND natuurlijk_persoon.deleted_on IS NULL AND uuid = %(uuid_2)s::UUID"
        )
        assert compiled_select.params["uuid_1"] == contact_uuid
        assert compiled_select.params["uuid_2"] == contact_uuid

    def test_update_case_requestor(self):
        self.cmd.update_geo_feature_relationships = mock.Mock()

        case_uuid = uuid4()

        mock_case = mock.Mock()
        mock_case.configure_mock(
            uuid=case_uuid,
            title="123",
            family="case",
            assignee="Piet Friet",
            subtitle="Sub Title",
            status="open",
            type="Zaaktype",
            use_geojson_address=True,
            requestor="Requestor Name Here",
            requestor_uuid=uuid4(),
            custom_fields=[],
        )

        self.session.execute().fetchone.side_effect = [mock_case]
        self.session.reset_mock()

        old_requestor_uuid = uuid4()
        new_requestor_uuid = uuid4()

        self.cmd.update_case_requestor(
            case_uuid=case_uuid,
            old_requestor_uuid=old_requestor_uuid,
            new_requestor_uuid=new_requestor_uuid,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.update_geo_feature_relationships.assert_called_once_with(
            origin_uuid=case_uuid,
            added=[new_requestor_uuid],
            removed=[old_requestor_uuid],
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        # First "arg" of "args" of the first call
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT zaak.uuid, CAST(zaak.id AS VARCHAR) AS title, %(param_1)s AS family, CAST(subject.properties AS JSON) ->> %(param_2)s AS assignee, zaak.onderwerp AS subtitle, zaak.status, zaaktype_node.titel AS type, coalesce(zaaktype_node.adres_geojson, %(coalesce_1)s) AS use_geojson_address, coalesce(zaak_betrokkenen.naam, %(coalesce_2)s) AS requestor, coalesce(bedrijf_1.uuid, natuurlijk_persoon_1.uuid, subject_1.uuid, NULL) AS requestor_uuid, json_agg(json_build_object(%(json_build_object_1)s, bibliotheek_kenmerken.magic_string, %(json_build_object_2)s, bibliotheek_kenmerken.value_type, %(json_build_object_3)s, bibliotheek_kenmerken.naam, %(json_build_object_4)s, CAST(zaaktype_kenmerken.properties AS JSON))) AS custom_fields \n"
            "FROM zaak JOIN zaaktype_kenmerken ON zaaktype_kenmerken.zaaktype_node_id = zaak.zaaktype_node_id JOIN bibliotheek_kenmerken ON bibliotheek_kenmerken.id = zaaktype_kenmerken.bibliotheek_kenmerken_id JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id LEFT OUTER JOIN (zaak_betrokkenen LEFT OUTER JOIN bedrijf AS bedrijf_1 ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_1)s AND zaak_betrokkenen.gegevens_magazijn_id = bedrijf_1.id LEFT OUTER JOIN natuurlijk_persoon AS natuurlijk_persoon_1 ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_2)s AND zaak_betrokkenen.gegevens_magazijn_id = natuurlijk_persoon_1.id LEFT OUTER JOIN subject AS subject_1 ON zaak_betrokkenen.betrokkene_type = %(betrokkene_type_3)s AND zaak_betrokkenen.gegevens_magazijn_id = subject_1.id) ON zaak.aanvrager = zaak_betrokkenen.id \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID GROUP BY zaak.uuid, zaak.id, zaak.onderwerp, subject.properties, zaaktype_node.titel, zaaktype_node.adres_geojson, zaak_betrokkenen.naam, zaak.status, bedrijf_1.uuid, natuurlijk_persoon_1.uuid, subject_1.uuid"
        )
        assert compiled_select.params["uuid_1"] == case_uuid

    def test_update_case_requestor_no_old_requestor(self):
        self.cmd.update_geo_feature_relationships = mock.Mock()

        case_uuid = uuid4()

        mock_case = mock.Mock()
        mock_case.configure_mock(
            uuid=case_uuid,
            title="123",
            family="case",
            assignee="Piet Friet",
            subtitle="Sub Title",
            status="open",
            type="Zaaktype",
            use_geojson_address=True,
            requestor="Requestor Name Here",
            requestor_uuid=uuid4(),
            custom_fields=[],
        )

        self.session.execute().fetchone.side_effect = [mock_case]
        self.session.reset_mock()

        new_requestor_uuid = uuid4()

        self.cmd.update_case_requestor(
            case_uuid=case_uuid,
            old_requestor_uuid=None,
            new_requestor_uuid=new_requestor_uuid,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.update_geo_feature_relationships.assert_called_once_with(
            origin_uuid=case_uuid,
            added=[new_requestor_uuid],
            removed=[],
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        # First "arg" of "args" of the first call
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert compiled_select.params["uuid_1"] == case_uuid

    def test_update_case_requestor_casetype_with_flag_off(self):
        self.cmd.update_geo_feature_relationships = mock.Mock()

        case_uuid = uuid4()

        mock_case = mock.Mock()
        mock_case.configure_mock(
            uuid=case_uuid,
            title="123",
            family="case",
            assignee="Piet Friet",
            subtitle="Sub Title",
            status="open",
            type="Zaaktype",
            use_geojson_address=False,
            requestor="Requestor Name Here",
            requestor_uuid=uuid4(),
            custom_fields=[],
        )

        self.session.execute().fetchone.side_effect = [mock_case]
        self.session.reset_mock()

        old_requestor_uuid = str(uuid4())
        new_requestor_uuid = str(uuid4())

        self.cmd.update_case_requestor(
            case_uuid=str(case_uuid),
            old_requestor_uuid=old_requestor_uuid,
            new_requestor_uuid=new_requestor_uuid,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.update_geo_feature_relationships.assert_not_called()

    def test_update_case_requestor_case_not_found(self):
        self.cmd.update_geo_feature_relationships = mock.Mock()

        case_uuid = uuid4()

        mock_case = mock.Mock()
        mock_case.configure_mock(
            uuid=case_uuid,
            title="123",
            family="case",
            assignee="Piet Friet",
            subtitle="Sub Title",
            status="open",
            type="Zaaktype",
            use_geojson_address=False,
            requestor="Requestor Name Here",
            requestor_uuid=uuid4(),
            custom_fields=[],
        )

        self.session.execute().fetchone.side_effect = [None]
        self.session.reset_mock()

        old_requestor_uuid = str(uuid4())
        new_requestor_uuid = str(uuid4())

        self.cmd.update_case_requestor(
            case_uuid=str(case_uuid),
            old_requestor_uuid=old_requestor_uuid,
            new_requestor_uuid=new_requestor_uuid,
            source_event_timestamp=datetime.datetime(
                2022, 10, 5, 10, 10, 10, tzinfo=datetime.UTC
            ),
        )

        self.cmd.update_geo_feature_relationships.assert_not_called()
