# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from uuid import UUID


def make_job_json(
    job_uuid: UUID,
    friendly_name: str = "example context",
    status: str = "pending",
    results_file: str | None = None,
    results_file_location: str | None = None,
    results_file_name: str | None = None,
    results_file_type: str | None = None,
    errors_file: str | None = None,
    errors_file_location: str | None = None,
    errors_file_name: str | None = None,
    selection: dict | None = None,
    total_item_count: int = 0,
    cancel_reason: str | None = None,
    job_type: str = "delete_case",
) -> bytes:
    return json.dumps(
        {
            "uuid": str(job_uuid),
            "status": status,
            "results_file": results_file,
            "results_file_location": results_file_location,
            "results_file_name": results_file_name,
            "results_file_type": results_file_type,
            "errors_file": errors_file,
            "errors_file_location": errors_file_location,
            "errors_file_name": errors_file_name,
            "job_description": {
                "job_type": job_type,
                "selection": selection or {"type": "filter", "filters": []},
            },
            "friendly_name": friendly_name,
            "started_at": "2024-10-10T10:10:10Z",
            "expires_at": "2024-10-13T10:10:10Z",
            "progress_percent": 0.0,
            "total_item_count": total_item_count,
            "cancel_reason": cancel_reason,
        }
    ).encode()
