# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from zsnl_domains.shared.services.template.template import process_template

some_date = datetime.date(2000, 4, 8)
some_datetime = datetime.datetime(2000, 5, 9, 23, 15, 0, tzinfo=datetime.UTC)


class TestTemplateProcessing:
    def test_process_empty_template(self):
        template = ""
        assert process_template(template, {}) == ""

    def test_process_template(self):
        template = """
        A template for testing
        ----------------------

        1. [[ j("$.var1") ]] <- a date with no processing
        2. [[ j("$.var2") | date ]] <- with
        3. [[ j("$.var3") | break ]] ^- extra line break
        4. [[ j("$.var4") ]] <- Regular string
        5. [[ j("$.var5") ]] <- Does not exist
        6. [[ j("$.var6") ]] <- Array field
        7. [[ j("$.var7") | date ]] <- Array field with filter
        8. [[ j("$.var8") ]] <- Dictionary
        9. [[ j("$.var9") ]] <- Boolean
        10. [[ j("$.var10") ]] <- None
        11. [[ j("$.var11") | date('yyyy MM d') ]] <- Date with filter parameter
        12. [[ j("$.var12[0:2]") ]] <- multiple matches (array slice)
        13. [[ j("$.var13") | isodate ]] <- Date with isodate filter parameter
        14. [[ j("$.var14") ]] <- Datetime with no processing
        15. [[ j("$.var15") | capitalize ]] <- Capitalize first letter
        16. [[ j("$.var16") | capitalize ]] <- Capitalize first letter for list
        17. [[ j("$.var17") | capitalize ]] <- Capitalize first letter for dict
        18. [[ j("$.var18") | capitalize ]] <- Number is not Capitalized
        """
        variables = {
            "var1": some_date,
            "var2": some_date,
            "var3": "linebreak-after",
            "var4": "some text",
            "var6": ["a", "b", "c", some_datetime],
            "var7": ["a", "b", "c", some_datetime],
            "var8": {"value": "test_value"},
            "var9": True,
            "var10": None,
            "var11": some_datetime,
            "var12": [1, 2, 3],
            "var13": some_datetime,
            "var14": some_datetime,
            "var15": "test",
            "var16": ["test"],
            "var17": {"value": "test"},
            "var18": 101,
        }

        result = process_template(template, variables)
        assert (
            result
            == """
        A template for testing
        ----------------------

        1. 08-04-2000 <- a date with no processing
        2. 8 april 2000 <- with
        3. linebreak-after
 ^- extra line break
        4. some text <- Regular string
        5.  <- Does not exist
        6. a, b, c, 09-05-2000 <- Array field
        7. a, b, c, 10 mei 2000 <- Array field with filter
        8. test_value <- Dictionary
        9. True <- Boolean
        10.  <- None
        11. 2000 05 10 <- Date with filter parameter
        12. 1, 2 <- multiple matches (array slice)
        13. 2000-05-10 <- Date with isodate filter parameter
        14. 09-05-2000 <- Datetime with no processing
        15. Test <- Capitalize first letter
        16. Test <- Capitalize first letter for list
        17. Test <- Capitalize first letter for dict
        18. 101 <- Number is not Capitalized
        """
        )

    def test_process_error(self):
        template = """[[ j("$.var1") | does_not_exist ]]"""
        variables = {"var1": "some_value"}

        result = process_template(template, variables)

        assert result == "Error in template: No filter named 'does_not_exist'."
