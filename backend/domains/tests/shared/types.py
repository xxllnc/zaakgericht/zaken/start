# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from zsnl_domains.shared import types


def test_comparison_filter_datetime():
    dt_filter = types.ComparisonFilterCondition[datetime.datetime](
        operator=types.ComparisonFilterOperator("ge"),
        operand="2021-11-03T11:52:10Z",
    )

    assert dt_filter.operator == "ge"

    assert isinstance(dt_filter.operand, datetime.datetime)

    assert dt_filter.operand == datetime.datetime(
        2021, 11, 3, 11, 52, 10, tzinfo=datetime.UTC
    )


def test_comparison_filter_int():
    int_filter = types.ComparisonFilterCondition[int](
        operator=types.ComparisonFilterOperator("eq"),
        operand="31337",
    )

    assert int_filter.operator == "eq"
    assert isinstance(int_filter.operand, int)
    assert int_filter.operand == 31337
