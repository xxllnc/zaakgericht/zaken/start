# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .case_type import CaseTypeEntity
from .case_type_version import CaseTypeVersionEntity
from .catalog_entities import (
    AttributeDetail,
    CaseTypeVersion,
    CustomObjectTypeDetail,
    DocumentTemplateDetail,
    EmailTemplateDetail,
    EntryType,
    Folder,
    ObjectTypeDetail,
    RelatedCaseType,
)
from .email_template import EmailTemplate
from .folder_entries import FolderEntry
from .versioned_casetype import VersionedCaseType

__all__ = [
    "AttributeDetail",
    "CaseTypeEntity",
    "CaseTypeVersion",
    "CaseTypeVersionEntity",
    "CustomObjectTypeDetail",
    "DocumentTemplateDetail",
    "EmailTemplate",
    "EmailTemplateDetail",
    "EntryType",
    "Folder",
    "FolderEntry",
    "ObjectTypeDetail",
    "RelatedCaseType",
    "VersionedCaseType",
]
