# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from dataclasses import dataclass
from datetime import datetime
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from typing import Literal
from uuid import UUID

VALID_ATTRIBUTE_TYPES = [
    "address_v2",
    "appointment_v2",
    "appointment",
    "bag_adres",
    "bag_adressen",
    "bag_openbareruimte",
    "bag_openbareruimtes",
    "bag_straat_adres",
    "bag_straat_adressen",
    "bankaccount",
    "calendar_supersaas",
    "calendar",
    "checkbox",
    "date",
    "email",
    "file",
    "geojson",
    "geolatlon",
    "googlemaps",
    "image_from_url",
    "numeric",
    "option",
    "relationship",
    "richtext",
    "select",
    "subject",
    "text_uc",
    "text",
    "textarea",
    "url",
    "valuta",
    "valutaex",
    "valutaex21",
    "valutaex6",
    "valutain",
    "valutain21",
    "valutain6",
]


DOCUMENT_FIELD_TYPES = {
    "document_category",
    "document_trust_level",
    "document_origin",
    "document_source",
}

APPOINTMENT_FIELD_TYPES = {
    "appointment_location_id",
    "appointment_product_id",
    "appointment_interface_uuid",
}

APPOINTMENT_V2_FIELD_TYPES = {
    "appointment_interface_uuid",
}

MULTIVALUE_FIELDS = {"option", "checkbox", "select"}


class Attribute(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        id: int,
        attribute_type: str,
        name: str,
        public_name: str,
        magic_string: str,
        type_multiple: bool,
        value_default: str,
        help: str,
        sensitive_field: bool,
        document_origin: str,
        document_trust_level: str,
        document_source: str,
        document_category: str,
        relationship_uuid: UUID,
        relationship_type: str,
        relationship_name: str,
        category_uuid: UUID,
        category_name: str,
        attribute_values: list,
        appointment_location_id,
        appointment_product_id,
        appointment_interface_uuid: UUID,
        commit_message: str,
    ):
        self.uuid = uuid
        self.id = id
        self.attribute_type = attribute_type
        self.name = name
        self.public_name = public_name
        self.magic_string = magic_string
        self.type_multiple = type_multiple
        self.value_default = value_default
        self.help = help
        self.sensitive_field = sensitive_field
        self.document_origin = document_origin
        self.document_trust_level = document_trust_level
        self.document_source = document_source
        self.document_category = document_category
        self.relationship_uuid = relationship_uuid
        self.relationship_type = relationship_type
        self.relationship_name = relationship_name
        self.category_uuid = category_uuid
        self.category_name = category_name
        self.attribute_values = attribute_values
        self.appointment_location_id = appointment_location_id
        self.appointment_product_id = appointment_product_id
        self.appointment_interface_uuid = appointment_interface_uuid
        self.deleted: datetime | None = None
        self.commit_message = commit_message

    def _is_valid_extra_attribute(self, field_type: str, key: str):
        if self.attribute_type == "file" and key in DOCUMENT_FIELD_TYPES:
            return True

        if (
            self.attribute_type == "appointment"
            and key in APPOINTMENT_FIELD_TYPES
        ):
            return True

        if (
            self.attribute_type == "appointment_v2"
            and key in APPOINTMENT_V2_FIELD_TYPES
        ):
            return True

        if (
            self.attribute_type in MULTIVALUE_FIELDS
            and key == "attribute_values"
        ):
            return True

        return False

    def _set_relationship_data(self, fields):
        if "relationship_data" not in fields.keys():
            return

        for key, fieldvalue in fields["relationship_data"].items():
            setattr(self, f"relationship_{key}", fieldvalue)

    @event(
        "AttributeEdited", extra_fields=["attribute_type", "commit_message"]
    )
    def edit(self, fields):
        """Update/edit an attribute.

        :param fields: fields of the attribute to be changed
        :type fields: dict
        """
        update_fields = {
            "name",
            "public_name",
            "type_multiple",
            "help",
            "value_default",
            "sensitive_field",
            "commit_message",
            "relationship_type",
            "relationship_uuid",
            "relationship_name",
        }

        for key, value in fields.items():
            if key in update_fields or self._is_valid_extra_attribute(
                field_type=fields["attribute_type"], key=key
            ):
                setattr(self, key, value)

        self._set_relationship_data(fields)

    @event(
        "AttributeCreated", extra_fields=["attribute_type", "commit_message"]
    )
    def create(self, fields):
        """Create an attribute.

        :param changes: fields of the attribute to be saved.
        :type changes: dict
        """
        create_fields = {
            "uuid",
            "attribute_type",
            "magic_string",
            "name",
            "public_name",
            "type_multiple",
            "help",
            "value_default",
            "sensitive_field",
            "category_uuid",
            "commit_message",
            "relationship_type",
            "relationship_uuid",
            "relationship_name",
        }

        try:
            fields["name"]
            fields["attribute_type"]
            fields["magic_string"]
        except KeyError as err:
            raise Conflict(
                f"{err} is a required field",
                "attribute/missing_required_field",
            ) from err

        if fields["attribute_type"] not in VALID_ATTRIBUTE_TYPES:
            attribute_type = fields["attribute_type"]
            raise Conflict(
                f"Invalid attribute_type '{attribute_type}'",
                "attribute/invalid_attribute_type",
            )

        for key, value in fields.items():
            if key in create_fields or self._is_valid_extra_attribute(
                field_type=fields["attribute_type"], key=key
            ):
                setattr(self, key, value)

        self._set_relationship_data(fields)

    @event("AttributeDeleted")
    def delete(self, reason: str):
        """Delete an attribute.

        :param reason: reason for delete
        :type reason: str
        """

        self.deleted = datetime.now()
        self.commit_message = reason


@dataclass
class AttributeDetailSearchResult:
    """Results when searching for Attribute's."""

    uuid: UUID
    id: int
    name: str
    magic_string: str
    value_type: str
    relationship_type: Literal["custom_object", "subject"] | None
    is_multiple: bool
