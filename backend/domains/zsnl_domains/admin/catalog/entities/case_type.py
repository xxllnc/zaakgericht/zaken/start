# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .catalog_entities import Folder
from datetime import datetime
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from uuid import UUID


class CaseTypeEntity(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        id: int,
        uuid: UUID,
        name: str,
        active: bool,
        identification: str,
        current_version: int,
        last_modified: datetime,
        current_version_uuid: UUID,
        initiator_source: list,
        folder: Folder = None,
    ):
        self.id = id
        self.uuid = uuid
        self.name = name
        self.active = active
        self.identification = identification
        self.current_version = current_version
        self.last_modified = last_modified
        self.used_in_case_types = []
        self.used_in_object_types = []
        self.reason = ""
        self.initiator_source = initiator_source
        self.current_version_uuid = current_version_uuid
        self.folder = folder
        self.deleted = None

    @event("CaseTypeOnlineStatusChanged")
    def change_online_status(self, active: bool, reason: str):
        """Change online status to active or inactive.

        :param active: status
        :type active: bool
        :raises Conflict: if no change is detected
        """
        if self.active is active:
            raise Conflict(
                f"Can't {'activate' if active else 'de-activate'} case_type, "
                + f"status is already: '{active}''",
                f"case_type/status_already_{str(active).lower()}",
            )

        self.active = active
        self.reason = reason

    @event("CaseTypeVersionUpdated")
    def update_case_type_version(
        self, new_version_uuid: UUID, new_version: int, reason: str
    ):
        """Update version of case type"""
        if str(self.current_version_uuid) == str(new_version_uuid):
            raise Conflict(
                f"Cannot update version {new_version_uuid}: it is already the active version"
            )

        self.current_version_uuid = new_version_uuid
        self.current_version = new_version
        self.reason = reason

    @event("CaseTypeDeleted")
    def delete(self, reason: str):
        """Delete a case type.

        :param reason: reason for delete
        :type active: str
        """
        self.deleted = datetime.now()
        self.reason = reason
