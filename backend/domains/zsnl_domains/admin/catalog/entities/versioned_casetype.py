# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import enum
from abc import ABC
from datetime import date, datetime
from decimal import Decimal
from minty.entity import Entity, ValueObject
from pydantic.v1 import Field, NonNegativeInt, validator
from typing import Literal, Self
from uuid import UUID

__all__ = [
    "VersionedCaseType",
]

ENTITY_TYPE: str = "versioned_casetype"


class PeriodType(enum.StrEnum):
    kalenderdagen = "kalenderdagen"
    werkdagen = "werkdagen"
    weken = "weken"
    vaste_einddatum = "einddatum"


class TriggerType(enum.StrEnum):
    Extern = "extern"
    Intern = "intern"
    Intern_Extern = "internextern"


class InitiatorType(enum.StrEnum):
    Aangaan = "aangaan"
    Aangeven = "aangeven"
    Aanmelden = "aanmelden"
    Aanschrijven = "aanschrijven"
    Aanvragen = "aanvragen"
    Afkopen = "afkopen"
    Afmelden = "afmelden"
    Indienen = "indienen"
    Inschrijven = "inschrijven"
    Melden = "melden"
    Ontvangen = "ontvangen"
    Opstellen = "opstellen"
    Opzeggen = "opzeggen"
    Registreren = "registreren"
    Reserveren = "reserveren"
    Starten = "starten"
    Stellen = "stellen"
    Uitvoeren = "uitvoeren"
    Vaststellen = "vaststellen"
    Versturen = "versturen"
    Voordragen = "voordragen"
    Vragen = "vragen"


class ProcessingLegalType(enum.StrEnum):
    Toestemming = "Toestemming"
    Overeenkomst = "Overeenkomst"
    Wettelijke_verplichting = "Wettelijke verplichting"
    Publiekrechtelijke_taak = "Publiekrechtelijke taak"
    Vitaal_belang = "Vitaal belang"
    Gerechtvaardigd_belang = "Gerechtvaardigd belang"


class ProcessingType(enum.StrEnum):
    Delen = "Delen"
    Muteren = "Muteren"
    Raadplegen = "Raadplegen"


class SubjectType(enum.StrEnum):
    Organization = "organization"
    Person = "person"
    Employee = "employee"


class DesignationOfConfidentialityType(enum.StrEnum):
    Openbaar = "Openbaar"
    Beperkt_openbaar = "Beperkt openbaar"
    Intern = "Intern"
    Zaakvertrouwelijk = "Zaakvertrouwelijk"
    Vertrouwelijk = "Vertrouwelijk"
    Confidentieel = "Confidentieel"
    Geheim = "Geheim"
    Zeer_geheim = "Zeer geheim"


class ResultTypes(enum.StrEnum):
    Aangegaan = "aangegaan"
    Aangehouden = "aangehouden"
    Aangekocht = "aangekocht"
    Aangesteld = "aangesteld"
    Aanvaard = "aanvaard"
    Afgeboekt = "afgeboekt"
    Afgebroken = "afgebroken"
    Afgehandeld = "afgehandeld"
    Afgesloten = "afgesloten"
    Afgewezen = "afgewezen"
    Akkoord = "akkoord"
    Akkoord_met_wijzigingen = "akkoord met wijzigingen"
    Behaald = "behaald"
    Betaald = "betaald"
    Beëindigd = "beëindigd"
    Buiten_behandeling_gesteld = "buiten behandeling gesteld"
    Definitief_toegekend = "definitief toegekend"
    Geannuleerd = "geannuleerd"
    Gedeeltelijk_gegrond = "gedeeltelijk gegrond"
    Gedeeltelijk_verleend = "gedeeltelijk verleend"
    Gedoogd = "gedoogd"
    Gegrond = "gegrond"
    Gegund = "gegund"
    Geleverd = "geleverd"
    Geweigerd = "geweigerd"
    Gewijzigd = "gewijzigd"
    Geïnd = "geïnd"
    Handhaving_uitgevoerd = "handhaving uitgevoerd"
    Ingericht = "ingericht"
    Ingeschreven = "ingeschreven"
    Ingesteld = "ingesteld"
    Ingetrokken = "ingetrokken"
    Ingewilligd = "ingewilligd"
    Niet_aangekocht = "niet aangekocht"
    Niet_aangesteld = "niet aangesteld"
    Niet_akkoord = "niet akkoord"
    Niet_behaald = "niet behaald"
    Niet_betaald = "niet betaald"
    Niet_doorgegaan = "niet doorgegaan"
    Niet_gegund = "niet gegund"
    Niet_geleverd = "niet geleverd"
    Niet_gewijzigd = "niet gewijzigd"
    Nietgeïnd = "niet geïnd"
    Niet_ingesteld = "niet ingesteld"
    Niet_ingetrokken = "niet ingetrokken"
    Niet_nodig = "niet nodig"
    Niet_ontvankelijk = "niet ontvankelijk"
    Niet_opgelegd = "niet opgelegd"
    Niet_opgeleverd = "niet opgeleverd"
    Niet_toegekend = "niet toegekend"
    Niet_uitgevoerd = "niet uitgevoerd"
    Niet_vastgesteld = "niet vastgesteld"
    Niet_verkregen = "niet verkregen"
    Niet_verleend = "niet verleend"
    Niet_verstrekt = "niet verstrekt"
    Niet_verwerkt = "niet verwerkt"
    Ongegrond = "ongegrond"
    Ontvankelijk = "ontvankelijk"
    Opgeheven = "opgeheven"
    Opgelegd = "opgelegd"
    Opgeleverd = "opgeleverd"
    Opgelost = "opgelost"
    Opgezegd = "opgezegd"
    Toegekend = "toegekend"
    Toezicht_uitgevoerd = "toezicht uitgevoerd"
    Uitgevoerd = "uitgevoerd"
    Vastgesteld = "vastgesteld"
    Verhuurd = "verhuurd"
    Verkocht = "verkocht"
    Verkregen = "verkregen"
    Verleend = "verleend"
    Vernietigd = "vernietigd"
    Verstrekt = "verstrekt"
    Verwerkt = "verwerkt"
    Voorlopig_toegekend = "voorlopig toegekend"
    Voorlopig_verleend = "voorlopig verleend"


class ArchivalNomination(enum.StrEnum):
    Bewaren_B = "Bewaren (B)"
    Conversie = "Conversie"
    Migratie = "Migratie"
    Overbrengen_O = "Overbrengen (O)"
    Overdracht = "Overdracht"
    Publicatie = "Publicatie"
    Vernietigen_V = "Vernietigen (V)"
    Vernietigen = "Vernietigen"
    Vervallen_beperkingen_openbaarheid = "Vervallen beperkingen openbaarheid"


class ArchivalNominationValuation(enum.StrEnum):
    Vervallen = "vervallen"
    Onherroepelijk = "onherroepelijk"
    Afhandeling = "afhandeling"
    Verwerking = "verwerking"
    Geweigerd = "geweigerd"
    Verleend = "verleend"
    Geboorte = "geboorte"
    EindeDienstverband = "einde-dienstverband"


class ResultOrigin(enum.StrEnum):
    Specifiek_benoemde_wet_en_regelgeving = (
        "Specifiek benoemde wet- en regelgeving"
    )
    Systeemanalyse = "Systeemanalyse"
    Trendanalyse = "Trendanalyse"
    Risicoanalyse = "Risicoanalyse"


class ProcessPeriod(enum.StrEnum):
    A = "A"
    B = "B"
    C = "C"
    D = "D"
    E = "E"


class GdprSource(ValueObject):
    public_source: bool = Field(
        ..., title=f"GDPR public source enabled of the {ENTITY_TYPE}"
    )
    registration: bool = Field(
        ..., title=f"GDPR registration enabled of the {ENTITY_TYPE}"
    )
    partner: bool = Field(
        ..., title=f"GDPR partner enabled of the {ENTITY_TYPE}"
    )
    sender: bool = Field(
        ..., title=f"GDPR sender enabled of the {ENTITY_TYPE}"
    )


class GdprKind(ValueObject):
    basic_details: bool = Field(
        ..., title=f"GDPR basic details enabled of the {ENTITY_TYPE}"
    )
    personal_id_number: bool = Field(
        ..., title=f"GDPR id number enabled of the {ENTITY_TYPE}"
    )
    income: bool = Field(
        ..., title=f"GDPR income enabled of the {ENTITY_TYPE}"
    )
    race_or_ethniticy: bool = Field(
        ..., title=f"GDPR race or ethniticy enabled of the {ENTITY_TYPE}"
    )
    political_views: bool = Field(
        ..., title=f"GDPR political views enabled of the {ENTITY_TYPE}"
    )
    religion: bool = Field(
        ..., title=f"GDPR religion enabled of the {ENTITY_TYPE}"
    )
    membership_union: bool = Field(
        ..., title=f"GDPR membership union enabled of the {ENTITY_TYPE}"
    )
    genetic_or_biometric_data: bool = Field(
        ...,
        title=f"GDPR genetic or biometric data enabled of the {ENTITY_TYPE}",
    )
    health: bool = Field(
        ..., title=f"GDPR health enabled of the {ENTITY_TYPE}"
    )
    sexual_identity: bool = Field(
        ..., title=f"GDPR sexual identity enabled of the {ENTITY_TYPE}"
    )
    criminal_record: bool = Field(
        ..., title=f"GDPR criminal record enabled of the {ENTITY_TYPE}"
    )
    offspring: bool = Field(
        ..., title=f"GDPR  offspring enabled of the {ENTITY_TYPE}"
    )


class Gdpr(ValueObject):
    enabled: bool = Field(..., title=f"GDPR enabled for the {ENTITY_TYPE}")
    processing_type: ProcessingType | None = Field(
        None, title=f"Processing type of {ENTITY_TYPE}"
    )
    process_foreign_country: bool = Field(
        ..., title=f"Proccess foreign country enabled of the {ENTITY_TYPE}"
    )

    process_foreign_country_reason: str | None = Field(
        None, title=f"Process foregein country reason for {ENTITY_TYPE}"
    )
    processing_legal: ProcessingLegalType | None = Field(
        None, title=f"Processing label for {ENTITY_TYPE}"
    )
    processing_legal_reason: str | None = Field(
        None, title=f"Processing legal reason for {ENTITY_TYPE}"
    )
    source: GdprSource = Field(..., title="Source of GDPR")
    kind: GdprKind = Field(..., title="Kind of GDPR")


DEFAULT_MONITORING_PERIOD_NUMERIC_VALUE = 1  # default value for a monitoring period. if no value, or an invalid value is supplied, this value will be filled
DEFAULT_MONITORING_PERIOD_DATE_VALUE = "2099-01-01"  # default value for a monitoring period for type vaste_einddatum. if no value, or an invalid value is supplied, this value will be filled


class MonitoringPeriod(ValueObject):
    type: PeriodType = Field(
        PeriodType.kalenderdagen, title="Type of monitoring period"
    )
    value: int | date | None = Field(
        DEFAULT_MONITORING_PERIOD_NUMERIC_VALUE,
        title="Value for the monitoring period, depending on the type, it must contain a date value (format dd-mm-yyyy) if the type is `einddatum`. For all other types it must be a numeric value",
    )

    @validator("value")
    @classmethod
    def validate_monitoring_period(cls, field_value, values, field, config):
        if values["type"] in (
            PeriodType.kalenderdagen,
            PeriodType.weken,
            PeriodType.werkdagen,
        ):
            if int(field_value) < 1:
                raise ValueError(
                    f"f Value '{field_value}' of {values['type']}' for monitoring must be 1 or higher"
                )

        return field_value


class CatalogFolder(ValueObject):
    uuid: UUID = Field(..., title="Uuid of the catalog folder")
    name: str = Field(None, title="Name of the catalog folder")


class GeneralAttributes(ValueObject):
    name: str = Field("", title=f"Name of the {ENTITY_TYPE}")
    identification: str = Field(
        "", title=f"Identification of the {ENTITY_TYPE}"
    )
    tags: str | None = Field(None, title=f"Tags for {ENTITY_TYPE}")
    description: str | None = Field(
        None, title=f"Description for {ENTITY_TYPE}"
    )
    case_summary: str | None = Field(
        None, title=f"Case summary for {ENTITY_TYPE}"
    )
    case_public_summary: str | None = Field(
        None, title=f"Case public summary for {ENTITY_TYPE}"
    )
    legal_period: MonitoringPeriod = Field(
        ...,
        title=f"Legal period for the {ENTITY_TYPE}",
    )
    service_period: MonitoringPeriod = Field(
        ...,
        title=f"Service period for the {ENTITY_TYPE}",
    )


class Documentation(ValueObject):
    process_description: str | None = Field(
        None, title=f"Process description of the {ENTITY_TYPE}"
    )
    initiator_type: InitiatorType | None = Field(
        None,
        title="Initiator type of the {ENTITY_TYPE}",
    )
    motivation: str | None = Field(
        None, title=f"Motivation of the {ENTITY_TYPE}"
    )
    purpose: str | None = Field(None, title=f"Purpose of the {ENTITY_TYPE}")
    archive_classification_code: str | None = Field(
        None, title=f"Archive classification code of the {ENTITY_TYPE}"
    )
    designation_of_confidentiality: DesignationOfConfidentialityType | None = (
        Field(
            None, title=f"Designation of confidentiality of the {ENTITY_TYPE}"
        )
    )

    responsible_subject: str | None = Field(
        None, title=f"Resposible subject of the {ENTITY_TYPE}"
    )
    responsible_relationship: str | None = Field(
        None, title=f"Responsible relationship of the {ENTITY_TYPE}"
    )
    possibility_for_objection_and_appeal: bool = Field(
        False,
        title=f"Possibility for objection and_appeal for the {ENTITY_TYPE}",
    )
    publication: bool = Field(
        False, title=f"Publication for the {ENTITY_TYPE}"
    )
    publication_text: str | None = Field(
        None, title=f"Publication text for {ENTITY_TYPE}"
    )
    bag: bool = Field(False, title=f"BAG enabled for the {ENTITY_TYPE}")
    lex_silencio_positivo: bool = Field(
        False, title=f"Lex silencio positivo enable for the {ENTITY_TYPE}"
    )
    may_postpone: bool = Field(
        False, title=f"May postpone enabled for the {ENTITY_TYPE}"
    )
    may_extend: bool = Field(
        False, title=f"May extend enabled for the {ENTITY_TYPE}"
    )
    extension_period: NonNegativeInt | None = Field(
        None, title=f"Extention period of the {ENTITY_TYPE}"
    )
    adjourn_period: NonNegativeInt | None = Field(
        None, title=f"Adjourn periond of the {ENTITY_TYPE}"
    )
    penalty_law: bool = Field(False, title=f"Penalty law of the {ENTITY_TYPE}")
    wkpb_applies: bool = Field(
        False, title=f"WKPB applies for the {ENTITY_TYPE}"
    )
    e_webform: str | None = Field(None, title=f"E-webform for {ENTITY_TYPE}")
    legal_basis: str = Field("", title=f"Legal basis for {ENTITY_TYPE}")
    local_basis: str | None = Field(
        None, title=f"Local basis for {ENTITY_TYPE}"
    )
    gdpr: Gdpr = Field(None, title=f"GDPR settings for this {ENTITY_TYPE}")


class Actions(ValueObject):
    enable_webform: bool = Field(
        False, title=f"Is webform enabled for the {ENTITY_TYPE}"
    )
    create_delayed: bool = Field(
        False, title=f"Case creation delayed enabled for the {ENTITY_TYPE}"
    )
    address_check: bool = Field(
        False, title=f"Address check enabled for the {ENTITY_TYPE}"
    )
    reuse_casedata: bool = Field(
        False, title=f"Reuse case data enabled for the {ENTITY_TYPE}"
    )
    enable_online_payment: bool = Field(
        False, title=f"Online payment enabled for the {ENTITY_TYPE}"
    )
    enable_manual_payment: bool = Field(
        False, title=f"Manual payment enabled for the {ENTITY_TYPE}"
    )
    email_required: bool = Field(
        False, title=f"Email required enabled for the {ENTITY_TYPE}"
    )
    phone_required: bool = Field(
        False, title=f"Phone required enabled for the {ENTITY_TYPE}"
    )
    mobile_required: bool = Field(
        False, title=f"Mobile required enabled for the {ENTITY_TYPE}"
    )
    disable_captcha: bool = Field(
        False, title=f"Disable captcha for the {ENTITY_TYPE}"
    )
    generate_pdf_end_webform: bool = Field(
        default=False,
        title=f"Generate PDF at end of webform for the {ENTITY_TYPE}",
    )


# pricing values may contain a maximum of 2 decimal places
PRICE_DECIMAL_PLACES: int = 2


class Price(ValueObject):
    web: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price web for the {ENTITY_TYPE}",
    )
    frontdesk: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price frontdesk for the {ENTITY_TYPE}",
    )
    phone: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price phone for the {ENTITY_TYPE}",
    )
    email: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price email for the {ENTITY_TYPE}",
    )
    assignee: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price assignee for the {ENTITY_TYPE}",
    )
    post: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price post for the {ENTITY_TYPE}",
    )
    chat: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price chat for the {ENTITY_TYPE}",
    )
    other: Decimal | None = Field(
        None,
        decimal_places=PRICE_DECIMAL_PLACES,
        title=f"Price other for the {ENTITY_TYPE}",
    )

    @validator(
        "web",
        "frontdesk",
        "phone",
        "email",
        "assignee",
        "post",
        "chat",
        "other",
        pre=True,
    )
    def convert_price_emptystring_to_none(cls, value: str):
        if value == "":
            return None
        else:
            return value


class WebForm(ValueObject):
    public_confirmation_message: str | None = Field(
        None, title=f"Public confirmation message for the {ENTITY_TYPE}"
    )
    public_confirmation_title: str | None = Field(
        None, title=f"Public confirmation title for the {ENTITY_TYPE}"
    )
    case_location_message: str | None = Field(
        None, title=f"Case location message for the {ENTITY_TYPE}"
    )
    pip_view_message: str | None = Field(
        None, title=f"Pip view message for the {ENTITY_TYPE}"
    )
    actions: Actions = Field(None, title=f"Actions for the {ENTITY_TYPE}")
    price: Price = Field(
        None, title=f"Pricing information for the {ENTITY_TYPE}"
    )


class RegistrationForm(ValueObject):
    allow_assigning_to_self: bool = Field(
        False, title=f"Allow assigning to self for the {ENTITY_TYPE}"
    )
    allow_assigning: bool = Field(
        False, title=f"Allow assigning for the {ENTITY_TYPE}"
    )
    show_confidentionality: bool = Field(
        False, title=f"Show confidentionality for the {ENTITY_TYPE}"
    )
    show_contact_details: bool = Field(
        False, title=f"Show contact details for the {ENTITY_TYPE}"
    )
    allow_add_relations: bool = Field(
        False, title=f"Allow add relations for the {ENTITY_TYPE}"
    )


class SubjectRelation(ValueObject, ABC):
    uuid: UUID = Field(..., title="Uuid of the subject relation")
    name: str = Field(..., title="Name of the subject relation")


class EmployeeRelation(SubjectRelation):
    type: Literal[SubjectType.Employee] = Field(
        "employee", title="Type of the subject relation"
    )


class PersonRelation(SubjectRelation):
    type: Literal[SubjectType.Person] = Field(
        "person", title="Type of the subject relation"
    )


class OrganizationRelation(SubjectRelation):
    type: Literal[SubjectType.Organization] = Field(
        "organization", title="Type of the subject relation"
    )


class Relations(ValueObject):
    trigger: TriggerType | None = Field(
        None, title=f"Trigger details for the {ENTITY_TYPE}"
    )
    allowed_requestor_types: list[str] | None = Field(
        None, title="List of allowed requestor types"
    )
    preset_requestor: PersonRelation | OrganizationRelation | None = Field(
        None, title=f"Preset requestor of the {ENTITY_TYPE}"
    )
    api_preset_assignee: EmployeeRelation | None = Field(
        None, title=f"Api preset requestor for the {ENTITY_TYPE}"
    )
    address_requestor_use_as_correspondence: bool = Field(
        False,
        title=f"Use address of requestor for correspondence for the {ENTITY_TYPE}",
    )
    address_requestor_use_as_case_address: bool = Field(
        False,
        title=f"Use address of requestor as case address for this {ENTITY_TYPE}",
    )
    address_requestor_show_on_map: bool = Field(
        False, title=f"Show address requestor on map for this {ENTITY_TYPE}"
    )

    def __update_trigger_default(self):
        if not self.trigger and self.allowed_requestor_types:
            set_extern = (
                "natuurlijk_persoon" in self.allowed_requestor_types
                or "natuurlijk_persoon" in self.allowed_requestor_types
                or "bedrijf" in self.allowed_requestor_types
            )
            set_intern = "medewerker" in self.allowed_requestor_types

            if set_extern and not set_intern:
                self.trigger = TriggerType.Extern
            elif not set_extern and set_intern:
                self.trigger = TriggerType.Intern
            else:
                self.trigger = TriggerType.Intern_Extern

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__update_trigger_default()


class CaseDossier(ValueObject):
    disable_pip_for_requestor: bool = Field(
        False, title=f"Disable pip for requestor for this {ENTITY_TYPE}"
    )
    lock_registration_phase: bool = Field(
        False, title=f"Lock registration phase for this {ENTITY_TYPE}"
    )
    queue_coworker_changes: bool = Field(
        False, title=f"Queue co worker changes for this {ENTITY_TYPE}"
    )
    allow_external_task_assignment: bool = Field(
        False, title=f"Allow external task assignment this {ENTITY_TYPE}"
    )
    default_document_folders: list[str] | None = Field(
        None, title=f"Default document folders for this {ENTITY_TYPE}"
    )
    default_html_email_template: str | None = Field(
        None, title=f"Default html template for this {ENTITY_TYPE}"
    )


class Notifications(ValueObject):
    external_notify_on_new_case: bool = Field(
        False,
        title=f"External notification on new case for this {ENTITY_TYPE}",
    )
    external_notify_on_new_document: bool = Field(
        False,
        title=f"External notification on new document for this {ENTITY_TYPE}",
    )
    external_notify_on_new_message: bool = Field(
        False,
        title=f"External notification on new message for this {ENTITY_TYPE}",
    )
    external_notify_on_exceed_term: bool = Field(
        False,
        title=f"External notification on exceed term for this {ENTITY_TYPE}",
    )
    external_notify_on_allocate_case: bool = Field(
        False,
        title=f"External notification on allocation of case for this {ENTITY_TYPE}",
    )
    external_notify_on_phase_transition: bool = Field(
        False,
        title=f"External notification on phase transition for this {ENTITY_TYPE}",
    )
    external_notify_on_task_change: bool = Field(
        False,
        title=f"External notification on task change for this {ENTITY_TYPE}",
    )
    external_notify_on_label_change: bool = Field(
        False,
        title=f"External notification on label change for this {ENTITY_TYPE}",
    )
    external_notify_on_subject_change: bool = Field(
        False,
        title=f"External notification on subject change for this {ENTITY_TYPE}",
    )


class ApiSettings(ValueObject):
    api_can_transition: bool = Field(
        False, title=f"Api can transition for this {ENTITY_TYPE}"
    )
    notifications: Notifications | None = Field(
        None, title=f"Api notifications for the {ENTITY_TYPE}"
    )


class Authorization(ValueObject):
    role_uuid: UUID = Field(..., title="Role uuid of this authorization entry")
    department_uuid: UUID = Field(
        ..., title="Department uuid of this authorization entry"
    )
    confidential: bool = Field(
        ..., title="Indicator if this authorization entry is confidential"
    )
    rights: list[str] = Field(
        ...,
        title="Rights configured for this authorization entry. Possible values are 'zaak_beheer', 'zaak_edit', 'zaak_read', 'zaak_search'",
    )


class ChildCaseType(ValueObject):
    uuid: UUID = Field(..., title="Uuid of the child casetype")
    name: str = Field(..., title="Name of the child casetype")


class MotherChild(ValueObject):
    enabled: bool = Field(
        ..., title="Flag that indicates if this child casetype is enabled"
    )
    casetype: ChildCaseType = Field(..., title="Child casetype")
    settings: dict = Field(
        ..., title="settings for the child casetype"
    )  # the type of the field is dict, frontend want to be flexible about what to store here.


class MotherCaseType(ValueObject):
    enabled: bool = Field(
        False, title="Flag if mother casetype settings are enabled"
    )
    child_casetypes: list[MotherChild] = Field(
        ..., title="List of child casetypes and settings"
    )


class ChangeLog(ValueObject):
    update_description: str | None = Field(
        None,
        title=f"Description of what changes have been made for {ENTITY_TYPE}",
    )
    update_components: list[str] = Field(
        ...,
        title="List of components that have been changed for {ENTITY_TYPE}",
    )


class PhaseAssignment(ValueObject):
    enabled: bool = Field(
        ..., title="Indicator if the phase assignment is enabled"
    )
    department_uuid: UUID = Field(
        ..., title="Department uuid for this phase assignment"
    )
    department_name: str = Field(
        ..., title="Department name for this phase assignment"
    )
    role_uuid: UUID = Field(..., title="Role uuid for this phase assignment")
    role_name: str = Field(..., title="Role name for this phase assignment")


class AttributeType(enum.StrEnum):
    address_v2 = "address_v2"
    appointment = "appointment"
    appointment_v2 = "appointment_v2"
    bag_adres = "bag_adres"
    bag_adressen = "bag_adressen"
    bag_openbareruimte = "bag_openbareruimte"
    bag_openbareruimtes = "bag_openbareruimtes"
    bag_straat_adres = "bag_straat_adres"
    bag_straat_adressen = "bag_straat_adressen"
    bankaccount = "bankaccount"
    calendar = "calendar"
    calendar_supersaas = "calendar_supersaas"
    checkbox = "checkbox"
    date = "date"
    email = "email"
    file = "file"
    geojson = "geojson"
    geolatlon = "geolatlon"
    googlemaps = "googlemaps"
    group = "group"
    image_from_url = "image_from_url"
    numeric = "numeric"
    option = "option"
    relationship = "relationship"
    richtext = "richtext"
    select = "select"
    subject = "subject"
    text = "text"
    text_uc = "text_uc"
    textarea = "textarea"
    textblock = "textblock"
    url = "url"
    valuta = "valuta"
    valutaex = "valutaex"
    valutaex21 = "valutaex21"
    valutaex6 = "valutaex6"
    valutain = "valutain"
    valutain21 = "valutain21"
    valutain6 = "valutain6"


class AttributeBase(ABC, ValueObject):
    is_group: bool = Field(..., title="Indicator if this is a group attribute")
    title: str | None = Field(..., title="Title for the attribute")
    help_intern: str | None = Field(
        ..., title="Internal help text for the attribute"
    )
    publish_pip: bool = Field(
        ..., title="Indicator if this attribute can be published on pip"
    )


# Testblock is not a real attribute, but is is listed in the phases attributes
class TextBlock(AttributeBase):
    attribute_type: Literal[AttributeType.textblock]


# Group is not a real attribute, but is is listed in the phases attributes
class Group(AttributeBase):
    attribute_type: Literal[AttributeType.group]


class AttributePermission(ValueObject):
    department_uuid: UUID = Field(
        ..., title="Department uuid for the attribute permission"
    )
    department_name: str = Field(
        ..., title="Department name for the attribute permission"
    )
    role_uuid: UUID = Field(
        ..., title="Role uuid for the attribute permission"
    )
    role_name: str = Field(..., title="Role name for the attribute permission")


# This Attribute class is the base class for al "real" attributes
class Attribute(AttributeBase):
    uuid: UUID = Field(..., title="UUID for the attribute")
    referential: bool = Field(
        ..., title="Indicator if the attribute is referential"
    )
    mandatory: bool = Field(
        ..., title="Indicator if the attribute is mandatory"
    )
    system_attribute: bool = Field(
        ..., title="Inidcator if this is a system attribute"
    )
    help_extern: str | None = Field(
        ..., title="External help text for the attribute"
    )
    attribute_name: str = Field(..., title="Attribute name")
    attribute_magic_string: str | None = Field(
        None, title="Attribute magic string"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    pip_can_change: bool = Field(
        ..., title="Indicator if requestor can change attribute value on PIP"
    )
    permissions: list[AttributePermission] | None = Field(
        ..., title="Permissions for the attribute"
    )
    skip_change_approval: bool = Field(
        ..., title="Indicator if change approval is needed"
    )


class AdresV2Attribute(Attribute):
    attribute_type: Literal[AttributeType.address_v2]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )
    show_on_map: bool = Field(..., title="Show attribute on map")
    map_wms_layer_id: str | None = Field(
        None, title="WMS layer id for this attribute"
    )
    map_wms_feature_attribute_label: str | None = Field(
        None, title="WMS feature attribute label for this attribute"
    )
    map_wms_feature_attribute_id: str | None = Field(
        None, title="WMS feature attribute id for this attribute"
    )


class AppointmentAttribute(Attribute):
    attribute_type: Literal[AttributeType.appointment]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class AppointmentV2Attribute(Attribute):
    attribute_type: Literal[AttributeType.appointment_v2]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class BankaccountAttribute(Attribute):
    attribute_type: Literal[AttributeType.bankaccount]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class CalendarAttribute(Attribute):
    attribute_type: Literal[AttributeType.calendar]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class CalendarSuperSaasAttribute(Attribute):
    attribute_type: Literal[AttributeType.calendar_supersaas]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class CheckboxSuperSaasAttribute(Attribute):
    attribute_type: Literal[AttributeType.checkbox]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class DateAttributeLimitation(ValueObject):
    value: int = Field(..., title="Value for date limitation")
    active: bool = Field(..., title="Active indicator for date limitation")
    term: str = Field(
        ..., title="Unit for date limitation [days | weeks | months | years]"
    )
    during: str = Field(
        ..., title="During for the date limitation [pre | post]"
    )
    reference: UUID | Literal["currentDate"] = Field(
        ...,
        title="Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute",
    )


class DateAttribute(Attribute):
    attribute_type: Literal[AttributeType.date]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    start_date_limitation: DateAttributeLimitation | None = Field(
        None, title="Start date limitation for date attribute"
    )
    end_date_limitation: DateAttributeLimitation | None = Field(
        None, title="End date limitation for date attribute"
    )


class EmailAttribute(Attribute):
    attribute_type: Literal[AttributeType.email]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class FileAttribute(Attribute):
    attribute_type: Literal[AttributeType.file]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class GeoJsonAttribute(Attribute):
    attribute_type: Literal[AttributeType.geojson]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ImageFromUrlAttribute(Attribute):
    attribute_type: Literal[AttributeType.image_from_url]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class NumericAttribute(Attribute):
    attribute_type: Literal[AttributeType.numeric]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class OptionAttribute(Attribute):
    attribute_type: Literal[AttributeType.option]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class RelationAttribute(Attribute, ABC):
    attribute_type: Literal[AttributeType.relationship]
    show_on_map: bool = Field(..., title="Show attribute on map")


class SubjectRelationAttribute(RelationAttribute):
    relationship_type: Literal["subject"] = Field(
        "subject", title="Indicator for the relationship type"
    )
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    relationship_subject_role: str | None = Field(
        None, title="Selected role for subject relation"
    )


class CustomObjectRelationAttribute(RelationAttribute):
    relationship_type: Literal["custom_object"] = Field(
        "custom_object", title="Indicator for the relationship type"
    )
    create_custom_object_enabled: bool = Field(
        ..., title="Is object creation enabled"
    )
    create_custom_object_label: str | None = Field(
        None, title="Label for object creation"
    )
    create_custom_object_attribute_mapping: dict | None = Field(
        None, title="Attribute mapping for object creation"
    )


class CaseRelationAttribute(RelationAttribute):
    relationship_type: Literal["case"] = Field(
        "case",
        title="Indicator for the relationship type. This case relation type is not supported anymore.",
    )


class RichtextAttribute(Attribute):
    attribute_type: Literal[AttributeType.richtext]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class SelectAttribute(Attribute):
    attribute_type: Literal[AttributeType.select]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class SubjectAttribute(Attribute):
    attribute_type: Literal[AttributeType.subject]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class TextAttribute(Attribute):
    attribute_type: Literal[AttributeType.text]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class TextUcAttribute(Attribute):
    attribute_type: Literal[AttributeType.text_uc]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class TextareaAttribute(Attribute):
    attribute_type: Literal[AttributeType.textarea]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class UrlAttribute(Attribute):
    attribute_type: Literal[AttributeType.url]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaAttribute(Attribute):
    attribute_type: Literal[AttributeType.valuta]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaExAttribute(Attribute):
    attribute_type: Literal[AttributeType.valutaex]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaEx21Attribute(Attribute):
    attribute_type: Literal[AttributeType.valutaex21]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaEx6Attribute(Attribute):
    attribute_type: Literal[AttributeType.valutaex6]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaInAttribute(Attribute):
    attribute_type: Literal[AttributeType.valutain]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaIn21Attribute(Attribute):
    attribute_type: Literal[AttributeType.valutain21]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class ValutaIn6Attribute(Attribute):
    attribute_type: Literal[AttributeType.valutain6]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )


class GoogleMapsAttribute(Attribute):
    attribute_type: Literal[AttributeType.googlemaps]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )
    show_on_map: bool = Field(..., title="Show attribute on map")


class AdresByStreetnameAttribute(Attribute):
    attribute_type: Literal[AttributeType.bag_straat_adres]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )


class AdressesByStreetnameAttribute(Attribute):
    attribute_type: Literal[AttributeType.bag_straat_adressen]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )


class AdresByZipcodeAttribute(Attribute):
    attribute_type: Literal[AttributeType.bag_adres]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )


class AdressesByZipcodeAttribute(Attribute):
    attribute_type: Literal[AttributeType.bag_adressen]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )


class OpenbareruimteAttribute(Attribute):
    attribute_type: Literal[AttributeType.bag_openbareruimte]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )


class OpenbareruimtesAttribute(Attribute):
    attribute_type: Literal[AttributeType.bag_openbareruimtes]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    use_as_case_address: bool = Field(
        False, title="Use this attribute as case address"
    )


class LocationWithMapAttribute(Attribute):
    attribute_type: Literal[AttributeType.geolatlon]
    is_multiple: bool | None = Field(
        None, title="Indicator if multiple values is configured"
    )  # this is optional, because this attribute is not required when posting a versioned_casetype
    label_multiple: str | None = Field(
        None, title="Label for allowing multiple values"
    )
    map_case_location: bool = Field(
        ..., title="Indicator if attribute must be used as case location"
    )
    map_wms_layer_id: str | None = Field(
        None, title="WMS layer id for this attribute"
    )
    map_wms_feature_attribute_label: str | None = Field(
        None, title="WMS feature attribute label for this attribute"
    )
    map_wms_feature_attribute_id: UUID | None = Field(
        None, title="WMS feature attribute uuid for this attribute"
    )


class Result(ValueObject):
    name: str = Field(None, title="Name of the result")
    result_type: ResultTypes = Field(..., title="Type of result")
    is_default: bool = Field(False, title="Is default result")
    selection_list: str | None = Field(default=None, title="Selection list")
    selection_list_number: str | None = Field(
        default=None, title="Selection list number"
    )
    selection_list_source_date: date | None = Field(
        default=None, title="Selection list source date"
    )
    selection_list_end_date: date | None = Field(
        default=None, title="Selection list end date"
    )
    archival_trigger: bool = Field(
        default=None, title="Achival trigger is activated"
    )
    archival_nomination: ArchivalNomination = Field(
        default=..., title="Archive nomination"
    )
    retention_period: int | None = Field(
        default=None, title="Retention period in days."
    )
    archival_nomination_valuation: ArchivalNominationValuation | None = Field(
        default=None, title="Arhive nomination valuation"
    )
    comments: str | None = Field(default="", title="Comments for this result")
    processtype_name: str | None = Field("", title="Processtype name")
    processtype_number: str | None = Field(
        default="", title="Processtype number"
    )
    processtype_description: str | None = Field(
        default="", title="Processtype description"
    )
    processtype_explanation: str | None = Field(
        default="", title="Processtype explanation"
    )
    processtype_object: str | None = Field(
        default="", title="Processtype object"
    )
    procestype_generic: Literal["Generiek"] | Literal["Specifiek"] | None = (
        Field(default=None, title="Procestype generic")
    )
    origin: ResultOrigin | None = Field(
        default=None, title="Origin for this result"
    )
    process_period: ProcessPeriod | None = Field(
        default=None, title="Process period for this result"
    )


class Template(ValueObject):
    catalog_template_uuid: UUID = Field(
        default=None, title="UUID of the document_tempate from the catalog"
    )
    catalog_template_name: str = Field(
        default=None, title="Name of the document template from the catalog"
    )
    label: str = Field(default=None, title="Label of the template")
    description: str = Field(default=None, title="Description of the template")
    auto_generate: bool = Field(
        default=False,
        title="Autogenerate the template document on phase transition",
    )
    allow_edit: bool = Field(default=False, title="Allow editing the filename")
    custom_filename: str = Field(
        default=None, title="Filename for the generated document"
    )
    target_format: Literal["odt"] | Literal["pdf"] | Literal["docx"] = Field(
        default="odt", title="Target format of the template"
    )
    document_attribute_uuid: UUID | None = Field(
        default=None,
        title="UUID of the attribute where the generated document will be added.",
    )
    document_attribute_name: str | None = Field(
        default=None,
        title="Name of the attribute where the generated document will be added.",
    )


class BaseMessage(ValueObject):
    receiver: (
        Literal["aanvrager"]
        | Literal["zaak_behandelaar"]
        | Literal["coordinator"]
        | Literal["gemachtigde"]
    )
    catalog_message_uuid: UUID
    catalog_message_label: str
    cc: str
    bcc: str
    send_automatic: str


class Overig(BaseMessage):
    receiver: Literal["overig"]
    to: str


class Betrokkene(BaseMessage):
    receiver: Literal["betrokkene"]
    person_involved_role: str


class Behandelaar(BaseMessage):
    receiver: Literal["behandelaar"]
    subject: EmployeeRelation


class Case(ValueObject):
    related_casetype: UUID = Field(..., title="UUID of the case")
    # aanvragen
    requestor_type: (
        Literal["aanvrager"]
        | Literal["anders"]
        | Literal["behandelaar"]
        | Literal["betrokkene"]
        | Literal["ontvanger"]
        | Literal["deelzaak"]
        | Literal["gerelateerd"]
    ) = Field(..., title="Type or requestor for creating the case")
    requestor: PersonRelation | OrganizationRelation | None = Field(
        None, title="Requestor of the case"
    )
    kind: (
        Literal["deelzaak"]
        | Literal["gerelateerd"]
        | Literal["vervolgzaak_datum"]
        | Literal["vervolgzaak"]
    ) = Field(..., title="Kind of case creation")
    start_after_fixeddate: date | None = Field(
        ..., title="Fixed date when creating the case"
    )
    start_after: int | str | None = Field(
        ...,
        title="Start create case after (can be a integer specifying number of dates, or a magic_string)",
    )
    automatic: bool = Field(..., title="Start case at finish of phase")
    # behandelen
    allocation: AttributePermission | None = Field(
        ..., title="Department and role settings for allocation"
    )
    handle_in_phase: int | None = Field(
        ...,
        title="Phase number to handle this case. Start counting with phase number 1",
    )
    assign_automatic: bool = Field(..., title="Automatically assign case")
    # kopieeren
    copy_subject_roles_enabled: bool = Field(
        ..., title="Copy involved subjects"
    )
    copy_subject_roles: list[str] | None = Field(
        None, title="Copy subjects with role"
    )
    copy_all_attributes: bool = Field(..., title="Copy all attributes")
    copy_attributes_mapping: dict[str, UUID] | None = Field(
        None,
        title="Key, value pair of attribute mapping by uuid. {source : destination}",
    )
    copy_case_relations: bool = Field(..., title="Copy case relations")
    copy_object_relations: bool = Field(..., title="Copy object relations")
    # overig
    show_on_pip: bool = Field(..., title="Show on pip")
    pip_label: str | None = Field(None, title="Pip lable")
    add_subjects_involved: bool = Field(..., title="Add subjects")
    subjects_involved_authorized_for_case: bool = Field(
        ..., title="Authorize subjects involved"
    )
    subjects_involved_email_notifcation: bool = Field(
        ..., title="Send email to subjects authorized"
    )
    subjects_involved_role: str | None = Field(None, title="Role for subject")
    subject_involved: PersonRelation | OrganizationRelation | None = Field(
        None, title="Subject involved"
    )
    subject_involved_magic_string: str | None = Field(
        None, title="Magic_string prefix for involved subject"
    )
    creation_style: (
        Literal["relaties_creation_style_form_option"] | Literal["background"]
    ) = Field(..., title="Creation style")
    owner_role: str | None = Field(default=None, title="Role for owner")


class InvolvedSubject(ValueObject):
    involved_subject: PersonRelation | OrganizationRelation = Field(
        None, title="Involved subject"
    )
    role: str = Field(..., title="Involved subject role")
    magic_string: str = Field(..., title="Involved subject magic_string")
    authorized_for_case: bool = Field(
        False, title="Indicator if the subject is authorized for the case"
    )
    notify_by_email: bool = Field(
        False, title="Indicator if the subject must be notified by email"
    )


class Phase(ValueObject):
    milestone_name: str = Field(..., title="Name of the phase milestone")
    phase_name: str = Field(..., title="Name of the phase")
    term_in_days: int = Field(..., title="Term in days for milestone")
    created: datetime | None = Field(
        None, title="Create datetime of the phase"
    )
    last_modified: datetime | None = Field(
        None, title="Last modified datetime of the phase"
    )
    assignment: PhaseAssignment | None = Field(
        None, title="Assignment settings for the phase"
    )
    templates: list[Template] | None = Field(
        default=None, title="Templates for this phase."
    )
    email_templates: (
        list[BaseMessage | Overig | Betrokkene | Behandelaar] | None
    ) = Field(default=[], title="Email templates for this phase")
    tasks: list[str] | None = Field(
        default=[], title="Configured tasks for the phase"
    )
    cases: list[Case] | None = Field(
        default=None, title="Cases for this phase"
    )
    involved_subjects: list[InvolvedSubject] | None = Field(
        default=None, title="List of involved subjects for this phase"
    )
    custom_fields: (
        list[
            AdresByStreetnameAttribute
            | AdresByZipcodeAttribute
            | AdressesByStreetnameAttribute
            | AdressesByZipcodeAttribute
            | AdresV2Attribute
            | AppointmentAttribute
            | AppointmentV2Attribute
            | BankaccountAttribute
            | CalendarAttribute
            | CalendarSuperSaasAttribute
            | CheckboxSuperSaasAttribute
            | DateAttribute
            | EmailAttribute
            | FileAttribute
            | GeoJsonAttribute
            | GoogleMapsAttribute
            | Group
            | ImageFromUrlAttribute
            | LocationWithMapAttribute
            | NumericAttribute
            | OpenbareruimteAttribute
            | OpenbareruimtesAttribute
            | OptionAttribute
            | RichtextAttribute
            | SelectAttribute
            | SubjectAttribute
            | TextareaAttribute
            | TextAttribute
            | TextBlock
            | TextUcAttribute
            | UrlAttribute
            | ValutaAttribute
            | ValutaEx21Attribute
            | ValutaEx6Attribute
            | ValutaExAttribute
            | ValutaIn21Attribute
            | ValutaIn6Attribute
            | ValutaInAttribute
            | SubjectRelationAttribute
            | CustomObjectRelationAttribute
            | CaseRelationAttribute
        ]
        | None
    ) = Field([], title="Custom fields for the phase")

    @validator("cases")
    def check_for_duplicate_casetypes(cls, cases: list[Case]):
        duplicates = []
        for case in cases or []:
            if case.related_casetype in duplicates:
                raise ValueError(
                    f"A casetype can only be added once per phase. Duplicate casetypes found with uuid {case.related_casetype}."
                )
            else:
                duplicates.append(case.related_casetype)

        return cases


class VersionedCaseType(Entity):
    entity_type = ENTITY_TYPE
    entity_id__fields: list[str] = []
    uuid: UUID = Field(..., title=f"Internal identifier of the {entity_type}")
    casetype_uuid: UUID = Field(..., title="UUID of the casetype")
    active: bool = Field(..., title="Indicator if the casetype is active")
    catalog_folder: CatalogFolder | None = Field(
        ..., title=f"Calatog folder for the {entity_type}"
    )
    general_attributes: GeneralAttributes = Field(
        ...,
        title=f"General attributes for the {entity_type}",
    )
    documentation: Documentation = Field(
        ...,
        title=f"Documentation for the {entity_type}",
    )
    relations: Relations = Field(..., title=f"Relations for the {entity_type}")
    webform: WebForm = Field(
        ..., title=f"WebForm attributes for the {entity_type}"
    )
    registrationform: RegistrationForm = Field(
        ..., title=f"RegistrationForm attributes for the {entity_type}"
    )
    case_dossier: CaseDossier = Field(
        ..., title=f"Case dossier inforation for the {entity_type}"
    )
    api: ApiSettings = Field(..., title=f"Api settings for the {entity_type}")
    authorization: list[Authorization] = Field(
        ..., title=f"Authorization settings for the {entity_type}"
    )
    child_casetype_settings: MotherCaseType | None = Field(
        ..., title=f"Mother casetype settings for the {entity_type}"
    )
    change_log: ChangeLog | None = Field(
        ..., title=f"Describing the changes for the {entity_type}"
    )
    phases: list[Phase] = Field(
        ..., title=f"Phases for this {entity_type}", min_items=2
    )
    results: list[Result] | None = Field(
        default=None, title=f"Results for the {entity_type}"
    )

    @classmethod
    @Entity.event(name="VersionedCasetypeCreated", fire_always=True)
    def create(cls, **kwargs) -> Self:
        obj = cls(**kwargs)
        return obj

    @classmethod
    @Entity.event(name="VersionedCasetypeUpdated", fire_always=True)
    def update(cls, **kwargs) -> Self:
        obj = cls(**kwargs)
        return obj

    @validator("phases")
    def tasks_not_in_first_phase(
        cls, phases: list[Phase] | None
    ) -> list[Phase] | None:
        if not phases:
            return phases

        if phases[0].tasks is not None and len(phases[0].tasks) > 0:
            raise ValueError("Tasks are not allowed in the first phase")

        return phases

    @validator("phases")
    def cases_kind_vervolgzaak_only_in_last_phase(cls, phases: list[Phase]):
        ### check vervolgzaak and vervolgzaak_datum in cases.kind is only allowed in last phase ###
        for phase in phases[:-1]:
            for case in phase.cases or []:
                if case.kind not in ["deelzaak", "gerelateerd"]:
                    raise ValueError(
                        f"The value {case.kind} in attribute cases.kind is only allowed in the last phase."
                    )

        return phases
