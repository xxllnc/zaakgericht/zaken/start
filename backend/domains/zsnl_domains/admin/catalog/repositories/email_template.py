# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from .. import entities
from minty.exceptions import Conflict, NotFound
from sqlalchemy import sql
from sqlalchemy.exc import IntegrityError
from uuid import UUID
from zsnl_domains.database import schema


class EmailTemplateRepository(ZaaksysteemRepositoryBase):
    mapping = {
        "category_uuid": "bibliotheek_categorie_id",
        "label": "label",
        "subject": "subject",
        "message": "message",
        "sender": "sender",
        "sender_address": "sender_address",
        "attachments": "attachments",
        "commit_message": None,
        "deleted": "deleted",
    }

    def get_email_template_details_by_uuid(
        self, uuid: UUID
    ) -> entities.EmailTemplate:
        """Get detailed information about an email template by uuid.

        :param DatabaseRepositoryBase.
        :type DatabaseRepositoryBase: DatabaseRepositoryBase
        :param uuid: UUID of the email template
        :type uuid: UUID
        :raises NotFound: When email template with uuid not found
        :return: EmailTemplate entity
        :rtype: EmailTemplate
        """

        query = (
            sql.select(
                schema.BibliotheekNotificatie.uuid,
                schema.BibliotheekNotificatie.id,
                schema.BibliotheekNotificatie.label,
                schema.BibliotheekNotificatie.sender,
                schema.BibliotheekNotificatie.sender_address,
                schema.BibliotheekNotificatie.message,
                schema.BibliotheekNotificatie.subject,
                schema.BibliotheekCategorie.uuid.label("category_uuid"),
            )
            .select_from(
                sql.join(
                    schema.BibliotheekNotificatie,
                    schema.BibliotheekCategorie,
                    schema.BibliotheekNotificatie.bibliotheek_categorie_id
                    == schema.BibliotheekCategorie.id,
                    isouter=True,
                )
            )
            .where(
                sql.and_(
                    schema.BibliotheekNotificatie.uuid == uuid,
                    schema.BibliotheekNotificatie.deleted.is_(None),
                )
            )
        )

        query_result = self.session.execute(query).fetchone()

        if query_result is None:
            raise NotFound(
                f"Email template with uuid {uuid} not found",
                "email_template/not_found",
            )

        attachments = self._get_email_template_attachments(
            email_template_id=query_result.id
        )

        return self._transform_to_entity(
            query_result=query_result,
            attachments=attachments,
        )

    def _transform_to_entity(
        self,
        query_result: list,
        attachments: list,
    ):
        """Transform an email_template object to an EmailTemplate Entity.

        :param email_template: Email_template object
        :type email_template: BibliotheekNotificatie
        :param bibliotheek_categorie: Parent category of email_template
        :type email_template: BibliotheekCategorie
        :param attachments: Documents attached to an email_template
        :type email_template: list
        :return: EmailTemplate Entity
        :rtype: EmailTemplate
        """

        entity = entities.EmailTemplate(
            uuid=query_result.uuid,
            id=query_result.id,
            label=query_result.label,
            sender=query_result.sender,
            sender_address=query_result.sender_address,
            subject=query_result.subject,
            message=query_result.message,
            category_uuid=getattr(query_result, "category_uuid", None),
            attachments=attachments,
        )
        entity.event_service = self.event_service
        return entity

    def _get_email_template_attachments(self, email_template_id: int):
        """Get the list of documents attached to an email_template .

        :param email_template_id: Id of the email_template.
        :type email_template_id: int
        :return: list of documents attached
        :rtype: list
        """
        attachments = self.session.execute(
            sql.select(
                schema.BibliotheekKenmerk.uuid,
                schema.BibliotheekKenmerk.naam,
            ).where(
                sql.and_(
                    schema.BibliotheekNotificatieKenmerk.bibliotheek_notificatie_id
                    == email_template_id,
                    schema.BibliotheekKenmerk.id
                    == schema.BibliotheekNotificatieKenmerk.bibliotheek_kenmerken_id,
                )
            )
        ).fetchall()

        return [
            {"name": each.naam, "uuid": str(each.uuid)} for each in attachments
        ]

    def create_new_email_template(self, uuid: UUID, fields: dict):
        emailtemplate_entity = entities.EmailTemplate(
            uuid=uuid,
            id=None,
            label=None,
            sender=None,
            sender_address=None,
            subject=None,
            message=None,
            category_uuid=None,
            attachments=[],
        )
        emailtemplate_entity.event_service = self.event_service
        emailtemplate_entity.create(fields=fields)
        return emailtemplate_entity

    def delete_email_template(self, uuid: UUID, reason: str):
        email_template = entities.EmailTemplate(
            uuid=uuid,
            id=None,
            label=None,
            sender=None,
            sender_address=None,
            subject=None,
            message=None,
            category_uuid=None,
            attachments=[],
        )
        email_template.event_service = self.event_service
        email_template.delete(reason=reason)
        return email_template

    def save(self):
        for event in self.event_service.event_list:
            if event.entity_type == "EmailTemplate":
                if event.event_name == "EmailTemplateEdited":
                    self._edit_email_template(event)
                elif event.event_name == "EmailTemplateCreated":
                    self._insert_email_template(event)
                elif event.event_name == "EmailTemplateDeleted":
                    self._delete_email_template(event)

    def _generate_database_values(self, event):
        values = {"uuid": event.entity_id}
        for change in event.changes:
            db_fieldname = self.mapping[change["key"]]
            if db_fieldname == "attachments":
                self._handle_attachments(change, event.entity_id)
            elif db_fieldname == "bibliotheek_categorie_id":
                stmt = (
                    sql.select(schema.BibliotheekCategorie.id)
                    .where(
                        schema.BibliotheekCategorie.uuid == change["new_value"]
                    )
                    .scalar_subquery()
                )
                values[db_fieldname] = stmt
            elif db_fieldname is None:
                pass
            else:
                values[db_fieldname] = change["new_value"]

        values["search_term"] = self._prepare_search_term(
            values=values, entity_id=event.entity_id
        )

        return values

    def _prepare_search_term(self, values: dict, entity_id: UUID):
        search_term_items = ["label", "subject", "message"]
        search_term = [
            values[item]
            for item in search_term_items
            if values.get(item, False)
        ]
        search_term.append(str(entity_id))

        return " ".join(search_term)

    def _insert_email_template(self, event):
        with self.session.no_autoflush:
            insert_values = self._generate_database_values(event=event)
            insert_stmt = sql.insert(schema.BibliotheekNotificatie).values(
                **insert_values
            )

            try:
                self.session.execute(insert_stmt)
            except IntegrityError as err:
                uuid = event.entity_id
                raise Conflict(
                    f"Email template with uuid {uuid} already exists",
                    "email_template/already_exists_with_uuid",
                ) from err

    def _edit_email_template(self, event):
        email_template_uuid = event.entity_id
        update_values = self._generate_database_values(event=event)

        update_stmt = (
            sql.update(schema.BibliotheekNotificatie)
            .where(schema.BibliotheekNotificatie.uuid == email_template_uuid)
            .values(**update_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_stmt)
        self._regenerate_search_term(email_template_uuid)

    def get_email_template_info_by_uuid(self, email_template_uuid):
        email_template = self.session.execute(
            sql.select(
                schema.BibliotheekNotificatie.label,
                schema.BibliotheekNotificatie.message,
                schema.BibliotheekNotificatie.subject,
            ).where(schema.BibliotheekNotificatie.uuid == email_template_uuid)
        ).fetchone()

        if not email_template:
            raise NotFound(
                f"Email template with uuid '{email_template_uuid}' not found.",
                "email_template/not_found",
            )

        return email_template

    def _regenerate_search_term(self, email_template_uuid: UUID):
        email_template = self.get_email_template_info_by_uuid(
            email_template_uuid=email_template_uuid
        )
        updated_values = {
            "label": email_template.label,
            "message": email_template.message,
            "subject": email_template.subject,
        }
        search_term = self._prepare_search_term(
            values=updated_values, entity_id=email_template_uuid
        )

        update_stmt = (
            sql.update(schema.BibliotheekNotificatie)
            .where(schema.BibliotheekNotificatie.uuid == email_template_uuid)
            .values(search_term=search_term)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_stmt)

    def _delete_email_template(self, event):
        if self._get_case_types_related_to_email_template(
            uuid=event.entity_id
        ):
            raise Conflict(
                "Email template is used in case types",
                "email_template/used_in_case_types",
            )
        email_template_uuid = event.entity_id
        delete_values = self._generate_database_values(event)
        delete_values["bibliotheek_categorie_id"] = None
        delete_stmt = (
            sql.update(schema.BibliotheekNotificatie)
            .where(schema.BibliotheekNotificatie.uuid == email_template_uuid)
            .values(**delete_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(delete_stmt)

    def _handle_attachments(self, change: dict, entity_uuid: UUID):
        old_attachments = {a["uuid"] for a in change["old_value"]}
        new_attachments = {a["uuid"] for a in change["new_value"]}

        attachments_to_delete = old_attachments - new_attachments
        attachments_to_insert = new_attachments - old_attachments

        if attachments_to_insert:
            self._insert_attachments(
                uuids_to_insert=attachments_to_insert, entity_uuid=entity_uuid
            )
        if attachments_to_delete:
            self._delete_attachments(
                uuids_to_delete=attachments_to_delete, entity_uuid=entity_uuid
            )

    def _insert_attachments(self, uuids_to_insert: list, entity_uuid: UUID):
        rows_to_insert = []
        for uuid in uuids_to_insert:
            insert_stmt = schema.BibliotheekNotificatieKenmerk(
                bibliotheek_notificatie_id=sql.select(
                    schema.BibliotheekNotificatie.id
                ).where(schema.BibliotheekNotificatie.uuid == entity_uuid),
                bibliotheek_kenmerken_id=sql.select(
                    schema.BibliotheekKenmerk.id
                ).where(schema.BibliotheekKenmerk.uuid == uuid),
            )
            rows_to_insert.append(insert_stmt)

        self.session.add_all(rows_to_insert)

    def _delete_attachments(self, uuids_to_delete: list, entity_uuid: UUID):
        delete_stmt = (
            sql.delete(schema.BibliotheekNotificatieKenmerk)
            .where(
                sql.and_(
                    schema.BibliotheekNotificatieKenmerk.bibliotheek_notificatie_id
                    == sql.select(schema.BibliotheekNotificatie.id)
                    .where(schema.BibliotheekNotificatie.uuid == entity_uuid)
                    .scalar_subquery(),
                    schema.BibliotheekNotificatieKenmerk.bibliotheek_kenmerken_id.in_(
                        sql.select(schema.BibliotheekKenmerk.id)
                        .where(
                            schema.BibliotheekKenmerk.uuid.in_(uuids_to_delete)
                        )
                        .scalar_subquery()
                    ),
                )
            )
            .execution_options(synchronize_session=False)
        )

        self.session.execute(delete_stmt)

    def _get_case_types_related_to_email_template(self, uuid: UUID):
        """Get the count of case_types related to an email_template.

        :param email_template_uuid: email_template uuid
        :type email_template_uuid: UUID
        :return: Count of case_types related to an email_template
        :rtype: int
        """
        email_template_id = (
            sql.select(schema.BibliotheekNotificatie.id)
            .where(schema.BibliotheekNotificatie.uuid == uuid)
            .scalar_subquery()
        )

        zaaktype_joined = sql.join(
            schema.ZaaktypeNotificatie,
            schema.ZaaktypeNode,
            schema.ZaaktypeNotificatie.zaaktype_node_id
            == schema.ZaaktypeNode.id,
        ).join(
            schema.Zaaktype,
            schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
        )

        case_types_qry_stmt = (
            sql.select(sql.func.count())
            .select_from(zaaktype_joined)
            .where(
                sql.and_(
                    schema.Zaaktype.deleted.is_(None),
                    schema.ZaaktypeNotificatie.bibliotheek_notificatie_id
                    == email_template_id,
                )
            )
        )
        related_case_types = self.session.execute(
            case_types_qry_stmt
        ).fetchone()
        return related_case_types[0]
