# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from ..entities.object_type import ObjectType
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import and_, delete
from uuid import UUID
from zsnl_domains.database import schema


class ObjectTypeRepository(ZaaksysteemRepositoryBase):
    object_type_mapping = {"id": "id", "name": "name", "deleted": "deleted"}

    def get_object_type_by_uuid(self, uuid: UUID):
        """Get object_type by uuid.

        :param uuid: object_type uuid
        :type uuid: UUID
        """
        try:
            query_result = (
                self.session.query(schema.ObjectBibliotheekEntry)
                .filter(
                    and_(
                        schema.ObjectData.uuid == uuid,
                        schema.ObjectData.uuid
                        == schema.ObjectBibliotheekEntry.object_uuid,
                        schema.ObjectData.object_class == "type",
                    )
                )
                .one()
            )
        except NoResultFound as err:
            raise NotFound(
                f"Object type with uuid: '{uuid}' not found.",
                "object_type/not_found",
            ) from err

        object_type = ObjectType(
            uuid=query_result.object_uuid, name=query_result.name
        )
        return object_type

    def delete_object_type(self, uuid: UUID, reason: str):
        """Delete an object type.

        :param uuid: uuid of object type
        :type uuid: UUID
        :param reason: reason for deleting an object_type
        :type reason: str
        :return: ObjectType
        :rtype: ObjectType
        """
        object_type = self.get_object_type_by_uuid(uuid=uuid)
        object_type.event_service = self.event_service
        object_type.delete(reason=reason)
        return object_type

    def save(self):
        """Save changes on object type entity back to database.

        :param case_type: object type
        :type case_type: entities.ObjectType
        """
        for event in self.event_service.event_list:
            if event.entity_type == "ObjectType":
                if event.event_name == "ObjectTypeDeleted":
                    self._delete_object_type(event)

    def _delete_object_type(self, event):
        uuid = event.entity_id
        delete_stmt = delete(schema.ObjectData).where(
            and_(schema.ObjectData.uuid == uuid)
        )
        try:
            self.session.execute(delete_stmt)
        except IntegrityError as err:
            raise Conflict(
                "Object type is used in case type",
                "object_type/used_in_case_types",
            ) from err
