# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy.sql import and_, select
from zsnl_domains.database import schema


def attribute_search_query(search_string: str, attr_type: str):
    """Search Attributes by name and filter by attribute type query.

    :param search_string: search string
    :type search_string: str
    :param attr_type: attribute type
    :type attr_type: str
    :return: search statement
    :rtype: sqlalchemy object
    """
    stmt = select(
        schema.BibliotheekKenmerk.uuid,
        schema.BibliotheekKenmerk.id,
        schema.BibliotheekKenmerk.naam.label("name"),
        schema.BibliotheekKenmerk.magic_string,
        schema.BibliotheekKenmerk.value_type,
        schema.BibliotheekKenmerk.type_multiple,
        schema.BibliotheekKenmerk.relationship_type,
    )
    stmt = stmt.where(
        and_(
            schema.BibliotheekKenmerk.naam.ilike(f"%{search_string}%"),
            schema.BibliotheekKenmerk.deleted.is_(None),
        )
    )
    if attr_type:
        stmt = stmt.where(schema.BibliotheekKenmerk.value_type == attr_type)
    return stmt
