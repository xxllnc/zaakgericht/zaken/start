# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from minty.exceptions import Conflict
from sqlalchemy import sql
from zsnl_domains.database import schema


class AssertZaaktypeUniqueCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        row = self.session.execute(
            sql.select(schema.Zaaktype.uuid).where(
                schema.Zaaktype.uuid == changes.get("casetype_uuid")
            )
        ).fetchone()
        if row:
            raise Conflict(
                f"Casetype with uuid {changes.get('casetype_uuid')} already exists"
            )

        row = self.session.execute(
            sql.select(
                schema.ZaaktypeNode.uuid,
                schema.ZaaktypeNode.titel,
            )
            .select_from(
                sql.join(
                    schema.ZaaktypeNode,
                    schema.Zaaktype,
                    schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
                )
            )
            .where(
                sql.and_(
                    False
                    if not changes.get("general_attributes")
                    else schema.ZaaktypeNode.titel
                    == getattr_from_dict(changes, "general_attributes.name"),
                    schema.Zaaktype.uuid != changes.get("casetype_uuid"),
                )
            )
            .limit(1)
        ).fetchone()

        if row:
            raise Conflict(
                f"Casetype not unique. Castype node with uuid {row.uuid} has duplicate name"
            )
