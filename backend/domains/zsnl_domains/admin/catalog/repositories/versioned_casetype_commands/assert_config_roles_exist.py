# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.constants import BASE_RELATION_ROLES
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from zsnl_domains.database import schema


class AssertConfigRolesExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        """Check for changes if all config roles exist in the db
        If one of the config roles does not exist, raise an exception
        """

        supplied_config_roles: set = set()
        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            for involved_subject in getattr_from_dict(
                phase, "involved_subjects", default_val=[]
            ):
                supplied_config_roles.add(
                    getattr_from_dict(involved_subject, "role")
                )

        # remove None and BASE_RELATION_ROLES value from set
        supplied_config_roles = {
            r for r in supplied_config_roles if r is not None
        }
        supplied_config_roles = {
            r for r in supplied_config_roles if r not in BASE_RELATION_ROLES
        }

        # select role from db and store result in context
        custom_roles_in_db = []
        if supplied_config_roles:
            roles_in_db = self.session.execute(
                sql.select(schema.Config.value).where(
                    schema.Config.parameter == "custom_relation_roles"
                )
            ).fetchone()
            if roles_in_db and roles_in_db.value:
                for custom_role in (
                    roles_in_db.value.removeprefix("[")
                    .removesuffix("]")
                    .split(",")
                ):
                    custom_roles_in_db.append(
                        custom_role.removeprefix('"').removesuffix('"')
                    )

        diff = supplied_config_roles.difference(custom_roles_in_db)
        if len(diff) > 0:
            raise NotFound(
                f"Config role(s) with name {', '.join(sorted(diff))} not found."
            )
