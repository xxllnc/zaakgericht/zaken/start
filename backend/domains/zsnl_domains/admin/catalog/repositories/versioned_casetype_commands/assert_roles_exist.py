# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext, DbEntry
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class AssertRolesExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        """Check for changes if all roles exist in the db
        If one of the roles does not exist, raise an exception
        If all roles exist, the id of the roles is stored in the CasetypeContext,
        so they can be used when inserting the casetype records. Query doesn't have to be performed again then
        """
        role_uuids: set = set()

        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            role_uuids.add(getattr_from_dict(phase, "assignment.role_uuid"))
            # collect all role uuids used in attributes
            for custom_field in getattr_from_dict(
                phase, "custom_fields", default_val=[]
            ):
                for permission in getattr_from_dict(
                    custom_field, "permissions", default_val=[]
                ):
                    role_uuids.add(getattr_from_dict(permission, "role_uuid"))

                # collect all group uuids for cases configures in a phase
                for case in getattr_from_dict(
                    phase, attr="cases", default_val=[]
                ):
                    role_uuids.add(
                        getattr_from_dict(case, "allocation.role_uuid")
                    )

        # collect all role uuids for zaaktype_autorization
        for autorisation in getattr_from_dict(changes, "authorization"):
            role_uuids.add(getattr_from_dict(autorisation, "role_uuid"))

        # remove None value from set
        role_uuids = {r for r in role_uuids if r is not None}

        # select role from db and store result in context
        roles_db = {
            str(r.uuid): DbEntry(id=r.id, name=r.name)
            for r in self.session.execute(
                sql.select(
                    schema.Role.id, schema.Role.uuid, schema.Role.name
                ).where(schema.Role.uuid.in_(role_uuids))
            ).fetchall()
        }
        diff = role_uuids.difference(roles_db.keys())
        if len(diff) > 0:
            raise NotFound(
                f"Role(s) with uuid {', '.join(sorted(diff))} not found."
            )

        for k, v in roles_db.items():
            self.get_context().set_role(role_uuid=UUID(k), role=v)
