# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .assert_casetype_exist import AssertCasetypeExist
from .assert_casetype_exists import AssertZaaktypeExistsCommand
from .assert_casetype_unique import AssertZaaktypeUniqueCommand
from .assert_config_roles_exist import AssertConfigRolesExist
from .assert_emailtemplate_exists import AssertEmailTemplatesExist
from .assert_groups_exist import AssertGroupsExist
from .assert_kenmerken_exists import AssertKenmerkenExist
from .assert_roles_exist import AssertRolesExist
from .assert_template_exist import AssertTemplatesExist
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from .insert_casetype_betrokkenen import InsertZaaktypeBetrokkenenCommand
from .insert_casetype_cases import InsertZaaktypeCasesCommand
from .insert_casetype_command import InsertZaaktypeCommand
from .insert_casetype_default_involved_subjects import (
    InsertZaaktypeDefaultInvolvedSubjectsCommand,
)
from .insert_casetype_definition import InsertZaaktypeDefinitionCommand
from .insert_casetype_node import InsertZaaktypeNodeCommand
from .insert_casetype_notifications import InsertZaaktypeNotificationsCommand
from .insert_casetype_results import InsertZaaktypeResultsCommand
from .insert_logging import InsertLoggingCommand
from .insert_zaaktype_kenmerken import InsertZaaktypeKenmerkenCommand
from .insert_zaaktype_status import InsertZaaktypeStatusCommand
from .insert_zaaktype_tasks import InsertZaaktypeTasksCommand
from .insert_zaaktype_templates import InsertZaaktypeTemplatesCommand
from .update_casetype_authorization import UpdateZaaktypeAuthorizationCommand
from .update_casetype_command import UpdateZaaktypeCommand
from .update_casetype_node import UpdateZaaktypeNodeCommand
from .update_casetype_node_id import UpdateZaaktypeNodeIdCommand
from .update_casetype_version import UpdateZaaktypeVersionCommand
from collections.abc import Callable
from minty.cqrs import UserInfo


class CasetypeBuilder:
    def create_casetype(
        self,
        session: Callable,
        changes: dict,
        user_info: UserInfo,
    ):
        context = CasetypeContext()
        context.set_current_user_uuid(user_info.user_uuid)
        commands: list[DbCommand[dict, CasetypeContext]] = [
            AssertZaaktypeUniqueCommand(session, context),
            AssertGroupsExist(session, context),
            AssertRolesExist(session, context),
            AssertKenmerkenExist(session, context),
            AssertTemplatesExist(session, context),
            AssertEmailTemplatesExist(session, context),
            AssertCasetypeExist(session, context),
            AssertConfigRolesExist(session, context),
            InsertZaaktypeCommand(session, context),
            InsertZaaktypeDefinitionCommand(session, context),
            InsertZaaktypeNodeCommand(session, context),
            UpdateZaaktypeAuthorizationCommand(session, context),
            InsertZaaktypeBetrokkenenCommand(session, context),
            UpdateZaaktypeVersionCommand(session, context),
            InsertLoggingCommand(session, context),
            UpdateZaaktypeNodeCommand(session, context),
            InsertZaaktypeStatusCommand(session, context),
            InsertZaaktypeKenmerkenCommand(session, context),
            InsertZaaktypeResultsCommand(session, context),
            InsertZaaktypeTasksCommand(session, context),
            InsertZaaktypeTemplatesCommand(session, context),
            InsertZaaktypeNotificationsCommand(session, context),
            InsertZaaktypeCasesCommand(session, context),
            InsertZaaktypeDefaultInvolvedSubjectsCommand(session, context),
            UpdateZaaktypeNodeIdCommand(session, context),
        ]
        for command in commands:
            command.execute(changes=changes)

    def update_casetype(
        self,
        session: Callable,
        changes: dict,
        user_info: UserInfo,
    ):
        context = CasetypeContext()
        context.set_current_user_uuid(user_info.user_uuid)
        commands: list[DbCommand[dict, CasetypeContext]] = [
            AssertZaaktypeExistsCommand(session, context),
            AssertGroupsExist(session, context),
            AssertRolesExist(session, context),
            AssertKenmerkenExist(session, context),
            AssertTemplatesExist(session, context),
            AssertEmailTemplatesExist(session, context),
            AssertCasetypeExist(session, context),
            AssertConfigRolesExist(session, context),
            UpdateZaaktypeCommand(session, context),
            InsertZaaktypeDefinitionCommand(session, context),
            InsertZaaktypeNodeCommand(session, context),
            UpdateZaaktypeAuthorizationCommand(session, context),
            InsertZaaktypeBetrokkenenCommand(session, context),
            UpdateZaaktypeVersionCommand(session, context),
            InsertLoggingCommand(session, context),
            UpdateZaaktypeNodeCommand(session, context),
            InsertZaaktypeStatusCommand(session, context),
            InsertZaaktypeKenmerkenCommand(session, context),
            InsertZaaktypeResultsCommand(session, context),
            InsertZaaktypeTasksCommand(session=session, context=context),
            InsertZaaktypeTemplatesCommand(session, context),
            InsertZaaktypeNotificationsCommand(session, context),
            InsertZaaktypeCasesCommand(session, context),
            InsertZaaktypeDefaultInvolvedSubjectsCommand(session, context),
            UpdateZaaktypeNodeIdCommand(session, context),
        ]

        for command in commands:
            command.execute(changes=changes)
