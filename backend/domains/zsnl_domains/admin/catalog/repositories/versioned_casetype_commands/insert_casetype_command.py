# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        bibliotheekcategorie_id_qry = None
        if getattr_from_dict(changes, "catalog_folder.uuid"):
            bibliotheekcategorie_id_qry = (
                sql.select(schema.BibliotheekCategorie.id)
                .where(
                    schema.BibliotheekCategorie.uuid
                    == getattr_from_dict(changes, "catalog_folder.uuid")
                )
                .scalar_subquery()
            )
        id = self.session.execute(
            sql.insert(schema.Zaaktype)
            .values(
                uuid=changes.get("casetype_uuid"),
                bibliotheek_categorie_id=bibliotheekcategorie_id_qry,
                active=changes.get("active"),
            )
            .returning(schema.Zaaktype.id)
        ).fetchone()
        self.get_context().set_zaaktype_id(id.id)
