# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeDefaultInvolvedSubjectsCommand(
    DbCommand[dict, CasetypeContext]
):
    def execute(
        self,
        changes: dict,
    ) -> None:
        for phase_count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            involved_subjects = getattr_from_dict(phase, "involved_subjects")
            subjects_to_insert = []
            for involved_subject in involved_subjects or []:
                subjects_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "zaak_status_id": self.get_context().get_phase_id(
                            phase_count
                        ),
                        "betrokkene_identifier": util.create_legacy_subject_id(
                            session=self.session,
                            subject_uuid=getattr_from_dict(
                                involved_subject, "involved_subject.uuid"
                            ),
                            subject_type=getattr_from_dict(
                                involved_subject, "involved_subject.type"
                            ),
                        ),
                        "betrokkene_type": getattr_from_dict(
                            involved_subject, "involved_subject.type"
                        ),
                        "naam": getattr_from_dict(
                            involved_subject, "involved_subject.name"
                        ),
                        "rol": getattr_from_dict(involved_subject, "role"),
                        "magic_string_prefix": getattr_from_dict(
                            involved_subject, "magic_string"
                        ),
                        "gemachtigd": getattr_from_dict(
                            involved_subject, "authorized_for_case"
                        ),
                        "notify": getattr_from_dict(
                            involved_subject, "notify_by_email"
                        ),
                    }
                )
            if len(subjects_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeStandaardBetrokkenen).values(
                        subjects_to_insert
                    )
                )
