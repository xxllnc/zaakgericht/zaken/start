# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeNotificationsCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        for phase_count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            email_templates = getattr_from_dict(phase, "email_templates")
            email_templates_to_insert = []
            for email_template in email_templates or []:
                attribute = self.get_context().get_email_template(
                    email_template["catalog_message_uuid"]
                )
                attribute_id = attribute.id if attribute else None

                email_templates_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "zaak_status_id": self.get_context().get_phase_id(
                            phase_count
                        ),
                        "rcpt": email_template.get("receiver"),
                        "email": email_template.get("to", ""),
                        "automatic": util.bool_to_int(
                            email_template["send_automatic"]
                        ),
                        "cc": email_template.get("cc", ""),
                        "bcc": email_template.get("bcc", ""),
                        "betrokkene_role": email_template.get(
                            "person_involved_role", None
                        ),
                        "bibliotheek_notificaties_id": attribute_id,
                        "behandelaar": util.create_legacy_subject_id(
                            session=self.session,
                            subject_uuid=email_template["subject"]["uuid"],
                            subject_type=email_template["subject"]["type"],
                        )
                        if email_template.get("subject", None)
                        else None,
                    }
                )

            if len(email_templates_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeNotificatie).values(
                        email_templates_to_insert
                    )
                )
