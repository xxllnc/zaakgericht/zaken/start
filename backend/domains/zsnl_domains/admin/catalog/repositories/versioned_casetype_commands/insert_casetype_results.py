# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeResultsCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        last_phase_num = len(
            getattr_from_dict(changes, "phases", default_val=[])
        )
        if last_phase_num > 0:
            zaaktype_status_id = self.get_context().get_phase_id(
                last_phase_num
            )
            results = getattr_from_dict(changes, "results")
            results_to_insert = []
            for result in results or []:
                results_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "zaaktype_status_id": zaaktype_status_id,
                        "label": getattr_from_dict(result, "name"),
                        "resultaat": getattr_from_dict(result, "result_type"),
                        "standaard_keuze": getattr_from_dict(
                            result, "is_default"
                        ),
                        "selectielijst": getattr_from_dict(
                            result, "selection_list"
                        ),
                        "selectielijst_brondatum": getattr_from_dict(
                            result, "selection_list_source_date"
                        ),
                        "selectielijst_einddatum": getattr_from_dict(
                            result, "selection_list_end_date"
                        ),
                        "trigger_archival": getattr_from_dict(
                            result, "archival_trigger"
                        ),
                        "archiefnominatie": getattr_from_dict(
                            result, "archival_nomination"
                        ),
                        "bewaartermijn": getattr_from_dict(
                            result, "retention_period"
                        ),
                        "ingang": getattr_from_dict(
                            result, "archival_nomination_valuation"
                        ),
                        "comments": getattr_from_dict(result, "comments"),
                        "properties": self.build_properties(result),
                    }
                )
            if len(results_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeResultaten).values(
                        results_to_insert
                    )
                )

    def build_properties(self, result: dict) -> dict:
        return {
            "selectielijst_nummer": getattr_from_dict(
                result, "selection_list_number", ""
            ),
            "procestype_nummer": getattr_from_dict(
                result, "processtype_number", ""
            ),
            "procestype_naam": getattr_from_dict(
                result, "processtype_name", ""
            ),
            "procestype_omschrijving": getattr_from_dict(
                result, "processtype_description", ""
            ),
            "procestype_toelichting": getattr_from_dict(
                result, "processtype_explanation", ""
            ),
            "procestype_object": getattr_from_dict(
                result, "processtype_object", ""
            ),
            "procestype_generiek": getattr_from_dict(
                result, "procestype_generic", ""
            ),
            "herkomst": getattr_from_dict(result, "origin", ""),
            "procestermijn": getattr_from_dict(result, "process_period", ""),
        }
