# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from typing import Tuple
from uuid import UUID
from zsnl_domains.database import schema


class InsertZaaktypeKenmerkenCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        result = self.__insert_zaaktype_kenmerken(changes)
        self.__update_zaaktype_kenmerken(changes=changes, records=result)

    def __get_record_by_attribute_uuid(self, records: dict, uuid: UUID) -> int:
        attribute = self.get_context().get_attribute_by_uuid(uuid=uuid)

        if attribute:
            for k, v in records.items():
                if v["bibliotheek_kenmerken_id"] == attribute.id:
                    return k

        # if attribute not found, then raise an exception
        raise NotFound(f"Attribute with uuid {uuid} not found in casetype")

    def __update_properties_for_custom_field(
        self, records: dict, custom_field: dict
    ) -> Tuple[int, dict] | None:
        if custom_field["attribute_type"] == "date":
            # zaaktype_kenmerken containing a date attribute can have
            # references to other zaaktype_kenmerken in the datelimit
            # configuration (which is stored in the properties column).
            # this function checks if such a config is configured, lookup the
            # id of the referenced zaaktype_kenmerken record, and update the
            # properties data
            start_reference = getattr_from_dict(
                custom_field, "start_date_limitation.reference"
            )
            end_reference = getattr_from_dict(
                custom_field, "end_date_limitation.reference"
            )
            if start_reference and start_reference != "currentDate":
                start_reference = self.__get_record_by_attribute_uuid(
                    records=records, uuid=start_reference
                )
            if end_reference and end_reference != "currentDate":
                end_reference = self.__get_record_by_attribute_uuid(
                    records=records, uuid=end_reference
                )
            if start_reference or end_reference:
                record_id_to_update = self.__get_record_by_attribute_uuid(
                    records=records,
                    uuid=custom_field["uuid"],
                )
                result = {
                    record_id_to_update: records[record_id_to_update][
                        "properties"
                    ]
                }

                if start_reference:
                    result[record_id_to_update]["date_limit"]["start"].update(
                        {"reference": start_reference}
                    )
                if end_reference:
                    result[record_id_to_update]["date_limit"]["end"].update(
                        {"reference": end_reference}
                    )
                return record_id_to_update, result[record_id_to_update]

    def __update_zaaktype_kenmerken(self, changes: dict, records: dict):
        # some zaaktype_kenmerken records refer to other zaaktype_kenmerken records, which are just inserted.
        # id's are known after inserting.
        # Now we have the id's we can update the records.

        # Check for date attributes containing a date limitation and fill the "reference" attribute with the id of the just inserted record
        # records_for_update: dict = {}  # id, properties
        record_list_for_update: list[dict] = []
        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            for custom_field in getattr_from_dict(
                phase, "custom_fields", default_val=[]
            ):
                if custom_field.get("attribute_type") == "date":
                    record_id, updated_properties = (
                        self.__update_properties_for_custom_field(
                            records=records, custom_field=custom_field
                        )
                    ) or (None, None)
                    if record_id and updated_properties:
                        record_list_for_update.append(
                            {"id": record_id, "properties": updated_properties}
                        )

        # update te records with updated properties
        for record in record_list_for_update:
            self.session.execute(
                sql.update(schema.ZaaktypeKenmerk)
                .values({"properties": record["properties"]})
                .where(schema.ZaaktypeKenmerk.id == record["id"])
            )

    # insert zaaktype_kenmerken and return a dict containing the inserted record id, including properties and bibliotheek_kenmerken_id
    # example: {record.id: {"properties": record.properties, "bibliotheek_kenmerken_id": record.bibliotheek_kenmerken_id}}
    def __insert_zaaktype_kenmerken(self, changes: dict) -> dict:
        inserted_zaaktype_kenmerken_records: dict = {}
        for phase_count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            kenmerken_to_insert = []

            for custom_field in getattr_from_dict(
                phase, "custom_fields", default_val=[]
            ):
                bibliotheek_kenmerken_id = None
                if self.get_context().get_attribute_by_uuid(
                    uuid=getattr_from_dict(custom_field, "uuid")
                ):
                    bibliotheek_kenmerken_id = (
                        self.get_context()
                        .get_attribute_by_uuid(
                            uuid=getattr_from_dict(custom_field, "uuid")
                        )
                        .id
                    )
                kenmerken_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "zaak_status_id": self.get_context().get_phase_id(
                            phase_count
                        ),
                        "bibliotheek_kenmerken_id": bibliotheek_kenmerken_id,
                        "value_mandatory": getattr_from_dict(
                            custom_field, "mandatory", False
                        ),
                        "label": getattr_from_dict(custom_field, "title"),
                        "help": getattr_from_dict(custom_field, "help_intern"),
                        "pip": util.bool_to_int(
                            getattr_from_dict(custom_field, "publish_pip")
                        ),
                        "bag_zaakadres": util.bool_to_int(
                            getattr_from_dict(
                                custom_field, "use_as_case_address"
                            )
                        ),
                        "pip_can_change": getattr_from_dict(
                            custom_field, "pip_can_change"
                        ),
                        "referential": getattr_from_dict(
                            custom_field, "referential", False
                        ),
                        "is_systeemkenmerk": getattr_from_dict(
                            custom_field, "system_attribute"
                        ),
                        "help_extern": getattr_from_dict(
                            custom_field, "help_extern"
                        ),
                        "label_multiple": getattr_from_dict(
                            custom_field, "label_multiple"
                        ),
                        "properties": self.__build_properties(custom_field),
                        "is_group": getattr_from_dict(
                            custom_field, "is_group"
                        ),
                        "required_permissions": {
                            "selectedUnits": self.__build_permissions(
                                getattr_from_dict(custom_field, "permissions")
                            )
                        },
                        # "zaakinformatie_view": None,  # Not used anymore
                        # "value_default": None,  # Not used anymore
                        # "publish_public": None,  # Not used anymore
                        # "object_metadata": {},  # TODO objecten v1
                        # "object_id": None,  # TODO objecten_v1
                    }
                )

            if len(kenmerken_to_insert) > 0:
                # insert zaaktype_kenmerken and fetch id's and bibliotheek_kenmerken_id's.
                # for date attributes including date limitations, a reference to ZaaktypeKenmerken.id is stored in the properties column
                # updating these records below with the fetched id's in this call
                inserted_zaaktype_kenmerken = self.session.execute(
                    sql.insert(schema.ZaaktypeKenmerk)
                    .values(kenmerken_to_insert)
                    .returning(
                        schema.ZaaktypeKenmerk.id,
                        schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                        schema.ZaaktypeKenmerk.properties,
                    )
                ).fetchall()

                inserted_zaaktype_kenmerken_records.update(
                    {
                        ztk.id: {
                            "properties": ztk.properties,
                            "bibliotheek_kenmerken_id": ztk.bibliotheek_kenmerken_id,
                        }
                        for ztk in inserted_zaaktype_kenmerken
                    }
                )
        return inserted_zaaktype_kenmerken_records

    def __build_properties(self, custom_field: dict) -> dict:
        wms_feature_attribute_ref = self._context.get_attribute_by_uuid(
            getattr_from_dict(custom_field, "map_wms_feature_attribute_id")
        )
        properties = {}
        properties.update(
            {
                "skip_change_approval": util.bool_to_int(
                    getattr_from_dict(custom_field, "skip_change_approval")
                ),
                "custom_object": {
                    "create": {
                        "enabled": util.bool_to_int(
                            getattr_from_dict(
                                custom_field,
                                "create_custom_object_enabled",
                            )
                        ),
                        "label": getattr_from_dict(
                            custom_field,
                            "create_custom_object_label",
                        ),
                    },
                    "attributes": getattr_from_dict(
                        custom_field,
                        "create_custom_object_attribute_mapping",
                    ),
                    "edit": {"enabled": 0},
                    "delete": {"enabled": 0},
                },
                "relationship_subject_role": getattr_from_dict(
                    custom_field, "relationship_subject_role"
                ),
                "show_on_map": util.bool_to_int(
                    getattr_from_dict(custom_field, "show_on_map")
                ),
                "map_wms_layer_id": getattr_from_dict(
                    custom_field, "map_wms_layer_id"
                ),
                "map_wms_feature_attribute_label": wms_feature_attribute_ref.name
                if wms_feature_attribute_ref is not None
                else None,
                "map_wms_feature_attribute_id": wms_feature_attribute_ref.id
                if wms_feature_attribute_ref is not None
                else None,
                "map_case_location": util.bool_to_int(
                    getattr_from_dict(custom_field, "map_case_location")
                ),
            }
        )
        properties["date_limit"] = {}
        if getattr_from_dict(custom_field, "start_date_limitation"):
            properties["date_limit"].update(
                {
                    "start": {
                        "active": util.bool_to_int(
                            getattr_from_dict(
                                custom_field,
                                "start_date_limitation.active",
                            )
                        ),
                        "num": getattr_from_dict(
                            custom_field, "start_date_limitation.value"
                        ),
                        "term": getattr_from_dict(
                            custom_field, "start_date_limitation.term"
                        ),
                        "during": getattr_from_dict(
                            custom_field, "start_date_limitation.during"
                        ),
                    }
                }
            )
        if getattr_from_dict(custom_field, "end_date_limitation"):
            properties["date_limit"].update(
                {
                    "end": {
                        "active": util.bool_to_int(
                            getattr_from_dict(
                                custom_field,
                                "end_date_limitation.active",
                            )
                        ),
                        "num": getattr_from_dict(
                            custom_field, "end_date_limitation.value"
                        ),
                        "term": getattr_from_dict(
                            custom_field, "end_date_limitation.term"
                        ),
                        "during": getattr_from_dict(
                            custom_field, "end_date_limitation.during"
                        ),
                    }
                }
            )
        if properties["date_limit"] == {}:
            # delete key if no datelimit active
            del properties["date_limit"]

        return properties

    def __build_permissions(self, permissions: list | None) -> list:
        db_permissions: list = []
        if not permissions or not isinstance(permissions, list):
            return db_permissions

        for permission in permissions:
            role = self.get_context().get_role(
                getattr_from_dict(permission, "role_uuid")
            )
            department = self.get_context().get_group(
                getattr_from_dict(permission, "department_uuid")
            )
            db_permissions.append(
                {
                    "role_id": str(role.id),
                    "role_name": role.name,
                    "org_unit_id": str(department.id),
                    "org_unit_name": department.name,
                }
            )
        return db_permissions
