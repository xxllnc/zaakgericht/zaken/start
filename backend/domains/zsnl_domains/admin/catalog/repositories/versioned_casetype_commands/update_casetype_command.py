# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class UpdateZaaktypeCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        values = {}
        bibliotheekcategorie_id_qry = None
        if getattr_from_dict(changes, "catalog_folder.uuid"):
            bibliotheekcategorie_id_qry = (
                sql.select(schema.BibliotheekCategorie.id)
                .where(
                    schema.BibliotheekCategorie.uuid
                    == getattr_from_dict(changes, "catalog_folder.uuid")
                )
                .scalar_subquery()
            )
            values["bibliotheek_categorie_id"] = bibliotheekcategorie_id_qry

        values["active"] = getattr_from_dict(changes, "active")
        self.session.execute(
            sql.update(schema.Zaaktype)
            .values(**values)
            .where(
                schema.Zaaktype.uuid
                == getattr_from_dict(changes, "casetype_uuid")
            )
        )
