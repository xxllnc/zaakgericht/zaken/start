# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class UpdateZaaktypeNodeCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        self.session.execute(
            sql.update(schema.ZaaktypeNode)
            .values(
                logging_id=self.get_context().get_logging_id(),
            )
            .where(
                schema.ZaaktypeNode.id
                == self.get_context().get_zaaktype_node_id()
            )
        )
