# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class UpdateZaaktypeVersionCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        self.session.execute(
            sql.update(schema.Zaaktype)
            .values(
                zaaktype_node_id=self.get_context().get_zaaktype_node_id(),
            )
            .where(schema.Zaaktype.id == self.get_context().get_zaaktype_id())
        )
