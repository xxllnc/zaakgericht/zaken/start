# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from collections.abc import Callable
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


def bool_to_int(value: bool) -> int:
    return 1 if value is True else 0


def bool_to_ja_nee(value: bool) -> str:
    return "Ja" if value is True else "Nee"


def bool_to_on_none(value: bool) -> str | None:
    return "on" if value is True else None


def create_legacy_subject_id(
    session: Callable, subject_uuid: UUID, subject_type: str
) -> str | None:
    result: str | None = None
    if not subject_uuid or not subject_type:
        # if no argument supplied, return none
        return None

    match subject_type:
        case "person":
            id_row = session.execute(
                sql.select(schema.NatuurlijkPersoon.id.label("id")).where(
                    schema.NatuurlijkPersoon.uuid == subject_uuid
                )
            ).fetchone()
            if id_row:
                result = f"betrokkene-natuurlijk_persoon-{id_row.id}"
        case "organization":
            id_row = session.execute(
                sql.select(schema.Bedrijf.id.label("id")).where(
                    schema.Bedrijf.uuid == subject_uuid
                )
            ).fetchone()
            if id_row:
                result = f"betrokkene-bedrijf-{id_row.id}"
        case "employee":
            id_row = session.execute(
                sql.select(schema.Subject.id.label("id")).where(
                    schema.Subject.uuid == subject_uuid
                )
            ).fetchone()
            if id_row:
                result = f"betrokkene-medewerker-{id_row.id}"
    if not result:
        raise NotFound(
            f"Subject {subject_type} with uuid {subject_uuid} not found"
        )

    return result
