# SPDX-FileCopyrightText: xxllnc Zaakgericht B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from typing import Literal
from uuid import UUID


class Transaction(Entity):
    """
    Minimal representation of an transaction, as part of a transaction record
    """

    entity_type: Literal["transaction"] = "transaction"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID


class TransactionRecord(Entity):
    """
    A transaction record, the "execution unit" of a transaction
    """

    entity_type: Literal["transaction_record"] = "transaction_record"
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    entity_relationships: list[str] = ["transaction"]

    uuid: UUID

    input: str
    output: str
    is_error: bool

    transaction: Transaction

    date_executed: datetime

    error_message: str | None
