# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
from ..constants import (
    APPOINTMENT_INTEGRATION_TYPES,
    APPOINTMENT_V2_INTEGRATION_TYPES,
    DOCUMENT_INTEGRATION_TYPES,
)
from ..entities import (
    EmailIntegration,
    Integration,
    Transaction,
    TransactionRecord,
)
from ..repositories import (
    EmailIntegrationRepository,
    IntegrationRepository,
    TransactionDataRepository,
    TransactionFilters,
    TransactionRecordRepository,
    TransactionRepository,
    TransactionSortOrder,
)
from minty.cqrs import QueryBase
from minty.entity import EntityCollection, EntityResponse
from pydantic.v1 import validate_arguments
from typing import Any, cast
from uuid import UUID


class Queries(QueryBase):
    """Queries class for the integrations"""

    @property
    def integration_repository(self) -> IntegrationRepository:
        repo = cast(
            IntegrationRepository,
            self.get_repository("integration"),
        )
        return repo

    @property
    def email_integration_repository(self) -> EmailIntegrationRepository:
        repo = cast(
            EmailIntegrationRepository,
            self.get_repository("email_integration"),
        )
        return repo

    @property
    def transaction_repository(self) -> TransactionRepository:
        repo = cast(
            TransactionRepository,
            self.get_repository("transaction"),
        )
        return repo

    @property
    def transaction_data_repository(self) -> TransactionDataRepository:
        repo = cast(
            TransactionDataRepository,
            self.get_repository("transaction_data"),
        )
        return repo

    @property
    def transaction_record_repository(self) -> TransactionRecordRepository:
        repo = cast(
            TransactionRecordRepository,
            self.get_repository("transaction_record"),
        )
        return repo

    @validate_arguments
    def get_email_integration(
        self, integration_uuid: UUID
    ) -> EmailIntegration:
        """
        Retrieve an email integration
        """

        integration = self.email_integration_repository.get(
            uuid=integration_uuid
        )

        return integration

    @validate_arguments
    def get_active_appointment_integrations(self) -> list[Integration]:
        """Get active appointment integrations.

        :return: List of active appointment integrations.
        """

        integrations = self.integration_repository.get_active_integrations(
            integration_types=APPOINTMENT_INTEGRATION_TYPES
        )
        return integrations

    @validate_arguments
    def get_active_appointment_v2_integrations(self) -> list[Integration]:
        """Get active appointment integrations.

        :return: List of active "v2" appointment integrations.
        """

        integrations = self.integration_repository.get_active_integrations(
            integration_types=APPOINTMENT_V2_INTEGRATION_TYPES
        )
        return integrations

    @validate_arguments
    def get_active_integrations_for_document(self) -> list[Integration]:
        """Retrieve a list of active integrations for document templating/creation.

        :return: List of active integrations for document.
        :rtype: list
        """

        document_integrations = (
            self.integration_repository.get_active_integrations(
                integration_types=DOCUMENT_INTEGRATION_TYPES
            )
        )
        return document_integrations

    @validate_arguments
    def get_integrations(self) -> list[Integration]:
        """
        Retrieve a list of all non-deleted integrations
        """
        return self.integration_repository.get_all()

    @validate_arguments
    def get_transaction(self, uuid: UUID) -> EntityResponse[Transaction]:
        """
        Retrieve a single transaction, given its UUID
        """
        transaction = self.transaction_repository.get(uuid)
        integration = self.integration_repository.get_multiple(
            [transaction.integration.uuid]
        )

        return EntityResponse(transaction, included_entities=integration)

    @validate_arguments
    def get_transaction_data(self, uuid: UUID) -> EntityResponse[Transaction]:
        """
        Retrieve a single transaction's input data, given its UUID
        """
        return self.transaction_data_repository.get(uuid)

    @validate_arguments
    def get_transactions(
        self,
        page: int = 1,
        page_size: int = 20,
        sort: TransactionSortOrder = TransactionSortOrder.created_desc,
        filters: TransactionFilters | None = None,
        cursor: str | None = None,
    ) -> EntityCollection[Transaction]:
        """
        Retrieve a list of transactions (filtered, paged and sorted)

        Args:
            page (int): page number to be retrieve
            page_size (int): number of records to be retrieved
            sort (TransactionSortOrder): sort order of the returned transactions
            filters (TransactionFilters): filter used for filtering
            cursor (str): the current cursor

        Returns:
            EntityCollection[Transaction]: collection of transactions
        """
        decoded_cursor: dict | None = None

        if cursor:
            decoded_bytes: bytes = base64.b64decode(cursor)
            decoded_json: str = decoded_bytes.decode("utf-8")
            decoded_cursor: dict = json.loads(decoded_json)
            page_size: int = decoded_cursor.get("page_size")

        transactions: EntityCollection[Transaction] = (
            self.transaction_repository.get_list(
                page=page,
                page_size=page_size,
                sort=sort,
                filters=filters,
                cursor=decoded_cursor,
            )
        )

        integrations: list[Integration] = []
        integration_uuids: list[UUID] = []
        for t in transactions:
            if t:
                integration_uuids.append(t.integration.uuid)

        if integration_uuids:
            integrations = self.integration_repository.get_multiple(
                integration_uuids
            )

        transactions_cursor: dict | None = self._calculate_cursor(
            cursor, transactions, sort, page_size
        )

        transactions.included_entities = integrations
        transactions.cursor = transactions_cursor

        return transactions

    def _calculate_cursor(
        self,
        cursor: str | None,
        transactions: EntityCollection[Transaction],
        sort: TransactionSortOrder,
        page_size: int,
    ) -> dict[str, str | None] | None:
        """
        Calculate the new current, next and previous cursor based on the found
        transactions.

        Args:
            cursor (str): the current cursor
            transactions (EntityCollection[Transaction]): the transactions
            sort (TransactionSortOrder): sort order
            page_size (int): number of records to be retrieved

        Returns:
            None: if no transactions are found
            dict: dictionary with the old/current cursor, the next cursor and
                  the previous cursor
        """
        if (not transactions.entities) or (
            (len(transactions.entities) == 1)
            and (not transactions.entities[0])
        ):
            return None

        # Get last item value for the given transactions.
        last_item_value: dict[str, Any] = self._get_last_item_value(
            sort=sort, transactions=transactions
        )

        cursor_next: dict[str, Any] = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": transactions.entities[-1].id,
            "last_item": str(last_item_value.get("next")),
        }
        base64_string_next: str = self._encode_to_base64(cursor_next)

        cursor_previous: dict[str, Any] = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": transactions.entities[0].id,
            "last_item": str(last_item_value.get("previous")),
        }
        base64_string_previous: str = self._encode_to_base64(
            obj=cursor_previous
        )

        new_cursor: dict[str, str | None] = {
            "next": base64_string_next,
            "previous": base64_string_previous,
            "current": cursor,
        }

        return new_cursor

    def _encode_to_base64(self, obj: dict[str | Any]) -> str:
        """
        Encode the dictionary to a base64 encoded string.

        Args:
            obj [dict[str, Any]]: dictionary to encode

        Returns:
            str: the base64 encoding string
        """
        json_str: str = json.dumps(obj)
        base64_bytes: bytes = base64.b64encode(json_str.encode("utf-8"))
        return base64_bytes.decode("utf-8")

    def _get_last_item_value(
        self,
        sort: TransactionSortOrder,
        transactions: EntityCollection[Transaction],
    ) -> dict[str, Any]:
        """
        Retrieve the latest item value in the list of transactions for the
        next and previous cursor. For the next cursor, this is the last item
        value in the list. For the previous cursor, this is the first item
        value in the list.

        Args:
            sort (TransactionSortOrder): sort order
            transactions (EntityCollection[Transaction]): transactions list

        Returns:
            None: if no transactions are found
            dict: dictionary with the item value for the next and previous
                  cursor
        """

        # Remove descending identifier if exists
        sort_key: str = sort.replace("-", "")

        last_item_value_based_on_sort: dict = {
            TransactionSortOrder.created_asc: {
                "next": transactions.entities[-1].created,
                "previous": transactions.entities[0].created,
            },
        }

        return last_item_value_based_on_sort[sort_key]

    @validate_arguments
    def get_transaction_records(
        self, transaction_uuid: UUID
    ) -> list[TransactionRecord]:
        """
        Get all transaction records for the specified transaction
        """

        records = self.transaction_record_repository.get_list(transaction_uuid)

        return records
