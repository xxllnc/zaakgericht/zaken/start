# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
import minty.exceptions
from .... import ZaaksysteemRepositoryBase
from ....shared.types import SearchCursorTypedDict
from ..entities.transaction import (
    Transaction,
    TransactionIntegration,
)
from minty.entity import EntityCollection
from pydantic.v1 import BaseModel, Field, validator
from sqlalchemy import exc as sql_exc
from sqlalchemy import sql
from typing import Any, Final, Literal, assert_never
from uuid import UUID
from zsnl_domains.database import schema


class TransactionFilters(BaseModel):
    integrations: set[UUID] | None = Field(
        default=None, alias="relationship.integration.id"
    )

    has_errors: Literal["only_errors", "has_errors", "no_errors"] | None = (
        Field(default=None)
    )

    keyword: str | None = Field(default=None)

    class Config:
        allow_population_by_field_name = True

    @validator("integrations", pre=True)
    def _single_integration_as_set(cls, v):
        if isinstance(v, str):
            return set([v])

        return v


class TransactionSortOrder(enum.StrEnum):
    created_desc = "-created"
    created_asc = "created"


_coalesce = sql.func.coalesce
TRANSACTION_QUERY: Final = (
    sql.select(
        schema.Transaction.uuid,
        schema.Transaction.id,
        schema.Transaction.direction,
        schema.Transaction.date_created,
        schema.Transaction.date_last_retry,
        schema.Transaction.date_next_retry,
        schema.Transaction.external_transaction_id,
        schema.Transaction.automated_retry_count,
        _coalesce(schema.Transaction.error_count, 0).label("error_count"),
        _coalesce(schema.Transaction.success_count, 0).label("success_count"),
        schema.Transaction.error_fatal,
        schema.Transaction.processed,
        schema.Transaction.error_message,
        schema.Transaction.preview_data,
        schema.Interface.uuid.label("integration_uuid"),
        sql.select(sql.func.count("*"))
        .select_from(schema.TransactionRecord)
        .where(
            schema.TransactionRecord.transaction_id == schema.Transaction.id
        )
        .scalar_subquery()
        .label("record_count"),
    )
    .select_from(
        sql.join(
            schema.Transaction,
            schema.Interface,
            sql.and_(
                schema.Transaction.interface_id == schema.Interface.id,
                schema.Interface.date_deleted.is_(None),
            ),
        )
    )
    .where(schema.Transaction.date_deleted.is_(None))
)

COUNT_QUERY: Final = sql.select(sql.func.count("*")).select_from(
    sql.join(
        schema.Transaction,
        schema.Interface,
        sql.and_(
            schema.Transaction.interface_id == schema.Interface.id,
            schema.Interface.date_deleted.is_(None),
        ),
    )
)


class TransactionRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Transaction"
    _events_to_calls = {}

    def get(self, uuid: UUID) -> Transaction:
        try:
            transaction_row = self.session.execute(
                TRANSACTION_QUERY.where(schema.Transaction.uuid == uuid)
            ).one()
        except sql_exc.NoResultFound as e:
            raise minty.exceptions.NotFound(
                "No transaction found with that id", "transaction/not_found"
            ) from e

        return self._inflate_from_row(transaction_row)

    def get_list(
        self,
        page: int,
        page_size: int,
        sort: TransactionSortOrder,
        filters: TransactionFilters | None,
        cursor: SearchCursorTypedDict | None = None,
    ) -> EntityCollection[Transaction]:
        """
        Retrieve a list of transactions.

        Args:
            page (int): page number to be retrieve
            page_size (int): number of records to be retrieved
            sort (TransactionSortOrder): sort order of the returned transactions
            filters (TransactionFilters): filter used for filtering
            cursor (str): cursor to the last result set

        Returns:
            EntityCollection[Transaction]: collection of transactions
        """
        transaction_query = self._apply_filters(filters, TRANSACTION_QUERY)

        if not cursor:
            offset: int = self._calculate_offset(
                page=page, page_size=page_size
            )
            sort_order = self._get_sort_order(order=sort)
            transaction_rows = self.session.execute(
                transaction_query.limit(page_size)
                .offset(offset)
                .order_by(*sort_order)
            ).fetchall()
        else:
            qry_stmt = self._calculate_offset_based_on_cursor(
                qry_stmt=transaction_query, sort=sort, cursor=cursor
            )
            transaction_rows = self.session.execute(qry_stmt).fetchall()

        count_query = self._apply_filters(filters, COUNT_QUERY)
        total_count = self.session.execute(count_query).scalar()

        return EntityCollection(
            [self._inflate_from_row(row) for row in transaction_rows],
            total_results=total_count,
        )

    def _inflate_from_row(self, row) -> Transaction:
        if not row:
            return None

        preview = ""
        if isinstance(row.preview_data, list) and len(row.preview_data) > 0:
            preview = row.preview_data[0].get("preview_string", "")

        return Transaction(
            entity_id=row.uuid,
            uuid=row.uuid,
            id=row.id,
            external_id=row.external_transaction_id,
            created=row.date_created,
            direction=row.direction,
            error_message=row.error_message,
            last_retry=row.date_last_retry,
            next_retry=row.date_next_retry,
            retry_count=row.automated_retry_count,
            error_count=row.error_count,
            error_fatal=row.error_fatal or False,
            success_count=row.success_count,
            processed=row.processed,
            record_count=row.record_count,
            entity_meta_summary=preview,
            integration=TransactionIntegration(
                entity_id=row.integration_uuid,
                uuid=row.integration_uuid,
            ),
        )

    def _apply_filters(
        self, filters: TransactionFilters | None, query: sql.Select[Any]
    ) -> sql.Select[Any]:
        if not filters:
            return query

        if filters.integrations:
            query = query.where(
                schema.Transaction.interface_id.in_(
                    other=sql.select(schema.Interface.id).where(
                        schema.Interface.uuid.in_(filters.integrations)
                    )
                )
            )

        match filters.has_errors:
            case None:
                pass

            case "only_errors":
                query = query.where(schema.Transaction.error_fatal.is_(True))
            case "has_errors":
                query = query.where(schema.Transaction.error_count > 0)
            case "no_errors":
                query = query.where(
                    sql.and_(
                        sql.or_(
                            schema.Transaction.error_count == 0,
                            schema.Transaction.error_count.is_(None),
                        ),
                        schema.Transaction.error_fatal.is_(False),
                    )
                )

            # 20240820 Coverage doesn't understand exhaustive matches
            # https://github.com/pytest-dev/pytest-cov/issues/533#issuecomment-1848266478
            case _:  # pragma: no cover
                assert_never(filters.has_errors)

        if filters.keyword:
            query = query.where(
                schema.Transaction.text_vector.match(filters.keyword)
            )

        return query

    def _calculate_offset_based_on_cursor(
        self,
        qry_stmt,
        sort: TransactionSortOrder,
        cursor: SearchCursorTypedDict,
    ):
        """
        Calculate the new query to fetch transactions based on the cursor.

        Args:
            cursor (str): the current cursor
            transactions (EntityCollection[Transaction]): the found
                          transactions
            sort (TransactionSortOrder): sort order
            page_size (int): number of records to be retrieved

        Returns:
            sqlalchemy.sql.selectable.Select: SQL query for retrieving the
            transactions
        """
        last_result: str = cursor["last_item"]
        last_id: int = cursor.get("last_item_identifier", 1)
        page: str = cursor.get("page")
        page_size: int = cursor.get("page_size", 1)

        sort_order = self._get_sort_order(order=sort)

        if page == "next":
            cursor_subquery = self._get_subquery_cursor_next_page(
                last_result=last_result,
                last_id=last_id,
                sort=sort,
            )
            qry_stmt = qry_stmt.where(cursor_subquery)
            qry_stmt = qry_stmt.order_by(*sort_order).limit(page_size)

        elif page == "previous":
            cursor_subquery = self._get_subquery_cursor_previous_page(
                last_result=last_result,
                last_id=last_id,
                sort=sort,
            )
            qry_stmt = qry_stmt.where(cursor_subquery)

            subquery_sort: str = sort[1:] if sort[0] == "-" else "-" + sort
            subquery_sort_order = self._get_sort_order(order=subquery_sort)

            inner_query = (
                qry_stmt.order_by(*subquery_sort_order).limit(page_size)
            ).subquery()

            order = self._get_sort_order(alias=inner_query, order=sort)
            qry_stmt = sql.select(inner_query).order_by(*order)

        return qry_stmt

    def _get_subquery_cursor_next_page(
        self,
        last_result: str,
        last_id: int,
        sort: TransactionSortOrder,
    ):
        PAGINATION_SUBQUERY = {
            TransactionSortOrder.created_asc: sql.or_(
                sql.and_(
                    schema.Transaction.date_created == last_result,
                    schema.Transaction.id < last_id,
                ).self_group(),
                schema.Transaction.date_created > last_result,
            ),
            TransactionSortOrder.created_desc: sql.or_(
                sql.and_(
                    schema.Transaction.date_created == last_result,
                    schema.Transaction.id < last_id,
                ).self_group(),
                schema.Transaction.date_created < last_result,
            ),
        }
        return PAGINATION_SUBQUERY[sort]

    def _get_subquery_cursor_previous_page(
        self,
        last_result: str,
        last_id: int,
        sort: TransactionSortOrder,
    ):
        PAGINATION_SUBQUERY = {
            TransactionSortOrder.created_asc: sql.or_(
                sql.and_(
                    schema.Transaction.date_created == last_result,
                    schema.Transaction.id > last_id,
                ).self_group(),
                schema.Transaction.date_created < last_result,
            ),
            TransactionSortOrder.created_desc: sql.or_(
                sql.and_(
                    schema.Transaction.date_created == last_result,
                    schema.Transaction.id > last_id,
                ).self_group(),
                schema.Transaction.date_created > last_result,
            ),
        }
        return PAGINATION_SUBQUERY[sort]

    def _get_sort_order(
        self,
        alias=None,
        order: TransactionSortOrder = TransactionSortOrder.created_asc,
    ):
        table: Any = schema.Transaction if alias is None else alias.c

        SORT_ORDER: Final[dict[str, Any]] = {
            TransactionSortOrder.created_asc: [
                sql.asc(table.date_created),
                sql.asc(table.id),
            ],
            TransactionSortOrder.created_desc: [
                sql.desc(table.date_created),
                sql.desc(table.id),
            ],
        }

        return SORT_ORDER[order]
