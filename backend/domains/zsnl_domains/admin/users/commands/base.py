# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import TransactionRepository, UserRepository
from minty.validation import validate_with
from pkgutil import get_data
from typing import TypedDict, cast
from uuid import UUID


class UserListEntry(TypedDict):
    old_username: str
    new_username: str


class RenameUser(minty.cqrs.SplitCommandBase):
    name = "rename_user"

    @validate_with(get_data(__name__, "validation/rename_user.json"))
    def __call__(
        self,
        users: list[UserListEntry],
        integration_uuid: str,
        dry_run: bool = False,
    ):
        """
        Rename username with given name
        """
        user_repo = cast(UserRepository, self.get_repository("user"))
        transaction_repo = cast(
            TransactionRepository, self.get_repository("transaction")
        )
        input_data = {
            "users": users,
            "integration_uuid": integration_uuid,
            "dry_run": dry_run,
        }
        transaction = transaction_repo.create_transaction(
            integration_uuid=integration_uuid,
            input_data=str(input_data),
        )
        user_list = []

        for user_record in users:
            old_user = user_repo.find_user_by_name(
                name=user_record["old_username"],
                integration_uuid=UUID(integration_uuid),
            )
            new_user = user_repo.find_user_by_name(
                name=user_record["new_username"],
                integration_uuid=UUID(integration_uuid),
            )

            if old_user and (
                not new_user and user_record["new_username"] not in user_list
            ):
                old_user.rename(name=user_record["new_username"])
                user_list.append(user_record["new_username"])
                input_data = {
                    "user": user_record,
                    "integration_uuid": integration_uuid,
                    "dry_run": dry_run,
                }
                if dry_run:
                    output_data = f'Ran in dry run mode, user name "{user_record["old_username"]}" not updated to "{user_record["new_username"]}'
                else:
                    output_data = f'User name "{user_record["old_username"]}" successfully updated to "{user_record["new_username"]}"'

                transaction.add_transaction_record(
                    is_error=False,
                    input_data=str(input_data),
                    output_data=str(output_data),
                    preview_string="Renaming user",
                )

        user_repo.save(dry_run=dry_run)
        transaction_repo.save()
