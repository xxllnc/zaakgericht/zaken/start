# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class User(Entity):
    entity_type = "user"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Internal Identifier for the user")
    name: str = Field(..., title="Name of the user")
    integration_uuid: UUID = Field(
        "", title="Internal identifier of the interface to which user belongs"
    )

    @Entity.event(name="UserRenamed", fire_always=True)
    def rename(self, name: str):
        """Rename username entry."""
        self.name = name
