# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from datetime import datetime, timezone
from minty.cqrs import Event, UserInfo
from sqlalchemy import sql
from uuid import UUID, uuid4
from zsnl_domains.admin.users.entities import Transaction
from zsnl_domains.database import schema


class TransactionRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Transaction"
    _events_to_calls = {
        "TransactionCreated": "_create_transaction",
        "TransactionRecordAdded": "_add_transaction_record",
    }

    def create_transaction(self, integration_uuid: UUID, input_data: str):
        transaction_uuid = uuid4()
        transaction = Transaction(
            integration_uuid=integration_uuid,
            status=None,
            uuid=transaction_uuid,
            entity_id=transaction_uuid,
            transaction_records=[],
            input_data=None,
            _event_service=self.event_service,
        )
        transaction.create(
            integration_uuid=integration_uuid,
            input_data=input_data,
        )
        return transaction

    def _create_transaction(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        change = event.format_changes()

        date = datetime.now(timezone.utc)
        external_id = str(uuid4())
        self.session.execute(
            sql.insert(schema.Transaction).values(
                {
                    "uuid": event.entity_id,
                    "interface_id": 1,
                    "external_transaction_id": str(external_id),
                    "input_data": change["input_data"],
                    "date_created": date.strftime("%Y-%m-%dt%H:%M:%S"),
                    "date_last_retry": date.strftime("%Y-%m-%dt%H:%M:%S"),
                    "processed": True,
                    "direction": "incoming",
                    "text_vector": str(external_id),
                    "error_message": None,
                }
            )
        )

    def _add_transaction_record(
        self, event: Event, dry_run: bool = False, user_info: UserInfo = None
    ):
        change = event.format_changes()
        date = datetime.now(timezone.utc)
        transaction_id = self._get_transaction_id(event.entity_id)
        for transaction_record in change["transaction_records"]:
            self.session.execute(
                sql.insert(schema.TransactionRecord).values(
                    {
                        "transaction_id": transaction_id,
                        "input": str(transaction_record["input_data"]),
                        "output": str(transaction_record["output_data"]),
                        "is_error": False,
                        "date_executed": date.strftime("%Y-%m-%dt%H:%M:%S"),
                        "date_deleted": date.strftime("%Y-%m-%dt%H:%M:%S"),
                        "preview_string": transaction_record["preview_string"],
                    }
                )
            )

    def _get_transaction_id(self, transaction_uuid: UUID):
        query = sql.select(schema.Transaction.id).where(
            schema.Transaction.uuid == transaction_uuid
        )
        transaction_row = self.session.execute(query).fetchone()
        return transaction_row.id
