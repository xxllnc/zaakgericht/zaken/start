# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from minty.cqrs import Event, UserInfo
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.admin.users.entities import User
from zsnl_domains.database import schema


class UserRepository(ZaaksysteemRepositoryBase):
    _for_entity = "User"
    _events_to_calls = {
        "UserRenamed": "_rename_user",
    }

    def find_user_by_name(self, name: str, integration_uuid: UUID):
        user_qry = (
            sql.select(
                schema.UserEntity.uuid,
                schema.UserEntity.source_identifier.label("name"),
                schema.Interface.uuid.label("integration_uuid"),
            )
            .select_from(
                sql.join(
                    schema.UserEntity,
                    schema.Interface,
                    schema.UserEntity.source_interface_id
                    == schema.Interface.id,
                )
            )
            .where(
                sql.and_(
                    schema.UserEntity.source_interface_id
                    == sql.select(schema.Interface.id)
                    .where(schema.Interface.uuid == str(integration_uuid))
                    .scalar_subquery(),
                    schema.UserEntity.source_identifier == name,
                )
            )
        )
        user_row = self.session.execute(user_qry).fetchone()
        if not user_row:
            return None

        return User(
            entity_id=user_row.uuid,
            uuid=user_row.uuid,
            name=user_row.name,
            integration_uuid=user_row.integration_uuid,
            _event_service=self.event_service,
        )

    def _rename_user(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        change = event.format_changes()
        uuid = event.entity_id
        new_username = change["name"]
        if new_username:
            self.session.execute(
                sql.update(schema.UserEntity)
                .where(schema.UserEntity.uuid == uuid)
                .values({"source_identifier": new_username})
            ).execution_options(synchronize_session=False)
        if dry_run is True:
            self.session.rollback()
