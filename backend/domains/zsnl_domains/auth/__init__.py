# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .commands import AUTH_COMMANDS
from .queries import AUTH_QUERIES
from .repositories import (
    EventRepository,
    RoleRepository,
    SubjectLoginHistoryRepository,
    SubjectRepository,
    TokenResponseRepository,
    UserRepository,
)

REQUIRED_REPOSITORIES = {
    "event": EventRepository,
    "role": RoleRepository,
    "subject": SubjectRepository,
    "subject_login_history": SubjectLoginHistoryRepository,
    "token_response": TokenResponseRepository,
    "user": UserRepository,
}


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return minty.cqrs.DomainCommandContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
        command_lookup_table=AUTH_COMMANDS,
    )


def get_query_instance(repository_factory, context, user_uuid):
    return minty.cqrs.DomainQueryContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        query_lookup_table=AUTH_QUERIES,
    )
