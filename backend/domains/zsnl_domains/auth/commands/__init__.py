# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from . import event, subject, subject_login_history

AUTH_COMMANDS = minty.cqrs.build_command_lookup_table(
    commands={
        event.AddEventAuth0yInterfaceNotActive,
        event.AddEventAuth0yUserMarkedAsDeleted,
        event.AddEventAuth0UserNotActive,
        subject.CreateUser,
        subject_login_history.CreateSubjectLoginHistory,
    }
)
