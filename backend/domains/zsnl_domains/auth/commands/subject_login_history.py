# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty.cqrs
import minty.exceptions
from ..repositories import SubjectLoginHistoryRepository
from pydantic.v1 import validate_arguments
from typing import cast
from uuid import UUID

logger = logging.getLogger(__name__)


class CreateSubjectLoginHistory(minty.cqrs.SplitCommandBase):
    name = "create_subject_login_history"

    @validate_arguments
    def __call__(
        self,
        ip: str,
        subject_username: str,
        subject_id: int,
        subject_uuid: UUID,
        success: bool,
    ):
        logger.debug(
            f"Create a subject login history entry with ip {ip}, "
            f"subject_username {subject_username}, subject_id {subject_id}, "
            f"subject_uuid {subject_uuid} and success {success}"
        )

        repo = cast(
            SubjectLoginHistoryRepository,
            self.get_repository("subject_login_history"),
        )

        repo.create_subject_login_history(
            ip=ip,
            subject_username=subject_username,
            subject_id=subject_id,
            subject_uuid=subject_uuid,
            success=success,
        )

        repo.save()
