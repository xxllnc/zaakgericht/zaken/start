# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from typing import Self


class Role(Entity):
    "Role"

    permissions: list[str] | None = Field(
        default=None, title="Permissions of the role"
    )

    entity_type = "role"

    @classmethod
    def create(cls, **kwargs) -> Self:
        role: Self = cls(**kwargs)
        return role
