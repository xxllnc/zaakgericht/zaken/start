# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import date
from minty.entity import Entity
from pydantic.v1 import Field
from typing import Self
from uuid import UUID


class Subject(Entity):
    "Subject"

    entity_type = "subject"
    entity_id__fields: list[str] = ["uuid"]

    id: int | None = Field(default=None, title="Id of subject")
    uuid: UUID = Field(..., title="Internal identifier of subject")
    subject_type: str = Field(..., title="Type of subject")
    properties: dict | None = Field(
        default=None, title="Properties of subject"
    )
    settings: dict | None = Field(default=None, title="Settings of subject")
    username: str = Field(..., title="Username of subject")
    last_modified: date | None = Field(
        ..., title="Last modified date of subject"
    )
    role_ids: list[int] = Field(..., title="Role ids of subject")
    group_ids: list[int] = Field(..., title="Group ids of subject")
    nobody: bool = Field(..., title="nobody of subject")
    system: bool = Field(..., title="system of subject")

    @classmethod
    @Entity.event(name="Auth0UserCreated", fire_always=True)
    def create(cls, **kwargs) -> Self:
        subject: Self = cls(**kwargs)
        return subject
