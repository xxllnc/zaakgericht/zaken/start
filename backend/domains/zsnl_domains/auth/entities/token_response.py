# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from minty.entity import Entity
from pydantic.v1 import Field
from typing import Self


class TokenResponse(Entity):
    "TokenResponse"

    id_token: str = Field(..., title="The id token")
    access_token: str = Field(..., title="The access token")
    scope: str = Field(..., title="The scope of the token")
    expires_in: int = Field(..., title="The lifetime in seconds of the token")
    token_type: str = Field(..., title="The type of token")

    entity_type = "token_response"

    @classmethod
    def create(cls, **kwargs) -> Self:
        token_response: Self = cls(**kwargs)
        return token_response
