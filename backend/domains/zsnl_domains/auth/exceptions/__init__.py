# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from .exceptions import (
    Auth0Error,
    Auth0Warning,
    CreateSubjectLoginHistoryError,
    CreateUserByNameError,
    FindActiveInterfaceError,
    FindRolesError,
    FindUserByNameError,
    GetTokenError,
    GetTokenResponseError,
    GetTokenResponseJsonError,
    GetTokenResponseStatusError,
    IdTokenNotFoundError,
    InterfaceNotActiveWarning,
    NewUserNotFoundError,
    RolesNotFoundError,
    TokenTypeNotBearerError,
    TokenTypeNotSetError,
    UserMarkedAsDeletedWarning,
    UserNotActiveWarning,
)

__all__ = [
    "Auth0Error",
    "Auth0Warning",
    "CreateSubjectLoginHistoryError",
    "CreateUserByNameError",
    "FindActiveInterfaceError",
    "FindRolesError",
    "FindUserByNameError",
    "GetTokenError",
    "GetTokenResponseError",
    "GetTokenResponseJsonError",
    "GetTokenResponseStatusError",
    "IdTokenNotFoundError",
    "InterfaceNotActiveWarning",
    "NewUserNotFoundError",
    "RolesNotFoundError",
    "TokenTypeNotBearerError",
    "TokenTypeNotSetError",
    "UserMarkedAsDeletedWarning",
    "UserNotActiveWarning",
]
