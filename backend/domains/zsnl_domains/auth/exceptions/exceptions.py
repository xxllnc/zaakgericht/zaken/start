# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


class Auth0Error(Exception):
    """Base exception used by the auth module."""

    pass


class Auth0Warning(Warning):
    """Base warning used by the auth module."""

    pass


class FindUserByNameError(Auth0Error):
    """Raised when there is something wrong finding the user by name."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"Find user by name '{name}' failed.")


class CreateUserByNameError(Auth0Error):
    """Raised when there is something wrong creating a new user by name."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"Create user by name '{name}' failed.")


class NewUserNotFoundError(Auth0Error):
    """Raised when the newly created user cannot be found."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"Cannot find new created user with name '{name}'.")


class UserNotActiveWarning(Auth0Warning):
    """Warned when the user is not active."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"User with name '{name}' is not active.")


class UserMarkedAsDeletedWarning(Auth0Warning):
    """Warned when the user is marked as deleted."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"User with name '{name}' is marked as deleted.")


class FindRolesError(Auth0Error):
    """Raised when there is something wrong finding roles based on ids."""

    def __init__(self, ids):
        self.ids = ids
        super().__init__(f"Find roles by role ids '{ids}' failed.")


class RolesNotFoundError(Auth0Error):
    """Raised when there are no roles found based on ids."""

    def __init__(self, ids):
        self.ids = ids
        super().__init__(f"No roles found based on ids {ids}.")


class GetTokenError(Auth0Error):
    """Raised when there is something wrong exchanging an code for a token."""

    def __init__(self, url, data):
        self.url = url
        self.data = data
        super().__init__(
            f"Exchange an authorization code for a token failed. "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class GetTokenResponseJsonError(Auth0Error):
    """Raised when the response body does not contain valid json."""

    def __init__(self, url, data):
        self.url = url
        self.data = data
        super().__init__(
            f"Response body does not contain valid json content. "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class GetTokenResponseStatusError(Auth0Error):
    """Raised when the response status code is not 200 (OK)."""

    def __init__(self, url, data):
        self.url = url
        self.data = data
        super().__init__(
            f"Response status code is not 200 (OK). "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class GetTokenResponseError(Auth0Error):
    """Raised when the response contains an error."""

    def __init__(self, url, data, payload):
        self.url = url
        self.data = data
        self.payload = payload
        super().__init__(
            f"Response body contains error '{payload}'. "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class TokenTypeNotSetError(Auth0Error):
    """Raised when the token type is not set."""

    def __init__(self, url, data, payload):
        self.url = url
        self.data = data
        self.payload = payload
        super().__init__(
            f"Token type is not set '{payload}'. "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class TokenTypeNotBearerError(Auth0Error):
    """Raised when the token type is not Bearer."""

    def __init__(self, url, data, payload):
        self.url = url
        self.data = data
        self.payload = payload
        super().__init__(
            f"Token type is not bearer '{payload}'. "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class IdTokenNotFoundError(Auth0Error):
    """Raised when no ID token is found."""

    def __init__(self, url, data, payload):
        self.url = url
        self.data = data
        self.payload = payload
        super().__init__(
            f"No ID token found '{payload}'. "
            f"Post token url '{url}' with request parameters '{data}'."
        )


class FindActiveInterfaceError(Auth0Error):
    """Raised when there is something wrong finding the active interface."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"Find active interface with name '{name}' failed.")


class InterfaceNotActiveWarning(Auth0Warning):
    """Warned when the interface does not exist or is not active."""

    def __init__(self, name):
        self.name = name
        super().__init__(
            f"Interface with name '{name}' does not exist or is not active."
        )


class CreateSubjectLoginHistoryError(Auth0Error):
    """Raised when there is something wrong creating a new subject login
    history record."""

    def __init__(self, name):
        self.name = name
        super().__init__(
            f"Create subject login history for user '{name}' failed."
        )
