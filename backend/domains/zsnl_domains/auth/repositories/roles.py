# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import Role
from ..exceptions import FindRolesError, RolesNotFoundError
from minty.entity import EntityCollection
from sqlalchemy import sql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from zsnl_domains.database import schema

role_query = sql.select(
    schema.Role.permissions,
)


class RoleRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Roles"
    _events_to_calls = {}

    def get_permissions_for_roles(
        self, role_ids: list[str]
    ) -> EntityCollection[Role] | None:
        """Find all roles with their permissions based on a list of role ids.

        Args:
            role_ids (list(str)): The list of role ids

        Returns:
            List or Roles if roles are found
            None when role ids are not specified.

        Raises:
            FindRolesError: If finding roles fails.
            RolesNotFoundError: If no role is found.
        """

        self.logger.debug(
            "Find all roles with their permissions based on"
            f"a list of role ids {role_ids}"
        )

        if not role_ids:
            self.logger.debug("No role ids specified.")
            return None

        try:
            qry_stmt = role_query.where(schema.Role.id.in_(role_ids))
            role_rows = self.session.execute(qry_stmt).fetchall()
        except (SQLAlchemyError, DBAPIError) as err:
            raise FindRolesError(role_ids) from err

        if not role_rows:
            raise RolesNotFoundError(role_ids)

        return EntityCollection[Role](
            [self._entity_from_row(row=r) for r in role_rows]
        )

    def _entity_from_row(self, row) -> Role:
        role = Role(
            permissions=row.permissions,
        )
        return role
