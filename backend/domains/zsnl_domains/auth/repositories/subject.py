# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import Subject
from ..exceptions import CreateUserByNameError
from datetime import datetime, timezone
from minty.cqrs import Event, UserInfo
from sqlalchemy import sql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from uuid import uuid4
from zsnl_domains.database import schema


class SubjectRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Subject"
    _events_to_calls = {
        "Auth0UserCreated": "_create_subject",
    }

    def create_user(self, username: str) -> Subject:
        """Create an user of type employee.

        Args:
            username (str): The username

        Returns:
            Subject

        Raises:
            CreateUserByNameError: If creating a new user fails.
        """

        self.logger.debug(
            f"Create an user of type employee with username {username}"
        )

        new_subject_uuid: uuid4 = uuid4()
        new_last_modified: datetime = datetime.now(timezone.utc)

        subject: Subject = Subject(
            uuid=new_subject_uuid,
            username=username,
            subject_type="employee",
            last_modified=new_last_modified,
            group_ids=[],
            role_ids=[],
            nobody=False,
            system=False,
        )
        subject.create(
            uuid=new_subject_uuid,
            username=username,
            subject_type="employee",
            last_modified=new_last_modified,
            group_ids=[],
            role_ids=[],
            nobody=False,
            system=False,
            _event_service=self.event_service,
        )
        return subject

    def _get_auth0_interface_query(self):
        """Get the auth0 interface id.
        The interface must be active and not deleted.
        """
        return (
            sql.select(
                schema.Interface.id,
            )
            .select_from(
                schema.Interface,
            )
            .where(
                sql.and_(
                    schema.Interface.module == "authldap",
                    schema.Interface.active == "true",
                    schema.Interface.date_deleted.is_(None),
                )
            )
        )

    def _create_subject(
        self, event: Event, user_info: UserInfo, dry_run: bool = False
    ):
        changes = event.format_changes()

        try:
            interface_id = self.session.execute(
                self._get_auth0_interface_query()
            ).fetchone()

            role_row = self.session.execute(
                sql.select(schema.Role.id).where(
                    schema.Role.name == "Behandelaar"
                )
            ).fetchone()
            role_id = [role_row.id]

            subject_id = self.session.execute(
                sql.insert(schema.Subject)
                .values(
                    uuid=changes.get("uuid"),
                    username=changes.get("username"),
                    subject_type=changes.get("subject_type"),
                    last_modified=changes.get("last_modified"),
                    group_ids=changes.get("group_ids"),
                    role_ids=role_id,
                    nobody=changes.get("nobody"),
                    system=changes.get("system"),
                )
                .returning(schema.Subject.id)
            ).fetchone()

            self.session.execute(
                sql.insert(schema.UserEntity).values(
                    uuid=changes.get("uuid"),
                    subject_id=subject_id.id,
                    source_interface_id=interface_id.id,
                    source_identifier=changes.get("username"),
                    date_created=datetime.now(timezone.utc),
                    active=True,
                    properties="{}",
                )
            )
        except (SQLAlchemyError, DBAPIError) as err:
            raise CreateUserByNameError(changes.get("username")) from err
