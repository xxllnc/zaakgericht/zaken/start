# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import requests
from ... import ZaaksysteemRepositoryBase
from ..entities import TokenResponse
from ..exceptions import (
    GetTokenError,
    GetTokenResponseError,
    GetTokenResponseJsonError,
    GetTokenResponseStatusError,
    IdTokenNotFoundError,
    TokenTypeNotBearerError,
    TokenTypeNotSetError,
)
from requests.exceptions import JSONDecodeError as RequestsJSONDecodeError
from requests.exceptions import RequestException
from typing import cast


class TokenResponseRepository(ZaaksysteemRepositoryBase):
    _for_entity = "TokenResponse"
    _events_to_calls = {}

    def get_token(
        self,
        code: str,
        hostname: str,
        client_id: str,
        client_secret: str,
        domain: str,
    ) -> TokenResponse:
        """
        Use the Auth0 Authorization Code Flow to exchange an Authorization
        Code for a Token.

        Args:
            code (str): The Authorization Code received from the authorize call
            hostname (str): The hostname to redirect to
            client_id (str): The application's client ID
            client_secret (str): The application's client secret
            domain (str): The domain of the Auth0 tenant

        Returns:
            TokenResponse

        Raises:
            GetTokenError: If exchanging code for a token fails.
            GetTokenResponseJsonError: If response body does not contain valid
                                       json.
            GetTokenResponseStatusError: If response is not HTTP status 200.
            GetTokenResponseError: If response contains an error.
            TokenTypeNotSetError: If token type is not set.
            TokenTypeNotBearerError: If token type is not Bearer.
            IdTokenNotFoundError: If no ID token is found.
        """

        self.logger.debug(
            f"Exchange code '{code}' for token. Hostname = {hostname}"
            f", client_id = {client_id}, client_secret = {client_secret}"
            f"and domain = {domain}"
        )

        oauth_token_url: str = f"https://{domain}/oauth/token"
        redirect_uri: str = f"https://{hostname}/"

        oauth_token_data: dict = {
            "grant_type": "authorization_code",
            "code": code,
            "client_id": client_id,
            "client_secret": client_secret,
            "redirect_uri": redirect_uri,
        }

        try:
            response = requests.post(
                url=oauth_token_url,
                data=oauth_token_data,
                verify=True,
                timeout=30,
            )

        except RequestException as re:
            raise GetTokenError(
                url=oauth_token_url, data=oauth_token_data
            ) from re

        if response.status_code != 200:
            raise GetTokenResponseStatusError(
                url=oauth_token_url, data=oauth_token_data
            )

        try:
            response_data = response.json()
        except (RequestsJSONDecodeError, ValueError) as ve:
            raise GetTokenResponseJsonError(
                url=oauth_token_url, data=oauth_token_data
            ) from ve

        self.logger.debug(f"Response payload {response_data}")

        if "error" in response_data:
            raise GetTokenResponseError(
                url=oauth_token_url,
                data=oauth_token_data,
                payload=response_data,
            )

        if not response_data.get("token_type", None):
            raise TokenTypeNotSetError(
                url=oauth_token_url,
                data=oauth_token_data,
                payload=response_data,
            )

        if response_data.get("token_type").lower() != "bearer":
            raise TokenTypeNotBearerError(
                url=oauth_token_url,
                data=oauth_token_data,
                payload=response_data,
            )

        if not response_data.get("id_token", None):
            raise IdTokenNotFoundError(
                url=oauth_token_url,
                data=oauth_token_data,
                payload=response_data,
            )

        return cast(
            TokenResponse,
            TokenResponse.create(
                id_token=response_data["id_token"],
                access_token=response_data["access_token"],
                scope=response_data["scope"],
                expires_in=response_data["expires_in"],
                token_type=response_data["token_type"],
            ),
        )
