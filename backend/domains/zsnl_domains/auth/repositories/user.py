# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import User
from ..exceptions import (
    FindActiveInterfaceError,
    FindUserByNameError,
    InterfaceNotActiveWarning,
)
from sqlalchemy import sql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from typing import cast
from zsnl_domains.database import schema


class UserRepository(ZaaksysteemRepositoryBase):
    _for_entity = "User"
    _events_to_calls = {}

    def _get_auth0_interface_query(self):
        """Get the auth0 interface id.
        The interface must be active and not deleted.
        """
        return (
            sql.select(
                schema.Interface.id,
            )
            .select_from(
                schema.Interface,
            )
            .where(
                sql.and_(
                    schema.Interface.module == "authldap",
                    schema.Interface.active == "true",
                    schema.Interface.date_deleted.is_(None),
                )
            )
        )

    def find_user_by_name(self, name: str) -> User | None:
        """Find the user by name.
        The user must be an employee and must belong to the authldap interface.
        Search for both active and inactive employees.
        It also searches for deleted employees.

        Args:
            name (str): The username

        Returns:
            User if user is found.
            None if user is not found.

        Raises:
            FindUserByNameError: If finding user by name fails.
        """

        self.logger.debug(f"Find user by name {name}")

        try:
            user_qry = (
                sql.select(
                    schema.UserEntity.id.label("user_id"),
                    schema.Subject.id.label("subject_id"),
                    schema.Subject.uuid,
                    schema.Subject.role_ids,
                    schema.Subject.username.label("name"),
                    schema.UserEntity.active,
                    schema.UserEntity.date_deleted,
                )
                .select_from(
                    sql.join(
                        schema.Subject,
                        schema.UserEntity,
                        schema.UserEntity.subject_id == schema.Subject.id,
                    )
                )
                .where(
                    sql.and_(
                        schema.Subject.username.ilike(f"{name}"),
                        schema.Subject.subject_type == "employee",
                        schema.UserEntity.source_interface_id
                        == self._get_auth0_interface_query().scalar_subquery(),
                    )
                )
            )

            user_row = self.session.execute(user_qry).fetchone()
        except (SQLAlchemyError, DBAPIError) as err:
            raise FindUserByNameError(name) from err

        if not user_row:
            return None

        return cast(
            User,
            User.create(
                id=user_row.subject_id,
                uuid=user_row.uuid,
                name=user_row.name,
                login_entity_id=user_row.user_id,
                role_ids=user_row.role_ids,
                active=user_row.active,
                date_deleted=user_row.date_deleted,
            ),
        )

    def find_active_interface(self) -> None:
        """Check if an active interface exists.

        Raises:
            FindActiveInterfaceError: If finding active interface fails.
            InterfaceNotActiveWarning: If interface does not exist or is not
                                       active.
        """

        self.logger.debug("Check if active interface exists.")

        try:
            interface_qry = (
                sql.select(
                    schema.Interface.id,
                )
                .select_from(
                    schema.Interface,
                )
                .where(
                    sql.and_(
                        schema.Interface.module == "authldap",
                        schema.Interface.active == "true",
                        schema.Interface.date_deleted.is_(None),
                    )
                )
            )

            interface_row = self.session.execute(interface_qry).fetchone()
        except (SQLAlchemyError, DBAPIError) as err:
            raise FindActiveInterfaceError(name="authldap") from err

        if not interface_row:
            raise InterfaceNotActiveWarning(name="authldap")
