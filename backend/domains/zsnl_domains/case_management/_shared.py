# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
from ..shared.entities.case import (
    ValidCaseArchivalState,
)
from .entities import Employee, Organization, Person
from .entities._shared import CaseStatus
from .entities.person import PersonLimited
from .repositories import (
    CaseLocationRepository,
    CaseTypeRepository,
    CaseTypeResultRepository,
    EmployeeRepository,
    OrganizationRepository,
    PersonRepository,
    RoleRepository,
    SavedSearchLabelRepository,
)
from collections.abc import Callable
from datetime import datetime, timezone
from typing import Final, cast
from uuid import UUID


def get_subject(
    get_repository: Callable, subject_type: str, subject_uuid: UUID
) -> Person | Organization | Employee:
    person_repo = cast(PersonRepository, get_repository("person"))
    organization_repo = cast(
        OrganizationRepository, get_repository("organization")
    )
    employee_repo = cast(EmployeeRepository, get_repository("employee"))

    if subject_type == "subject":
        # Sometimes, relations are stored as "subject" instead of the actual
        # type.
        # In that case we need to figure out what kind of relationship it
        # actually is by trying all of them.
        persons = person_repo.get_persons_by_uuid(
            uuids=(subject_uuid,),
        )
        organizations = organization_repo.get_organizations_by_uuid(
            uuids=(subject_uuid,),
        )
        employees = employee_repo.get_employees_by_uuid(
            uuids=(subject_uuid,),
        )

        if persons or organizations or employees:
            return [*persons, *organizations, *employees][0]

        raise minty.exceptions.NotFound(
            "Contact with uuid '{uuid}' not found.", "contact/not_found"
        )

    elif subject_type == "person":
        subject = person_repo.find_person_by_uuid(subject_uuid)
    elif subject_type == "organization":
        subject = organization_repo.find_organization_by_uuid(subject_uuid)
    elif subject_type == "employee":
        subject = employee_repo.find_employee_by_uuid(subject_uuid)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subject


def get_subject_limited(
    get_repository: Callable, subject_type: str, subject_uuid: UUID
) -> PersonLimited | Organization | Employee:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subject = subject_repo.find_person_by_uuid_limited(subject_uuid)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subject = subject_repo.find_organization_by_uuid_limited(subject_uuid)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subject = subject_repo.find_employee_by_uuid_limited(subject_uuid)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subject


def get_subjects(
    get_repository: Callable, subject_type: str, subject_uuids: list[UUID]
) -> list[Person] | list[Organization] | list[Employee]:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subjects = subject_repo.get_persons_by_uuid(subject_uuids)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subjects = subject_repo.get_organizations_by_uuid(subject_uuids)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subjects = subject_repo.get_employees_by_uuid(subject_uuids)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subjects


def delete_unused_saved_search_labels(get_repository: Callable):
    saved_search_label_repo = cast(
        SavedSearchLabelRepository,
        get_repository("saved_search_label"),
    )
    unused_labels = saved_search_label_repo.get_unused_lables()
    if unused_labels:
        for label in unused_labels:
            label.delete()
        saved_search_label_repo.save()


def get_case_types(get_repository: Callable, case_type_version_uuids: list):
    case_type_repo = cast(CaseTypeRepository, get_repository("case_type"))
    case_types = case_type_repo.find_case_types_by_list_uuids(
        case_type_version_uuids
    )
    return case_types


def get_case_type_result(
    get_repository: Callable, case_type_result_uuids: list
):
    case_type_result_repo = cast(
        CaseTypeResultRepository, get_repository("case_type_result")
    )
    case_type_results = (
        case_type_result_repo.find_case_type_result_by_list_uuids(
            case_type_result_uuids
        )
    )
    return case_type_results


def get_case_locations(get_repository: Callable, case_location_uuids: list):
    case_location_repo = cast(
        CaseLocationRepository, get_repository("case_location")
    )
    case_locations = case_location_repo.find_case_location_by_list_uuids(
        case_location_uuids
    )
    return case_locations


def get_case_role_department(get_repository: Callable, role_uuid: list[UUID]):
    case_role_repo = cast(RoleRepository, get_repository("role"))

    case_role_department = case_role_repo.find_by_uuid_list(
        role_uuid=role_uuid
    )
    return case_role_department


def check_case_can_delete(
    case, case_repo, object_relationship_repo, check_warnings: bool = False
) -> str | None:
    parent_case_data = None

    if not case.status == CaseStatus.resolved:
        raise minty.exceptions.Conflict(
            f"Only resolved cases can be deleted (but status={case.status}).",
            "case/delete_case/status_not_allowed",
        )
    if not case.archival_state == ValidCaseArchivalState.vernietigen:
        raise minty.exceptions.Conflict(
            f"Only cases with archival status 'vernietigen' can be deleted (but archival status={case.archival_state}).",
            "case/delete_case/archival_status_not_allowed",
        )
    if (not case.destruction_date) or (
        not case.destruction_date < datetime.now(timezone.utc).date()
    ):
        raise minty.exceptions.Conflict(
            "Destruction date has not passed",
            "case/delete_case/destruction_date_not_passed",
        )

    # check children:
    if case.child_uuids:
        child_case_uuids = case.child_uuids.split(",")
        child_cases = case_repo.get_status_destruction_date_by_case_uuids(
            case_uuids=child_case_uuids
        )
        for child_case in child_cases:
            if (child_case.status != CaseStatus.resolved) or (
                child_case.destruction_date
                and child_case.destruction_date > datetime.now(timezone.utc)
            ):
                raise minty.exceptions.Conflict(
                    "Subcase not resolved or subcase destruction date has not passed.",
                    "case/delete_case/subcase_not_allowed",
                )

    # check parent case
    if case.parent_uuid:
        parent_case_data = case_repo.get_status_destruction_date_by_case_uuids(
            case_uuids=[case.parent_uuid]
        )[0]

        if (
            parent_case_data
            and not parent_case_data.status == CaseStatus.resolved
        ):
            raise minty.exceptions.Conflict(
                "Parent case not resolved. Case can not be deleted.",
                "case/delete_case/parent_case_not_resolved",
            )

    if check_warnings:
        return _check_case_delete_warnings(
            case, parent_case_data, case_repo, object_relationship_repo
        )


def _check_case_delete_warnings(
    case, parent_case_data, case_repo, object_relationship_repo
) -> str:
    warnings: list[str] = []

    # check parent destruction date
    if (
        parent_case_data
        and parent_case_data.destruction_date
        and parent_case_data.destruction_date > datetime.now(timezone.utc)
    ):
        warnings.append("Destruction date of main case has not passed.")

    # check has related custom objects
    related_custom_objects = (
        object_relationship_repo.find_object_relations_for_case_basic(
            case_uuid=case.uuid
        )
    )
    if related_custom_objects:
        warnings.append("Case has object relations")

    # check for case relations
    if case.related_uuids:
        related_cases_uuids = case.related_uuids.split(",")
        related_cases_data = (
            case_repo.get_status_destruction_date_by_case_uuids(
                case_uuids=related_cases_uuids
            )
        )
        status_check_failed = False
        destruction_date_check_failed = False

        now = datetime.now(timezone.utc)

        for related_case_data in related_cases_data:
            if (
                related_case_data.status != CaseStatus.resolved
                and not status_check_failed
            ):
                warnings.append("Status of related case not resolved")
                status_check_failed = True
            elif (
                (related_case_data.destruction_date is None)
                or (related_case_data.destruction_date > now)
            ) and not destruction_date_check_failed:
                warnings.append(
                    "Destruction date of related case has not passed."
                )
                destruction_date_check_failed = True
            if status_check_failed and destruction_date_check_failed:
                break

    return "\n".join(warnings)


NOTIFICATION_TYPE_ACTION_CONFIG_MAP: Final = {
    "new_document": "new_document",
    "send_case_assignee_email": "new_assigned_case",
}
