# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
import minty.cqrs
import minty.exceptions
from ...shared import zs_context_vars
from .._shared import (
    NOTIFICATION_TYPE_ACTION_CONFIG_MAP,
    check_case_can_delete,
    get_subject,
)
from ..entities import (
    CaseContact,
    CaseContactEmployee,
    CaseTypeVersionEntity,
    Department,
    Role,
    StaticRuleAction,
    Subject,
)
from ..repositories import (
    CaseRepository,
    CaseTypeRepository,
    CaseTypeResultRepository,
    CustomObjectRepository,
    DepartmentRepository,
    EmailTemplateRepository,
    EmployeeRepository,
    ObjectRelationRepository,
    RoleRepository,
    SubjectRelationRepository,
    SubjectRepository,
)
from ..services import CaseTemplateService, RuleEngine
from abc import ABC, abstractmethod
from collections.abc import Callable
from datetime import date
from minty.cqrs import UserInfo
from minty.entity import ValueObject
from minty.exceptions import Forbidden, NotFound
from minty.validation import validate_with
from pkgutil import get_data
from pydantic.v1 import Field, validate_arguments
from typing import Literal, cast
from uuid import UUID
from zsnl_domains.case_management.entities.case import (
    ValidCaseDestructionDateType,
)


class UserNameProvider:
    def __init__(self, get_repository: Callable, user_uuid: UUID):
        self.logger = logging.getLogger()
        self.get_repository = get_repository
        self.user_uuid = user_uuid
        self.subject_cache: dict[
            str, Subject | None
        ] = {}  # cache for username dict (user_uuid : subject)

    def _get_user_name(self) -> str:
        if "user_uuid" not in self.subject_cache:
            try:
                # cache the subject so the amount of queries is reduced for retrieving the username
                self.subject_cache["user_uuid"] = cast(
                    SubjectRepository, self.get_repository("subject")
                ).find_subject_by_uuid(self.user_uuid)
            except minty.exceptions.NotFound:
                # fill with None to prevent many find_subject_by_uuid calls in this thread
                self.logger.debug(
                    f"No subject found for user_uuid {self.user_uuid}"
                )
                self.subject_cache["user_uuid"] = None
        return (
            self.subject_cache["user_uuid"].username
            if self.subject_cache["user_uuid"]
            else ""
        )


def init_template_service(func):
    def wrapper(*args, **kwargs):
        command = args[0]
        if isinstance(command, minty.cqrs.SplitCommandBase):
            contextvar_key = None
            try:
                # contextvar.get() throws an LookupError if no value present
                zs_context_vars.template_service.get()
            except LookupError:
                contextvar_key = zs_context_vars.template_service.set(
                    CaseTemplateService(
                        UserNameProvider(
                            command.get_repository, command.user_uuid
                        )._get_user_name
                    )
                )
            try:
                func(*args, **kwargs)
            finally:
                if contextvar_key:  # pragma: no cover
                    zs_context_vars.template_service.reset(contextvar_key)
                    contextvar_key = None

        else:
            raise NotImplementedError(
                "This decorator can only be applied on a SplitCommandBase class function"
            )

    return wrapper


def init_rule_engine(func):
    def wrapper(*args, **kwargs):
        command = args[0]
        if isinstance(command, Create):
            contextvar_key = None
            try:
                # contextvar.get() throws an LookupError if no value present
                zs_context_vars.rule_engine.get()
            except LookupError:
                contextvar_key = zs_context_vars.rule_engine.set(
                    RuleEngine(
                        allowed_actions=command.get_allowed_ruleactions()
                    )
                )
            try:
                func(*args, **kwargs)
            finally:
                if contextvar_key:
                    zs_context_vars.rule_engine.reset(contextvar_key)
                    contextvar_key = None
        else:
            # just perform the function
            func(*args, **kwargs)

    return wrapper


class CaseCommandBase(minty.cqrs.SplitCommandBase, ABC):
    @init_rule_engine
    @init_template_service
    def __call__(self, *args, **kwargs):
        self.call(*args, **kwargs)

    @abstractmethod
    def call(self, *args, **kwargs) -> None:
        raise NotImplementedError

    def get_allowed_ruleactions(self) -> list[StaticRuleAction]:
        """
        By default the ruleactions below are allowed to be preformed.
        Overwrite this function when other ruleactions are allowed.
        """
        return StaticRuleAction.get_default_actions()


class SetRegistrationDate(CaseCommandBase):
    name = "set_case_registration_date"

    @validate_with(get_data(__name__, "validation/change_case_date.json"))
    def call(
        self,
        case_uuid: str,
        target_date: str,
    ):
        """
        Set case registration date for given case id and date.

        :param case_uuid: case uuid
        :param target_date: date to change registration date to
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        case.set_registration_date(
            registration_date=target_date,
            recalculate_target_completiondate=False,
        )
        repo.save()


class SetTargetCompletionDate(CaseCommandBase):
    name = "set_case_target_completion_date"

    @validate_with(get_data(__name__, "validation/change_case_date.json"))
    def call(self, case_uuid: str, target_date: str):
        """
        Set case target completion date for given case id and target date.

        :param case_uuid: case uuid
        :param target_date: new target completion date
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.set_target_completion_date(
            target_completion_date=target_date,
        )
        repo.save()


class SetCompletionDate(CaseCommandBase):
    name = "set_case_completion_date"

    @validate_with(get_data(__name__, "validation/change_case_date.json"))
    def call(self, case_uuid: str, target_date: str):
        """
        Set completion date of completed case.

        :param case_uuid: case uuid
        :param target_date: new completion date
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.set_completion_date(
            completion_date=target_date,
        )
        repo.save()


class AddValidsignTimelineEntry(CaseCommandBase):
    name = "add_validsign_timeline_entry"

    @validate_arguments
    def call(
        self,
        case_uuid: str,
        participants: list[dict] | None,
    ):
        """
        Set completion date of completed case.

        :param case_uuid: uuid of the case the item is added to
        :param form_id: id of the form
        :param integration_id: the id of the intergration used
        :param name: Name of the signing request
        :param participants: the data of those signing
        :param documents: the documents being signed
        """

        repo = cast(CaseRepository, self.get_repository("case"))

        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        case.add_validsign_timeline_entry(participants=participants)


class Pause(CaseCommandBase):
    name = "pause_case"

    @validate_with(get_data(__name__, "validation/pause_case.json"))
    def call(
        self,
        case_uuid: str,
        suspension_reason: str,
        suspension_term_type: str,
        suspension_term_value=None,
    ):
        """Pause case until given date and set suspension reason.

        :param case_uuid: case uuid
        :param suspension_reason: reason for suspension
        :param suspension_term_value: int or string or None the type of the suspension
        :param suspension_term_type: String or None the type of the suspension
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.pause(
            suspension_reason=suspension_reason,
            suspension_term_value=suspension_term_value,
            suspension_term_type=suspension_term_type,
        )
        repo.save()


class Resume(CaseCommandBase):
    name = "resume_case"

    @validate_with(get_data(__name__, "validation/resume_case.json"))
    def call(
        self,
        case_uuid: str,
        resume_reason: str,
        stalled_until_date: str,
        stalled_since_date: str,
    ):
        """
        Resume stalled case and calculate new target completion date.

        :param case_uuid: case uuid
        :param resume_reason: Reason for the resume.
        :param stalled_until_date: stalled until date
        :param stalled_since_date: stalled since date
        """
        repo = cast(CaseRepository, self.get_repository("case"))

        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.resume(
            stalled_since_date=stalled_since_date,
            stalled_until_date=stalled_until_date,
            resume_reason=resume_reason,
        )
        repo.save()


class AssignToDepartment(CaseCommandBase):
    name = "assign_case_to_department"

    @validate_with(
        get_data(__name__, "validation/assign_case_to_department.json")
    )
    def call(
        self,
        case_uuid: str,
        department_uuid: str,
        role_uuid: str,
    ):
        """
        Assign case to given department and role.

        :param case_uuid: str of the case
        :param department_uuid: str of the department
        :param role_uuid: str of the role
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        department_repo = cast(
            DepartmentRepository, self.get_repository("department")
        )
        department_entity = department_repo.find(UUID(department_uuid))

        role_repo = cast(RoleRepository, self.get_repository("role"))
        role_entity = role_repo.find(UUID(role_uuid))

        if role_entity or department_entity:
            case.clear_assignee()
            case.set_allocation(
                department=department_entity,
                role=role_entity,
            )

        case_repo.save()


class AssignToUser(CaseCommandBase):
    name = "assign_case_to_user"

    @validate_arguments
    def call(
        self,
        case_uuid: UUID,
        user_uuid: UUID,
    ):
        """
        Assign case to given user.
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))

        subject = case_repo.get_contact_employee(uuid=user_uuid)

        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="write",
        )

        # only do the check if not assigning the case to yourself
        if user_uuid != self.cmd.user_info.user_uuid:
            employee_repo = cast(
                EmployeeRepository, self.get_repository("employee")
            )

            employee = employee_repo.find_employee_by_uuid(uuid=user_uuid)
            isAdmin = False

            for role in employee.roles:
                if (
                    role.entity_meta_summary == "Administrator"
                    or role.entity_meta_summary == "Zaaksysteembeheerder"
                ):
                    isAdmin = True
                    break

            if not isAdmin:
                user_info_new_user = UserInfo(
                    user_uuid=user_uuid,
                    permissions={"admin": False},
                )

                try:
                    case_repo.find_case_by_uuid(
                        case_uuid=case_uuid,
                        user_info=user_info_new_user,
                        permission="write",
                    )
                except NotFound as e:
                    raise Forbidden(
                        "Assigned user is not allowed to read the case.",
                        "case/assign_case_to_user/not_allowed",
                    ) from e

        case.set_assignee(assignee=subject)

        if not case.coordinator:
            case.set_coordinator(coordinator=subject)

        case.set_status(status="open")

        self.cmd.reassign_case_messages(
            case_uuid=case_uuid,
            subject_uuid=user_uuid,
        )

        case_repo.save()


class AssignToSelf(CaseCommandBase):
    name = "assign_case_to_self"

    @validate_arguments
    def call(self, case_uuid: UUID):
        """
        Assign case to self. Special case of "Assign to user".

        :param case_uuid: case uuid
        """

        self.cmd.assign_case_to_user(
            case_uuid=case_uuid, user_uuid=self.user_uuid
        )


class ChangeCoordinator(CaseCommandBase):
    name = "change_case_coordinator"

    @validate_with(
        get_data(__name__, "validation/change_case_coordinator.json")
    )
    def call(self, case_uuid: str, coordinator_uuid: str):
        """
        Changes a coordinator of a case.

        :param case_uuid: UUID of the chase
        :param coordinator_uuid: UUID of the new coordinator (must be employee)
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))
        coordinator_entity = case_repo.get_contact_employee(
            uuid=UUID(coordinator_uuid)
        )

        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        case.set_coordinator(coordinator=coordinator_entity)

        case_repo.save()


class Create(CaseCommandBase):
    name = "create_case"

    def _get_assignee_entity(
        self, case_type_version: CaseTypeVersionEntity, assignee_uuid, type
    ) -> CaseContactEmployee | None:
        """
        Get assignee entity for case creation.

        :param case_type: case type for the case
        :param assignee_uuid: assignee for the case
        :return: assignee for the case as subject entity
        """
        if not assignee_uuid and case_type_version.preset_assignee:
            assignee_uuid = case_type_version.preset_assignee["id"]

        assignee_entity = None
        if assignee_uuid:
            case_repo = cast(CaseRepository, self.get_repository("case"))
            if type == "employee":
                assignee_entity = case_repo.get_contact_employee(
                    uuid=assignee_uuid
                )
            elif type == "person":
                assignee_entity = case_repo.get_contact_person(
                    uuid=assignee_uuid
                )
            elif type == "organization":
                assignee_entity = case_repo.get_contact_organization(
                    uuid=assignee_uuid
                )

        return assignee_entity

    def _get_department_entity(
        self,
        case_type_version: CaseTypeVersionEntity,
        department_uuid: UUID | None,
        use_assignee_department=False,
        assignee_entity: CaseContact | None = None,
    ) -> Department | None:
        """
        Get department entity for case creation.

        :param case_type: case type for the case
        :param department_uuid: uuid of department
        :return: department for the case as Department entity
        """
        if use_assignee_department and assignee_entity:
            return assignee_entity.department

        if not department_uuid and case_type_version.phases[0].allocation:
            department_uuid = case_type_version.phases[
                0
            ].allocation.department.uuid

        if department_uuid:
            department_repo = cast(
                DepartmentRepository, self.get_repository("department")
            )
            department_entity = department_repo.find(department_uuid)
            return department_entity

        return None

    def _get_role_entity(
        self,
        case_type_version: CaseTypeVersionEntity,
        role_uuid: UUID | None,
    ) -> Role | None:
        """Get role entity for case.

        :param case_type_version: case type of case
        :param role_uuid: uuid of the role
        :return: role for case as Role entity
        """
        if not role_uuid and case_type_version.phases[0].allocation:
            role_uuid = case_type_version.phases[0].allocation.role.uuid

        if role_uuid:
            role_repo = cast(RoleRepository, self.get_repository("role"))
            role_entity = role_repo.find(role_uuid)
            return role_entity

        return None

    def _get_requestor_entity(
        self, requestor_uuid: UUID | None, requstor_type: str
    ) -> CaseContact | None:
        """Get requestor entity for case."""
        requestor_entity = None
        if requestor_uuid:
            case_repo = cast(CaseRepository, self.get_repository("case"))
            if requstor_type == "employee":
                requestor_entity = case_repo.get_contact_employee(
                    uuid=requestor_uuid
                )
            elif requstor_type == "person":
                requestor_entity = case_repo.get_contact_person(
                    uuid=requestor_uuid
                )
            elif requstor_type == "organization":
                requestor_entity = case_repo.get_contact_organization(
                    uuid=requestor_uuid
                )

        return requestor_entity

    def get_allowed_ruleactions(self) -> list[StaticRuleAction]:
        return StaticRuleAction.get_case_create_actions()

    @validate_with(get_data(__name__, "validation/create_case.json"))
    def call(
        self,
        case_uuid: str,
        case_type_version_uuid: str,
        contact_channel: str,
        requestor: dict,
        custom_fields: dict | None = None,
        confidentiality: str | None = None,
        assignment: dict | None = None,
        contact_information: dict | None = None,
        options: dict | None = None,
    ):
        """
        Create a case.

        :param user_info: user_info
        :param case_uuid: UUID of the case(UUID is an optional field).
        :param case_type_uuid: UUID of the case type the case descended from.
        :param contactchannel: In which way the case ended in creation.
        :param requestor: requestor of the case.
        :param custom_fields: custom_fields of the case
        :param confidentiality: confidentiality of the case
        :param assignee: assignee of the case
        """
        # zs_context_vars.rule_engine.set(RuleEngine())

        case_repo = cast(CaseRepository, self.get_repository("case"))
        case_type_version = case_repo.get_case_type_version(
            case_type_version_uuid=UUID(case_type_version_uuid)
        )

        assignment = assignment or {}
        use_assignee_department = False
        send_email_to_assignee = False
        assignee_uuid = department_uuid = role_uuid = None
        assignee_entity = None

        if assignment.get("employee"):
            use_assignee_department = assignment["employee"].get(
                "use_employee_department"
            )
            send_email_to_assignee = assignment["employee"].get(
                "send_email_notification"
            )
            assignee_uuid = assignment["employee"]["id"]
            assignee_entity = self._get_assignee_entity(
                case_type_version,
                assignee_uuid,
                assignment["employee"].get("type"),
            )

        if assignment.get("department"):
            department_uuid = UUID(assignment["department"]["id"])

        if assignment.get("role"):
            role_uuid = UUID(assignment["role"]["id"])

        requestor_entity = self._get_requestor_entity(
            requestor["id"], requestor["type"]
        )
        department_entity = self._get_department_entity(
            case_type_version,
            department_uuid,
            use_assignee_department,
            assignee_entity,
        )
        role_entity = self._get_role_entity(case_type_version, role_uuid)

        case = case_repo.create_new_case(
            user_info=self.cmd.user_info,
            uuid=UUID(case_uuid),
            case_type_version=case_type_version,
            contact_channel=contact_channel,
            requestor=requestor_entity,
            custom_fields=custom_fields,
            confidentiality=confidentiality,
            contact_information=contact_information,
            options=options,
        )

        if assignee_entity:
            case.set_assignee(
                assignee=assignee_entity,
                send_email_to_assignee=send_email_to_assignee,
            )
            if str(assignee_entity.uuid) == str(self.user_uuid):
                case.set_coordinator(coordinator=assignee_entity)
                case.set_status(status="open")

        if role_entity or department_entity:
            case.set_allocation(
                department=department_entity,
                role=role_entity,
            )

        case_repo.save()


class EnqueueAssigneeMail(CaseCommandBase):
    name = "enqueue_case_assignee_email"

    @validate_with(
        get_data(__name__, "validation/enqueue_case_assignee_email.json")
    )
    def call(self, case_uuid: UUID):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="read",
        )
        case.enqueue_assignee_email()
        repo.save()


class SendAssigneeMail(CaseCommandBase):
    name = "send_case_assignee_email"

    @validate_with(
        get_data(__name__, "validation/send_case_assignee_email.json")
    )
    def call(self, case_uuid: str, queue_id: str):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="read",
        )
        case.send_assignee_email(queue_id=queue_id)
        repo.save()


class SynchronizeRelations(CaseCommandBase):
    name = "synchronize_relations_for_case"

    def _sync_subject_relation(
        self,
        repository,
        case_uuid,
        existing_subject_relations,
        custom_field_definition,
        custom_field_value,
    ):
        # This is not the prettiest way to handle this, but the legacy code
        # sends json-in-a-string
        custom_field_value = [
            json.loads(value) if isinstance(value, str) else value
            for value in custom_field_value
        ]

        if len(custom_field_value) == 0:
            # The user emptied the custom field. Remove relation.
            for relation in existing_subject_relations:
                self.logger.debug(f"Removing subject relation {relation.uuid}")
                relation.delete()
            return

        try:
            subject_uuid = custom_field_value[0]["value"]
            subject_type = custom_field_value[0]["specifics"][
                "relationship_type"
            ]
        except (KeyError, TypeError):
            self.logger.warning(
                "Exception caught while reading custom field value",
                exc_info=True,
            )
            return

        # The custom field has a value.
        found_existing = False
        for relation in existing_subject_relations:
            if str(relation.subject.id) != subject_uuid:
                relation.delete()
            else:
                found_existing = True

        if found_existing:
            self.logger.debug(
                "Found existing relation. Not creating a new one."
            )
            return
        subject = get_subject(self.get_repository, subject_type, subject_uuid)

        case_repo = cast(CaseRepository, self.get_repository("case"))
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="write",
        )

        repository.create_subject_relation(
            case=case,
            subject=subject,
            role=custom_field_definition.relationship_subject_role,
            magic_string_prefix=custom_field_definition.field_magic_string,
            authorized=False,
            permission=None,
            send_confirmation_email=False,
            source_custom_field_type_id=custom_field_definition.uuid,
        )

    def _get_values_from_custom_field(self, custom_field_value) -> list:
        relations = []
        for object in custom_field_value:
            object_value = object.get("value")
            if isinstance(object_value, str):
                # single value relation
                relations.append(object)
            elif isinstance(object_value, list):
                # multi value relation
                relations.extend(object_value)
            else:
                raise ValueError(f"Invalid relation value {object}")
        return relations

    def _sync_object_relation(
        self,
        case_uuid: UUID,
        existing_object_relations,
        custom_field_definition,
        custom_field_value,
    ):
        custom_field_value = [
            json.loads(value) if isinstance(value, str) else value
            for value in custom_field_value
        ]

        old_objects = {
            o.custom_object_uuid
            for o in existing_object_relations
            if o.source_custom_field_type_id == custom_field_definition.uuid
        }

        relations = self._get_values_from_custom_field(custom_field_value)

        new_objects = set()
        for rel in relations:
            new_objects.add(UUID(rel.get("value")))

        removed_objects = old_objects - new_objects
        added_objects = new_objects - old_objects

        custom_object_repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )

        # remove custom_object relation(s)
        if removed_objects:
            for to_remove in removed_objects:
                custom_object = custom_object_repo.find_by_uuid(uuid=to_remove)
                if not custom_object:
                    self.logger.warning(
                        f"Custom object with uuid={to_remove} not found"
                    )
                    continue

                custom_object.unrelate_from(
                    relationship_type="case",
                    related_uuids=[case_uuid],
                    source_custom_field_uuid=custom_field_definition.uuid,
                )

        # add new custom_object relation(s)
        if added_objects:
            for to_add in added_objects:
                custom_object = custom_object_repo.find_by_uuid(uuid=to_add)
                if not custom_object:
                    self.logger.warning(
                        f"Custom object with uuid={to_add} not found"
                    )
                    continue

                custom_object.relate_to(
                    relationship_type="case",
                    related_uuids=[case_uuid],
                    source_custom_field_uuid=custom_field_definition.uuid,
                )

        custom_object_repo.save()

    @validate_with(
        get_data(__name__, "validation/synchronize_relations_for_case.json")
    )
    def call(self, case_uuid, custom_fields):
        case_type_repo = cast(
            CaseTypeRepository, self.get_repository("case_type")
        )
        case_type_version = case_type_repo.find_case_type_version_for_case(
            case_uuid=case_uuid
        )

        relation_fields = case_type_version.get_custom_fields_of_type(
            "relationship"
        )

        subject_relation_repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )
        existing_subject_relations = (
            subject_relation_repo.find_subject_relations_for_case(
                case_uuid=case_uuid, user_info=self.cmd.user_info
            )
        )

        object_relation_repo = cast(
            ObjectRelationRepository, self.get_repository("object_relation")
        )
        existing_object_relations = (
            object_relation_repo.find_object_relations_for_case(
                case_uuid=case_uuid
            )
        )

        for relation_field in relation_fields:
            if relation_field not in custom_fields:
                continue

            custom_field_definition = relation_fields[relation_field]

            if custom_field_definition.relationship_type == "subject":
                existing_relations = [
                    rel
                    for rel in existing_subject_relations
                    if str(rel.source_custom_field_type_id)
                    == str(custom_field_definition.uuid)
                ]

                self._sync_subject_relation(
                    repository=subject_relation_repo,
                    case_uuid=case_uuid,
                    existing_subject_relations=existing_relations,
                    custom_field_definition=custom_field_definition,
                    custom_field_value=custom_fields[relation_field],
                )
            elif custom_field_definition.relationship_type == "custom_object":
                existing_relations = [
                    rel
                    for rel in existing_object_relations
                    if rel.source_custom_field_type_id
                    == custom_field_definition.uuid
                ]

                self._sync_object_relation(
                    case_uuid=case_uuid,
                    existing_object_relations=existing_relations,
                    custom_field_definition=custom_field_definition,
                    custom_field_value=custom_fields[relation_field],
                )
            else:
                self.logger.debug(
                    f"Relation type ignored: {custom_field_definition.relationship_type}"
                )

        subject_relation_repo.save()


class SetCaseParent(CaseCommandBase):
    name = "set_case_parent"

    @validate_arguments
    def call(self, case_uuid: str, parent_uuid: str):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        main_case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        main_case.set_case_parent(parent_uuid=parent_uuid)
        case_repo.save(
            user_info=self.cmd.user_info,
        )


class SetCaseResult(CaseCommandBase):
    name = "set_case_result"

    @validate_arguments
    def call(self, case_uuid: str, case_type_result_uuid: str):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case_type_result_repo = cast(
            CaseTypeResultRepository, self.get_repository("case_type_result")
        )
        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case_repo.cache[case_uuid] = case

        # check if the case_type_result is available for this case
        matching_result = case_type_result_repo.get(
            case_uuid=UUID(case_uuid),
            case_type_result_uuid=UUID(case_type_result_uuid),
            user_info=self.cmd.user_info,
            permission="read",
        )

        case.set_case_result(matching_result)

        case_repo.save(user_info=self.cmd.user_info)


class NewDestructionDate(ValueObject):
    type: ValidCaseDestructionDateType = Field(
        ..., title="Type of destruction date manipulation"
    )
    destruction_date: date | None = Field(
        None, title="Period to calculate destruction date"
    )
    period: int | None = Field(
        None, title="Period to calculate destruction date"
    )


class SetDestructionDate(CaseCommandBase):
    name = "set_destruction_date"

    @validate_arguments
    def call(
        self,
        case_uuid: UUID,
        reason: str,
        new_destruction_date: NewDestructionDate,
    ):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="manage",
        )
        case_repo.cache[str(case_uuid)] = case
        preservation_term_info = None

        if (
            new_destruction_date.type
            == ValidCaseDestructionDateType.recalculate
        ):
            preservation_term_info = case_repo.get_preservation_term_info(
                new_destruction_date.period
            )

        case.update_destruction_date(
            type=new_destruction_date.type,
            destruction_date=new_destruction_date.destruction_date,
            reason=reason,
            preservation_term_info=preservation_term_info,
        )
        if new_destruction_date.type != ValidCaseDestructionDateType.clear:
            case.set_archival_state(
                "overdragen"
                if preservation_term_info
                and preservation_term_info.preservation_term_label == "Bewaren"
                and case.archival_state != "overdragen"
                else "vernietigen"
            )

        case_repo.save(user_info=self.cmd.user_info)


class SynchronizeSubject(CaseCommandBase):
    name = "synchronize_subject"

    @validate_arguments
    def call(self, case_uuid: UUID):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="read",
        )
        case_repo.cache[str(case_uuid)] = case
        case.synchronize_subject()
        case_repo.save(user_info=self.cmd.user_info)


class NotifyCaseAssignee(CaseCommandBase):
    name = "notify_case_assignee"

    @validate_arguments
    def call(
        self,
        case_uuid: UUID,
        event_actor: UUID,
        notification_type: Literal["new_document", "send_case_assignee_email"],
    ):
        """
        Notify the case assignee of a change to their case.
        """

        case_repo = cast(CaseRepository, self.get_repository("case"))

        # Reasoning for "unsafe" use:
        # (2024-07-25) The original action that caused the notification
        # may have been uploaded by a PIP user, and find_case_by_uuid does not
        # support the "PIP ACL" check
        case = case_repo.unsafe_find_case_by_uuid(
            case_uuid=case_uuid,
        )

        if not case.assignee:
            self.logger.debug(
                f"Case {case.entity_id} has no assignee: nobody to notify"
            )
            return

        if not case.assignee.email:
            self.logger.debug(
                f"Case {case.entity_id} assignee has no email: cannot notify"
            )
            return

        if case.assignee.uuid == event_actor:
            self.logger.debug(
                f"Action on case {case.entity_id} performed by assignee. Will not self-notify."
            )
            return

        if not case.assignee.settings:
            self.logger.debug(
                f"Case {case.entity_id} assignee has no settings: cannot notify"
            )
            return

        if (
            case.assignee.settings
            and (
                notification_settings := case.assignee.settings.get(
                    "notifications"
                )
            )
            and (
                not notification_settings.get(
                    NOTIFICATION_TYPE_ACTION_CONFIG_MAP[notification_type]
                )
            )
        ):
            self.logger.debug("User disabled email notifications.")
            return

        template_repo = cast(
            EmailTemplateRepository, self.get_repository("email_template")
        )
        template = template_repo.get_notification_template(
            notification_type=notification_type
        )

        if not template:
            self.logger.debug(
                f"Template for '{notification_type}' not configured, not notifying"
            )
            return

        case.notify_assignee(template=template)

        case_repo.save()


class DeleteCase(CaseCommandBase):
    name = "delete_case"

    @validate_arguments
    def call(
        self,
        case_uuid: UUID,
    ):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        object_relationship_repo = cast(
            ObjectRelationRepository, self.get_repository("object_relation")
        )
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="manage",
        )
        check_case_can_delete(
            case=case,
            case_repo=case_repo,
            object_relationship_repo=object_relationship_repo,
        )

        case.delete()
        case_repo.save(user_info=self.cmd.user_info)
