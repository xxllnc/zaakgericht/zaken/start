# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CaseRelationRepository
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID


class CreateCaseRelation(minty.cqrs.SplitCommandBase):
    name = "create_case_relation"

    @validate_with(get_data(__name__, "validation/create_case_relation.json"))
    def __call__(self, uuid1: str, uuid2: str):
        repo = cast(
            CaseRelationRepository, self.get_repository("case_relation")
        )
        repo.create_case_relation(UUID(uuid1), UUID(uuid2))
        repo.save(user_info=self.cmd.user_info)


class ReorderCaseRelation(minty.cqrs.SplitCommandBase):
    name = "reorder_case_relation"

    @validate_with(get_data(__name__, "validation/reorder_case_relation.json"))
    def __call__(self, relation_uuid: str, case_uuid: str, new_index):
        repo = cast(
            CaseRelationRepository, self.get_repository("case_relation")
        )
        relation = repo.find_case_relation_by_uuid(
            relation_uuid=relation_uuid,
            user_info=self.cmd.user_info,
            permission="read",
        )
        relation.reorder(case_uuid, new_index)
        repo.save(user_info=self.cmd.user_info)


class DeleteCaseRelation(minty.cqrs.SplitCommandBase):
    name = "delete_case_relation"

    @validate_with(get_data(__name__, "validation/delete_case_relation.json"))
    def __call__(self, relation_uuid: UUID, case_uuid: UUID):
        repo = cast(
            CaseRelationRepository, self.get_repository("case_relation")
        )
        case_relation = repo.find_case_relation_by_uuid(
            relation_uuid=relation_uuid,
            user_info=self.cmd.user_info,
            permission="write",
        )
        case_relation.delete(current_case_uuid=case_uuid)
        repo.save(user_info=self.cmd.user_info)
