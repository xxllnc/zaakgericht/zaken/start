# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.cqrs
from ..repositories import CaseRepository
from typing import cast
from uuid import UUID


class CaseRelationsRecover(minty.cqrs.SplitCommandBase):
    name = "recover_case_relations"

    def __call__(self, case_uuid):
        repo = cast(CaseRepository, self.get_repository("case"))

        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        updated_custom_fields = {}
        for cf, value in case.custom_fields.items():
            updated_custom_fields[cf] = [json.dumps(value["value"])]

        case.sync_custom_fields(case.custom_fields)
