# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from .. import repositories
from ..entities import dashboard as entity
from pydantic.v1 import conlist, validate_arguments
from typing import cast
from uuid import UUID


class UpdateDashboard(minty.cqrs.SplitCommandBase):
    name = "update_dashboard"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        widgets: conlist(entity.Widget) | None = None,
    ):
        repo = cast(
            repositories.DashboardRepository,
            self.get_repository("dashboard"),
        )

        dashboard = repo.get(uuid, user_info=self.cmd.user_info)
        dashboard.set_widgets(uuid=uuid, widgets=widgets)
        repo.save()
