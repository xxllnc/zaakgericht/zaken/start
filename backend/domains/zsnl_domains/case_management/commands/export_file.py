# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from .. import repositories
from pydantic.v1 import validate_arguments
from typing import cast
from uuid import UUID


class ExportFileIncreaseDownloadCounter(minty.cqrs.SplitCommandBase):
    name = "export_file_increase_download_counter"

    @validate_arguments
    def __call__(self, export_file_uuid: UUID):
        repo = cast(
            repositories.ExportFileRepository,
            self.get_repository("export_file"),
        )

        export_file = repo.get_export_file_by_uuid(
            export_file_uuid, user_info=self.cmd.user_info
        )
        export_file.increase_download_counter()
        repo.save()
