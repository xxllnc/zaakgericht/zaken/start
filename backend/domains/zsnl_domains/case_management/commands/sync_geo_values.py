# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .._shared import get_subject
from ..entities import AuthorizationLevel
from ..entities import custom_object as entity
from ..repositories import CaseRepository, CustomObjectRepository
from ..services import CustomObjectTemplateService
from minty.exceptions import Forbidden, NotFound
from typing import cast


class SyncGeoValues(minty.cqrs.SplitCommandBase):
    name = "sync_geo_values"

    def __call__(
        self,
        custom_object_uuid: str | None,
        case_uuid: str | None,
        contact_uuid: str | None,
        type: str | None,
    ):
        if custom_object_uuid:
            repo = cast(
                CustomObjectRepository, self.get_repository("custom_object")
            )
            custom_object = repo.find_by_uuid(
                uuid=custom_object_uuid,
                user_info=self.cmd.user_info,
                authorization=AuthorizationLevel.read,
            )
            self._sync_geo_custom_object(custom_object)

        if case_uuid:
            repo = cast(CaseRepository, self.get_repository("case"))

            try:
                case = repo.find_case_by_uuid(
                    case_uuid=case_uuid,
                    user_info=self.cmd.user_info,
                    permission=AuthorizationLevel.readwrite,
                )
            except NotFound as e:
                raise Forbidden(
                    "Not allowed to perform action geo_sync for this case.",
                    "case/geo_sync/not_allowed",
                ) from e
            case.case_geo_sync()

            # sync custom_objects related to the case
            custom_object_repo = cast(
                CustomObjectRepository, self.get_repository("custom_object")
            )
            custom_objects = custom_object_repo.filter(
                user_info=self.cmd.user_info,
                params={"relationships.cases.id": case_uuid},
            )
            for custom_object in custom_objects:
                self._sync_geo_custom_object(custom_object)

        if contact_uuid and type:
            contact = get_subject(
                get_repository=self.get_repository,
                subject_type=type,
                subject_uuid=contact_uuid,
            )

            contact.sync_geo_value()

    def _sync_geo_custom_object(self, custom_object):
        if (
            custom_object.status == entity.ValidObjectStatus.inactive
            and not self._user_has_permission("admin")
        ):
            raise minty.exceptions.Forbidden(
                "Current user does not have rights to update inactive objects"
            )
        custom_object.sync_geo(
            template_service=CustomObjectTemplateService(),
        )
