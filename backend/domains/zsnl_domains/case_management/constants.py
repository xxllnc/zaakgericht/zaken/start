# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import re
from ..shared.constants import (
    BASE_RELATION_ROLES as SHARED_BASE_RELATION_ROLES,
)
from typing import Final

FIRST_PHASE_STATUS = 1
BETROKKENE_TYPE_NUMBER_FOR_PERSON = 1
BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION = 2

BASE_RELATION_ROLES: Final[list[str]] = SHARED_BASE_RELATION_ROLES

PERMISSIONS_FOR_SUBJECT_RELATION = ["none", "write", "read", "search"]


EVENT_TYPES = [
    "auth/alternative",
    "case/checklist/item/update",
    "case/checklist/item/create",
    "case/checklist/item/remove",
    "case/close",
    "case/create",
    "case/document/assign",
    "case/document/create",
    "case/document/label",
    "case/document/metadata/update",
    "case/document/publish",
    "case/document/reject",
    "case/document/remove",
    "case/document/rename",
    "case/document/replace",
    "case/document/restore",
    "case/document/trash",
    "case/document/unpublish",
    "case/document/copy",
    "case/document/sign",
    "case/document/properties/update",
    "case/note/create",
    "case/pip/feedback",
    "case/pip/reject_update",
    "case/pip/updatefield",
    "case/update/purge_date",
    "case/update/registration_date",
    "case/update/status",
    "case/update/subject",
    "case/update/target_date",
    "case/update/allocation",
    "case/attribute/update",
    "case/attribute/create",
    "case/attribute/remove",
    "case/relation/update",
    "document/assign",
    "document/create",
    "case/accept",
    "document/delete_document",
    "document/metadata/update",
    "email/send",
    "case/document/delete_document",
    "document/assignment/reject",
    "case/subject/add",
    "case/subject/update",
    "case/subject/remove",
    "case/task/new_assignee",
    "case/task/set_completion",
    "case/relation/remove",
    "case/update/completion_date",
    "case/update/case_type",
    "case/update/confidentiality",
    "case/update/department",
    "case/update/field",
    "case/update/milestone",
    "case/update/relation",
    "case/update/requestor",
    "case/update/result",
    "case/update/settle_date",
    "case/suspend",
    "case/reopen",
    "case/publish",
    "case/object/create",
    "case/object/delete",
    "case/object/relate",
    "case/contact_moment/created",
    "case/note/created",
    "case/email/created",
    "subject/contact_moment/delete",
    "subject/message/delete",
    "subject/note/delete",
    "case/thread/link",
]


def _is_document_event(event_type: str) -> bool:
    match = re.match(r"^[a-z\/]*document\/[_a-z\/]*", event_type)
    if match and match.string == event_type:
        return True
    return False


def _is_case_update_event(event_type: str) -> bool:
    match = re.match(r"^case[a-z\/]*update[_a-z\/]*", event_type)
    if match and match.string == event_type:
        return True
    return False


DOCUMENT_EVENTS = [e for e in EVENT_TYPES if _is_document_event(e)]
CASE_EVENTS = [e for e in EVENT_TYPES if _is_case_update_event(e)]
