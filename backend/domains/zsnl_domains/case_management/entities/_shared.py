# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import email_validator
import enum
import re
import string
from minty.entity import Entity, ValueObject
from minty.exceptions import ValidationError
from pydantic.v1 import Field, validator
from typing import Literal, Union
from uuid import UUID


class AuthorizationLevel(enum.StrEnum):
    read = "read"
    readwrite = "readwrite"
    admin = "admin"


class CaseAuthorizationLevel(enum.StrEnum):
    search = "search"
    read = "read"
    write = "write"
    manage = "manage"


class RelatedCaseType(Entity):
    """
    Reference to a case_type entity
    """

    entity_type: str = Field("case_type", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the case type")


class RelatedCaseTypeVersion(Entity):
    """
    Reference to a case_type_version entity
    """

    entity_type: str = Field("case_type_version", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the case type version")


class RelatedDepartment(Entity):
    """Reference to a department entity"""

    entity_type: str = Field("department", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class RelatedRole(Entity):
    """Reference to a Role entity"""

    entity_type: str = Field("role", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class RelatedEmployee(Entity):
    """Reference to an employee entity"""

    entity_type: str = Field("employee", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class RelatedPerson(Entity):
    """Reference to a person entity"""

    entity_type: str = Field("person", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the person")


class RelatedOrganization(Entity):
    """Reference to an organization entity"""

    entity_type: str = Field("organization", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the organization")


class BaseAddress(ValueObject):
    country: str = Field(..., title="Country")
    country_code: int | None = Field(None, title="Country Code of the address")
    geo_lat_long: str | None = Field(
        None, title="Geolocation value of address"
    )


class DutchAddress(BaseAddress):
    country_code: Literal[6030] = 6030
    street: str = Field(..., title="Street name")
    street_number: int = Field(..., title="House number")
    street_number_letter: str | None = Field(
        None, title="House letter (part of house number)"
    )
    street_number_suffix: str | None = Field(
        None, title="House number extension"
    )
    zipcode: str | None = Field(None, title="Postal code (zipcode)")
    city: str = Field(..., title="City")
    is_foreign: bool = Field(
        False,
        const=True,
        title="Indicates that this is an address outside the Netherlands",
    )

    @validator("street_number_letter")
    def is_valid_street_number_letter(cls, v):
        if v in [None, "", " "]:
            return None

        if len(v) == 1 and v[0] in string.ascii_letters:
            return v

        raise ValueError("House letter should be 1 letter (A-Z)")

    @validator("country")
    def is_valid_dutch(cls, v):
        if v in [None, "", " "]:
            return None

        if v == "Nederland":
            return v

        raise ValueError(
            "Dutch adress only valid for address with country Nederland"
        )


class InternationalAddress(BaseAddress):
    address_line_1: str = Field(..., title="First line of the address")
    address_line_2: str | None = Field(
        None, title="Second line of the address"
    )
    address_line_3: str | None = Field(None, title="Third line of the address")
    is_foreign: bool = Field(
        True,
        const=True,
        title="Indicates that this is an address outside the Netherlands",
    )


class InvalidatedAddress(BaseAddress):
    address_line_1: str | None = Field(None, title="First line of the address")
    address_line_2: str | None = Field(
        None, title="Second line of the address"
    )
    address_line_3: str | None = Field(None, title="Third line of the address")
    street: str | None = Field(None, title="Street name")
    street_number: int | None = Field(None, title="House number")
    street_number_letter: str | None = Field(
        None, title="House letter (part of house number)"
    )
    street_number_suffix: str | None = Field(
        None, title="House number extension"
    )
    zipcode: str | None = Field(None, title="Postal code (zipcode)")
    city: str | None = Field(None, title="City")
    is_foreign: bool = Field(
        False,
        title="Indicates that this is an address outside the Netherlands",
    )


class CuracaoAddress(ValueObject):
    country: str = Field("Curaçao", title="Country")
    country_code: Literal[5107] = 5107
    street: str = Field(..., title="Street name")
    street_number: int = Field(..., title="House number")
    street_number_letter: str | None = Field(
        None, title="House letter (part of house number)"
    )
    street_number_suffix: str | None = Field(
        None, title="House number extension"
    )
    zipcode: str | None = Field(None, title="Postal code (zipcode)")
    city: str = Field(..., title="City")

    @validator("street_number_letter")
    def is_valid_street_number_letter(cls, v):
        if v in [None, "", " "]:
            return None

        if len(v) == 1 and v[0] in string.ascii_letters:
            return v

        raise ValueError("House letter should be 1 letter (A-Z)")

    @validator("country")
    def is_valid_curacao(cls, v):
        if v in [None, "", " "]:
            return None

        if v == "Curaçao":
            return v

        raise ValueError(
            "Curaçao adress only valid for address with country Curaçao"
        )


Address = Union[DutchAddress, InternationalAddress, CuracaoAddress]


ContactAddress = Union[
    DutchAddress, InternationalAddress, CuracaoAddress, InvalidatedAddress
]


class ValidPreferredContactChannel(enum.StrEnum):
    pip = "pip"
    email = "email"
    phone = "phone"
    mail = "mail"


class ContactInformation(ValueObject):
    is_anonymous_contact: bool | None = Field(
        default=False,
        title="Is the contact 'anonymous' or 'locked' (contact data cannot be changed)",
    )

    email: str | None = Field(None, title="Email address of the contact")
    phone_number: str | None = Field(None, title="Preferred phone number")
    mobile_number: str | None = Field(None, title="Mobile phone number")
    internal_note: str | None = Field(None, title="Internal note")
    preferred_contact_channel: ValidPreferredContactChannel | None = Field(
        "pip", title="Preffered contact channel"
    )


class RelatedCustomObject(Entity):
    entity_type = "custom_object"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the custom object")


class CaseConfidentiality(enum.StrEnum):
    public = "public"
    internal = "internal"
    confidential = "confidential"


class CaseStatus(enum.StrEnum):
    new = "new"
    open = "open"
    stalled = "stalled"
    resolved = "resolved"
    deleted = "deleted"


class CasePayment(ValueObject):
    amount: float | None = Field(None, title="payment amount")
    status: str | None = Field(None, title="payment status")


class CaseResultArchivalAttributes(ValueObject):
    state: str | None = Field(None, title="archival state")
    selection_list: str | None = Field(None, title="selection list")


class CaseResult(ValueObject):
    result: str = Field(None, title="Short name (slug) for the result")
    result_name: str = Field(None, title="Custom name of the result")
    result_uuid: UUID = Field(
        None, title="UUID of the result in the case type"
    )
    archival_attributes: CaseResultArchivalAttributes | None = Field(
        None, title="Archival attributes for the result"
    )


class ContactType(enum.StrEnum):
    person = "person"
    organization = "organization"
    employee = "employee"


def is_valid_bsn(bsn: str) -> bool:
    if 6 < len(bsn) < 10:
        bsn = bsn.zfill(9)
    else:
        return False
    sum = 0
    for i in range(9, 0, -1):
        if i > 1:
            sum += i * int(bsn[9 - i])
        elif i == 1:
            sum -= i * int(bsn[8])

    if sum and sum % 11 == 0:
        return True

    return False


def validate_contact_information(contact_information):
    phone_number_regex = re.compile(r"^\+?[0-9]{6,20}$")

    if email := contact_information.email:
        try:
            validated = email_validator.validate_email(
                email, check_deliverability=False
            )
            email = validated.normalized
        except email_validator.EmailNotValidError as e:
            raise ValidationError("Invalid Email", "email/invalid") from e
    if phone_number := contact_information.phone_number:
        if not re.fullmatch(phone_number_regex, phone_number):
            raise ValidationError(
                "Invalid Phone Number", "phone_number/invalid"
            )

    if mobile_number := contact_information.mobile_number:
        if not re.fullmatch(phone_number_regex, mobile_number):
            raise ValidationError(
                "Invalid Mobile Number", "mobile_number/invalid"
            )
