# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import email_validator
import enum
import json
import logging
import re
import string
from ...shared import zs_context_vars
from ..constants import FIRST_PHASE_STATUS
from ._shared import (
    CaseAuthorizationLevel,
    CaseConfidentiality,
    CasePayment,
    CaseResult,
    CaseStatus,
)
from .case_type_result import CaseTypeResultBasic
from .case_type_version import CaseTypeVersionEntity
from .department import Department
from .role import Role
from datetime import date, datetime, timedelta, timezone
from dateutil.relativedelta import relativedelta
from dateutil.tz import gettz
from minty import date_calculation
from minty.entity import Entity, ValueObject
from minty.exceptions import ConfigurationConflict, Conflict, CQRSException
from pydantic.v1 import Field
from typing import TYPE_CHECKING, Any, Literal, cast
from urllib.parse import urlparse
from uuid import UUID, uuid4

logger = logging.getLogger(__name__)

# Prevent import loops by only importing when checking type information:
if TYPE_CHECKING:
    from ..entities.email_template import EmailTemplate
    from ..services import CaseTemplateService, RuleEngine


def apply_case_rules(func):
    def wrapper(*args, **kwargs):
        instance = args[0]
        func(*args, **kwargs)
        if isinstance(instance, Case):
            # Execute the rules for the current phase
            rule_engine: RuleEngine = zs_context_vars.rule_engine.get()
            if not rule_engine:
                raise CQRSException(
                    "Implemention error, rule_engine not available. Is the command a subclass of 'CaseCommandBase'?"
                )  # pragma: no cover
            if instance.case_type_version.phases[instance.milestone - 1].rules:
                rule_engine.execute_rules(
                    rules=instance.case_type_version.phases[
                        instance.milestone - 1
                    ].rules,
                    case=instance,
                )

    return wrapper


def case_event_decorator(name: str, extra_fields: list[str] | None = None):
    def decorator(func):
        return apply_case_rules(
            Entity.event(
                name=name, fire_always=True, extra_fields=extra_fields
            )(func)
        )

    return decorator


class ValidCaseDestructionDateType(enum.StrEnum):
    """Valid destruction date manipulation type for the case"""

    clear = "clear"
    set = "set"
    recalculate = "recalculate"


class CaseMeta(ValueObject):
    id: int | None = Field(None, title="Id of case meta")
    completion: str | None = Field(None, title="case completion info")
    suspension_reason: str | None = Field(None, title="case suspension info")
    stalled_since_date: date | None = Field(None, title="stalled since date")


class CasePhase(ValueObject):
    label: str | None = Field(None, title="label of phase")
    milestone_label: str | None = Field(
        None, title="milestone label of the phase"
    )


class CaseContact(Entity):
    """Requestor Entity"""

    entity_type = "case_contact"
    entity_id__fields: list[str] = ["uuid"]
    id: int | None = Field(None, title="Id of the requestor")
    uuid: UUID | None = Field(None, title="Internal identifier of requestor")
    role: str | None = Field(None, title="Role of the contact")


class CaseContactPerson(CaseContact):
    """Contains details of person related to case with various roles"""

    entity_type = "person"
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "magic_string_prefix",
        "role",
    ]
    type: str | None = Field(None, title="Type is person")
    name: str | None = Field(None, title="Name of the person")
    magic_string_prefix: str | None = Field(None, title="Magic string prefix")
    surname: str | None = Field(None, title="Surname of the person")
    surname_prefix: str | None = Field(
        None, title="Surname prefix of the person"
    )
    family_name: str | None = Field(None, title="Family name of the person")

    full_name: str | None = Field(None, title="Full name of the person")
    first_names: str | None = Field(None, title="First names for the person")
    initials: str | None = Field(None, title="Initials for the person")
    naamgebruik: str | None = Field(None, title="Identifier of case type")
    bsn: str | None = Field(None, title="BSN number for the person")
    noble_title: str | None = Field(None, title="Title of the person")
    gender: str | None = Field(None, title="Gender of the person")

    date_of_birth: datetime | None = Field(
        None, title="person's date of birth"
    )
    date_of_divorce: datetime | None = Field(
        None, title="Date of divorce for person"
    )

    place_of_birth: str | None = Field(None, title="person's place of birth")
    country_of_birth: str | None = Field(
        None, title="person's country of birth"
    )
    salutation: str | None = Field(None, title="Salutation used for v")
    salutation1: str | None = Field(
        None, title="Another Salutation used for person"
    )

    salutation2: str | None = Field(
        None, title="Another Salutation used for person"
    )

    unique_name: str | None = Field(None, title="Unique name for the person")

    subject_type: str | None = Field(None, title="type of the subject")
    status: str | None = Field(None, title="Status of person")
    place_of_residence: str | None = Field(
        None, title="place of residence for person"
    )
    country_of_residence: str | None = Field(
        None, title="country of residence for person"
    )
    street: str | None = Field(None, title="Address street name")
    residence_street: str | None = Field(
        None, title="Residence address street name"
    )
    zipcode: str | None = Field(None, title="Zipcode value")
    residence_zipcode: str | None = Field(
        None, title="Residence address zipcode"
    )
    house_number: str | None = Field(None, title="House number for the person")

    residence_house_number: str | None = Field(
        None, title="Residence house number for person"
    )
    mobile_number: str | None = Field(
        None, title="Contact mobile number of person"
    )
    phone_number: str | None = Field(
        None, title="contact phone number of person"
    )
    email: str | None = Field(None, title="Email address of person")

    a_number: str | None = Field(
        None, title="Another identifier in the dutch systems, called a_nummer"
    )

    is_secret: bool | None = Field(None, title="If its secret value")

    foreign_residence_address_line1: str | None = Field(
        None, title="foreign residence address first line"
    )
    foreign_residence_address_line2: str | None = Field(
        None, title="foreign residence address second line"
    )
    foreign_residence_address_line3: str | None = Field(
        None, title="foreign residence address third line"
    )

    investigation: bool | None = Field(
        None, title="investigation value for person"
    )

    date_of_marriage: datetime | None = Field(
        None, title="Marriage date for person"
    )

    date_of_death: datetime | None = Field(
        None, title="Date of death for person"
    )
    residence_place_of_residence: str | None = Field(
        None, title="Place of residence"
    )
    has_briefadres: bool | None = Field(
        None, title="Boolean indicating if person has correspondence address"
    )


class CaseContactEmployee(CaseContact):
    """Contains details of employee related to case with roles such as assginee,requestor etc"""

    entity_type = "employee"
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "magic_string_prefix",
        "role",
    ]
    name: str | None = Field(default=None, title="Name of the employee")
    type: str | None = Field(default=None, title="Type of the employee")
    surname: str | None = Field(default=None, title="Surname of the employee")
    magic_string_prefix: str | None = Field(
        default=None, title="Magic string prefix"
    )
    family_name: str | None = Field(
        default=None, title="Family name of the employee"
    )
    full_name: str | None = Field(
        default=None, title="Full name of the employee"
    )

    first_names: str | None = Field(
        default=None, title="First names for the employee"
    )
    initials: str | None = Field(
        default=None, title="Initials for the employee"
    )
    last_name: str | None = Field(
        default=None, title="Last name for the employee"
    )
    title: str | None = Field(default=None, title="Title for the employee")
    unique_name: str | None = Field(
        default=None, title="Unique name for the employee"
    )
    subject_type: str | None = Field(default=None, title="type of the subject")
    status: str | None = Field(default=None, title="Status of employee")
    street: str | None = Field(default=None, title="Address street name")
    residence_street: str | None = Field(
        default=None, title="Residence address street name"
    )
    zipcode: str | None = Field(None, title="Zipcode value")
    residence_zipcode: str | None = Field(
        default=None, title="Residence address zipcode"
    )
    house_number: str | None = Field(
        default=None, title="House number for the employee"
    )
    residence_house_number: str | None = Field(
        default=None, title="Residence house number for employee"
    )
    phone_number: str | None = Field(
        default=None, title="contact phone number of employee"
    )
    email: str | None = Field(None, title="Email address of employee")
    department: str | None = Field(
        default=None, title="Name of the department for employee"
    )
    correspondence_zipcode: str | None = Field(
        default=None, title="correspondence zipcode value"
    )
    correspondence_street: str | None = Field(
        default=None, title="correspondence address street value"
    )
    properties: dict | None = Field(
        default=None, title="properties of subject"
    )
    settings: dict | None = Field(default=None, title="Settings of the user")


class CaseContactOrganization(CaseContact):
    """Contains details of organization related to case with various roles"""

    entity_type = "organization"
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "magic_string_prefix",
        "role",
    ]
    magic_string_prefix: str | None = Field(None, title="Magic string prefix")

    type: str | None = Field(None, title="Type of the organization")

    name: str | None = Field(None, title="Name of the organization")

    unique_name: str | None = Field(
        None, title="Unique name for the organization"
    )

    subject_type: str | None = Field(None, title="type of the organization")
    status: str | None = Field(None, title="Status of v")

    place_of_residence: str | None = Field(
        None, title="place of residence for organization"
    )

    country_of_residence: str | None = Field(
        None, title="country of residence for organization"
    )

    street: str | None = Field(None, title="Address street name")
    residence_street: str | None = Field(
        None, title="Residence address street name"
    )
    zipcode: str | None = Field(None, title="Zipcode value")

    residence_zipcode: str | None = Field(
        None, title="Residence address zipcode"
    )

    house_number: str | None = Field(
        None, title="House number for the organization"
    )

    residence_house_number: str | None = Field(
        None, title="Residence house number for organization"
    )

    mobile_number: str | None = Field(
        None, title="Contact mobile number of organization"
    )
    phone_number: str | None = Field(
        None, title="contact phone number of organization"
    )
    email: str | None = Field(None, title="Email address of organization")

    foreign_residence_address_line1: str | None = Field(
        None, title="foreign residence address first line"
    )
    foreign_residence_address_line2: str | None = Field(
        None, title="foreign residence address second line"
    )
    foreign_residence_address_line3: str | None = Field(
        None, title="foreign residence address third line"
    )

    correspondence_zipcode: str | None = Field(
        None, title="correspondence zipcode value"
    )

    correspondence_place_of_residence: str | None = Field(
        None, title="correspondence place of residence"
    )

    correspondence_street: str | None = Field(
        None, title="correspondence address street value"
    )

    correspondence_house_number: str | None = Field(
        None, title="correspondence house number"
    )

    coc: str | None = Field(
        None, title="Chamber of Commerce number of the organization"
    )

    type_of_business_entity: str | None = Field(
        None, title="Type of the business"
    )

    establishment_number: str | None = Field(
        None, title="Establishment number of requestor organization"
    )

    trade_name: str | None = Field(
        None, title="Trade name for requestor organization"
    )
    residence_place_of_residence: str | None = Field(
        None, title="Place of residence"
    )
    login: str | None = Field(None, title="KVK number for the organization")
    has_briefadres: bool | None = Field(
        None,
        title="Boolean indicating if organization has correspondence address",
    )


class CaseTypePrice(ValueObject):
    """Price details for case type"""

    web: str | None = Field(None, title="Price for web")
    counter: str | None = Field(None, title="Price for counter")
    telephone: str | None = Field(None, title="Price for telephone")
    email: str | None = Field(None, title="Price for email")
    employee: str | None = Field(None, title="Price for employee")
    post: str | None = Field(None, title="Price for post")


class DepartmentSummary(ValueObject):
    uuid: UUID = Field(..., title="Identifier for this department")
    name: str = Field(..., title="Name of the department")


class CaseDepartment(ValueObject):
    uuid: UUID = Field(..., title="Identifier for this department")
    name: str = Field(..., title="Name of the department")
    description: str | None = Field(
        "", title="Longer description of the department"
    )
    parent: DepartmentSummary | None = Field(
        None, title="Parent department of this department"
    )


class CaseRole(ValueObject):
    uuid: UUID = Field(..., title="Identifier for this role")
    name: str = Field(..., title="Name of the role")
    description: str | None = Field("", title="Longer description of the role")

    parent: DepartmentSummary | None = Field(
        None, title="Department this role is part of"
    )


class CaseTypeForCase(Entity):
    """Details of case type related to given case"""

    entity_type: Literal["case_type"] = "case_type"
    entity_id__fields: list[str] = ["uuid"]
    uuid: UUID = Field(..., title="Identifier for this case type")


class DestructionReason(ValueObject):
    reason: str | None = Field(..., title="Reason for destruction")
    label: str | None = Field(..., title="Label showing destruction period")


class Participant(ValueObject):
    name: str | None = Field(..., title="First name of the Participant")
    lastName: str | None = Field(..., title="Last name of the Participant")
    email: str | None = Field(..., title="Email of the Participant ")
    phone: str | None = Field(..., title="Phone number")


class CaseNotification(ValueObject):
    sender_name: str | None
    sender_address: str | None
    subject: str
    body: str
    recipient_address: str


class Case(Entity):
    entity_type = "case"
    entity_id__fields: list[str] = ["uuid"]

    entity_relationships: list[str] = [
        "coordinator",
        "assignee",
        "requestor",
        "case_type",
        "case_type_version",
        "related_contacts",
    ]

    entity_meta_authorizations: set[CaseAuthorizationLevel] | None = Field(
        None, title="Authorizations the current user has for this case"
    )
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "entity_meta_authorizations",
    ]
    entity_internal__fields: list[str] = ["notification"]

    id: int | None = Field(None, title="Id of Case")
    number: int = Field(None, title="Id of Case")
    uuid: UUID | None = Field(None, title="Internal identifier of this entity")
    department: Department = Field(None, title="department of the case")
    role: Role | None = Field(None, title="role")
    destruction_date: date | None = Field(
        None, title="destruction date for the case"
    )
    destruction_reason: DestructionReason | None = Field(
        None, title="Reason for case destruction"
    )
    phase: CasePhase | None = Field(None, title="case phase")
    result: CaseResult | None = Field(None, title="result for the case")
    archival_state: str | None = Field(
        None, title="archival state of the case"
    )
    public_summary: str | None = Field(None, title="case summary")
    status: CaseStatus = Field(None, title="case status")
    created_date: date = Field(None, title="case created date")
    registration_date: date = Field(None, title="case registration date")
    target_completion_date: date | None = Field(
        None, title="case target completion date"
    )
    completion_date: date | None = Field(None, title="case completion date")
    stalled_until_date: date | None = Field(None, title="stalled until date")
    milestone: int = Field(None, title="milestone for the case")
    suspension_reason: str = Field(None, title="suspension reason")
    completion: str | None = Field(None, title="case completion")
    stalled_since_date: date | None = Field(None, title="stalled since date")
    last_modified_date: date = Field(None, title="last modification date")
    coordinator: CaseContactEmployee | None = Field(
        None, title="case coordinator"
    )
    assignee: CaseContactEmployee | None = Field(None, title="case assignee")
    requestor: CaseContact = Field(None, title="case requestor")
    subject: str | None = Field(None, title="subject for the case")
    subject_extern: str | None = Field(None, title="extern subject")
    request_trigger: str | None = Field(None, title="request trigger")
    contact_channel: str = Field(None, title="contact channel")
    resume_reason: str | None = Field(None, title="reason for resuming")
    summary: str | None = Field(None, title="case summary")
    payment: CasePayment | None = Field(None, title="case payment")
    confidentiality: CaseConfidentiality | None = Field(
        None, title="confidentiality of the case"
    )
    meta: CaseMeta | None = Field(None, title="meta info for the case")
    case_actions: dict | None = Field(None, title="actions for the case")
    enqueued_subcases_data: str | None = Field(None, title="subcase data")
    queue_ids: str | None = Field(None, title="queue id list")
    custom_fields: dict | None = Field(
        None, title="custom fields for the case"
    )
    subjects: list | None = Field(None, title="list of subjects in the case")
    alternative_case_number: str | None = Field(
        None, title="alternative case number"
    )
    urgency: str | None = Field(None, title="urgency of the case")
    preset_client: bool = Field(None, title="preset client for the case")
    contact_information: dict | None = Field(None, title="contact information")
    send_email_to_assignee: str | None = Field(None, title="Email info")
    assignee_email_queue_id: str | None = Field(
        None, title="email queue id of assignee"
    )
    case_type_version: CaseTypeVersionEntity | None = Field(
        None, title="type of the case"
    )
    case_type: CaseTypeForCase | None = Field(None, title="type of the case")

    enqueued_documents_data: str | None = Field(
        None, title="enqueued documents data"
    )
    enqueued_emails_data: str | None = Field(
        None, title="enqueued emails data"
    )
    enqueued_subjects_data: str | None = Field(
        None, title="enqueued subjects data"
    )
    # related_contact
    related_contacts: list[CaseContact] | None = Field(
        None, title="Roles for given case"
    )
    progress_status: float | None = Field(
        None, title="Progress status for given case"
    )
    case_type_phase: str | None = Field(None, title="phase of case type")
    unaccepted_files_count: int | None = Field(
        None, title="Count of unaccepted files"
    )
    unaccepted_attribute_update_count: int | None = Field(
        None, title="Count of unaccepted updates"
    )
    related_uuids: str | None = Field(
        None, title="List of uuids of cases related to given case"
    )
    parent_uuid: UUID | None = Field(None, title="UUID of parent case")
    child_uuids: str | None = Field(
        None, title="List of uuids of child cases for given case"
    )
    destructable: int | None = Field(
        None, title="Status indicating in case is destructable"
    )
    # system attributes
    type_of_archiving: str | None = Field(
        None, title="Type of archiving for given case"
    )
    period_of_preservation: str | None = Field(
        None, title="Time period for preservation of the case"
    )
    suspension_rationale: str | None = Field(
        None, title="suspension reason for the case"
    )
    result_description: str | None = Field(
        None, title="Result description for the case"
    )
    result_explanation: str | None = Field(
        None, title="Result explanation for the case"
    )
    result_selection_list_number: str | None = Field(
        None, title="Result selection number"
    )
    result_process_type_number: str | None = Field(
        None, title="Result process type number for the case"
    )
    result_process_type_name: str | None = Field(
        None, title="Result process type name for the case"
    )
    result_process_type_description: str | None = Field(
        None, title="Result process type description for the case"
    )
    result_process_type_explanation: str | None = Field(
        None, title="Result process type explanation for the case"
    )
    result_process_type_object: str | None = Field(
        None, title="Result process type object for the case"
    )
    result_process_type_generic: str | None = Field(
        None, title="Geenric result process type for the case"
    )
    result_origin: str | None = Field(None, title="Result origin for the case")
    result_process_term: str | None = Field(
        None, title="Term for result process"
    )
    active_selection_list: str | None = Field(
        None, title="Active selection list"
    )
    aggregation_scope: str | None = Field(
        None, title="Scope of aggregation for the case"
    )
    number_parent: int | None = Field(None, title="Number of parent")
    number_master: int | None = Field(None, title="Number of master")
    number_previous: int | None = Field(None, title="Previous number")
    premature_completion_rationale: str | None = Field(
        None, title="Premature completion reason"
    )
    case_location: str | None = Field(
        None, title="Location details for the case"
    )
    start_date: datetime | None = Field(None, title="Start date of the case")
    price: str | None = Field(None, title="Price for given case")
    lead_time_real: int | None = Field(None, title="Lead time for given case")
    progress_status: float | None = Field(
        None, title="Progress status for given case"
    )
    progress_days: int | None = Field(
        None, title="Progress days for given case"
    )
    related_cases: str | None = Field(
        None, title="Cases related to given case"
    )
    days_left: int | None = Field(
        None, title="Number of days left for case completion"
    )
    number_relations: int | None = Field(
        None, title="Number of related cases for the case"
    )
    relations: str | None = Field(None, title="Related cases for the case")
    relations_complete: str | None = Field(
        None, title="Indicating status relations"
    )
    documents: str | None = Field(None, title="List of documents in the case")
    case_documents: str | None = Field(
        None, title="List of documents with label"
    )
    case_type_phase: str | None = Field(None, title="phase of case type")
    unaccepted_files_count: int | None = Field(
        None, title="Count of unaccepted files"
    )
    document_sign_participants: list[Participant] | None = Field(
        None, title="Participants used in document signing"
    )

    notification: CaseNotification | None = Field(
        default=None, title="Email notification to be sent out about the case"
    )
    deleted: date | None = Field(None, title="case deleted date")

    @Entity.event(name="CaseRegistrationDateSet", fire_always=True)
    def set_registration_date(
        self,
        registration_date: str,
        recalculate_target_completiondate: bool,
    ):
        """Set new registration date after validating this is allowed.

        :param registration_date: registration date
        :type registration_date: str format('2019-01-20')
        :raises Conflict: if validation fails
        """
        registration_date_object = date.fromisoformat(registration_date)
        if recalculate_target_completiondate:
            self.set_target_completion_date(
                target_completion_date=self.date_to_str(
                    self._calculate_target_completion_date(
                        interval_for_completion=self.case_type_version.terms.lead_time_service.value,
                        interval_for_completion_type=self.case_type_version.terms.lead_time_service.type,
                        registration_date=registration_date_object,
                    )
                ),
            )
        if self.completion_date is not None:
            if registration_date_object > self.completion_date:
                raise Conflict(
                    "Not allowed to set 'registration_date' after 'completion_date'."
                )
        if self.target_completion_date is not None:
            if registration_date_object > self.target_completion_date:
                raise Conflict(
                    "Not allowed to set 'registration_date' after 'target_completion_date'."
                )

        self.registration_date = registration_date

        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CaseTargetCompletionDateSet", fire_always=True)
    def set_target_completion_date(self, target_completion_date: str):
        """Set target completion date after validating this is allowed.

        :param target_completion_date: target completion date
        :type target_completion_date: str format('2019-01-20')
        :raises Conflict: if validation fails.
        """
        target_completion_date_object = date.fromisoformat(
            target_completion_date
        )

        if self.registration_date > target_completion_date_object:
            raise Conflict(
                "Not allowed to set 'target_completion_date' before 'registration_date'."
            )
        self.target_completion_date = target_completion_date
        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CaseSubjectSynchronized", fire_always=True)
    def synchronize_subject(self):
        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CaseCompletionDateSet", fire_always=True)
    def set_completion_date(self, completion_date: str):
        """set new completion date after validating this is allowed.

        :param completion_date: completion date
        :type completion_date: str format('2019-01-20')
        :raises Conflict: if validation fails
        """
        if self.status != "resolved":
            raise Conflict(
                "'set_completion_date' only allowed for 'resolved' "
                + f"cases, current status:'{self.status}'",
                "case/completion_date_not_allowed_for_resolved",
            )

        completion_date_object = date.fromisoformat(completion_date)
        if completion_date_object < self.registration_date:
            raise Conflict(
                "Not allowed to set 'completion_date' before 'registration_date'.",
                "case/invalid_completion_date",
            )
        nl_date_today = datetime.now(tz=gettz("Europe/Amsterdam")).date()
        if completion_date_object > nl_date_today:
            raise Conflict(
                "Not allowed to set 'completion_date' in the future.",
                "case/invalid_completion_date_in_future",
            )

        self.completion_date = completion_date

        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CasePaused", fire_always=True)
    def pause(
        self,
        suspension_reason: str,
        suspension_term_type: str,
        suspension_term_value: int | str | None,
    ):
        """
        Set a case to "paused"/stalled state.

        :param suspension_reason: Reason for pausing the case
        :param suspension_term_value: How long to pause the case for
        :param suspension_term_type: Unit for suspension_term_value
        """
        if self.status != "open":
            raise Conflict(
                f"'pause' only allowed for 'open' cases; not '{self.status}'",
                "case/pause/not_open",
            )

        today = datetime.now(tz=gettz("Europe/Amsterdam")).date()
        stalled_until_date = self._get_stalled_until_date(
            term_value=suspension_term_value,
            term_type=suspension_term_type,
            start_date=today,
        )

        self.set_status("stalled")
        self.suspension_reason = suspension_reason
        self.stalled_until_date = stalled_until_date
        self.stalled_since_date = today
        self._sync_subject()
        self._sync_subject_extern()

    def _get_stalled_until_date(
        self,
        term_value: int | str | None,
        term_type: str,
        start_date: date | None = None,
    ):
        """
        Get the stalled/postponed date for a case.

        Raises a KeyError when term type doesn't match the list of interval types.

        :param term_value: The amount of days/business days/weeks/date to postpone the case to.
        :param term_type: The type of the postpone.
        :param start_date: start date to compare to when a fixed date is selected.
        :return: The formatted datetime string
        :rtype str format('2019-01-20')
        """

        interval_type = {
            "weeks": "weeks",
            "work_days": "business_days",
            "calendar_days": "days",
        }

        if term_type == "indefinite":
            return None

        if term_value is None:
            raise Conflict(f"Term type '{term_type}' is missing a term value.")

        if term_type == "fixed_date":
            fixed_date = self.str_to_date(term_value)
            if fixed_date <= start_date:
                raise Conflict(
                    f"Selected date is in the past. Date: '{fixed_date}'"
                )
            return fixed_date

        if term_type in interval_type:
            return date_calculation.add_timedelta_to_date(
                interval_type[term_type], term_value, start_date
            )

    def _calculate_target_completion_date(
        self,
        interval_for_completion: str,
        interval_for_completion_type: str,
        registration_date: date,
    ) -> date:
        """Calculate target_completion_date for a case from registration_date, interval_for_completion and interval_for_completion_type.
        Raises Conflict when interval_for_completion or interval_for_completion_type is invalid.

            :param interval_for_completion: The amount of days/business days/weeks/date to complete.
            :type interval_for_completion: string
            :param interval_for_completion_type: The type of the interval.
            :type interval_for_completion_type: str
            :param registration_date: registration date of a case.
            :type registration_date: date format('2019-01-20')
            :return: The formatted datetime string
            :rtype str format('2019-01-20')
        """

        interval_type_mapping = {
            "weken": "weeks",
            "werkdagen": "business_days",
            "kalenderdagen": "days",
        }

        if interval_for_completion_type == "einddatum":
            try:
                return datetime.strptime(
                    interval_for_completion, "%d-%m-%Y"
                ).date()
            except ValueError as err:
                raise ConfigurationConflict(
                    f"Time data '{interval_for_completion}'  does not match '%d-%m-%Y'",
                    "case/invalid_target_completion_date",
                ) from err

        if interval_for_completion_type in interval_type_mapping:
            return date_calculation.add_timedelta_to_date(
                interval_type_mapping[interval_for_completion_type],
                int(interval_for_completion),
                registration_date,
            )

        raise ConfigurationConflict(
            f"Invalid completion type '{interval_for_completion_type}' specified.",
            "case/invalid_target_completion_type",
        )

    @Entity.event(name="CaseResumed", fire_always=True)
    def resume(
        self,
        stalled_since_date: str,
        stalled_until_date: str,
        resume_reason: str,
    ):
        """Resume stalled case and calculate new completion date.
        Set stalled until and stalled since if they are set in the past to resume a case in the past.

        :param stalled_since_date: stalled since date
        :type stalled_since_date: str format('2019-01-20')
        :param stalled_until_date: stalled until date
        :type stalled_until_date: str format('2019-01-20')
        :param resume_reason: Reason for the resume.
        :type resume_reason: str
        """
        if self.status != "stalled":
            raise Conflict(
                "'resume' only allowed for 'stalled' cases, "
                + f"current status:'{self.status}'"
            )
        stalled_until_date = self.str_to_date(stalled_until_date)
        stalled_since_date = self.str_to_date(stalled_since_date)
        stalled_delta = stalled_until_date - stalled_since_date

        new_completion_date = self.date_to_str(
            self.target_completion_date + timedelta(days=stalled_delta.days)
        )
        today = datetime.now(tz=gettz("Europe/Amsterdam")).date()

        is_less_then_stalled_until_date = (
            self.stalled_until_date is not None
            and stalled_since_date < self.stalled_until_date
        )

        new_stalled_since_date = today
        new_stalled_until_date = today

        if today > stalled_since_date or is_less_then_stalled_until_date:
            new_stalled_since_date = stalled_since_date

        if today > stalled_until_date:
            new_stalled_until_date = stalled_until_date

        self.set_status("open")
        self.set_target_completion_date(new_completion_date)

        self.stalled_until_date = new_stalled_until_date
        self.stalled_since_date = new_stalled_since_date
        self.resume_reason = resume_reason
        self.suspension_reason = None

        self._sync_subject()
        self._sync_subject_extern()

    def str_to_date(self, date_string: str) -> date:
        """Convert given date string to `datetime` object.

        :param date_string: date string to convert
        :type date_string: str format('2019-01-20')
        :return: datetime object
        :rtype: datetime
        """
        return datetime.strptime(date_string, "%Y-%m-%d").date()

    def date_to_str(self, date_obj: date) -> str:
        """Convert `datetime` object to string format('2019-01-20').

        :param date_obj: datetime object
        :type date_obj: datetime
        :return: date string
        :rtype: str format('2019-01-20')
        """
        return date_obj.strftime("%Y-%m-%d")

    def _is_valid_email(self, value: Any) -> bool:
        """Check if the supplied value is a valid email address."""
        if not self._is_valid_string(value):
            return False

        try:
            email_validator.validate_email(value)
        except email_validator.EmailNotValidError:
            return False

        return True

    def _is_valid_url(self, value: Any) -> bool:
        """Check if the value is a valid url."""
        parsed_url = urlparse(value)
        return bool(
            parsed_url.scheme and (parsed_url.netloc or parsed_url.path)
        )

    def _is_valid_string(self, value: Any) -> bool:
        """Check if the value is a string."""
        return isinstance(value, str)

    def _is_valid_uppercase_string(self, value: Any) -> bool:
        """Check if the the string is a valid uppercase string."""
        if not self._is_valid_string(value):
            return False

        return value.upper() == value

    def _is_valid_number(self, number: Any) -> bool:
        """Check if the parameter is a number."""
        return isinstance(number, int)

    def _is_valid_iban(self, value: Any) -> bool:
        """Check if the value is a valid IBAN"""
        if not self._is_valid_string(value):
            return False

        LETTERS = {
            ord(d): str(i)
            for i, d in enumerate(string.digits + string.ascii_uppercase)
        }

        try:
            number_iban_1 = (value[4:] + value[:4]).translate(LETTERS)
            number_iban_2 = (value[:2] + "00" + value[4:]).translate(LETTERS)
            iban_check_digits = f"{98 - (int(number_iban_2) % 97):0>2}"
        except ValueError:
            return False

        if int(number_iban_1) % 97 == 1 and iban_check_digits:
            return True

        return False

    def _is_valid_currency(self, currency: Any) -> bool:
        """Check if the currency is valid."""
        currency_str = str(currency)
        return bool(re.match(r"\d+(?:[.]\d{2})?$", currency_str))

    def _is_valid_geo_json_point(self, geo_json: Any) -> bool:
        """Check if the value is a valid GeoJSON Point."""

        try:
            geo_json_type = geo_json["type"]
            features = geo_json["features"]
            feature = features[0]
            feature_type = feature["type"]
            geometry = feature["geometry"]
            geometry_type = geometry["type"]
            lon, lat = (geometry["coordinates"][0], geometry["coordinates"][1])
        except Exception:
            return False

        if (
            geo_json_type == "FeatureCollection"
            and len(features) == 1
            and feature_type == "Feature"
            and geometry_type == "Point"
            and -180 <= lat <= 180
            and -90 <= lon <= 90
        ):
            return True

        return False

    def _is_valid_phone_number(self, value: str) -> bool:
        """Check if valid telephone number.(10 to 15 digits and an optional + at the start).

        :param telephone_number: user_inputted telephone_number
        :type telephone_number: str or int
        :return: True if valid telephone_number
        :rtype: boolean
        """
        if not self._is_valid_string(value):
            return False

        return bool(re.match(r"^\+?\d{10,15}$", value))

    def _validate_custom_fields(
        self, custom_fields, case_type_version, allow_missing_required_fields
    ):
        if case_type_version:
            self._custom_fields_allowedness_check(
                custom_fields, case_type_version
            )
            if not allow_missing_required_fields:
                self._custom_fields_requiredness_check(
                    custom_fields, case_type_version
                )
            self._custom_fields_type_correctness_check(
                custom_fields, case_type_version
            )

    def _custom_fields_allowedness_check(
        self, custom_fields, case_type_version
    ):
        """Check if case_type allows the specified custom_fields. If any of the
        specified fields are not present in the case type, the Conflict exception is raised.

        :param custom_fields: custom fields
        :type custom_fields: dict
        :param case_type: case_type
        :type case_type: CasetypeVersionEntity
        :raises Conflict: When case_type has no phases in it.
        :raises Conflict: When any of the inputted custom_fields is not present in phase one of case_type.
        """
        custom_fields = set(custom_fields.keys())
        if len(case_type_version.phases) == 0:
            raise Conflict(
                f"Case type '{case_type_version.uuid}' has no phases in it.",
                "case_type/no_phases",
            )

        allowed_custom_fields = {
            custom_field.field_magic_string
            for custom_field in case_type_version.phases[0].custom_fields
            if custom_field.is_hidden_field is False
        }

        if custom_fields.difference(allowed_custom_fields):
            custom_fields_not_allowed = custom_fields.difference(
                allowed_custom_fields
            )
            raise Conflict(
                f"Custom fields '{','.join(custom_fields_not_allowed)}' is/are not allowed",
                "case/custom_fields_not_allowed",
            )

    def _custom_fields_requiredness_check(
        self, custom_fields, case_type_version
    ):
        """Check if all of the required custom_fields of case_type_version are present in inputted custom fields.
        If not raise Conflict.

        :param custom_fields: custom_fields
        :type custom_fields: dict
        :param case_type_version: case type
        :type case_type_version: CasetypeVersionEntity
        :raises Conflict: If any of the required custom fields is not present.
        """
        custom_fields = {magic_string for magic_string in custom_fields}

        required_custom_fields = {
            c_f.field_magic_string
            for c_f in case_type_version.phases[0].custom_fields
            if c_f.is_required
        }

        if not required_custom_fields.issubset(custom_fields):
            missing_custom_fields = required_custom_fields.difference(
                custom_fields
            )
            raise Conflict(
                f"Custom fields '{','.join(missing_custom_fields)}' is/are required",
                "case/required_custom_fields_missing",
            )

    def _assert_valid_checkbox_type(
        self,
        custom_field_magic_string,
        custom_field_values,
        custom_field_options,
    ):
        """Assert valid values for check_box custom_field.

        :param custom_field_magic_string: magic string of custom field
        :type custom_field_magic_string: str
        :param custom_field_values: list of check_box values
        :type custom_field_values: list
        :param custom_field_options: Valid field options for check_box defined in case_type_version.
        :type custom_field_options: list
        """
        if any(not isinstance(value, list) for value in custom_field_values):
            raise Conflict(
                f"Value for custom_field '{custom_field_magic_string}' is not a list",
                "case/custom_field_check_box_value_not_list",
            )

        if len(custom_field_values) > 1:
            raise Conflict(
                "Multiple values for check box is not supported",
                "case/multi_value_checkbox_bot_supported",
            )

        for value in custom_field_values:
            for option in value:
                if option not in custom_field_options:
                    raise Conflict(
                        f"Option '{option}' for custom_field '{custom_field_magic_string}' is not in '{','.join(custom_field_options)}'",
                        "case/custom_field_invalid_option",
                    )

    def _assert_valid_option_or_select_type(
        self,
        custom_field_magic_string,
        custom_field_values,
        custom_field_options,
    ):
        """Assert valid values for options or select custom_field.

        :param custom_field_magic_string: magic string of custom field
        :type custom_field_magic_string: str
        :param custom_field_values: list of values for option/select
        :type custom_field_values: list
        :param custom_field_values: Valid filed options for option/select defined in case_type_version.
        :type custom_field_options: list
        """
        for option in custom_field_values:
            if option not in custom_field_options:
                raise Conflict(
                    f"Option '{option}' for custom_field '{custom_field_magic_string}' is not in '{','.join(custom_field_options)}'",
                    "case/custom_field_invalid_option",
                )

    def _calculate_date_from_custom_field_date_limit(self, date_limit):
        """Calculate date from date_limit of custom_field.

        :param date_limit: dictionary defines the date_limit for custom_field
        :type date_limit: dict
        :return: date
        :rtype: date
        """
        if date_limit and date_limit.active:
            interval_type = date_limit.interval_type
            interval = date_limit.interval
            if date_limit.during == "pre":
                interval = -(interval)
            if date_limit.reference == "current":
                reference_date = date.today()

            calculated_date = date_calculation.add_timedelta_to_date(
                interval_type, interval, reference_date
            )
            return calculated_date

    def _assert_valid_date_type(
        self, custom_field_magic_string, custom_field_values, date_field_limit
    ):
        """Assert valid values for date custom_field.

        :param custom_field_magic_string: magic string of custom field
        :type custom_field_magic_string: str
        :param custom_field_values: list of values for custom field
        :type custom_field_values: list
        :param date_field_limit: Dictionary defines the date limits for date custom_field.
        :type date_field_limit: dict
        :raises Conflict: When custom field value is of invalid type.
        """
        start_limit = date_field_limit.start
        end_limit = date_field_limit.end

        for value in custom_field_values:
            try:
                date = self.str_to_date(value)
            except Exception as e:
                raise Conflict(
                    f"Value '{value}' for custom field '{custom_field_magic_string}' is not of type 'date'",
                    "case/custom_field_invalid_type",
                ) from e

            start_date = self._calculate_date_from_custom_field_date_limit(
                start_limit
            )
            end_date = self._calculate_date_from_custom_field_date_limit(
                end_limit
            )

            if self._invalid_dates(start_date, end_date):
                raise Conflict(
                    f"Start date '{self.date_to_str(start_date)}' is greater than '{self.date_to_str(end_date)}'",
                    "case/custom_field_start_date_greater_than_end_date",
                )

            if self._invalid_dates(start_date, date):
                raise Conflict(
                    f"Date '{value}' is less than start date '{self.date_to_str(start_date)}'",
                    "case/custom_field_date_less_than_start_date",
                )

            if self._invalid_dates(date, end_date):
                raise Conflict(
                    f"Date '{value}' is greater than end date '{self.date_to_str(end_date)}'",
                    "case/custom_field_date_greater_than_end_date",
                )

    def _invalid_dates(self, first_date, second_date):
        """Check if first_date is greater than second_date.

        :param first_date: first date
        :type first_date: date or None
        :param second_date: second date
        :type second_date: date or None
        :return: True if first_date is greater than second_date.
        :rtype: bool
        """
        if None in [first_date, second_date]:
            return False

        if first_date > second_date:
            return True

        return False

    def _assert_valid_text_or_numeric_type(
        self, custom_field_magic_string, custom_field_values, custom_field_type
    ):
        """Assert valid values for text or numeric custom_field.

        :param custom_field_magic_string: magic string of custom field
        :type custom_field_magic_string: str
        :param custom_field_values: list of values for custom field
        :type custom_field_values: list
        :param custom_field_type: Type of custom field defined in case_type_version.
        :type custom_field_type: str
        :raises Conflict: When custom field value is of invalid type.
        """

        type_check_mapping = {
            "text": self._is_valid_string,
            "richtext": self._is_valid_string,
            "textarea": self._is_valid_string,
            "text_uc": self._is_valid_uppercase_string,
            "url": self._is_valid_url,
            "image_from_url": self._is_valid_url,
            "bankaccount": self._is_valid_iban,
            "email": self._is_valid_email,
            "numeric": self._is_valid_number,
            "valuta": self._is_valid_currency,
            "valutain": self._is_valid_currency,
            "valutaex": self._is_valid_currency,
            "valutain6": self._is_valid_currency,
            "valutaex6": self._is_valid_currency,
            "valutain21": self._is_valid_currency,
            "valutaex21": self._is_valid_currency,
        }
        is_valid_type = type_check_mapping[custom_field_type]
        for value in custom_field_values:
            if not is_valid_type(value):
                raise Conflict(
                    f"Value '{value}' for custom field '{custom_field_magic_string}' is not of type '{custom_field_type}'",
                    "case/custom_field_invalid_type",
                )

    def _assert_valid_geolatlon_type(
        self, custom_field_magic_string, custom_field_values
    ):
        """Assert valid values for geolatlon custom field.

        :param custom_field_magic_string: magic string of custom field
        :type custom_field_magic_string: str
        :param custom_field_values: values for custom field
        :type custom_field_values: list
        :raises Conflict: When the custom field value is not of type geolatlon
        """
        for value in custom_field_values:
            if not self._is_valid_geo_json_point(value):
                raise Conflict(
                    f"Value '{value}' for custom field '{custom_field_magic_string}' is not a valid geoJSON Point",
                    "case/custom_field_invalid_type",
                )

    def _custom_fields_type_correctness_check(
        self, custom_fields, case_type_version
    ):
        """Check if the value of inputted custom_fields are of valid type.

        :param custom_fields: custom_fields
        :type custom_fields: dict
        :param case_type: case_type_version
        :type case_type: CasetypeVersionEntity
        :raises Conflict: If the value of inputted custom_field is not of valid type.
        """

        for magic_string, custom_field_values in custom_fields.items():
            if not isinstance(custom_field_values, list):
                raise Conflict(
                    f"Custom field values '{custom_field_values}' for '{magic_string}' is not a list",
                    "case/custom_field_values_not_list",
                )

        for magic_string, custom_field_values in custom_fields.items():
            for c_f in case_type_version.phases[0].custom_fields:
                if c_f.field_magic_string == magic_string:
                    (
                        custom_field_type,
                        custom_field_options,
                        date_field_limit,
                    ) = (
                        c_f.field_type,
                        c_f.field_options,
                        c_f.date_field_limit,
                    )
                    break

            if custom_field_type == "checkbox":
                self._assert_valid_checkbox_type(
                    magic_string, custom_field_values, custom_field_options
                )

            elif custom_field_type in ["option", "select"]:
                self._assert_valid_option_or_select_type(
                    magic_string, custom_field_values, custom_field_options
                )

            elif custom_field_type == "date":
                self._assert_valid_date_type(
                    magic_string, custom_field_values, date_field_limit
                )

            elif custom_field_type == "geolatlon":
                self._assert_valid_geolatlon_type(
                    magic_string, custom_field_values
                )

            else:
                self._assert_valid_text_or_numeric_type(
                    magic_string, custom_field_values, custom_field_type
                )

    def _modify_custom_fields(self, custom_fields):
        """Modify custom_fileds from request_params to match custom_fields in JSON response of get_case,
        which is like {cf_magic_string: {"value": [value1, value2]}}
        """
        modified_custom_fields = {}
        for magic_string, custom_field_values in custom_fields.items():
            modified_custom_fields.update(
                {magic_string: {"value": custom_field_values}}
            )
        return modified_custom_fields

    def _assert_valid_contact_information(self, contact_information: dict):
        """Assert valid contact information for a case.

        :param contact_information: contact_information
        :type contact_information: dict
        :raises Conflict: If invalid mobile_number or phone_number or email is given
        """
        if contact_information.get(
            "mobile_number"
        ) and not self._is_valid_phone_number(
            contact_information["mobile_number"]
        ):
            mobile_number = contact_information["mobile_number"]
            raise Conflict(
                f"'{mobile_number}' is not a valid mobile number",
                "case/invalid_mobile_number",
            )
        if contact_information.get(
            "phone_number"
        ) and not self._is_valid_phone_number(
            contact_information["phone_number"]
        ):
            phone_number = contact_information["phone_number"]
            raise Conflict(
                f"'{phone_number}' is not a valid phone number",
                "case/invalid_phone_number",
            )

        if "email" in contact_information:
            email_address = contact_information["email"]
            try:
                email_validator.validate_email(email_address)
            except email_validator.EmailNotValidError as e:
                raise Conflict(
                    f"'{email_address}' is not a valid email address",
                    "case/invalid_email",
                ) from e

    @Entity.event(
        name="CaseAssigneeChanged", fire_always=True, extra_fields=["uuid"]
    )
    def set_assignee(
        self,
        assignee: CaseContactEmployee,
        send_email_to_assignee: bool = False,
    ):
        """Set assignee for a case.

        :param assignee: SubjectEntity of new assignee
        :type assignee: Subject
        """
        if self.status == "resolved":
            raise Conflict(
                "Assigning a case not allowed for resolved cases.",
                "case/assign_case_not_allowed_for_resolved_cases",
            )

        if isinstance(assignee, CaseContactEmployee):
            self.assignee = assignee
            self.send_email_to_assignee = send_email_to_assignee
        else:
            raise TypeError("Parameter assignee is not of type Subject.")

        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CaseCoordinatorSet", fire_always=True)
    def set_coordinator(
        self,
        coordinator: CaseContactEmployee,
    ):
        """Set coordinator for a case.

        :param coordinator: SubjectEntity of new coordinator
        :type coordinator: SubjectEntity
        """
        if isinstance(coordinator, CaseContactEmployee):
            self.coordinator = coordinator
        else:
            raise TypeError("Parameter assignee is not of type Subject.")
        if isinstance(coordinator, CaseContact):
            self.coordinator = coordinator
        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CaseStatusSet", fire_always=True)
    def set_status(
        self,
        status: str,
    ):
        """Setting the status of the case.

        :param status: status
        :type status: str
        """
        self.status = status
        self._sync_subject()
        self._sync_subject_extern()

    @case_event_decorator(name="CaseCreated")
    def create(
        self,
        contact_channel: str,
        requestor: CaseContact,
        custom_fields: dict | None,
        allow_missing_required_fields: bool,
        case_type_version: object,
        confidentiality: str | None,
        contact_information: dict | None,
        related_contacts: list | None,
    ):
        """Create a case.

        :param contactchannel: contact channel for the case.
        :type contactchannel: str
        :param requestor: requestor of the case.
        :type requestor: dict
        :param custom_fields: custom_fields for the case
        :type custom_fields: Optional[dict]
        :param allow_missing_required_fields: flag to skip required custom fields
        :type allow_missing_required_fields: bool
        :param confidentiality: confidentiality for the case
        :type confidentiality: Optional[str]
        :param case_type_version: case_type_version for the case
        :type case_type_version: CasetypeVersionEntity
        """
        today = datetime.utcnow()
        confidentiality = confidentiality or "public"
        custom_fields = custom_fields or {}

        if confidentiality not in ["public", "internal", "confidential"]:
            raise Conflict(
                f"Value '{confidentiality}' is not allowed for confidentiality. Allowed values are 'public, internal, confidential'",
                "case/invalid_confidentiality",
            )

        self.requestor = requestor
        self.contact_channel = contact_channel
        self.status = "new"
        self.created_date = today
        self.registration_date = today
        self.last_modified_date = today
        self.milestone = FIRST_PHASE_STATUS
        self.case_type_version = case_type_version
        self.request_trigger = "extern"
        self.subject = case_type_version.case_summary
        self.subject_extern = case_type_version.case_public_summary
        self.target_completion_date = self._calculate_target_completion_date(
            interval_for_completion=case_type_version.terms.lead_time_service.value,
            interval_for_completion_type=case_type_version.terms.lead_time_service.type,
            registration_date=today.date(),
        )
        self.related_contacts = related_contacts
        self._validate_custom_fields(
            custom_fields=custom_fields,
            case_type_version=case_type_version,
            allow_missing_required_fields=allow_missing_required_fields,
        )
        self.custom_fields = self._modify_custom_fields(
            custom_fields=custom_fields
        )

        self.confidentiality = confidentiality

        if contact_information:
            self._assert_valid_contact_information(contact_information)
            self.contact_information = contact_information

        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="SubcasesEnqueued", fire_always=True)
    def enqueue_subcases(self):
        data = {}
        for action in self.case_actions[self.milestone].get("cases", []):
            if action["automatic"]:
                data[str(uuid4())] = action["data"]
        self.enqueued_subcases_data = json.dumps(data)

    @Entity.event(name="SubcasesCreated", fire_always=True)
    def create_subcases(self, queue_ids):
        self.queue_ids = json.dumps(queue_ids)

    @Entity.event(name="DocumentsEnqueued", fire_always=True)
    def enqueue_documents(self):
        data = {}
        for action in self.case_actions[self.milestone].get("documents", []):
            if action["automatic"]:
                data[str(uuid4())] = action["data"]
        self.enqueued_documents_data = json.dumps(data)

    @Entity.event(name="DocumentsGenerated", fire_always=True)
    def generate_documents(self, queue_ids):
        self.queue_ids = json.dumps(queue_ids)

    @Entity.event(name="EmailsEnqueued", fire_always=True)
    def enqueue_emails(self):
        data = {}
        for action in self.case_actions[self.milestone].get("emails", []):
            if action["automatic"]:
                data[str(uuid4())] = action["data"]
        self.enqueued_emails_data = json.dumps(data)

    @Entity.event(name="EmailsGenerated", fire_always=True)
    def generate_emails(self, queue_ids):
        self.queue_ids = json.dumps(queue_ids)

    @Entity.event(name="SubjectsEnqueued", fire_always=True)
    def enqueue_subjects(self):
        data = {}
        for action in self.case_actions[self.milestone].get("subjects", []):
            if action["automatic"]:
                data[str(uuid4())] = action["data"]
        self.enqueued_subjects_data = json.dumps(data)

    @Entity.event(name="SubjectsGenerated", fire_always=True)
    def generate_subjects(self, queue_ids):
        self.queue_ids = json.dumps(queue_ids)

    @Entity.event(name="CaseTransitioned", fire_always=True)
    def transition_case(self):
        pass

    def _full_date_format(self, input_date):
        return input_date.strftime("%d-%m-%Y %H:%M:%S")

    def magic_strings_lookup(self):
        streefafhandeldatum = self.target_completion_date
        if self.status == "stalled":
            streefafhandeldatum = "Opgeschort"

        #   attributes = self.attributes or {}
        result = {}
        if self.result is not None:
            result = self.result

        payment = result.get("payment", {})
        archival_attributes = result.get("archival_attributes", {})

        return {
            "zaaknummer": self.id,
            "alternatief_zaaknummer": self.alternative_case_number,
            "startdatum": "".join(
                map(
                    str,
                    [
                        self.registration_date.day,
                        self.registration_date.month,
                        self.registration_date.year,
                    ],
                )
            ),
            "laatst_gewijzigd": self.last_modified_date,
            "datum_aangemaakt": self.created_date,
            "datum": datetime.now(timezone.utc),
            "null": None,
            "aggregatieniveau": "Dossier",
            "registratiedatum": self.registration_date,
            "registratiedatum_volledig": self._full_date_format(
                self.registration_date
            ),
            "afhandeldatum": self.completion_date,
            "afhandeldatum_volledig": self._full_date_format(
                self.completion_date
            ),
            "uiterste_vernietigingsdatum": self.destruction_date,
            "zaak_bedrag": payment.get("amount", 0),
            "bedrag_web": payment.get("amount", 0),
            "pdc_tarief": payment.get("amount", 0),
            "status": self.status,
            "archiefstatus": self.archival_state,
            "streefafhandeldatum": streefafhandeldatum,
            "contactkanaal": self.contact_channel,
            "opgeschort_tot": self.stalled_until_date,
            "opgeschort_sinds": self.stalled_since_date,
            "reden_opschorting": self.suspension_reason,
            "selectielijst": archival_attributes.get("selection_list", None),
            "vertrouwelijkheid": self.confidentiality,
            "behandelaar_id": getattr(self.assignee, "uuid", None),
            "behandelaar": getattr(self.assignee, "username", None),
            "behandelaar_nummer": getattr(self.assignee, "id", None),
            "coordinator_id": getattr(self.coordinator, "uuid", None),
            "coordinator": getattr(self.coordinator, "username", None),
            "coordinator_nummer": getattr(self.coordinator, "id", None),
            "zaak_onderwerp": self.subject,
            "zaak_onderwerp_external": self.subject_extern,
            "betaalstatus": payment.get("status", None),
            "resultaat": result.get("result", None),
            "resultaat_id": result.get("result_uuid", None),
        }

    @Entity.event(name="CaseAllocationSet", fire_always=True)
    def set_allocation(self, department: Department | None, role: Role | None):
        self.department = department
        self.role = role
        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CaseAssigneeEmailEnqueued", fire_always=True)
    def enqueue_assignee_email(self):
        self.assignee_email_queue_id = str(uuid4())

    @Entity.event(name="CaseAssigneeEmailSent", fire_always=True)
    def send_assignee_email(self, queue_id):
        self.queue_ids = json.dumps([queue_id])

    @Entity.event(name="CaseAssigneeCleared", fire_always=True)
    def clear_assignee(self):
        self.assignee = None
        self._sync_subject()
        self._sync_subject_extern()

    @Entity.event(name="CustomFieldUpdated", fire_always=False)
    def update_custom_field(self, magic_string: str, new_value):
        # Because we record changes to an attribute, this
        # will not work on a reference... We have to re-assign the object
        # afterwards.
        copy_custom_fields = (
            self.custom_fields.copy() if self.custom_fields else {}
        )
        copy_custom_fields[magic_string] = new_value
        self.custom_fields = copy_custom_fields

    def _sync_subject(self):
        template_service: "CaseTemplateService" = (
            zs_context_vars.try_get_template_service()
        )
        if template_service:
            subject = template_service.render_value(
                case=self, template_attribute="case_summary"
            )
            self.subject = subject

    def _sync_subject_extern(self):
        template_service: "CaseTemplateService" = (
            zs_context_vars.try_get_template_service()
        )
        if template_service:
            subject_extern = template_service.render_value(
                case=self, template_attribute="case_public_summary"
            )
            self.subject_extern = subject_extern

    @Entity.event(name="SetCaseParent", fire_always=True, extra_fields=["id"])
    def set_case_parent(self, parent_uuid):
        self.parent_uuid = parent_uuid

    @Entity.event(name="CaseResultSet", fire_always=True)
    def set_case_result(self, case_type_result: CaseTypeResultBasic):
        if self.status not in ("open", "stalled", "new"):
            raise Conflict(
                f"Not allowed to update case_result for case with status {self.status}"
            )

        if case_type_result.trigger_archival:
            self.clear_destruction_date()
        else:
            if not self.completion_date:
                logger.warning(
                    f"Zaak {self.uuid}: no afhandeldatum. Not setting vernietingsdatum."
                )
            else:
                destruction_date = (
                    case_type_result._calculate_destruction_date(
                        self.completion_date
                    )
                )
                if destruction_date:
                    self.set_destruction_date(
                        destruction_date=destruction_date
                    )

            self.set_archival_state(
                "overdragen"
                if case_type_result.preservation_term_label == "Bewaren"
                and self.archival_state != "overdragen"
                else "vernietigen"
            )

        self.result = CaseResult(
            result_uuid=case_type_result.uuid,
            result=case_type_result.result,
            archival_attributes=None,
        )

    def update_destruction_date(
        self,
        type: str,
        destruction_date: date | None,
        reason: str,
        preservation_term_info: dict | None,
    ):
        if not self.status == "resolved":
            raise Conflict(
                "Destruction date can only be updated for resolved cases.",
                "case/change_destruction_date/not_allowed",
            )
        if type == ValidCaseDestructionDateType.clear:
            self.clear_destruction_date(reason=reason)
        if type == ValidCaseDestructionDateType.set:
            self.set_destruction_date(destruction_date, reason)
        if type == ValidCaseDestructionDateType.recalculate:
            self.recalculate_destruction_date(preservation_term_info, reason)

    @Entity.event(name="CaseDestructionDateCleared", fire_always=True)
    def clear_destruction_date(self, reason=None):
        self.destruction_date = None
        if reason:
            self.destruction_reason = DestructionReason(
                reason=reason, label=None
            )

    @Entity.event(name="CaseDestructionDateSet", fire_always=True)
    def set_destruction_date(self, destruction_date: date, reason=None):
        self.destruction_date = destruction_date
        if reason:
            self.destruction_reason = DestructionReason(
                reason=reason, label=None
            )

    @Entity.event(name="CaseDestructionDateRecalculated", fire_always=True)
    def recalculate_destruction_date(
        self, preservation_term_info, reason=None
    ):
        destruction_date = self._destruction_date_by_term(
            preservation_term_info
        )
        self.destruction_date = destruction_date
        if reason:
            self.destruction_reason = DestructionReason(
                reason=reason,
                label=preservation_term_info.preservation_term_label,
            )

    def _destruction_date_by_term(self, preservation_term_info):
        unit = preservation_term_info.preservation_term_unit
        amount = int(preservation_term_info.preservation_term_unit_amount)
        delta = None
        if (
            self.completion_date is None
            or preservation_term_info.preservation_term_label == "Bewaren"
        ):
            return None

        if unit == "week":
            delta = relativedelta(weeks=amount)
        elif unit == "month":
            delta = relativedelta(months=amount)
        elif unit == "year":
            delta = relativedelta(years=amount)
        else:
            return None

        return self.completion_date + delta

    @Entity.event(name="CaseArchivalStateSet", fire_always=True)
    def set_archival_state(self, archival_state: str):
        self.archival_state = archival_state

    @Entity.event(name="CasePaymentAmountSet", fire_always=True)
    def set_payment_amount(self, payment_amount: float):
        payment = self.payment or CasePayment(amount=None, status=None)
        payment.amount = payment_amount
        self.payment = payment

    @Entity.event(name="CustomFieldSync", fire_always=True)
    def sync_custom_fields(self, custom_fields):
        self.custom_fields = custom_fields

    @Entity.event(name="GeoSynced", fire_always=True)
    def case_geo_sync(self):
        self.custom_fields = self.custom_fields
        self.last_modified_date = self.last_modified_date

    @Entity.event(
        name="AddValidsignTimelineEntry",
        fire_always=True,
    )
    def add_validsign_timeline_entry(self, participants):
        self.document_sign_participants = participants

    @Entity.event(name="AssigneeNotified", fire_always=False)
    def notify_assignee(self, template: "EmailTemplate"):
        """
        Notify the assignee of a case by sending a templated email that
        someone else changed "their" case.
        """
        # This should only be called on cases with an assignee, and if that
        # assignee has an email addres configured.
        assert self.assignee
        assert self.assignee.email

        template_service = cast(
            "CaseTemplateService | None",
            zs_context_vars.try_get_template_service(),
        )

        # This should always be called from a command context that loads the
        # template service; it should never be `None` here
        assert template_service, (
            "Template service must exist for notification to work."
        )

        subject = template_service.render_string(
            case=self,
            template=template.subject,
        )
        body = template_service.render_string(
            case=self,
            template=template.body,
        )

        self.notification = CaseNotification(
            sender_name=template.sender,
            sender_address=template.sender_address,
            subject=subject,
            body=body,
            recipient_address=self.assignee.email,
        )

        return

    @Entity.event(
        name="CaseDeleted",
        fire_always=False,
        extra_fields=["id", "confidentiality"],
    )
    def delete(self):
        self.status = CaseStatus.deleted
        self.deleted = datetime.now()


class ValidEventCategory(enum.StrEnum):
    document_category = "document"
    case_update = "case_update"
