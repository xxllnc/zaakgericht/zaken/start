# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from . import _shared
from minty.entity import Entity
from pydantic.v1 import Field, NonNegativeInt
from uuid import UUID


class CaseBasic(Entity):
    entity_type = "case_basic"
    entity_id__fields: list[str] = ["uuid"]
    entity_relationships: list[str] = [
        "assignee",
        "case_type_version",
        "case_type",
        "coordinator",
        "department",
        "requestor",
        "role",
        "recipient",
    ]
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "entity_meta_authorizations",
    ]
    entity_meta_authorizations: set[_shared.CaseAuthorizationLevel] = Field(
        ..., title="Authorizations the current user has for this case"
    )

    uuid: UUID = Field(..., title="Internal identifier of this entity")

    number: int = Field(..., title="Id of Case")
    custom_fields: dict = Field(..., title="custom fields for the case")

    result: _shared.CaseResult | None = Field(None, title="Case result")

    confidentiality: _shared.CaseConfidentiality = Field(
        ..., title="Confidentiality level of the case"
    )

    contact_channel: str = Field(
        ..., title="Contact channel used to register the case"
    )

    milestone: NonNegativeInt = Field(
        ...,
        title="Latest milestone (phase) that was successfully completed in the case.",
    )

    payment: _shared.CasePayment | None = Field(
        None, title="Payment details for the case, if applicable"
    )

    progress_status: float | None = Field(
        None,
        title="Current progress of the case, as a percentage of allotted time passed; empty if case is stalled",
    )

    status: _shared.CaseStatus = Field(..., title="Current state of the case")
    html_email_template: str | None = Field(
        None, title="Shortname of the HTML template to use for emails"
    )

    registration_date: datetime.date = Field(
        ..., title="Registration date of the case"
    )

    stalled_since_date: datetime.date | None = Field(
        None,
        title="If the case status is 'stalled', this indicates when that started.",
    )
    stalled_until_date: datetime.date | None = Field(
        None,
        title="If the case status is 'stalled', this indicates when it is planned to end.",
    )

    target_completion_date: datetime.date = Field(
        ..., title="Target completion date for the case"
    )
    completion_date: datetime.date | None = Field(
        ..., title="Completion date for the case"
    )
    destruction_date: datetime.date | None = Field(
        None, title="Date when the case should be destroyed (if set)"
    )

    summary: str = Field(..., title="Summary of the case")
    public_summary: str = Field(..., title="Public summary of the case")

    # parent: reference
    case_type: _shared.RelatedCaseType = Field(
        ..., title="Reference to the case type of this case"
    )
    case_type_version: _shared.RelatedCaseTypeVersion = Field(
        ...,
        title="Reference to the specific version of the case type of this case",
    )

    requestor: (
        _shared.RelatedPerson
        | _shared.RelatedOrganization
        | _shared.RelatedEmployee
    ) = Field(..., title="Requestor relationship")
    requestor_is_preset_client: bool = Field(
        ...,
        title="Indicates whether the current requestor of the case is the preset requestor in the case type",
    )

    assignee: _shared.RelatedEmployee | None = Field(
        None, title="Assignee relationship"
    )
    coordinator: _shared.RelatedEmployee | None = Field(
        None, title="Coordinator relationship"
    )

    recipient: None | (
        _shared.RelatedPerson
        | _shared.RelatedOrganization
        | _shared.RelatedEmployee
    ) = Field(None, title="Recipient relationship")

    department: _shared.RelatedDepartment | None = Field(
        None, title="Department the case is assigned to"
    )
    role: _shared.RelatedRole | None = Field(
        None, title="Role the case is assigned to"
    )

    num_unaccepted_files: NonNegativeInt = Field(
        ..., title="Number of unaccepted documents in the case"
    )
    num_unaccepted_updates: NonNegativeInt = Field(
        ..., title="Number of unaccepted attribute updates in the case"
    )
    num_unread_communication: NonNegativeInt = Field(
        ..., title="Number of unread communications (emails, etc.) in the case"
    )
