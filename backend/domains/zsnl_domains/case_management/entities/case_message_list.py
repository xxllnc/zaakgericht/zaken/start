# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .subject import Subject
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from uuid import UUID


class CaseMessageList(EntityBase):
    @property
    def entity_id(self):
        return self.case_uuid

    def __init__(self, case_uuid: UUID, case_id: int):
        self.case_id = case_id
        self.case_uuid = case_uuid
        self.user = None

    @event("CaseMessagesReassigned", extra_fields=["case_id"])
    def assign_to_user(self, user: Subject):
        """Assign case messages to the specified subject

        :param subject: Subject to (re)assign to
        :type subject: Subject
        """
        if user.subject_type != "employee":
            raise Conflict("Messages can only be assigned to employees")
        self.user = user
