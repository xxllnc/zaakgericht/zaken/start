# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from ._shared import RelatedEmployee, RelatedOrganization, RelatedPerson
from .case_type_result import CaseTypeResultBasic
from .department import Department
from .role import Role
from datetime import datetime
from minty.entity import Entity, ValueObject
from pydantic.v1 import Field
from typing import Any
from uuid import UUID


class CaseTypeMetaData(ValueObject):
    # Required metadata fields
    legal_basis: str = Field(..., title="Source of the person")
    process_description: str = Field(
        ..., title="Process description of case_type"
    )

    # Optional metadata fields
    may_postpone: str | None = Field(None, title="Source of the person")
    may_extend: str | None = Field(None, title="Source of the person")
    extension_period: str | None = Field(None, title="Source of the person")
    adjourn_period: str | None = Field(None, title="Source of the person")
    e_webform: str | None = Field(None, title="Source of the person")
    motivation: str | None = Field(None, title="Source of the person")
    purpose: str | None = Field(None, title="Source of the person")

    archive_classification_code: str | None = Field(
        None, title="Source of the person"
    )
    designation_of_confidentiality: str | None = Field(
        None, title="Source of the person"
    )
    responsible_subject: str | None = Field(None, title="Source of the person")
    responsible_relationship: str | None = Field(
        None, title="Source of the person"
    )
    possibility_for_objection_and_appeal: str | None = Field(
        None, title="Source of the person"
    )
    publication: str | None = Field(None, title="Source of the person")
    publication_text: str | None = Field(None, title="Source of the person")
    bag: str | None = Field(None, title="Source of the person")
    lex_silencio_positivo: str | None = Field(
        None, title="Source of the person"
    )
    penalty_law: str | None = Field(None, title="Source of the person")
    wkpb_applies: str | None = Field(None, title="Source of the person")
    local_basis: str | None = Field(None, title="Source of the person")


class CaseTypePaymentDict(ValueObject):
    amount: str | None = Field(None, title="Source of the person")


class CaseTypePayment(ValueObject):
    assignee: CaseTypePaymentDict | None = Field(
        None, title="Payment for assignee"
    )
    frontdesk: CaseTypePaymentDict | None = Field(
        None, title="Payment for frontdesk"
    )
    phone: CaseTypePaymentDict | None = Field(None, title="Payment for phone")
    mail: CaseTypePaymentDict | None = Field(None, title="Payment for mail")
    webform: CaseTypePaymentDict | None = Field(
        None, title="Payment for webform"
    )
    post: CaseTypePaymentDict | None = Field(None, title="Payment for post")


class CaseTypeDefaultFolder(ValueObject):
    name: str = Field(..., title="Name of default folder")


class CaseTypeSettings(ValueObject):
    allow_reuse_casedata: bool | None = Field(
        False, title="Boolean indicates allow reuse of case_data"
    )
    enable_webform: bool | None = Field(
        False, title="Boolean indicates enable of webform"
    )
    enable_online_payment: bool | None = Field(
        False, title="Boolean indicates enable online payment"
    )
    enable_manual_payment: bool | None = Field(
        False, title="Boolean indicates enable manual payment"
    )
    require_email_on_webform: bool | None = Field(
        False, title="Boolean indicates require email on webform"
    )
    require_phonenumber_on_webform: bool | None = Field(
        False, title="Boolean indicates require phonenumber on webform"
    )
    require_mobilenumber_on_webform: bool | None = Field(
        False, title="Boolean indicates require mobile_number on webform"
    )
    disable_captcha_for_predefined_requestor: bool | None = Field(
        False,
        title="Boolean indicates disable_captcha_for_predefined_requestor",
    )
    show_confidentiality: bool | None = Field(
        False, title="Boolean indicates showing confidentiality"
    )
    show_contact_info: bool | None = Field(
        False, title="Boolean indicates showing contact info"
    )
    enable_subject_relations_on_form: bool | None = Field(
        False, title="Boolean indicates enabling subject relations on form"
    )
    open_case_on_create: bool | None = Field(
        False, title="Boolean indicates open case on create"
    )
    enable_allocation_on_form: bool | None = Field(
        False, title="Boolean indicates enable allocation on requestor"
    )
    disable_pip_for_requestor: bool | None = Field(
        False, title="Boolean indicates disable pip for requestor"
    )
    allow_external_task_assignment: bool | None = Field(
        False,
        title="Allow tasks in cases of this case type to be assigned to external contacts",
    )

    list_of_default_folders: list[CaseTypeDefaultFolder] | None = Field(
        [], title="List of case_type default folders"
    )

    lock_registration_phase: bool | None = Field(
        False, title="Boolean indicates lock_registration_phase"
    )
    enable_queue_for_changes_from_other_subjects: bool | None = Field(
        False,
        title="Boolean indicates if queue for changes from other subjects is enabled",
    )
    check_acl_on_allocation: bool | None = Field(
        False, title="Boolean indicates if there is an ACL check on allocation"
    )
    api_can_transition: bool | None = Field(
        False, title="Boolean indicates if API can transition"
    )
    is_public: bool | None = Field(
        False, title="Boolean indicates if case_type is public"
    )
    text_confirmation_message_title: str | None = Field(
        None, title="Text confirmation message title"
    )

    text_public_confirmation_message: str | None = Field(
        None, title="Text public confirmation message title"
    )
    payment: CaseTypePayment = Field(..., title="Case type payment")


class CaseTypeRequestor(ValueObject):
    type_of_requestors: list[str] | None = Field(
        [], title="Source of the person"
    )
    use_for_correspondence: bool | None = Field(
        False, title="Source of the person"
    )


class CaseTypeAllocation(ValueObject):
    department: Department = Field(..., title="Department assigned for phase")
    role: Role = Field(..., title="Role assigned for phase")
    role_set: int | None = Field(None, title="Role set for phase")


class RelatedCaseRequestor(ValueObject):
    requestor_type: str = Field(..., title="Type of requestor for case")
    related_role: str | None = Field(None, title="Related role for case")


class CaseRelatedToCaseType(ValueObject):
    # Required metadata fields

    type_of_relation: str = Field(..., title="Type of case_relation")
    related_casetype_element: UUID = Field(
        ..., title="UUID of related element"
    )
    copy_custom_fields_from_parent: bool | None = Field(
        False, title="Copy custom_fields from parent"
    )
    start_on_transition: bool | None = Field(
        False, title="Boolean to check if the case_type starts on transition"
    )
    open_case_on_create: bool | None = Field(
        False, title="Open case on create"
    )
    resolve_before_phase: int | None = Field(
        None, title="Resolve before phase"
    )
    show_in_pip: bool | None = Field(False, title="Show the case in pip")
    label_in_pip: str | None = Field(None, title="Label case in pip")
    requestor: RelatedCaseRequestor | None = Field(
        None, title="Requestor of related case"
    )
    allocation: CaseTypeAllocation | None = Field(
        None, title="Allocation of case_type"
    )


class CustomFieldPublication(ValueObject):
    name: str = Field(..., title="Name of custom field publication")


class CustomFieldDateLimitObject(ValueObject):
    active: bool = Field(..., title="Is the datelimit is active")
    interval_type: str = Field(..., title="Type of interval")
    interval: int = Field(..., title="Value of interval")
    during: str = Field(..., title="During time for datelimit")
    reference: str = Field(..., title="Reference of datelimit")


class CustomFieldDateLimit(ValueObject):
    start: CustomFieldDateLimitObject | None = Field(
        None, title="Name of custom field publication"
    )
    end: CustomFieldDateLimitObject | None = Field(
        None, title="Name of custom field publication"
    )


class CustomFieldRelatedToCaseType(ValueObject):
    name: str = Field(..., title="Name of custom field")
    uuid: UUID = Field(..., title="UUID of the catalog attribute")
    public_name: str | None = Field(None, title="Public name of custom_field")
    title: str | None = Field(None, title="Title of custom_object")
    is_multiple: bool | None = Field(
        False, title="Is the custom_field multiple choice"
    )
    title_multiple: str | None = Field(
        None, title="Title multiple of custom_field"
    )
    description: str | None = Field(None, title="Description of case_type")
    external_description: str | None = Field(
        None, title="External description of case_type"
    )
    is_required: bool | None = Field(
        False, title="Is the custom_field required"
    )
    requestor_can_change_from_pip: bool | None = Field(
        False, title="Can requestor chnage custom_field from pip"
    )
    is_hidden_field: bool | None = Field(
        False, title="Is the custom_field hidden"
    )
    enable_skip_of_queue: bool | None = Field(
        False, title="Is skip of queue enabled for case_type"
    )
    sensitive_data: bool | None = Field(
        False, title="Does the custom_field contains sensitive data"
    )
    field_magic_string: str = Field(..., title="Magic string of custom_field")
    field_type: str = Field(..., title="Type of custom_field")
    default_value: str | None = Field(
        None, title="Default value for custom_field"
    )
    referential: bool | None = Field(
        False, title="Referential for custom_field"
    )
    publish_on: list[CustomFieldPublication] | None = Field(
        [], title="Places where the case_type is published"
    )
    edit_authorizations: list[CaseTypeAllocation] | None = Field(
        [], title="Edit authorizations for custom_field"
    )
    field_options: list[str] | None = Field(
        [], title="Options available for custom_field"
    )
    date_field_limit: CustomFieldDateLimit = Field(
        [], title="Options available for custom_field"
    )

    relationship_uuid: UUID | None = Field(
        None, title="Inertnal identifier of custom_field relationship"
    )
    relationship_type: str | None = Field(None, title="Type of relationship")
    relationship_name: str | None = Field(
        None, title="Name of custom field relationship"
    )
    relationship_subject_role: str | None = Field(
        None, title="Name of custom field relationship"
    )


class DocumentData(ValueObject):
    description: str = "Sjabloon"
    filename: str = Field(..., title="Filename of document")
    target_format: str = Field(..., title="Target format for document")
    automatisch_genereren: bool | None = Field(
        False, title="If the document is automatically generated"
    )


class DocumentRelatedToCaseType(ValueObject):
    # Required metadata fields

    automatic: bool | None = Field(False, title="If the document is automatic")
    label: str = Field(..., title="Label on document")
    data: DocumentData = Field(..., title="Copy custom_fields from parent")


class CaseDocumentAttachment(ValueObject):
    selected: int = 1
    name: str = Field(..., title="Name of case_document attachment")
    case_document_ids: int = Field(..., title="Id of case document")


class EmailData(ValueObject):
    description: str = "E-mail"
    rcpt: str = Field(..., title="Email rcpt")
    subject: str = Field(..., title="Subject of email")
    body: str = Field(..., title="Body of email")
    sender_address: str | None = Field(None, title="Address of the sender")
    sender: str | None = Field(None, title="Name of sender")
    cc: str | None = Field(None, title="CC of email")
    bcc: str | None = Field(None, title="BCC of email")
    betrokkene_role: str | None = Field(None, title="Role of email receiver")
    email: str | None = Field(None, title="Email template")
    behandelaar: str | None = Field(None, title="Email receiver")
    intern_block: str | None = Field(None, title="Intern block of email")

    case_document_attachments: list[CaseDocumentAttachment] | None = Field(
        [], title="CC of email"
    )
    automatic_phase: bool | None = Field(
        False, title="If the email is automatic"
    )
    zaaktype_notificatie_id: int = Field(
        ..., title="Id of zaaktype_notificati"
    )
    bibliotheek_notificaties_uuid: UUID | None = Field(
        ..., title="UUID of bibliotheek_notificatie"
    )
    bibliotheek_notificaties_id: int = Field(
        ..., title="Id of bibliotheek_notificatie"
    )


class EmailRelatedToCaseType(ValueObject):
    # Required metadata fields

    automatic: bool = Field(..., title="If the email is automatic")
    label: str = Field(..., title="Label on email")
    data: EmailData = Field(..., title="Email data")


class SubjectData(ValueObject):
    betrokkene_identifier: str = "Identifier of releted subject"
    naam: str = Field(..., title="Name of subject")
    rol: str | None = Field(None, title="Role of subject")
    magic_string_prefix: str | None = Field(
        None, title="Magic string for subject"
    )
    gemachtigd: bool | None = Field(
        False, title="Boolean indicating if the subject is authorized"
    )
    notify: bool | None = Field(False, title="Should the subject get notified")
    uuid: UUID = Field(..., title="UUID of the subject")
    betrokkene_type: str = Field(..., title="Type of subject")


class SubjectRelatedToCaseType(ValueObject):
    # Required metadata fields

    automatic: bool = Field(..., title="If the email is automatic")
    label: str = Field(..., title="Label of subject relationship")
    data: SubjectData = Field(..., title="Subject data")


class RelatedFolder(Entity):
    entity_type = "folder"
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="UUID of the folder")
    name: str = Field(..., title="Name of folder")


class CaseTypeTermObject(ValueObject):
    type: str = Field(..., title="Type of case_type term")
    value: str = Field(..., title="Value of case_type term")


class CaseTypeTerms(ValueObject):
    lead_time_legal: CaseTypeTermObject = Field(..., title="Lead time legal")
    lead_time_service: CaseTypeTermObject = Field(
        ..., title="Lead time service"
    )


class StaticRuleAttribute(enum.StrEnum):
    contactchannel = "contactchannel"
    aanvrager = "aanvrager"
    payment_status = "payment_status"
    confidentiality = "confidentiality"
    vertrouwelijkheidsaanduiding = "vertrouwelijkheidsaanduiding"
    beroep_mogelijk = "beroep_mogelijk"
    publicatie = "publicatie"
    bag = "bag"
    lex_silencio_positivo = "lex_silencio_positivo"
    opschorten_mogelijk = "opschorten_mogelijk"
    verlenging_mogelijk = "verlenging_mogelijk"
    wet_dwangsom = "wet_dwangsom"
    wkpb = "wkpb"
    preferred_contact_channel = "preferred_contact_channel"


class StaticRuleAction(enum.StrEnum):
    show_attribute = "show_attribute"
    hide_attribute = "hide_attribute"
    show_text_block = "show_text_block"
    hide_text_block = "hide_text_block"
    show_group = "show_group"
    hide_group = "hide_group"
    pause_application = "pause_application"
    set_value = "set_value"
    set_value_formula = "set_value_formula"
    set_value_magic_string = "set_value_magic_string"
    date_calculations = "date_calculations"
    update_registation_date = "wijzig_registratiedatum"
    update_target_completion_date = "update_target_completion_date"
    generate_template = "generate_template"
    start_case = "start_case"
    set_allocation = "set_allocation"
    send_email = "send_email"
    add_subject = "add_subject"
    change_confidentiality = "change_confidentiality"
    change_html_mail_template = "change_html_mail_template"
    set_online_payment = "set_online_payment"
    send_external_system_message = "send_external_system_message"
    trigger_object_action = "trigger_object_action"

    @classmethod
    def get_default_actions(cls) -> list["StaticRuleAction"]:
        return [
            StaticRuleAction.show_attribute,
            StaticRuleAction.hide_attribute,
            StaticRuleAction.show_text_block,
            StaticRuleAction.hide_text_block,
            StaticRuleAction.show_group,
            StaticRuleAction.hide_group,
            StaticRuleAction.pause_application,
            StaticRuleAction.set_value,
            StaticRuleAction.set_value_formula,
            StaticRuleAction.set_value_magic_string,
            StaticRuleAction.date_calculations,
            StaticRuleAction.start_case,
            StaticRuleAction.change_confidentiality,
            StaticRuleAction.change_html_mail_template,
        ]

    @classmethod
    def get_phase_change_actions(cls) -> list["StaticRuleAction"]:
        #  this method is prepared to be used for phase change cmd's.
        #  currently we do not have them, that's why this is untested.
        actions = cls.get_default_actions()
        actions.extend(
            [
                StaticRuleAction.set_allocation,
                StaticRuleAction.update_registation_date,
                StaticRuleAction.generate_template,
                StaticRuleAction.send_email,
                StaticRuleAction.add_subject,
                StaticRuleAction.trigger_object_action,
                StaticRuleAction.send_external_system_message,
            ]
        )
        return actions

    @classmethod
    def get_case_create_actions(cls) -> list["StaticRuleAction"]:
        actions = cls.get_default_actions()
        actions.extend(
            [
                StaticRuleAction.update_registation_date,
                StaticRuleAction.update_target_completion_date,
                StaticRuleAction.set_online_payment,
            ]
        )
        return actions


class RuleCondition(ValueObject):
    value: Any = Field(None, title="Value of rule condition")
    kenmerk: StaticRuleAttribute | str | None = Field(
        None, title="Attribute releated to rule"
    )
    value_checkbox: Any = Field(None, title="Value checkbox")


class RuleAction(ValueObject):
    action: StaticRuleAction | str | None = Field(
        None, title="Case type rule action"
    )
    settings: dict = Field([], title="Settings related to this action")


class CasetypeRule(ValueObject):
    name: str = Field(..., title="Name of case_type_rule")
    condition_type: str = Field(..., title="Type of condition")
    conditions: list[RuleCondition] = Field(
        ..., title="List of rule conditions"
    )
    match_actions: list[RuleAction] = Field(
        ...,
        title="List of rule actions to execute when the condition matches",
    )
    nomatch_actions: list[RuleAction] | None = Field(
        [],
        title="List of rule actions to execute when the condition doesnot match",
    )


class CaseTypePhase(ValueObject):
    # # Required metadata fields

    name: str = Field(..., title="Name of case_type phase")
    milestone: int = Field(..., title="Milestone for case_type phase")
    phase: str = Field(..., title="Phase number for case_type phase")
    allocation: CaseTypeAllocation | None = Field(
        None, title="Allocation of case_type phase"
    )
    cases: list[CaseRelatedToCaseType] | None = Field(
        [], title="Cases releted to case_type"
    )
    custom_fields: list[CustomFieldRelatedToCaseType] | None = Field(
        [], title="Custom_fields releted to case_type"
    )
    documents: list[DocumentRelatedToCaseType] | None = Field(
        [], title="Documents releted to case_type"
    )
    emails: list[EmailRelatedToCaseType] | None = Field(
        [], title="Emails releted to case_type"
    )
    subjects: list[SubjectRelatedToCaseType] | None = Field(
        [], title="Subjects releted to case_type"
    )
    checklist_items: list[str] | None = Field(
        [], title="Checklist items releted to case_type"
    )
    rules: list[CasetypeRule] | None = Field([], title="Rules on case_type")


class CaseTypeVersionEntity(Entity):
    entity_type = "case_type"
    entity_id__fields: list[str] = ["uuid"]

    # Properties
    id: str = Field(..., title="ID of the case_type node")
    uuid: UUID = Field(..., title="UUID of the case_type node")
    case_type_uuid: UUID = Field(..., title="UUID of the case_type")

    version: int = Field(
        ...,
        title="Version number of case_type",
    )

    name: str = Field(..., title="Name of case_type")
    description: str | None = Field(None, title="Description of case_type")
    identification: str | None = Field(
        None, title="Identification for case_type"
    )
    tags: str | None = Field(None, title="Tags for case_type")
    case_summary: str | None = Field(None, title="Summary of case_type")
    case_public_summary: str | None = Field(
        None, title="Public summary of case_type"
    )

    active: bool = Field(..., title="Active status of case_type")

    phases: list[CaseTypePhase] = Field(
        ..., title="Array of phases for case_type"
    )

    requestor: CaseTypeRequestor = Field(..., title="Requestor of case_type")

    settings: CaseTypeSettings = Field(..., title="Case type settings")
    terms: CaseTypeTerms = Field(..., title="Terms for a case_type")
    metadata: CaseTypeMetaData = Field(..., title="Meta data of case_type")

    case_type_results: list[CaseTypeResultBasic] = Field(
        ..., title="Possible result for the case_type"
    )
    initiator_source: str | None = Field(
        None, title="Initiator source for case_type"
    )
    initiator_type: str | None = Field(
        None, title="Initiator type for case_type"
    )
    is_eligible_for_case_creation: bool = Field(
        ...,
        title="Boolean to check if the case_type is eligible for case creation",
    )

    created: datetime = Field(
        ...,
        title="Created date of case_type",
    )
    last_modified: datetime = Field(
        ...,
        title="Last modified date for case_type",
    )

    deleted: datetime | None = Field(
        None,
        title="Deleted date of case_type",
    )

    # Relationships
    catalog_folder: RelatedFolder | None = Field(
        None, title="Folder where the case_type is located"
    )
    preset_assignee: RelatedEmployee | None = Field(
        None, title="Preset assignee for case_type"
    )

    preset_requestor: None | (
        RelatedOrganization | RelatedPerson
    ) = Field(None, title="Preset requestor of the case type")

    entity_relationships: list[str] = [
        "catalog_folder",
        "preset_assignee",
        "preset_requestor",
    ]

    def get_custom_fields_of_type(
        self, field_type: str
    ) -> dict[str, CustomFieldRelatedToCaseType]:
        """
        Retrieve a dictionary containing of all custom fields of the given field_type
        in the case type, keyed on the magic_string of its attribute type.

        If an attribute is present in multiple phases, the last one found will be
        used.
        """

        return {
            custom_field.field_magic_string: custom_field
            for phase in self.phases
            for custom_field in phase.custom_fields
            if custom_field.field_type == field_type
        }
