# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class Country(Entity):
    "Data structure for country data"

    entity_type = "country"
    entity_id__fields: list[str] = ["uuid"]
    uuid: UUID = Field(..., title="Identifier for this country")
    name: str = Field(..., title="Name of the country")
    code: int = Field(..., title="Dutch code of the country")
