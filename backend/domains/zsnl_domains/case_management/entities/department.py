# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class DepartmentSummary(Entity):
    entity_type = "department"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Identifier for this department")
    name: str = Field(..., title="Name of the department")


class Department(DepartmentSummary):
    entity_relationships: list[str] = ["parent"]
    description: str = Field("", title="Longer description of the department")

    parent: DepartmentSummary | None = Field(
        None, title="Parent department of this department"
    )
