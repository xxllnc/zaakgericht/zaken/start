# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import RelatedEmployee
from minty.entity import Entity, ValueObject
from pydantic.v1 import Field
from uuid import UUID


class NotificationSettings(ValueObject):
    """Notification settings for an employee"""

    new_document: bool = Field(
        ..., title="Notify outside zaaksytem in case of new document events."
    )
    new_assigned_case: bool = Field(
        ...,
        title="Send notification outside zaaksytem when new case is assigned to the employee",
    )
    case_term_exceeded: bool = Field(
        ...,
        title="Send notification outside zaaksytem when the processing time of case has expired",
    )

    new_ext_pip_message: bool = Field(
        ...,
        title="Send notification outside zaaksyteem when employee receives new pip message",
    )
    new_attribute_proposal: bool = Field(
        ...,
        title="Send notification outside zaaksytem when attribute has changed",
    )
    case_suspension_term_exceeded: bool = Field(
        ...,
        title="Send notification when the suspension period of case has expired",
    )


class Signature(Entity):
    """Signature filestore of an employee"""

    entity_type = "file"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the signature filestore")


class EmployeeSettings(Entity):
    """Settings for an employee"""

    entity_type = "employee_settings"

    notification_settings: NotificationSettings | None = Field(
        None, title="Notification settings of employee"
    )
    entity_relationships: list[str] = ["signature", "employee"]
    signature: Signature | None = Field(None, title="Signature of employee")

    phone_extension: str | None = Field(
        None,
        title="The phone extension is used by the phone/PBX integration "
        "to show a pop-up when there's a call on the specified extension.",
    )

    employee: RelatedEmployee = Field(
        ..., title="Employee to which this settings are related"
    )

    # Events
    @Entity.event(
        name="NotificationSettingsSet",
        fire_always=True,
        extra_fields=["employee"],
    )
    def set_notification_settings(self, notification_settings: dict):
        self.notification_settings = NotificationSettings(
            **notification_settings
        )

    @Entity.event(
        name="SignatureSaved", fire_always=True, extra_fields=["employee"]
    )
    def save_signature(self, file_uuid: UUID):
        self.signature = Signature(
            **{"uuid": file_uuid, "entity_id": file_uuid}
        )

    @Entity.event(
        name="SignatureDeleted", fire_always=True, extra_fields=["employee"]
    )
    def delete_signature(self):
        self.signature = None

    @Entity.event(
        name="PhoneExtensionChanged",
        fire_always=True,
        extra_fields=["employee"],
    )
    def set_phone_extension(self, extension: str | None):
        self.phone_extension = extension
