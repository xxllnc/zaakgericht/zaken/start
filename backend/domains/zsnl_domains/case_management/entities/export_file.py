# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class ExportFile(Entity):
    "Data structure for export file data"

    entity_type = "export_file"
    entity_id__fields: list[str] = ["uuid"]
    uuid: UUID = Field(..., title="Identifier for this export_file")
    name: str = Field(..., title="Name of the export_file")
    expires_at: datetime = Field(
        ..., title="The date that export file expires"
    )
    downloads: int = Field(0, title="Number of downloads")
    token: str = Field(..., title="Token of the export_file")

    file_uuid: UUID
    storage_location: str
    mime_type: str

    @Entity.event("DownloadCounterIncreased", fire_always=False)
    def increase_download_counter(self):
        self.downloads += 1
