# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class ObjectRelation(Entity):
    """Entity represents object relation of attribute in the case"""

    entity_type = "object_relation"
    entity_id__fields: list[str] = ["uuid"]

    attribute_id: str | None = Field(..., title="Id of the the attribute")
    source_custom_field_type_id: UUID | None = Field(
        ..., title="Id of the the attribute"
    )
    magic_string: str = Field(..., title="Magic string of the attribute")
    uuid: UUID | None = Field(
        ..., title="UUID of the custom object relationship"
    )
    custom_object_uuid: UUID | None = Field(
        ..., title="UUID of the custom object"
    )
