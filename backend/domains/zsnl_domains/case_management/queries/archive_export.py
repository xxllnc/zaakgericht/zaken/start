# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from ...document.repositories import FileRepository as DocumentFileRepository
from ...shared.repositories.custom_fields import CustomFieldsRepository
from .. import repositories
from ..services import ArchiveTemplateService
from pydantic.v1 import validate_arguments
from typing import cast
from uuid import UUID


class RunArchiveExport(minty.cqrs.SplitQueryBase):
    name = "run_archive_export"

    @validate_arguments
    def __call__(self, case_uuid: UUID, export_file_uuid: UUID | None):
        archive_repo = cast(
            repositories.ArchiveExportRepository,
            self.get_repository("archive_export"),
        )
        archive_tmlo_repo = cast(
            repositories.ArchiveExportTmloRepository,
            self.get_repository("archive_export_tmlo"),
        )
        custom_fields_repo = cast(
            CustomFieldsRepository,
            self.get_repository("custom_fields"),
        )
        case_relation_repo = cast(
            repositories.CaseRelationRepository,
            self.get_repository("case_relation"),
        )
        file_repo = cast(
            DocumentFileRepository,
            self.get_repository("document_file"),
        )
        export_archive_config = archive_repo.get_export_archive_config()
        case = archive_repo.get_case(
            case_uuid=case_uuid,
            custom_fields_repo=custom_fields_repo,
            user_info=self.qry.user_info,
        )
        tmlo_case = None

        if case and (
            case.archive_settings.archive_type == "tmlo"
            or export_archive_config["export_type"] == "sidecar"
        ):
            tmlo_case = case
        else:
            raise ValueError(
                "Archive export is not generated as there are no TMLO cases"
            )

        tmlo_archive = archive_tmlo_repo.build_tmlo_archive_structure(
            export_config=export_archive_config,
            export_file_uuid=export_file_uuid,
            case=tmlo_case,
            template_service=ArchiveTemplateService(),
            user_info=self.qry.user_info,
            case_relation_repo=case_relation_repo,
            file_repo=file_repo,
        )
        return {
            "file_storage_location": tmlo_archive["upload_result"][
                "storage_location"
            ],
            "file_uuid": str(export_file_uuid),
        }
