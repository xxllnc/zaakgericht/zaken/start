# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
import minty.cqrs
from ...shared.case_filters import CaseFilter
from .._shared import (
    get_case_locations,
    get_case_role_department,
    get_case_type_result,
    get_case_types,
    get_subjects,
)
from ..entities.case_search_result import CaseSearchOrder, CaseSearchResult
from ..repositories import CaseSearchResultRepository
from minty.entity import EntityCollection
from pydantic.v1 import validate_arguments
from typing import cast


class CaseSearch(minty.cqrs.SplitQueryBase):
    name = "search_case"

    @validate_arguments
    def __call__(
        self,
        page: int = 1,
        page_size: int = 25,
        filters: CaseFilter | None = None,
        sort: CaseSearchOrder = CaseSearchOrder.number_desc,
        includes: list[str] | None = None,
        full_content: bool | None = False,
        cursor: str | None = None,
    ) -> EntityCollection[CaseSearchResult]:
        """
        Search for Cases based on the filters
        """
        if not filters:
            filters = CaseFilter()

        repo = cast(
            CaseSearchResultRepository,
            self.get_repository("case_search_result"),
        )
        # decode cursor
        decoded_cursor = None
        if cursor:
            decoded_bytes = base64.b64decode(cursor)
            decoded_json = decoded_bytes.decode("utf-8")
            decoded_cursor = json.loads(decoded_json)
            page_size = decoded_cursor.get("page_size")

        cases = repo.search(
            user_info=self.qry.user_info,
            permission="search",
            page=page,
            page_size=page_size,
            filters=filters.dict(),
            sort=sort,
            included=includes,
            full_content=full_content or False,
            cursor=decoded_cursor,
        )

        included_entities = []
        if includes:
            if "case_type" in includes:
                included_entities += self._get_case_types(cases)
            if "case_type_result" in includes:
                included_entities += self._get_case_type_results(cases)
            if "case_location" in includes:
                included_entities += self._get_case_location(cases)
            if "case_role_department" in includes:
                included_entities += self._get_case_role_department(cases)
            if any(
                item in includes
                for item in ["requestor", "assignee", "coordinator"]
            ):
                included_entities += self._get_contacts(includes, cases)

        cursor = self._calculate_cursor(cursor, cases, sort, page_size)
        cases.included_entities = included_entities
        cases.cursor = cursor

        return cases

    def _get_contacts(self, includes_contact_types, cases):
        contact_list = []
        if "requestor" in includes_contact_types:
            contact_list = contact_list + [
                {
                    "uuid": entity.requestor.entity_id,
                    "type": entity.requestor.entity_type,
                }
                for entity in cases.entities
            ]
        if "assignee" in includes_contact_types:
            contact_list = contact_list + [
                {"uuid": entity.assignee.entity_id, "type": "employee"}
                for entity in cases.entities
                if entity.assignee
            ]
        if "coordinator" in includes_contact_types:
            contact_list = contact_list + [
                {"uuid": entity.coordinator.entity_id, "type": "employee"}
                for entity in cases.entities
                if entity.coordinator
            ]
        contact_list = {v["uuid"]: v for v in contact_list}.values()
        person_list = [
            v["uuid"] for v in contact_list if v["type"] == "person"
        ]

        employee_list = [
            v["uuid"] for v in contact_list if v["type"] == "employee"
        ]
        organization_list = [
            v["uuid"] for v in contact_list if v["type"] == "organization"
        ]

        related_contacts = []
        if len(person_list) >= 1:
            try:
                subjects = get_subjects(
                    get_repository=self.get_repository,
                    subject_type="person",
                    subject_uuids=person_list,
                )
                related_contacts.extend(subjects)
            except Exception:
                self.logger.info(
                    "Error during get_subjects person in case/search"
                )
        if len(employee_list) >= 1:
            try:
                subjects = get_subjects(
                    get_repository=self.get_repository,
                    subject_type="employee",
                    subject_uuids=employee_list,
                )
                related_contacts.extend(subjects)
            except Exception:
                self.logger.info(
                    "Error during get_subjects employee in case/search"
                )
        if len(organization_list) >= 1:
            try:
                subjects = get_subjects(
                    get_repository=self.get_repository,
                    subject_type="organization",
                    subject_uuids=organization_list,
                )
                related_contacts.extend(subjects)
            except Exception:
                self.logger.info(
                    "Error during get_subjects organization in case/search"
                )
        return related_contacts

    def _get_case_types(self, cases):
        case_type_version_list = [
            entity.case_type.entity_id
            for entity in cases.entities
            if entity.case_type
        ]
        case_types = get_case_types(
            get_repository=self.get_repository,
            case_type_version_uuids=case_type_version_list,
        )

        return case_types

    def _get_case_type_results(self, cases):
        case_type_result_list = [
            entity.case_type_result.entity_id
            for entity in cases.entities
            if entity.case_type_result
        ]
        case_type_results = get_case_type_result(
            get_repository=self.get_repository,
            case_type_result_uuids=case_type_result_list,
        )

        return case_type_results

    def _get_case_location(self, cases):
        case_location_list = [
            entity.case_location.entity_id
            for entity in cases.entities
            if entity.case_location
        ]
        case_locations = get_case_locations(
            get_repository=self.get_repository,
            case_location_uuids=case_location_list,
        )

        return case_locations

    def _get_case_role_department(self, cases):
        role_list = [
            entity.role.entity_id for entity in cases.entities if entity.role
        ]

        case_role_department = get_case_role_department(
            get_repository=self.get_repository,
            role_uuid=role_list,
        )
        return case_role_department

    def _calculate_cursor(self, cursor, cases, sort, page_size):
        if not cases.entities:
            return None
        last_item_value = self._get_last_item_value(sort, cases)
        cursor_next = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": cases.entities[-1].number,
            "last_item": last_item_value.get("next"),
        }
        cursor_previous = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": cases.entities[0].number,
            "last_item": last_item_value.get("previous"),
        }

        # Encode to Base64

        json_string_next = json.dumps(cursor_next)
        json_string_previous = json.dumps(cursor_previous)

        base64_bytes_next = base64.b64encode(json_string_next.encode("utf-8"))
        base64_string_next = base64_bytes_next.decode("utf-8")
        base64_bytes_previous = base64.b64encode(
            json_string_previous.encode("utf-8")
        )
        base64_string_previous = base64_bytes_previous.decode("utf-8")
        cursor = {
            "next": base64_string_next,
            "previous": base64_string_previous,
            "current": cursor,
        }

        return cursor

    def _get_last_item_value(self, sort, cases):
        sort = sort.replace("-", "")
        last_item_value_based_on_sort = {
            CaseSearchOrder.number_asc: {
                "next": cases.entities[-1].number,
                "previous": cases.entities[0].number,
            },
            CaseSearchOrder.unread_message_count_asc: {
                "next": cases.entities[-1].unread_message_count,
                "previous": cases.entities[0].unread_message_count,
            },
            CaseSearchOrder.unaccepted_files_count_asc: {
                "next": cases.entities[-1].unaccepted_files_count,
                "previous": cases.entities[0].unaccepted_files_count,
            },
            CaseSearchOrder.unaccepted_attribute_update_count_asc: {
                "next": cases.entities[-1].unaccepted_attribute_update_count,
                "previous": cases.entities[
                    0
                ].unaccepted_attribute_update_count,
            },
            CaseSearchOrder.registration_date_asc: {
                "next": cases.entities[-1].registration_date,
                "previous": cases.entities[0].registration_date,
            },
            CaseSearchOrder.completion_date_asc: {
                "next": cases.entities[-1].completion_date,
                "previous": cases.entities[0].completion_date,
            },
        }
        return last_item_value_based_on_sort[sort]


class CaseSearchTotalResults(minty.cqrs.SplitQueryBase):
    name = "search_case_total_results"

    @validate_arguments
    def __call__(self, filters: CaseFilter | None = None):
        if not filters:
            filters = CaseFilter()
        repo = cast(
            CaseSearchResultRepository,
            self.get_repository("case_search_result"),
        )
        total_result = repo.search_total_results(
            user_info=self.qry.user_info,
            permission="search",
            filters=filters.dict(),
        )

        return total_result
