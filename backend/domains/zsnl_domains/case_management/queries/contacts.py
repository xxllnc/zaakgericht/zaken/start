# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .. import entities
from .._shared import get_subject, get_subject_limited
from ..repositories import (
    EmployeeRepository,
    EmployeeSettingsRepository,
    OrganizationRepository,
    PersonRepository,
    PersonSensitiveDataRepository,
)
from collections.abc import Iterable
from minty.exceptions import Forbidden
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID


class GetContact(minty.cqrs.SplitQueryBase):
    name = "get_contact"

    def __call__(self, type: str, uuid: UUID):
        """Get a contact, given its type and UUID"""

        repository = self.get_repository

        contact = get_subject(
            get_repository=repository,
            subject_type=type,
            subject_uuid=uuid,
        )

        # The send_query_event is in all repositories so picking one for all
        # get contact calls works
        repo = cast(
            OrganizationRepository, self.get_repository("organization")
        )

        event = minty.cqrs.events.Event.create_basic(
            domain="zsnl_domains_case_management",
            context=self.context,
            entity_type=contact.__class__.__name__,
            entity_id=uuid,
            event_name="ContactViewed",
            entity_data={"name": contact.name},
            user_info=self.qry.user_info,
        )
        repo.send_query_event(event)

        return contact


class GetOrganization(minty.cqrs.SplitQueryBase):
    name = "get_organization_by_uuid"

    def __call__(self, organization_uuid: str):
        """
        Get Organization entity as python dictionary by given `organization_uuid`.

        :param organization_uuid: identifier of organization
        :return: Organization entity
        """

        repo = cast(
            OrganizationRepository, self.get_repository("organization")
        )

        organization = repo.find_organization_by_uuid(uuid=organization_uuid)
        return {"result": organization}


class GetOrganizations(minty.cqrs.SplitQueryBase):
    name = "get_organizations_by_uuid"

    def __call__(
        self, organization_uuids: Iterable[str]
    ) -> list[entities.Organization]:
        """
        Get a list of organization entities by their UUIDs

        :param organization_uuid: Iterable of UUIDs of organizations
        :return: List of Organization entities
        """

        repo = cast(
            OrganizationRepository, self.get_repository("organization")
        )

        organizations = repo.get_organizations_by_uuid(
            uuids=map(lambda u: UUID(u), organization_uuids)
        )
        return organizations


class GetEmployee(minty.cqrs.SplitQueryBase):
    name = "get_employee_by_uuid"

    def __call__(self, employee_uuid: str):
        """
        Get Employee entity as python dictionary by given `employee_uuid`.

        :param organization_uuid: identifier of employee
        :return: Employee entity
        """

        repo = cast(EmployeeRepository, self.get_repository("employee"))
        employee = repo.find_employee_by_uuid(uuid=UUID(employee_uuid))
        return {"result": employee}


class GetEmployees(minty.cqrs.SplitQueryBase):
    name = "get_employees_by_uuid"

    def __call__(
        self, employee_uuids: Iterable[str]
    ) -> list[entities.Employee]:
        """
        Get Employee entity as python dictionary by given `employee_uuid`.

        :param employee_uuids: Iterable containing employee UUIDs
        :return: List of employee entities
        """

        repo = cast(EmployeeRepository, self.get_repository("employee"))
        employees = repo.get_employees_by_uuid(
            uuids=map(lambda u: UUID(u), employee_uuids)
        )
        return employees


class GetPerson(minty.cqrs.SplitQueryBase):
    name = "get_person_by_uuid"

    def __call__(self, person_uuid: str):
        """
        Get Person entity as python dictionary by given `person_uuid`.

        :param person_uuid: identifier of person
        :return: Person entity
        """

        repo = cast(PersonRepository, self.get_repository("person"))
        person = repo.find_person_by_uuid(uuid=UUID(person_uuid))
        return {"result": person}


class GetPersons(minty.cqrs.SplitQueryBase):
    name = "get_persons_by_uuid"

    def __call__(self, person_uuids: Iterable[str]) -> list[entities.Person]:
        """
        Get a list of Person entities, with the specified `person_uuids`.

        :param person_uuid: identifier of person
        :return: List of Person entities
        """

        repo = cast(PersonRepository, self.get_repository("person"))
        persons = repo.get_persons_by_uuid(
            uuids=map(lambda u: UUID(str(u)), person_uuids)
        )
        return persons


class GetContactSettings(minty.cqrs.SplitQueryBase):
    name = "get_contact_settings"

    def __call__(self, uuid: str):
        """Get notification settings for an employee"""

        repo = cast(
            EmployeeSettingsRepository,
            self.get_repository("employee_settings"),
        )
        contact_settings = repo.find_by_uuid(
            uuid=UUID(uuid), user_info=self.qry.user_info
        )

        return contact_settings


class GetPersonSensitiveData(minty.cqrs.SplitQueryBase):
    name = "get_person_sensitive_data"

    @validate_with(
        get_data(__name__, "validation/get_person_sensitive_data.json")
    )
    def __call__(self, uuid: str, sensitive_data: str):
        """Get person sensitive data"""

        repo = cast(
            PersonSensitiveDataRepository,
            self.get_repository("person_sensitive_data"),
        )

        if (
            self.qry.user_info.permissions.get(
                "view_sensitive_contact_data", False
            )
            is True
        ) or (self.qry.user_info.permissions.get("admin", False) is True):
            person_data = repo.find_person_sensitive_data_by_uuid(
                uuid=UUID(uuid), sensitive_data=sensitive_data
            )

            return person_data
        else:
            raise Forbidden(
                "You don't have enough rights to get sensitive information for the given person",
                "contact/get_person_sensitive_data/premission_denied",
            )


class GetContactLimited(minty.cqrs.SplitQueryBase):
    name = "get_contact_limited"

    def __call__(self, type: str, uuid: UUID):
        """Get a limited info of contact, given its type and UUID"""

        contact = get_subject_limited(
            get_repository=self.get_repository,
            subject_type=type,
            subject_uuid=uuid,
        )
        return contact
