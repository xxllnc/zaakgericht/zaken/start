# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .. import entities
from ..repositories import CountryRepository
from typing import cast


class GetCountriesList(minty.cqrs.SplitQueryBase):
    name = "get_countries_list"

    def __call__(self, active: bool | None = None) -> list[entities.Country]:
        repo = cast(CountryRepository, self.get_repository("country"))

        active = False if active == "false" else True
        countries_list = repo.get_countries_list(active=active)
        return countries_list
