# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import AuthorizationLevel
from ..repositories import CustomObjectTypeRepository
from minty.exceptions import Conflict, NotFound
from typing import cast
from uuid import UUID


class GetCustomObjectType(minty.cqrs.SplitQueryBase):
    name = "get_custom_object_type_by_uuid"

    def __call__(self, uuid: str):
        """
        Get object definition by uuid.

        :param uuid: identifier of the custom_object_type
        :return: Custom object type entity
        """

        repo = cast(
            CustomObjectTypeRepository,
            self.get_repository("custom_object_type"),
        )
        objecttype = repo.find_by_uuid(
            uuid=UUID(uuid), user_info=self.qry.user_info
        )

        if not objecttype:
            raise NotFound(
                "Could not find 'custom_object_type' with uuid {}".format(
                    uuid
                ),
                "case_management/custom_object/object_type/not_found",
            )

        return objecttype


class GetCustomObjectTypeVersionIndependent(minty.cqrs.SplitQueryBase):
    name = "get_custom_object_type_by_version_independent_uuid"

    def __call__(
        self,
        uuid: str,
        use: str | None = None,
    ):
        """
        Get object definition by uuid.

        :param uuid: identifier of the custom_object_type
        :type uuid: UUID
        :return: custom_object_type
        :rtype: dict
        """
        # This call is also used in catalog for editing an object_type.
        # No ACL check is required in catalog, so the default value of
        # authorization is None
        authorization = None

        if use:
            try:
                authorization_mapping = {
                    "read": AuthorizationLevel.read,
                    "write": AuthorizationLevel.readwrite,
                    "manage": AuthorizationLevel.admin,
                }
                authorization = authorization_mapping[use]
            except KeyError as e:
                raise Conflict(
                    f"Invalid parameter 'use={use}' to retrieve custom_object_type",
                    "case_management/custom_object/object_type/invalid_use",
                ) from e

        repo = cast(
            CustomObjectTypeRepository,
            self.get_repository("custom_object_type"),
        )

        objecttype = repo.find_by_version_independent_uuid(
            uuid=UUID(uuid),
            user_info=self.qry.user_info,
            authorization=authorization,
        )

        if not objecttype:
            raise NotFound(
                "Could not find or rights are insufficient for 'custom_object_type' with uuid {}".format(
                    uuid
                ),
                "case_management/custom_object/object_type/not_found",
            )

        return objecttype
