# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import dateutil.parser
import minty.cqrs
from ...shared.validators import split_comma_separated_field
from .. import entities
from ..entities.case import ValidEventCategory
from ..repositories import TimelineEntryRepository
from datetime import timezone
from minty.validation import validate_with
from pkgutil import get_data
from pydantic.v1 import BaseModel, Field, validate_arguments, validator
from typing import cast
from uuid import UUID


class GetCustomObjectEventLogs(minty.cqrs.SplitQueryBase):
    name = "get_custom_object_event_logs"

    @validate_with(
        get_data(__name__, "validation/get_custom_object_event_logs.json")
    )
    def __call__(
        self,
        page: str,
        page_size: str,
        custom_object_uuid: str,
        period_start: str = None,
        period_end: str = None,
    ) -> list[entities.TimelineEntry]:
        """Get a list of custom object log event entries"""

        repo = cast(
            TimelineEntryRepository, self.get_repository("timeline_entry")
        )

        if period_start:
            start = dateutil.parser.isoparse(period_start)
        else:
            start = None

        if period_end:
            end = dateutil.parser.isoparse(period_end)
        else:
            end = None

        result = repo.get_custom_object_event_logs(
            page=int(page),
            page_size=int(page_size),
            period_start=start,
            period_end=end,
            custom_object_uuid=UUID(custom_object_uuid),
        )
        return result


class GetContactEventLogs(minty.cqrs.SplitQueryBase):
    name = "get_contact_event_logs"

    @validate_with(
        get_data(__name__, "validation/get_contact_event_logs.json")
    )
    def __call__(
        self,
        page: str,
        page_size: str,
        contact_type: str,
        contact_uuid: str,
        period_start: str = None,
        period_end: str = None,
    ) -> list[entities.TimelineEntry]:
        """Get a list of contact log event entries"""

        repo = cast(
            TimelineEntryRepository, self.get_repository("timeline_entry")
        )

        if period_start:
            start = dateutil.parser.isoparse(period_start)
            start = start.replace(tzinfo=timezone.utc)
        else:
            start = None

        if period_end:
            end = dateutil.parser.isoparse(period_end)
            end = end.replace(tzinfo=timezone.utc)
        else:
            end = None

        result = repo.get_contact_event_logs(
            page=int(page),
            page_size=int(page_size),
            contact_type=contact_type,
            contact_uuid=contact_uuid,
            period_start=start,
            period_end=end,
            user_info=self.qry.user_info,
        )
        return result


class CaseEventLogsFilter(BaseModel):
    filter_attributes_category: set[ValidEventCategory] | None = Field(
        None, alias="attributes.category"
    )

    class Config:
        validate_all = True

    _split_comma_separated_fields = validator(
        "filter_attributes_category",
        allow_reuse=True,
        pre=True,
    )(split_comma_separated_field)


class GetCaseEventLogs(minty.cqrs.SplitQueryBase):
    name = "get_case_event_logs"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        case_uuid: UUID,
        period_start: datetime.datetime | None = None,
        period_end: datetime.datetime | None = None,
        filters: CaseEventLogsFilter | None = None,
    ) -> list[entities.TimelineEntry]:
        """Get a list of case log event entries"""
        if filters is None:
            filters = CaseEventLogsFilter.parse_obj({})

        repo = cast(
            TimelineEntryRepository, self.get_repository("timeline_entry")
        )

        result = repo.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=case_uuid,
            period_start=period_start,
            period_end=period_end,
            user_info=self.qry.user_info,
            **filters.dict(),
        )
        return result
