# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.cqrs.events
from ... import ZaaksysteemRepositoryBase
from .. import entities
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)


class ArchiveExportRepository(ZaaksysteemRepositoryBase):
    _for_entity = "ArchiveExport"
    _events_to_calls = {"ArchiveExportCreated": "_create_export"}

    def get_case_query(self, case_uuid: UUID, user_info: minty.cqrs.UserInfo):
        return (
            sql.select(
                schema.CaseV2.id,
                schema.CaseV2.uuid,
                schema.CaseV2.registratiedatum.label("registration_date"),
                schema.CaseV2.type_of_archiving,
                schema.CaseV2.custom_fields,
                schema.CaseV2.file_custom_fields,
                schema.CaseV2.confidentiality,
                schema.CaseV2.result,
                schema.CaseV2.result_uuid,
                schema.CaseV2.result_description,
                schema.CaseV2.result_explanation,
                schema.CaseV2.archival_state,
                schema.CaseV2.active_selection_list,
                schema.CaseV2.onderwerp.label("summary"),
                schema.CaseV2.onderwerp_extern.label("public_summary"),
                schema.CaseV2.case_type_version,
                schema.CaseV2.vernietigingsdatum.label("destruction_date"),
                schema.CaseV2.assignee_obj,
                sql.literal(
                    True
                    if (user_info and user_info.permissions.get("admin"))
                    else False
                ).label("user_is_admin"),
                sql.func.array(
                    sql.select(schema.CaseAcl.permission)
                    .select_from(schema.CaseAcl)
                    .where(schema.CaseAcl.case_id == schema.CaseV2.id)
                    .where(schema.CaseAcl.subject_uuid == user_info.user_uuid)
                    .group_by(schema.CaseAcl.permission)
                    .scalar_subquery()
                ).label("authorizations"),
                sql.func.json_build_object(
                    "archive_type",
                    schema.CasetypeArchiveSettings.archive_type,
                    "id",
                    schema.CasetypeArchiveSettings.id,
                    "uuid",
                    schema.CasetypeArchiveSettings.uuid,
                    "casetype_node_id",
                    schema.CasetypeArchiveSettings.casetype_node_id,
                    "name",
                    schema.CasetypeArchiveSettings.name,
                    "classification_code",
                    schema.CasetypeArchiveSettings.classification_code,
                    "classification_description",
                    schema.CasetypeArchiveSettings.classification_description,
                    "classification_source_tmlo",
                    schema.CasetypeArchiveSettings.classification_source_tmlo,
                    "classification_date",
                    schema.CasetypeArchiveSettings.classification_date,
                    "description_tmlo",
                    schema.CasetypeArchiveSettings.description_tmlo,
                    "location",
                    schema.CasetypeArchiveSettings.location,
                    "coverage_in_time_tmlo",
                    schema.CasetypeArchiveSettings.coverage_in_time_tmlo,
                    "coverage_in_geo_tmlo",
                    schema.CasetypeArchiveSettings.coverage_in_geo_tmlo,
                    "language_tmlo",
                    schema.CasetypeArchiveSettings.language_tmlo,
                    "user_rights",
                    schema.CasetypeArchiveSettings.user_rights,
                    "user_rights_description",
                    schema.CasetypeArchiveSettings.user_rights_description,
                    "user_rights_date_period",
                    schema.CasetypeArchiveSettings.user_rights_date_period,
                    "confidentiality",
                    schema.CasetypeArchiveSettings.confidentiality,
                    "confidentiality_description",
                    schema.CasetypeArchiveSettings.confidentiality_description,
                    "confidentiality_date_period",
                    schema.CasetypeArchiveSettings.confidentiality_date_period,
                    "form_genre",
                    schema.CasetypeArchiveSettings.form_genre,
                    "form_publication",
                    schema.CasetypeArchiveSettings.form_publication,
                    "structure",
                    schema.CasetypeArchiveSettings.structure,
                    "generic_metadata_tmlo",
                    schema.CasetypeArchiveSettings.generic_metadata_tmlo,
                ).label("archive_settings"),
                schema.ZaaktypeNode.code.label("case_type_identification"),
                sql.func.array(
                    sql.select(
                        sql.func.json_build_object(
                            "id",
                            schema.File.id,
                            "uuid",
                            schema.File.uuid,
                            "extension",
                            schema.File.extension,
                            "filename",
                            schema.File.name.label("filename"),
                            "mimetype",
                            schema.Filestore.mimetype,
                            "size",
                            schema.Filestore.size,
                            "store_uuid",
                            schema.Filestore.uuid.label("store_uuid"),
                            "storage_location",
                            schema.Filestore.storage_location,
                            "md5",
                            schema.Filestore.md5,
                            "date_created",
                            schema.File.date_created,
                            "confidentiality",
                            schema.FileMetaData.trust_level.label(
                                "confidentiality"
                            ),
                            "case_id",
                            schema.File.case_id,
                        )
                    )
                    .select_from(
                        sql.join(
                            schema.File,
                            schema.Filestore,
                            schema.File.filestore_id == schema.Filestore.id,
                        )
                    )
                    .join(
                        schema.FileMetaData,
                        schema.FileMetaData.id == schema.File.metadata_id,
                        isouter=True,
                    )
                    .where(
                        sql.and_(
                            schema.File.case_id == schema.CaseV2.id,
                            schema.File.date_deleted.is_(None),
                            schema.File.destroyed.is_(False),
                        )
                    )
                    .scalar_subquery()
                ).label("documents"),
            )
            .select_from(
                sql.join(
                    schema.CaseV2,
                    schema.CasetypeArchiveSettings,
                    schema.CaseV2.zaaktype_node_id
                    == schema.CasetypeArchiveSettings.casetype_node_id,
                    isouter=True,
                ).join(
                    schema.ZaaktypeNode,
                    schema.CaseV2.zaaktype_node_id == schema.ZaaktypeNode.id,
                    isouter=True,
                )
            )
            .where(
                sql.and_(
                    user_allowed_cases_subquery(
                        user_info=user_info,
                        permission="read",
                        case_alias=schema.CaseV2,
                    ),
                    schema.CaseV2.uuid == case_uuid,
                )
            )
        )

    def get_case(self, case_uuid: UUID, custom_fields_repo, user_info):
        query = self.get_case_query(case_uuid, user_info)

        query_result = self.session.execute(query).fetchall()

        if not query_result:
            return

        return self._transform_to_entity(
            query_result=query_result[0], custom_fields_repo=custom_fields_repo
        )

    def get_export_archive_config(self):
        query = sql.select(schema.Interface.interface_config).where(
            sql.and_(
                schema.Interface.module == "export_mapping_topx",
                schema.Interface.date_deleted.is_(None),
                schema.Interface.active,
            )
        )

        config = self.session.execute(query).fetchone()

        if not config:
            raise NotFound("No configuration found for archive export")

        return config[0]

    def _transform_to_entity(self, query_result, custom_fields_repo):
        archive_settings = query_result.archive_settings
        return entities.ArchiveExportCase.parse_obj(
            {
                "entity_id": query_result.uuid,
                "uuid": query_result.uuid,
                "id": query_result.id,
                "registration_date": query_result.registration_date,
                "confidentiality": query_result.confidentiality,
                "summary": str(query_result.summary or ""),
                "public_summary": str(query_result.public_summary or ""),
                "destruction_date": query_result.destruction_date,
                "custom_fields": custom_fields_repo.format_custom_fields_for_case_basic(
                    query_result.custom_fields, query_result.file_custom_fields
                ).custom_fields,
                "assignee": {
                    "entity_type": "employee",
                    "entity_id": query_result.assignee_obj["uuid"],
                    "uuid": query_result.assignee_obj["uuid"],
                    "entity_meta_summary": query_result.assignee_obj[
                        "display_name"
                    ],
                }
                if query_result.assignee_obj
                else None,
                "result": {
                    "result": query_result.result,
                    "result_name": query_result.result_description,
                    "result_uuid": query_result.result_uuid,
                    "archival_attributes": {
                        "state": query_result.archival_state,
                        "selection_list": query_result.active_selection_list
                        or "",
                    },
                }
                if query_result.result_uuid
                else None,
                "case_type_version": {
                    "name": query_result.case_type_version["name"],
                    "uuid": query_result.case_type_version["uuid"],
                    "id": query_result.case_type_identification,
                    "entity_id": query_result.case_type_version["uuid"],
                    "entity_meta_summary": query_result.case_type_version[
                        "name"
                    ],
                },
                "type_of_archiving": query_result.type_of_archiving,
                "result_explanation": query_result.result_explanation,
                "authorizations": query_result.authorizations,
                "documents": self._transform_documents_to_entity(
                    query_result.documents
                ),
                "archive_settings": {
                    "archive_type": archive_settings["archive_type"],
                    "name": archive_settings["name"] or [],
                    "classification_code": archive_settings[
                        "classification_code"
                    ]
                    or [],
                    "classification_description": archive_settings[
                        "classification_description"
                    ]
                    or [],
                    "classification_source_tmlo": archive_settings[
                        "classification_source_tmlo"
                    ]
                    or [],
                    "classification_date": archive_settings[
                        "classification_date"
                    ]
                    or [],
                    "description_tmlo": archive_settings["description_tmlo"]
                    or [],
                    "location": archive_settings["location"] or [],
                    "coverage_in_time_tmlo": archive_settings[
                        "coverage_in_time_tmlo"
                    ]
                    or [],
                    "coverage_in_geo_tmlo": archive_settings[
                        "coverage_in_geo_tmlo"
                    ]
                    or [],
                    "language_tmlo": str(
                        archive_settings["language_tmlo"] or ""
                    ),
                    "user_rights": archive_settings["user_rights"] or [],
                    "user_rights_description": archive_settings[
                        "user_rights_description"
                    ]
                    or [],
                    "user_rights_date_period": archive_settings[
                        "user_rights_date_period"
                    ]
                    or [],
                    "confidentiality": archive_settings["confidentiality"]
                    or [],
                    "confidentiality_description": archive_settings[
                        "confidentiality_description"
                    ]
                    or [],
                    "confidentiality_date_period": archive_settings[
                        "confidentiality_date_period"
                    ]
                    or [],
                    "form_genre": archive_settings["form_genre"] or [],
                    "form_publication": archive_settings["form_publication"]
                    or [],
                    "structure": archive_settings["structure"] or [],
                    "generic_metadata_tmlo": archive_settings[
                        "generic_metadata_tmlo"
                    ]
                    or [],
                },
            }
        )

    def _transform_documents_to_entity(self, documents):
        documents_list = []
        for row in documents:
            document = entities.CaseDocument.parse_obj(
                {
                    "id": row["id"],
                    "uuid": row["uuid"],
                    "extension": row["extension"],
                    "filename": row["filename"],
                    "mimetype": row["mimetype"],
                    "size": row["size"],
                    "store_uuid": row["store_uuid"],
                    "md5": row["md5"],
                    "date_created": row["date_created"],
                    "confidentiality": row["confidentiality"],
                    "storage_location": row["storage_location"][0],
                    "case_id": row["case_id"],
                }
            )
            documents_list.append(document)
        return documents_list
