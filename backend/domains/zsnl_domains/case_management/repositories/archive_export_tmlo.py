# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.cqrs
import minty.cqrs.events
import os
import re
import tempfile
from ... import ZaaksysteemRepositoryBase
from .. import entities
from ..services import ArchiveTemplateService
from .case_relation import CaseRelationRepository
from sqlalchemy import sql
from typing import Any
from uuid import UUID
from xml.etree.ElementTree import Element, indent, tostring
from zipfile import ZIP_DEFLATED, ZipFile
from zsnl_domains.database import schema

archive_mapping = {
    "Bewaren (B)": "Overdracht",
    "Vernietigen (V)": "Vernietigen",
    "Overbrengen (O)": "Migratie",
}

file_confidentiality_mapping = {
    "Confidentieel": "Vertrouwelijk",
    "Openbaar": "Niet vertrouwelijk",
    "Beperkt openbaar": "Niet vertrouwelijk",
    "Intern": "Organisatievertrouwelijk",
    "Zaakvertrouwelijk": "Niet vertrouwelijk",
}


class ArchiveExportTmloRepository(ZaaksysteemRepositoryBase):
    def _create_archive_metadata(
        self, export_id: str, export_path: str
    ) -> tuple[str, str]:
        metadata = {
            "identificatiekenmerk": export_id,
            "aggregatieniveau": "Archief",
            "naam": "TopX export",
        }

        xml_string = self._generate_xml(metadata, sub_element="aggregatie")
        file_path = os.path.join(export_path, f"{export_id}.metadata")
        return file_path, xml_string

    def _create_case_type_metadata(
        self, export_path: str, case_type: dict
    ) -> tuple[str, str, str]:
        metadata = {
            "identificatiekenmerk": [case_type["uuid"], case_type["id"]],
            "aggregatieniveau": "Serie",
            "naam": case_type["name"],
        }

        xml_string = self._generate_xml(metadata, sub_element="aggregatie")
        folder_path = os.path.join(export_path, f"{case_type['id']}")
        file_path = os.path.join(folder_path, f"{case_type['id']}.metadata")

        return folder_path, file_path, xml_string

    def _create_document_metadata(self, export_path, document):
        metadata = {
            "identificatiekenmerk": [document.uuid],
            "aggregatieniveau": "Archiefstuk",
            "naam": document.filename + "." + document.extension,
            "relatie": self._get_relation_for_document(document),
            "formaat": self._get_file_format(document),
        }

        xml_string = self._generate_xml(metadata, sub_element="bestand")
        folder_path = os.path.join(export_path, f"{document.id}")
        file_path = os.path.join(folder_path, f"{document.id}.metadata")

        return folder_path, file_path, xml_string

    def _get_relation_for_document(self, document):
        return {
            "relatieID": document.case_id,
            "typeRelatie": "record",
        }

    def _get_file_format(self, document):
        return {
            "identificatiekenmerk": document.store_uuid,
            "bestandsnaam": {
                "naam": document.filename,
                "extensie": document.extension,
            },
            "type": "document",
            "omvang": document.size,
            "bestandsformaat": document.mimetype,
            "fysiekeIntegriteit": {
                "algoritme": "md5",
                "waarde": document.md5,
                "datumEnTijd": self.format_date(document.date_created),
            },
            "datumAanmaak": self.format_date(document.date_created),
        }

    def format_date(self, date_string):
        dt = datetime.datetime.fromisoformat(str(date_string))
        dt = dt.astimezone(datetime.timezone.utc)
        new_date_string = dt.isoformat(timespec="seconds").replace(
            "+00:00", "Z"
        )
        return new_date_string

    def _create_case_metadata(
        self,
        export_path: str,
        case: entities.ArchiveExportCase,
        template_service: ArchiveTemplateService,
        case_relation_repo: CaseRelationRepository,
        export_id: str,
        user_info: minty.cqrs.UserInfo | None = None,
    ) -> tuple[str, str, str]:
        tmlo = self._get_tmlo_fields_from_case(case, template_service)
        metadata = {
            "entiteittype": "1-entiteiten model",
            "identificatiekenmerk": [case.uuid],
            "aggregatieniveau": "Dossier",
            "naam": self._get_name_for_case(case, tmlo),
            "classificatie": self._get_classification(tmlo) if tmlo else "",
            "omschrijving": tmlo["description_tmlo"]
            if "description_tmlo" in tmlo
            else "",
            "plaats": tmlo["location"] if "location" in tmlo else "",
            "dekking": self._get_coverage(tmlo) if tmlo else "",
            "taal": tmlo["language_tmlo"]
            if "language_tmlo" in tmlo
            else "Dut",
            "gebruikersRechten": self._get_user_rights(tmlo) if tmlo else "",
            "eventGeschiedenis": self._get_event_log_for_case(case.id),
            "openbaarheid": self._get_publicity(tmlo) if tmlo else "",
            "vorm": self._get_form(tmlo) if tmlo else "",
            "actor": self._get_actor(case),
            "relations": self._get_relations_for_case(
                case=case,
                case_relation_repo=case_relation_repo,
                user_info=user_info,
                export_id=export_id,
            ),
            "vertrouwelijkheid": self._get_confidentiality(case),
            "activiteitWerkproce": self._get_activiteitWerkproc(case),
            "externIdentificatiekenmerk": self._get_external_identification(
                case
            ),
            "generiekeMetadata": tmlo["generic_metadata_tmlo"]
            if "generic_metadata_tmlo" in tmlo
            else [],
        }

        xml_string = self._generate_xml(metadata, sub_element="aggregatie")
        base_path = os.path.join(export_path, f"{case.case_type_version.id}")
        folder_path = os.path.join(base_path, f"{case.id}")
        file_path = os.path.join(folder_path, f"{case.id}.metadata")

        return folder_path, file_path, xml_string

    def _get_external_identification(
        self,
        case: entities.ArchiveExportCase,
    ) -> dict[str, Any] | None:
        if "ztc_extern_kenmerk" in case.custom_fields:
            value = case.custom_fields["ztc_extern_kenmerk"]["value"]
            return {"nummerBinnenSysteem": value}

    def _get_activiteitWerkproc(self, case) -> dict[str, Any]:
        case_type = case.case_type_version
        return {
            "identificatiekenmerk": [case_type.id],
            "aggregatieniveau": "Process",
            "naam": case_type.name,
        }

    def _get_confidentiality(
        self,
        case: entities.ArchiveExportCase,
    ) -> dict[str, Any]:
        return {
            "classificatieNiveau": case.confidentiality,
            "datumOfPeriode": case.registration_date,
        }

    def format_fields(self, my_dict) -> dict[Any, Any]:
        updated_dict = {}
        for key, value in my_dict.items():
            try:
                updated_dict[key] = eval(value)
            except (ValueError, SyntaxError, NameError):
                updated_dict[key] = value

        return updated_dict

    def _get_tmlo_fields_from_case(
        self, case: entities.ArchiveExportCase, template_service
    ) -> dict[Any, Any]:
        fields = {}
        # All tags will alwasys be present in xml.
        # if value is not available then it will be an empty tag.
        for item, value in case.archive_settings:
            if value == "":
                fields[item] = value
            else:
                fields[item] = template_service.render_value(
                    case=case, template_attribute=item
                )
        return self.format_fields(fields)

    def _get_actor(
        self,
        case: entities.ArchiveExportCase,
    ):
        summary = (
            case.assignee.entity_meta_summary
            if case.assignee is not None
            else ""
        )
        return {
            "actor": {
                "identificatiekenmerk": summary,
                "geautoriseerdeNaam": summary,
            }
        }

    def _get_relations_for_case(
        self,
        case: entities.ArchiveExportCase,
        case_relation_repo: CaseRelationRepository,
        user_info: minty.cqrs.UserInfo,
        export_id: str,
    ) -> list[Any]:
        relations = []
        relations.append(
            {
                "relatieID": export_id,
                "typeRelatie": "Archief",
            }
        )
        case_relations = case_relation_repo.get_case_relations(
            case_uuid=case.uuid,
            authorization=case.authorizations,
            user_info=user_info,
        )
        if len(case_relations) >= 1:
            for row in case_relations:
                relations.append(
                    {
                        "relatieID": row.object1_uuid
                        if row.object1_uuid != row.current_case_uuid
                        else row.object2_uuid,
                        "typeRelatie": "Dossier",
                    }
                )
        if len(case.documents) >= 1:
            for item in case.documents:
                relations.append(
                    {
                        "relatieID": str(item),
                        "typeRelatie": "Bestand",
                    }
                )
        return relations

    def _get_event_log_for_case(self, case_id: int) -> list[Any]:
        # These are required event types as per TMLO specification,
        # other event types should not be added in the list.

        event_types = [
            "case/document/publish",
            "case/document/replace",
            "case/document/copy_case_into",
            "case/update/completion_date",
            "case/update/result",
            "case/close",
        ]
        query = sql.select(
            schema.Logging.event_type,
            schema.Logging.created_by,
            schema.Logging.onderwerp,
        ).where(
            sql.and_(
                sql.or_(
                    schema.Logging.created_by.isnot(None),
                    schema.Logging.created_by_name_cache.isnot(None),
                ),
                schema.Logging.event_type.in_(event_types),
                schema.Logging.zaak_id == case_id,
            )
        )

        log_entries = self.session.execute(query).fetchall()
        log_records = []
        for log in log_entries:
            log_records.append(
                {
                    "type": log.event_type,
                    "beschrijving": re.sub(
                        re.compile("<[^>]*?>"), " ", log.onderwerp
                    ),
                    "verantwoordelijkeFunctionaris": log.created_by,
                }
            )

        return log_records

    def _get_name_for_case(
        self, case: entities.ArchiveExportCase, tmlo: dict
    ) -> list[Any]:
        names = []
        if "name" in tmlo and len(tmlo["name"]) >= 1:
            names.append(tmlo["name"])

        if case.summary != "":
            names.append(case.summary)
        if case.public_summary != "":
            names.append(case.public_summary)
        if case.case_type_version.entity_meta_summary != "":
            names.append(case.case_type_version.entity_meta_summary)
        return names

    def _get_publicity(self, tmlo: dict):
        return [
            {
                "omschrijvingBeperkingen": tmlo["confidentiality_description"],
                "datumOfPeriode": {
                    "datumEnTijd": tmlo["confidentiality_date_period"]
                },
            }
        ]

    def _get_coverage(self, tmlo: dict):
        return {
            "inTijd": tmlo["coverage_in_time_tmlo"],
            "geografischGebied": tmlo["coverage_in_geo_tmlo"],
        }

    def _get_classification(self, tmlo: dict):
        return {
            "code": tmlo["classification_code"],
            "omschrijving": tmlo["classification_description"],
            "bron": tmlo["classification_source_tmlo"],
            "datumOfPeriode": tmlo["classification_date"],
        }

    def _get_form(self, tmlo: dict):
        return {
            "redactieGenre": tmlo["form_genre"],
            "verschijningsvorm": tmlo["form_publication"],
            "structuur": tmlo["structure"],
        }

    def _get_user_rights(self, tmlo: dict):
        return {
            "Omschrijving voorwaarden": tmlo["user_rights_description"],
            "Datum/Periode": tmlo["user_rights_date_period"],
        }

    def _generate_xml(self, metadata, sub_element):
        url = "http://www.nationaalarchief.nl/ToPX/v2.3"

        root = Element("x0:ToPX", {"xmlns:x0": f"{url}"})
        aggregatie = Element(f"x0:{sub_element}")
        self.dict_to_xml(aggregatie, metadata)

        root.append(aggregatie)
        indent(root)
        return tostring(root, encoding="utf-8", xml_declaration=True)

    def dict_to_xml(self, root_element, data):
        for key, value in data.items():
            if isinstance(value, dict):
                element = Element(f"x0:{key}")
                self.dict_to_xml(element, value)
                root_element.append(element)
            elif isinstance(value, list):
                for item in value:
                    item_element = Element(f"x0:{key}")
                    if isinstance(item, dict):
                        self.dict_to_xml(item_element, item)
                    else:
                        if isinstance(item, (str, int, float, bool, UUID)):
                            item_element.text = str(item)
                    root_element.append(item_element)
            else:
                element = Element(f"x0:{key}")
                element.text = str(value)
                root_element.append(element)

        indent(root_element)
        return tostring(root_element, encoding="utf-8", xml_declaration=True)

    def build_tmlo_archive_structure(
        self,
        export_config: dict,
        export_file_uuid: UUID,
        case: entities.ArchiveExportCase,
        user_info: minty.cqrs.UserInfo,
        template_service: ArchiveTemplateService,
        case_relation_repo: CaseRelationRepository,
        file_repo,
    ) -> None:
        export_id = export_config["export_id"]
        export_path = os.path.join(export_id)

        archive_metadata_file_path, export_metadata = (
            self._create_archive_metadata(export_id, export_path)
        )
        upload_result = None

        with (
            tempfile.NamedTemporaryFile() as file_handle,
            ZipFile(
                file_handle, "w", ZIP_DEFLATED, compresslevel=9
            ) as new_archive,
        ):
            # Create Archive aggregation path and metadata
            new_archive.mkdir(str(export_id))
            new_archive.writestr(archive_metadata_file_path, export_metadata)

            # create case metadata
            if case:
                case_types = {}
                if case.case_type_version.uuid not in case_types:
                    case_types[case.case_type_version.uuid] = {
                        "name": case.case_type_version.name,
                        "id": case.case_type_version.id,
                    }

                (
                    case_folder_path,
                    case_metadata_file_path,
                    case_metadata,
                ) = self._create_case_metadata(
                    export_path=export_path,
                    case=case,
                    template_service=template_service,
                    case_relation_repo=case_relation_repo,
                    export_id=export_id,
                    user_info=user_info,
                )
                new_archive.mkdir(case_folder_path)
                new_archive.writestr(case_metadata_file_path, case_metadata)
                for document in case.documents:
                    (
                        document_folder_path,
                        document_metadata_file_path,
                        document_metadata,
                    ) = self._create_document_metadata(
                        export_path=case_folder_path, document=document
                    )
                    new_archive.mkdir(document_folder_path)
                    new_archive.writestr(
                        document_metadata_file_path, document_metadata
                    )
                    file_path = os.path.join(
                        document_folder_path,
                        f"{document.filename + document.extension}",
                    )

                    file_handle_temp = file_repo.get_tempfile(
                        document.store_uuid,
                        document.storage_location,
                    )

                    new_archive.writestr(file_path, file_handle_temp.read())
                for case_type_uuid in case_types:
                    case_type = case_types[case_type_uuid]
                    case_type["uuid"] = case_type_uuid
                    (
                        case_type_folder_path,
                        case_type_metadata_file_path,
                        case_type_metadata,
                    ) = self._create_case_type_metadata(
                        export_path=export_path,
                        case_type=case_type,
                    )
                    new_archive.mkdir(case_type_folder_path)
                    new_archive.writestr(
                        case_type_metadata_file_path, case_type_metadata
                    )
            new_archive.close()
            upload_result = self._get_infrastructure("s3").upload(
                file_handle=file_handle,
                uuid=export_file_uuid,
                tags={"type": "job_result"},
            )
            return {
                "upload_result": upload_result,
                "filestore_uuid": export_file_uuid,
                "export_file_uuid": export_file_uuid,
                "template_service": template_service,
            }
