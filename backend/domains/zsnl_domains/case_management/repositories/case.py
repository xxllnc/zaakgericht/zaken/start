# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import dateutil
import dateutil.parser
import json
import minty.cqrs
import os
import pytz
import sqlalchemy.exc
import sqlalchemy.types as sqltypes
import ssl
import urllib.request
from ... import ZaaksysteemRepositoryBase
from ...shared.util import get_country_name_from_landcode
from ..constants import (
    BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
    BETROKKENE_TYPE_NUMBER_FOR_PERSON,
)
from ..entities import (
    Case,
    CaseContact,
    CaseContactEmployee,
    CaseContactOrganization,
    CaseContactPerson,
    CaseTypeForCase,
    CaseTypeVersionEntity,
    _shared,
)
from . import case_acl
from .case_acl import (
    add_entity_from_natural_person,
    add_entity_from_organization,
    add_entity_from_subject,
)
from .database_queries import (
    case_action_data_from_sub_case_query,
    case_action_label_query,
    case_action_status_query,
)
from datetime import datetime, timezone
from minty.exceptions import Conflict, NotFound
from pydantic.v1 import validate_arguments
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from urllib.error import HTTPError
from uuid import UUID
from zsnl_domains import case_management
from zsnl_domains.case_management.entities.case import (
    CaseDepartment,
    CaseRole,
    DepartmentSummary,
)
from zsnl_domains.case_management.entities.check_result import CheckResult
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)
from zsnl_domains.shared.repositories.date_utilities import format_date
from zsnl_domains.shared.repositories.person import is_person_secret

API_HOSTNAME = os.environ.get("API_HOSTNAME", None)
CA_BUNDLE = os.environ.get("REQUESTS_CA_BUNDLE", None)


def authorizations_subquery(user_uuid: UUID | None):
    if user_uuid is None:
        return sql.literal(None).label("authorizations")

    return sql.func.array(
        sql.select(schema.CaseAcl.permission)
        .select_from(schema.CaseAcl)
        .where(schema.CaseAcl.case_id == schema.CaseV2.id)
        .where(schema.CaseAcl.subject_uuid == user_uuid)
        .group_by(schema.CaseAcl.permission)
        .scalar_subquery()
    ).label("authorizations")


def current_user_rights(user_uuid: UUID | None):
    """
    Returns 3 columns that indicate whether the current user (as specified by
    the `user_uuid` parameter) is the case requestor, assignee or coordinator.
    """
    if user_uuid is None:
        return [
            sql.literal(None).label("user_is_requestor"),
            sql.literal(None).label("user_is_assignee"),
            sql.literal(None).label("user_is_coordinator"),
        ]

    user_id_query = (
        sql.select(schema.Subject.id)
        .where(schema.Subject.uuid == user_uuid)
        .scalar_subquery()
    )

    return [
        sql.case(
            (
                sql.and_(
                    schema.CaseV2.aanvrager_type == "medewerker",
                    schema.CaseV2.aanvrager_gm_id == user_id_query,
                ),
                True,
            ),
            else_=False,
        ).label("user_is_requestor"),
        sql.case(
            (schema.CaseV2.behandelaar_gm_id == user_id_query, True),
            else_=False,
        ).label("user_is_assignee"),
        sql.case(
            (schema.CaseV2.coordinator_gm_id == user_id_query, True),
            else_=False,
        ).label("user_is_coordinator"),
    ]


def get_case_query(case_uuid: UUID, user_info: minty.cqrs.UserInfo | None):
    return sql.select(
        schema.CaseV2.id,
        schema.CaseV2.uuid,
        schema.CaseV2.onderwerp,
        schema.CaseV2.route_ou,
        schema.CaseV2.route_role,
        schema.CaseV2.vernietigingsdatum,
        schema.CaseV2.archival_state,
        schema.CaseV2.status,
        schema.CaseV2.contactkanaal,
        schema.CaseV2.created,
        schema.CaseV2.registratiedatum,
        schema.CaseV2.streefafhandeldatum,
        schema.CaseV2.afhandeldatum,
        schema.CaseV2.stalled_until,
        schema.CaseV2.milestone,
        schema.CaseV2.last_modified,
        schema.CaseV2.behandelaar_gm_id,
        schema.CaseV2.coordinator_gm_id,
        schema.CaseV2.aanvrager,
        schema.CaseV2.aanvraag_trigger,
        schema.CaseV2.onderwerp_extern,
        schema.CaseV2.resultaat,
        schema.CaseV2.resultaat_id,
        schema.CaseV2.result_uuid,
        schema.CaseV2.payment_amount,
        schema.CaseV2.payment_status,
        schema.CaseV2.confidentiality,
        schema.CaseV2.behandelaar,
        schema.CaseV2.coordinator,
        schema.CaseV2.urgency,
        schema.CaseV2.preset_client,
        schema.CaseV2.prefix,
        schema.CaseV2.active_selection_list,
        schema.CaseV2.case_status,
        schema.CaseV2.case_meta,
        schema.CaseV2.case_role,
        schema.CaseV2.type_of_archiving,
        schema.CaseV2.period_of_preservation,
        schema.CaseV2.result_description,
        schema.CaseV2.result_explanation,
        schema.CaseV2.result_selection_list_number,
        schema.CaseV2.result_process_type_number,
        schema.CaseV2.result_process_type_name,
        schema.CaseV2.result_process_type_description,
        schema.CaseV2.result_process_type_explanation,
        schema.CaseV2.result_process_type_generic,
        schema.CaseV2.result_origin,
        schema.CaseV2.result_process_term,
        schema.CaseV2.aggregation_scope,
        schema.CaseV2.number_parent,
        schema.CaseV2.number_master,
        schema.CaseV2.number_previous,
        schema.CaseV2.price,
        schema.CaseV2.result_process_type_object,
        schema.CaseV2.custom_fields,
        schema.CaseV2.file_custom_fields,
        schema.CaseV2.case_actions,
        schema.CaseV2.case_department,
        schema.CaseV2.case_subjects,
        schema.CaseV2.destructable,
        schema.CaseV2.suspension_rationale,
        schema.CaseV2.premature_completion_rationale,
        schema.CaseV2.days_left,
        schema.CaseV2.lead_time_real,
        schema.CaseV2.progress_days,
        sql.literal(
            True
            if (user_info and user_info.permissions.get("admin"))
            else False
        ).label("user_is_admin"),
        authorizations_subquery(user_info.user_uuid if user_info else None),
        *current_user_rights(user_info.user_uuid if user_info else None),
    ).where(
        sql.and_(
            schema.CaseV2.uuid == case_uuid,
            schema.CaseV2.status != "deleted",
        )
    )


def _get_roles_query(case_uuid):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "uuid",
                schema.ZaakBetrokkenen.subject_id,
                "type",
                schema.ZaakBetrokkenen.betrokkene_type,
                "role",
                sql.func.coalesce(
                    schema.ZaakBetrokkenen.rol,
                    sql.case(
                        (
                            schema.ZaakBetrokkenen.id == schema.Case.aanvrager,
                            "Aanvrager",
                        ),
                        (
                            schema.ZaakBetrokkenen.id
                            == schema.Case.behandelaar,
                            "Behandelaar",
                        ),
                        (
                            schema.ZaakBetrokkenen.id
                            == schema.Case.coordinator,
                            "Coordinator",
                        ),
                        else_=None,
                    ),
                ),
                "magic_string_prefix",
                sql.func.coalesce(
                    schema.ZaakBetrokkenen.magic_string_prefix,
                    sql.case(
                        (
                            schema.ZaakBetrokkenen.id == schema.Case.aanvrager,
                            "aanvrager",
                        ),
                        (
                            schema.ZaakBetrokkenen.id
                            == schema.Case.behandelaar,
                            "behandelaar",
                        ),
                        (
                            schema.ZaakBetrokkenen.id
                            == schema.Case.coordinator,
                            "coordinator",
                        ),
                        else_=None,
                    ),
                ).label("magic_string_prefix"),
            )
        )
        .select_from(
            sql.join(
                schema.Case,
                schema.ZaakBetrokkenen,
                sql.and_(
                    schema.Case.id == schema.ZaakBetrokkenen.zaak_id,
                    schema.ZaakBetrokkenen.deleted.is_(None),
                ),
            )
        )
        .where(schema.Case.uuid == case_uuid)
        .label("realted_contacts")
    )


def _get_person_info(uuid):
    return (
        sql.select(
            sql.func.json_build_object(
                "id",
                schema.NatuurlijkPersoon.id,
                "naamgebruik",
                schema.NatuurlijkPersoon.surname,
                "aanduiding_naamgebruik",
                schema.NatuurlijkPersoon.aanduiding_naamgebruik,
                "burgerservicenummer",
                schema.NatuurlijkPersoon.burgerservicenummer,
                "a_nummer",
                schema.NatuurlijkPersoon.a_nummer,
                "voorletters",
                schema.NatuurlijkPersoon.voorletters,
                "voornamen",
                schema.NatuurlijkPersoon.voornamen,
                "geslachtsnaam",
                schema.NatuurlijkPersoon.geslachtsnaam,
                "voorvoegsel",
                schema.NatuurlijkPersoon.voorvoegsel,
                "geslachtsaanduiding",
                schema.NatuurlijkPersoon.geslachtsaanduiding,
                "nationaliteitscode1",
                schema.NatuurlijkPersoon.nationaliteitscode1,
                "nationaliteitscode2",
                schema.NatuurlijkPersoon.nationaliteitscode2,
                "nationaliteitscode3",
                schema.NatuurlijkPersoon.nationaliteitscode3,
                "geboorteplaats",
                schema.NatuurlijkPersoon.geboorteplaats,
                "geboorteland",
                schema.NatuurlijkPersoon.geboorteland,
                "geboortedatum",
                schema.NatuurlijkPersoon.geboortedatum,
                "aanhef_aanschrijving",
                schema.NatuurlijkPersoon.aanhef_aanschrijving,
                "voorletters_aanschrijving",
                schema.NatuurlijkPersoon.voorletters_aanschrijving,
                "voornamen_aanschrijving",
                schema.NatuurlijkPersoon.voornamen_aanschrijving,
                "naam_aanschrijving",
                schema.NatuurlijkPersoon.naam_aanschrijving,
                "voorvoegsel_aanschrijving",
                schema.NatuurlijkPersoon.voorvoegsel_aanschrijving,
                "burgerlijke_staat",
                schema.NatuurlijkPersoon.burgerlijke_staat,
                "indicatie_geheim",
                schema.NatuurlijkPersoon.indicatie_geheim,
                "land_waarnaar_vertrokken",
                schema.NatuurlijkPersoon.land_waarnaar_vertrokken,
                "import_datum",
                schema.NatuurlijkPersoon.import_datum,
                "adres_id",
                schema.NatuurlijkPersoon.adres_id,
                "authenticated",
                schema.NatuurlijkPersoon.authenticated,
                "authenticatedby",
                schema.NatuurlijkPersoon.authenticatedby,
                "deleted_on",
                schema.NatuurlijkPersoon.deleted_on,
                "verblijfsobject_id",
                schema.NatuurlijkPersoon.verblijfsobject_id,
                "datum_overlijden",
                schema.NatuurlijkPersoon.datum_overlijden,
                "onderzoek_persoon",
                schema.NatuurlijkPersoon.onderzoek_persoon,
                "onderzoek_huwelijk",
                schema.NatuurlijkPersoon.onderzoek_huwelijk,
                "onderzoek_overlijden",
                schema.NatuurlijkPersoon.onderzoek_overlijden,
                "onderzoek_verblijfplaats",
                schema.NatuurlijkPersoon.onderzoek_verblijfplaats,
                "partner_a_nummer",
                schema.NatuurlijkPersoon.partner_a_nummer,
                "partner_burgerservicenummer",
                schema.NatuurlijkPersoon.partner_burgerservicenummer,
                "partner_voorvoegsel",
                schema.NatuurlijkPersoon.partner_voorvoegsel,
                "partner_geslachtsnaam",
                schema.NatuurlijkPersoon.partner_geslachtsnaam,
                "datum_huwelijk",
                schema.NatuurlijkPersoon.datum_huwelijk,
                "datum_huwelijk_ontbinding",
                schema.NatuurlijkPersoon.datum_huwelijk_ontbinding,
                "in_gemeente",
                schema.NatuurlijkPersoon.in_gemeente,
                "landcode",
                schema.NatuurlijkPersoon.landcode,
                "uuid",
                schema.NatuurlijkPersoon.uuid,
                "active",
                schema.NatuurlijkPersoon.active,
                "adellijke_titel",
                schema.NatuurlijkPersoon.adellijke_titel,
                "preferred_contact_channel",
                schema.NatuurlijkPersoon.preferred_contact_channel,
                "related_custom_object_id",
                schema.NatuurlijkPersoon.related_custom_object_id,
                "contact_data",
                sql.func.json_build_object(
                    "mobiel",
                    schema.ContactData.mobiel,
                    "telefoonnummer",
                    schema.ContactData.telefoonnummer,
                    "email",
                    schema.ContactData.email,
                    "note",
                    schema.ContactData.note,
                ),
                "address",
                sql.func.json_build_object(
                    "straatnaam",
                    schema.Adres.straatnaam,
                    "huisnummer",
                    schema.Adres.huisnummer,
                    "huisletter",
                    schema.Adres.huisletter,
                    "huisnummertoevoeging",
                    schema.Adres.huisnummertoevoeging,
                    "nadere_aanduiding",
                    schema.Adres.nadere_aanduiding,
                    "postcode",
                    schema.Adres.postcode,
                    "woonplaats",
                    schema.Adres.woonplaats,
                    "gemeentedeel",
                    schema.Adres.gemeentedeel,
                    "functie_adres",
                    schema.Adres.functie_adres,
                    "datum_aanvang_bewoning",
                    schema.Adres.datum_aanvang_bewoning,
                    "woonplaats_id",
                    schema.Adres.woonplaats_id,
                    "gemeente_code",
                    schema.Adres.gemeente_code,
                    "hash",
                    schema.Adres.hash,
                    "import_datum",
                    schema.Adres.import_datum,
                    "adres_buitenland1",
                    schema.Adres.adres_buitenland1,
                    "adres_buitenland2",
                    schema.Adres.adres_buitenland2,
                    "adres_buitenland3",
                    schema.Adres.adres_buitenland3,
                    "landcode",
                    schema.Adres.landcode,
                ),
            )
        )
        .select_from(
            sql.join(
                schema.NatuurlijkPersoon,
                schema.Adres,
                schema.Adres.id == schema.NatuurlijkPersoon.adres_id,
                isouter=True,
            ).join(
                schema.ContactData,
                sql.and_(
                    schema.ContactData.gegevens_magazijn_id
                    == schema.NatuurlijkPersoon.id,
                    schema.ContactData.betrokkene_type
                    == BETROKKENE_TYPE_NUMBER_FOR_PERSON,
                ),
                isouter=True,
            )
        )
        .where(schema.NatuurlijkPersoon.uuid == uuid)
    )


def _get_employee_info(uuid):
    return (
        sql.select(
            sql.func.json_build_object(
                "id",
                schema.Subject.id,
                "uuid",
                schema.Subject.uuid,
                "subject_type",
                schema.Subject.subject_type,
                "properties",
                schema.Subject.properties,
                "settings",
                schema.Subject.settings,
                "username",
                schema.Subject.username,
                "role_ids",
                schema.Subject.role_ids,
                "group_ids",
                schema.Subject.group_ids,
                "nobody",
                schema.Subject.nobody,
                "system",
                schema.Subject.system,
                "related_custom_object_id",
                schema.Subject.related_custom_object_id,
                "employee_address",
                _get_employee_address(uuid),
            )
        )
        .select_from(schema.Subject)
        .where(
            sql.and_(
                schema.Subject.uuid == uuid,
                schema.Subject.subject_type == "employee",
            )
        )
    )


def _get_employee_address(uuid):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "id",
                schema.Config.id,
                "parameter",
                schema.Config.parameter,
                "value",
                schema.Config.value,
                "advanced",
                schema.Config.advanced,
                "uuid",
                schema.Config.uuid,
                "definition_id",
                schema.Config.definition_id,
            )
        )
        .where(
            sql.and_(
                schema.Config.parameter == "customer_info_postcode",
                schema.Config.parameter == "customer_info_straatnaam",
                schema.Config.parameter == "customer_info_huisnummer",
            )
        )
        .label("employee_address")
    )


def _get_organization_info(uuid):
    return (
        sql.select(
            sql.func.json_build_object(
                "id",
                schema.Bedrijf.id,
                "uuid",
                schema.Bedrijf.uuid,
                "dossiernummer",
                schema.Bedrijf.dossiernummer,
                "handelsnaam",
                schema.Bedrijf.handelsnaam,
                "rechtsvorm",
                schema.Bedrijf.rechtsvorm,
                "vestiging_straatnaam",
                schema.Bedrijf.vestiging_straatnaam,
                "vestigingsnummer",
                schema.Bedrijf.vestigingsnummer,
                "vestiging_huisnummer",
                schema.Bedrijf.vestiging_huisnummer,
                "vestiging_huisnummertoevoeging",
                schema.Bedrijf.vestiging_huisnummertoevoeging,
                "vestiging_postcode",
                schema.Bedrijf.vestiging_postcode,
                "correspondence_info",
                sql.func.json_build_object(
                    "correspondentie_straatnaam",
                    schema.Bedrijf.correspondentie_straatnaam,
                    "correspondentie_huisnummer",
                    schema.Bedrijf.correspondentie_huisnummer,
                    "correspondentie_huisnummertoevoeging",
                    schema.Bedrijf.correspondentie_huisnummertoevoeging,
                    "correspondentie_postcode",
                    schema.Bedrijf.correspondentie_postcode,
                    "correspondentie_woonplaats",
                    schema.Bedrijf.correspondentie_woonplaats,
                    "correspondentie_adres_buitenland1",
                    schema.Bedrijf.correspondentie_adres_buitenland1,
                    "correspondentie_adres_buitenland2",
                    schema.Bedrijf.correspondentie_adres_buitenland2,
                    "correspondentie_adres_buitenland3",
                    schema.Bedrijf.correspondentie_adres_buitenland3,
                    "correspondentie_adres",
                    schema.Bedrijf.correspondentie_adres,
                    "correspondentie_postcodewoonplaats",
                    schema.Bedrijf.correspondentie_postcodewoonplaats,
                    "correspondentie_huisletter",
                    schema.Bedrijf.correspondentie_huisletter,
                    "correspondentie_landcode",
                    schema.Bedrijf.correspondentie_landcode,
                ),
                "vestiging_huisletter",
                schema.Bedrijf.vestiging_huisletter,
                "vestiging_landcode",
                schema.Bedrijf.vestiging_landcode,
                "subdossiernummer",
                schema.Bedrijf.subdossiernummer,
                "hoofdvestiging_dossiernummer",
                schema.Bedrijf.hoofdvestiging_dossiernummer,
                "hoofdvestiging_subdossiernummer",
                schema.Bedrijf.hoofdvestiging_subdossiernummer,
                "vorig_dossiernummer",
                schema.Bedrijf.vorig_dossiernummer,
                "vorig_subdossiernummer",
                schema.Bedrijf.vorig_subdossiernummer,
                "kamernummer",
                schema.Bedrijf.kamernummer,
                "faillisement",
                schema.Bedrijf.faillisement,
                "surseance",
                schema.Bedrijf.surseance,
                "telefoonnummer",
                schema.Bedrijf.telefoonnummer,
                "email",
                schema.Bedrijf.email,
                "vestiging_adres",
                schema.Bedrijf.vestiging_adres,
                "vestiging_postcodewoonplaats",
                schema.Bedrijf.vestiging_postcodewoonplaats,
                "vestiging_woonplaats",
                schema.Bedrijf.vestiging_woonplaats,
                "hoofdactiviteitencode",
                schema.Bedrijf.hoofdactiviteitencode,
                "nevenactiviteitencode1",
                schema.Bedrijf.nevenactiviteitencode1,
                "nevenactiviteitencode2",
                schema.Bedrijf.nevenactiviteitencode2,
                "werkzamepersonen",
                schema.Bedrijf.nevenactiviteitencode2,
                "contact_naam",
                schema.Bedrijf.contact_naam,
                "contact_aanspreektitel",
                schema.Bedrijf.contact_aanspreektitel,
                "contact_voorletters",
                schema.Bedrijf.contact_voorletters,
                "contact_voorvoegsel",
                schema.Bedrijf.contact_voorvoegsel,
                "contact_geslachtsnaam",
                schema.Bedrijf.contact_geslachtsnaam,
                "contact_geslachtsaanduiding",
                schema.Bedrijf.contact_geslachtsaanduiding,
                "authenticated",
                schema.Bedrijf.authenticated,
                "authenticatedby",
                schema.Bedrijf.authenticatedby,
                "import_datum",
                schema.Bedrijf.import_datum,
                "verblijfsobject_id",
                schema.Bedrijf.verblijfsobject_id,
                "vestigingsnummer",
                schema.Bedrijf.vestigingsnummer,
                "vestiging_adres_buitenland1",
                schema.Bedrijf.vestiging_adres_buitenland1,
                "vestiging_adres_buitenland2",
                schema.Bedrijf.vestiging_adres_buitenland2,
                "vestiging_adres_buitenland3",
                schema.Bedrijf.vestiging_adres_buitenland3,
                "contact_data",
                sql.func.json_build_object(
                    "mobiel",
                    schema.ContactData.mobiel,
                    "telefoonnummer",
                    schema.ContactData.telefoonnummer,
                    "email",
                    schema.ContactData.email,
                    "note",
                    schema.ContactData.note,
                ),
            )
        )
        .select_from(
            sql.join(
                schema.Bedrijf,
                schema.ContactData,
                sql.and_(
                    schema.ContactData.gegevens_magazijn_id
                    == schema.Bedrijf.id,
                    schema.ContactData.betrokkene_type
                    == BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
                ),
                isouter=True,
            )
        )
        .where(schema.Bedrijf.uuid == uuid)
    )


class CaseRepository(ZaaksysteemRepositoryBase):
    case_mapping = {
        "id": "id",
        "uuid": "uuid",
        "department": "route_ou",
        "role": "route_role",
        "assignee_gm_id": "behandelaar_gm_id",
        "coordinator_gm_id": "coordinator_gm_id",
        "destruction_date": "vernietigingsdatum",
        "archival_state": "archival_state",
        "status": "status",
        "created_date": "created",
        "registration_date": "registratiedatum",
        "target_completion_date": "streefafhandeldatum",
        "completion_date": "afhandeldatum",
        "stalled_until_date": "stalled_until",
        "milestone": "milestone",
        "last_modified_date": "last_modified",
        "subject": "onderwerp",
        "subject_extern": "onderwerp_extern",
        "deleted": "deleted",
    }

    case_meta_mapping = {
        "suspension_reason": "opschorten",
        "completion": "afhandeling",
        "stalled_since_date": "stalled_since",
        "stalled_until_date": "stalled_until",
    }
    case_postponed = {
        "status": "status",
        "suspension_reason": "suspension_rationale",
        "stalled_until_date": "date_stalled_until",
        "stalled_since_date": "date_stalled_since",
    }

    _for_entity = "Case"
    _events_to_calls = {
        "AssigneeNotified": "_do_nothing",
        "CaseAllocationSet": "_update_allocation",
        "CaseArchivalStateSet": "_update",
        "CaseAssigneeChanged": "_update_assignee",
        "CaseAssigneeCleared": "_clear_assignee",
        "CaseAssigneeEmailEnqueued": "_enqueue_assignee_email",
        "CaseAssigneeEmailSent": "_send_assignee_email",
        "CaseCompletionDateSet": "_update",
        "CaseCoordinatorSet": "_update_coordinator",
        "CaseCreated": "_create_case",
        "CaseDestructionDateCleared": "_update",
        "CaseDestructionDateRecalculated": "_update",
        "CaseDestructionDateSet": "_update",
        "CasePaused": "_update",
        "CasePaymentAmountSet": "_update_payment_amount",
        "CaseRegistrationDateSet": "_update",
        "CaseResultSet": "_update_result",
        "CaseResumed": "_update",
        "CaseStatusSet": "_update_status",
        "CaseSubjectSynchronized": "_update",
        "CaseTargetCompletionDateSet": "_update",
        "CaseTransitioned": "_transition_case",
        "CustomFieldUpdated": "_update_custom_fields",
        "DocumentsEnqueued": "_enqueue_documents",
        "DocumentsGenerated": "_generate_documents",
        "EmailsEnqueued": "_enqueue_emails",
        "EmailsGenerated": "_generate_emails",
        "SetCaseParent": "_set_case_parent",
        "SubcasesCreated": "_create_subcases",
        "SubcasesEnqueued": "_enqueue_subcases",
        "SubjectsEnqueued": "_enqueue_subjects",
        "SubjectsGenerated": "_generate_subjects",
        "CaseDeleted": "_delete_case",
    }

    def unsafe_find_case_by_uuid(self, case_uuid: UUID) -> Case:
        query = get_case_query(case_uuid, None)
        query_result = self.session.execute(query).fetchone()
        if query_result is None:
            raise NotFound(f"Case with uuid '{case_uuid}' not found.")
        case_entity = self._transform_to_entity(
            query_result=query_result,
        )
        return case_entity

    def find_case_by_uuid(
        self, case_uuid: UUID, user_info: minty.cqrs.UserInfo, permission
    ) -> Case:
        """Return case entity for given id by fetching case from database.

        :param case_uuid: case uuid
        :type case_uuid: UUID
        :param user_uuid: UUID of the user performing the action
        :type user_uuid: uuid
        :param permission: permission needed for the action ("search", "read",
            "write" or "manage")
        :type permission: str
        :return: case entity
        :rtype: CaseEntity
        """
        query = get_case_query(case_uuid, user_info)
        query = query.where(
            sql.and_(
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission=permission,
                    case_alias=schema.CaseV2,
                ),
            )
        )
        query_result = self.session.execute(query).fetchone()
        if query_result is None:
            raise NotFound(f"Case with uuid '{case_uuid}' not found.")
        case_entity = self._transform_to_entity(
            query_result=query_result,
        )

        return case_entity

    def _transform_case_actions(self, case_actions):
        actions = {}
        action_type_lookup = {
            "case": "cases",
            "template": "documents",
            "email": "emails",
            "subject": "subjects",
        }
        for case_action in case_actions:
            (action_phase, action_type, data) = (
                case_action["status"],
                case_action["type"],
                json.loads(case_action["data"]),
            )
            if actions.get(action_phase, None) is None:
                actions[action_phase] = {}
            actual_type = action_type_lookup.get(action_type, action_type)
            if actions[action_phase].get(actual_type, None) is None:
                actions[action_phase][actual_type] = []

            actions[action_phase][actual_type].append(
                {"data": data, "automatic": case_action["automatic"]}
            )

        return actions

    def _translate_contacts(
        self, case_uuid
    ) -> tuple[
        CaseContact | None,
        CaseContact | None,
        CaseContact | None,
        list[CaseContact] | None,
    ]:
        roles_list = self.session.execute(
            _get_roles_query(case_uuid)
        ).fetchone()
        related_contacts = []
        (requestor, coordinator, assignee) = None, None, None

        for contact in roles_list[0]:
            role_details = None

            if contact["type"] == "natuurlijk_persoon":
                role_details = self.get_contact_person(
                    contact["uuid"],
                    contact["magic_string_prefix"],
                    role=contact["role"],
                )
            elif contact["type"] == "medewerker":
                role_details = self.get_contact_employee(
                    contact["uuid"],
                    contact["magic_string_prefix"],
                    role=contact["role"],
                )
            elif contact["type"] == "bedrijf":
                role_details = self.get_contact_organization(
                    contact["uuid"],
                    contact["magic_string_prefix"],
                    role=contact["role"],
                )
            if role_details:
                if contact["role"] == "Aanvrager":
                    requestor = role_details
                elif contact["role"] == "Coordinator":
                    coordinator = role_details
                elif contact["role"] == "Behandelaar":
                    assignee = role_details
                else:
                    related_contacts.append(role_details)

        return requestor, coordinator, assignee, related_contacts

    def _translate_subjects(self, subjects):
        mapped_type = {
            "medewerker": "employee",
            "natuurlijk_persoon": "person",
            "bedrijf": "organization,",
        }
        subjects_list = []
        if subjects:
            subjects_list = [
                {
                    "type": mapped_type[subject["betrokkene_type"]],
                    "uuid": str(subject["subject_id"]),
                    "meta": {"role": subject["rol"]},
                }
                for subject in subjects
                if subject["subject_id"] is not None
            ]
        return subjects_list

    def _translate_department(self, department) -> CaseDepartment | None:
        if department is None:
            return None

        if department["parent_uuid"] is not None:
            parent = DepartmentSummary(
                uuid=department["parent_uuid"],
                name=department["parent_name"],
            )
        else:
            parent = None

        return (
            CaseDepartment(
                uuid=department["uuid"],
                name=department["name"],
                description=department["description"] or "",
                parent=parent,
            )
            if department["uuid"] and department["name"]
            else None
        )

    def _translate_role(self, role) -> CaseRole | None:
        if role["uuid"] is None:
            return None

        if role["parent_uuid"] is not None:
            parent = DepartmentSummary(
                uuid=role["parent_uuid"],
                name=role["parent_name"],
            )
        else:
            parent = None

        # at least uuid and name must be present to create a valid CaseRole
        return (
            CaseRole(
                uuid=role["uuid"],
                name=role["name"],
                description=role["description"] or "",
                parent=parent,
            )
            if role["uuid"] and role["name"]
            else None
        )

    def _get_related_cases_for_case(self, case_id: int) -> dict:
        case_a = sql.alias(schema.Case.__table__)
        case_b = sql.alias(schema.Case.__table__)

        related_cases_query = (
            sql.select(
                case_b.c.id.label("case_id_b"),
                case_b.c.uuid.label("case_uuid_b"),
            )
            .select_from(
                sql.join(
                    schema.CaseRelation,
                    case_a,
                    case_a.c.id == schema.CaseRelation.case_id_a,
                ).join(case_b, case_b.c.id == schema.CaseRelation.case_id_b)
            )
            .where(
                sql.and_(
                    sql.or_(
                        schema.CaseRelation.case_id_a == case_id,
                        schema.CaseRelation.case_id_b == case_id,
                    ),
                    case_a.c.status != "deleted",
                    case_a.c.deleted.is_(None),
                    case_b.c.status != "deleted",
                    case_b.c.deleted.is_(None),
                )
            )
        )
        parent = sql.alias(schema.Case.__table__)
        parent_query = sql.select(
            parent.c.id.label("case_a_id"),
            parent.c.uuid.label("case_a_uuid"),
            sql.literal("parent").label("case_type"),
        ).select_from(
            sql.join(parent, schema.Case, parent.c.id == schema.Case.pid)
        )

        children = sql.alias(schema.Case.__table__)
        children_query = sql.select(
            children.c.id.label("case_a_id"),
            children.c.uuid.label("case_a_uuid"),
            sql.literal("child").label("case_type"),
        ).select_from(
            sql.join(children, schema.Case, children.c.pid == schema.Case.id)
        )

        parent_child = self.session.execute(
            sql.union(
                parent_query.where(schema.Case.id == case_id),
                children_query.where(schema.Case.id == case_id),
            )
        ).fetchall()

        parent_child_ids = ",".join(
            [str(item.case_a_id) for item in parent_child]
        )
        parent_child_ids = []
        parent_uuid = None
        child_uuids = []
        for item in parent_child:
            parent_child_ids.append(str(item.case_a_id))

            if item.case_type == "parent":
                parent_uuid = item.case_a_uuid
            elif item.case_type == "child":
                child_uuids.append(str(item.case_a_uuid))

        relations_complete = self.session.execute(
            children_query.where(
                sql.and_(
                    schema.Case.id == case_id, children.c.status == "resolved"
                )
            )
        ).fetchall()

        if len(relations_complete) == 0:
            relations_complete_status = "Nee"
        else:
            relations_complete_status = "Ja"
        query_result = self.session.execute(related_cases_query).fetchall()
        related_ids = [str(item.case_id_b) for item in query_result]
        related_uuids = ",".join(
            [str(item.case_uuid_b) for item in query_result]
        )
        relations = ",".join(related_ids + parent_child_ids)
        return {
            "related_cases": ",".join(related_ids),
            "relations": relations,
            "relations_complete": relations_complete_status,
            "related_uuids": related_uuids,
            "parent_uuid": parent_uuid,
            "child_uuids": ",".join(child_uuids),
        }

    def _get_documents_for_case(self, case_id) -> tuple[str, str]:
        document_labels_query = sql.func.array(
            sql.select(
                sql.func.json_build_object(
                    "uuid",
                    schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid,
                    "name",
                    schema.ZaaktypeDocumentKenmerkenMap.name,
                )
            )
            .select_from(
                sql.join(
                    schema.ZaaktypeDocumentKenmerkenMap,
                    sql.join(
                        schema.FileCaseDocument,
                        schema.Case,
                        schema.Case.id == schema.FileCaseDocument.case_id,
                    ),
                    sql.and_(
                        schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id
                        == schema.FileCaseDocument.bibliotheek_kenmerken_id,
                        schema.ZaaktypeDocumentKenmerkenMap.zaaktype_node_id
                        == schema.Case.zaaktype_node_id,
                    ),
                )
            )
            .where(
                sql.and_(
                    schema.FileCaseDocument.file_id == schema.File.id,
                    schema.File.case_id == schema.Case.id,
                )
            )
            .label("document_labels_subquery")
        )

        documents_query = (
            sql.select(
                schema.File.id,
                schema.File.name,
                schema.File.document_status.label("status"),
                schema.File.extension,
                document_labels_query.label("labels"),
            )
            .select_from(
                sql.join(
                    schema.File,
                    schema.Case,
                    schema.File.case_id == schema.Case.id,
                    isouter=True,
                )
            )
            .where(
                sql.and_(
                    schema.File.case_id == case_id,
                    schema.File.date_deleted.is_(None),
                    schema.File.destroyed.is_(False),
                )
            )
            .order_by(schema.File.name)
        )

        query_result = self.session.execute(documents_query).fetchall()
        documents = []
        case_documents = []
        for item in query_result:
            documents.append(f"{item.name}{item.extension} ({item.status})")
            if item.labels:
                case_documents.append(
                    f"{item.name}{item.extension} ({item.status})"
                )

        return ",".join(documents), ",".join(case_documents)

    def _get_case_location(self, case_id: int) -> str | None:
        case_location = None
        location_query = (
            sql.select(
                sql.cast(schema.BagCache.bag_data, sql_pg.JSON)[
                    "weergavenaam"
                ].astext.label("case_location"),
            )
            .select_from(
                sql.join(
                    schema.ZaakBag,
                    schema.BagCache,
                    schema.ZaakBag.bag_id == schema.BagCache.bag_id,
                    isouter=True,
                )
            )
            .where(schema.ZaakBag.zaak_id == case_id)
        )
        query_result = self.session.execute(location_query).fetchone()
        if query_result:
            case_location = query_result.case_location
        return case_location

    @validate_arguments
    def _get_authorizations(
        self,
        user_is_admin: bool | None = None,
        user_is_requestor: bool | None = None,
        user_is_assignee: bool | None = None,
        user_is_coordinator: bool | None = None,
        authorizations: list[_shared.CaseAuthorizationLevel] | None = None,
    ) -> set[_shared.CaseAuthorizationLevel] | None:
        if authorizations is None:
            return None

        if user_is_admin:
            return {
                _shared.CaseAuthorizationLevel.search,
                _shared.CaseAuthorizationLevel.read,
                _shared.CaseAuthorizationLevel.write,
                _shared.CaseAuthorizationLevel.manage,
            }

        all_authorizations = set(authorizations)

        if user_is_requestor:
            all_authorizations |= {
                _shared.CaseAuthorizationLevel.search,
                _shared.CaseAuthorizationLevel.read,
            }

        if user_is_coordinator or user_is_assignee:
            all_authorizations |= {
                _shared.CaseAuthorizationLevel.search,
                _shared.CaseAuthorizationLevel.read,
                _shared.CaseAuthorizationLevel.write,
            }

        return all_authorizations

    def _transform_to_entity(self, query_result):
        case = query_result
        phase = query_result.case_status
        meta_result = {}
        meta_result = query_result.case_meta

        actions = self._transform_case_actions(query_result.case_actions)

        alternative_case_number = case.id
        if case.prefix not in [None, ""]:
            alternative_case_number = "-".join([case.prefix, str(case.id)])

        (
            requestor,
            coordinator,
            assignee,
            related_contacts,
        ) = self._translate_contacts(case_uuid=case.uuid)
        case_type_version = self._get_case_type_version(case.id)
        case_type = CaseTypeForCase(
            entity_id=case_type_version.case_type_uuid,
            uuid=case_type_version.case_type_uuid,
        )

        progress_status = case.milestone / len(case_type_version.phases)

        case_type_phase = None
        if len(case_type_version.phases) > 1:
            case_type_phase = case_type_version.phases[1].phase

        stalled_since_date = None
        if "stalled_since" in meta_result:
            stalled_since_date = format_date(meta_result["stalled_since"])

        case_type_phase = None
        if len(case_type_version.phases) > 1:
            case_type_phase = case_type_version.phases[1].phase

        documents, case_documents = self._get_documents_for_case(case.id)
        custom_fields_repo = self._get_repo(case_management, "custom_fields")
        new_case = Case.parse_obj(
            {
                "entity_id": case.uuid,
                "entity_meta_summary": case.onderwerp,
                "id": case.id,
                "number": case.id,
                "uuid": case.uuid,
                "department": self._translate_department(case.case_department),
                "role": self._translate_role(case.case_role),
                "destruction_date": format_date(case.vernietigingsdatum),
                "archival_state": case.archival_state,
                "status": case.status,
                "contact_channel": case.contactkanaal,
                "created_date": format_date(case.created),
                "registration_date": format_date(case.registratiedatum),
                "target_completion_date": format_date(
                    case.streefafhandeldatum
                ),
                "completion_date": format_date(case.afhandeldatum),
                "stalled_until_date": format_date(case.stalled_until),
                "milestone": case.milestone,
                "last_modified_date": format_date(case.last_modified),
                "suspension_reason": meta_result.get("opschorten", None),
                "completion": meta_result.get("afhandeling", None),
                "stalled_since_date": stalled_since_date,
                "assignee": assignee,
                "coordinator": coordinator,
                "requestor": requestor,
                "request_trigger": case.aanvraag_trigger,
                "meta": self._translate_meta_for_case_entity(meta_result),
                "phase": self._translate_phase_for_case_entity(
                    phase["fase"], phase["naam"]
                ),
                "public_summary": case.onderwerp_extern,
                "result": {
                    "result": case.resultaat,
                    "result_name": case.result_description,
                    "result_uuid": case.result_uuid,
                    "archival_attributes": {
                        "state": case.archival_state,
                        "selection_list": case.active_selection_list or "",
                    },
                }
                if case.result_uuid
                else None,
                "custom_fields": custom_fields_repo.format_custom_fields_for_case(
                    custom_fields=query_result.custom_fields,
                    file_custom_fields=query_result.file_custom_fields,
                ).custom_fields,
                "payment": self._translate_payment_for_case_entity(
                    case.payment_amount, case.payment_status
                ),
                "confidentiality": case.confidentiality,
                "case_actions": actions,
                "subjects": self._translate_subjects(case.case_subjects),
                "case_type_version": case_type_version,
                "case_type": case_type,
                "alternative_case_number": alternative_case_number,
                "urgency": case.urgency,
                "preset_client": case.preset_client,
                "related_contacts": related_contacts,
                "progress_status": progress_status,
                "case_type_phase": case_type_phase,
                "unaccepted_files_count": meta_result.get(
                    "unaccepted_files_count", None
                ),
                "unaccepted_attribute_update_count": meta_result.get(
                    "unaccepted_attribute_update_count", None
                ),
                "destructable": case.destructable,
                "type_of_archiving": case.type_of_archiving,
                "period_of_preservation": case.period_of_preservation,
                "result_description": case.result_description,
                "result_explanation": case.result_explanation,
                "result_selection_list_number": case.result_selection_list_number,
                "result_process_type_number": case.result_process_type_number,
                "result_process_type_name": case.result_process_type_name,
                "result_process_type_description": case.result_process_type_description,
                "result_process_type_generic": case.result_process_type_generic,
                "result_origin": case.result_origin,
                "result_process_term": case.result_process_term,
                "active_selection_list": case.active_selection_list,
                "aggregation_scope": case.aggregation_scope,
                "number_parent": case.number_parent,
                "number_master": case.number_master,
                "number_previous": case.number_previous,
                "price": case.price,
                "result_process_type_object": case.result_process_type_object,
                **self._get_related_cases_for_case(case.id),
                "documents": documents,
                "case_documents": case_documents,
                "progress_days": case.progress_days
                if case.progress_days
                else None,
                "case_location": self._get_case_location(case.id),
                "entity_meta_authorizations": self._get_authorizations(
                    case.user_is_admin,
                    case.user_is_requestor,
                    case.user_is_assignee,
                    case.user_is_coordinator,
                    case.authorizations,
                ),
                "_event_service": self.event_service,
            }
        )

        self.cache[str(new_case.entity_id)] = new_case

        return new_case

    # This code will disappear once the creation of subcases
    # has been moved to Python [20191012]
    def _get_metadata_for_queue_item(self, user_uuid):
        metadata_ = {
            "require_object_model": 1,
            "subject_id": case_acl.get_subject_by_uuid(
                user_uuid, self.session
            ).Subject.id,
            "target": "backend",
        }
        return metadata_

    def _do_nothing(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        """
        A "do-nothing" event action, that can be used so "save" does not
        complain about a missing handler.
        """
        pass

    def _enqueue_subcases(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        # This code will disappear once the creation of subcases
        # has been moved to Python [20191012]
        changes = self._format_changes(event)
        case_row = self.cache[event.entity_id]
        data_dict = json.loads(changes["enqueued_subcases_data"])
        for queue_id, subcase_dict in data_dict.items():
            subcase_dict["loop_protection_counter"] = 1
            values = {
                "id": queue_id,
                "object_id": event.entity_id,
                "status": "pending",
                "type": "create_case_subcase",
                "label": "Create case " + str(case_row.id),
                "data": subcase_dict,
                "date_created": datetime.now(timezone.utc),
                "date_started": None,
                "date_finished": None,
                "parent_id": None,
                "priority": 1000,
                "metadata": self._get_metadata_for_queue_item(event.user_uuid),
            }
            insert_stmt = sql.insert(schema.Queue)
            self.session.execute(insert_stmt, [values])

    def _enqueue_documents(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        # This code will disappear once the creation of documents
        # has been moved to Python [20191012]
        changes = self._format_changes(event)
        data_dict = json.loads(changes["enqueued_documents_data"])
        for queue_id, document in data_dict.items():
            document["loop_protection_counter"] = 1
            values = {
                "id": queue_id,
                "object_id": event.entity_id,
                "status": "pending",
                "type": "create_case_document",
                "label": "Create case document",
                "data": document,
                "date_created": datetime.now(timezone.utc),
                "date_started": None,
                "date_finished": None,
                "parent_id": None,
                "priority": 1000,
                "metadata": self._get_metadata_for_queue_item(event.user_uuid),
            }
            insert_stmt = sql.insert(schema.Queue)
            self.session.execute(insert_stmt, [values])

    def _enqueue_emails(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        # This code will disappear once the creation of emails
        # has been moved to Python [20191012]
        changes = self._format_changes(event)
        data_dict = json.loads(changes["enqueued_emails_data"])
        for queue_id, email in data_dict.items():
            email["loop_protection_counter"] = 1
            values = {
                "id": queue_id,
                "object_id": event.entity_id,
                "status": "pending",
                "type": "send_email",
                "label": "Create case email",
                "data": email,
                "date_created": datetime.now(timezone.utc),
                "date_started": None,
                "date_finished": None,
                "parent_id": None,
                "priority": 1000,
                "metadata": self._get_metadata_for_queue_item(event.user_uuid),
            }
            insert_stmt = sql.insert(schema.Queue)
            self.session.execute(insert_stmt, [values])

    def _enqueue_subjects(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        # This code will disappear once the creation of subjects
        # has been moved to Python [20191012]
        changes = self._format_changes(event)
        data_dict = json.loads(changes["enqueued_subjects_data"])
        for queue_id, email in data_dict.items():
            email["loop_protection_counter"] = 1
            values = {
                "id": queue_id,
                "object_id": event.entity_id,
                "status": "pending",
                "type": "add_case_subject",
                "label": "Create case subject",
                "data": email,
                "date_created": datetime.now(timezone.utc),
                "date_started": None,
                "date_finished": None,
                "parent_id": None,
                "priority": 1000,
                "metadata": self._get_metadata_for_queue_item(event.user_uuid),
            }
            insert_stmt = sql.insert(schema.Queue)
            self.session.execute(insert_stmt, [values])

    def _run_queue_items(self, changes):
        if CA_BUNDLE:
            ctx = ssl.create_default_context(cafile=CA_BUNDLE)
        else:
            ctx = ssl.create_default_context()

        if API_HOSTNAME:
            run_queue_url_base = f"http://{API_HOSTNAME}/api/queue/"
        else:
            run_queue_url_base = f"https://{self.context}/api/queue/"

        for queue_id in json.loads(changes["queue_ids"]):
            run_queue_url = run_queue_url_base + f"{queue_id}/run"

            try:
                self.logger.debug("Calling API URL: " + run_queue_url)

                req = urllib.request.Request(run_queue_url)
                req.add_header("Host", self.context)
                response = urllib.request.urlopen(req, context=ctx)

                response_read = response.read()
                response_dict = json.loads(response_read)
                self.logger.info(
                    str(datetime.now())
                    + " Completed queue_id "
                    + queue_id
                    + ": "
                    + json.dumps(response_dict)
                )
            except HTTPError as error:
                self.logger.error(f"Error during API call: {error}")

    def _create_subcases(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = self._format_changes(event)
        self._run_queue_items(changes)

    def _generate_documents(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = self._format_changes(event)
        self._run_queue_items(changes)

    def _generate_emails(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = self._format_changes(event)
        self._run_queue_items(changes)

    def _generate_subjects(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = self._format_changes(event)
        self._run_queue_items(changes)

    def create_new_case(
        self,
        user_info: minty.cqrs.UserInfo,
        uuid: UUID,
        case_type_version: CaseTypeVersionEntity,
        contact_channel: str,
        requestor: CaseContact | None,
        custom_fields: dict | None,
        confidentiality: str | None,
        contact_information: dict | None,
        options: dict | None,
    ) -> Case:
        """Initialize new case for case creation.

        :param user_info: user_info
        :type user_info: object
        :param uuid: uuid of the case
        :type uuid: UUID
        :param contactchannel: contact channel
        :type contactchannel: str
        :param requestor: requestor of case
        :type requestor: object
        :param custom_fields: custom_fields of case
        :type custom_fields: Optional[dict]
        :param confidentiality: confidentiality of case
        :type confidentiality: Optional[str]
        :return: Case entity
        :rtype: Case
        :raises Conflict: When case type has any of the rules/documents/emails/subcases/involved_entities at phase one.
        """

        if not case_type_version.active:
            raise Conflict("Case type is not active.", "case_type/not_active")

        if not case_type_version.is_eligible_for_case_creation:
            raise Conflict(
                "Case type is not eligible for api V2.",
                "case_type/not_eligible_for_v2",
            )

        allow_missing_required_fields = False
        if options and options.get("allow_missing_required_fields"):
            if user_info.permissions.get("zaak_create_skip_required"):
                allow_missing_required_fields = True
            else:
                user_uuid = user_info.user_uuid
                raise Conflict(
                    f"User with uuid '{user_uuid}' doesnot have permission to skip the required fields",
                    "case/user_cannot_skip_required_fields",
                )
        related_contacts = []
        new_case = Case(
            entity_id=uuid,
            id=None,
            number=None,
            uuid=uuid,
            department=None,
            role=None,
            destruction_date=None,
            archival_state=None,
            status=None,
            contact_channel=None,
            created_date=None,
            registration_date=None,
            target_completion_date=None,
            completion_date=None,
            stalled_until_date=None,
            milestone=None,
            suspension_reason=None,
            completion=None,
            stalled_since_date=None,
            last_modified_date=None,
            coordinator=None,
            assignee=None,
            requestor=None,
            request_trigger=None,
            resume_reason=None,
            summary=None,
            meta=[],
            attributes={},
            related_contacts=related_contacts,
            _event_service=self.event_service,
        )
        new_case.create(
            contact_channel=contact_channel,
            requestor=requestor,
            case_type_version=case_type_version,
            custom_fields=custom_fields,
            allow_missing_required_fields=allow_missing_required_fields,
            confidentiality=confidentiality,
            contact_information=contact_information,
            related_contacts=related_contacts,
        )
        self.cache[str(case_type_version.uuid)] = case_type_version
        self.cache[str(uuid)] = new_case
        return new_case

    def _format_changes(self, event: minty.cqrs.Event):
        changes = {}
        for change in event.changes:
            changes[change["key"]] = change["new_value"]
        return changes

    def _update_requestor(self, case_id, requestor):
        """Update requestor/aanvrager field for a case.

        :param case_id: id of the case
        :type case_id: int
        :param requestor: new value for requestor
        :type requestor: dict
        """
        magic_string_prefix = requestor.get("magic_string_prefix")

        if requestor["type"] == "person":
            natuurlijk_persoon_uuid = requestor["uuid"]
            person_query = _get_person_info(natuurlijk_persoon_uuid)

            person = self.session.execute(person_query).fetchone()
            if person is None:
                raise NotFound(
                    f"Person with uuid '{natuurlijk_persoon_uuid}' not found.",
                    "natuurlijk_persoon/not_found",
                )
            requestor_entity = self._generate_entity_from_person(
                person=person[0], magic_string_prefix=magic_string_prefix
            )

            self.cache[natuurlijk_persoon_uuid] = requestor_entity

            person_snapshot_id = self._add_snapshots_for_person(
                person=person[0]
            )
            involved_entity_requestor_id = add_entity_from_natural_person(
                person[0],
                person_snapshot_id,
                case_id,
                "Aanvrager",
                self.session,
            )
            return involved_entity_requestor_id, requestor_entity.id
        elif requestor["type"] == "employee":
            subject_uuid = requestor["uuid"]
            employee_query = _get_employee_info(subject_uuid)

            subject = self.session.execute(employee_query).fetchone()
            if subject is None:
                raise NotFound(
                    f"Employee with uuid '{subject_uuid}' not found.",
                    "employee/not_found",
                )

            # generate Requestor entity and cache it in the repo
            requestor_entity = self._generate_entity_from_employee(
                subject=subject[0], magic_string_prefix=magic_string_prefix
            )
            self.cache[subject_uuid] = requestor_entity

            involved_entity_requestor_id = add_entity_from_subject(
                subject[0], case_id, "Aanvrager", self.session
            )

            return involved_entity_requestor_id, subject[0]["id"]
        elif requestor["type"] == "organization":
            organization_uuid = requestor["uuid"]
            organization_query = _get_organization_info(organization_uuid)

            organization = self.session.execute(organization_query).fetchone()
            if organization is None:
                raise NotFound(
                    f"Organization with uuid '{organization_uuid}' not found.",
                    "organization/not_found",
                )

            # generate Requestor entity and cache it in the repo
            requestor_entity = self._generate_entity_from_organization(
                organization=organization[0],
                magic_string_prefix=magic_string_prefix,
            )
            self.cache[organization_uuid] = requestor_entity

            organization_snapshot_id = self._add_snapshot_for_organization(
                organization=organization[0]
            )
            involved_entity_requestor_id = add_entity_from_organization(
                organization[0],
                organization_snapshot_id,
                case_id,
                "Aanvrager",
                self.session,
            )
            return involved_entity_requestor_id, organization[0].id

        else:
            raise Conflict(
                f"Requestor of type '{requestor['type']}' is not allowed for a case."
            )

    def _add_snapshots_for_person(self, person):
        """Add snapshots for Natural person.

        :param person: natural person object
        :type person: schema.NatuurlijkPersoon
        :param adress: address of person
        :type adress: schema.Adres
        :return: requestor_snapshot_id
        :rtype: int
        """
        # add snapshots of person in GmNatuurlijkPersoon table
        person_snapshot_values = {
            "gegevens_magazijn_id": person.id,
            "burgerservicenummer": person.burgerservicenummer,
            "a_nummer": person.a_nummer,
            "voorletters": person.voorletters,
            "voorvoegsel": person.voorvoegsel,
            "voornamen": person.voornamen,
            "geslachtsnaam": person.geslachtsnaam,
            "geslachtsaanduiding": person.geslachtsaanduiding,
            "nationaliteitscode1": person.nationaliteitscode1,
            "nationaliteitscode2": person.nationaliteitscode2,
            "nationaliteitscode3": person.nationaliteitscode3,
            "geboorteplaats": person.geboorteplaats,
            "geboorteland": person.geboorteland,
            "geboortedatum": person.geboortedatum,
            "aanhef_aanschrijving": person.aanhef_aanschrijving,
            "voorletters_aanschrijving": person.voorletters_aanschrijving,
            "voornamen_aanschrijving": person.voornamen_aanschrijving,
            "naam_aanschrijving": person.naam_aanschrijving,
            "voorvoegsel_aanschrijving": person.voorvoegsel_aanschrijving,
            "burgerlijke_staat": person.burgerlijke_staat,
            "indicatie_geheim": person.indicatie_geheim,
            "import_datum": person.import_datum,
            "authenticatedby": person.authenticatedby,
            "verblijfsobject_id": person.verblijfsobject_id,
            "datum_overlijden": person.datum_overlijden,
            "aanduiding_naamgebruik": person.aanduiding_naamgebruik,
            "onderzoek_persoon": person.onderzoek_persoon,
            "onderzoek_huwelijk": person.onderzoek_huwelijk,
            "onderzoek_overlijden": person.onderzoek_overlijden,
            "onderzoek_verblijfplaats": person.onderzoek_verblijfplaats,
            "partner_a_nummer": person.partner_a_nummer,
            "partner_burgerservicenummer": person[
                "partner_burgerservicenummer"
            ],
            "partner_voorvoegsel": person.partner_voorvoegsel,
            "partner_geslachtsnaam": person.partner_geslachtsnaam,
            "datum_huwelijk": person.datum_huwelijk,
            "datum_huwelijk_ontbinding": person.datum_huwelijk_ontbinding,
            "adres_id": None,
            "landcode": person.landcode,
            "naamgebruik": person.naamgebruik,
            "adellijke_titel": person.adellijke_titel,
        }
        person_snapshot_insert_stmt = sql.insert(
            schema.GmNatuurlijkPersoon
        ).values(person_snapshot_values)

        person_snapshot_id = self.session.execute(
            person_snapshot_insert_stmt
        ).inserted_primary_key[0]

        # add snapshot of address in GmAdres table
        if person.address:
            address = person.address
            address_snapshot_values = {
                "straatnaam": address["straatnaam"],
                "huisnummer": address["huisnummer"],
                "huisnummertoevoeging": address["huisnummertoevoeging"],
                "nadere_aanduiding": address["nadere_aanduiding"],
                "postcode": address["postcode"],
                "woonplaats": address["woonplaats"],
                "functie_adres": address["functie_adres"],
                "landcode": address["landcode"],
                "natuurlijk_persoon_id": person_snapshot_id,
                "gemeentedeel": address["gemeentedeel"],
                "datum_aanvang_bewoning": address["datum_aanvang_bewoning"],
                "woonplaats_id": address["woonplaats_id"],
                "gemeente_code": address["gemeente_code"],
                "hash": address["hash"],
                "import_datum": address["import_datum"],
                "adres_buitenland1": address["adres_buitenland1"],
                "adres_buitenland2": address["adres_buitenland2"],
                "adres_buitenland3": address["adres_buitenland3"],
            }
            address_snapshot_insert_stmt = sql.insert(schema.GmAdres).values(
                address_snapshot_values
            )

            address_snapshot_id = self.session.execute(
                address_snapshot_insert_stmt
            ).inserted_primary_key[0]
        else:
            address_snapshot_id = None

        # update person_snapshot with correct adress_snapshot id
        self.session.execute(
            sql.update(schema.GmNatuurlijkPersoon)
            .where(schema.GmNatuurlijkPersoon.id == person_snapshot_id)
            .values({"adres_id": address_snapshot_id})
            .execution_options(synchronize_session=False)
        )

        return person_snapshot_id

    def _add_snapshot_for_organization(self, organization):
        """Add snapshot for Organization.

        :param organization: organization
        :type organization: schema.Bedrijf
        :return: organization_snapshot_id
        :rtype: int
        """
        # add snapshot of organization in GmBedrijf table
        organization_snapshot_values = {
            "gegevens_magazijn_id": organization.id,
            "dossiernummer": organization.dossiernummer,
            "subdossiernummer": organization.subdossiernummer,
            "hoofdvestiging_dossiernummer": organization[
                "hoofdvestiging_dossiernummer"
            ],
            "hoofdvestiging_subdossiernummer": organization[
                "hoofdvestiging_subdossiernummer"
            ],
            "vorig_dossiernummer": organization.vorig_dossiernummer,
            "vorig_subdossiernummer": organization.vorig_subdossiernummer,
            "handelsnaam": organization.handelsnaam,
            "rechtsvorm": organization.rechtsvorm,
            "kamernummer": organization.kamernummer,
            "faillisement": organization.faillisement,
            "surseance": organization.surseance,
            "telefoonnummer": organization.telefoonnummer,
            "email": organization.email,
            "vestiging_adres": organization.vestiging_adres,
            "vestiging_straatnaam": organization.vestiging_straatnaam,
            "vestiging_huisnummer": organization.vestiging_huisnummer,
            "vestiging_huisnummertoevoeging": organization[
                "vestiging_huisnummertoevoeging"
            ],
            "vestiging_postcodewoonplaats": organization[
                "vestiging_postcodewoonplaats"
            ],
            "vestiging_postcode": organization.vestiging_postcode,
            "vestiging_woonplaats": organization.vestiging_woonplaats,
            "correspondentie_adres": organization.correspondence_info[
                "correspondentie_adres"
            ],
            "correspondentie_straatnaam": organization.correspondence_info[
                "correspondentie_straatnaam"
            ],
            "correspondentie_huisnummer": organization.correspondence_info[
                "correspondentie_huisnummer"
            ],
            "correspondentie_huisnummertoevoeging": organization.correspondence_info[
                "correspondentie_huisnummertoevoeging"
            ],
            "correspondentie_postcodewoonplaats": organization.correspondence_info[
                "correspondentie_postcodewoonplaats"
            ],
            "correspondentie_postcode": organization.correspondence_info[
                "correspondentie_postcode"
            ],
            "correspondentie_woonplaats": organization.correspondence_info[
                "correspondentie_woonplaats"
            ],
            "hoofdactiviteitencode": organization.hoofdactiviteitencode,
            "nevenactiviteitencode1": organization.nevenactiviteitencode1,
            "nevenactiviteitencode2": organization.nevenactiviteitencode2,
            "werkzamepersonen": organization.werkzamepersonen,
            "contact_naam": organization.contact_naam,
            "contact_aanspreektitel": organization.contact_aanspreektitel,
            "contact_voorletters": organization.contact_voorletters,
            "contact_voorvoegsel": organization.contact_voorvoegsel,
            "contact_geslachtsnaam": organization.contact_geslachtsnaam,
            "contact_geslachtsaanduiding": organization[
                "contact_geslachtsaanduiding"
            ],
            "authenticated": organization.authenticated,
            "authenticatedby": organization.authenticatedby,
            "import_datum": organization.import_datum,
            "verblijfsobject_id": organization.verblijfsobject_id,
            "vestigingsnummer": organization.vestigingsnummer,
            "vestiging_huisletter": organization.vestiging_huisletter,
            "correspondentie_huisletter": organization.correspondence_info[
                "correspondentie_huisletter"
            ],
            "vestiging_adres_buitenland1": organization[
                "vestiging_adres_buitenland1"
            ],
            "vestiging_adres_buitenland2": organization[
                "vestiging_adres_buitenland2"
            ],
            "vestiging_adres_buitenland3": organization[
                "vestiging_adres_buitenland3"
            ],
            "vestiging_landcode": organization.vestiging_landcode,
            "correspondentie_adres_buitenland1": organization.correspondence_info[
                "correspondentie_adres_buitenland1"
            ],
            "correspondentie_adres_buitenland2": organization.correspondence_info[
                "correspondentie_adres_buitenland2"
            ],
            "correspondentie_adres_buitenland3": organization.correspondence_info[
                "correspondentie_adres_buitenland3"
            ],
            "correspondentie_landcode": organization.correspondence_info[
                "correspondentie_landcode"
            ],
        }
        organization_snapshot_insert_stmt = sql.insert(
            schema.GmBedrijf
        ).values(organization_snapshot_values)

        organization_snapshot_id = self.session.execute(
            organization_snapshot_insert_stmt
        ).inserted_primary_key[0]

        return organization_snapshot_id

    def _create_case(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        """Create a case.

        :param event: Casecreated event
        :type event: event
        """
        changes = self._format_changes(event)
        if changes.get("contact_information"):
            # Update contact data for requestor of case
            requestor_uuid = changes["requestor"]["uuid"]
            requestor_type = changes["requestor"]["type"]
            contact_information = changes["contact_information"]
            self._update_contact_info_for_requestor(
                requestor_uuid, requestor_type, contact_information
            )

        # Insert object_data for case in ObjectData table.
        case_type_uuid = changes["case_type_version"]["case_type_uuid"]
        object_data_insert_stmt = sql.insert(schema.ObjectData).values(
            {
                "uuid": event.entity_id,
                "object_class": "case",
                "class_uuid": case_type_uuid,
            },
        )

        # Insert case in Zaak table and update id of cached case_entity.
        values = self._generate_database_values(
            uuid=event.entity_id, formatted_changes=changes
        )

        case_insert_stmt = sql.insert(schema.Case).values(values)

        try:
            self.session.execute(object_data_insert_stmt)
            case_id = self.session.execute(
                case_insert_stmt
            ).inserted_primary_key[0]
        except sqlalchemy.exc.IntegrityError as e:
            raise Conflict(
                "Specified case UUID is not unique", "case/uuid_not_unique"
            ) from e

        self.cache[event.entity_id].id = case_id

        # Insert case_meta properties for case. The case won't load on dashboard,
        # because perl-api checks for current_deadline, which is in zaak_mata table
        case_meta_insert_stmt = sql.insert(schema.CaseMeta).values(
            {
                "zaak_id": case_id,
                "last_modified": datetime.now(timezone.utc),
                "current_deadline": self._generate_case_meta_current_deadline(
                    case_id
                ),
            },
        )
        self.session.execute(case_meta_insert_stmt)

        # update ZaakBetrokkene table for requestor with case_id
        self.session.execute(
            sql.update(schema.ZaakBetrokkenen)
            .where(schema.ZaakBetrokkenen.id == values["aanvrager"])
            .values({"zaak_id": case_id})
            .execution_options(synchronize_session=False)
        )

        # update ZaakBetrokkene table for assignee with case_id
        if changes.get("assignee"):
            self.session.execute(
                sql.update(schema.ZaakBetrokkenen)
                .where(schema.ZaakBetrokkenen.id == values["behandelaar"])
                .values({"zaak_id": case_id})
                .execution_options(synchronize_session=False)
            )

        case_type_version_uuid = changes["case_type_version"]["uuid"]
        requestor_uuid = changes["requestor"]["uuid"]

        # upadte Object data for the case with case_id
        self.session.execute(
            sql.update(schema.ObjectData)
            .where(schema.ObjectData.uuid == event.entity_id)
            .values({"object_id": case_id})
            .execution_options(synchronize_session=False)
        )

        custom_fields = changes.get("custom_fields", None)
        if custom_fields:
            self._insert_custom_fields(
                case_uuid=event.entity_id,
                case_type_version_uuid=case_type_version_uuid,
                custom_fields=custom_fields,
            )

        self._insert_case_actions(
            case_uuid=event.entity_id,
            case_type_version_uuid=case_type_version_uuid,
        )

        self._insert_checklist_items(
            case_uuid=event.entity_id,
            case_type_version_uuid=case_type_version_uuid,
        )

    def _generate_database_values(self, uuid, formatted_changes):
        """Generate databse values for case creation/update.

        :param event: event
        :type event: event
        :return: values for database update or insert
        :rtype: dict
        """

        case_mapping = {
            "id": "id",
            "uuid": "uuid",
            "case_type_uuid": None,
            "case_type": None,
            "case_type_version": "case_type_version",
            "role": "route_role",
            "department": "route_ou",
            "status": "status",
            "requestor": "aanvrager",
            "request_trigger": "aanvraag_trigger",
            "contact_channel": "contactkanaal",
            "created_date": "created",
            "registration_date": "registratiedatum",
            "target_completion_date": "streefafhandeldatum",
            "milestone": "milestone",
            "subject": "onderwerp",
            "confidentiality": "confidentiality",
            "payment_amount": "payment_amount",
            "subject_extern": "onderwerp_extern",
            "last_modified_date": "last_modified",
            "custom_fields": None,
            "contact_information": None,
            "related_contacts": None,
        }

        values = {
            "uuid": uuid,
            "prefix": self._get_case_number_prefix_from_config(),
        }
        for key in formatted_changes:
            db_fieldname = case_mapping[key]

            if db_fieldname == "aanvrager":
                (
                    values["aanvrager"],
                    values["aanvrager_gm_id"],
                ) = self._update_requestor(
                    case_id=None, requestor=formatted_changes[key]
                )
            elif db_fieldname == "route_ou":
                values[db_fieldname] = (
                    sql.select(schema.Group.id)
                    .where(
                        schema.Group.uuid
                        == formatted_changes[key]["entity_id"]
                    )
                    .scalar_subquery()
                )
            elif db_fieldname == "route_role":
                values[db_fieldname] = (
                    sql.select(schema.Role.id)
                    .where(
                        schema.Role.uuid == formatted_changes[key]["entity_id"]
                    )
                    .scalar_subquery()
                )
            elif db_fieldname == "case_type_version":
                values["zaaktype_id"] = (
                    sql.select(schema.ZaaktypeNode.zaaktype_id)
                    .where(
                        schema.ZaaktypeNode.uuid
                        == formatted_changes[key]["uuid"]
                    )
                    .scalar_subquery()
                )
                values["zaaktype_node_id"] = (
                    sql.select(schema.ZaaktypeNode.id)
                    .where(
                        schema.ZaaktypeNode.uuid
                        == formatted_changes[key]["uuid"]
                    )
                    .scalar_subquery()
                )
            elif db_fieldname is None:
                pass
            else:
                values[db_fieldname] = formatted_changes[key]
        return values

    def _generate_entity_from_person(
        self,
        person,
        magic_string_prefix: str | None = None,
        role: str | None = None,
    ) -> CaseContactPerson:
        """Generate person entity"""

        name = person["naamgebruik"]
        initials = None

        if person["voornamen"]:
            name = person["voornamen"] + " " + name
            initials = person["voornamen"][0] + "."

        if person["geslachtsaanduiding"] == "M":
            gender = "man"
            salutation = "meneer"
            salutation1 = "heer"
            salutation2 = "de heer"
        elif person["geslachtsaanduiding"] == "V":
            gender = "vrouw"
            salutation = "mevrouw"
            salutation1 = "heer"
            salutation2 = "de heer"
        else:
            gender = ""
            salutation = ""
            salutation1 = ""
            salutation2 = ""

        house_number = None
        if person["address"]:
            address = person["address"]
            house_number = (
                (
                    str(address["huisnummer"])
                    + " - "
                    + str(address["huisnummertoevoeging"])
                )
                if address["huisnummertoevoeging"]
                else str(address["huisnummer"])
            )

        unique_name = "betrokkene-natuurlijk_persoon-" + str(person["id"])

        investigation = False
        for tmp in ["persoon", "huwelijk", "overlijden", "verblijfplaats"]:
            tmp = "onderzoek_" + tmp
            if tmp in person and person[tmp] is True:
                investigation = True

        has_briefadres = True
        if person["address"]["functie_adres"] == "W":
            has_briefadres = False

        person_entity = CaseContactPerson(
            entity_type="person",
            entity_id=person["uuid"],
            type="person",
            magic_string_prefix=magic_string_prefix,
            id=person["id"],
            uuid=person["uuid"],
            requestor_type="Natuurlijk persoon",
            name=name,
            surname=person["geslachtsnaam"],
            surname_prefix=person["voorvoegsel"],
            family_name=person["geslachtsnaam"],
            full_name=name,
            first_names=person["voornamen"],
            initials=initials,
            naamgebruik=person["naamgebruik"],
            bsn=person["burgerservicenummer"],
            noble_title=person["adellijke_titel"],
            gender=gender,
            date_of_birth=person["geboortedatum"],
            date_of_divorce=person["datum_huwelijk_ontbinding"],
            place_of_birth=person["geboorteplaats"],
            country_of_birth=person["geboorteland"],
            salutation=salutation,
            salutation1=salutation1,
            salutation2=salutation2,
            unique_name=unique_name,
            subject_type="natuurlijk_persoon",
            status="Actief" if person["active"] else "Inactief",
            place_of_residence=person["address"].get("woonplaat", None),
            residence_place_of_residence=person["address"].get(
                "woonplaat", None
            ),
            country_of_residence=get_country_name_from_landcode(
                person["landcode"]
            ),
            street=person["address"].get("straatnaam", None),
            residence_street=person["address"].get("straatnaam", None),
            zipcode=person["address"].get("postcode", None),
            residence_zipcode=person["address"].get("postcode", None),
            house_number=house_number,
            residence_house_number=house_number,
            mobile_number=person["contact_data"].get("mobiel", None),
            phone_number=person["contact_data"].get("telefoonnummer", None),
            email=person["contact_data"].get("email", None),
            a_number=person["a_nummer"],
            is_secret=is_person_secret(person["indicatie_geheim"]),
            foreign_residence_address_line1=person["address"].get(
                "adres_buitenland1", None
            ),
            foreign_residence_address_line2=person["address"].get(
                "adres_buitenland2", None
            ),
            foreign_residence_address_line3=person["address"].get(
                "adres_buitenland1", None
            ),
            investigation=investigation,
            date_of_marriage=person["datum_huwelijk"],
            date_of_death=person["datum_overlijden"],
            has_briefadres=has_briefadres,
            role=role,
        )
        return person_entity

    def _generate_entity_from_employee(
        self,
        subject,
        magic_string_prefix: str | None = None,
        role: str | None = None,
    ) -> CaseContactEmployee:
        """Generate employee entity."""
        department = None

        if subject["group_ids"]:
            department_query = sql.select(
                schema.Group.name.label("department")
            ).where(schema.Group.id == subject["group_ids"][0])
            department = self.session.execute(department_query).fetchone()[0]
        (zipcode, streetname, house_number) = (None, None, None)
        if subject["employee_address"] is not None:
            for each in subject["employee_address"]:
                if each["parameter"] == "customer_info_postcode":
                    zipcode = each["value"]
                elif each["parameter"] == "customer_info_straatnaam":
                    streetname = each["value"]
                elif each["parameter"] == "customer_info_huisnummer":
                    house_number = str(each["value"])

        unique_name = "betrokkene-medewerker-" + str(subject["id"])
        properties = json.loads(subject["properties"])
        settings = json.loads(subject["settings"])

        employee_entity = CaseContactEmployee(
            entity_id=subject["uuid"],
            magic_string_prefix=magic_string_prefix,
            id=subject["id"],
            uuid=subject["uuid"],
            type="employee",
            name=properties.get("displayname", None),
            surname=properties.get("sn", None),
            family_name=properties.get("sn", None),
            full_name=properties.get("displayname", None),
            first_names=properties.get("givenname", None),
            initials=properties.get("initials", None),
            last_name=properties.get("sn", None),
            title=properties.get("title", None),
            unique_name=unique_name,
            subject_type="medewerker",
            status="Actief",
            street=streetname,
            residence_street=streetname,
            zipcode=zipcode,
            residence_zipcode=zipcode,
            house_number=house_number,
            residence_house_number=house_number,
            phone_number=properties.get("telephonenumber", None),
            email=properties.get("mail", None),
            department=department,
            correspondence_zipcode=zipcode,
            correspondence_street=streetname,
            properties=properties,
            settings=settings,
            role=role,
        )
        return employee_entity

    def _generate_entity_from_organization(
        self,
        organization: dict,
        magic_string_prefix: str | None = None,
        role: str | None = None,
    ) -> CaseContactOrganization:
        """Generate organization entity."""
        correspondence_house_number = str(
            organization["correspondence_info"]["correspondentie_huisnummer"]
        )

        if organization["correspondence_info"]["correspondentie_huisletter"]:
            correspondence_house_number = (
                correspondence_house_number
                + " "
                + organization["correspondence_info"][
                    "correspondentie_huisletter"
                ]
            )
        if organization["correspondence_info"][
            "correspondentie_huisnummertoevoeging"
        ]:
            correspondence_house_number = (
                correspondence_house_number
                + " - "
                + organization["correspondence_info"][
                    "correspondentie_huisnummertoevoeging"
                ]
            )

        residence_house_number = str(organization["vestiging_huisnummer"])

        if organization["vestiging_huisletter"]:
            residence_house_number = (
                residence_house_number
                + " "
                + organization["vestiging_huisletter"]
            )
        if organization["vestiging_huisnummertoevoeging"]:
            residence_house_number = (
                residence_house_number
                + " - "
                + organization["vestiging_huisnummertoevoeging"]
            )

        has_briefadres = False
        for tmp in [
            "woonplaats",
            "straatnaam",
            "postcode",
            "adres_buitenland1",
            "adres_buitenland2",
            "adres_buitenland3",
        ]:
            tmp = "correspondentie_" + tmp

            if (
                organization["correspondence_info"][tmp] is not None
                and len(organization["correspondence_info"][tmp]) > 0
            ):
                has_briefadres = True
        unique_name = "betrokkene-bedrijf-" + str(organization["id"])
        organization_entity = CaseContactOrganization(
            entity_id=organization["uuid"],
            entity_type="organization",
            type="organization",
            magic_string_prefix=magic_string_prefix,
            id=organization["id"],
            uuid=organization["uuid"],
            requestor_type="Bedrijf",
            name=organization["handelsnaam"],
            unique_name=unique_name,
            subject_type="bedrijf",
            status="Actief",
            place_of_residence=organization["correspondence_info"][
                "correspondentie_woonplaats"
            ],
            residence_place_of_residence=organization["correspondence_info"][
                "correspondentie_woonplaats"
            ],
            country_of_residence=get_country_name_from_landcode(
                organization["vestiging_landcode"]
            ),
            street=organization["vestiging_straatnaam"],
            residence_street=organization["vestiging_straatnaam"],
            zipcode=organization["vestiging_postcode"],
            residence_zipcode=organization["vestiging_postcode"],
            house_number=residence_house_number,
            residence_house_number=residence_house_number,
            mobile_number=getattr(
                organization["contact_data"], "mobiel", None
            ),
            phone_number=getattr(
                organization["contact_data"], "telefoonnummer", None
            ),
            email=getattr(organization["contact_data"], "email", None),
            foreign_residence_address_line1=organization[
                "correspondence_info"
            ]["correspondentie_adres_buitenland1"],
            foreign_residence_address_line2=organization[
                "correspondence_info"
            ]["correspondentie_adres_buitenland2"],
            foreign_residence_address_line3=organization[
                "correspondence_info"
            ]["correspondentie_adres_buitenland3"],
            correspondence_zipcode=organization["correspondence_info"][
                "correspondentie_postcode"
            ],
            correspondence_place_of_residence=organization[
                "correspondence_info"
            ]["correspondentie_woonplaats"],
            correspondence_street=organization["correspondence_info"][
                "correspondentie_straatnaam"
            ],
            correspondence_house_number=correspondence_house_number,
            coc=organization["dossiernummer"],
            login=organization["dossiernummer"],
            type_of_business_entity=organization["rechtsvorm"],
            establishment_number=organization["vestigingsnummer"],
            trade_name=organization["handelsnaam"],
            has_briefadres=has_briefadres,
            role=role,
        )
        return organization_entity

    def _insert_custom_fields(
        self, case_uuid, case_type_version_uuid, custom_fields
    ):
        """Insert custom fields in case property and zaak_kenmerk tables.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param case_type_version_uuid: UUID of case_type_version
        :type case_type_version_uuid: UUID
        :param custom_fields: custom_fields for case
        :type custom_fields: dict
        """
        case_attribute_insert_values = []

        for (
            custom_field_magic_string,
            custom_field_dict,
        ) in custom_fields.items():
            custom_field_definition = (
                self._get_custom_field_definiton_from_case_type(
                    case_type_version_uuid, custom_field_magic_string
                )
            )
            custom_field_db_values = self._get_custom_field_database_value(
                custom_field_definition,
                custom_field_dict["value"],
            )

            if custom_field_db_values:
                case_attribute_insert_value = (
                    self._generate_case_attribute_insert_value(
                        case_uuid,
                        custom_field_definition,
                        custom_field_db_values,
                    )
                )
                case_attribute_insert_values.append(
                    case_attribute_insert_value
                )

        if case_attribute_insert_values:
            self.session.execute(
                schema.ZaakKenmerk.__table__.insert().values(
                    case_attribute_insert_values
                )
            )

    def _get_custom_field_definiton_from_case_type(
        self, case_type_version_uuid, custom_field_magic_string
    ):
        """Get custom field definition from case_type.

        :param case_type_version_uuid: UUID fo case_type
        :type case_type_version_uuid: UUID
        :param custom_field_magic_string: magic string for custom field
        :type custom_field_magic_string: str
        :return: custom_field which matches the magic_string
        :rtype: dict
        """
        case_type_version = self.cache[str(case_type_version_uuid)]
        for case_type_custom_field in case_type_version.phases[
            0
        ].custom_fields:
            if (
                case_type_custom_field.field_magic_string
                == custom_field_magic_string
            ):
                return case_type_custom_field

    def _generate_case_attribute_insert_value(
        self, case_uuid, custom_field_definition, custom_field_values
    ):
        """Generate insert value for custom_field in ZaakKenmerk table.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param custom_field_definition: custom_field definition from case_type
        :type custom_field_definition: dict
        :param custom_field_values: list of values of custom_field
        :type custom_field_values: list
        :return: Insert value for custom_field in ZaakKenmerk table.
        :rtype: dict
        """
        return {
            "zaak_id": sql.select(schema.Case.id).where(
                schema.Case.uuid == case_uuid
            ),
            "bibliotheek_kenmerken_id": sql.select(
                schema.BibliotheekKenmerk.id
            ).where(
                schema.BibliotheekKenmerk.magic_string
                == custom_field_definition.field_magic_string
            ),
            "value": custom_field_values,
            "magic_string": custom_field_definition.field_magic_string,
        }

    def _insert_case_actions(
        self, case_uuid: UUID, case_type_version_uuid: UUID
    ):
        """Insert case_actions for a case.

        :param case_uuid: UUID of the case
        :type case_uuid: UUID
        :param case_type_version_uuid: UUID of the case_type
        :type case_type_version_uuid: UUID
        """
        case_row = self.cache[case_uuid]
        case_type_row = self.cache[case_type_version_uuid]
        case_actions = []
        for phase in case_type_row.phases:
            for sub_case in getattr(phase, "cases", []):
                case_actions.append(
                    self._generate_case_action_value_from_sub_case(
                        case_row.id,
                        case_type_row,
                        phase.milestone,
                        sub_case,
                    )
                )
            for document in getattr(phase, "documents", []):
                case_actions.append(
                    self._generate_case_action_for_document(
                        case_row.id,
                        case_type_row,
                        phase.milestone,
                        document,
                    )
                )
            for email in getattr(phase, "emails", []):
                case_actions.append(
                    self._generate_case_action_for_email(
                        case_row.id,
                        case_type_row,
                        phase.milestone,
                        email,
                    )
                )
            for subject in getattr(phase, "subjects", []):
                case_actions.append(
                    self._generate_case_action_for_subject(
                        case_row.id,
                        case_type_row,
                        phase.milestone,
                        subject,
                    )
                )
            if phase.allocation and phase.phase != "Afhandelen":
                case_actions.append(
                    self._generate_case_action_for_allocation(
                        case_row.id,
                        case_type_row,
                        phase.milestone,
                        phase.allocation,
                    )
                )
        if case_actions:
            self.session.execute(
                schema.CaseAction.__table__.insert().values(case_actions)
            )

    def _generate_case_action_value_from_sub_case(
        self, case_id, case_type_row, milestone, sub_case
    ):
        """Generate case_action value from sub case.

        :param case_id: case id
        :type case_id: int
        :param case_type: parent case_type row
        :type case_type: CaseTypeVersionEntity
        :param milestone: milestone
        :type milestone: int
        :param related_case: related case
        :type related_case: dict
        :return: case_action value for related_case
        :rtype: dict
        """
        sub_case_uuid = sub_case.related_casetype_element

        return {
            "case_id": case_id,
            "casetype_status_id": case_action_status_query(
                case_type_row.uuid, milestone
            ),
            "type": "case",
            "label": case_action_label_query(sub_case_uuid)
            if sub_case_uuid
            else case_type_row.name,
            "automatic": sub_case.start_on_transition,
            "data": case_action_data_from_sub_case_query(
                case_type_row.uuid, sub_case_uuid, milestone
            ),
        }

    def _generate_case_action_for_document(
        self, case_id, case_type_row, milestone, document
    ):
        return {
            "case_id": case_id,
            "casetype_status_id": case_action_status_query(
                case_type_row.uuid, milestone
            ),
            "type": "template",
            "label": document.label,
            "automatic": document.automatic,
            "data": document.data.dict(),
        }

    def _generate_case_action_for_email(
        self, case_id, case_type_row, milestone, email
    ):
        return {
            "case_id": case_id,
            "casetype_status_id": case_action_status_query(
                case_type_row.uuid, milestone
            ),
            "type": "email",
            "label": email.label,
            "automatic": email.automatic,
            "data": email.data.dict(),
        }

    def _generate_case_action_for_subject(
        self, case_id, case_type_row, milestone, subject
    ):
        subject_data = subject.data.dict()
        subject_data["uuid"] = str(subject_data["uuid"])

        return {
            "case_id": case_id,
            "casetype_status_id": case_action_status_query(
                case_type_row.uuid, milestone
            ),
            "type": "subject",
            "label": subject.data.naam,
            "automatic": 0,
            "data": subject_data,
        }

    def _get_custom_field_database_value(
        self, custom_field_definition, custom_field_values
    ):
        """Get database value for custom fields to save in case_property table and zaak_kenmerk table.

        :param custom_field_definition: custom_field definition from case_type
        :type custom_field_definition: dict
        :param custom_field_values: custom filed values
        :type custom_field_values: list
        """
        if (
            custom_field_definition.field_type
            in ["text", "textarea", "numeric", "option", "select"]
            and len(custom_field_values) == 0
            and custom_field_definition.default_value
        ):
            return [custom_field_definition.default_value]

        if custom_field_definition.field_type == "geolatlon":
            coordinates_list = []
            for value in custom_field_values:
                coordinates = [
                    str(coordinate)
                    for coordinate in value["features"][0]["geometry"][
                        "coordinates"
                    ]
                ]
                coordinates.reverse()
                coordinates_list.append(",".join(coordinates))
            return coordinates_list

        if (
            custom_field_definition.field_type == "checkbox"
            and custom_field_values
        ):
            return custom_field_values[0]
        return custom_field_values

    def _insert_checklist_items(
        self, case_uuid: UUID, case_type_version_uuid: UUID
    ):
        """Insert checklist items from case type to checklist and checklist_item table.

        :param case_uuid: UUID of the case
        :type case_uuid: UUID
        param case_type_version_uuid: UUID of the case_type
        :type case_uuid: UUID
        """
        case_type_row = self.cache[case_type_version_uuid]
        checklist_items = []

        for phase in case_type_row.phases:
            if phase.checklist_items:
                checklist_id = self._insert_checklist(
                    case_uuid, phase.milestone
                )
                for sequence, checklist_item in enumerate(
                    phase.checklist_items, start=1
                ):
                    value = {
                        "checklist_id": checklist_id,
                        "label": checklist_item,
                        "sequence": sequence,
                        "state": False,
                        "user_defined": False,
                    }
                    checklist_items.append(value)

        if checklist_items:
            self.session.execute(
                schema.ChecklistItem.__table__.insert().values(checklist_items)
            )

    def _insert_checklist(self, case_uuid: UUID, status_milestone: int):
        """Insert checklist in checklist tabe and return inserted checklist_id.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param status_milestone: milestone of case
        :type status_milestone: int
        :return: id of inserted checklist
        :rtype: int
        """
        insert_statement = sql.insert(schema.Checklist).values(
            {
                "case_id": sql.select(schema.Case.id)
                .where(schema.Case.uuid == case_uuid)
                .scalar_subquery(),
                "case_milestone": status_milestone,
            },
        )
        return self.session.execute(insert_statement).inserted_primary_key[0]

    def _update_contact_info_for_requestor(
        self,
        requestor_uuid: UUID,
        requestor_type: str,
        contact_information: dict,
    ):
        """Update contact information for case requestor.

        :param requestor_uuid: uuid of case requestor.
        :type requestor_uuid: UUID
        :param requestor_type: type of requestor.
        :type requestor_type: str
        :param contact_information: new contact information for case requestor.
        :type contact_information: dict
        """
        value = {}
        for key in contact_information.keys():
            if key == "mobile_number":
                value["mobiel"] = contact_information["mobile_number"]
            elif key == "phone_number":
                value["telefoonnummer"] = contact_information["phone_number"]
            elif key == "email":
                value["email"] = contact_information["email"]

        if requestor_type == "person":
            self._update_contact_info_for_person(requestor_uuid, value)
        elif requestor_type == "organization":
            self._update_contact_info_for_organization(requestor_uuid, value)
        else:
            pass

    def _update_contact_info_for_person(
        self, person_uuid: UUID, contact_info_value: dict
    ):
        """Update the contact information for person.

        :param person_uuid: UUID of person
        :type person_uuid: UUID
        :param contact_info_value: new value of contact information
        :type contact_info_value: dict
        :raises Conflict: If person with uuid not found
        """

        query_result = self.session.execute(
            sql.select(
                schema.NatuurlijkPersoon.id.label("person_id"),
                schema.ContactData.id.label("contact_data_id"),
            )
            .select_from(
                sql.join(
                    schema.NatuurlijkPersoon,
                    schema.ContactData,
                    sql.and_(
                        schema.ContactData.gegevens_magazijn_id
                        == schema.NatuurlijkPersoon.id,
                        schema.ContactData.betrokkene_type
                        == BETROKKENE_TYPE_NUMBER_FOR_PERSON,
                    ),
                    isouter=True,
                )
            )
            .where(schema.NatuurlijkPersoon.uuid == person_uuid)
        ).fetchone()

        if not query_result:
            raise Conflict(
                f"Person with uuid '{person_uuid}' not found",
                "person_not_found",
            )
        if query_result.contact_data_id:
            self.session.execute(
                sql.update(schema.ContactData)
                .where(
                    sql.and_(
                        schema.ContactData.id == query_result.contact_data_id
                    )
                )
                .values(contact_info_value)
                .execution_options(synchronize_session=False)
            )
        else:
            contact_info_value.update(
                {
                    "gegevens_magazijn_id": query_result.person_id,
                    "betrokkene_type": BETROKKENE_TYPE_NUMBER_FOR_PERSON,
                }
            )
            self.session.execute(
                sql.insert(schema.ContactData).values(contact_info_value)
            )

    def _update_contact_info_for_organization(
        self, organiztaion_uuid: UUID, contact_info_value: dict
    ):
        """Update contact information for organization.

        :param organiztaion_uuid: uuid of organization
        :type organiztaion_uuid: UUID
        :param contact_info_value: new value of contact information for organization
        :type contact_info_value: dict
        :raises Conflict: Organozatin with uuid not found.
        """
        query_result = self.session.execute(
            sql.select(
                schema.Bedrijf.id.label("organization_id"),
                schema.ContactData.id.label("contact_data_id"),
            )
            .select_from(
                sql.join(
                    schema.Bedrijf,
                    schema.ContactData,
                    sql.and_(
                        schema.ContactData.gegevens_magazijn_id
                        == schema.Bedrijf.id,
                        schema.ContactData.betrokkene_type
                        == BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
                    ),
                    isouter=True,
                )
            )
            .where(schema.Bedrijf.uuid == organiztaion_uuid)
        ).fetchone()

        if not query_result:
            raise Conflict(
                f"Organization with uuid '{organiztaion_uuid}' not found",
                "organization_not_found",
            )
        if query_result.contact_data_id:
            self.session.execute(
                sql.update(schema.ContactData)
                .where(schema.ContactData.id == query_result.contact_data_id)
                .values(contact_info_value)
                .execution_options(synchronize_session=False)
            )
        else:
            contact_info_value.update(
                {
                    "gegevens_magazijn_id": query_result.organization_id,
                    "betrokkene_type": BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
                }
            )
            self.session.execute(
                sql.insert(schema.ContactData).values(contact_info_value)
            )

    def _generate_case_action_for_allocation(
        self, case_id, case_type_row, milestone, allocation
    ):
        """Genarate database value for case_action of type allocation.

        :param case_id: id of the case
        :type case_id: int
        :param case_type_row: casetype_row
        :type case_type_row: csetypeVersionEntity
        :param milestone: milestone for the case.
        :type milestone: int
        :param allocation: allocation
        :type allocation: dict
        :return: databse value to be stored in case_action table
        :rtype: dict
        """
        query_result = self.session.execute(
            sql.select(
                schema.Group.id.label("department_id"),
                schema.Role.id.label("role_id"),
            ).where(
                sql.and_(
                    schema.Group.uuid == allocation.department.uuid,
                    schema.Role.uuid == allocation.role.uuid,
                )
            )
        ).fetchone()
        department_id = query_result.department_id
        role_id = query_result.role_id

        return {
            "case_id": case_id,
            "casetype_status_id": case_action_status_query(
                case_type_row.uuid, milestone
            ),
            "type": "allocation",
            "label": f"{department_id}, {role_id}",
            "automatic": bool(allocation.role_set),
            "data": {
                "original_ou_id": department_id,
                "ou_id": department_id,
                "original_role_id": role_id,
                "role_id": role_id,
                "description": "Toewijzing",
                "role_set": allocation.role_set,
            },
        }

    def _get_case_number_prefix_from_config(self):
        """Get case number prefix from config table.

        :return: case number prefix.
        :rtype: str
        """
        qry_stmt = sql.select(schema.Config.value).where(
            schema.Config.parameter == "case_number_prefix"
        )
        query_result = self.session.execute(qry_stmt).fetchone()
        if query_result:
            return query_result[0]
        return ""

    def _update_assignee(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo = None,
        dry_run: bool = False,
    ):
        """Update assignee for case.

        :param changes: CaseAssigneeChanged changes
        :type changes: dict
        """
        changes = self._format_changes(event)
        assignee = changes.get("assignee")

        if assignee:
            try:
                subject_uuid = assignee["entity_id"]
            except KeyError:
                subject_uuid = assignee["uuid"]

            case_uuid = event.entity_id
            employee_query = _get_employee_info(subject_uuid)

            subject_row = self.session.execute(employee_query).fetchone()

            if subject_row is None:
                raise NotFound(
                    f"Employee with uuid '{subject_uuid}' not found.",
                    "employee/not_found",
                )
            subject_row = subject_row[0]
            case_id = self.session.execute(
                sql.select(schema.Case.id).where(schema.Case.uuid == case_uuid)
            ).fetchone()[0]

            # delete existing assignee for case from ZaakBetrokkenen table
            delete_stmt = (
                sql.update(schema.ZaakBetrokkenen)
                .where(
                    sql.and_(
                        schema.Case.uuid == case_uuid,
                        schema.ZaakBetrokkenen.id == schema.Case.behandelaar,
                    )
                )
                .values({"deleted": sql.func.now()})
                .execution_options(synchronize_session=False)
            )
            self.session.execute(delete_stmt)

            # insert new assignee row for case in ZaakBetrokkenen table
            assignee_entity_id = add_entity_from_subject(
                subject_row, case_id, "Behandelaar", self.session
            )
            # update case with new assignee and new subjects
            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        "behandelaar": assignee_entity_id,
                        "behandelaar_gm_id": subject_row["id"],
                        "onderwerp": changes.get("subject", None),
                        "onderwerp_extern": changes.get(
                            "subject_extern", None
                        ),
                        "last_modified": datetime.now(timezone.utc),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)

    def _clear_assignee(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo = None,
        dry_run: bool = False,
    ):
        """Remove assignee for the case.
        :param changes: CaseAssigneeCleared changes
        :type changes: dict
        """
        changes = self._format_changes(event)
        assignee = changes.get("assignee")

        if assignee is None:
            case_uuid = event.entity_id

            update_stmt = (
                sql.update(schema.ZaakBetrokkenen)
                .where(
                    sql.and_(
                        schema.Case.uuid == case_uuid,
                        schema.ZaakBetrokkenen.id == schema.Case.behandelaar,
                    )
                )
                .values({"deleted": datetime.now(timezone.utc)})
                .execution_options(synchronize_session=False)
            )
            self.session.execute(update_stmt)

            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        "behandelaar": None,
                        "behandelaar_gm_id": None,
                        "last_modified": datetime.now(timezone.utc),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)

    def _update_allocation(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo = None,
        dry_run: bool = False,
    ):
        """Update allocation for case.

        :param changes: CaseAllocationSet event
        :type changes: dict
        """
        changes = self._format_changes(event)
        case_uuid = event.entity_id
        if changes.get("department") or changes.get("role"):
            try:
                department_uuid = changes["department"]["entity_id"]
            except KeyError:
                department_uuid = changes["department"]["uuid"]
                self.cache[case_uuid].department = changes["department"]
            try:
                role_uuid = changes["role"]["entity_id"]
            except KeyError:
                role_uuid = changes["role"]["uuid"]

            department_id = (
                sql.select(schema.Group.id)
                .where(schema.Group.uuid == department_uuid)
                .scalar_subquery()
            )
            role_id = (
                sql.select(schema.Role.id)
                .where(schema.Role.uuid == role_uuid)
                .scalar_subquery()
            )

            # update case with new alloaction
            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        "route_ou": department_id,
                        "route_role": role_id,
                        "onderwerp": changes.get("subject", None),
                        "onderwerp_extern": changes.get(
                            "subject_extern", None
                        ),
                        "last_modified": datetime.now(timezone.utc),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)

    def _enqueue_assignee_email(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo = None,
        dry_run: bool = False,
    ):
        # This code will disappear once the creation of email
        # has been moved to Python [20191122]
        changes = self._format_changes(event)
        queue_id = changes["assignee_email_queue_id"]
        values = {
            "id": queue_id,
            "object_id": event.entity_id,
            "status": "pending",
            "type": "send_case_assignee_email",
            "label": "Send email to case assignee",
            "data": {"case_uuid": str(event.entity_id)},
            "date_created": datetime.now(timezone.utc),
            "date_started": None,
            "date_finished": None,
            "parent_id": None,
            "priority": 1000,
            "metadata": self._get_metadata_for_queue_item(event.user_uuid),
        }
        insert_stmt = sql.insert(schema.Queue)
        self.session.execute(insert_stmt, [values])

    def _send_assignee_email(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        # This code will disappear once the creation of email
        # has been moved to Python [20191122]
        changes = self._format_changes(event)
        self._run_queue_items(changes)

    def _update_coordinator(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo = None,
        dry_run: bool = False,
    ):
        """Update coordinator for case.

        :param changes: CaseCoordinatorSet event
        :type changes: dict
        """
        changes = self._format_changes(event)
        coordinator = changes.get("coordinator")

        if coordinator:
            try:
                subject_uuid = coordinator["entity_id"]
            except KeyError:
                subject_uuid = coordinator["uuid"]
            case_uuid = event.entity_id
            employee_query = _get_employee_info(subject_uuid)

            subject_row = self.session.execute(employee_query).fetchone()

            if subject_row is None:
                raise NotFound(
                    f"Employee with uuid '{subject_uuid}' not found.",
                    "employee/not_found",
                )
            subject_row = subject_row[0]
            case_id = self.session.execute(
                sql.select(schema.Case.id).where(schema.Case.uuid == case_uuid)
            ).fetchone()[0]

            # delete existing cooridnator for case from ZaakBetrokkenen table
            delete_stmt = (
                sql.update(schema.ZaakBetrokkenen)
                .where(
                    sql.and_(
                        schema.Case.uuid == case_uuid,
                        schema.ZaakBetrokkenen.id == schema.Case.coordinator,
                    )
                )
                .values({"deleted": sql.func.now()})
                .execution_options(synchronize_session=False)
            )
            self.session.execute(delete_stmt)

            # insert new coordinator row for case in ZaakBetrokkenen table
            coordinator_entity_id = add_entity_from_subject(
                subject_row, case_id, "Coordinator", self.session
            )

            # update case with new coordinator
            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        "coordinator": coordinator_entity_id,
                        "coordinator_gm_id": subject_row["id"],
                        "onderwerp": changes.get("subject", None),
                        "onderwerp_extern": changes.get(
                            "subject_extern", None
                        ),
                        "last_modified": datetime.now(timezone.utc),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)

    def _update_status(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run: bool = False,
    ):
        """Update status for a case."""
        case_uuid = event.entity_id
        changes = self._format_changes(event)
        new_status = changes.get("status")
        if new_status:
            # update case with new status
            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        "status": new_status,
                        "onderwerp": changes.get("subject", None),
                        "onderwerp_extern": changes.get(
                            "subject_extern", None
                        ),
                        "last_modified": datetime.now(timezone.utc),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)
            self.cache[case_uuid].status = new_status

    def _update_payment_amount(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run: bool = False,
    ):
        """Update payment_amount for a case."""
        case_uuid = event.entity_id
        changes = self._format_changes(event)
        payment_amount = changes["payment"]["amount"]

        if payment_amount:
            # update case payment_amount
            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        "payment_amount": payment_amount,
                        "last_modified": datetime.now(timezone.utc),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)

    def _update_result(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run: bool = False,
    ):
        """Update result for a case.

        :param event: CaseResultSet event
        :type event: cqrs.event
        """
        case_uuid = event.entity_id
        changes = self._format_changes(event)
        new_result = changes.get("result")
        if new_result:
            # update case with new result
            case_update_stmt = (
                sql.update(schema.Case)
                .where(schema.Case.uuid == case_uuid)
                .values(
                    {
                        schema.Case.resultaat: new_result["result"],
                        schema.Case.resultaat_id: sql.select(
                            schema.ZaaktypeResultaten.id
                        )
                        .where(
                            sql.and_(
                                schema.ZaaktypeResultaten.uuid
                                == new_result["result_uuid"],
                            )
                        )
                        .scalar_subquery(),
                    }
                )
                .execution_options(synchronize_session=False)
            )
            self.session.execute(case_update_stmt)

    def _translate_meta_for_case_entity(self, meta_result):
        meta_mapping = {
            "afhandeling": "completion",
            "opschorten": "suspension_reason",
            "id": "id",
        }

        meta_data = {}
        if meta_result is not None:
            for key, objkey in meta_mapping.items():
                meta_data[key] = meta_result.get(objkey)

            meta_data["stalled_since_date"] = None
            if meta_result["stalled_since"] is not None:
                meta_data["stalled_since_date"] = format_date(
                    meta_result["stalled_since"]
                ).date()
        return meta_data

    def _translate_phase_for_case_entity(self, label, milestone_label):
        result = {}
        if label or milestone_label:
            result["label"] = label
            result["milestone_label"] = milestone_label

        return result

    def _translate_payment_for_case_entity(self, amount, status):
        result = {}
        if amount or status:
            result["amount"] = amount
            result["status"] = status

        return result

    def _get_case_type_version_uuid(self, case_id):
        query = sql.select(schema.ZaaktypeNode.uuid)

        query = query.where(
            sql.and_(
                schema.ZaaktypeNode.id == schema.Case.zaaktype_node_id,
                schema.Case.id == case_id,
            )
        )

        result = self.session.execute(query).fetchone()[0]
        if not result:
            raise NotFound(
                "Case type version not found.",
                "case_type/version_not_found",
            )
        return result

    def _get_case_type_version(self, case_id):
        case_type_version_uuid = self._get_case_type_version_uuid(case_id)
        case_type_entity = self.get_case_type_version(case_type_version_uuid)
        case_type_entity.entity_type = "case_type_version"
        return case_type_entity

    def get_case_type_version(self, case_type_version_uuid):
        case_type_version_repo = self._get_repo(case_management, "case_type")
        case_type_entity = (
            case_type_version_repo.find_case_type_version_by_uuid(
                case_type_version_uuid
            )
        )
        return case_type_entity

    def _format_date(self, date):
        if date is not None:
            date = dateutil.parser.isoparse(str(date))
            date = date.replace(tzinfo=timezone.utc).astimezone(
                tz=pytz.timezone("Europe/Amsterdam")
            )
        return date

    def _update(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run: bool = False,
    ):
        change = event.format_changes()
        case_row = self.cache[str(event.entity_id)]
        update_case_values = {"last_modified": sql.func.now()}
        update_case_meta_values = {}

        for item in change:
            if item in self.case_mapping:
                db_fieldname = self.case_mapping[item]
                update_case_values[db_fieldname] = change[item]
            elif item in self.case_meta_mapping:
                db_fieldname = self.case_meta_mapping[item]
                update_case_meta_values[db_fieldname] = change[item]

        self._update_case_values(
            values=update_case_values, case_uuid=case_row.uuid
        )
        self._update_case_meta_values(
            values=update_case_meta_values, case_id=case_row.id
        )

    def _update_case_values(self, values, case_uuid):
        if len(values) > 1:
            update_case_stmt = (
                sql.update(schema.Case)
                .where(
                    schema.Case.uuid == case_uuid,
                )
                .values(**values)
                .execution_options(synchronize_session=False)
            )
            self.session.execute(update_case_stmt)

    def _update_case_meta_values(self, values, case_id):
        if len(values) > 0:
            update_case_meta_stmt = (
                sql.update(schema.CaseMeta)
                .where(schema.CaseMeta.zaak_id == case_id)
                .values(**values)
                .execution_options(synchronize_session=False)
            )
            self.session.execute(update_case_meta_stmt)

    # This functions creates a dict for the values that can be stored in the
    # current_timeline, based on the case_id.
    def _generate_case_meta_current_deadline(self, case_id: int) -> dict:
        qry = (
            sql.select(
                sql.func.json_build_object(
                    "start",
                    datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "days",
                    schema.ZaaktypeStatus.termijn,
                    "current",
                    0,
                    "phase_id",
                    schema.ZaaktypeStatus.id,
                    "phase_no",
                    schema.ZaaktypeStatus.status,
                )
            )
            .select_from(
                sql.join(
                    schema.Case,
                    schema.ZaaktypeStatus,
                    sql.and_(
                        schema.Case.zaaktype_node_id
                        == schema.ZaaktypeStatus.zaaktype_node_id,
                        schema.Case.milestone + 1
                        == schema.ZaaktypeStatus.status,
                    ),
                )
            )
            .where(schema.Case.id == case_id)
        )
        return self.session.execute(qry).fetchone()[0]

    def _get_changed_custom_fields(self, event: minty.cqrs.Event):
        changed_fields = {}
        custom_field_changes = next(
            change
            for change in event.changes
            if change["key"] == "custom_fields"
        )
        for field, value in custom_field_changes["new_value"].items():
            if (
                not custom_field_changes["old_value"].get(field)
                or custom_field_changes["old_value"][field] != value
            ):
                changed_fields[field] = value
        return changed_fields

    def _update_custom_fields(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        case_uuid = event.entity_id
        for magic_string, value in self._get_changed_custom_fields(
            event
        ).items():
            bibliotheek_kenmerk_fields = (
                self._get_bibliotheek_kenmerk_fields_by_magic_string(
                    magic_string
                )
            )
            values = {}
            if bibliotheek_kenmerk_fields.value_type == "relationship":
                values = {"value": [json.dumps(value)]}
            else:
                values["value"] = value

            self.session.execute(
                sql_pg.insert(schema.ZaakKenmerk)
                .values(
                    {
                        "zaak_id": self._get_case_number_by_uuid(case_uuid),
                        "bibliotheek_kenmerken_id": bibliotheek_kenmerk_fields.id,
                        "magic_string": magic_string,
                        **values,
                    },
                )
                .on_conflict_do_update(
                    index_elements=[
                        schema.ZaakKenmerk.zaak_id,
                        schema.ZaakKenmerk.bibliotheek_kenmerken_id,
                    ],
                    set_={**values},
                )
            )

    def _get_bibliotheek_kenmerk_fields_by_magic_string(
        self, magic_string: str
    ):
        return self.session.execute(
            sql.select(
                schema.BibliotheekKenmerk.id,
                schema.BibliotheekKenmerk.value_type,
            )
            .select_from(schema.BibliotheekKenmerk)
            .where(schema.BibliotheekKenmerk.magic_string == magic_string)
        ).fetchone()

    def _get_case_number_by_uuid(self, case_uuid: UUID):
        return self.session.execute(
            sql.select(schema.Case.id)
            .select_from(schema.Case)
            .where(schema.Case.uuid == str(case_uuid))
        ).fetchone()[0]

    def _transition_case(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        # Transiton of case does nothing. Logic for case_transition is in perl
        # TODO: Write case_transition logic in python
        pass

    def get_contact_person(
        self,
        uuid: UUID,
        magic_string_prefix: str | None = None,
        role: str | None = None,
    ) -> CaseContactPerson | None:
        person_query = _get_person_info(uuid)

        query_result = self.session.execute(person_query).fetchone()
        if query_result is None:
            raise NotFound(
                f"Person with uuid '{uuid}' not found.",
                "natuurlijk_persoon/not_found",
            )

        if query_result:
            return self._generate_entity_from_person(
                person=query_result[0],
                magic_string_prefix=magic_string_prefix,
                role=role,
            )

    def get_contact_employee(
        self, uuid: UUID, magic_string_prefix: str = None, role: str = None
    ) -> CaseContactEmployee | None:
        employee_query = _get_employee_info(uuid)

        query_result = self.session.execute(employee_query).fetchone()
        if query_result is None:
            raise NotFound(
                f"Employee with uuid '{uuid}' not found.",
                "employee/not_found",
            )
        if query_result:
            return self._generate_entity_from_employee(
                subject=query_result[0],
                magic_string_prefix=magic_string_prefix,
                role=role,
            )

    def get_contact_organization(
        self, uuid: UUID, magic_string_prefix: str = None, role: str = None
    ) -> CaseContactOrganization | None:
        organization_query = _get_organization_info(uuid)

        query_result = self.session.execute(organization_query).fetchone()

        if query_result is None:
            raise NotFound(
                f"Organization with uuid '{uuid}' not found.",
                "organization/not_found",
            )
        if query_result:
            return self._generate_entity_from_organization(
                organization=query_result[0],
                magic_string_prefix=magic_string_prefix,
                role=role,
            )

    def _get_case_id_pid_by_uuid(self, case_uuid: UUID, user_info, permission):
        query = sql.select(schema.Case.pid, schema.Case.id).where(
            sql.and_(
                user_allowed_cases_subquery(
                    user_info=user_info, permission=permission
                ),
                schema.Case.uuid == case_uuid,
            )
        )
        query_result = self.session.execute(query).fetchone()

        if query_result is None:
            raise NotFound(f"Case with uuid '{case_uuid}' not found.")
        return query_result

    def _set_case_parent(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = self._format_changes(event)
        case_uuid = event.entity_id
        case_id = event.entity_data["id"]
        parent_uuid = changes.get("parent_uuid")
        # get parent case

        parent_case = self._get_case_id_pid_by_uuid(
            case_uuid=UUID(parent_uuid),
            user_info=userinfo,
            permission="write",
        )
        parent_case_pid = parent_case.pid
        if parent_case_pid:
            self._case_pid_circular_dependecy_check(
                case_id=case_id, parent_id=parent_case.id
            )

        # update case pid
        self.session.execute(
            sql.update(schema.Case)
            .where(schema.Case.uuid == case_uuid)
            .values(
                {
                    "pid": parent_case.id,
                }
            )
            .execution_options(synchronize_session=False)
        )

    def _case_pid_circular_dependecy_check(self, case_id, parent_id):
        q1_alias = sql.alias(schema.Case)
        q2_alias = sql.alias(schema.Case)
        q_recursive = (
            sql.select(
                q1_alias.c.id,
                sql.cast([], sql_pg.ARRAY(sqltypes.BigInteger)).label(
                    "parents"
                ),
            )
            .where(
                sql.and_(
                    q1_alias.c.pid.is_(None),
                    q1_alias.c.status != "deleted",
                    q1_alias.c.deleted.is_(None),
                )
            )
            .cte(recursive=True)
        )
        q_recursive = q_recursive.union_all(
            sql.select(
                q2_alias.c.id,
                q_recursive.c.parents.op("||")(q2_alias.c.pid),
            )
            .where(
                sql.and_(
                    q2_alias.c.status != "deleted",
                    q2_alias.c.deleted.is_(None),
                )
            )
            .join(q_recursive, q_recursive.c.id == q2_alias.c.pid)
        )
        q3 = q_recursive.select().where(
            sql.and_(
                q_recursive.c.parents.op("@>")([case_id]),
                q_recursive.c.id == parent_id,
            )
        )
        result = self.session.execute(q3).fetchall()

        if result:
            raise Conflict(
                f"Deze zaak kan niet worden gerelateerd aan '{parent_id}'."
            )

    def get_preservation_term_info(self, code: int):
        query_result = self.session.execute(
            sql.select(
                schema.ResultPreservationTerms.unit.label(
                    "preservation_term_unit"
                ),
                schema.ResultPreservationTerms.unit_amount.label(
                    "preservation_term_unit_amount"
                ),
                schema.ResultPreservationTerms.label.label(
                    "preservation_term_label"
                ),
            ).where(schema.ResultPreservationTerms.code == code)
        ).fetchone()
        if query_result is None:
            raise NotFound(
                f"Time period information for code {code} not found",
                "time_period_info/not_found",
            )
        return query_result

    def check_allocation_rights(
        self,
        department_uuid: UUID,
        role_uuid: UUID,
        case_uuid: UUID | None,
        casetype_uuid: UUID | None,
    ) -> CheckResult:
        # Retrieve id of the department + its sub-departments
        department_ids_query = (
            sql.select(sql.func.unnest(schema.Group.path).label("id"))
            .where(schema.Group.uuid == department_uuid)
            .scalar_subquery()
        )
        department_ids_subquery = department_ids_query.subquery()

        role_id_query = (
            sql.select(schema.Role.id)
            .where(schema.Role.uuid == role_uuid)
            .scalar_subquery()
        )

        positions_query = (
            sql.select(
                sql.cast(department_ids_subquery.c.id, sqltypes.Text)
                + "|"
                + sql.cast(
                    sql.select(schema.Role.id)
                    .where(schema.Role.uuid == role_uuid)
                    .scalar_subquery(),
                    sqltypes.Text,
                )
            )
            .select_from(department_ids_subquery)
            .scalar_subquery()
        )

        if case_uuid:
            query_result = self._check_rights_case(
                case_uuid, role_id_query, department_ids_query, positions_query
            )
        else:
            query_result = self._check_rights_casetype(
                casetype_uuid, role_id_query, department_ids_query
            )

        return CheckResult.parse_obj({"check_result": bool(query_result)})

    def _check_rights_case(
        self,
        case_uuid: UUID,
        role_id_query,
        department_ids_query,
        positions_query,
    ):
        casetype_rights_query = (
            sql.select(True)
            .select_from(
                sql.join(
                    schema.ZaaktypeAuthorisation,
                    schema.Case,
                    schema.ZaaktypeAuthorisation.zaaktype_id
                    == schema.Case.zaaktype_id,
                )
            )
            .where(
                sql.and_(
                    schema.Case.uuid == case_uuid,
                    schema.ZaaktypeAuthorisation.role_id == role_id_query,
                    schema.ZaaktypeAuthorisation.ou_id.in_(
                        department_ids_query
                    ),
                    schema.ZaaktypeAuthorisation.confidential
                    == schema.Case.confidential,
                )
            )
        )

        case_rights_query = (
            sql.select(True)
            .select_from(
                sql.join(
                    schema.Case,
                    schema.CaseAuthorisation,
                    schema.Case.id == schema.CaseAuthorisation.zaak_id,
                )
            )
            .where(
                sql.and_(
                    schema.Case.uuid == case_uuid,
                    schema.CaseAuthorisation.entity_type == "position",
                    schema.CaseAuthorisation.entity_id.in_(positions_query),
                )
            )
        )

        query_result = self.session.execute(
            sql.union(
                casetype_rights_query,
                case_rights_query,
            )
        ).fetchone()

        return query_result

    def _check_rights_casetype(
        self, casetype_uuid, role_id_query, department_ids_query
    ):
        casetype_rights_query = (
            sql.select(True)
            .select_from(
                sql.join(
                    schema.ZaaktypeAuthorisation,
                    schema.Zaaktype,
                    schema.ZaaktypeAuthorisation.zaaktype_id
                    == schema.Zaaktype.id,
                )
            )
            .where(
                schema.Zaaktype.uuid == casetype_uuid,
                schema.ZaaktypeAuthorisation.role_id == role_id_query,
                schema.ZaaktypeAuthorisation.ou_id.in_(department_ids_query),
            )
        )

        query_result = self.session.execute(casetype_rights_query).fetchone()

        return query_result

    def _delete_case(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        change = event.format_changes()
        case_id = event.entity_data["id"]
        update_case_values = {"last_modified": sql.func.now()}

        # set status to deleted
        for item in change:
            if item in self.case_mapping:
                db_fieldname = self.case_mapping[item]
                update_case_values[db_fieldname] = change[item]

        update_case_stmt = (
            sql.update(schema.Case)
            .where(
                schema.Case.uuid == event.entity_id,
            )
            .values(**update_case_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_case_stmt)

        # delete case relations
        delete_case_relations = sql.delete(schema.CaseRelation).where(
            sql.or_(
                schema.CaseRelation.case_id_a == case_id,
                schema.CaseRelation.case_id_b == case_id,
            )
        )
        self.session.execute(delete_case_relations)

        # delete messages and logging
        delete_messages = sql.delete(schema.Message).where(
            schema.Message.logging_id.in_(
                sql.select(schema.Logging.id).where(
                    schema.Logging.zaak_id == case_id
                )
            )
        )
        delete_logging = sql.delete(schema.Logging).where(
            schema.Logging.zaak_id == case_id,
        )
        self.session.execute(delete_messages)
        self.session.execute(delete_logging)

        # delete custom object relation
        delete_co_relations = sql.delete(
            schema.CustomObjectRelationship
        ).where(
            schema.CustomObjectRelationship.related_case_id == case_id,
        )
        self.session.execute(delete_co_relations)

        # delete ObjectData
        object_data_query = sql.delete(schema.ObjectData).where(
            schema.ObjectData.object_class == "case",
            schema.ObjectData.object_id == case_id,
        )
        self.session.execute(object_data_query)

        #  update child_zaak.pid
        update_child_case_pid_stmt = (
            sql.update(schema.Case)
            .where(
                schema.Case.pid == case_id,
            )
            .values({"pid": None})
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_child_case_pid_stmt)

    def get_status_destruction_date_by_case_uuids(self, case_uuids: list):
        query = sql.select(
            schema.Case.uuid,
            schema.Case.status,
            schema.Case.vernietigingsdatum.label("destruction_date"),
        ).where(schema.Case.uuid.in_(case_uuids))
        return self.session.execute(query).fetchall()
