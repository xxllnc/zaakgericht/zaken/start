# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import geojson
from ... import ZaaksysteemRepositoryBase
from ..entities import CaseLocation
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from sqlalchemy.engine.row import Row
from uuid import UUID
from zsnl_domains.database import schema

ZAAK_BAG = sql.alias(schema.ZaakBag)
BAG_CACHE = sql.alias(schema.BagCache)


class CaseLocationRepository(ZaaksysteemRepositoryBase):
    CASE_LOCATION_BASE_QUERY = sql.select(
        ZAAK_BAG.c.uuid.label("uuid"),
        ZAAK_BAG.c.bag_type.label("bag_type"),
        ZAAK_BAG.c.bag_id.label("bag_id"),
        ZAAK_BAG.c.bag_verblijfsobject_id.label("residential_object_id"),
        ZAAK_BAG.c.bag_openbareruimte_id.label("public_space_id"),
        ZAAK_BAG.c.bag_nummeraanduiding_id.label("number_indication_id"),
        ZAAK_BAG.c.bag_pand_id.label("premise_id"),
        ZAAK_BAG.c.bag_standplaats_id.label("location_id"),
        ZAAK_BAG.c.bag_ligplaats_id.label("lay_id"),
        ZAAK_BAG.c.bag_coordinates_wsg.label("coordinates"),
        sql.cast(BAG_CACHE.c.bag_data, sql_pg.JSON)[
            "weergavenaam"
        ].astext.label("full_address"),
        sql.cast(BAG_CACHE.c.bag_data, sql_pg.JSON)[
            "centroide_ll"
        ].astext.label("geo_coordinates"),
    ).select_from(
        sql.join(
            ZAAK_BAG,
            BAG_CACHE,
            ZAAK_BAG.c.bag_id == BAG_CACHE.c.bag_id,
            isouter=True,
        )
    )

    _for_entity = "CaseLocation"

    def find_case_location_by_list_uuids(
        self, case_location_uuids: list[UUID]
    ) -> list[CaseLocation]:
        query = self.CASE_LOCATION_BASE_QUERY.where(
            ZAAK_BAG.c.uuid.in_(case_location_uuids)
        )

        results = self.session.execute(query).fetchall()
        return [self._sqla_to_entity(result) for result in results]

    def _sqla_to_entity(self, row: Row) -> CaseLocation:
        coordinates_geojson = self._convert_coordinates_to_geojson(
            row.geo_coordinates
        )
        return CaseLocation(
            entity_id=row.uuid,
            uuid=row.uuid,
            bag_type=row.bag_type,
            bag_id=row.bag_id,
            residential_object_id=row.residential_object_id,
            public_space_id=row.public_space_id,
            number_indication_id=row.number_indication_id,
            premise_id=row.premise_id,
            location_id=row.location_id,
            lay_id=row.lay_id,
            coordinates=row.coordinates,
            full_address=row.full_address,
            geo_coordinates=coordinates_geojson,
        )

    def _convert_coordinates_to_geojson(self, coordinates: str):
        try:
            coordinates = (
                coordinates.replace("POINT", "")
                .replace("(", "")
                .replace(")", "")
                .split(" ")
            )
            coordinates_geojson = geojson.Point(
                (float(coordinates[0]), float(coordinates[1]))
            )
            coordinates_geojson = {
                "type": "Feature",
                "geometry": coordinates_geojson,
            }
        except Exception:
            coordinates_geojson = None

        return coordinates_geojson
