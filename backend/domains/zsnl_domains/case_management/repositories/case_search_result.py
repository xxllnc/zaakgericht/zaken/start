# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ...database import schema
from ...shared.case_filters import CaseFilterApplicator
from ...shared.entities import total_result_count
from ...shared.repositories import case_acl
from ...shared.repositories.attribute_acl import (
    user_allowed_read_attributes_subquery,
)
from ...shared.types import SearchCursorTypedDict
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.engine.row import Row
from sqlalchemy.sql import selectable
from typing import Final
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import (
    case_search_result,
)

CASE_ALIAS: Final = sql.alias(schema.Case.__table__)
ZAAK_BETROKKENEN_ASSIGNEE_ALIAS: Final = sql.alias(
    schema.ZaakBetrokkenen.__table__
)
ZAAK_BETROKKENEN_COORDINATOR_ALIAS: Final = sql.alias(
    schema.ZaakBetrokkenen.__table__
)
ZAAK_BETROKKENEN_REQUESTOR_ALIAS: Final = sql.alias(
    schema.ZaakBetrokkenen.__table__
)
ZAAK_ZAAKTYPE_RESULTATEN_ALIAS: Final = sql.alias(
    schema.ZaaktypeResultaten.__table__
)
ZAAK_BAG_ALIAS: Final = sql.alias(schema.ZaakBag.__table__)
NATUURLIJK_PERSOON_ALIAS: Final = sql.alias(schema.NatuurlijkPersoon.__table__)


class CaseSearchResultRepository(ZaaksysteemRepositoryBase):
    file_custom_fields_query = sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "name",
                schema.BibliotheekKenmerk.naam,
                "magic_string",
                schema.BibliotheekKenmerk.magic_string,
                "type",
                schema.BibliotheekKenmerk.value_type,
                "value",
                sql.func.array(
                    sql.select(
                        sql.func.json_build_object(
                            "md5",
                            schema.Filestore.md5,
                            "size",
                            schema.Filestore.size,
                            "uuid",
                            schema.Filestore.uuid,
                            "filename",
                            schema.File.name + schema.File.extension,
                            "mimetype",
                            schema.Filestore.mimetype,
                            "is_archivable",
                            schema.Filestore.is_archivable,
                            "original_name",
                            schema.Filestore.original_name,
                            "thumbnail_uuid",
                            schema.Filestore.thumbnail_uuid,
                        ),
                    )
                    .select_from(
                        sql.join(
                            schema.FileCaseDocument,
                            schema.File,
                            sql.and_(
                                schema.FileCaseDocument.file_id
                                == schema.File.id,
                                schema.FileCaseDocument.case_id
                                == CASE_ALIAS.c.id,
                            ),
                        ).join(
                            schema.Filestore,
                            schema.File.filestore_id == schema.Filestore.id,
                        )
                    )
                    .where(
                        schema.FileCaseDocument.bibliotheek_kenmerken_id
                        == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                    )
                    .scalar_subquery()
                ),
            )
        )
        .select_from(
            sql.join(
                schema.ZaaktypeKenmerk,
                schema.BibliotheekKenmerk,
                sql.and_(
                    schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                    == schema.BibliotheekKenmerk.id,
                    schema.BibliotheekKenmerk.value_type == "file",
                ),
            ).join(
                schema.ZaaktypeNode,
                schema.ZaaktypeNode.id
                == schema.ZaaktypeKenmerk.zaaktype_node_id,
            )
        )
        .where(
            schema.ZaaktypeNode.id == CASE_ALIAS.c.zaaktype_node_id,
        )
        .scalar_subquery()
    ).label("file_custom_fields")

    _COUNT_PHASES_QUERY = (
        sql.select(sql.func.count(schema.ZaaktypeStatus.id))
        .where(
            schema.ZaaktypeStatus.zaaktype_node_id == schema.ZaaktypeNode.id
        )
        .scalar_subquery()
        .label("count_phases")
    )

    _for_entity = "CaseSearchResult"

    def search(
        self,
        user_info: UserInfo,
        permission: str,
        page: int,
        page_size: int,
        filters: dict,
        sort: case_search_result.CaseSearchOrder,
        included: list[str] | None,
        full_content: bool,
        cursor: SearchCursorTypedDict | None = None,
    ) -> EntityCollection[case_search_result.CaseSearchResult]:
        """Return a EntityCollection of the CaseSearchResult entity based on the search parameters.
        :return: EntityCollection of CaseSearchResult
        """

        self._get_custom_fields_query(user_info=user_info)

        self._CASE_SEARCH_BASE_QUERY = self._get_base_search_query(
            full_content=full_content, included=included
        )

        qry_stmt = self._get_search_query(
            case_alias=CASE_ALIAS,
            user_info=user_info,
            permission=permission,
            filters=filters,
        )

        if not cursor:
            sort_order = self._get_sort_order(order=sort)
            offset = self._calculate_offset(page, page_size)
            qry_stmt = (
                qry_stmt.order_by(*sort_order).limit(page_size).offset(offset)
            )
        else:
            qry_stmt = self._calculate_offset_based_on_cursor(
                qry_stmt, sort, cursor
            )

        case_rows = self.session.execute(qry_stmt).fetchall()
        cases_list = [
            self._entity_from_row(row, included) for row in case_rows
        ]

        entities = [case for case in cases_list if case is not None]

        return EntityCollection(entities=entities)

    def search_total_results(
        self,
        user_info: UserInfo,
        permission: str,
        filters: dict,
    ):
        """Get total count of Case based on given filter."""

        self._get_custom_fields_query(user_info=user_info)

        self._CASE_SEARCH_BASE_QUERY = self._get_base_search_query()

        qry_stmt = self._get_search_query(
            case_alias=CASE_ALIAS,
            user_info=user_info,
            permission=permission,
            filters=filters,
        )
        total_results = total_result_count.TotalResultCount(
            total_results=self._get_count(qry_stmt)
        )

        return total_results

    def _get_custom_fields_query(self, user_info):
        custom_field_filters = [schema.ZaakKenmerk.zaak_id == CASE_ALIAS.c.id]
        custom_fields_acl = user_allowed_read_attributes_subquery(user_info)
        if custom_fields_acl is not None:
            custom_field_filters.append(custom_fields_acl)
        custom_field_filter = sql.and_(*custom_field_filters)

        self.custom_fields_query = sql.func.array(
            sql.select(
                sql.func.json_build_object(
                    "name",
                    schema.BibliotheekKenmerk.naam,
                    "magic_string",
                    schema.BibliotheekKenmerk.magic_string,
                    "value",
                    schema.ZaakKenmerk.value,
                    "type",
                    schema.BibliotheekKenmerk.value_type,
                    "is_multiple",
                    schema.BibliotheekKenmerk.type_multiple,
                )
            )
            .select_from(
                sql.join(
                    schema.ZaakKenmerk,
                    schema.BibliotheekKenmerk,
                    schema.ZaakKenmerk.bibliotheek_kenmerken_id
                    == schema.BibliotheekKenmerk.id,
                )
            )
            .where(custom_field_filter)
            .scalar_subquery()
        ).label("custom_fields")

    def _get_base_search_query(self, full_content=False, included=None):
        select_onderwerp = sql.func.substring(
            CASE_ALIAS.c.onderwerp, 1, 250
        ).label("onderwerp")
        select_onderwerp_external = sql.func.substring(
            CASE_ALIAS.c.onderwerp_extern, 1, 250
        ).label("onderwerp_extern")
        if full_content:
            select_onderwerp = CASE_ALIAS.c.onderwerp
            select_onderwerp_external = CASE_ALIAS.c.onderwerp_extern

        role_subquery = None
        if included and "case_role_department" in included:
            role_subquery = (
                sql.select(schema.Role.uuid)
                .where(schema.Role.id == CASE_ALIAS.c.route_role)
                .scalar_subquery()
            )
        else:
            role_subquery = sql.literal_column("NULL")

        return sql.select(
            CASE_ALIAS.c.id,
            CASE_ALIAS.c.uuid,
            CASE_ALIAS.c.status,
            CASE_ALIAS.c.vernietigingsdatum,
            CASE_ALIAS.c.archival_state,
            CASE_ALIAS.c.registratiedatum,
            CASE_ALIAS.c.afhandeldatum,
            CASE_ALIAS.c.streefafhandeldatum.label("target_completion_date"),
            sql.func.get_date_progress_from_case(
                CASE_ALIAS.c.afhandeldatum,
                CASE_ALIAS.c.streefafhandeldatum,
                CASE_ALIAS.c.registratiedatum,
                CASE_ALIAS.c.status,
            ).label("percentage_days_left"),
            select_onderwerp,
            select_onderwerp_external,
            (
                sql.cast(CASE_ALIAS.c.milestone, sqltypes.Float)
                / self._COUNT_PHASES_QUERY
            ).label("progress"),
            schema.ZaaktypeNode.titel,
            schema.ZaaktypeNode.uuid.label("case_type_version"),
            schema.CaseMeta.unaccepted_files_count,
            schema.CaseMeta.unread_communication_count,
            schema.CaseMeta.unaccepted_attribute_update_count,
            CASE_ALIAS.c.requestor_v1_json["preview"].label("requestor_name"),
            CASE_ALIAS.c.requestor_v1_json["instance"]["subject"][
                "type"
            ].label("requestor_type"),
            CASE_ALIAS.c.requestor_v1_json["reference"].label(
                "requestor_uuid"
            ),
            CASE_ALIAS.c.assignee_v1_json["preview"].label("assignee_name"),
            CASE_ALIAS.c.assignee_v1_json["reference"].label("assignee_uuid"),
            CASE_ALIAS.c.coordinator_v1_json["preview"].label(
                "coordinator_name"
            ),
            CASE_ALIAS.c.coordinator_v1_json["reference"].label(
                "coordinator_uuid"
            ),
            self.custom_fields_query,
            self.file_custom_fields_query,
            ZAAK_ZAAKTYPE_RESULTATEN_ALIAS.c.trigger_archival.label(
                "period_of_preservation_active"
            ),
            ZAAK_ZAAKTYPE_RESULTATEN_ALIAS.c.uuid.label("case_type_result"),
            ZAAK_BAG_ALIAS.c.uuid.label("case_location"),
            CASE_ALIAS.c.stalled_until.label("stalled_until"),
            CASE_ALIAS.c.contactkanaal.label("contact_channel"),
            role_subquery.label("role"),
        ).select_from(
            sql.join(
                CASE_ALIAS,
                schema.ZaaktypeNode,
                CASE_ALIAS.c.zaaktype_node_id == schema.ZaaktypeNode.id,
            )
            .join(
                schema.CaseMeta,
                CASE_ALIAS.c.id == schema.CaseMeta.zaak_id,
            )
            .join(
                schema.Zaaktype, CASE_ALIAS.c.zaaktype_id == schema.Zaaktype.id
            )
            .join(
                ZAAK_BETROKKENEN_ASSIGNEE_ALIAS,
                ZAAK_BETROKKENEN_ASSIGNEE_ALIAS.c.id
                == CASE_ALIAS.c.behandelaar,
                isouter=True,
            )
            .join(
                ZAAK_BETROKKENEN_COORDINATOR_ALIAS,
                ZAAK_BETROKKENEN_COORDINATOR_ALIAS.c.id
                == CASE_ALIAS.c.coordinator,
                isouter=True,
            )
            .join(
                ZAAK_BETROKKENEN_REQUESTOR_ALIAS,
                ZAAK_BETROKKENEN_REQUESTOR_ALIAS.c.id
                == CASE_ALIAS.c.aanvrager,
            )
            .join(
                ZAAK_ZAAKTYPE_RESULTATEN_ALIAS,
                ZAAK_ZAAKTYPE_RESULTATEN_ALIAS.c.id
                == CASE_ALIAS.c.resultaat_id,
                isouter=True,
            )
            .join(
                ZAAK_BAG_ALIAS,
                ZAAK_BAG_ALIAS.c.id == CASE_ALIAS.c.locatie_zaak,
                isouter=True,
            )
        )

    def _get_search_query(
        self,
        case_alias: selectable.NamedFromClause,
        user_info: UserInfo,
        permission: str,
        filters: dict,
    ):
        case_search_query = self._CASE_SEARCH_BASE_QUERY.where(
            case_acl.user_allowed_cases_subquery(
                user_info, permission, case_alias=case_alias.c
            )
        )

        cf = CaseFilterApplicator(
            CASE_ALIAS,
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS,
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS,
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS,
            ZAAK_ZAAKTYPE_RESULTATEN_ALIAS,
            ZAAK_BAG_ALIAS,
            NATUURLIJK_PERSOON_ALIAS,
        )

        case_search_query = cf.apply_to_query(
            case_search_query, filters, user_info
        )
        return case_search_query

    def _calculate_offset_based_on_cursor(self, qry_stmt, sort, cursor):
        last_result = cursor["last_item"]
        last_id = cursor.get("last_item_identifier", 1)
        page = cursor.get("page")
        page_size = cursor.get("page_size")
        sort_order = self._get_sort_order(order=sort)

        if page == "next":
            cursor_subquery = self._get_subquery_cursor_next_page(
                last_result, last_id, sort
            )
            qry_stmt = qry_stmt.where(cursor_subquery)
            qry_stmt = qry_stmt.order_by(*sort_order).limit(page_size)
        elif page == "previous":
            cursor_subquery = self._get_subquery_cursor_previous_page(
                last_result, last_id, sort
            )
            qry_stmt = qry_stmt.where(cursor_subquery)
            subquery_sort = sort[1:] if sort[0] == "-" else "-" + sort
            subquery_sort_order = self._get_sort_order(order=subquery_sort)

            inner_query = (
                qry_stmt.order_by(*subquery_sort_order).limit(page_size)
            ).subquery()

            order = self._get_sort_order(inner_query, sort)

            qry_stmt = sql.select(inner_query).order_by(*order)

        return qry_stmt

    def _get_subquery_cursor_next_page(self, last_result, last_id, sort):
        PAGINATION_SUBQUERY = {
            case_search_result.CaseSearchOrder.number_asc: CASE_ALIAS.c.id
            > last_result,
            case_search_result.CaseSearchOrder.unread_message_count_asc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unread_communication_count == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                schema.CaseMeta.unread_communication_count > last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_files_count_asc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_files_count == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_files_count > last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_asc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_attribute_update_count
                    == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_attribute_update_count
                > last_result,
            ),
            case_search_result.CaseSearchOrder.registration_date_asc: sql.or_(
                sql.and_(
                    CASE_ALIAS.c.registratiedatum == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                CASE_ALIAS.c.registratiedatum > last_result,
            ),
            case_search_result.CaseSearchOrder.number_desc: CASE_ALIAS.c.id
            < last_result,
            case_search_result.CaseSearchOrder.unread_message_count_desc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unread_communication_count == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                schema.CaseMeta.unread_communication_count < last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_files_count_desc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_files_count == last_result,
                    CASE_ALIAS.c.id < last_id,
                ),
                schema.CaseMeta.unaccepted_files_count < last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_desc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_attribute_update_count
                    == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_attribute_update_count
                < last_result,
            ),
            case_search_result.CaseSearchOrder.registration_date_desc: sql.or_(
                sql.and_(
                    CASE_ALIAS.c.registratiedatum == last_result,
                    CASE_ALIAS.c.id < last_id,
                ).self_group(),
                CASE_ALIAS.c.registratiedatum < last_result,
            ),
        }
        return PAGINATION_SUBQUERY[sort]

    def _get_subquery_cursor_previous_page(self, last_result, last_id, sort):
        PAGINATION_SUBQUERY = {
            case_search_result.CaseSearchOrder.number_asc: CASE_ALIAS.c.id
            < last_result,
            case_search_result.CaseSearchOrder.unread_message_count_asc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unread_communication_count == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                schema.CaseMeta.unread_communication_count < last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_files_count_asc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_files_count == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_files_count < last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_asc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_attribute_update_count
                    == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_attribute_update_count
                < last_result,
            ),
            case_search_result.CaseSearchOrder.registration_date_asc: sql.or_(
                sql.and_(
                    CASE_ALIAS.c.registratiedatum == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                CASE_ALIAS.c.registratiedatum < last_result,
            ),
            case_search_result.CaseSearchOrder.number_desc: CASE_ALIAS.c.id
            > last_result,
            case_search_result.CaseSearchOrder.unread_message_count_desc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unread_communication_count == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                schema.CaseMeta.unread_communication_count > last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_files_count_desc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_files_count == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_files_count > last_result,
            ),
            case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_desc: sql.or_(
                sql.and_(
                    schema.CaseMeta.unaccepted_attribute_update_count
                    == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                schema.CaseMeta.unaccepted_attribute_update_count
                > last_result,
            ),
            case_search_result.CaseSearchOrder.registration_date_desc: sql.or_(
                sql.and_(
                    CASE_ALIAS.c.registratiedatum == last_result,
                    CASE_ALIAS.c.id > last_id,
                ).self_group(),
                CASE_ALIAS.c.registratiedatum > last_result,
            ),
        }
        return PAGINATION_SUBQUERY[sort]

    def _get_sort_order(
        self,
        alias=CASE_ALIAS,
        order=case_search_result.CaseSearchOrder.number_desc,
    ):
        SORT_ORDER = {
            case_search_result.CaseSearchOrder.number_asc: [
                sql.asc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.unread_message_count_asc: [
                sql.asc(schema.CaseMeta.unread_communication_count),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.unaccepted_files_count_asc: [
                sql.asc(schema.CaseMeta.unaccepted_files_count),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_asc: [
                sql.asc(schema.CaseMeta.unaccepted_attribute_update_count),
                sql.desc(CASE_ALIAS.c.id),
            ],
            case_search_result.CaseSearchOrder.registration_date_asc: [
                sql.asc(alias.c.registratiedatum),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.completion_date_asc: [
                sql.asc(alias.c.afhandeldatum),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.number_desc: [
                sql.desc(alias.c.id)
            ],
            case_search_result.CaseSearchOrder.unread_message_count_desc: [
                sql.desc(schema.CaseMeta.unread_communication_count),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.unaccepted_files_count_desc: [
                sql.desc(schema.CaseMeta.unaccepted_files_count),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_desc: [
                sql.desc(schema.CaseMeta.unaccepted_attribute_update_count),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.registration_date_desc: [
                sql.desc(alias.c.registratiedatum),
                sql.desc(alias.c.id),
            ],
            case_search_result.CaseSearchOrder.completion_date_desc: [
                sql.desc(alias.c.afhandeldatum),
                sql.desc(alias.c.id),
            ],
        }
        return SORT_ORDER[order]

    def _entity_from_row(
        self, row: Row, included: list[str] | None
    ) -> case_search_result.CaseSearchResult | None:
        """Initialize CaseSearchResult Entity from a database row"""

        (
            requestor,
            assignee,
            coordinator,
            case_type,
            case_type_result,
            case_location,
            case_department_role,
        ) = self._fill_included_relationship_data(row, included)

        custom_fields_repo = self._get_repo(case_management, "custom_fields")
        try:
            case_result = case_search_result.CaseSearchResult(
                entity_id=row.uuid,
                uuid=row.uuid,
                number=row.id,
                status=row.status,
                destruction_date=row.vernietigingsdatum,
                archival_state=row.archival_state,
                case_type_title=row.titel,
                subject=row.onderwerp,
                subject_external=row.onderwerp_extern,
                percentage_days_left=row.percentage_days_left,
                progress=row.progress * 100,
                unread_message_count=row.unread_communication_count,
                unaccepted_files_count=row.unaccepted_files_count,
                unaccepted_attribute_update_count=row.unaccepted_attribute_update_count,
                requestor=requestor,
                assignee=assignee,
                coordinator=coordinator,
                case_type=case_type,
                registration_date=row.registratiedatum,
                completion_date=row.afhandeldatum,
                custom_fields=custom_fields_repo.format_custom_fields_for_case_basic(
                    custom_fields=row.custom_fields,
                    file_custom_fields=row.file_custom_fields,
                ).custom_fields,
                target_completion_date=row.target_completion_date,
                period_of_preservation_active=row.period_of_preservation_active
                or False,
                case_type_result=case_type_result,
                case_location=case_location,
                stalled_until=row.stalled_until,
                contact_channel=row.contact_channel,
                role=case_department_role,
            )
            return case_result
        except Exception:
            return None

    def _fill_included_relationship_data(
        self, row: Row, included: list[str] | None
    ):
        requestor = assignee = coordinator = case_type = case_type_result = (
            case_location
        ) = case_role_department = None

        contact_type_matpping = {
            "person": "person",
            "employee": "employee",
            "company": "organization",
        }
        requestor = {
            "entity_id": row.requestor_uuid,
            "entity_type": contact_type_matpping[row.requestor_type],
            "uuid": row.requestor_uuid,
            "type": contact_type_matpping[row.requestor_type],
            "name": row.requestor_name,
        }

        if not included:
            return (
                requestor,
                assignee,
                coordinator,
                case_type,
                case_type_result,
                case_location,
                case_role_department,
            )

        if "assignee" in included:
            assignee = (
                None
                if not row.assignee_uuid
                else {
                    "entity_id": row.assignee_uuid,
                    "uuid": row.assignee_uuid,
                    "name": row.assignee_name,
                    "type": "employee",
                }
            )
        if "coordinator" in included:
            coordinator = (
                None
                if not row.coordinator_uuid
                else {
                    "entity_id": row.coordinator_uuid,
                    "uuid": row.coordinator_uuid,
                    "name": row.coordinator_name,
                    "type": "employee",
                }
            )
        if "case_type" in included:
            case_type = (
                None
                if not row.case_type_version
                else {
                    "entity_id": row.case_type_version,
                    "uuid": row.case_type_version,
                    "name": row.titel,
                }
            )
        if "case_type_result" in included:
            case_type_result = (
                None
                if not row.case_type_result
                else {
                    "entity_id": row.case_type_result,
                    "uuid": row.case_type_result,
                }
            )

        if "case_location" in included:
            case_location = (
                None
                if not row.case_location
                else {
                    "entity_id": row.case_location,
                    "uuid": row.case_location,
                    "type": "case_location",
                }
            )

        if "case_role_department" in included:
            case_role_department = (
                None
                if not row.role
                else {
                    "entity_id": row.role,
                }
            )

        return (
            requestor,
            assignee,
            coordinator,
            case_type,
            case_type_result,
            case_location,
            case_role_department,
        )
