# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import re
from ... import ZaaksysteemRepositoryBase
from ..entities import (
    CaseTypeVersionEntity,
    CaseTypeVersionSearchResultEntity,
    StaticRuleAction,
)
from ..entities.case_type_version import RuleAction
from .database_queries import (
    case_type_version_by_id_query,
    case_type_version_by_list_uuids_query,
    is_simple_case_type_query,
)
from minty.exceptions import NotFound
from sqlalchemy.sql import and_, select
from uuid import UUID
from zsnl_domains.database import schema


class CaseTypeRepository(ZaaksysteemRepositoryBase):
    def is_simple_case_type(self, uuid: UUID) -> bool:
        """Determine if it is possible to create a case with
        the specified case type with the v2 API create_case

        :param case_type_uuid: case_type uuid
        :type uuid: UUID
        :return: True or False
        :rtype: boolean
        """
        qry_stmt = is_simple_case_type_query(uuid)

        query_result = self.session.execute(qry_stmt).fetchone()
        return query_result is not None

    def _generate_settings_for_case_type(self, case_type):
        """Generate settings for a case_type.

        :param case_type: case_type
        :type case_type: Query result
        :return: settings for a case_type
        :rtype: dict
        """

        list_of_default_folders = []

        if case_type["properties"].get("default_directories"):
            list_of_default_folders = [
                {"name": directory}
                for directory in case_type["properties"].get(
                    "default_directories"
                )
            ]

        settings = {
            "allow_reuse_casedata": case_type["allow_reuse_casedata"],
            "enable_webform": case_type["enable_webform"],
            "enable_online_payment": case_type["enable_online_payment"],
            "enable_manual_payment": case_type["properties"].get(
                "offline_betaling"
            ),
            "require_email_on_webform": case_type["require_email_on_webform"],
            "require_phonenumber_on_webform": case_type[
                "require_phonenumber_on_webform"
            ],
            "require_mobilenumber_on_webform": case_type[
                "require_mobilenumber_on_webform"
            ],
            "disable_captcha_for_predefined_requestor": case_type[
                "properties"
            ].get("no_captcha"),
            "show_confidentiality": case_type["properties"].get(
                "confidentiality"
            ),
            "show_contact_info": case_type["show_contact_info"],
            "enable_subject_relations_on_form": case_type[
                "enable_subject_relations_on_form"
            ],
            "open_case_on_create": case_type["open_case_on_create"],
            "enable_allocation_on_form": case_type[
                "enable_allocation_on_form"
            ],
            "disable_pip_for_requestor": case_type[
                "disable_pip_for_requestor"
            ],
            "lock_registration_phase": case_type["properties"].get(
                "lock_registration_phase"
            ),
            "enable_queue_for_changes_from_other_subjects": case_type[
                "properties"
            ].get("queue_coworker_changes"),
            "check_acl_on_allocation": case_type["properties"].get(
                "check_permissions_for_assignee"
            ),
            "api_can_transition": case_type["properties"].get(
                "api_can_transition"
            ),
            "is_public": case_type["is_public"],
            "list_of_default_folders": list_of_default_folders,
            "text_confirmation_message_title": case_type["properties"].get(
                "public_confirmation_title"
            ),
            "text_public_confirmation_message": case_type["properties"].get(
                "public_confirmation_message"
            ),
            "allow_external_task_assignment": case_type["properties"].get(
                "allow_external_task_assignment", False
            ),
            "payment": {
                "assignee": {
                    "amount": case_type["properties"].get(
                        "pdc_tarief_behandelaar"
                    )
                },
                "frontdesk": {
                    "amount": case_type["properties"].get("pdc_tarief_balie")
                },
                "phone": {
                    "amount": case_type["properties"].get(
                        "pdc_tarief_telefoon"
                    )
                },
                "mail": {
                    "amount": case_type["properties"].get("pdc_tarief_email")
                },
                "webform": {"amount": case_type["webform_amount"]},
                "post": {
                    "amount": case_type["properties"].get("pdc_tarief_post")
                },
            },
        }

        return settings

    def _generate_metadata_for_case_type(self, case_type):
        """Generate metada for a case_type.

        :param case_type: case_type
        :type case_type: sqla query results
        :return: metadata for a case_type
        :rtype: dict
        """
        metadata = {
            "may_postpone": case_type["properties"].get("opschorten_mogelijk"),
            "may_extend": case_type["properties"].get("verlenging_mogelijk"),
            "extension_period": case_type["properties"].get(
                "verdagingstermijn"
            ),
            "adjourn_period": case_type["properties"].get("verdagingstermijn"),
            "e_webform": case_type["properties"].get("e_formulier"),
            "process_description": case_type["process_description"],
            "motivation": case_type["properties"].get("aanleiding"),
            "purpose": case_type["properties"].get("doel"),
            "archive_classification_code": case_type["properties"].get(
                "archiefclassicatiecode"
            ),
            "designation_of_confidentiality": case_type["properties"].get(
                "vertrouwelijkheidsaanduiding"
            ),
            "responsible_subject": case_type["properties"].get(
                "verantwoordelijke"
            ),
            "responsible_relationship": case_type["properties"].get(
                "verantwoordingsrelatie"
            ),
            "possibility_for_objection_and_appeal": case_type[
                "properties"
            ].get("beroep_mogelijk"),
            "publication": case_type["properties"].get("publicatie"),
            "publication_text": case_type["properties"].get("publicatietekst"),
            "bag": case_type["properties"].get("bag"),
            "lex_silencio_positivo": case_type["properties"].get(
                "lex_silencio_positivo"
            ),
            "penalty_law": case_type["properties"].get("wet_dwangsom"),
            "wkpb_applies": case_type["properties"].get("wkpb"),
            "legal_basis": case_type["legal_basis"],
            "local_basis": case_type["properties"].get("lokale_grondslag"),
        }

        return metadata

    def _format_database_rules(self, rules, custom_fields: list):
        """Format database rules"""
        rules_list = []
        db_field_to_entity_field_mapping = {
            "verberg_kenmerk": "hide_attribute",
            "toon_kenmerk": "show_attribute",
            "vul_waarde_in": "set_value",
            "vul_waarde_in_met_formule": "set_value_formula",
            "pauzeer_aanvraag": "pause_application",
            "toewijzing": "set_allocation",
        }

        for rule in rules:
            rule_settings = json.loads(rule["settings"])

            if not rule_settings.get("condition_type", None):
                # If no condition_type found, continue with next rule
                continue
            formatted_rule = {
                "name": rule["name"],
                "condition_type": rule_settings["condition_type"],
                "conditions": [],
                "match_actions": [],
                "nomatch_actions": [],
            }

            conditions_dict = {}
            match_actions_dict = {}
            nomatch_actions_dict = {}

            for key, value in rule_settings.items():
                match_action_found = re.match("^actie_([0-9]+)_(.+)$", key)
                nomatch_action_found = re.match("^ander_([0-9]+)_(.+)$", key)
                condition_found = re.match("^voorwaarde_([0-9]+)_(.+)$", key)

                if condition_found:
                    index = condition_found.group(1)
                    condition_field = condition_found.group(2)
                    if index in conditions_dict:
                        conditions_dict[index].update({condition_field: value})
                    else:
                        conditions_dict[index] = {
                            condition_field: value,
                        }

                if match_action_found:
                    index = match_action_found.group(1)
                    action_field = match_action_found.group(2)
                    if index in match_actions_dict:
                        match_actions_dict[index]["settings"].update(
                            {action_field: value}
                        )
                    else:
                        action = rule_settings[
                            f"actie_{match_action_found.group(1)}"
                        ]
                        match_actions_dict[index] = {
                            "action": db_field_to_entity_field_mapping.get(
                                action, action
                            ),
                            "settings": {action_field: value},
                        }

                if nomatch_action_found:
                    index = nomatch_action_found.group(1)
                    nomatch_action_field = nomatch_action_found.group(2)

                    if index in nomatch_actions_dict:
                        nomatch_actions_dict[index]["settings"].update(
                            {nomatch_action_field: value}
                        )
                    else:
                        action = rule_settings[
                            f"ander_{nomatch_action_found.group(1)}"
                        ]
                        nomatch_actions_dict[index] = {
                            "action": db_field_to_entity_field_mapping.get(
                                action, action
                            ),
                            "settings": {nomatch_action_field: value},
                        }

            formatted_rule["conditions"] = [
                value for key, value in conditions_dict.items()
            ]
            formatted_rule["match_actions"] = [
                value for key, value in match_actions_dict.items()
            ]
            formatted_rule["nomatch_actions"] = [
                value for key, value in nomatch_actions_dict.items()
            ]
            rules_list.append(formatted_rule)

        self._overwrite_rule_fields(
            rules=rules_list, custom_fields=custom_fields
        )
        return rules_list

    def _overwrite_rule_fields(self, rules: list, custom_fields: list):
        # overwrite certain actions values
        self._overwrite_kenmerk_rule_action_setting(
            rules=rules, custom_fields=custom_fields
        )

    def _overwrite_kenmerk_rule_action_setting(
        self, rules: list, custom_fields: list
    ):
        def is_kenmerk_action(action):
            return action["action"] in [
                StaticRuleAction.set_value.value,
                StaticRuleAction.show_attribute.value,
                StaticRuleAction.hide_attribute.value,
                StaticRuleAction.update_registation_date,
                StaticRuleAction.date_calculations,
            ]

        custom_fields_by_id = {
            custom_field["id"]: custom_field for custom_field in custom_fields
        }
        for rule in rules:
            actions = []
            # get all actions that can contain the kenmerk setting
            actions.extend(
                list(filter(is_kenmerk_action, rule["match_actions"]))
            )
            actions.extend(
                list(filter(is_kenmerk_action, rule["nomatch_actions"]))
            )
            for action in actions:
                kenmerk_to_process = [
                    "kenmerk",
                    "dst_attr_id",
                    "src_attr_id",
                    "datum_bibliotheek_kenmerken_id",
                ]
                for kenmerk in kenmerk_to_process:
                    self._update_kenmerk(custom_fields_by_id, action, kenmerk)

    def _update_kenmerk(
        self,
        custom_fields_by_id: list,
        action: RuleAction,
        kenmerk: str,
    ):
        kenmerk_val = action["settings"].get(kenmerk, None)
        if kenmerk_val:
            attribute_id = None
            if isinstance(kenmerk_val, int):
                attribute_id = kenmerk_val
            else:
                try:
                    attribute_id = int(kenmerk_val)
                except ValueError:
                    # Weirdness in the database!
                    self.logger.warning(
                        f"Invalid data for rule action kenmerk setting {kenmerk_val}. Should be int or numeric string"
                    )
            if attribute_id and attribute_id in custom_fields_by_id:
                # overwrite the kenmerk id
                action["settings"][kenmerk] = custom_fields_by_id[attribute_id]

    def _sqla_to_entity(self, query_result) -> CaseTypeVersionEntity:
        """Initialize CasetypeVersionEntity from sqla object.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type entity
        :rtype: entities.CaseTypeVersionEntity
        """
        for phase in query_result["phases"]:
            phase["rules"] = (
                self._format_database_rules(
                    phase["rules"], phase["custom_fields"]
                )
                if phase.get("rules", [])
                else []
            )
        case_type = CaseTypeVersionEntity.parse_obj(
            {
                "entity_id": query_result["uuid"],
                "id": query_result["id"],
                "uuid": query_result["uuid"],
                "case_type_uuid": query_result["case_type_uuid"],
                "version": query_result["version"],
                "name": query_result["name"],
                "description": query_result["description"],
                "identification": query_result["identification"],
                "tags": query_result["tags"],
                "case_summary": query_result["case_summary"],
                "case_public_summary": query_result["case_public_summary"],
                "active": query_result["active"],
                "requestor": query_result["requestor"],
                "catalog_folder": {
                    "entity_id": query_result["catalog_folder"]["uuid"],
                    "uuid": query_result["catalog_folder"]["uuid"],
                    "entity_meta_summary": query_result["catalog_folder"][
                        "name"
                    ],
                    "name": query_result["catalog_folder"]["name"],
                }
                if query_result["catalog_folder"]
                else None,
                "metadata": self._generate_metadata_for_case_type(
                    query_result
                ),
                "settings": self._generate_settings_for_case_type(
                    query_result
                ),
                "phases": query_result["phases"],
                "terms": query_result["terms"],
                "initiator_type": query_result["initiator_type"],
                "initiator_source": query_result["initiator_source"],
                "created": query_result["created"],
                "last_modified": query_result["last_modified"],
                "deleted": query_result["deleted"],
                "is_eligible_for_case_creation": self.is_simple_case_type(
                    uuid=query_result["uuid"]
                ),
                "preset_requestor": {
                    "entity_type": query_result["preset_requestor"]["type"],
                    "entity_id": query_result["preset_requestor"]["uuid"],
                    "uuid": query_result["preset_requestor"]["uuid"],
                    "entity_meta_summary": query_result["preset_requestor"][
                        "name"
                    ],
                }
                if query_result["preset_requestor"]
                else None,
                "preset_assignee": {
                    "entity_id": query_result["preset_assignee"]["uuid"],
                    "uuid": query_result["preset_assignee"]["uuid"],
                    "entity_meta_summary": query_result["preset_assignee"][
                        "name"
                    ],
                }
                if query_result["preset_assignee"]
                else None,
                # Services, etc. needed by the entity
                "case_type_results": query_result["results"]
                if query_result["results"]
                else [],
                "_event_service": self.event_service,
            }
        )

        self.cache[str(case_type.uuid)] = case_type
        return case_type

    def _get_case_type_node_id(self, case_type_uuid):
        case_type_row = self.session.execute(
            select(schema.ZaaktypeNode.id).where(
                schema.ZaaktypeNode.uuid == case_type_uuid
            )
        ).fetchone()

        if not case_type_row:
            raise NotFound(
                f"Case type version with version_uuid: '{case_type_uuid}' not found.",
                "case_type/version_not_found",
            )

        return case_type_row.id

    def _get_case_type_node_id_for_case(self, case_uuid):
        case_type_row = self.session.execute(
            select(schema.Case.zaaktype_node_id.label("id")).where(
                schema.Case.uuid == case_uuid
            )
        ).fetchone()

        if not case_type_row:
            raise NotFound(
                f"Case type version for case: '{case_uuid}' not found.",
                "case_type/version_not_found",
            )

        return case_type_row.id

    def find_case_type_version_by_uuid(
        self, version_uuid: UUID
    ) -> CaseTypeVersionEntity:
        """Find  version of case_type by version_uuid.

        :param uuid: version_uuid
        :type uuid: UUID
        :raises NotFound: record not found
        :return: case type
        :rtype: entities.CaseTypeVersionEntity
        """
        case_type_node_id = self._get_case_type_node_id(version_uuid)
        qry_stmt = case_type_version_by_id_query(case_type_node_id)

        query_result = self.session.execute(qry_stmt).fetchone()

        if not query_result:
            raise NotFound(
                f"Case type version with version_uuid: '{version_uuid}' not found.",
                "case_type/version_not_found",
            )
        return self._sqla_to_entity(query_result=query_result[0])

    def find_case_type_version_for_case(
        self, case_uuid: UUID
    ) -> CaseTypeVersionEntity:
        """Find case type version for a specific case.

        :param uuid: UUID of the case
        :raises NotFound: record not found
        :return: case type version
        """
        case_type_node_id = self._get_case_type_node_id_for_case(case_uuid)
        qry_stmt = case_type_version_by_id_query(case_type_node_id)
        query_result = None

        try:
            query_result = self.session.execute(qry_stmt).fetchone()
        except Exception as e:
            self.logger.warning(
                f"Caught exception during case_type_version query: {e}"
            )

        if not query_result:
            raise NotFound(
                f"Case type version for case with uuid: '{case_uuid}' not found.",
                "case_type/version_not_found",
            )
        return self._sqla_to_entity(query_result=query_result[0])

    def find_active_version_by_uuid(self, case_type_uuid: UUID):
        """Find active version of a casetype by case_type_uuid

        :param case_type_uuid: uuid of the case_type
        :type case_type_uuid: UUID
        :raises NotFound: record not found
        :return: CaseTypeVersion
        :rtype: CaseTypeVersionEntity
        """
        latest_version = self.session.execute(
            select(schema.ZaaktypeNode.uuid).where(
                and_(
                    schema.Zaaktype.uuid == case_type_uuid,
                    schema.ZaaktypeNode.id == schema.Zaaktype.zaaktype_node_id,
                )
            )
        ).fetchone()

        if not latest_version:
            raise NotFound(
                f"Case type with uuid '{case_type_uuid}' not found.",
                "case_type/not_found",
            )

        return self.find_case_type_version_by_uuid(
            version_uuid=latest_version.uuid
        )

    def find_case_types_by_list_uuids(self, case_type_ids):
        qry_stmt = case_type_version_by_list_uuids_query(case_type_ids)
        query_result = None

        query_result = self.session.execute(qry_stmt).fetchall()

        return [
            self._sqla_to_entity_case_type_search_result(query_result=result)
            for result in query_result
        ]

    def _sqla_to_entity_case_type_search_result(
        self, query_result
    ) -> CaseTypeVersionSearchResultEntity:
        case_type = CaseTypeVersionSearchResultEntity.parse_obj(
            {
                "entity_id": query_result.uuid,
                "uuid": query_result.uuid,
                "case_type_uuid": query_result.case_type_uuid,
                "version": query_result.version,
                "name": query_result.name,
                "description": query_result.description,
                "identification": query_result.identification,
                "tags": query_result.tags,
                "case_summary": query_result.case_summary,
                "case_public_summary": query_result.case_public_summary,
                "active": query_result.active,
                "created": query_result.created,
                "last_modified": query_result.last_modified,
                "deleted": query_result.deleted,
                "payment": {
                    "assignee": {
                        "amount": query_result.properties.get(
                            "pdc_tarief_behandelaar"
                        )
                    },
                    "frontdesk": {
                        "amount": query_result.properties.get(
                            "pdc_tarief_balie"
                        )
                    },
                    "phone": {
                        "amount": query_result.properties.get(
                            "pdc_tarief_telefoon"
                        )
                    },
                    "mail": {
                        "amount": query_result.properties.get(
                            "pdc_tarief_email"
                        )
                    },
                    "webform": {"amount": query_result.webform_amount},
                    "post": {
                        "amount": query_result.properties.get(
                            "pdc_tarief_post"
                        )
                    },
                },
                "terms": query_result.terms,
                "initiator_type": query_result.initiator_type,
                "initiator_source": query_result.initiator_source,
                "metadata": self._generate_metadata_for_case_type(
                    query_result._asdict()
                ),
            }
        )
        self.cache[str(case_type.uuid)] = case_type
        return case_type
