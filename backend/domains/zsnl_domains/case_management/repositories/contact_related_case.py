# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from .. import entities
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from sqlalchemy import case, sql
from sqlalchemy import types as sqltypes
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)
from zsnl_domains.shared.util import TimedInMilliseconds, prepare_search_term

DEFAULT_SORT_ORDER = [sql.desc("case_id")]

CASE_SORT_ORDERS = {
    "attributes.number": [sql.asc(schema.Case.id)],
    "attributes.status": [sql.asc("status"), *DEFAULT_SORT_ORDER],
    "attributes.days_left": [sql.asc("days_left"), *DEFAULT_SORT_ORDER],
    "attributes.result": [
        sql.asc(schema.Case.resultaat),
        *DEFAULT_SORT_ORDER,
    ],
    "attributes.progress": [sql.asc("progress"), *DEFAULT_SORT_ORDER],
    "relationships.case_type.name": [
        sql.asc(schema.ZaaktypeNode.titel),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.number": [*DEFAULT_SORT_ORDER],
    "-attributes.status": [sql.desc("status"), *DEFAULT_SORT_ORDER],
    "-attributes.days_left": [sql.desc("days_left"), *DEFAULT_SORT_ORDER],
    "-attributes.result": [
        sql.desc(schema.Case.resultaat),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.progress": [sql.desc("progress"), *DEFAULT_SORT_ORDER],
    "-relationships.case_type.name": [
        sql.desc(schema.ZaaktypeNode.titel),
        *DEFAULT_SORT_ORDER,
    ],
}


def _apply_filter_to_include_role(select_stmt, roles_to_include, contact_info):
    if "Aanvrager" in roles_to_include:
        select_stmt = select_stmt.where(
            sql.and_(
                schema.Case.aanvrager_gm_id == contact_info.id,
                schema.Case.aanvrager_type == contact_info.type,
            )
        )
    if "Behandelaar" in roles_to_include:
        select_stmt = select_stmt.where(
            schema.Case.behandelaar_gm_id == contact_info.id
        )
    if "Coordinator" in roles_to_include:
        select_stmt = select_stmt.where(
            schema.Case.coordinator_gm_id == contact_info.id
        )
    return select_stmt


def _apply_filter_to_exclude_role(select_stmt, roles_to_exclude, contact_info):
    if "Aanvrager" in roles_to_exclude:
        select_stmt = select_stmt.where(
            sql.not_(
                sql.and_(
                    schema.Case.aanvrager_gm_id == contact_info.id,
                    schema.Case.aanvrager_type == contact_info.type,
                ),
            )
        )
    if "Behandelaar" in roles_to_exclude:
        select_stmt = select_stmt.where(
            schema.Case.behandelaar_gm_id != contact_info.id
        )
    if "Coordinator" in roles_to_exclude:
        select_stmt = select_stmt.where(
            schema.Case.coordinator_gm_id != contact_info.id
        )
    return select_stmt


def _get_cases_on_location_query(contact_info):
    if contact_info.type == "natuurlijk_persoon":
        return sql.and_(
            schema.ZaakBag.bag_type == "nummeraanduiding",
            schema.ZaakBag.bag_id == contact_info.location,
        )

    if contact_info.type == "bedrijf":
        return sql.and_(
            schema.ZaakBag.bag_type == "nummeraanduiding",
            schema.ZaakBag.bag_verblijfsobject_id == contact_info.location,
        )
    return


class ContactRelatedCaseRepository(ZaaksysteemRepositoryBase):
    def get_contact_related_cases_list(
        self,
        contact_uuid: UUID,
        user_info: UserInfo,
        page: int,
        page_size: int,
        include_cases_on_location: bool,
        is_authorized: bool | None = None,
        status_list: set[str] = None,
        sort: str | None = None,
        roles_to_include: set[str] | None = None,
        roles_to_exclude: set[str] | None = None,
        keyword: str | None = None,
    ) -> EntityCollection[entities.ContactRelatedCase]:
        contact_info = self._get_contact_type(contact_uuid)
        select_stmt = self._get_base_case_query()
        is_filtered = False
        select_stmt = select_stmt.where(
            sql.and_(user_allowed_cases_subquery(user_info, "search"))
        )

        if status_list:
            select_stmt = select_stmt.where(
                schema.Case.status.in_(status_list)
            )

        if roles_to_include:
            is_filtered = True
            select_stmt = _apply_filter_to_include_role(
                select_stmt, roles_to_include, contact_info
            )

        if roles_to_exclude:
            is_filtered = True
            select_stmt = _apply_filter_to_exclude_role(
                select_stmt, roles_to_exclude, contact_info
            )

        if is_authorized is not None:
            is_filtered = True
            select_stmt = select_stmt.where(
                sql.cast(
                    schema.Case.betrokkenen_cache.op("->")(contact_uuid).op(
                        "->"
                    )("pip_authorized"),
                    sqltypes.Boolean,
                ).is_(is_authorized)
            )

        cases_on_location_query = _get_cases_on_location_query(contact_info)

        if (
            contact_info.location
            and include_cases_on_location is True
            and cases_on_location_query is not None
        ):
            select_stmt = select_stmt.where(cases_on_location_query)
        elif is_filtered is False and cases_on_location_query is not None:
            select_stmt = select_stmt.where(
                sql.or_(
                    cases_on_location_query,
                    schema.Case.betrokkenen_cache.op("?")(contact_uuid),
                )
            )
        else:
            select_stmt = select_stmt.where(
                schema.Case.betrokkenen_cache.op("?")(contact_uuid)
            )

        if keyword:
            prepared_search_term = prepare_search_term(search_term=keyword)
            select_stmt = select_stmt.where(
                sql.and_(
                    schema.Case.id == schema.CaseMeta.zaak_id,
                    schema.CaseMeta.text_vector.match(prepared_search_term),
                )
            )

        if sort:
            sort_order = CASE_SORT_ORDERS[sort]
        else:
            sort_order = DEFAULT_SORT_ORDER

        offset = self._calculate_offset(page, page_size)
        select_stmt = (
            select_stmt.order_by(*sort_order).limit(page_size).offset(offset)
        )
        return self._execute_query(select_stmt, contact_uuid=contact_uuid)

    def _execute_query(
        self, query: sql.expression.Select, contact_uuid: UUID
    ) -> EntityCollection[entities.ContactRelatedCase]:
        with TimedInMilliseconds(
            "get_contact_related_cases_list", self.MAX_QUERYTIME_COUNT_MS
        ) as executed_in_time:
            result = self.session.execute(query).fetchall()

        rows = []
        for row in result:
            rows.append(self._inflate_row_to_entity(row, contact_uuid))

        total_results = None
        if executed_in_time():
            total_results = self._get_count(query)

        return EntityCollection[entities.ContactRelatedCase](
            rows, total_results=total_results
        )

    def _inflate_row_to_entity(
        self, row, contact_uuid: UUID
    ) -> entities.ContactRelatedCase:
        roles = []
        is_authorized = None
        mapping = {
            "uuid": "uuid",
            "entity_id": "uuid",
            "number": "case_id",
            "result": "result",
            "status": "status",
            "progress": "progress",
            "summary": "summary",
            "entity_meta_summary": "summary",
            "archival_state": "archival_state",
            "destruction_date": "vernietigingsdatum",
            "registration_date": "registration_date",
        }

        case_type_version_mapping = {
            "uuid": "case_type_version_uuid",
            "entity_id": "case_type_version_uuid",
            "entity_meta_summary": "case_type_version_title",
        }

        entity_data = {}
        for key, objkey in mapping.items():
            entity_data[key] = getattr(row, objkey)

        entity_data["case_type"] = {}
        for key, objkey in case_type_version_mapping.items():
            entity_data["case_type"][key] = getattr(row, objkey)

        days_left = None

        if row.days_left is not None:
            days_left = {
                "amount": row.days_left.days,
                "unit": "days",
            }

        entity_data["days_left"] = days_left

        if str(contact_uuid) in row.betrokkenen_cache:
            contact_cache = row.betrokkenen_cache[str(contact_uuid)]

            is_authorized = contact_cache["pip_authorized"]
            contact_roles = contact_cache["roles"]

            for contact_role in contact_roles.values():
                roles.append(contact_role["role"])

        entity_data["roles"] = roles
        entity_data["is_authorized"] = is_authorized
        return entities.ContactRelatedCase.parse_obj(
            {**entity_data, "_event_service": self.event_service}
        )

    def _get_base_case_query(self):
        count_phases = (
            sql.select(sql.func.count(schema.ZaaktypeStatus.id))
            .where(
                schema.ZaaktypeStatus.zaaktype_node_id
                == schema.ZaaktypeNode.id
            )
            .scalar_subquery()
            .label("count_phases")
        )

        return sql.select(
            schema.Case.uuid,
            schema.Case.id.label("case_id"),
            schema.Case.onderwerp.label("summary"),
            schema.Case.resultaat.label("result"),
            schema.Case.status,
            schema.Case.betrokkenen_cache,
            (
                sql.cast(schema.Case.milestone, sqltypes.Float) / count_phases
            ).label("progress"),
            schema.Case.archival_state.label("archival_state"),
            schema.Case.vernietigingsdatum.label("vernietigingsdatum"),
            schema.ZaaktypeNode.uuid.label("case_type_version_uuid"),
            schema.ZaaktypeNode.titel.label("case_type_version_title"),
            schema.ZaakBag.bag_id,
            schema.ZaakBag.bag_type,
            case(
                (schema.Case.status == "stalled", None),
                else_=(
                    case(
                        (
                            schema.Case.streefafhandeldatum.is_(None),
                            sql.func.current_date(),
                        ),
                        else_=schema.Case.streefafhandeldatum,
                    )
                    - case(
                        (
                            schema.Case.afhandeldatum.is_(None),
                            sql.func.current_date(),
                        ),
                        else_=schema.Case.afhandeldatum,
                    )
                ),
            ).label("days_left"),
            schema.Case.registratiedatum.label("registration_date"),
        ).select_from(
            sql.join(
                schema.Case,
                schema.ZaaktypeNode,
                schema.ZaaktypeNode.id == schema.Case.zaaktype_node_id,
            ).join(
                schema.ZaakBag,
                schema.Case.locatie_zaak == schema.ZaakBag.id,
                isouter=True,
            )
        )

    def _get_contact_type(self, contact_uuid):
        natural_person = (
            sql.select(
                sql.literal_column("'natuurlijk_persoon'").label("type"),
                schema.NatuurlijkPersoon.id,
                schema.Adres.bag_id.label("location"),
            )
            .select_from(
                sql.join(
                    schema.NatuurlijkPersoon,
                    schema.Adres,
                    schema.NatuurlijkPersoon.adres_id == schema.Adres.id,
                    isouter=True,
                )
            )
            .where(schema.NatuurlijkPersoon.uuid == contact_uuid)
        )

        organization = sql.select(
            sql.literal_column("'bedrijf'").label("type"),
            schema.Bedrijf.id,
            sql.cast(schema.Bedrijf.vestiging_bag_id, sqltypes.String).label(
                "location"
            ),
        ).where(schema.Bedrijf.uuid == contact_uuid)

        employee = sql.select(
            sql.literal_column("'medewerker'").label("type"),
            schema.Subject.id,
            sql.null().label("location"),
        ).where(
            sql.and_(
                schema.Subject.uuid == contact_uuid,
                schema.Subject.subject_type == "employee",
            )
        )

        return self.session.execute(
            sql.union_all(natural_person, organization, employee)
        ).fetchone()
