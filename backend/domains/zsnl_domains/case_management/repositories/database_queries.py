# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..constants import FIRST_PHASE_STATUS
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import aliased
from zsnl_domains.database import schema


def is_simple_case_type_query(case_type_version_uuid):
    """Query to determine if it is possible to create a case with
    the specified case_type version with the v2 API create_case

    :param case_type_uuid: case_type_version uuid
    :type uuid: UUID
    :return: True or False
    :rtype
    """

    zaaktype_joined = sql.join(
        schema.ZaaktypeStatus,
        schema.ZaaktypeNode,
        sql.and_(
            schema.ZaaktypeStatus.status == FIRST_PHASE_STATUS,
            schema.ZaaktypeStatus.zaaktype_node_id == schema.ZaaktypeNode.id,
            schema.ZaaktypeNode.uuid == case_type_version_uuid,
            sql.and_(
                sql.func.coalesce(
                    sql.cast(
                        schema.ZaaktypeNode.adres_andere_locatie,
                        sqltypes.Boolean,
                    ),
                    False,
                ).is_(False),
                sql.func.coalesce(
                    sql.cast(
                        schema.ZaaktypeNode.adres_aanvrager, sqltypes.Boolean
                    ),
                    False,
                ).is_(False),
            ),
        ),
    )

    qry_stmt = sql.select(1).select_from(zaaktype_joined)

    return qry_stmt


def _allocation_related_to_case_type_query():
    allocation_related_to_case_type = (
        sql.select(
            sql.func.json_build_object(
                "department",
                sql.func.json_build_object(
                    "uuid", schema.Group.uuid, "name", schema.Group.name
                ),
                "role",
                sql.func.json_build_object(
                    "uuid", schema.Role.uuid, "name", schema.Role.name
                ),
                "role_set",
                schema.ZaaktypeStatus.role_set,
            )
        )
        .where(
            sql.and_(
                schema.Group.id == schema.ZaaktypeStatus.ou_id,
                schema.Role.id == schema.ZaaktypeStatus.role_id,
            )
        )
        .label("allocation_related_to_case_type_query")
    )
    return allocation_related_to_case_type


def _case_type_requestors_query():
    return sql.func.array(
        sql.select(schema.ZaaktypeBetrokkenen.betrokkene_type)
        .where(
            schema.ZaaktypeBetrokkenen.zaaktype_node_id
            == schema.ZaaktypeNode.id
        )
        .label("case_type_requestors_query")
    )


def _case_type_results_query():
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "uuid",
                schema.ZaaktypeResultaten.uuid,
                "name",
                sql.func.coalesce(
                    schema.ZaaktypeResultaten.label,
                    schema.ZaaktypeResultaten.resultaat,
                ),
                "result",
                schema.ZaaktypeResultaten.resultaat,
                "trigger_archival",
                schema.ZaaktypeResultaten.trigger_archival,
                "preservation_term_label",
                schema.ResultPreservationTerms.label,
                "preservation_term_unit",
                schema.ResultPreservationTerms.unit,
                "preservation_term_unit_amount",
                schema.ResultPreservationTerms.unit_amount,
            )
        )
        .select_from(
            sql.join(
                schema.ZaaktypeResultaten,
                schema.ResultPreservationTerms,
                schema.ResultPreservationTerms.code
                == schema.ZaaktypeResultaten.bewaartermijn,
            )
        )
        .where(
            schema.ZaaktypeResultaten.zaaktype_node_id
            == schema.ZaaktypeNode.id
        )
        .order_by(
            schema.ZaaktypeResultaten.id
        )  # the order is important for selecting the correct
        .label("case_type_results_query")
    )


def _cases_related_to_case_type_query(case_type_node_id):
    related_case_type = aliased(schema.Zaaktype)
    related_case_type_node = aliased(schema.ZaaktypeNode)

    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "type_of_relation",
                schema.ZaaktypeRelatie.relatie_type,
                "related_casetype_element",
                related_case_type_node.uuid,
                "copy_custom_fields_from_parent",
                sql.cast(
                    schema.ZaaktypeRelatie.kopieren_kenmerken,
                    sqltypes.Boolean,
                ),
                "start_on_transition",
                sql.cast(
                    schema.ZaaktypeRelatie.status.isnot(None),
                    sqltypes.Boolean,
                ),
                "open_case_on_create",
                schema.ZaaktypeRelatie.automatisch_behandelen,
                "resolve_before_phase",
                schema.ZaaktypeRelatie.required,
                "show_in_pip",
                schema.ZaaktypeRelatie.show_in_pip,
                "label_in_pip",
                schema.ZaaktypeRelatie.pip_label,
                "requestor",
                sql.func.json_build_object(
                    "requestor_type",
                    schema.ZaaktypeRelatie.eigenaar_type,
                    "related_role",
                    schema.ZaaktypeRelatie.eigenaar_role,
                ),
                "allocation",
                sql.func.json_build_object(
                    "department",
                    sql.func.json_build_object(
                        "uuid",
                        schema.Group.uuid,
                        "name",
                        schema.Group.name,
                    ),
                    "role",
                    sql.func.json_build_object(
                        "uuid", schema.Role.uuid, "name", schema.Role.name
                    ),
                ),
            )
        )
        .where(
            sql.and_(
                schema.ZaaktypeNode.id == case_type_node_id,
                schema.ZaaktypeRelatie.zaaktype_node_id
                == schema.ZaaktypeNode.id,
                schema.ZaaktypeRelatie.zaaktype_status_id
                == schema.ZaaktypeStatus.id,
                schema.Group.id == schema.ZaaktypeRelatie.ou_id,
                schema.Role.id == schema.ZaaktypeRelatie.role_id,
                schema.ZaaktypeRelatie.relatie_zaaktype_id
                == related_case_type.id,
                related_case_type_node.id
                == related_case_type.zaaktype_node_id,
            )
        )
        .label("cases_related_to_case_type_query")
    )


def _case_type_phases_query(case_type_node_id):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "name",
                schema.ZaaktypeStatus.naam,
                "milestone",
                schema.ZaaktypeStatus.status,
                "phase",
                schema.ZaaktypeStatus.fase,
                "allocation",
                _allocation_related_to_case_type_query(),
                "cases",
                _cases_related_to_case_type_query(case_type_node_id),
                "custom_fields",
                _custom_fields_related_to_case_type_query(case_type_node_id),
                "documents",
                _documents_related_to_case_type_query(case_type_node_id),
                "emails",
                _emails_related_to_case_type_query(case_type_node_id),
                "subjects",
                _subjects_related_to_case_type_query(case_type_node_id),
                "checklist_items",
                _checklist_items_related_to_case_type_query(),
                "rules",
                _rules_related_to_phase_query(case_type_node_id),
            )
        )
        .where(
            sql.and_(
                schema.ZaaktypeStatus.zaaktype_node_id
                == schema.ZaaktypeNode.id,
                schema.ZaaktypeNode.id == case_type_node_id,
            )
        )
        .order_by(schema.ZaaktypeStatus.status)
        .label("case_type_phases_query")
    )


def case_type_version_by_id_query(case_type_node_id):
    return sql.select(
        sql.func.json_build_object(
            "id",
            schema.ZaaktypeNode.uuid,
            "uuid",
            schema.ZaaktypeNode.uuid,
            "case_type_uuid",
            schema.Zaaktype.uuid,
            "version",
            schema.ZaaktypeNode.version,
            "active",
            schema.Zaaktype.active,
            "name",
            schema.ZaaktypeNode.titel,
            "created",
            schema.ZaaktypeNode.created,
            "deleted",
            schema.ZaaktypeNode.deleted,
            "last_modified",
            schema.ZaaktypeNode.last_modified,
            "initiator_source",
            schema.ZaaktypeNode.trigger,
            "identification",
            schema.ZaaktypeNode.code,
            "is_public",
            schema.ZaaktypeNode.is_public,
            "tags",
            schema.ZaaktypeNode.zaaktype_trefwoorden,
            "description",
            schema.ZaaktypeNode.zaaktype_omschrijving,
            "enable_allocation_on_form",
            schema.ZaaktypeNode.toewijzing_zaakintake,
            "allow_reuse_casedata",
            schema.ZaaktypeNode.aanvrager_hergebruik,
            "enable_subject_relations_on_form",
            schema.ZaaktypeNode.extra_relaties_in_aanvraag,
            "open_case_on_create",
            schema.ZaaktypeNode.automatisch_behandelen,
            "require_email_on_webform",
            schema.ZaaktypeNode.contact_info_email_required,
            "require_mobilenumber_on_webform",
            schema.ZaaktypeNode.contact_info_mobile_phone_required,
            "require_phonenumber_on_webform",
            schema.ZaaktypeNode.contact_info_phone_required,
            "show_contact_info",
            schema.ZaaktypeNode.contact_info_intake,
            "enable_online_payment",
            schema.ZaaktypeNode.online_betaling,
            "enable_webform",
            schema.ZaaktypeNode.webform_toegang,
            "disable_pip_for_requestor",
            schema.ZaaktypeNode.prevent_pip,
            "use_requestor_address_for_correspondence",
            schema.ZaaktypeNode.adres_aanvrager,
            "properties",
            sql.cast(schema.ZaaktypeNode.properties, sqltypes.JSON),
            "initiator_type",
            schema.ZaaktypeDefinitie.handelingsinitiator,
            "webform_amount",
            schema.ZaaktypeDefinitie.pdc_tarief,
            "legal_basis",
            schema.ZaaktypeDefinitie.grondslag,
            "process_description",
            schema.ZaaktypeDefinitie.procesbeschrijving,
            "case_summary",
            sql.func.coalesce(schema.ZaaktypeDefinitie.extra_informatie, ""),
            "case_public_summary",
            sql.func.coalesce(
                schema.ZaaktypeDefinitie.extra_informatie_extern, ""
            ),
            "terms",
            sql.func.json_build_object(
                "lead_time_legal",
                sql.func.json_build_object(
                    "type",
                    schema.ZaaktypeDefinitie.afhandeltermijn_type,
                    "value",
                    schema.ZaaktypeDefinitie.afhandeltermijn,
                ),
                "lead_time_service",
                sql.func.json_build_object(
                    "type",
                    schema.ZaaktypeDefinitie.servicenorm_type,
                    "value",
                    schema.ZaaktypeDefinitie.servicenorm,
                ),
            ),
            "catalog_folder",
            sql.func.json_build_object(
                "uuid",
                schema.BibliotheekCategorie.uuid,
                "name",
                schema.BibliotheekCategorie.naam,
            ),
            "phases",
            _case_type_phases_query(case_type_node_id),
            "requestor",
            sql.func.json_build_object(
                "type_of_requestors",
                _case_type_requestors_query(),
                "use_for_correspondence",
                sql.cast(
                    sql.func.coalesce(schema.ZaaktypeNode.adres_aanvrager, 0)
                    == 1,
                    sqltypes.Boolean,
                ),
            ),
            "preset_requestor",
            _case_type_preset_requestor_query(),
            "preset_assignee",
            _case_type_preset_assignee_query(),
            "results",
            _case_type_results_query(),
        )
    ).where(
        sql.and_(
            schema.ZaaktypeNode.id == case_type_node_id,
            schema.Zaaktype.id == schema.ZaaktypeNode.zaaktype_id,
            schema.Zaaktype.deleted.is_(None),
            schema.ZaaktypeDefinitie.id
            == schema.ZaaktypeNode.zaaktype_definitie_id,
            schema.BibliotheekCategorie.id
            == schema.Zaaktype.bibliotheek_categorie_id,
        )
    )


def case_type_version_by_list_uuids_query(case_type_ids):
    return sql.select(
        schema.ZaaktypeNode.uuid.label("uuid"),
        schema.Zaaktype.uuid.label("case_type_uuid"),
        schema.ZaaktypeNode.version.label("version"),
        schema.Zaaktype.active.label("active"),
        schema.ZaaktypeNode.titel.label("name"),
        schema.ZaaktypeNode.created.label("created"),
        schema.ZaaktypeNode.deleted.label("deleted"),
        schema.ZaaktypeNode.last_modified.label("last_modified"),
        schema.ZaaktypeNode.trigger.label("initiator_source"),
        schema.ZaaktypeNode.code.label("identification"),
        schema.ZaaktypeNode.is_public.label("is_public"),
        schema.ZaaktypeNode.zaaktype_trefwoorden.label("tags"),
        schema.ZaaktypeNode.zaaktype_omschrijving.label("description"),
        sql.cast(schema.ZaaktypeNode.properties, sqltypes.JSON).label(
            "properties"
        ),
        schema.ZaaktypeDefinitie.handelingsinitiator.label("initiator_type"),
        schema.ZaaktypeDefinitie.pdc_tarief.label("webform_amount"),
        schema.ZaaktypeDefinitie.procesbeschrijving.label(
            "process_description"
        ),
        sql.func.coalesce(schema.ZaaktypeDefinitie.extra_informatie, "").label(
            "case_summary"
        ),
        sql.func.coalesce(
            schema.ZaaktypeDefinitie.extra_informatie_extern, ""
        ).label("case_public_summary"),
        schema.ZaaktypeDefinitie.grondslag.label("legal_basis"),
        sql.func.json_build_object(
            "lead_time_legal",
            sql.func.json_build_object(
                "type",
                schema.ZaaktypeDefinitie.afhandeltermijn_type,
                "value",
                schema.ZaaktypeDefinitie.afhandeltermijn,
            ),
            "lead_time_service",
            sql.func.json_build_object(
                "type",
                schema.ZaaktypeDefinitie.servicenorm_type,
                "value",
                schema.ZaaktypeDefinitie.servicenorm,
            ),
        ).label("terms"),
        sql.func.json_build_object(
            "uuid",
            schema.BibliotheekCategorie.uuid,
            "name",
            schema.BibliotheekCategorie.naam,
        ).label("catalog_folder"),
    ).where(
        sql.and_(
            schema.ZaaktypeNode.uuid.in_(case_type_ids),
            schema.Zaaktype.id == schema.ZaaktypeNode.zaaktype_id,
            schema.Zaaktype.deleted.is_(None),
            schema.ZaaktypeDefinitie.id
            == schema.ZaaktypeNode.zaaktype_definitie_id,
            schema.BibliotheekCategorie.id
            == schema.Zaaktype.bibliotheek_categorie_id,
        )
    )


def _custom_fields_related_to_case_type_query(case_type_node_id):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "id",
                schema.BibliotheekKenmerk.id,
                "name",
                schema.BibliotheekKenmerk.naam,
                "uuid",
                schema.BibliotheekKenmerk.uuid,
                "public_name",
                schema.BibliotheekKenmerk.naam_public,
                "title",
                schema.ZaaktypeKenmerk.label,
                "is_multiple",
                schema.BibliotheekKenmerk.type_multiple,
                "title_multiple",
                schema.ZaaktypeKenmerk.label_multiple,
                "description",
                schema.ZaaktypeKenmerk.help,
                "external_description",
                schema.ZaaktypeKenmerk.help_extern,
                "is_required",
                schema.ZaaktypeKenmerk.value_mandatory,
                "requestor_can_change_from_pip",
                sql.func.coalesce(
                    schema.ZaaktypeKenmerk.pip_can_change, False
                ),
                "is_hidden_field",
                sql.func.coalesce(
                    schema.ZaaktypeKenmerk.is_systeemkenmerk, False
                ),
                "enable_skip_of_queue",
                sql.func.coalesce(
                    sql.cast(
                        schema.ZaaktypeKenmerk.properties, sqltypes.JSON
                    ).op("->>")("skip_change_approval"),
                    "0",
                )
                == "1",
                "sensitive_data",
                sql.func.coalesce(
                    sql.cast(
                        schema.BibliotheekKenmerk.properties, sqltypes.JSON
                    ).op("->>")("sensitive_field"),
                    "off",
                )
                == "on",
                "field_magic_string",
                schema.BibliotheekKenmerk.magic_string,
                "field_type",
                schema.BibliotheekKenmerk.value_type,
                "default_value",
                schema.BibliotheekKenmerk.value_default,
                "referential",
                sql.cast(
                    sql.func.coalesce(
                        schema.ZaaktypeKenmerk.referential, False
                    ).is_(True),
                    sqltypes.Boolean,
                ),
                "publish_on",
                _publications_related_to_custom_field_query(),
                "edit_authorizations",
                _edit_authorizations_related_to_custom_field_query(),
                "field_options",
                _options_related_to_custom_field_query(),
                "date_field_limit",
                _date_limit_related_to_custom_field_query(),
                "relationship_type",
                schema.BibliotheekKenmerk.relationship_type,
                "relationship_name",
                schema.BibliotheekKenmerk.relationship_name,
                "relationship_uuid",
                schema.BibliotheekKenmerk.relationship_uuid,
                "relationship_subject_role",
                sql.func.coalesce(
                    sql.cast(
                        schema.ZaaktypeKenmerk.properties, sqltypes.JSON
                    ).op("->>")("relationship_subject_role"),
                    "",
                ),
            )
        )
        .where(
            sql.and_(
                schema.ZaaktypeKenmerk.zaak_status_id
                == schema.ZaaktypeStatus.id,
                schema.ZaaktypeKenmerk.zaaktype_node_id
                == schema.ZaaktypeNode.id,
                schema.BibliotheekKenmerk.id
                == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                schema.ZaaktypeNode.id == case_type_node_id,
            )
        )
        .label("custom_fields_related_to_case_type_query")
    )


def _publications_related_to_custom_field_query():
    publications = []
    if schema.ZaaktypeKenmerk.pip:
        publications.append({"name": "pip"})
    if schema.ZaaktypeKenmerk.publish_public:
        publications.append({"name": "web"})
    return publications


def _edit_authorizations_related_to_custom_field_query():
    custom_field_selected_units = sql.cast(
        schema.ZaaktypeKenmerk.required_permissions, sqltypes.JSON
    ).op("->")("selectedUnits")

    authorizations_sub_query = sql.func.json_array_elements(
        custom_field_selected_units
    ).lateral("units")

    custom_field_alias = aliased(schema.ZaaktypeKenmerk)

    authorizations_joined = sql.join(
        custom_field_alias,
        authorizations_sub_query,
        custom_field_alias.id == schema.ZaaktypeKenmerk.id,
    )

    custom_field_joined = authorizations_joined.join(
        schema.Group,
        schema.Group.id
        == sql.cast(
            sql.column(authorizations_sub_query.name).op("->>")("org_unit_id"),
            sqltypes.Integer,
        ),
    ).join(
        schema.Role,
        schema.Role.id
        == sql.cast(
            sql.column(authorizations_sub_query.name).op("->>")("role_id"),
            sqltypes.Integer,
        ),
    )

    authorizations_json = sql.func.json_build_object(
        "department",
        sql.func.json_build_object(
            "uuid", schema.Group.uuid, "name", schema.Group.name
        ),
        "role",
        sql.func.json_build_object(
            "uuid", schema.Role.uuid, "name", schema.Role.name
        ),
    )

    stmt = (
        sql.select(authorizations_json)
        .select_from(custom_field_joined)
        .label("edit_authorizations_related_to_custom_field")
    )
    return sql.func.array(stmt)


def _options_related_to_custom_field_query():
    options_related_to_custom_field_query = (
        sql.select(schema.BibliotheekKenmerkenValues.value)
        .where(
            schema.BibliotheekKenmerkenValues.bibliotheek_kenmerken_id
            == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
        )
        .label("options_related_to_custom_field")
    )

    return sql.func.array(options_related_to_custom_field_query).label(
        "options_related_to_custom_field"
    )


def case_action_data_from_sub_case_query(
    case_type_version_uuid, sub_case_uuid, status_milestone
):
    sub_case_type = aliased(schema.ZaaktypeNode)
    parent_case_type = aliased(schema.ZaaktypeNode)

    qry_stmt = sql.select(
        sql.func.json_build_object(
            "id",
            schema.ZaaktypeRelatie.id,
            "zaaktype_node_id",
            schema.ZaaktypeRelatie.zaaktype_node_id,
            "zaaktype_status_id",
            schema.ZaaktypeRelatie.zaaktype_status_id,
            "relatie_zaaktype_id",
            schema.ZaaktypeRelatie.relatie_zaaktype_id,
            "relatie_type",
            schema.ZaaktypeRelatie.relatie_type,
            "start_delay",
            schema.ZaaktypeRelatie.start_delay,
            "status",
            schema.ZaaktypeRelatie.status,
            "kopieren_kenmerken",
            schema.ZaaktypeRelatie.kopieren_kenmerken,
            "ou_id",
            schema.ZaaktypeRelatie.ou_id,
            "role_id",
            schema.ZaaktypeRelatie.role_id,
            "automatisch_behandelen",
            schema.ZaaktypeRelatie.automatisch_behandelen,
            "required",
            schema.ZaaktypeRelatie.required,
            "betrokkene_authorized",
            schema.ZaaktypeRelatie.betrokkene_authorized,
            "betrokkene_notify",
            schema.ZaaktypeRelatie.betrokkene_notify,
            "betrokkene_id",
            schema.ZaaktypeRelatie.betrokkene_id,
            "betrokkene_role",
            schema.ZaaktypeRelatie.betrokkene_role,
            "betrokkene_role_set",
            schema.ZaaktypeRelatie.betrokkene_role_set,
            "betrokkene_prefix",
            schema.ZaaktypeRelatie.betrokkene_prefix,
            "eigenaar_type",
            schema.ZaaktypeRelatie.eigenaar_type,
            "eigenaar_role",
            schema.ZaaktypeRelatie.eigenaar_role,
            "eigenaar_id",
            schema.ZaaktypeRelatie.eigenaar_id,
            "show_in_pip",
            schema.ZaaktypeRelatie.show_in_pip,
            "pip_label",
            schema.ZaaktypeRelatie.pip_label,
            "created",
            schema.ZaaktypeRelatie.created,
            "last_modified",
            schema.ZaaktypeRelatie.last_modified,
        )
    ).where(
        sql.and_(
            parent_case_type.uuid == case_type_version_uuid,
            sub_case_type.uuid == sub_case_uuid,
            schema.ZaaktypeStatus.zaaktype_node_id == parent_case_type.id,
            schema.ZaaktypeStatus.status == status_milestone,
            schema.ZaaktypeRelatie.zaaktype_node_id == parent_case_type.id,
            schema.ZaaktypeRelatie.zaaktype_status_id
            == schema.ZaaktypeStatus.id,
            schema.ZaaktypeRelatie.relatie_zaaktype_id
            == sub_case_type.zaaktype_id,
        )
    )
    return qry_stmt


def case_action_status_query(case_type_uuid, status_milestone):
    qry_stmt = sql.select(schema.ZaaktypeStatus.id).where(
        sql.and_(
            schema.ZaaktypeStatus.status == status_milestone,
            schema.ZaaktypeNode.uuid == case_type_uuid,
            schema.ZaaktypeStatus.zaaktype_node_id == schema.ZaaktypeNode.id,
        )
    )
    return qry_stmt


def case_action_label_query(sub_case_uuid):
    qry_stmt = sql.select(schema.ZaaktypeNode.titel).where(
        schema.ZaaktypeNode.uuid == sub_case_uuid
    )
    return qry_stmt


def ids_related_to_case_query(case_uuid):
    return sql.select(
        sql.func.json_build_object(
            "case_id",
            schema.Case.id,
            "case_type_id",
            schema.Case.zaaktype_id,
            "case_type_node_id",
            schema.Case.zaaktype_node_id,
            "case_cordinator_id",
            schema.Case.coordinator_gm_id,
            "case_assignee_id",
            schema.Case.behandelaar_gm_id,
            "department_id",
            schema.Case.route_ou,
            "role_id",
            schema.Case.route_role,
        )
    ).where(schema.Case.uuid == case_uuid)


def _documents_related_to_case_type_query(case_type_node_id):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "automatic",
                sql.func.coalesce(
                    sql.cast(
                        schema.ZaaktypeSjabloon.automatisch_genereren,
                        sqltypes.Boolean,
                    ),
                    False,
                ),
                "label",
                schema.BibliotheekSjabloon.naam,
                "data",
                sql.func.json_build_object(
                    "description",
                    "Sjabloon",
                    "bibliotheek_sjablonen_id",
                    schema.ZaaktypeSjabloon.bibliotheek_sjablonen_id,
                    "bibliotheek_kenmerken_id",
                    schema.ZaaktypeSjabloon.bibliotheek_kenmerken_id,
                    "filename",
                    schema.Filestore.original_name,
                    "target_format",
                    sql.func.coalesce(
                        schema.ZaaktypeSjabloon.target_format, "odt"
                    ),
                    "automatisch_genereren",
                    sql.func.coalesce(
                        sql.cast(
                            schema.ZaaktypeSjabloon.automatisch_genereren,
                            sqltypes.Boolean,
                        ),
                        False,
                    ),
                ),
            )
        )
        .where(
            sql.and_(
                schema.Filestore.id == schema.BibliotheekSjabloon.filestore_id,
                schema.ZaaktypeSjabloon.bibliotheek_sjablonen_id
                == schema.BibliotheekSjabloon.id,
                schema.Zaaktype.zaaktype_node_id
                == schema.ZaaktypeSjabloon.zaaktype_node_id,
                schema.Zaaktype.zaaktype_node_id == case_type_node_id,
                schema.ZaaktypeSjabloon.zaak_status_id
                == schema.ZaaktypeStatus.id,
            )
        )
        .label("documents_related_to_case_type_query")
    )


def _get_case_document_attachments_query(case_type_node_id):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "selected",
                1,
                "name",
                schema.BibliotheekKenmerk.naam,
                "case_document_ids",
                schema.ZaaktypeKenmerk.id,
            )
        )
        .where(
            sql.and_(
                schema.BibliotheekNotificatie.id
                == schema.BibliotheekNotificatieKenmerk.bibliotheek_notificatie_id,
                schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekNotificatieKenmerk.bibliotheek_kenmerken_id,
                schema.BibliotheekNotificatieKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekKenmerk.id,
                schema.ZaaktypeNode.id == case_type_node_id,
                schema.ZaaktypeKenmerk.zaaktype_node_id
                == schema.ZaaktypeNode.id,
            )
        )
        .label("case_document_attachments")
    )


def _emails_related_to_case_type_query(case_type_node_id):
    emails_related_to_case_type_query = sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "automatic",
                (
                    sql.func.coalesce(
                        schema.ZaaktypeNotificatie.intern_block, 0
                    )
                    == 0
                )
                and (
                    sql.func.coalesce(schema.ZaaktypeNotificatie.automatic, 0)
                    == 1
                ),
                "label",
                schema.BibliotheekNotificatie.label,
                "data",
                sql.func.json_build_object(
                    "description",
                    "E-mail",
                    "rcpt",
                    schema.ZaaktypeNotificatie.rcpt,
                    "subject",
                    schema.BibliotheekNotificatie.subject,
                    "body",
                    schema.BibliotheekNotificatie.message,
                    "sender_address",
                    schema.BibliotheekNotificatie.sender_address,
                    "sender",
                    schema.BibliotheekNotificatie.sender,
                    "cc",
                    schema.ZaaktypeNotificatie.cc,
                    "bcc",
                    schema.ZaaktypeNotificatie.bcc,
                    "betrokkene_role",
                    schema.ZaaktypeNotificatie.betrokkene_role,
                    "email",
                    schema.ZaaktypeNotificatie.email,
                    "behandelaar",
                    schema.ZaaktypeNotificatie.behandelaar,
                    "intern_block",
                    schema.ZaaktypeNotificatie.intern_block,
                    "case_document_attachments",
                    _get_case_document_attachments_query(case_type_node_id),
                    "automatic_phase",
                    sql.func.coalesce(
                        sql.cast(
                            schema.ZaaktypeNotificatie.automatic,
                            sqltypes.Boolean,
                        ),
                        False,
                    ),
                    "zaaktype_notificatie_id",
                    schema.ZaaktypeNotificatie.id,
                    "bibliotheek_notificaties_uuid",
                    schema.BibliotheekNotificatie.uuid,
                    "bibliotheek_notificaties_id",
                    schema.ZaaktypeNotificatie.bibliotheek_notificatie_id,
                ),
            )
        )
        .where(
            sql.and_(
                schema.ZaaktypeNotificatie.zaaktype_node_id
                == case_type_node_id,
                schema.BibliotheekNotificatie.id
                == schema.ZaaktypeNotificatie.bibliotheek_notificatie_id,
                schema.ZaaktypeNotificatie.zaak_status_id
                == schema.ZaaktypeStatus.id,
            )
        )
        .label("emails_related_to_case_type_query")
    )

    return emails_related_to_case_type_query


def _subjects_related_to_case_type_query(case_type_node_id):
    subjects_related_to_case_type_query = sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "label",
                schema.ZaaktypeStandaardBetrokkenen.naam,
                "automatic",
                False,
                "data",
                sql.func.json_build_object(
                    "betrokkene_identifier",
                    schema.ZaaktypeStandaardBetrokkenen.betrokkene_identifier,
                    "naam",
                    schema.ZaaktypeStandaardBetrokkenen.naam,
                    "rol",
                    schema.ZaaktypeStandaardBetrokkenen.rol,
                    "magic_string_prefix",
                    schema.ZaaktypeStandaardBetrokkenen.magic_string_prefix,
                    "gemachtigd",
                    schema.ZaaktypeStandaardBetrokkenen.gemachtigd,
                    "notify",
                    schema.ZaaktypeStandaardBetrokkenen.notify,
                    "uuid",
                    schema.ZaaktypeStandaardBetrokkenen.uuid,
                    "betrokkene_type",
                    schema.ZaaktypeStandaardBetrokkenen.betrokkene_type,
                ),
            )
        )
        .where(
            sql.and_(
                schema.ZaaktypeStandaardBetrokkenen.zaaktype_node_id
                == schema.ZaaktypeNode.id,
                schema.ZaaktypeStandaardBetrokkenen.zaak_status_id
                == schema.ZaaktypeStatus.id,
                schema.ZaaktypeNode.id == case_type_node_id,
            )
        )
        .label("subjects_related_to_case_type_query")
    )

    return subjects_related_to_case_type_query


def _date_limit_related_to_custom_field_query():
    date_limit = sql.cast(schema.ZaaktypeKenmerk.properties, sqltypes.JSON)[
        "date_limit"
    ]
    start_limit = sql.cast(date_limit["start"], sqltypes.JSON)
    end_limit = sql.cast(date_limit["end"], sqltypes.JSON)

    date_limit_json = sql.func.json_build_object(
        "start",
        sql.func.json_build_object(
            "active",
            sql.cast(
                sql.func.coalesce(start_limit.op("->>")("active"), "0") == "1",
                sqltypes.Boolean,
            ),
            "interval_type",
            start_limit["term"],
            "interval",
            sql.cast(
                sql.func.coalesce(
                    sql.func.nullif(start_limit.op("->>")("num"), ""), "0"
                ),
                sqltypes.Integer,
            ),
            "during",
            start_limit["during"],
            "reference",
            start_limit["reference"],
        ),
        "end",
        sql.func.json_build_object(
            "active",
            sql.cast(
                sql.func.coalesce(end_limit.op("->>")("active"), "0") == "1",
                sqltypes.Boolean,
            ),
            "interval_type",
            end_limit["term"],
            "interval",
            sql.cast(
                sql.func.coalesce(
                    sql.func.nullif(end_limit.op("->>")("num"), ""), "0"
                ),
                sqltypes.Integer,
            ),
            "during",
            end_limit["during"],
            "reference",
            end_limit["reference"],
        ),
    )

    # if custom_field is not of date type return empty json, else date_limit json
    date_limit_related_to_custom_field = sql.case(
        (
            sql.cast(schema.ZaaktypeKenmerk.properties, sqltypes.JSON)
            .op("->")("date_limit")
            .is_(None),
            sql.cast({}, sqltypes.JSON),
        ),
        else_=sql.cast(date_limit_json, sqltypes.JSON),
    )
    return date_limit_related_to_custom_field


def _checklist_items_related_to_case_type_query():
    return sql.func.array(
        sql.select(schema.ZaaktypeChecklistItem.label)
        .where(
            schema.ZaaktypeChecklistItem.casetype_status_id
            == schema.ZaaktypeStatus.id
        )
        .order_by(schema.ZaaktypeChecklistItem.id)
        .label("_checklist_items_related_to_case_type")
    )


def _case_type_preset_requestor_query():
    contact_uuid = sql.func.get_subject_by_legacy_id(
        schema.ZaaktypeDefinitie.preset_client
    )
    return sql.case(
        (
            sql.or_(
                schema.ZaaktypeDefinitie.preset_client.is_(None),
                schema.ZaaktypeDefinitie.preset_client == "",
            ),
            None,
        ),
        else_=sql.union_all(
            sql.select(
                sql.func.jsonb_build_object(
                    "type",
                    "person",
                    "uuid",
                    schema.NatuurlijkPersoon.uuid,
                    "name",
                    sql.func.get_display_name_for_person(
                        postgresql.hstore(
                            postgresql.array(
                                ["voorletters", "naamgebruik", "geslachtsnaam"]
                            ),
                            postgresql.array(
                                [
                                    schema.NatuurlijkPersoon.voorletters,
                                    schema.NatuurlijkPersoon.naamgebruik,
                                    schema.NatuurlijkPersoon.geslachtsnaam,
                                ]
                            ),
                        )
                    ),
                )
            ).where(schema.NatuurlijkPersoon.uuid == contact_uuid),
            sql.select(
                sql.func.jsonb_build_object(
                    "type",
                    "organization",
                    "uuid",
                    schema.Bedrijf.uuid,
                    "name",
                    sql.func.get_display_name_for_company(
                        postgresql.hstore(
                            postgresql.array(["handelsnaam"]),
                            postgresql.array([schema.Bedrijf.handelsnaam]),
                        )
                    ),
                )
            ).where(schema.Bedrijf.uuid == contact_uuid),
        )
        .limit(1)
        .scalar_subquery(),
    )


def _case_type_preset_assignee_query():
    preset_assignee_indentifier = sql.cast(
        schema.ZaaktypeNode.properties, sqltypes.JSON
    ).op("->>")("preset_owner_identifier")
    assignee_id = sql.cast(
        sql.func.substring(preset_assignee_indentifier, r"\d+"),
        sqltypes.Integer,
    )
    preset_assignee_json = (
        sql.select(
            sql.func.json_build_object(
                "uuid",
                schema.Subject.uuid,
                "name",
                schema.Subject.username,
            )
        )
        .where(schema.Subject.id == assignee_id)
        .scalar_subquery()
    )
    preset_assignee = sql.case(
        (
            sql.or_(
                preset_assignee_indentifier == "",
                preset_assignee_indentifier.is_(None),
            ),
            sql.cast({}, sqltypes.JSON),
        ),
        else_=sql.cast(preset_assignee_json, sqltypes.JSON),
    )
    return preset_assignee


def _rules_related_to_phase_query(case_type_node_id):
    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "name",
                schema.ZaaktypeRegel.naam,
                "created",
                schema.ZaaktypeRegel.created,
                "last_modified",
                schema.ZaaktypeRegel.last_modified,
                "settings",
                schema.ZaaktypeRegel.settings,
                "active",
                schema.ZaaktypeRegel.active,
                "is_group",
                schema.ZaaktypeRegel.is_group,
            )
        )
        .where(
            sql.and_(
                schema.ZaaktypeRegel.zaaktype_node_id == case_type_node_id,
                schema.ZaaktypeRegel.zaak_status_id
                == schema.ZaaktypeStatus.id,
            )
        )
        .label("_rules_related_to_phase_query")
    )
