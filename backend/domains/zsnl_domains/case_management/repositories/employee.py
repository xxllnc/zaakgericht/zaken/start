# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import typing
from ... import ZaaksysteemRepositoryBase
from ..entities import Employee
from ..entities._shared import RelatedCustomObject
from ..entities.employee import (
    EmployeeLimited,
    RelatedDepartment,
    RelatedRole,
    ValidEmployeeStatus,
)
from minty.cqrs.events import Event
from minty.exceptions import NotFound
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.database import schema


def _employee_active_state_query():
    return (
        sql.select(sql.func.coalesce(schema.UserEntity.active, False))
        .where(schema.UserEntity.subject_id == schema.Subject.id)
        .limit(1)
    )


def _employee_department_query():
    return sql.select(
        sql.func.json_build_object(
            "uuid", schema.Group.uuid, "name", schema.Group.name
        )
    ).where(schema.Group.id == schema.Subject.group_ids[1])


def _employee_roles_query():
    subject = sql.alias(schema.Subject)

    role_ids = sql.func.json_array_elements_text(
        sql.func.array_to_json(subject.c.role_ids)
    ).lateral("role_id")

    roles_joined = sql.join(
        subject, role_ids, subject.c.uuid == schema.Subject.uuid
    ).join(
        schema.Role,
        schema.Role.id
        == sql.cast(sql.column(role_ids.name), sqltypes.Integer),
    )

    department = sql.alias(schema.Group)
    department_joined = roles_joined.join(
        department, department.c.id == schema.Role.parent_group_id
    )

    return sql.func.array(
        sql.select(
            sql.func.json_build_object(
                "uuid",
                schema.Role.uuid,
                "name",
                schema.Role.name,
                "department",
                sql.func.json_build_object(
                    "uuid", department.c.uuid, "name", department.c.name
                ),
            )
        )
        .select_from(department_joined)
        .label("roles")
    )


subject_query = sql.select(
    schema.Subject.uuid,
    schema.Subject.username.label("name"),
    sql.func.coalesce(
        sql.cast(schema.Subject.properties, sqltypes.JSON)["givenname"],
        None,
    ).label("first_name"),
    sql.func.coalesce(
        sql.cast(schema.Subject.properties, sqltypes.JSON)["sn"], None
    ).label("surname"),
    sql.func.coalesce(
        sql.cast(schema.Subject.properties, sqltypes.JSON)["title"],
        None,
    ).label("title"),
    sql.func.json_build_object(
        "email",
        sql.func.coalesce(
            sql.cast(schema.Subject.properties, sqltypes.JSON)["mail"],
            None,
        ),
        "phone_number",
        sql.func.coalesce(
            sql.cast(schema.Subject.properties, sqltypes.JSON)[
                "telephonenumber"
            ],
            None,
        ),
    ).label("contact_information"),
    _employee_active_state_query().label("active"),
    _employee_department_query().label("department"),
    _employee_roles_query().label("roles"),
    schema.CustomObject.uuid.label("related_custom_object_uuid"),
    sql.cast(schema.Subject.properties, postgresql.JSON)[
        "displayname"
    ].astext.label("summary"),
).select_from(
    sql.join(
        schema.Subject,
        schema.CustomObject,
        schema.Subject.related_custom_object_id == schema.CustomObject.id,
        isouter=True,
    )
)

subject_limited_query = sql.select(
    schema.Subject.uuid,
    sql.cast(schema.Subject.properties, postgresql.JSON)[
        "displayname"
    ].astext.label("summary"),
)


class EmployeeRepository(ZaaksysteemRepositoryBase):
    def get_employees_by_uuid(
        self, uuids: typing.Iterable[UUID]
    ) -> list[Employee]:
        """Find employee by uuid.

        :param uuid: UUID of the employee
        :type uuid: UUID
        :raises NotFound: If employee not present
        :return: Employee entity
        :rtype: Employee entity
        """

        query = subject_query.where(
            sql.and_(
                schema.Subject.uuid.in_(uuids),
                schema.Subject.subject_type == "employee",
            )
        )
        query_result = self.session.execute(query).fetchall()
        return [self._sqla_to_entity(query_result=row) for row in query_result]

    def find_employee_by_uuid(self, uuid: UUID) -> Employee:
        """Find employee by uuid.

        :param uuid: UUID of the employee
        :type uuid: UUID
        :raises NotFound: If employee not present
        :return: Employee entity
        :rtype: Employee entity
        """

        qry_stmt = subject_query.where(
            sql.and_(
                schema.Subject.uuid == uuid,
                schema.Subject.subject_type == "employee",
            )
        )

        query_result = self.session.execute(qry_stmt).fetchone()

        if not query_result:
            raise NotFound(
                f"Employee with uuid '{uuid}' not found.", "employee/not_found"
            )
        return self._sqla_to_entity(query_result=query_result)

    def find_employee_by_uuid_limited(self, uuid: UUID) -> EmployeeLimited:
        """Find employee by uuid."""

        qry_stmt = subject_limited_query.where(
            sql.and_(
                schema.Subject.uuid == uuid,
                schema.Subject.subject_type == "employee",
            )
        )

        query_result = self.session.execute(qry_stmt).fetchone()

        if not query_result:
            raise NotFound(
                f"Employee with uuid '{uuid}' not found.", "employee/not_found"
            )
        return self._sqla_to_entity_limited(query_result=query_result)

    def _sqla_to_entity(self, query_result):
        """Initialize Employee Entity from sqla object.

        :param query_result: sqla query results
        :type query_result: object
        :return: employee entity
        :rtype: entities.Employee
        """
        department = RelatedDepartment(
            **{
                "entity_id": query_result.department["uuid"],
                "uuid": query_result.department["uuid"],
                "entity_meta_summary": query_result.department["name"],
            }
        )

        roles = []
        for role in query_result.roles:
            roles.append(
                RelatedRole(
                    **{
                        "entity_id": role["uuid"],
                        "uuid": role["uuid"],
                        "entity_meta_summary": role["name"],
                        "parent": {
                            "entity_id": role["department"]["uuid"],
                            "uuid": role["department"]["uuid"],
                            "entity_meta_summary": role["department"]["name"],
                        },
                    }
                )
            )

        related_custom_object_uuid = query_result.related_custom_object_uuid
        is_active = query_result.roles != [] and query_result.active
        employee = Employee(
            uuid=query_result.uuid,
            entity_id=query_result.uuid,
            entity_meta_summary=query_result.summary,
            source="Zaaksysteem",
            status=ValidEmployeeStatus.active
            if query_result.active
            else ValidEmployeeStatus.inactive,
            name=query_result.name,
            title=query_result.title,
            first_name=query_result.first_name,
            surname=query_result.surname,
            roles=roles,
            department=department,
            contact_information=query_result.contact_information,
            is_active=is_active,
            related_custom_object=(
                None
                if related_custom_object_uuid is None
                else RelatedCustomObject(
                    entity_id=related_custom_object_uuid,
                    uuid=related_custom_object_uuid,
                )
            ),
            # Services, etc. needed by the entity
            _event_service=self.event_service,
        )

        return employee

    def _sqla_to_entity_limited(self, query_result) -> EmployeeLimited:
        """Initialize EmployeeLimited Entity from sqla object."""
        employee = EmployeeLimited(
            uuid=query_result.uuid,
            entity_id=query_result.uuid,
            entity_meta_summary=query_result.summary,
        )

        return employee

    def _save_employee(self, event: Event):
        changes = event.format_changes()
        related_custom_object = changes["related_custom_object"]

        if related_custom_object:
            related_custom_object_uuid = related_custom_object["uuid"]
            custom_object_row = self.session.execute(
                sql.select(schema.CustomObject.id).where(
                    schema.CustomObject.uuid == related_custom_object_uuid
                )
            ).fetchone()

            if custom_object_row is None:
                raise NotFound(
                    f"No custom object found with uuid={related_custom_object_uuid}",
                    "employee/related_custom_object_not_found",
                )

            related_custom_object_id = custom_object_row.id
        else:
            related_custom_object_id = None

        self.session.execute(
            sql.update(schema.Subject)
            .where(schema.Subject.uuid == event.entity_id)
            .values(related_custom_object_id=related_custom_object_id)
            .execution_options(synchronize_session=False)
        )

    def save(self):
        for event in self.event_service.event_list:
            if (
                event.entity_type == "Employee"
                and event.event_name == "RelatedCustomObjectSet"
            ):
                self._save_employee(event)
