# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.cqrs.events
from ... import ZaaksysteemRepositoryBase
from .. import entities
from ..entities import ExportFile
from datetime import datetime, timezone
from minty.exceptions import NotFound
from minty_infra_storage import s3
from sqlalchemy import sql
from typing import cast
from uuid import UUID
from zsnl_domains.database import schema

export_file_query = sql.select(
    schema.ExportQueue.uuid.label("uuid"),
    schema.ExportQueue.subject_uuid.label("subject_uuid"),
    schema.ExportQueue.expires.label("expires"),
    schema.ExportQueue.downloaded.label("downloaded"),
    schema.ExportQueue.token.label("token"),
    schema.Filestore.original_name.label("original_name"),
    schema.Filestore.uuid.label("file_uuid"),
    schema.Filestore.mimetype.label("mime_type"),
    schema.Filestore.storage_location.label("storage_location"),
).select_from(
    sql.join(
        schema.ExportQueue,
        schema.Filestore,
        schema.ExportQueue.filestore_id == schema.Filestore.id,
    )
)

DEFAULT_SORT_ORDER = [sql.asc(schema.ExportQueue.expires)]


class ExportFileRepository(ZaaksysteemRepositoryBase):
    _for_entity = "ExportFile"
    _events_to_calls: dict[str, str] = {
        "DownloadCounterIncreased": "_save_download_counter_increased",
    }

    def get_export_file_by_uuid(
        self, uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> ExportFile:
        query = export_file_query.where(
            sql.and_(
                schema.ExportQueue.subject_uuid == user_info.user_uuid,
                schema.ExportQueue.expires > datetime.now(timezone.utc),
                schema.ExportQueue.uuid == uuid,
            )
        )

        export_file = self.session.execute(query).fetchone()
        if not export_file:
            raise NotFound(
                f"File with uuid '{uuid}' not found.",
                "file/not_found",
            )

        return self._entity_from_row(export_file)

    def get_export_file_by_token(
        self, token: str, user_info: minty.cqrs.UserInfo
    ) -> ExportFile:
        query = export_file_query.where(
            sql.and_(
                schema.ExportQueue.subject_uuid == user_info.user_uuid,
                schema.ExportQueue.expires > datetime.now(timezone.utc),
                schema.ExportQueue.token == token,
            )
        )

        export_file = self.session.execute(query).fetchone()

        return self._entity_from_row(export_file)

    def get_export_download_url(
        self, export_file: ExportFile, user_info: minty.cqrs.UserInfo
    ) -> str:
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        download_url = store.get_download_url(
            uuid=export_file.file_uuid,
            storage_location=export_file.storage_location,
            mime_type=export_file.mime_type,
            filename=export_file.name,
            download=True,
        )

        # Downloads are counted by an event handler, so queries do not have
        # to have write access to the database.
        event = minty.cqrs.events.Event.create_basic(
            domain="zsnl_domains_case_management",
            context=self.context,
            entity_type="ExportFile",
            entity_id=export_file.uuid,
            event_name="ExportFileDownloaded",
            entity_data={},
            user_info=user_info,
        )
        self.send_query_event(event)

        return download_url

    def get_export_file_list(
        self, page: int, page_size: int, user_uuid: UUID
    ) -> list[ExportFile]:
        """Get list of Export Files."""

        offset = self._calculate_offset(page, page_size)

        # Get only user specific files
        query = export_file_query.where(
            sql.and_(
                schema.ExportQueue.subject_uuid == user_uuid,
                schema.ExportQueue.expires > datetime.now(timezone.utc),
            )
        )

        export_files = self.session.execute(
            query.order_by(*DEFAULT_SORT_ORDER).limit(page_size).offset(offset)
        ).fetchall()

        return [
            self._entity_from_row(row=export_files_row)
            for export_files_row in export_files
        ]

    def _entity_from_row(self, row) -> ExportFile:
        export_file = entities.ExportFile.parse_obj(
            {
                "uuid": row.uuid,
                "entity_id": row.uuid,
                "name": row.original_name,
                "expires_at": row.expires,
                "downloads": row.downloaded,
                "token": row.token,
                "file_uuid": row.file_uuid,
                "mime_type": row.mime_type,
                "storage_location": row.storage_location[0],
                # Internal:
                "_event_service": self.event_service,
            }
        )
        return export_file

    def _save_download_counter_increased(
        self,
        event: minty.cqrs.events.Event,
        user_info: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        update_statement = (
            sql.update(schema.ExportQueue)
            .where(schema.ExportQueue.uuid == event.entity_id)
            .values(downloaded=schema.ExportQueue.downloaded + 1)
        )

        self.session.execute(update_statement)
