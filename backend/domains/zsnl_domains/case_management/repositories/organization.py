# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import typing
from ... import ZaaksysteemRepositoryBase
from ...shared.util import (
    get_country_name_from_landcode,
    get_landcode_from_country_name,
    get_type_of_business_from_entity_code,
)
from .. import constants
from ..constants import BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION
from ..entities import Organization
from ..entities._shared import RelatedCustomObject
from minty.cqrs import UserInfo
from minty.cqrs.events import Event
from minty.exceptions import NotFound
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.case_management.entities.organization import (
    OrganizationLimited,
)
from zsnl_domains.database import schema


def _organization_contact_person_info_query():
    return sql.func.json_build_object(
        "initials",
        schema.Bedrijf.contact_voorletters,
        "insertions",
        schema.Bedrijf.contact_voorvoegsel,
        "family_name",
        schema.Bedrijf.contact_geslachtsnaam,
        "first_name",
        schema.Bedrijf.contact_naam,
    )


def _organization_contact_info_query():
    return sql.func.json_build_object(
        "email",
        schema.ContactData.email,
        "phone_number",
        schema.ContactData.telefoonnummer,
        "mobile_number",
        schema.ContactData.mobiel,
        "internal_note",
        schema.ContactData.note,
        "preferred_contact_channel",
        schema.Bedrijf.preferred_contact_channel,
        "is_anonymous_contact",
        sql.case(
            (
                schema.Subject.properties.isnot(None),
                sql.cast(schema.Subject.properties, postgresql.JSONB)[
                    "anonymous"
                ].astext.cast(sqltypes.BOOLEAN),
            ),
            else_=False,
        ),
    )


def _organization_location_address_query():
    address_query = sql.func.json_build_object(
        "street",
        schema.Bedrijf.vestiging_straatnaam,
        "zipcode",
        schema.Bedrijf.vestiging_postcode,
        "street_number",
        schema.Bedrijf.vestiging_huisnummer,
        "street_number_letter",
        schema.Bedrijf.vestiging_huisletter,
        "street_number_suffix",
        schema.Bedrijf.vestiging_huisnummertoevoeging,
        "city",
        schema.Bedrijf.vestiging_woonplaats,
        "country",
        schema.Bedrijf.vestiging_landcode,
        "bag_id",
        schema.Bedrijf.vestiging_bag_id,
        "is_foreign",
        False,
        "geo_lat_long",
        schema.Bedrijf.vestiging_latlong,
    )
    foreign_address_query = sql.func.json_build_object(
        "address_line_1",
        schema.Bedrijf.vestiging_adres_buitenland1,
        "address_line_2",
        schema.Bedrijf.vestiging_adres_buitenland2,
        "address_line_3",
        schema.Bedrijf.vestiging_adres_buitenland3,
        "country",
        schema.Bedrijf.vestiging_landcode,
        "bag_id",
        None,
        "is_foreign",
        True,
        "geo_lat_long",
        schema.Bedrijf.vestiging_latlong,
    )
    address = sql.case(
        # No address of this type.
        (
            sql.and_(
                sql.func.coalesce(
                    schema.Bedrijf.vestiging_adres_buitenland1,
                    schema.Bedrijf.vestiging_adres_buitenland2,
                    schema.Bedrijf.vestiging_adres_buitenland3,
                    schema.Bedrijf.vestiging_straatnaam,
                    "",
                )
                == ""
            ),
            None,
        ),
        # 6030 = The Netherlands
        (
            sql.and_(
                schema.Bedrijf.vestiging_straatnaam != "",
                sql.or_(
                    schema.Bedrijf.vestiging_landcode == 5107,
                    schema.Bedrijf.vestiging_landcode == 6030,
                ),
            ),
            sql.cast(address_query, sqltypes.JSON),
        ),
        else_=sql.cast(foreign_address_query, sqltypes.JSON),
    )

    return address


def _organization_correspondence_address_query():
    address_query = sql.func.json_build_object(
        "street",
        schema.Bedrijf.correspondentie_straatnaam,
        "zipcode",
        schema.Bedrijf.correspondentie_postcode,
        "street_number",
        schema.Bedrijf.correspondentie_huisnummer,
        "street_number_letter",
        schema.Bedrijf.correspondentie_huisletter,
        "street_number_suffix",
        schema.Bedrijf.correspondentie_huisnummertoevoeging,
        "city",
        schema.Bedrijf.correspondentie_woonplaats,
        "country",
        schema.Bedrijf.correspondentie_landcode,
        "is_foreign",
        False,
    )
    foreign_address_query = sql.func.json_build_object(
        "address_line_1",
        schema.Bedrijf.correspondentie_adres_buitenland1,
        "address_line_2",
        schema.Bedrijf.correspondentie_adres_buitenland2,
        "address_line_3",
        schema.Bedrijf.correspondentie_adres_buitenland3,
        "country",
        schema.Bedrijf.correspondentie_landcode,
        "is_foreign",
        True,
    )

    address = sql.case(
        # No address of this type.
        (
            sql.and_(
                sql.func.coalesce(
                    schema.Bedrijf.correspondentie_adres_buitenland1,
                    schema.Bedrijf.correspondentie_adres_buitenland2,
                    schema.Bedrijf.correspondentie_adres_buitenland3,
                    schema.Bedrijf.correspondentie_straatnaam,
                    "",
                )
                == "",
            ),
            None,
        ),
        # 6030 = The Netherlands
        (
            sql.and_(
                schema.Bedrijf.correspondentie_straatnaam != "",
                sql.or_(
                    schema.Bedrijf.correspondentie_landcode == 5107,
                    schema.Bedrijf.correspondentie_landcode == 6030,
                ),
            ),
            sql.cast(address_query, sqltypes.JSON),
        ),
        else_=sql.cast(foreign_address_query, sqltypes.JSON),
    )

    return address


organization_query = (
    sql.select(
        schema.Bedrijf.uuid,
        schema.Bedrijf.authenticated,
        schema.Bedrijf.authenticatedby,
        schema.Bedrijf.handelsnaam.label("name"),
        schema.Bedrijf.date_founded,
        schema.Bedrijf.date_registration.label("date_registered"),
        schema.Bedrijf.date_ceased,
        schema.Bedrijf.rsin,
        schema.Bedrijf.oin,
        schema.Bedrijf.dossiernummer.label("coc_number"),
        schema.Bedrijf.vestigingsnummer.label("coc_location_number"),
        schema.Bedrijf.rechtsvorm.label("business_entity_code"),
        schema.Bedrijf.main_activity,
        schema.Bedrijf.secondairy_activities.label("secondary_activities"),
        schema.Bedrijf.preferred_contact_channel,
        _organization_contact_info_query().label("contact_information"),
        _organization_location_address_query().label("location_address"),
        _organization_correspondence_address_query().label(
            "correspondence_address"
        ),
        schema.CustomObject.uuid.label("related_custom_object_uuid"),
        _organization_contact_person_info_query().label("contact_person_info"),
        schema.Bedrijf.handelsnaam.label("summary"),
    )
    .select_from(
        sql.join(
            schema.Bedrijf,
            schema.CustomObject,
            schema.Bedrijf.related_custom_object_id == schema.CustomObject.id,
            isouter=True,
        )
        .join(
            schema.ContactData,
            sql.and_(
                schema.ContactData.gegevens_magazijn_id == schema.Bedrijf.id,
                schema.ContactData.betrokkene_type
                == constants.BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
            ),
            isouter=True,
        )
        .join(
            schema.Subject,
            sql.and_(
                schema.Subject.uuid == schema.Bedrijf.uuid,
                schema.Subject.subject_type == "company",
            ),
            isouter=True,
        )
    )
    .where(schema.Bedrijf.deleted_on.is_(None))
)

organization_limited_query = sql.select(
    schema.Bedrijf.uuid,
    schema.Bedrijf.date_ceased,
    schema.Bedrijf.dossiernummer.label("coc_number"),
    schema.Bedrijf.vestigingsnummer.label("coc_location_number"),
    _organization_location_address_query().label("location_address"),
    _organization_correspondence_address_query().label(
        "correspondence_address"
    ),
    schema.Bedrijf.handelsnaam.label("summary"),
).where(schema.Bedrijf.deleted_on.is_(None))


class OrganizationRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Organization"
    _events_to_calls = {
        "RelatedCustomObjectSet": "_save_organization",
        "ContactInformationSaved": "_save_contact_information",
        "OrganizationUpdated": "_update_non_authentic",
    }

    def get_organizations_by_uuid(
        self, uuids: typing.Iterable[UUID]
    ) -> list[Organization]:
        """Find multiple organizations by their uuids.

        :param uuids: Iterable containing UUIDs of the organizations to retrieve.
        :type uuid: typing.Iterable[UUID]
        :raises NotFound: If organization not present
        :return: List of Organization entities
        :rtype: List[Organization]
        """

        query = organization_query.where(schema.Bedrijf.uuid.in_(uuids))
        query_result = self.session.execute(query).fetchall()

        return [self._sqla_to_entity(query_result=row) for row in query_result]

    def find_organization_by_uuid(self, uuid) -> OrganizationLimited:
        """Find organization by uuid."""

        qry_stmt = organization_query.where(schema.Bedrijf.uuid == uuid)
        query_result = self.session.execute(qry_stmt).fetchone()
        if not query_result:
            raise NotFound(
                f"Organization with uuid '{uuid}' not found.",
                "organization/not_found",
            )
        return self._sqla_to_entity(query_result=query_result)

    def find_organization_by_uuid_limited(self, uuid) -> OrganizationLimited:
        """Find organization by uuid.

        :param uuid: UUID of the organization
        :type uuid: UUID
        :raises NotFound: If organization not present
        :return: Organization entity
        :rtype: Organization entity
        """

        qry_stmt = organization_limited_query.where(
            schema.Bedrijf.uuid == uuid
        )
        query_result = self.session.execute(qry_stmt).fetchone()
        if not query_result:
            raise NotFound(
                f"Organization with uuid '{uuid}' not found.",
                "organization/not_found",
            )
        return self._sqla_to_entity_limited(query_result=query_result)

    def _sqla_to_entity(self, query_result) -> Organization:
        """Initialize Organization Entity from sqla object.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type entity
        :rtype: entities.Organization
        """

        source = (
            query_result.authenticatedby
            if query_result.authenticated
            else "Zaaksysteem"
        )

        location_address = query_result.location_address
        has_valid_address = False
        if location_address:
            location_address["country_code"] = location_address["country"]

            location_address["country"] = get_country_name_from_landcode(
                location_address["country"]
            )

            if location_address["bag_id"]:
                has_valid_address = True

        correspondence_address = query_result.correspondence_address
        if correspondence_address:
            correspondence_address["country_code"] = correspondence_address[
                "country"
            ]

            correspondence_address["country"] = get_country_name_from_landcode(
                correspondence_address["country"]
            )

        is_active = True
        if query_result.date_ceased:
            is_active = query_result.date_ceased > datetime.date.today()

        related_custom_object_uuid = query_result.related_custom_object_uuid

        organization = Organization(
            entity_id=query_result.uuid,
            uuid=query_result.uuid,
            name=query_result.name,
            source=source,
            date_founded=query_result.date_founded,
            date_registered=query_result.date_registered,
            date_ceased=query_result.date_ceased,
            rsin=query_result.rsin,
            oin=query_result.oin,
            coc_number=query_result.coc_number,
            coc_location_number=query_result.coc_location_number,
            organization_type=get_type_of_business_from_entity_code(
                query_result.business_entity_code
            ),
            main_activity=query_result.main_activity
            if bool(query_result.main_activity)
            else None,
            secondary_activities=query_result.secondary_activities,
            location_address=location_address,
            correspondence_address=correspondence_address,
            contact_information=query_result.contact_information,
            preferred_contact_channel=query_result.preferred_contact_channel,
            related_custom_object=(
                None
                if related_custom_object_uuid is None
                else RelatedCustomObject(
                    entity_id=related_custom_object_uuid,
                    uuid=related_custom_object_uuid,
                )
            ),
            contact_person=query_result.contact_person_info,
            has_valid_address=has_valid_address,
            entity_meta_summary=query_result.summary,
            # Services, etc. needed by the entity
            _event_service=self.event_service,
            is_active=is_active,
            authenticated=query_result.authenticated,
        )

        return organization

    def _sqla_to_entity_limited(self, query_result) -> OrganizationLimited:
        """Initialize OrganizationLimited Entity from sqla object."""

        has_correspondence_address = False
        is_ceased = False
        has_valid_address = False

        if query_result.location_address and query_result.location_address.get(
            "bag_id"
        ):
            has_valid_address = True

        if query_result.correspondence_address:
            has_correspondence_address = True

        if query_result.date_ceased:
            is_ceased = True

        organization = OrganizationLimited(
            entity_id=query_result.uuid,
            uuid=query_result.uuid,
            has_valid_address=has_valid_address,
            entity_meta_summary=query_result.summary,
            has_correspondence_address=has_correspondence_address,
            coc_number=query_result.coc_number,
            coc_location_number=query_result.coc_location_number,
            is_ceased=is_ceased,
        )
        return organization

    def _save_organization(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        changes = event.format_changes()
        related_custom_object = changes["related_custom_object"]

        if related_custom_object:
            custom_object_uuid = related_custom_object["uuid"]
            custom_object_row = self.session.execute(
                sql.select(schema.CustomObject.id).where(
                    schema.CustomObject.uuid == custom_object_uuid
                )
            ).fetchone()

            if custom_object_row is None:
                raise NotFound(
                    f"No custom object found with uuid={custom_object_uuid}",
                    "organization/related_custom_object_not_found",
                )

            related_custom_object_id = custom_object_row.id
        else:
            related_custom_object_id = None

        self.session.execute(
            sql.update(schema.Bedrijf)
            .where(schema.Bedrijf.uuid == event.entity_id)
            .values(related_custom_object_id=related_custom_object_id)
            .execution_options(synchronize_session=False)
        )

    def _save_contact_information(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        """Save contact_infromation for organization"""
        changes = event.format_changes()
        contact_information = changes["contact_information"]

        anonymous_property = {
            "anonymous": "1"
            if contact_information.get("is_anonymous_contact", False)
            else "0"
        }

        if contact_information.get("is_anonymous_contact", None) is not None:
            org_properties = self.session.execute(
                sql.select(schema.Subject.properties).where(
                    schema.Subject.uuid == event.entity_id,
                    schema.Subject.subject_type == "company",
                )
            ).scalar_one_or_none()

            if org_properties:
                new_properties = {
                    **org_properties,
                    **anonymous_property,
                }
                # `subject` row exists; update

                self.session.execute(
                    sql.update(schema.Subject)
                    .where(
                        sql.and_(
                            schema.Bedrijf.uuid == event.entity_id,
                            schema.Subject.uuid == schema.Bedrijf.uuid,
                            schema.Subject.subject_type == "company",
                        )
                    )
                    .values(properties=new_properties)
                    .execution_options(synchronize_session=False)
                )
            else:
                self.session.execute(
                    sql.insert(schema.Subject).values(
                        {
                            "properties": anonymous_property,
                            "uuid": event.entity_id,
                            "subject_type": "company",
                            "username": event.entity_data["name"],
                        }
                    )
                )

        # Update preferred_contact_channel property for organization in bedrijf table
        self.session.execute(
            sql.update(schema.Bedrijf)
            .where(schema.Bedrijf.uuid == event.entity_id)
            .values(
                preferred_contact_channel=contact_information.get(
                    "preferred_contact_channel", None
                )
            )
            .execution_options(synchronize_session=False)
        )

        contact_data = self.session.execute(
            sql.select(schema.ContactData.id).where(
                sql.and_(
                    schema.Bedrijf.uuid == event.entity_id,
                    schema.ContactData.gegevens_magazijn_id
                    == schema.Bedrijf.id,
                    schema.ContactData.betrokkene_type
                    == BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
                )
            )
        ).fetchone()

        if contact_data:
            # Update contact_data for organization in contact_data table
            self.session.execute(
                sql.update(schema.ContactData)
                .where(
                    sql.and_(
                        schema.Bedrijf.uuid == event.entity_id,
                        schema.ContactData.gegevens_magazijn_id
                        == schema.Bedrijf.id,
                        schema.ContactData.betrokkene_type
                        == BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
                    )
                )
                .values(
                    mobiel=contact_information.get("mobile_number", None),
                    telefoonnummer=contact_information.get(
                        "phone_number", None
                    ),
                    email=contact_information.get("email", None),
                    note=contact_information.get("internal_note", None),
                    last_modified=None,
                )
                .execution_options(synchronize_session=False)
            )
        else:
            # Insert contact_data for organization in contact_data table
            self.session.execute(
                sql.insert(schema.ContactData).values(
                    mobiel=contact_information.get("mobile_number", None),
                    telefoonnummer=contact_information.get(
                        "phone_number", None
                    ),
                    email=contact_information.get("email", None),
                    note=contact_information.get("internal_note", None),
                    betrokkene_type=BETROKKENE_TYPE_NUMBER_FOR_ORGANIZATION,
                    gegevens_magazijn_id=sql.select(schema.Bedrijf.id)
                    .where(schema.Bedrijf.uuid == event.entity_id)
                    .scalar_subquery(),
                )
            )

    def _update_non_authentic(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        changes = event.format_changes()

        # Update Organization Data
        self.session.execute(
            sql.update(schema.Bedrijf)
            .where(schema.Bedrijf.uuid == event.entity_id)
            .values(self._organization_update_data_map(changes))
            .execution_options(synchronize_session=False)
        )

    def _organization_update_data_map(self, changes) -> dict:
        values = {
            "handelsnaam": changes.get("name"),
            "dossiernummer": changes.get("coc_number"),
            "vestigingsnummer": changes.get("coc_location_number"),
            "rechtsvorm": changes.get("organization_type"),
            "search_term": self._search_term(changes),
            "search_order": self._search_term(changes),
        }
        if changes.get("contact_person"):
            values.update(
                self._map_contact_person_data(changes.get("contact_person"))
            )
        if "location_address" in changes:
            address_data = (
                changes["location_address"]
                if changes["location_address"]
                else {}
            )
            values.update(self._map_address_data(address_data, "vestiging"))

        if "correspondence_address" in changes:
            address_data = (
                changes["correspondence_address"]
                if changes["correspondence_address"]
                else {}
            )
            values.update(
                self._map_address_data(address_data, "correspondentie")
            )
        return values

    def _map_address_data(
        self, address_data: dict, address_indication: str
    ) -> dict:
        if "street" in address_data:
            address_updata_data = {
                address_indication + "_straatnaam": address_data.get("street"),
                address_indication + "_postcode": address_data.get("zipcode"),
                address_indication + "_huisnummer": address_data.get(
                    "street_number"
                ),
                address_indication + "_huisletter": address_data.get(
                    "street_number_letter"
                ),
                address_indication + "_huisnummertoevoeging": address_data.get(
                    "street_number_suffix"
                ),
                address_indication + "_woonplaats": address_data.get("city"),
                address_indication
                + "_landcode": get_landcode_from_country_name(
                    address_data.get("country", "")
                ),
                address_indication + "_adres_buitenland1": None,
                address_indication + "_adres_buitenland2": None,
                address_indication + "_adres_buitenland3": None,
            }

        else:
            address_updata_data = {
                address_indication + "_adres_buitenland1": address_data.get(
                    "address_line_1"
                ),
                address_indication + "_adres_buitenland2": address_data.get(
                    "address_line_2"
                ),
                address_indication + "_adres_buitenland3": address_data.get(
                    "address_line_3"
                ),
                address_indication
                + "_landcode": get_landcode_from_country_name(
                    address_data.get("country", "")
                ),
                address_indication + "_straatnaam": None,
                address_indication + "_postcode": None,
                address_indication + "_huisnummer": None,
                address_indication + "_huisletter": None,
                address_indication + "_huisnummertoevoeging": None,
                address_indication + "_woonplaats": None,
            }

        return address_updata_data

    def _map_contact_person_data(self, contact_person: dict) -> dict:
        contact_update_data = {
            "contact_naam": contact_person.get("first_name"),
            "contact_voorletters": contact_person.get("initials"),
            "contact_voorvoegsel": contact_person.get("insertions"),
            "contact_geslachtsnaam": contact_person.get("family_name"),
        }

        return contact_update_data

    def _search_term(self, changes):
        name = changes.get("name", "")
        location_address = changes.get("location_address", {})

        address_parts = " ".join(
            [
                str(location_address.get(x, ""))
                for x in location_address
                if location_address.get(x)
            ]
        )
        search_term = " ".join([name, address_parts])

        return search_term
