# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from ... import ZaaksysteemRepositoryBase
from ...shared.util import (
    get_country_name_from_landcode,
    get_landcode_from_country_name,
    getattr_from_dict,
)
from ..constants import BETROKKENE_TYPE_NUMBER_FOR_PERSON
from ..entities import Person
from ..entities._shared import RelatedCustomObject
from ..entities.person import PersonLimited
from collections.abc import Iterable
from minty.cqrs import UserInfo
from minty.cqrs.events import Event
from minty.exceptions import NotFound
from sqlalchemy import Integer, sql
from sqlalchemy.orm import aliased
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.person import is_person_secret


class AddressFunctionCode(enum.StrEnum):
    B = "B"  # "B"riefadres - Correspondence address
    W = "W"  # "W"oonadres - Residence address


def _person_contact_info_query():
    return sql.select(
        sql.func.json_build_object(
            "email",
            schema.ContactData.email,
            "phone_number",
            schema.ContactData.telefoonnummer,
            "mobile_number",
            schema.ContactData.mobiel,
            "internal_note",
            schema.ContactData.note,
        )
    ).where(
        sql.and_(
            schema.ContactData.gegevens_magazijn_id
            == schema.NatuurlijkPersoon.id,
            schema.ContactData.betrokkene_type
            == BETROKKENE_TYPE_NUMBER_FOR_PERSON,
        )
    )


def _person_address_query(address_function: AddressFunctionCode):
    address = sql.alias(schema.Adres)

    return sql.select(
        sql.case(
            (
                sql.and_(
                    address.c.straatnaam != "",
                    sql.or_(
                        address.c.landcode == 5107,
                        address.c.landcode == 6030,
                    ),
                ),
                sql.func.json_build_object(
                    "street",
                    address.c.straatnaam,
                    "zipcode",
                    address.c.postcode,
                    "street_number",
                    address.c.huisnummer,
                    "street_number_letter",
                    address.c.huisletter,
                    "street_number_suffix",
                    address.c.huisnummertoevoeging,
                    "city",
                    address.c.woonplaats,
                    "country_code",
                    address.c.landcode,
                    "bag_id",
                    address.c.bag_id,
                    "is_foreign",
                    False,
                    "geo_lat_long",
                    address.c.geo_lat_long,
                ),
            ),
            else_=sql.func.json_build_object(
                "country_code",
                address.c.landcode,
                "address_line_1",
                address.c.adres_buitenland1,
                "address_line_2",
                address.c.adres_buitenland2,
                "address_line_3",
                address.c.adres_buitenland3,
                "bag_id",
                None,
                "is_foreign",
                True,
                "geo_lat_long",
                address.c.geo_lat_long,
            ),
        )
    ).where(
        sql.and_(
            address.c.natuurlijk_persoon_id == schema.NatuurlijkPersoon.id,
            address.c.functie_adres == address_function.value,
        )
    )


person_query = (
    sql.select(
        schema.NatuurlijkPersoon.uuid,
        schema.NatuurlijkPersoon.authenticated,
        schema.NatuurlijkPersoon.authenticatedby,
        schema.NatuurlijkPersoon.voornamen.label("first_names"),
        schema.NatuurlijkPersoon.voorletters.label("initials"),
        schema.NatuurlijkPersoon.voorvoegsel.label("insertions"),
        schema.NatuurlijkPersoon.geslachtsnaam.label("family_name"),
        schema.NatuurlijkPersoon.adellijke_titel.label("noble_title"),
        schema.NatuurlijkPersoon.surname.label("surname"),
        schema.NatuurlijkPersoon.geboortedatum.label("date_of_birth"),
        schema.NatuurlijkPersoon.datum_overlijden.label("date_of_death"),
        schema.NatuurlijkPersoon.geslachtsaanduiding.label("gender"),
        schema.NatuurlijkPersoon.in_gemeente.label("inside_municipality"),
        schema.NatuurlijkPersoon.landcode,
        schema.NatuurlijkPersoon.preferred_contact_channel,
        schema.NatuurlijkPersoon.active.label("active"),
        _person_contact_info_query().label("contact_information"),
        _person_address_query(AddressFunctionCode.W).label("address"),
        _person_address_query(AddressFunctionCode.B).label(
            "correspondence_address"
        ),
        schema.NatuurlijkPersoon.indicatie_geheim.label("indicatie_geheim"),
        schema.CustomObject.uuid.label("related_custom_object_uuid"),
        schema.Subject.properties,
        sql.func.concat(
            schema.NatuurlijkPersoon.adellijke_titel + " ",
            schema.NatuurlijkPersoon.voorletters + " ",
            schema.NatuurlijkPersoon.surname,
        ).label("summary"),
        schema.ObjectSubscription.external_id.label("external_identifier"),
    )
    .where(schema.NatuurlijkPersoon.deleted_on.is_(None))
    .select_from(
        sql.join(
            schema.NatuurlijkPersoon,
            schema.CustomObject,
            schema.NatuurlijkPersoon.related_custom_object_id
            == schema.CustomObject.id,
            isouter=True,
        )
        .join(
            schema.Subject,
            sql.and_(
                schema.Subject.uuid == schema.NatuurlijkPersoon.uuid,
                schema.Subject.subject_type == "person",
            ),
            isouter=True,
        )
        .join(
            schema.ObjectSubscription,
            sql.and_(
                schema.NatuurlijkPersoon.id
                == sql.cast(schema.ObjectSubscription.local_id, Integer),
                schema.ObjectSubscription.local_table == "NatuurlijkPersoon",
                schema.ObjectSubscription.date_deleted.is_(None),
            ),
            isouter=True,
        )
    )
)

person_limited_query = sql.select(
    schema.NatuurlijkPersoon.uuid,
    schema.NatuurlijkPersoon.datum_overlijden.label("date_of_death"),
    schema.NatuurlijkPersoon.active.label("active"),
    _person_address_query(AddressFunctionCode.W).label("address"),
    _person_address_query(AddressFunctionCode.B).label(
        "correspondence_address"
    ),
    schema.NatuurlijkPersoon.indicatie_geheim.label("indicatie_geheim"),
    sql.func.concat(
        schema.NatuurlijkPersoon.adellijke_titel + " ",
        schema.NatuurlijkPersoon.voorletters + " ",
        schema.NatuurlijkPersoon.surname,
    ).label("summary"),
    schema.NatuurlijkPersoon.onderzoek_persoon,
    schema.NatuurlijkPersoon.onderzoek_huwelijk,
    schema.NatuurlijkPersoon.onderzoek_overlijden,
    schema.NatuurlijkPersoon.onderzoek_verblijfplaats,
).where(schema.NatuurlijkPersoon.deleted_on.is_(None))

CORRESPONDENCE_ADDRESS = aliased(schema.Adres)
SEARCH_TERM_QUERY = sql.select(
    schema.NatuurlijkPersoon.voornamen.label("first_names"),
    schema.NatuurlijkPersoon.voorletters.label("initials"),
    schema.NatuurlijkPersoon.voorvoegsel.label("insertions"),
    schema.NatuurlijkPersoon.geslachtsnaam.label("family_name"),
    schema.NatuurlijkPersoon.burgerservicenummer.label("burgerservicenummer"),
    schema.NatuurlijkPersoon.persoonsnummer.label("person_number"),
    schema.NatuurlijkPersoon.geboortedatum.label("date_of_birth"),
    schema.NatuurlijkPersoon.datum_overlijden.label("date_of_death"),
    schema.Adres.straatnaam.label("address.street"),
    schema.Adres.huisnummer.label("address.street_number"),
    schema.Adres.huisletter.label("address.street_number_letter"),
    schema.Adres.woonplaats.label("address.city"),
    schema.Adres.postcode.label("address.zipcode"),
    CORRESPONDENCE_ADDRESS.straatnaam.label("correspondence_address.street"),
    CORRESPONDENCE_ADDRESS.huisnummer.label(
        "correspondence_address.street_number"
    ),
    CORRESPONDENCE_ADDRESS.huisletter.label(
        "correspondence_address.street_number_letter"
    ),
    CORRESPONDENCE_ADDRESS.woonplaats.label("correspondence_address.city"),
    CORRESPONDENCE_ADDRESS.postcode.label("correspondence_address.zipcode"),
    schema.ContactData.email.label("contact_information.email"),
).select_from(
    sql.join(
        schema.NatuurlijkPersoon,
        schema.Adres,
        schema.NatuurlijkPersoon.adres_id == schema.Adres.id,
        isouter=True,
    )
    .join(
        CORRESPONDENCE_ADDRESS,
        sql.and_(
            schema.NatuurlijkPersoon.id
            == CORRESPONDENCE_ADDRESS.natuurlijk_persoon_id,
            CORRESPONDENCE_ADDRESS.functie_adres == AddressFunctionCode.B,
        ),
        isouter=True,
    )
    .join(
        schema.ContactData,
        sql.and_(
            schema.ContactData.gegevens_magazijn_id
            == schema.NatuurlijkPersoon.id,
            schema.ContactData.betrokkene_type
            == BETROKKENE_TYPE_NUMBER_FOR_PERSON,
        ),
    )
)


CONTACT_DATA_MAP = {
    "voornamen": "first_names",
    "geslachtsnaam": "family_name",
    "voorvoegsel": "insertions",
    "adellijke_titel": "noble_title",
    "geslachtsaanduiding": "gender",
    "in_gemeente": "inside_municipality",
    "voorletters": "initials",
    "adres_id": "address_id",
    "search_term": "search_term",
    "search_order": "search_order",
    "landcode": "country_code",
    "naamgebruik": "surname",
    "persoonsnummer": "sedula_number",
}


class PersonRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Person"
    _events_to_calls = {
        "RelatedCustomObjectSet": "_save_person",
        "BsnRetrieved": "_bsn_retrieved",
        "ContactInformationSaved": "_save_contact_information",
        "PersonUpdated": "_update_non_authentic",
        "NonAuthenticBsnUpdated": "_update_bsn_non_authentic",
        "NonAuthenticSedulaUpdated": "_update_sedula_number_non_authentic",
    }

    def get_persons_by_uuid(self, uuids: Iterable[UUID]) -> list[Person]:
        """Retrieve multiple "person" objects by their uuids.

        :param uuids: Iterable containing UUIDs of the persons to retrieve.
        :raises NotFound: If person not present
        :return: List of persons
        """
        query = person_query.where(schema.NatuurlijkPersoon.uuid.in_(uuids))
        query_result = self.session.execute(query).fetchall()

        return [self._sqla_to_entity(query_result=row) for row in query_result]

    def find_person_by_uuid(self, uuid: UUID) -> Person:
        """Find person by uuid.

        :param uuid: UUID of the person
        :type uuid: UUID
        :raises NotFound: If person not present
        :return: Person entity
        :rtype: Person entity
        """
        qry_stmt = person_query.where(schema.NatuurlijkPersoon.uuid == uuid)
        query_result = self.session.execute(qry_stmt).fetchone()
        if not query_result:
            raise NotFound(
                f"Person with uuid '{uuid}' not found.", "person/not_found"
            )

        return self._sqla_to_entity(query_result=query_result)

    def find_person_by_uuid_limited(self, uuid: UUID) -> PersonLimited:
        """Find person by uuid.

        :param uuid: UUID of the person
        :type uuid: UUID
        :raises NotFound: If person not present
        :return: Person entity
        :rtype: Person entity
        """
        qry_stmt = person_limited_query.where(
            schema.NatuurlijkPersoon.uuid == uuid
        )
        query_result = self.session.execute(qry_stmt).fetchone()
        if not query_result:
            raise NotFound(
                f"Person with uuid '{uuid}' not found.", "person/not_found"
            )

        return self._sqla_to_entity_limited(query_result=query_result)

    def _get_id_of_person_by_uuid(self, uuid: UUID) -> int:
        query_result = self.session.execute(
            sql.select(schema.NatuurlijkPersoon.id).where(
                schema.NatuurlijkPersoon.uuid == uuid
            )
        ).fetchone()
        if not query_result:
            raise NotFound(
                f"Person with uuid '{uuid}' not found.", "person/not_found"
            )
        return query_result.id

    def _sqla_to_entity(self, query_result) -> Person:
        """Initialize Person Entity from sqla object.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type entity
        :rtype: entities.Organization
        """
        residence_address = None

        correspondence_address = None
        is_anonymous_contact = False

        has_valid_address = False
        country_code = None
        if address := query_result.address:
            address["country"] = get_country_name_from_landcode(
                address["country_code"]
            )

            if address["bag_id"]:
                has_valid_address = True

            country_code = address["country_code"]
            residence_address = address

        if correspondence_address := query_result.correspondence_address:
            correspondence_address["country"] = get_country_name_from_landcode(
                correspondence_address["country_code"]
            )

        related_custom_object_uuid = query_result.related_custom_object_uuid

        if query_result.properties:
            try:
                is_anonymous_contact = bool(
                    int(query_result.properties.get("anonymous", "0"))
                )
            except ValueError:
                is_anonymous_contact = False

        name = query_result.surname
        if query_result.initials:
            name = query_result.initials + " " + name

        if query_result.contact_information:
            contact_information = query_result.contact_information
            contact_information.update(
                {
                    "is_anonymous_contact": is_anonymous_contact or False,
                    "preferred_contact_channel": query_result.preferred_contact_channel,
                }
            )
        else:
            contact_information = {
                "is_anonymous_contact": is_anonymous_contact or False,
                "preferred_contact_channel": query_result.preferred_contact_channel,
            }

        person = Person(
            uuid=query_result.uuid,
            entity_id=query_result.uuid,
            authenticated=query_result.authenticated,
            source=query_result.authenticatedby,
            first_names=query_result.first_names,
            initials=query_result.initials,
            insertions=query_result.insertions,
            family_name=query_result.family_name,
            noble_title=query_result.noble_title,
            surname=query_result.surname,
            name=name,
            date_of_birth=query_result.date_of_birth,
            date_of_death=query_result.date_of_death,
            gender=self._gender_of_person(query_result.gender),
            inside_municipality=query_result.inside_municipality,
            residence_address=residence_address,
            correspondence_address=correspondence_address,
            contact_information=contact_information,
            has_valid_address=has_valid_address,
            is_active=self._is_active(query_result),
            is_secret=is_person_secret(query_result.indicatie_geheim),
            related_custom_object=(
                None
                if related_custom_object_uuid is None
                else RelatedCustomObject(
                    entity_id=related_custom_object_uuid,
                    uuid=related_custom_object_uuid,
                )
            ),
            country_code=country_code,
            entity_meta_summary=query_result.summary,
            external_identifier=query_result.external_identifier,
            # Services, etc. needed by the entity
            _event_service=self.event_service,
        )

        return person

    def _sqla_to_entity_limited(self, query_result) -> PersonLimited:
        """Initialize PersonLimited Entity from sqla object."""
        has_correspondence_address = False
        is_deceased = False
        has_valid_address = False
        is_under_investigation = False

        if address := query_result.address:
            if address["bag_id"]:
                has_valid_address = True

        if query_result.correspondence_address:
            has_correspondence_address = True

        if query_result.date_of_death:
            is_deceased = True

        if (
            query_result.onderzoek_persoon
            or query_result.onderzoek_huwelijk
            or query_result.onderzoek_overlijden
            or query_result.onderzoek_verblijfplaats
        ):
            is_under_investigation = True

        person_limited = PersonLimited(
            uuid=query_result.uuid,
            entity_id=query_result.uuid,
            has_valid_address=has_valid_address,
            is_deceased=is_deceased,
            has_correspondence_address=has_correspondence_address,
            is_secret=is_person_secret(query_result.indicatie_geheim),
            entity_meta_summary=query_result.summary,
            is_under_investigation=is_under_investigation,
        )

        return person_limited

    def _gender_of_person(self, gender_value):
        gender = None
        if gender_value in ["M", "X"]:
            gender = gender_value
        elif gender_value in ["V", "F"]:
            gender = "F"

        return gender

    def _save_person(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        changes = event.format_changes()
        related_custom_object = changes["related_custom_object"]

        if related_custom_object:
            custom_object_uuid = related_custom_object["uuid"]
            custom_object_row = self.session.execute(
                sql.select(schema.CustomObject.id).where(
                    schema.CustomObject.uuid == custom_object_uuid
                )
            ).fetchone()

            if custom_object_row is None:
                raise NotFound(
                    f"No custom object found with uuid={custom_object_uuid}",
                    "person/related_custom_object_not_found",
                )

            related_custom_object_id = custom_object_row.id
        else:
            related_custom_object_id = None

        self.session.execute(
            sql.update(schema.NatuurlijkPersoon)
            .where(schema.NatuurlijkPersoon.uuid == event.entity_id)
            .values(related_custom_object_id=related_custom_object_id)
            .execution_options(synchronize_session=False)
        )

    def _save_contact_information(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        """Save contact_information for a person"""
        changes = event.format_changes()
        contact_information = changes["contact_information"]

        if contact_information.get("is_anonymous_contact", None) is not None:
            person_properties = self.session.execute(
                sql.select(schema.Subject.properties).where(
                    schema.Subject.uuid == event.entity_id,
                    schema.Subject.subject_type == "person",
                )
            ).fetchone()

            if person_properties:
                person_properties_value = person_properties[0]
                person_properties_value["anonymous"] = (
                    "1"
                    if contact_information.get("is_anonymous_contact", False)
                    else "0"
                )

                # Update property anonymous for person in subject table
                self.session.execute(
                    sql.update(schema.Subject)
                    .where(
                        sql.and_(
                            schema.NatuurlijkPersoon.uuid == event.entity_id,
                            schema.Subject.uuid
                            == schema.NatuurlijkPersoon.uuid,
                            schema.Subject.subject_type == "person",
                        )
                    )
                    .values(properties=person_properties_value)
                    .execution_options(synchronize_session=False)
                )
            else:
                person_properties_value = {
                    "anonymous": "1"
                    if contact_information.get("is_anonymous_contact", False)
                    else "0"
                }
                # Insert property anonymous for person in subject table
                self.session.execute(
                    sql.insert(schema.Subject).values(
                        {
                            "properties": person_properties_value,
                            "uuid": event.entity_id,
                            "subject_type": "person",
                            "username": event.entity_data["name"],
                        }
                    )
                )

        # update search_term for Contact
        search_term = self._generate_search_term(
            changes, natuurlijk_persoon_uuid=UUID(str(event.entity_id))
        )

        # Update preferred_contact_channel property for person in natuurlijk_persoon table
        self.session.execute(
            sql.update(schema.NatuurlijkPersoon)
            .where(schema.NatuurlijkPersoon.uuid == event.entity_id)
            .values(
                preferred_contact_channel=contact_information.get(
                    "preferred_contact_channel", None
                ),
                search_term=search_term,
            )
            .execution_options(synchronize_session=False)
        )

        contact_data = self.session.execute(
            sql.select(schema.ContactData.id).where(
                sql.and_(
                    schema.NatuurlijkPersoon.uuid == event.entity_id,
                    schema.ContactData.gegevens_magazijn_id
                    == schema.NatuurlijkPersoon.id,
                    schema.ContactData.betrokkene_type
                    == BETROKKENE_TYPE_NUMBER_FOR_PERSON,
                )
            )
        ).fetchone()

        if contact_data:
            # Update contact_data for person in contact_data table
            self.session.execute(
                sql.update(schema.ContactData)
                .where(
                    sql.and_(
                        schema.NatuurlijkPersoon.uuid == event.entity_id,
                        schema.ContactData.gegevens_magazijn_id
                        == schema.NatuurlijkPersoon.id,
                        schema.ContactData.betrokkene_type
                        == BETROKKENE_TYPE_NUMBER_FOR_PERSON,
                    )
                )
                .values(
                    mobiel=contact_information.get("mobile_number", None),
                    telefoonnummer=contact_information.get(
                        "phone_number", None
                    ),
                    email=contact_information.get("email", None),
                    note=contact_information.get("internal_note", None),
                    last_modified=None,
                )
                .execution_options(synchronize_session=False)
            )
        else:
            # Insert contact_data for person in contact_data table
            self.session.execute(
                sql.insert(schema.ContactData).values(
                    mobiel=contact_information.get("mobile_number", None),
                    telefoonnummer=contact_information.get(
                        "phone_number", None
                    ),
                    email=contact_information.get("email", None),
                    note=contact_information.get("internal_note", None),
                    betrokkene_type=BETROKKENE_TYPE_NUMBER_FOR_PERSON,
                    gegevens_magazijn_id=sql.select(
                        schema.NatuurlijkPersoon.id
                    )
                    .where(schema.NatuurlijkPersoon.uuid == event.entity_id)
                    .scalar_subquery(),
                )
            )

    def _add_address_of_contact(
        self,
        address: dict,
        contact_id: int,
        correspondence_address: bool,
        country_code: str,
    ) -> str:
        values = {
            "natuurlijk_persoon_id": contact_id,
            "landcode": country_code,
            "functie_adres": "B" if correspondence_address else "W",
        }
        if not address.get("is_foreign"):
            # Dutch Address
            values.update(
                {
                    "straatnaam": address.get("street"),
                    "huisnummer": address.get("street_number"),
                    "huisletter": address.get("street_number_letter"),
                    "huisnummertoevoeging": address.get(
                        "street_number_suffix"
                    ),
                    "postcode": address.get("zipcode"),
                    "woonplaats": address.get("city"),
                }
            )
        else:
            # Foreign address
            values.update(
                {
                    "adres_buitenland1": address.get("address_line_1"),
                    "adres_buitenland2": address.get("address_line_2", ""),
                    "adres_buitenland3": address.get("address_line_3", ""),
                }
            )
        exec_result = self.session.execute(
            sql.insert(schema.Adres).values(values)
        )

        address_id = exec_result.inserted_primary_key[0]
        return address_id

    def _update_non_authentic(
        self, event: Event, dry_run: bool, user_info: UserInfo | None = None
    ):
        contact_id = self._get_id_of_person_by_uuid(event.entity_id)
        changes = event.format_changes()
        # Add address
        changes["address"] = changes.get("residence_address")
        correspondence_address = changes.get("correspondence_address")

        address_id = correspondence_address_id = None
        added_address_ids = []
        if changes["address"]:
            address_id = self._add_address_of_contact(
                contact_id=contact_id,
                address=changes["address"],
                correspondence_address=False,
                country_code=changes["country_code"],
            )
            changes["address_id"] = address_id
            added_address_ids.append(address_id)
        if correspondence_address:
            correspondence_address_id = self._add_address_of_contact(
                contact_id=contact_id,
                address=correspondence_address,
                correspondence_address=True,
                country_code=get_landcode_from_country_name(
                    correspondence_address["country"]
                ),
            )
            added_address_ids.append(correspondence_address_id)
        changes["search_term"] = changes["search_order"] = (
            self._generate_search_term(
                changes, natuurlijk_persoon_uuid=UUID(str(event.entity_id))
            )
        )

        # Update Person Data
        self.session.execute(
            sql.update(schema.NatuurlijkPersoon)
            .where(schema.NatuurlijkPersoon.uuid == event.entity_id)
            .values(self._contact_data_map(changes))
            .execution_options(synchronize_session=False)
        )
        # delete existing addresses

        self.session.execute(
            sql.delete(schema.Adres)
            .where(
                sql.and_(
                    schema.Adres.natuurlijk_persoon_id == contact_id,
                    schema.Adres.id.notin_(added_address_ids),
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _contact_data_map(self, changes) -> dict:
        values = {}
        for k, v in CONTACT_DATA_MAP.items():
            if changes.get(v) is not None:
                values[k] = changes.get(v)
        return values

    def _generate_search_term(self, changes, natuurlijk_persoon_uuid: UUID):
        # get searchterms for the natuurlijkpersoon.
        # There are several "commands" to update data of a natuurlijkpersoon
        # (One changes contact_data, the other natuurlijkpersoon data)
        # this function retrieves all search_terms, overwrites the
        # changed values and builds the search_term.
        query = SEARCH_TERM_QUERY.where(
            schema.NatuurlijkPersoon.uuid == natuurlijk_persoon_uuid
        )
        search_terms_definition = [
            "first_names",
            "initials",
            "insertions",
            "family_name",
            "burgerservicenummer",
            "person_number",
            "date_of_birth",
            "date_of_death",
            "address.street",
            "address.street_number",
            "address.street_number_letter",
            "address.city",
            "address.zipcode",
            "correspondence_address.street",
            "correspondence_address.street_number",
            "correspondence_address.street_number_letter",
            "correspondence_address.city",
            "correspondence_address.zipcode",
            "contact_information.email",
        ]
        search_term_data = self.session.execute(query).fetchone()
        search_term_dict = (
            {}
            if not search_term_data
            else {k: v for k, v in search_term_data._mapping.items() if v}
        )
        # update changes into dict
        NOT_FOUND = "NOT FOUND"
        for item in search_terms_definition:
            # None is allowed value, so define NOT FOUND as default
            # (which is not expected as a normal value)
            changed_item_value = getattr_from_dict(
                obj=changes, attr=item, default_val=NOT_FOUND
            )
            if changed_item_value != NOT_FOUND:
                search_term_dict[item] = changed_item_value

        search_term = (" ").join(
            str(v) for k, v in search_term_dict.items() if v
        )
        return search_term

    def _is_active(self, query_result):
        if not query_result.active:
            return False
        if query_result.date_of_death:
            return False
        return True

    def _bsn_retrieved(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        """
        This is needed only for log reasons but we do it with logging that
        is why this is not needed
        """
        pass

    def _update_bsn_non_authentic(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        changes = event.format_changes()
        # Update Contact BSN
        self.session.execute(
            sql.update(schema.NatuurlijkPersoon)
            .where(schema.NatuurlijkPersoon.uuid == event.entity_id)
            .values(burgerservicenummer=changes["bsn"])
            .execution_options(synchronize_session=False)
        )

    def _update_sedula_number_non_authentic(
        self, event: Event, dry_run: bool, user_info: UserInfo = None
    ):
        changes = event.format_changes()
        # Update Contact SedulaNumber
        self.session.execute(
            sql.update(schema.NatuurlijkPersoon)
            .where(schema.NatuurlijkPersoon.uuid == event.entity_id)
            .values(persoonsnummer=changes["sedula_number"])
            .execution_options(synchronize_session=False)
        )
