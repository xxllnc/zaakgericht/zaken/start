# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import DepartmentSummary, Role
from minty.entity import EntityCollection
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema

role_query = sql.select(
    schema.Role.uuid,
    schema.Role.name,
    schema.Role.description,
    schema.Group.uuid.label("parent_uuid"),
    schema.Group.name.label("parent_name"),
).select_from(
    sql.join(
        schema.Role,
        schema.Group,
        schema.Role.parent_group_id == schema.Group.id,
    )
)


class RoleRepository(ZaaksysteemRepositoryBase):
    def find(self, uuid):
        """Find a single role by uuid."""

        qry_stmt = role_query.where(schema.Role.uuid == uuid)

        role_row = self.session.execute(qry_stmt).fetchone()

        if not role_row:
            raise NotFound(
                f"Role with uuid '{uuid}' not found.", "role/not_found"
            )
        return self._entity_from_row(row=role_row)

    def find_by_id(self, id):
        """Find a single role by id."""

        qry_stmt = role_query.where(schema.Role.id == id)

        role_row = self.session.execute(qry_stmt).fetchone()

        if not role_row:
            raise NotFound(f"Role with id '{id}' not found.", "role/not_found")
        return self._entity_from_row(row=role_row)

    def find_all(
        self, filter_parent_uuid: UUID | None = None
    ) -> EntityCollection[Role]:
        """Get all roles, possibly filtered by department.

        Roles from underlying departments are also found."""

        query = role_query
        if filter_parent_uuid is not None:
            group_alias1 = sql.alias(schema.Group)
            group_alias2 = sql.alias(schema.Group)

            group_ids_subquery = (
                sql.select(group_alias2.c.id)
                .select_from(
                    sql.join(
                        group_alias1,
                        group_alias2,
                        sql.or_(
                            # PostgreSQL arrays start at 1 so this is the first
                            # element of the path: the root.
                            group_alias2.c.id == group_alias1.c.path[1],
                            group_alias2.c.id == group_alias1.c.id,
                        ),
                    )
                )
                .where(group_alias1.c.uuid == filter_parent_uuid)
            )

            query = query.where(schema.Group.id.in_(group_ids_subquery))

        role_rows = self.session.execute(query).fetchall()

        return EntityCollection[Role](
            [self._entity_from_row(row=r) for r in role_rows]
        )

    def find_by_uuid_list(self, role_uuid: list[UUID]):
        query = role_query.where(schema.Role.uuid.in_(role_uuid))
        role_rows = self.session.execute(query).fetchall()

        return EntityCollection[Role](
            [self._entity_from_row(row=r) for r in role_rows]
        )

    def _entity_from_row(self, row) -> Role:
        """Initialize Department Entity from a database row"""
        if row.parent_uuid is not None:
            parent = DepartmentSummary(
                entity_id=row.parent_uuid,
                entity_meta_summary=row.parent_name,
                uuid=row.parent_uuid,
                name=row.parent_name,
            )
        else:
            parent = None

        role = Role(
            entity_id=row.uuid,
            entity_meta_summary=row.name,
            uuid=row.uuid,
            name=row.name,
            description=row.description or "",
            parent=parent,
        )
        return role
