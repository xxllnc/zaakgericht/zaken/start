# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.case_management.entities import Department, Subject
from zsnl_domains.database import schema


class SubjectRepository(ZaaksysteemRepositoryBase):
    _for_entity = "subject"
    _events_to_calls: dict[str, str] = {}
    """Get Subject by uuid.

    :param DatabaseRepositoryBase: DatabaseRepositoryBase
    :type DatabaseRepositoryBase: DatabaseRepositoryBase
    :raises NotFound: When subject canot be found.
    :return: Subject Entity
    :rtype: Subject
    """

    def find_subject_by_uuid(self, subject_uuid: UUID):
        subject_qry = sql.select(
            schema.Subject.uuid,
            schema.Subject.id,
            schema.Subject.subject_type,
            schema.Subject.properties,
            schema.Subject.settings,
            schema.Subject.username,
            schema.Subject.last_modified,
            schema.Subject.role_ids,
            schema.Subject.group_ids,
            schema.Subject.nobody,
            schema.Subject.system,
            schema.Group.uuid.label("department_uuid"),
            schema.Group.name.label("department_name"),
        ).where(
            sql.and_(
                schema.Subject.uuid == subject_uuid,
                schema.Group.id == schema.Subject.group_ids[1],
            )
        )
        subject_row = self.session.execute(subject_qry).fetchone()
        if subject_row is None:
            raise NotFound(
                f"Subject with uuid '{subject_uuid}' not found.",
                "subject/not_found",
            )

        return Subject(
            entity_id=subject_row.uuid,
            id=subject_row.id,
            uuid=subject_row.uuid,
            subject_type=subject_row.subject_type,
            properties=subject_row.properties,
            settings=subject_row.settings,
            username=subject_row.username,
            last_modified=subject_row.last_modified,
            role_ids=subject_row.role_ids,
            group_ids=subject_row.group_ids,
            nobody=subject_row.nobody,
            system=subject_row.system,
            department=Department(
                uuid=subject_row.department_uuid,
                name=subject_row.department_name,
            ),
        )
