# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from ... import ZaaksysteemRepositoryBase
from .. import entities
from minty import cqrs
from minty import exceptions as minty_exc
from sqlalchemy import dialects, sql
from sqlalchemy import exc as sql_exc
from sqlalchemy import types as sqltypes
from typing import Any, TypedDict
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)
from zsnl_domains.shared.util import escape_term_for_like

task_query = sql.select(
    schema.Case.uuid.label("case_uuid"),
    schema.Case.id.label("case_id"),
    schema.Case.milestone.label("case_milestone"),
    schema.Case.uuid.label("case_status"),
    schema.Checklist.case_milestone.label("phase"),
    schema.ChecklistItem.uuid,
    schema.ChecklistItem.label,
    schema.ChecklistItem.description,
    schema.ChecklistItem.due_date,
    schema.ChecklistItem.state,
    schema.ChecklistItem.user_defined,
    schema.ChecklistItem.product_code,
    schema.ChecklistItem.dso_action_request,
    schema.Subject.uuid.label("assignee_uuid"),
    sql.func.get_display_name_for_employee(
        dialects.postgresql.hstore(
            dialects.postgresql.array(["properties"]),
            dialects.postgresql.array([schema.Subject.properties]),
        )
    ).label("assignee_display_name"),
    schema.Subject.properties.label("assignee_properties"),
    schema.NatuurlijkPersoon.uuid.label("assignee_person_uuid"),
    sql.func.get_display_name_for_person(
        dialects.postgresql.hstore(
            dialects.postgresql.array(
                ["voorletters", "naamgebruik", "geslachtsnaam"]
            ),
            dialects.postgresql.array(
                [
                    schema.NatuurlijkPersoon.voorletters,
                    schema.NatuurlijkPersoon.naamgebruik,
                    schema.NatuurlijkPersoon.geslachtsnaam,
                ]
            ),
        )
    ).label("assignee_person_display_name"),
    schema.Bedrijf.uuid.label("assignee_organization_uuid"),
    sql.func.get_display_name_for_company(
        dialects.postgresql.hstore(
            dialects.postgresql.array(["handelsnaam"]),
            dialects.postgresql.array([schema.Bedrijf.handelsnaam]),
        )
    ).label("assignee_organization_display_name"),
    schema.Group.name.label("department_name"),
    schema.Group.uuid.label("department_uuid"),
    schema.ZaaktypeNode.titel.label("case_type_title"),
    schema.Zaaktype.uuid.label("case_type_uuid"),
    sql.cast(
        sql.func.coalesce(
            sql.cast(
                schema.ZaaktypeNode.properties, dialects.postgresql.JSON
            ).op("->>")("allow_external_task_assignment"),
            "0",
        ),
        sqltypes.Boolean,
    ).label("allow_external_task_assignment"),
).select_from(
    sql.join(
        schema.Checklist,
        schema.ChecklistItem,
        schema.Checklist.id == schema.ChecklistItem.checklist_id,
    )
    .join(
        schema.Case,
        schema.Case.id == schema.Checklist.case_id,
    )
    .join(
        schema.ZaaktypeNode,
        schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
    )
    .join(
        schema.Zaaktype,
        schema.Case.zaaktype_id == schema.Zaaktype.id,
    )
    .join(
        schema.Subject,
        schema.ChecklistItem.assignee_id == schema.Subject.id,
        isouter=True,
    )
    .join(
        schema.NatuurlijkPersoon,
        schema.ChecklistItem.assignee_person_id == schema.NatuurlijkPersoon.id,
        isouter=True,
    )
    .join(
        schema.Bedrijf,
        schema.ChecklistItem.assignee_organization_id == schema.Bedrijf.id,
        isouter=True,
    )
    .join(
        right=schema.Group,
        onclause=schema.Case.route_ou == schema.Group.id,
    )
)

DEFAULT_SORT_ORDER = [
    schema.Checklist.case_milestone,
    schema.ChecklistItem.sequence,
    schema.ChecklistItem.id,
]

TASK_SORT_ORDERS = {
    "attributes.title": [
        sql.asc(schema.ChecklistItem.label),
        *DEFAULT_SORT_ORDER,
    ],
    "attributes.due_date": [
        sql.asc(schema.ChecklistItem.due_date),
        *DEFAULT_SORT_ORDER,
    ],
    "relationships.case.number": [
        sql.asc(schema.Case.id),
        *DEFAULT_SORT_ORDER,
    ],
    "relationships.assignee.name": [
        sql.asc(
            sql.cast(schema.Subject.properties, dialects.postgresql.JSON)[
                "displayname"
            ].astext
        ),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.title": [
        sql.desc(schema.ChecklistItem.label),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.due_date": [
        sql.desc(schema.ChecklistItem.due_date),
        *DEFAULT_SORT_ORDER,
    ],
    "-relationships.case.number": [
        sql.desc(schema.Case.id),
        *DEFAULT_SORT_ORDER,
    ],
    "-relationships.assignee.name": [
        sql.desc(
            sql.cast(schema.Subject.properties, dialects.postgresql.JSON)[
                "displayname"
            ].astext
        ),
        *DEFAULT_SORT_ORDER,
    ],
}


def _apply_filters_to_query(
    query,
    case_uuids,
    case_ids,
    case_type_uuids,
    assignee_uuids,
    department_uuids,
):
    if case_uuids:
        query = query.where(schema.Case.uuid.in_(case_uuids))

    if case_ids:
        query = query.where(schema.Case.id.in_(case_ids))

    if case_type_uuids:
        query = query.where(schema.Zaaktype.uuid.in_(case_type_uuids))

    if assignee_uuids:
        query = query.where(schema.Subject.uuid.in_(assignee_uuids))

    if department_uuids:
        query = query.where(schema.Group.uuid.in_(department_uuids))

    return query


class MessageAssigneeData(TypedDict):
    "Data structure for assignee data used to create `schema.Message` rows"

    legacy_id: str | None
    uuid: UUID | None


class TaskRepository(ZaaksysteemRepositoryBase):
    def create_task(
        self, uuid: UUID, title: str, case: entities.Case, phase: int
    ) -> entities.Task:
        """Create a new task for supplied case & phase.

        :return: Task
        :rtype: entities.Task
        """

        task = entities.Task(
            uuid=uuid,
            title=None,
            description=None,
            case=None,
            case_type=None,
            due_date=None,
            completed=None,
            user_defined=None,
            assignee=None,
            phase=None,
            department=None,
            product_code=None,
            dso_action_request=None,
        )

        task.event_service = self.event_service
        task.create(title=title, case=case, phase=phase)
        return task

    def get_task(
        self, task_uuid: UUID, permission: str, user_info: minty.cqrs.UserInfo
    ) -> entities.Task:
        """Get task by uuid.
        :raises minty_exc.Forbidden: No permission on related case.
        :return: Task
        :rtype: entities.Task
        """
        stmt = task_query

        where_clause = sql.and_(
            schema.ChecklistItem.uuid == task_uuid,
            user_allowed_cases_subquery(user_info, permission=permission),
        )
        qry = self.session.execute(stmt.where(where_clause))

        task_row = qry.fetchone()

        # no result means we assume the user doesn't have permission to case.
        if not task_row:
            raise minty_exc.Forbidden(
                "Not allowed to perform action on task for this case.",
                "case/task/not_allowed",
            )

        return self._transform_to_entity(row=task_row)

    def get_task_list(
        self,
        user_info: minty.cqrs.UserInfo,
        page: int,
        page_size: int,
        phase: int | None = None,
        completed: bool | None = None,
        assignee_uuids: list[UUID] | None = None,
        case_uuids: list[UUID] | None = None,
        case_ids: list[int] | None = None,
        case_type_uuids: list[UUID] | None = None,
        department_uuids: list[UUID] | None = None,
        title_words: list[str] | None = None,
        keyword: str | None = None,
        sort: str | None = None,
    ) -> list[entities.Task]:
        stmt = task_query.where(
            user_allowed_cases_subquery(user_info=user_info, permission="read")
        )

        stmt = _apply_filters_to_query(
            stmt,
            case_uuids=case_uuids,
            case_ids=case_ids,
            case_type_uuids=case_type_uuids,
            assignee_uuids=assignee_uuids,
            department_uuids=department_uuids,
        )

        if title_words:
            word_filters = []
            for word in title_words:
                escaped_word = escape_term_for_like(word)
                word_filters.append(
                    schema.ChecklistItem.label.ilike(
                        f"%{escaped_word}%", escape="~"
                    )
                )

            stmt = stmt.where(sql.or_(*word_filters))

        if keyword:
            escaped_keyword = escape_term_for_like(keyword)
            stmt = stmt.where(
                schema.ChecklistItem.label.ilike(
                    f"%{escaped_keyword}%", escape="~"
                ),
            )

        if phase:
            stmt = stmt.where(schema.Checklist.case_milestone == phase)

        if completed is not None:
            stmt = stmt.where(schema.ChecklistItem.state == completed)

        if sort:
            sort_order = TASK_SORT_ORDERS[sort]
        else:
            # Default sort order; used in cases
            sort_order = DEFAULT_SORT_ORDER

        offset = self._calculate_offset(page, page_size)
        stmt = stmt.order_by(*sort_order).limit(page_size).offset(offset)

        query = self.session.execute(stmt)
        task_rows = query.fetchall()

        return [self._transform_to_entity(row=row) for row in task_rows]

    def _transform_to_entity(self, row: Any) -> entities.Task:
        case_dict = {
            "uuid": row.case_uuid,
            "id": row.case_id,
            "status": row.case_status,
            "milestone": row.case_milestone,
        }

        case_type_dict = {
            "uuid": row.case_type_uuid,
            "title": row.case_type_title,
        }

        department_dict = {
            "type": "department",
            "uuid": row.department_uuid,
            "display_name": row.department_name,
        }

        task = entities.Task(
            uuid=row.uuid,
            title=row.label,
            description=row.description,
            due_date=row.due_date,
            completed=row.state,
            user_defined=row.user_defined,
            assignee=self._extract_assignee(row=row),
            case=case_dict,
            case_type=case_type_dict,
            phase=row.phase,
            department=department_dict,
            can_assign_externally=row.allow_external_task_assignment,
            product_code=row.product_code,
            dso_action_request=row.dso_action_request,
        )
        task.event_service = self.event_service
        return task

    def _extract_assignee(
        self, row: Any
    ) -> entities.task.TaskAssigneeData | None:
        """Extract assignee from result row.

        :param row: result row
        :type row: object
        :return: assignee dict
        :rtype: dict
        """
        if row.assignee_uuid:
            return entities.task.TaskAssigneeData(
                type=entities._shared.ContactType.employee,
                id=row.assignee_uuid,
                display_name=row.assignee_display_name,
            )
        elif row.assignee_person_uuid:
            return entities.task.TaskAssigneeData(
                type=entities._shared.ContactType.person,
                id=row.assignee_person_uuid,
                display_name=row.assignee_person_display_name,
            )
        elif row.assignee_organization_uuid:
            return entities.task.TaskAssigneeData(
                type=entities._shared.ContactType.organization,
                id=row.assignee_organization_uuid,
                display_name=row.assignee_organization_display_name,
            )

        return None

    def save(self):
        for ev in self.event_service.get_events_by_type(entity_type="Task"):
            if ev.event_name == "TaskCreated":
                self._insert_task(event=ev)
            elif ev.event_name == "TaskDeleted":
                self._delete_task(event=ev)
            elif ev.event_name == "TaskUpdated":
                self._update_task(event=ev)
            elif ev.event_name == "TaskCompletionSet":
                self._set_completion(event=ev)

    def _set_completion(self, event: cqrs.Event):
        """Update completion in database.

        :param event: TaskCompleted event
        :type event: cqrs.Event
        """
        changes = event.format_changes()
        values = {"state": changes["completed"]}
        update_stmt = (
            sql.update(schema.ChecklistItem)
            .where(schema.ChecklistItem.uuid == event.entity_id)
            .values(values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_stmt)

    def _get_assignee(self, type: str, uuid: str):
        assignee = {
            "assignee_id": None,
            "assignee_person_id": None,
            "assignee_organization_id": None,
        }

        if type == "employee":
            row = self.session.execute(
                sql.select(schema.Subject.id).where(
                    schema.Subject.uuid == uuid
                )
            ).fetchone()

            if row:
                assignee["assignee_id"] = row.id
        elif type == "person":
            row = self.session.execute(
                sql.select(schema.NatuurlijkPersoon.id).where(
                    schema.NatuurlijkPersoon.uuid == uuid
                )
            ).fetchone()

            if row:
                assignee["assignee_person_id"] = row.id
        elif type == "organization":
            row = self.session.execute(
                sql.select(schema.Bedrijf.id).where(
                    schema.Bedrijf.uuid == uuid
                )
            ).fetchone()

            if row:
                assignee["assignee_organization_id"] = row.id

        return assignee

    def _update_task(self, event: cqrs.Event):
        """
        Update task in database.
        """
        changes = event.format_changes()
        if changes["assignee"]:
            assignee = self._get_assignee(
                changes["assignee"]["type"], changes["assignee"]["id"]
            )

            elements_present = list(
                filter(lambda x: x is not None, assignee.values())
            )
            if len(elements_present) != 1:
                raise minty.exceptions.NotFound("Assignee not found")

        else:
            assignee = {
                "assignee_id": None,
                "assignee_person_id": None,
                "assignee_organization_id": None,
            }

        values = {
            "label": changes["title"],
            "due_date": changes["due_date"],
            "description": changes["description"],
            "product_code": changes["product_code"],
            "dso_action_request": changes["dso_action_request"],
            **assignee,
        }
        update_stmt = (
            sql.update(schema.ChecklistItem)
            .where(schema.ChecklistItem.uuid == event.entity_id)
            .values(values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_stmt)

    def _delete_task(self, event: cqrs.Event):
        """Delete task in database.

        :param event: TaskDeleted event
        :type event: cqrs.Event
        """
        task_uuid = event.entity_id
        delete_stmt = (
            sql.delete(schema.ChecklistItem)
            .where(schema.ChecklistItem.uuid == task_uuid)
            .execution_options(synchronize_session=False)
        )

        self.session.execute(delete_stmt)

    def _insert_task(self, event: cqrs.Event):
        """Insert new task in database

        :param event: TaskCreated event
        :type event: cqrs.Event
        :raises minty_exc.Conflict: when uuid already exists
        """
        changes = event.format_changes()
        case_uuid = changes["case"]["uuid"]
        values = {
            "checklist_id": self._get_phase_id(
                case_uuid=case_uuid, phase=changes["phase"]
            ),
            "label": changes["title"],
            "state": changes["completed"],
            "sequence": None,
            "user_defined": changes["user_defined"],
            "uuid": event.entity_id,
            "due_date": None,
            "description": changes["description"],
            "assignee_id": None,
        }
        insert_stmt = sql.insert(schema.ChecklistItem).values(values)

        try:
            self.session.execute(insert_stmt)
        except sql_exc.IntegrityError as err:
            raise minty_exc.Conflict(
                f"entities.Task with uuid '{event.entity_id}' already exists."
            ) from err

    def _get_phase_id(self, case_uuid: UUID, phase: int) -> int:
        """Get phase id (checklist.id) or call insert checklist function.

        :param case_uuid: uuid for case
        :type case_uuid: UUID
        :param phase: phase
        :type phase: int
        :return: phase_id (checklist.id)
        :rtype: int
        """
        stmt = sql.select(schema.Checklist.id).where(
            sql.and_(
                schema.Checklist.case_id
                == sql.select(schema.Case.id)
                .where(schema.Case.uuid == case_uuid)
                .scalar_subquery(),
                schema.Checklist.case_milestone == phase,
            )
        )
        result = self.session.execute(stmt).fetchone()

        if result:
            phase_id = result.id
        else:
            phase_id = self._insert_phase(case_uuid=case_uuid, phase=phase)
        return phase_id

    def _insert_phase(self, case_uuid: UUID, phase: int) -> int:
        """Insert new phase (checklist) in database.

        :param case_uuid: case uuid
        :type case_uuid: UUID
        :param phase: phase
        :type phase: int
        :return: phase_id (checklist.id)
        :rtype: int
        """
        values = {
            "case_id": sql.select(schema.Case.id)
            .where(schema.Case.uuid == case_uuid)
            .scalar_subquery(),
            "case_milestone": phase,
        }
        insert_stmt = self.session.execute(
            sql.insert(schema.Checklist).values(values)
        )
        phase_id = insert_stmt.inserted_primary_key[0]
        return phase_id

    def create_task_status_notification_for_case_assignee(
        self, task: entities.Task, user_uuid: UUID
    ):
        """Create notification for `case_assignee` when task is completed."

        Only creates notification when user is not case_asssignee.

        :param task: task
        :type task: entities.Task
        :param user_uuid: user
        :type user_uuid: UUID
        """
        onderwerp = f'Taak "{task.title}" {"afgerond" if task.completed else "heropend"}.'
        case_id = task.case["id"]
        event_type = "case/task/set_completion"

        logging_id = self._insert_logging(
            case_id=case_id,
            subject=onderwerp,
            user_uuid=user_uuid,
            event_type=event_type,
        )
        subject_data = self._get_case_assignee_contact_id(case_id=case_id)

        if str(user_uuid) != str(subject_data["uuid"]):
            self._insert_notification(
                logging_id=logging_id,
                subject=onderwerp,
                assignee_uuid=subject_data["uuid"],
            )

    def create_assigned_notification_for_task_assignee(
        self, task: entities.Task, user_uuid: UUID, assignee_uuid: UUID
    ):
        """Create notification of new task assigment.

        :param task: task
        :type task: entities.Task
        :param user_uuid: user
        :type user_uuid: UUID
        :param assignee: new assignee
        :type assignee: entities.Employee
        """
        onderwerp = f'Taak "{task.title}" toegewezen.'
        case_id = task.case["id"]
        event_type = "case/task/new_assignee"

        logging_id = self._insert_logging(
            case_id=case_id,
            subject=onderwerp,
            user_uuid=user_uuid,
            event_type=event_type,
        )
        self._insert_notification(
            logging_id=logging_id,
            subject=onderwerp,
            assignee_uuid=assignee_uuid,
        )

    def _insert_notification(
        self, logging_id: int, subject: str, assignee_uuid: UUID | None
    ):
        insert_values_notification = {
            "logging_id": logging_id,
            "message": subject,
            "subject_id": sql.select(
                "betrokkene-medewerker-"
                + sql.cast(schema.Subject.id, sqltypes.VARCHAR),
            )
            .where(schema.Subject.uuid == assignee_uuid)
            .scalar_subquery(),
            "is_read": False,
            "is_archived": False,
        }
        insert_stmt_notification = sql.insert(schema.Message).values(
            {**insert_values_notification}
        )

        self.session.execute(insert_stmt_notification)

    def _insert_logging(
        self, case_id: int, subject: str, user_uuid: UUID, event_type: str
    ) -> int:
        restricted_case = sql.case(
            (schema.Case.confidentiality == "confidential", True),
            else_=False,
        )

        restricted = (
            sql.select(restricted_case)
            .where(schema.Case.id == case_id)
            .scalar_subquery()
        )

        created_by_name = (
            sql.select(
                sql.cast(schema.Subject.properties, dialects.postgresql.JSON)[
                    "displayname"
                ]
            )
            .where(schema.Subject.uuid == user_uuid)
            .scalar_subquery()
        )

        insert_values_logging = {
            "zaak_id": case_id,
            "component": "zaak",
            "onderwerp": subject,
            "event_type": event_type,
            "event_data": {"case_id": f"{case_id}", "content": ""},
            "restricted": restricted,
            "created_by_name_cache": created_by_name,
        }
        insert_stmt_logging = sql.insert(schema.Logging).values(
            {**insert_values_logging}
        )
        logging_id = self.session.execute(
            insert_stmt_logging
        ).inserted_primary_key[0]
        return logging_id

    def _get_case_assignee_contact_id(
        self, case_id: UUID
    ) -> MessageAssigneeData:
        """Temp function to create notification and logging."""

        joined_qry = sql.join(
            schema.Case,
            schema.ZaakBetrokkenen,
            schema.Case.behandelaar == schema.ZaakBetrokkenen.id,
        ).join(
            schema.Subject,
            sql.and_(
                schema.Subject.id == schema.ZaakBetrokkenen.betrokkene_id,
                schema.ZaakBetrokkenen.betrokkene_type == "medewerker",
            ),
        )

        qry = (
            sql.select(
                schema.ZaakBetrokkenen.betrokkene_type,
                schema.ZaakBetrokkenen.betrokkene_id,
                schema.Subject.uuid,
            )
            .select_from(joined_qry)
            .where(schema.Case.id == case_id)
        )
        res = self.session.execute(qry).fetchone()
        if res is None:
            return {"legacy_id": None, "uuid": None}

        return {
            "legacy_id": f"betrokkene-{res.betrokkene_type}-{res.betrokkene_id}",
            "uuid": res.uuid,
        }
