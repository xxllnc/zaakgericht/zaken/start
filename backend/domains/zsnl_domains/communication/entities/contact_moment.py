# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .contact import Contact
from .message import Message
from datetime import datetime, timezone
from minty.cqrs import event
from uuid import UUID


class ContactMoment(Message):
    def __init__(
        self,
        uuid: UUID,
        thread_uuid: UUID,
        case_uuid: UUID | None,
        created_by: Contact,
        created_by_displayname: str,
        recipient: Contact,
        recipient_displayname: str,
        message_slug: str,
        last_modified: datetime,
        created_date: datetime,
        message_date: datetime,
        content: str,
        channel: str,
        direction: str,
    ):
        super().__init__(
            message_type="contact_moment",
            uuid=uuid,
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            message_slug=message_slug,
            created_date=created_date,
            message_date=message_date,
            last_modified=last_modified,
            created_by=created_by,
            created_by_displayname=created_by_displayname,
        )
        self.content = content
        self.direction = direction
        self.channel = channel
        self.recipient = recipient
        self.recipient_displayname = recipient_displayname

    @event("ContactMomentCreated", extra_fields=["thread_uuid"])
    def create(
        self,
        thread_uuid: UUID,
        created_by: Contact,
        recipient: Contact,
        content,
        channel,
        direction,
    ):
        now = datetime.now(timezone.utc).isoformat()

        self.thread_uuid = thread_uuid
        self.message_slug = content[:180]
        self.message_type = self.message_type
        self.created_by = created_by
        self.created_by_displayname = created_by.name
        self.content = content
        self.channel = channel
        self.direction = direction
        self.recipient = recipient
        self.recipient_displayname = recipient.name
        self.created_date = now
        self.last_modified = now
