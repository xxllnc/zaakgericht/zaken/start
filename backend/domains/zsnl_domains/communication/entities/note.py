# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .contact import Contact
from .message import Message
from datetime import datetime, timezone
from minty.cqrs import event
from uuid import UUID


class Note(Message):
    def __init__(
        self,
        uuid: UUID,
        thread_uuid: UUID,
        case_uuid: UUID | None,
        created_by: Contact,
        message_slug: str,
        last_modified: datetime,
        created_date: datetime,
        message_date: datetime,
        content: str,
        created_by_displayname: str,
    ):
        super().__init__(
            message_type="note",
            uuid=uuid,
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            message_slug=message_slug,
            created_date=created_date,
            message_date=message_date,
            last_modified=last_modified,
            created_by=created_by,
            created_by_displayname=created_by_displayname,
        )
        self.content = content

    @event("NoteCreated", extra_fields=["thread_uuid"])
    def create(self, thread_uuid: UUID, created_by: Contact, content: str):
        now = datetime.now(timezone.utc).isoformat()

        self.thread_uuid = thread_uuid
        self.message_slug = content[:180]
        self.message_type = self.message_type
        self.created_by = created_by
        self.created_by_displayname = created_by.name
        self.content = content
        self.created_date = now
        self.last_modified = now
