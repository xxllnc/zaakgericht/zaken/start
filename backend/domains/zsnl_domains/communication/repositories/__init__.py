# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .attachment import AttachmentRepository
from .case import CaseRepository
from .contact import ContactRepository
from .contact_moment_overview import ContactMomentOverviewRepository
from .email_template import EmailTemplateRepository
from .file import FileRepository
from .message import MessageRepository
from .thread import ThreadRepository

__all__ = [
    "AttachmentRepository",
    "CaseRepository",
    "ContactMomentOverviewRepository",
    "ContactRepository",
    "EmailTemplateRepository",
    "FileRepository",
    "MessageRepository",
    "ThreadRepository",
]
