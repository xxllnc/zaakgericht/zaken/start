# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities.case import Case
from . import database_queries
from collections.abc import Iterable
from minty.exceptions import Conflict, Forbidden, NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class CaseRepository(ZaaksysteemRepositoryBase):
    def find_case_by_uuid(
        self, case_uuid: UUID, user_uuid: UUID, permission: str
    ):
        """Return case id for a given case_uuid by fetching it from database with ACL."""
        case_query = database_queries.find_case_query(
            case_uuid=case_uuid,
            user_uuid=user_uuid,
            permission=permission,
            db=self.session,
        )

        result = self.session.execute(case_query).fetchall()

        if len(result) == 0:
            raise NotFound("Case not found.", "communication/case/not_found")
        if len(result) > 1:
            raise Conflict(
                "Multiple cases found with the same UUID.",
                "communication/case/multiple_found",
            )

        return self._transform_to_entity(result[0])

    def unsafe_find_case_by_uuid(self, case_uuid: UUID):
        """
        Return case id for a given case_uuid by fetching it from database.

        WARNING! This method does NOT do ACL checks.
        You probably need to use `find_case_by_uuid()` instead!
        """
        case_query = database_queries.unsafe_case_query.where(
            schema.Case.uuid == case_uuid
        )

        result = self.session.execute(case_query).fetchall()

        if len(result) == 0:
            raise NotFound("Case not found.", "communication/case/not_found")
        if len(result) > 1:
            raise Conflict(
                "Multiple cases found with the same UUID.",
                "communication/case/multiple_found",
            )

        return self._transform_to_entity(result[0])

    def get_case_by_id(self, case_id: int):
        """Return case for a given `case_id`, without ACL checks, but excluding
        resolved cases."""
        query = database_queries.unsafe_case_query.where(
            sql.and_(
                schema.Case.id == case_id, schema.Case.status != "resolved"
            )
        )

        result = self.session.execute(query).fetchall()

        if len(result) == 0:
            raise NotFound("Case not found.", "communication/case/not_found")
        if len(result) > 1:
            raise Conflict(
                "Multiple cases found with the same id.",
                "communication/case/multiple_found",
            )

        case = result[0]

        return self._transform_to_entity(case)

    def get_case_for_pip_user(self, user_uuid: UUID, case_uuid: UUID):
        """Return Case entity for a given PIP user_uuid and case_uuid by fetching it from database with pip ACL."""
        case = self.session.execute(
            database_queries.get_case_for_pip_query(
                case_uuid=case_uuid, user_uuid=user_uuid
            )
        ).fetchone()

        if not case:
            raise Forbidden(
                "Not allowed to view this case",
                "communication/case/not_allowed",
            )

        return self._transform_to_entity(case)

    def find_case_for_pip_user(self, user_uuid: UUID, thread_uuid: UUID):
        """Return Case entity for a given PIP user_uuid by fetching it from database with pip ACL."""
        case = self.session.execute(
            database_queries.get_case_from_thread_query(
                thread_uuid=thread_uuid, user_uuid=user_uuid
            )
        ).fetchone()

        if not case:
            raise NotFound("Case not found.", "communication/case/not_found")

        return self._transform_to_entity(case)

    def find_case_by_thread_uuid(
        self, thread_uuid: UUID, user_uuid: UUID, user_info
    ) -> Case or None:
        """Find case by uuid.

        :param thread_uuid: thread uuid
        :type thread_uuid: UUID
        :param user_uuid: user uuid
        :type user_uuid: UUID
        :param user_info: User info from the request
        :type user_info: object
        :raises Forbidden: when user has no permission to case
        :raises NotFound: when thread is not found
        :return: result
        :rtype: Case or None
        """

        if user_info.permissions.get("pip_user", False):
            return self.find_case_for_pip_user(user_uuid, thread_uuid)

        qry = database_queries.case_by_thread_uuid(
            db=self.session,
            user_uuid=user_uuid,
            permission="read",
            thread_uuid=thread_uuid,
        )

        result = self.session.execute(qry).fetchone()

        if result is None:
            raise NotFound(
                f"Thread with id: '{thread_uuid}' not found.",
                "communication/thread/not_found",
            )

        if result.case_id is None:
            case = None
        elif result.case_uuid is not None:
            case = self._transform_to_entity(result)
        elif result.case_uuid is None:
            raise Forbidden(
                f"Not allowed to view communication for thread with id: '{thread_uuid}'",
                "communication/thread/not_allowed",
            )

        return case

    def search_cases(
        self,
        search_term: str,
        user_uuid: UUID,
        permission: str,
        case_status_filter: Iterable[str] | None = None,
        limit: int | None = None,
    ) -> list[Case]:
        """Retrieve a list of Cases, given a search term and a requested permission.
        Limit parameter can be a integer or none in which case a full list of results will be returned.

        If the keyword is None or request permission is not a permission the user has,
        it will return no result(empty list).

        :param search_term: The search term to retrieve Cases.
        :type search_term: string
        :param user_uuid: Currently logged in user making the request.
        :type user_uuid: UUID
        :param permission: The requested permission for a case.
        :type permission: string
        :param case_status_filter: List of case statuses to filter on. Only
            cases with one of the supplied statuses will be returned.
        :type case_status_filter: Iterable[str] or None
        :param limit: The limit to return the result list of cases.
        :type limit: int or None
        :return: A result set of Case objects
        :rtype: List[Contact]
        """

        case_query = database_queries.search_cases_query(
            search_term=search_term,
            user_uuid=user_uuid,
            db=self.session,
            permission=permission,
        )

        if case_status_filter:
            case_query = case_query.where(
                schema.Case.status.in_(case_status_filter)
            )

        if limit:
            case_query = case_query.limit(limit)

        cases = self.session.execute(case_query).fetchall()

        return [self._transform_to_entity(case_row) for case_row in cases]

    def get_case_list_for_contact(
        self, is_pip_user: bool, user_uuid: UUID, contact_uuid: UUID
    ) -> list[Case]:
        """Retrieve a list of Cases for the specified contact that the current
        user has access to

        :param user_uuid: UUID of the user requesting the case list
        :type user_uuid: UUID
        :param contact_uuid: UUID of the contact to filter for
        :type contact_uuid: UUID
        :return: A
        :rtype: List[Case]
        """
        cases = self.session.execute(
            database_queries.list_cases_query(
                db=self.session,
                is_pip_user=is_pip_user,
                user_uuid=user_uuid,
                contact_uuid=contact_uuid,
            )
        ).fetchall()

        return [self._transform_to_entity(case_row) for case_row in cases]

    def _transform_to_entity(self, case_row):
        return Case(
            id=case_row.case_id,
            uuid=case_row.case_uuid,
            status=case_row.case_status,
            description=case_row.case_description,
            description_public=case_row.case_description_public,
            case_type_name=case_row.case_type_name,
        )
