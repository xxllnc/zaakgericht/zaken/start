# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import functools
from ...shared.types import FilterUUIDExplicitEmptyValue
from .pip_acl_query import pip_acl_query
from .util import base62_decode
from sqlalchemy import orm, sql, types
from sqlalchemy.dialects.postgresql import BIT, JSON
from typing import AbstractSet, Literal, Tuple
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories import case_acl
from zsnl_domains.shared.util import escape_term_for_like, prepare_search_term

CONTACT_DATA_TYPE_PERSON = 1
CONTACT_DATA_TYPE_ORGANIZATION = 2


natural_person_query = sql.select(
    schema.NatuurlijkPersoon.id,
    schema.NatuurlijkPersoon.uuid,
    sql.func.concat(
        schema.NatuurlijkPersoon.adellijke_titel + " ",
        schema.NatuurlijkPersoon.voorletters + " ",
        schema.NatuurlijkPersoon.surname,
    ).label("name"),
    schema.NatuurlijkPersoon.object_type.label("type"),
    schema.Adres.straatnaam.label("street_name"),
    schema.Adres.huisnummer.label("house_number"),
    schema.Adres.huisletter.label("house_letter"),
    schema.Adres.huisnummertoevoeging.label("addition"),
    schema.Adres.woonplaats.label("place"),
    schema.Adres.postcode,
    sql.func.concat(
        schema.Adres.adres_buitenland1 + " ",
        schema.Adres.adres_buitenland2 + " ",
        schema.Adres.adres_buitenland3,
    ).label("abroad_address"),
    sql.case(
        (schema.ContactData.email.label("email") == "", None),
        (
            schema.ContactData.email.label("email").isnot(None),
            schema.ContactData.email.label("email"),
        ),
        else_=None,
    ).label("email"),
).select_from(
    sql.join(
        schema.NatuurlijkPersoon,
        schema.Adres,
        schema.Adres.id == schema.NatuurlijkPersoon.adres_id,
        isouter=True,
    ).join(
        schema.ContactData,
        sql.and_(
            schema.ContactData.betrokkene_type == CONTACT_DATA_TYPE_PERSON,
            schema.ContactData.gegevens_magazijn_id
            == schema.NatuurlijkPersoon.id,
        ),
        isouter=True,
    )
)


organization_address_streetnaam = sql.case(
    (
        schema.Bedrijf.vestiging_straatnaam.isnot(None),
        schema.Bedrijf.vestiging_straatnaam,
    ),
    (
        schema.Bedrijf.correspondentie_straatnaam.isnot(None),
        schema.Bedrijf.correspondentie_straatnaam,
    ),
    else_=None,
).label("street_name")


organization_address_house_number = sql.case(
    (
        schema.Bedrijf.vestiging_huisnummer.isnot(None),
        schema.Bedrijf.vestiging_huisnummer,
    ),
    (
        schema.Bedrijf.correspondentie_huisnummer.isnot(None),
        schema.Bedrijf.correspondentie_huisnummer,
    ),
    else_=None,
).label("house_number")


organization_address_house_letter = sql.case(
    (
        schema.Bedrijf.vestiging_huisletter.isnot(None),
        schema.Bedrijf.vestiging_huisletter,
    ),
    (
        schema.Bedrijf.correspondentie_huisletter.isnot(None),
        schema.Bedrijf.correspondentie_huisletter,
    ),
    else_=None,
).label("house_letter")

organization_address_addition = sql.case(
    (
        schema.Bedrijf.vestiging_huisnummertoevoeging.isnot(None),
        schema.Bedrijf.vestiging_huisnummertoevoeging,
    ),
    (
        schema.Bedrijf.correspondentie_huisnummertoevoeging.isnot(None),
        schema.Bedrijf.correspondentie_huisnummertoevoeging,
    ),
    else_=None,
).label("addition")

organization_address_place = sql.case(
    (
        schema.Bedrijf.vestiging_woonplaats.isnot(None),
        schema.Bedrijf.vestiging_woonplaats,
    ),
    (
        schema.Bedrijf.correspondentie_woonplaats.isnot(None),
        schema.Bedrijf.correspondentie_woonplaats,
    ),
    else_=None,
).label("place")

organization_address_postcode = sql.case(
    (
        schema.Bedrijf.vestiging_postcode.isnot(None),
        schema.Bedrijf.vestiging_postcode,
    ),
    (
        schema.Bedrijf.correspondentie_postcode.isnot(None),
        schema.Bedrijf.correspondentie_postcode,
    ),
    else_=None,
).label("postcode")

organization_query = sql.select(
    schema.Bedrijf.id,
    schema.Bedrijf.uuid,
    schema.Bedrijf.handelsnaam.label("name"),
    schema.Bedrijf.object_type.label("type"),
    organization_address_streetnaam,
    organization_address_house_number,
    organization_address_house_letter,
    organization_address_addition,
    organization_address_place,
    organization_address_postcode,
    sql.func.concat(
        schema.Bedrijf.vestiging_adres_buitenland1 + " ",
        schema.Bedrijf.vestiging_adres_buitenland2 + " ",
        schema.Bedrijf.vestiging_adres_buitenland3,
    ).label("abroad_address"),
    sql.case(
        (schema.ContactData.email.label("email") == "", None),
        (
            schema.ContactData.email.label("email").isnot(None),
            schema.ContactData.email.label("email"),
        ),
        else_=None,
    ).label("email"),
).select_from(
    sql.join(
        schema.Bedrijf,
        schema.ContactData,
        sql.and_(
            schema.ContactData.betrokkene_type
            == CONTACT_DATA_TYPE_ORGANIZATION,
            schema.ContactData.gegevens_magazijn_id == schema.Bedrijf.id,
        ),
        isouter=True,
    )
)


employee_query = sql.select(
    schema.Subject.id,
    schema.Subject.uuid,
    sql.cast(schema.Subject.properties, JSON)["displayname"].astext.label(
        "name"
    ),
    schema.Subject.subject_type.label("type"),
    sql.expression.null().label("street_name"),
    sql.expression.null().label("house_number"),
    sql.expression.null().label("house_letter"),
    sql.expression.null().label("addition"),
    sql.expression.null().label("place"),
    sql.expression.null().label("abroad_address"),
    sql.expression.null().label("postcode"),
    sql.cast(schema.Subject.properties, JSON)["mail"].astext.label("email"),
).where(schema.Subject.subject_type == "employee")


natural_person_simple_query = sql.select(
    schema.NatuurlijkPersoon.id,
    schema.NatuurlijkPersoon.uuid,
    sql.func.concat(
        schema.NatuurlijkPersoon.voorletters + " ",
        schema.NatuurlijkPersoon.surname,
    ).label("name"),
    sql.literal_column("'person'").label("type"),
)

organization_simple_query = sql.select(
    schema.Bedrijf.id,
    schema.Bedrijf.uuid,
    schema.Bedrijf.handelsnaam.label("name"),
    sql.literal_column("'organization'").label("type"),
)


employee_simple_query = sql.select(
    schema.Subject.id,
    schema.Subject.uuid,
    sql.cast(schema.Subject.properties, JSON)["displayname"].astext.label(
        "name"
    ),
    sql.literal_column("'employee'").label("type"),
)

# UNSAFE case query -- does not include ACLS. Use with care.
unsafe_case_query: sql.Select[
    Tuple[
        int,
        UUID | None,
        Literal["new", "open", "stalled", "resolved", "deleted"],
        str | None,
        str | None,
        str | None,
    ]
] = (
    sql.select(
        schema.Case.id.label("case_id"),
        schema.Case.uuid.label("case_uuid"),
        schema.Case.status.label("case_status"),
        schema.Case.onderwerp.label("case_description"),
        schema.Case.onderwerp_extern.label("case_description_public"),
        schema.ZaaktypeNode.titel.label("case_type_name"),
    )
    .select_from(
        sql.join(
            schema.Case,
            schema.ZaaktypeNode,
            schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
        ).join(schema.CaseMeta, schema.CaseMeta.zaak_id == schema.Case.id)
    )
    .where(
        sql.and_(
            schema.Case.deleted.is_(None), schema.Case.status != "deleted"
        )
    )
)


thread_list_query = sql.select(
    schema.Thread.id,
    schema.Thread.uuid,
    schema.Thread.contact_uuid,
    schema.Thread.contact_displayname,
    schema.Thread.case_id,
    schema.Thread.thread_type,
    schema.Thread.created,
    schema.Thread.last_modified,
    schema.Thread.last_message_cache,
    schema.Thread.message_count,
    schema.Thread.unread_pip_count,
    schema.Thread.unread_employee_count,
    schema.Case.uuid.label("case_uuid"),
    schema.Case.status.label("case_status"),
    schema.Case.onderwerp.label("case_description"),
    schema.Case.onderwerp_extern.label("case_description_public"),
    schema.ZaaktypeNode.titel.label("case_type_name"),
    schema.Thread.attachment_count,
    schema.Thread.is_notification,
).select_from(
    sql.join(
        schema.Thread,
        sql.join(
            schema.Case,
            schema.ZaaktypeNode,
            schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
        ),
        schema.Thread.case_id == schema.Case.id,
        isouter=True,
    )
)


def contact_type_query(contact_uuid: UUID):
    """Query to get the type out of either from 3 tables(Bedrijf, NatuurlijkPersoon, Subject).

    :param contact_uuid: The uuid keyword to retrieve a contact type.
    :type contact_uuid: string
    :return: SQLAlchemy query for one Contact Type
    """

    natural_person = sql.select(
        sql.literal_column("'person'").label("type")
    ).where(sql.and_(schema.NatuurlijkPersoon.uuid == contact_uuid))

    organization = sql.select(
        sql.literal_column("'organization'").label("type")
    ).where(sql.and_(schema.Bedrijf.uuid == contact_uuid))

    employee = sql.select(
        sql.literal_column("'employee'").label("type")
    ).where(
        sql.and_(
            schema.Subject.uuid == contact_uuid,
            schema.Subject.subject_type == "employee",
        )
    )
    return sql.union_all(natural_person, organization, employee)


def contact_query(contact_uuid):
    """Search from 3 tables(Bedrijf, NatuurlijkPersoon, Subject)
     and combine them with a "UNION ALL" where in the normal case one or the other table will return a row.
     Otherwise both tables where the uuid searching for,
     exists in both these tables, in that case, first row is fetched.

    :param contact_uuid: The uuid keyword to retrieve a contact.
    :type contact_uuid: string
    :return: SQLAlchemy query for one Contact row
    """

    natural_person = natural_person_simple_query.where(
        sql.and_(schema.NatuurlijkPersoon.uuid == contact_uuid)
    )

    organization = organization_simple_query.where(
        sql.and_(schema.Bedrijf.uuid == contact_uuid)
    )

    employee = employee_simple_query.where(
        sql.and_(
            schema.Subject.uuid == contact_uuid,
            schema.Subject.subject_type == "employee",
        )
    )
    return sql.union_all(natural_person, organization, employee)


def search_contact_query(keyword: str, type_filter: AbstractSet[str]):
    """Combine the generic search result from 3 tables (Bedrijf,
    NatuurlijkPersoon and Subject) and combine them with a "UNION ALL" where
    each has the same data returned to create a list of Contact.

    :param keyword: The search keyword to retrieve contents of the contacts.
    :type keyword: string
    :param type_filter: Set containing the types to include. Valid options in
        here are: "employee", "organization" and "person"
    :type type_filter: AbstractSet[str]
    :return: SQLAlchemy query for list of Contacts
    """
    keyword_escaped = "%".join(map(escape_term_for_like, keyword.split()))
    queries = []
    if "person" in type_filter:
        natural_person = (
            natural_person_query.where(
                sql.and_(
                    schema.NatuurlijkPersoon.search_term.ilike(
                        f"%{keyword_escaped}%"
                    ),
                    schema.NatuurlijkPersoon.active.is_(True),
                    schema.NatuurlijkPersoon.deleted_on.is_(None),
                )
            )
            .order_by(schema.NatuurlijkPersoon.voornamen)
            .limit(20)
        )
        queries.append(natural_person)

    if "organization" in type_filter:
        organization = (
            organization_query.where(
                sql.and_(
                    schema.Bedrijf.search_term.ilike(f"%{keyword_escaped}%"),
                    schema.Bedrijf.deleted_on.is_(None),
                )
            )
            .order_by(schema.Bedrijf.handelsnaam)
            .limit(20)
        )
        queries.append(organization)

    if "employee" in type_filter:
        employee = (
            employee_query.where(
                sql.and_(
                    sql.or_(
                        schema.Subject.username.ilike(f"%{keyword_escaped}%"),
                        sql.cast(schema.Subject.properties, types.JSON)
                        .op("->>")("displayname")
                        .ilike(f"%{keyword_escaped}%"),
                        sql.cast(schema.Subject.properties, types.JSON)
                        .op("->>")("givenname")
                        .ilike(f"%{keyword_escaped}%"),
                        sql.cast(schema.Subject.properties, types.JSON)
                        .op("->>")("sn")
                        .ilike(f"%{keyword_escaped}%"),
                    ),
                    sql.func.array_length(schema.Subject.role_ids, 1) > 0,
                )
            )
            .order_by(schema.Subject.username)
            .limit(20)
        )
        queries.append(employee)

    return sql.union_all(*queries)


def thread_by_partial_uuid_statement(partial_uuid: str):
    """Returns a query that selects a thread based on the partial UUID

    The system uses partial UUIDs in the subject of outgoing and incoming
    messages to find the correct thread.

    These partial UUIDs are the base62 representation of the 48 least
    significant bits of the thread UUID.
      dec 272255816853352 = hex f79d7f406768 = b62 1FJDPZvtQ

    The database can only handle UUIDs in their "hexadecimal" form:
      2e2aeb6f-840b-4b23-b531-f79d7f406768

    The query removes dashes, takes the rightmost 12 characters and prepends
    the letter "x" so the following "cast" knows it's hexadecimal:
      xf79d7f406768

    This is then cast to a 48-bit bit field (12 characters representing 4 bits
    each = 48 bits), which is then cast to bigint for comparison.
      272255816853352

    The database contains an index on this same expression.

    :param partial_uuid: The partial UUID from the message subjet
    :type partial_uuid: str
    :return: Database query, ready to run
    """
    partial_uuid_int = base62_decode(partial_uuid)

    return thread_list_query.where(
        sql.and_(
            # New messages can't be added if the case is already resolved
            schema.Case.status.notin_(["deleted", "resolved"]),
            schema.Case.deleted.is_(None),
            partial_uuid_int
            == sql.cast(
                sql.cast(
                    "x"
                    + sql.func.translate(
                        sql.func.right(
                            sql.cast(schema.Thread.uuid, types.Text), 12
                        ),
                        "-",
                        "",
                    ),
                    BIT(48),
                ),
                types.BigInteger,
            ),
        )
    )


def thread_by_uuid_statement(
    thread_uuid: UUID,
    check_acl: bool,
    session: orm.Session,
    user_uuid: UUID,
    permission: str,
):
    if check_acl:
        acl_clause = _case_acls_query(
            db=session, user_uuid=user_uuid, permission=permission
        )
        statement = thread_list_query.where(
            sql.and_(
                schema.Thread.uuid == thread_uuid,
                sql.or_(schema.Thread.case_id.is_(None), acl_clause),
            )
        )
    else:
        statement = thread_list_query.where(
            sql.and_(schema.Thread.uuid == thread_uuid)
        )
    return statement


def _message_types_query(message_types: set[str] | None):
    """Get the query addition of message type filtering or not(empty in the query translation).

    :param message_types: a set of message types like external, contact_moment, note.
    Validated before hitting these queries
    :type message_types: set
    :return: query or True which translates and optimized by sqlalchemy to nothing in the raw query,
    so the condition of message types is not being considered.
    """
    if message_types:
        return schema.Thread.last_message_cache.cast(JSON)[
            "message_type"
        ].astext.in_(message_types)

    return True


def _case_acls_query(db, user_uuid: UUID, permission: str = "read"):
    """Get the query addition of the case ACL's.

    :param db: SQLAlchemy session to use for retrieval
    :type db: session
    :param user_uuid: User wanting to retrieve the case
    :type user_uuid: UUID
    :param permission: Purpose the case is retrieved for. Can be one of
        "search", "read", "write" or "manage".
    :type permission: str
    :return: query
    """
    return case_acl.allowed_cases_subquery(
        db=db, user_uuid=user_uuid, permission=permission
    )


def get_threads_from_case_query(
    case_uuid: FilterUUIDExplicitEmptyValue,
    case_acls_query,
    message_types_query,
):
    """
    Get thread list from a case view of case filtering With ACL's.

    When case_uuid is a null byte, results will be filtered by case_uuid equals None and NO ACL's.
    """
    if case_uuid == "\0":
        case_check = schema.Thread.case_id.is_(None)
        case_acls_query = True
    else:
        case_check = schema.Case.uuid == case_uuid

    where_clause = sql.and_(case_check, case_acls_query, message_types_query)
    query = thread_list_query.where(where_clause)

    return query.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def get_threads_from_contact_query(
    contact_uuid: FilterUUIDExplicitEmptyValue,
    case_acls_query,
    message_types_query,
):
    """
    Get thread list from a contact view filtering With ACL's on the linked cases of the contact.

    When contact_uuid is a null byte, results will be filtered by contact_uuid equals None but with ACL's.
    """

    if contact_uuid == "\0":
        contact_uuid = None

    where_clause = sql.and_(
        schema.Thread.contact_uuid == contact_uuid,
        sql.or_(schema.Thread.case_id.is_(None), case_acls_query),
        message_types_query,
    )
    query = thread_list_query.where(where_clause)

    return query.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def find_case_query(case_uuid: UUID, user_uuid: UUID, permission: str, db):
    """Return a database query to find and retrieve a single case, with ACLs applied

    :param case_uuid: UUID of the case to retrieve
    :type case_uuid: UUID
    :param user_uuid: UUID of the user retrieving the case
    :type user_uuid: UUID
    :param permission: Permission (read/write/manage/search) the requested
    :type permission: str
    :param db: SQLAlchemy database session to use
    :return: A SQLAlchemy `select` instance, ready for use.
    """

    return unsafe_case_query.where(
        sql.and_(
            schema.Case.uuid == case_uuid,
            case_acl.allowed_cases_subquery(
                db=db, user_uuid=user_uuid, permission=permission
            ),
        )
    )


def get_threads_from_case_and_contact_query(
    contact_uuid: FilterUUIDExplicitEmptyValue,
    case_uuid: FilterUUIDExplicitEmptyValue,
    case_acls_query,
    message_types_query,
):
    """Get the thread list for case and contact being those both a uuid or a null byte.
    No ACL's are applied if the case_uuid is a null byte, all the other scenarios are run with ACL's.
    Given that case_acls_query is a query addition, setting it to True
    will be optimized by sql alchemy and nothing will be shown in the raw query.

    :param contact_uuid: UUID
    :param case_uuid: UUID
    :param case_acls_query: query
    :param message_types_query: query
    :return: query
    """
    if case_uuid == "\0":
        case_check = schema.Thread.case_id.is_(None)
        case_acls_query = True
    else:
        case_check = schema.Case.uuid == case_uuid

    if contact_uuid == "\0":
        contact_uuid_check = schema.Thread.contact_uuid.is_(None)
    else:
        contact_uuid_check = schema.Thread.contact_uuid == contact_uuid

    where_clause = sql.and_(
        case_check,
        contact_uuid_check,
        case_acls_query,
        message_types_query,
    )
    query = thread_list_query.where(where_clause)

    return query.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def get_all_thread_list_query(case_acls_query, message_types_query):
    """Query addition to the generic thread list query to get all queries, ACL's applied,
    with or without a message types.

    :param case_acls_query: query
    :param message_types_query: query
    :return: query
    """
    query = thread_list_query.where(
        sql.and_(
            message_types_query,
            sql.or_(schema.Thread.case_id.is_(None), case_acls_query),
        )
    )

    return query.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def get_thread_list_query(
    user_uuid: UUID,
    case_uuid: FilterUUIDExplicitEmptyValue,
    contact_uuid: FilterUUIDExplicitEmptyValue,
    db: orm.Session,
    message_types: set[str] | None,
    permission: str = "read",
):
    """
    A general function to get a query based on provided parameters and conditions.

    Will return a query to execute in the repository to get a thread list.
    Will run ACL's on all queries below except get_public_threads_query.
    """
    message_types_query = _message_types_query(message_types=message_types)

    case_acls_query = _case_acls_query(
        db=db, user_uuid=user_uuid, permission=permission
    )

    if case_uuid and contact_uuid:
        # This case also handles both being `\x00` -- which explicitly searches
        # for NULL case_uuid and contact_uuid.
        # This is used for the "communication intake".
        return get_threads_from_case_and_contact_query(
            case_uuid=case_uuid,
            contact_uuid=contact_uuid,
            case_acls_query=case_acls_query,
            message_types_query=message_types_query,
        )

    if case_uuid:
        return get_threads_from_case_query(
            case_uuid=case_uuid,
            case_acls_query=case_acls_query,
            message_types_query=message_types_query,
        )

    if contact_uuid:
        return get_threads_from_contact_query(
            contact_uuid=contact_uuid,
            case_acls_query=case_acls_query,
            message_types_query=message_types_query,
        )

    return get_all_thread_list_query(
        case_acls_query=case_acls_query,
        message_types_query=message_types_query,
    )


def get_message_by_uuid(message_uuid: UUID):
    stmt = _message_select_statement().where(
        schema.ThreadMessage.uuid == message_uuid
    )
    return stmt


@functools.cache
def _message_select_statement():
    thread_and_case = sql.outerjoin(
        schema.Thread, schema.Case, schema.Thread.case_id == schema.Case.id
    )

    external_message_join = (
        sql.join(
            schema.ThreadMessage,
            thread_and_case,
            schema.Thread.id == schema.ThreadMessage.thread_id,
        )
        .outerjoin(
            schema.ThreadMessageContactmoment,
            schema.ThreadMessage.thread_message_contact_moment_id
            == schema.ThreadMessageContactmoment.id,
        )
        .outerjoin(
            schema.ThreadMessageNote,
            schema.ThreadMessage.thread_message_note_id
            == schema.ThreadMessageNote.id,
        )
        .outerjoin(
            schema.ThreadMessageExternal,
            schema.ThreadMessage.thread_message_external_id
            == schema.ThreadMessageExternal.id,
        )
    )

    select_stmt = sql.select(
        schema.Thread.uuid.label("thread_uuid"),
        # Messages inherit their "is_notification" from the thread they're in
        schema.Thread.is_notification.label("is_notification"),
        schema.Case.uuid.label("case_uuid"),
        schema.Case.html_email_template.label("case_html_email_template"),
        schema.ThreadMessage.uuid,
        schema.ThreadMessage.type,
        schema.ThreadMessage.message_slug,
        schema.ThreadMessage.message_date,
        schema.ThreadMessage.created_by_uuid,
        schema.ThreadMessage.created_by_displayname,
        schema.ThreadMessage.created,
        schema.ThreadMessage.last_modified,
        schema.ThreadMessage.thread_id,
        schema.ThreadMessageContactmoment.contact_channel.label(
            "contactmoment_channel"
        ),
        schema.ThreadMessageContactmoment.direction.label(
            "contactmoment_direction"
        ),
        schema.ThreadMessageContactmoment.content.label(
            "contactmoment_content"
        ),
        schema.ThreadMessageNote.content.label("note_content"),
        schema.ThreadMessageExternal.content.label("external_message_content"),
        schema.ThreadMessageExternal.subject.label("external_message_subject"),
        schema.ThreadMessageExternal.direction,
        schema.ThreadMessageExternal.participants,
        schema.ThreadMessageExternal.read_employee,
        schema.ThreadMessageExternal.read_pip,
        schema.ThreadMessageExternal.attachment_count,
        schema.ThreadMessageExternal.type.label("external_message_type"),
        schema.ThreadMessageExternal.failure_reason.label(
            "external_message_failure_reason"
        ),
        sql.cast(
            schema.ThreadMessageExternal.source_file_id.isnot(None),
            types.Boolean,
        ).label("is_imported"),
        schema.ThreadMessageContactmoment.recipient_uuid,
        schema.ThreadMessageContactmoment.recipient_displayname,
        attachment_subquery(schema.ThreadMessage.id).label("attachments"),
    ).select_from(external_message_join)

    return select_stmt


def get_last_message_by_thread_uuid(thread_uuid: UUID):
    stmt = (
        _message_select_statement()
        .where(
            schema.ThreadMessage.thread_id
            == sql.select(schema.Thread.id)
            .where(schema.Thread.uuid == thread_uuid)
            .scalar_subquery()
        )
        .order_by(schema.ThreadMessage.created.desc())
    )
    return stmt


def get_message_list(thread_uuid: UUID):
    """Get list of messages from by thread uuid.

    IMPORTANT: No ACL check is performed in this query.

    :param thread_uuid: thread uuid
    :type thread_uuid: UUID
    :return: query
    :rtype: query object
    """
    query = (
        _message_select_statement()
        .where(
            sql.and_(
                schema.Thread.uuid == thread_uuid,
                schema.ThreadMessage.thread_id == schema.Thread.id,
            )
        )
        .order_by(schema.ThreadMessage.created)
    )
    return query


def case_by_thread_uuid(
    db, user_uuid: UUID, permission: str, thread_uuid: UUID
):
    """Case related to a thread query and do ACL checks.

    ACL check performed if case_id is not None.

    :param db: database session
    :type db: DB
    :param user_uuid: user uuid
    :type user_uuid: UUID
    :param permission: permission set user needs
    :type permission: str
    :param thread_uuid: uuid
    :type thread_uuid: UUID
    :return: qry
    :rtype: query object
    """

    case_join = sql.join(
        schema.Case,
        schema.ZaaktypeNode,
        schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
    )

    thread_case = sql.outerjoin(
        schema.Thread, case_join, schema.Thread.case_id == schema.Case.id
    )
    query = sql.select(
        schema.Thread.id,
        schema.Thread.case_id,
        schema.Case.uuid.label("case_uuid"),
        schema.Case.status.label("case_status"),
        schema.Case.onderwerp.label("case_description"),
        schema.Case.onderwerp_extern.label("case_description_public"),
        schema.ZaaktypeNode.titel.label("case_type_name"),
    ).select_from(thread_case)

    qry = query.where(
        sql.and_(
            schema.Thread.uuid == thread_uuid,
            sql.or_(
                schema.Thread.case_id.is_(None),
                case_acl.allowed_cases_subquery(
                    db=db, user_uuid=user_uuid, permission=permission
                ),
            ),
        )
    )
    return qry


def search_cases_query(
    search_term: str, db: orm.Session, user_uuid: UUID, permission: str
):
    """Search Cases by a search term query and do ACL checks.

    :param search_term: The search term to retrieve Cases.
    :type search_term: string
    :param db: database session
    :type db: DB
    :param user_uuid: Currently logged in user making the request.
    :type user_uuid: UUID
    :param permission: The requested permission for a case.
    :type permission: string
    :return: query
    :rtype: query object
    """

    prepared_search_term = prepare_search_term(search_term=search_term)

    status_condition = True

    query = unsafe_case_query.where(
        sql.and_(
            schema.CaseMeta.text_vector.bool_op("@@")(
                sql.func.to_tsquery(prepared_search_term)
            ),
            status_condition,
            case_acl.allowed_cases_subquery(
                db=db, user_uuid=user_uuid, permission=permission
            ),
        )
    ).order_by(
        sql.desc(
            sql.case(  # Tip the scale when the case id matches the search term
                (sql.cast(schema.Case.id, types.Text) == search_term, 100),
                else_=0,
            )
            + sql.func.ts_rank_cd(
                schema.CaseMeta.text_vector,
                sql.func.to_tsquery(prepared_search_term),
            )
        )
    )

    return query


def requestor_uuid_from_case_query(case_uuid: UUID):
    """Query  to get an requestor uuid( persopn/natuurlijk_person/employee table) of a case.

    :param case_uuid: Case uuid
    :type case_uuid: UUID
    :return: query
    :rtype: query Object
    """
    case_join = sql.join(
        schema.ZaakBetrokkenen,
        schema.Case,
        schema.Case.aanvrager == schema.ZaakBetrokkenen.id,
    )

    query = (
        (sql.select(schema.ZaakBetrokkenen.subject_id).select_from(case_join))
        .where(schema.Case.uuid == case_uuid)
        .scalar_subquery()
    )

    return query


def get_case_from_thread_query(thread_uuid: UUID, user_uuid: UUID):
    """Query to get a case id and uuid to create a simple case entity with uuid and id.

    :param thread_uuid: thread uuid for a pip user.
    :type thread_uuid: UUID
    :param user_uuid: User uuid to check for with pip acl's.
    :type user_uuid: UUID
    :return: query
    :rtype: query Object
    """

    case_join = sql.join(
        schema.Case,
        schema.ZaaktypeNode,
        schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
    )

    thread_case = sql.outerjoin(
        schema.Thread, case_join, schema.Thread.case_id == schema.Case.id
    )
    query = (
        sql.select(
            schema.Case.id.label("case_id"),
            schema.Case.uuid.label("case_uuid"),
            schema.Case.status.label("case_status"),
            schema.Case.onderwerp.label("case_description"),
            schema.Case.onderwerp_extern.label("case_description_public"),
            schema.ZaaktypeNode.titel.label("case_type_name"),
        )
        .select_from(thread_case)
        .where(
            sql.and_(
                schema.Thread.uuid == thread_uuid,
                schema.Case.uuid.isnot(None),
                schema.Case.uuid.in_(pip_acl_query(user_uuid)),
            )
        )
    )

    return query


def list_cases_query(
    db, is_pip_user: bool, user_uuid: UUID, contact_uuid: UUID
):
    """Search for all Cases user_uuid has access to, that contact_uuid also has access to.

    :param user_uuid: UUID of the user
    :type user_uuid: UUID
    :param contact_uuid: UUID of the "owner" of the cases
    :type contact_uuid: UUID
    :return: query
    :rtype: query object
    """
    query = unsafe_case_query.where(
        schema.Case.uuid.in_(pip_acl_query(contact_uuid))
    )

    if not is_pip_user:
        query = query.where(
            case_acl.allowed_cases_subquery(
                db=db, user_uuid=user_uuid, permission="read"
            )
        )

    return query


def attachment_subquery(message_id):
    preview = sql.alias(schema.Filestore)
    attachment = (
        sql.join(
            schema.Filestore,
            schema.ThreadMessageAttachment,
            schema.Filestore.id == schema.ThreadMessageAttachment.filestore_id,
        )
        .join(
            schema.ThreadMessageAttachmentDerivative,
            sql.and_(
                schema.ThreadMessageAttachmentDerivative.thread_message_attachment_id
                == schema.ThreadMessageAttachment.id,
                schema.ThreadMessageAttachmentDerivative.type == "pdf",
            ),
            isouter=True,
        )
        .join(
            preview,
            preview.c.id
            == schema.ThreadMessageAttachmentDerivative.filestore_id,
            isouter=True,
        )
    )

    attachment_json = (
        sql.select(
            sql.func.json_build_object(
                "uuid",
                schema.ThreadMessageAttachment.uuid,
                "file_uuid",
                schema.Filestore.uuid,
                "filename",
                schema.ThreadMessageAttachment.filename,
                "size",
                schema.Filestore.size,
                "mimetype",
                schema.Filestore.mimetype,
                "md5",
                schema.Filestore.md5,
                "date_created",
                schema.Filestore.date_created,
                "storage_location",
                schema.Filestore.storage_location,
                "preview_uuid",
                preview.c.uuid,
            )
        )
        .select_from(attachment)
        .where(schema.ThreadMessageAttachment.thread_message_id == message_id)
        .label("attachment_json_subquery")
    )

    json_array = sql.func.array_to_json(sql.func.array(attachment_json))
    return json_array


def get_file_by_attachment(
    db, attachment_uuid: UUID, user_uuid: UUID, is_pip: bool
):
    preview = sql.alias(schema.Filestore)

    file_att = (
        sql.join(
            schema.Filestore,
            schema.ThreadMessageAttachment,
            schema.Filestore.id == schema.ThreadMessageAttachment.filestore_id,
        )
        .join(
            schema.ThreadMessageAttachmentDerivative,
            sql.and_(
                schema.ThreadMessageAttachmentDerivative.thread_message_attachment_id
                == schema.ThreadMessageAttachment.id,
                schema.ThreadMessageAttachmentDerivative.type == "pdf",
                schema.Filestore.mimetype != "application/pdf",
            ),
            isouter=True,
        )
        .join(
            preview,
            preview.c.id
            == schema.ThreadMessageAttachmentDerivative.filestore_id,
            isouter=True,
        )
    )

    file_att_message_thread_case = (
        file_att.join(
            schema.ThreadMessage,
            schema.ThreadMessage.id
            == schema.ThreadMessageAttachment.thread_message_id,
            isouter=False,
        )
        .join(
            schema.Thread,
            schema.ThreadMessage.thread_id == schema.Thread.id,
            isouter=False,
        )
        .join(
            schema.Case, schema.Case.id == schema.Thread.case_id, isouter=True
        )
    )
    if is_pip:
        acl_clause = sql.and_(
            schema.Thread.thread_type == "external",
            schema.Case.uuid.in_(pip_acl_query(user_uuid)),
        )
    else:
        # ACL check when a case is not None
        acl_clause = sql.or_(
            schema.Thread.case_id.is_(None),
            case_acl.allowed_cases_subquery(
                db=db, user_uuid=user_uuid, permission="read"
            ),
        )

    file_select = (
        sql.select(
            schema.Filestore.uuid,
            schema.ThreadMessageAttachment.filename.label("original_name"),
            schema.Filestore.size,
            schema.Filestore.mimetype,
            schema.Filestore.md5,
            schema.Filestore.date_created,
            schema.Filestore.storage_location,
            schema.ThreadMessageAttachment.uuid.label("attachment_uuid"),
            preview.c.uuid.label("preview_uuid"),
            preview.c.mimetype.label("preview_mimetype"),
            preview.c.storage_location.label("preview_storage_location"),
            preview.c.original_name.label("preview_filename"),
        )
        .select_from(file_att_message_thread_case)
        .where(
            sql.and_(
                schema.ThreadMessageAttachment.uuid == attachment_uuid,
                acl_clause,
            )
        )
    )
    return file_select


def delete_message(session: orm.Session, uuid: str, entity_type: str):
    delete_msg_stmt = (
        sql.delete(schema.ThreadMessage)
        .where(schema.ThreadMessage.uuid == uuid)
        .execution_options(synchronize_session=False)
    )
    if entity_type == "Note":
        msg_id = sql.select(schema.ThreadMessage.thread_message_note_id).where(
            schema.ThreadMessage.uuid == uuid
        )
        detail_id = session.execute(msg_id).fetchone().thread_message_note_id
        msg_detail = (
            sql.delete(schema.ThreadMessageNote)
            .where(schema.ThreadMessageNote.id == detail_id)
            .execution_options(synchronize_session=False)
        )
    elif entity_type == "ContactMoment":
        msg_id = sql.select(
            schema.ThreadMessage.thread_message_contact_moment_id
        ).where(schema.ThreadMessage.uuid == uuid)
        detail_id = (
            session.execute(msg_id).fetchone().thread_message_contact_moment_id
        )
        msg_detail = (
            sql.delete(schema.ThreadMessageContactmoment)
            .where(schema.ThreadMessageContactmoment.id == detail_id)
            .execution_options(synchronize_session=False)
        )
    elif entity_type == "ExternalMessage":
        msg_id = sql.select(
            schema.ThreadMessage.thread_message_external_id
        ).where(schema.ThreadMessage.uuid == uuid)
        detail_id = (
            session.execute(msg_id).fetchone().thread_message_external_id
        )
        msg_detail = (
            sql.delete(schema.ThreadMessageExternal)
            .where(
                schema.ThreadMessage.thread_message_external_id == detail_id
            )
            .execution_options(synchronize_session=False)
        )
    session.execute(delete_attachments(message_uuid=uuid))
    session.execute(delete_msg_stmt)
    session.execute(msg_detail)


def delete_attachments(message_uuid: UUID):
    delete_attachments_stmt = (
        sql.delete(schema.ThreadMessageAttachment)
        .where(
            schema.ThreadMessageAttachment.thread_message_id
            == sql.select(schema.ThreadMessage.id)
            .where(schema.ThreadMessage.uuid == message_uuid)
            .scalar_subquery()
        )
        .execution_options(synchronize_session=False)
    )
    return delete_attachments_stmt


def delete_thread(uuid: UUID):
    delete_thread_stmt = (
        sql.delete(schema.Thread)
        .where(schema.Thread.uuid == uuid)
        .execution_options(synchronize_session=False)
    )
    return delete_thread_stmt


def get_thread_pip_query(user_uuid: UUID):
    """Main query to get the external thread messages in a thread for PIP only.

    :param user_uuid: Logged in user uuid
    :type user_uuid: UUID
    :return: query
    """

    case_join = sql.join(
        schema.Case,
        schema.ZaaktypeNode,
        schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
    )

    thread_join = sql.join(
        schema.Thread,
        case_join,
        schema.Thread.case_id == schema.Case.id,
        isouter=False,
    )

    where_clause = sql.and_(
        sql.cast(schema.Thread.last_message_cache, JSON)["message_type"].astext
        == "pip_message",
        schema.Case.uuid.in_(pip_acl_query(user_uuid)),
        schema.Thread.is_notification.is_(False),
    )

    query = (
        sql.select(
            schema.Thread.id,
            schema.Thread.uuid,
            schema.Thread.contact_uuid,
            schema.Thread.contact_displayname,
            schema.Thread.thread_type,
            schema.Thread.created,
            schema.Thread.last_modified,
            schema.Thread.last_message_cache,
            schema.Thread.message_count,
            schema.Thread.case_id.label("case_id"),
            schema.Thread.unread_pip_count,
            schema.Thread.unread_employee_count,
            schema.Case.uuid.label("case_uuid"),
            schema.Case.status.label("case_status"),
            schema.Case.onderwerp.label("case_description"),
            schema.Case.onderwerp_extern.label("case_description_public"),
            schema.ZaaktypeNode.titel.label("case_type_name"),
            schema.Thread.attachment_count,
            schema.Thread.is_notification,
        )
        .select_from(thread_join)
        .where(where_clause)
    )

    return query


def get_all_threads_for_logged_in_pip_user(user_uuid: UUID):
    thread_pip_query = get_thread_pip_query(user_uuid=user_uuid)

    qry = thread_pip_query
    return qry.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def get_pip_thread_from_case_query(
    user_uuid: UUID, case_uuid: FilterUUIDExplicitEmptyValue
):
    """Query to get thread list for a case for PIP with PIP ACL's.

    :param user_uuid: Logged in user uuid
    :type user_uuid: UUID
    :param case_uuid: case_uuid to fetch the thread list for, with PIP ACL's.
    :type case_uuid: UUID
    :return: query
    """
    thread_pip_query = get_thread_pip_query(user_uuid=user_uuid)
    query = thread_pip_query.where(schema.Case.uuid == case_uuid)

    return query.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def get_pip_thread_from_contact_query(user_uuid: UUID, contact_uuid):
    """Query to get thread list for a contact with all the related cases for a contact/pip user with PIP ACL's.

    :param user_uuid: Logged in user uuid
    :type user_uuid: UUID
    :param contact_uuid: contact_uuid to fetch the thread list for, with PIP ACL's.
    :type contact_uuid: UUID
    :return: query
    """
    thread_pip_query = get_thread_pip_query(user_uuid=user_uuid)
    query = thread_pip_query.where(schema.Thread.contact_uuid == contact_uuid)

    return query.order_by(
        schema.Thread.last_message_cache.cast(JSON)["created"]
        .astext.cast(types.DateTime(timezone=True))
        .desc()
    )


def get_thread_list_pip_query(
    user_uuid: UUID,
    case_uuid: FilterUUIDExplicitEmptyValue,
    contact_uuid: FilterUUIDExplicitEmptyValue,
):
    """Main query to get the thread list for pip by case or contact.

    :param user_uuid: Logged in user uuid
    :type user_uuid: UUID
    :param case_uuid: case_uuid to fetch the thread list for, with PIP ACL's.
    :type case_uuid: UUID
    :param contact_uuid: contact_uuid to fetch the thread list for, with PIP ACL's.
    :type contact_uuid: UUID
    :return: query
    """
    if case_uuid is None and contact_uuid is None:
        return get_all_threads_for_logged_in_pip_user(user_uuid=user_uuid)

    if contact_uuid:
        return get_pip_thread_from_contact_query(
            user_uuid=user_uuid, contact_uuid=contact_uuid
        )

    if case_uuid:
        return get_pip_thread_from_case_query(
            user_uuid=user_uuid, case_uuid=case_uuid
        )


def get_pip_message_list_query(thread_uuid: UUID, case_id):
    """Get list of messages from by thread uuid.

    IMPORTANT: No ACL check is performed in this query.
    Only check if the current pip user actually owns the thread
    and the case of the thread is the one from the pip acl checked.

    :param thread_uuid: thread uuid
    :type thread_uuid: UUID
    :return: query
    :rtype: query object
    """
    thread = sql.join(
        schema.ThreadMessage,
        sql.join(
            schema.Thread, schema.Case, schema.Case.id == schema.Thread.case_id
        ),
        schema.Thread.id == schema.ThreadMessage.thread_id,
    )
    external_message_join = thread.join(
        schema.ThreadMessageExternal,
        schema.ThreadMessage.thread_message_external_id
        == schema.ThreadMessageExternal.id,
        isouter=False,
    )

    query = (
        sql.select(
            schema.Thread.uuid.label("thread_uuid"),
            schema.Case.uuid.label("case_uuid"),
            schema.ThreadMessage.uuid,
            schema.ThreadMessage.type,
            schema.ThreadMessage.message_slug,
            schema.ThreadMessage.created_by_uuid,
            schema.ThreadMessage.created_by_displayname,
            schema.ThreadMessage.created,
            schema.ThreadMessage.last_modified,
            schema.ThreadMessage.message_date,
            schema.ThreadMessageExternal.content.label(
                "external_message_content"
            ),
            schema.ThreadMessageExternal.subject.label(
                "external_message_subject"
            ),
            schema.ThreadMessageExternal.type.label("external_message_type"),
            schema.ThreadMessageExternal.failure_reason.label(
                "external_message_failure_reason"
            ),
            schema.ThreadMessageExternal.participants,
            attachment_subquery(schema.ThreadMessage.id).label("attachments"),
            schema.ThreadMessageExternal.direction,
            sql.cast(
                schema.ThreadMessageExternal.source_file_id.isnot(None),
                types.Boolean,
            ).label("is_imported"),
            schema.ThreadMessageExternal.read_employee,
            schema.ThreadMessageExternal.read_pip,
            schema.ThreadMessageExternal.attachment_count,
        )
        .select_from(external_message_join)
        .where(
            sql.and_(
                schema.Thread.uuid == thread_uuid,
                schema.Thread.case_id == case_id,
                schema.ThreadMessageExternal.type == "pip",
            )
        )
    ).order_by(schema.ThreadMessage.created)
    return query


def get_case_for_pip_query(case_uuid, user_uuid):
    """Query to get a case id and uuid to create a simple case entity with uuid and id, ACL-ed.

    :param user_uuid: User uuid to check for with pip acl's.
    :type user_uuid: UUID
    :return: query
    :rtype: query Object
    """

    query = unsafe_case_query.where(
        sql.and_(
            schema.Case.uuid == case_uuid,
            schema.Case.uuid.in_(pip_acl_query(user_uuid)),
        )
    )

    return query


def get_message_query(db, user_uuid, is_pip):
    """Get list of messages the user has access to.

    :param user_uuid: user uuid uuid
    :type user_uuid: UUID
    :param is_pip: Indication if the user is type pip or not.
    :type is_pip: bool
    :return: query
    :rtype: query object
    """
    query = _message_select_statement()

    if is_pip:
        return query.where(schema.Case.uuid.in_(pip_acl_query(user_uuid)))
    else:
        return query.where(
            sql.or_(
                schema.Thread.case_id.is_(None),
                case_acl.allowed_cases_subquery(
                    db=db, user_uuid=user_uuid, permission="read"
                ),
            )
        )


def get_file_by_attachment_without_acl_check(attachment_uuid: UUID):
    """Query to get a file by attachment, without acl check.

    :param attachment_uuid: UUID of the attachment.
    :type attachment_uuid: UUID
    :return: query
    :rtype: query
    """
    file_attachment_join = (
        sql.join(
            schema.Filestore,
            schema.ThreadMessageAttachment,
            schema.Filestore.id == schema.ThreadMessageAttachment.filestore_id,
        )
        .join(
            schema.ThreadMessage,
            schema.ThreadMessage.id
            == schema.ThreadMessageAttachment.thread_message_id,
            isouter=False,
        )
        .join(
            schema.Thread,
            schema.ThreadMessage.thread_id == schema.Thread.id,
            isouter=False,
        )
    )

    file_select = (
        sql.select(
            schema.Filestore.uuid,
            schema.ThreadMessageAttachment.filename.label("original_name"),
            schema.Filestore.size,
            schema.Filestore.mimetype,
            schema.Filestore.md5,
            schema.Filestore.date_created,
            schema.Filestore.storage_location,
            schema.ThreadMessageAttachment.uuid.label("attachment_uuid"),
        )
        .select_from(file_attachment_join)
        .where(
            sql.and_(schema.ThreadMessageAttachment.uuid == attachment_uuid)
        )
    )
    return file_select


def subject_relation_uuids_from_case_query(case_uuid: UUID):
    """Query to get the uuids of employees, naturrlijk_persons, and organizations related with case(Aanvarger, Coordinator, Advocaat etc).

    :param case_uuid: Case uuid
    :type case_uuid: UUID
    :return: query
    :rtype: query Object
    """
    case_join = sql.join(
        schema.ZaakBetrokkenen,
        schema.Case,
        sql.and_(
            schema.ZaakBetrokkenen.zaak_id == schema.Case.id,
            schema.ZaakBetrokkenen.deleted.is_(None),
        ),
    )

    query = (
        sql.select(schema.ZaakBetrokkenen.subject_id).select_from(case_join)
    ).where(schema.Case.uuid == case_uuid)

    return query


def contacts_query(contact_uuids):
    """Search from 3 tables(Bedrijf, NatuurlijkPersoon, Subject)
     and combine them with a "UNION ALL" where it returns contacts with given contact_uuids .

    :param contact_uuids: The list of uuids keyword to retrieve a contact.
    :type contact_uuids: List
    :return: SQLAlchemy query for Contact rows
    """

    natural_person = natural_person_simple_query.where(
        schema.NatuurlijkPersoon.uuid.in_(contact_uuids)
    )

    organization = organization_simple_query.where(
        schema.Bedrijf.uuid.in_(contact_uuids)
    )

    employee = employee_simple_query.where(
        sql.and_(
            schema.Subject.uuid.in_(contact_uuids),
            schema.Subject.subject_type == "employee",
        )
    )
    return sql.union_all(natural_person, organization, employee)


def contact_moment_list_query() -> sql.selectable.Select:
    """Get list of all contact moments in the system."""
    thread_join = sql.join(
        schema.Thread,
        schema.ThreadMessage,
        sql.and_(schema.Thread.id == schema.ThreadMessage.thread_id),
    )
    query = (
        sql.select(
            schema.Thread.contact_uuid,
            schema.Thread.contact_displayname.label("contact"),
            schema.Thread.case_id,
            schema.ThreadMessageContactmoment.direction,
            schema.ThreadMessage.uuid,
            schema.Thread.created,
            schema.ThreadMessageContactmoment.content.label("summary"),
            schema.ThreadMessageContactmoment.contact_channel.label("channel"),
            schema.Thread.uuid.label("thread_uuid"),
        )
        .select_from(
            sql.join(
                schema.ThreadMessageContactmoment,
                thread_join,
                schema.ThreadMessage.thread_message_contact_moment_id
                == schema.ThreadMessageContactmoment.id,
                isouter=False,
            ),
        )
        .order_by(schema.Thread.created.desc())
        .limit(5000)
    )
    return query
