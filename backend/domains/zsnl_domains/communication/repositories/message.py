# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import dkim
import email_validator
import html
import logging
import os
import requests
import time
from ... import ZaaksysteemRepositoryBase
from ..entities import (
    AttachedFile,
    Contact,
    ContactMoment,
    ExternalMessage,
    Message,
    Note,
)
from . import util
from .database_queries import (
    contact_type_query,
    delete_message,
    get_last_message_by_thread_uuid,
    get_message_by_uuid,
    get_message_list,
    get_message_query,
    get_pip_message_list_query,
)
from datetime import datetime, timezone
from email import headerregistry
from email import message as email_message
from email import utils as email_utils
from io import SEEK_SET, BytesIO
from minty.cqrs import Event
from minty.exceptions import (
    ConfigurationConflict,
    Conflict,
    CQRSException,
    NotFound,
)
from minty_infra_email.infrastructure import (
    EmailConfiguration,
    EmailInfrastructure,
)
from minty_infra_storage import S3Infrastructure
from sqlalchemy import sql
from sqlalchemy.exc import IntegrityError
from typing import Any
from uuid import UUID, uuid4
from zsnl_domains.database import schema

logger = logging.getLogger(__name__)

MAX_MESSAGE_SIZE_BYTES = int(
    os.environ.get("MAX_MESSAGE_SIZE_BYTES", 20 * 1024 * 1024)
)


def _strip_newlines(val):
    return " ".join(val.splitlines())


class MessageRepository(ZaaksysteemRepositoryBase):
    REQUIRED_INFRASTRUCTURE = {
        **ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE,
        "s3": S3Infrastructure(),
        "email": EmailInfrastructure,
    }

    message_mapping = {
        "message_slug": "message_slug",
        "message_type": "type",
        "created_date": "created",
        "last_modified": "last_modified",
        "created_by_displayname": "created_by_displayname",
        "message_date": "message_date",
    }

    note_mapping = {"content": "content"}

    contact_moment_mapping = {
        "channel": "contact_channel",
        "direction": "direction",
        "content": "content",
        "recipient_displayname": "recipient_displayname",
    }

    external_message_mapping = {
        "subject": "subject",
        "external_message_type": "type",
        "content": "content",
        "participants": "participants",
        "direction": "direction",
        "read_employee": "read_employee",
        "read_pip": "read_pip",
        "attachment_count": "attachment_count",
    }

    def get_messages_by_thread_uuid(
        self, uuid: UUID, user_info, case_id, page, page_size
    ) -> list[Message]:
        """get message for given thread uuid.

        IMPORTANT: no ACL checks are performed in this function (for performance
        reasons), ACL checks are done when retrieving the case from case_repository
        and this function should be called before this function. (in the Query)

        :param uuid: uuid
        :type uuid: UUID
        :param user_info: user info from the request
        :type user_info: object
        :param case_id: Case id, if pip user passes acl's, in use only if there is a pip user requesting a message list.
        :type case_id: int or None
        :return: list of messages
        :rtype: List[Message]
        """
        if user_info.permissions.get("pip_user", False) and case_id:
            get_message_list_query = get_pip_message_list_query(
                thread_uuid=uuid, case_id=case_id
            )

        else:
            get_message_list_query = get_message_list(thread_uuid=uuid)

        if page and page_size:
            offset = self._calculate_offset(page, page_size)
            get_message_list_query = get_message_list_query.limit(
                page_size
            ).offset(offset)

        message_rows = self.session.execute(get_message_list_query).fetchall()
        return [
            self._transform_to_entity(message_row=row) for row in message_rows
        ]

    def get_message_by_uuid(self, message_uuid):
        """Get message entity for

        No ACL checks done in this function.

        :param message_uuid: message_uuid
        :type message_uuid: UUID
        :return: message entity
        :rtype: [type]
        """
        result = self.session.execute(
            get_message_by_uuid(message_uuid=message_uuid)
        ).fetchone()
        if not result:
            raise NotFound(
                f"Message with uuid '{message_uuid}' not found.",
                "communication/message/not_found",
            )

        return self._transform_to_entity(message_row=result)

    def _get_contact(self, contact_uuid: UUID, contact_name: str):
        """Get and return a Contact entity for use in Message entity transformation.

        :param contact_uuid: The contact uuid stored in a message row
        :type contact_uuid: UUID
        :param contact_name: Contact display name store in a message row
        :type contact_name: str
        :return:
        """

        if contact_uuid is None:
            return None

        contact_row = self.session.execute(
            contact_type_query(contact_uuid)
        ).fetchone()

        if not contact_row:
            raise NotFound(
                f"Contact with uuid '{contact_uuid}' not found.",
                "communication/contact/not_found",
            )

        return Contact(
            uuid=contact_uuid, type=contact_row.type, name=contact_name
        )

    def _transform_to_entity(self, message_row):
        created_by = self._get_contact(
            message_row.created_by_uuid, message_row.created_by_displayname
        )

        if message_row.type == "note":
            msg = Note(
                uuid=message_row.uuid,
                thread_uuid=message_row.thread_uuid,
                case_uuid=message_row.case_uuid,
                created_by=created_by,
                message_slug=message_row.message_slug,
                last_modified=message_row.last_modified,
                created_date=message_row.created,
                message_date=message_row.message_date
                or message_row.created,  # Messages before MINTY-3278 have no message_date set
                content=message_row.note_content,
                created_by_displayname=created_by.name,
            )
        elif message_row.type == "contact_moment":
            recipient = self._get_contact(
                message_row.recipient_uuid, message_row.recipient_displayname
            )
            msg = ContactMoment(
                uuid=message_row.uuid,
                thread_uuid=message_row.thread_uuid,
                case_uuid=message_row.case_uuid,
                created_by=created_by,
                message_slug=message_row.message_slug,
                last_modified=message_row.last_modified,
                created_date=message_row.created,
                message_date=message_row.message_date
                or message_row.created,  # Messages before MINTY-3278 have no message_date set
                content=message_row.contactmoment_content,
                channel=message_row.contactmoment_channel,
                direction=message_row.contactmoment_direction,
                recipient=recipient,
                created_by_displayname=created_by.name,
                recipient_displayname=recipient.name,
            )
        elif message_row.type == "external":
            msg = ExternalMessage(
                uuid=message_row.uuid,
                thread_uuid=message_row.thread_uuid,
                case_uuid=message_row.case_uuid,
                created_by=created_by,
                created_by_displayname=created_by.name if created_by else "",
                message_slug=message_row.message_slug,
                last_modified=message_row.last_modified,
                created_date=message_row.created,
                message_date=message_row.message_date
                or message_row.created,  # Messages from previous iteration don't always have a message_date set.
                content=message_row.external_message_content,
                subject=message_row.external_message_subject,
                external_message_type=message_row.external_message_type,
                attachments=self._transform_attachments(
                    message_row.attachments
                ),
                participants=message_row.participants,
                direction=message_row.direction,
                original_message_file=None,
                is_imported=message_row.is_imported,
                read_employee=message_row.read_employee,
                read_pip=message_row.read_pip,
                attachment_count=message_row.attachment_count,
                failure_reason=message_row.external_message_failure_reason,
                case_html_email_template=getattr(
                    message_row, "case_html_email_template", None
                ),
                is_notification=getattr(message_row, "is_notification", False),
            )
        else:
            raise Conflict(f"Unknown message row type: {message_row.type}")

        msg.event_service = self.event_service
        return msg

    def _transform_attachments(self, attachments):
        transformed_attachments = []
        for att in attachments:
            file = AttachedFile(
                uuid=att["uuid"],
                file_uuid=att["file_uuid"],
                filename=att["filename"],
                size=att["size"],
                mimetype=att["mimetype"],
                md5=att["md5"],
                date_created=att["date_created"],
                storage_location=att["storage_location"],
                preview_uuid=att["preview_uuid"],
            )
            transformed_attachments.append(file)
        return transformed_attachments

    def create_contact_moment(
        self,
        thread_uuid,
        created_by: Contact,
        content: str,
        channel: str,
        direction: str,
        contact: Contact,
        contact_moment_uuid: UUID,
    ):
        contact_moment = ContactMoment(
            uuid=contact_moment_uuid,
            thread_uuid=None,
            case_uuid=None,
            created_by=None,
            created_by_displayname=None,
            recipient=None,
            recipient_displayname=None,
            message_slug=None,
            last_modified=None,
            created_date=None,
            message_date=None,
            content=None,
            channel=None,
            direction=None,
        )
        contact_moment.event_service = self.event_service
        contact_moment.create(
            thread_uuid=thread_uuid,
            created_by=created_by,
            content=content,
            channel=channel,
            direction=direction,
            recipient=contact,
        )
        return contact_moment

    def create_note(
        self, uuid, thread_uuid, created_by: Contact, content: str
    ):
        note = Note(
            uuid=uuid,
            thread_uuid=None,
            case_uuid=None,
            created_by=None,
            message_slug=None,
            last_modified=None,
            created_date=None,
            message_date=None,
            content=None,
            created_by_displayname=None,
        )
        note.event_service = self.event_service
        note.create(
            thread_uuid=thread_uuid, created_by=created_by, content=content
        )
        return note

    def create_external_message(
        self,
        thread_uuid,
        case_id: int | None,
        created_by: Contact | None,
        content: str,
        subject: str,
        external_message_type: str,
        external_message_uuid: UUID,
        participants: list[dict[str, str]],
        direction: str,
        original_message_file: UUID | None,
        message_date: datetime | None = None,
        is_imported=False,
        creator_type="employee",
        is_notification=False,
    ):
        external_message = ExternalMessage(
            uuid=external_message_uuid,
            thread_uuid=None,
            case_uuid=None,
            created_by=None,
            created_by_displayname=None,
            message_slug=None,
            last_modified=None,
            created_date=None,
            content=None,
            subject=None,
            external_message_type=None,
            attachments=None,
            participants=[],
            direction=None,
            original_message_file=None,
            message_date=message_date,
            is_imported=None,
            creator_type=None,
        )
        external_message.event_service = self.event_service

        # Subject is prefixed with the configured prefix, followed by the first
        # few digits of the thread uuid
        if (
            direction == "outgoing"
            and external_message_type == "email"
            and not is_notification
        ):
            email_config = self.get_email_configuration()
            message_subject = "[{} {} {}] {}".format(
                email_config["subject_prefix"],
                case_id,
                # Takes the last 48 bits of the thread UUID
                _encode_thread_uuid(UUID(str(thread_uuid))),
                subject,
            )
        else:
            message_subject = subject

        if (
            created_by
            and external_message_type == "pip"
            and created_by.type == "employee"
        ):
            created_by.name = self.get_customer_name()

        external_message.create(
            thread_uuid=thread_uuid,
            created_by=created_by,
            content=content,
            subject=message_subject,
            external_message_type=external_message_type,
            attachments=None,
            participants=participants,
            direction=direction,
            original_message_file=original_message_file,
            message_date=message_date,
            is_imported=is_imported,
            creator_type=creator_type,
        )

        return external_message

    def save(self):
        """Save changes on Message entity back to database."""
        for event in self.event_service.event_list:
            if event.event_name == "NoteCreated":
                self.insert_note(event)
            if event.event_name == "ContactMomentCreated":
                self.insert_contact_moment(event)
            if event.event_name == "ExternalMessageCreated":
                self.insert_external_message(event)
            if event.event_name == "MessageDeleted":
                self._delete_message(event)
            if event.entity_type == "ExternalMessage":
                self._save_external_message_entity_type_handler()

    def _save_external_message_entity_type_handler(self):
        """
        Handle events of type External Message for self.save()
        Handling of too many if branches causes flake to error
        on complexity.
        """
        for event in self.event_service.event_list:
            if event.event_name == "ExternalMessageSent":
                self._send_email(event)
            if event.event_name == "ExternalMessageRead":
                self._mark_message_read(event)
            if event.event_name == "ExternalMessageMarkedUnread":
                self._mark_message_unread(event)
            if event.event_name == "ExternalMessageAttachmentCountIncremented":
                self._update_message_attachment_count(event)

    def _send_email(self, event: Event):
        """Create an `email_message.EmailMessage` and send it using the configured email server"""

        email_config = self.get_email_configuration()
        external_message = self.get_message_by_uuid(event.entity_id)

        # This can only be called on ExternalMessage, but just to be sure:
        assert isinstance(external_message, ExternalMessage)

        storage_infra = self._get_infrastructure(name="s3")
        message = _build_message(
            external_message=external_message,
            context=self.context,
            email_config=email_config,
            storage_infra=storage_infra,
        )
        total_attachment_size = 0

        for attachment in external_message.attachments:
            total_attachment_size += attachment.size

            if total_attachment_size > MAX_MESSAGE_SIZE_BYTES:
                error_message = (
                    f"Error sending email message: too large "
                    f"({total_attachment_size} > max={MAX_MESSAGE_SIZE_BYTES})"
                )
                self.logger.error(error_message)
                self._update_thread_message_external_failure_reason(
                    external_message_uuid=external_message.uuid,
                    failure_reason=error_message,
                )
                return

            self._add_attachment(message, attachment)

        if email_config["dkim"]["use_dkim"]:
            try:
                _dkim_sign_message(
                    message=message, dkim_config=email_config["dkim"]
                )
            except dkim.KeyFormatError as e:
                error_message = (
                    "Error generating DKIM signature: Key format error"
                )
                self.logger.error(
                    error_message,
                    exc_info=e,
                )
                self._update_thread_message_external_failure_reason(
                    external_message_uuid=external_message.uuid,
                    failure_reason=error_message,
                )

        source_file_id = self._save_message_source(
            message_uuid=external_message.uuid,
            message_source=message.as_bytes(),
        )

        self.session.execute(
            sql.update(schema.ThreadMessageExternal)
            .values(source_file_id=source_file_id)
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == external_message.uuid,
                )
            )
            .execution_options(synchronize_session=False)
        )

        email_infra = self._get_infrastructure(name="email")

        try:
            email_infra.send(message, email_config["infrastructure_config"])
        except Exception as exc:
            error_message = f"Error sending email message: {exc}"
            self.logger.error(error_message, exc_info=True)
            self._update_thread_message_external_failure_reason(
                external_message_uuid=external_message.uuid,
                failure_reason=error_message,
            )

    def _update_thread_message_external_failure_reason(
        self, external_message_uuid: UUID, failure_reason: str
    ) -> None:
        """
        When a External Message fails to send, update the
        failure_reason in the message instance. After that
        also update the last_message_cache. Since the mail
        action of sending mail is initiated from the consumer,
        and the existing last_message_cache will become
        outdated.

        :param external_message_uuid: UUID: The uuid of the external message
        :param failure_reason: str: The failure reason
        :return: None
        """

        self.session.execute(
            sql.update(schema.ThreadMessageExternal)
            .values(failure_reason=failure_reason)
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == external_message_uuid,
                )
            )
            .execution_options(synchronize_session=False)
        )
        thread_message_external = self.get_message_by_uuid(
            external_message_uuid
        )

        # Here we assume that the last_message is the one that failed
        # to send. Which _could_ become an issue on heavy use of sending
        # messages in a thread.
        last_message_row = self.session.execute(
            get_last_message_by_thread_uuid(
                thread_uuid=thread_message_external.thread_uuid
            )
        ).fetchone()

        if not last_message_row:
            return

        values = self._create_cached_values(last_message_row)

        self._update_thread_attributes(
            last_message_cache=values,
            thread_id=last_message_row.thread_id,
        )

    def _save_message_source(
        self,
        message_uuid: UUID,
        message_source: bytes,
        message_file_uuid: UUID = None,
        pip_message: bool = False,
        pip_message_filename=None,
    ):
        if message_file_uuid is None:
            message_file_uuid = uuid4()

        storage_infra = self._get_infrastructure(name="s3")

        message_fh = BytesIO(initial_bytes=message_source)
        upload_result = storage_infra.upload(message_fh, message_file_uuid)

        if pip_message:
            if pip_message_filename is None:
                original_filename = f"pip-message-{message_file_uuid}.txt"
            else:
                original_filename = pip_message_filename
        else:
            original_filename = f"message-{message_uuid}.eml"

        filestore_id = self.session.execute(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": message_file_uuid,
                    "original_name": original_filename,
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        ).inserted_primary_key[0]

        return filestore_id

    def _add_attachment(
        self,
        message: email_message.EmailMessage,
        attachment: AttachedFile,
    ):
        (maintype, subtype) = attachment.mimetype.split("/", 2)

        storage_infra = self._get_infrastructure(name="s3")

        file_fh = BytesIO(initial_bytes=b"")
        storage_infra.download_file(
            destination=file_fh,
            file_uuid=attachment.file_uuid,
            storage_location=attachment.storage_location[0],
        )
        file_fh.seek(0, SEEK_SET)

        message.add_attachment(
            file_fh.read(),
            maintype=maintype,
            subtype=subtype,
            filename=attachment.filename,
        )

    def _ensure_fresh_access_token(
        self,
        integration_uuid: UUID,
        integration_config: dict[str, Any],
        internal_config: dict[str, Any],
    ) -> str:
        max_expiration = time.time() + 60
        access_token_expiration = internal_config.get(
            "access_token_expiration", 0
        )
        access_token = internal_config.get("access_token")
        refresh_token = internal_config["refresh_token"]

        if (
            access_token
            and access_token_expiration
            and access_token_expiration > max_expiration
        ):
            self.logger.debug("Access token still valid; reusing")
            return access_token

        tenant_id = integration_config["ms_tenant_id"]
        client_id = integration_config["ms_client_id"]
        client_secret = integration_config["ms_client_secret"]

        token_url = (
            f"https://login.microsoft.com/{tenant_id}/oauth2/v2.0/token"
        )

        self.logger.debug(f"Requesting new access token from '{token_url}'")

        res = requests.post(
            token_url,
            {
                "client_id": client_id,
                "client_secret": client_secret,
                "grant_type": "refresh_token",
                "refresh_token": refresh_token,
            },
        )
        res.raise_for_status()

        response_data = res.json()

        if "error" in response_data or "access_token" not in response_data:
            self.session.execute(
                sql.update(schema.Interface)
                .where(schema.Interface.uuid == integration_uuid)
                .values(
                    internal_config=schema.Interface.internal_config
                    + {"access_token_failed": 1},
                )
            )
            self.logger.info(f"Error response: {response_data})")
            raise CQRSException("Error during token refresh.")

        self.logger.debug("Received new access/refresh tokens")

        new_access_token_expiration = time.time() + response_data["expires_in"]
        new_access_token = response_data["access_token"]
        new_refresh_token = response_data["refresh_token"]

        self.session.execute(
            sql.update(schema.Interface)
            .where(schema.Interface.uuid == integration_uuid)
            .values(
                internal_config=schema.Interface.internal_config
                + {
                    "access_token": new_access_token,
                    "access_token_expiration": new_access_token_expiration,
                    "access_token_failed": 0,
                    "refresh_token": new_refresh_token,
                },
            )
        )

        return new_access_token

    def _get_smarthost_config(
        self,
        integration_uuid: UUID,
        integration_config: dict[str, Any],
        internal_config: dict[str, Any],
    ) -> EmailConfiguration:
        kind = integration_config.get("kind", "custom")

        if kind == "custom":
            return EmailConfiguration(
                smarthost_mode="basic",
                smarthost_hostname=integration_config.get(
                    "smarthost_hostname", None
                ),
                smarthost_port=integration_config.get("smarthost_port", 587),
                smarthost_security="starttls",
                smarthost_username=integration_config.get(
                    "smarthost_username"
                ),
                smarthost_password=integration_config.get(
                    "smarthost_password"
                ),
            )

        # Preconfigured: Microsoft.
        access_token = self._ensure_fresh_access_token(
            integration_uuid=integration_uuid,
            integration_config=integration_config,
            internal_config=internal_config,
        )

        return EmailConfiguration(
            smarthost_mode="oauth2",
            smarthost_username=integration_config["ms_username"],
            smarthost_token=access_token,
            smarthost_hostname="smtp.office365.com",
            smarthost_port=587,
            smarthost_security="starttls",
        )

    def get_customer_name(self) -> str | None:
        return self.session.execute(
            sql.select(schema.Config.value).where(
                schema.Config.parameter == "customer_info_naam"
            )
        ).scalar_one()

    def get_email_configuration(self):
        """Retrieve email configuration as a dictionary

        :raises ConfigurationConflict: Email configuration is invalid or missing
        :return: A dictionary containing the email configuration
        :rtype: dict
        """
        integration_config = self.session.execute(
            sql.select(
                schema.Interface.uuid,
                schema.Interface.interface_config,
                schema.Interface.internal_config,
            )
            .where(
                sql.and_(
                    schema.Interface.module == "emailconfiguration",
                    schema.Interface.active.is_(True),
                    schema.Interface.date_deleted.is_(None),
                )
            )
            .with_for_update(key_share=True)
        ).fetchone()
        if integration_config is None:
            raise ConfigurationConflict(
                "No outgoing email configuration found",
                "communication/email/no_configuration",
            )

        config = integration_config.interface_config
        use_smarthost = bool(config.get("use_smarthost", False))

        if use_smarthost:
            email_config = self._get_smarthost_config(
                integration_config.uuid,
                integration_config.interface_config,
                integration_config.internal_config,
            )
        else:
            # Email infrastructure takes care of the defaults for us
            email_config = EmailConfiguration(
                smarthost_mode="none", smarthost_security="none"
            )

        return {
            "dkim": self._get_dkim_settings(config),
            "subject_prefix": config.get("subject", "ZS"),
            "sender_name": config.get("sender_name", ""),
            "sender_address": config["api_user"],
            "infrastructure_config": email_config,
            "rich_email": config.get("rich_email", 0),
            "rich_email_templates": config.get("rich_email_templates", []),
        }

    def _get_dkim_settings(self, integration_config):
        system_config = self.infrastructure_factory.get_config(self.context)

        dkim_settings: dict[str, Any] = {
            "use_dkim": bool(integration_config.get("use_dkim", False))
        }

        if dkim_settings["use_dkim"]:
            dkim_settings["dkim_domain"] = integration_config.get(
                "dkim_domain", ""
            ).encode("UTF-8")
            dkim_settings["dkim_algorithm"] = integration_config.get(
                "dkim_algorithm", "rsa-sha256"
            ).encode("UTF-8")

            use_custom_dkim_key = bool(
                integration_config.get("custom_dkim", False)
            )
            if use_custom_dkim_key:
                dkim_settings["dkim_selector"] = integration_config.get(
                    "dkim_selector", ""
                ).encode("UTF-8")

                storage_infra = self._get_infrastructure(name="s3")

                key_fh = BytesIO(initial_bytes=b"")
                storage_infra.download_file(
                    destination=key_fh,
                    file_uuid=integration_config["dkim_key"][0]["uuid"],
                    storage_location=integration_config["dkim_key"][0][
                        "storage_location"
                    ][0],
                )
                key_fh.seek(0, SEEK_SET)
            else:
                dkim_settings["dkim_selector"] = (
                    system_config["email"]
                    .get("dkim_selector", "unconfigured")
                    .encode("UTF-8")
                )
                key_fh = open(system_config["email"]["dkim_private_key"], "rb")

            dkim_settings["dkim_private_key"] = key_fh.read()
            key_fh.close()

        return dkim_settings

    def insert_contact_moment(self, event):
        message = self._generate_message_database_values(event)

        contact_moment = self._generate_contact_moment_database_values(event)
        insert_stmt = sql.insert(schema.ThreadMessageContactmoment).values(
            {**contact_moment}
        )
        try:
            contact_moment_inserted_id = self.session.execute(
                insert_stmt
            ).inserted_primary_key[0]
        except IntegrityError as e:
            raise Conflict(
                f"Message of type {message['type']} with uuid {event.entity_id} is not created",
                f"communication/message/{message['type']}/not_created",
            ) from e

        message["thread_message_contact_moment_id"] = (
            contact_moment_inserted_id
        )

        self._insert_message(values=message)

        self._update_cache(
            thread_uuid=event.entity_data["thread_uuid"],
        )

    def insert_note(self, event):
        message = self._generate_message_database_values(event)

        note = self._generate_note_database_values(event)

        insert_stmt = sql.insert(schema.ThreadMessageNote).values({**note})
        try:
            note_inserted_id = self.session.execute(
                insert_stmt
            ).inserted_primary_key[0]
        except IntegrityError as e:
            raise Conflict(
                f"Message of type {message['type']} with uuid {event.entity_id} is not created",
                f"communication/message/{message['type']}/not_created",
            ) from e
        message["thread_message_note_id"] = note_inserted_id
        self._insert_message(values=message)

        self._update_cache(
            thread_uuid=event.entity_data["thread_uuid"],
        )

    def insert_external_message(self, event):
        message = self._generate_message_database_values(event)
        external_message = self._generate_external_message_database_values(
            event
        )
        insert_stmt = sql.insert(schema.ThreadMessageExternal).values(
            {**external_message}
        )
        try:
            external_message_inserted_id = self.session.execute(
                insert_stmt
            ).inserted_primary_key[0]
        except IntegrityError as e:
            logger.exception("Database integrity error")
            raise Conflict(
                f"Message of type {message['type']} with uuid {event.entity_id} is not created",
                f"communication/message/{message['type']}/not_created",
            ) from e

        message["thread_message_external_id"] = external_message_inserted_id

        self._insert_message(values=message)

        self._update_cache(
            thread_uuid=event.entity_data["thread_uuid"],
        )

    def _insert_message(self, values: dict):
        insert_stmt = sql.insert(schema.ThreadMessage).values({**values})
        try:
            self.session.execute(insert_stmt)
        except IntegrityError as e:
            raise Conflict(
                f"Message with uuid: {values['uuid']} is not created",
                "communication/message/not_created",
            ) from e

    def _update_thread_attributes(
        self,
        last_message_cache: dict,
        thread_id: int,
    ):
        update_statement = (
            sql.update(schema.Thread)
            .where(schema.Thread.id == thread_id)
            .values(last_message_cache=last_message_cache)
            .execution_options(synchronize_session=False)
        )
        try:
            self.session.execute(update_statement)
        except IntegrityError as e:
            raise Conflict(
                "Thread message cache not created",
                "communication/thread/cache/not_created",
            ) from e

    def _generate_message_database_values(self, event):
        message = {}
        message["uuid"] = event.entity_id
        for change in event.changes:
            field_name = self.message_mapping.get(change["key"])
            if field_name:
                message[field_name] = change["new_value"]

            if change["key"] == "created_by":
                if change["new_value"]:
                    message["created_by_uuid"] = change["new_value"][
                        "entity_id"
                    ]
                else:
                    message["created_by_uuid"] = None
            if change["key"] == "thread_uuid":
                message["thread_id"] = (
                    sql.select(schema.Thread.id)
                    .where(schema.Thread.uuid == change["new_value"])
                    .scalar_subquery()
                )
        return message

    def _generate_note_database_values(self, event):
        note = {}
        for change in event.changes:
            field_name = self.note_mapping.get(change["key"], None)
            if field_name:
                note[field_name] = change["new_value"]

        return note

    def _generate_contact_moment_database_values(self, event):
        contact_moment = {}
        for change in event.changes:
            field_name = self.contact_moment_mapping.get(change["key"], None)
            if field_name:
                contact_moment[field_name] = change["new_value"]

            if change["key"] == "recipient":
                contact_moment["recipient_uuid"] = change["new_value"][
                    "entity_id"
                ]

        return contact_moment

    def _generate_external_message_database_values(self, event):
        external_message = {}
        try:
            is_imported = event.new_value("is_imported")
        except IndexError:
            is_imported = False
        for change in event.changes:
            field_name = self.external_message_mapping.get(change["key"], None)

            if field_name:
                external_message[field_name] = change["new_value"]

            if change["key"] == "original_message_file":
                external_message["source_file_id"] = (
                    sql.select(schema.Filestore.id)
                    .where(schema.Filestore.uuid == change["new_value"])
                    .scalar_subquery()
                )

            if change["key"] == "participants":
                (
                    participants_valid,
                    participants_invalid,
                ) = _list_of_participants(change, is_imported)

                if participants_invalid:
                    error_message = (
                        "Error sending email message: Invalid address "
                        f"({','.join(participants_invalid)})"
                    )
                    external_message["failure_reason"] = error_message

                external_message["participants"] = participants_valid

        if (
            "subject" in external_message
            and external_message["subject"] is None
        ):
            external_message["subject"] = ""

        return external_message

    def _update_cache(self, thread_uuid: UUID):
        last_message_row = self.session.execute(
            get_last_message_by_thread_uuid(thread_uuid=thread_uuid)
        ).fetchone()

        if not last_message_row:
            return
        values = self._create_cached_values(last_message_row)
        self._update_thread_attributes(
            last_message_cache=values,
            thread_id=last_message_row.thread_id,
        )

    def __extract_sender(self, participants: list) -> str:
        for participant in participants:
            if participant["role"] == "from":
                display_name: str | None = participant.get("display_name")
                address: str = participant.get("address", "")
                return display_name or address

        return ""

    def _create_cached_values(self, message):
        # Not all previous iterations have a (set) message_date in the message
        # Use it as last_message_cache `created`-date if available, to show the
        # thread_date in the UI as the message_date of the message.
        if message.message_date is not None:
            created = str(object=message.message_date.isoformat())
        else:
            created = str(message.created.isoformat())

        displayname = message.created_by_displayname
        if not displayname and message.participants:
            displayname = self.__extract_sender(message.participants)

        msg_cache = {
            "message_type": str(message.type),
            "slug": message.message_slug or "",
            "created_name": displayname or "",
            "created": created,
            "failure_reason": None,
        }

        if message.type == "note":
            msg_cache = msg_cache
        elif message.type == "contact_moment":
            msg_cache.update(
                {
                    "direction": str(message.contactmoment_direction),
                    "channel": str(message.contactmoment_channel),
                    "recipient_name": str(message.recipient_displayname),
                }
            )
        elif message.type == "external":
            external_type = message.external_message_type
            external_type = (
                external_type + "_message"
                if external_type == "pip"
                else external_type
            )

            if external_type == "email":
                failure_reason = message.external_message_failure_reason
            else:
                failure_reason = None

            msg_cache.update(
                {
                    "message_type": external_type,
                    "subject": str(message.external_message_subject),
                    "failure_reason": failure_reason,
                }
            )

        return msg_cache

    def _delete_message(self, event):
        message = self.get_message_by_uuid(event.entity_id)

        # We want the PIP messages be prevented from deletetion
        # since they are their own source of truth. Deletion of
        # PIP messages remove all existing references to the message
        if (
            message.message_type == "external"
            and message.external_message_type == "pip"
        ):
            raise Conflict(
                "PIP messages can not be deleted",
                "communication/message/delete_message/pip_deletion_prevented",
            )

        delete_message(
            session=self.session,
            uuid=event.entity_id,
            entity_type=event.entity_type,
        )
        self._update_cache(
            thread_uuid=event.entity_data["thread_uuid"],
        )

    def _get_case_assignee_contact_id(self, case_id: int):
        """Temp function to create notification and logging."""

        # TODO move notification creation to rabbitmq listener
        joined_qry = sql.join(
            schema.Case,
            schema.ZaakBetrokkenen,
            schema.Case.behandelaar == schema.ZaakBetrokkenen.id,
        ).join(
            schema.Subject,
            sql.and_(
                schema.Subject.id == schema.ZaakBetrokkenen.betrokkene_id,
                schema.ZaakBetrokkenen.betrokkene_type == "medewerker",
            ),
        )

        qry = (
            sql.select(
                schema.ZaakBetrokkenen.betrokkene_type,
                schema.ZaakBetrokkenen.betrokkene_id,
                schema.Subject.uuid,
            )
            .select_from(joined_qry)
            .where(schema.Case.id == case_id)
        )
        res = self.session.execute(qry).fetchone()
        if res is None:
            return {"legacy_id": None, "uuid": None}

        return {
            "legacy_id": f"betrokkene-{res.betrokkene_type}-{res.betrokkene_id}",
            "uuid": res.uuid,
        }

    def get_messages_by_uuid(
        self, uuids: list[UUID], user_info: object
    ) -> list[Message]:
        """Get external_messages(pip or email) for given uuids.

        :param uuids: uuids
        :type uuids: List[UUID]
        :param user_info: user info from the request
        :type user_info: object
        :return: list of messages
        :rtype: List[Message]
        """
        user_uuid = user_info.user_uuid
        is_pip = user_info.permissions.get("pip_user", False)

        query = get_message_query(self.session, user_uuid, is_pip).where(
            schema.ThreadMessage.uuid.in_(uuids)
        )
        message_list = self.session.execute(query).fetchall()

        return [
            self._transform_to_entity(message_row=message)
            for message in message_list
        ]

    def _mark_message_read(self, event):
        thread_message_uuid = event.entity_id
        external_message = self._generate_external_message_database_values(
            event
        )
        self.session.execute(
            sql.update(schema.ThreadMessageExternal)
            .values(external_message)
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == thread_message_uuid,
                    schema.ThreadMessage.type == "external",
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _mark_message_unread(self, event):
        thread_message_uuid = event.entity_id
        external_message = self._generate_external_message_database_values(
            event
        )

        self.session.execute(
            sql.update(schema.ThreadMessageExternal)
            .values(external_message)
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == thread_message_uuid,
                    schema.ThreadMessage.type == "external",
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _update_message_attachment_count(self, event):
        thread_message_uuid = event.entity_id
        external_message = self._generate_external_message_database_values(
            event
        )
        self.session.execute(
            sql.update(schema.ThreadMessageExternal)
            .values(external_message)
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == thread_message_uuid,
                    schema.ThreadMessage.type == "external",
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _update_message_thread_external_source_file_id(
        self, thread_message_uuid, file_store_id
    ):
        """
        When saving a external message to the filestore, the source_file_id
        in the thread_message_external table is set to the file_store_id
        of the filestore-row of the saved message

        :param thread_message_external_uuid: UUID of the external thread message
        :param file_store_id: the id of the filesore which is created when the external
                              thread message is stored
        :return: source_file_id is set True of False
        """

        # Seperate the SQL Join for readability
        sql_join = sql.join(
            schema.ThreadMessageExternal,
            schema.ThreadMessage,
            schema.ThreadMessage.thread_message_external_id
            == schema.ThreadMessageExternal.id,
        )

        # Execute the update as in the psql-update query in the psql comment above:

        sql_query = (
            sql.update(schema.ThreadMessageExternal)
            .values(source_file_id=file_store_id)
            .where(
                schema.ThreadMessageExternal.id
                == (
                    sql.select(schema.ThreadMessageExternal.id)
                    .select_from(sql_join)
                    .where(schema.ThreadMessage.uuid == thread_message_uuid)
                    .scalar_subquery()
                )
            )
            .execution_options(synchronize_session=False)
        )

        self.session.execute(sql_query)


def _add_html_version(
    message, external_message, context, email_config, storage_infra
):
    """Add the html version.  This converts the message into a multipart/alternative container,
    with the original text message as the first part and the new html message as the second part.
    """

    rich_template = None
    for template in email_config["rich_email_templates"]:
        if template["label"] == external_message.case_html_email_template:
            rich_template = template
            break

    if not rich_template and email_config["rich_email_templates"]:
        rich_template = email_config["rich_email_templates"][0]

    if rich_template:
        html_template = rich_template["template"]
        html_image = None
        if rich_template.get("image", []):
            html_image = rich_template["image"][0]

        # This escapes "HTML special" characters
        escaped_body_text = html.escape(external_message.content)
        # In HTML newlines are "just another space",
        # and if you have multiple, they get combined into a single space.
        escaped_body_text = escaped_body_text.replace("\n", "<br>")

        html_template = html_template.replace(
            "{{message}}", escaped_body_text, 1
        )

        if html_image:
            html_template = html_template.replace(
                "{{image_url}}", f"cid:{html_image['uuid']}", 1
            )

        message.add_alternative(html_template, subtype="html")

        # We include a related image in the html part, and we save a copy of what we are going to send to disk,
        # as well as sending it.
        if html_image:
            img = BytesIO(initial_bytes=b"")
            extension = html_image["mimetype"].split("/")[1]
            storage_infra.download_file(
                destination=img,
                file_uuid=html_image["uuid"],
                storage_location=html_image["storage_location"][0],
            )

            img.seek(0, SEEK_SET)
            message.get_payload()[1].add_related(
                img.read(), "image", extension, cid=html_image["uuid"]
            )

    return message


def _get_participants_for_message(external_message, email_config):
    """Get participants for an email_message"""

    logger = logging.getLogger(__name__)
    participants = {}

    for participant in external_message.participants:
        if participant["address"] == email_config["sender_address"]:
            # Skip adding ourselves to the recipient list
            continue
        try:
            email_validator.validate_email(participant["address"])
        except email_validator.EmailNotValidError as e:
            logger.info(
                "Invalid email:"
                f"Address: {participant['address']} "
                f"Exception: {e}"
            )
            continue

        role = participant["role"].lower()
        try:
            participants[role]
        except KeyError:
            participants[role] = []

        try:
            participants[role].append(
                headerregistry.Address(
                    display_name=participant["display_name"],
                    addr_spec=participant["address"],
                )
            )
        except Exception as e:
            logger.error(
                "Error adding participant: "
                f"Address: {participant['address']} "
                f"Displayname: {participant['display_name']}' "
                f"Exception: {e}"
            )
    return participants


def _set_message_headers(message, external_message, context, email_config):
    """Set header values for an email message"""

    participants = _get_participants_for_message(
        external_message, email_config
    )
    for header, value in participants.items():
        message[header] = value

    if "from" not in message:
        message["from"] = (
            f"{email_config['sender_name']} <{email_config['sender_address']}>"
        )
    message["Subject"] = _strip_newlines(external_message.subject)
    message["Message-ID"] = f"<{external_message.uuid}@{context}>"
    message["Date"] = email_utils.format_datetime(
        datetime.now(tz=timezone.utc)
    )
    return message


def _build_message(
    external_message: ExternalMessage,
    context: str,
    email_config,
    storage_infra=None,
):
    message = email_message.EmailMessage()
    message.set_content(external_message.content)

    if (
        email_config["rich_email"] == 1
        and external_message.case_uuid
        and not external_message.is_notification
    ):
        logger.info("Adding HTML version")
        _add_html_version(
            message, external_message, context, email_config, storage_infra
        )

    message = _set_message_headers(
        message, external_message, context, email_config
    )

    return message


def _dkim_sign_message(
    message: email_message.EmailMessage,
    dkim_config: dict,
):
    logger = logging.getLogger(__name__)
    signature = dkim.dkim_sign(
        message=message.as_bytes(),
        selector=dkim_config["dkim_selector"],
        domain=dkim_config["dkim_domain"],
        canonicalize=(b"relaxed", b"simple"),
        signature_algorithm=dkim_config["dkim_algorithm"],
        privkey=dkim_config["dkim_private_key"],
        logger=logger,
    )
    sig = signature.decode()
    sig = sig.replace("\r\n", "")

    logger.debug(f"DKIM signature: '{sig}'")
    message.add_header("DKIM-Signature", sig.lstrip("DKIM-Signature: "))


def _encode_thread_uuid(thread_uuid):
    return util.base62_encode(int(thread_uuid) & 0xFFFFFFFFFFFF)


def _list_of_participants(change, is_imported):
    participants_valid = []
    participants_invalid = []
    # No need to validate the email of participants for imported messages and
    # can add all of them as participant, else it would show an error.
    for participant in change["new_value"]:
        if not is_imported:
            try:
                email_validator.validate_email(participant["address"])
                participants_valid.append(participant)
            except email_validator.EmailNotValidError:
                participants_invalid.append(participant["address"])
        else:
            participants_valid.append(participant)
    return participants_valid, participants_invalid
