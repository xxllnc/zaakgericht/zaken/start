# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from ... import ZaaksysteemRepositoryBase
from ...shared.types import FilterUUIDExplicitEmptyValue
from ...shared.util import escape_term_for_like
from ..entities import Case, Contact, Thread
from . import database_queries
from minty.exceptions import Conflict, NotFound
from sqlalchemy import and_, cast, sql
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import IntegrityError
from uuid import UUID
from zsnl_domains.database import schema

logger = logging.getLogger(__name__)


class ThreadRepository(ZaaksysteemRepositoryBase):
    thread_mapping = {
        "thread_type": "thread_type",
        "contact_displayname": "contact_displayname",
        "created": "created",
        "last_modified": "last_modified",
        "last_message_cache": "last_message_cache",
        "unread_employee_count": "unread_employee_count",
        "unread_pip_count": "unread_pip_count",
        "attachment_count": "attachment_count",
        "is_notification": "is_notification",
    }

    entity_mapping = {"contact": "contact_uuid"}

    def get_thread_list(
        self,
        case_uuid: FilterUUIDExplicitEmptyValue,
        contact_uuid: FilterUUIDExplicitEmptyValue,
        message_types: str | None,
        user_uuid: UUID,
        permission: str,
        keyword: str | None,
        user_info,
        page: int,
        page_size: int,
    ) -> list[Thread]:
        """
        Retrieve list of Thread entities, by type (external, contactmoment,
         etc.) and by case UUID or contact UUID.

        If there are no results an empty list is returned.
        """

        is_pip_user = user_info.permissions.get("pip_user", False)
        if is_pip_user:
            thread_list_query = database_queries.get_thread_list_pip_query(
                user_uuid=user_uuid,
                case_uuid=case_uuid,
                contact_uuid=contact_uuid,
            )
        else:
            split_message_types = self._prepare_message_types(message_types)
            thread_list_query = database_queries.get_thread_list_query(
                case_uuid=case_uuid,
                contact_uuid=contact_uuid,
                message_types=split_message_types,
                db=self.session,
                user_uuid=user_uuid,
                permission=permission,
            ).where(schema.Thread.is_notification.is_(False))

        if keyword:
            escaped_keyword = "%" + escape_term_for_like(keyword) + "%"

            last_message_cache = sql.cast(
                schema.Thread.last_message_cache, postgresql.JSON
            )
            thread_list_query = thread_list_query.where(
                sql.or_(
                    sql.func.lower(last_message_cache["slug"].astext).like(
                        sql.func.lower(escaped_keyword), escape="~"
                    ),
                    sql.func.lower(last_message_cache["subject"].astext).like(
                        sql.func.lower(escaped_keyword), escape="~"
                    ),
                    sql.func.lower(
                        last_message_cache["created_name"].astext
                    ).like(sql.func.lower(escaped_keyword), escape="~"),
                    sql.func.lower(
                        last_message_cache["recipient_name"].astext
                    ).like(sql.func.lower(escaped_keyword), escape="~"),
                )
            )

        offset = self._calculate_offset(page, page_size)
        thread_list_query = thread_list_query.limit(page_size).offset(offset)

        thread_list = self.session.execute(thread_list_query).fetchall()

        return [
            self._transform_to_entity(thread_row) for thread_row in thread_list
        ]

    def _prepare_message_types(
        self, message_types: str | None
    ) -> set[str] | None:
        allowed_types = {
            "contact_moment",
            "email",
            "note",
            "pip_message",
            "postex",
        }

        if not message_types:
            return None

        exploded_types = set(message_types.split(","))

        the_difference = exploded_types.difference(allowed_types)

        if the_difference:
            raise Conflict(
                f"Allowed message types are {sorted(allowed_types)}.",
                "communication/message_type/not_allowed",
            )

        return exploded_types

    def get_thread_by_partial_uuid(self, partial_uuid):
        """Retrieve a thread, given a partial UUID (as used in email subject lines)

        :param partial_uuid: The partial UUID, Base62 encoded, from the subject
        :type partial_uuid: str
        """
        threads = self.session.execute(
            database_queries.thread_by_partial_uuid_statement(
                partial_uuid=partial_uuid
            )
        ).fetchall()

        if len(threads) > 1:
            raise Conflict(
                f"Multiple threads with partial UUID {partial_uuid} found",
                "communication/thread/multiple_found",
            )
        elif len(threads) == 0:
            raise NotFound(
                f"Thread with partial UUID {partial_uuid} was not found",
                "communication/thread/not_found",
            )

        # Exactly one thread was returned
        return self._transform_to_entity(threads[0])

    def get_notification_thread_for_case(self, case_uuid: UUID):
        query = database_queries.thread_list_query.where(
            sql.and_(
                schema.Thread.case_id
                == sql.select(schema.Case.id)
                .where(schema.Case.uuid == case_uuid)
                .scalar_subquery(),
                schema.Thread.is_notification.is_(True),
            )
        )

        result = self.session.execute(query).fetchone()
        if not result:
            return None

        return self._transform_to_entity(result)

    def get_thread_by_uuid(
        self,
        thread_uuid: UUID,
        check_acl: bool,
        user_uuid: UUID,
        permission: str,
    ):
        result = self.session.execute(
            database_queries.thread_by_uuid_statement(
                thread_uuid=thread_uuid,
                check_acl=check_acl,
                session=self.session,
                user_uuid=user_uuid,
                permission=permission,
            )
        ).fetchone()
        if not result:
            return None

        return self._transform_to_entity(result)

    def get_thread_for_pip_by_uuid(self, thread_uuid: UUID, user_uuid: UUID):
        """Get thread for a pip user by UUID.

        :param thread_uuid: uuid of the thread.
        :type thread_uuid: UUID
        :param user_uuid: uuid of the user.
        :type user_uuid: UUID
        :return: thread as Thread Entity
        :rtype: Thread
        """
        result = self.session.execute(
            database_queries.get_thread_pip_query(user_uuid=user_uuid).where(
                schema.Thread.uuid == thread_uuid
            )
        ).fetchone()

        if not result:
            return None

        return self._transform_to_entity(result)

    def _transform_to_entity(self, result_row) -> Thread:
        """Create a Thread entity from a database result row."""
        if result_row.case_id:
            case = Case(
                id=result_row.case_id,
                uuid=result_row.case_uuid,
                status=result_row.case_status,
                description=result_row.case_description,
                description_public=result_row.case_description_public,
                case_type_name=result_row.case_type_name,
            )
        else:
            case = None

        contact = None
        if result_row.contact_uuid:
            contact = Contact(
                id=None,
                uuid=result_row.contact_uuid,
                name=result_row.contact_displayname,
                type=None,
            )

        # Not all previous iterations provide a failure_reason
        # in the last message cache. So check if it is in there
        # if not, add it and set it no None
        last_message_cache = result_row.last_message_cache
        if "failure_reason" not in last_message_cache:
            last_message_cache["failure_reason"] = None

        thread = Thread(
            uuid=result_row.uuid,
            id=result_row.id,
            thread_type=result_row.last_message_cache["message_type"],
            created=result_row.created,
            last_modified=result_row.last_modified,
            last_message_cache=last_message_cache,
            case=case,
            contact=contact,
            number_of_messages=result_row.message_count,
            unread_pip_count=result_row.unread_pip_count,
            unread_employee_count=result_row.unread_employee_count,
            attachment_count=result_row.attachment_count,
            is_notification=result_row.is_notification,
        )
        thread.event_service = self.event_service
        return thread

    def create(
        self,
        new_thread_uuid: UUID,
        thread_type: str,
        contact: Contact | None,
        case: Case | None,
        is_notification: bool = False,
    ) -> Thread:
        "Create a thread."

        thread = Thread(uuid=new_thread_uuid)
        thread.event_service = self.event_service

        thread.create(
            thread_type=thread_type,
            contact=contact,
            case=case,
            is_notification=is_notification,
        )
        return thread

    def save(self):
        """Save changes on Thread entity back to database."""
        unread_count_sync_threads = set()

        for event in self.event_service.event_list:
            if event.event_name == "ThreadCreated":
                self.insert_thread(event)
            if event.event_name == "ThreadToCaseLinked":
                self._link_thread_to_case(event)
            if event.event_name == "ThreadDeleted":
                self._delete_thread(event)
            if (
                event.event_name == "ThreadUnReadCountIncremented"
                or event.event_name == "ThreadUnReadCountDecremented"
            ):
                self._update_thread_unread_count(event)
                unread_count_sync_threads.add(event.entity_id)
            if (
                event.event_name == "ThreadAttachmentCountIncremented"
                or event.event_name == "ThreadAttachmentCountDecremented"
            ):
                self._update_thread_attachment_count(event)

        for thread_uuid in unread_count_sync_threads:
            self.logger.debug(
                f"Must sync case unread count for thread {thread_uuid}"
            )

    def _link_thread_to_case(self, event):
        """Update thread  case_id to link it to a case."""
        update_values = self._generate_database_values(event=event)
        last_message_cache = update_values["last_message_cache"]
        update_statement = (
            sql.update(schema.Thread)
            .where(
                and_(
                    schema.Thread.case_id.is_(None),
                    cast(schema.Thread.last_message_cache, postgresql.JSON)[
                        "message_type"
                    ].astext
                    == last_message_cache["message_type"],
                    schema.Thread.uuid == event.entity_id,
                )
            )
            .values(
                case_id=update_values["case_id"],
                last_modified=update_values["last_modified"],
            )
            .execution_options(synchronize_session=False)
        )

        try:
            self.session.execute(update_statement)
        except IntegrityError as e:
            raise Conflict(
                f"Thread with uuid {event.entity_id} could not be linked to case number {update_values['case_id']}",
                "communication/thread/not_linked",
            ) from e

    def _generate_database_values(self, event):
        """Generate a map with the table columns and values to insert to the table."""
        db_values = {}
        db_values["uuid"] = event.entity_id

        for change in event.changes:
            if change["key"] == "contact":
                db_values["contact_uuid"] = change["new_value"]["entity_id"]
            elif change["key"] == "case":
                db_values["case_id"] = (
                    sql.select(schema.Case.id)
                    .where(
                        schema.Case.uuid == change["new_value"]["entity_id"]
                    )
                    .scalar_subquery()
                )
            elif (
                change["key"] == "unread_employee_count"
                or change["key"] == "unread_pip_count"
            ):
                db_fieldname = change["key"]
                db_values[db_fieldname] = self._validate_unread_messages_count(
                    event_name=event.event_name,
                    thread_uuid=event.entity_id,
                    type=db_fieldname,
                    thread_count=change["new_value"],
                )
            else:
                db_fieldname = self.thread_mapping.get(change["key"])
                if db_fieldname:
                    db_values[db_fieldname] = change["new_value"]

        return db_values

    def _validate_unread_messages_count(
        self, event_name: str, thread_uuid: UUID, type: str, thread_count: int
    ):
        new_thread_count = new_unread_employee_count = new_unread_pip_count = 0
        current_thread_count = self.session.execute(
            sql.select(
                schema.Thread.unread_employee_count,
                schema.Thread.unread_pip_count,
            ).where(schema.Thread.uuid == thread_uuid)
        ).fetchone()
        if current_thread_count is None:
            return new_thread_count

        old_unread_employee_count = current_thread_count.unread_employee_count
        old_unread_pip_count = current_thread_count.unread_pip_count

        if event_name == "ThreadUnReadCountDecremented":
            new_unread_employee_count = old_unread_employee_count - 1
            new_unread_pip_count = old_unread_pip_count - 1

        if event_name == "ThreadUnReadCountIncremented":
            new_unread_employee_count = old_unread_employee_count + 1
            new_unread_pip_count = old_unread_pip_count + 1

        if type == "unread_employee_count":
            if (
                new_unread_employee_count == thread_count
                and new_unread_employee_count >= 0
            ):
                new_thread_count = thread_count
        elif type == "unread_pip_count":
            if (
                new_unread_pip_count == thread_count
                and new_unread_pip_count >= 0
            ):
                new_thread_count = thread_count

        return new_thread_count

    def insert_thread(self, event):
        """Add new row to Thread table."""

        insert_values = self._generate_database_values(event=event)
        insert_stmt = sql.insert(schema.Thread).values({**insert_values})

        try:
            self.session.execute(insert_stmt)
        except IntegrityError as e:
            raise Conflict(
                f"Thread with uuid {insert_values['uuid']} could not be created",
                "communication/thread/not_created",
            ) from e

    def _delete_thread(self, event):
        self.session.execute(database_queries.delete_thread(event.entity_id))

    def _update_thread_unread_count(self, event):
        update_values = self._generate_database_values(event=event)
        update_statement = (
            sql.update(schema.Thread)
            .where(and_(schema.Thread.uuid == event.entity_id))
            .values(update_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_statement)

    def _update_thread_attachment_count(self, event):
        update_values = self._generate_database_values(event=event)
        update_statement = (
            sql.update(schema.Thread)
            .where(and_(schema.Thread.uuid == event.entity_id))
            .values(update_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_statement)
