# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .base import UploadTemplate

STYLE_CONFIGURATION_COMMANDS = minty.cqrs.build_command_lookup_table(
    commands={UploadTemplate}
)
