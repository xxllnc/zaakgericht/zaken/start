# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID

MAX_FILE_SIZE = 10  # size in MB

VALID_CONFIG_FILE_TYPES = [
    "config.json",
    "cssProps.json",
    "favicon.ico",
    "logo-login.png",
    "logo-pip.png",
    "logo-login.svg",
    "logo-pip.svg",
    "logo-login.jpg",
    "logo-pip.jpg",
    "logo-login.jpeg",
    "logo-pip.jpeg",
    "stylesheet.css",
]


class StyleConfiguration(Entity):
    "Represents the style configuration for templates"

    uuid: UUID = Field(..., title="Unique Identifier for the style config")
    tenant: str = Field(..., title="Tenant identifier for the style config")
    name: str = Field(..., title="Name of the style config")
    content: str = Field(..., title="Content of the style config")
    extension: str = Field(..., title="File extension of the style config")
    mimetype: str = Field(..., title="File mimetype of the style config")

    entity_type = "style_configuration"
    entity_id__fields: list[str] = ["uuid"]

    @classmethod
    @Entity.event(name="StyleConfigurationCreated", fire_always=True)
    def create(cls, **kwargs):
        style_configuration = cls(**kwargs)
        return style_configuration
