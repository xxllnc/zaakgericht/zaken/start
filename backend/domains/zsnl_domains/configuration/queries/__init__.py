# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .base import GetContent, GetTenants

STYLE_CONFIGURATION_QUERIES = minty.cqrs.build_query_lookup_table(
    queries={GetTenants, GetContent}
)
