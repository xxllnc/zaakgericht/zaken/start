# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from .style_configuration import StyleConfigurationRepository

__all__ = ["StyleConfigurationRepository"]
