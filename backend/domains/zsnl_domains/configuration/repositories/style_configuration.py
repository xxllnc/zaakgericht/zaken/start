# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import StyleConfiguration, Tenant
from datetime import datetime, timezone
from minty.cqrs import Event, UserInfo
from minty.exceptions import NotFound, ValidationError
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from typing import Optional
from zsnl_domains.database import schema

base_style_config_query = sql.select(
    schema.StyleConfig.content,
    schema.StyleConfig.last_modified,
    schema.StyleConfig.name,
    schema.StyleConfig.tenant,
    schema.StyleConfig.uuid,
    schema.StyleConfig.extension,
    schema.StyleConfig.mimetype,
)

base_tenant_query = sql.select(
    schema.StyleConfig.tenant, schema.StyleConfig.name
)


class StyleConfigurationRepository(ZaaksysteemRepositoryBase):
    _for_entity = "StyleConfiguration"
    _events_to_calls = {
        "StyleConfigurationCreated": "_create_style_configuration"
    }

    def create_style_configuration(
        self, uuid, tenant, name, extension, content, mimetype
    ):
        return StyleConfiguration.create(
            # Basic attributes
            content=content,
            name=name,
            extension=extension,
            tenant=tenant,
            uuid=uuid,
            mimetype=mimetype,
            # Services
            _event_service=self.event_service,
        )

    def _create_style_configuration_for_zss(self, changes):
        values = {
            "uuid": changes["uuid"],
            "content": changes["content"],
            "last_modified": datetime.now(timezone.utc),
            "name": changes["name"],
            "extension": changes["extension"],
            "tenant": changes["tenant"],
            "mimetype": changes["mimetype"],
        }
        current_template = self.session.execute(
            (base_style_config_query).where(
                sql.and_(
                    schema.StyleConfig.extension == "zss",
                    schema.StyleConfig.tenant == changes["tenant"],
                )
            )
        ).fetchone()

        if current_template:
            self.session.execute(
                sql.update(schema.StyleConfig)
                .where(
                    sql.and_(
                        schema.StyleConfig.tenant == changes["tenant"],
                        schema.StyleConfig.extension == "zss",
                    )
                )
                .values(values)
                .execution_options(synchronize_session=False)
            )
        else:
            self.session.execute(
                sql_pg.insert(schema.StyleConfig).values(**values)
            )

    def _create_style_configuration(
        self,
        event: Event,
        user_info: UserInfo | None = None,
        dry_run: bool = False,
    ) -> None:
        changes = event.format_changes()
        self.logger.info(
            f"Creating style configuration record for tenant={changes['tenant']} and file {changes['name']}"
        )
        if changes["extension"] == "zss":
            self._create_style_configuration_for_zss(changes)
        else:
            self.session.execute(
                sql_pg.insert(schema.StyleConfig)
                .values(
                    uuid=changes["uuid"],
                    tenant=changes["tenant"],
                    name=changes["name"],
                    content=changes["content"],
                    extension=changes["extension"],
                    last_modified=datetime.now(timezone.utc),
                    mimetype=changes["mimetype"],
                )
                .on_conflict_do_update(
                    index_elements=[
                        schema.StyleConfig.tenant,
                        schema.StyleConfig.name,
                    ],
                    set_={
                        "uuid": changes["uuid"],
                        "content": changes["content"],
                        "last_modified": datetime.now(timezone.utc),
                        "name": changes["name"],
                        "extension": changes["extension"],
                        "tenant": changes["tenant"],
                        "mimetype": changes["mimetype"],
                    },
                )
            )

    def validate_tenant(self, tenant: str) -> Optional[bool]:
        system_config = self.infrastructure_factory.get_config(
            context=self.context
        )
        valid_hosts = list(system_config["VirtualHosts"])

        # Add current host if its not in the list already
        if [self.context] not in valid_hosts:
            valid_hosts.extend([self.context])

        if tenant not in valid_hosts:
            raise ValidationError(
                f"Invalid tenant.Valid tenants are {','.join(valid_hosts)}",
                "tenant/invalid",
            )
        return

    def _get_template_info_for_tenants(self, tenant_list):
        rows = self.session.execute(
            (base_tenant_query).where(
                sql.and_(
                    schema.StyleConfig.tenant.in_(tenant_list),
                    schema.StyleConfig.name.notin_(
                        [
                            "logo-login",
                            "logo-pip",
                            "cssProps",
                            "favicon",
                            "config",
                            "stylesheet",
                        ]
                    ),
                )
            )
        ).fetchall()
        template_info = {}
        for row in rows:
            template_info[row.tenant] = row.name

        return template_info

    def get_tenants(self):
        system_config = self.infrastructure_factory.get_config(
            context=self.context
        )
        tenants = [self.context]
        virtual_hosts = system_config["VirtualHosts"].keys()
        if len(virtual_hosts) >= 1:
            tenants.extend(list(virtual_hosts))

        template_info = self._get_template_info_for_tenants(tenants)

        tenat_template_list = []
        for tenant in tenants:
            template = (
                template_info[tenant] if tenant in template_info else None
            )
            tenat_template_list.append({"name": tenant, "template": template})

        return [
            self._entity_from_tenants_list(row) for row in tenat_template_list
        ]

    def _entity_from_tenants_list(self, row):
        return Tenant.parse_obj(
            {"name": row["name"], "template": row["template"]}
        )

    def get_content(self, name, tenant):
        style_config_row = self.session.execute(
            (base_style_config_query).where(
                sql.and_(
                    schema.StyleConfig.name == name,
                    schema.StyleConfig.tenant == tenant,
                )
            )
        ).fetchone()

        if style_config_row is None:
            raise NotFound(f"No config found for {name}.")

        return self._entity_from_row(style_config_row)

    def _entity_from_row(self, row):
        return StyleConfiguration.parse_obj(
            {
                "uuid": row.uuid,
                "entity_id": row.uuid,
                "tenant": row.tenant,
                "last_modified": row.last_modified,
                "name": row.name,
                "content": row.content,
                "extension": row.extension,
                "mimetype": row.mimetype,
            }
        )
