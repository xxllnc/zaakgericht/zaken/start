# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from .base import Base
from .types import GUID, JSONEncodedDict, UTCDateTime
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.schema import (
    FetchedValue,
    ForeignKey,
)
from sqlalchemy.types import Enum
from typing import Literal, Optional, get_args
from uuid import UUID

ThreadType = Literal["note", "contact_moment", "external", "postex"]


class Thread(Base):
    """A Thread representation"""

    __tablename__ = "thread"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, nullable=False, unique=True, server_default=FetchedValue()
    )

    contact_uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    contact_displayname: Mapped[Optional[str]]

    case_id: Mapped[Optional[int]] = mapped_column(ForeignKey("zaak.id"))

    created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    thread_type: Mapped[ThreadType] = mapped_column(
        Enum(*get_args(ThreadType))
    )
    last_message_cache: Mapped[dict] = mapped_column(JSONEncodedDict)
    message_count: Mapped[int] = mapped_column(default=0)
    unread_pip_count: Mapped[int] = mapped_column(default=0)
    unread_employee_count: Mapped[int] = mapped_column(default=0)
    attachment_count: Mapped[int] = mapped_column(default=0)
    is_notification: Mapped[bool] = mapped_column(default=False)


ThreadMessageType = Literal["note", "contact_moment", "external", "postex"]


class ThreadMessage(Base):
    """A Thread Message representation"""

    __tablename__ = "thread_message"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, nullable=False, unique=True, server_default=FetchedValue()
    )

    thread_id: Mapped[int] = mapped_column(ForeignKey("thread.id"))

    message_slug: Mapped[str]
    type: Mapped[ThreadMessageType] = mapped_column(
        Enum(*get_args(ThreadMessageType))
    )

    created_by_uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    created_by_displayname: Mapped[Optional[str]]

    created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    message_date: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime, nullable=True
    )

    thread_message_note_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("thread_message_note.id")
    )
    thread_message_contact_moment_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("thread_message_contact_moment.id")
    )
    thread_message_external_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("thread_message_external.id")
    )


class ThreadMessageNote(Base):
    """A Thread Message Note representation"""

    __tablename__ = "thread_message_note"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    content: Mapped[str]


ThreadMessageContactmomentChannel = Literal[
    "assignee",
    "email",
    "external_application",
    "frontdesk",
    "mail",
    "phone",
    "social_media",
    "webform",
    "chat",
    "other",
]
ThreadMessageContactmomentDirection = Literal["incoming", "outgoing"]


class ThreadMessageContactmoment(Base):
    """A Thread Message Contact Moment representation"""

    __tablename__ = "thread_message_contact_moment"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    content: Mapped[str]
    contact_channel: Mapped[ThreadMessageContactmomentChannel] = mapped_column(
        Enum(*get_args(ThreadMessageContactmomentChannel))
    )

    direction: Mapped[Optional[ThreadMessageContactmomentDirection]] = (
        mapped_column(Enum(*get_args(ThreadMessageContactmomentDirection)))
    )
    recipient_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("subject.uuid")
    )
    recipient_displayname: Mapped[str]


ThreadMessageExternalType = Literal["pip", "email", "postex"]
ThreadMessageExternalDirection = Literal["incoming", "outgoing", "unspecified"]


class ThreadMessageExternal(Base):
    """A Thread Message "External Message" representation"""

    __tablename__ = "thread_message_external"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    content: Mapped[str]
    subject: Mapped[str]
    participants: Mapped[list[dict]] = mapped_column(JSONEncodedDict)
    type: Mapped[ThreadMessageExternalType] = mapped_column(
        Enum(*get_args(ThreadMessageExternalType))
    )
    direction: Mapped[ThreadMessageExternalDirection] = mapped_column(
        Enum(*get_args(ThreadMessageExternalDirection))
    )
    source_file_id: Mapped[int] = mapped_column(
        ForeignKey("filestore.id"), nullable=True
    )
    read_employee: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    read_pip: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    attachment_count: Mapped[int] = mapped_column(default=0)
    failure_reason: Mapped[Optional[str]]


class ThreadMessageAttachment(Base):
    """A Thread Message Attachment representation"""

    __tablename__ = "thread_message_attachment"

    uuid: Mapped[UUID] = mapped_column(
        GUID, nullable=False, unique=True, server_default=FetchedValue()
    )
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)

    filestore_id: Mapped[int] = mapped_column(
        ForeignKey("filestore.id"), nullable=False
    )
    filename: Mapped[str] = mapped_column(nullable=False)
    thread_message_id: Mapped[int] = mapped_column(
        ForeignKey("thread_message.id"), nullable=False
    )


ThreadMessageAttachmentDerivativeType = Literal[
    "pdf", "thumbnail", "docx", "doc"
]


class ThreadMessageAttachmentDerivative(Base):
    """A Thread Message Attachment Derivative representation"""

    __tablename__ = "thread_message_attachment_derivative"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    thread_message_attachment_id: Mapped[int] = mapped_column(
        ForeignKey("thread_message_attachment.id")
    )
    filestore_id: Mapped[int] = mapped_column(ForeignKey("filestore.id"))
    max_width: Mapped[int]
    max_height: Mapped[int]
    date_generated: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    type: Mapped[ThreadMessageAttachmentDerivativeType] = mapped_column(
        Enum(*get_args(ThreadMessageAttachmentDerivativeType))
    )
