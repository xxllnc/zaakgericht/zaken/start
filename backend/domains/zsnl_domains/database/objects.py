# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from .base import Base
from .types import GUID, UTCDateTime
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.schema import (
    FetchedValue,
    ForeignKey,
    PrimaryKeyConstraint,
)
from sqlalchemy.types import Enum
from typing import Literal, Optional, get_args
from uuid import UUID


class CustomObjectType(Base):
    """Table pointing to the active custom object definition"""

    __tablename__ = "custom_object_type"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    catalog_folder_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )

    authorization_definition: Mapped[Optional[dict]] = mapped_column(JSONB)

    custom_object_type_version_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object_type_version.id")
    )


CustomObjectTypeAclAuthorization = Literal["readwrite", "admin", "read"]


class CustomObjectTypeAcl(Base):
    """View on several tables, to determine which user has access to which custom_object"""

    __tablename__ = "custom_object_type_acl"
    __table_args__ = (
        PrimaryKeyConstraint(
            "custom_object_type_id", "group_id", "role_id", "authorization"
        ),
    )
    custom_object_type_id: Mapped[int] = mapped_column(
        ForeignKey("custom_object_type.id")
    )
    authorization: Mapped[CustomObjectTypeAclAuthorization] = mapped_column(
        Enum(*get_args(CustomObjectTypeAclAuthorization))
    )
    group_id: Mapped[int] = mapped_column(ForeignKey("groups.id"))
    role_id: Mapped[int] = mapped_column(ForeignKey("roles.id"))


CustomObjectTypeVersionStatus = Literal["active", "inactive"]


class CustomObjectTypeVersion(Base):
    """Table containing custom object definitions"""

    __tablename__ = "custom_object_type_version"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    name: Mapped[Optional[str]]
    title: Mapped[Optional[str]]
    subtitle: Mapped[Optional[str]]
    status: Mapped[CustomObjectTypeVersionStatus] = mapped_column(
        Enum(*get_args(CustomObjectTypeVersionStatus))
    )
    version: Mapped[int]
    external_reference: Mapped[Optional[str]]

    custom_object_type_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object_type.id")
    )

    custom_field_definition: Mapped[Optional[dict]] = mapped_column(JSONB)
    authorizations: Mapped[Optional[dict]] = mapped_column(JSONB)
    relationship_definition: Mapped[Optional[dict]] = mapped_column(JSONB)

    audit_log: Mapped[dict] = mapped_column(JSONB)

    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )


CustomObjectVersionContentArchiveStatus = Literal[
    "archived",
    "to destroy",
    "to preserve",
]


class CustomObjectVersionContent(Base):
    __tablename__ = "custom_object_version_content"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    archive_status: Mapped[
        Optional[CustomObjectVersionContentArchiveStatus]
    ] = mapped_column(Enum(*get_args(CustomObjectVersionContentArchiveStatus)))
    archive_ground: Mapped[Optional[str]]
    archive_retention: Mapped[Optional[int]]

    custom_fields: Mapped[Optional[dict]] = mapped_column(JSONB)


class CustomObject(Base):
    __tablename__ = "custom_object"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    custom_object_version_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object_version.id")
    )


CustomObjectVersionStatus = Literal[
    "active",
    "inactive",
    "draft",
]


class CustomObjectVersion(Base):
    __tablename__ = "custom_object_version"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    title: Mapped[Optional[str]]
    subtitle: Mapped[Optional[str]] = mapped_column(nullable=True)
    external_reference: Mapped[Optional[str]] = mapped_column(nullable=True)

    status: Mapped[CustomObjectVersionStatus] = mapped_column(
        Enum(*get_args(CustomObjectVersionStatus))
    )
    version: Mapped[int]

    custom_object_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object.id"), nullable=True
    )

    custom_object_version_content_id: Mapped[int] = mapped_column(
        ForeignKey("custom_object_version_content.id")
    )

    custom_object_type_version_id: Mapped[int] = mapped_column(
        ForeignKey("custom_object_type_version.id")
    )

    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )


CustomObjectRelationshipType = Literal[
    "document", "case", "custom_object", "subject"
]


class CustomObjectRelationship(Base):
    __tablename__ = "custom_object_relationship"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        autoincrement=True,
    )
    custom_object_id: Mapped[int] = mapped_column(
        primary_key=True,
    )
    custom_object_version_id: Mapped[Optional[int]]

    # Relationship meta data
    relationship_type: Mapped[CustomObjectRelationshipType] = mapped_column(
        Enum(*get_args(CustomObjectRelationshipType))
    )
    relationship_magic_string_prefix: Mapped[Optional[str]]

    # Relationship "document"
    related_document_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("file.id")
    )

    # Relationship "case"
    related_case_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaak.id")
    )

    # Relationship "subject"
    related_employee_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("subject.id")
    )
    related_person_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("natuurlijk_persoon.id")
    )
    related_organization_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bedrijf.id")
    )

    # Relationship "custom_object"
    related_custom_object_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object.id")
    )

    related_uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    # Relationship Attribute
    source_custom_field_type_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )

    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
