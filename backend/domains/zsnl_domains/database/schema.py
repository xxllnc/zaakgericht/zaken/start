# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import decimal
from .base import Base
from .communication_module import (  # noqa: F401
    Thread,
    ThreadMessage,
    ThreadMessageAttachment,
    ThreadMessageAttachmentDerivative,
    ThreadMessageContactmoment,
    ThreadMessageExternal,
    ThreadMessageNote,
)
from .objects import (  # noqa: F401
    CustomObject,
    CustomObjectRelationship,
    CustomObjectType,
    CustomObjectTypeAcl,
    CustomObjectTypeVersion,
    CustomObjectVersion,
    CustomObjectVersionContent,
)
from .types import GUID, JSONEncodedDict, UTCDate, UTCDateTime
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import (
    FetchedValue,
    ForeignKey,
    PrimaryKeyConstraint,
    UniqueConstraint,
)
from sqlalchemy.types import (
    BigInteger,
    Date,
    DateTime,
    Enum,
    Integer,
    Numeric,
    SmallInteger,
    String,
    Text,
)
from typing import Literal, Optional, get_args
from uuid import UUID

# TODO Split schema, automate filling of __all__ (remove the F401 above)

QueueItemStatus = Literal[
    "pending", "running", "finished", "failed", "waiting"
]


PreferredContactChannel = Literal["email", "mail", "pip", "phone"]


class Queue(Base):
    """Legacy background task queue"""

    __tablename__ = "queue"

    id: Mapped[UUID] = mapped_column(
        GUID, primary_key=True, server_default=FetchedValue()
    )
    object_id: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=False, server_default=FetchedValue()
    )
    status: Mapped[QueueItemStatus] = mapped_column(
        Enum(*get_args(QueueItemStatus))
    )
    type: Mapped[str]
    label: Mapped[str]
    data: Mapped[dict] = mapped_column(JSONEncodedDict)
    date_created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    date_started: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_finished: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    parent_id: Mapped[Optional[UUID]] = mapped_column(
        GUID, ForeignKey("queue.id")
    )
    priority: Mapped[int]
    _metadata: Mapped[dict] = mapped_column(
        "metadata", JSONEncodedDict, server_default="{}"
    )


class CaseMeta(Base):
    """Case-specific metadata

    This includes reasons for stalling and resumption, and the "stalled-since"
    date.
    """

    __tablename__ = "zaak_meta"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    zaak_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    verlenging: Mapped[Optional[str]] = mapped_column(String(255))
    opschorten: Mapped[Optional[str]] = mapped_column(String(255))
    deel: Mapped[Optional[str]] = mapped_column(String(255))
    gerelateerd: Mapped[Optional[str]] = mapped_column(String(255))
    vervolg: Mapped[Optional[str]] = mapped_column(String(255))
    afhandeling: Mapped[Optional[str]] = mapped_column(String(255))
    stalled_since: Mapped[Optional[datetime.date]] = mapped_column(UTCDate)
    current_deadline: Mapped[dict] = mapped_column(
        postgresql.JSONB, default={}
    )
    deadline_timeline: Mapped[list[dict]] = mapped_column(
        postgresql.JSONB, default="[]"
    )
    last_modified: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    unread_communication_count: Mapped[int] = mapped_column(default=0)
    unaccepted_attribute_update_count: Mapped[int] = mapped_column(default=0)
    unaccepted_files_count: Mapped[int] = mapped_column(default=0)
    pending_changes: Mapped[dict] = mapped_column(
        postgresql.JSONB, default="{}"
    )
    text_vector: Mapped[Optional[str]] = mapped_column(postgresql.TSVECTOR)


CaseArchivalState = Literal["overdragen", "vernietigen"]
CaseStatus = Literal["new", "open", "stalled", "resolved", "deleted"]
CaseTrigger = Literal["extern", "intern"]
CaseConfidentiality = Literal["public", "internal", "confidential"]


class Case(Base):
    """Main "case" storage"""

    __tablename__ = "zaak"

    meta = relationship("CaseMeta")
    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    pid: Mapped[Optional[int]] = mapped_column(
        BigInteger, ForeignKey("zaak.id")
    )
    number_master: Mapped[Optional[int]] = mapped_column(
        BigInteger, ForeignKey("zaak.id")
    )

    coordinator: Mapped[Optional[int]]
    behandelaar: Mapped[Optional[int]]
    aanvrager: Mapped[int]

    route_ou: Mapped[Optional[int]]
    route_role: Mapped[Optional[int]]

    behandelaar_gm_id: Mapped[Optional[int]]
    coordinator_gm_id: Mapped[Optional[int]]
    aanvrager_gm_id: Mapped[Optional[int]]
    aanvrager_type: Mapped[Optional[str]]

    archival_state: Mapped[Optional[CaseArchivalState]] = mapped_column(
        Enum(*get_args(CaseArchivalState))
    )
    status: Mapped[CaseStatus] = mapped_column(Enum(*get_args(CaseStatus)))

    created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    last_modified: Mapped[datetime.datetime] = mapped_column(UTCDateTime)

    registratiedatum: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    streefafhandeldatum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    afhandeldatum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    vernietigingsdatum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    stalled_until: Mapped[Optional[datetime.date]] = mapped_column(UTCDate)

    milestone: Mapped[int]

    relates_to: Mapped[Optional[int]]
    zaaktype_id: Mapped[int]
    zaaktype_node_id: Mapped[int]
    contactkanaal: Mapped[str]
    aanvraag_trigger: Mapped[CaseTrigger] = mapped_column(
        Enum(*get_args(CaseTrigger))
    )
    onderwerp: Mapped[Optional[str]]
    onderwerp_extern: Mapped[Optional[str]]

    resultaat: Mapped[Optional[str]]
    resultaat_id: Mapped[Optional[int]]

    besluit: Mapped[Optional[str]]

    locatie_zaak: Mapped[Optional[int]]
    locatie_correspondentie: Mapped[Optional[int]]

    vervolg_van: Mapped[Optional[int]] = mapped_column(BigInteger)
    payment_status: Mapped[Optional[str]]
    payment_amount: Mapped[Optional[decimal.Decimal]] = mapped_column(
        Numeric(100, 2)
    )
    confidentiality: Mapped[CaseConfidentiality] = mapped_column(
        Enum(*get_args(CaseConfidentiality))
    )
    confidential: Mapped[bool]

    duplicate_prevention_token: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    urgency: Mapped[Optional[str]]
    urgency_date_medium: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    urgency_date_high: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    preset_client: Mapped[bool] = mapped_column(default=False)
    prefix: Mapped[str]
    html_email_template: Mapped[Optional[str]]

    betrokkenen_cache: Mapped[dict] = mapped_column(postgresql.JSONB)
    requestor_v1_json: Mapped[dict] = mapped_column(postgresql.JSONB)
    assignee_v1_json: Mapped[dict] = mapped_column(postgresql.JSONB)
    coordinator_v1_json: Mapped[dict] = mapped_column(postgresql.JSONB)


class ZaakBetrokkenen(Base):
    """Table containing links to entities "involved" with a case

    For instance, its assignee, the requestor, etc."""

    __tablename__ = "zaak_betrokkenen"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    zaak_id: Mapped[Optional[int]] = mapped_column(
        BigInteger, ForeignKey("zaak.id")
    )

    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    betrokkene_type: Mapped[Optional[str]]
    betrokkene_id: Mapped[Optional[int]]
    gegevens_magazijn_id: Mapped[Optional[int]]

    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    subject_id: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=False, server_default=FetchedValue()
    )

    naam: Mapped[Optional[str]]
    rol: Mapped[Optional[str]]
    magic_string_prefix: Mapped[Optional[str]]
    verificatie: Mapped[Optional[str]]

    pip_authorized: Mapped[bool] = mapped_column(default=False)
    authorisation: Mapped[Optional[str]]

    bibliotheek_kenmerken_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )


ObjectDataAclGroupName = Literal["confidential", "public"]


class ObjectData(Base):
    """Object storage (including mirrors of cases)"""

    __tablename__ = "object_data"
    uuid: Mapped[UUID] = mapped_column(
        GUID, primary_key=True, server_default=FetchedValue()
    )
    object_class: Mapped[str]
    object_id: Mapped[Optional[int]]
    class_uuid: Mapped[Optional[UUID]] = mapped_column(GUID, unique=False)

    acl_groupname: Mapped[Optional[ObjectDataAclGroupName]] = mapped_column(
        Enum(*get_args(ObjectDataAclGroupName))
    )

    invalid: Mapped[bool] = mapped_column(default=False)
    properties: Mapped[str]
    text_vector: Mapped[Optional[str]] = mapped_column(postgresql.TSVECTOR)
    index_hstore: Mapped[Optional[dict]] = mapped_column(postgresql.HSTORE)
    date_created: Mapped[datetime.datetime] = mapped_column(
        UTCDateTime,
        server_default=sql.text("NOW()"),
    )
    date_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )


ObjectAclEntryScope = Literal["instance", "type"]


class ObjectAclEntry(Base):
    """ACL entries for object_data rows"""

    __tablename__ = "object_acl_entry"

    uuid: Mapped[UUID] = mapped_column(GUID, primary_key=True)

    object_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("object_data.uuid")
    )
    entity_type: Mapped[str]
    entity_id: Mapped[str]
    capability: Mapped[str]
    scope: Mapped[ObjectAclEntryScope] = mapped_column(
        Enum(*get_args(ObjectAclEntryScope))
    )
    groupname: Mapped[Optional[str]]


class Group(Base):
    """User groups"""

    __tablename__ = "groups"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    path: Mapped[list[int]] = mapped_column(postgresql.ARRAY(Integer))
    name: Mapped[Optional[str]]
    description: Mapped[Optional[str]]
    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    roles = relationship("Role", back_populates="parent_group")


class Role(Base):
    """User roles"""

    __tablename__ = "roles"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    parent_group_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("groups.id")
    )
    parent_group = relationship("Group", back_populates="roles", uselist=False)

    name: Mapped[Optional[str]]
    description: Mapped[Optional[str]]
    system_role: Mapped[Optional[bool]]
    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    permissions: Mapped[Optional[str]]


SubjectType = Literal["person", "company", "employee"]


class Subject(Base):
    """Users of the system"""

    __tablename__ = "subject"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    subject_type: Mapped[SubjectType] = mapped_column(
        Enum(*get_args(SubjectType)),
    )
    username: Mapped[str]

    properties: Mapped[dict] = mapped_column(JSONEncodedDict)
    settings: Mapped[dict] = mapped_column(JSONEncodedDict)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    role_ids: Mapped[list[int]] = mapped_column(postgresql.ARRAY(Integer))
    group_ids: Mapped[list[int]] = mapped_column(postgresql.ARRAY(Integer))
    nobody: Mapped[bool] = mapped_column(default=False)
    system: Mapped[bool] = mapped_column(default=False)

    related_custom_object_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object.id")
    )
    related_custom_object = relationship("CustomObject")


class ContactRelationship(Base):
    "Relations between contacts"

    __tablename__ = "contact_relationship_view"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    contact: Mapped[int]
    contact_uuid: Mapped[UUID] = mapped_column(GUID, unique=False)
    contact_type: Mapped[str]

    relation: Mapped[int]
    relation_uuid: Mapped[UUID] = mapped_column(GUID, unique=False)
    relation_type: Mapped[str]


class Logging(Base):
    """Log entries"""

    __tablename__ = "logging"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    zaak_id: Mapped[Optional[int]] = mapped_column(
        BigInteger, ForeignKey("zaak.id")
    )
    betrokkene_id: Mapped[Optional[str]]
    aanvrager_id: Mapped[Optional[str]]
    is_bericht: Mapped[Optional[int]]
    component: Mapped[Optional[str]]
    component_id: Mapped[int]
    seen: Mapped[Optional[int]]
    onderwerp: Mapped[Optional[str]]
    bericht: Mapped[Optional[str]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted_on: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    event_type: Mapped[Optional[str]]
    event_data: Mapped[Optional[dict]] = mapped_column(JSONEncodedDict)
    created_by: Mapped[Optional[str]]
    modified_by: Mapped[Optional[str]]
    deleted_by: Mapped[Optional[str]]
    created_for: Mapped[Optional[str]]
    created_by_name_cache: Mapped[Optional[str]]
    object_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("object_data.uuid")
    )
    restricted: Mapped[bool] = mapped_column(default=False)


class Message(Base):
    """Messages for users of the system"""

    __tablename__ = "message"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    message: Mapped[str]
    subject_id: Mapped[Optional[str]]
    logging_id: Mapped[int] = mapped_column(ForeignKey("logging.id"))
    is_read: Mapped[Optional[bool]] = mapped_column(default=False)
    is_archived: Mapped[bool] = mapped_column(default=False)

    created: Mapped[datetime.datetime] = mapped_column(
        UTCDateTime, server_default=FetchedValue()
    )


class BibliotheekCategorie(Base):
    """Catalog categories ("directories")"""

    __tablename__ = "bibliotheek_categorie"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    pid: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )
    naam: Mapped[Optional[str]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )


class Directory(Base):
    """Directory data"""

    __tablename__ = "directory"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str]
    case_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    original_name: Mapped[str]
    path: Mapped[list[int]] = mapped_column(postgresql.ARRAY(Integer))
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )


DocumentStatus = Literal["original", "copy", "replaced", "converted"]


class ScheduledJobs(Base):
    __tablename__ = "scheduled_jobs"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    task: Mapped[str] = mapped_column(String(255))
    scheduled_for: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    parameters: Mapped[Optional[str]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    schedule_type: Mapped[Optional[str]] = mapped_column(String(255))
    case_id: Mapped[Optional[int]] = mapped_column(
        BigInteger, ForeignKey("zaak.id")
    )
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )


class File(Base):
    """File data"""

    __tablename__ = "file"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    filestore_id: Mapped[int] = mapped_column(ForeignKey("filestore.id"))

    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=False, server_default=FetchedValue()
    )
    root_file_id: Mapped[Optional[int]]

    name: Mapped[str]
    extension: Mapped[str]

    version: Mapped[Optional[int]]
    case_id: Mapped[Optional[int]] = mapped_column(
        BigInteger, ForeignKey("zaak.id")
    )
    metadata_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("file_metadata.id")
    )
    subject_id: Mapped[Optional[str]]
    directory_id: Mapped[Optional[int]]

    creation_reason: Mapped[str]
    accepted: Mapped[bool] = mapped_column(default=False)

    rejection_reason: Mapped[Optional[str]]
    rejected_by_display_name: Mapped[Optional[str]]
    reject_to_queue: Mapped[Optional[bool]] = mapped_column(default=False)

    is_duplicate_name: Mapped[bool] = mapped_column(default=False)

    publish_pip: Mapped[bool] = mapped_column(default=False)
    publish_website: Mapped[bool] = mapped_column(default=False)

    date_created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    created_by: Mapped[str]
    date_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    modified_by: Mapped[Optional[str]]
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted_by: Mapped[Optional[str]]
    destroyed: Mapped[Optional[bool]] = mapped_column(default=False)
    scheduled_jobs_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("scheduled_jobs.id")
    )
    active_version: Mapped[bool] = mapped_column(default=False)
    is_duplicate_of: Mapped[Optional[int]]

    # queue(bool) potentially show this file in "intake"-queue, if false then always excluded.
    queue: Mapped[bool] = mapped_column(default=True)
    document_status: Mapped[DocumentStatus] = mapped_column(
        Enum(*get_args(DocumentStatus)),
        default="original",
    )
    generator: Mapped[Optional[str]]
    lock_timestamp: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    lock_subject_id: Mapped[Optional[UUID]] = mapped_column(GUID, unique=False)
    lock_subject_name: Mapped[Optional[str]]

    # shared is only used by MS365, its set to true to enable multi user edit
    # ("Shared locking")
    shared: Mapped[Optional[bool]]

    confidential: Mapped[bool] = mapped_column(default=False)
    search_term: Mapped[Optional[str]]
    search_index: Mapped[Optional[str]] = mapped_column(postgresql.TSVECTOR)

    object_type: Mapped[str]

    searchable_id: Mapped[int]
    search_order: Mapped[Optional[str]]

    intake_owner: Mapped[Optional[str]]

    intake_group_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("groups.id")
    )
    intake_role_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("roles.id")
    )

    intake_group = relationship("Group", foreign_keys=[intake_group_id])
    intake_role = relationship("Role", foreign_keys=[intake_role_id])

    skip_intake: Mapped[bool] = mapped_column(default=False)


class Filestore(Base):
    """File storage reference"""

    __tablename__ = "filestore"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    original_name: Mapped[str]
    size: Mapped[int]
    mimetype: Mapped[str]
    md5: Mapped[str]

    date_created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    storage_location: Mapped[Optional[list[str]]] = mapped_column(
        postgresql.ARRAY(String)
    )
    is_archivable: Mapped[bool]
    virus_scan_status: Mapped[str]
    thumbnail_uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=False, server_default=FetchedValue()
    )


class BibliotheekSjabloon(Base):
    """Document templates for use in case types"""

    __tablename__ = "bibliotheek_sjablonen"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    bibliotheek_categorie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    naam: Mapped[Optional[str]]
    help: Mapped[Optional[str]]
    template_external_name: Mapped[Optional[str]]

    filestore_id: Mapped[int] = mapped_column(ForeignKey("filestore.id"))
    interface_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("interface.id")
    )

    magic_strings = relationship("BibliotheekSjabloonMagicString")


class BibliotheekSjabloonMagicString(Base):
    """Cache table for magic strings used in document templates"""

    __tablename__ = "bibliotheek_sjablonen_magic_string"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    bibliotheek_sjablonen_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_sjablonen.id")
    )
    value: Mapped[Optional[str]]


class BibliotheekNotificatieKenmerk(Base):
    """Association table for attachments to email templates"""

    __tablename__ = "bibliotheek_notificatie_kenmerk"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    bibliotheek_notificatie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_notificaties.id")
    )
    bibliotheek_kenmerken_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )


class BibliotheekNotificatie(Base):
    """Email templates for use in case types"""

    __tablename__ = "bibliotheek_notificaties"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    bibliotheek_categorie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    label: Mapped[Optional[str]]
    subject: Mapped[Optional[str]]
    message: Mapped[Optional[str]]
    sender: Mapped[Optional[str]]
    sender_address: Mapped[Optional[str]]
    search_term: Mapped[Optional[str]]

    attachments = relationship(
        "BibliotheekKenmerk",
        secondary="bibliotheek_notificatie_kenmerk",
        back_populates="attachment_to",
    )


BibliotheekKenmerkValueType = Literal[
    "address_v2",
    "appointment_v2",
    "appointment",
    "bag_adres",
    "bag_adressen",
    "bag_openbareruimte",
    "bag_openbareruimtes",
    "bag_straat_adres",
    "bag_straat_adressen",
    "bankaccount",
    "calendar_supersaas",
    "calendar",
    "checkbox",
    "date",
    "email",
    "file",
    "geojson",
    "geolatlon",
    "googlemaps",
    "image_from_url",
    "numeric",
    "option",
    "relationship",
    "richtext",
    "select",
    "subject",
    "text_uc",
    "text",
    "textarea",
    "url",
    "valuta",
    "valutaex",
    "valutaex21",
    "valutaex6",
    "valutain",
    "valutain21",
    "valutain6",
]


class BibliotheekKenmerk(Base):
    """Attributes for use in cases or object types"""

    __tablename__ = "bibliotheek_kenmerken"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    bibliotheek_categorie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    naam: Mapped[Optional[str]]
    naam_public: Mapped[str]
    magic_string: Mapped[Optional[str]]

    help: Mapped[str]
    description: Mapped[Optional[str]]

    value_type: Mapped[BibliotheekKenmerkValueType] = mapped_column(
        Enum(*get_args(BibliotheekKenmerkValueType))
    )
    value_default: Mapped[str]

    document_categorie: Mapped[Optional[str]]
    file_metadata_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("file_metadata.id")
    )

    type_multiple: Mapped[bool] = mapped_column(default=False)
    properties: Mapped[Optional[dict]] = mapped_column(JSONEncodedDict)
    search_term: Mapped[Optional[str]]
    search_order: Mapped[Optional[str]]
    version: Mapped[int]

    relationship_type: Mapped[Optional[str]]
    relationship_name: Mapped[Optional[str]]
    relationship_uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=False
    )

    attachment_to = relationship(
        "BibliotheekNotificatie",
        secondary="bibliotheek_notificatie_kenmerk",
        back_populates="attachments",
    )


class BibliotheekKenmerkenValues(Base):
    """Values of an attribute(BibliotheekKenmerk) of type option or checkbox"""

    __tablename__ = "bibliotheek_kenmerken_values"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    bibliotheek_kenmerken_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )
    value: Mapped[Optional[str]]
    active: Mapped[bool]
    sort_order: Mapped[int]


class FileMetaData(Base):
    """FileMetaData of an attribute(BibliotheekKenmerk) of type file"""

    __tablename__ = "file_metadata"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    description: Mapped[Optional[str]]
    trust_level: Mapped[str]
    origin: Mapped[Optional[str]]
    document_category: Mapped[Optional[str]]
    origin_date: Mapped[Optional[datetime.date]] = mapped_column(Date)
    pronom_format: Mapped[Optional[str]]
    appearance: Mapped[Optional[str]]
    structure: Mapped[Optional[str]]
    document_source: Mapped[Optional[str]]


class ObjectBibliotheekEntry(Base):
    """Catalog entries for object types"""

    __tablename__ = "object_bibliotheek_entry"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    bibliotheek_categorie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )

    object_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("object_data.uuid")
    )
    name: Mapped[str]


class Zaaktype(Base):
    """Case types"""

    __tablename__ = "zaaktype"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    bibliotheek_categorie_id: Mapped[int] = mapped_column(
        ForeignKey("bibliotheek_categorie.id")
    )

    zaaktype_node_id: Mapped[int] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    version: Mapped[Optional[int]]

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    active: Mapped[bool]

    zaaktype_nodes = relationship(
        "ZaaktypeNode",
        foreign_keys="[ZaaktypeNode.zaaktype_id]",
        back_populates="zaaktype",
    )
    current_zaaktype_node = relationship(
        "ZaaktypeNode", foreign_keys=[zaaktype_node_id]
    )


class ZaaktypeNode(Base):
    """Case type versions"""

    __tablename__ = "zaaktype_node"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    zaaktype_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype.id")
    )
    zaaktype_definitie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_definitie.id")
    )

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)

    titel: Mapped[Optional[str]]
    code: Mapped[Optional[str]]
    trigger: Mapped[Optional[str]]
    version: Mapped[Optional[int]]
    active: Mapped[Optional[int]]
    properties: Mapped[dict] = mapped_column(JSONEncodedDict)
    is_public: Mapped[Optional[bool]]
    zaaktype_trefwoorden: Mapped[Optional[str]]
    zaaktype_omschrijving: Mapped[Optional[str]]

    webform_toegang: Mapped[Optional[int]]
    webform_authenticatie: Mapped[Optional[str]]
    adres_relatie: Mapped[Optional[str]]
    aanvrager_hergebruik: Mapped[Optional[int]]
    automatisch_aanvragen: Mapped[Optional[int]]
    automatisch_behandelen: Mapped[Optional[int]]
    toewijzing_zaakintake: Mapped[Optional[int]]
    toelichting: Mapped[Optional[str]]
    online_betaling: Mapped[Optional[int]]
    adres_andere_locatie: Mapped[Optional[int]]
    adres_aanvrager: Mapped[Optional[int]]
    adres_geojson: Mapped[Optional[bool]] = mapped_column(default=False)

    bedrijfid_wijzigen: Mapped[Optional[int]]
    zaaktype_vertrouwelijk: Mapped[Optional[int]]
    extra_relaties_in_aanvraag: Mapped[Optional[bool]]
    contact_info_intake: Mapped[bool]
    prevent_pip: Mapped[bool] = mapped_column(default=False)
    contact_info_email_required: Mapped[Optional[bool]] = mapped_column(
        default=False
    )
    contact_info_phone_required: Mapped[Optional[bool]] = mapped_column(
        default=False
    )
    contact_info_mobile_phone_required: Mapped[Optional[bool]] = mapped_column(
        default=False
    )
    moeder_zaaktype_id: Mapped[Optional[int]]

    logging_id: Mapped[Optional[int]] = mapped_column(ForeignKey("logging.id"))

    zaaktype = relationship(
        "Zaaktype", foreign_keys=[zaaktype_id], back_populates="zaaktype_nodes"
    )

    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    v0_json: Mapped[dict] = mapped_column(postgresql.JSONB)


class ZaaktypeStatus(Base):
    """A single phase in a case type version"""

    __tablename__ = "zaaktype_status"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    zaaktype_node_id: Mapped[int] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )

    status: Mapped[Optional[int]]
    status_type: Mapped[Optional[str]]
    termijn: Mapped[int]
    phase_id: Mapped[Optional[int]]

    naam: Mapped[Optional[str]]
    fase: Mapped[Optional[str]]
    ou_id: Mapped[Optional[int]]
    role_id: Mapped[Optional[int]]
    checklist: Mapped[Optional[int]]
    role_set: Mapped[Optional[int]]


class ZaaktypeKenmerk(Base):
    """Attributes, as used in case type versions"""

    __tablename__ = "zaaktype_kenmerken"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    zaaktype_node_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    zaak_status_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )

    bibliotheek_kenmerken_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )
    object_id: Mapped[Optional[UUID]] = mapped_column(
        GUID, ForeignKey("object_data.uuid")
    )

    value_mandatory: Mapped[bool]
    label: Mapped[Optional[str]]
    help: Mapped[Optional[str]]
    pip: Mapped[Optional[int]]
    zaakinformatie_view: Mapped[Optional[int]] = mapped_column(default=1)
    bag_zaakadres: Mapped[Optional[int]]
    value_default: Mapped[Optional[str]]
    pip_can_change: Mapped[Optional[bool]]
    publish_public: Mapped[Optional[int]]
    referential: Mapped[bool]

    is_systeemkenmerk: Mapped[Optional[bool]] = mapped_column(default=False)
    required_permissions: Mapped[Optional[dict]] = mapped_column(
        JSONEncodedDict
    )
    version: Mapped[Optional[int]]
    help_extern: Mapped[Optional[str]]
    object_metadata: Mapped[dict] = mapped_column(JSONEncodedDict, default={})
    label_multiple: Mapped[Optional[str]]
    properties: Mapped[Optional[dict]] = mapped_column(JSONEncodedDict)
    is_group: Mapped[bool] = mapped_column(default=False)


class ZaaktypeDocumentKenmerkenMap(Base):
    """
    Represents the materialized view `zaaktype_document_kenmerken_map`, which
    contains the zaaktype_kenmerken_id (and some other useful fields) for the
    first instance of a document field in a case type.
    """

    __tablename__ = "zaaktype_document_kenmerken_map"

    zaaktype_node_id: Mapped[int] = mapped_column(primary_key=True)
    bibliotheek_kenmerken_id: Mapped[int] = mapped_column(primary_key=True)

    case_document_id: Mapped[int]
    case_document_uuid: Mapped[UUID] = mapped_column(GUID)

    referential: Mapped[bool]
    magic_string: Mapped[str]
    name: Mapped[str]
    public_name: Mapped[str]

    show_on_pip: Mapped[bool]
    show_on_website: Mapped[bool]


class ZaaktypeSjabloon(Base):
    """Document templates, as used in case type versions"""

    __tablename__ = "zaaktype_sjablonen"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    zaaktype_node_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    zaak_status_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )

    bibliotheek_sjablonen_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_sjablonen.id")
    )

    bibliotheek_kenmerken_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )
    automatisch_genereren: Mapped[Optional[int]]  # TODO Make boolean
    target_format: Mapped[Optional[str]]
    help: Mapped[Optional[str]]
    custom_filename: Mapped[Optional[str]]
    label: Mapped[Optional[str]]
    allow_edit: Mapped[Optional[bool]]


class ZaaktypeNotificatie(Base):
    """Email templates, as used in case type versions"""

    __tablename__ = "zaaktype_notificatie"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    zaaktype_node_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    zaak_status_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )
    bibliotheek_notificatie_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bibliotheek_notificaties.id"),
        name="bibliotheek_notificaties_id",
    )

    label: Mapped[Optional[str]]
    rcpt: Mapped[Optional[str]]
    onderwerp: Mapped[Optional[str]]
    bericht: Mapped[Optional[str]]
    intern_block: Mapped[Optional[int]]
    email: Mapped[Optional[str]]
    behandelaar: Mapped[Optional[str]]
    automatic: Mapped[Optional[int]]
    cc: Mapped[Optional[str]]
    bcc: Mapped[Optional[str]]
    betrokkene_role: Mapped[Optional[str]]


ZaaktypeRelatieCreationStyle = Literal["background", "form"]


class ZaaktypeRelatie(Base):
    """Casetypes related to a casetype"""

    __tablename__ = "zaaktype_relatie"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    zaaktype_node_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    zaaktype_status_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )
    relatie_zaaktype_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype.id")
    )
    relatie_type: Mapped[Optional[str]]

    eigenaar_type: Mapped[str] = mapped_column(default="aanvrager")

    start_delay: Mapped[Optional[str]] = mapped_column(String(255))
    status: Mapped[bool] = mapped_column(default=False)

    kopieren_kenmerken: Mapped[Optional[int]]
    ou_id: Mapped[Optional[int]] = mapped_column(ForeignKey("groups.id"))
    role_id: Mapped[Optional[int]] = mapped_column(ForeignKey("roles.id"))
    automatisch_behandelen: Mapped[Optional[bool]]
    required: Mapped[Optional[str]] = mapped_column(String(12))

    subject_role: Mapped[list[str]] = mapped_column(postgresql.ARRAY(String))
    copy_subject_role: Mapped[Optional[bool]] = mapped_column(default=False)

    betrokkene_authorized: Mapped[Optional[bool]]
    betrokkene_notify: Mapped[Optional[bool]]
    betrokkene_id: Mapped[Optional[str]]
    betrokkene_role: Mapped[Optional[str]]
    betrokkene_role_set: Mapped[Optional[str]]
    betrokkene_prefix: Mapped[Optional[str]]
    eigenaar_role: Mapped[Optional[str]]
    eigenaar_id: Mapped[Optional[str]]

    show_in_pip: Mapped[bool] = mapped_column(default=False)
    pip_label: Mapped[Optional[str]]
    copy_related_cases: Mapped[bool] = mapped_column(default=False)
    copy_related_objects: Mapped[bool] = mapped_column(default=False)
    copy_selected_attributes: Mapped[dict] = mapped_column(postgresql.HSTORE)
    creation_style: Mapped[ZaaktypeRelatieCreationStyle] = mapped_column(
        Enum(*get_args(ZaaktypeRelatieCreationStyle))
    )


class ZaaktypeResultaten(Base):
    __tablename__ = "zaaktype_resultaten"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    zaaktype_node_id: Mapped[Optional[int]]
    zaaktype_status_id: Mapped[Optional[int]]
    resultaat: Mapped[Optional[str]]
    ingang: Mapped[Optional[str]]
    bewaartermijn: Mapped[Optional[int]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    dossiertype: Mapped[Optional[str]] = mapped_column(String(50))
    label: Mapped[Optional[str]]
    selectielijst: Mapped[Optional[str]]
    archiefnominatie: Mapped[Optional[str]]
    comments: Mapped[Optional[str]]
    external_reference: Mapped[Optional[str]]
    trigger_archival: Mapped[bool]
    selectielijst_brondatum: Mapped[Optional[datetime.date]] = mapped_column(
        Date
    )
    selectielijst_einddatum: Mapped[Optional[datetime.date]] = mapped_column(
        Date
    )

    properties: Mapped[dict] = mapped_column(JSONEncodedDict)
    standaard_keuze: Mapped[bool]


class ZaaktypeStandaardBetrokkenen(Base):
    """Involved entities related to a casetype"""

    __tablename__ = "zaaktype_standaard_betrokkenen"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    zaaktype_node_id: Mapped[int] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    zaak_status_id: Mapped[int] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )
    betrokkene_type: Mapped[str]
    betrokkene_identifier: Mapped[str]
    naam: Mapped[str]
    rol: Mapped[str]
    magic_string_prefix: Mapped[Optional[str]]
    gemachtigd: Mapped[bool]
    notify: Mapped[bool]


class ZaaktypeRegel(Base):
    __tablename__ = "zaaktype_regel"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    zaaktype_node_id: Mapped[Optional[int]]
    zaak_status_id: Mapped[Optional[int]]
    naam: Mapped[Optional[str]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    settings: Mapped[Optional[str]]
    active: Mapped[Optional[bool]]
    is_group: Mapped[bool]


class ResultPreservationTerms(Base):
    __tablename__ = "result_preservation_terms"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    code: Mapped[int]
    label: Mapped[str]
    unit: Mapped[str]
    unit_amount: Mapped[int]


CaseAuthorisationEntityType = Literal["position", "user"]
CaseAuthorisationEntityScope = Literal["instance"]
CaseAuthorizationCapability = Literal["search", "read", "write", "manage"]


class CaseAuthorisation(Base):
    """Case-level authorisation rules"""

    __tablename__ = "zaak_authorisation"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    capability: Mapped[CaseAuthorizationCapability] = mapped_column(
        Enum(*get_args(CaseAuthorizationCapability))
    )
    zaak_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    entity_id: Mapped[str]
    entity_type: Mapped[CaseAuthorisationEntityType] = mapped_column(
        Enum(*get_args(CaseAuthorisationEntityType))
    )
    scope: Mapped[CaseAuthorisationEntityScope] = mapped_column(
        Enum(*get_args(CaseAuthorisationEntityScope))
    )


class Interface(Base):
    """Contains configuration of integrations with external systems"""

    __tablename__ = "interface"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    name: Mapped[str]
    active: Mapped[bool]
    multiple: Mapped[bool]
    module: Mapped[str]
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )

    interface_config: Mapped[dict] = mapped_column(JSONEncodedDict)
    internal_config: Mapped[dict] = mapped_column(postgresql.JSONB)

    case_type_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype.id")
    )
    objecttype_id: Mapped[Optional[UUID]] = mapped_column(
        GUID, ForeignKey("object_data.uuid")
    )


class FileCaseDocument(Base):
    __tablename__ = "file_case_document"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    file_id: Mapped[int] = mapped_column(ForeignKey("file.id"))
    magic_string: Mapped[str] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.magic_string")
    )
    bibliotheek_kenmerken_id: Mapped[int] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )
    case_id: Mapped[int] = mapped_column(ForeignKey("zaak.id"))


class Bedrijf(Base):
    __tablename__ = "bedrijf"

    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    object_type: Mapped[Optional[str]]
    handelsnaam: Mapped[Optional[str]]
    email: Mapped[Optional[str]] = mapped_column(String(128))
    telefoonnummer: Mapped[Optional[str]] = mapped_column(String(10))
    werkzamepersonen: Mapped[Optional[int]]

    dossiernummer: Mapped[Optional[str]] = mapped_column(String(8))
    subdossiernummer: Mapped[Optional[str]] = mapped_column(String(4))
    hoofdvestiging_dossiernummer: Mapped[Optional[str]] = mapped_column(
        String(8)
    )
    hoofdvestiging_subdossiernummer: Mapped[Optional[str]] = mapped_column(
        String(4)
    )

    vorig_dossiernummer: Mapped[Optional[str]] = mapped_column(String(8))
    vorig_subdossiernummer: Mapped[Optional[str]] = mapped_column(String(4))

    rechtsvorm: Mapped[Optional[int]] = mapped_column(SmallInteger)
    kamernummer: Mapped[Optional[int]] = mapped_column(SmallInteger)
    faillisement: Mapped[Optional[int]] = mapped_column(SmallInteger)
    surseance: Mapped[Optional[int]] = mapped_column(SmallInteger)

    contact_naam: Mapped[Optional[str]] = mapped_column(String(64))
    contact_aanspreektitel: Mapped[Optional[str]] = mapped_column(String(45))
    contact_voorvoegsel: Mapped[Optional[str]] = mapped_column(String(8))
    contact_voorletters: Mapped[Optional[str]] = mapped_column(String(19))
    contact_geslachtsnaam: Mapped[Optional[str]] = mapped_column(String(95))
    contact_geslachtsaanduiding: Mapped[Optional[str]] = mapped_column(
        String(1)
    )

    vestiging_adres: Mapped[Optional[str]]
    vestiging_straatnaam: Mapped[Optional[str]]
    vestiging_huisnummer: Mapped[Optional[int]] = mapped_column(BigInteger)
    vestiging_huisnummertoevoeging: Mapped[Optional[str]]
    vestiging_postcodewoonplaats: Mapped[Optional[str]]
    vestiging_postcode: Mapped[Optional[str]] = mapped_column(String(6))
    vestiging_woonplaats: Mapped[Optional[str]]
    vestiging_latlong: Mapped[Optional[str]]

    correspondentie_straatnaam: Mapped[Optional[str]]
    correspondentie_huisnummer: Mapped[Optional[int]]
    correspondentie_huisnummertoevoeging: Mapped[Optional[str]]
    correspondentie_postcodewoonplaats: Mapped[Optional[str]]
    correspondentie_postcode: Mapped[Optional[str]]
    correspondentie_woonplaats: Mapped[Optional[str]]
    correspondentie_adres: Mapped[Optional[str]]

    hoofdactiviteitencode: Mapped[Optional[int]]
    nevenactiviteitencode1: Mapped[Optional[int]]
    nevenactiviteitencode2: Mapped[Optional[int]]

    authenticated: Mapped[Optional[int]] = mapped_column(SmallInteger)
    authenticatedby: Mapped[Optional[str]]
    fulldossiernummer: Mapped[Optional[str]]
    import_datum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted_on: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    verblijfsobject_id: Mapped[Optional[str]] = mapped_column(String(16))
    system_of_record: Mapped[Optional[str]] = mapped_column(String(32))
    system_of_record_id: Mapped[Optional[int]] = mapped_column(BigInteger)
    vestigingsnummer: Mapped[Optional[int]] = mapped_column(BigInteger)
    vestiging_huisletter: Mapped[Optional[str]]
    correspondentie_huisletter: Mapped[Optional[str]]
    vestiging_adres_buitenland1: Mapped[Optional[str]]
    vestiging_adres_buitenland2: Mapped[Optional[str]]
    vestiging_adres_buitenland3: Mapped[Optional[str]]
    vestiging_landcode: Mapped[Optional[int]] = mapped_column(default=6030)

    correspondentie_adres_buitenland1: Mapped[Optional[str]]
    correspondentie_adres_buitenland2: Mapped[Optional[str]]
    correspondentie_adres_buitenland3: Mapped[Optional[str]]
    correspondentie_landcode: Mapped[Optional[int]] = mapped_column(
        default=6030
    )

    rsin: Mapped[Optional[int]] = mapped_column(BigInteger)
    oin: Mapped[Optional[int]] = mapped_column(BigInteger)

    date_founded: Mapped[Optional[datetime.date]] = mapped_column(Date)
    date_registration: Mapped[Optional[datetime.date]] = mapped_column(Date)
    date_ceased: Mapped[Optional[datetime.date]] = mapped_column(Date)

    main_activity: Mapped[dict] = mapped_column(
        postgresql.JSONB, server_default="{}"
    )
    secondairy_activities: Mapped[dict] = mapped_column(
        postgresql.JSONB, server_default="[]"
    )

    search_term: Mapped[Optional[str]]
    search_order: Mapped[Optional[str]]

    related_custom_object_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object.id")
    )
    related_custom_object = relationship("CustomObject")
    preferred_contact_channel: Mapped[Optional[PreferredContactChannel]] = (
        mapped_column(
            Enum(
                *get_args(PreferredContactChannel),
                name="preferred_contact_channel",
            )
        )
    )

    vestiging_bag_id: Mapped[Optional[int]] = mapped_column(BigInteger)


class NatuurlijkPersoon(Base):
    __tablename__ = "natuurlijk_persoon"

    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    object_type: Mapped[Optional[str]]
    search_term: Mapped[Optional[str]]
    search_order: Mapped[Optional[str]]
    burgerservicenummer: Mapped[Optional[str]] = mapped_column(String(9))
    a_nummer: Mapped[Optional[str]] = mapped_column(String(10))
    voorletters: Mapped[Optional[str]] = mapped_column(String(50))
    voornamen: Mapped[Optional[str]]
    geslachtsnaam: Mapped[Optional[str]]
    voorvoegsel: Mapped[Optional[str]] = mapped_column(String(50))
    geslachtsaanduiding: Mapped[Optional[str]] = mapped_column(String(3))
    nationaliteitscode1: Mapped[Optional[int]] = mapped_column(SmallInteger)
    nationaliteitscode2: Mapped[Optional[int]] = mapped_column(SmallInteger)
    nationaliteitscode3: Mapped[Optional[int]] = mapped_column(SmallInteger)
    geboorteplaats: Mapped[Optional[str]]
    geboorteland: Mapped[Optional[str]]
    geboortedatum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    aanhef_aanschrijving: Mapped[Optional[str]] = mapped_column(String(10))
    voorletters_aanschrijving: Mapped[Optional[str]] = mapped_column(
        String(20)
    )
    voornamen_aanschrijving: Mapped[Optional[str]] = mapped_column(String(200))
    naam_aanschrijving: Mapped[Optional[str]]
    voorvoegsel_aanschrijving: Mapped[Optional[str]] = mapped_column(
        String(50)
    )
    burgerlijke_staat: Mapped[Optional[str]] = mapped_column(String(1))
    indicatie_geheim: Mapped[Optional[str]] = mapped_column(String(1))
    land_waarnaar_vertrokken: Mapped[Optional[int]] = mapped_column(
        SmallInteger
    )
    import_datum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    adres_id: Mapped[Optional[int]] = mapped_column(Integer)

    authenticated: Mapped[Optional[bool]] = mapped_column(default=False)
    authenticatedby: Mapped[Optional[str]]

    deleted_on: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    verblijfsobject_id: Mapped[Optional[str]] = mapped_column(String(16))
    datum_overlijden: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    aanduiding_naamgebruik: Mapped[Optional[str]] = mapped_column(String(1))
    onderzoek_persoon: Mapped[Optional[bool]]
    onderzoek_huwelijk: Mapped[Optional[bool]]
    onderzoek_overlijden: Mapped[Optional[bool]]
    onderzoek_verblijfplaats: Mapped[Optional[bool]]
    partner_a_nummer: Mapped[Optional[str]] = mapped_column(String(50))
    partner_burgerservicenummer: Mapped[Optional[str]] = mapped_column(
        String(50)
    )
    partner_voorvoegsel: Mapped[Optional[str]] = mapped_column(String(50))
    partner_geslachtsnaam: Mapped[Optional[str]] = mapped_column(String(50))
    datum_huwelijk: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    datum_huwelijk_ontbinding: Mapped[Optional[datetime.datetime]] = (
        mapped_column(UTCDateTime)
    )
    in_gemeente: Mapped[Optional[bool]]
    landcode: Mapped[int] = mapped_column(default=6030)
    naamgebruik: Mapped[Optional[str]]
    active: Mapped[bool] = mapped_column(default=True)
    adellijke_titel: Mapped[Optional[str]]

    related_custom_object_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("custom_object.id")
    )
    related_custom_object = relationship("CustomObject")

    preferred_contact_channel: Mapped[Optional[PreferredContactChannel]] = (
        mapped_column(
            Enum(
                *get_args(PreferredContactChannel),
                name="preferred_contact_channel",
            )
        )
    )

    surname: Mapped[Optional[str]]
    persoonsnummer: Mapped[Optional[str]] = mapped_column(String(10))


AdresFunctie = Literal["W", "B"]


class Adres(Base):
    __tablename__ = "adres"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    straatnaam: Mapped[Optional[str]]
    huisnummer: Mapped[Optional[int]] = mapped_column(BigInteger)
    huisletter: Mapped[Optional[str]] = mapped_column(String(1))
    huisnummertoevoeging: Mapped[Optional[str]]
    nadere_aanduiding: Mapped[Optional[str]] = mapped_column(String(35))
    postcode: Mapped[Optional[str]] = mapped_column(String(6))

    woonplaats: Mapped[Optional[str]]
    gemeentedeel: Mapped[Optional[str]]
    functie_adres: Mapped[AdresFunctie] = mapped_column(
        Enum(*get_args(AdresFunctie))
    )
    datum_aanvang_bewoning: Mapped[Optional[datetime.date]] = mapped_column(
        Date
    )
    woonplaats_id: Mapped[Optional[str]] = mapped_column(String(32))

    gemeente_code: Mapped[Optional[int]] = mapped_column(SmallInteger)
    hash: Mapped[Optional[str]] = mapped_column(String(32))
    import_datum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted_on: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    adres_buitenland1: Mapped[Optional[str]]
    adres_buitenland2: Mapped[Optional[str]]
    adres_buitenland3: Mapped[Optional[str]]
    landcode: Mapped[Optional[int]] = mapped_column(default=6030)
    natuurlijk_persoon_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey(NatuurlijkPersoon.id)
    )
    bag_id: Mapped[Optional[str]]
    geo_lat_long: Mapped[Optional[str]]


class ContactData(Base):
    """Contact data in the system"""

    __tablename__ = "contact_data"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gegevens_magazijn_id: Mapped[Optional[int]]
    betrokkene_type: Mapped[Optional[int]]
    mobiel: Mapped[Optional[str]] = mapped_column(String(255))
    telefoonnummer: Mapped[Optional[str]] = mapped_column(String(255))
    email: Mapped[Optional[str]] = mapped_column(String(255))
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    note: Mapped[Optional[str]]


class GmNatuurlijkPersoon(Base):
    """Snapshot of citizens in the system"""

    __tablename__ = "gm_natuurlijk_persoon"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gegevens_magazijn_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("natuurlijk_persoon.id")
    )
    betrokkene_type: Mapped[Optional[int]]

    burgerservicenummer: Mapped[Optional[str]] = mapped_column(String(9))
    a_nummer: Mapped[Optional[str]] = mapped_column(String(10))
    voorletters: Mapped[Optional[str]] = mapped_column(String(50))
    voornamen: Mapped[Optional[str]]
    geslachtsnaam: Mapped[Optional[str]]
    voorvoegsel: Mapped[Optional[str]] = mapped_column(String(50))
    geslachtsaanduiding: Mapped[Optional[str]] = mapped_column(String(3))
    nationaliteitscode1: Mapped[Optional[int]] = mapped_column(SmallInteger)
    nationaliteitscode2: Mapped[Optional[int]] = mapped_column(SmallInteger)
    nationaliteitscode3: Mapped[Optional[int]] = mapped_column(SmallInteger)
    geboorteplaats: Mapped[Optional[str]]
    geboorteland: Mapped[Optional[str]]
    geboortedatum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    aanhef_aanschrijving: Mapped[Optional[str]] = mapped_column(String(10))
    voorletters_aanschrijving: Mapped[Optional[str]] = mapped_column(
        String(20)
    )
    voornamen_aanschrijving: Mapped[Optional[str]] = mapped_column(String(200))
    naam_aanschrijving: Mapped[Optional[str]]
    voorvoegsel_aanschrijving: Mapped[Optional[str]] = mapped_column(
        String(50)
    )
    burgerlijke_staat: Mapped[Optional[str]] = mapped_column(String(1))
    indicatie_geheim: Mapped[Optional[str]] = mapped_column(String(1))
    import_datum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    adres_id: Mapped[Optional[int]] = mapped_column(ForeignKey("gm_adres.id"))

    authenticated: Mapped[Optional[int]] = mapped_column(SmallInteger)

    authenticatedby: Mapped[Optional[str]]

    verblijfsobject_id: Mapped[Optional[str]] = mapped_column(String(16))
    datum_overlijden: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    aanduiding_naamgebruik: Mapped[Optional[str]] = mapped_column(String(1))
    onderzoek_persoon: Mapped[Optional[bool]]
    onderzoek_huwelijk: Mapped[Optional[bool]]
    onderzoek_overlijden: Mapped[Optional[bool]]
    onderzoek_verblijfplaats: Mapped[Optional[bool]]
    partner_a_nummer: Mapped[Optional[str]] = mapped_column(String(50))
    partner_burgerservicenummer: Mapped[Optional[str]] = mapped_column(
        String(50)
    )
    partner_voorvoegsel: Mapped[Optional[str]] = mapped_column(String(50))
    partner_geslachtsnaam: Mapped[Optional[str]] = mapped_column(String(50))
    datum_huwelijk: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    datum_huwelijk_ontbinding: Mapped[Optional[datetime.datetime]] = (
        mapped_column(UTCDateTime)
    )
    landcode: Mapped[int] = mapped_column(default=6030)
    naamgebruik: Mapped[Optional[str]]
    adellijke_titel: Mapped[Optional[str]]


class GmAdres(Base):
    """Snapshot of addresses in the system"""

    __tablename__ = "gm_adres"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    straatnaam: Mapped[Optional[str]]
    huisnummer: Mapped[Optional[int]] = mapped_column(BigInteger)
    huisletter: Mapped[Optional[str]] = mapped_column(String(1))
    huisnummertoevoeging: Mapped[Optional[str]]
    nadere_aanduiding: Mapped[Optional[str]] = mapped_column(String(35))
    postcode: Mapped[Optional[str]] = mapped_column(String(6))

    woonplaats: Mapped[Optional[str]]
    gemeentedeel: Mapped[Optional[str]]
    functie_adres: Mapped[str] = mapped_column(String(1))
    datum_aanvang_bewoning: Mapped[Optional[datetime.date]] = mapped_column(
        Date
    )
    woonplaats_id: Mapped[Optional[str]] = mapped_column(String(32))

    gemeente_code: Mapped[Optional[int]] = mapped_column(SmallInteger)
    hash: Mapped[Optional[str]] = mapped_column(String(32))
    import_datum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted_on: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    adres_buitenland1: Mapped[Optional[str]]
    adres_buitenland2: Mapped[Optional[str]]
    adres_buitenland3: Mapped[Optional[str]]
    landcode: Mapped[Optional[int]] = mapped_column(default=6030)
    natuurlijk_persoon_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("gm_natuurlijk_persoon.id")
    )


class GmBedrijf(Base):
    __tablename__ = "gm_bedrijf"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gegevens_magazijn_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bedrijf.id")
    )
    handelsnaam: Mapped[Optional[str]]
    email: Mapped[Optional[str]] = mapped_column(String(128))
    telefoonnummer: Mapped[Optional[str]] = mapped_column(String(10))
    werkzamepersonen: Mapped[Optional[int]]

    dossiernummer: Mapped[Optional[str]] = mapped_column(String(8))
    subdossiernummer: Mapped[Optional[str]] = mapped_column(String(4))
    hoofdvestiging_dossiernummer: Mapped[Optional[str]] = mapped_column(
        String(8)
    )
    hoofdvestiging_subdossiernummer: Mapped[Optional[str]] = mapped_column(
        String(4)
    )

    vorig_dossiernummer: Mapped[Optional[str]] = mapped_column(String(8))

    vorig_subdossiernummer: Mapped[Optional[str]] = mapped_column(String(4))

    rechtsvorm: Mapped[Optional[int]] = mapped_column(SmallInteger)
    kamernummer: Mapped[Optional[int]] = mapped_column(SmallInteger)
    faillisement: Mapped[Optional[int]] = mapped_column(SmallInteger)
    surseance: Mapped[Optional[int]] = mapped_column(SmallInteger)

    contact_naam: Mapped[Optional[str]] = mapped_column(String(64))
    contact_aanspreektitel: Mapped[Optional[str]] = mapped_column(String(45))
    contact_voorvoegsel: Mapped[Optional[str]] = mapped_column(String(8))
    contact_voorletters: Mapped[Optional[str]] = mapped_column(String(19))
    contact_geslachtsnaam: Mapped[Optional[str]] = mapped_column(String(95))
    contact_geslachtsaanduiding: Mapped[Optional[str]] = mapped_column(
        String(1)
    )

    vestiging_adres: Mapped[Optional[str]]
    vestiging_straatnaam: Mapped[Optional[str]]
    vestiging_huisnummer: Mapped[Optional[int]] = mapped_column(BigInteger)
    vestiging_huisnummertoevoeging: Mapped[Optional[str]]
    vestiging_postcodewoonplaats: Mapped[Optional[str]]
    vestiging_postcode: Mapped[Optional[str]] = mapped_column(String(6))
    vestiging_woonplaats: Mapped[Optional[str]]

    correspondentie_straatnaam: Mapped[Optional[str]]
    correspondentie_huisnummer: Mapped[Optional[int]]
    correspondentie_huisnummertoevoeging: Mapped[Optional[str]]
    correspondentie_postcodewoonplaats: Mapped[Optional[str]]
    correspondentie_postcode: Mapped[Optional[str]]
    correspondentie_woonplaats: Mapped[Optional[str]]
    correspondentie_adres: Mapped[Optional[str]]

    hoofdactiviteitencode: Mapped[Optional[int]]
    nevenactiviteitencode1: Mapped[Optional[int]]
    nevenactiviteitencode2: Mapped[Optional[int]]

    authenticated: Mapped[Optional[int]] = mapped_column(SmallInteger)
    authenticatedby: Mapped[Optional[str]]
    import_datum: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    verblijfsobject_id: Mapped[Optional[str]] = mapped_column(String(16))
    system_of_record: Mapped[Optional[str]] = mapped_column(String(32))
    system_of_record_id: Mapped[Optional[int]] = mapped_column(BigInteger)
    vestigingsnummer: Mapped[Optional[int]] = mapped_column(BigInteger)
    vestiging_huisletter: Mapped[Optional[str]]
    correspondentie_huisletter: Mapped[Optional[str]]
    vestiging_adres_buitenland1: Mapped[Optional[str]]
    vestiging_adres_buitenland2: Mapped[Optional[str]]
    vestiging_adres_buitenland3: Mapped[Optional[str]]
    vestiging_landcode: Mapped[Optional[int]] = mapped_column(default=6030)
    correspondentie_adres_buitenland1: Mapped[Optional[str]]
    correspondentie_adres_buitenland2: Mapped[Optional[str]]
    correspondentie_adres_buitenland3: Mapped[Optional[str]]
    correspondentie_landcode: Mapped[Optional[int]] = mapped_column(
        default=6030
    )

    main_activity: Mapped[dict] = mapped_column(
        postgresql.JSONB, server_default="{}"
    )
    secondairy_activities: Mapped[dict] = mapped_column(
        postgresql.JSONB, server_default="[]"
    )


class Config(Base):
    """Contains configuration of the system"""

    __tablename__ = "config"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    definition_id: Mapped[UUID] = mapped_column(GUID, unique=True)
    parameter: Mapped[Optional[str]] = mapped_column(String(128), unique=True)
    value: Mapped[Optional[str]]
    advanced: Mapped[bool] = mapped_column(default=True)


class ZaaktypeDefinitie(Base):
    """Case type definitions"""

    __tablename__ = "zaaktype_definitie"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    openbaarheid: Mapped[Optional[str]] = mapped_column(String(255))
    handelingsinitiator: Mapped[Optional[str]] = mapped_column(String(255))
    grondslag: Mapped[Optional[str]]
    procesbeschrijving: Mapped[Optional[str]] = mapped_column(String(255))
    afhandeltermijn: Mapped[Optional[str]] = mapped_column(String(255))
    afhandeltermijn_type: Mapped[Optional[str]] = mapped_column(String(255))
    selectielijst: Mapped[Optional[str]] = mapped_column(String(255))
    servicenorm: Mapped[Optional[str]] = mapped_column(String(255))
    servicenorm_type: Mapped[Optional[str]]
    pdc_voorwaarden: Mapped[Optional[str]]
    pdc_description: Mapped[Optional[str]]
    pdc_meenemen: Mapped[Optional[str]]
    pdc_tarief: Mapped[Optional[str]]
    omschrijving_upl: Mapped[Optional[str]]
    aard: Mapped[Optional[str]]
    preset_client: Mapped[Optional[str]] = mapped_column(String(255))
    extra_informatie: Mapped[Optional[str]]
    extra_informatie_extern: Mapped[Optional[str]]


class ZaakKenmerk(Base):
    """Custom attributes of the case."""

    __tablename__ = "zaak_kenmerk"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    zaak_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    bibliotheek_kenmerken_id: Mapped[int] = mapped_column(
        ForeignKey("bibliotheek_kenmerken.id")
    )
    value: Mapped[list[str]] = mapped_column(postgresql.ARRAY(Text))
    magic_string: Mapped[str]
    value_type: Mapped[BibliotheekKenmerkValueType] = mapped_column(
        Enum(*get_args(BibliotheekKenmerkValueType))
    )


class ZaaktypeBetrokkenen(Base):
    """Requestors of a castype."""

    __tablename__ = "zaaktype_betrokkenen"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    zaaktype_node_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    betrokkene_type: Mapped[Optional[str]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )


class CaseAction(Base):
    """Actions for a case during phase transition."""

    __tablename__ = "case_action"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    case_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    casetype_status_id: Mapped[int] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )
    type: Mapped[Optional[str]] = mapped_column(String(64))
    label: Mapped[Optional[str]] = mapped_column(String(255))
    automatic: Mapped[Optional[bool]]
    data: Mapped[Optional[dict]] = mapped_column(JSONEncodedDict)
    state_tainted: Mapped[Optional[bool]] = mapped_column(default=False)
    data_tainted: Mapped[Optional[bool]] = mapped_column(default=False)


class ZaaktypeChecklistItem(Base):
    """Checklist items, as used in casetype version"""

    __tablename__ = "zaaktype_status_checklist_item"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    casetype_status_id: Mapped[int] = mapped_column(
        ForeignKey("zaaktype_status.id")
    )
    label: Mapped[Optional[str]]
    external_reference: Mapped[Optional[str]]


class Checklist(Base):
    """Checklist associated with a case"""

    __tablename__ = "checklist"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    case_id: Mapped[int] = mapped_column(ForeignKey("zaak.id"))
    case_milestone: Mapped[Optional[int]]


class ChecklistItem(Base):
    """Checklist items associated with a case"""

    __tablename__ = "checklist_item"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    checklist_id: Mapped[int] = mapped_column(ForeignKey("checklist.id"))
    label: Mapped[Optional[str]]
    state: Mapped[bool] = mapped_column(default=False)
    sequence: Mapped[Optional[int]]
    user_defined: Mapped[bool] = mapped_column(default=True)
    deprecated_answer: Mapped[Optional[str]]
    due_date: Mapped[Optional[datetime.date]] = mapped_column(Date)
    description: Mapped[Optional[str]]
    assignee_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("subject.id")
    )
    assignee_person_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("natuurlijk_persoon.id")
    )
    assignee_organization_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("bedrijf.id")
    )
    product_code: Mapped[Optional[str]]
    dso_action_request: Mapped[bool] = mapped_column(default=False)


class UserEntity(Base):
    """User entitiy related with a subject"""

    __tablename__ = "user_entity"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    source_interface_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("interface.id")
    )
    source_identifier: Mapped[str]
    subject_id: Mapped[Optional[int]] = mapped_column(ForeignKey("subject.id"))
    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    properties: Mapped[Optional[dict]] = mapped_column(
        JSONEncodedDict, default="{}"
    )
    password: Mapped[Optional[str]] = mapped_column(String(255))
    active: Mapped[bool] = mapped_column(default=True)


class ObjectRelationship(Base):
    """Relationships between objects"""

    __tablename__ = "object_relationships"

    uuid: Mapped[UUID] = mapped_column(GUID, primary_key=True)
    object1_uuid: Mapped[UUID] = mapped_column(GUID)
    object2_uuid: Mapped[UUID] = mapped_column(GUID)
    type1: Mapped[str]
    type2: Mapped[str]
    object1_type: Mapped[str]
    object2_type: Mapped[str]
    blocks_deletion: Mapped[bool] = mapped_column(default=False)
    title1: Mapped[Optional[str]]
    title2: Mapped[Optional[str]]
    owner_object_uuid: Mapped[Optional[UUID]] = mapped_column(GUID)


class CaseRelation(Base):
    """Relationships between cases used for sorting"""

    __tablename__ = "case_relation"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    # These fields are nullable on the database, but that's
    # actually wrong, so we force them to not null Python side
    case_id_a: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    case_id_b: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    order_seq_a: Mapped[Optional[int]]
    order_seq_b: Mapped[Optional[int]]
    type_a: Mapped[Optional[str]]
    type_b: Mapped[Optional[str]]


CasePermissions = Literal["manage", "write", "read", "search"]


class CaseAcl(Base):
    """View on several tables, to determine which user has access to which case"""

    __tablename__ = "case_acl"
    __table_args__ = (
        PrimaryKeyConstraint(
            "case_id", "subject_id", "permission", "casetype_id"
        ),
    )
    case_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("zaak.id"))
    case_uuid: Mapped[UUID] = mapped_column(GUID, ForeignKey("zaak.uuid"))
    permission: Mapped[CasePermissions] = mapped_column(
        Enum(*get_args(CasePermissions))
    )
    subject_id: Mapped[int] = mapped_column(ForeignKey("subject.id"))

    subject_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("subject.uuid")
    )
    casetype_id: Mapped[int] = mapped_column(ForeignKey("zaaktype.id"))


class ZaaktypeAuthorisation(Base):
    __tablename__ = "zaaktype_authorisation"

    zaaktype_node_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype_node.id")
    )
    current_zaaktype_node = relationship(
        "ZaaktypeNode", foreign_keys=[zaaktype_node_id]
    )

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    recht: Mapped[Optional[str]]
    created: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    deleted: Mapped[Optional[datetime.datetime]] = mapped_column(UTCDateTime)
    role_id: Mapped[Optional[int]] = mapped_column(ForeignKey("roles.id"))
    ou_id: Mapped[Optional[int]] = mapped_column(ForeignKey("groups.id"))

    zaaktype_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("zaaktype.id")
    )
    zaaktype = relationship("Zaaktype")

    confidential: Mapped[bool] = mapped_column(default=False)


class SubjectPositionMatrix(Base):
    __tablename__ = "subject_position_matrix"

    __table_args__ = (
        PrimaryKeyConstraint("subject_id", "group_id", "role_id", "position"),
    )

    subject_id: Mapped[int]
    group_id: Mapped[int]
    role_id: Mapped[int]
    position: Mapped[str]


class FileDerivative(Base):
    """Preview data for a file"""

    __tablename__ = "file_derivative"

    id: Mapped[UUID] = mapped_column(
        GUID, server_default=sql.text("uuid_generate_v4()"), primary_key=True
    )
    file_id: Mapped[int] = mapped_column(ForeignKey("file.id"))
    filestore_id: Mapped[int] = mapped_column(ForeignKey("filestore.id"))
    max_width: Mapped[int]
    max_height: Mapped[int]
    date_generated: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    type: Mapped[str]


# GEO Schemas
class ServiceGeoJSON(Base):
    """Model representing the service_geojson table"""

    __tablename__ = "service_geojson"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    geo_json: Mapped[dict] = mapped_column(postgresql.JSONB)
    last_modified: Mapped[datetime.datetime] = mapped_column(
        DateTime,
        server_default=sql.text("CURRENT_TIMESTAMP"),
    )


class ServiceGeoJSONRelationship(Base):
    """Model representing sub-table service_geojson_relationship"""

    __tablename__ = "service_geojson_relationship"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    related_uuid: Mapped[UUID] = mapped_column(GUID, unique=False)
    service_geojson_id: Mapped[int] = mapped_column(
        GUID, ForeignKey("service_geojson.id")
    )
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        DateTime, server_default=sql.text("CURRENT_TIMESTAMP")
    )

    __table_args__ = (
        UniqueConstraint(
            "related_uuid", "service_geojson_id", name="unique_relation"
        ),
    )


class Transaction(Base):
    """Model representing transaction table"""

    __tablename__ = "transaction"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(GUID, unique=True)
    interface_id: Mapped[int]
    external_transaction_id: Mapped[Optional[str]]
    input_data: Mapped[Optional[str]]
    input_file: Mapped[Optional[str]]
    automated_retry_count: Mapped[Optional[int]]
    date_created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    date_last_retry: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    date_next_retry: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    processed: Mapped[Optional[bool]] = mapped_column(default=False)
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    error_count: Mapped[Optional[int]] = mapped_column(default=0)
    direction: Mapped[str]
    success_count: Mapped[Optional[int]] = mapped_column(default=0)
    total_count: Mapped[Optional[int]] = mapped_column(default=0)
    processor_params: Mapped[Optional[str]]
    error_fatal: Mapped[Optional[bool]]
    preview_data: Mapped[dict | list] = mapped_column(JSONEncodedDict)
    error_message: Mapped[Optional[str]]
    text_vector: Mapped[Optional[str]] = mapped_column(postgresql.TSVECTOR)


class TransactionRecord(Base):
    """Model representing transaction_record table"""

    __tablename__ = "transaction_record"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    transaction_id: Mapped[Optional[int]]
    input: Mapped[str]
    output: Mapped[str]
    is_error: Mapped[bool]
    date_executed: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    preview_string: Mapped[Optional[str]]
    last_error: Mapped[Optional[str]]


class ExportQueue(Base):
    """Model representing export_queue table"""

    __tablename__ = "export_queue"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(GUID, unique=True)
    subject_id: Mapped[int] = mapped_column(ForeignKey("subject.id"))
    subject_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("subject.uuid")
    )
    expires: Mapped[datetime.datetime] = mapped_column(
        UTCDateTime, server_default=sql.text("NOW() + 3 days")
    )
    token: Mapped[str]
    filestore_id: Mapped[int] = mapped_column(ForeignKey("filestore.id"))
    filestore_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("filestore.uuid")
    )
    downloaded: Mapped[int] = mapped_column(default=0)


ZaakBagType = Literal[
    "nummeraanduiding", "verblijfsobject", "pand", "openbareruimte"
]


class ZaakBag(Base):
    """Model representing zaak_bag table"""

    __tablename__ = "zaak_bag"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )

    pid: Mapped[Optional[int]]
    zaak_id: Mapped[Optional[int]] = mapped_column(BigInteger)
    bag_type: Mapped[Optional[ZaakBagType]] = mapped_column(
        Enum(*get_args(ZaakBagType))
    )
    bag_id: Mapped[Optional[str]]
    bag_verblijfsobject_id: Mapped[Optional[str]]
    bag_openbareruimte_id: Mapped[Optional[str]]
    bag_nummeraanduiding_id: Mapped[Optional[str]]
    bag_pand_id: Mapped[Optional[str]]
    bag_standplaats_id: Mapped[Optional[str]]
    bag_ligplaats_id: Mapped[Optional[str]]
    bag_coordinates_wsg: Mapped[Optional[str]]


class CaseRelationship(Base):
    "Relations between cases"

    __tablename__ = "view_case_relationship"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    case_id: Mapped[int] = mapped_column(BigInteger)
    relation_id: Mapped[int]
    type: Mapped[str]
    relation_uuid: Mapped[UUID] = mapped_column(GUID)
    order_seq: Mapped[int]


class CountryCode(Base):
    __tablename__ = "country_code"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    dutch_code: Mapped[int]
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    label: Mapped[str]
    historical: Mapped[bool]


class SavedSearch(Base):
    __tablename__ = "saved_search"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    name: Mapped[str]
    kind: Mapped[str]
    owner_id: Mapped[int] = mapped_column(ForeignKey("subject.id"))
    filters: Mapped[dict] = mapped_column(postgresql.JSONB)
    permissions: Mapped[list[str]] = mapped_column(postgresql.ARRAY(Text))
    columns: Mapped[dict] = mapped_column(postgresql.JSONB)
    sort_column: Mapped[str]
    sort_order: Mapped[str]
    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_updated: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    updated_by: Mapped[Optional[int]] = mapped_column(ForeignKey("subject.id"))
    template: Mapped[Optional[str]]


class SavedSearchPermission(Base):
    __tablename__ = "saved_search_permission"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    saved_search_id: Mapped[int] = mapped_column(ForeignKey("saved_search.id"))
    group_id: Mapped[int] = mapped_column(ForeignKey("groups.id"))
    role_id: Mapped[int] = mapped_column(ForeignKey("roles.id"))
    permission: Mapped[str]
    sort_order: Mapped[Optional[int]]

    __table_args__ = (
        UniqueConstraint(
            "saved_search_id", "group_id", "role_id", "permission"
        ),
    )


class ObjectSubscription(Base):
    __tablename__ = "object_subscription"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    interface_id: Mapped[int]
    external_id: Mapped[str]
    local_table: Mapped[str]
    local_id: Mapped[str]
    date_created: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    date_deleted: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime
    )
    object_preview: Mapped[Optional[str]]
    config_interface_id: Mapped[Optional[int]]


class CaseV2(Base):
    __tablename__ = "view_case_v2"
    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    onderwerp: Mapped[str]
    route_ou: Mapped[int]
    route_role: Mapped[int]
    vernietigingsdatum: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    archival_state: Mapped[str]
    status: Mapped[str]
    contactkanaal: Mapped[str]
    created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    registratiedatum: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    streefafhandeldatum: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    afhandeldatum: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    stalled_until: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    milestone: Mapped[int]
    last_modified: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    behandelaar_gm_id: Mapped[int]
    coordinator_gm_id: Mapped[int]
    aanvrager: Mapped[int]
    aanvraag_trigger: Mapped[CaseTrigger] = mapped_column(
        Enum(*get_args(CaseTrigger))
    )
    onderwerp_extern: Mapped[str]
    resultaat: Mapped[str]
    resultaat_id: Mapped[int]
    payment_amount: Mapped[decimal.Decimal] = mapped_column(Numeric(100, 2))
    payment_status: Mapped[str]
    confidentiality: Mapped[dict] = mapped_column(postgresql.JSONB)
    behandelaar: Mapped[int]
    coordinator: Mapped[int]
    urgency: Mapped[str]
    preset_client: Mapped[bool] = mapped_column(default=False)
    prefix: Mapped[str]
    active_selection_list: Mapped[str]
    case_status: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_meta: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_role: Mapped[dict] = mapped_column(postgresql.JSONB)
    type_of_archiving: Mapped[str]
    period_of_preservation: Mapped[str]
    result_uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    result: Mapped[str]
    result_description: Mapped[str]
    result_explanation: Mapped[str]
    result_selection_list_number: Mapped[int]
    result_process_type_number: Mapped[int]
    result_process_type_name: Mapped[str]
    result_process_type_description: Mapped[str]
    result_process_type_explanation: Mapped[str]
    result_process_type_generic: Mapped[str]
    result_origin: Mapped[str]
    result_process_term: Mapped[str]
    aggregation_scope: Mapped[str]
    number_parent: Mapped[int] = mapped_column(BigInteger)
    number_master: Mapped[int] = mapped_column(BigInteger)
    number_previous: Mapped[int] = mapped_column(BigInteger)
    price: Mapped[str]
    result_process_type_object: Mapped[str]
    custom_fields: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_actions: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_department: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_subjects: Mapped[dict] = mapped_column(postgresql.JSONB)
    destructable: Mapped[bool]
    aanvrager_gm_id: Mapped[int]
    aanvrager_type: Mapped[str]
    zaaktype_id: Mapped[int]
    zaaktype_node_id: Mapped[int]
    deleted: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    suspension_rationale: Mapped[str]
    premature_completion_rationale: Mapped[str]
    days_left: Mapped[int]
    lead_time_real: Mapped[int]
    progress_days: Mapped[str]
    html_email_template: Mapped[str]
    file_custom_fields: Mapped[dict] = mapped_column(postgresql.JSONB)

    requestor_obj: Mapped[dict] = mapped_column(postgresql.JSONB)
    assignee_obj: Mapped[dict] = mapped_column(postgresql.JSONB)
    coordinator_obj: Mapped[dict] = mapped_column(postgresql.JSONB)

    case_type: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_type_version: Mapped[dict] = mapped_column(postgresql.JSONB)
    is_multiple: Mapped[bool]


class BagCache(Base):
    __tablename__ = "bag_cache"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    bag_type: Mapped[str]
    bag_id: Mapped[str]
    bag_data: Mapped[dict] = mapped_column(postgresql.JSONB)
    date_created: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
    last_modified: Mapped[datetime.datetime] = mapped_column(UTCDateTime)


class Dashboard(Base):
    __tablename__ = "dashboard"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    widgets: Mapped[dict] = mapped_column(postgresql.JSONB)


class SavedSearchLabels(Base):
    __tablename__ = "saved_search_labels"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[Optional[UUID]] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    label: Mapped[str]


class SavedSearchLabelsMapping(Base):
    __tablename__ = "saved_search_label_mapping"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    label_id: Mapped[int]
    saved_search_id: Mapped[int]


class ZaaktypeObjectMutation(Base):
    __tablename__ = "zaaktype_object_mutation"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    zaaktype_node_id: Mapped[int]
    zaaktype_status_id: Mapped[int]
    attribute_uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    mutation_type: Mapped[str]
    object_title: Mapped[Optional[str]]
    phase_transition: Mapped[bool]
    system_attribute_changes: Mapped[dict] = mapped_column(postgresql.JSONB)
    attribute_mapping: Mapped[dict] = mapped_column(postgresql.JSONB)


class ViewCaseTypeVersionV2(Base):
    __tablename__ = "view_case_type_version_v2"
    uuid: Mapped[UUID] = mapped_column(GUID, primary_key=True)
    casetype_uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    active: Mapped[bool]
    catalog_folder: Mapped[Optional[dict]] = mapped_column(postgresql.JSONB)
    general_attributes: Mapped[dict] = mapped_column(postgresql.JSONB)
    documentation: Mapped[dict] = mapped_column(postgresql.JSONB)
    relations: Mapped[dict] = mapped_column(postgresql.JSONB)
    webform: Mapped[dict] = mapped_column(postgresql.JSONB)
    registrationform: Mapped[dict] = mapped_column(postgresql.JSONB)
    case_dossier: Mapped[dict] = mapped_column(postgresql.JSONB)
    api: Mapped[dict] = mapped_column(postgresql.JSONB)
    authorization: Mapped[Optional[dict]] = mapped_column(postgresql.JSONB)
    child_casetype_settings: Mapped[dict] = mapped_column(postgresql.JSONB)
    phases: Mapped[dict] = mapped_column(postgresql.JSONB)
    results: Mapped[dict] = mapped_column(postgresql.JSONB)


# Table to store style configuration
class StyleConfig(Base):
    __tablename__ = "style_configuration"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    uuid: Mapped[UUID] = mapped_column(
        GUID, unique=True, server_default=FetchedValue()
    )
    tenant: Mapped[str]
    name: Mapped[str]
    extension: Mapped[str]
    content: Mapped[Optional[str]]
    last_modified: Mapped[Optional[datetime.datetime]] = mapped_column(
        UTCDateTime,
        server_default=sql.text("CURRENT_TIMESTAMP"),
    )
    mimetype: Mapped[str]

    __table_args__ = (UniqueConstraint("tenant", "name"),)


class CasetypeArchiveSettings(Base):
    __tablename__ = "zaaktype_archive_settings"
    id: Mapped[int]
    uuid: Mapped[int] = mapped_column(primary_key=True)
    casetype_node_id: Mapped[int]
    name: Mapped[Optional[str]]
    classification_code: Mapped[Optional[str]]
    classification_description: Mapped[Optional[str]]
    classification_source_tmlo: Mapped[Optional[str]]
    classification_date: Mapped[Optional[str]]
    description_tmlo: Mapped[Optional[str]]
    location: Mapped[Optional[str]]
    coverage_in_time_tmlo: Mapped[Optional[str]]
    coverage_in_geo_tmlo: Mapped[Optional[str]]
    language_tmlo: Mapped[Optional[str]]
    user_rights: Mapped[Optional[str]]
    user_rights_description: Mapped[Optional[str]]
    user_rights_date_period: Mapped[Optional[str]]
    confidentiality: Mapped[Optional[str]]
    confidentiality_description: Mapped[Optional[str]]
    confidentiality_date_period: Mapped[Optional[str]]
    form_genre: Mapped[Optional[str]]
    form_publication: Mapped[Optional[str]]
    structure: Mapped[Optional[str]]
    generic_metadata_tmlo: Mapped[list[str]] = mapped_column(
        postgresql.ARRAY(str)
    )
    archive_type: Mapped[str]


class SubjectLoginHistory(Base):
    __tablename__ = "subject_login_history"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    ip: Mapped[str] = mapped_column(postgresql.INET)
    subject_id: Mapped[int] = mapped_column(ForeignKey("subject.id"))
    subject_uuid: Mapped[UUID] = mapped_column(
        GUID, ForeignKey("subject.uuid")
    )
    success: Mapped[bool] = mapped_column(default=True)
    method: Mapped[str]
    date_attempt: Mapped[datetime.datetime] = mapped_column(UTCDateTime)
