# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from datetime import timedelta
from minty.cqrs import CommandBase
from minty.exceptions import Conflict
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_call
from typing import Any, Literal, cast
from typing_extensions import TypedDict
from uuid import UUID, uuid4
from zsnl_domains.case_management.repositories import (
    CaseRepository as CMCaseRepository,
)
from zsnl_domains.document.entities.document import (
    Document,
    DocumentCategory,
    DocumentConfidentiality,
    DocumentOrigin,
    DocumentPublishSettings,
    DocumentStatus,
)
from zsnl_domains.document.repositories import (
    CaseRepository,
    DirectoryRepository,
    DocumentArchiveRepository,
    DocumentLabelRepository,
    DocumentRepository,
    FileRepository,
    FileStoreSource,
    MessageRepository,
)


class CreateCaseRegistrationPDFFields(TypedDict):
    attributes: dict[str, Any]
    case_number: int
    case_uuid: UUID
    casetype_version_uuid: UUID
    hostname: str
    origin: str | None
    requestor: dict[str, Any]


class Commands(CommandBase):
    """Document domain commands."""

    @validate_with(get_data(__name__, "validation/create_document.json"))
    def create_document(
        self,
        document_uuid,
        filename,
        store_uuid,
        mimetype,
        size,
        storage_location,
        md5,
        magic_strings: list[str] | None,
        editor_update: bool | None = False,
        directory_uuid=None,
        replaces=None,
        case_uuid=None,
        skip_intake=False,
        auto_accept=False,
        accept_extended_mimetypes: bool = False,
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )

        if replaces:
            document_uuid = replaces
            auto_accept = True

        document = docu_repo.create_document(
            document_uuid=document_uuid,
            filename=filename,
            store_uuid=store_uuid,
            directory_uuid=directory_uuid,
            case_uuid=case_uuid,
            mimetype=mimetype,
            size=size,
            storage_location=storage_location,
            md5=md5,
            skip_intake=skip_intake,
            auto_accept=auto_accept,
            editor_update=editor_update,
            accept_extended_mimetypes=accept_extended_mimetypes,
            replaces=replaces,
        )
        if magic_strings:
            labels = label_repo.get_document_labels_for_case_magicstrings(
                case_uuid=case_uuid,
                user_uuid=self.user_uuid,
                magic_strings=magic_strings,
            )
            document.apply_labels(labels=labels, case_uuid=case_uuid)

        docu_repo.save()

    @validate_with(get_data(__name__, "validation/create_file.json"))
    def create_file(
        self, file_uuid, filename, mimetype, size, storage_location, md5
    ):
        file_repo = cast(FileRepository, self.get_repository("file"))
        file_repo.create_file(
            file_uuid, filename, mimetype, size, storage_location, md5
        )
        file_repo.save()

    @validate_with(
        get_data(__name__, "validation/create_document_from_attachment.json")
    )
    def create_document_from_attachment(self, attachment_uuid):
        document_repository = cast(
            DocumentRepository, self.get_repository("document")
        )
        document_repository.create_document_from_attachment(attachment_uuid)
        document_repository.save()

    @validate_with(
        get_data(__name__, "validation/create_document_from_message.json")
    )
    def create_document_from_message(
        self,
        message_uuid: str,
        output_format: Literal["pdf"] | Literal["original"] = "pdf",
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        message_repo = cast(MessageRepository, self.get_repository("message"))
        file_repo = cast(FileRepository, self.get_repository("file"))

        document_uuid = uuid4()

        if output_format == "pdf":
            filestore_source: FileStoreSource = (
                message_repo.get_filestore_source_with_pdf_from_message_uuid(
                    message_uuid=message_uuid
                )
            )
        elif output_format == "original":
            filestore_source: FileStoreSource = message_repo.get_filestore_source_with_original_file_uuid_from_message_uuid(
                message_uuid=message_uuid
            )

        filename = (
            filestore_source.subject
            if filestore_source.subject
            else "Geen onderwerp"
        )

        file = file_repo.get_file_by_uuid(
            file_uuid=filestore_source.filestore_uuid
        )

        docu_repo.create_document(
            document_uuid=document_uuid,
            filename=filename + file.extension,
            store_uuid=filestore_source.filestore_uuid,
            directory_uuid=None,
            case_uuid=filestore_source.case_uuid,
            mimetype=file.mimetype,
            size=file.size,
            storage_location=file.storage_location,
            md5=file.md5,
        )

        docu_repo.save()

    @validate_call
    def create_document_from_file(
        self,
        document_uuid: UUID,
        file_uuid: UUID,
        case_uuid: UUID | None = None,
        directory_uuid: UUID | None = None,
        magic_strings: list[str] | None = None,
        skip_intake: bool = False,
    ) -> None:
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )
        file_repo = cast(FileRepository, self.get_repository("file"))

        # If there is no user_info in the command_instance, we assume that
        # this command is being called from event_handler document_events_consumer
        # and the user will be always an employee (not pip).
        is_pip_user = False
        if self.user_info:
            is_pip_user = self.user_info.permissions.get("pip_user", False)

        file = file_repo.get_file_by_uuid(file_uuid=file_uuid)

        create_params = {
            "document_uuid": document_uuid,
            "filename": file.basename + file.extension,
            "store_uuid": file_uuid,
            "case_uuid": case_uuid,
            "mimetype": file.mimetype,
            "size": file.size,
            "storage_location": file.storage_location,
            "md5": file.md5,
            "is_pip_user": is_pip_user,
            "skip_intake": skip_intake,
        }

        if directory_uuid:
            create_params["directory_uuid"] = directory_uuid

        document = docu_repo.create_document(**create_params)

        if magic_strings and case_uuid:
            labels = label_repo.get_document_labels_for_case_magicstrings(
                case_uuid=case_uuid,
                user_uuid=self.user_uuid,
                magic_strings=magic_strings,
            )
            document.apply_labels(labels=labels, case_uuid=case_uuid)

        docu_repo.save()

    @validate_with(
        get_data(
            __name__, "validation/create_document_from_message_archive.json"
        )
    )
    def create_document_from_message_archive(self, message_uuid, file_uuid):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        file_repo = cast(FileRepository, self.get_repository("file"))
        message_repo = cast(MessageRepository, self.get_repository("message"))

        message = message_repo.get_message_by_uuid(message_uuid=message_uuid)

        file = file_repo.get_file_by_uuid(file_uuid=file_uuid)

        document_uuid = uuid4()
        docu_repo.create_document(
            document_uuid=document_uuid,
            filename=file.basename + file.extension,
            store_uuid=file_uuid,
            directory_uuid=None,
            case_uuid=message.case_uuid,
            mimetype=file.mimetype,
            size=file.size,
            storage_location=file.storage_location,
            md5=file.md5,
        )

        docu_repo.save()

    @validate_call
    def add_document_to_case(
        self,
        document_uuid: UUID,
        case_uuid: UUID,
        document_label_uuids: list[UUID] | None = None,
    ):
        if document_label_uuids is None:
            document_label_uuids = []

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        labels = []
        if document_label_uuids:
            document_label_repo = cast(
                DocumentLabelRepository, self.get_repository("document_label")
            )

            labels = document_label_repo.get_document_labels_by_uuid(
                uuids=document_label_uuids, case_uuid=case_uuid
            )

        document.add_to_case(case_uuid=case_uuid)
        document.apply_labels(labels=labels, case_uuid=case_uuid)
        document_repo.save()

    @validate_call
    def update_document(
        self,
        document_uuid: UUID,
        basename: str | None = None,
        description: str | None = None,
        document_category: DocumentCategory | None = None,
        origin: DocumentOrigin | None = None,
        origin_date: datetime.date | None = None,
        confidentiality: DocumentConfidentiality | None = None,
        document_source: str | None = None,
        publish_settings: DocumentPublishSettings | None = None,
        status: DocumentStatus | None = None,
        pronom_format: str | None = None,
        appearance: str | None = None,
        structure: str | None = None,
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.update(
            basename=basename,
            description=description,
            document_category=document_category,
            origin=origin,
            origin_date=origin_date,
            confidentiality=confidentiality,
            document_source=document_source,
            publish_settings=publish_settings,
            status=status,
            pronom_format=pronom_format,
            appearance=appearance,
            structure=structure,
        )
        document_repo.save()

    @validate_with(get_data(__name__, "validation/delete_document.json"))
    def delete_document(
        self, user_info: dict, document_uuid: UUID, reason: str = ""
    ):
        """
        Permanently delete a document.

        :param user_info: dict: The user_info dict
        :param document_uuid: UUID: The document_uuid to delete
        :param reason: str: Optional reason for deletion
        :return:

        :raises NotFound when document_uuid is not found (code: document/not_found)
        :raises Conflict when document_uuid is assigned to a case (code: document/delete_document/assigned_to_case)
        """

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid,
            user_info=user_info,
            integrity_check=True,
        )
        document.delete(reason=reason)
        document_repo.save()

    @validate_with(
        get_data(__name__, "validation/assign_document_to_user.json")
    )
    def assign_document_to_user(self, document_uuid: UUID, user_uuid: UUID):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.assign_document_to_user(intake_owner_uuid=user_uuid)
        document_repo.save()

    @validate_with(
        get_data(__name__, "validation/assign_document_to_role.json")
    )
    def assign_document_to_role(
        self, document_uuid: UUID, group_uuid: UUID, role_uuid: UUID
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.assign_document_to_role(
            intake_group_uuid=group_uuid, intake_role_uuid=role_uuid
        )
        document_repo.save()

    @validate_with(
        get_data(__name__, "validation/reject_assigned_document.json")
    )
    def reject_assigned_document(
        self, document_uuid: UUID, rejection_reason: str
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.reject_from_intake(rejection_reason=rejection_reason)
        document_repo.save(self.user_info)

    @validate_with(get_data(__name__, "validation/create_directory.json"))
    def create_directory(
        self,
        directory_uuid: UUID,
        case_uuid: UUID,
        name: str,
        parent_uuid: UUID | None = None,
    ):
        """
        Create a Directory. A Directory entry is created by a given directory_uuid,
        a coupled case_uuid and a given name of the directory.
        It is possible to create a Directory entry within a directory hierarchy
        if an optional parent_uuid is given. When an optional parent_uuid is given,
        the case_id of the parent Directory must match the case_id of the directory
        being created, and the path of the parent Directory is copied into the
        Directory's path column and appended with it's parents ID. When no parent_uuid
        is given the Directory database path column will be filled with an empty list,
        which makes it a directory in the root in the case.

        :param directory_uuid UUID: The UUID for the new directory to be created.
        :param case_uuid UUID: The coupled case_uuid
        :param name: The given Directory name
        :param parent_uuid: An optional Directory parent_uuid
        :return:
        """
        assert self.user_info

        case_repo = cast(CMCaseRepository, self.get_repository("cm_case"))
        directory_repo = cast(
            DirectoryRepository, self.get_repository("directory")
        )

        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.user_info,
            permission="write",
        )

        if parent_uuid:
            parent_directory = directory_repo.get_directory_by_uuid(
                uuid=parent_uuid
            )
            if case.id != parent_directory.case_id:
                raise Conflict(
                    f"The parent directory's case_id does not match the given case_id ({case.id} != {parent_directory.case_id})",
                    "domains/document/create_directory/parent_case_id_mismatch",
                )

            parent_directory_id = directory_repo.get_directory_id_by_uuid(
                parent_directory.uuid
            )
            new_path = parent_directory.path
            new_path.append(parent_directory_id)
        else:
            new_path = []

        directory_repo.create_directory(
            directory_uuid,
            name,
            case.id,
            new_path,
        )

        directory_repo.save()

    @validate_call
    def apply_labels_to_document(
        self, document_uuids: list[UUID], document_label_uuids: list[UUID]
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document_label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )

        for document_uuid in document_uuids:
            document = document_repo.get_document_by_uuid(
                document_uuid=document_uuid, user_info=self.user_info
            )
            labels = document_label_repo.get_document_labels_by_uuid(
                uuids=document_label_uuids, case_uuid=document.case_uuid
            )
            document.apply_labels(labels=labels, case_uuid=document.case_uuid)

        document_repo.save()

    @validate_call
    def remove_labels_from_document(
        self, document_uuids: list[UUID], document_label_uuids: list[UUID]
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document_label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )

        for document_uuid in document_uuids:
            document = document_repo.get_document_by_uuid(
                document_uuid=document_uuid, user_info=self.user_info
            )
            labels = document_label_repo.get_document_labels_by_uuid(
                uuids=document_label_uuids, case_uuid=document.case_uuid
            )
            document.remove_labels(labels=labels, case_uuid=document.case_uuid)

        document_repo.save()

    @validate_call
    def move_to_directory(
        self,
        destination_directory_uuid: UUID | None = None,
        source_documents: list[UUID] | None = None,
        source_directories: list[UUID] | None = None,
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        directory_repo = cast(
            DirectoryRepository, self.get_repository("directory")
        )

        destination_directory = None
        destination_directory_id = None

        if destination_directory_uuid is not None:
            destination_directory = directory_repo.get_directory_by_uuid(
                destination_directory_uuid
            )
            destination_directory_id = directory_repo.get_directory_id_by_uuid(
                destination_directory.uuid
            )

        # Move documents
        if source_documents:
            for document_uuid in source_documents:
                document = document_repo.get_document_by_uuid(
                    document_uuid=document_uuid, user_info=self.user_info
                )
                if (
                    destination_directory
                    and document.case_display_number
                    != destination_directory.case_id
                ):
                    raise Conflict(
                        f"The destination directories case_id does not match the given documents case_id ({destination_directory.case_id} != {document.case_display_number})",
                        "domains/document/move_to_directory/document_case_id_mismatch",
                    )
                else:
                    document.move_to_directory(
                        directory_uuid=destination_directory_uuid
                    )

            document_repo.save()

        # move directories
        if source_directories:
            for directory_uuid in source_directories:
                directory = directory_repo.get_directory_by_uuid(
                    uuid=directory_uuid
                )
                directory_id = directory_repo.get_directory_id_by_uuid(
                    directory.uuid
                )

                if (
                    destination_directory
                    and directory.case_id != destination_directory.case_id
                ):
                    raise Conflict(
                        f"The destination directories case_id does not match the given directories case_id ({destination_directory.case_id} != {directory.case_id})",
                        "domains/document/move_to_directory/directory_case_id_mismatch",
                    )
                else:
                    if destination_directory_id and (
                        (directory_id in destination_directory.path)
                        or (directory_id == destination_directory_id)
                    ):
                        raise Conflict(
                            "Can not move directory to itself or its child directories",
                            "domains/document/move_to_directory/moving_to_self",
                        )
                    else:
                        new_path = []
                        if destination_directory_id is not None:
                            new_path.extend(destination_directory.path)
                            new_path.append(destination_directory_id)

                        directory.move(path=new_path)

            directory_repo.save()

    @validate_call
    def accept_document(
        self,
        document_uuid: UUID,
        as_version_of: UUID | None = None,
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.accept_document(as_version_of=as_version_of)
        document_repo.save()

    @validate_call
    def request_document_archive(
        self,
        case_uuid: UUID,
        zip_all: bool,
        directory_uuids: list[str],
        document_uuids: list[str],
        export_file_uuid: UUID,
    ):
        document_archive_requested = cast(
            DocumentArchiveRepository,
            self.get_repository("document_archive_request"),
        )

        doc = document_archive_requested.get_request_for_event()

        doc.document_archive_requested(
            case_uuid,
            zip_all,
            directory_uuids,
            document_uuids,
            export_file_uuid,
        )

    @validate_call
    def create_document_archive(
        self,
        case_uuid: UUID,
        zip_all: bool,
        directory_uuids: list[UUID],
        document_uuids: list[UUID],
        export_file_uuid: UUID | None = None,
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        file_repo = cast(FileRepository, self.get_repository("file"))
        directory_repo = cast(
            DirectoryRepository, self.get_repository("directory")
        )

        # Get all files for the case
        document_uuids_in_case = docu_repo.get_document_uuids_by_case_uuid(
            case_uuid
        )
        if zip_all:
            for document_uuid_in_case in document_uuids_in_case:
                document_uuids.append(document_uuid_in_case)

        self._get_document_uuids_for_directories(
            directory_uuids, directory_repo, docu_repo, document_uuids
        )

        for document_uuid_str in document_uuids:
            if document_uuid_str not in document_uuids_in_case:
                raise Conflict(
                    f"The document uuid '{document_uuid_str}' is not part of the case with uuid '{case_uuid}'",
                    "domains/document/create_document_archive/document_case_mismatch",
                )

        document_archive_requested = cast(
            DocumentArchiveRepository,
            self.get_repository("document_archive_request"),
        )
        zip_file_uuid = uuid4()

        upload_result = document_archive_requested.create_zip(
            zip_file_uuid,
            document_uuids,
            docu_repo,
            file_repo,
            directory_repo,
            self.user_info,
            case_uuid,
        )

        document_archive_requested.store_export(
            user_info=self.user_info,
            case_uuid=case_uuid,
            upload_result=upload_result,
            filestore_uuid=zip_file_uuid,
            export_file_uuid=export_file_uuid,
        )

        doc = document_archive_requested.get_request_for_event()
        doc.document_archive_created(case_uuid, document_uuids)

    def _get_document_uuids_for_directories(
        self,
        directory_uuids: list[UUID],
        directory_repo: DirectoryRepository,
        docu_repo: DocumentRepository,
        document_uuids: list[UUID],
    ):
        all_directories_to_zip: list[UUID] = []

        for directory_uuid in directory_uuids:
            if directory_uuid not in all_directories_to_zip:
                all_directories_to_zip.append(directory_uuid)

                children = directory_repo.get_child_folders(directory_uuid)

                for child in children:
                    if child not in all_directories_to_zip:
                        all_directories_to_zip.append(child)

        # Get all document in the folders
        for directory_uuid in all_directories_to_zip:
            directory_id = directory_repo.get_directory_id_by_uuid(
                directory_uuid
            )
            document_uuids_in_directory = (
                docu_repo.get_document_uuids_by_directory_id(directory_id)
            )

            for document_uuid_in_directory in document_uuids_in_directory:
                if document_uuid_in_directory not in document_uuids:
                    if document_uuid_in_directory is not None:
                        document_uuids.append(document_uuid_in_directory)

    def _create_preview_for_document(self, document: Document):
        if document.has_preview():
            self.logger.debug(
                f"Document {document.entity_id}: Already has a preview"
            )
            return

        if not document.can_preview():
            self.logger.debug(
                f"Document {document.entity_id}: Not previewable (yet)"
            )
            return

        self.logger.debug(f"Document {document.entity_id}: Requesting preview")
        document.create_preview()

    @validate_call
    def create_preview_for_documents_for_file(self, file_uuid: UUID) -> None:
        """
        Create a "preview" derivative for every document the specified
        file_uuid is linked to.
        """

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        if document_repo.is_rate_limited("command:create_preview"):
            return

        documents = document_repo.get_documents_by_file_uuid_unsafe(
            file_uuid=file_uuid
        )

        for document in documents:
            self._create_preview_for_document(document)

        document_repo.save()
        return

    @validate_call
    def create_preview_for_document(
        self,
        document_uuid: UUID,
        is_manual_request: bool = False,
    ) -> None:
        """
        Create a "preview" derivative for a specific document.
        """

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        # Manual preview requests (click to open) have their own rate-limiting
        # so don't _also_ limit them here.
        if not is_manual_request and document_repo.is_rate_limited(
            "command:create_preview"
        ):
            return

        document = document_repo.get_document_by_uuid_unsafe(
            document_uuid=document_uuid
        )
        self._create_preview_for_document(document)

        document_repo.save()
        return

    def _create_thumbnail_for_document(self, document: Document) -> None:
        if document.has_thumbnail():
            self.logger.debug(
                f"Document {document.entity_id}: Already has a preview"
            )
            return

        if not document.can_thumbnail():
            self.logger.debug(
                f"Document {document.entity_id}: Cannot create thumbnail (yet)"
            )
            return

        document.create_thumbnail()
        return

    @validate_call
    def create_thumbnail_for_documents_for_file(self, file_uuid: UUID) -> None:
        """
        Create a "thumbnail" derivative for every document the specified
        file_uuid is linked to.
        """

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        if document_repo.is_rate_limited("command:create_thumbnail"):
            return

        documents = document_repo.get_documents_by_file_uuid_unsafe(
            file_uuid=file_uuid
        )

        for document in documents:
            self._create_thumbnail_for_document(document)

        document_repo.save()
        return

    @validate_call
    def create_thumbnail_for_document(
        self,
        document_uuid: UUID,
        is_manual_request: bool = False,
    ) -> None:
        """
        Create a "thumbnail" derivative for a specific document.
        """

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        # Manual thumbnail requests (mouseover) have their own rate-limiting
        # so don't _also_ limit them here.
        if not is_manual_request and document_repo.is_rate_limited(
            "command:create_thumbnail"
        ):
            return

        document = document_repo.get_document_by_uuid_unsafe(
            document_uuid=document_uuid
        )
        self._create_thumbnail_for_document(document)

        document_repo.save()
        return

    def _set_search_index_for_document(
        self,
        document_repo: DocumentRepository,
        document: Document,
        delayed: bool = False,
    ) -> None:
        """
        Set the search terms for a document this call is rate limted by the setting command:set_search_index
        that can be changed in the deployments repository. Its called from two places the http-document with
        delayed False where the document initialy are processed and are rate limited.
        The second source is the document processor that handles the delayed search indexing at a slow rate
        due there only being a limited amount of pods active that handle that queue and those calls are not
        rate limited a second time.

        :param document_repo DocumentRepository: Contains the method needed to check if rate limiting is needed.
        :param document Document: the file to generate the search terms for.
        :param delayed bool: indicates if the request is comming form the sheduler that handles delayed request(so it does not get delayed twice)
        :return:
        """

        if document.has_search_index:
            self.logger.debug(
                f"Document {document.entity_id}: Already has a search index"
            )
            return

        if not document.can_search_index():
            self.logger.debug(
                msg=f"Document {document.entity_id}: Cannot make search index"
            )
            return

        if delayed or not document_repo.is_rate_limited(
            "command:set_search_index"
        ):
            document.set_search_index()
        else:
            document.set_search_index_delayed()
        return

    @validate_call
    def set_search_terms_for_document(self, document_uuid: UUID) -> None:
        """
        Extract a document's text and store it in the document's indexed
        search field.
        """

        if not self.user_info:
            self.logger.debug("No user info, cannot set search terms")
            return

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid_unsafe(
            document_uuid=document_uuid
        )
        self._set_search_index_for_document(document_repo, document)

        document_repo.save()
        return

    @validate_call
    def set_search_terms_for_document_delayed(
        self, document_uuid: UUID
    ) -> None:
        """
        Extract a document's text and store it in the document's indexed
        search field.
        """

        if not self.user_info:
            self.logger.debug("No user info, cannot set search terms")
            return

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid_unsafe(
            document_uuid=document_uuid
        )
        self._set_search_index_for_document(document_repo, document, True)

        document_repo.save()
        return

    @validate_call
    def set_search_terms_for_documents_for_file(self, file_uuid: UUID) -> None:
        """
        Extract a document's text and store it in the document's indexed
        search field, for each document linked to a specific file.
        """

        if not self.user_info:
            self.logger.debug("No user info, cannot set search terms")
            return

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        documents = document_repo.get_documents_by_file_uuid_unsafe(
            file_uuid=file_uuid
        )

        for document in documents:
            self._set_search_index_for_document(document_repo, document)

        document_repo.save()
        return

    @validate_call
    def create_pdf_for_case_registration(
        self, case_uuid: UUID, fields: CreateCaseRegistrationPDFFields
    ) -> None:
        """Create a PDF copy of the registration form, with the values entered
        by the requestor of the case"""

        case_repo = cast(CaseRepository, self.get_repository("case"))

        case = case_repo.get_case_by_uuid_unsafe(case_uuid=case_uuid)

        if not case.needs_registration_form:
            self.logger.debug(
                f"Case {case.id} ({case_uuid}) does not need a registration PDF"
            )
            return

        case.request_registration_form(fields)

        case_repo.save(user_info=self.user_info)

    @validate_call
    def upload_registration_form(
        self,
        local_filename: str,
        name: str,
        case_uuid: UUID,
        magic_string: str,
    ):
        assert self.user_info, "No user info, cannot upload registration form"

        document_uuid = uuid4()

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )

        document = document_repo.create_document_v2(
            user_info=self.user_info,
            document_uuid=document_uuid,
            local_filename=local_filename,
            name=name,
            case_uuid=case_uuid,
            auto_accept=False,
        )

        labels = label_repo.get_document_labels_for_case_magicstrings(
            case_uuid=case_uuid,
            user_uuid=self.user_uuid,
            magic_strings=[magic_string],
            is_system=True,
        )

        # Accept the document as a new document (not a new version of an existing document)
        document.accept_document(as_version_of=None)
        document.apply_labels(labels=labels, case_uuid=case_uuid)

        document_repo.save(self.user_info)

    @validate_call
    def acquire_document_lock_for_wopi(
        self,
        document_uuid: UUID,
        user_uuid: UUID | None = None,
        user_display_name: str | None = None,
        # Default: lock for 30 minutes (as per MS WOPI docs)
        lock_seconds: int = 30 * 60,
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))

        document = docu_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )
        document.acquire_lock(
            user_uuid=user_uuid,
            user_display_name=user_display_name,
            timestamp=datetime.datetime.now(tz=datetime.UTC)
            + timedelta(seconds=lock_seconds),
            shared=True,
        )
        docu_repo.save()

    @validate_call
    def extend_document_lock_for_wopi(
        self,
        document_uuid: UUID,
        user_uuid: UUID,
        # Default: extend lock for 30 minutes (as per MS WOPI docs)
        lock_seconds: int = 30 * 60,
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))

        document = docu_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        is_locked = docu_repo.is_locked(document_lock=document.lock)

        if is_locked is True:
            document.extend_lock(
                timestamp=datetime.datetime.now(tz=datetime.UTC)
                + timedelta(seconds=lock_seconds),
                shared=True,
            )
        else:
            raise Conflict(
                "File is not locked.",
                "file/lock/not_locked",
            )

        docu_repo.save()

    def release_document_lock_for_wopi(
        self, document_uuid: UUID, user_uuid: UUID
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        document = docu_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )
        document.release_lock(
            user_uuid=None,
            user_display_name=None,
            timestamp=None,
            shared=None,
        )

        docu_repo.save()
