# SPDX-FileCopyrightText: xxllnc Zaakgericht B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, ValueObject
from pydantic.v1 import Field
from typing import Any, Literal
from uuid import UUID


class CustomField(ValueObject):
    phase_number: int
    magic_string: str
    value_type: str
    is_internal: bool
    label: str


class CaseType(ValueObject):
    label: str
    needs_registration_form: bool
    custom_fields: list[CustomField]


class Case(Entity):
    entity_type: Literal["case"] = "case"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., description="Unique identifier of this case")
    id: int = Field(..., description="Numeric id of the case")

    case_type: CaseType = Field(..., description="Details from the case type")

    field_values: dict[str, Any] | None = Field(
        default=None, description="Custom field values"
    )

    @property
    def needs_registration_form(self) -> bool:
        # logic to determine whether a PDF needs to be made goes here
        return self.case_type.needs_registration_form

    @Entity.event(
        name="RegistrationFormRequested",
        fire_always=False,
        extra_fields=(
            "id",
            "case_type",
        ),
    )
    def request_registration_form(self, fields: dict[str, Any]):
        self.field_values = fields
