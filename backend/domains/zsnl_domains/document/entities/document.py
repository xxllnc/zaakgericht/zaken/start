# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from . import shared
from .document_label import DocumentLabel
from .shared import (
    ALLOWED_MIME_TYPES,
    ALLOWED_MIME_TYPES_EXTENDED,
    ThumbnailCreationMethod,
)
from collections.abc import Sequence
from datetime import date, datetime
from dateutil.tz import gettz
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict, Forbidden
from typing import Final, Literal
from typing_extensions import TypedDict
from uuid import UUID

logger = logging.getLogger(__name__)

MAX_SIZE_FOR_PREVIEW: Final[int] = 200 * 1024 * 1024  # 200 Mebibytes
MAX_SIZE_FOR_THUMBNAIL: Final[int] = MAX_SIZE_FOR_PREVIEW
MAX_SIZE_FOR_SEARCH_TERMS: Final[int] = MAX_SIZE_FOR_PREVIEW

PREVIEWABLE_MIMETYPES: Final[set[tuple[str, str]]] = {
    # Keys of ALLOWED_MIMETYPES are tuples of the form (extension, mimetype)
    k
    for k, v in (ALLOWED_MIME_TYPES | ALLOWED_MIME_TYPES_EXTENDED).items()
    if v["previewable"]
}
THUMBNAILABLE_MIMETYPES_DIRECT: Final[set[tuple[str, str]]] = {
    k
    for k, v in (ALLOWED_MIME_TYPES | ALLOWED_MIME_TYPES_EXTENDED).items()
    if v["thumbnailable"] == ThumbnailCreationMethod.direct
}
THUMBNAILABLE_MIMETYPES_FROM_PREVIEW: Final[set[tuple[str, str]]] = {
    k
    for k, v in (ALLOWED_MIME_TYPES | ALLOWED_MIME_TYPES_EXTENDED).items()
    if v["thumbnailable"] == ThumbnailCreationMethod.from_preview
}
EXTRACT_SEARCH_TERMS: Final[set[tuple[str, str]]] = {
    k
    for k, v in (ALLOWED_MIME_TYPES | ALLOWED_MIME_TYPES_EXTENDED).items()
    if v["extract_search_terms"]
}

DocumentCategory = Literal[
    "Aangifte",
    "Aanmaning",
    "Aanmelding",
    "Aanvraag",
    "Advies",
    "Afbeelding",
    "Afmelding",
    "Afspraak",
    "Agenda",
    "Akte",
    "Bankgarantie",
    "Begroting",
    "Bekendmaking",
    "Beleidsdocument",
    "Benoeming",
    "Berekening",
    "Beroepschrift",
    "Beschikking",
    "Besluit",
    "Besluitenlijst",
    "Bestek",
    "Bestemmingsplan",
    "Betaalafspraak",
    "Betalingsherinnering",
    "Bevestiging",
    "Bezwaarschrift",
    "Brief",
    "Brochure",
    "Catalogus",
    "Checklist",
    "Circulaire",
    "Declaratie",
    "Dwangbevel",
    "Factuur",
    "Film",
    "Foto",
    "Garantiebewijs",
    "Geluidsfragment",
    "Gespreksverslag",
    "Gids",
    "Grafiek",
    "Herinnering",
    "Identificatiebewijs",
    "Kaart",
    "Kennisgeving",
    "Klacht",
    "Lastgeving",
    "Mededeling",
    "Melding",
    "Norm",
    "Nota",
    "Notitie",
    "Offerte",
    "Ontvangstbevestiging",
    "Ontwerp",
    "Opdracht",
    "Overeenkomst",
    "Pakket Van Eisen",
    "Persbericht",
    "Plan",
    "Plan Van Aanpak",
    "Polis",
    "Procesbeschrijving",
    "Proces-verbaal",
    "Publicatie",
    "Rapport",
    "Regeling",
    "Register",
    "Rooster",
    "Ruimtelijk plan",
    "Sollicitatiebrief",
    "Statistische opgave",
    "Taxatierapport",
    "Technische tekening",
    "Tekening",
    "Uitnodiging",
    "Uitspraak",
    "Uittreksel",
    "Vergaderverslag",
    "Vergunning",
    "Verklaring",
    "Verordening",
    "Verslag",
    "Verslag van bevindingen",
    "Verspreidingslijst",
    "Verweerschrift",
    "Verzoek",
    "Verzoekschrift",
    "Voordracht",
    "Voorschrift",
    "Voorstel",
    "Wet",
    "Zienswijze",
]
DocumentConfidentiality = Literal[
    "Openbaar",
    "Beperkt openbaar",
    "Intern",
    "Zaakvertrouwelijk",
    "Vertrouwelijk",
    "Confidentieel",
    "Geheim",
    "Zeer geheim",
]
DocumentOrigin = Literal["Uitgaand", "Intern", "Inkomend"]
DocumentStatus = Literal["Origineel", "Kopie", "Vervangen", "Geconverteerd"]


class DocumentPublishSettings(TypedDict):
    pip: bool
    website: bool


class Document(EntityBase):
    extension: str | None
    mimetype: str | None

    @property
    def entity_id(self):
        return self.document_uuid

    def __init__(
        self,
        document_uuid,
        basename=None,
        extension=None,
        store_uuid=None,
        directory_uuid=None,
        case_uuid=None,
        case_display_number=None,
        mimetype=None,
        size=None,
        storage_location=None,
        md5=None,
        is_archivable=None,
        virus_scan_status=None,
        accepted=None,
        date_modified=None,
        thumbnail=None,
        creator_uuid=None,
        creator_displayname=None,
        attachment_uuid=None,
        message_uuid=None,
        creator_type=None,
        output_format="pdf",
        preview_uuid=None,
        preview_storage_location=None,
        preview_mimetype=None,
        labels=None,
        lock=None,
        origin=None,
        origin_date=None,
        description=None,
        confidentiality=None,
        document_category=None,
        document_source=None,
        document_number=None,
        current_version=None,
        integrity_check_successful=None,
        destroy_reason=None,
        rejection_reason=None,
        rejected_by_display_name=None,
        intake_owner_uuid=None,
        intake_group_uuid=None,
        intake_role_uuid=None,
        skip_intake=False,
        thumbnail_uuid=None,
        thumbnail_storage_location=None,
        thumbnail_mimetype=None,
        auto_accept=False,
        explicit_accept=False,
        has_search_index=False,
        publish: DocumentPublishSettings | None = None,
        status: DocumentStatus | None = None,
        pronom_format: str | None = None,
        appearance: str | None = None,
        structure: str | None = None,
    ):
        self.document_uuid = document_uuid
        self.basename = basename
        self.extension = extension
        self.store_uuid = store_uuid

        self.directory_uuid = directory_uuid
        self.case_uuid = case_uuid
        self.case_display_number = case_display_number
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable: bool | None = is_archivable
        self.virus_scan_status = virus_scan_status
        self.accepted = accepted
        self.date_modified = date_modified
        self.thumbnail = thumbnail
        self.creator_uuid = creator_uuid
        self.creator_displayname = creator_displayname
        self.attachment_uuid = attachment_uuid
        self.message_uuid = message_uuid
        self.creator_type = creator_type
        self.output_format = output_format

        self.preview_uuid = preview_uuid
        self.preview_storage_location = preview_storage_location
        self.preview_mimetype = preview_mimetype

        self.labels = labels
        self.lock = lock
        self.origin: DocumentOrigin | None = origin
        self.origin_date: date | None = origin_date
        self.description: str | None = description
        self.confidentiality: DocumentConfidentiality | None = confidentiality
        self.document_category: DocumentCategory | None = document_category

        self.document_number = document_number
        self.current_version = current_version

        self.integrity_check_successful = integrity_check_successful
        self.rejection_reason = rejection_reason
        self.rejected_by_display_name = rejected_by_display_name
        self.intake_owner_uuid = intake_owner_uuid
        self.intake_group_uuid = intake_group_uuid
        self.intake_role_uuid = intake_role_uuid
        self.skip_intake = skip_intake

        self.thumbnail_uuid = thumbnail_uuid
        self.thumbnail_storage_location = thumbnail_storage_location
        self.thumbnail_mimetype = thumbnail_mimetype
        self.auto_accept = auto_accept
        self.explicit_accept = explicit_accept

        self.has_search_index = has_search_index

        self.publish: DocumentPublishSettings = (
            publish or DocumentPublishSettings(pip=False, website=False)
        )
        self.document_source: str | None = document_source
        self.status: DocumentStatus | None = status

        self.pronom_format: str | None = pronom_format
        self.appearance: str | None = appearance
        self.structure: str | None = structure

    @event("DocumentCreated")
    def create(
        self,
        basename: str,
        extension: str,
        accepted: bool,
        store_uuid: UUID,
        directory_uuid: UUID | None,
        case_uuid: UUID,
        mimetype: str,
        size: int,
        storage_location: str,
        md5: str,
        creator_type: str = "employee",
        skip_intake: bool = False,
        auto_accept: bool = False,
        virus_scan_status: str = "pending",
        accept_extended_mimetypes: bool = False,
    ):
        if self.store_uuid is not None:
            raise Conflict(
                "Can't create an already existing document",
                "document/already_exists",
            )

        if basename == "" or basename.startswith("."):
            raise Forbidden(
                "File name may not start with '.'", "file/starts_with_dot"
            )

        self.basename = basename
        self.extension = extension
        self.accepted = accepted
        self.store_uuid = store_uuid
        self.directory_uuid = directory_uuid
        self.case_uuid = case_uuid
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = shared.is_archivable(
            extension, mimetype, accept_extended_mimetypes
        )
        self.virus_scan_status = virus_scan_status
        self.creator_type = creator_type
        self.skip_intake = skip_intake
        self.auto_accept = auto_accept
        self.labels = []

    @event("DocumentFromAttachmentCreated")
    def create_document_from_attachment(self, attachment_uuid):
        self.attachment_uuid = attachment_uuid

    @event(
        "DocumentAddedToCase",
        extra_fields=[
            "md5",
            "size",
            "basename",
            "extension",
            "mimetype",
            "is_archivable",
            "description",
            "origin",
            "origin_date",
        ],
    )
    def add_to_case(self, case_uuid: UUID):
        if self.case_uuid:
            raise Conflict(
                f"Document with uuid '{self.document_uuid}' is already linked to a case with uuid '{self.case_uuid}'.",
                "document/already_linked_to_a_case",
            )
        self.case_uuid = case_uuid

    @event("DocumentUpdated")
    def update(
        self,
        basename: str | None,
        description: str | None,
        document_category: DocumentCategory | None,
        origin: DocumentOrigin | None,
        origin_date: date | None,
        confidentiality: DocumentConfidentiality | None,
        document_source: str | None,
        publish_settings: DocumentPublishSettings | None = None,
        status: DocumentStatus | None = None,
        pronom_format: str | None = None,
        appearance: str | None = None,
        structure: str | None = None,
    ):
        today = datetime.now(tz=gettz("Europe/Amsterdam")).date()
        self.basename = basename if basename is not None else self.basename
        self.description = (
            description if description is not None else self.description
        )
        self.document_source = (
            document_source
            if document_source is not None
            else self.document_source
        )
        self.pronom_format = (
            pronom_format if pronom_format is not None else self.pronom_format
        )
        self.appearance = (
            appearance if appearance is not None else self.appearance
        )
        self.structure = structure if structure is not None else self.structure

        self.status = status or self.status
        self.document_category = document_category or self.document_category
        self.origin = origin or self.origin
        self.origin_date = origin_date or today
        self.confidentiality = confidentiality or self.confidentiality

        self.publish = publish_settings or self.publish

    @event("DocumentDeleted", extra_fields=["destroy_reason"])
    def delete(self, reason=""):
        """
        Permanently deletes a document. Works for documents not assigned to a case (document-intake).
        ToDo: When deleting from Document Tab (so assigned to a case) allow deletion of documents in Trash Bin.
        ToDo: So also make a trash_document method that puts it in the Trash Bin.
        :return:
        """
        if self.case_uuid is not None:
            raise Conflict(
                f"Document '{self.document_uuid}' is assign to case '{self.case_uuid}'",
                "document/delete_document/assigned_to_case",
            )

        self.destroy_reason = reason

    @event("DocumentAssignedToUser")
    def assign_document_to_user(self, intake_owner_uuid):
        self.intake_owner_uuid = intake_owner_uuid
        self.rejection_reason = None

    @event("DocumentAssignedToRole")
    def assign_document_to_role(self, intake_group_uuid, intake_role_uuid):
        self.intake_group_uuid = intake_group_uuid
        self.intake_role_uuid = intake_role_uuid
        self.rejection_reason = None

    @event("DocumentAssignmentRejected")
    def reject_from_intake(self, rejection_reason: str):
        if [
            self.intake_owner_uuid,
            self.intake_group_uuid,
            self.intake_role_uuid,
        ].count(None) == 3:
            raise Conflict(
                "Cannot reject a document that is not assigned",
                "document/reject_unassigned",
            )

        if self.case_uuid is not None:
            raise Conflict(
                "Cannot reject a document that is linked to a case",
                "document/reject_case_document",
            )

        self.intake_owner_uuid = None
        self.intake_group_uuid = None
        self.intake_role_uuid = None
        self.rejection_reason = rejection_reason

    @event("LabelsApplied", extra_fields=["case_uuid"])
    def apply_labels(self, labels: Sequence[DocumentLabel], case_uuid: UUID):
        existing_label_uuids = [str(label["uuid"]) for label in self.labels]
        new_labels = [*self.labels]

        for label in labels:
            if str(label.uuid) not in existing_label_uuids:
                new_labels.append(label)

        self.labels = new_labels
        self.case_uuid = case_uuid

    @event("LabelsRemoved", extra_fields=["case_uuid"])
    def remove_labels(self, labels: Sequence[DocumentLabel], case_uuid: UUID):
        new_labels = []

        labels_to_remove = [str(label.uuid) for label in labels]

        for label in self.labels:
            if str(label["uuid"]) not in labels_to_remove:
                new_labels.append(label)

        self.labels = new_labels
        self.case_uuid = case_uuid

    @event("DocumentMoved")
    def move_to_directory(self, directory_uuid):
        self.directory_uuid = directory_uuid

    @event(
        "DocumentAccepted",
        extra_fields=[
            "md5",
            "size",
            "basename",
            "extension",
            "mimetype",
            "is_archivable",
            "description",
            "origin",
            "origin_date",
            "case_uuid",
            "labels",
            "document_number",
        ],
    )
    def accept_document(self, as_version_of: UUID | None = None):
        if self.accepted:
            raise Conflict(
                "Cannot accept an already accepted document",
                "document/already_accepted",
            )
        if not self.case_uuid:
            raise Conflict(
                "Cannot accept a document that is not in a case",
                "document/not_in_case",
            )

        self.accepted = True

        if as_version_of:
            self.document_uuid = as_version_of

    @property
    def _extension_mimetype_key(self) -> tuple[str, str]:
        extension = self.extension or ""
        extension = extension.lower()

        return (extension, self.mimetype or "unknown/unknown")

    def can_preview(self) -> bool:
        if self.size is None:
            raise Conflict("Document size is undefined")

        if (
            self.virus_scan_status == "ok"
            and self.size <= MAX_SIZE_FOR_PREVIEW
            and self._extension_mimetype_key in PREVIEWABLE_MIMETYPES
        ):
            return True

        return False

    def has_preview(self) -> bool:
        if (
            self.preview_mimetype
            and self.preview_uuid
            and self.preview_storage_location
        ):
            return True

        # PDF files are their own preview
        if self.mimetype == "application/pdf":
            return True

        return False

    def can_thumbnail(self) -> bool:
        if self.size is None:
            raise Conflict("Document size is undefined")

        extension_mimetype_key = self._extension_mimetype_key

        if (
            self.virus_scan_status == "ok"
            and self.size <= MAX_SIZE_FOR_THUMBNAIL
            and (
                extension_mimetype_key in THUMBNAILABLE_MIMETYPES_DIRECT
                or (
                    extension_mimetype_key
                    in THUMBNAILABLE_MIMETYPES_FROM_PREVIEW
                    and self.has_preview()
                )
            )
        ):
            return True

        return False

    def has_thumbnail(self) -> bool:
        if (
            self.thumbnail_mimetype
            and self.thumbnail_uuid
            and self.thumbnail_storage_location
        ):
            return True

        return False

    @event(
        "PreviewCreated",
        extra_fields=[
            "size",
            "extension",
            "mimetype",
            "store_uuid",
            "storage_location",
        ],
    )
    def create_preview(self):
        pass

    @event(
        "ThumbnailCreated",
        extra_fields=[
            "size",
            "extension",
            "mimetype",
            "store_uuid",
            "storage_location",
            "preview_uuid",
            "preview_storage_location",
            "preview_mimetype",
        ],
    )
    def create_thumbnail(self):
        pass

    def can_search_index(self) -> bool:
        if self.size is None:
            raise Conflict("Document size is undefined")

        if (
            self.virus_scan_status == "ok"
            and self.size <= MAX_SIZE_FOR_SEARCH_TERMS
            and (self.extension, self.mimetype) in EXTRACT_SEARCH_TERMS
            and not self.has_search_index
        ):
            return True

        return False

    @event("SearchIndexSetDelayed")
    def set_search_index_delayed(self):
        pass

    @event(
        "SearchIndexSet",
        extra_fields=[
            "size",
            "extension",
            "mimetype",
            "store_uuid",
            "storage_location",
        ],
    )
    def set_search_index(self):
        pass

    @event(name="LockAcquired")
    def acquire_lock(self, user_uuid, user_display_name, timestamp, shared):
        lock = {
            "user_uuid": user_uuid,
            "user_display_name": user_display_name,
            "timestamp": timestamp,
            "shared": shared,
        }
        self.lock = lock

    @event(name="LockReleased")
    def release_lock(self, user_uuid, user_display_name, timestamp, shared):
        lock = {
            "user_uuid": user_uuid,
            "user_display_name": user_display_name,
            "timestamp": timestamp,
            "shared": shared,
        }
        self.lock = lock

    @event(name="LockExtended")
    def extend_lock(self, timestamp, shared):
        new_lock = self.lock
        new_lock["timestamp"] = timestamp
        new_lock["shared"] = shared
        self.lock = new_lock


class File(EntityBase):
    @property
    def entity_id(self):
        return self.file_uuid

    def __init__(
        self,
        file_uuid,
        basename=None,
        extension=None,
        mimetype=None,
        size=None,
        storage_location=None,
        md5=None,
        is_archivable=None,
        virus_scan_status=None,
    ):
        self.file_uuid = file_uuid
        self.basename = basename
        self.extension = extension
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = is_archivable
        self.virus_scan_status = virus_scan_status

    @property
    def filename(self):
        return self.basename + self.extension

    @event("FileCreated")
    def create(
        self, basename, extension, mimetype, size, storage_location, md5
    ):
        if basename == "" or basename.startswith("."):
            raise Forbidden(
                "File name may not start with '.'", "file/starts_with_dot"
            )

        self.basename = basename
        self.extension = extension
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = shared.is_archivable(extension, mimetype)
        self.virus_scan_status = "pending"
