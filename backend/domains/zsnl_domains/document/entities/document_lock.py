# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class DocumentLock(Entity):
    """Document Lock entity."""

    entity_type = "document_lock"

    user_display_name: str | None = Field(..., title="Name of user")
    user_uuid: UUID | None = Field(..., title="Identifier for the user")
    timestamp: datetime | None = Field(..., title="Lock timestamp")
    case_id: int | None = Field(None, title="Internal identifier for the case")
    shared: bool | None = Field(None, title="Indication for shared lock")
