# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import pytz
from collections.abc import Iterable, Mapping
from datetime import datetime, timezone
from minty.entity import Entity, ValueObject
from pydantic.v1 import Field
from uuid import UUID

logger = logging.getLogger(__name__)


class MessageAttachment(ValueObject):
    """
    File attachments to a external message
    """

    filename: str = Field(..., title="The filename of the attachment")


class Contact(ValueObject):
    """
    Contact information of the sender of a external message
    """

    contact_type: str = Field(..., title="The type of the contact")


class MessageExternal(Entity):
    """
    Message External Entity
    """

    entity_type = "message_external"
    entity_id__fields: list[str] = ["id"]

    id: int = Field(..., title="The message_external id")
    case_id: int | None = Field(None, title="The case id")
    type: str = Field(..., title="The external message type")
    created_by_displayname: str | None = Field(
        "", title="The displayname of the sender"
    )
    subject: str = Field(..., title="The message subject")
    participants: Iterable[Mapping[str, str]] = Field(
        ..., title="The message participants"
    )
    message_date: datetime | None = Field(
        None, title="The datetime the external message was created"
    )
    created: datetime = Field(
        ...,
        title="The datetime when the database entry of the message was created",
    )
    content: str = Field(..., title="The message content")

    source_file_uuid: UUID | None = Field(
        None, title="The UUID of the original source file"
    )
    attachments: list[MessageAttachment] | None = Field(
        [], title="Optional list of attachments"
    )

    def get_email_as_bytes(self) -> bytes:
        """
        Transform an external email message as bytes for
        storage or conversion.
        :return: bytes
        """
        msg_txt = ""
        msg_txt += f"E-Mailbericht Zaak {self.case_id}\n"
        msg_txt += "-" * 80 + "\n\n"

        msg_txt += self.get_participants_as_text_header()

        display_date = self.message_date

        display_date = display_date.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )
        display_date = display_date.strftime("%d-%m-%Y om %H:%M:%S")

        msg_txt += f"{'Datum:':<7}{display_date}\n\n"

        msg_txt += f"Onderwerp: {self.subject}\n"
        msg_txt += f"\n{self.content}\n"

        msg_txt += self.get_attachment_list_as_text()

        return bytes(msg_txt, "utf-8")

    def get_participants_as_text_header(self):
        """
        Parse the participants in the external message
        and create a human readable text header for use
        in conversion or storage of the email.

        :return: str
        """
        from_header = list()
        to_header = list()
        cc_header = list()
        bcc_header = list()

        msg_txt = ""

        for p in self.participants:
            if p["role"] == "from":
                from_entry = (
                    p["address"] + " " + f"({p['display_name']})"
                    if p["display_name"] != ""
                    and p["address"] != p["display_name"]
                    else p["address"]
                )
                from_header.append(from_entry)

            elif p["role"] == "to":
                to_entry = (
                    p["address"] + " " + f"({p['display_name']})"
                    if p["display_name"] != ""
                    and p["address"] != p["display_name"]
                    else p["address"]
                )
                to_header.append(to_entry)

            elif p["role"] == "cc":
                cc_entry = (
                    p["address"] + " " + f"({p['display_name']})"
                    if p["display_name"] != ""
                    and p["address"] != p["display_name"]
                    else p["address"]
                )
                cc_header.append(cc_entry)

            elif p["role"] == "bcc":
                bcc_entry = (
                    p["address"] + " " + f"({p['display_name']})"
                    if p["display_name"] != ""
                    and p["address"] != p["display_name"]
                    else p["address"]
                )
                bcc_header.append(bcc_entry)

        if len(from_header) > 0:
            msg_txt += f"{'Van:':<7}"
            msg_txt += ", ".join(from_header)
            msg_txt += "\n\n"

        if len(to_header) > 0:
            msg_txt += f"{'Aan:':<7}"
            msg_txt += ", ".join(to_header)
            msg_txt += "\n\n"

        if len(cc_header) > 0:
            msg_txt += f"{'CC:':<7}"
            msg_txt += ", ".join(cc_header)
            msg_txt += "\n\n"

        if len(bcc_header) > 0:
            msg_txt += f"{'BCC:':<7}"
            msg_txt += ", ".join(bcc_header)
            msg_txt += "\n\n"

        return msg_txt

    def get_pip_message_as_bytes(self, contact: Contact | None) -> bytes:
        """
        Create a Bytes utf-8 encoded message flat .txt
        from a PIP-external message Message, to save in the file
        infrastructure and reference from the filestore.
        :param message: The message
        :param contact: The contact information of the message creator
        :return: The Bytes utf-8 encoded message
        """
        display_date = self.message_date

        display_date = display_date.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )
        display_date = display_date.strftime("%d-%m-%Y om %H:%M:%S")

        from_role = self.get_pip_message_sender_role(contact)
        to_role = self.get_pip_message_recipient_role(from_role)

        txt_message = ""
        txt_message = ""
        txt_message += f"PIP bericht Zaak {self.case_id}\n"
        txt_message += "-" * 80 + "\n\n"

        txt_message += (
            f"{'Van:':<7}{self.created_by_displayname} ({from_role})\n\n"
        )
        txt_message += f"{'Aan:':<7}{to_role}\n\n"
        txt_message += f"{'Datum:':<7}{display_date}\n\n"
        txt_message += f"Onderwerp: {self.subject}\n"
        txt_message += f"\n{self.content}\n"

        txt_message += self.get_attachment_list_as_text()

        return bytes(txt_message, "utf-8")

    def get_pip_message_sender_role(self, contact: Contact | None) -> str:
        """
        :param the Contact object of the message_creator

        Get the role of the PIP message sender. PIP messages are sent by
        users of zaaksysteem internal (employee) which makes his role
        "Behandelaar" or by PIP users (person, or organization) which
        makes the role "Aanvrager". If there is no Contact information
        we can assume that is sent by the Aanvrager.
        """

        if contact and contact.contact_type == "employee":
            role = "Behandelaar"
        else:
            role = "Aanvrager"

        return role

    @staticmethod
    def get_pip_message_recipient_role(sender_role: str = None) -> str:
        """
        For a PIP message we can assume that if the sender is 'Aanvrager'
        the recipient role will be 'Behandelaar'. And if the sender role
        is unknown the recipient role will be 'Aanvrager'

        :return: str
        """
        if sender_role == "Aanvrager":
            recipient_role = "Behandelaar"
        else:
            recipient_role = "Aanvrager"

        return recipient_role

    def get_attachment_list_as_text(self) -> str:
        msg_txt = "\nBijlagen: geen"

        if len(self.attachments) > 0:
            msg_txt = "\nBijlagen:\n"

            for a in self.attachments:
                msg_txt += f"{a.filename}\n"

        return msg_txt
