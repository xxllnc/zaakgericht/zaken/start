# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from minty.exceptions import Forbidden
from typing import Final, TypedDict


class ThumbnailCreationMethod(enum.Enum):
    none = enum.auto()
    direct = enum.auto()
    from_preview = enum.auto()


class AllowedMimeTypeSetting(TypedDict):
    archivable: bool
    previewable: bool
    thumbnailable: ThumbnailCreationMethod
    extract_search_terms: bool


OPENXML_BASE: Final[str] = "application/vnd.openxmlformats-officedocument."
ALLOWED_MIME_TYPES: Final[dict[tuple[str, str], AllowedMimeTypeSetting]] = {
    (".3gp", "audio/3gpp"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".7bdat", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".7z", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".aac", "audio/x-aac"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".aac", "audio/x-hx-aac-adts"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".accdb", "application/msaccess"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ai", "application/postscript"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ai", "application/pdf"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".aif", "audio/x-aiff"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".aiff", "audio/x-aiff"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".asc", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".avi", "video/x-msvideo"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".avi", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".bmp", "image/bmp"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".bmp", "image/x-ms-bmp"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".cer", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".cer", "application/x-pem-file"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".cer", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".crt", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".crt", "application/x-pem-file"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".crt", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".csr", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".css", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".csv", "application/csv"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".csv", "text/csv"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".csv", "text/plain"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".dae", "text/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dae", "model/vnd.collada+xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dbf", "application/x-dbf"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dbf", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dcm", "application/dicom"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".der", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".doc", "application/msword"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".doc", "application/x-ole-storage"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".doc", "application/CDFV2"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".docm", "application/vnd.ms-word.document.macroEnabled.12"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (
        ".docx",
        OPENXML_BASE + "wordprocessingml.document",
    ): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".dot", "application/msword"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".dotm", "application/vnd.ms-word.template.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (
        ".dotx",
        OPENXML_BASE + "wordprocessingml.template",
    ): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".dta", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dwg", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dwg", "image/vnd.dwg"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dxf", "text/plain"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dxf", "image/vnd.dxf"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".eml", "message/rfc822"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".eml", "text/plain"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".eml", "text/html"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".eps", "application/postscript"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".fbx", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".flac", "audio/flac"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".gif", "image/gif"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".gml", "application/gml+xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".gml", "application/gml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".gml", "application/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".gml", "text/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".hdf", "application/x-hdf"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".heic", "image/heic"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".heic", "image/heif"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".heif", "image/heic"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".heif", "image/heif"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".html", "text/html"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".html", "text/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".htm", "text/html"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".indd", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".jfif", "image/jpeg"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".jfif", "image/pjpeg"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".jpeg", "image/jpeg"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".jpe", "image/jpeg"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".jpg", "image/jpeg"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".jp2", "image/jp2"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".json", "application/json"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".json", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".key", "application/x-pem-file"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".key", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".kml", "application/vnd.google-earth.kml+xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".kml", "text/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mdb", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mid", "audio/midi"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mid", "application/x-mid"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mid", "text/plain"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mif", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mov", "video/quicktime"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mkv", "video/x-matroska"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mp3", "audio/mpeg"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mp4", "video/mp4"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".m4a", "audio/x-m4a"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mpeg", "video/mpeg"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".mpg", "video/mpeg"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".msg", "application/vnd.ms-outlook"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".msg", "application/x-ole-storage"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".msg", "application/vnd.ms-office"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".msg", "application/CDFV2"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".numbers", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".obj", "text/plain"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".obj", "model/obj"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".odf", "application/vnd.oasis.opendocument.text"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".odp", "application/vnd.oasis.opendocument.presentation"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".ods", "application/vnd.oasis.opendocument.spreadsheet"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".odt", "application/vnd.oasis.opendocument.text"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".ogg", "audio/ogg"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".pages", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".pdf", "application/pdf"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".pem", "application/x-pem-file"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".pem", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".png", "image/png"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".por", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".pot", "application/vnd.ms-powerpoint"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (
        ".potx",
        OPENXML_BASE + "presentationml.template",
    ): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ppa", "application/vnd.ms-powerpoint"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".pps", "application/vnd.ms-powerpoint"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ppsx", OPENXML_BASE + "presentationml.slideshow"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".ppt", "application/vnd.ms-powerpoint"): {
        "archivable": False,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".pptm", OPENXML_BASE + "presentation.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".pptx", OPENXML_BASE + "presentationml.presentation"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".psd", "image/vnd.adobe.photoshop"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".rtf", "application/rtf"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".rtf", "text/rtf"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".sas7bdat", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".sav", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".shp", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".sldx", OPENXML_BASE + "presentationml.slide"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".sql", "text/plain"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".sql", "application/x-sql"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".svg", "image/svg+xml"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": False,
    },
    (".tab", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".tiff", "image/tiff"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".tif", "image/tiff"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.direct,
        "extract_search_terms": True,
    },
    (".txt", "text/plain"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".vsdx", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".wav", "audio/wav"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".webm", "video/webm"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".x3d", "model/x3d+xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xhtml", "application/xhtml+xml"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".xls", "application/vnd.ms-excel"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".xls", "application/x-ole-storage"): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xlsm", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (
        ".xlsx",
        OPENXML_BASE + "spreadsheetml.sheet",
    ): {
        "archivable": True,
        "previewable": True,
        "thumbnailable": ThumbnailCreationMethod.from_preview,
        "extract_search_terms": True,
    },
    (".xlt", "application/vnd.ms-excel"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xltm", "application/vnd.ms-excel.template.macroEnabled.12"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (
        ".xltx",
        OPENXML_BASE + "spreadsheetml.template",
    ): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xml", "application/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xml", "text/xml"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xps", "application"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".xps", "application/vnd.ms-xpsdocument"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".zip", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".zip", "application/zip"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ztb", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ztb", "application/zip"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
}

ALLOWED_MIME_TYPES_EXTENDED: Final[
    dict[tuple[str, str], AllowedMimeTypeSetting]
] = {
    (".m4a", "audio/mp4"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".7z", "application/x-7z-compressed"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".zip", "application/x-zip-compressed"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".ics", "text/calendar"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".html", "text/partial-html"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".pdf", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".msg", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".docx", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".doc", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".jpg", "application/octet-stream"): {
        "archivable": True,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": True,
    },
    (".mso", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".zip", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dwg", "application/acad"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".dwg", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".url", "text/plain"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
    (".oxps", "application/octet-stream"): {
        "archivable": False,
        "previewable": False,
        "thumbnailable": ThumbnailCreationMethod.none,
        "extract_search_terms": False,
    },
}


def is_archivable(
    extension: str, mimetype: str, accept_extended_mimetypes: bool = False
) -> bool:
    """
    Returns whether a given extension + MIME type combination is considered
    "archivable".

    Raises an exception if the mime type/extension combination is not in
    our whitelist.
    """
    if accept_extended_mimetypes:
        allowed = ALLOWED_MIME_TYPES | ALLOWED_MIME_TYPES_EXTENDED
    else:
        allowed = ALLOWED_MIME_TYPES

    if (extension.lower(), mimetype) not in allowed:
        raise Forbidden(
            f"Unknown file extension or file type ({extension=}, {mimetype=}).",
            "file/type_not_allowed",
        )

    return allowed[(extension.lower(), mimetype)]["archivable"]
