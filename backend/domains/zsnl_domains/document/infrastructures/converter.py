# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import http.client
import io
import json
import os
import tempfile
import urllib
import urllib.error
import urllib.request
from minty import Base, exceptions
from typing import Any, BinaryIO, cast
from typing_extensions import deprecated


class ConverterWrapper(Base):
    def __init__(self, base_url):
        self.base_url = base_url

    def convert_file_handle(
        self,
        to_type: str,
        input_handle: BinaryIO,
        output_handle: BinaryIO,
        options: dict[str, Any] | None = None,
    ) -> None:
        """
        Convert a file from one mimetype to other.

        Generally, files can only be converted to "similar" formats:
        images to images, documents to documents, etc. An exception to this
        is PDF: most formats can be converted to PDF, and PDF can be turned
        into PNG.
        """

        # Base64 encode the file to a temporary file and put it in a data
        # structure that can be converted to JSON for the converter
        with tempfile.TemporaryFile() as encoded_content_handle:
            base64.encode(input_handle, encoded_content_handle)
            encoded_content_handle.seek(0, os.SEEK_SET)

            with io.TextIOWrapper(
                buffer=encoded_content_handle, encoding="ascii"
            ) as tempfile_text:
                values = {
                    "to_type": to_type,
                    "content": tempfile_text.read(),
                    "options": options or {},
                }

        # Write the JSON to a temp file (so we don't use the memory more than
        # once), and send build the HTTP request object
        with tempfile.TemporaryFile() as json_tempfile:
            json_tempfile_textmode = io.TextIOWrapper(
                buffer=json_tempfile, encoding="ascii"
            )
            json.dump(values, json_tempfile_textmode, ensure_ascii=True)
            json_tempfile_textmode.flush()
            json_tempfile_textmode.detach()

            json_tempfile.seek(0, os.SEEK_SET)

            url = self.base_url + "/v1/convert"
            req = urllib.request.Request(
                url=url,
                data=json_tempfile,
                headers={"Content-Type": "application/json"},
            )

            try:
                response = urllib.request.urlopen(req, timeout=60)
                content = cast(
                    bytes, json.load(response)["content"].encode("ascii")
                )
            except (
                urllib.error.HTTPError,
                urllib.error.URLError,
                http.client.HTTPException,
                OSError,
            ) as e:
                raise exceptions.Conflict(
                    "Error during document conversion"
                ) from e

        base64.decode(io.BytesIO(content), output_handle)
        return

    @deprecated("Use convert_file_handle, it waste less memory")
    def convert(
        self,
        to_type: str,
        content: bytes,
        options: dict[str, str] | None = None,
    ):
        """
        Convert a file from one mimetype to other.

        Generally, files can only be converted to "similar" formats:
        images to images, documents to documents, etc. An exception to this
        is PDF: most formats can be converted to PDF, and PDF can be turned
        into PNG.
        """

        with (
            tempfile.TemporaryFile() as output_handle,
            io.BytesIO(content) as input_handle,
        ):
            self.convert_file_handle(
                to_type=to_type,
                input_handle=input_handle,
                output_handle=output_handle,
                options=options,
            )

            output_handle.seek(0, os.SEEK_SET)

            return output_handle.read()


class ConverterInfrastructure(Base):
    """Infrastructure for file conversion"""

    def __call__(self, config):
        """Initialize ConverterWrapper"""
        return ConverterWrapper(base_url=config["converter_base_url"])
