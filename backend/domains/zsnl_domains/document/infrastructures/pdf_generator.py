# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import requests
from minty import Base
from pydantic import validate_call
from typing import Any
from typing_extensions import TypedDict


class EmptyDict(TypedDict): ...


class Address(TypedDict):
    human_value: str
    coordinates: str
    bag_id: str


class PDFTemplateValuesAttributes(TypedDict):
    name: str
    value: Any | None
    address: list[Address] | list[EmptyDict]
    type: str
    sequence: int


class PDFTemplateValuesMeta(TypedDict):
    customer: str
    casetype_name: str
    case_number: int
    date: str


class PDFTemplateValuesPreparedRequest(TypedDict):
    upload_url: str
    token: str


class PDFTemplateValues(TypedDict):
    attributes: list[PDFTemplateValuesAttributes]
    meta: PDFTemplateValuesMeta
    prepared_request: PDFTemplateValuesPreparedRequest


class PDFGenerator(Base):
    def __init__(
        self, host: str, user: str, password: str, modelid: int
    ) -> None:
        self.host = host
        self.user = user
        self.password = password
        self.modelid = modelid
        self.request_url = f"https://{self.host}/open?username={self.user}&password={self.password}&modelid={self.modelid}&fmt=json"
        super().__init__()

    @validate_call
    def generate_pdf(self, template_values: PDFTemplateValues) -> None:
        self.logger.debug(f"Created template_values:\n{template_values}")
        self._log_request_to_generator(template_values)
        response = requests.post(self.request_url, json=template_values)

        response.raise_for_status()
        self._log_response_from_generator(response)

    def _log_request_to_generator(
        self, template_values: PDFTemplateValues
    ) -> None:
        attributes = template_values.get("attributes", {})
        meta = template_values.get("meta", {})

        log_string = f"Sending request to pdf generator:\nattributes:\n{attributes}\nmeta:\n{meta}"
        self.logger.info(log_string)

    def _log_response_from_generator(
        self, response: requests.Response
    ) -> None:
        response_json = {}
        try:
            response_json = response.json()
        except requests.exceptions.JSONDecodeError:  # pragma: no cover
            # should never happen, if it starts happening chose what we wanna log for this
            self.logger.error("Did not get valid json back from pdf generator")

        if response_json:  # pragma: no cover # this is always true, if not there are bigger issues
            fields_to_keep: list[str] = [
                "sessionid",
                "dbname",
                "modelid",
                "uniqueid",
                "modelname",
                "modelversion",
                "modeldescription",
                "modelinfo",
            ]

            json_to_log = {}

            for key in fields_to_keep:
                value = response_json.get(key, "")
                json_to_log[key] = value

        self.logger.info(
            f"Shortened response from pdf generator:\n{json_to_log}"
        )


class PDFGeneratorInfrastructure(Base):
    """Infrastructure for pdf generation"""

    def __call__(self, config):
        host = config["pdf_host"]
        user = config["pdf_user"]
        password = config["pdf_password"]
        modelid = config["pdf_modelid"]
        return PDFGenerator(
            host=host, user=user, password=password, modelid=modelid
        )
