# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import requests
from minty import Base
from typing import BinaryIO


def must_skip_ocr(mime_type: str) -> bool:
    # Images need OCR to extact text, so don't skip it
    if mime_type.startswith("image/"):
        return False

    # Some (scanned) PDF files may be just pages of images. For those files,
    # OCR is needed. Tika is smart enough to only OCR when needed.
    if mime_type == "application/pdf":
        return False

    return True


class TikaWrapper(Base):
    def __init__(self, tika_base_url: str):
        self.tika_base_url = tika_base_url
        super().__init__()

    def get_text(self, mime_type: str, handle: BinaryIO):
        """
        Call Tika to extract the text from the document that can be read from
        `handle`.
        """
        url = self.tika_base_url + "/tika"

        response = requests.put(
            url=url,
            headers={
                "Accept": "text/plain",
                "Content-Type": mime_type,
                "X-Tika-OCRskipOcr": str(must_skip_ocr(mime_type)).lower(),
            },
            data=handle,
        )
        response.raise_for_status()

        return response.text


class TikaInfrastructure(Base):
    """Infrastructure for accessing Tika"""

    def __call__(self, config):
        return TikaWrapper(tika_base_url=config["tika_url"])
