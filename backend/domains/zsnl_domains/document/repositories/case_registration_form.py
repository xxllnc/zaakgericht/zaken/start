# SPDX-FileCopyrightText: xxllnc Zaakgericht B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from minty.cqrs import UserInfo
from minty_infra_storage import s3
from redis import Redis
from sqlalchemy import sql
from typing import cast
from uuid import UUID
from zsnl_domains import ZaaksysteemRepositoryBase
from zsnl_domains.database import schema
from zsnl_domains.document.entities.case_registration_form import (
    CaseRegistrationForm,
    CaseRegistrationFormDownload,
)
from zsnl_domains.document.repositories.case import (
    REGISTRATION_FORM_MAGIC_STRING,
)
from zsnl_domains.shared.repositories import case_acl

_c = schema.Case
_fcd = schema.FileCaseDocument
_fs = schema.Filestore
_f = schema.File


REGISTRATION_FORM_QUERY = (
    sql.select(
        _c.id, _fs.uuid, _fs.original_name, _fs.mimetype, _fs.storage_location
    )
    .select_from(
        sql.join(_c, _fcd, _fcd.case_id == _c.id)
        .join(_f, _f.id == _fcd.file_id)
        .join(_fs, _f.filestore_id == _fs.id)
    )
    .where(_fcd.magic_string == REGISTRATION_FORM_MAGIC_STRING)
)


class CaseRegistrationFormRepository(ZaaksysteemRepositoryBase):
    _for_entity: str = "CaseRegistrationForm"

    def _is_anonymous_token_present(
        self,
        case_uuid: UUID,
    ) -> bool:
        redis = cast(
            Redis,
            self._get_infrastructure("redis"),
        )
        configuration = self.infrastructure_factory.get_config(self.context)
        instance_identifier = configuration["instance_uuid"]
        token = f"{instance_identifier}:tokens:pdf_form:{case_uuid!s}"
        self.logger.debug(f"Checking for token: {token} in Redis")
        value = redis.get(token)
        if value:
            self.logger.debug(
                "Token found in Redis for anonymous user, query without acl check"
            )
            return True
        else:
            self.logger.debug(
                "No token found for this registration_form in Redis, returning None"
            )
            return False

    def get_case_by_uuid(
        self,
        case_uuid: UUID,
        user_info: UserInfo,
    ) -> CaseRegistrationForm | None | str:
        query = REGISTRATION_FORM_QUERY.where(_c.uuid == case_uuid)
        if user_info.permissions.get("anonymous", False):
            # if this is for anonymous_user check redis for temp auth token
            # if the token in present and the user is anonymous we SKIP ACL checks
            # this is to make this call work for prefilled requestors, anonymous users
            # we only skip the ACL checks if:
            #   - api user has a session object
            #   - api user has a stripped session object (no pip or __user field in)
            #     session data
            #   - if the token created for this specific case is present in Redis,
            #     that token expires after 15 minutes
            # This gives safety because a threat actor cannot brute force the uuid
            # in the 15 minute timeframe and other users still get the ACL checks
            # done
            if self._is_anonymous_token_present(case_uuid):
                # skip ACL checks
                pass
            else:
                # token not present return None
                return "Token expired or not present"

        elif user_info.permissions.get("pip_user", False):
            # for the get_registration_form and download_registration_form call
            # we only check if the caller is the requestor of the case not the
            # full acl checks if the caller is a pip_user,
            # this allows the query to return the
            # registration_form entity even when the case itself is hidden to
            # the user/requestor
            # the download button only shows up for the requestor and if someone
            # does the api call without being the requestor or gebruiker it fails
            query = query.where(
                case_acl.is_user_requestor_of_case_query(
                    user_uuid=user_info.user_uuid, case_uuid=case_uuid
                ),
            )
        elif user_info.permissions.get("gebruiker", False):
            query = query.where(
                case_acl.user_allowed_cases_subquery(
                    user_info, "read", case_alias=_c
                )
            )
        elif user_info.permissions.get("admin", False):
            # for admin do no acl check
            pass
        else:  # pragma: no cover
            # should not happen, but is placed here as a failsafe
            return
        row = self.session.execute(query).fetchone()

        if not row:
            # RegistrationForm does not yet exist, return None
            return

        return CaseRegistrationForm(
            uuid=case_uuid,
            document_uuid=row.uuid,
            id=row.id,
            entity_id=row.uuid,
            mimetype=row.mimetype,
            document_name=row.original_name,
            storage_location=row.storage_location[0],
            download_object=CaseRegistrationFormDownload(entity_id=case_uuid),
        )

    def get_download_link(
        self, case_registration_form: CaseRegistrationForm
    ) -> str:
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        return store.get_download_url(
            uuid=case_registration_form.document_uuid,
            storage_location=case_registration_form.storage_location,
            mime_type=case_registration_form.mimetype,
            filename=case_registration_form.document_name,
            download=True,
        )
