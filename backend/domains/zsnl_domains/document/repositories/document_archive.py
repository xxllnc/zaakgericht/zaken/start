# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import secrets
import tempfile
from ... import ZaaksysteemRepositoryBase
from ..entities import Directory, Document, DocumentArchive
from .directory import DirectoryRepository
from .document import DocumentRepository
from .file import FileRepository
from datetime import datetime, timedelta, timezone
from minty import exceptions
from sqlalchemy import sql
from uuid import UUID
from zipfile import ZIP_DEFLATED, ZipFile
from zsnl_domains.database import schema


class DocumentArchiveRepository(ZaaksysteemRepositoryBase):
    def get_request_for_event(self):
        return DocumentArchive(
            case_uuid=None,
            zip_all=False,
            document_uuids=[],
            directory_uuids=[],
            export_file_uuid=None,
            _event_service=self.event_service,
        )

    def store_export(
        self,
        user_info: minty.cqrs.UserInfo,
        case_uuid: UUID,
        upload_result: dict,
        filestore_uuid: UUID,
        export_file_uuid: UUID,
    ):
        # Insert details of uploaded file into filestore table.
        output_user_uuid = user_info.user_uuid
        today = datetime.now(timezone.utc)
        stringified_date = today.strftime("%Y%m%dT%H%M%S")
        filestore_id = self.session.execute(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": filestore_uuid,
                    "original_name": f"zs-export-zipfile-{case_uuid}-{stringified_date}.zip",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        ).inserted_primary_key[0]

        # Insert new export in export_queue table.
        self.session.execute(
            sql.insert(schema.ExportQueue).values(
                {
                    "uuid": export_file_uuid,
                    "subject_id": sql.select(schema.Subject.id)
                    .where(
                        sql.and_(
                            schema.Subject.uuid == output_user_uuid,
                            schema.Subject.subject_type == "employee",
                        )
                    )
                    .scalar_subquery(),
                    "subject_uuid": output_user_uuid,
                    "expires": today
                    + timedelta(
                        days=3
                    ),  # Will expire in 3 days from the date of creation
                    "token": secrets.token_hex(nbytes=32),
                    "filestore_id": filestore_id,
                    "filestore_uuid": filestore_uuid,
                    "downloaded": 0,
                },
            )
        )

    def create_zip(
        self,
        zip_file_uuid: UUID,
        document_uuids: list[UUID],
        docu_repo: DocumentRepository,
        file_repo: FileRepository,
        directory_repo: DirectoryRepository,
        user_info: minty.cqrs.UserInfo,
        case_uuid: UUID,
    ):
        all_folders = directory_repo.get_folders_for_case(case_uuid)

        folder_map: dict = {}
        for folder in all_folders:
            folder_map[folder.uuid] = folder
        folder_map_id: dict = {}
        for folder in all_folders:
            folder_map_id[folder.id] = folder
        all_folders_paths = self.get_folder_paths_for_case(folder_map_id)

        with (
            tempfile.NamedTemporaryFile() as file_handle,
            ZipFile(
                file_handle, "w", ZIP_DEFLATED, compresslevel=9
            ) as new_archive,
        ):
            folders = []

            for document_uuid in document_uuids:
                try:
                    document: Document = docu_repo.get_document_by_uuid(
                        document_uuid,
                        user_info=user_info,
                        integrity_check=True,
                    )

                    path = self.get_path_for_file(
                        document, folder_map, folder_map_id
                    )
                    folders.extend(path.split("/"))
                    filename_with_path = (
                        path + document.basename + document.extension
                    )

                    assert document.store_uuid
                    assert document.storage_location

                    file_handle_temp = file_repo.get_tempfile(
                        document.store_uuid,
                        document.storage_location[0],
                    )

                    new_archive.writestr(
                        filename_with_path, file_handle_temp.read()
                    )
                except exceptions.NotFound:
                    self.logger.info(
                        f"skipping zipping of document with uuid {document_uuid}"
                    )

            for folder in all_folders_paths:
                if folders.__contains__(folder.split("/")[0]):
                    new_archive.mkdir(folder)

            new_archive.close()

            # zip to S3
            return file_repo.upload_zip(
                file_handle=file_handle, file_uuid=zip_file_uuid
            )

    def get_path_for_file(
        self, document, directories_uuid, directories_id
    ) -> str:
        path: str = ""
        if document.directory_uuid is not None:
            directory: Directory = directories_uuid[document.directory_uuid]

            for folder_id in directory.path:
                if folder_id is not None:
                    path_folder = directories_id[int(folder_id)]
                    if path_folder.name is not None:
                        path = path + path_folder.name + "/"

            if directory.name is not None:
                path = path + directory.name + "/"

        return path

    def get_folder_paths_for_case(self, folder_map_id) -> list[str]:
        result: list[str] = []

        for directory in folder_map_id.values():
            folder = ""
            for path in directory.path:
                folder += folder_map_id[int(path)].name + "/"
            folder += directory.name + "/"
            result.append(folder)
        return result
