# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from sqlalchemy import cast, sql
from sqlalchemy import types as sqltypes
from zsnl_domains.database import schema


def get_user_display_name_and_id(session, user_uuid):
    if user_uuid is None:
        return {"id": None, "displayname": None}

    select_employee = sql.select(
        schema.Subject.id,
        cast(schema.Subject.properties, sqltypes.JSON)
        .op("->>")("displayname")
        .label("displayname"),
    ).where(
        sql.and_(
            schema.Subject.uuid == user_uuid,
            schema.Subject.subject_type == "employee",
        )
    )

    select_person = sql.select(
        schema.NatuurlijkPersoon.id,
        sql.func.concat_ws(
            " ",
            schema.NatuurlijkPersoon.voorletters,
            schema.NatuurlijkPersoon.voorvoegsel,
            schema.NatuurlijkPersoon.geslachtsnaam,
        ).label("displayname"),
    ).where(schema.NatuurlijkPersoon.uuid == user_uuid)

    select_organization = sql.select(
        schema.Bedrijf.id, schema.Bedrijf.handelsnaam.label("displayname")
    ).where(schema.Bedrijf.uuid == user_uuid)

    user_result = session.execute(
        sql.union_all(select_employee, select_person, select_organization)
    ).fetchone()

    return {"id": user_result.id, "displayname": user_result.displayname}


def insert_logging_and_message(session, insert_params):
    current_time = str(datetime.datetime.now())
    user_data = get_user_display_name_and_id(
        session, insert_params["user_uuid"]
    )

    exec_result = session.execute(
        sql.insert(schema.Logging).values(
            {
                "zaak_id": insert_params["case_id"],
                "component": "document",
                "component_id": insert_params["file_id"],
                "onderwerp": f'Document "{insert_params["name"]}" toegevoegd',
                "created": current_time,
                "last_modified": current_time,
                "event_type": "case/document/create",
                "event_data": {
                    "reason": 'Document "'
                    + insert_params["name"]
                    + '" toegevoegd'
                },
                "created_by": insert_params["created_by"],
                "created_by_name_cache": user_data["displayname"],
                "restricted": False,
            },
        )
    )
    logging_id = exec_result.inserted_primary_key[0]

    # The `str` is needed to get rid of a possible UUID type conversion problem
    if str(insert_params.get("case_assignee_uuid", None)) != str(
        insert_params["user_uuid"]
    ):
        user_data = get_user_display_name_and_id(
            session, insert_params.get("case_assignee_uuid", None)
        )
        exec_result = session.execute(
            sql.insert(schema.Message).values(
                {
                    "message": f"Document {insert_params['name']} aangemaakt",
                    "subject_id": "betrokkene-medewerker-"
                    + str(user_data["id"]),
                    "logging_id": logging_id,
                    "is_read": False,
                    "is_archived": False,
                },
            )
        )
