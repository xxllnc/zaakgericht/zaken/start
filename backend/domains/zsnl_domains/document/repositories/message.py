# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytz
import tempfile
from ... import ZaaksysteemRepositoryBase
from ..entities import Message, MessageExternal
from ..entities.message_external import Contact
from ..repositories.database_queries import (
    get_message_by_uuid,
    get_message_email_source_file_by_uuid,
)
from . import database_queries
from .shared import FileStoreSource, clean_filename_part
from datetime import timezone
from io import SEEK_SET
from minty.exceptions import Conflict, NotFound
from sqlalchemy import sql
from uuid import UUID, uuid4
from zsnl_domains.database import schema


class MessageRepository(ZaaksysteemRepositoryBase):
    """
    The message repository that manages interaction between entities
    and database operations.
    """

    def get_message_by_uuid(self, message_uuid):
        """Get message by uuid.

        :param message_uuid: UUID of the message.
        :type message_uuid: UUID
        :raises NotFound: When message with uuid not found.
        :return: Message
        :rtype: Message
        """
        result = self.session.execute(
            get_message_by_uuid(message_uuid=message_uuid)
        ).fetchone()

        if not result:
            raise NotFound(
                f"Message with uuid '{message_uuid}' not found.",
                "document/message/not_found",
            )

        return self._transform_to_entity(result)

    def _get_contact_by_uuid(self, contact_uuid: UUID) -> Contact:
        """
        Get the contact information from an message.created_by_uuid.
        If the created_by_uuid happens to be None, return None. Raise
        Conflict when there is a UUID but no contact information is
        available.
        """

        contact_row = self.session.execute(
            database_queries.contact_query(contact_uuid)
        ).fetchone()

        if contact_row is None:
            raise Conflict(
                f"No Contact found with UUID '{contact_uuid}'",
                "document/message/no_contact_found",
            )

        return Contact(contact_type=contact_row.type)

    def get_filestore_source_with_pdf_from_message_uuid(
        self, message_uuid
    ) -> FileStoreSource:
        """
        Get a FileStoreSource with created PDF  by a given message_uuid.

        :param message_uuid
        :type UUID
        :return FileStoreSource
        :type namedtuple(case_uuid, filestore_uuid, subject)
        """
        message = self.get_message_by_uuid(message_uuid=message_uuid)

        if message.message_type != "external":
            raise Conflict(
                "Can only save by original source file for external email messages",
                "document/message/no_external_message",
            )

        if message.message_external.case_id is None:
            raise Conflict(
                f"Email message with uuid '{message_uuid}' cannot be linked.",
                "document/filestore/not_allowed",
            )

        pdf_uuid = self._create_pdf_from_message(message=message)

        return FileStoreSource(
            case_uuid=message.case_uuid,
            filestore_uuid=pdf_uuid,
            subject=message.message_external.subject,
        )

    def get_filestore_source_with_original_file_uuid_from_message_uuid(
        self, message_uuid
    ) -> FileStoreSource:
        """
        Get a FileStoreSource for an external message with a
        original source file which is linked to a case.

        :param message_uuid:
        :type; UUID
        :return FileStoreSource
        :type namedtuple(case_uuid, filestore_uuid, subject)
        """

        message = self._get_message_by_uuid_with_original_source_file_uuid(
            message_uuid=message_uuid
        )

        allowed_message_types = ["email"]
        if message.message_external.type not in allowed_message_types:
            raise Conflict(
                f"Only messages of type {', '.join(allowed_message_types)} can be saved in original format",
                "domains/document/message/not_allowed",
            )

        if message.message_external.case_id is None:
            raise Conflict(
                f"Email message with uuid '{message_uuid}' cannot be linked.",
                "document/filestore/not_allowed",
            )

        return FileStoreSource(
            case_uuid=message.case_uuid,
            filestore_uuid=message.message_external.source_file_uuid,
            subject=message.message_external.subject,
        )

    def _transform_to_entity(self, query_result):
        """Transform a message_row to entity Message.

        :param query_result: message_row from database.
        :type query_result: database record
        :return: Message Entity
        :rtype: Message
        """

        # Not all messages in the database from previous
        # iterations have a message_date set. Then use
        # message.created - date instead.

        if query_result.message_date is not None:
            message_date = query_result.message_date
        else:
            message_date = query_result.created

        message_external = None

        if query_result.type == "external":
            external_params = {
                "id": query_result.external_message_id,
                "case_id": query_result.case_id,
                "message_date": message_date,
                "created": query_result.created,
                "subject": query_result.external_message_subject,
                "created_by_displayname": query_result.created_by_displayname,
                "participants": query_result.participants,
                "type": query_result.external_message_type,
                "content": query_result.external_message_content,
                "attachments": (
                    query_result.attachments
                    if query_result.attachment_count > 0
                    else []
                ),
            }

            # It depends on the select statement if the external message source
            # file UUID was asked, usually for create_document_from_message(
            #     message_uuid='blabla', output_format = "original"
            # ) which uses the original source file UUID, else use default None

            try:
                external_params["source_file_uuid"] = (
                    query_result.external_message_source_file_uuid
                )
            except Exception:
                pass

            message_external = MessageExternal(**external_params)

        params = {
            "uuid": query_result.uuid,
            "case_uuid": query_result.case_uuid,
            "created_by_displayname": query_result.created_by_displayname,
            "created_by_uuid": query_result.created_by_uuid,
            "created": query_result.created,
            "message_type": query_result.type,
            "last_modified": query_result.last_modified,
            "message_date": message_date,
            "message_external": message_external,
        }

        message = Message(**params)
        return message

    def _create_pdf_from_message(self, message: Message) -> UUID:
        """
        Create PDF file from a Message and store it in the FileStore.
        :param message: Message Object
        :return: FileStore PDF UUID
        """
        converter_infra = self._get_infrastructure("converter")
        storage_infra = self._get_infrastructure("s3")

        convertable_message_types = ["email", "pip"]

        if message.message_external.type not in convertable_message_types:
            raise Conflict(
                f"Only messages of type {', '.join(convertable_message_types)} can be saved as PDF",
                "domains/document/message/not_allowed",
            )

        display_date = message.message_date

        display_date = display_date.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )
        display_date = display_date.strftime("%d-%m-%Y om %H:%M:%S")

        message.message_external.subject = (
            f"{message.message_external.subject} op {display_date}"
        )

        if message.message_external.type == "email":
            message_as_bytes = message.message_external.get_email_as_bytes()

        elif message.message_external.type == "pip":
            if message.created_by_uuid is not None:
                contact = self._get_contact_by_uuid(message.created_by_uuid)
            else:
                contact = None

            message_as_bytes = (
                message.message_external.get_pip_message_as_bytes(
                    contact=contact
                )
            )
        else:
            raise Conflict(f"Unknown message type: {message.message_external}")

        pdf_file = converter_infra.convert(
            to_type="application/pdf", content=message_as_bytes
        )

        # Write to disk when using > 5MB memory (max_size = bytes)
        file_handle = tempfile.SpooledTemporaryFile(max_size=5242880)
        file_handle.write(pdf_file)
        filestore_uuid = uuid4()
        file_handle.seek(0, SEEK_SET)

        upload_result = storage_infra.upload(
            file_handle=file_handle, uuid=filestore_uuid
        )

        base_name = clean_filename_part(message.message_external.subject)

        self.session.execute(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": filestore_uuid,
                    "original_name": base_name + ".pdf",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": "application/pdf",
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        )

        return filestore_uuid

    def _get_message_by_uuid_with_original_source_file_uuid(
        self, message_uuid
    ) -> Message:
        query = get_message_email_source_file_by_uuid(
            message_uuid=message_uuid
        )

        result = self.session.execute(query).fetchone()

        if not result:
            raise NotFound(
                f"External email message with original source file, uuid '{message_uuid}', not found.",
                "document/message/not_found",
            )

        return self._transform_to_entity(result)
