# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import json
import logging
import minty.cqrs
import minty.exceptions
from ..entities import Case, GeoFieldTypesEnum
from ..repositories import (
    CaseRepository,
    ContactRepository,
    CustomObjectRepository,
)
from collections.abc import Mapping
from copy import deepcopy
from pydantic.v1 import validate_arguments
from typing import Any, cast
from uuid import UUID


def _merge_geojson(geojson: dict, additional_geojson: dict):
    "Merge two GeoJSON FeatureCollections into the first one"

    try:
        if additional_geojson["type"] == "FeatureCollection":
            geojson["features"].extend(additional_geojson["features"])
        else:
            geojson["features"].append(additional_geojson)
    except (KeyError, TypeError):
        # KeyError: no 'features' in GeoJSON input
        # TypeError: input is not a dict (so it's not valid geojson)
        pass

    return


def _insert_origin_data(geo_field_value, origin_data):
    """
    Add Zaaksysteem origin properties to a GeoJSON Feature or FeatureSet
    """
    try:
        if geo_field_value["type"] == "FeatureCollection":
            for feature_entry in geo_field_value["features"]:
                if (
                    "properties" not in feature_entry
                    or feature_entry["properties"] is None
                ):
                    feature_entry["properties"] = {}
                feature_entry["properties"]["zaaksysteem"] = origin_data
        elif geo_field_value["type"] == "Feature":
            if (
                "properties" not in geo_field_value
                or geo_field_value["properties"] is None
            ):
                geo_field_value["properties"] = {}
            geo_field_value["properties"]["zaaksysteem"] = origin_data

    except (KeyError, TypeError) as e:
        # Invalid GeoJSON
        logger = logging.getLogger(__name__)
        logger.debug(f"Invalid GeoJSON data: '{e}'")
        pass


def _ensure_object_is_list(object):
    if isinstance(object, list):
        return object
    else:
        return [object]


def _handle_geojson_field(geo_features, field):
    # For GeoJSON fields, the value is the GeoJSON
    _merge_geojson(geo_features, field)


def _handle_address_field(geo_features, field):
    # Address (V2) fields have a "geojson" key in their value
    try:
        _merge_geojson(geo_features, field["geojson"])
    except (KeyError, TypeError):
        pass


def _handle_relationship_field(related_uuids, field):
    if isinstance(field, str):
        # single value from object
        related_uuids.append(field)
    elif isinstance(field, dict) and isinstance(field["value"], str):
        # multi value object and single value case
        related_uuids.append(field["value"])
    elif isinstance(field, dict) and isinstance(field["value"], list):
        # multi value case
        for relation in field["value"]:
            related_uuids.append(relation["value"])
    else:
        raise ValueError(f"Invalid value for field {field}")


def _extract_map_values(map_magic_strings, custom_fields):
    geo_features = {"type": "FeatureCollection", "features": []}
    related_uuids = []

    for map_magic_string in map_magic_strings:
        field_type = map_magic_string.field_type
        magic_string = map_magic_string.magic_string

        try:
            geo_field_value = _ensure_object_is_list(
                custom_fields[magic_string]["value"]
            )
        except (KeyError, TypeError):
            continue
        if field_type in [
            GeoFieldTypesEnum.geojson,
            GeoFieldTypesEnum.address,
            GeoFieldTypesEnum.relationship,
        ]:
            for field in geo_field_value:
                if field_type == GeoFieldTypesEnum.geojson:
                    _handle_geojson_field(
                        geo_features=geo_features, field=field
                    )
                elif field_type == GeoFieldTypesEnum.address:
                    _handle_address_field(
                        geo_features=geo_features, field=field
                    )
                elif field_type == GeoFieldTypesEnum.relationship:
                    _handle_relationship_field(
                        related_uuids=related_uuids, field=field
                    )
    return geo_features, related_uuids


class UpdateCustomObjectGeo(minty.cqrs.SplitCommandBase):
    name = "update_custom_object_geo"

    @validate_arguments
    def __call__(
        self,
        object_uuid: UUID,
        custom_fields: Mapping[str, Any],
        cases: list[UUID],
        source_event_timestamp: datetime.datetime,
    ):
        "Update geo cache and links for a specific custom object"

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.get_by_uuid(uuid=object_uuid)
        custom_fields = self.__get_object_origin_data(
            custom_object, custom_fields
        )
        geo_json, related_uuids = _extract_map_values(
            map_magic_strings=custom_object.map_magic_strings,
            custom_fields=custom_fields,
        )

        self.cmd.create_geo_feature(
            uuid=str(custom_object.uuid),
            geojson=geo_json,
            source_event_timestamp=source_event_timestamp,
        )
        # Set which other things need to be shown on the object map:
        self.cmd.set_geo_feature_relationships(
            origin_uuid=str(custom_object.uuid),
            related_uuids=related_uuids,
            source_event_timestamp=source_event_timestamp,
        )

        for case in cases:
            # Objects that are created in a case need need to be visible on the
            # map in the case.
            # This adds the relationship from the case(s) to this object.
            self.cmd.update_geo_feature_relationships(
                origin_uuid=case,
                added=[custom_object.uuid],
                removed=[],
                source_event_timestamp=source_event_timestamp,
            )

    def __get_object_origin_data(self, custom_object, custom_fields):
        for map_magic_string in custom_object.map_magic_strings:
            field_type = map_magic_string.field_type
            magic_string = map_magic_string.magic_string

            try:
                geo_field_value = custom_fields[magic_string]["value"]
            except (KeyError, TypeError):
                continue

            origin_data = {
                "origin": {
                    "identifier": str(custom_object.uuid),
                    "title": custom_object.title,
                    "subtitle": custom_object.subtitle,
                    "status": custom_object.status,
                    "family": "object",
                    "type": custom_object.type,
                    "custom_field_magicstring": magic_string,
                    "custom_field_label": map_magic_string.field_label,
                }
            }
            try:
                if field_type == GeoFieldTypesEnum.geojson:
                    _insert_origin_data(geo_field_value, origin_data)
                elif field_type == GeoFieldTypesEnum.address:
                    origin_data["origin"]["location_bagid"] = geo_field_value[
                        "bag"
                    ]["id"]
                    origin_data["origin"]["location_description"] = (
                        geo_field_value["address"]["full"]
                    )
                    _insert_origin_data(
                        geo_field_value["geojson"], origin_data
                    )
            except (KeyError, TypeError):
                pass

        return custom_fields


class UpdateContactGeo(minty.cqrs.SplitCommandBase):
    name = "update_contact_geo"

    @validate_arguments
    def __call__(
        self,
        contact_uuid: UUID,
        geojson: dict,
        source_event_timestamp: datetime.datetime,
    ):
        geo_value = deepcopy(geojson)

        try:
            contact_repo = cast(
                ContactRepository, self.get_repository("contact")
            )
            contact = contact_repo.get_by_uuid(uuid=contact_uuid)
        except minty.exceptions.NotFound:
            return

        _insert_origin_data(geo_value, contact.origin_data())

        self.cmd.create_geo_feature(
            uuid=contact_uuid,
            geojson=geo_value,
            source_event_timestamp=source_event_timestamp,
        )


class UpdateCaseGeo(minty.cqrs.SplitCommandBase):
    name = "update_case_geo"

    @validate_arguments
    def __call__(
        self,
        case_uuid: UUID,
        custom_fields: dict[str, list[str]],
        source_event_timestamp: datetime.datetime,
    ):
        "Update geo cache for a specific case"

        repo = cast(CaseRepository, self.get_repository("case"))
        try:
            case = repo.get_by_uuid(uuid=case_uuid)
        except minty.exceptions.NotFound:
            self.logger.warning(f"Case with uuid={case_uuid} not found")
            return

        geo_json, related_uuids = self.__extract_map_values_for_case(
            case=case,
            custom_fields=custom_fields,
        )

        if case.use_geojson_address:
            related_uuids.append(str(case.requestor_uuid))

        self.cmd.create_geo_feature(
            uuid=str(case.uuid),
            geojson=geo_json,
            source_event_timestamp=source_event_timestamp,
        )

        # Set which other things need to be shown on the case map:
        self.cmd.set_geo_feature_relationships(
            origin_uuid=str(case.uuid),
            related_uuids=related_uuids,
            source_event_timestamp=source_event_timestamp,
        )

    def __extract_map_values_for_case(
        self, case: Case, custom_fields: dict[str, list[str]]
    ):
        """
        Case custom fields need to be "unpacked" because the legacy events
        contain JSON in a string that's in a list (this is the way the old app
        stores custom fields, because some kinds of fields can have multiple
        values).
        """

        unpacked_custom_fields = {}
        for map_magic_string in case.map_magic_strings:
            field_type = map_magic_string.field_type
            magic_string = map_magic_string.magic_string
            if (
                magic_string not in custom_fields
                or len(custom_fields[magic_string]) == 0
            ):
                continue

            if field_type in [
                GeoFieldTypesEnum.geojson,
                GeoFieldTypesEnum.address,
                GeoFieldTypesEnum.relationship,
            ]:
                custom_field_unpacked = []
                geo_field_value = _ensure_object_is_list(
                    custom_fields[magic_string]
                )

                for custom_field in geo_field_value:
                    try:
                        # in the Perl CaseChangedEvent the custom_field is send as a string, the python event is a dict already
                        custom_field_unpacked.append(
                            json.loads(custom_field)
                            if isinstance(custom_field, str)
                            else custom_field
                        )
                        unpacked_custom_fields[magic_string] = {
                            "value": custom_field_unpacked
                        }
                    except json.JSONDecodeError as e:
                        self.logger.error(
                            f"Error decoding '{field_type}' custom field in case: '{e}'."
                        )
        unpacked_custom_fields = self.__get_case_origin_data(
            case, unpacked_custom_fields
        )
        return _extract_map_values(
            map_magic_strings=case.map_magic_strings,
            custom_fields=unpacked_custom_fields,
        )

    def __get_case_origin_data(self, case, unpacked_custom_fields):
        for map_magic_string in case.map_magic_strings:
            field_type = map_magic_string.field_type
            magic_string = map_magic_string.magic_string

            try:
                geo_field = unpacked_custom_fields[magic_string]["value"]
            except (KeyError, TypeError):
                continue

            field_origin_data = {
                "origin": {
                    "identifier": str(case.uuid),
                    "title": case.title,
                    "subtitle": case.subtitle,
                    "status": case.status,
                    "family": case.family,
                    "type": case.type,
                    "custom_field_magicstring": magic_string,
                    "custom_field_label": map_magic_string.field_label,
                    "assignee": case.assignee,
                    "requestor": case.requestor,
                }
            }

            for geo_field_value in geo_field:
                try:
                    if field_type == GeoFieldTypesEnum.geojson:
                        _insert_origin_data(geo_field_value, field_origin_data)
                    elif field_type == GeoFieldTypesEnum.address:
                        field_origin_data["origin"]["location_bagid"] = (
                            geo_field_value["bag"]["id"]
                        )
                        field_origin_data["origin"]["location_description"] = (
                            geo_field_value["address"]["full"]
                        )

                        _insert_origin_data(
                            geo_field_value["geojson"], field_origin_data
                        )
                except (KeyError, TypeError):
                    pass

        return unpacked_custom_fields


class UpdateCaseRequestor(minty.cqrs.SplitCommandBase):
    name = "update_case_requestor"

    @validate_arguments
    def __call__(
        self,
        case_uuid: UUID,
        new_requestor_uuid: UUID,
        source_event_timestamp: datetime.datetime,
        old_requestor_uuid: UUID | None = None,
    ):
        repo = cast(CaseRepository, self.get_repository("case"))
        try:
            case = repo.get_by_uuid(uuid=case_uuid)
        except minty.exceptions.NotFound:
            self.logger.warning(f"Case with uuid={case_uuid} not found")
            return

        if case.use_geojson_address:
            self.cmd.update_geo_feature_relationships(
                origin_uuid=case_uuid,
                added=[new_requestor_uuid],
                removed=[old_requestor_uuid] if old_requestor_uuid else [],
                source_event_timestamp=source_event_timestamp,
            )
        else:
            self.logger.debug(
                f"Geo for requestor of case {case_uuid} does not need to be linked"
            )
