# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from minty.entity import ValueObject
from pydantic.v1 import Field


class GeoFieldTypesEnum(enum.StrEnum):
    geojson = "geojson"
    address = "address_v2"
    relationship = "relationship"


class MapMagicString(ValueObject):
    field_type: GeoFieldTypesEnum = Field(
        ...,
        title="Type of custom field; used to determine how to use the value",
    )
    magic_string: str = Field(..., title="Magic string of the custom field")
    field_label: str | None = Field(..., title="Label of the custom field")
