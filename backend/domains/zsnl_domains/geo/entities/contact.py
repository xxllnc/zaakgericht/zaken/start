# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class Contact(Entity):
    """
    Entity representing a contact as seen by the geo domain.

    It is very minimal and only contains the information needed to store in the
    geo domain.
    """

    entity_type = "contact"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Unique Identifier for a contact")

    name: str = Field(..., title="Display-name of the contact")
    address: str = Field(..., title="Full address of the contact")

    type: str = Field(..., title="Type of contact (person, organization)")
    status: str | None = Field(
        None, title="Status of the contact (active or inactive)"
    )

    def origin_data(self):
        return {
            "origin": {
                "identifier": str(self.uuid),
                "title": self.name,
                "location_description": self.address,
                "family": self.type,
                "status": self.status,
            }
        }
