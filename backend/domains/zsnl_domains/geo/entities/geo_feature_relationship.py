# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class GeoFeatureRelationship(Entity):
    """Represents a relation between two objects, for the purposes of linking
    their geo information together."""

    uuid: UUID = Field(..., title="Identifier for this entity")

    origin_uuid: UUID = Field(
        ..., title="UUID of the 'original' object or case"
    )
    related_uuid: UUID = Field(..., title="UUID of related object")

    date_deleted: datetime.date | None = Field(
        None, title="Date the geofeature relationship is deleted"
    )
    last_modified: datetime.datetime | None = Field(
        None,
        title="Timestamp indicating when the geo relationship was last modified",
    )

    entity_type = "geo_feature_relationship"
    entity_id__fields: list[str] = ["id"]

    @classmethod
    @Entity.event(name="GeoFeatureRelationshipCreated", fire_always=True)
    def create(cls, **kwargs):
        geo_feature_rel = cls(**kwargs)
        return geo_feature_rel

    @Entity.event(name="GeoFeatureRelationshipDeleted", fire_always=True)
    def delete(self):
        self.date_deleted = datetime.date.today()
