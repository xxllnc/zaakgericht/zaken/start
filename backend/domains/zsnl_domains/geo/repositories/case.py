# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import Case
from minty.exceptions import NotFound
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.database import schema

requestor_organization = sql.alias(schema.Bedrijf.__table__)
requestor_person = sql.alias(schema.NatuurlijkPersoon.__table__)
requestor_employee = sql.alias(schema.Subject.__table__)

case_query = (
    sql.select(
        schema.Case.uuid,
        sql.cast(schema.Case.id, sqltypes.String).label("title"),
        sql.literal("case").label("family"),
        sql.cast(schema.Subject.properties, postgresql.JSON)[
            "displayname"
        ].astext.label("assignee"),
        schema.Case.onderwerp.label("subtitle"),
        schema.Case.status,
        schema.ZaaktypeNode.titel.label("type"),
        sql.func.coalesce(schema.ZaaktypeNode.adres_geojson, False).label(
            "use_geojson_address"
        ),
        sql.func.coalesce(schema.ZaakBetrokkenen.naam, "").label("requestor"),
        sql.func.coalesce(
            requestor_organization.c.uuid,
            requestor_person.c.uuid,
            requestor_employee.c.uuid,
            None,
        ).label("requestor_uuid"),
        sql.func.json_agg(
            sql.func.json_build_object(
                "magic_string",
                schema.BibliotheekKenmerk.magic_string,
                "field_type",
                schema.BibliotheekKenmerk.value_type,
                "label",
                schema.BibliotheekKenmerk.naam,
                "properties",
                sql.cast(schema.ZaaktypeKenmerk.properties, postgresql.JSON),
            )
        ).label("custom_fields"),
    )
    .select_from(
        sql.join(
            schema.Case,
            schema.ZaaktypeKenmerk,
            schema.ZaaktypeKenmerk.zaaktype_node_id
            == schema.Case.zaaktype_node_id,
        )
        .join(
            schema.BibliotheekKenmerk,
            schema.BibliotheekKenmerk.id
            == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
        )
        .join(
            schema.ZaaktypeNode,
            schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
        )
        .join(
            schema.Subject,
            schema.Case.behandelaar_gm_id == schema.Subject.id,
            isouter=True,
        )
        .join(
            sql.join(
                schema.ZaakBetrokkenen,
                requestor_organization,
                sql.and_(
                    schema.ZaakBetrokkenen.betrokkene_type == "bedrijf",
                    schema.ZaakBetrokkenen.gegevens_magazijn_id
                    == requestor_organization.c.id,
                ),
                isouter=True,
            )
            .join(
                requestor_person,
                sql.and_(
                    schema.ZaakBetrokkenen.betrokkene_type
                    == "natuurlijk_persoon",
                    schema.ZaakBetrokkenen.gegevens_magazijn_id
                    == requestor_person.c.id,
                ),
                isouter=True,
            )
            .join(
                requestor_employee,
                sql.and_(
                    schema.ZaakBetrokkenen.betrokkene_type == "medewerker",
                    schema.ZaakBetrokkenen.gegevens_magazijn_id
                    == requestor_employee.c.id,
                ),
                isouter=True,
            ),
            schema.Case.aanvrager == schema.ZaakBetrokkenen.id,
            isouter=True,
        )
    )
    .group_by(
        schema.Case.uuid,
        schema.Case.id,
        schema.Case.onderwerp,
        schema.Subject.properties,
        schema.ZaaktypeNode.titel,
        schema.ZaaktypeNode.adres_geojson,
        schema.ZaakBetrokkenen.naam,
        schema.Case.status,
        requestor_organization.c.uuid,
        requestor_person.c.uuid,
        requestor_employee.c.uuid,
    )
)


class CaseRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Case"
    _events_to_calls: dict[str, str] = {}

    def get_by_uuid(self, uuid: UUID):
        row = self.session.execute(
            case_query.where(schema.Case.uuid == uuid)
        ).fetchone()

        if row is None:
            raise NotFound(
                f"Case with uuid={uuid} not found", "geo/case/not_found"
            )

        return self.__entity_from_row(row=row)

    def __entity_from_row(self, row) -> Case:
        return Case(
            uuid=row.uuid,
            entity_id=row.uuid,
            title=row.title,
            family=row.family,
            assignee=row.assignee,
            subtitle=row.subtitle,
            type=row.type,
            requestor=row.requestor,
            requestor_uuid=row.requestor_uuid,
            status=row.status,
            use_geojson_address=row.use_geojson_address,
            map_magic_strings=[
                {
                    "magic_string": cf["magic_string"],
                    "field_type": cf["field_type"],
                    "field_label": cf["label"],
                }
                for cf in row.custom_fields
                if self.__use_on_map(cf)
            ],
        )

    def __use_on_map(self, custom_field_definition) -> bool:
        """Takes the definition of a single custom field, as stored in the
        database, and returns whether it has the "use on map" flag set."""

        if custom_field_definition["properties"] is None:
            return False

        show_on_map = False
        try:
            show_on_map = custom_field_definition["properties"]["show_on_map"]
            # Because Perl has no "boolean" type, this is stored as a string
            # "1" or "0".
            # when no value attribute is specified in a checkbox,
            # the value "on" is posted
        except (KeyError, TypeError):
            return False

        if show_on_map is True or show_on_map in {"on", "1", 1}:
            return True

        return False
