# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import CustomObject
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema

custom_object_query = sql.select(
    schema.CustomObject.uuid,
    schema.CustomObjectTypeVersion.custom_field_definition,
    schema.CustomObjectVersion.title,
    schema.CustomObjectVersion.subtitle,
    schema.CustomObjectTypeVersion.name.label("type"),
    schema.CustomObjectVersion.status,
    sql.literal("object").label("family"),
).select_from(
    sql.join(
        schema.CustomObjectVersion,
        schema.CustomObject,
        schema.CustomObjectVersion.custom_object_id == schema.CustomObject.id,
    ).join(
        schema.CustomObjectTypeVersion,
        schema.CustomObjectVersion.custom_object_type_version_id
        == schema.CustomObjectTypeVersion.id,
    )
)


class CustomObjectRepository(ZaaksysteemRepositoryBase):
    _for_entity = "CustomObject"
    _events_to_calls: dict[str, str] = {}

    def get_by_uuid(self, uuid: UUID) -> CustomObject:
        row = self.session.execute(
            custom_object_query.where(schema.CustomObjectVersion.uuid == uuid)
        ).fetchone()

        if row is None:
            raise NotFound(
                f"Custom object with uuid={uuid} not found",
                "geo/custom_object/not_found",
            )
        return self.__entity_from_row(row=row)

    def __entity_from_row(self, row) -> CustomObject:
        return CustomObject(
            uuid=row.uuid,
            entity_id=row.uuid,
            map_magic_strings=[
                {
                    "magic_string": cf["magic_string"],
                    "field_type": cf["custom_field_type"],
                    "field_label": cf["label"],
                }
                for cf in row.custom_field_definition["custom_fields"]
                if self.__use_on_map(cf)
            ],
            title=row.title,
            subtitle=row.subtitle,
            status=row.status,
            family=row.family,
            type=row.type,
        )

    def __use_on_map(self, custom_field_definition) -> bool:
        """Takes the definition of a single custom field, as stored in the
        database, and returns whether it has the "use on map" set."""

        try:
            return custom_field_definition["custom_field_specification"][
                "use_on_map"
            ]
        except (KeyError, TypeError):
            # 1. custom_field_specification or use_on_map did not exist
            # 2. or custom_field_specification was not a dict
            return False
