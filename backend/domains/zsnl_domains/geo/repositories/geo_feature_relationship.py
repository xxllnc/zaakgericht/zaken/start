# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from ... import ZaaksysteemRepositoryBase
from ..entities import GeoFeatureRelationship
from minty.cqrs import Event, UserInfo
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from uuid import UUID
from zsnl_domains.database import schema

geo_feature_relation_query = sql.select(
    schema.ServiceGeoJSONRelationship.uuid.label("entity_id"),
    # GeoFeatureRelationships are retrieved from the perspective of a
    # "source" object with a "relationship" field in it; this is
    # stored the other way around in the service_geojson tables: the
    # "source" object is in the "related_uuid" field of the
    # service_geojson_relationship table.
    schema.ServiceGeoJSON.uuid.label("related_uuid"),
    schema.ServiceGeoJSONRelationship.related_uuid.label("origin_uuid"),
    schema.ServiceGeoJSONRelationship.last_modified,
).select_from(
    sql.join(
        schema.ServiceGeoJSONRelationship,
        schema.ServiceGeoJSON,
        schema.ServiceGeoJSON.id
        == schema.ServiceGeoJSONRelationship.service_geojson_id,
    )
)


class GeoFeatureRelationshipRepository(ZaaksysteemRepositoryBase):
    _for_entity = "GeoFeatureRelationship"
    _events_to_calls = {
        "GeoFeatureRelationshipCreated": "_create_geo_feature_relationship",
        "GeoFeatureRelationshipDeleted": "_delete_geo_feature_relationship",
    }

    def create_geo_feature_relationship(
        self,
        relationship_uuid: UUID,
        origin_uuid: UUID,
        related_uuid: UUID,
        last_modified: datetime.datetime,
    ) -> GeoFeatureRelationship:
        return GeoFeatureRelationship.create(
            uuid=relationship_uuid,
            origin_uuid=origin_uuid,
            related_uuid=related_uuid,
            last_modified=last_modified,
            # Internal values
            entity_id=relationship_uuid,
            # Services
            _event_service=self.event_service,
        )

    def get_relationships(
        self, origin_uuid: UUID
    ) -> list[GeoFeatureRelationship]:
        "Retrieve all existing geo feature relations an object has"

        relation_rows = self.session.execute(
            geo_feature_relation_query.where(
                schema.ServiceGeoJSONRelationship.related_uuid == origin_uuid,
            )
        ).fetchall()

        return [
            self._entity_from_row(row=relation_row)
            for relation_row in relation_rows
        ]

    def _entity_from_row(self, row) -> GeoFeatureRelationship:
        return GeoFeatureRelationship.parse_obj(
            {
                "uuid": row.entity_id,
                "origin_uuid": row.origin_uuid,
                "related_uuid": row.related_uuid,
                # Internals
                "entity_id": row.entity_id,
                # Services
                "_event_service": self.event_service,
                "last_modified": row.last_modified,
            }
        )

    # Event handlers
    def _create_geo_feature_relationship(
        self,
        event: Event,
        user_info: UserInfo | None = None,
        dry_run: bool = False,
    ):
        changes = event.format_changes()
        empty_geojson = {"type": "FeatureCollection", "features": []}

        # try to insert a service_geojson record so it is always available.
        # In some scenarios this record is not present yet.
        service_geojson_values = {
            "uuid": changes["related_uuid"],
            "geo_json": empty_geojson,
            # When creating a new empty `service_geojson` record just to have
            # something to refer to with a foreign key in
            # `service_geojson_relationship`, set the timestamp to somewhere
            # far in the past, so any other update has priority and overwrites
            # the empty value.
            "last_modified": datetime.datetime.fromtimestamp(
                0, tz=datetime.UTC
            )
            or datetime.datetime.now(tz=datetime.UTC),
        }

        self.session.execute(
            sql_pg.insert(schema.ServiceGeoJSON)
            .values(service_geojson_values)
            .on_conflict_do_nothing()
        )

        self.session.execute(
            sql_pg.insert(schema.ServiceGeoJSONRelationship)
            .from_select(
                [
                    "uuid",
                    "service_geojson_id",
                    "related_uuid",
                    "last_modified",
                ],
                sql.select(
                    sql.literal(event.entity_id),
                    schema.ServiceGeoJSON.id,
                    sql.literal(str(changes["origin_uuid"])),
                    sql.literal(changes["last_modified"]),
                ).where(schema.ServiceGeoJSON.uuid == changes["related_uuid"]),
            )
            .on_conflict_do_nothing()
        )

    def _delete_geo_feature_relationship(
        self,
        event: Event,
        user_info: UserInfo | None = None,
        dry_run: bool = False,
    ):
        self.session.execute(
            sql.delete(schema.ServiceGeoJSONRelationship)
            .where(schema.ServiceGeoJSONRelationship.uuid == event.entity_id)
            .execution_options(synchronize_session=False)
        )
