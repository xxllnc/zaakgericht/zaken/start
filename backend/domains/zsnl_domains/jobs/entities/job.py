# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
import minty.cqrs
import minty.exceptions
from ...shared.case_filters import CaseFilter
from datetime import datetime
from minty.entity import Entity, ValueObject
from pydantic.v1 import ConstrainedFloat, Field
from typing import Literal, Self
from uuid import UUID


class JobStatus(enum.StrEnum):
    pending = enum.auto()  # Job created, waiting for enumeration
    running = (
        enum.auto()
    )  # Cases (or objects, etc) enumerated, actions executing
    processed = enum.auto()  # All actions executed
    finished = (
        enum.auto()
    )  # Post-processing complete, result files ready for download
    cancelled = enum.auto()  # Job cancelled by user or system
    deleted = enum.auto()  # Job deleted by user


class JobCancelReason(enum.StrEnum):
    user_action = enum.auto()
    source_not_found = enum.auto()
    source_not_finished = enum.auto()


# Selection filters for "case" related jobs


class CaseSelectionFilter(ValueObject):
    "Case selection using a filter"

    type: Literal["filter"] = Field(...)
    filters: CaseFilter


class CaseSelectionOtherJob(ValueObject):
    """
    Case selection from a source job.

    The source job must be in the "finished" state (successfully completed).
    """

    type: Literal["source_job"] = Field(...)
    source_job: UUID


class CaseSelectionIdList(ValueObject):
    "Case selection from a list of user-supplied UUIDs"

    type: Literal["id_list"] = Field(...)
    id_list: list[UUID] = Field(..., min_items=1, max_items=100)


class CaseJob(ValueObject):
    selection: (
        CaseSelectionFilter | CaseSelectionOtherJob | CaseSelectionIdList
    ) = Field(discriminator="type")

    class Config:
        validate_all = True
        validate_assignment = True
        extra = "forbid"


# Types of jobs:


class JobDescriptionDeleteCase(CaseJob):
    "Job that runs 'delete_case' on the selection"

    job_type: Literal["delete_case"] = Field(...)


class JobDescriptionSimulateDeleteCase(CaseJob):
    "Job that runs 'check_delete_case' on the selection"

    job_type: Literal["simulate_delete_case"] = Field(...)


class JobDescriptionArchiveExport(CaseJob):
    "Job that runs 'generate_archive_export' on the selection to generate topx export"

    job_type: Literal["run_archive_export"] = Field(...)


# To add more kinds of jobs, define a new TypedDict above, and add it to the
# JobDescription type below using `|`.
#
# Example:
#   JobDescription = JobDescriptionDeleteCase | JobDescriptionSomethingElse

JobDescription = (
    JobDescriptionDeleteCase
    | JobDescriptionSimulateDeleteCase
    | JobDescriptionArchiveExport
)


class Percentage(ConstrainedFloat):
    minimum = 0.0
    maximum = 100.0


class Job(Entity):
    """
    Entity representing a job as seen by the job domain.
    """

    uuid: UUID = Field(..., title="Unique Identifier for a job")
    status: JobStatus = Field(..., title="Status of the job")
    progress_percent: Percentage = Field(
        default=0.0, title="Percentage of job that is done"
    )
    job_description: JobDescription = Field(
        ...,
        discriminator="job_type",
        title="Description of job. Includes job type and any parameters required for that type of job.",
    )
    results_file: str | None = Field(
        default=None, title="Name of results file in file store"
    )
    results_file_name: str | None = Field(
        default=None, title="Filename of results file, for the user"
    )
    results_file_location: str | None = Field(
        default=None, title="Storage location of the results file"
    )
    results_file_type: str | None = Field(
        default=None, title="MIME type of the results file"
    )
    results_file_size: int | None = Field(
        default=None, title="Size of the results file"
    )

    errors_file: str | None = Field(
        default=None, title="Name of errors file in file store"
    )
    errors_file_name: str | None = Field(
        default=None, title="Filename of errors file, for the user"
    )
    errors_file_location: str | None = Field(
        default=None, title="Storage location of the errors file"
    )
    errors_file_size: int | None = Field(
        default=None, title="Size of the errors file"
    )

    # No errors_file_type, those are always CSV

    friendly_name: str = Field(
        ...,
        title="Friendly (user-supplied?) name of the job",
    )
    started_at: datetime = Field(..., title="Job started at")
    expires_at: datetime = Field(
        ..., title="Date when job will automatically be deleted"
    )

    total_item_count: int | None = Field(
        default=None, title="Total number of items affected by this job"
    )

    cancel_reason: JobCancelReason | None = Field(
        default=None,
        title="Reason the job was cancelled (if the status is 'cancelled')",
    )

    entity_type: Literal["job"] = "job"
    entity_id__fields: list[str] = ["uuid"]
    entity_internal__fields: list[str] = [
        "results_file",
        "results_file_name",
        "results_file_location",
        "errors_file",
        "errors_file_name",
        "errors_file_location",
    ]

    @classmethod
    @Entity.event(name="JobCreated", fire_always=False)
    def create(
        cls,
        uuid: UUID,
        job_description: JobDescription,
        friendly_name: str,
        started_at: datetime,
        expires_at: datetime,
        event_service: minty.cqrs.EventService,
    ) -> Self:
        return cls(
            uuid=uuid,
            entity_id=uuid,
            status=JobStatus.pending,
            job_description=job_description,
            friendly_name=friendly_name,
            started_at=started_at,
            expires_at=expires_at,
            _event_service=event_service,
        )

    @Entity.event(
        name="JobDeleted",
        fire_always=False,
        extra_fields=["friendly_name"],
    )
    def delete(self):
        if self.status not in (JobStatus.cancelled, JobStatus.finished):
            raise minty.exceptions.Conflict(
                "Only finished or cancelled jobs can be deleted",
                "jobs/delete_wrong_status",
            )

        self.status = JobStatus.deleted

    @Entity.event(
        name="JobCancelled",
        fire_always=False,
        extra_fields=["friendly_name"],
    )
    def cancel(self, cancel_reason: JobCancelReason):
        if self.status not in (JobStatus.running, JobStatus.pending):
            raise minty.exceptions.Conflict(
                "Only running or pending jobs can be cancelled",
                "jobs/cancel_wrong_status",
            )

        self.status = JobStatus.cancelled
        self.cancel_reason = cancel_reason

    @Entity.event(name="JobPrepared", fire_always=False)
    def finish_preparing(self):
        if self.status != "pending":
            raise minty.exceptions.Conflict(
                "Only pending jobs can finish being prepared.",
                "jobs/preparing_wrong_status",
            )

        self.status = JobStatus.running

    @Entity.event(name="JobProgressed", fire_always=True)
    def mark_progress(self):
        if self.status != JobStatus.running:
            raise minty.exceptions.Conflict(
                "Only running jobs can progress.",
                "jobs/progress_wrong_status",
            )

    @Entity.event(name="JobProcessed", fire_always=False)
    def mark_processed(self):
        if self.status != JobStatus.running:
            raise minty.exceptions.Conflict(
                "Only running jobs can finish processing.",
                "jobs/processed_wrong_status",
            )

        self.status = JobStatus.processed

    @Entity.event(name="JobFinished", fire_always=False)
    def mark_finished(self):
        if self.status != JobStatus.processed:
            raise minty.exceptions.Conflict(
                "Only processed jobs can be finished.",
                "jobs/finished_wrong_status",
            )

        self.status = JobStatus.finished
