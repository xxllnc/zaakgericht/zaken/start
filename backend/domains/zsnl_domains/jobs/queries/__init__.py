# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .base import JobQueryBase

JOBS_QUERIES: dict[str, type[minty.cqrs.SplitQueryBase]] = (
    minty.cqrs.build_query_lookup_table(
        queries={*JobQueryBase.__subclasses__()}
    )
)
