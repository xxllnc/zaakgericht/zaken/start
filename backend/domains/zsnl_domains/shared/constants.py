# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from typing import Final

BASE_RELATION_ROLES: Final[list[str]] = [
    "Advocaat",
    "Auditor",
    "Aannemer",
    "Bewindvoerder",
    "Familielid",
    "Gemachtigde",
    "Mantelzorger",
    "Ouder",
    "Verzorger",
    "Ontvanger",
]
