# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import entity
from pydantic.v1 import Field
from zsnl_domains.shared.custom_field.types import base as typebase
from zsnl_domains.shared.custom_field.types.geojson import GeoJSON


class Bag(entity.ValueObject):
    type: str = Field("nummeraanduiding", title="Value for BAG Object")
    id: str = Field(..., title="BAG-ID")

    @classmethod
    def __get_validators__(cls):
        # one or more validators may be yielded which will be called in the
        # order to validate the input, each validator will receive as an input
        # the value returned from the previous validator
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not (isinstance(v, dict)):
            # Only dictionary({"type":"nummeraanduiding", "id":"1234"}) is allowed
            raise TypeError("value is not a valid dict")

        split_id = v["id"].split("-", 1)
        decimal_id = split_id[len(split_id) - 1]
        if not decimal_id.isdecimal():
            id = v["id"]
            # Only digits are allowed for bag id
            raise ValueError(
                f"Invalid bag id '{id}'. Only digits are allowed for bag id."
            )

        # Converting bag id to integer to remove the preceding zeroes ("0000123" will be converted into "123")
        split_id[len(split_id) - 1] = str(int(split_id[len(split_id) - 1]))
        v["id"] = "-".join(split_id)
        return v


class Address(entity.ValueObject):
    full: str | None = Field(None, title="Human readable full address")
    # You can add fields like street, place, city


class AddressV2(entity.ValueObject):
    bag: Bag | None = Field(None, title="BAG details of addres_v2")
    address: Address | None = Field(None, title="Address details of addres_v2")
    geojson: GeoJSON | None = Field(
        None, title="GeoJSON of addres_v2 to show on the maps"
    )


class CustomFieldTypeAddressV2(typebase.CustomFieldValueBase):
    type: str = Field(
        "address_v2", title="A value for custom_field address_v2"
    )
    value: AddressV2 | None = Field(
        None, title="Value for custom_field address_v2"
    )
