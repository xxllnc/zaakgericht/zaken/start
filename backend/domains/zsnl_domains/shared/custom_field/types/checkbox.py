# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic.v1 import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldTypeCheckbox(typebase.CustomFieldValueMultiValueBase):
    type: str = Field("checkbox", title="A list of values formatted as text")

    @classmethod
    def get_validators(cls, custom_field):
        return {"option_validator": cls._get_option_validator(custom_field)}
