# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic.v1 import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldCurrency(typebase.CustomFieldValueBase):
    type: str = Field(
        "currency", title="A value formatted as a ISO-4217 currency string"
    )
    value: str = Field(
        ...,
        title="The value for this field formatted as a ISO-4217 currency string",
    )
