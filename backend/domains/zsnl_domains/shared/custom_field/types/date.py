# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic.v1 import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldTypeDate(typebase.CustomFieldValueBase):
    type: str = Field(
        "date", title="A value formatted as a ISO8601 date string"
    )
    value: str = Field(
        ...,
        title="The value for this field formatted as a ISO8601 date string",
    )

    # We use a custom validator since SQLAlchemy does not handle JSON-serialization
    # of Python's date and datetime fields very well. We use a str in this
    # CustomFieldType and validate against isoformat in the validator.
    @classmethod
    def get_validators(cls, custom_field):
        return {"date_validator": cls._get_date_validator()}
