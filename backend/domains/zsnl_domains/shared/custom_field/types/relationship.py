# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from minty import entity
from pydantic.v1 import Field
from typing import List, Union
from uuid import UUID
from zsnl_domains.shared.custom_field.types import base as typebase


class RelationshipTypeDocumentMetadata(entity.ValueObject):
    filename: str = Field(
        ..., title="The filename of this document including its extension"
    )


class RelationshipTypeMetadata(entity.ValueObject):
    summary: str = Field(..., title="Title of the custom object")
    description: str | None = Field(
        None, title="Subtitle of the custom object"
    )


class RelationshipTypes(enum.StrEnum):
    case = "case"
    custom_object = "custom_object"
    document = "document"
    subject = "subject"


class RelationshipCustomFieldDetails(entity.ValueObject):
    relationship_type: RelationshipTypes = Field(
        ...,
        title="Type of this relationship, one of [document,case,custom_object]",
    )
    metadata: None | (
        RelationshipTypeDocumentMetadata | RelationshipTypeMetadata
    ) = Field(None, title="Optional metadata about the field")


class CustomFieldTypeRelationship(typebase.CustomFieldValueBase):
    type: str = Field(
        "relationship", title="A value referencing another type of object"
    )
    # value is of the type Union[UUID, "List[CustomFieldTypeRelationship]"].
    # It may contain:
    # - a direct relationship ({"value"="uuid"})
    # - an list of relationship data {"value"=[{"value" = "uuid",
    #    "specifics" : "something"}]}
    # Both are supported
    value: Union[UUID, "List[CustomFieldTypeRelationship]"] = Field(
        ...,
        title="A relation, or a list of relations to another field",
    )

    specifics: RelationshipCustomFieldDetails | None = Field(
        None, title="Optional metadata about the field"
    )


CustomFieldTypeRelationship.update_forward_refs()
