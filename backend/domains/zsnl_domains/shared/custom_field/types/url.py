# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic.v1 import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldURL(typebase.CustomFieldValueBase):
    type: str = Field("url", title="A value for a web address (URL).")

    value: str = Field(
        ...,
        title="The value for this web address (URL) field.",
    )
