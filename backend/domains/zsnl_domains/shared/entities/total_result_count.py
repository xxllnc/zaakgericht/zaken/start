# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field, NonNegativeInt
from typing import Literal


class TotalResultCount(Entity):
    """Count of results of query"""

    entity_type: Literal["result_count"] = "result_count"

    total_results: NonNegativeInt = Field(..., title="Total number of results")
