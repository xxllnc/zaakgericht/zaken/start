# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from sqlalchemy import sql, types
from zsnl_domains.database import schema


def user_allowed_read_attributes_subquery(
    user_info: minty.cqrs.UserInfo,
    bibliotheek_kenmerk_alias=schema.BibliotheekKenmerk,
):
    """
    Build a partial query that limits custom fields queries to ones the user has
    access to.

    :param user_info: UserInfo representing the user to check the ACL for
    :param bibliotheek_kenmerk_alias: Alias to the case table to join on. Should be
        used if the main query uses an alias for `schema.BibliotheekKenmerk`.
    :return: Subquery that can be used to restrict case queries to allowed
        cases.
    """

    sensitive_data_permissions = [
        "admin",
        "beheer_zaaktype_admin",
        "zaak_beheer",
        "beheer",
    ]
    subquery = None

    hide_attributes_sensitive_data = True
    for permission in sensitive_data_permissions:
        if user_info.permissions.get(permission, False):
            hide_attributes_sensitive_data = False
            break

    if hide_attributes_sensitive_data:
        subquery = sql.or_(
            sql.cast(bibliotheek_kenmerk_alias.properties, types.JSON)
            .op("->>")("sensitive_field")
            .is_(None),
            sql.not_(
                sql.cast(bibliotheek_kenmerk_alias.properties, types.JSON).op(
                    "->>"
                )("sensitive_field")
                == "on"
            ),
        )
    return subquery
