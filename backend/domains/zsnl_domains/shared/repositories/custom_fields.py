# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from ... import ZaaksysteemRepositoryBase
from .. import entities
from datetime import datetime
from minty.exceptions import NotFound
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from typing import Any
from zsnl_domains.database import schema

valid_json_types = [
    "geojson",
    "relationship",
    "address_v2",
    "appointment_v2",
]
valid_bag_address_types = [
    "bag_adres",
    "bag_adressen",
    "bag_openbareruimte",
    "bag_openbareruimtes",
    "bag_straat_adres",
    "bag_straat_adressen",
]
multivalue_custom_field_types = [*valid_bag_address_types, "checkbox"]


class CustomFieldsRepository(ZaaksysteemRepositoryBase):
    def _is_multiple(self, field_type, is_multiple) -> bool:
        if field_type in valid_bag_address_types or is_multiple is True:
            return True
        else:
            return False

    def _get_address_from_bag_id(self, bag_ids: list) -> str:
        query = sql.select(
            sql.cast(schema.BagCache.bag_data, sql_pg.JSON)[
                "weergavenaam"
            ].astext.label("address")
        ).where(schema.BagCache.bag_id.in_(bag_ids))
        query_result = self.session.execute(query).fetchall()
        if query_result is None:
            raise NotFound(
                f"Address for bag id '{bag_ids}' not found.",
                "Address/not_found",
            )
        return ",".join(item.address for item in query_result)

    def _format_date_custom_field(self, value):
        date_value = None
        # try parsing the date using these patterns:
        # examples: 13-05-1939, 1939-05-13, 19390513, 13051939
        date_patterns = ["%d-%m-%Y", "%Y-%m-%d", "%Y%m%d", "%d%m%Y"]
        val = value
        # some date attributes contain time. Strip time from value if time is present.
        if val and "T" in str(val):
            val = val.split("T", 1)[0]

        for pattern in date_patterns:
            try:
                date_value = datetime.strptime(str(val), pattern)
                break
            except ValueError:
                pass

        return date_value.strftime("%Y-%m-%d") if date_value else None

    def _format_json_custom_fields(self, value):
        try:
            updated_value = json.loads(value)
        except (json.decoder.JSONDecodeError, TypeError):
            updated_value = {}
        return updated_value

    def _generate_bag_address_custom_field_values(
        self, value: str | list, format_bag_address: bool
    ):
        bag_ids = []
        for bag_id in value:
            bag_ids.append(self.extract_bag_id(bag_id))
        return (
            bag_ids
            if format_bag_address is False
            else self._get_address_from_bag_id(bag_ids)
        )

    def extract_bag_id(self, bag_id: str) -> str | None:
        if "nummeraanduiding-" in bag_id:
            return bag_id.lower().replace("nummeraanduiding-", "")
        if "openbareruimte-" in bag_id:
            return bag_id.lower().replace("openbareruimte-", "")

    def _generate_custom_field_values(
        self,
        value: Any,
        field_type: str,
        is_multiple,
        format_bag_address: bool,
    ) -> Any:
        if (
            self._is_multiple(field_type, is_multiple) is False
            and field_type not in multivalue_custom_field_types
        ) or (
            self._is_multiple(field_type, is_multiple)
            and field_type == "relationship"
        ):
            value = value[0]
        if field_type in valid_json_types:
            value = self._format_json_custom_fields(value)
        if field_type == "address_v2" and value.get("bag", {}).get("id"):
            value["bag"]["id"] = self.extract_bag_id(value["bag"]["id"])
        elif field_type in valid_bag_address_types:
            value = self._generate_bag_address_custom_field_values(
                value, format_bag_address
            )
        elif field_type == "date":
            value = self._format_date_custom_field(value)

        return value

    def _format_custom_fields(
        self,
        custom_fields: dict,
        file_custom_fields: dict | None,
        format_bag_address: bool,
    ) -> dict | None:
        custom_fields_list = {}
        if custom_fields:
            for field in custom_fields:
                if field["value"]:
                    custom_fields_list[field["magic_string"]] = {
                        "type": field["type"],
                        "value": self._generate_custom_field_values(
                            value=field["value"],
                            field_type=field["type"],
                            is_multiple=field["is_multiple"],
                            format_bag_address=format_bag_address,
                        ),
                    }
        if file_custom_fields:
            for field in file_custom_fields:
                if field["value"]:
                    custom_fields_list[field["magic_string"]] = {
                        "type": field["type"],
                        "value": field["value"],
                    }

        return custom_fields_list

    def format_custom_fields_for_case_basic(
        self, custom_fields: dict, file_custom_fields: dict | None
    ) -> entities.CustomFields | None:
        custom_fields = self._format_custom_fields(
            custom_fields, file_custom_fields, format_bag_address=False
        )

        return entities.CustomFields(custom_fields=custom_fields.items())

    def format_custom_fields_for_case(
        self, custom_fields: dict, file_custom_fields: dict | None
    ) -> entities.CustomFields | None:
        custom_fields = self._format_custom_fields(
            custom_fields, file_custom_fields, format_bag_address=True
        )
        return entities.CustomFields(custom_fields=custom_fields.items())
