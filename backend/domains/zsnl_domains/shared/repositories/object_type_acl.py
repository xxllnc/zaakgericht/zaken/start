# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from minty.cqrs import UserInfo
from sqlalchemy import sql
from zsnl_domains.case_management.entities import AuthorizationLevel
from zsnl_domains.database import schema


def allowed_object_type_v2_subquery(
    user_info: UserInfo,
    authorization: AuthorizationLevel,
    object_type_alias: sql.expression.Alias,
):
    """Return a subquery (that can be plugged into a `WHERE`) that filters v2
    custom_object_types based on authorizations.

    :param user_uuid: UUID of the user
    :param authorization: custom_object_type authorization level
    :param object_alias: `schema.CustomObjectType` aliased to use for the object_type itself
    """

    read_authorizations = [
        AuthorizationLevel.read,
        AuthorizationLevel.readwrite,
        AuthorizationLevel.admin,
    ]

    write_authorizations = [
        AuthorizationLevel.readwrite,
        AuthorizationLevel.admin,
    ]

    authorizations = read_authorizations
    if authorization in write_authorizations:
        authorizations = write_authorizations

    return sql.exists(
        sql.select(1)
        .select_from(
            sql.join(
                schema.CustomObjectType,
                schema.CustomObjectTypeAcl,
                sql.and_(
                    schema.CustomObjectTypeAcl.custom_object_type_id
                    == schema.CustomObjectType.id,
                    schema.CustomObjectTypeAcl.authorization.in_(
                        authorizations
                    ),
                ),
            )
            .join(schema.Subject, schema.Subject.uuid == user_info.user_uuid)
            .join(
                schema.SubjectPositionMatrix,
                sql.and_(
                    schema.SubjectPositionMatrix.role_id
                    == schema.CustomObjectTypeAcl.role_id,
                    schema.SubjectPositionMatrix.group_id
                    == schema.CustomObjectTypeAcl.group_id,
                    schema.SubjectPositionMatrix.subject_id
                    == schema.Subject.id,
                ),
            )
        )
        .where(schema.CustomObjectType.id == object_type_alias.c.id)
    )
