# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from babel import dates

DEFAULT_DATE_FORMAT = "d MMMM yyyy"
DEFAULT_TZ = "Europe/Amsterdam"
ISO_DATE_FORMAT = "yyyy-MM-dd"


def __break_filter(value: str):
    if len(value) and len(value.strip()):
        value = value + "\n"
    return value


def _format_date(value, date_format):
    if isinstance(value, str):
        try:
            date = datetime.datetime.strptime(value, "%d-%m-%Y")
            return _format_date(date, date_format)
        except ValueError:
            return value

    if isinstance(value, dict):
        value["value"] = _format_date(value["value"], date_format)
        return value

    if isinstance(value, list):
        return [_format_date(v, date_format) for v in value]

    if isinstance(value, datetime.datetime):
        return dates.format_datetime(
            value,
            date_format,
            tzinfo=DEFAULT_TZ,
            locale="nl",
        )
    elif isinstance(value, datetime.date):
        return dates.format_date(value, date_format, locale="nl")

    return value


def __isodate_filter(value, date_format=ISO_DATE_FORMAT):
    return _format_date(value, date_format)


def __date_filter(value, date_format=DEFAULT_DATE_FORMAT):
    return _format_date(value, date_format)


def __capitalize_filter(value: str | dict | list):
    if isinstance(value, str):
        return value.capitalize()

    elif isinstance(value, dict):
        value["value"] = value["value"].capitalize()
        return value

    elif isinstance(value, list):
        return [v.capitalize() for v in value]

    return value


ALL_FILTERS = {
    "date": __date_filter,
    "break": __break_filter,
    "capitalize": __capitalize_filter,
    "isodate": __isodate_filter,
}
