# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
import nltk.stem.snowball
import re
import time
from .types import FilterOperator
from html.parser import HTMLParser
from lingua import IsoCode639_1, LanguageDetectorBuilder
from pkgutil import get_data
from typing import Final

# Mapping from the language name returned by Tika to the language name
# understood by the database.
LANGUAGE_MAP: Final = {
    IsoCode639_1.NL: "dutch",
    IsoCode639_1.DE: "german",
    IsoCode639_1.EN: "english",
    IsoCode639_1.ES: "spanish",
    IsoCode639_1.FR: "french",
    IsoCode639_1.IT: "italian",
}

LANGUAGE_DETECTOR: Final = LanguageDetectorBuilder.from_iso_codes_639_1(
    *LANGUAGE_MAP.keys()
).build()


country_codes_json = get_data(__name__, "country_codes.json").decode("utf-8")
country_codes_list = json.loads(country_codes_json)
country_codes = {}
country_names = {}
for each in country_codes_list:
    country_codes[each["dutch_code"]] = each["label"]
    country_names[each["label"]] = each["dutch_code"]

legal_entity_codes_json = get_data(__name__, "legal_entity_codes.json").decode(
    "utf-8"
)
legal_entity_codes_list = json.loads(legal_entity_codes_json)
legal_entity_codes = {}
legal_entity_labels = {}
for each in legal_entity_codes_list:
    legal_entity_codes[each["code"]] = each["label"]
    legal_entity_labels[each["label"]] = each["code"]


def prepare_search_term(search_term: str):
    """
    Prepare a search term ("as entered") for use with TSVECTOR database fields.

    The prepared keywords for the search term will look like this:
    `case1:* & case2:*`.
    """

    language = LANGUAGE_DETECTOR.detect_language_of(search_term)
    language_iso = language.iso_code_639_1 if language else IsoCode639_1.NL

    stemmer = nltk.stem.snowball.SnowballStemmer(LANGUAGE_MAP[language_iso])

    keywords = re.split(r"\W+", search_term)
    cleaned_keywords = {
        re.sub(r"[^A-Za-z0-9.@]+", "", keyword) for keyword in keywords
    }
    stemmed_keywords = {
        stemmer.stem(word) for word in cleaned_keywords if word
    }

    # The search_terms for file are stored as stemmed words in database.
    formatted_keywords = {f"{keyword}:*" for keyword in stemmed_keywords}

    return " & ".join(sorted(formatted_keywords))


def escape_term_for_like(term: str) -> str:
    return term.replace("~", "~~").replace("%", "~%").replace("_", "~_")


def get_type_of_business_from_entity_code(
    legal_entity_code: int,
) -> str | None:
    return legal_entity_codes.get(legal_entity_code, None)


def get_entity_code_from_type_of_business(
    type_of_business: str,
) -> int | None:
    return legal_entity_labels.get(type_of_business, None)


def get_country_name_from_landcode(landcode: int):
    return country_codes.get(landcode, None)


def get_landcode_from_country_name(country_name: str):
    return country_names.get(country_name, None)


def get_operator_values_from_filter(
    filter_dict, default_operator=FilterOperator.or_operator
):
    operator = filter_dict.get("operator", default_operator)
    values = filter_dict.get("values")
    return operator, values


def getattr_from_dict(obj: dict, attr, default_val=None):
    """
    Get attribute from dict recursively or else the default_val
    attr is the key ref seperated by dots .
    for example:
    getattr_from_dict({"request": {"folder": {"uuid", "3f7f8a0d-365f-4a85-aa7a-38d74e9f4dff"}}}, "request.folder.uuid")
    """
    attrs = attr.split(".")
    if len(attrs) == 1 or obj.get(attrs[0]) is None:
        val = obj.get(attrs[0], default_val)
        # deal with None values in dict when having another default_val
        if val is None and default_val is not None:
            return default_val
        else:
            return val
    else:
        return getattr_from_dict(
            obj.get(attrs[0], {}),
            attr[attr.find(".") + 1 :],
            default_val=default_val,
        )


class TimedInMilliseconds:
    """Use this timer to determine if a block of code is executed within time.
    The threshold in milliseconds must be passed as an argument to the constructor.
    Usage example:
    with TimedInMilliseconds(50) as executed_in_time:
        ... (do potentionally long thing)

     if executed_in_time():
         ... (code that must be excecuted when above code is executed within 50ms.)

    """

    def __init__(self, name: str, threshold_ms: int):
        self.name = name
        self.threshold_ms = threshold_ms
        self.logger = logging.getLogger(__name__)

    def __enter__(self):
        self.start = time.perf_counter() * 1000
        self.end = 0.0

        def _within_threshold():
            duration = self.end - self.start
            within_threshold = self.threshold_ms >= duration

            if not within_threshold:
                self.logger.debug(
                    f"Timed code '{self.name}' took longer than "
                    f"{self.threshold_ms}ms: {duration:.3f}ms"
                )

            return within_threshold

        return _within_threshold

    def __exit__(self, *args):
        self.end = time.perf_counter() * 1000


class HTMLTagStripper(HTMLParser):
    def __init__(self, html_string: str, rich_text_field_value=False):
        super().__init__()
        self.reset()
        self.html_string = html_string
        self.strict = False
        self.convert_charrefs = True
        self.stripped_text = ""
        self.order_list_index = 0
        self.skip = 0
        self.currenty_parsing_ol = False
        self.prefix = ""
        self.rich_text_field_value = rich_text_field_value
        self.feed(html_string)

    def handle_data(self, d) -> None:
        # In HTML, whitespace is "collapsed" into a single space.
        d = re.sub(r"\s+", " ", d, count=0)
        d = d.strip()

        if d and self.skip == 0:
            self.stripped_text += d

    def _add_newline_to_stripped_text(self):
        self.stripped_text += "\n" + self.prefix

    def _handle_li(self, attrs: list[tuple[str, str | None]]):
        base_string = (
            f"{self.order_list_index}. " if self.currenty_parsing_ol else "- "
        )
        self.stripped_text += f"{base_string}" + "".join(
            attr[0] for attr in attrs
        )
        if self.currenty_parsing_ol:
            self.order_list_index += 1

    def handle_starttag(
        self, tag: str, attrs: list[tuple[str, str | None]]
    ) -> None:
        match tag:
            case "blockquote":
                self.prefix += "> "
                self._add_newline_to_stripped_text()
            case "ol":
                self.order_list_index += 1
                self.currenty_parsing_ol = True
                self._add_newline_to_stripped_text()
            case "ul":
                self._add_newline_to_stripped_text()
            case "li":
                self._handle_li(attrs)
            case "a":
                for att in [a for a in attrs if a[0] == "href"]:
                    self.stripped_text += f"{att[1]} -> "
            case "head" | "script" | "style":
                self.skip += 1
            case "br":
                self.stripped_text += "\n" + self.prefix

    def handle_endtag(self, tag: str) -> None:
        match tag:
            case "blockquote":
                self.prefix = self.prefix[0:-2]
            case "p" | "div":
                # One newline to end the paragraph/div
                self._add_newline_to_stripped_text()
                # One newline to have an empty line between paragraphs
                if not self.rich_text_field_value:
                    # Rich text fields are special: <p>s are "single-spaced"
                    self._add_newline_to_stripped_text()
                # (One newline to bring them all and in the darkness bind them)
            case "li":
                self._add_newline_to_stripped_text()
            case "ol":
                self.order_list_index = 0
                self.currenty_parsing_ol = False
            case "head" | "script" | "style":
                self.skip -= 1

    def handle_startendtag(self, tag, attrs):
        if tag == "br":
            self._add_newline_to_stripped_text()
