# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from .test_zsnl_admin_http import MockRequest
from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden
from unittest import mock
from uuid import uuid4
from zsnl_admin_http.views import document_template_views
from zsnl_domains.admin.catalog.entities.document_template import (
    DocumentTemplate,
)


class TestDocumentTemplateViews:
    def test_document_template_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )
        views = [
            document_template_views.create_document_template,
            document_template_views.edit_document_template,
            document_template_views.get_document_template_detail,
            document_template_views.delete_document_template,
        ]
        for view in views:
            with pytest.raises(HTTPForbidden):
                view(request)

    def test_get_document_template_detail_incorrect_params(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )

        with pytest.raises(HTTPBadRequest):
            document_template_views.get_document_template_detail(request)

    def test_get_document_template_detail(self):
        uuid = str(uuid4())
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"document_template_id": uuid},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
            current_route=f"/api/v2/admin/catalog/get_document_template_detail?document_template_id={uuid}",
        )
        mock_document_template = DocumentTemplate(
            uuid=uuid,
            id=2,
            name="document",
            integration_uuid="69735eae-3d20-4a02-8571-4ebd87417876",
            integration_reference="template name",
            file_uuid=None,
            file_name=None,
            help="extra info",
            category_uuid=None,
        )
        mock_query_instance.get_document_template_details_for_edit.return_value = mock_document_template

        res = document_template_views.get_document_template_detail(request)
        assert res == {
            "data": {
                "type": "document_template",
                "id": uuid,
                "attributes": {
                    "name": mock_document_template.name,
                    "integration_uuid": mock_document_template.integration_uuid,
                    "integration_reference": mock_document_template.integration_reference,
                    "file_uuid": mock_document_template.file_uuid,
                    "file_name": mock_document_template.file_name,
                    "category_uuid": mock_document_template.category_uuid,
                    "help": mock_document_template.help,
                },
            },
            "links": {
                "self": f"/api/v2/admin/catalog/get_document_template_detail?document_template_id={uuid}"
            },
        }

    def test_create_document_template(self):
        mock_command = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
        )
        # validation of fields will happen in the domain
        request.body = {
            "document_template_uuid": str(uuid4()),
            "fields": {"name": "doc1"},
        }
        res = document_template_views.create_document_template(request)
        assert res == {"data": {"success": True}}
        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = document_template_views.create_document_template(request)

    def test_edit_document_template(self):
        mock_command = mock.MagicMock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
        )
        # validation of fields will happen in the domain
        request.body = {
            "document_template_uuid": str(uuid4()),
            "fields": {"name": "doc1"},
        }
        res = document_template_views.edit_document_template(request)
        assert res == {"data": {"success": True}}

        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = document_template_views.edit_document_template(request)

    def test_delete_document_template(self):
        mock_command = mock.MagicMock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
        )
        # validation of fields will happen in the domain
        request.body = {
            "document_template_uuid": str(uuid4()),
            "reason": "reason for delete",
        }
        res = document_template_views.delete_document_template(request)
        assert res == {"data": {"success": True}}

        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = document_template_views.delete_document_template(request)
