# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pyramid.httpexceptions
import pyramid.response
import pytest
import zsnl_admin_http.views.integrations.email as email
import zsnl_domains.admin.integrations.entities.integration as integration
from .test_zsnl_admin_http import MockRequest
from unittest import mock
from uuid import uuid4


class TestEmailIntegrationViews:
    def test_start_oauth2_flow(self):
        integration_uuid = uuid4()

        mock_query_instance = mock.Mock()
        mock_command_instance = mock.Mock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": json.dumps(
                        {
                            "subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                            "permissions": {"admin": True},
                        }
                    )
                }
            },
            params={"integration_uuid": str(integration_uuid)},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
            command_instances={
                "zsnl_domains.admin.integrations": mock_command_instance
            },
        )

        mock_integration = mock.Mock()
        mock_integration.generate_login_url.return_value = (
            integration.LoginUrlData(
                "redirect_url",
                b"code_verifier",
                b"state",
            )
        )

        mock_query_instance.get_email_integration.return_value = (
            mock_integration
        )

        rv = email.start_oauth2_flow(request)

        assert isinstance(rv, pyramid.response.Response)
        assert rv.status_int == 302
        assert rv.location == "redirect_url"

        mock_query_instance.get_email_integration.assert_called_once_with(
            integration_uuid=str(integration_uuid)
        )

        mock_integration.generate_login_url.assert_called_once_with(
            redirect_uri="https://example.com/finish_oauth2_flow"
        )
        mock_command_instance.save_email_integration_login_state.assert_called_once_with(
            integration_uuid=str(integration_uuid),
            code_verifier=b"code_verifier",
            state=b"state",
        )

    def test_start_oauth2_flow_unauthorized(self):
        integration_uuid = uuid4()

        mock_query_instance = mock.Mock()
        mock_command_instance = mock.Mock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": json.dumps(
                        {
                            "subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                            "permissions": {"not_admin": True},
                        }
                    )
                }
            },
            params={"integration_uuid": str(integration_uuid)},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
            command_instances={
                "zsnl_domains.admin.integrations": mock_command_instance
            },
        )

        with pytest.raises(pyramid.httpexceptions.HTTPForbidden):
            _ = email.start_oauth2_flow(request)

        mock_command_instance.save_email_integration_login_state.assert_not_called()

    def test_finish_oauth2_flow(self):
        mock_query_instance = mock.Mock()
        mock_command_instance = mock.Mock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": json.dumps(
                        {
                            "subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                            "permissions": {"admin": True},
                        }
                    )
                }
            },
            params={"code": "code", "state": "state"},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
            command_instances={
                "zsnl_domains.admin.integrations": mock_command_instance
            },
        )

        mock_integration = mock.Mock()
        mock_integration.generate_login_url.return_value = (
            "redirect_url",
            "code_verifier",
            "state",
        )

        mock_query_instance.get_email_integration.return_value = (
            mock_integration
        )

        rv = email.finish_oauth2_flow(request)

        assert rv == "Authorisatie geslaagd. Dit venster mag gesloten worden."

        mock_command_instance.exchange_code_for_refresh_token.assert_called_once_with(
            redirect_uri="https://example.com/finish_oauth2_flow",
            state="state",
            code="code",
        )

    def test_finish_oauth2_flow_remote_error(self):
        mock_query_instance = mock.Mock()
        mock_command_instance = mock.Mock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": json.dumps(
                        {
                            "subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                            "permissions": {"admin": True},
                        }
                    )
                }
            },
            params={
                "code": "code",
                "state": "state",
                "error": "error",
                "error_description": "something's wrong",
            },
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
            command_instances={
                "zsnl_domains.admin.integrations": mock_command_instance
            },
        )

        mock_integration = mock.Mock()
        mock_integration.generate_login_url.return_value = (
            "redirect_url",
            "code_verifier",
            "state",
        )

        mock_query_instance.get_email_integration.return_value = (
            mock_integration
        )

        rv = email.finish_oauth2_flow(request)

        assert rv == "Error: error: something's wrong"

        mock_command_instance.exchange_code_for_refresh_token.assert_not_called()

    def test_finish_oauth2_flow_not_allowed(self):
        integration_uuid = uuid4()

        mock_query_instance = mock.Mock()
        mock_command_instance = mock.Mock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": json.dumps(
                        {
                            "subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                            "permissions": {"not_admin": True},
                        }
                    )
                }
            },
            params={"integration_uuid": str(integration_uuid)},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
            command_instances={
                "zsnl_domains.admin.integrations": mock_command_instance
            },
        )

        with pytest.raises(pyramid.httpexceptions.HTTPForbidden):
            _ = email.finish_oauth2_flow(request)

        mock_command_instance.save_email_integration_login_state.assert_not_called()
