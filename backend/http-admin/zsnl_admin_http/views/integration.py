# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class IntegrationViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "get_integrations": {
                "cq": "query",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.admin.integrations",
                "run": "get_integrations",
                "from": {"request_params": {}},
                "entity_type": "integration",
            },
            "get_transaction": {
                "cq": "query",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.admin.integrations",
                "run": "get_transaction",
                "from": {
                    "request_params": {
                        "uuid": "uuid",
                    }
                },
                "entity_type": "transaction",
            },
            "get_transaction_data": {
                "cq": "query",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.admin.integrations",
                "run": "get_transaction_data",
                "from": {
                    "request_params": {
                        "uuid": "uuid",
                    }
                },
                "entity_type": "transaction_data",
            },
            "get_transactions": {
                "cq": "query",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.admin.integrations",
                "run": "get_transactions",
                "from": {
                    "request_params": {
                        "page": "page",
                        "page_size": "page_size",
                        "sort": "sort",
                        "filters[]": "filters",
                        "cursor": "cursor",
                    }
                },
                "entity_type": "transaction",
            },
            "get_transaction_records": {
                "cq": "query",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.admin.integrations",
                "run": "get_transaction_records",
                "from": {
                    "request_params": {
                        "transaction_uuid": "transaction_uuid",
                    }
                },
                "entity_type": "transaction_record",
            },
        },
    }
