# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.request import Request


@protected_route("beheer_zaaktype_admin")
def delete_object_type(request: Request, user_info):
    try:
        object_type_uuid = request.json_body["object_type_uuid"]
        reason = request.json_body["reason"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"uuid": object_type_uuid, "reason": reason}

    cmd = request.get_command_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )
    cmd.delete_object_type(**command_params)

    return {"data": {"success": True}}
