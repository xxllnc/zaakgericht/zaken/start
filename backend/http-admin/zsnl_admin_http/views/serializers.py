# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs import UserInfo
from zsnl_domains.admin.catalog.entities import CaseTypeEntity, attribute


def get_iso_format_date(date):
    if date and hasattr(date, "isoformat"):
        return date.isoformat()
    return None


def related_case_type(ct):
    return {
        "type": "case_type",
        "id": str(ct.uuid),
        "attributes": {
            "name": ct.name,
            "active": ct.active,
            "version": ct.version,
            "is_current_version": ct.is_current_version,
        },
    }


def folder_entry(entry):
    return {
        "type": "folder_entry",
        "id": str(entry.uuid),
        "attributes": {
            "type": entry.entry_type,
            "name": entry.name,
            "active": entry.active,
        },
        "links": {
            "self": f"/api/v2/admin/catalog/get_entry_detail?type={entry.entry_type}&item_id={entry.uuid}"
        },
    }


def root_folder_details():
    return {
        "type": "folder",
        "id": None,
        "attributes": {
            "name": None,
            "parent_name": None,
            "parent_id": None,
            "last_modified": None,
        },
        "links": {"self": "/api/v2/admin/catalog/get_folder_contents"},
    }


def folder_details(details, user_info=None):
    if not details.uuid:
        return root_folder_details()
    return {
        "type": "folder",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "parent_name": details.parent_name,
            "parent_id": str(details.parent_uuid)
            if details.parent_uuid
            else None,
            "last_modified": details.last_modified.isoformat()
            if details.last_modified
            else None,
        },
        "links": {
            "self": f"/api/v2/admin/catalog/get_folder_contents?folder_id={details.uuid!s}"
        },
    }


def case_type_version_history_details(details):
    return {
        "type": "case_type_version",
        "case_type_id": str(details.case_type_uuid),
        "attributes": {
            "id": str(details.uuid),
            "name": details.name,
            "username": details.username,
            "display_name": details.display_name,
            "created": get_iso_format_date(details.created),
            "last_modified": get_iso_format_date(details.last_modified),
            "active": details.active,
            "reason": details.reason,
            "version": details.version,
            "change_note": details.change_note,
            "modified_components": details.modified_components,
        },
    }


def case_type_details(details: CaseTypeEntity, user_info: UserInfo):
    return {
        "type": "case_type",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "last_modified": details.last_modified.isoformat()
            if details.last_modified
            else None,
            "active": details.active,
            "identification": details.identification,
            "current_version": details.current_version,
            "context": details.initiator_source,
        },
        "relationships": {
            "used_in_case_types": [
                related_case_type(ct) for ct in details.used_in_case_types
            ],
            "folder": folder_details(details.folder),
        },
        "links": {
            "export": f"/beheer/zaaktypen/{details.uuid}/export",
            "version_history": f"/api/v2/admin/catalog/get_case_type_history?case_type_id={details.uuid}",
        },
    }


def attribute_details(details, user_info):
    return {
        "type": "attribute",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "last_modified": details.last_modified.isoformat()
            if details.last_modified
            else None,
            "magic_string": details.magic_string,
            "is_multiple": details.is_multiple,
            "value_type": details.value_type,
        },
        "relationships": {
            "used_in_case_types": [
                related_case_type(ct) for ct in details.used_in_case_types
            ],
            "folder": folder_details(details.folder),
        },
    }


def email_template_details(details, user_info):
    return {
        "type": "email_template",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "last_modified": details.last_modified.isoformat()
            if details.last_modified
            else None,
        },
        "relationships": {
            "used_in_case_types": [
                related_case_type(ct) for ct in details.used_in_case_types
            ],
            "folder": folder_details(details.folder),
        },
    }


def document_template_details(details, user_info):
    return {
        "type": "document_template",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "filename": details.filename,
            "has_default_integration": details.has_default_integration,
            "last_modified": details.last_modified.isoformat()
            if details.last_modified
            else None,
        },
        "relationships": {
            "used_in_case_types": [
                related_case_type(ct) for ct in details.used_in_case_types
            ],
            "folder": folder_details(details.folder),
        },
        "links": {
            "download": f"/beheer/bibliotheek/sjablonen/x/0/download/{details.uuid}"
        },
    }


def object_type_details(details, user_info):
    return {
        "type": "object_type",
        "id": str(details.uuid),
        "attributes": {"name": details.name},
        "relationships": {
            "used_in_case_types": [
                related_case_type(ct) for ct in details.used_in_case_types
            ],
            "folder": folder_details(details.folder),
        },
    }


def custom_object_type_details(details, user_info):
    return {
        "type": "custom_object_type",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "version_independent_uuid": str(details.version_independent_uuid),
            "title": details.title,
            "status": details.status,
            "version": details.version,
            "external_reference": details.external_reference,
            "date_created": get_iso_format_date(details.date_created),
            "last_modified": get_iso_format_date(details.last_modified),
        },
        "relationships": {"folder": folder_details(details.folder)},
    }


def attribute_details_for_edit(details):
    return {
        "type": "attribute",
        "id": str(details.uuid),
        "attributes": {
            "attribute_type": details.attribute_type,
            "name": details.name,
            "public_name": details.public_name,
            "magic_string": details.magic_string,
            "type_multiple": details.type_multiple,
            "sensitive_field": details.sensitive_field,
            "help": details.help,
            "value_default": details.value_default,
            "category_name": details.category_name,
            "category_uuid": str(details.category_uuid)
            if details.category_uuid
            else None,
            "document_origin": details.document_origin,
            "document_trust_level": details.document_trust_level,
            "document_source": details.document_source,
            "document_category": details.document_category,
            "relationship_data": {
                "type": details.relationship_type,
                "name": details.relationship_name,
                "uuid": str(details.relationship_uuid)
                if details.relationship_uuid
                else None,
            },
            "attribute_values": details.attribute_values,
            "appointment_location_id": details.appointment_location_id,
            "appointment_product_id": details.appointment_product_id,
            "appointment_interface_uuid": details.appointment_interface_uuid,
        },
    }


def integration(integration):
    return {
        "type": "integration",
        "id": str(integration.uuid),
        "attributes": {
            "module": integration.module,
            "name": integration.name,
            "active": integration.active,
        },
    }


def attribute_search_results(
    results: list[attribute.AttributeDetailSearchResult],
) -> list:
    serialized_response = []
    for r in results:
        serialized_response.append(
            {
                "type": "attribute",
                "id": str(r.uuid),
                "attributes": {
                    "name": r.name,
                    "magic_string": r.magic_string,
                    "value_type": r.value_type,
                    "is_multiple": r.is_multiple,
                    "relationship_type": r.relationship_type,
                },
            }
        )
    return serialized_response


def email_template_details_for_edit(details):
    return {
        "type": "email_template",
        "id": str(details.uuid),
        "attributes": {
            "label": details.label,
            "sender": details.sender if details.sender is not None else "",
            "sender_address": details.sender_address
            if details.sender_address is not None
            else "",
            "subject": details.subject,
            "message": details.message,
            "category_uuid": str(details.category_uuid)
            if details.category_uuid
            else None,
            "attachments": details.attachments,
        },
    }


def document_template_details_for_edit(details):
    return {
        "type": "document_template",
        "id": str(details.uuid),
        "attributes": {
            "name": details.name,
            "file_uuid": str(details.file_uuid) if details.file_uuid else None,
            "file_name": str(details.file_name) if details.file_name else None,
            "integration_uuid": str(details.integration_uuid)
            if details.integration_uuid
            else None,
            "integration_reference": details.integration_reference,
            "help": details.help,
            "category_uuid": str(details.category_uuid)
            if details.category_uuid
            else None,
        },
    }
