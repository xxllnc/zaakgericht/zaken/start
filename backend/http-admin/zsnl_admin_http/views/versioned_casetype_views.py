# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# from ._shared import create_link_from_entity
from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class VersionedCaseTypeViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "get_versioned_casetype": {
                "cq": "query",
                "auth_permissions": {"beheer_zaaktype_admin"},
                "domain": "zsnl_domains.admin.catalog",
                "run": "get_versioned_casetype_with_uuid",
                "from": {
                    "request_params": {
                        "uuid": "uuid",
                        "version_uuid": "version_uuid",
                    }
                },
            }
        },
        "POST": {
            "create_versioned_casetype": {
                "cq": "command",
                "auth_permissions": {"beheer_zaaktype_admin"},
                "domain": "zsnl_domains.admin.catalog",
                "run": "create_versioned_casetype",
                "from": {"json": {"_all": True}},
                "generate_missing_id": [
                    "casetype_uuid",
                    "casetype_version_uuid",
                ],
                "command_primary": "casetype_version_uuid",
                "entity_type": "versioned_casetype",
            },
            "update_versioned_casetype": {
                "cq": "command",
                "auth_permissions": {"beheer_zaaktype_admin"},
                "domain": "zsnl_domains.admin.catalog",
                "run": "update_versioned_casetype",
                "from": {"json": {"_all": True}},
                "generate_missing_id": [
                    "casetype_uuid",
                    "casetype_version_uuid",
                ],
                "command_primary": "casetype_version_uuid",
                "entity_type": "versioned_casetype",
            },
        },
    }
