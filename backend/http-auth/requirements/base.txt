setuptools>=69.2.0

../minty
../minty-pyramid

../minty-infra-sqlalchemy

python-json-logger~=2.0
requests~=2.24
PyYAML~=6.0.1

# project dependencies 
../domains
../zsnl-pyramid
