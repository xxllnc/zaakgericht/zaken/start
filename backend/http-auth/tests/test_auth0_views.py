# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import codecs
import jose.jwt
import json
import pytest
import yaml
from datetime import datetime, timedelta
from jose.backends import RSAKey
from jose.constants import ALGORITHMS
from minty.cqrs import UserInfo
from pyramid.response import Response
from requests.exceptions import HTTPError, RequestException
from typing import Final
from unittest import TestCase, mock
from uuid import uuid4
from zsnl_auth_http.__init__ import log_record_factory
from zsnl_auth_http.views import auth0
from zsnl_auth_http.views.auth0 import (
    ConfigError,
    RequestParameterError,
    StateValidationError,
    TokenValidationError,
)
from zsnl_domains.auth.entities import Role, TokenResponse, User
from zsnl_domains.auth.exceptions import (
    Auth0Error,
    CreateUserByNameError,
    FindActiveInterfaceError,
    FindUserByNameError,
    InterfaceNotActiveWarning,
    NewUserNotFoundError,
    UserMarkedAsDeletedWarning,
    UserNotActiveWarning,
)

INSTANCE_UUID: Final = "instance_uuid"
OIDC: Final = "oidc"
OIDC_CLIENT_ID: Final = "client_id"
OIDC_CLIENT_SECRET: Final = "client_secret"
OIDC_DOMAIN: Final = "domain"
OIDC_ORGANIZATION_ID: Final = "oidc_organization_id"

HTTP_X_REAL_IP: Final = "X-Real-IP"
HTTP_X_FORWARDED_FOR: Final = "X-Forwarded-For"

PRIVATE_KEY_2048_PKCS1: Final = b"""-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAom6GcUPchmHxBuV3zJ60EPC7y30WiiVxn1WXSPHmfqaj0q2U
xS03YugkYmX9lB/EQ6Z5bOY9VuL1oMudL6Dkb9aYYEBZHVgejV7vtYuYT19QMesn
AsmGq8etie7XyWHzfWTxljbF53yvxXJMixcFzebAov9pUiV9Hmy3hYVLw3J1NXVg
gPZpUT2oF+qAayhPsOi2b0CrIE3FvioDx7IiRXKFpV/1gah3NRSKxCrsxV6V+UGO
+trP1ViWiu4oXB5j25kZmkgI0lXG60p58DUUeCOnEemvurltf9T9IEs7LGBEzUYm
itGSY4ZOY3MabPypRfFRRotZEDyZjshq4xfXAwIDAQABAoIBAFclRzoTd4gdmevi
RwDgEKmaDpchGGurpScgC5eWONywWOpaOJwFI1cMRyEHqSHEXU8STMkxSa2I/NF1
DHMWNhkOoBfbzjPhKBse2Sqkp2XGNEdj6z0ik/8rlR6QpvMjezhGZRr7bfhBPCiJ
pylkg7exWp7Yu0/YTyV4nImlNz23GvrYHFtzDzTtn9gW4fe46wI08s4PqH/TyBh8
QkwkTwOKTk6n/xz2hND/shUOGjaoS0o6y4+8v3O1JYUWa7YZaIFofvF/dHR0yieg
2gQjc0c6+VeBm8dEbn3he+KnIBwQbWsiCuWL6Jq4XPtMbqutfovIYf9lRB+3q2PI
VSh3mwECgYEAzhOhG+usoxjJGk2wVJH5wnHL0zyH8gWF4SnnxwwdBOF4kdLB2eva
SJsi8rJQMT0TC4wZ6TsD2fJXGazIyM6OnD+52AViiUsLVS5MR7qEMNitdkWEtDx9
Xve50NF9XkTrn6+cgqvfJ9ezE4cOaiD3Eov1u/HbHRx3K2Qf9IzvGoMCgYEAycgk
yOSYR0A3xKcfCQyOU0THEZWBBzd6zBAQ6VQNbcWHh1E8Ve0td6cvCxfydW1pIzXE
7b7J/BgMoC9oSQPfqQJQF2O0XESrdNgXjscfFpVgPfzbFQNgf7d0DSq4b/A5n5QR
HVMmWzVQoRQUwqTNeVxs0NpY6W6Meqv3i/KJqYECgYA/KyMyhM55fCqA5pmLgueV
Y/5/tMlTNcAxIgBLMnpeuaKUyI7ldveFVBClZmVQgpEo8/wpUw6+Kxvp4d32N+Ld
IGeeQSBQR3Gk3blCL3k/49tgKrUf7n7bsoIB8YVFdUjovRLzty2DcAoTjU2s2IgD
5mUgBGYPCV+6LEnjU6QjcwKBgGg+0FJBVzKoSKd+N5hzNixqwfWhqXFTBkvamQIS
fIWToTsVivhRekXwx2sRyh9EkSaxprW09aEZw5wWIehm6evk1//dcNaiW3oYEcOf
t73xGjGsKnsmrXoOCxSqV3LtRrfcxSLDTHOejbNKLpeIkOb8CvOzem/OvyC5K0DP
4rMBAoGBAJStRo5xQ2F9cyZW8vLd4eR3FHXxF/7Moxr6AyV3RLUjMhkwB8ZcFLUQ
dXI4NN9leDeIpNaGU6ozr+At3f50GtCWxdUppy9FDh5qDamBV8K4/+uNqFPiKFQ9
uwNcJ8daMgVZ0QBrD3CBcSZQrfC484BlV6spJ3C16qDVSQPt7sAI
-----END RSA PRIVATE KEY-----"""

TEST_JWK_PRIVATE = RSAKey(PRIVATE_KEY_2048_PKCS1, ALGORITHMS.RS256)


user_info = UserInfo(user_uuid="subject_uuid", permissions="permissions")


def mock_protected_route(permission, *args):
    def view_wrapper(view):
        def request_wrapper(request, user_info):
            return view(request=request, user_info=user_info)

        return request_wrapper

    return view_wrapper


class MockRedis:
    def __init__(self, initial_cache_content=None):
        if initial_cache_content is None:
            self.cache_content = {}
        else:
            self.cache_content = initial_cache_content

        self.calls = []

    def __getitem__(self, key: str | bytes):
        self.calls.append(("__getitem__", key))
        return self.cache_content[key]

    def set(self, name: str | bytes, value: str | bytes, **kwargs):
        self.calls.append(("set", name, value, kwargs))
        self.cache_content[name] = value

    def get(self, key: str | bytes) -> str | bytes | None:
        result = self.cache_content.get(key, (None,))
        if isinstance(result, tuple):
            return result[0]
        return result

    def getdel(self, key: str | bytes) -> str | bytes | None:
        result = self.cache_content.get(key, (None,))

        try:
            del self.cache_content[key]
        except:  # noqa
            pass

        if isinstance(result, tuple):
            return result[0]
        return result


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int(iat.timestamp()),
            "exp": int((iat + timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "test.example"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    payload: dict = auth0._retrieve_decoded_id_token(
        id_token=id_token,
        client_id="test_client",
        domain="example.com",
        instance_identifier="instance_id",
        cache=mock_redis,
    )

    assert len(payload) == 8
    assert payload["email"] == "test@example.com"
    assert payload["iss"] == "https://example.com/"
    assert payload["aud"] == "test_client"
    assert payload["iat"] == int(iat.timestamp())
    assert payload["exp"] == int((iat + timedelta(seconds=60)).timestamp())
    assert payload["sub"] == "sub"
    assert payload["sid"] == "sid"
    assert payload["org_id"] == "org_123456789"


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_header_kid_not_found(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int(iat.timestamp()),
            "exp": int((iat + timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers=None,
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(KeyError):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_header_invalid(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int(iat.timestamp()),
            "exp": int((iat + timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "invalid.kid"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(Exception):  # noqa: B017
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_get_request_not_ok(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int((iat - timedelta(seconds=60)).timestamp()),
            "exp": int((iat - timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "test.example"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {}
    mock_metadata_response.status_code = 400

    mock_requests.get.side_effect = [
        mock_metadata_response,
    ]

    with pytest.raises(HTTPError):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_jwks_requestexception(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int((iat - timedelta(seconds=60)).timestamp()),
            "exp": int((iat - timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "test.example"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    mock_keys_response = RequestException

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(RequestException):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_expired_signature_error(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int((iat - timedelta(seconds=60)).timestamp()),
            "exp": int((iat - timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "test.example"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(jose.jwt.ExpiredSignatureError):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_claim_error_aud_invalid(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int(iat.timestamp()),
            "exp": int((iat + timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "test.example"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(jose.jwt.JWTClaimsError):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client_invalid",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_claim_error_iss_invalid(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int(iat.timestamp()),
            "exp": int((iat + timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={"kid": "test.example"},
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(jose.jwt.JWTClaimsError):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com.invalid",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_retrieve_decoded_id_token_jtw_error_unable_to_decode(mock_requests):
    mock_redis = MockRedis()

    iat = datetime.now()

    id_token = jose.jwt.encode(
        claims={
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "iat": int(iat.timestamp()),
            "exp": int((iat + timedelta(seconds=60)).timestamp()),
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        },
        key=TEST_JWK_PRIVATE,
        headers={
            "kid": "test.example",
            "alg": "",
        },
        algorithm=jose.constants.ALGORITHMS.RS256,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks",
    }
    mock_metadata_response.status_code = 200

    public_keys: dict = TEST_JWK_PRIVATE.public_key().to_dict()
    public_keys["kid"] = "test.example"

    mock_keys_response = mock.MagicMock()
    mock_keys_response.json.return_value = {"keys": [public_keys]}
    mock_keys_response.status_code = 200

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    with pytest.raises(jose.jwt.JWTError):
        auth0._retrieve_decoded_id_token(
            id_token=id_token,
            client_id="test_client",
            domain="example.com",
            instance_identifier="instance_id",
            cache=mock_redis,
        )


@mock.patch(
    "zsnl_auth_http.views.auth0._create_redis_session_add_login_attempt"
)
@mock.patch("zsnl_auth_http.views.auth0._find_user_by_token_email")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_successfull(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
    mock_find_user_by_token_email,
    mock_create_redis_session_add_login_attempt,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    token_email: str = "test@example.com"
    mock_retrieve_email_from_token.return_value = token_email

    test_user: User = User.create(
        id=1,
        uuid=uuid4(),
        name="email@example.com",
        login_entity_id=1,
        role_ids=[],
        active=True,
        date_deleted=None,
    )

    mock_find_user_by_token_email.return_value = test_user

    mock_create_redis_session_add_login_attempt.return_value = True

    response = auth0.callback(request=mock_requests)
    assert isinstance(response, Response)
    assert response.status_code == 302
    assert response.location == "/auth/page/login_success"


@mock.patch(
    "zsnl_auth_http.views.auth0._create_redis_session_add_login_attempt"
)
@mock.patch("zsnl_auth_http.views.auth0._find_user_by_token_email")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_successfull_redirect_to_first_login(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
    mock_find_user_by_token_email,
    mock_create_redis_session_add_login_attempt,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    token_email: str = "test@example.com"
    mock_retrieve_email_from_token.return_value = token_email

    test_user: User = User.create(
        id=1,
        uuid=uuid4(),
        name="email@example.com",
        login_entity_id=1,
        role_ids=[],
        active=True,
        date_deleted=None,
    )

    mock_find_user_by_token_email.return_value = test_user

    mock_create_redis_session_add_login_attempt.return_value = True

    response = auth0.callback(request=mock_requests)
    assert isinstance(response, Response)
    assert response.status_code == 302
    assert response.location == "/auth/page/login_success"


@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_failed_exception_in_get_auth0_authorize_response_para(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
):
    mock_get_auth0_authorize_response_parameters.side_effect = (
        RequestParameterError("Required parameter not found")
    )

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_failed_exception_in_get_configuration(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    mock_get_configuration.side_effect = ConfigError(
        "Required configuration not found"
    )

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_failed_exception_in_validate_state(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.side_effect = StateValidationError(
        "Auth0 State validation error"
    )

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_failed_exception_in_query_get_token(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    mock_query_instance = mock.MagicMock()
    mock_requests.get_query_instance.side_effect = [
        mock_query_instance,
    ]
    mock_query_instance.get_token.side_effect = Auth0Error()

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_failed_exception_in_decoded_id(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    tokens: TokenResponse = None
    mock_query_instance = mock.MagicMock()
    mock_requests.get_query_instance.side_effect = [
        mock_query_instance,
    ]
    mock_query_instance.get_token.side_effect = tokens

    mock_retrieve_decoded_id_token.side_effect = Exception(
        "Invalid token header"
    )

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_failed_exception_in_retrieve_email(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    tokens: TokenResponse = None
    mock_query_instance = mock.MagicMock()
    mock_requests.get_query_instance.side_effect = [
        mock_query_instance,
    ]
    mock_query_instance.get_token.side_effect = tokens

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    mock_retrieve_email_from_token.side_effect = TokenValidationError(
        "Invalid token"
    )

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._find_user_by_token_email")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_find_user_by_token_email_auth0_exception(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
    mock_find_user_by_token_email,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    tokens: TokenResponse = None
    mock_query_instance = mock.MagicMock()
    mock_requests.get_query_instance.side_effect = [
        mock_query_instance,
    ]
    mock_query_instance.get_token.side_effect = tokens

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    token_email: str = "test@example.com"
    mock_retrieve_email_from_token.return_value = token_email

    mock_command_instance = mock.MagicMock()
    mock_requests.get_command_instance.side_effect = [
        mock_command_instance,
    ]

    mock_find_user_by_token_email.side_effect = FindUserByNameError(
        token_email
    )
    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch("zsnl_auth_http.views.auth0._find_user_by_token_email")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_find_user_by_token_email_auth0_warning(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
    mock_find_user_by_token_email,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    tokens: TokenResponse = None
    mock_query_instance = mock.MagicMock()
    mock_requests.get_query_instance.side_effect = [
        mock_query_instance,
    ]
    mock_query_instance.get_token.side_effect = tokens

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    token_email: str = "test@example.com"
    mock_retrieve_email_from_token.return_value = token_email

    mock_command_instance = mock.MagicMock()
    mock_requests.get_command_instance.side_effect = [
        mock_command_instance,
    ]

    mock_find_user_by_token_email.side_effect = UserNotActiveWarning(
        token_email
    )
    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


@mock.patch(
    "zsnl_auth_http.views.auth0._create_redis_session_add_login_attempt"
)
@mock.patch("zsnl_auth_http.views.auth0._find_user_by_token_email")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_create_redis_session_add_login_attempt_successfull(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
    mock_find_user_by_token_email,
    mock_create_redis_session_add_login_attempt,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    token_email: str = "test@example.com"
    mock_retrieve_email_from_token.return_value = token_email

    test_user: User = User.create(
        id=1,
        uuid=uuid4(),
        name="email@example.com",
        login_entity_id=1,
        role_ids=[],
        active=True,
        date_deleted=None,
    )

    mock_find_user_by_token_email.return_value = test_user

    mock_create_redis_session_add_login_attempt.return_value = True

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/page/login_success"


@mock.patch(
    "zsnl_auth_http.views.auth0._create_redis_session_add_login_attempt"
)
@mock.patch("zsnl_auth_http.views.auth0._find_user_by_token_email")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_email_from_token")
@mock.patch("zsnl_auth_http.views.auth0._retrieve_decoded_id_token")
@mock.patch("zsnl_auth_http.views.auth0._validate_state")
@mock.patch("zsnl_auth_http.views.auth0._get_configuration")
@mock.patch(
    "zsnl_auth_http.views.auth0._get_auth0_authorize_response_parameters"
)
@mock.patch("zsnl_auth_http.views.auth0.requests")
def test_callback_create_redis_session_add_login_attempt_unsuccessfull(
    mock_requests,
    mock_get_auth0_authorize_response_parameters,
    mock_get_configuration,
    mock_validate_state,
    mock_retrieve_decoded_id_token,
    mock_retrieve_email_from_token,
    mock_find_user_by_token_email,
    mock_create_redis_session_add_login_attempt,
):
    state: str = "state_1234"
    code: str = "temp_code"
    mock_get_auth0_authorize_response_parameters.return_value = state, code

    configuration: dict = {
        OIDC: {
            OIDC_CLIENT_ID: "id",
            OIDC_CLIENT_SECRET: "secret",
            OIDC_DOMAIN: "domain",
        },
        OIDC_ORGANIZATION_ID: "id",
        INSTANCE_UUID: "uuid",
    }
    mock_get_configuration.return_value = configuration

    mock_requests.infrastructure_factory.get_infrastructure(
        None, "redis"
    ).getdel.return_value = bytes(state, encoding="utf-8")

    mock_validate_state.return_value = None

    iat = datetime.now()
    payload: dict = {
        "email": "test@example.com",
        "iss": "https://example.com/",
        "aud": "test_client",
        "iat": int(iat.timestamp()),
        "exp": int((iat + timedelta(seconds=60)).timestamp()),
        "sub": "sub",
        "sid": "sid",
        "org_id": "org_123456789",
    }

    mock_retrieve_decoded_id_token.return_value = payload

    token_email: str = "test@example.com"
    mock_retrieve_email_from_token.return_value = token_email

    test_user: User = User.create(
        id=1,
        uuid=uuid4(),
        name="email@example.com",
        login_entity_id=1,
        role_ids=[],
        active=True,
        date_deleted=None,
    )

    mock_find_user_by_token_email.return_value = test_user

    mock_create_redis_session_add_login_attempt.return_value = False

    response = auth0.callback(request=mock_requests)
    assert response.status_code == 302
    assert response.location == "/auth/logout?auth0l=1"


class MockRequest:
    def __init__(
        self,
        cookies,
        sessions,
        params,
        query_instances,
        command_instances=None,
        current_route=None,
        host=None,
        infrastructure_factory=None,
        headers=None,
        client_addr=None,
    ):
        self.cookies = cookies
        self.sessions = sessions
        self.params = params
        self.query_instances = query_instances
        self.command_instances = command_instances
        self.authorization = None

        if current_route is None:
            self.current_route = "mock_current_route"
        else:
            self.current_route = current_route

        if host is None:
            self.host = "mock_host"
        else:
            self.host = host

        if infrastructure_factory is None:
            self.infrastructure_factory = mock.MagicMock()
        else:
            self.infrastructure_factory = infrastructure_factory

        if headers is None:
            self.headers = {}
        else:
            self.headers = headers

        if client_addr is None:
            self.client_addr = ""
        else:
            self.client_addr = client_addr

    @property
    def json_body(self):
        return self.body

    @property
    def session_id(self):
        return self.cookies["zaaksysteem_session"]

    @property
    def response(self):
        return mock.MagicMock()

    def retrieve_session(self):
        return self.sessions[self.session_id]

    def get_query_instance(self, domain, user_uuid, user_info=None):
        return self.query_instances[domain]

    def get_command_instance(self, domain, user_uuid, user_info=None):
        return self.command_instances[domain]

    def current_route_path(self):
        return self.current_route

    def route_url(self, name, _scheme):
        return f"{_scheme}://example.com/{name}"

    def route_path(self, name, _query=None):
        route = self.current_route.split("?")[0]
        if _query:
            route = f"{route}?folder_id={_query['folder_id']}"
        return route


class TestAuth0(TestCase):
    def test_retrieve_email_from_token(self):
        token_payload: dict = {
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        token_email: str = auth0._retrieve_email_from_token(
            payload=token_payload,
            client_id="test_client",
            domain="example.com",
            organization_id="org_123456789",
        )

        assert token_email == "test@example.com"

    def test_retrieve_email_from_token_iss_not_set(self):
        token_payload: dict = {
            "email": "test@example.com",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client",
                domain="example.com",
                organization_id="org_123456789",
            )

    def test_retrieve_email_from_token_iss_mismatch(self):
        token_payload: dict = {
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client",
                domain="example.com.mismatch",
                organization_id="org_123456789",
            )

    def test_retrieve_email_from_token_aud_not_set(self):
        token_payload: dict = {
            "email": "test@example.com",
            "iss": "https://example.com/",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client",
                domain="example.com",
                organization_id="org_123456789",
            )

    def test_retrieve_email_from_token_aud_mismatch(self):
        token_payload: dict = {
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client_not_valid",
                domain="example.com",
                organization_id="org_123456789",
            )

    def test_retrieve_email_from_token_org_id_not_set(self):
        token_payload: dict = {
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client",
                domain="example.com",
                organization_id="org_123456789",
            )

    def test_retrieve_email_from_token_org_id_mismatch(self):
        token_payload: dict = {
            "email": "test@example.com",
            "iss": "https://example.com/",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client",
                domain="example.com",
                organization_id="org_123456789_mismatch",
            )

    def test_retrieve_email_from_token_email_not_set(self):
        token_payload: dict = {
            "iss": "https://example.com/",
            "aud": "test_client",
            "sub": "sub",
            "sid": "sid",
            "org_id": "org_123456789",
        }

        with pytest.raises(auth0.TokenValidationError):
            auth0._retrieve_email_from_token(
                payload=token_payload,
                client_id="test_client",
                domain="example.com",
                organization_id="org_123456789",
            )

    def test_log_record_factory(self):
        """
        Testing the log record factory
        """
        record = log_record_factory(
            name="foo",
            level="WARN",
            pathname="/dev/null",
            lineno=42,
            args={},
            msg="bar",
            exc_info=None,
        )

        assert record.msg == "bar"
        assert record.zs_component == "zsnl_auth_http"

    def test_get_client_ip_x_real_ip(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={},
            headers={HTTP_X_REAL_IP: "127.0.0.1"},
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        client_ip: str = auth0._get_client_ip(request=request)
        assert client_ip == "127.0.0.1"

    def test_get_client_ip_x_forwarded_for(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={},
            headers={HTTP_X_FORWARDED_FOR: "127.0.0.1"},
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        client_ip: str = auth0._get_client_ip(request=request)
        assert client_ip == "127.0.0.1"

    def test_get_client_ip_x_forwarded_for_with_comma(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={},
            headers={HTTP_X_FORWARDED_FOR: "127.0.0.1, 127.0.0.2, 127.0.0.3"},
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        client_ip: str = auth0._get_client_ip(request=request)
        assert client_ip == "127.0.0.1"

    def test_get_client_ip_x_client_address(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={},
            headers={},
            client_addr="127.0.0.1",
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        client_ip: str = auth0._get_client_ip(request=request)
        assert client_ip == "127.0.0.1"

    def test_get_auth0_authorize_response_parameters(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "state": "abcdefg",
                "code": "temp_code",
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        state, code = auth0._get_auth0_authorize_response_parameters(
            request=request
        )
        assert state == "abcdefg"
        assert code == "temp_code"

    def test_get_auth0_authorize_response_parameters_state_param_missing(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "code": "temp_code",
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        with pytest.raises(RequestParameterError):
            auth0._get_auth0_authorize_response_parameters(request=request)

    def test_get_auth0_authorize_response_parameters_state_param_empty(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "state": "",
                "code": "temp_code",
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        with pytest.raises(RequestParameterError):
            auth0._get_auth0_authorize_response_parameters(request=request)

    def test_get_auth0_authorize_response_parameters_state_param_none(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "state": None,
                "code": "temp_code",
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        with pytest.raises(RequestParameterError):
            auth0._get_auth0_authorize_response_parameters(request=request)

    def test_get_auth0_authorize_response_parameters_code_param_missing(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "state": "abcdefg",
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        with pytest.raises(RequestParameterError):
            auth0._get_auth0_authorize_response_parameters(request=request)

    def test_get_auth0_authorize_response_parameters_code_param_empty(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "state": "abcdefg",
                "code": "",
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        with pytest.raises(RequestParameterError):
            auth0._get_auth0_authorize_response_parameters(request=request)

    def test_get_auth0_authorize_response_parameters_code_param_none(self):
        request = MockRequest(
            cookies={},
            sessions=None,
            params={
                "state": "abcdefg",
                "code": None,
            },
            query_instances={"zsnl_domains.auth": mock.MagicMock()},
        )

        with pytest.raises(RequestParameterError):
            auth0._get_auth0_authorize_response_parameters(request=request)

    def test_get_configuration(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: "domain",
            },
            OIDC_ORGANIZATION_ID: "id",
            INSTANCE_UUID: "uuid",
        }
        configuration: dict = auth0._get_configuration(request=request)
        assert configuration is not None

    def test_get_configuration_no_configuration(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {}
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_no_oidc(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {"test": None}
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_not_valid(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {OIDC: None}
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_client_id_not_found(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_SECRET: None,
                OIDC_DOMAIN: None,
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_client_secret_not_found(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: None,
                OIDC_DOMAIN: None,
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_domain_not_found(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: None,
                OIDC_CLIENT_SECRET: None,
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_client_id_not_none(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: None,
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: "domain",
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_client_secret_not_none(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: None,
                OIDC_DOMAIN: "domain",
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_domain_not_none(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: None,
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_no_oidc_organization_id(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: "domain",
            }
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_oidc_organization_id_not_valid(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: "domain",
            },
            OIDC_ORGANIZATION_ID: None,
            INSTANCE_UUID: "uuid",
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_no_instance_uuid(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: "domain",
            },
            OIDC_ORGANIZATION_ID: "id",
        }
        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_get_configuration_instance_uuid_not_valid(self):
        request = mock.MagicMock()
        request.infrastructure_factory.get_config.return_value = {
            OIDC: {
                OIDC_CLIENT_ID: "id",
                OIDC_CLIENT_SECRET: "secret",
                OIDC_DOMAIN: "domain",
            },
            OIDC_ORGANIZATION_ID: "id",
            INSTANCE_UUID: None,
        }

        with pytest.raises(ConfigError):
            auth0._get_configuration(request=request)

    def test_validate_state_state(self):
        state: str = "state_1234"
        session_id = "session_id_1234"
        instance_identifier: str = "instance_id"
        cache_key: str = f"{instance_identifier}:auth0:state:{session_id}"

        mock_redis = MockRedis()
        mock_redis.set(cache_key, bytes(state, encoding="utf-8"))

        auth0._validate_state(
            redis=mock_redis,
            state=state,
            session_id=session_id,
            instance_identifier=instance_identifier,
        )

    def test_validate_state_no_state_data_in_cache(self):
        state: str = "state_1234"
        session_id = "session_id_1234"
        instance_identifier: str = "instance_id"
        mock_redis = MockRedis()
        with pytest.raises(StateValidationError):
            auth0._validate_state(
                redis=mock_redis,
                state=state,
                session_id=session_id,
                instance_identifier=instance_identifier,
            )

    def test_validate_state_state_data_not_valid(self):
        state: str = "state_1234"
        session_id = "session_id_1234"
        instance_identifier: str = "instance_id"
        cache_key: str = f"{instance_identifier}:auth0:state:{session_id}"

        mock_redis = MockRedis()
        mock_redis.set(cache_key, bytes("wrong_state", encoding="utf-8"))

        with pytest.raises(StateValidationError):
            auth0._validate_state(
                redis=mock_redis,
                state=state,
                session_id=session_id,
                instance_identifier=instance_identifier,
            )

    def test_find_user_by_token_email(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        test_user: User = User.create(
            id=1,
            uuid=uuid4(),
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=None,
        )

        mock_query_instance.find_user_by_name.return_value = test_user
        mock_command_instance.create_user.return_value = []

        user: User = auth0._find_user_by_token_email(
            token_email="email@example.com",
            query_instance=mock_query_instance,
            command_instance=mock_command_instance,
        )
        assert user is test_user
        assert user.active is True
        assert user.date_deleted is None

    def test_find_user_by_token_email_find_active_interface_exception(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        mock_query_instance.find_active_interface.side_effect = (
            FindActiveInterfaceError(name="authldap")
        )

        with pytest.raises(FindActiveInterfaceError):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    def test_find_user_by_token_email_find_active_interface_warning(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_query_instance.find_active_interface.side_effect = (
            InterfaceNotActiveWarning(name="authldap")
        )

        with pytest.raises(InterfaceNotActiveWarning):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    def test_find_user_by_token_email_find_user_exception(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        mock_query_instance.find_user_by_name.side_effect = (
            FindUserByNameError(name="name")
        )

        with pytest.raises(FindUserByNameError):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    def test_find_user_by_token_email_user_inactive(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        test_user: User = User.create(
            id=1,
            uuid=uuid4(),
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=False,
            date_deleted=None,
        )

        mock_query_instance.find_user_by_name.return_value = test_user
        mock_command_instance.create_user.return_value = []

        with pytest.raises(UserNotActiveWarning):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    def test_find_user_by_token_email_user_marked_as_deleted(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        test_user: User = User.create(
            id=1,
            uuid=uuid4(),
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=datetime.now(),
        )

        mock_query_instance.find_user_by_name.return_value = test_user
        mock_command_instance.create_user.return_value = []

        with pytest.raises(UserMarkedAsDeletedWarning):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    def test_find_user_by_token_email_create_user_exception(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        mock_query_instance.find_user_by_name.return_value = []
        mock_command_instance.create_user.side_effect = CreateUserByNameError(
            "name"
        )

        with pytest.raises(CreateUserByNameError):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    def test_find_user_by_token_email_no_user_found_and_user_created(self):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        test_user: User = User.create(
            id=1,
            uuid=uuid4(),
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=datetime.now(),
        )

        mock_query_instance.find_user_by_name.side_effect = [None, test_user]
        mock_command_instance.create_user.return_value = []

        user: User | None = auth0._find_user_by_token_email(
            token_email="email@example.com",
            query_instance=mock_query_instance,
            command_instance=mock_command_instance,
        )
        assert user is user

    def test_find_user_by_token_email_no_user_found_after_user_is_created(
        self,
    ):
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()

        mock_query_instance.find_user_by_name.side_effect = [None, None]
        mock_command_instance.create_user.return_value = []

        with pytest.raises(
            NewUserNotFoundError,
            match="Cannot find new created user with name 'email@example.com'.",
        ):
            auth0._find_user_by_token_email(
                token_email="email@example.com",
                query_instance=mock_query_instance,
                command_instance=mock_command_instance,
            )

    @mock.patch("zsnl_auth_http.views.auth0._get_client_ip")
    @mock.patch("zsnl_auth_http.views.auth0._create_redis_session_for_user")
    @mock.patch("zsnl_auth_http.views.auth0.requests")
    def test_create_redis_session_add_login_attempt(
        self,
        mock_requests,
        mock_create_redis_session_for_user,
        mock_get_client_ip,
    ):
        mock_create_redis_session_for_user.return_value = None
        mock_get_client_ip.return_value = None

        mock_redis = MockRedis()
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_subject_login_history.return_value = None

        login_success: bool = auth0._create_redis_session_add_login_attempt(
            request=mock_requests,
            redis=mock_redis,
            user=mock.MagicMock(),
            query_instance=mock_query_instance,
            command_instance=mock_command_instance,
        )
        assert login_success is True

    @mock.patch("zsnl_auth_http.views.auth0._get_client_ip")
    @mock.patch("zsnl_auth_http.views.auth0._create_redis_session_for_user")
    @mock.patch("zsnl_auth_http.views.auth0.requests")
    def test_create_redis_session_add_login_attempt_create_session_fails(
        self,
        mock_requests,
        mock_create_redis_session_for_user,
        mock_get_client_ip,
    ):
        mock_create_redis_session_for_user.side_effect = Exception()
        mock_get_client_ip.return_value = None

        mock_redis = MockRedis()
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_subject_login_history.return_value = None

        login_success: bool = auth0._create_redis_session_add_login_attempt(
            request=mock_requests,
            redis=mock_redis,
            user=mock.MagicMock(),
            query_instance=mock_query_instance,
            command_instance=mock_command_instance,
        )
        assert login_success is False

    @mock.patch("zsnl_auth_http.views.auth0._get_client_ip")
    @mock.patch("zsnl_auth_http.views.auth0._create_redis_session_for_user")
    @mock.patch("zsnl_auth_http.views.auth0.requests")
    def test_create_redis_session_add_login_attempt_get_client_ip_fails(
        self,
        mock_requests,
        mock_create_redis_session_for_user,
        mock_get_client_ip,
    ):
        mock_create_redis_session_for_user.return_value = None
        mock_get_client_ip.side_effect = Exception()

        mock_redis = MockRedis()
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_subject_login_history.return_value = None

        login_success: bool = auth0._create_redis_session_add_login_attempt(
            request=mock_requests,
            redis=mock_redis,
            user=mock.MagicMock(),
            query_instance=mock_query_instance,
            command_instance=mock_command_instance,
        )
        assert login_success is False

    @mock.patch("zsnl_auth_http.views.auth0._get_client_ip")
    @mock.patch("zsnl_auth_http.views.auth0._create_redis_session_for_user")
    @mock.patch("zsnl_auth_http.views.auth0.requests")
    def test_create_redis_session_add_login_attempt_create_login_history_fails(
        self,
        mock_requests,
        mock_create_redis_session_for_user,
        mock_get_client_ip,
    ):
        mock_create_redis_session_for_user.return_value = None
        mock_get_client_ip.return_value = None

        mock_redis = MockRedis()
        mock_query_instance = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_subject_login_history.side_effect = (
            Exception()
        )

        login_success: bool = auth0._create_redis_session_add_login_attempt(
            request=mock_requests,
            redis=mock_redis,
            user=mock.MagicMock(),
            query_instance=mock_query_instance,
            command_instance=mock_command_instance,
        )
        assert login_success is False

    def test_create_redis_session_for_user(self):
        session_id: str = "31337"
        cache_key_session: str = f"yaml:session:{session_id}"

        mock_redis = MockRedis()
        session_data: bytes = b"---\n__created: 1724756533\n__expire_keys: {}\n__session_host: dev.zaaksysteem.nl\n__updated: 1724756534\nrequest_id: dev-a5c4fe-fafd67\nlogin_failed: 2\n"
        mock_redis.set(
            cache_key_session, codecs.encode(session_data, "base64")
        )

        uuid = uuid4()

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = []

        user: User = User.create(
            id=1,
            uuid=uuid,
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=None,
        )

        auth0._create_redis_session_for_user(
            redis=mock_redis,
            session_id=session_id,
            user=user,
            query_instance=mock_query_instance,
        )

        reddis_data_raw = mock_redis.get(cache_key_session)
        reddis_data_decoded = codecs.decode(reddis_data_raw, "base64")
        data: dict = yaml.load(reddis_data_decoded, Loader=yaml.SafeLoader)

        assert len(data) == 8
        assert data.get("__user_realm") == "zaaksysteem"
        assert data.get("auth0_login") is True
        assert data.get("__created") == 1724756533
        assert data.get("__expire_keys") == {}
        assert data.get("__session_host") == "dev.zaaksysteem.nl"
        assert data.get("__updated") == 1724756534
        assert data.get("request_id") == "dev-a5c4fe-fafd67"

        data_user: dict = json.loads(data.get("__user"))
        assert len(data_user) == 8
        assert data_user.get("subject_uuid") == str(uuid)
        assert data_user.get("external_api_objects") == []
        assert data_user.get("display_name") == "email@example.com"
        assert data_user.get("is_external_api") == 0
        assert data_user.get("login_entity_id") == "1"
        assert data_user.get("id") == 1
        assert data_user.get("uuid") == str(uuid)
        assert data_user.get("permissions") == {}

    def test_create_redis_session_for_user_no_session_cache(self):
        session_id: str = "31337"
        cache_key_session: str = f"yaml:session:{session_id}"

        mock_redis = MockRedis()

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = []

        uuid = uuid4()

        user: User = User.create(
            id=1,
            uuid=uuid,
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=None,
        )

        auth0._create_redis_session_for_user(
            redis=mock_redis,
            session_id=session_id,
            user=user,
            query_instance=mock_query_instance,
        )

        reddis_data_raw = mock_redis.get(cache_key_session)
        reddis_data_decoded = codecs.decode(reddis_data_raw, "base64")
        data: dict = yaml.load(reddis_data_decoded, Loader=yaml.SafeLoader)

        assert len(data) == 3
        assert data.get("__user_realm") == "zaaksysteem"
        assert data.get("auth0_login") is True

        data_user: dict = json.loads(data.get("__user"))
        assert len(data_user) == 8
        assert data_user.get("subject_uuid") == str(uuid)
        assert data_user.get("external_api_objects") == []
        assert data_user.get("display_name") == "email@example.com"
        assert data_user.get("is_external_api") == 0
        assert data_user.get("login_entity_id") == "1"
        assert data_user.get("id") == 1
        assert data_user.get("uuid") == str(uuid)
        assert data_user.get("permissions") == {}

    def test_create_redis_session_for_user_session_cache_none(self):
        session_id: str = "31337"
        cache_key_session: str = f"yaml:session:{session_id}"

        mock_redis = MockRedis()
        mock_redis.set(cache_key_session, None)

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = []

        uuid = uuid4()

        user: User = User.create(
            id=1,
            uuid=uuid,
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=None,
        )

        auth0._create_redis_session_for_user(
            redis=mock_redis,
            session_id=session_id,
            user=user,
            query_instance=mock_query_instance,
        )

        reddis_data_raw = mock_redis.get(cache_key_session)
        reddis_data_decoded = codecs.decode(reddis_data_raw, "base64")
        data: dict = yaml.load(reddis_data_decoded, Loader=yaml.SafeLoader)

        assert len(data) == 3
        assert data.get("__user_realm") == "zaaksysteem"
        assert data.get("auth0_login") is True

        data_user: dict = json.loads(data.get("__user"))
        assert len(data_user) == 8
        assert data_user.get("subject_uuid") == str(uuid)
        assert data_user.get("external_api_objects") == []
        assert data_user.get("display_name") == "email@example.com"
        assert data_user.get("is_external_api") == 0
        assert data_user.get("login_entity_id") == "1"
        assert data_user.get("id") == 1
        assert data_user.get("uuid") == str(uuid)
        assert data_user.get("permissions") == {}

    def test_create_redis_session_for_user_session_cache_empty(self):
        session_id: str = "31337"
        cache_key_session: str = f"yaml:session:{session_id}"

        mock_redis = MockRedis()
        mock_redis.set(cache_key_session, codecs.encode(b'""', "base64"))

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = []

        uuid = uuid4()

        user: User = User.create(
            id=1,
            uuid=uuid,
            name="email@example.com",
            login_entity_id=1,
            role_ids=[],
            active=True,
            date_deleted=None,
        )

        auth0._create_redis_session_for_user(
            redis=mock_redis,
            session_id=session_id,
            user=user,
            query_instance=mock_query_instance,
        )

        reddis_data_raw = mock_redis.get(cache_key_session)
        reddis_data_decoded = codecs.decode(reddis_data_raw, "base64")
        data: dict = yaml.load(reddis_data_decoded, Loader=yaml.SafeLoader)

        assert len(data) == 3
        assert data.get("__user_realm") == "zaaksysteem"
        assert data.get("auth0_login") is True

        data_user: dict = json.loads(data.get("__user"))
        assert len(data_user) == 8
        assert data_user.get("subject_uuid") == str(uuid)
        assert data_user.get("external_api_objects") == []
        assert data_user.get("display_name") == "email@example.com"
        assert data_user.get("is_external_api") == 0
        assert data_user.get("login_entity_id") == "1"
        assert data_user.get("id") == 1
        assert data_user.get("uuid") == str(uuid)
        assert data_user.get("permissions") == {}

    def test_set_permissions(self):
        uuid = uuid4()

        user_data_permissions: dict = {}
        user_data: dict = {
            "uuid": str(uuid),
            "permissions": user_data_permissions,
        }

        role1: Role = Role.create(
            permissions=["permission_1", "permission_2", "permission_3"],
        )
        roles: list[Role] = []
        roles.append(role1)

        role_ids: list[str] = [1]

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = roles

        auth0._set_permissions(
            user_data=user_data,
            role_ids=role_ids,
            query_instance=mock_query_instance,
        )

        assert len(user_data) == 2
        assert user_data.get("uuid") == str(uuid)

        permissions: dict = user_data.get("permissions")
        assert len(permissions) == 3
        assert permissions.get("permission_1") is True
        assert permissions.get("permission_2") is True
        assert permissions.get("permission_3") is True

    def test_set_permissions_no_role_permissions(self):
        uuid = uuid4()

        user_data_permissions: dict = {}
        user_data: dict = {
            "uuid": str(uuid),
            "permissions": user_data_permissions,
        }

        role1: Role = Role.create(
            permissions=None,
        )
        roles: list[Role] = []
        roles.append(role1)

        role_ids: list[str] = [1]

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = roles

        auth0._set_permissions(
            user_data=user_data,
            role_ids=role_ids,
            query_instance=mock_query_instance,
        )

        assert len(user_data) == 2
        assert user_data.get("uuid") == str(uuid)

        permissions: dict = user_data.get("permissions")
        assert len(permissions) == 0

    def test_set_permissions_no_roles(self):
        uuid = uuid4()

        user_data_permissions: dict = {}
        user_data: dict = {
            "uuid": str(uuid),
            "permissions": user_data_permissions,
        }

        role_ids: list[str] = []

        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_permissions_for_roles.return_value = []

        auth0._set_permissions(
            user_data=user_data,
            role_ids=role_ids,
            query_instance=mock_query_instance,
        )

        assert len(user_data) == 2
        assert user_data.get("uuid") == str(uuid)

        permissions: dict = user_data.get("permissions")
        assert len(permissions) == 0
