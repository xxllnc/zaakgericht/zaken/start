# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.0.1"

import logging
import minty
import minty_infra_sqlalchemy
import minty_pyramid
import os
from .routes import routes
from minty.middleware import AmqpPublisherMiddleware
from zsnl_domains import auth
from zsnl_pyramid import platform_key

ZS_COMPONENT = "zsnl_auth_http"

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    return record


def main(*args, **kwargs):
    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    loader = minty_pyramid.Engine(
        domains=[auth],
        query_middleware=[
            minty_infra_sqlalchemy.DatabaseTransactionQueryMiddleware(
                "database"
            )
        ],
        command_wrapper_middleware=[
            minty_infra_sqlalchemy.DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="auth", infrastructure_name="amqp"
            ),
        ],
    )
    config = loader.setup(*args, **kwargs)

    # Enable platform key access to this service
    config.include(platform_key)

    routes.add_routes(config)
    return loader.main()
