# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import codecs
import jose.exceptions
import json
import logging
import os
import requests
import yaml
from minty.entity import EntityCollection
from pyramid.request import Request
from pyramid.response import Response
from redis import Redis
from requests.exceptions import HTTPError
from typing import Final, cast
from uuid import uuid4
from zsnl_domains.auth.entities import Role, TokenResponse, User
from zsnl_domains.auth.exceptions import (
    Auth0Error,
    Auth0Warning,
    InterfaceNotActiveWarning,
    NewUserNotFoundError,
    UserMarkedAsDeletedWarning,
    UserNotActiveWarning,
)

logger = logging.getLogger(__name__)

APPLICATION_JSON: Final = "application/json"
AUTHORIZATION_CODE: Final = "authorization_code"
INSTANCE_HOSTNAME: Final = "instance_hostname"
INSTANCE_UUID: Final = "instance_uuid"
OIDC: Final = "oidc"
OIDC_CLIENT_ID: Final = "client_id"
OIDC_CLIENT_SECRET: Final = "client_secret"
OIDC_DOMAIN: Final = "domain"
OIDC_ORGANIZATION_ID: Final = "oidc_organization_id"

AUTH0_OPENID_PROVIDER_METADATA_CACHE_SECONDS: Final = int(
    os.environ.get("AUTH0_OPENID_PROVIDER_METADATA_CACHE_SECONDS", "3600")
)  # 1 hour cache lifetime
AUTH0_JWKS_CACHE_SECONDS: Final = int(
    os.environ.get("AUTH0_JWKS_CACHE_SECONDS", "600")
)  # 10 minutes cache lifetime
AUTH0_USER_SESSION_CACHE_SECONDS: Final = int(
    os.environ.get(
        "AUTH0_USER_SESSION_CACHE_SECONDS", "60"
    )  # 1 minute cache lifetime
)

REDIRECT_LOGIN_FAILURE: Final = "/auth/logout?auth0l=1"
REDIRECT_LOGIN_SUCCESS: Final = "/auth/page/login_success"

HTTP_X_REAL_IP: Final = "X-Real-IP"
HTTP_X_FORWARDED_FOR: Final = "X-Forwarded-For"


class RequestParameterError(Exception):
    pass


class StateValidationError(Exception):
    pass


class ConfigError(Exception):
    pass


class TokenValidationError(Exception):
    pass


def _get_auth0_authorize_response_parameters(
    request: Request,
) -> tuple[str, str]:
    """Get the Auth0 authorize response parameters 'state' and 'code'.

    Parameter 'state' is an opaque arbitrary alphanumeric string that our
    application adds to the initial request that Auth0 includes when
    redirecting back to our application. This to prevent cross-site request
    forgery (CSRF) attacks.

    Parameter 'code' contains the Authorization Code we must exchange for
    a token.

    Args:
        request: The request

    Returns:
        state (str): the state
        code (str): the authorication code

    Raises:
        RequestParameterError: If required parameter is not found
                               or not specified.
    """

    logger.debug("Get and check Auth0 authorize response parameters")

    state: str | None = None
    code: str | None = None

    try:
        state = request.params["state"]
    except KeyError as ke:
        raise RequestParameterError(
            "Required parameter not found: state"
        ) from ke

    try:
        code = request.params["code"]
    except KeyError as ke:
        raise RequestParameterError(
            "Required parameter not found: code"
        ) from ke

    if not state:
        raise RequestParameterError("Required parameter not specified: state")

    if not code:
        raise RequestParameterError("Required parameter not specified: code")

    logger.debug(f"Auth0 state = {state}, code = {code}")
    return state, code


def _get_configuration(request: Request) -> dict:
    """Obtains the configuration from the request and check if it is valid.
    The configuration is required to exchange an Authorization Code for a
    token.

    Args:
        request: The request

    Returns:
        configuration (dict): the configuration

    Raises:
        ConfigError: If required configuration is missing.
    """

    logger.debug("Get and check configuration")

    configuration: dict | None = request.infrastructure_factory.get_config(
        request.host
    )

    if not configuration:
        raise ConfigError("Required configuration not found.")

    oidc_config: dict | None = configuration.get(OIDC, None)
    if not oidc_config:
        raise ConfigError(
            f"Invalid configuration, OIDC value not found; expected {OIDC}"
        )

    required_oidc_configuration = {
        OIDC_CLIENT_ID,
        OIDC_CLIENT_SECRET,
        OIDC_DOMAIN,
    }
    if not required_oidc_configuration.issubset(oidc_config.keys()):
        raise ConfigError("Invalid OIDC configuration.")

    for key in [OIDC_CLIENT_ID, OIDC_CLIENT_SECRET, OIDC_DOMAIN]:
        if not oidc_config.get(key, None):
            raise ConfigError(
                f"Invalid OIDC configuration, value not found; expected {key}"
            )

    for key in [OIDC_ORGANIZATION_ID, INSTANCE_UUID]:
        if not configuration.get(key, None):
            raise ConfigError(
                f"Invalid configuration, value not found; expected {key}"
            )

    logger.debug(f"Auth0 configuration = {configuration}")
    return configuration


def _validate_state(
    redis: Redis, state: str, session_id: str, instance_identifier: str
):
    """Our application adds a 'state' to the initial request and stores it in
    the redis cache. Auth0 includes a 'state' when redirecting back to our
    application. Here we validate whether the 'state' values are the same to
    prevent cross-site request forgery (CSRF) attacks.

    Args:
        redis: redis
        state: the state
        session_id: the session id
        instance_identifier: the instance identifier

    Raises:
        StateValidationError: If state is invalid.
    """

    logger.debug(
        f"Validate state, state = {state}, session_id = "
        f"{session_id}, instance_identifier = {instance_identifier}"
    )

    cache_key: str = f"{instance_identifier}:auth0:state:{session_id}"

    try:
        state_data: str | None = cast(
            str | None, redis.getdel(cache_key).decode("utf-8")
        )
    except (TypeError, AttributeError):
        logger.warning(f"No state date found in cache for key {cache_key}")
        state_data = None

    if not state_data:
        raise StateValidationError(
            f"Invalid state, state value not found; expected {state}"
        )

    if state != state_data:
        raise StateValidationError(
            f"Invalid state, state value mismatch; expected {state}, "
            f"found {state_data}"
        )

    logger.debug("Auth0 state is valid.")


def _retrieve_json_cached(
    url: str,
    cache: Redis,
    cache_key: str,
    cache_duration_seconds: str,
) -> dict:
    """
    Retrieve the data from the cache. Return this data if available. If the
    data is not available in the cache, make a http get call to retrieve the
    data. This data is stored in the cache with a time to live.

    Args:
        url (str): The url where the data is located.
        cache (Redis): The cache
        cache_key (str): The key where the data is stored in the cache
        cache_duration_seconds (str): The lifetime of the set cache in seconds.

    Returns:
        the cached data

    Raises:
        HTTPError: Failed to retrieve data from url
    """

    logger.debug(
        f"Retrieve and set cache data, url = {url}, cache_key = {cache_key} "
        f"and cache_duration_seconds = {cache_duration_seconds}"
    )

    try:
        return json.loads(cache[cache_key])
    except (KeyError, json.decoder.JSONDecodeError):
        logger.debug(f"No data found in cache for key {cache_key}")
        pass

    response: Response = requests.get(url=url, verify=True, timeout=30)

    if response.status_code != 200:
        error_msg: str = f"Failed to retrieve data from {url}. HTTP status is"
        f" {response.status_code}, excepted 200 (OK)."
        raise HTTPError(error_msg)

    json_data = response.json()

    cache.set(cache_key, json.dumps(json_data), ex=cache_duration_seconds)

    return json_data


def _retrieve_decoded_id_token(
    id_token: str,
    client_id: str,
    domain: str,
    instance_identifier: str,
    cache: Redis,
) -> dict:
    """Determine if the ID Token is valid and return the decoded
    token when it is valid.

    The JSON Web Token (JWT) token signature is generated using a Signing
    Algorithm. Before we can use the JWT we validate the JTW using its
    signature. To verify the JWT we need the JSON Web Key Set (JWKS), that
    containts a set of keys with the public keys. To locate the JWKS endpoint
    (jwks_uri), the Discovery endpoint is used. With this endpoint we can
    find configuration information for the specified OpenID Connect provider
    (the OpenID Provider Metadata).

    Raises:
        RequestException: failed to retrieve data from OpenID Connect Discovery
        Exception: when token header is invalid
        KeyError: when the jwt kid value, which indicates what key was used to
                  sign the JWT, is not set
        ExpiredSignatureError: If the signature has expired.
        JWTClaimsError: If any claim is invalid in any way.
        JWTError: If the signature is invalid in any way.
    """

    logger.debug(
        f"Retrieve decoded id token, id_token {id_token}, client_id "
        f"{client_id}, domain = {domain}, instance_identifier = "
        f"{instance_identifier}"
    )

    cache_key: str = f"oidc_cache:{instance_identifier}"

    oidc_discovery_endpoint_url: str = (
        f"https://{domain}/.well-known/openid-configuration"
    )

    openid_provider_metadata: dict = _retrieve_json_cached(
        url=oidc_discovery_endpoint_url,
        cache=cache,
        cache_key=f"{cache_key}:metadata",
        cache_duration_seconds=AUTH0_OPENID_PROVIDER_METADATA_CACHE_SECONDS,
    )

    jwks: dict = _retrieve_json_cached(
        url=openid_provider_metadata["jwks_uri"],
        cache=cache,
        cache_key=f"{cache_key}:jwks",
        cache_duration_seconds=AUTH0_JWKS_CACHE_SECONDS,
    )

    unverified_header_data: dict = jose.jwt.get_unverified_header(id_token)

    rsa_key = {}

    try:
        for key in jwks["keys"]:
            if key["kid"] == unverified_header_data["kid"]:
                rsa_key = key
    except KeyError as ke:
        logger.error(f"Invalid ID token header: {ke}", exc_info=True)
        raise ke

    if not rsa_key:
        logger.error("Invalid ID token header")
        raise Exception("Invalid ID token header")

    issuer: str = f"https://{domain}/"

    try:
        payload = jose.jwt.decode(
            token=id_token,
            key=rsa_key,
            algorithms=unverified_header_data.get("alg", None),
            audience=client_id,
            issuer=issuer,
        )
    except jose.jwt.ExpiredSignatureError as ese:
        logger.error(
            f"Unable to decode the ID token, token is expired: {ese}",
            exc_info=True,
        )
        raise ese
    except jose.jwt.JWTClaimsError as jce:
        logger.error(
            f"Unable to decode the ID token, claim is invalid: {jce}",
            exc_info=True,
        )
        raise jce
    except jose.jwt.JWTError as je:
        logger.error(
            f"Unable to decode the ID token: {je}",
            exc_info=True,
        )
        raise je

    logger.debug(f"The decoded ID token {payload}")
    return payload


def _retrieve_email_from_token(
    payload: dict,
    client_id: str,
    domain: str,
    organization_id: str,
) -> str:
    """An ID token is encoded as a JSON Web Token (JWT), a standard format
    that allows us to easily inspect its content. So inspect the content of
    the ID Token to make sure it is valid. If valid return the email claim.
    """

    logger.debug(
        f"Retrieve email from token, payload is {payload}, "
        f"client_id = {client_id}, domain = {domain}, "
        f"organization_id = {organization_id}"
    )

    # Issuer
    if "iss" not in payload or not isinstance(payload["iss"], str):
        logger.error(
            "Issuer (iss) claim must be a string present in the ID token"
        )
        raise TokenValidationError(
            "Issuer (iss) claim must be a string present in the ID token"
        )

    issuer: str = f"https://{domain}/"
    token_issuer: str = payload["iss"]

    if token_issuer != issuer:
        logger.error(
            "Invalid token, Issuer (iss) claim mismatch in the ID token;"
            " expected {}, found {}".format(issuer, token_issuer)
        )
        raise TokenValidationError(
            "Invalid token, Issuer (iss) claim mismatch in the ID token;"
            " expected {}, found {}".format(issuer, token_issuer)
        )

    # Audience
    if "aud" not in payload or not isinstance(payload["aud"], str):
        logger.error(
            "Audience (aud) claim must be a string present in the ID token"
        )
        raise TokenValidationError(
            "Audience (aud) claim must be a string present in the ID token"
        )

    token_audience: str = payload["aud"]
    if token_audience != client_id:
        logger.error(
            "Invalid token, Audience (aud) claim mismatch in the ID token;"
            " expected {}, found {}".format(client_id, token_audience)
        )
        raise TokenValidationError(
            "Invalid token, Audience (aud) claim mismatch in the ID token;"
            " expected {}, found {}".format(client_id, token_audience)
        )

    # Organization
    if "org_id" not in payload or not isinstance(payload["org_id"], str):
        logger.error(
            "Organization (org_id) claim must be a string present in the"
            "ID token"
        )
        raise TokenValidationError(
            "Organization (org_id) claim must be a string present in the"
            "ID token"
        )

    token_org_id: str = payload["org_id"]
    if token_org_id != organization_id:
        logger.error(
            "Invalid token, Organization (org_id) claim mismatch in the ID"
            " token; expected {},"
            " found {}".format(organization_id, token_org_id)
        )
        raise TokenValidationError(
            "Invalid token, Organization (org_id) claim mismatch in the ID"
            " token; expected {},"
            "found {}".format(organization_id, token_org_id)
        )

    # Email
    if "email" not in payload or not isinstance(payload["email"], str):
        logger.error(
            "Email (email) claim must be a string present in the ID token"
        )
        raise TokenValidationError(
            "Email (email) claim must be a string present in the ID token"
        )

    logger.debug(f"Found email {payload['email']}")
    return payload["email"]


def _find_user_by_token_email(
    token_email: str, query_instance, command_instance
) -> User:
    """Search for an user whose name matches the token email.
    If the user is found the user is returned. If the user is not found a
    new user is created.

    Args:
        token_email (str): The token email

    Returns:
        User if user is found or the new created user.

    Raises:
        UserNotActiveWarning: If the found user is not active.
        UserMarkedAsDeletedWarning: If the found user is marked as deleted.
        NewUserNotFoundError: If the new user cannot be found.
    """

    logger.debug(f"Find user by token email {token_email}")

    event_params: dict = {
        "username": token_email,
        "interface_name": "authldap",
    }

    # We need an active interface. Interface also should not be marked
    # as deleted.
    try:
        query_instance.find_active_interface()
    except InterfaceNotActiveWarning as inaw:
        command_instance.add_event_auth0_interface_not_active(**event_params)
        raise inaw

    user: User | None = query_instance.find_user_by_name(name=token_email)

    if user and not user.active:
        command_instance.add_event_auth0_user_not_active(**event_params)
        raise UserNotActiveWarning(name=token_email)

    if user and user.date_deleted:
        command_instance.add_event_auth0_user_marked_as_deleted(**event_params)
        raise UserMarkedAsDeletedWarning(name=token_email)

    if user:
        logger.debug(f"User found {user}")
        return user

    logger.debug(f"User not found, creating new user for {token_email}")

    command_instance.create_user(username=token_email)

    user = query_instance.find_user_by_name(name=token_email)

    if not user:
        raise NewUserNotFoundError(name=token_email)

    logger.debug(f"New user created {user}")
    return user


def _create_redis_session_for_user(
    redis: Redis,
    session_id: str,
    user: User,
    query_instance,
) -> str:
    """Create redis session for the user."""

    logger.debug(
        f"Create redis session for user, session_id = {session_id}"
        f", user = {user}"
    )

    cache_key_session: str = f"yaml:session:{session_id}"

    session_data_raw: str | None = None
    try:
        session_data_raw = cast(
            str | None, redis.get(cache_key_session).decode("utf-8")
        )
    except (TypeError, AttributeError) as e:
        logger.warning(
            f"Unable to read session data for key {cache_key_session}: {e}",
            exc_info=True,
        )
        session_data_raw = None

    logger.debug(f"session_data_raw {session_data_raw}")

    data: dict = {}

    if session_data_raw:
        session_data = base64.b64decode(session_data_raw)
        data: dict = yaml.load(session_data, Loader=yaml.SafeLoader)
        if not data:
            data = {}

    logger.debug(f"data is {data}")

    user_data: dict = {
        "subject_uuid": str(user.uuid),
        "external_api_objects": [],
        "display_name": user.name,
        "is_external_api": 0,
        "login_entity_id": user.login_entity_id,
        "id": user.id,
        "uuid": str(user.uuid),
        "permissions": dict(),
    }

    _set_permissions(user_data, user.role_ids, query_instance)

    user_data_json_dump = json.dumps(user_data)

    user: dict = {
        "__user": user_data_json_dump,
        "__user_realm": "zaaksysteem",
        "auth0_login": True,
    }

    data.update(user)

    # delete session attributes
    data.pop("login_failed", None)
    data.pop("login_auth0_failed", None)

    logger.debug(f"data to store {data}")

    data_yaml = yaml.dump(data)
    data_yaml_base64 = codecs.encode(data_yaml.encode("utf-8"), "base64")

    logger.debug(
        f"Store data under key {cache_key_session}, data = {data_yaml_base64}"
        f" and ex = {AUTH0_USER_SESSION_CACHE_SECONDS}"
    )

    redis.set(
        name=cache_key_session,
        value=data_yaml_base64,
        ex=AUTH0_USER_SESSION_CACHE_SECONDS,
    )


def _set_permissions(
    user_data: dict, role_ids: EntityCollection, query_instance
):
    """Set the permissions for the user."""

    logger.debug(
        f"set_permissions user_data = {user_data}, role_ids {role_ids}"
    )

    roles: Role | None = query_instance.get_permissions_for_roles(
        role_ids=role_ids,
    )

    for role in roles:
        logger.debug(f"role = {role}, role permissions = {role.permissions}")
        if role.permissions:
            for permission in role.permissions:
                user_data["permissions"][permission] = True

    logger.debug(f"Updated user_data = {user_data}")


def _get_client_ip(request: Request) -> str:
    """Get the client IP from the request, we look for the header 'X-Real-IP'
       or 'X-Forwarded-For' or fall back to the actual IP.

    Args:
        request: The request

    Returns:
        Client IP address.
    """

    logger.debug("Get the client IP from the request.")

    http_x_real_ip: str = request.headers.get(HTTP_X_REAL_IP, "")
    if http_x_real_ip:
        return http_x_real_ip

    http_x_forwarded_for: str = request.headers.get(HTTP_X_FORWARDED_FOR, "")
    if http_x_forwarded_for:
        # Strip everything after the first comma.
        return http_x_forwarded_for.split(",", 1)[0]

    return request.client_addr


def _get_code_and_configuration(request: Request) -> tuple[str, dict]:
    """Check that the Auth0 authorize response parameters are set.
    To exchange an authorization code for a token we need configuration.
    This configuration is obtained from the request and checks be run to
    determine if it is valid. If the configuration is valid, we validate
    that the 'state' values are the same on the initial request to prevent
    cross-site request forgery (CSRF) attacks. If validation is successful,
    the authorization code and configuration are returned.

    Args:
        request: The request

    Returns:
        code (str): the authorication code
        configuration (dict): the configuration
    """

    logger.debug("Get code and configuration.")

    # First check that the Auth0 authorize response parameters are set.
    state: str | None = None
    code: str | None = None
    state, code = _get_auth0_authorize_response_parameters(request=request)

    # Specific configuration is required to exchange an authorization code
    # for a token.
    configuration: dict = _get_configuration(request=request)

    # Validate the state stored in the cache against the state of the
    # Auth0 authorize response parameters.
    session_id: str = request.session_id
    instance_identifier: str = configuration.get(INSTANCE_UUID)
    redis_infra: Redis = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )

    _validate_state(
        redis=redis_infra,
        state=state,
        session_id=session_id,
        instance_identifier=instance_identifier,
    )

    # Validation is successful, return authorization code and configuration
    return code, configuration


def _create_redis_session_add_login_attempt(
    request: Request,
    redis: Redis,
    user: User,
    query_instance,
    command_instance,
) -> bool:
    """Create redis session for the user and add login attempt."""

    logger.debug(
        f"Create redis session for user and add login attempt, session_id = "
        f"{request.session_id}, user = {user}"
    )

    login_success: bool = False
    session_id: str = request.session_id

    try:
        _create_redis_session_for_user(
            redis=redis,
            session_id=session_id,
            user=user,
            query_instance=query_instance,
        )

        # Now we have a successful login.
        login_success = True
    except Exception as e:
        logger.error(
            f"Error, cannot create/update redis session for the user {user} : "
            f"{e}",
            exc_info=True,
        )

    # Store (un)successful login

    try:
        client_ip: str = _get_client_ip(request=request)
        command_instance.create_subject_login_history(
            ip=client_ip,
            subject_username=user.name,
            subject_id=user.id,
            subject_uuid=user.uuid,
            success=login_success,
        )
    except Exception as e:
        logger.error(
            f"Error, cannot update subject login history for user {user} : "
            f"{e}",
            exc_info=True,
        )
        return False

    # Login is (un)successful
    return login_success


def callback(request: Request):
    """Logic to exchange an Auth0 authorization code for the ID Token. First
    check that the Auth0 authorize response parameters are set. To exchange an
    authorization code for a token we need configuration. This configuration
    is obtained from the request and checks be run to determine if it is valid.
    If the configuration is valid, we validate that the 'state' values are the
    same on the initial request to prevent cross-site request forgery (CSRF)
    attacks. The next step is to exchange to auhorization code for tokens.
    Currently we only use the ID token. An ID token proves that the user has
    been authenticated. The result of the authentication process based on
    OpenID Connect. An ID token is encoded as a JSON Web Token (JWT). We use
    this to determine if the ID Token is valid. If the token is valid, we
    retrieve the token's email claim. The content of the email claim is used
    to search for a user whose name matches the claim. If no user is found,
    a new user is created. We store information about this user in the
    session cache, for example the permissions for this user.The final step
    is to redirect the user back to the 'home' page of the application.
    On the first login, because a new user is created, the user will be
    redirected to the 'first login' page.

    If an error occurs during the process, the user will be returned to the
    login page and an error message will be displayed.
    """

    logger.info(
        f"Host {request.host}, processing Auth0 callback for containing "
        "authorization code"
    )

    # First check if the Auth0 authorize response parameters are set and
    # if the configuration is valid. If validation is successful the
    # authorization code and configuration are returned.
    code: str = ""
    configuration: dict = {}

    try:
        code, configuration = _get_code_and_configuration(request=request)
    except RequestParameterError as sve:
        logger.error(
            f"Error, validation Auth0 authorize response parameters failed : "
            f"{sve}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)
    except ConfigError as ce:
        logger.error(
            f"Error, Auth0 configuration is not valid : {ce}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)
    except StateValidationError as sve:
        logger.error(
            f"Error, validation Auth0 state failed : {sve}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)

    # Now that we have an Authorization Code, we must exchange it for tokens.
    oidc: dict = configuration.get(OIDC)
    query_instance = request.get_query_instance(
        domain="zsnl_domains.auth",
        user_uuid=uuid4(),
        user_info=None,
    )
    try:
        tokens: TokenResponse = query_instance.get_token(
            code=code,
            hostname=request.host,
            client_id=oidc.get(OIDC_CLIENT_ID),
            client_secret=oidc.get(OIDC_CLIENT_SECRET),
            domain=oidc.get(OIDC_DOMAIN),
        )
    except Auth0Error as ae:
        logger.error(
            f"Error, cannot exchange code '{code}' for tokens : {ae}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)

    logger.debug(f"Tokens are {tokens}")

    # The ID token proves that the user has been authenticated. Lets verify
    # if the ID Token is valid.
    instance_identifier: str = configuration.get(INSTANCE_UUID)
    redis_infra: Redis = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )
    token_payload: dict = {}
    try:
        token_payload = _retrieve_decoded_id_token(
            id_token=tokens.id_token,
            client_id=oidc.get(OIDC_CLIENT_ID),
            domain=oidc.get(OIDC_DOMAIN),
            instance_identifier=instance_identifier,
            cache=redis_infra,
        )
    except Exception as ex:
        logger.error(
            f"Error, cannot decode the ID token : {ex}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)

    # Inspect the content of the ID Token to make sure it is valid. If valid
    # return the email claim.
    token_email: str = ""
    try:
        token_email = _retrieve_email_from_token(
            payload=token_payload,
            client_id=oidc.get(OIDC_CLIENT_ID),
            domain=oidc.get(OIDC_DOMAIN),
            organization_id=configuration.get(OIDC_ORGANIZATION_ID),
        )
    except TokenValidationError as tve:
        logger.error(
            f"Error, content of ID token is not valid : {tve}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)

    # Search for a user whose name matches the token email. If the user is not
    # found, create a new one.
    user: User | None = None
    command_instance = request.get_command_instance(
        domain="zsnl_domains.auth", user_uuid=uuid4(), user_info=None
    )
    try:
        user = _find_user_by_token_email(
            token_email=token_email,
            query_instance=query_instance,
            command_instance=command_instance,
        )
    except Auth0Error as ae:
        logger.error(
            f"Error, cannot find or create user by token email {token_email}"
            f" : {ae}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)
    except Auth0Warning as aw:
        logger.warning(
            f"Warning, cannot find or create user by token email {token_email}"
            f" : {aw}",
            exc_info=True,
        )
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)

    # So we have an user.
    # Create redis session with user data.
    # login_success: bool = False
    login_success: bool = _create_redis_session_add_login_attempt(
        request=request,
        redis=redis_infra,
        user=user,
        query_instance=query_instance,
        command_instance=command_instance,
    )
    if login_success:
        # Login successful.
        # If a new user is created, he or she will be redirected to the
        # 'first login' page. Otherwise the user will be redirected to the
        # 'home' page.
        logger.info("Processing of Auth0 callback was successful")
        return Response(status_int=302, location=REDIRECT_LOGIN_SUCCESS)
    else:
        logger.info("Processing of Auth0 callback was unsuccessful")
        return Response(status_int=302, location=REDIRECT_LOGIN_FAILURE)
