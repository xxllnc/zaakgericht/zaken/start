# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import importlib
import logging
import pytest
from collections import namedtuple
from datetime import datetime
from jsonpickle.pickler import Pickler
from pyramid.httpexceptions import HTTPBadRequest
from unittest import mock
from uuid import uuid4
from zsnl_case_management_http import log_record_factory
from zsnl_case_management_http.views.handlers import (
    CaseHandler,
    CasetypeHandler,
)
from zsnl_domains.case_management.entities import CaseRelation, CaseSummary
from zsnl_domains.case_management.entities.subject_relation import (
    SubjectRelationRelatedSubject,
)


def mock_protected_route(view):
    def view_wrapper(view):
        def request_wrapper(request):
            UserInfo = namedtuple("UserInfo", "user_uuid permissions")

            user_info = UserInfo(
                user_uuid="subject_uuid", permissions="permissions"
            )

            return view(request=request, user_info=user_info)

        return request_wrapper

    return view_wrapper


class TestCasetypeHandler:
    @mock.patch.object(CasetypeHandler, "flatten")
    def test_case_type_flatten(self, mock_case_type_flatten):
        handler = CasetypeHandler(None)
        mock_case_type_flatten.return_value = "test"

        result = handler.flatten("case_type", None)
        assert result == "test"


class TestCaseHandler:
    @mock.patch.object(CaseHandler, "flatten")
    def test_case_type_flatten(self, mock_case_flatten):
        handler = CaseHandler(None)
        mock_case_flatten.return_value = "test"

        result = handler.flatten("case_type", None)
        assert result == "test"


class TestCaseManagementViews:
    def setup_method(self):
        with mock.patch(
            "minty_pyramid.session_manager.protected_route",
            mock_protected_route,
        ):
            from zsnl_case_management_http.views import case_management

            importlib.reload(case_management)

            self.case_man = case_management

    @mock.patch.object(Pickler, "flatten")
    def test_get_case(self, mock_case_flatten):
        case_uuid = uuid4()
        mock_case_flatten.return_value = "test"
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance

        mock_request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.get_case(request=mock_request)

        mock_request.params = {"case_uuid": case_uuid}

        mock_instance.get_case_by_uuid = mock.MagicMock()

        return_value = self.case_man.get_case(request=mock_request)
        assert return_value == {"data": "test"}

        mock_request.get_query_instance.assert_called_with(
            "zsnl_domains.case_management", "subject_uuid"
        )
        mock_request.get_query_instance().get_case_by_uuid.assert_called_once()

    def test_set_registration_date(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
            "target_date": "2019-01-21",
        }
        return_value = self.case_man.set_registration_date(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        mock_request.get_command_instance().set_case_registration_date.assert_called_once()

    def test_set_registration_date_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba"
        }
        with pytest.raises(HTTPBadRequest):
            self.case_man.set_registration_date(request=mock_request)

    def test_set_target_completion_date(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "target_date": "2019-01-21",
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
        }
        return_value = self.case_man.set_target_completion_date(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
            "target_date": "2019-01-21",
        }
        mock_request.get_command_instance().set_case_target_completion_date.assert_called_once_with(
            **params
        )

    def test_set_target_completion_date_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {"": None}
        with pytest.raises(HTTPBadRequest):
            self.case_man.set_target_completion_date(request=mock_request)

    def test_set_completion_date(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "target_date": "2019-01-21",
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
        }
        return_value = self.case_man.set_completion_date(request=mock_request)
        assert return_value == {"data": {"success": True}}
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
            "target_date": "2019-01-21",
        }
        mock_request.get_command_instance().set_case_completion_date.assert_called_once_with(
            **params
        )

    def test_set_completion_date_no_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba"
        }
        with pytest.raises(HTTPBadRequest):
            self.case_man.set_completion_date(request=mock_request)

    def test_assign_case_to_self(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {"case_uuid": str(uuid4())}
        return_value = self.case_man.assign_case_to_self(request=mock_request)
        assert return_value == {"data": {"success": True}}
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {"case_uuid": mock_request.json_body["case_uuid"]}
        mock_request.get_command_instance().assign_case_to_self.assert_called_once_with(
            **params
        )

    def test_assign_case_to_self_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.assign_case_to_self(request=mock_request)

    def test_change_case_coordinator(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "coordinator_uuid": str(uuid4()),
        }
        return_value = self.case_man.change_case_coordinator(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "subject_uuid": mock_request.json_body["coordinator_uuid"],
        }
        mock_request.get_command_instance().change_case_coordinator.assert_called_once_with(
            **params
        )

    def test_change_case_coordinator_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {"case_uuid": uuid4()}
        with pytest.raises(HTTPBadRequest):
            self.case_man.change_case_coordinator(request=mock_request)

    def test_pause_case_for_weeks(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "suspension_reason": "test reason",
            "suspension_term_value": 5,
            "suspension_term_type": "weeks",
        }
        return_value = self.case_man.pause_case(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "suspension_reason": mock_request.json_body["suspension_reason"],
            "suspension_term_value": mock_request.json_body[
                "suspension_term_value"
            ],
            "suspension_term_type": mock_request.json_body[
                "suspension_term_type"
            ],
        }
        mock_request.get_command_instance().pause_case.assert_called_once_with(
            **params
        )

    def test_pause_case_for_calendar_days(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "suspension_reason": "test reason",
            "suspension_term_value": 5,
            "suspension_term_type": "calendar_days",
        }
        return_value = self.case_man.pause_case(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "suspension_reason": mock_request.json_body["suspension_reason"],
            "suspension_term_value": mock_request.json_body[
                "suspension_term_value"
            ],
            "suspension_term_type": mock_request.json_body[
                "suspension_term_type"
            ],
        }
        mock_request.get_command_instance().pause_case.assert_called_once_with(
            **params
        )

    def test_pause_case_for_business_days(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "suspension_reason": "test reason",
            "suspension_term_value": 5,
            "suspension_term_type": "work_days",
        }
        return_value = self.case_man.pause_case(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "suspension_reason": mock_request.json_body["suspension_reason"],
            "suspension_term_value": mock_request.json_body[
                "suspension_term_value"
            ],
            "suspension_term_type": mock_request.json_body[
                "suspension_term_type"
            ],
        }
        mock_request.get_command_instance().pause_case.assert_called_once_with(
            **params
        )

    def test_pause_case_for_end_date(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "suspension_reason": "test reason",
            "suspension_term_value": "20-07-2019",
            "suspension_term_type": "fixed_date",
        }
        return_value = self.case_man.pause_case(request=mock_request)
        stalled_until_date = datetime.strptime(
            mock_request.json_body["suspension_term_value"], "%d-%m-%Y"
        ).strftime("%Y-%m-%d")

        assert return_value == {"data": {"success": True}}
        assert stalled_until_date == "2019-07-20"

        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "suspension_reason": mock_request.json_body["suspension_reason"],
            "suspension_term_value": mock_request.json_body[
                "suspension_term_value"
            ],
            "suspension_term_type": mock_request.json_body[
                "suspension_term_type"
            ],
        }
        mock_request.get_command_instance().pause_case.assert_called_once_with(
            **params
        )

    def test_pause_case_for_indefinite_period(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "suspension_reason": "test reason",
            "suspension_term_type": "indefinite",
        }
        return_value = self.case_man.pause_case(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "suspension_reason": mock_request.json_body["suspension_reason"],
            "suspension_term_value": None,
            "suspension_term_type": "indefinite",
        }
        mock_request.get_command_instance().pause_case.assert_called_once_with(
            **params
        )

    def test_resume_case(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": str(uuid4()),
            "resume_reason": "test reason",
            "stalled_since_date": "2019-07-01",
            "stalled_until_date": "2019-07-02",
        }
        return_value = self.case_man.resume_case(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": mock_request.json_body["case_uuid"],
            "resume_reason": mock_request.json_body["resume_reason"],
            "stalled_since_date": mock_request.json_body["stalled_since_date"],
            "stalled_until_date": mock_request.json_body["stalled_until_date"],
        }
        mock_request.get_command_instance().resume_case.assert_called_once_with(
            **params
        )

        for param in params:
            test_dict = dict(params)
            del test_dict[param]
            mock_request.json_body = test_dict
            with pytest.raises(HTTPBadRequest):
                self.case_man.resume_case(request=mock_request)

    def test_old_record_factory(self):
        record = log_record_factory(
            pathname="fake_pathname",
            lineno=1,
            msg="fake_msg",
            args=[],
            exc_info=None,
            name="fake_name",
            level=0,
        )
        assert record.zs_component == "zsnl_case_management_http"

    @mock.patch("zsnl_case_management_http.log_record_factory")
    def test_log_record_factory(self, mock_log_record_factory):
        record = logging.getLogRecordFactory()
        record.zs_component = "zsnl_case_management_http"
        logging.setLogRecordFactory(mock_log_record_factory)
        assert record.zs_component == "zsnl_case_management_http"

    def test_pause_case_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {"case_uuid": uuid4()}
        with pytest.raises(HTTPBadRequest):
            self.case_man.pause_case(request=mock_request)

    def test_resume_missing_param(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba"
        }
        with pytest.raises(HTTPBadRequest):
            self.case_man.resume_case(request=mock_request)

    def test_create_case(self):
        case_uuid = str(uuid4())
        mock_request = mock.MagicMock()

        required_fields = [
            "case_type_version_uuid",
            "contact_channel",
            "requestor",
        ]
        for field in required_fields:
            mock_request.json_body = {
                "case_uuid": case_uuid,
                "case_type_version_uuid": mock.MagicMock(),
                "contact_channel": mock.MagicMock(),
                "requestor": mock.MagicMock(),
            }
            del mock_request.json_body[field]
            with pytest.raises(HTTPBadRequest):
                self.case_man.create_case(request=mock_request)

        mock_request.json_body = {
            "case_uuid": case_uuid,
            "case_type_version_uuid": mock.MagicMock(),
            "contact_channel": mock.MagicMock(),
            "requestor": mock.MagicMock(),
            "custom_fields": mock.MagicMock(),
            "confidentiality": mock.MagicMock(),
            "assignee": mock.MagicMock(),
            "contact_information": mock.MagicMock(),
        }
        return_value = self.case_man.create_case(request=mock_request)
        assert return_value == {
            "data": {"success": True, "type": "case", "id": case_uuid}
        }

        del mock_request.json_body["case_uuid"]
        return_value = self.case_man.create_case(request=mock_request)
        assert return_value["data"]["id"] != case_uuid

    @mock.patch.object(Pickler, "flatten")
    def test_get_case_type_version(self, mock_case_type_flatten):
        mock_instance = mock.MagicMock()
        mock_request = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance
        version_uuid = uuid4()
        mock_case_type_flatten.return_value = "test"

        mock_request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.get_case_type_version(request=mock_request)

        mock_request.params = {"version_uuid": version_uuid}

        result = self.case_man.get_case_type_version(request=mock_request)
        assert result == {"data": "test"}
        mock_request.get_query_instance().get_case_type_version_by_uuid.assert_called_once()

    @mock.patch.object(Pickler, "flatten")
    def test_get_case_type_active_version(self, mock_case_type_flatten):
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance
        mock_request.configuration = {}
        case_type_uuid = uuid4()
        mock_case_type_flatten.return_value = {
            "meta": {"is_eligible_for_case_creation": True},
            "attributes": "test",
        }

        mock_request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.get_case_type_active_version(request=mock_request)

        mock_request.params = {"case_type_uuid": case_type_uuid}
        result = self.case_man.get_case_type_active_version(
            request=mock_request
        )
        assert result == {
            "data": {
                "meta": {"is_eligible_for_case_creation": False},
                "attributes": "test",
            }
        }
        mock_request.get_query_instance().get_case_type_active_version.assert_called_once()

    @mock.patch.object(Pickler, "flatten")
    def test_get_case_type_active_version_eligible(
        self, mock_case_type_flatten
    ):
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance
        case_type_uuid = uuid4()
        mock_request.configuration = {"enable_v2_case_eligibility": True}
        mock_case_type_flatten.return_value = {
            "meta": {"is_eligible_for_case_creation": True},
            "attributes": "test",
        }

        mock_request.params = {"case_type_uuid": case_type_uuid}
        result = self.case_man.get_case_type_active_version(
            request=mock_request
        )
        assert result == {
            "data": {
                "meta": {"is_eligible_for_case_creation": True},
                "attributes": "test",
            }
        }
        mock_request.get_query_instance().get_case_type_active_version.assert_called_once()

    def test_set_subject_related_custom_object(self):
        json_body = {
            "person_uuid": str(uuid4()),
            "custom_object_uuid": str(uuid4()),
        }
        mock_cmd = mock.Mock()
        mock_request = mock.Mock()
        mock_request.json_body = json_body
        UserInformation = namedtuple("UserInfo", "user_uuid permissions")
        user_info = UserInformation(
            user_uuid="subject_uuid", permissions="permissions"
        )
        mock_request.get_command_instance.return_value = mock_cmd

        rv = self.case_man.set_subject_related_custom_object(mock_request)

        assert rv == {"data": {"success": True}}

        mock_request.get_command_instance.assert_called_once_with(
            "zsnl_domains.case_management", "subject_uuid", user_info=user_info
        )
        mock_cmd.set_subject_related_custom_object.assert_called_once_with(
            **json_body
        )

    @mock.patch.object(Pickler, "flatten")
    def test_get_subject(self, mock_case_type_flatten):
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance
        subject_uuid = uuid4()
        mock_case_type_flatten.return_value = "test"

        mock_request.params = {"organization_uuid": subject_uuid}
        result = self.case_man.get_subject(request=mock_request)
        assert result == {"data": "test"}
        mock_request.get_query_instance().get_organization_by_uuid.assert_called_once()

        mock_request.params = {"person_uuid": subject_uuid}
        result = self.case_man.get_subject(request=mock_request)
        assert result == {"data": "test"}
        mock_request.get_query_instance().get_person_by_uuid.assert_called_once()

        mock_request.params = {"employee_uuid": subject_uuid}
        result = self.case_man.get_subject(request=mock_request)
        assert result == {"data": "test"}
        mock_request.get_query_instance().get_employee_by_uuid.assert_called_once()

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {
                "organization_uuid": uuid4(),
                "employee_uuid": uuid4(),
            }
            self.case_man.get_subject(request=mock_request)

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {}
            self.case_man.get_subject(request=mock_request)

    @mock.patch.object(Pickler, "flatten")
    def test_get_subject_relations(self, mock_flatten):
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance
        case_uuid = uuid4()
        mock_flatten.return_value = "test"
        mock_request.get_query_instance().get_subject_relations_for_case.return_value = {
            "result": ["test"]
        }

        mock_request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.get_subject_relations(request=mock_request)

        mock_request.params = {"case_uuid": case_uuid}
        result = self.case_man.get_subject_relations(request=mock_request)
        assert result == {
            "data": ["test"],
            "links": {"self": mock_request.current_route_path()},
        }
        mock_request.get_query_instance().get_subject_relations_for_case.assert_called_once()

    @mock.patch.object(Pickler, "flatten")
    @mock.patch(
        "zsnl_case_management_http.views.case_management._get_included_subjects"
    )
    def test_get_subject_relations_valid_include(
        self, mock_get_included, mock_flatten
    ):
        mock_request = mock.MagicMock()
        mock_request.params = {"case_uuid": str(uuid4()), "include": "subject"}
        mock_flatten.return_value = "test"
        mock_get_included.return_value = ["a"]

        mock_instance = mock.MagicMock()
        mock_request.get_query_instance.return_value = mock_instance
        mock_request.get_query_instance().get_subject_relations_for_case.return_value = {
            "result": ["test"]
        }

        result = self.case_man.get_subject_relations(request=mock_request)

        assert result == {
            "data": ["test"],
            "links": {"self": mock_request.current_route_path()},
            "included": ["test"],
        }
        mock_get_included.assert_called_once_with(mock_instance, ["test"])

    @mock.patch.object(Pickler, "flatten")
    def test_get_subject_relations_invalid_include(self, mock_flatten):
        mock_request = mock.MagicMock()
        mock_request.params = {"case_uuid": str(uuid4()), "include": "invalid"}

        with pytest.raises(HTTPBadRequest):
            self.case_man.get_subject_relations(request=mock_request)

    def test__get_included_subjects(self):
        mock_query = mock.MagicMock()
        mock_query.get_persons_by_uuid.return_value = ["person"]
        mock_query.get_organizations_by_uuid.return_value = ["organization"]
        mock_query.get_employees_by_uuid.return_value = ["employee"]
        rs = namedtuple("SubjectRelationRelatedSubject", "subject")
        person_uuid_1 = uuid4()
        person_uuid_2 = uuid4()
        organization_uuid_1 = uuid4()
        organization_uuid_2 = uuid4()
        employee_uuid_1 = uuid4()
        employee_uuid_2 = uuid4()
        related_subjects = [
            rs(
                subject=SubjectRelationRelatedSubject(
                    type="person", id=person_uuid_1
                )
            ),
            rs(
                subject=SubjectRelationRelatedSubject(
                    type="person", id=person_uuid_2
                )
            ),
            rs(
                subject=SubjectRelationRelatedSubject(
                    type="organization", id=organization_uuid_1
                )
            ),
            rs(
                subject=SubjectRelationRelatedSubject(
                    type="organization", id=organization_uuid_2
                )
            ),
            rs(
                subject=SubjectRelationRelatedSubject(
                    type="employee", id=employee_uuid_1
                )
            ),
            rs(
                subject=SubjectRelationRelatedSubject(
                    type="employee", id=employee_uuid_2
                )
            ),
        ]

        res = self.case_man._get_included_subjects(
            mock_query, related_subjects
        )

        assert res == ["person", "organization", "employee"]
        mock_query.get_persons_by_uuid.assert_called_once_with(
            person_uuids={str(person_uuid_1), str(person_uuid_2)}
        )
        mock_query.get_organizations_by_uuid.assert_called_once_with(
            organization_uuids={
                str(organization_uuid_1),
                str(organization_uuid_2),
            }
        )
        mock_query.get_employees_by_uuid.assert_called_once_with(
            employee_uuids={str(employee_uuid_1), str(employee_uuid_2)}
        )

    def test_create_subject_relation(self):
        mock_request = mock.MagicMock()
        return_value = self.case_man.create_subject_relation(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

        required_fields = [
            "case_uuid",
            "subject",
            "role",
        ]
        for field in required_fields:
            mock_request.json_body = {
                "case_uuid": str(uuid4),
                "subject": mock.MagicMock(),
                "role": mock.MagicMock(),
                "permission": "write",
            }
            del mock_request.json_body[field]
            with pytest.raises(HTTPBadRequest):
                self.case_man.create_subject_relation(request=mock_request)

        mock_request.json_body = {
            "case_uuid": mock.MagicMock(),
            "subject": mock.MagicMock(),
            "magic_string_prefix": mock.MagicMock(),
            "role": mock.MagicMock(),
            "authorized": mock.MagicMock(),
            "send_confirmation_email": mock.MagicMock(),
            "permission": "write",
        }
        return_value = self.case_man.create_subject_relation(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

    def test_create_case_relation(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {}

        with pytest.raises(HTTPBadRequest):
            self.case_man.create_case_relation(request=mock_request)

        uuid1 = str(uuid4())
        uuid2 = str(uuid4())
        mock_request.json_body = {"uuid1": uuid1, "uuid2": uuid2}

        mock_command = mock.MagicMock()
        mock_request.get_command_instance.return_value = mock_command
        self.case_man.create_case_relation(request=mock_request)
        UserInformation = namedtuple("UserInfo", "user_uuid permissions")

        user_info = UserInformation(
            user_uuid="subject_uuid", permissions="permissions"
        )
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management",
            user_uuid="subject_uuid",
            user_info=user_info,
        )

    @mock.patch.object(Pickler, "flatten")
    def test_get_case_relations(self, mock_flatten):
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_relation = mock.MagicMock()

        mock_relation.object1_uuid = "fake_uuid"
        mock_relation.summary1 = "fake_summary"
        mock_relation.case_id_a = 1
        mock_instance.get_case_relations.return_value = {
            "result": [mock_relation]
        }
        mock_request.get_query_instance.return_value = mock_instance
        case_uuid = uuid4()
        mock_flatten.return_value = "test"

        mock_request.current_route_path.return_value = "fake_path"
        mock_request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.get_case_relations(request=mock_request)

        mock_request.params = {"case_uuid": case_uuid}

        result = self.case_man.get_case_relations(request=mock_request)
        assert result == {
            "links": {"self": "fake_path"},
            "data": ["test"],
            "included": [],
        }
        mock_request.get_query_instance().get_case_relations.assert_called_once_with(
            case_uuid=case_uuid
        )

    @mock.patch.object(Pickler, "flatten")
    def test_get_case_relations_with_authorization(self, mock_flatten):
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        mock_relation = mock.MagicMock()

        mock_relation.object1_uuid = "fake_uuid"
        mock_relation.summary1 = "fake_summary"
        mock_relation.case_id_a = 1
        mock_instance.get_case_relations.return_value = {
            "result": [mock_relation]
        }
        mock_request.get_query_instance.return_value = mock_instance
        case_uuid = uuid4()
        mock_flatten.return_value = "test"

        mock_request.current_route_path.return_value = "fake_path"
        mock_request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.get_case_relations(request=mock_request)

        mock_request.params = {
            "case_uuid": case_uuid,
            "authorization": "admin",
        }

        result = self.case_man.get_case_relations(request=mock_request)
        assert result == {
            "links": {"self": "fake_path"},
            "data": ["test"],
            "included": [],
        }
        mock_request.get_query_instance().get_case_relations.assert_called_once_with(
            case_uuid=case_uuid, authorization="admin"
        )

    def test_update_subject_relation(self):
        mock_request = mock.MagicMock()
        return_value = self.case_man.update_subject_relation(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

        required_fields = ["relation_uuid", "magic_string_prefix", "role"]
        for field in required_fields:
            mock_request.json_body = {
                "relation_uuid": mock.MagicMock(),
                "magic_string_prefix": mock.MagicMock(),
                "role": mock.MagicMock(),
            }
            del mock_request.json_body[field]
            with pytest.raises(HTTPBadRequest):
                self.case_man.update_subject_relation(request=mock_request)

        mock_request.json_body = {
            "relation_uuid": mock.MagicMock(),
            "magic_string_prefix": mock.MagicMock(),
            "role": mock.MagicMock(),
            "authorized": mock.MagicMock(),
            "permission": "write",
        }
        return_value = self.case_man.update_subject_relation(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

    def test_delete_subject_relation(self):
        mock_request = mock.MagicMock()
        return_value = self.case_man.delete_subject_relation(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

        mock_request.json_body = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.delete_subject_relation(request=mock_request)

        mock_request.json_body = {"relation_uuid": mock.MagicMock()}
        return_value = self.case_man.delete_subject_relation(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

    def test_reorder_case_relation(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {}
        UserInformation = namedtuple("UserInfo", "user_uuid permissions")

        user_info = UserInformation(
            user_uuid="subject_uuid", permissions="permissions"
        )
        with pytest.raises(HTTPBadRequest):
            self.case_man.reorder_case_relation(request=mock_request)

        relation_uuid = str(uuid4())
        case_uuid = str(uuid4())
        new_index = 1
        mock_request.json_body = {
            "relation_uuid": relation_uuid,
            "case_uuid": case_uuid,
            "new_index": new_index,
        }

        mock_command = mock.MagicMock()
        mock_request.get_command_instance.return_value = mock_command

        self.case_man.reorder_case_relation(request=mock_request)
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management",
            user_uuid="subject_uuid",
            user_info=user_info,
        )
        mock_command.reorder_case_relation.assert_called_once_with(
            relation_uuid=relation_uuid,
            case_uuid=case_uuid,
            new_index=new_index,
        )

    def test_delete_case_relation(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {}
        with pytest.raises(HTTPBadRequest):
            self.case_man.delete_case_relation(request=mock_request)

        mock_request.json_body = {
            "relation_uuid": mock.MagicMock(),
            "case_uuid": mock.MagicMock(),
        }
        return_value = self.case_man.delete_case_relation(request=mock_request)
        assert return_value == {"data": {"success": True}}

    def test_get_case_relations_with_include(self):
        # test for case relation
        mock_request = mock.MagicMock()
        mock_instance = mock.MagicMock()
        case_uuid_1 = uuid4()
        case_uuid_2 = uuid4()
        assignee_uuid = uuid4()
        result_uuid_1 = uuid4()
        result_uuid_2 = uuid4()

        mock_relation = CaseRelation(
            object1_uuid=case_uuid_1,
            summary1="summary1",
            case_id_a=1,
            object2_uuid=case_uuid_2,
            summary2="summary2",
            case_id_b=2,
            current_case_uuid=case_uuid_1,
        )
        mock_case_summary = CaseSummary(
            uuid=case_uuid_1,
            summary="summary1",
            progress_status=0.33333,
            number=1,
            casetype_title="title1",
            result={
                "result": "afgehandeld",
                "result_name": "afgehandeld",
                "result_uuid": result_uuid_1,
                "archival_attributes": {
                    "state": "vernietigen",
                    "selection_list": "12",
                },
            },
            assignee={"uuid": assignee_uuid, "name": "beheerder"},
        )
        mock_case_summary_2 = CaseSummary(
            uuid=case_uuid_2,
            summary="summary2",
            progress_status=0.5,
            number=2,
            casetype_title="title2",
            result={
                "result": "afgehandeld",
                "result_name": "afgehandeld",
                "result_uuid": result_uuid_2,
                "archival_attributes": {
                    "state": "vernietigen",
                    "selection_list": "12",
                },
            },
            assignee={"uuid": assignee_uuid, "name": "beheerder"},
        )

        mock_instance.get_case_relations.return_value = {
            "result": [mock_relation]
        }
        mock_instance.get_case_summaries_by_uuid.return_value = {
            "result": [mock_case_summary, mock_case_summary_2]
        }
        mock_request.get_query_instance.return_value = mock_instance

        mock_request.params = {
            "case_uuid": case_uuid_1,
            "include": "this_case,other_case",
        }

        res = self.case_man.get_case_relations(request=mock_request)
        mock_request.get_query_instance().get_case_relations.assert_called_once_with(
            case_uuid=case_uuid_1
        )

        assignee_uuid = str(assignee_uuid)
        case_uuid_1 = str(case_uuid_1)
        case_uuid_2 = str(case_uuid_2)

        assert res["included"][0] == {
            "type": "case_summary",
            "id": case_uuid_1,
            "attributes": {
                "number": 1,
                "progress_status": 0.33333,
                "casetype_title": "title1",
                "summary": "summary1",
                "result": {
                    "result": "afgehandeld",
                    "result_name": "afgehandeld",
                    "result_uuid": result_uuid_1,
                    "archival_attributes": {
                        "state": "vernietigen",
                        "selection_list": "12",
                    },
                },
            },
            "relationships": {
                "assignee": {
                    "links": {
                        "self": f"/api/v2/cm/subject/get_subject?employee_uuid={assignee_uuid}"
                    },
                    "data": {"type": "subject", "id": assignee_uuid},
                    "meta": {"display_name": "beheerder"},
                }
            },
            "links": {
                "self": f"/api/v2/cm/case/get_case?case_uuid={case_uuid_1}"
            },
        }
        assert res["included"][1] == {
            "type": "case_summary",
            "id": case_uuid_2,
            "attributes": {
                "number": 2,
                "progress_status": 0.5,
                "casetype_title": "title2",
                "summary": "summary2",
                "result": {
                    "result": "afgehandeld",
                    "result_name": "afgehandeld",
                    "result_uuid": result_uuid_2,
                    "archival_attributes": {
                        "state": "vernietigen",
                        "selection_list": "12",
                    },
                },
            },
            "relationships": {
                "assignee": {
                    "links": {
                        "self": f"/api/v2/cm/subject/get_subject?employee_uuid={assignee_uuid}"
                    },
                    "data": {"type": "subject", "id": assignee_uuid},
                    "meta": {"display_name": "beheerder"},
                }
            },
            "links": {
                "self": f"/api/v2/cm/case/get_case?case_uuid={case_uuid_2}"
            },
        }

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {
                "case_uuid": "fake_uuid",
                "include": "other_case,assignee",
            }
            self.case_man.get_case_relations(request=mock_request)

    def test_assign_cse_to_user(self):
        mock_request = mock.MagicMock()
        mock_request.POST = {}
        mock_request.POST["case_uuid"] = uuid4()
        mock_request.POST["user_uuid"] = uuid4()

        return_value = self.case_man.assign_case_to_user(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance().assign_case_to_user.assert_called_once()

    def test_assign_cse_to_department(self):
        mock_request = mock.MagicMock()
        mock_request.POST = {}
        mock_request.POST["case_uuid"] = uuid4()
        mock_request.POST["department_uuid"] = uuid4()
        mock_request.POST["role_uuid"] = uuid4()

        return_value = self.case_man.assign_case_to_department(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance().assign_case_to_department.assert_called_once()

    def test_recover_case_relations(self):
        mock_request = mock.MagicMock()
        mock_request.POST = {}
        mock_request.POST["case_uuid"] = uuid4()

        return_value = self.case_man.recover_case_relations(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance().recover_case_relations.assert_called_once()

    def test_sync_geo_values(self):
        mock_request = mock.MagicMock()
        mock_request.POST = {}
        mock_request.POST["custom_object_uuid"] = uuid4()

        return_value = self.case_man.sync_geo_values(request=mock_request)
        assert return_value == {"data": {"success": True}}

        mock_request.get_command_instance().sync_geo_values.assert_called_once()

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {
                "contact_uuid": "fake_uuid",
            }
            self.case_man.sync_geo_values(request=mock_request)

    def test_add_validsign_timeline_entry(self):
        mock_request = mock.MagicMock()

        participants = [
            {
                "name": "name1",
                "lastName": "lastname1",
                "email": "string",
                "phone": "string",
                "order": 0,
            },
            {
                "name": "name2",
                "lastName": "lastname2",
                "email": "string",
                "phone": "string",
                "order": 0,
            },
        ]

        mock_request.json_body = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
            "participants": participants,
        }
        return_value = self.case_man.add_validsign_timeline_entry(
            request=mock_request
        )
        assert return_value == {"data": {"success": True}}
        mock_request.get_command_instance.assert_called_with(
            domain="zsnl_domains.case_management", user_uuid="subject_uuid"
        )
        params = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
            "participants": participants,
        }
        mock_request.get_command_instance().add_validsign_timeline_entry.assert_called_once_with(
            **params
        )

    def test_add_validsign_timeline_entry_no_participants(self):
        mock_request = mock.MagicMock()

        mock_request.json_body = {
            "case_uuid": "ff020d3c-3a85-11e9-8149-8b0d71e6c5ba",
        }

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {
                "case_uuid": "fake_uuid",
            }
            self.case_man.add_validsign_timeline_entry(request=mock_request)
