# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.4.1"

import logging
import minty
import minty_pyramid
import os
from .routes import routes
from minty.middleware import AmqpPublisherMiddleware
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import case_management
from zsnl_perl_migration import (
    LegacyEventBroadcastMiddleware,
    LegacyEventQueueMiddleware,
)
from zsnl_pyramid import platform_key

legacy_broadcast_events = [
    ("zsnl_domains.case_management", "CaseTargetCompletionDateSet"),
    ("zsnl_domains.case_management", "CaseRegistrationDateSet"),
    ("zsnl_domains.case_management", "CaseCompletionDateSet"),
    ("zsnl_domains.case_management", "CaseAssigneeChanged"),
    ("zsnl_domains.case_management", "CaseCoordinatorSet"),
    ("zsnl_domains.case_management", "CasePaused"),
    ("zsnl_domains.case_management", "CaseResumed"),
    ("zsnl_domains.case_management", "CaseCreated"),
    ("zsnl_domains.case_management", "CaseStatusSet"),
    ("zsnl_domains.case_management", "AddValidsignTimelineEntry"),
]

interesting_case_relation_events = [
    ("zsnl_domains.case_management", "CaseRelationCreated"),
    ("zsnl_domains.case_management", "CaseRelationReordered"),
    ("zsnl_domains.case_management", "CaseRelationDeleted"),
]

interesting_subject_relation_events = [
    ("zsnl_domains.case_management", "SubjectRelationCreated"),
    ("zsnl_domains.case_management", "SubjectRelationUpdated"),
    ("zsnl_domains.case_management", "SubjectRelationDeleted"),
]

ZS_COMPONENT = "zsnl_case_management_http"

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    return record


def main(*args, **kwargs):
    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    loader = minty_pyramid.Engine(
        domains=[case_management],
        command_wrapper_middleware=[
            LegacyEventQueueMiddleware(
                database_infra_name="database",
                interesting_events=legacy_broadcast_events,
                interesting_case_relation_events=interesting_case_relation_events,
                interesting_subject_relation_events=interesting_subject_relation_events,
            ),
            DatabaseTransactionMiddleware("database"),
            LegacyEventBroadcastMiddleware(
                amqp_infra_name="amqp",
                interesting_events=legacy_broadcast_events,
                interesting_case_relation_events=interesting_case_relation_events,
                interesting_subject_relation_events=interesting_subject_relation_events,
            ),
            AmqpPublisherMiddleware(
                publisher_name="case_management", infrastructure_name="amqp"
            ),
        ],
    )
    config = loader.setup(*args, **kwargs)

    # Enable platform key access to this service
    config.include(platform_key)

    routes.add_routes(config)
    return loader.main()
