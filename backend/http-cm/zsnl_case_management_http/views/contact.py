# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class ContactViews(JSONAPIEntityView):
    # Move this away in new version, and do this sexyer
    create_link_from_entity = create_link_from_entity

    # Move this to openapi.json
    view_mapper = {
        "GET": {
            "get_contact": {
                "cq": "query",
                "auth_permissions": {"contact_search"},
                "domain": "zsnl_domains.case_management",
                "run": "get_contact",
                "from": {"request_params": {"type": "type", "uuid": "uuid"}},
            },
            "get_contact_limited": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_contact_limited",
                "from": {"request_params": {"type": "type", "uuid": "uuid"}},
            },
            "get_contact_settings": {
                "cq": "query",
                "auth_permissions": {"contact_search"},
                "domain": "zsnl_domains.case_management",
                "run": "get_contact_settings",
                "from": {"request_params": {"uuid": "uuid"}},
            },
            "get_contact_event_logs": {
                "cq": "query",
                "auth_permissions": {"contact_search"},
                "domain": "zsnl_domains.case_management",
                "run": "get_contact_event_logs",
                "from": {
                    "request_params": {
                        "contact_type": "contact_type",
                        "contact_uuid": "contact_uuid",
                        "period_start": "period_start",
                        "period_end": "period_end",
                    }
                },
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
            "get_person_sensitive_data": {
                "cq": "query",
                "auth_permissions": {"view_sensitive_contact_data"},
                "domain": "zsnl_domains.case_management",
                "run": "get_person_sensitive_data",
                "from": {
                    "request_params": {
                        "uuid": "uuid",
                        "sensitive_data": "sensitive_data",
                    }
                },
            },
        },
        "POST": {
            "set_notification_settings": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_notification_settings",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "employee_settings",
            },
            "save_signature": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "save_signature",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "employee_settings",
            },
            "delete_signature": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "delete_signature",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "employee_settings",
            },
            "change_phone_extension": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "change_phone_extension",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "employee_settings",
            },
            "save_contact_information": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "save_contact_information",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "type",
            },
            "export_timeline": {
                "cq": "command",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.case_management",
                "run": "request_export",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "type",
            },
            "update_non_authentic_contact": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_non_authentic_contact",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "type",
            },
        },
    }
