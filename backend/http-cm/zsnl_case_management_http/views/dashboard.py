# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class DashboardViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "get_dashboard": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_dashboard",
                "from": {},
            }
        },
        "POST": {
            "update_dashboard": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_dashboard",
                "from": {"json": {"_all": True}},
            }
        },
    }
