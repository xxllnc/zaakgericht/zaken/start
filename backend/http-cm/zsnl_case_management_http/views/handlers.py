# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import jsonpickle.handlers
from datetime import datetime
from uuid import UUID
from zsnl_domains.case_management.entities import (
    Case,
    CaseRelation,
    CaseSummary,
    CaseTypeVersionEntity,
    Employee,
    Organization,
    Person,
    SubjectRelation,
)


def _subject_related_custom_object_relationship_only(
    subject: Person | Organization | Employee,
):
    if subject.related_custom_object:
        return {
            "related_custom_object": {
                "links": {
                    "self": f"/api/v2/cm/custom_object/get_custom_object?uuid={subject.related_custom_object.uuid}"
                },
                "data": {
                    "type": "custom_object",
                    "id": subject.related_custom_object.uuid,
                },
            }
        }

    return {}


def _subject_related_custom_object(subject: Person | Organization | Employee):
    relationships = _subject_related_custom_object_relationship_only(subject)

    if relationships != {}:
        return {"relationships": relationships}

    return {}


@jsonpickle.handlers.register(datetime, base=True)
class DateTimeHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, obj, data):
        return obj.isoformat()


@jsonpickle.handlers.register(UUID, base=True)
class UUIDHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, obj, data):
        return str(obj)


def _get_formatted_value(value_to_format):
    if value_to_format.get("value", None) in [None, ""]:
        return None

    if value_to_format["bibliotheek_kenmerken_type"] == "date":
        return value_to_format["value"][:10]

    return value_to_format["value"]


@jsonpickle.handlers.register(CaseRelation, base=True)
class CaseRelationHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, case_relation, data):
        first_case = {
            "data": {
                "type": "case_summary",
                "id": str(case_relation.object1_uuid),
            },
            "meta": {"relation_type": case_relation.relationship_type1},
        }
        second_case = {
            "data": {
                "type": "case_summary",
                "id": str(case_relation.object2_uuid),
            },
            "meta": {"relation_type": case_relation.relationship_type2},
        }
        this_case, other_case = None, None
        sequence_number = None
        if case_relation.object1_uuid == case_relation.current_case_uuid:
            sequence_number = case_relation.order_seq_b
            this_case, other_case = first_case, second_case
        else:
            sequence_number = case_relation.order_seq_a
            this_case, other_case = second_case, first_case

        return {
            "type": "case_relation",
            "id": str(case_relation.uuid),
            "attributes": {
                "relation_type": case_relation.relation_type,
                "blocks_deletion": case_relation.blocks_deletion or False,
                "owner_uuid": str(case_relation.owner_uuid)
                if case_relation.owner_uuid
                else None,
                "sequence_number": sequence_number,
            },
            "relationships": {
                "this_case": this_case,
                "other_case": other_case,
            },
        }


# This is needed to allow correct registration of the handlers
# because they have to be in the same module as the pickler
class Flattener(jsonpickle.pickler.Pickler):
    pass


@jsonpickle.handlers.register(Case, base=True)
class CaseHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, case, data):
        pickler = jsonpickle.pickler.Pickler(unpicklable=False)
        attributes_dict = pickler.flatten(case.attributes)
        custom_fields = {}
        for name, values in attributes_dict["custom_fields"].items():
            if len(values) == 0:
                continue

            custom_fields[name] = {
                "value": _get_formatted_value(values[0]),
                "type": values[0]["bibliotheek_kenmerken_type"],
            }

        confidentiality_mapping = {
            "public": "Openbaar",
            "internal": "Intern",
            "confidential": "Vertrouwelijk",
        }

        assignee = None
        coordinator = None

        if case.assignee is not None:
            assignee = {
                "data": {
                    "type": "employee",
                    "id": str(case.assignee.uuid or ""),
                }
            }

        if case.coordinator is not None:
            coordinator = {
                "data": {
                    "type": "employee",
                    "id": str(case.coordinator.uuid or ""),
                }
            }

        return {
            "type": "case",
            "id": str(case.uuid),
            "attributes": {
                "number": case.id,
                "summary": case.attributes["summary"],
                "public_summary": case.attributes["public_summary"],
                "status": {
                    "name": case.status,
                    "reason": case.suspension_reason,
                    "since": str(case.stalled_since_date),
                    "until": str(case.stalled_until_date),
                },
                "registration_date": str(case.registration_date),
                "target_completion_date": str(case.target_completion_date),
                "completion_date": str(case.completion_date),
                "destruction_date": str(case.destruction_date),
                "phase": case.attributes["phase"],
                "result": case.attributes["result"],
                "contactchannel": case.contact_channel,
                "payment": {
                    "amount": case.payment_amount,
                    "status": None,  # This field is not present in the case entity
                },
                "custom_fields": custom_fields,
                "confidentiality": {
                    "original": case.confidentiality,
                    "mapped": confidentiality_mapping.get(
                        case.confidentiality, None
                    ),
                },
            },
            "meta": {
                "last_modified_datetime": case.last_modified_date.isoformat(),
                "created_datetime": case.created_date.isoformat(),
                "summary": attributes_dict["summary"],
            },
            "relationships": {
                "assignee": assignee,
                "coordinator": coordinator,
                "requestor": {
                    "data": {
                        "id": case.requestor["uuid"],
                        "type": case.requestor["type"],
                    }
                },
                "subjects": case.subjects,
                "casetype": {
                    "data": {
                        "type": "casetype",
                        "id": case.case_type_version_uuid,
                    }
                },
            },
        }


@jsonpickle.handlers.register(CaseTypeVersionEntity, base=True)
class CasetypeHandler(jsonpickle.handlers.BaseHandler):
    def format_metadata(self, metadata):
        metadata = metadata.dict()
        for key in [
            "may_postpone",
            "may_extend",
            "possibility_for_objection_and_appeal",
            "publication",
            "penalty_law",
            "bag",
            "lex_silencio_positivo",
            "wkpb_applies",
        ]:
            metadata[key] = metadata[key] == "Ja"
        return metadata

    def format_settings(self, settings):
        settings = settings.dict()
        for key in settings:
            if settings[key] in [1, "1"]:
                settings[key] = True
            elif settings[key] is None:
                settings[key] = False
        return settings

    def format_catalog_folder(self, catalog_folder):
        return {
            "data": {
                "id": str(catalog_folder.uuid),
                "type": "folder",
                "meta": {"name": catalog_folder.name},
            }
        }

    def format_related_contact(self, contact):
        return {
            "data": {
                "id": str(contact.uuid),
                "type": contact.entity_type,
                "meta": {"name": contact.entity_meta_summary},
            }
        }

    def format_terms(self, object):
        data = {}
        for attr, value in object.__dict__.items():
            dict = {}
            for attr2, value2 in value.__dict__.items():
                dict[attr2] = value2
            data[attr] = dict
        return data

    def format_nested_object(self, obj):
        if not hasattr(obj, "__dict__"):
            return obj
        result = {}
        for key, val in obj.__dict__.items():
            if key in [
                "entity_type",
                "entity_id",
                "entity_meta_summary",
                "entity_relationships",
                "entity_meta__fields",
                "entity_id__fields",
                "entity_changelog",
                "entity_data",
            ] or key.startswith("_"):
                continue

            if isinstance(val, UUID):
                element = str(val)
            elif isinstance(val, list):
                element = []
                for item in val:
                    element.append(self.format_nested_object(item))
            else:
                element = self.format_nested_object(val)
            result[key] = element
        return result

    def flatten(self, obj, data):
        relationships = {}

        if obj.catalog_folder:
            relationships["catalog_folder"] = self.format_catalog_folder(
                obj.catalog_folder
            )

        if obj.preset_assignee:
            relationships["preset_assignee"] = self.format_related_contact(
                obj.preset_assignee
            )

        if obj.preset_requestor:
            relationships["preset_requestor"] = self.format_related_contact(
                obj.preset_requestor
            )

        return {
            "type": "case_type",
            "id": str(obj.uuid),
            "meta": {
                "last_modified": obj.last_modified,
                "created": obj.created,
                "summary": obj.name,
                "is_eligible_for_case_creation": obj.is_eligible_for_case_creation,
            },
            "relationships": relationships,
            "attributes": {
                "name": obj.name,
                "identification": obj.identification,
                "description": obj.description,
                "tags": obj.tags,
                "case_summary": obj.case_summary,
                "case_public_summary": obj.case_public_summary,
                "metadata": self.format_metadata(obj.metadata),
                "settings": self.format_settings(obj.settings),
                "initiator_source": obj.initiator_source,
                "initiator_type": obj.initiator_type,
                "terms": self.format_nested_object(obj.terms),
                "requestor": obj.requestor.__dict__,
                "phases": [
                    self.format_nested_object(phase) for phase in obj.phases
                ],
            },
        }


@jsonpickle.handlers.register(Organization, base=True)
class OrganizationHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, obj, data):
        return {
            "type": "organization",
            "id": str(obj.uuid),
            "attributes": {
                "name": obj.name,
                "source": obj.source,
                "coc_number": obj.coc_number,
                "coc_location_number": obj.coc_location_number,
                "organization_type": obj.organization_type,
                "has_correspondence_address": bool(obj.correspondence_address),
                "location_address": obj.location_address.dict()
                if obj.location_address
                else None,
                "correspondence_address": obj.correspondence_address.dict()
                if obj.correspondence_address
                else None,
                "contact_information": obj.contact_information.dict(),
                "has_valid_address": obj.has_valid_address,
            },
            **_subject_related_custom_object(obj),
        }


@jsonpickle.handlers.register(Employee, base=True)
class EmployeeHandler(jsonpickle.handlers.BaseHandler):
    def _format_role(self, role):
        return {
            "type": "role",
            "id": str(role.uuid),
            "meta": {"name": role.entity_meta_summary},
        }

    def _format_department(self, department):
        return {
            "type": "department",
            "id": str(department.uuid),
            "meta": {"name": department.entity_meta_summary},
        }

    def flatten(self, obj, data):
        return {
            "type": "employee",
            "id": str(obj.uuid),
            "attributes": {
                "source": obj.source,
                "status": obj.status,
                "first_name": obj.first_name,
                "surname": obj.surname,
                "contact_information": obj.contact_information.dict()
                if obj.contact_information is not None
                else None,
                "has_valid_address": obj.has_valid_address,
            },
            "relationships": {
                "department": {
                    "data": self._format_department(obj.department)
                },
                "roles": {
                    "data": [self._format_role(role) for role in obj.roles]
                },
                **_subject_related_custom_object_relationship_only(obj),
            },
        }


@jsonpickle.handlers.register(Person, base=True)
class PersonHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, obj, data):
        pickler = jsonpickle.pickler.Pickler(unpicklable=False)
        return {
            "type": "person",
            "id": str(obj.uuid),
            "attributes": {
                "authenticated": obj.authenticated,
                "source": obj.source,
                "first_names": obj.first_names,
                "insertions": obj.insertions,
                "family_name": obj.family_name,
                "noble_title": obj.noble_title,
                "surname": obj.surname,
                "date_of_birth": pickler.flatten(obj.date_of_birth),
                "date_of_death": pickler.flatten(obj.date_of_death),
                "gender": obj.gender,
                "inside_municipality": obj.inside_municipality,
                "has_correspondence_address": bool(obj.correspondence_address),
                "residence_address": obj.residence_address.dict()
                if obj.residence_address
                else None,
                "correspondence_address": obj.correspondence_address.dict()
                if obj.correspondence_address
                else None,
                "contact_information": obj.contact_information.dict()
                if obj.contact_information
                else None,
                "has_valid_address": obj.has_valid_address,
            },
            "relationships": {
                **_subject_related_custom_object_relationship_only(obj)
            },
        }


@jsonpickle.handlers.register(SubjectRelation, base=True)
class SubjectRelationHandler(jsonpickle.handlers.BaseHandler):
    def format_subject(self, subject):
        subject_id = subject.id
        subject_type = subject.type
        subject_name = subject.name
        return {
            "data": {
                "id": str(subject_id),
                "type": subject_type,
                "meta": {"display_name": subject_name},
            },
            "links": {
                "related": f"/api/v2/cm/subject/get_subject?{subject_type}_uuid={subject_id}"
            },
        }

    def format_case(self, case):
        case_id = case.id
        return {
            "data": {"id": str(case_id), "type": "case"},
            "links": {
                "related": f"/api/v2/cm/case/get_case?case_uuid={case_id}"
            },
        }

    def flatten(self, obj, data):
        attributes = {
            "magic_string_prefix": obj.magic_string_prefix,
            "role": obj.role,
            "is_preset_client": obj.is_preset_client,
            "source_custom_field_magic_string": obj.source_custom_field_magic_string,
        }
        if obj.subject.type == "employee":
            attributes.update({"permission": obj.permission or "none"})
        else:
            attributes.update({"authorized": obj.authorized})

        return {
            "type": obj.type,
            "id": str(obj.uuid),
            "attributes": attributes,
            "relationships": {
                "case": self.format_case(obj.case),
                "subject": self.format_subject(obj.subject),
            },
        }


@jsonpickle.handlers.register(CaseSummary, base=True)
class CaseSummaryHandler(jsonpickle.handlers.BaseHandler):
    def flatten(self, case_summary, data):
        case_uuid = str(case_summary.uuid)

        relationships = {}
        if case_summary.assignee and case_summary.assignee.uuid:
            assignee_uuid = str(case_summary.assignee.uuid)
            relationships["assignee"] = {
                "links": {
                    "self": f"/api/v2/cm/subject/get_subject?employee_uuid={assignee_uuid}"
                },
                "data": {"type": "subject", "id": assignee_uuid},
                "meta": {"display_name": case_summary.assignee.name},
            }

        return {
            "type": "case_summary",
            "id": str(case_uuid),
            "attributes": {
                "number": case_summary.number,
                "progress_status": case_summary.progress_status,
                "casetype_title": case_summary.casetype_title,
                "summary": case_summary.summary,
                "result": case_summary.result,
            },
            "relationships": relationships,
            "links": {
                "self": f"/api/v2/cm/case/get_case?case_uuid={case_uuid}"
            },
        }
