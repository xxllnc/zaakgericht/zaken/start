# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class SearchResultViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "search": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "search",
                "from": {
                    "request_params": {
                        "type": "type",
                        "keyword": "keyword",
                        "max_results": {"name": "max_results", "type": int},
                        "max_results_per_type": {
                            "name": "max_results_per_type",
                            "type": int,
                        },
                        "filter[]": "filter_params",
                    }
                },
            },
            "search_attribute": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "search_attribute",
                "from": {
                    "request_params": {"filter[]": "filters", "sort": "sort"}
                },
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
        },
        "POST": {
            "search_custom_objects": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "search_custom_objects",
                "from": {"json": {"_all": True}},
            },
            "search_custom_object_total_results": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "search_custom_object_total_results",
                "from": {"json": {"_all": True}},
            },
        },
    }
