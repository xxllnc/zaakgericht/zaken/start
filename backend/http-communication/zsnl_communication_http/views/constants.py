# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

IMAGE_MIMETYPES = ["image/png", "image/gif", "image/bmp", "image/jpeg"]
PDF_MIMETYPE = "application/pdf"
