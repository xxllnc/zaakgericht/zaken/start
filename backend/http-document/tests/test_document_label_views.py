# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from pyramid.httpexceptions import HTTPForbidden
from unittest import mock
from uuid import uuid4
from zsnl_document_http.views import document_label


class TestDocumentLabelViews:
    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    def test_views_document_label(self, mock_get_logged_in_user):
        request = mock.MagicMock()
        request.matched_route.name = "get_document_labels"
        request.method = "GET"
        request.params = {"case_uuid": uuid4()}

        user_info = mock.MagicMock()
        user_info.uuid = uuid4()
        mock_get_logged_in_user.return_value = user_info

        entity = mock.MagicMock()
        type(entity).entity_type = mock.PropertyMock(
            return_value="document_label"
        )
        type(entity).entity_id = mock.PropertyMock(return_value=uuid4())

        dl = document_label.DocumentLabelViews(context="test", request=request)
        dl()
        assert dl.create_link_from_entity(entity) is None

        with pytest.raises(HTTPForbidden):
            user_info.permissions = {"gebruiker": False}
            dl()
