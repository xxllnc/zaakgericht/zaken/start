# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_document_http.views import document


class TestDocumentViews:
    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    def test_views_document(self, mock_get_logged_in_user):
        request = mock.MagicMock()
        document_uuid = str(uuid4())
        request.matched_route.name = "add_document_to_case"
        request.method = "POST"
        request.json_body = {
            "document_uuid": document_uuid,
            "case_uuid": str(uuid4()),
            "document_label_uuids": [],
            "origin": None,
            "description": None,
            "orign_date": None,
        }

        user_info = mock.MagicMock()
        user_info.uuid = uuid4()
        mock_get_logged_in_user.return_value = user_info

        dl = document.DocumentViews(context="test", request=request)
        dl()

        request.route_url.assert_called_once_with(
            "get_document",
            _query={"document_uuid": document_uuid},
            _scheme="https",
        )

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    def test_assign_document_to_user(self, mock_get_logged_in_user):
        user_info = mock.MagicMock()
        user_info.uuid = uuid4()
        mock_get_logged_in_user.return_value = user_info

        document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.matched_route.name = "assign_document_to_user"
        request.method = "POST"
        request.json_body = {
            "document_uuid": document_uuid,
            "user_uuid": str(uuid4()),
        }

        document.DocumentViews(context="test", request=request)()

        request.route_url.assert_called_once_with(
            "get_document",
            _query={"document_uuid": document_uuid},
            _scheme="https",
        )

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    def test_assign_document_to_role(self, mock_get_logged_in_user):
        user_info = mock.MagicMock()
        user_info.uuid = uuid4()
        mock_get_logged_in_user.return_value = user_info

        document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.matched_route.name = "assign_document_to_role"
        request.method = "POST"
        request.json_body = {
            "document_uuid": document_uuid,
            "group_uuid": str(uuid4()),
            "role_uuid": str(uuid4()),
        }

        document.DocumentViews(context="test", request=request)()

        request.route_url.assert_called_once_with(
            "get_document",
            _query={"document_uuid": document_uuid},
            _scheme="https",
        )
