# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.2.3"

import logging
import minty
import minty_pyramid
import os
from .routes import routes
from minty.middleware import AmqpPublisherMiddleware
from minty_infra_sqlalchemy import (
    DatabaseTransactionMiddleware,
    DatabaseTransactionQueryMiddleware,
)
from zsnl_domains import document
from zsnl_perl_migration import (
    LegacyEventBroadcastMiddleware,
    LegacyEventQueueMiddleware,
)
from zsnl_pyramid import platform_key, session_invitation

ZS_COMPONENT = "zsnl_document_http"

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    return record


document_events = [
    ("zsnl_domains.document", "DocumentCreated"),
    ("zsnl_domains.document", "FileCreated"),
    ("zsnl_domains.document", "DocumentUpdated"),
]


def main(*args, **kwargs):
    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    loader = minty_pyramid.Engine(
        domains=[document],
        query_middleware=[DatabaseTransactionQueryMiddleware("database")],
        command_wrapper_middleware=[
            LegacyEventQueueMiddleware(
                database_infra_name="database",
                interesting_document_events=document_events,
            ),
            DatabaseTransactionMiddleware("database"),
            LegacyEventBroadcastMiddleware(
                amqp_infra_name="amqp",
                interesting_document_events=document_events,
            ),
            AmqpPublisherMiddleware(
                publisher_name="document", infrastructure_name="amqp"
            ),
        ],
    )
    config = loader.setup(*args, **kwargs)

    # Enable platform key access to this service
    config.include(platform_key)
    config.include(session_invitation)

    routes.add_routes(config)
    return loader.main()
