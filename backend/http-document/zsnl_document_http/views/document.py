# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class DocumentViews(JSONAPIEntityView):
    def create_link_from_entity(
        self, entity=None, entity_type=None, entity_id=None
    ):
        if entity_type == "document":
            return self.request.route_url(
                "get_document",
                _query={"document_uuid": entity_id},
                _scheme="https",
            )

    view_mapper = {
        "POST": {
            "add_document_to_case": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "add_document_to_case",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuid",
                "entity_type": "document",
            },
            "update_document": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "update_document",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuid",
                "entity_type": "document",
            },
            "assign_document_to_user": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "assign_document_to_user",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuid",
                "entity_type": "document",
            },
            "reject_assigned_document": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "reject_assigned_document",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuid",
                "entity_type": "document",
            },
            "assign_document_to_role": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "assign_document_to_role",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuid",
                "entity_type": "document",
            },
            "apply_labels_to_document": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "apply_labels_to_document",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuids",
                "entity_type": "document",
            },
            "remove_labels_from_document": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "remove_labels_from_document",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuids",
                "entity_type": "document",
            },
            "move_to_directory": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "move_to_directory",
                "from": {"json": {"_all": True}},
                "command_primary": "destination_directory_uuid",
                "entity_type": "document",
            },
            "accept_document": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "accept_document",
                "from": {"json": {"_all": True}},
                "command_primary": "document_uuid",
                "entity_type": "document",
            },
            "request_document_archive": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "request_document_archive",
                "from": {"json": {"_all": True}},
                "command_primary": "export_file_uuid",
                "entity_type": "document_archive",
                "generate_missing_id": "export_file_uuid",
            },
        },
    }
