# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class RegistrationFormViews(JSONAPIEntityView):
    def create_link_from_entity(
        self, entity=None, entity_type=None, entity_id=None
    ) -> str | None:
        if entity.entity_type == "registration_form_download":
            return self.request.route_path(
                "download_registration_form",
                _query={"case_uuid": entity.entity_id},
            )

    view_mapper = {
        "GET": {
            "get_registration_form": {
                "cq": "query",
                "auth_permissions": {"gebruiker", "pip_user", "anonymous"},
                "domain": "zsnl_domains.document",
                "run": "get_registration_form",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            },
            "download_registration_form": {
                "cq": "query",
                "auth_permissions": {"gebruiker", "pip_user", "anonymous"},
                "domain": "zsnl_domains.document",
                "run": "download_registration_form",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            },
        },
    }
