# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
import shutil
from minty.cqrs import UserInfo
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPForbidden,
    HTTPInternalServerError,
)
from pyramid.request import Request
from pyramid.response import Response
from redis import Redis
from tempfile import NamedTemporaryFile
from typing import IO, Any, cast
from uuid import uuid4
from zsnl_document_http.views import constants

logger = logging.getLogger(__name__)


@protected_route("gebruiker")
def create_document(request, user_info: UserInfo):
    """Create a document

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    if request.POST.get("document_uuid", "") == "":
        request.POST["document_uuid"] = str(uuid4())

    required_fields, document_file = _get_required_fields_for_create_document(
        request
    )

    if "skip_intake" not in request.POST and "case_uuid" not in request.POST:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": "At least one of `case_uuid` or `skip_intake`"
                        " is required"
                    }
                ]
            }
        )

    config = _get_upload_filestore_config(request)
    infra_type = _get_infrastructure_type(config=config)

    upload_infrastructure = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name=infra_type
    )

    result_params = upload_infrastructure.upload(
        file_handle=document_file.file, uuid=str(uuid4())
    )

    try:
        required_fields["case_uuid"] = request.POST["case_uuid"]
    except KeyError:
        pass

    required_fields["document_uuid"] = request.POST["document_uuid"]
    required_fields["store_uuid"] = str(result_params["uuid"])
    required_fields["filename"] = document_file.filename
    required_fields["mimetype"] = result_params["mime_type"]
    required_fields["storage_location"] = result_params["storage_location"]
    required_fields["size"] = result_params["size"]
    required_fields["md5"] = result_params["md5"]

    create_params = {
        **required_fields,
    }

    create_params["skip_intake"] = bool(request.POST.get("skip_intake", False))

    if "magic_string" in request.POST:
        create_params["magic_strings"] = request.POST.get(
            "magic_string"
        ).split(",")
    else:
        create_params["magic_strings"] = []

    if "directory_uuid" in request.POST:
        create_params["directory_uuid"] = request.POST["directory_uuid"]
    if "replaces" in request.POST:
        create_params["replaces"] = request.POST["replaces"]

    __add_bool_attribute_if_admin(
        request=request,
        user_info=user_info,
        attribute_name="accept_extended_mimetypes",
        attributes=create_params,
    )
    __add_bool_attribute_if_admin(
        request=request,
        user_info=user_info,
        attribute_name="auto_accept",
        attributes=create_params,
    )

    cmd = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.create_document(**create_params)

    return {"data": {"success": True}}


def __add_bool_attribute_if_admin(
    request, user_info: UserInfo, attribute_name: str, attributes: dict
) -> None:
    if attribute_name in request.POST:
        if user_info and user_info.permissions.get("admin"):
            attributes[attribute_name] = request.POST[
                attribute_name
            ].lower() in ["true", 1, "1"]
        else:
            raise HTTPForbidden(
                json={
                    "error": "auto_accept attribute is only supported for admin users."
                }
            )


def _get_required_fields_for_create_document(request):
    """Extract required fields from "case create" POST"""

    required_fields: dict[str, Any] = {"document_uuid": None}
    try:
        for field in required_fields:
            required_fields[field] = str(request.POST[field])
        document_file = request.POST["document_file"]
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    if not hasattr(document_file, "file"):
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "The 'document_file' field should be a file."}
                ]
            }
        )

    return required_fields, document_file


@protected_route("gebruiker", "pip_user")
def create_file(request: Request, user_info: UserInfo):
    """Create a document

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    try:
        file_content = request.POST["file"].file
        filename = request.POST["file"].filename
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    file_uuid = str(uuid4())
    config = request.infrastructure_factory.get_config(context=None)[
        "filestore"
    ]
    if isinstance(config, list):
        config = config[0]

    infra_type = _get_infrastructure_type(config=config)

    upload_infrastructure = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name=infra_type
    )
    upload_result = upload_infrastructure.upload(
        file_handle=file_content, uuid=file_uuid
    )

    cmd = request.get_command_instance(
        domain="zsnl_domains.document", user_uuid=user_info.user_uuid
    )

    cmd.create_file(
        file_uuid=file_uuid,
        filename=filename,
        mimetype=upload_result["mime_type"],
        storage_location=upload_result["storage_location"],
        size=upload_result["size"],
        md5=upload_result["md5"],
    )

    return {"data": {"type": "file", "id": str(file_uuid)}}


def _get_upload_filestore_config(request: Request):
    config = request.infrastructure_factory.get_config(context=None)[
        "filestore"
    ]

    if isinstance(config, list):
        config = config[0]

    return config


def _get_infrastructure_type(config: dict) -> str:
    """Returns the correct  infrastructure type."""
    infra_type = None
    if config.get("type", None) is None:
        infra_type = "swift"
    elif config["type"] == "s3Legacy":
        infra_type = "s3"
    else:
        infra_type = config["type"]
    return infra_type


def _serialize_document(document):
    meta = {
        "document_number": document.document_number,
        "last_modified_datetime": str(document.date_modified.isoformat()),
    }

    attributes = {
        "basename": document.basename,
        "extension": document.extension,
        "name": document.basename + document.extension,
        "mimetype": document.mimetype,
        "filesize": document.size,
        "md5": document.md5,
        "security": {"virus_status": document.virus_scan_status},
        "archive": {"is_archivable": document.is_archivable},
        "accepted": document.accepted,
        "current_version": document.current_version,
        "publish": document.publish,
        "labels": document.labels,
    }

    attributes["metadata"] = {
        "description": document.description,
        "confidentiality": document.confidentiality,
        "document_category": document.document_category,
        "origin": document.origin,
        "origin_date": document.origin_date.isoformat()
        if document.origin_date
        else None,
        "status": document.status,
        "pronom_format": document.pronom_format,
        "appearance": document.appearance,
        "structure": document.structure,
    }

    if document.integrity_check_successful is not None:
        attributes.update(
            {"integrity_check_successful": document.integrity_check_successful}
        )

    relationships = {
        "created_by": {
            "data": {"type": "subject", "id": str(document.creator_uuid)},
            "meta": {"display_name": document.creator_displayname},
        }
    }
    if document.case_uuid:
        relationships["case"] = {
            "data": {"type": "case", "id": str(document.case_uuid)},
            "meta": {"display_number": document.case_display_number},
        }
    if document.directory_uuid:
        relationships["directory"] = {
            "data": {"type": "directory", "id": str(document.directory_uuid)}
        }
    if document.store_uuid:
        relationships["file"] = {
            "data": {"type": "file", "id": str(document.store_uuid)}
        }

    return {
        "type": "document",
        "id": str(document.document_uuid),
        "meta": meta,
        "attributes": attributes,
        "relationships": relationships,
    }


@protected_route("gebruiker")
def get_document(request: Request, user_info: UserInfo):
    """Create a document

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    try:
        document_uuid = request.params["document_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    integrity_check = request.params.get("integrity") == "1"

    query_instance = request.get_query_instance(
        "zsnl_domains.document", user_info.user_uuid, user_info
    )

    document = query_instance.get_document_by_uuid(
        document_uuid=document_uuid, integrity_check=integrity_check
    )

    return {
        "meta": {"api_version": 2},
        "links": {"self": request.current_route_path()},
        "data": _serialize_document(document),
    }


@protected_route("gebruiker")
def create_document_from_attachment(request: Request, user_info: UserInfo):
    """Create a document from a file

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    try:
        attachment_uuid = request.json_body["attachment_uuid"]
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command = request.get_command_instance(
        domain="zsnl_domains.document", user_uuid=user_info.user_uuid
    )
    command.create_document_from_attachment(attachment_uuid=attachment_uuid)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def create_document_from_message(request: Request, user_info: UserInfo):
    """Create a document from an email file

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    try:
        message_uuid = request.json_body["message_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    output_format = request.json_body.get("output_format", "pdf")

    command = request.get_command_instance(
        domain="zsnl_domains.document", user_uuid=user_info.user_uuid
    )
    command.create_document_from_message(
        message_uuid=message_uuid, output_format=output_format
    )

    return {"data": {"success": True}}


@protected_route("gebruiker")
def search_document(request: Request, user_info: UserInfo):
    """Search document by filter.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    case_uuid = request.params.get("case_uuid")
    keyword = request.params.get("keyword")

    if not case_uuid and not keyword:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "Missing 'case_uuid' or 'keyword' for search"}
                ]
            }
        )

    query_instance = request.get_query_instance(
        "zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    documents = query_instance.search_document(
        case_uuid=case_uuid, keyword=keyword
    )
    data = []
    for document in documents:
        serialized_doc = _serialize_document(document)
        serialized_doc["links"] = {
            "self": f"/api/v2/document/get_document?document_uuid={document.document_uuid}"
        }
        data.append(serialized_doc)
    return {
        "meta": {"api_version": 2},
        "links": {"self": request.current_route_path()},
        "data": data,
    }


def _serialize_directory_entry(entry):
    attributes = {
        "name": entry.name,
        "entry_type": entry.entry_type,
        "description": entry.description,
        "extension": entry.extension,
        "mimetype": entry.mimetype,
        "accepted": entry.accepted,
        "last_modified_date_time": entry.last_modified_date_time.isoformat()
        if entry.last_modified_date_time
        else None,
        "document_number": entry.document_number,
        "rejection_reason": entry.rejection_reason
        if entry.rejection_reason
        else None,
        "rejected_by_display_name": entry.rejected_by_display_name
        if entry.rejection_reason
        else None,
        "assignment": entry.assignment,
    }
    relationships = {}
    links = {}

    if entry.case and entry.case["uuid"]:
        relationships["case"] = {
            "data": {"type": "case", "id": str(entry.case["uuid"])},
            "meta": {"display_number": entry.case["display_number"]},
        }

    if entry.parent and entry.parent["uuid"]:
        relationships["parent"] = {
            "data": {"type": "directory", "id": str(entry.parent["uuid"])},
            "meta": {"display_name": entry.parent["display_name"]},
        }

    if entry.modified_by and entry.modified_by["uuid"]:
        relationships["modified_by"] = {
            "data": {"type": "subject", "id": str(entry.modified_by["uuid"])},
            "meta": {"display_name": entry.modified_by["display_name"]},
        }

    if entry.document and entry.document["uuid"]:
        relationships["document"] = {
            "data": {"type": "document", "id": str(entry.document["uuid"])}
        }

        links["download"] = {
            "href": f"/api/v2/document/download_document?id={entry.uuid}"
        }

        if entry.document["preview_available"]:
            links["preview"] = {
                "href": f"/api/v2/document/preview_document?id={entry.uuid}"
            }
            if entry.mimetype in constants.IMAGE_MIMETYPES:
                links["preview"]["meta"] = {"content-type": entry.mimetype}
            else:
                links["preview"]["meta"] = {
                    "content-type": constants.PDF_MIMETYPE
                }

        if entry.document.get("thumbnail_uuid", None):
            links["thumbnail"] = {
                "href": f"/api/v2/document/thumbnail_document?id={entry.uuid}",
                "meta": {"content-type": entry.document["thumbnail_mimetype"]},
            }

    if entry.directory and entry.directory["uuid"]:
        relationships["directory"] = {
            "data": {"type": "directory", "id": str(entry.directory["uuid"])}
        }

    return {
        "links": links,
        "type": entry.type,
        "id": str(entry.uuid),
        "attributes": attributes,
        "relationships": relationships,
    }


def _serialize_directory(directory):
    relationships = {}
    if directory.parent:
        relationships["parent"] = {
            "data": {"type": "directory", "id": directory.parent["uuid"]}
        }
    return {
        "id": str(directory.uuid),
        "type": "directory",
        "attributes": {"name": directory.name},
        "relationships": relationships,
    }


@protected_route("gebruiker", "pip_user")
def get_directory_entries_for_case(request: Request, user_info: UserInfo):
    """Get directory the user has access to. Filter the result based on case_uuid etc.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.params["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    for key in request.params:
        if key not in [
            "case_uuid",
            "filter[relationships.parent.id]",
            "filter[fulltext]",
            "no_empty_folders",
        ]:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Invalid parameter '{key}'"}]}
            )

    directory_uuid = request.params.get("filter[relationships.parent.id]")
    search_term = request.params.get("filter[fulltext]")
    no_empty_folders = request.params.get("no_empty_folders", "1")

    if no_empty_folders not in ["0", "1"]:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": f"Invalid value '{no_empty_folders}' for parameter 'no_empty_folders'"
                    }
                ]
            }
        )
    query = request.get_query_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    document_entries = query.get_directory_entries_for_case(
        case_uuid=case_uuid,
        directory_uuid=directory_uuid,
        search_term=search_term,
        no_empty_folders=no_empty_folders == "1",
    )
    serialized_data = [
        _serialize_directory_entry(entry) for entry in document_entries
    ]

    included = []
    if directory_uuid and directory_uuid != "\u0000":
        parent_directories = query.get_parent_directories_for_directory(
            directory_uuid=directory_uuid
        )
        included = [
            _serialize_directory(directory) for directory in parent_directories
        ]

    return {
        "links": {"self": request.current_route_path()},
        "meta": {"api_version": 2},
        "data": serialized_data,
        "included": included,
    }


@protected_route("gebruiker", "pip_user")
def create_document_from_file(request: Request, user_info: UserInfo):
    """Create a document from file for a specific case.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    try:
        file_uuid = request.json_body["file_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    case_uuid = request.json_body.get("case_uuid")
    skip_intake = request.json_body.get("skip_intake", False)
    document_uuid = request.json_body.get("document_uuid", str(uuid4()))
    directory_uuid = request.json_body.get("directory_uuid")
    magic_strings = request.json_body.get("magic_string")

    command = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    command.create_document_from_file(
        document_uuid=document_uuid,
        file_uuid=file_uuid,
        case_uuid=case_uuid,
        directory_uuid=directory_uuid,
        magic_strings=magic_strings,
        skip_intake=skip_intake,
    )

    return {"data": {"success": True}}


@protected_route("gebruiker", "pip_user")
def download_document(request: Request, user_info: UserInfo):
    """Request a download url for given document uuid.
    Response will contain re-direct(302) and url."""

    try:
        attachment_uuid = request.params["id"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"error": f"Missing parameter {error}"}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    file_download_url = query_instance.get_document_download_link(
        document_uuid=attachment_uuid
    )

    return Response(status_int=302, location=file_download_url)


@protected_route("gebruiker")
def get_directory_entries_for_intake(request: Request, user_info: UserInfo):
    """Get documents for document intake as directory_entries.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """
    _assert_parameter_names(
        request.params,
        [
            "filter[fulltext]",
            "filter[attributes.assignment.is_set]",
            "filter[attributes.assignment.is_self]",
            "page",
            "page_size",
            "sort",
        ],
    )
    raw_page = request.params.get("page", "1")
    raw_page_size = request.params.get("page_size", "100")
    (page, page_size) = _check_paging_limits(
        page=raw_page, page_size=raw_page_size, max_results=10000
    )

    get_dictionary_entries_params = {}
    get_dictionary_entries_params["page"] = page
    get_dictionary_entries_params["page_size"] = page_size
    get_dictionary_entries_params["sort"] = request.params.get("sort")
    get_dictionary_entries_params["search_term"] = request.params.get(
        "filter[fulltext]"
    )

    assigned = request.params.get("filter[attributes.assignment.is_set]")
    if assigned == "true":
        get_dictionary_entries_params["assigned"] = True
    elif assigned == "false":
        get_dictionary_entries_params["assigned"] = False
    elif assigned is not None:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": f"Invalid value {assigned} for 'filter[attributes.assignment.is_set]'"
                    }
                ]
            }
        )

    assigned_to_self = request.params.get(
        "filter[attributes.assignment.is_self]"
    )
    if assigned_to_self == "true":
        get_dictionary_entries_params["assigned_to_self"] = True

    query = request.get_query_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    document_entries = query.get_directory_entries_for_intake(
        **get_dictionary_entries_params
    )
    serialized_data = [
        _serialize_directory_entry(entry) for entry in document_entries
    ]

    return {
        "links": {"self": request.current_route_path()},
        "meta": {"api_version": 2},
        "data": serialized_data,
    }


@protected_route("gebruiker")
def preview_document(request: Request, user_info: UserInfo):
    try:
        document_uuid = request.params["id"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"error": f"Missing parameter {error}"}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.document", user_info.user_uuid, user_info=user_info
    )

    preview_url = query_instance.get_document_preview_link(
        document_uuid=document_uuid
    )
    return Response(status_int=302, location=preview_url)


@protected_route("gebruiker")
def delete_document(request: Request, user_info: UserInfo):
    try:
        document_uuid = request.json_body["document_uuid"]
    except KeyError as e:
        raise HTTPBadRequest(
            json={"error": f"Missing required arguments: {e}"}
        ) from e

    try:
        reason = request.json_body["reason"]
    except KeyError:
        reason = ""

    command = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    command.delete_document(
        user_info=user_info, document_uuid=document_uuid, reason=reason
    )

    return {"data": {"success": True}}


@protected_route("gebruiker")
def create_directory(request: Request, user_info: UserInfo):
    required_parameters = ["case_uuid", "name"]
    missing_parameters = list()

    for req_par in required_parameters:
        if req_par not in request.json_body:
            missing_parameters.append(req_par)

    if len(missing_parameters) > 0:
        raise HTTPBadRequest(
            json={"error": f"Missing required arguments: {missing_parameters}"}
        )

    case_uuid = request.json_body["case_uuid"]
    name = request.json_body["name"]

    if "parent_uuid" in request.json_body:
        parent_uuid = request.json_body["parent_uuid"]
    else:
        parent_uuid = ""

    if "directory_uuid" in request.json_body:
        directory_uuid = request.json_body["directory_uuid"]
    else:
        directory_uuid = str(uuid4())

    command = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    command.create_directory(
        directory_uuid=directory_uuid,
        case_uuid=case_uuid,
        name=name,
        parent_uuid=parent_uuid,
    )

    return {"data": {"directory_uuid": directory_uuid}}


@protected_route("gebruiker")
def thumbnail_document(request: Request, user_info: UserInfo):
    try:
        document_uuid = request.params["id"]

    except KeyError as error:
        raise HTTPBadRequest(
            json={"error": f"Missing parameter {error}"}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.document", user_info.user_uuid, user_info
    )

    thumbnail_url = query_instance.get_document_thumbnail_link(
        document_uuid=document_uuid
    )
    return Response(status_int=302, location=thumbnail_url)


@protected_route("gebruiker")
def edit_document_online(request: Request, user_info: UserInfo):
    """Get edit document url

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: url
    :rtype: string
    """

    try:
        doc_uuid = request.params["id"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"error": [{"title": f"Missing parameter {error}"}]}
        ) from error

    editor_type = request.params.get("editor_type", "zoho")
    query_instance = request.get_query_instance(
        "zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    save_url = request.route_url(
        "update_document_from_external_editor", _scheme="https"
    )
    lock_acquire_url = request.route_url(
        "acquire_document_lock_for_wopi", _scheme="https"
    )
    lock_extend_url = request.route_url(
        "extend_document_lock_for_wopi", _scheme="https"
    )
    lock_release_url = request.route_url(
        "release_document_lock_for_wopi", _scheme="https"
    )

    if editor_type == "msonline":
        try:
            close_url = request.params["close_url"]
            host_page_url = request.params["host_page_url"]
        except KeyError as e:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Missing parameter {e}"}]}
            ) from e
        if not close_url:
            raise HTTPBadRequest(
                json={"errors": [{"title": "Empty 'close_url'"}]}
            )
        if not host_page_url:
            raise HTTPBadRequest(
                json={"errors": [{"title": "Empty 'host_page_url'"}]}
            )

        wopi_config = query_instance.get_ms_wopi_configuration(
            document_uuid=doc_uuid,
            user_info=user_info,
            save_url=save_url,
            close_url=close_url,
            host_page_url=host_page_url,
            context="https://" + request.host,
            lock_acquire_url=lock_acquire_url,
            lock_release_url=lock_release_url,
            lock_extend_url=lock_extend_url,
        )

        return wopi_config

    url = query_instance.get_edit_document_url(
        document_uuid=doc_uuid, user_info=user_info, save_url=save_url
    )
    return Response(status_int=302, location=url)


def create_document_with_token(request: Request):
    """
    Callback to create a document in a case using a single-use token
    """

    try:
        file_handle: IO[bytes] = request.POST.getone("file").file
        token: str = request.POST.getone("token")

        if len(token) > 100 or (not token.isalnum()):
            raise ValueError(token)

    except (KeyError, AttributeError, ValueError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter: '{error}'"}]}
        ) from error

    redis_infra: Redis = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )
    configuration = request.infrastructure_factory.get_config(request.host)
    instance_identifier = configuration["instance_uuid"]

    token_key = f"{instance_identifier}:tokens:{token}"

    token_data_raw = cast(bytes | None, redis_infra.getdel(token_key))

    if not token_data_raw:
        raise HTTPBadRequest(json={"errors": [{"title": "Token error"}]})

    try:
        token_data = json.loads(token_data_raw.decode("utf-8"))
    except (json.JSONDecodeError, UnicodeDecodeError) as err:
        raise HTTPInternalServerError(
            json={"errors": [{"title": "Token error"}]}
        ) from err

    user_info = UserInfo.from_dict(token_data["user_info"])

    cmd = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    if token_data["type"] == "registration_form":
        with NamedTemporaryFile("wb") as output_file:
            shutil.copyfileobj(file_handle, output_file)
            output_file.flush()

            cmd.upload_registration_form(
                local_filename=output_file.name,
                magic_string=token_data["magic_string"],
                case_uuid=token_data["case_uuid"],
                name=token_data["filename"],
            )
    else:
        raise HTTPInternalServerError(
            json={"errors": [{"title": "Unsupported token found"}]}
        )

    return {"data": {"success": True}}


def release_document_lock_for_wopi(request: Request):
    try:
        document_uuid = request.json_body["document_uuid"]
        user_uuid = request.json_body["user_uuid"]
        authentication_token = request.json_body["authentication_token"]
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    redis_infra = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )
    configuration = request.infrastructure_factory.get_config(request.host)
    shortname = configuration["instance_uuid"]
    redis_key = f"msonline:edit_document_online:{shortname}:{document_uuid}:{user_uuid}"
    try:
        redis_value = redis_infra.get(redis_key).decode("utf-8")
        redis_data = json.loads(redis_value)
    except (TypeError, AttributeError):
        redis_data = None
    if redis_data is None:
        raise HTTPForbidden(json={"error": "Unknown security token"})

    redis_token = redis_data["token"]
    if redis_token != authentication_token:
        raise HTTPForbidden(json={"error": "Failed to update document"})

    user_info = UserInfo(
        user_uuid=redis_data["user_uuid"],
        permissions=redis_data["user_permissions"],
    )

    params = {"document_uuid": document_uuid, "user_uuid": user_uuid}
    cmd = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_uuid,
        user_info=user_info,
    )
    cmd.release_document_lock_for_wopi(**params)
    redis_infra.delete(redis_key)


def extend_document_lock_for_wopi(request: Request):
    try:
        document_uuid = request.json_body["document_uuid"]
        user_uuid = request.json_body["user_uuid"]
        authentication_token = request.json_body["authentication_token"]
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    redis_infra = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )
    configuration = request.infrastructure_factory.get_config(request.host)
    shortname = configuration["instance_uuid"]
    redis_key = f"msonline:edit_document_online:{shortname}:{document_uuid}:{user_uuid}"

    try:
        redis_value = redis_infra.get(redis_key).decode("utf-8")
        redis_data = json.loads(redis_value)
    except (TypeError, AttributeError):
        redis_data = None
    if redis_data is None:
        raise HTTPForbidden(json={"error": "Unknown security token"})

    redis_token = redis_data["token"]
    if redis_token != authentication_token:
        raise HTTPForbidden(json={"error": "Failed to update document"})

    user_info = UserInfo(
        user_uuid=redis_data["user_uuid"],
        permissions=redis_data["user_permissions"],
    )

    params = {
        "document_uuid": document_uuid,
        "user_uuid": user_uuid,
    }
    if "lock_seconds" in request.json_body:
        params["lock_seconds"] = request.json_body["lock_seconds"]

    cmd = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_uuid,
        user_info=user_info,
    )
    cmd.extend_document_lock_for_wopi(**params)
    redis_infra.expire(redis_key, 3600)


def acquire_document_lock_for_wopi(request: Request):
    try:
        document_uuid = request.json_body["document_uuid"]
        user_uuid = request.json_body["user_uuid"]
        user_display_name = request.json_body["user_display_name"]
        authentication_token = request.json_body["authentication_token"]
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    redis_infra = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )
    configuration = request.infrastructure_factory.get_config(request.host)
    shortname = configuration["instance_uuid"]
    redis_key = f"msonline:edit_document_online:{shortname}:{document_uuid}:{user_uuid}"
    try:
        redis_value = redis_infra.get(redis_key).decode("utf-8")
        redis_data = json.loads(redis_value)
    except (TypeError, AttributeError):
        redis_data = None
    if redis_data is None:
        raise HTTPForbidden(json={"error": "Unknown security token"})

    redis_token = redis_data["token"]
    if redis_token != authentication_token:
        raise HTTPForbidden(json={"error": "Failed to update document"})

    params = {
        "document_uuid": document_uuid,
        "user_uuid": user_uuid,
        "user_display_name": user_display_name,
    }
    if "lock_seconds" in request.json_body:
        params["lock_seconds"] = request.json_body["lock_seconds"]

    user_info = UserInfo(
        user_uuid=redis_data["user_uuid"],
        permissions=redis_data["user_permissions"],
    )

    cmd = request.get_command_instance(
        domain="zsnl_domains.document",
        user_uuid=user_uuid,
        user_info=user_info,
    )
    cmd.acquire_document_lock_for_wopi(**params)


def update_document_from_external_editor(request: Request):
    """Create a document from external editor

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :return: response
    :rtype: dict
    """

    try:
        file_handle = request.POST["content"].file
        file_name = request.POST["content"].filename
        document_uuid = request.POST["document_uuid"]
        editor_type = request.POST.get("editor_type", "zoho")
        authentication_token = request.POST["authentication_token"]
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    redis_infra = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name="redis"
    )
    configuration = request.infrastructure_factory.get_config(request.host)
    shortname = configuration["instance_uuid"]
    user_uuid = None
    if editor_type == "zoho":
        redis_key = f"zoho_authentication_token:edit_document_online:{shortname}:{document_uuid}"
    elif editor_type == "msonline":
        user_uuid = request.POST["user_uuid"]
        redis_key = f"msonline:edit_document_online:{shortname}:{document_uuid}:{user_uuid}"
    else:
        raise HTTPForbidden(
            json={
                "errors": [
                    {"title": f"Editor '{editor_type}' is not supported."}
                ]
            }
        )

    try:
        redis_value = redis_infra.get(redis_key).decode("utf-8")
        redis_data = json.loads(redis_value)
    except (TypeError, AttributeError):
        redis_data = None
    if redis_data is None:
        raise HTTPForbidden(json={"error": "Unknown security token"})

    user_uuid = user_uuid or redis_data["user_uuid"]
    case_uuid = redis_data["case_uuid"]
    redis_token = redis_data["token"]

    if redis_token != authentication_token:
        raise HTTPForbidden(json={"error": "Failed to update document"})

    config = request.infrastructure_factory.get_config(context=None)[
        "filestore"
    ]

    if isinstance(config, list):
        config = config[0]

    infra_type = _get_infrastructure_type(config=config)

    upload_infrastructure = request.infrastructure_factory.get_infrastructure(
        context=request.host, infrastructure_name=infra_type
    )
    result_params = upload_infrastructure.upload(
        file_handle=file_handle, uuid=str(uuid4())
    )
    required_fields = {}
    required_fields["case_uuid"] = case_uuid
    required_fields["document_uuid"] = document_uuid
    required_fields["store_uuid"] = str(result_params["uuid"])
    required_fields["filename"] = file_name
    required_fields["mimetype"] = result_params["mime_type"]
    required_fields["storage_location"] = result_params["storage_location"]
    required_fields["size"] = result_params["size"]
    required_fields["md5"] = result_params["md5"]
    required_fields["editor_update"] = True

    create_params = {
        **required_fields,
        "directory_uuid": request.POST.get("directory_uuid", None),
        "magic_strings": request.POST.getall("magic_string"),
        "skip_intake": True,
        "auto_accept": True,
    }
    cmd = request.get_command_instance(
        domain="zsnl_domains.document", user_uuid=user_uuid
    )
    cmd.create_document(**create_params)

    return (
        _get_new_version_and_url_for_document(
            request, document_uuid, redis_data
        )
        if editor_type == "msonline"
        else "Succes"
    )


def _get_new_version_and_url_for_document(request, document_uuid, redis_data):
    """Afther update return new version and new_file_url to Wopi proxy service."""
    user_info = UserInfo(
        user_uuid=redis_data["user_uuid"],
        permissions=redis_data["user_permissions"],
    )
    query_instance = request.get_query_instance(
        "zsnl_domains.document", user_info.user_uuid, user_info
    )

    new_version = query_instance.get_document_by_uuid(
        document_uuid=document_uuid
    ).current_version

    new_file_url = query_instance.get_document_download_link(
        document_uuid=document_uuid
    )
    return {"new_version": str(new_version), "new_file_url": new_file_url}


def _assert_parameter_names(request_params, allowed_params: list[str]):
    for param in request_params:
        if param not in allowed_params:
            raise HTTPBadRequest(
                json={
                    "errors": [
                        {"title": f"Unsupported parameter parameter '{param}'"}
                    ]
                }
            )


def _check_paging_limits(page, page_size, max_results):
    try:
        page = int(page)
    except ValueError as error:
        raise HTTPBadRequest(
            json={
                "errors": [{"title": "Parameter page is not a valid integer"}]
            }
        ) from error

    try:
        page_size = int(page_size)
    except ValueError as error:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "Parameter page_size is not a valid integer"}
                ]
            }
        ) from error
    if page < 1:
        raise HTTPBadRequest(
            json={"errors": [{"title": "'page' must be >= 1"}]}
        )

    if page_size < 1:
        raise HTTPBadRequest(
            json={"errors": [{"title": "'page_size' must be >= 1"}]}
        )

    if page_size > max_results:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": f"Requested page size {page_size} > max size {max_results}."
                    }
                ]
            }
        )

    return (page, page_size)
