# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# Force rebuild for Perl 5.38.2
FROM perl:5.40-threaded-bookworm AS production

ARG BUILD_TIMESTAMP=undefined

WORKDIR /usr/src/converter
COPY backend/http-documentconverter/cpanwrapper .
COPY backend/http-documentconverter/cpanfile /usr/src/converter/cpanfile

RUN useradd -ms /bin/bash converter -d /home/converter

RUN    echo "deb [signed-by=/usr/share/keyrings/debian-archive-keyring.gpg] http://deb.debian.org/debian bookworm contrib non-free" \
    > /etc/apt/sources.list.d/non-free.list \
    && echo "deb [signed-by=/usr/share/keyrings/debian-archive-keyring.gpg] http://deb.debian.org/debian bookworm-backports main contrib non-free" \
    > /etc/apt/sources.list.d/backports.list \
    && apt-get update \
    && apt-get -y upgrade \
    && apt-get -y --no-install-recommends install \
    cmake \
    libpng-dev libtiff-dev libgif-dev libjpeg-dev libmagic-dev \
    libheif-dev \
    libgirepository1.0-dev libcairo2-dev libpoppler-glib-dev \
    fonts-crosextra-caladea fonts-crosextra-carlito fonts-croscore \
    fonts-freefont-ttf fonts-liberation2 fonts-ocr-a fonts-ocr-b \
    fonts-dejavu fonts-texgyre fonts-lato \
    fonts-roboto \
    xfonts-scalable \
    ttf-mscorefonts-installer \
    unoconv libreoffice-writer libreoffice-calc libreoffice-impress \
    xvfb xauth libgl1-mesa-dri \
    wkhtmltopdf poppler-utils \
    # https://rt.cpan.org/Ticket/Display.html?id=133641
    && ./cpanwrapper --notest Glib::Object::Introspection \
    && ./cpanwrapper --notest --installdeps . \
    # Mark libs installed by -dev as manual install so autoremove doesn't
    # delete them
    &&  apt-mark manual libpng16-16 libtiff6 libgif7 libjpeg62-turbo \
    libheif1 \
    libmagic1 libgirepository-1.0-1 libcairo2 \
    libpoppler-glib8 libcairo-gobject2 gobject-introspection \
    libgirepository-1.0-1 \
    gobject-introspection \
    gir1.2-glib-2.0 \
    gir1.2-freedesktop \
    && apt-get purge -y \
    cmake \
    libpng-dev \
    libtiff-dev \
    libgif-dev \
    libjpeg-dev \
    libmagic-dev \
    libheif-dev \
    libgirepository1.0-dev \
    libcairo2-dev \
    libpoppler-glib-dev \
    && apt-get autoremove --purge -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN echo "$BUILD_TIMESTAMP" > /build-timestamp

COPY backend/http-documentconverter /usr/src/converter

EXPOSE 5032

USER converter
CMD [ "bin/run_starman.pl", "--preload-app", "--port", "5032", "bin/converter-service.pl" ]
