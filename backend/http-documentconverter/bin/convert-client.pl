#! /usr/bin/env perl

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

use warnings;
use strict;
use autodie;
use v5.22;

use File::Slurp qw(read_file write_file);
use Getopt::Long;
use JSON::MaybeXS;
use LWP::UserAgent;
use MIME::Base64;

my ($debug, $source_file, $dest_file, $dest_type);
GetOptions (
    "debug"           => \$debug,
    "source_file|sf=s" => \$source_file,
    "dest_file|df=s"  => \$dest_file,
    "dest_type|dt=s"  => \$dest_type,
);

die "Required argument --source_file=/--sf= not specified.\n" unless $source_file;
die "Required argument --dest_file=/--df= not specified.\n" unless $dest_file;
die "Required argument --dest_type=/--dt= not specified.\n" unless $dest_type;

my $ua = LWP::UserAgent->new();
if ($debug) {
    require LWP::ConsoleLogger::Easy;
    LWP::ConsoleLogger::Easy::debug_ua($ua);
}

my $source_data = read_file($source_file, binmode => ':raw');

my $response = $ua->post(
    'http://localhost:5032/v1/convert',
    Content_Type => 'application/json',
    Content => encode_json(
        {
            to_type => $dest_type,
            content => encode_base64($source_data),
            options => {},
        }
    )
);

my $rv = decode_json($response->decoded_content);
if ($response->is_success) {
    write_file($dest_file, decode_base64($rv->{content}));
}
else {
    print STDERR $rv->{error} . "\n";
}
