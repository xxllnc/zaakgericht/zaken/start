#!/usr/bin/env perl

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";

use ConvertService::Web;
use File::Copy;
use File::Path qw(make_path);
use Plack::Builder;

# Drop a default LibreOffice configuration:
my $lo_config_dir = "$ENV{HOME}/.config/libreoffice/4/user";
if (!-d $lo_config_dir) {
    make_path($lo_config_dir);

    copy("etc/registrymodifications.xcu", "$lo_config_dir/registrymodifications.xcu")
        or die "Could not set LibreOffice defaults: $!";
}

builder {
    ConvertService::Web->to_app;
}
