#! /bin/bash
# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# Script to run Unoconv
# 
# It first kills any old LibreOffice instances that are still running,
# because sometimes LO "hangs".
#
# There should never be more than one instance of this running at a time
# (e.g. using `flock`), or a running LibreOffice instance that's actively
# converting a file may be killed.

set -eu

pkill -9 soffice.bin || true

exec "$@"
