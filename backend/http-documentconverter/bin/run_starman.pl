#! /usr/bin/env perl

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# This needs to run its "INIT" - including it from the converter plugin/app is too late.
use Glib::Object::Introspection;

if ($ENV{STARMAN_WORKERS}) {
    unshift(@ARGV, ('--workers' => $ENV{STARMAN_WORKERS}));
}

# Run starman as usual
require '/usr/local/bin/starman';
