# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

package ConvertService::Plugin;
use Moo::Role;

requires 'name', 'can_convert', 'convert';

1;
