#! /usr/bin/env perl

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

use warnings;
use strict;

use Test::More;

use ConvertService::Plugin::Imager;
use FindBin;
use Image::ExifTool qw(ImageInfo);

my $csi = ConvertService::Plugin::Imager->new();

for my $source (qw(jpeg png gif bmp tiff x-portable-pixmap)) {
    for my $dest (qw(jpeg png gif tiff)) {
        ok(
             $csi->can_convert(from => "image/$source", to => "image/$dest"),
            "Can convert from image/$source to image/$dest"
        );
        ok(
            !$csi->can_convert(from => "application/pdf", to => "image/$dest"),
            "Cannot convert from application/pdf to image/$dest"
        );
        ok(
            !$csi->can_convert(from => "image/$source", to => "video/mp4"),
            "Cannot convert from image/$source to video/mp4"
        );
    }

    ok(
        $csi->can_convert(from => "image/$source", to => "application/pdf"),
        "Can convert from image/$source to application/pdf"
    );
}

# "Normal" image conversion
{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.bmp",
        from_type   => 'image/bmp',
        to_type     => 'image/gif',
        options     => {},
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');

    is($info->{MIMEType}, 'image/gif', 'Conversion to GIF succeeded');

    is($info->{ImageWidth},  640, 'Image width not scaled, still 640');
    is($info->{ImageHeight}, 480, 'Image height not scaled, still 480');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.tif",
        from_type   => 'image/tiff',
        to_type     => 'image/png',
        options     => { width => 320, height => 200 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');

    is($info->{MIMEType}, 'image/png', 'Conversion to PNG succeeded');

    is($info->{ImageWidth},  267, 'Image width scaled to 267 (best fit)');
    is($info->{ImageHeight}, 200, 'Image height scaled to 200 (best fit)');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.tif",
        from_type   => 'image/tiff',
        to_type     => 'image/png',
        options     => { width => 200, height => 320 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');

    is($info->{MIMEType}, 'image/png', 'Conversion to PNG succeeded');

    is($info->{ImageWidth},  200, 'Image width scaled to 267 (best fit; different orientation)');
    is($info->{ImageHeight}, 150, 'Image height scaled to 150 (best fit; different orientation)');
}

# Image to PDF conversion
{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.tif",
        from_type   => 'image/tiff',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'TIF to PDF conversion succeeded');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.tif",
        from_type   => 'image/tiff',
        to_type     => 'application/pdf',
        options     => { width => 320, height => 240 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'TIF to PDF conversion with resize succeeded');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.bmp",
        from_type   => 'image/bmp',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'BMP to PDF conversion succeeded');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.bmp",
        from_type   => 'image/bmp',
        to_type     => 'application/pdf',
        options     => { width => 320, height => 240 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'BMP to PDF conversion with resize succeeded');
}

done_testing();
