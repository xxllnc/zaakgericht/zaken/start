{
  "components": {
    "schemas": {
      "DefGeoFieldTypesEnum": {
        "description": "An enumeration.",
        "enum": [
          "geojson",
          "address_v2",
          "relationship"
        ],
        "title": "DefGeoFieldTypesEnum",
        "type": "string"
      },
      "DefMapMagicString": {
        "description": "Introspectable object based on pydantic",
        "properties": {
          "field_label": {
            "title": "Label of the custom field",
            "type": "string"
          },
          "field_type": {
            "allOf": [
              {
                "$ref": "#/components/schemas/DefGeoFieldTypesEnum"
              }
            ],
            "title": "Type of custom field; used to determine how to use the value"
          },
          "magic_string": {
            "title": "Magic string of the custom field",
            "type": "string"
          }
        },
        "required": [
          "field_type",
          "magic_string",
          "field_label"
        ],
        "title": "DefMapMagicString",
        "type": "object"
      },
      "EntityCase": {
        "properties": {
          "data": {
            "description": "Entity representing a case as seen by the geo domain.\n\nIt is very minimal on purpose, as the \"actual\" case is part of the\ncase-management domain; this only has the UUID as and a list of magic\nstrings that should be represented on the map.",
            "properties": {
              "attributes": {
                "properties": {
                  "assignee": {
                    "title": "Assignee of the case",
                    "type": "string"
                  },
                  "family": {
                    "title": "Family for this entity is case",
                    "type": "string"
                  },
                  "map_magic_strings": {
                    "items": {
                      "$ref": "#/components/schemas/DefMapMagicString"
                    },
                    "title": "List of magic strings of custom fields that should be shown on the map",
                    "type": "array"
                  },
                  "requestor": {
                    "title": "Requestor of the case",
                    "type": "string"
                  },
                  "requestor_uuid": {
                    "format": "uuid",
                    "title": "UUID of requestor of the case",
                    "type": "string"
                  },
                  "status": {
                    "title": "Status of the case",
                    "type": "string"
                  },
                  "subtitle": {
                    "title": "Subtitle is the extra information for the case",
                    "type": "string"
                  },
                  "title": {
                    "title": "Id of the case",
                    "type": "string"
                  },
                  "type": {
                    "title": "Type of the case",
                    "type": "string"
                  },
                  "use_geojson_address": {
                    "title": "Flag that indicates whether the geo-data of the requestor needs to be linked",
                    "type": "boolean"
                  }
                },
                "required": [
                  "map_magic_strings",
                  "family",
                  "status",
                  "assignee",
                  "title",
                  "subtitle",
                  "type",
                  "requestor",
                  "use_geojson_address"
                ],
                "type": "object"
              },
              "id": {
                "format": "uuid",
                "type": "string"
              },
              "links": {
                "properties": {
                  "self": {
                    "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                    "pattern": "^/api/v2/.*$",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "meta": {
                "properties": {
                  "summary": {
                    "description": "Human readable summary of the content of the object",
                    "title": "Summary of the subject",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "relationships": {
                "properties": {},
                "type": "object"
              },
              "type": {
                "enum": [
                  "case"
                ],
                "example": "case",
                "type": "string"
              }
            },
            "required": [
              "type",
              "id",
              "attributes"
            ],
            "type": "object"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityCaseList": {
        "properties": {
          "data": {
            "items": {
              "description": "Entity representing a case as seen by the geo domain.\n\nIt is very minimal on purpose, as the \"actual\" case is part of the\ncase-management domain; this only has the UUID as and a list of magic\nstrings that should be represented on the map.",
              "properties": {
                "attributes": {
                  "properties": {
                    "assignee": {
                      "title": "Assignee of the case",
                      "type": "string"
                    },
                    "family": {
                      "title": "Family for this entity is case",
                      "type": "string"
                    },
                    "map_magic_strings": {
                      "items": {
                        "$ref": "#/components/schemas/DefMapMagicString"
                      },
                      "title": "List of magic strings of custom fields that should be shown on the map",
                      "type": "array"
                    },
                    "requestor": {
                      "title": "Requestor of the case",
                      "type": "string"
                    },
                    "requestor_uuid": {
                      "format": "uuid",
                      "title": "UUID of requestor of the case",
                      "type": "string"
                    },
                    "status": {
                      "title": "Status of the case",
                      "type": "string"
                    },
                    "subtitle": {
                      "title": "Subtitle is the extra information for the case",
                      "type": "string"
                    },
                    "title": {
                      "title": "Id of the case",
                      "type": "string"
                    },
                    "type": {
                      "title": "Type of the case",
                      "type": "string"
                    },
                    "use_geojson_address": {
                      "title": "Flag that indicates whether the geo-data of the requestor needs to be linked",
                      "type": "boolean"
                    }
                  },
                  "required": [
                    "map_magic_strings",
                    "family",
                    "status",
                    "assignee",
                    "title",
                    "subtitle",
                    "type",
                    "requestor",
                    "use_geojson_address"
                  ],
                  "type": "object"
                },
                "id": {
                  "format": "uuid",
                  "type": "string"
                },
                "links": {
                  "properties": {
                    "self": {
                      "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                      "pattern": "^/api/v2/.*$",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "meta": {
                  "properties": {
                    "summary": {
                      "description": "Human readable summary of the content of the object",
                      "title": "Summary of the subject",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "relationships": {
                  "properties": {},
                  "type": "object"
                },
                "type": {
                  "enum": [
                    "case"
                  ],
                  "example": "case",
                  "type": "string"
                }
              },
              "required": [
                "type",
                "id",
                "attributes"
              ],
              "type": "object"
            },
            "type": "array"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          },
          "meta": {
            "properties": {
              "total_results": {
                "type": "integer"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityContact": {
        "properties": {
          "data": {
            "description": "Entity representing a contact as seen by the geo domain.\n\nIt is very minimal and only contains the information needed to store in the\ngeo domain.",
            "properties": {
              "attributes": {
                "properties": {
                  "address": {
                    "title": "Full address of the contact",
                    "type": "string"
                  },
                  "name": {
                    "title": "Display-name of the contact",
                    "type": "string"
                  },
                  "status": {
                    "title": "Status of the contact (active or inactive)",
                    "type": "string"
                  },
                  "type": {
                    "title": "Type of contact (person, organization)",
                    "type": "string"
                  }
                },
                "required": [
                  "name",
                  "address",
                  "type"
                ],
                "type": "object"
              },
              "id": {
                "format": "uuid",
                "type": "string"
              },
              "links": {
                "properties": {
                  "self": {
                    "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                    "pattern": "^/api/v2/.*$",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "meta": {
                "properties": {
                  "summary": {
                    "description": "Human readable summary of the content of the object",
                    "title": "Summary of the subject",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "relationships": {
                "properties": {},
                "type": "object"
              },
              "type": {
                "enum": [
                  "contact"
                ],
                "example": "contact",
                "type": "string"
              }
            },
            "required": [
              "type",
              "id",
              "attributes"
            ],
            "type": "object"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityContactList": {
        "properties": {
          "data": {
            "items": {
              "description": "Entity representing a contact as seen by the geo domain.\n\nIt is very minimal and only contains the information needed to store in the\ngeo domain.",
              "properties": {
                "attributes": {
                  "properties": {
                    "address": {
                      "title": "Full address of the contact",
                      "type": "string"
                    },
                    "name": {
                      "title": "Display-name of the contact",
                      "type": "string"
                    },
                    "status": {
                      "title": "Status of the contact (active or inactive)",
                      "type": "string"
                    },
                    "type": {
                      "title": "Type of contact (person, organization)",
                      "type": "string"
                    }
                  },
                  "required": [
                    "name",
                    "address",
                    "type"
                  ],
                  "type": "object"
                },
                "id": {
                  "format": "uuid",
                  "type": "string"
                },
                "links": {
                  "properties": {
                    "self": {
                      "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                      "pattern": "^/api/v2/.*$",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "meta": {
                  "properties": {
                    "summary": {
                      "description": "Human readable summary of the content of the object",
                      "title": "Summary of the subject",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "relationships": {
                  "properties": {},
                  "type": "object"
                },
                "type": {
                  "enum": [
                    "contact"
                  ],
                  "example": "contact",
                  "type": "string"
                }
              },
              "required": [
                "type",
                "id",
                "attributes"
              ],
              "type": "object"
            },
            "type": "array"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          },
          "meta": {
            "properties": {
              "total_results": {
                "type": "integer"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityCustomObject": {
        "properties": {
          "data": {
            "description": "Entity representing a custom object as seen by the geo domain.\n\nIt is very minimal on purpose, as the \"actual\" custom object is part of the\ncase-management domain; this only has the version-independent UUID and the\nfields that should be represented on a map.",
            "properties": {
              "attributes": {
                "properties": {
                  "family": {
                    "title": "Family of a custom object is Object",
                    "type": "string"
                  },
                  "map_magic_strings": {
                    "items": {
                      "$ref": "#/components/schemas/DefMapMagicString"
                    },
                    "title": "List of magic strings of custom fields that should be shown on the map",
                    "type": "array"
                  },
                  "status": {
                    "title": "Status of a custom object",
                    "type": "string"
                  },
                  "subtitle": {
                    "title": "Subtitle of a custom object",
                    "type": "string"
                  },
                  "title": {
                    "title": "Title of a custom object",
                    "type": "string"
                  },
                  "type": {
                    "title": "Type of a custom object",
                    "type": "string"
                  }
                },
                "required": [
                  "map_magic_strings",
                  "title",
                  "subtitle",
                  "status",
                  "family",
                  "type"
                ],
                "type": "object"
              },
              "id": {
                "format": "uuid",
                "type": "string"
              },
              "links": {
                "properties": {
                  "self": {
                    "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                    "pattern": "^/api/v2/.*$",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "meta": {
                "properties": {
                  "summary": {
                    "description": "Human readable summary of the content of the object",
                    "title": "Summary of the subject",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "relationships": {
                "properties": {},
                "type": "object"
              },
              "type": {
                "enum": [
                  "custom_object"
                ],
                "example": "custom_object",
                "type": "string"
              }
            },
            "required": [
              "type",
              "id",
              "attributes"
            ],
            "type": "object"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityCustomObjectList": {
        "properties": {
          "data": {
            "items": {
              "description": "Entity representing a custom object as seen by the geo domain.\n\nIt is very minimal on purpose, as the \"actual\" custom object is part of the\ncase-management domain; this only has the version-independent UUID and the\nfields that should be represented on a map.",
              "properties": {
                "attributes": {
                  "properties": {
                    "family": {
                      "title": "Family of a custom object is Object",
                      "type": "string"
                    },
                    "map_magic_strings": {
                      "items": {
                        "$ref": "#/components/schemas/DefMapMagicString"
                      },
                      "title": "List of magic strings of custom fields that should be shown on the map",
                      "type": "array"
                    },
                    "status": {
                      "title": "Status of a custom object",
                      "type": "string"
                    },
                    "subtitle": {
                      "title": "Subtitle of a custom object",
                      "type": "string"
                    },
                    "title": {
                      "title": "Title of a custom object",
                      "type": "string"
                    },
                    "type": {
                      "title": "Type of a custom object",
                      "type": "string"
                    }
                  },
                  "required": [
                    "map_magic_strings",
                    "title",
                    "subtitle",
                    "status",
                    "family",
                    "type"
                  ],
                  "type": "object"
                },
                "id": {
                  "format": "uuid",
                  "type": "string"
                },
                "links": {
                  "properties": {
                    "self": {
                      "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                      "pattern": "^/api/v2/.*$",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "meta": {
                  "properties": {
                    "summary": {
                      "description": "Human readable summary of the content of the object",
                      "title": "Summary of the subject",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "relationships": {
                  "properties": {},
                  "type": "object"
                },
                "type": {
                  "enum": [
                    "custom_object"
                  ],
                  "example": "custom_object",
                  "type": "string"
                }
              },
              "required": [
                "type",
                "id",
                "attributes"
              ],
              "type": "object"
            },
            "type": "array"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          },
          "meta": {
            "properties": {
              "total_results": {
                "type": "integer"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityGeoFeature": {
        "properties": {
          "data": {
            "description": "Represents the geographic features in a custom object",
            "properties": {
              "attributes": {
                "properties": {
                  "geo_json": {
                    "title": "GeoJSON object detailing the feature",
                    "type": "object"
                  },
                  "origin": {
                    "description": "\n        Geo features are retrieved based on the UUID of an entity in the system.\n\n        An entity can be related to other entities, and get \"related geo\" in that\n        way.\n\n        This list indicates which UUID(s) in the query led to this geo feature\n        entity being returned.\n        ",
                    "items": {
                      "format": "uuid",
                      "type": "string"
                    },
                    "title": "List of UUIDs indicating 'why' this result is being returned",
                    "type": "array"
                  }
                },
                "required": [
                  "geo_json",
                  "origin"
                ],
                "type": "object"
              },
              "id": {
                "format": "uuid",
                "type": "string"
              },
              "links": {
                "properties": {
                  "self": {
                    "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                    "pattern": "^/api/v2/.*$",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "meta": {
                "properties": {
                  "summary": {
                    "description": "Human readable summary of the content of the object",
                    "title": "Summary of the subject",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "relationships": {
                "properties": {},
                "type": "object"
              },
              "type": {
                "enum": [
                  "geo_feature"
                ],
                "example": "geo_feature",
                "type": "string"
              }
            },
            "required": [
              "type",
              "id",
              "attributes"
            ],
            "type": "object"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityGeoFeatureList": {
        "properties": {
          "data": {
            "items": {
              "description": "Represents the geographic features in a custom object",
              "properties": {
                "attributes": {
                  "properties": {
                    "geo_json": {
                      "title": "GeoJSON object detailing the feature",
                      "type": "object"
                    },
                    "origin": {
                      "description": "\n        Geo features are retrieved based on the UUID of an entity in the system.\n\n        An entity can be related to other entities, and get \"related geo\" in that\n        way.\n\n        This list indicates which UUID(s) in the query led to this geo feature\n        entity being returned.\n        ",
                      "items": {
                        "format": "uuid",
                        "type": "string"
                      },
                      "title": "List of UUIDs indicating 'why' this result is being returned",
                      "type": "array"
                    }
                  },
                  "required": [
                    "geo_json",
                    "origin"
                  ],
                  "type": "object"
                },
                "id": {
                  "format": "uuid",
                  "type": "string"
                },
                "links": {
                  "properties": {
                    "self": {
                      "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                      "pattern": "^/api/v2/.*$",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "meta": {
                  "properties": {
                    "summary": {
                      "description": "Human readable summary of the content of the object",
                      "title": "Summary of the subject",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "relationships": {
                  "properties": {},
                  "type": "object"
                },
                "type": {
                  "enum": [
                    "geo_feature"
                  ],
                  "example": "geo_feature",
                  "type": "string"
                }
              },
              "required": [
                "type",
                "id",
                "attributes"
              ],
              "type": "object"
            },
            "type": "array"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          },
          "meta": {
            "properties": {
              "total_results": {
                "type": "integer"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityGeoFeatureRelationship": {
        "properties": {
          "data": {
            "description": "Represents a relation between two objects, for the purposes of linking\ntheir geo information together.",
            "properties": {
              "attributes": {
                "properties": {
                  "origin_uuid": {
                    "format": "uuid",
                    "title": "UUID of the 'original' object or case",
                    "type": "string"
                  },
                  "related_uuid": {
                    "format": "uuid",
                    "title": "UUID of related object",
                    "type": "string"
                  },
                  "uuid": {
                    "format": "uuid",
                    "title": "Identifier for this entity",
                    "type": "string"
                  }
                },
                "required": [
                  "uuid",
                  "origin_uuid",
                  "related_uuid"
                ],
                "type": "object"
              },
              "id": {
                "format": "uuid",
                "type": "string"
              },
              "links": {
                "properties": {
                  "self": {
                    "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                    "pattern": "^/api/v2/.*$",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "meta": {
                "properties": {
                  "summary": {
                    "description": "Human readable summary of the content of the object",
                    "title": "Summary of the subject",
                    "type": "string"
                  }
                },
                "type": "object"
              },
              "relationships": {
                "properties": {},
                "type": "object"
              },
              "type": {
                "enum": [
                  "geo_feature_relationship"
                ],
                "example": "geo_feature_relationship",
                "type": "string"
              }
            },
            "required": [
              "type",
              "id",
              "attributes"
            ],
            "type": "object"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "EntityGeoFeatureRelationshipList": {
        "properties": {
          "data": {
            "items": {
              "description": "Represents a relation between two objects, for the purposes of linking\ntheir geo information together.",
              "properties": {
                "attributes": {
                  "properties": {
                    "origin_uuid": {
                      "format": "uuid",
                      "title": "UUID of the 'original' object or case",
                      "type": "string"
                    },
                    "related_uuid": {
                      "format": "uuid",
                      "title": "UUID of related object",
                      "type": "string"
                    },
                    "uuid": {
                      "format": "uuid",
                      "title": "Identifier for this entity",
                      "type": "string"
                    }
                  },
                  "required": [
                    "uuid",
                    "origin_uuid",
                    "related_uuid"
                  ],
                  "type": "object"
                },
                "id": {
                  "format": "uuid",
                  "type": "string"
                },
                "links": {
                  "properties": {
                    "self": {
                      "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                      "pattern": "^/api/v2/.*$",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "meta": {
                  "properties": {
                    "summary": {
                      "description": "Human readable summary of the content of the object",
                      "title": "Summary of the subject",
                      "type": "string"
                    }
                  },
                  "type": "object"
                },
                "relationships": {
                  "properties": {},
                  "type": "object"
                },
                "type": {
                  "enum": [
                    "geo_feature_relationship"
                  ],
                  "example": "geo_feature_relationship",
                  "type": "string"
                }
              },
              "required": [
                "type",
                "id",
                "attributes"
              ],
              "type": "object"
            },
            "type": "array"
          },
          "links": {
            "properties": {
              "self": {
                "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                "pattern": "^/api/v2/.*$",
                "type": "string"
              }
            },
            "type": "object"
          },
          "meta": {
            "properties": {
              "total_results": {
                "type": "integer"
              }
            },
            "type": "object"
          }
        },
        "required": [
          "data",
          "links"
        ],
        "type": "object"
      },
      "entities": {}
    }
  }
}
