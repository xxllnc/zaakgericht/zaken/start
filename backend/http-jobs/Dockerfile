# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

#####################################################################
### Stage 1: Setup base python with requirements
#####################################################################
FROM python:3.11-slim-bookworm AS base

ARG BUILD_TIMESTAMP=undefined
ARG BUILD_REF=master

# Add necessary packages to system
RUN apt-get update && apt-get install -y \
    build-essential \
    libmagic1 \
    git

COPY backend/domains /opt/domains
COPY backend/zsnl-pyramid /opt/zsnl-pyramid
COPY backend/minty                  /opt/minty
COPY backend/minty-pyramid          /opt/minty-pyramid
COPY backend/minty-infra-amqp       /opt/minty-infra-amqp
COPY backend/minty-infra-email      /opt/minty-infra-email
COPY backend/minty-infra-misc       /opt/minty-infra-misc
COPY backend/minty-infra-sqlalchemy /opt/minty-infra-sqlalchemy
COPY backend/minty-infra-storage    /opt/minty-infra-storage

COPY backend/http-jobs/requirements/base.txt /opt/http-jobs/requirements.txt

WORKDIR /opt/http-jobs

RUN python3 -m venv /opt/virtualenv \
    && . /opt/virtualenv/bin/activate \
    && pip install --upgrade pip \
    && pip install -r requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

RUN echo "$BUILD_TIMESTAMP" > /opt/build-timestamp

COPY backend/http-jobs /opt/http-jobs

#####################################################################
## Stage 2: Production build
#####################################################################
FROM python:3.11-slim-bookworm AS production

ENV OTAP=production
ENV PYTHONUNBUFFERED=on
ENV PATH="/opt/virtualenv/bin:$PATH"
RUN apt-get update && apt-get install -y libmagic1 libpq5

# Set up application
COPY --from=base /opt /opt/

WORKDIR /opt/http-jobs
RUN pip install -e .

# Run as non-root user
USER 65534

#####################################################################
## Stage 3: QA environment
#####################################################################
FROM base AS quality-and-testing

ENV OTAP=test

COPY backend/http-jobs/requirements/test.txt /opt/http-jobs/requirements-test.txt
WORKDIR /opt/http-jobs

RUN pip install -r requirements-test.txt

RUN  mkdir -p /root/test_result \
    && bin/git-hooks/pre-commit -c -j /root/test_result/junit.xml

#####################################################################
## Stage 4: Development image
#####################################################################
FROM quality-and-testing AS development

ENV OTAP=development

COPY backend/http-jobs/requirements/dev.txt /opt/http-jobs/requirements-dev.txt
WORKDIR /opt/http-jobs

RUN pip install -r requirements-dev.txt

RUN pip install --no-deps .
