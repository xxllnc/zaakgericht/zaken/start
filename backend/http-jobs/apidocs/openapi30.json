{
  "openapi": "3.0.3",
  "servers": [
    {
      "url": "/"
    }
  ],
  "info": {
    "title": "Jobs management API documentation",
    "description": "CRUD operations for the Jobs service",
    "version": "v2",
    "x-package-name": "zsnl_jobs_http",
    "x-url-base": [
      "/api/v2/jobs/"
    ],
    "contact": {
      "name": "Zaaksysteem Development"
    }
  },
  "tags": [
    {
      "name": "Job"
    }
  ],
  "paths": {
    "/api/v2/jobs/get_jobs": {
      "get": {
        "x-view": "jobs",
        "x-view-class": "JobsView",
        "tags": [
          "Job"
        ],
        "operationId": "get_jobs",
        "summary": "Retrieve jobs",
        "description": "Retrieve jobs",
        "responses": {
          "200": {
            "description": "Jobs",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/DefaultSuccessMultiple"
                    },
                    {
                      "$ref": "./generated_entities.json#/components/schemas/EntityJobList"
                    }
                  ]
                }
              }
            }
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    },
    "/api/v2/jobs/create_job": {
      "post": {
        "x-view": "jobs",
        "x-view-class": "JobsView",
        "tags": [
          "Job"
        ],
        "operationId": "create_job",
        "summary": "Creates a new job",
        "description": "Creates a new job",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "job_description": {
                    "discriminator": {
                      "propertyName": "job_type"
                    },
                    "oneOf": [
                      {
                        "$ref": "./generated_entities.json#/components/schemas/JobDescriptionDeleteCase"
                      },
                      {
                        "$ref": "./generated_entities.json#/components/schemas/JobDescriptionSimulateDeleteCase"
                      },
                      {
                        "$ref": "./generated_entities.json#/components/schemas/JobDescriptionArchiveExport"
                      }
                    ]
                  },
                  "friendly_name": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/JSONAPICommandSuccess"
                }
              }
            }
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    },
    "/api/v2/jobs/cancel_job": {
      "post": {
        "x-view": "jobs",
        "x-view-class": "JobsView",
        "tags": [
          "Job"
        ],
        "operationId": "cancel_job",
        "summary": "Cancel job",
        "description": "Cancel job",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "job_uuid": {
                    "type": "string",
                    "format": "uuid"
                  }
                },
                "required": [
                  "job_uuid"
                ],
                "additionalProperties": false
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseSuccess"
                }
              }
            }
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    },
    "/api/v2/jobs/delete_job": {
      "post": {
        "x-view": "jobs",
        "x-view-class": "JobsView",
        "tags": [
          "Job"
        ],
        "operationId": "delete_job",
        "summary": "delete job",
        "description": "delete job",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "job_uuid": {
                    "type": "string",
                    "format": "uuid"
                  }
                },
                "required": [
                  "job_uuid"
                ],
                "additionalProperties": false
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseSuccess"
                }
              }
            }
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    },
    "/api/v2/jobs/download_result": {
      "get": {
        "x-view": "jobs",
        "x-view-class": "JobsView",
        "tags": [
          "Job"
        ],
        "operationId": "download_result",
        "summary": "Download the result of a job",
        "description": "Download the job output.",
        "parameters": [
          {
            "in": "query",
            "name": "job_uuid",
            "required": true,
            "schema": {
              "type": "string",
              "format": "uuid"
            },
            "description": "UUID of the job to get the result for"
          }
        ],
        "responses": {
          "302": {
            "description": "Successful response: redirect to the download URL of the file"
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    },
    "/api/v2/jobs/download_errors": {
      "get": {
        "x-view": "jobs",
        "x-view-class": "JobsView",
        "tags": [
          "Job"
        ],
        "operationId": "download_errors",
        "summary": "Download the 'errors' file for a job",
        "description": "Download the job error output.",
        "parameters": [
          {
            "in": "query",
            "name": "job_uuid",
            "required": true,
            "schema": {
              "type": "string",
              "format": "uuid"
            },
            "description": "UUID of the job to get the error result for"
          }
        ],
        "responses": {
          "302": {
            "description": "Successful response: redirect to the download URL of the file"
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "DefaultSuccess": {
        "type": "object",
        "required": [
          "meta"
        ],
        "properties": {
          "meta": {
            "type": "object",
            "required": [
              "api_version"
            ],
            "properties": {
              "api_version": {
                "type": "integer",
                "example": 2
              }
            }
          }
        }
      },
      "DefaultSuccessMultiple": {
        "allOf": [
          {
            "$ref": "#/components/schemas/DefaultSuccess"
          },
          {
            "type": "object",
            "properties": {
              "links": {
                "type": "object",
                "properties": {
                  "self": {
                    "type": "string",
                    "example": "http://example.zs.nl/?page=2"
                  },
                  "prev": {
                    "type": "string",
                    "example": "http://example.zs.nl/?page=1"
                  },
                  "next": {
                    "type": "string",
                    "example": "http://example.zs.nl/?page=3"
                  }
                }
              }
            }
          }
        ]
      },
      "ResponseSuccess": {
        "properties": {
          "data": {
            "type": "object",
            "properties": {
              "data": {
                "type": "object",
                "nullable": true,
                "example": null
              }
            }
          }
        }
      },
      "JSONAPICommandSuccess": {
        "properties": {
          "data": {
            "type": "object",
            "properties": {
              "type": {
                "type": "string"
              },
              "id": {
                "type": "string",
                "format": "uuid"
              },
              "links": {
                "type": "object",
                "properties": {
                  "self": {
                    "type": "string",
                    "example": "http://example.zs.nl/?uuid=39da57ba-7c8e-4638-be09-be66cdd32149"
                  }
                }
              }
            }
          }
        }
      },
      "ResponseError": {
        "required": [
          "errors"
        ],
        "properties": {
          "errors": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "title": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    }
  }
}