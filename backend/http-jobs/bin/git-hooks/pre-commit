#! /bin/sh

# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

set -e

while getopts "fcj:" opt; do
  case "${opt}" in
    f)
      fix_errors=1
      ;;
    c)
      test_coverage=1
      ;;
    j)
      junit_file="${OPTARG}"
      ;;
    \?)
      echo "Usage: $0 [-f] [-c] [-j=filename]"
      ;;
  esac
done


PYTESTARGS=""
if [ -n "$test_coverage" ]; then
  PYTESTARGS="$PYTESTARGS --cov-report=xml --cov-report=term --cov-branch --cov=."
fi

if [ -n "$junit_file" ]; then
  PYTESTARGS="$PYTESTARGS --junitxml=$junit_file"
fi

RUFF_CHECK_ARGS="--fix"
RUFF_FORMAT_ARGS=""
if [ -z "$fix_errors" ]; then
  RUFF_CHECK_ARGS=""
  RUFF_FORMAT_ARGS="--diff"
fi

MYPYARGS="--ignore-missing-imports --follow-imports=silent --show-column-numbers"

read -r COMMANDS <<EOF
    pytest $PYTESTARGS && \
    echo 'Run ruff check $RUFF_CHECK_ARGS .' && \
    ruff check $RUFF_CHECK_ARGS . && \
    echo 'Run ruff format $RUFF_FORMAT_ARGS .' && \
    ruff format $RUFF_FORMAT_ARGS . && \
    echo 'Run mypy $MYPYARGS [disabled]' && \
    echo mypy $MYPYARGS . && \
    pip-audit --ignore-vuln GHSA-8fww-64cx-x8p5 --ignore-vuln PYSEC-2023-46 \
    --ignore-vuln GHSA-wj6h-64fc-37mp --ignore-vuln GHSA-cjwg-qfpm-7377 \
    --ignore-vuln GHSA-6c5p-j8vq-pqhj && \
    liccheck -s strategy.ini -r <(grep -F -v '../' requirements/base.txt)
EOF

if [ -f .run-git-hooks-via-compose ]; then
  echo "$COMMANDS" | docker-compose run -T "${PWD##*/}" bash
else
  echo "$COMMANDS" | bash
fi
