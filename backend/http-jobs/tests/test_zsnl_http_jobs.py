# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from unittest import TestCase


class TestJobsFeature(TestCase):
    """
    Class containing main tests for the ZSNL HTTP Jobs Service
    """
