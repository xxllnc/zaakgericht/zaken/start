# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


def add_routes(config):
    """Add routes to pyramid application.

    :param config: pyramid config file
    :type config: Configurator
    """
    handlers = [
        {
            "route": "/api/v2/jobs/openapi/{filename}",
            "handler": "apidocs_fileserver",
            "method": "GET",
            "view": "zsnl_jobs_http.views.apidocs.apidocs_fileserver",
        },
        {
            "route": "/api/v2/jobs/get_jobs",
            "handler": "get_jobs",
            "method": "GET",
            "view": "zsnl_jobs_http.views.jobs.JobsView",
            "viewClass": "JobsView",
        },
        {
            "route": "/api/v2/jobs/download_result",
            "handler": "download_result",
            "method": "GET",
            "view": "zsnl_jobs_http.views.jobs.JobsView",
            "viewClass": "JobsView",
        },
        {
            "route": "/api/v2/jobs/download_errors",
            "handler": "download_errors",
            "method": "GET",
            "view": "zsnl_jobs_http.views.jobs.JobsView",
            "viewClass": "JobsView",
        },
        {
            "route": "/api/v2/jobs/delete_job",
            "handler": "delete_job",
            "method": "POST",
            "view": "zsnl_jobs_http.views.jobs.JobsView",
            "viewClass": "JobsView",
        },
        {
            "route": "/api/v2/jobs/create_job",
            "handler": "create_job",
            "method": "POST",
            "view": "zsnl_jobs_http.views.jobs.JobsView",
            "viewClass": "JobsView",
        },
        {
            "route": "/api/v2/jobs/cancel_job",
            "handler": "cancel_job",
            "method": "POST",
            "view": "zsnl_jobs_http.views.jobs.JobsView",
            "viewClass": "JobsView",
        },
    ]

    for h in handlers:
        config.add_route(
            name=h["handler"], pattern=h["route"], request_method=h["method"]
        )
        config.add_view(
            view=h["view"],
            route_name=h["handler"],
            request_method=h["method"],
        )
