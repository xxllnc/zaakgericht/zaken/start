# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class JobsView(JSONAPIEntityView):
    view_mapper = {
        "GET": {
            "get_jobs": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.jobs",
                "run": "get_jobs",
                "from": {},
            },
            "download_result": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.jobs",
                "run": "download_result",
                "from": {
                    "request_params": {
                        "job_uuid": "job_uuid",
                    }
                },
            },
            "download_errors": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.jobs",
                "run": "download_errors",
                "from": {
                    "request_params": {
                        "job_uuid": "job_uuid",
                    }
                },
            },
        },
        "POST": {
            "create_job": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.jobs",
                "run": "create_job",
                "from": {
                    "json": {"_all": True},
                },
                "command_primary": "job_uuid",
                "generate_missing_id": ["job_uuid"],
                "entity_type": "job",
            },
            "delete_job": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.jobs",
                "run": "delete_job",
                "from": {"json": {"job_uuid": "job_uuid"}},
            },
            "cancel_job": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.jobs",
                "run": "cancel_job",
                "from": {"json": {"job_uuid": "job_uuid"}},
            },
        },
    }
