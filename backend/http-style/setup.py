#!/usr/bin/env python

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# -*- coding: utf-8 -*-

"""The setup script."""

import os
from setuptools import find_packages, setup


def normalize(package):
    if package.startswith("."):
        p = package.split("/")[-1]
        return p + "@file://" + os.path.normpath(os.getcwd() + "/" + package)

    return package


with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("requirements/base.txt") as f:
    requirements = [normalize(line) for line in f.read().splitlines()]

with open("requirements/test.txt") as f:
    test_requirements = [normalize(line) for line in f.read().splitlines()]
with open("requirements/dev.txt") as f:
    dev_requirements = [normalize(line) for line in f.read().splitlines()]


setup(
    author="Sharda Deshmukh",
    author_email="sharda.deshmukh@xxllnc.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="Service to store and retrieve custom style info and system config",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    license="EUPL license",
    long_description=readme,
    include_package_data=True,
    keywords="zsnl_style_http",
    name="zsnl_style_http",
    packages=find_packages(include=["zsnl_style_http", "zsnl_style_http.*"]),
    package_data={"": ["*.json"]},
    test_suite="tests",
    tests_require=test_requirements,
    extras_require={"dev": dev_requirements},
    url="https://gitlab.com/minty-python/http-style",
    version="0.0.1",
    zip_safe=False,
    entry_points={"paste.app_factory": ["main = zsnl_style_http:main"]},
)
