#!/usr/bin/env python

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# -*- coding: utf-8 -*-

"""The setup script."""

import os
from setuptools import find_packages, setup


def normalize(package):
    if package.startswith("."):
        p = package.split("/")[-1]
        return p + "@file://" + os.getcwd() + "/" + package

    return package


with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("requirements/base.txt") as f:
    requirements = [normalize(line) for line in f.read().splitlines()]

with open("requirements/test.txt") as f:
    test_requirements = [normalize(line) for line in f.read().splitlines()]

setup(
    author="Martijn van de Streek",
    author_email="martijn@mintlab.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="Middleware to help transition from Perl to Python",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    license="EUPL license",
    long_description=readme,
    include_package_data=True,
    keywords="zsnl_perl_migration",
    name="migration-libraries",
    packages=find_packages(
        include=["zsnl_perl_migration", "zsnl_perl_migration.*"]
    ),
    package_data={"": ["*.json"]},
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-migration_libraries",
    version="1.0.1",
    zip_safe=False,
)
