# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import time
from minty import cqrs
from minty_infra_amqp import AMQPInfrastructure
from zsnl_perl_migration import LegacyEventBroadcastMiddleware


class mock_infra_factory:
    def __init__(self, config):
        self.registered_infrastructure = {}
        self.config = config

    def get_infrastructure(self, infrastructure_name: str, context: str):
        """Retrieve an infrastructure instance for the selected instance."""
        infra = self.registered_infrastructure[infrastructure_name](
            config=self.config
        )
        return infra

    def register_infrastructure(self, name: str, infrastructure: object):
        """Register an infrastructure class with the factory."""
        self.registered_infrastructure[name] = infrastructure

    def get_config(self, context):
        return {"logging_id": "id123"}


@pytest.mark.live
class TestLiveRmq:
    def setup_method(self):
        """Create all needed infrastructure to do live test."""
        # Wait for RabbitMQ docker container to start
        time.sleep(3)

        self.func = lambda: True
        config = {"amqp": {"url": "http://rabbitmq:5672"}}
        # config = {"amqp": {"url": "http://localhost:5672"}}

        self.infra_factory = mock_infra_factory(config=config)

        self.interesting_events = [("domain", "something_interesting")]
        self.infra_factory.register_infrastructure(
            name="rabbitmq_name", infrastructure=AMQPInfrastructure()
        )
        self.context = "rabbitmq_context"

        self.channel = self.infra_factory.get_infrastructure(
            "rabbitmq_name", context=self.context
        )
        self.channel.queue.declare(queue="live_testing_queue")
        self.channel.queue.bind(
            queue="live_testing_queue",
            exchange="amq.topic",
            routing_key="zs.v0.#",
        )
        middleware_tmp = LegacyEventBroadcastMiddleware(
            amqp_infra_name="rabbitmq_name",
            interesting_events=self.interesting_events,
        )
        user_uuid = "3743d1d2-6b58-11e9-821e-9fee66554ea4"
        self.event = cqrs.Event(
            domain="domain",
            event_name="something_interesting",
            context="testcontext",
            user_uuid="231fad7a-35e3-11e9-bb58-c7379328ee3b",
            uuid="cc7f6422-6677-11e9-8260-3f46294ecf2b",
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="CaseType",
            entity_id="231fad7a-35e3-11e9-bb58-c7379328ee3b",
            changes={},
            entity_data={},
        )

        mock_event_service = cqrs.EventService(
            correlation_id="req-12345",
            domain="domain",
            context="testcontext",
            user_uuid=user_uuid,
        )
        mock_event_service.event_list.append(self.event)

        self.middleware = middleware_tmp(
            infrastructure_factory=self.infra_factory,
            event_service=mock_event_service,
            correlation_id="req-12345",
            domain="domain",
            context="testcontext",
            user_uuid="231fad7a-35e3-11e9-bb58-c7379328ee3b",
        )

    def teardown_method(self):
        self.channel.queue.delete(queue="live_testing_queue")
        self.channel.close()

    def test_legacy_event_broadcast_live(self):
        """Publish Message and see if message is as expected."""

        self.middleware(func=self.func)

        msg = self.channel.basic.get("live_testing_queue")
        assert msg.json() == {
            "url": f"https://{self.event.context}/api/queue/{self.event.uuid}/run",
            "instance_hostname": self.event.context,
        }
        msg.ack()
