## QA (tests, documentation)
pytest~=8.1
pytest-cov~=5.0
pytest-mock~=3.3
pytest-spec~=3.0

## Code style tools
ruff~=0.9
mypy~=1.8

## QA (tests, documentation, security, licenses)
pip-audit
liccheck~=0.1

## Framework
../minty
