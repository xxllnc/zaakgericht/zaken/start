# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import os
import pytest
import sqlalchemy.pool
from minty_infra_sqlalchemy import (
    DatabaseSessionInfrastructure,
    DatabaseTransactionMiddleware,
    DatabaseTransactionQueryMiddleware,
)
from unittest import mock


class MockException(Exception):
    pass


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name][context]


class MockDatabaseConnection:
    def __init__(self):
        self.commit_called = 0
        self.rollback_called = 0
        self.close_called = 0

    def commit(self):
        self.commit_called += 1

    def rollback(self):
        self.rollback_called += 1

    def close(self):
        self.close_called += 1


class MockDatabaseSession:
    def __init__(self, connection):
        self.commit_called = 0
        self.rollback_called = 0
        self.close_called = 0

        self.connection = connection

    def commit(self):
        self.commit_called += 1

    def rollback(self):
        self.rollback_called += 1

    def close(self):
        self.close_called += 1

    @property
    def bind(self):
        return self.connection


class TestDatabaseTransactionMiddleware:
    """Test database transaction middleware"""

    def test_middleware_commit(self):
        mock_connection = MockDatabaseConnection()
        mock_database = MockDatabaseSession(mock_connection)
        mock_infra = {"test1": {"testcontext": mock_database}}
        infra_factory = MockInfrastructureFactory(mock_infra)

        mock_database_ro = MockDatabaseSession(mock_connection)
        mock_infra_ro = {"test1": {"testcontext": mock_database_ro}}
        infra_factory_ro = MockInfrastructureFactory(mock_infra_ro)

        repo_factory = mock.Mock(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
        )

        middleware_class = DatabaseTransactionMiddleware("test1")
        middleware = middleware_class(
            repository_factory=repo_factory,
            event_service=None,
            correlation_id=None,
            domain="d",
            context="testcontext",
            user_uuid="u",
        )
        f_called = None

        def f():
            nonlocal f_called
            f_called = True

        middleware(f)

        assert f_called
        assert mock_database.commit_called == 1
        assert mock_database.rollback_called == 0
        assert mock_database.close_called == 0

        assert mock_database_ro.commit_called == 0
        assert mock_database_ro.rollback_called == 0
        assert mock_database_ro.close_called == 0

        assert mock_connection.commit_called == 1
        assert mock_connection.rollback_called == 0
        assert mock_connection.close_called == 0

    def test_middleware_rollback(self):
        mock_connection = MockDatabaseConnection()
        mock_database = MockDatabaseSession(mock_connection)
        mock_infra = {"test1": {"testcontext": mock_database}}
        infra_factory = MockInfrastructureFactory(mock_infra)

        mock_database_ro = MockDatabaseSession(mock_connection)
        mock_infra_ro = {"test1": {"testcontext": mock_database_ro}}
        infra_factory_ro = MockInfrastructureFactory(mock_infra_ro)

        repo_factory = mock.Mock(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
        )

        middleware_class = DatabaseTransactionMiddleware("test1")
        middleware = middleware_class(
            repository_factory=repo_factory,
            event_service=None,
            correlation_id=None,
            domain="d",
            context="testcontext",
            user_uuid="u",
        )

        def f():
            raise MockException

        with pytest.raises(MockException):
            middleware(f)

        assert mock_database.commit_called == 0
        assert mock_database.rollback_called == 1
        assert mock_database.close_called == 0

        assert mock_database_ro.commit_called == 0
        assert mock_database_ro.rollback_called == 0
        assert mock_database_ro.close_called == 0

        assert mock_connection.commit_called == 0
        assert mock_connection.rollback_called == 1
        assert mock_connection.close_called == 0


class TestDatabaseTransactionQueryMiddleware:
    """Test database query middleware for transactions"""

    def test_middleware_commit(self):
        mock_connection = MockDatabaseConnection()
        mock_database = MockDatabaseSession(mock_connection)
        mock_infra = {"test1": {"testcontext": mock_database}}
        infra_factory = MockInfrastructureFactory(mock_infra)

        mock_database_ro = MockDatabaseSession(mock_connection)
        mock_infra_ro = {"test1": {"testcontext": mock_database_ro}}
        infra_factory_ro = MockInfrastructureFactory(mock_infra_ro)

        repo_factory = mock.Mock(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
        )

        middleware_class = DatabaseTransactionQueryMiddleware("test1")
        middleware = middleware_class(
            repository_factory=repo_factory,
            correlation_id=None,
            domain="d",
            context="testcontext",
            user_uuid="u",
        )

        f_called = False

        def f():
            nonlocal f_called
            f_called = True
            return "x"

        rv = middleware(f)

        assert rv == "x"
        assert f_called
        assert mock_database.commit_called == 0
        assert mock_database.rollback_called == 0
        assert mock_database.close_called == 0

        assert mock_database_ro.commit_called == 0
        assert mock_database_ro.close_called == 0
        # Rollback is done internally by sqlalchemy when `close` is called.
        assert mock_database_ro.rollback_called == 0

        assert mock_connection.commit_called == 0
        assert mock_connection.rollback_called == 0
        assert mock_connection.close_called == 0

    def test_middleware_rollback(self):
        mock_connection = MockDatabaseConnection()
        mock_database = MockDatabaseSession(mock_connection)
        mock_infra = {"test1": {"testcontext": mock_database}}
        infra_factory = MockInfrastructureFactory(mock_infra)

        mock_database_ro = MockDatabaseSession(mock_connection)
        mock_infra_ro = {"test1": {"testcontext": mock_database_ro}}
        infra_factory_ro = MockInfrastructureFactory(mock_infra_ro)

        repo_factory = mock.Mock(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
        )
        middleware_class = DatabaseTransactionQueryMiddleware("test1")
        middleware = middleware_class(
            repository_factory=repo_factory,
            correlation_id=None,
            domain="d",
            context="testcontext",
            user_uuid="u",
        )

        def f():
            raise MockException

        with pytest.raises(MockException):
            middleware(f)

        assert mock_database.commit_called == 0
        assert mock_database.rollback_called == 0
        assert mock_database.close_called == 0
        assert mock_database_ro.commit_called == 0
        assert mock_database_ro.close_called == 0
        # Rollback is done internally by sqlalchemy when `close` is called.
        assert mock_database_ro.rollback_called == 0


class TestDatabaseSessionInfrastructure:
    @mock.patch("sqlalchemy.engine_from_config")
    @mock.patch("sqlalchemy.orm.Session")
    def test_session_cycle(self, session_mock, sqlalchemy_mock):
        dsi = DatabaseSessionInfrastructure("prefix1.")

        assert dsi.prefix == "prefix1."
        session_config = {"mock_config": "mock_value"}

        session = dsi(session_config)

        opts = sqlalchemy_mock.call_args[1]

        for key, value in {
            "configuration": session_config,
            "prefix": "prefix1.",
            "poolclass": sqlalchemy.pool.NullPool,
        }.items():
            assert opts[key] == value

        assert opts["json_serializer"]

        session_mock.assert_called_with(bind=sqlalchemy_mock().connect())

        session.close.assert_not_called()

        dsi.clean_up(session)

        session.close.assert_called()

    @mock.patch("sqlalchemy.engine_from_config")
    @mock.patch("sqlalchemy.orm.Session")
    def test_session_cycle_postgresql(self, session_mock, sqlalchemy_mock):
        dsi = DatabaseSessionInfrastructure("prefix1.")

        assert dsi.prefix == "prefix1."
        session_config = {
            "mock_config": "mock_value",
            "prefix1.url": "postgresql://foobar",
        }
        session_config_psycopg3 = {
            "mock_config": "mock_value",
            "prefix1.url": "postgresql+psycopg://foobar",
        }

        session = dsi(session_config)

        opts = sqlalchemy_mock.call_args[1]

        for key, value in {
            "configuration": session_config_psycopg3,
            "prefix": "prefix1.",
            "poolclass": sqlalchemy.pool.NullPool,
        }.items():
            assert opts[key] == value

        assert opts["json_serializer"]

        session_mock.assert_called_with(bind=sqlalchemy_mock().connect())

        session.close.assert_not_called()

        dsi.clean_up(session)

        session.close.assert_called()


class TestDatabaseSessionLocks:
    @mock.patch("sqlalchemy.engine_from_config")
    @mock.patch("sqlalchemy.orm.Session")
    def test_default_lock(self, session_mock, sqlalchemy_mock: mock.MagicMock):
        mock_engine = mock.MagicMock()
        mock_engine.configure_mock(name="mock_engine")
        mock_connection = mock.MagicMock()
        mock_connection.configure_mock(name="mock_connection")

        sqlalchemy_mock.return_value = mock_engine
        mock_engine.connect.return_value = mock_connection

        dsi = DatabaseSessionInfrastructure("prefix1")
        dsi.__call__({"mock_config": "mock_value"})

        assert (
            mock_connection.execute.call_args[0][0].text
            == "SET lock_timeout = 30000"
        )

    @mock.patch.dict(os.environ, {"DB_LOCK_TIMEOUT_MS": "20"})
    @mock.patch("sqlalchemy.engine_from_config")
    @mock.patch("sqlalchemy.orm.Session")
    def test_env_lock(self, session_mock, sqlalchemy_mock: mock.MagicMock):
        mock_engine = mock.MagicMock()
        mock_engine.configure_mock(name="mock_engine")
        mock_connection = mock.MagicMock()
        mock_connection.configure_mock(name="mock_connection")

        sqlalchemy_mock.return_value = mock_engine
        mock_engine.connect.return_value = mock_connection

        dsi = DatabaseSessionInfrastructure("prefix1")
        dsi.__call__({"mock_config": "mock_value"})

        assert (
            mock_connection.execute.call_args[0][0].text
            == "SET lock_timeout = 20"
        )
