#!/bin/bash

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /tmp/tests && pytest --junitxml=/tmp/tests/junit.xml --cov=minty_infra_storage --cov-report=term
