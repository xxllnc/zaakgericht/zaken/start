# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "3.0.1"

from .s3 import S3Infrastructure

__all__ = ["S3Infrastructure"]
