#!/usr/bin/env python

# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# -*- coding: utf-8 -*-

"""The setup script."""

import os
from setuptools import find_packages, setup


def normalize(package):
    if package.startswith("."):
        p = package.split("/")[-1]
        return p + "@file://" + os.path.normpath(os.getcwd() + "/" + package)

    return package


with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("requirements/base.txt") as f:
    requirements = [normalize(line) for line in f.read().splitlines()]

with open("requirements/test.txt") as f:
    test_requirements = [normalize(line) for line in f.read().splitlines()]

setup(
    author="Michiel Ootjers",
    author_email="michiel@mintlab.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.9",
    ],
    description="Minty HTTP Api server based on Pyramid",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    license="EUPL license",
    long_description_content_type="text/x-rst",
    long_description=readme,
    include_package_data=True,
    keywords="minty_pyramid",
    name="minty-pyramid",
    packages=find_packages(include=["minty_pyramid", "minty_pyramid.*"]),
    package_data={
        "minty_pyramid.code_generation": ["templates/*.j2"],
        "minty_pyramid": ["py.typed"],
    },
    entry_points={
        "console_scripts": [
            "generate-routes = minty_pyramid.code_generation:generate_routes",
            "generate-views = minty_pyramid.code_generation:generate_views",
            "generate-entities = minty_pyramid.code_generation:generate_entities",
        ]
    },
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/minty-python/minty-pyramid",
    version="3.0.2",
    zip_safe=False,
)
