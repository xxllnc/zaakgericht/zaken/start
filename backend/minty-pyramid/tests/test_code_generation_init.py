# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from click.testing import CliRunner
from minty.entity import Entity, ValueObject
from minty_pyramid.code_generation import (
    entities,
    generate,
    generate_entities,
    generate_routes,
    generate_views,
    openapi,
)
from pydantic.v1 import Field
from typing import Any, List
from unittest import mock
from uuid import UUID


class TestInitGeneration:
    @mock.patch("minty_pyramid.code_generation.generate.apidocs")
    @mock.patch("minty_pyramid.code_generation.generate.test_routes")
    @mock.patch("minty_pyramid.code_generation.generate.routes")
    @mock.patch("minty_pyramid.code_generation.openapi.load_json_file")
    @mock.patch("minty_pyramid.code_generation.openapi.parse_routes")
    def test_generate_routes(
        self, m_parse_routes, m_load_file, m_routes, m_test_routes, m_apidocs
    ):
        m_load_file.return_value = {
            "file": True,
            "info": {"x-url-base": True, "x-package-name": "test_package"},
        }
        m_parse_routes.return_value = {"routes": True}

        runner = CliRunner()
        filename = "test_openapi.json"
        package = "test_package"
        res = runner.invoke(generate_routes, ["--filename", filename])

        assert res.exit_code == 0
        m_routes.assert_called_once_with({"routes": True}, package)
        m_test_routes.assert_called_once_with({"routes": True}, package)

    @mock.patch("minty_pyramid.code_generation.generate.test_routes")
    @mock.patch("minty_pyramid.code_generation.generate.routes")
    @mock.patch("minty_pyramid.code_generation.openapi.load_json_file")
    @mock.patch("minty_pyramid.code_generation.openapi.parse_routes")
    def test_generate_routes_no_info(
        self, m_parse_routes, m_load_file, m_routes, m_test_routes
    ):
        m_load_file.return_value = {"file": True}
        m_parse_routes.return_value = {"routes": True}
        runner = CliRunner()
        filename = "test_openapi.json"
        res = runner.invoke(generate_routes, ["--filename", filename])

        assert res.exit_code == 0

    @mock.patch("minty_pyramid.code_generation.generate.views")
    @mock.patch("minty_pyramid.code_generation.openapi.load_json_file")
    @mock.patch("minty_pyramid.code_generation.openapi.parse_routes")
    def test_generate_views(self, m_parse_routes, m_load_file, m_views):
        m_load_file.return_value = {"file": True}
        m_parse_routes.return_value = {"views": True}

        runner = CliRunner()
        filename = "test_openapi.json"
        package = "minty_package"
        res = runner.invoke(
            generate_views, ["--package-name", package, "--filename", filename]
        )

        assert res.exit_code == 0
        m_views.assert_called_once_with({"views": True}, package)


class TestOpenAPI:
    def test_parse_routes(self):
        file = openapi.load_json_file("tests/data/test_data2.json")
        expected_routes = [
            {
                "route": "/api/v2/cm/openapi/{filename}",
                "handler": "apidocs_fileserver",
                "renderer": "json",
                "method": "GET",
                "view": "test.views.apidocs.apidocs_fileserver",
                "handler_file": "apidocs",
                "params": [],
                "grouped_params": {},
            },
            {
                "route": "/bag/{bag_object_type}/id/{bag_id}",
                "method": "GET",
                "handler": "get_record_by_id",
                "renderer": "json",
                "view": "test.views.views.get_record_by_id",
                "handler_file": "views",
                "params": [],
                "grouped_params": {},
            },
            {
                "route": "/bag/{bag_object_type}/search",
                "method": "GET",
                "handler": "search",
                "renderer": "json",
                "view": "test.views.views.search",
                "handler_file": "views",
                "params": [],
                "grouped_params": {},
            },
            {
                "route": "/bag/{bag_object_type}/address",
                "method": "GET",
                "handler": "get_by_address",
                "renderer": "json",
                "view": "test.views.views.get_by_address",
                "handler_file": "views",
                "params": [],
                "grouped_params": {},
            },
            {
                "route": "/bag/{bag_object_type}/find-nearest",
                "method": "GET",
                "handler": "find_nearest_record",
                "renderer": "json",
                "view": "test.views.views.find_nearest_record",
                "handler_file": "views",
                "params": [],
                "grouped_params": {},
            },
            {
                "grouped_params": {},
                "handler": "find_nearest_record",
                "handler_file": "views",
                "method": "POST",
                "params": [],
                "renderer": "json",
                "route": "/bag/{bag_object_type}/find-closest",
                "view": "test.views.views.BagViews",
                "viewclass": "BagViews",
            },
        ]
        routes = openapi.parse_routes(openapi_file=file, package_name="test")
        assert routes == expected_routes

    @mock.patch("minty_pyramid.code_generation.openapi.add_object_to_grouping")
    def test_group_params_by_query_method(self, m_add_object_to_grouping):
        params = [
            {
                "in": "path",
                "required": False,
                "expected_type": "path",
                "schema": {"type": "string"},
            },
            {
                "in": "query",
                "required": True,
                "expected_type": "query_required",
                "schema": {"type": "string"},
            },
            {
                "in": "query",
                "required": False,
                "expected_type": "query_optional",
                "schema": {"type": "string"},
            },
            {
                "in": "query",
                "required": False,
                "schema": {"type": "array"},
                "expected_type": "query_multiple",
            },
        ]

        for param in params:
            openapi.group_params_by_query_method(params=[param])
            m_add_object_to_grouping.assert_called_with(
                item=param, grouping={}, group_name=param["expected_type"]
            )

    def test_add_object_to_grouping(self):
        grouping = {}
        item = {"item": "one"}
        group_name = "test_group"
        openapi.add_object_to_grouping(
            item=item, grouping=grouping, group_name=group_name
        )
        assert grouping == {"test_group": [{"item": "one"}]}
        item2 = {"item": "two"}
        openapi.add_object_to_grouping(
            item=item2, grouping=grouping, group_name=group_name
        )
        assert grouping == {"test_group": [{"item": "one"}, {"item": "two"}]}

    def test_load_file(self):
        file = openapi.load_json_file("tests/data/test_data.json")
        assert file == {"test": "data"}


class TestGenerate:
    def test_template_loader(self):
        from jinja2 import Template

        routes_template = generate.load_template("routes.j2")
        test_routes_template = generate.load_template("test_routes.j2")
        assert isinstance(routes_template, Template)
        assert isinstance(test_routes_template, Template)

    @mock.patch("minty_pyramid.code_generation.generate.open")
    def test_routes(self, m_open):
        file_handle = m_open.return_value.__enter__.return_value
        generate.routes(
            [
                {
                    "handler": "test_handler",
                    "method": "GET",
                    "route": "/hello/world",
                }
            ],
            "package_name",
        )
        file_handle.write.assert_called_once()

    @mock.patch("minty_pyramid.code_generation.generate.open")
    def test_views(self, m_open):
        file_handle = m_open.return_value.__enter__.return_value
        generate.views([{"handler": "test_handler"}], "package_name")
        file_handle.write.assert_called_once()

    @mock.patch("minty_pyramid.code_generation.generate.open")
    @mock.patch("minty_pyramid.code_generation.generate.group_routes_by")
    def test_test_routes(self, m_group_routes_by, m_open):
        file_handle = m_open.return_value.__enter__.return_value
        m_group_routes_by.return_value = {}
        generate.test_routes("{}", "package_name")

        file_handle.write.assert_called_once()

    def test_group_routes_by_file(self):
        routes = [
            {"handler_file": "views", "no": "1"},
            {"handler_file": "views", "no": "2"},
            {"handler_file": "custom", "no": "3"},
            {"handler_file": "custom", "no": "14"},
        ]
        grouped_routes_expected = {
            "views": [
                {"handler_file": "views", "no": "1"},
                {"handler_file": "views", "no": "2"},
            ],
            "custom": [
                {"handler_file": "custom", "no": "3"},
                {"handler_file": "custom", "no": "14"},
            ],
        }
        grouped_routes = generate.group_routes_by(
            routes=routes, param="handler_file"
        )

        assert grouped_routes == grouped_routes_expected

    @mock.patch("minty_pyramid.code_generation.generate.open")
    def test_apidocs(self, mock_open):
        generate.apidocs("test_package")


class MockFurType(ValueObject):
    name: str = Field(..., titl="Type of fur")


class MockOwnerRelative(Entity):
    entity_type = "relative"
    entity_id__fields: list[str] = ["uuid"]
    name: str = Field(..., title="Name of relative")
    uuid: UUID = Field(..., title="Unique identifier of relative")


class MockOwner(Entity):
    entity_type = "owner"
    entity_id__fields: list[str] = ["uuid"]
    entity_relationships: list[str] = ["relatives"]

    name: str = Field(..., title="Name of owner")
    uuid: UUID = Field(..., title="Unique identifier of owner")

    relatives: List[MockOwnerRelative] = Field(..., title="Relatives of owner")


class MockAnimal(Entity):
    entity_type = "animal"
    entity_relationships: list[str] = ["owner", "not_pydantic_relationship"]
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Unique identifier of animal")

    owner: MockOwner = Field(...)
    name: str = Field(..., title="Name of animal")

    fur_type: MockFurType = Field(..., title="Type of fur")
    not_pydantic_relationship: Any = Field(
        ..., title="Non pydantic relationship"
    )

    number_of_legs: int = Field(4, title="Number of legs", const=True)


class MockEmpty(Entity):
    entity_type = "empty"

    entity_id__fields: list[str] = ["uuid"]
    uuid: UUID = Field(..., title="Unique identifier of empty entity")


class MockLinked(Entity):
    entity_type = "linked"

    entity_id__fields: list[str] = ["uuid"]
    uuid: UUID = Field(..., title="Unique identifier of empty entity")


class MockLinker(Entity):
    entity_type = "linker"

    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Unique identifier of empty entity")

    # Link, but NOT a relationship
    linked: MockLinked = Field(
        ..., title="Link that should not be in 'relationships'"
    )


class TestEntityGenerator:
    @mock.patch("minty_pyramid.code_generation.entities.import_module")
    def _get_mocked_generator(self, mock_importlib):
        used_entities_all = [
            "MockAnimal",
            "MockOwner",
            "MockEmpty",
            "MockLinker",
            "MockLinked",
        ]
        mock_importlib.return_value = mock.Mock(
            __all__=used_entities_all,
            MockAnimal=MockAnimal,
            MockOwner=MockOwner,
            MockEmpty=MockEmpty,
            MockLinker=MockLinker,
            MockLinked=MockLinked,
        )
        eg = entities.EntityGenerator(entity_module_name="test")

        return eg

    def _get_generated(self):
        eg = self._get_mocked_generator()

        return eg.generate()

    def test_check_format(self):
        generated = self._get_generated()

        assert generated["components"]["schemas"]

        schemas = generated["components"]["schemas"]

        assert schemas["EntityMockAnimal"]
        assert schemas["EntityMockOwner"]
        assert schemas["EntityMockEmpty"]

        assert schemas["EntityMockAnimal"]["properties"]["data"]
        animal = schemas["EntityMockAnimal"]["properties"]["data"]
        assert animal["properties"]["type"]
        assert animal["properties"]["type"]["example"] == "animal"
        assert animal["properties"]["type"]["example"] == "animal"
        assert animal["properties"]["id"]["format"] == "uuid"
        assert animal["properties"]["attributes"]["properties"]["name"]
        assert animal["properties"]["attributes"]["properties"][
            "number_of_legs"
        ] == {
            "enum": [4],
            "title": "Number of legs",
            "type": "integer",
        }
        assert (
            "uuid"
            not in animal["properties"]["attributes"]["properties"].keys()
        )
        assert (
            "owner"
            not in animal["properties"]["attributes"]["properties"].keys()
        )
        assert animal["properties"]["relationships"]["properties"]["owner"]
        assert (
            animal["properties"]["relationships"]["properties"]["owner"][
                "properties"
            ]["data"]["properties"]["type"]["example"]
            == "owner"
        )

        assert schemas["EntityMockEmpty"]["required"] == ["data", "links"]

        assert schemas["EntityMockAnimalList"]["properties"] == {
            "data": {
                "items": schemas["EntityMockAnimal"]["properties"]["data"],
                "type": "array",
            },
            "links": {
                "type": "object",
                "properties": {
                    "self": {
                        "type": "string",
                        "pattern": "^/api/v2/.*$",
                        "example": "/api/v2/get_something?uuid=1a2b3c4d-1a2b-1a2b-1a2b-1a2b3c4d5e6f",
                    }
                },
            },
            "meta": {
                "type": "object",
                "properties": {"total_results": {"type": "integer"}},
            },
        }

    def test_check_list_context(self):
        generated = self._get_generated()

        assert generated["components"]["schemas"]

        schemas = generated["components"]["schemas"]
        assert schemas["EntityMockAnimalList"]
        assert schemas["EntityMockOwnerList"]

        assert (
            schemas["EntityMockAnimalList"]["properties"]["data"]["type"]
            == "array"
        )

        assert schemas["EntityMockAnimalList"]["properties"]["data"]["items"][
            "properties"
        ]["attributes"]["properties"]["name"]

    @mock.patch("minty_pyramid.code_generation.entities.import_module")
    def test_generate_json(self, mock_importlib):
        eg = self._get_mocked_generator()

        assert eg.generate_json().startswith("{")

    @mock.patch("minty_pyramid.code_generation.open")
    @mock.patch("minty_pyramid.code_generation.entities.EntityGenerator")
    def test_can_generate_files(self, mock_entity_generator, mock_open):
        runner = CliRunner()

        filename = "generate_entities.json"
        assert runner.invoke(
            generate_entities,
            [
                "--filename",
                filename,
                "--entitymod",
                "zsnl_domains.case_management.entities",
            ],
        )
