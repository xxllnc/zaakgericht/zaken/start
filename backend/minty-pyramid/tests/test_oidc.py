# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import jose.constants
import jose.jwk
import jose.jwt
import json
from cryptography.hazmat.backends import default_backend as CryptographyBackend
from cryptography.hazmat.primitives.asymmetric import ec as CryptographyEc
from minty_pyramid import oidc
from unittest import mock
from uuid import uuid4

EC_KEY = CryptographyEc.generate_private_key(
    curve=CryptographyEc.SECP521R1(), backend=CryptographyBackend()
)
EC_PUBLIC_KEY = EC_KEY.public_key()

TEST_JWK_PRIVATE = jose.jwk.ECKey(
    key=EC_KEY, algorithm=jose.constants.ALGORITHMS.ES512
)
TEST_JWK = jose.jwk.ECKey(
    key=EC_PUBLIC_KEY, algorithm=jose.constants.ALGORITHMS.ES512
)


class MockRedis:
    def __init__(self, initial_cache_content):
        self.cache_content = initial_cache_content
        self.calls = []

    def __getitem__(self, key):
        self.calls.append(("__getitem__", key))
        return self.cache_content[key]

    def set(self, key, value, **kwargs):
        self.calls.append(("set", key, value, kwargs))
        self.cache_content[key] = value


@mock.patch("minty_pyramid.oidc.requests")
def test_parse_token(mock_requests):
    mock_redis = MockRedis({})

    instance_uuid = str(uuid4())
    user_uuid = str(uuid4())

    access_token = jose.jwt.encode(
        claims={
            "aud": "test_client",
            "sub": user_uuid,
            "scope": "scope1 scope2 scope3",
        },
        key=TEST_JWK_PRIVATE,
        algorithm=jose.constants.ALGORITHMS.ES512,
    )

    mock_metadata_response = mock.Mock()
    mock_metadata_response.json.return_value = {
        "jwks_uri": "https://example.com/jwks"
    }
    mock_keys_response = mock.Mock()
    mock_keys_response.json.return_value = {"keys": [TEST_JWK.to_dict()]}

    mock_requests.get.side_effect = [
        mock_metadata_response,
        mock_keys_response,
    ]

    parsed_token = oidc.parse_token(
        access_token=access_token,
        oidc_client_id="test_client",
        oidc_endpoint_config="https://example.com/oidc",
        instance_uuid=instance_uuid,
        cache=mock_redis,  # type: ignore
    )

    assert mock_redis.cache_content == {
        f"oidc_cache:{instance_uuid}:metadata": json.dumps(
            {"jwks_uri": "https://example.com/jwks"}
        ),
        f"oidc_cache:{instance_uuid}:jwks": json.dumps(
            {"keys": [TEST_JWK.to_dict()]}
        ),
    }
    assert parsed_token["user_uuid"] == user_uuid
    assert parsed_token["scope"] == {"scope1", "scope2", "scope3"}

    # It should use the cache, not do HTTP requests again (doing so
    # would lead to "pop from empty list" errors here)
    parsed_token = oidc.parse_token(
        access_token=access_token,
        oidc_client_id="test_client",
        oidc_endpoint_config="https://example.com/oidc",
        instance_uuid=instance_uuid,
        cache=mock_redis,  # type: ignore
    )
    assert parsed_token["user_uuid"] == user_uuid
    assert parsed_token["scope"] == {"scope1", "scope2", "scope3"}

    assert len(mock_redis.calls) == 6

    # First parse: check if OIDC metadata is cached (no)
    assert mock_redis.calls[0][0] == "__getitem__"

    # First parse: set freshly downloaded OIDC metadata
    assert mock_redis.calls[1][0] == "set"

    # First parse: check if JWKS is cached (no)
    assert mock_redis.calls[2][0] == "__getitem__"

    # First parse: set freshly downloaded JWKS
    assert mock_redis.calls[3][0] == "set"

    # Second parse: check if OIDC metadata is cached (yes)
    assert mock_redis.calls[4][0] == "__getitem__"

    # Second parse: check if JWKS is cached (yes)
    assert mock_redis.calls[5][0] == "__getitem__"
