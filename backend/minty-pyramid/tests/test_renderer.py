# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import decimal
import json
from minty.entity import Entity, EntityCollection, EntityResponse, ValueObject
from minty_pyramid.renderer import jsonapi
from pydantic.v1 import Field
from pyramid import testing
from typing import List, Optional, Set
from uuid import uuid4

now = datetime.datetime.now(tz=datetime.timezone.utc)


class MockEntityNestedRelation(Entity):
    entity_id = uuid4()
    entity_type = "mock_nested_relationship"
    entity_meta_summary = "Summarily"

    name: str = "Testnestedrelationship"


class MockEntityRelation(Entity):
    entity_id = uuid4()
    entity_type = "mock_relationship"
    entity_meta_summary = "Summarily"

    name: str = "Testrelationship"

    entity_relationships: List = []
    nested_relation: Optional[MockEntityNestedRelation] = None


class MockValueObject(ValueObject):
    name: str = "mock_value_object_name"
    id: str = "mock_value_object_id"
    aliased: str = Field("mock_value_object_aliased", alias="alias")


class MockEntityClass(Entity):
    entity_id = uuid4()
    entity_type = "mock_class"
    entity_relationships: list[str] = ["another_class", "another_classes"]

    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "entity_meta_test",
        "entity_meta_test_falsey",
    ]

    entity_meta_test: str = "Some meta"
    entity_meta_test_falsey: List[str] = Field(default_factory=list)

    uuid: Optional[str] = None

    name: Optional[str] = "Testname"
    date: datetime.datetime = now
    a_set: Set[str] = {"first", "second"}

    another_class: Optional[MockEntityRelation] = None

    another_classes: Optional[List[MockEntityRelation]] = None
    value_object_list: Optional[List[MockValueObject]] = None
    decimal_value_1: Optional[decimal.Decimal] = None
    decimal_value_2: Optional[decimal.Decimal] = None
    decimal_value_3: Optional[decimal.Decimal] = None


class MockSimpleEntityClass(Entity):
    entity_id = uuid4()
    entity_type = "mock_simple_class"

    uuid: Optional[str] = None
    name: Optional[str] = "Testname"


class MockView:
    def create_link_from_entity(self, entity):
        if entity.entity_type == "mock_class":
            return f"/api/v2/test/{entity.entity_id}"
        else:
            return None


class TestJSONApiRenderer:
    def _makeOne(self, **kw):
        return jsonapi(**kw)

    def test_adapters(self):
        renderer = self._makeOne()(None)

        request = testing.DummyRequest()
        mock_entity = MockEntityClass(
            another_class=MockEntityRelation(),
            another_classes=[MockEntityRelation(), MockEntityRelation()],
            value_object_list=[MockValueObject(), MockValueObject()],
            decimal_value_1=decimal.Decimal("12.5"),
            decimal_value_2=None,
            decimal_value_3=decimal.Decimal("10000.1234"),
        )
        result = json.loads(
            renderer(
                mock_entity,
                {"request": request, "view": MockView()},
            )
        )

        assert result
        assert result["id"]
        assert result["meta"]["test"] == "Some meta"
        assert result["meta"]["test_falsey"] == []
        assert result["attributes"]["name"] == "Testname"
        assert result["attributes"]["a_set"] == ["first", "second"]
        assert result["attributes"]["date"] == now.isoformat()
        assert result["attributes"]["decimal_value_1"] == "12.5"
        assert result["attributes"]["decimal_value_2"] is None
        assert result["attributes"]["decimal_value_3"] == "10000.1234"
        assert result["attributes"]["value_object_list"] == [
            {
                "id": "mock_value_object_id",
                "name": "mock_value_object_name",
                "alias": "mock_value_object_aliased",
            },
            {
                "id": "mock_value_object_id",
                "name": "mock_value_object_name",
                "alias": "mock_value_object_aliased",
            },
        ]
        assert (
            result["links"]["self"] == f"/api/v2/test/{mock_entity.entity_id}"
        )
        assert (
            result["relationships"]["another_class"]["data"]["type"]
            == "mock_relationship"
        )
        assert (
            result["relationships"]["another_class"]["meta"]["summary"]
            == "Summarily"
        )
        assert isinstance(result["relationships"]["another_classes"], List)
        assert (
            result["relationships"]["another_classes"][0]["meta"]["summary"]
            == "Summarily"
        )
        assert "links" not in result["relationships"]["another_class"]

    def test_entity_collection_adapter(self):
        ec = EntityCollection(
            [
                MockEntityClass(another_class=MockEntityRelation()),
                MockEntityClass(another_class=MockEntityRelation()),
            ]
        )

        request = testing.DummyRequest()
        assert jsonapi._entity_collection_adapter(None, ec, request) == [
            e for e in ec
        ]

    def test_entity_response_adapter(self):
        ec = EntityResponse(
            MockEntityClass(another_class=MockEntityRelation()),
        )

        request = testing.DummyRequest()
        assert jsonapi._entity_response_adapter(None, ec, request) == ec.entity

    def test_renderer_nested_relationship(self):
        renderer = self._makeOne()(None)

        request = testing.DummyRequest()
        mock_entity = MockEntityClass(
            another_class=MockEntityRelation(
                entity_relationships=["nested_relation"],
                nested_relation=MockEntityNestedRelation(),
            ),
        )
        result = json.loads(
            renderer(
                mock_entity,
                {"request": request, "view": MockView()},
            )
        )

        assert result

        assert (
            result["relationships"]["another_class"]["data"]["type"]
            == "mock_relationship"
        )
        assert result["relationships"]["another_class"]["relationships"][
            "nested_relation"
        ]

        assert (
            result["relationships"]["another_class"]["relationships"][
                "nested_relation"
            ]["data"]["type"]
            == "mock_nested_relationship"
        )

        assert (
            result["relationships"]["another_class"]["relationships"][
                "nested_relation"
            ]["meta"]["summary"]
            == "Summarily"
        )

    def test_meta_not_present(self):
        renderer = self._makeOne()(None)

        request = testing.DummyRequest()
        mock_entity = MockSimpleEntityClass()
        result = json.loads(
            renderer(
                mock_entity,
                {"request": request, "view": MockView()},
            )
        )

        assert result
        assert result["id"]
        assert "meta" not in result.keys()
