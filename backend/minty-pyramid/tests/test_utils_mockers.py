# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty_pyramid.utils.mockers as mockers
from unittest import mock


class TestExceptionHandlers:
    def setup_method(self):
        self.user_info = None

    def view_mock(self, request, user_info):
        self.request = request
        self.user_info = user_info

    def test_mock_protected_route(self):
        view_wrapper = mockers.mock_protected_route(self.view_mock)
        assert str(type(view_wrapper)) == "<class 'function'>"

        view_wrapper(mock.MagicMock())
        assert self.user_info is not None
