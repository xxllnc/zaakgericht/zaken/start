# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.entity
import pytest
from minty import entity
from minty_pyramid.views import pydantic_entity
from pydantic.v1 import Field
from pyramid import httpexceptions
from pyramid.response import Response
from unittest import mock
from uuid import UUID, uuid4


class Dog(entity.Entity):
    entity_type = "dog"

    uuid: UUID = Field(
        ..., title="Internal identifier of this specific object type version"
    )
    name: str = Field(..., title="Name of this object type")

    entity_id__fields: list[str] = ["uuid"]


class DogView(pydantic_entity.JSONAPIEntityView):
    view_mapper = {
        "GET": {
            "get_dog": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_dog_by_uuid",
                "from": {"request_params": {"uuid": "uuid"}},
            },
            "get_dog_with_puppies": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_dog_by_uuid",
                "from": {
                    "request_params": {"uuid": "uuid", "includes": "includes"}
                },
            },
            "download_dog": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "download_dog_by_uuid",
                "from": {"request_params": {"uuid": "uuid"}},
            },
            "filter_dogs": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_dog_by_uuid",
                "from": {
                    "request_params": {
                        "filter[]": "filter_params",
                        "name": "name",
                        "age": {"type": int, "name": "dog_age"},
                    }
                },
            },
            "get_dog_list": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_dog_by_uuid",
                "from": {},
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
        },
        "POST": {
            "create_dog": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_dog",
                "from": {
                    "request_params": {"uuid": "uuid"},
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "dog",
            },
            "update_dog": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_dog",
                "from": {
                    "request_params": {"uuid": "uuid"},
                    "json": {"name": "name", "size": "size"},
                },
                "command_primary": "uuid",
                "entity_type": "dog",
            },
            "update_dogs": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_dog",
                "from": {
                    "request_params": {"uuids": "uuids"},
                    "json": {"_all": True},
                },
                "command_primary": "uuids",
                "entity_type": "dog",
            },
            "update_animal": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_animal",
                "from": {
                    "request_params": {"uuid": "uuid", "type": "type"},
                    "json": {"name": "name", "size": "size"},
                },
                "command_primary": "uuid",
                "entity_type": "type",
            },
            "create_hippo": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_hippo",
                "from": {
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "hippo",
                "generate_missing_id": "uuid",
            },
            "create_hippo_version": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_hippo",
                "from": {
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "hippo",
                "generate_missing_id": ["uuid", "version_uuid"],
            },
        },
    }

    def wrap_update_dog(self, run, params):
        assert "update_dog" in str(run)
        assert params["name"]
        assert params["uuid"]


class DogViewWithLink(DogView):
    def create_link_from_entity(
        self, entity=None, entity_type=None, entity_id=None
    ):
        if entity is not None:
            entity_type = entity.entity_type
            entity_id = str(entity.entity_id)

        if entity_type == "dog":
            return self.request.route_url(
                "get_dog_object", _query={"uuid": entity_id}, _scheme="https"
            )


class TestCQRSPyramidView:
    view = None

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_a_proper_query_response(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog"
        request = mock.MagicMock(method="GET", matched_route=matched_route)
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        dog_uuid = uuid4()
        get_query_instance_mock = mock.MagicMock()
        get_query_instance_mock.get_dog_by_uuid.return_value = Dog(
            name="Barf", uuid=dog_uuid
        )
        self.view.request.get_query_instance.return_value = (
            get_query_instance_mock
        )

        response = self.view()
        assert response["data"]
        assert response["links"]
        assert response["links"]["self"]
        assert isinstance(response["data"], Dog)

        ###
        ### Test exceptions
        ###

        get_query_instance_mock.get_dog_by_uuid.return_value = None

        # self.view.request = request
        empty_data = self.view()
        assert empty_data["data"] is None

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_a_proper_query_redirect_response(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        matched_route = mock.MagicMock()
        type(matched_route).name = "download_dog"
        request = mock.MagicMock(method="GET", matched_route=matched_route)
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        get_query_instance_mock = mock.MagicMock()
        get_query_instance_mock.download_dog_by_uuid.return_value = (
            entity.RedirectResponse(
                location="https://here_you_can_download_dogs.com/"
            )
        )
        self.view.request.get_query_instance.return_value = (
            get_query_instance_mock
        )

        response = self.view()
        assert isinstance(response, Response)
        assert response.location == "https://here_you_can_download_dogs.com/"
        assert response.status_code == 302

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_a_proper_command_response(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        dog_uuid = uuid4()

        matched_route = mock.MagicMock()
        type(matched_route).name = "create_dog"
        request = mock.MagicMock(
            method="POST",
            matched_route=matched_route,
            params={"uuid": dog_uuid},
            json_body={"name": "Barf"},
        )

        def route_url_check(action, _query, _scheme):
            assert action == "get_dog_object"
            assert _query == {"uuid": dog_uuid}
            assert _scheme == "https"
            return "some-url"

        request.route_url.side_effect = route_url_check
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        get_command_instance_mock = mock.MagicMock()
        get_command_instance_mock.create_dog.return_value = Dog(
            name="Barf", uuid=dog_uuid
        )
        self.view.request.get_command_instance.return_value = (
            get_command_instance_mock
        )

        response = self.view()
        assert "data" in response
        assert "links" in response["data"]
        assert response["data"]["id"] == dog_uuid
        assert response["data"]["type"] == "dog"
        assert response["data"]["links"]["self"] == "some-url"

        ###
        ### Test exceptions
        ###

        request = mock.MagicMock(
            method="POST",
            matched_route=matched_route,
            params={"uuidnot": dog_uuid},
        )

        self.view.request = request
        empty_data = self.view()
        assert empty_data["data"] is None

        with pytest.raises(httpexceptions.HTTPForbidden) as excinfo:
            mock_check_user_permission.return_value = None
            self.view()

        assert "Access was denied to this resource" in str(excinfo.value)

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_a_proper_command_response_with_json_body(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        dog_uuid = uuid4()

        matched_route = mock.MagicMock()
        type(matched_route).name = "update_dog"
        request = mock.MagicMock(
            method="POST",
            matched_route=matched_route,
            params={"uuid": dog_uuid},
            json_body={"name": "Barf"},
        )

        def route_url_check(action, _query, _scheme):
            assert action == "get_dog_object"
            assert _query == {"uuid": dog_uuid}
            assert _scheme == "https"
            return

        request.route_url.side_effect = route_url_check
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        get_command_instance_mock = mock.MagicMock()
        get_command_instance_mock.create_dog.return_value = Dog(
            name="Barf", uuid=dog_uuid
        )
        self.view.request.get_command_instance.return_value = (
            get_command_instance_mock
        )

        response = self.view()
        assert "data" in response
        assert "links" not in response["data"]
        assert response["data"]["id"] == dog_uuid
        assert response["data"]["type"] == "dog"

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_a_command_response_with_json_body_no_links(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        dog_uuid = uuid4()

        matched_route = mock.MagicMock()
        type(matched_route).name = "update_dog"
        request = mock.MagicMock(
            method="POST",
            matched_route=matched_route,
            params={"uuid": dog_uuid},
            json_body={"name": "Barf"},
        )

        def route_url_check(action, _query, _scheme):
            assert action == "get_dog_object"
            assert _query == {"uuid": dog_uuid}
            assert _scheme == "https"
            return "some-url"

        request.route_url.side_effect = route_url_check
        self.view = DogView(context=mock.MagicMock(), request=request)

        get_command_instance_mock = mock.MagicMock()
        get_command_instance_mock.create_dog.return_value = Dog(
            name="Barf", uuid=dog_uuid
        )
        self.view.request.get_command_instance.return_value = (
            get_command_instance_mock
        )

        response = self.view()
        assert "data" in response
        assert "links" not in response["data"]
        assert response["data"]["id"] == dog_uuid
        assert response["data"]["type"] == "dog"

    def test_use_array_in_query_params(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={
                "filter[title]": "bla",
                "nofilter": "not",
                "filter[name]": "frits",
                "name": "wim",
            },
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        params = view._collect_params(
            DogViewWithLink.view_mapper["GET"]["filter_dogs"]
        )

        assert params == {
            "filter_params": {"name": "frits", "title": "bla"},
            "name": "wim",
        }

    def test_use_typed_query_params(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={"age": "42"},
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        params = view._collect_params(
            DogViewWithLink.view_mapper["GET"]["filter_dogs"]
        )

        assert params == {"dog_age": 42}

    def test_use_typed_query_params_wrong(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={"age": "abc"},
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        with pytest.raises(httpexceptions.HTTPBadRequest):
            view._collect_params(
                DogViewWithLink.view_mapper["GET"]["filter_dogs"]
            )

    def test_use_paging(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog_list"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={"page": 1, "page_size": 10, "max_results": 100},
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        params = view._collect_params(
            DogViewWithLink.view_mapper["GET"]["get_dog_list"]
        )

        assert params == {"page": 1, "page_size": 10}

    def test_paging_max_size_error(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog_list"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={"page": 1, "page_size": 101, "max_results": 100},
        )

        with pytest.raises(httpexceptions.HTTPBadRequest) as exception_info:
            view = DogViewWithLink(context=mock.MagicMock(), request=request)
            view._collect_params(
                DogViewWithLink.view_mapper["GET"]["get_dog_list"]
            )
        assert exception_info.value.json == {
            "errors": [{"title": "Requested page size 101 > max size 100."}]
        }

    def test_paging_min_size_error(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog_list"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={"page": 1, "page_size": -1, "max_results": 100},
        )

        with pytest.raises(httpexceptions.HTTPBadRequest) as exception_info:
            view = DogViewWithLink(context=mock.MagicMock(), request=request)
            view._collect_params(
                DogViewWithLink.view_mapper["GET"]["get_dog_list"]
            )
        assert exception_info.value.json == {
            "errors": [{"title": "'page_size' must be >= 1"}]
        }

    def test_paging_min_page_error(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog_list"
        request = mock.MagicMock(
            method="GET",
            matched_route=matched_route,
            params={"page": -1, "page_size": 1, "max_results": 100},
        )

        with pytest.raises(httpexceptions.HTTPBadRequest) as exception_info:
            view = DogViewWithLink(context=mock.MagicMock(), request=request)
            view._collect_params(
                DogViewWithLink.view_mapper["GET"]["get_dog_list"]
            )
        assert exception_info.value.json == {
            "errors": [{"title": "'page' must be >= 1"}]
        }

    def test_meta_total(self):
        mock_response = minty.entity.EntityCollection(
            total_results=123, entities=[], cursor={"next": "next"}
        )
        mock_request = mock.Mock(name="request")
        view = DogViewWithLink(context=mock.MagicMock(), request=mock_request)
        transformed = view.transform_response_from_command_or_query(
            response=mock_response,
            calldata={
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_dog_by_uuid",
                "from": {"request_params": {"uuid": "uuid"}},
            },
            params={},
        )

        assert transformed == {
            "data": mock_response,
            "links": {"self": mock_request.current_route_path()},
            "meta": {"cursor": {"next": "next"}, "total_results": 123},
        }

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_included_entities(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        matched_route = mock.MagicMock()
        type(matched_route).name = "get_dog_with_puppies"
        request = mock.MagicMock(method="GET", matched_route=matched_route)
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        get_query_instance_mock = mock.MagicMock()
        included_entities = [
            Dog(name="dog-1", uuid=uuid4()),
            Dog(name="dog-2", uuid=uuid4()),
        ]
        mock_response = minty.entity.EntityCollection(
            entities=[], included_entities=included_entities
        )
        get_query_instance_mock.get_dog_by_uuid.return_value = mock_response
        self.view.request.get_query_instance.return_value = (
            get_query_instance_mock
        )

        response = self.view()

        assert response["included"] == included_entities

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_a_proper_command_response_with_multiple_entities(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        dog_uuids = [uuid4(), uuid4()]

        matched_route = mock.MagicMock()
        type(matched_route).name = "update_dogs"
        request = mock.MagicMock(
            method="POST",
            matched_route=matched_route,
            params={"uuids": dog_uuids},
            json_body={},
        )

        def route_url_check(action, _query, _scheme):
            assert action == "get_dog_object"
            assert _query["uuid"] in dog_uuids
            assert _scheme == "https"
            dog_uuid = _query["uuid"]
            return f"https://test.nl/api/get_dog_object?uuid={dog_uuid}"

        request.route_url.side_effect = route_url_check
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        get_command_instance_mock = mock.MagicMock()
        self.view.request.get_command_instance.return_value = (
            get_command_instance_mock
        )

        response = self.view()
        assert "data" in response
        assert "links" not in response["data"]

        assert response["data"][0]["id"] == dog_uuids[0]
        assert response["data"][0]["type"] == "dog"
        assert (
            response["data"][0]["links"]["self"]
            == f"https://test.nl/api/get_dog_object?uuid={dog_uuids[0]}"
        )

        assert response["data"][1]["id"] == dog_uuids[1]
        assert response["data"][1]["type"] == "dog"
        assert (
            response["data"][1]["links"]["self"]
            == f"https://test.nl/api/get_dog_object?uuid={dog_uuids[1]}"
        )

    @mock.patch("minty_pyramid.views.pydantic_entity.get_logged_in_user")
    @mock.patch("minty_pyramid.views.pydantic_entity.check_user_permission")
    def test_get_command_response_which_supports_multiple_types_of_entity_type(
        self, mock_check_user_permission, mock_logged_in_user
    ):
        dog_uuid = uuid4()

        matched_route = mock.MagicMock()
        type(matched_route).name = "update_animal"
        request = mock.MagicMock(
            method="POST",
            matched_route=matched_route,
            params={"uuid": dog_uuid, "type": "dog"},
            json_body={"name": "Barf", "size": "34"},
        )

        def route_url_check(action, _query, _scheme):
            assert action == "get_dog_object"
            assert _scheme == "https"
            dog_uuid = _query["uuid"]
            return f"https://test.nl/api/get_dog_object?uuid={dog_uuid}"

        request.route_url.side_effect = route_url_check
        self.view = DogViewWithLink(context=mock.MagicMock(), request=request)

        get_command_instance_mock = mock.MagicMock()
        self.view.request.get_command_instance.return_value = (
            get_command_instance_mock
        )

        response = self.view()
        assert "data" in response
        assert "links" in response["data"]

        assert response["data"]["id"] == dog_uuid
        assert response["data"]["type"] == "dog"
        assert (
            response["data"]["links"]["self"]
            == f"https://test.nl/api/get_dog_object?uuid={dog_uuid}"
        )

    def test_generate_missing_id(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "create_hippo"
        request = mock.MagicMock(
            method="{POST}",
            matched_route=matched_route,
            params={},
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        params = view._collect_params(
            DogViewWithLink.view_mapper["POST"]["create_hippo"]
        )
        assert params["uuid"]
        assert isinstance(params["uuid"], UUID)

    def test_generate_missing_ids(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "create_hippo_version"
        request = mock.MagicMock(
            method="{POST}",
            matched_route=matched_route,
            params={},
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        params = view._collect_params(
            DogViewWithLink.view_mapper["POST"]["create_hippo_version"]
        )
        assert params["uuid"] and isinstance(params["uuid"], UUID)
        assert params["version_uuid"] and isinstance(
            params["version_uuid"], UUID
        )

    def test_generate_missing_id_no_overwrite(self):
        matched_route = mock.MagicMock()
        type(matched_route).name = "create_hippo_version"
        version_uuid = uuid4()
        request = mock.MagicMock(
            method="{POST}",
            matched_route=matched_route,
            json_body={"version_uuid": version_uuid},
        )
        view = DogViewWithLink(context=mock.MagicMock(), request=request)

        params = view._collect_params(
            DogViewWithLink.view_mapper["POST"]["create_hippo_version"]
        )
        assert params["uuid"] and isinstance(params["uuid"], UUID)
        assert (
            params["version_uuid"] and params["version_uuid"] == version_uuid
        )
