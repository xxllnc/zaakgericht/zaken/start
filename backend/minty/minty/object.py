# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic.v1 import BaseModel, Field, ValidationError
from pydantic.v1.generics import GenericModel

__all__ = ["Field", "GenericModel", "IntrospectableObject", "ValidationError"]


class IntrospectableObject(BaseModel):
    """Introspectable object based on pydantic"""

    class Config:
        validate_assignment = True
        arbitrary_types_allowed = True
