# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import Base
from .infrastructure import InfrastructureFactory
from typing import TYPE_CHECKING, Any, Optional

if TYPE_CHECKING:
    # This causes an import loop if imported outside of type checking
    import minty.cqrs


class RepositoryBase(Base):
    """Base class for repositories using the Minty CQRS/DDD framework"""

    REQUIRED_INFRASTRUCTURE: dict[str, Any]
    REQUIRED_INFRASTRUCTURE_RO: dict[str, Any]
    REQUIRED_INFRASTRUCTURE_RW: dict[str, Any]

    read_only: bool
    context: str
    infrastructure_factory: InfrastructureFactory
    infrastructure_factory_ro: InfrastructureFactory
    cache: dict[str, Any]
    event_service: "minty.cqrs.EventService"

    def __init__(
        self,
        infrastructure_factory: InfrastructureFactory,
        infrastructure_factory_ro: InfrastructureFactory,
        context: str,
        read_only: bool,
        event_service,
    ):
        self.infrastructure_factory = infrastructure_factory
        self.infrastructure_factory_ro = infrastructure_factory_ro
        self.context = context
        self.event_service = event_service
        self.read_only = read_only
        self.cache = {}

    def _get_infrastructure(self, name: str, read_only: bool = False):
        if self.read_only or read_only:
            return self.infrastructure_factory_ro.get_infrastructure(
                context=self.context, infrastructure_name=name
            )
        else:
            return self.infrastructure_factory.get_infrastructure(
                context=self.context, infrastructure_name=name
            )


class Repository(RepositoryBase):
    _for_entity: str
    _events_to_calls: dict[str, str]

    def save(
        self,
        user_info: Optional["minty.cqrs.UserInfo"] = None,
        dry_run: bool = False,
    ) -> None:
        """Uses the mapping _events_to_calls for calling the repo methods"""

        for ev in self.event_service.get_events_by_type(
            entity_type=self._for_entity
        ):
            if not ev.processed:
                method_to_call = getattr(
                    self, self._events_to_calls[ev.event_name]
                )

                method_to_call(ev, user_info, dry_run)
            if not dry_run:
                ev.processed = True


class RepositoryFactory(Base):
    """Create context-specific "repository" instances for domains"""

    __slots__ = [
        "infrastructure_factory",
        "infrastructure_factory_ro",
        "repositories",
    ]

    infrastructure_factory: InfrastructureFactory
    infrastructure_factory_ro: InfrastructureFactory
    repositories: dict[str, type[RepositoryBase]]

    def __init__(
        self,
        infrastructure_factory: InfrastructureFactory,
        infrastructure_factory_ro: InfrastructureFactory,
    ):
        """Initialize the repository factory with an infrastructure factory"""
        self.infrastructure_factory = infrastructure_factory
        self.infrastructure_factory_ro = infrastructure_factory_ro
        self.repositories = {}

    def flush_local_storage(self) -> None:
        """Flush pre-request local storage of infrastructure factories"""

        self.infrastructure_factory.flush_local_storage()
        self.infrastructure_factory_ro.flush_local_storage()

    def register_repository(
        self, name: str, repository: type[RepositoryBase]
    ) -> None:
        """Register a repository class with the repository factory."""
        self.repositories[name] = repository

    def get_repository(
        self,
        name: str,
        read_only: bool,
        context: str,
        event_service: "minty.cqrs.EventService | None" = None,
    ) -> RepositoryBase:
        """Retrieve a repository, given a name and optionally a context."""

        self.logger.debug(
            f"Creating repository of type '{name}' with context "
            + f"'{context}'"
        )

        repo_class = self.repositories[name]

        repo = repo_class(
            context=context,
            infrastructure_factory=self.infrastructure_factory,
            infrastructure_factory_ro=self.infrastructure_factory_ro,
            event_service=event_service,
            read_only=read_only,
        )

        return repo
