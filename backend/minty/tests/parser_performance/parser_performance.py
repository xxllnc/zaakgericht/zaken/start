# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import os
from minty.infrastructure import _parse_global_config

workdir = os.getcwd()


def run_apache_timing():
    """Parse both global and hostname-specific configuration files."""
    file1 = os.path.join(
        workdir, "tests/parser_performance/data/zaaksysteem.conf"
    )
    file2 = os.path.join(workdir, "tests/parser_performance/data/default.conf")

    _parse_global_config(file1)
    _parse_global_config(file2)


def run_json_timing():
    """Parse both global and hostname-specific configuration files."""
    file1 = os.path.join(
        workdir, "tests/parser_performance/data/zaaksysteem.json"
    )
    file2 = os.path.join(workdir, "tests/parser_performance/data/default.json")
    _parse_global_config(file1)
    _parse_global_config(file2)


def run_function(func, seconds):
    """Measure how many times a function can run in specified seconds.

    :param func: function to execute
    :type func: function
    :param seconds: amount of seconds to run function
    :type seconds: datetime.timedelta
    :return: n times function has completed run
    :rtype: int
    """
    n = 0
    run = True
    now = datetime.datetime.now()
    while run:
        func()
        n += 1
        stop = datetime.datetime.now()
        if (stop - now) >= seconds:
            run = False

    return n


def compare_json_and_apache():
    """Compare speed of JSON and Apache Parser."""
    runtime = datetime.timedelta(seconds=5)
    apache = run_function(run_apache_timing, seconds=runtime)
    json = run_function(run_json_timing, seconds=runtime)
    faster = json / apache
    print(f"json parsing   : {json} in {runtime}")  # noqa: T201
    print(f"apache parsing : {apache} in {runtime}")  # noqa: T201
    print(f"json is {round(faster, 2)} times faster")  # noqa: T201


if __name__ == "__main__":
    compare_json_and_apache()
