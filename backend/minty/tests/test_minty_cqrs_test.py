# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.cqrs import UserInfo
from minty.cqrs.test import InfraFactoryMock, MockRepositoryFactory, TestBase
from unittest.mock import MagicMock
from uuid import uuid4


class TestTheTestClass(TestBase):
    pass


class TestTheTests:
    def test_load_command_instance(self):
        testclass = TestBase()
        testclass.load_command_instance(MagicMock())

        assert testclass.session
        assert testclass.cmd

        user_uuid = str(uuid4())
        user_info = testclass.cmd.user_info.from_dict(
            {"user_uuid": user_uuid, "permissions": [], "type": "some_type"}
        )
        assert isinstance(user_info, UserInfo)

    def test_load_query_instance(self):
        testclass = TestBase()
        testclass.load_query_instance(MagicMock())

        assert testclass.session
        assert testclass.qry

    def test_load_command_instance_complex(self):
        def mockrepo(
            infrastructure_factory,
            infrastructure_factory_ro,
            context,
            event_service,
            read_only,
        ):
            return "repo"

        class MockDomain:
            REQUIRED_REPOSITORIES = {"foo": mockrepo, "bar": mockrepo}

            def get_command_instance(
                self, repository_factory, event_service, context, user_uuid
            ):
                command_mock = MagicMock()
                command_mock.repo_factory = repository_factory
                return command_mock

        testclass = TestBase()
        testclass.load_command_instance(MockDomain())

        assert testclass.cmd.repo_factory.repositories == {
            "foo": "repo",
            "bar": "repo",
        }

    def test_load_query_instance_complex(self):
        def mockrepo(
            infrastructure_factory,
            infrastructure_factory_ro,
            context,
            event_service,
            read_only,
        ):
            return "repo"

        class MockDomain:
            REQUIRED_REPOSITORIES = {"foo": mockrepo, "bar": mockrepo}

            def get_query_instance(
                self, repository_factory, context, user_uuid
            ):
                command_mock = MagicMock()
                command_mock.repo_factory = repository_factory
                return command_mock

        testclass = TestBase()
        testclass.load_query_instance(MockDomain())

        assert testclass.qry.repo_factory.repositories == {
            "foo": "repo",
            "bar": "repo",
        }

    def test_assert_has_event_name(self):
        testclass = TestBase()

        with pytest.raises(Exception) as excinfo:
            testclass.assert_has_event_name("TestClassCreated")

            assert "object has no attribute 'cmd'" in str(excinfo.value)

        testclass.load_command_instance(MagicMock())

        testclass.cmd.event_service.event_list = [
            MagicMock(event_name="TestClassCreated")
        ]

        # from pprint import pprint
        # for event in testclass.cmd.event_service.event_list:
        #   print(event)

        assert testclass.assert_has_event_name("TestClassCreated")
        assert testclass.assert_has_event_name(
            "TestClassCreated", "Custom descriptoin"
        )
        with pytest.raises(AssertionError):
            testclass.assert_has_event_name("TestClassCreatedd")

    def test_get_last_sql_query(self):
        testclass = TestBase()
        testclass.session = MagicMock()
        testclass.session.execute(MagicMock())
        assert testclass.get_last_sql_query()

    def test_get_cmd(self):
        testclass = TestBase()

        with pytest.raises(Exception) as excinfo:
            testclass.get_cmd()

        assert "first load the command_instance" in str(excinfo.value)

        testclass.load_command_instance(MagicMock())
        assert testclass.get_cmd()

    def test_get_infrastructure(self):
        testinfrafactory = InfraFactoryMock(infra={"database": MagicMock()})
        assert testinfrafactory.get_infrastructure(MagicMock(), "database")

    def test_get_repository(self):
        testrepository = MockRepositoryFactory("test", "test_ro")
        testrepository.repositories["test"] = MagicMock()
        assert testrepository.get_repository(
            "test", "blacontext", "blaeventservice", read_only=True
        )
