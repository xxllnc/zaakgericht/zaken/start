# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import date, timedelta
from minty.date_calculation import (
    NonBusinessDays,
    add_timedelta_to_date,
    next_business_day,
)
from unittest import mock


class TestCaseManagementQueries:
    @mock.patch("minty.date_calculation.next_business_day")
    def test_get_date_from_interval(self, next_business_day_mock):
        test_data = [
            {
                "interval_type": "months",
                "time": 1,
                "starting_date": date(2019, 1, 31),
                "expected_result": date(2019, 2, 28),
            },
            {
                "interval_type": "months",
                "time": 5,
                "starting_date": date(2019, 1, 31),
                "expected_result": date(2019, 6, 30),
            },
            {
                "interval_type": "months",
                "time": 11,
                "starting_date": date(2019, 1, 15),
                "expected_result": date(2019, 12, 15),
            },
            {
                "interval_type": "years",
                "time": 1,
                "starting_date": date(2019, 1, 1),
                "expected_result": date(2020, 1, 1),
            },
            {
                "interval_type": "years",
                "time": 1,
                "starting_date": date(2020, 2, 29),
                "expected_result": date(2021, 2, 28),
            },
            {
                "interval_type": "weeks",
                "time": 1,
                "starting_date": None,
                "expected_result": (date.today() + timedelta(weeks=1)),
            },
            {
                "interval_type": "days",
                "time": 14,
                "starting_date": None,
                "expected_result": (date.today() + timedelta(days=14)),
            },
            {
                "interval_type": "business_days",
                "time": 1,
                "starting_date": None,
                "expected_result": date(2222, 1, 1),
            },
        ]
        mock_next = mock.MagicMock()
        mock_next.__next__.return_value = date(2222, 1, 1)
        next_business_day_mock.return_value = mock_next

        for data in test_data:
            call_dict = {
                "interval_type": data["interval_type"],
                "time": data["time"],
                "starting_date": data["starting_date"],
            }
            result = add_timedelta_to_date(**call_dict)
            assert result == data["expected_result"]

        with pytest.raises(KeyError):
            add_timedelta_to_date(interval_type="centuries", time=12)

    def test_next_business_day(self):
        test_data = [
            {
                "date": date(2018, 12, 31),
                "expected_next_business_day": date(2019, 1, 2),
            },
            {
                "date": date(2020, 5, 4),
                "expected_next_business_day": date(2020, 5, 6),
            },
            {
                "date": date(2020, 4, 24),
                "expected_next_business_day": date(2020, 4, 28),
            },
        ]

        for data in test_data:
            nwd = next_business_day(initial_date=data["date"])
            res = next(nwd)
            assert res == data["expected_next_business_day"]

    def test_non_businessday_class(self):
        nbd = NonBusinessDays()
        res = nbd(2019)
        assert res == nbd.store[2019]
