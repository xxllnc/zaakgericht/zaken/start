# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import uuid
from datetime import datetime, timezone
from enum import Enum
from minty import cqrs, entity, exceptions
from pydantic.v1 import Field, ValidationError
from typing import Literal
from unittest import mock


class MockEntity(entity.EntityBase):
    entity_type: Literal["mini_entity"] = "mini_entity"

    def __init__(self):
        self.id = "id-12345"

    @property
    def entity_id(self):
        return self.id


class AnimalVersion(entity.ValueObject):
    version: int = 4


class AnimalAlias(entity.ValueObject):
    alias: str = Field("aliased", alias="aliased")


class Owner(entity.Entity):
    """Human Owner"""

    gender: str = entity.Field(
        None, title="Gender of the owner", regex="^male|female$"
    )
    name: str = entity.Field(..., doc="The name of the owner")

    # Required entity settings
    entity_type = "owner"


class Animal(entity.Entity):
    """Animals reunited"""

    # Required field, with description, None not allowed
    color: str = entity.Field(
        ..., title="Animal Color", description="Defines the color of this pet"
    )

    # Required field, without title, description, etc
    alive: bool = Field(...)

    # Optional field with default
    paws: int = 4

    # Optional field without default
    shelter: str | None = None
    location: str = entity.Field(
        None, title="City", description="City of animal"
    )

    owner: Owner | None = None

    # Required entity settings
    entity_type = "animal"
    _entity_relationships: list[str] = ["owner"]

    animal_version: AnimalVersion | None = None
    animal_alias: AnimalAlias | None = None

    @entity.Entity.event(name="AnimalUpdated", fire_always=True)
    def update(self, paws):
        self.paws = paws

    @classmethod
    @entity.Entity.event(name="AnimalCreated", fire_always=True)
    def create(cls, **kwargs):
        return cls(**kwargs)

    @entity.Entity.event(name="AnimalViewed", fire_always=True)
    def view_animal(self):
        pass  # no changes in the entity but force the event to be fired

    @entity.Entity.event(name="AnimalDeleted", fire_always=False)
    def delete(self):
        pass  # no changes in the entity, so no event will be fired

    @entity.Entity.event(name="AnimalPawsUpdated", fire_always=False)
    def update_paws(self, paws: int):
        self.paws = paws


class BadEntity(entity.Entity):
    @classmethod
    @entity.Entity.event(name="BadEntityCreated", fire_always=True)
    def create(cls, **kwargs):
        return


class MiniEntity(entity.Entity):
    entity_type = "mini_entity"

    size: int = Field(..., title="Size of mini entity")


class EnumColor(Enum):
    red = "red"
    black = "black"


class TestEventDecorator:
    def test_reflect(self):
        assert entity._reflect(value=None) is None
        assert entity._reflect(value=True) is True
        assert entity._reflect(value=10) == 10
        assert entity._reflect(value=10) == 10
        assert entity._reflect(value=4.8299829882398) == 4.8299829882398

        mock_ent = MockEntity()
        assert entity._reflect(value=mock_ent) == {
            "type": "MockEntity",
            "entity_id": "id-12345",
        }
        assert entity._reflect(value=[1, 2, 3, 4, True]) == [1, 2, 3, 4, True]
        assert entity._reflect(value=[[1, 2], [3, 4], [True]]) == [
            [1, 2],
            [3, 4],
            [True],
        ]
        assert entity._reflect(value=(1, 2, 3, 4, True)) == [1, 2, 3, 4, True]
        assert sorted(entity._reflect(value={"a", "b"})) == ["a", "b"]

        assert entity._reflect(value=[mock_ent, 2, 3, 4, True]) == [
            {"type": "MockEntity", "entity_id": "id-12345"},
            2,
            3,
            4,
            True,
        ]
        assert entity._reflect(value=[[mock_ent, 2], 3, 4, True]) == [
            [{"type": "MockEntity", "entity_id": "id-12345"}, 2],
            3,
            4,
            True,
        ]
        assert entity._reflect(value=[{"k": mock_ent}, 2, 3, 4, True]) == [
            {"k": {"type": "MockEntity", "entity_id": "id-12345"}},
            2,
            3,
            4,
            True,
        ]
        assert entity._reflect(value={"m": mock_ent}) == {
            "m": {"type": "MockEntity", "entity_id": "id-12345"}
        }

        dt = datetime.now(timezone.utc)
        assert entity._reflect(dt) == dt.isoformat()

        assert entity._reflect(
            Animal(color="white", alive=True, animal_version={"version": 6})
        ) == {
            "alive": True,
            "animal_alias": None,
            "animal_version": {"version": 6},
            "color": "white",
            "location": None,
            "owner": None,
            "paws": 4,
            "shelter": None,
        }

        assert entity._reflect(AnimalVersion(version=6)) == {"version": 6}
        assert entity._reflect(EnumColor("black")) == "black"


class TestEntity:
    def test_proper_subclassing(self):
        a = Animal(color="white", alive=False)

        with pytest.raises(ValidationError):
            a.paws = "four"

    def test_event_service(self):
        a = Animal(color="white", alive=True, _event_service="bla")

        assert a._event_service == "bla"

    def test_relationships(self):
        a = Animal(color="white", alive=True, _event_service="bla")

        assert "owner" in a._entity_relationships

    def test_change_log(self):
        event_service = mock.MagicMock()
        a = Animal(color="white", alive=True, _event_service=event_service)
        a.update(paws=2)

        assert a.paws == 2

    def test_object_data_per_object(self):
        a = Animal(color="white", alive=True, _event_service="bla")
        b = Animal(color="white", alive=True, _event_service="frits")

        assert a._event_service == "bla"
        assert b._event_service == "frits"

    def test_entity_dict(self):
        a = Animal(
            color="white",
            alive=True,
            owner=Owner(name="Frits"),
            animal_alias=AnimalAlias(),
        )

        assert a.entity_dict() == {
            "color": "white",
            "alive": True,
            "paws": 4,
            "shelter": None,
            "location": None,
            "owner": {"gender": None, "name": "Frits"},
            "animal_version": None,
            "animal_alias": {"aliased": "aliased"},
        }

    def test_event_decorator(self):
        event_service = mock.MagicMock()
        a = Animal.create(
            color="blue", alive=True, paws=3, _event_service=event_service
        )

        assert isinstance(a, Animal)

    def test_bad_entity(self):
        """Test to check the error message programmers get for entities that
        have a specific kind of bug."""

        event_service = mock.MagicMock()

        with pytest.raises(exceptions.ConfigurationConflict) as excinfo:
            BadEntity.create(
                color="blue", alive=True, paws=3, _event_service=event_service
            )

        assert "Return value of class method is not an entity." in str(
            excinfo.value
        )

    def test_capture_field_values(self):
        a = Animal(color="white", alive=True, owner=Owner(name="Frits"))
        a.capture_field_values(fields=["color"])

        assert a.entity_data == {
            "color": "white",
        }

    def test_animal_fire_always_event(self):
        event_service = cqrs.EventService(
            correlation_id=uuid.uuid4(),
            domain="unit-test",
            context="test",
            user_uuid=uuid.uuid4(),
        )
        a = Animal(
            color="white",
            alive=True,
            _event_service=event_service,
        )

        a.view_animal()

        assert (
            next(
                filter(
                    lambda event: event.event_name == "AnimalViewed",
                    event_service.event_list,
                )
            )
            is not None
        )

    def test_animal_event_no_changes(self):
        event_service = cqrs.EventService(
            correlation_id=uuid.uuid4(),
            domain="unit-test",
            context="test",
            user_uuid=uuid.uuid4(),
        )
        a = Animal(
            color="white",
            alive=True,
            _event_service=event_service,
        )

        a.delete()

        with pytest.raises(StopIteration):
            next(
                filter(
                    lambda event: event.event_name == "AnimalDeleted",
                    event_service.event_list,
                )
            )

    def test_animal_event_with_changes(self):
        # test if the event will be send if changes are recorded
        event_service = cqrs.EventService(
            correlation_id=uuid.uuid4(),
            domain="unit-test",
            context="test",
            user_uuid=uuid.uuid4(),
        )
        a = Animal(
            color="white",
            alive=True,
            _event_service=event_service,
        )

        a.update_paws(2)

        assert (
            next(
                filter(
                    lambda event: event.event_name == "AnimalPawsUpdated",
                    event_service.event_list,
                )
            )
            is not None
        )


def test_entity_collection():
    entities = [MiniEntity(size=5), MiniEntity(size=10), MiniEntity(size=100)]
    ec = entity.EntityCollection[MiniEntity](entities)

    iter_result = [e for e in ec]
    assert iter_result == entities


def test_entity_collection_included_entities():
    entities = [MiniEntity(size=5), MiniEntity(size=10), MiniEntity(size=100)]
    included_entities = [MiniEntity(size=20), MiniEntity(size=30)]
    ec = entity.EntityCollection(
        entities=entities, included_entities=included_entities
    )
    iter_result = [e for e in ec.included_entities]
    assert iter_result == included_entities


def test_entity_response():
    single_entity = MiniEntity(size=5)
    ec = entity.EntityResponse[MiniEntity](single_entity)

    assert ec.entity == single_entity
    assert ec.included_entities is None


def test_entity_response_included_entity():
    single_entity = MiniEntity(size=5)
    included_entity = MiniEntity(size=6)
    ec = entity.EntityResponse[MiniEntity](
        single_entity, included_entities=[included_entity]
    )

    assert ec.entity == single_entity
    assert ec.included_entities == [included_entity]


def test_redirect_response():
    rr = entity.RedirectResponse(location="https://google.com/")

    assert rr.location == "https://google.com/"
