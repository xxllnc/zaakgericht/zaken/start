# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.object import IntrospectableObject
from pydantic.v1 import Field, ValidationError


class Animal(IntrospectableObject):
    # Required field, with description, None not allowed
    color: str = Field(
        ..., title="Animal Color", description="Defines the color of this pet"
    )

    # Required field, without title, description, etc
    alive: bool = Field(...)

    # Optional field with default
    paws: int = 4

    # Optional field without default
    shelter: str | None = None
    location: str = Field(None, title="City", description="City of animal")


class TestIntrospectableAnimal:
    def test_setting_object_properties_from_init(self):
        a = Animal(color="white", alive=False)
        aa = Animal(color="black", alive=True)

        assert a.color == "white"
        assert a.alive is False
        assert aa.paws == 4

    def test_setting_object_properties_from_accessor(self):
        a = Animal(color="white", alive=False)

        assert a.color == "white"

        a.color = "black"

        assert a.color == "black"

    def test_setting_wrong_type_on_accessor(self):
        a = Animal(color="white", alive=False)

        with pytest.raises(ValidationError) as excinfo:
            a.paws = "four"

        assert "value is not a valid integer" in str(excinfo.value)

    def test_missing_required_field(self):
        with pytest.raises(ValidationError) as excinfo:
            Animal(color=None, alive=False)

        assert "none is not an allowed value" in str(excinfo.value)

    def test_setting_wrong_type_on_init(self):
        with pytest.raises(ValidationError) as excinfo:
            Animal(paws="white", alive=False)

        assert "value is not a valid integer" in str(excinfo.value)

    def test_dict(self):
        a = Animal(color="white", alive=False)

        assert {
            "alive": False,
            "color": "white",
            "location": None,
            "paws": 4,
            "shelter": None,
        } == a.dict()

    def test_schema(self):
        a = Animal(color="white", alive=False)

        assert {
            "description": "Introspectable object based on pydantic",
            "properties": {
                "alive": {"title": "Alive", "type": "boolean"},
                "color": {
                    "description": "Defines the color of this pet",
                    "title": "Animal Color",
                    "type": "string",
                },
                "location": {
                    "description": "City of animal",
                    "title": "City",
                    "type": "string",
                },
                "paws": {"default": 4, "title": "Paws", "type": "integer"},
                "shelter": {"title": "Shelter", "type": "string"},
            },
            "required": ["color", "alive"],
            "title": "Animal",
            "type": "object",
        } == a.schema()

    def test_entity_schema(self):
        pass

    def test_entity_dict(self):
        pass
