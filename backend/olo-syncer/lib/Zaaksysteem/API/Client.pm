package Zaaksysteem::API::Client;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::API::Client - Zaaksysteem API client

=head1 DESCRIPTION

This class astracts Zaaksysteem intra-service API interactions.

=head1 SYNOPSIS

    my $client = Zaaksysteem::API::Client->new(
        hostname => 'my.zaaksysteem.instance.tld',
        platform_key => 'abc123xyz'
    );

=cut

use BTTW::Tools;
use BTTW::Tools::UA qw[new_user_agent];
use HTTP::Request;
use HTTP::Request::Common ();
use HTTP::Headers;
use JSON::XS ();
use Scalar::Util qw[refaddr];
use URI;
use Zaaksysteem::API::Response;

=head1 ATTRIBUTES

=head2 user_agent

=cut

has user_agent => (
    is => 'rw',
    isa => 'LWP::UserAgent',
    builder => '_build_user_agent'
);

=head2 json

=cut

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    builder => '_build_json',
    handles => {
        _decode_body => 'decode',
        _encode_body => 'encode'
    }
);

=head2 timeout

=cut

has timeout => (
    is      => 'ro',
    isa     => 'Int',
    default => 10,
);

=head2 api_version

=cut

has api_version => (
    is => 'rw',
    isa => 'Int',
    default => 1
);

=head2 hostname

=cut

has hostname => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 platform_key

=cut

has platform_key => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 last_response

=cut

has last_response => (
    is => 'rw',
    isa => 'HTTP::Response',
    clearer => 'clear_last_response',
    predicate => 'has_last_response'
);

=head2 last_request

=cut

has last_request => (
    is => 'rw',
    isa => 'HTTP::Request',
    clearer => 'clear_last_request',
    predicate => 'has_last_request'
);

=head1 METHODS

=head2 request

Common interface for performing a request against the configured API.

Stores the request and response of the last call in L</last_response> and
L</last_request>.

=cut

sig request => 'HTTP::Request';

sub request {
    my $self = shift;
    my $request = shift;

    $self->clear_last_response;

    my $cloned_request = $request->clone;
    $cloned_request->header('ZS-Platform-Key' => '<redacted>');

    $self->last_request($cloned_request);

    my $response = $self->user_agent->request($request);

    $self->last_response($response);

    return $self->_build_response($response);
}

=head2 get

Performs an API request using the provided resource path.

    my $response = $client->get('file/78591c07-440c-4cc8-bc2f-412b2bd6d386');

=cut

sig get => 'Str, ?HashRef';

sub get {
    my $self = shift;
    my $path = shift;
    my $parameters = shift;

    return $self->request(HTTP::Request->new(
        'GET',
        $self->_build_uri($path, $parameters),
        $self->_build_headers
    ));
}

sig post => 'Str, ?HashRef';

sub post {
    my $self = shift;
    my $path = shift;
    my $parameters = shift || {};

    return $self->request(HTTP::Request->new(
        'POST',
        $self->_build_uri($path),
        $self->_build_headers('Content-Type' => 'application/json'),
        $self->_encode_body($parameters)
    ));
}

=head2 build_form_request

Builds a L<HTTP::Request> for a given resource path and parameter hash. The
request will be a C<multipart/form-data> C<POST>.

    my $request = $client->build_form_request('case/prepare_file', {
        upload_field_name => IO::File->new('my_file', 'r'),
        plain_form_field => 'abc'
    });

=cut

sig build_form_request => 'Str, HashRef';

sub build_form_request {
    my $self = shift;
    my $path = shift;
    my $form_data = shift;

    my %body_data = map {
        $_ => $self->_encode_form_field($form_data->{ $_ })
    } keys %{ $form_data };

    my ($content, $boundary) = HTTP::Request::Common::form_data(\%body_data);

    return HTTP::Request->new(
        'POST',
        $self->_build_uri($path),
        $self->_build_headers(
            'Content-Type' => HTTP::Headers::Util::join_header_words(
                'multipart/form-data' => undef,
                'boundary' => $boundary
            )
        ),
        $content
    );
}

=head1 PRIVATE METHODS

=head2 _build_user_agent

Builds a user agent for L</user_agent>.

=cut

sub _build_user_agent {
    my $self = shift;

    return LWP::UserAgent->new(
        ssl_opts => { verify_hostname => 0 },
        agent => 'Zaaksysteem-OLO/0.0.1',
        cookie_jar => HTTP::Cookies->new,
        timeout => $self->timeout,
    );

    return new_user_agent(@_, ca_cert => '/etc/zaaksysteem-olo-sync-service/sprint.crt');
}

=head2 _build_json

Builds a L<JSON::XS> instance for L</json>.

=cut

sub _build_json {
    return JSON::XS->new->utf8->pretty;
}

=head2 _build_uri

Builds a L<URI> instance given a resource path.

=cut

sub _build_uri {
    my $self = shift;
    my $path = shift;
    my $parameters = shift;

    my $uri_base;
    if (my $api_hostname = $ENV{API_HOSTNAME}) {
        $uri_base = "http://$api_hostname";
    } else {
        $uri_base = sprintf("https://%s", $self->hostname);
    }

    my $uri = URI->new(sprintf(
        '%s/api/v%s/%s',
        $uri_base,
        $self->api_version,
        $path
    ));

    if ($parameters) {
        $uri->query_form($parameters);
    }

    return $uri;
}

=head2 _build_headers

Builds a L<HTTP::Headers> instance with platform key and other metadata set
to Zaaksysteem API semantics.

=cut

sub _build_headers {
    my $self = shift;

    return HTTP::Headers->new(
        'Host' => $self->hostname,
        'Accept' => 'application/json',
        'ZS-Platform-Key' => $self->platform_key,
        @_
    );
}

=head2 _build_response

Builds a L<Zaaksysteem::API::Response> instance based on a given
L<HTTP::Response>.

=cut

sub _build_response {
    my $self = shift;
    my $response = shift;

    my $json = eval { $self->_decode_body($response->decoded_content) };

    unless (defined $json && ref $json eq 'HASH') {
        throw('api/client/response_invalid', sprintf(
            'Unexpected response body received: %s',
            $response->as_string
        ));
    }

    return Zaaksysteem::API::Response->new(
        request_id => $json->{ request_id },
        http_status => $json->{ status_code },
        data => $json->{ result }
    );
}

=head2 _encode_form_field

Helper for the L</build_form_request> method.

Takes some data as an argument, and tries to encode it in a format fit for a
C<multipart/form-data>-style request.

B<Scalar>: scalars are passed thru.
B<Hash>: hashrefs are JSON encoded
B<IO::Handle>: IO::Handle instances are read to EOF

Other value types will throw a C<zaaksysteem/api/form_field_data_unsupported>
exception.

=cut

sub _encode_form_field {
    my $self = shift;
    my $data = shift;
    my $filename;

    # Simple case, scalars get a passthru
    return $data unless ref $data;

    return $self->json->encode($data) if ref $data eq 'HASH';

    if (ref $data eq 'ARRAY') {
        ($filename, $data) = @{ $data };
    }

    unless (blessed $data) {
        throw('zaaksysteem/api/form_field_data_unsupported', sprintf(
            'Attempted to encode unsupported data as form field: "%s"',
            $data
        ));
    }
    
    if ($data->isa('IO::Handle')) {
        return [
            undef,
            $filename || refaddr($data),
            Content => do { local $/ = undef; <$data> }
        ];
    }

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
