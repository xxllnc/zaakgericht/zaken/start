package Zaaksysteem::API::Response;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::API::Response - Zaaksysteem API Response holder

=head1 DESCRIPTION

This class abstracts API responses.

=head1 SYNOPSIS

    my $response = $client->get(...);

=cut

use BTTW::Tools;

=head1 ATTRIBUTES

=cut

has request_id => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

has data => (
    is => 'ro',
    isa => 'HashRef',
    required => 1
);

has http_status => (
    is => 'ro',
    isa => 'Int',
    required => 1
);

=head1 METHODS

=head2 is_success

=cut

sub is_success {
    return shift->http_status == 200;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
