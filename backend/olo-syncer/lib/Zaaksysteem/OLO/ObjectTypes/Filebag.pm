package Zaaksysteem::OLO::ObjectTypes::Filebag;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Zaaksysteem::OLO::ObjectTypes::Filebag - Object type specification for
C<filebag> instances

=head1 DESCRIPTION

File uploads to Zaaksysteem use the C<multipart/form-data> style of uploads.
Since multiple files can be uploaded at the same time this way, Zaaksysteem
returns a C<filebag> object which references the newly created items.

=cut

use BTTW::Tools;
use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2 references

This attribute contains a map of identifiers to filenames for the uploaded
files.

=cut

szg_attr references => (
    value_type_name => 'complex'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
