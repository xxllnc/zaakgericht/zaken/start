package Zaaksysteem::OLO::ObjectTypes::SimpleCase;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Zaaksysteem::OLO::ObjectTypes::SimpleCase - Stub object type for C<case>
instances

=head1 DESCRIPTION

This class declares a simple representation of a C<case> object. Once the
full type becomes available this class will be deprecated.

=cut

use Syzygy::Syntax;

# Packagename gets mangled to simple_case
override name => sub { return 'case' };

# Allow oversaturated API response bodies to be valid cases.
sub ignore_unknown_attributes { return 1; };

=head1 OBJECT TYPE ATTRIBUTES

=head2 id

C<UUID> of the C<case>.

=cut

szg_attr id => (
    value_type_name => 'uuid'
);

=head2 attributes

Map of case attributes.

=cut

szg_attr attributes => (
    value_type_name => 'complex'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
