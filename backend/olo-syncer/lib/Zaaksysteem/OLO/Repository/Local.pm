package Zaaksysteem::OLO::Repository::Local;

use Moose;
use namespace::autoclean;

with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::OLO::Interface::Repository
];

=head1 NAME

Zaaksysteem::OLO::Repository::Local - Local filesystem repository
implementation.

=head1 DESCRIPTION

This class implements a L<Zaaksysteem::OLO::Interface::Repository> which uses
a filesystem directory as source of messages.

=head1 SYNOPSIS

    my $repo = Zaaksysteem::OLO::Repository::Local->new(
        path => '/path/to/repository'
    );

    my @messages = $repo->get_messages;

=cut

use BTTW::Tools;
use Path::Class::Dir ();
use Zaaksysteem::OLO::Message;

=head1 ATTRIBUTES

=head2 path

Path to the directory containing OLO messages

=cut

has path => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 get_messages

Implements L<Zaaksysteem::OLO::Interface::Repository/get_messages>.

=cut

sub get_messages {
    my $self = shift;
    my $since = shift;

    my $repo = $self->get_repo;

    my $message_dir = try {
        return $repo->subdir('berichten')->resolve;
    } catch {
        throw('olo/repository/local/repository_invalid', sprintf(
            'Caught exception initializing repository "%s": %s',
            $self->path,
            $_
        ));
    };

    my @messages;

    $message_dir->recurse(callback => sub {
        my $item = shift;

        return if $item->is_dir;

        my $filename = $item->stringify;

        return unless $filename =~ m[\.xml$];

        my ($timestamp_str, $id) = split m[_], $filename;

        my $timestamp = $self->parse_timestamp($timestamp_str);

        if ($since) {
            return if $since->compare($timestamp) > 0;
        }

        push @messages, Zaaksysteem::OLO::Message->new(
            entry_id => $filename,
            timestamp => $timestamp,
            _content_resolver => sub { return scalar($item->slurp) },
        );
    });

    return @messages;
}

=head2 get_repo

Returns a L<Path::Class::Dir> instance wherein the repository resides

=cut

sub get_repo {
    my $self = shift;

    return try {
        return Path::Class::Dir->new($self->path)->resolve;
    } catch {
        throw('olo/repository/local/path_invalid', sprintf(
            'Caught exception resolving repository path "%s": %s',
            $self->path,
            $_
        ));
    }
}

=head2 get_file

Returns a L<IO::File> instance for a given path.

=cut

sub get_file {
    my $self = shift;
    my $path_str = shift;

    my $repo = $self->get_repo;
    my $iterator = $repo;

    # Break up /path/to/file paths, remove empty pathparts (eg. leading /)
    my @path_parts = grep { length } split m[\/], $path_str;
    my $filename = pop @path_parts;

    for my $path_part (@path_parts) {
        try {
            $iterator = $iterator->subdir($path_part)->resolve;
        } catch {
            throw('olo/repository/local/path_invalid', sprintf(
                'Caught exception while resolving "%s" in "%s": %s',
                $path_part,
                $iterator->stringify,
                $_
            ));
        };
    }

    return try {
        return $iterator->file($filename)->resolve->open('r');
    } catch {
        throw('olo/repository/local/file_path_invalid', sprintf(
            'Caught exception while resolving file "%s" in "%s": %s',
            $filename,
            $iterator->stringify,
            $_
        ));
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
