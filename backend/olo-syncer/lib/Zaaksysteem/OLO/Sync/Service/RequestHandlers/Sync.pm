package Zaaksysteem::OLO::Sync::Service::RequestHandlers::Sync;

use Moose;

with qw[
    Zaaksysteem::Service::HTTP::RequestHandler
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::OLO::Sync::Service::RequestHandlers::Sync - Initiate a
synchronization run

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use DateTime::Format::ISO8601 qw[];
use List::Util qw[all];
use Syzygy::Types qw[UUID];
use Zaaksysteem::OLO::Model;
use Zaaksysteem::OLO::Repository::Local;
use Zaaksysteem::OLO::Repository::FTP;

=head1 METHODS

=head2 name

Declares the request handler name.

=cut

sub name { 'sync' }

=head2 dispatch

=cut

sub dispatch {
    my ($self, $request) = @_;

    my $args = assert_profile($request->body_parameters->as_hashref, profile => {
        required => {
            instance_hostname => 'Str',
            interface_id => UUID,
        },
        optional => {
            last_sync => 'DateTime',
            application_id => 'Str',
        },
        field_filters => {
            last_sync => sub {
                # Auto-inflate last_sync as a datetime, wrap in eval because
                # parse_datetime dumps parse failures to STDERR, and we're
                # already catching the error using the profile.
                return eval {
                    DateTime::Format::ISO8601->parse_datetime(@_)
                };
            },
        }
    })->valid;

    $self->service->set_log_context(
        instance_hostname => $args->{ instance_hostname },
    );

    $self->log->info(sprintf(
        'Synchronizing "%s" (interface "%s")',
        $args->{ instance_hostname },
        $args->{ interface_id },
    ));

    $self->service->set_log_context(instance_hostname => $args->{ instance_hostname });

    my $api = $self->service->new_api_client($args->{ instance_hostname });

    my $response = $api->get(sprintf('sysin/interface/%s', $args->{ interface_id }));

    my $model = $self->service->object_model;

    my $interface = try {
        return $model->read_graph_hash($response->data);
    } catch {
        $self->log->debug(dump_terse($_->object));

        $_->throw;
    };

    my $config = $interface->get_value('interface_config')->value;

    my $olo = Zaaksysteem::OLO::Model->new(
        api_client => $api,
        attribute_name_map => $interface->type->get_attribute_map($interface->instance),
        default_requestors => $interface->type->get_default_requestors($interface->instance),
        casetype_id => $interface->get_value('casetype_id')->value,
        interface_id => $args->{ interface_id },
        repository => Zaaksysteem::OLO::Repository::FTP->new(
            hostname => $config->{ host },
            username => $config->{ username },
            password => $config->{ password }
        )
    );

    my %sync_args = (
        since => $args->{ last_sync },
        application_id => $args->{ application_id },
    );

    # Cleanup undef params
    delete $sync_args{ $_ } for grep {
        not defined $sync_args{ $_ }
    } keys %sync_args;

    $olo->sync(\%sync_args);

    # Empty return results in a simple HTTP 200.
    return undef;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
