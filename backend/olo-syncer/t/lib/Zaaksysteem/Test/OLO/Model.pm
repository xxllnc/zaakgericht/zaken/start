package Zaaksysteem::Test::OLO::Model;

use Test::Class::Moose;
use Test::Fatal;
use Test::Mock::One;
use Sub::Override;
use DateTime;

use BTTW::Tools::RandomData qw(generate_uuid_v4);
use Syzygy::Object::Model;
use Zaaksysteem::API::Response;
use Zaaksysteem::OLO::Model;
use Zaaksysteem::OLO::Message;
use Zaaksysteem::OLO::ObjectTypes::SimpleCase;
use Zaaksysteem::OLO::ObjectTypes::Filebag;

use FindBin;

sub test_olo_model_init {
    my $model = Syzygy::Object::Model->get_instance;

    my %test_map = (
        case => 'Zaaksysteem::OLO::ObjectTypes::SimpleCase',
        filebag => 'Zaaksysteem::OLO::ObjectTypes::Filebag',
        interface => 'Zaaksysteem::OLO::ObjectTypes::Interface'
    );

    isa_ok $model->get_object_type($_), $test_map{ $_ }, "object type $_ loaded ok" for keys %test_map;
}

sub test_olo_update_message {
    my $last_get_request;
    my $last_post_request;

    my $case_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ $case_hash ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        build_form_request => sub {
            return \@_;
        },
        request => sub {
            my ($path, $attachments) = @{ shift() };

            return Zaaksysteem::API::Response->new(
                request_id => 'abc',
                http_status => 200,
                data => {
                    type => 'filebag',
                    instance => {
                        references => {
                            map {
                                generate_uuid_v4() => $_
                            } keys %{ $attachments }
                        }
                    }
                }
            );
        },
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        },
        post => sub {
            if ($_[0] =~ m[transaction]) {
                return Zaaksysteem::API::Response->new(
                    request_id => 'ijk',
                    http_status => 200,
                    data => {
                        type => 'sysin/transaction',
                        reference => generate_uuid_v4(),
                    }
                );
            }
            $last_post_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => 'xyz',
                data => $set_hash,
                http_status => 200
            );
        }
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => generate_uuid_v4(),
        interface_id => generate_uuid_v4(),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    my @messages = $model->repository->get_messages;

    # Grab our update test message
    my ($update_message) = grep { $_->stuf_reference eq '6418126' } @messages;

    lives_ok {
        $model->process_message($update_message)
    } 'processing update message lives';

    subtest update_case_get_request => sub {
        is ref $last_get_request, 'ARRAY', 'last get request set';

        my ($get_path, $get_params) = @{ $last_get_request };

        is $get_path, 'case', 'case resource path used in case retrieval request';
        is ref $get_params, 'HASH', 'case retrieval request has parameters';
        ok exists $get_params->{ zql }, 'case retrieval has zql parameter';
    };

    subtest update_case_post_request => sub {
        is ref $last_post_request, 'ARRAY', 'last post request set';

        my ($post_path, $post_params) = @{ $last_post_request };

        is $post_path, 'case/f08b7a09-3945-42ac-a620-9d1d3f963868/update',
            'case update resource path references mock case id';
     
        is ref $post_params, 'HASH', 'case update request has parameters';
        ok exists $post_params->{values}, 'expected "values" key found in update';
        ok exists $post_params->{values}{bar}, 'expected update key found in "values" key in update';
        is ref $post_params->{values}{bar}, 'ARRAY', 'update key contains array';
    }; 
}

sub test_olo_create_message {
    my $last_get_request;
    my $last_post_request;
    my $last_import_request;
    my @call_order;

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        build_form_request => sub {
            return \@_;
        },
        request => sub {
            my ($path, $attachments) = @{ shift() };

            return Zaaksysteem::API::Response->new(
                request_id => 'abc',
                http_status => 200,
                data => {
                    type => 'filebag',
                    instance => {
                        references => {
                            map {
                                generate_uuid_v4() => $_
                            } keys %{ $attachments }
                        }
                    }
                }
            );
        },
        get => sub {
            push @call_order, $_[0];

            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        },
        post => sub {
            push @call_order, $_[0];
            if ($_[0] =~ m[transaction]) {
                return Zaaksysteem::API::Response->new(
                    request_id => 'ijk',
                    http_status => 200,
                    data => {
                        type => 'sysin/transaction',
                        reference => 'ebf8a580-810e-42a7-a8b4-fbda6fc25d13',
                    }
                );
            } elsif ($_[0] =~ m[subject/import]) {
                $last_import_request = [ @_ ];
                return Zaaksysteem::API::Response->new(
                    request_id => 'lolol',
                    http_status => 200,
                    data => {
                        type => 'subject',
                        reference => '087b5dfc-d7c0-48b0-aec2-8c8681f9f894',
                    }
                );
            }
            $last_post_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => 'xyz',
                data => $set_hash,
                http_status => 200
            );
        }
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => 'd5ed38b6-9ca2-410d-a825-d14ea1c409ef',
        interface_id => 'a7eb6603-8adc-406a-8216-6618717f6055',
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    # Grab our create test message
    my ($create_message) = grep {
        $_->stuf_reference eq '6584064'
    } $model->repository->get_messages;

    lives_ok {
        $model->process_message($create_message)
    } 'model processed create message';

    subtest create_case_get_request => sub {
        is ref $last_get_request, 'ARRAY', 'last get request set';

        my ($get_path, $get_params) = @{ $last_get_request };

        is $get_path, 'case', 'case resource path used in case retrieval request';
        is ref $get_params, 'HASH', 'case retrieval request has parameters';
        ok exists $get_params->{ zql }, 'case retrieval has zql parameter';
    };


    subtest create_case_subject_import_request => sub {
        is ref $last_import_request, 'ARRAY', 'last "import" request set';

        my ($get_path, $get_params) = @{ $last_import_request };

        is $get_path, 'subject/import', 'correct API path used for subject import request';
        is ref $get_params, 'HASH', 'subject import request has parameters';
        cmp_deeply(
            $get_params,
            {
                query => {
                    match => {
                        subject_type => 'person',
                        'subject.personal_number' => '123546789',
                    }
                }
            },
            'subject import parameters formatted correctly'
        )
    };

    subtest create_case_post_request => sub {
        is ref $last_post_request, 'ARRAY', 'last post request set';

        my ($post_path, $post_params) = @{ $last_post_request };

        is $post_path, 'case/create',
            'case create resource path ok';
     
        is ref $post_params, 'HASH', 'case update request has parameters';
        ok exists $post_params->{ casetype_id }, 'expected casetype_id key found';
        ok exists $post_params->{ requestor }, 'expected requestor key found';
        ok exists $post_params->{ source }, 'expected source key found';
        ok exists $post_params->{ values }, 'expected values key found';
        is ref $post_params->{ values }, 'HASH', 'values key contains hashref';

        for my $key (keys %{ $post_params->{ values } }) {
            is ref $post_params->{ values }{ $key }, 'ARRAY', "values->$key key contains arrayref";
        }
    };

    cmp_deeply(
        \@call_order,
        [
            # Create transaction
            'sysin/interface/a7eb6603-8adc-406a-8216-6618717f6055/transaction/create',
            # Check if case exists
            'case',
            # Create transaction record
            'sysin/interface/a7eb6603-8adc-406a-8216-6618717f6055/transaction/ebf8a580-810e-42a7-a8b4-fbda6fc25d13/record/create',
            # Import requestor
            'subject/import',
            # Create case
            'case/create',
            # Update transaction record (finished)
            'sysin/interface/a7eb6603-8adc-406a-8216-6618717f6055/transaction/ebf8a580-810e-42a7-a8b4-fbda6fc25d13/record/ebf8a580-810e-42a7-a8b4-fbda6fc25d13/update',
            # Update transaction (finished)
            'sysin/interface/a7eb6603-8adc-406a-8216-6618717f6055/transaction/ebf8a580-810e-42a7-a8b4-fbda6fc25d13/update'
        ]
    );
}

sub test_olo_create_message_exists {
    my $last_get_request;

    my $case_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ $case_hash ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        },
        post => sub {
            return Zaaksysteem::API::Response->new(
                request_id => 'ijk',
                http_status => 200,
                data => {
                    type => 'sysin/transaction',
                    reference => generate_uuid_v4(),
                }
            );
        },
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => generate_uuid_v4(),
        interface_id => generate_uuid_v4(),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    # Grab our create test message
    my ($create_message) = grep {
        $_->stuf_reference eq '6584064'
    } $model->repository->get_messages;

    my $exception = exception { $model->process_message($create_message) };

    isa_ok $exception, 'BTTW::Exception::Base',
        'exception thrown by process_message';

    is $exception->type, 'olo/sync/process_create/case_found', 'case exists exception thrown';
}

sub test_olo_create_message_multiple {
    my $last_get_request;

    my $case_a_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $case_b_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ $case_a_hash, $case_b_hash ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        },
        post => sub {
            return Zaaksysteem::API::Response->new(
                request_id => 'ijk',
                http_status => 200,
                data => {
                    type => 'sysin/transaction',
                    reference => generate_uuid_v4(),
                }
            );
        },
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => generate_uuid_v4(),
        interface_id => generate_uuid_v4(),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    # Grab our create test message
    my ($create_message) = grep {
        $_->stuf_reference eq '6584064'
    } $model->repository->get_messages;

    my $exception = exception { $model->process_message($create_message) };

    isa_ok $exception, 'BTTW::Exception::Base',
        'exception thrown by process_message';

    is $exception->type, 'olo/sync/multiple_cases_found', 'multiple cases exception type';
}

sub test_olo_model_sync {
    my $repo = Zaaksysteem::OLO::Repository::Local->new(
        path => "$FindBin::Bin/data/repo"
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => Test::Mock::One->new('X-Mock-ISA' => 'Zaaksysteem::API::Client'),
        casetype_id => generate_uuid_v4(),
        interface_id => generate_uuid_v4(),
        repository => $repo,
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
    );

    my $test_no;
    my $process_called = 0;

    my $override = Sub::Override->new(
        'Zaaksysteem::OLO::Model::process_message',
        sub {
            my ($self, $message) = @_;

            $process_called++;

            return unless $test_no;

            is $message->application_id, $test_no, 'expected message got parsed';
        }
    );

    $model->sync;

    my @messages = $repo->get_messages;

    is $process_called, scalar @messages,
        'model sync called expected number of times';

    $test_no = 2817104;
    $process_called = 0;

    $model->sync({ application_id => $test_no });

    # application_id 2817104 is known to have 1 message in test store
    is $process_called, 1,
        'model sync called once for singular application_id';

    # application_id 2823764 is known to have 2 messages in test store
    $test_no = 2823764;
    $process_called = 0;

    $model->sync({ application_id => $test_no });

    is $process_called, 2,
        'model sync called twice for multi-message application_id';

    $process_called = 0;
    my $get_messages_called = 0;
    my $since_dt = DateTime->now;

    $override->override(
        'Zaaksysteem::OLO::Repository::Local::get_messages',
        sub {
            my ($self, $since) = @_;

            $get_messages_called++;

            # Rely on DateTime's internal comparison semantics
            ok $since == $since_dt, 'model sync passes correct datetime';

            # Prevent processing, empty list return
            return ();
        }
    );

    $model->sync({ since => $since_dt });

    is $get_messages_called, 1,
        'repo::get_messages called once for single model sync call';

    is $process_called, 0,
        'model sync with empty get_messages return does not call process_message';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
