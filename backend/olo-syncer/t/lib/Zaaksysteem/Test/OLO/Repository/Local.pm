package Zaaksysteem::Test::OLO::Repository::Local;

use Test::Class::Moose;
use Test::Fatal;
use Test::Mock::One;

use FindBin;
use Zaaksysteem::OLO::Repository::Local;

my $test_repo = "$FindBin::Bin/data/repo";

sub test_local_repository {
    my $repository = Zaaksysteem::OLO::Repository::Local->new(
        path => $test_repo
    );

    isa_ok $repository, 'Zaaksysteem::OLO::Repository::Local',
        'local repository constructor retval';

    my @messages = $repository->get_messages;

    is scalar @messages, 5, 'expected number of messages found';

    my $message = shift @messages;

    isa_ok $message, 'Zaaksysteem::OLO::Message';

    ok $message->timestamp->time_zone->is_utc,
        'timestamp is in UTC timezone';

    ok $message->timestamp->compare(DateTime->new(
        year => 2017,
        month => 01,
        day => 29,
        hour => 14,
        minute => 00,
        second => 23
    )), 'timestamp parsed';
}

sub test_local_repository_exceptions {
    my $nopository = Zaaksysteem::OLO::Repository::Local->new(
        path => '/path/to/nowhere'
    );

    my $nope_exception = exception { $nopository->get_repo };

    isa_ok $nope_exception, 'BTTW::Exception::Base',
        'repository does not exist exception instance';

    is $nope_exception->type, 'olo/repository/local/path_invalid',
        'repository does not exist exception type';

    my $repository = Zaaksysteem::OLO::Repository::Local->new(
        path => $test_repo
    );

    lives_ok { $repository->get_repo } 'repository exists';

    my $file = $repository->get_file(
        'berichten/2598177/20170129160023_2598177_vrgDi01IndienenAanvulling.xml'
    );

    isa_ok $file, 'IO::File', 'test data path valid';

    my $leadslash_file = $repository->get_file(
        '/berichten/2598177/20170129160023_2598177_vrgDi01IndienenAanvulling.xml'
    );

    isa_ok $leadslash_file, 'IO::File',
        'test data leading slash path valid';

    my $concat_file = $repository->get_file(
        '/berichten//2598177////20170129160023_2598177_vrgDi01IndienenAanvulling.xml'
    );

    isa_ok $concat_file, 'IO::File',
        'test data path with empty pathparts valid';

    my $fnf = exception { $repository->get_file('nope.foo') };

    isa_ok $fnf, 'BTTW::Exception::Base',
        'file not found exception instance';

    is $fnf->type, 'olo/repository/local/file_path_invalid',
        'file not found exception type';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
