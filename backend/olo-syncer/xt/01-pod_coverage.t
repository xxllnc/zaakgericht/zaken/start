use Test::More;
use Test::Pod::Coverage;

all_pod_coverage_ok({ trustme => [qw[BUILD]] });

done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::OLO::Sync uses the EUPL license, for more information please have a look at the C<LICENSE> file.

