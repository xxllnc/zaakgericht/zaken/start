package BTTW;

use strict;
use warnings;

our $VERSION = '0.014';

=head1 NAME

BTTW - Balls to the Wall utilities library

=head1 DESCRIPTION

BTTW is a collection of utility libraries used by the L<Zaaksysteem> project.

See L<BTTW::Tools> for more info.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
