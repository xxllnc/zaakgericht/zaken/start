package BTTW::Tools::UA;
our $VERSION = '0.014';
use warnings;
use strict;

use Exporter qw(import);
use HTTP::Cookies;
use LWP::UserAgent;
use BTTW::Tools;

our @EXPORT = qw(new_user_agent);

=head1 NAME

BTTW::Tools::UA - Generate LWP::UserAgent instances with sane defaults

=head1 DESCRIPTION

BTTW (and related services) use HTTP/HTTPS a lot to communicate with
external services.

This module exists to reduce code duplication and to make the C<LWP::UserAgent>
configuration more uniform.

=head1 SYNOPSIS

    use BTTW::Tools::UA;

    my $ua = new_user_agent(client_cert => '/etc/secret/my_cert.pem');
    $ua->get('https://secret.example.com/');

=head1 EXPORTED FUNCTIONS

=head2 new_user_agent

Create a new C<LWP::UserAgent> instance, pre-configured with sane defaults.

Supports the following named arguments, for extra configuration:

=over

=item * ca_cert

File containing trusted CA (root) certificates in PEM format.

=item * ca_path

Name of a directory containing trusted CA certificates.

By default, C</etc/ssl/certs> (the system certificate store) is used.

=item * client_cert

The client certificate to use (optional), in PEM or PKCS#12 format.

=item * client_key

The private key for the client certificate.

If the C<client_cert> file contains both the certificate and the key, this does
not need to be supplied.

=item * client_key_passwd_cb

A reference to function that will return the password to the private key for
the client certificate (or for the PKCS#12 file) when called.

=item * protocols_allowed

Array reference, containing allowed protocols. Defaults to ['https']

=item * timeout

Timeout in seconds. Defaults to 10.

=back

=cut

define_profile new_user_agent => (
    optional => {
        agent                => 'Str',
        ca_cert              => 'Str',
        ca_path              => 'Str',
        client_cert          => 'Str',
        client_key           => 'Str',
        client_key_passwd_cb => 'CodeRef',
        # protocols_allowed is actually an array of Str, but
        # Data::FormValidator hates ArrayRefs
        protocols_allowed    => 'Str',
        timeout              => 'Int',
    },
    defaults => {
        agent => sub {
            defined($BTTW::VERSION)
                ? "BTTW/$BTTW::VERSION"
                : "BTTW::Tools",
            },
        ca_path => '/etc/ssl/certs',
        protocols_allowed => sub { return [qw(https)]},
    }
);

sub new_user_agent {
    my %args = %{ assert_profile({@_})->valid };

    my %ssl_opts = (
        verify_hostname => 1
    );

    if ($args{ca_cert}) {
        $ssl_opts{SSL_ca_file} = $args{ca_cert};
    }
    else {
        $ssl_opts{SSL_ca_path} = $args{ca_path};
    }

    if ($args{client_cert}) {
        $ssl_opts{SSL_cert_file} = $args{client_cert};
        $ssl_opts{SSL_key_file}  = $args{client_key}
            if $args{client_key};
        $ssl_opts{SSL_passwd_cb} = $args{client_key_passwd_cb}
            if $args{client_key_passwd_cb};
    }

    return LWP::UserAgent->new(
        agent                 => $args{agent},
        ssl_opts              => \%ssl_opts,
        requests_redirectable => [qw(POST GET HEAD)],
        cookie_jar            => HTTP::Cookies->new(),
        protocols_allowed     => $args{protocols_allowed},
        timeout               => $args{timeout} // 10,
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|BTTW::CONTRIBUTORS> file.

BTTW uses the EUPL license, for more information please have a look at the L<LICENSE|BTTW::LICENSE> file.
