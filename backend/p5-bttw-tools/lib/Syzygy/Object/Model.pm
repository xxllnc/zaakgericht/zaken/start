package Syzygy::Object::Model;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with qw[
    MooseX::Log::Log4perl
];

=head1 NAME

Syzygy::Object::Model - Syzygy object model interaction class

=head1 DESCRIPTION

This class abstracts much of the Syzygy logic, and is the main interaction
method when working with Syzygy objects.

This class is implemented as a singleton, and can be accessed globally as a
consequence.

=cut

use BTTW::Tools;
use Module::Pluggable::Object;
use MooseX::Types::Moose qw[HashRef ArrayRef];
use Syzygy::Object;
use Syzygy::Object::Reference;
use Syzygy::Object::Instance;
use Syzygy::Object::Value;
use Syzygy::Types qw[ObjectType ObjectAttribute Source ValueType Name UUID];

my $_instance;

=head1 ATTRIBUTES

=head2 object_types

Contains a map of implementors of L<Syzygy::Interface::ObjectType>.

On instantiation of the model the classes in C<Syzygy::Object::Types> are read
from disk, and are be injected in this attribute.

=head3 Delegates

=over 4

=item has_object_type

Returns true-ish if a specified type exists in the map.

=item get_object_type

Getter for object types in the map.

=item set_object_type

Setter for object types in the map.

=item all_object_types

Returns all object type instances.

=item object_type_names

Returns all object type names.

=back

=cut

has object_types => (
    is => 'rw',
    isa => HashRef->parameterize(ObjectType),
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_object_type => 'exists',
        get_object_type => 'get',
        set_object_type => 'set',
        all_object_types => 'values',
        object_type_names => 'keys',
    }
);

=head2 value_types

Contains a map of implementors of L<Syzygy::Interface::ValueType>.

On instantiation of the model the classes in C<Syzygy::Object::ValueTypes> are
read from disk, and are injected in this attribute.

=head3 Delegates

=over 4

=item has_value_type

Returns true-ish if a specified type exists in the map.

=item get_value_type

Getter for value types in the map.

=item set_value_type

Setter for value types in the map.

=item all_value_types

Returns all value type instances.

=item value_type_names

Returns all value type names.

=back

=cut

has value_types => (
    is => 'rw',
    isa => HashRef->parameterize(ValueType),
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_value_type => 'exists',
        get_value_type => 'get',
        set_value_type => 'set',
        all_value_types => 'elements',
        value_type_names => 'keys'
    }
);

=head2 sources

Contains a map of implementors of L<Syzygy::Interface::Source>.

=head3 Delegates

=over 4

=item has_source

Returns true-ish if a specified source name exists in the map.

=item get_source

Getter for source instances in the map.

=item set_value

Setter for source instances in the map.

=item all_sources

Returns a list of all source instances.

=item source_names

Returns a list of all source names.

=back

=cut

has sources => (
    is => 'rw',
    isa => HashRef->parameterize(Source),
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_source => 'exists',
        get_source => 'get',
        set_source => 'set',
        all_sources => 'elements',
        source_names => 'keys'
    }
);

=head2 attributes

Syzygy maintains a globally consistent set of attributes with declare-once
typing per attribute. This attribute accumulates that mapping for every loaded
object type.

The use of this strategy is for ease of reasoning when it comes to objects in
multi-type sets. Where equivalently-named attributes could have different
implementations (based on value type), this attribute is used to enforce that
no such situation can occur.

Exposes C<has_attribute>, C<get_attribute>,
C<set_attribute>, and C<all_attributes>.

=cut

has attributes => (
    is => 'rw',
    isa => HashRef->parameterize(ObjectAttribute),
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_attribute => 'exists',
        get_attribute => 'get',
        set_attribute => 'set',
        all_attributes => 'values',
        all_attribute_fqns => 'keys'
    }
);

=head2 type_source_map

This attribute specifies a mapping of type names to source names.

The data is used by the model when deciding which source to use for reading
and writing objects.

=cut

has type_source_map => (
    is => 'rw',
    isa => ArrayRef,
    traits => [qw[Array]],
    default => sub { return [] },
);

=head2 get_instance

Returns an instance of this class. Will set the C<$_instance> package variable
with the instance if the variable is undefined.

    my $model = Syzygy::Object::Model->get_instance;

=cut

sub get_instance {
    my $this = shift;

    # Default to singleton behavior, but allow code 'that knows better' to
    # work with an ordinary model instance if desired.
    return $this if blessed $this;

    unless (blessed $this::_instance) {
        $this::_instance = $this->new;
        $this::_instance->preload;
    }

    return $this::_instance;
}

=head2 new_object

Convenience method for constructing L<Syzygy::Object> instances.

    my $object = $model->new_object('some_type', { foo => 'bar' });

=cut

sig new_object => 'Str, ?HashRef';

sub new_object {
    my $self = shift->get_instance;
    my $type_name = shift;
    my $instance_args = shift;

    unless ($self->has_object_type($type_name)) {
        throw('syzygy/object/model/object_type_not_found', sprintf(
            'Attempted to instantiate object for unknown type "%s"',
            $type_name
        ));
    }

    my $type = $self->get_object_type($type_name);

    my %object_args = (
        type => $type
    );

    if (defined $instance_args) {
        $object_args{ instance } = Syzygy::Object::Instance->new(
            values => $type->process_attribute_values($instance_args)
        );
    }

    return Syzygy::Object->new(%object_args);
}

=head2 new_value

Convenience method for constructing L<Syzygy::Value> instances.

    my $value = $model->new_value('some_type', 'my value');

=cut

sig new_value => 'Str, Defined';

sub new_value {
    my $self = shift->get_instance;
    my $type_name = shift;
    my $value = shift;

    unless ($self->has_value_type($type_name)) {
        throw('syzygy/object/model/value_type_not_found', sprintf(
            'Attempted to instantiate value object for unknown type "%s"',
            $type_name
        ));
    }

    my $type = $self->get_value_type($type_name);

    return $type->new_value($value);
}

=head2 read_graph_hash

This method consumes a C<APIv1> style hash of a Syzygy object and returns
the inflated object.

    my $service = Syzygy::Object::Model->read_graph_hash({
        type => 'service',
        reference => null,
        instance => {
            name => 'My service name',
            repository => 'https://my.repo.git/somewhere/out/there',
            version => 'v3.11'
        }
    });

=cut

define_profile read_graph_hash => (
    required => { 
        type => Name
    },
    optional => {
        reference => UUID,
        instance => HashRef
    },
    require_some => {
        reference_or_instance => [ 1, qw[reference instance] ]
    }
);

sig read_graph_hash => 'HashRef';

sub read_graph_hash {
    my $self = shift->get_instance;
    my $graph = assert_profile(shift)->valid;

    unless ($self->has_object_type($graph->{ type })) {
        throw('syzygy/object/model/unknown_object_type', sprintf(
            'Unknown object type "%s" while reading object graph',
            $graph->{ type }
        ));
    }

    my $type = $self->get_object_type($graph->{ type });

    unless (defined $graph->{ instance }) {
        return Syzygy::Object::Reference->new(
            type => $type,
            id => $graph->{ reference }
        );
    }

    my %object_args = (
        type => $type,
        instance => Syzygy::Object::Instance->new(
            values => $type->process_attribute_values($graph->{ instance })
        )
    );

    if ($graph->{ reference }) {
        $object_args{ id } = $graph->{ reference };
    }

    return Syzygy::Object->new(\%object_args);
}

=head2 index_attributes

Loops over all L<object_types> and runs L</index_attribute> on each associated
attribute.

=cut

sub index_attributes {
    my $self = shift->get_instance;

    $self->index_attribute($_) for map {
        $_->all_attributes
    } $self->all_object_types;

    return;
}

=head2 index_attribute

Takes a L<Syzygy::Object::Attribute> object and upserts a
L<Syzygy::Object::AttributeTemplate> in L</attribute_templates>.

Effectively a no-op if a template already exists for the attribute's
fully qualified name (name including namespace).

Throws a C<syzygy/object/model/attribute/value_type_conflict> exception if the
attribute's name already exists, but is associated with a different value
type.

=cut

sig index_attribute => 'Syzygy::Object::Attribute';

sub index_attribute {
    my $self = shift->get_instance;
    my $attribute = shift;

    my $fqn = $attribute->fqn;

    if ($self->has_attribute($fqn)) {
        my $registered_attribute = $self->get_attribute($fqn);
        my $value_type_name = $attribute->value_type_name;

        unless ($value_type_name eq $registered_attribute->value_type_name) {
            throw('syzygy/object/model/attribute/value_type_conflict', sprintf(
                'Attribute "%s": previously declared value type "%s" conflicts with "%s"',
                $fqn,
                $registered_attribute->value_type_name,
                $value_type_name
            ));
        }

        # Value type checks out, nothing more to do here.
        return;
    }

    $self->log->trace(sprintf(
        'Registering attribute "%s" (value type "%s")',
        $fqn,
        $attribute->value_type_name
    ));

    # Save a clone of the attribute so the original attribute can be safely
    # destroyed.
    $self->set_attribute($fqn, $attribute->meta->clone_object($attribute));

    return;
}

=head2 preload

Calls the loaders for L</object_types>, L</value_types>, and L</sources>.

This method should be called once, preferably using it's delegation via
L</get_instance>.

=cut

sub preload {
    my $self = shift;

    $self->load_value_type_modules;
    $self->load_object_type_modules;
    $self->load_source_modules;

    $self->index_attributes;

    return $self;
}

=head2 load_object_type_modules

Loads static L<Syzygy::Interface::ObjectType> objects into L</object_types>.

=cut

sub load_object_type_modules {
    my $self = shift;

    $self->set_object_type($_->name, $_) for $self->find_object_type_modules(@_);

    return;
}

=head2 load_value_type_modules

Loads static L<Syzygy::Interface::ValueType> objects into L</value_type>.

=cut

sub load_value_type_modules {
    my $self = shift;

    $self->set_value_type($_->name, $_) for $self->find_value_type_modules(@_);

    return;
}

=head2 load_source_modules

Loads static L<Syzygy::Interface::Source> objects into L</sources>.

=cut

sub load_source_modules {
    my $self = shift;

    $self->set_source($_->name, $_) for $self->find_source_modules(@_);

    return;
}

=head2 find_object_type_modules

Returns the list of objects implementing L<Syzygy::Interface::ObjectType> from
the C<Syzygy::Object::Types> namespace.

=cut

sub find_object_type_modules {
    return shift->find_modules(shift || 'Syzygy::Object::Types', ObjectType);
}

=head2 find_value_type_modules

Returns the list of objects implementing L<Syzygy::Interface::ValueType> from
the C<Syzygy::Object::ValueTypes> namespace.

=cut

sub find_value_type_modules {
    return shift->find_modules(shift || 'Syzygy::Object::ValueTypes', ValueType);
}

=head2 find_source_modules

Returns the list of objects implementing L<Syzygy::Interface::Source> from the
C<Syzygy::Object::Sources> namespace.

=cut

sub find_source_modules {
    return shift->find_modules(shift || 'Syzygy::Object::Sources', Source);
}

=head2 find_modules

Helper method for finding and (optionally) validating modules.

    my @modules = $model->find_modules('My::Namespace', MyMooseTypeConstraint);

=cut

sig find_modules => 'Str, ?Moose::Meta::TypeConstraint';

sub find_modules {
    my $self = shift;
    my $ns = shift;
    my $constraint = shift;

    my $finder = Module::Pluggable::Object->new(
        search_path => $ns,
        instantiate => 'new'
    );

    my @modules = $finder->plugins;

    if ($constraint) {
        $constraint->assert_valid($_) for @modules;
    }

    return @modules;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
