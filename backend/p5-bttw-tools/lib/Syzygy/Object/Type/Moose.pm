package Syzygy::Object::Type::Moose;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ObjectType
];

=head1 NAME

Syzygy::Object::Type::Moose - L<Moose> integration for type declaration

=head1 DESCRIPTION

This package implements a L<Syzygy::Interface::ObjectType> using L<Moose>
introspection.

This class cannot be instantiated by itself, it must first be extended.

=head1 SYNOPSIS

    package My::Object::Type;

    extends 'Syzygy::Object::Type::Moose';

    use Syzygy::Syntax;

    szg_attr name => (
        value_type_name => 'text'
    );

=cut

use BTTW::Tools;
use List::Util qw[any first];
use Syzygy::Object::Attribute;

=head1 METHODS

=cut

sub BUILD {
    if (ref shift eq __PACKAGE__) {
        throw('syzygy/object/type/moose/not_instantiable', sprintf(
            'Attempted instantiation of package %s, you must extend it instead.',
            __PACKAGE__
        ));
    }
}

=head2 name

Implements L<Syzygy::Interface::ObjectType/name>.

Inflects the object type name from the implementing package

=cut

sub name {
    my $self = shift;
    my $class = ref($self) || $self;

    my $stripped_class = (split m[::], $class)[-1];

    my @type_parts = $stripped_class =~ m/[A-Z](?:[A-Z]+|[a-z]*)(?=$|[A-Z])/g;

    return join '_', map { lc } @type_parts;
}

=head2 has_attribute

Implements L<Syzygy::Interface::ObjectType/has_attribute>.

=cut

sub has_attribute {
    my $self = shift;
    my $name = shift;

    return any { $_ eq $name } $self->attribute_names;
}

=head2 get_attribute

Implements L<Syzygy::Interface::ObjectType/get_attribute>.

=cut

sub get_attribute {
    my $self = shift;
    my $name = shift;

    my $attribute = first {
        $_->does('Syzygy::Interface::MooseAttribute') && $_->szg_name eq $name
    } map {
        $self->meta->get_attribute($_)
    } $self->meta->get_attribute_list;

    return unless defined $attribute;
    return $attribute->szg_attribute;
}

=head2 set_attribute

Implements L<Syzygy::Interface::ObjectType/set_attribute>.

This method will always throw
C<syzygy/object/type/set_attribute_on_static_type> because classes extending
from this package are expected to be static.

=cut

sub set_attribute {
    my $self = shift;

    throw('syzygy/object/type/set_attribute_on_static_type', sprintf(
        'Attempted setting a type attribute on static %s object type',
        $self->name
    ));
}

=head2 all_attributes

Implements L<Syzygy::Interface::MooseAttribute/all_attributes>.

=cut

sub all_attributes {
    my $self = shift;

    return map {
        $self->get_attribute($_);
    } $self->attribute_names;
}

=head2 attribute_names

Implements L<Syzygy::Interface::MooseAttribute/attribute_names>.

Returns the list of names of attributes implemented by the type.

    my @names = $type->attribute_names;

=cut

sub attribute_names {
    my $self = shift;

    return map {
        $_->szg_name
    } grep {
        $_->does('Syzygy::Interface::MooseAttribute')
    } map {
        $self->meta->get_attribute($_)
    } $self->meta->get_attribute_list;
}

=head2 build_preview

Implements L<Syzygy::Interface::ObjectType/build_preview>.

Should be overridden by subclasses for proper preview handling.

=cut

sub build_preview { }

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
