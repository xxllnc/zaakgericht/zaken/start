package Syzygy::Object::ValueTypes::String;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with 'Syzygy::Interface::ValueType';

=head1 NAME

Syzygy::Object::ValueTypes::String - L<Syzygy::Object>
L<C<string>|Syzygy::Manual::API::V1::ValueTypes/string> value class.

=head1 DESCRIPTION

This value type abstracts the concept of a simple string of characters within
the object infrastucture, and is usually used for single words, identifiers,
or short descriptive sentences.

=cut

use BTTW::Tools;

=head1 METHODS

=head2 name

Returns the static type name for this value type (C<string>).

=cut

sub name {
    return 'string';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return 'Str';
}

=head2 equal

Implements equality testing of two values in C<string> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return unless defined $a && defined $b;
    return unless $a->has_value && $b->has_value;

    return $a->value eq $b->value;
}

=head2 not_equal

Implements not-equality testing of two values in C<string> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 coerce

Implements value coercions from C<text> and C<string> values to C<string>
value instances.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;

    return $self->new_value($value->value) if $value->type_name eq 'string';
    return $self->new_value($value->value) if $value->type_name eq 'text';
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_interface>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return $value->value;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
