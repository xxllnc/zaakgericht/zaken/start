package Syzygy::Object::ValueTypes::URI;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ValueType
];

=head1 NAME

Syzygy::Object::ValueTypes::URI - C<uri> value type class

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use URI;

=head1 METHODS

=head2 name

Implements L<Syzygy::Interface::ValueType/name>.

=cut

sub name {
    return 'uri';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return 'URI';
}

=head2 coerce

Implements L<Syzygy::Interface::ValueType/coerce>.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    if ($value->type_name eq 'string') {
        return $self->new_value(URI->new($value->value));
    }
}

=head2 equal

Implements value equality comparison.

=cut

sub equal {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return unless defined $a && defined $b;

    return $a->value->eq($b->value);
}

=head2 not_equal

Implements value inequality comparsion.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return $value->value->as_string;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
