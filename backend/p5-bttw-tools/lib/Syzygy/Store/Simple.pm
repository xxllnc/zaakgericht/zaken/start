package Syzygy::Store::Simple;
our $VERSION = '0.006';
use Moose;

with qw[
    MooseX::Log::Log4perl
    Syzygy::Interface::Store
];

=head1 NAME

Syzygy::Store::Simple - In-memory implementation of
L<Syzygy::Interface::Store>

=head1 DESCRIPTION

This class implements a simple, in-memory, and relatively low-level
implementation of the L<Syzygy::Interface::Store> API.

Instances of this store can be easily populated, searched, and updated using
that API's semantics, and especially comes in handy when testing models with
database integration, without actually using the database.

=head1 SYNOPSIS

    use Syzygy::Store::Simple;
    use Syzygy::Syntax;

    my $store = Syzygy::Store::Simple->new;

    # 'create' an object instance in the store
    # use the returned id to reference the stored object.
    my $id = $store->create(Syzygy::Object->new(...));

    # retrieve an object instance using the previously captured id
    my $object = $store->retrieve($id);

    # search for objects using a query
    my @objects = $store->search(qb('object', {
        cond => qb_eq('my_field', 'search_value')
    });

    # update existing objects (globally set new value for 'my_field')
    my @updated_ids = $store->update(qb('object'), {
        my_field => szg_value(string => 'my_new_value')
    });

    # retrieve ids of matching objects
    my @exists_ids = $store->exists(qb(...));

    # delete items from the store, returns ids of deleted items
    my @deleted_ids = $store->delete(qb(...));

=cut

use BTTW::Tools;
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use List::Util qw[reduce];

use Syzygy::Syntax;

=head1 ATTRIBUTES

=head2 _storage

In-memory store of inserted entries.

=cut

has _storage => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { return {}; },
    handles => {
        _set_entry => 'set',
        _get_entry => 'get',
        _has_entry => 'exists',
        _delete_entry => 'delete',
        _entry_count => 'count',
        _entries => 'values'
    }
);

=head2 object_type

Typestring for the type accepted by the store

=cut

has object_type => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_object_type'
);

=head1 METHODS

=head2 search

Implements L<Syzygy::Interface::Store/search>.

=cut

sub search {
    my $self = shift;
    my $query = shift;

    my $entries = $self->_exec_query($query);

    return values %{ $entries };
}

=head2 create

Implements L<Syzygy::Interface::Store/create>

=cut

sub create {
    my $self = shift;
    my $entry = shift;

    # Strict object type check
    if ($self->has_object_type && $entry->type ne $self->object_type) {
        throw('simple_store/create/type_error', sprintf(
            'Cannot create object of type "%s" in store configured for type "%s"',
            $entry->type,
            $self->object_type
        ));
    }

    my $id = generate_uuid_v4;

    $self->_set_entry($id, $entry);

    return $id;
}

=head2 retrieve

Implements L<Syzygy::Interface::Store/create>

=cut

sub retrieve {
    my $self = shift;
    my $id = shift;

    return $self->_get_entry($id);
}

=head2 update

Implements L<Syzygy::Interface::Store/create>

=cut

sub update {
    my $self = shift;
    my $query = shift;
    my $update = shift;

    my $entries = $self->_exec_query($query);

    for my $id (keys %{ $entries }) {
        for my $field (keys %{ $update }) {
            if (defined $update->{ $field }) {
                $entries->{ $id }->set_value($field, $update->{ $field })
            } else {
                $entries->{ $id }->clear_value($field);
            }
        }
    }

    return keys %{ $entries };
}

=head2 delete

Implements L<Syzygy::Interface::Store/create>

=cut

sub delete {
    my $self = shift;
    my $query = shift;

    my $entries_to_delete = $self->_exec_query($query);

    my @keys = keys %{ $entries_to_delete };

    $self->_delete_entry($_) for @keys;

    return @keys;
}

=head2 exists

Implements L<Syzygy::Interface::Store/create>

=cut

sub exists {
    my $self = shift;
    my $query = shift;

    my $filtered_entries = $self->_exec_query($query);

    return keys %{ $filtered_entries };
}

sub _exec_query {
    my $self = shift;
    my $query = shift;

    if ($self->has_object_type && $query->type ne $self->object_type) {
        throw('simple_store/query/type_error', sprintf(
            'Cannot search objects of type "%s" in store configured for type "%s"',
            $query->type,
            $self->object_type
        ));
    }

    # Clone map for in-place filtering
    my $entries = { %{ $self->_storage } };

    $self->_filter_entries($query, $entries);

    return $entries;
}

sub _filter_entries {
    my $self = shift;
    my $query = shift;
    my $entries = shift;

    if ($query->has_cond) {
        my $condition = $self->_cond_as_sub($query->cond);

        for my $id (keys %{ $entries }) {
            unless ($condition->($entries->{ $id })->value) {
                delete $entries->{ $id };
            }
        }
    }

    if ($query->has_sort) {
        throw('simple_store/query/sort_not_supported', sprintf(
            '"%s": sorting not supported',
            $query->sort->expression->stringify
        ));
    }

    return;
}

sub _cond_as_sub {
    my $self = shift;
    my $condition = shift;

    my $should_trace = $self->log->is_trace;

    if ($condition->isa('Syzygy::Query::Expression::Conjunction')) {
        my @sub_subs = map { $self->_cond_as_sub($_) } $condition->all_expressions;

        return sub {
            $self->log->trace(sprintf(
                '"%s": empty conjunction',
                $condition->stringify
            )) if $should_trace;

            return szg_value(boolean => 0);
        } unless scalar @sub_subs;

        return sub {
            my $entry = shift;

            $self->log->trace(sprintf(
                '"%s": conjunction testing',
                $condition->stringify,
            )) if $should_trace;

            my $ok = reduce { $a && $b } 1, map { $_->($entry)->value } @sub_subs;

            return szg_value(boolean => $ok ? 1 : 0);
        };
    }

    if ($condition->isa('Syzygy::Query::Expression::Disjunction')) {
        my @sub_subs = map { $self->_cond_as_sub($_) } $condition->all_expressions;

        return sub {
            $self->log->trace(sprintf(
                '"%s": empty conjunction',
                $condition->stringify
            )) if $should_trace;

            return szg_value(boolean => 0);
        } unless scalar @sub_subs;

        return sub {
            my $entry = shift;

            $self->log->trace(sprintf(
                '"%s": conjunction testing',
                $condition->stringify,
            )) if $should_trace;

            for my $sub_sub (@sub_subs) {
                return szg_value(boolean => 1) if $sub_sub->($entry)->value;
            }

            return szg_value(boolean => 0);
        };
    }

    if ($condition->isa('Syzygy::Query::Expression::Comparison')) {
        my @sub_subs = map { $self->_cond_as_sub($_) } $condition->all_expressions;

        return sub {
            $self->log->trace(sprintf(
                '"%s": invalid argument count',
                $condition
            )) if $should_trace;

            return szg_value(boolean => 0);
        } unless scalar @sub_subs == 2;

        my ($a, $b) = @sub_subs;

        return sub {
            my $entry = shift;

            $self->log->trace(sprintf(
                '"%s": testing equality',
                $condition->stringify
            )) if $should_trace;

            my $value_a = $a->($entry);
            my $value_b = $b->($entry);

            unless (defined $value_a && defined $value_b) {
                return szg_value(boolean => 0);
            }

            my $value_type = $value_a->type;
            my $comparator = $value_type->can($condition->mode);

            unless (defined $comparator) {
                throw('simple_store/exec_query/cmp_mode_unsupported', sprintf(
                    '"%s" comparison not supported for type "%s"',
                    $condition->mode,
                    $value_a->type
                ));
            }

            return szg_value(
                boolean => ($comparator->($value_type, $value_a, $value_b) ? 1 : 0)
            );
        }
    }

    if ($condition->isa('Syzygy::Query::Expression::Field')) {
        return sub {
            my $entry = shift;

            $self->log->trace(sprintf(
                '"%s": retrieving field',
                $condition->stringify
            )) if $should_trace;

            return $entry->get_value($condition->name);
        };
    }

    if ($condition->isa('Syzygy::Query::Expression::Literal')) {
        return sub {
            $self->log->trace(sprintf(
                '"%s": literal value evaluation',
                $condition->stringify
            )) if $should_trace;

            return szg_value($condition->type, $condition->value);
        }
    }

    throw('simple_store/exec_query/unsupported_condition_expression', sprintf(
        'Condition "%s" is unsupported', $condition->stringify
    ));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
