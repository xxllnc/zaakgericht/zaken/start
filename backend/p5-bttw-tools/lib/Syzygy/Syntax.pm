package Syzygy::Syntax;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

=head1 NAME

Syzygy::Syntax - Object syntax helpers

=head1 SYNOPSIS

    package My::Fancy::Package;

    use Syzygy::Syntax;

    sub some_method {
        ...
        my $object_value = szg_val('string', 'my_value');
    }

=cut

use BTTW::Tools;
use List::Util qw[uniq];
use Moose::Exporter;

use Syzygy::Object::Reference;
use Syzygy::Object::Model;
use Syzygy::Query;
use Syzygy::Types qw[Name ValueType];

Moose::Exporter->setup_import_methods(
    also => 'Syzygy::Query',
    with_meta => [qw[
        szg_attr
    ]],
    as_is => [qw[
        szg_value
        szg_object_ref
    ]]
);

=head1 FUNCTIONS

=head2 szg_attr

Syzygy equivalent of the L<Moose/has> helper.

Adds a L<Moose::Meta::Attribute> to the calling package, while setting
defaults for L<Syzygy::Interface::MooseAttribute>.

    package My::Object::Type;

    use Syzygy::Syntax;

    szg_attr name => (
        value_type_name => 'string'
    );

=cut

sub szg_attr {
    my ($meta, $name, %opts) = @_;

    my $model = Syzygy::Object::Model->get_instance;

    unless (Name->check($name)) {
        throw('syzygy/object/attribute/name_invalid', sprintf(
            'Attribute name "%s" is not valid',
            $name // '<undef>'
        ));
    }

    # Mangle default trait with provided traits
    my @traits = @{ delete $opts{ traits } || [] };
    $opts{ traits } = [ uniq @traits, 'Syzygy::Interface::MooseAttribute' ];

    my $value_type = delete $opts{ value_type };
    my $registered_attribute = $model->get_attribute("sys/${name}");

    unless (defined $value_type) {
        my $value_type_name = delete $opts{ value_type_name };

        unless (defined $value_type_name) {
            throw('syzygy/object/attribute/value_type_name_missing', sprintf(
                'Attribute "%s": missing value_type or value_type_name',
                $name
            ));
        }

        unless (Name->check($value_type_name)) {
            throw('syzygy/object/attribute/value_type_name_invalid', sprintf(
                'Attribute "%s": value type name "%s" is invalid',
                $name,
                $value_type_name
            ));
        }

        unless ($model->has_value_type($value_type_name)) {
            throw('syzygy/object/attribute/value_type_invalid', sprintf(
                'Attribute "%s": value type "%s" not found',
                $name,
                $value_type_name
            ));
        }

        $value_type = $model->get_value_type($value_type_name);
    }

    unless (ValueType->check($value_type)) {
        throw('syzygy/object/attribute/value_type_missing', sprintf(
            'Attribute "%s": no value type specified (either value_type or value_type_name required)',
            $name
        ));
    }

    my $attribute = Syzygy::Object::Attribute->new(
        name => $name,
        value_type => $value_type,
        namespace => 'sys'
    );

    $model->index_attribute($attribute);

    $opts{ is } //= 'rw';
    $opts{ isa } = $value_type->perl_type_constraint;
    $opts{ szg_attribute } = $attribute;
    $opts{ szg_name } = $name;

    return $meta->add_attribute(sprintf('_szg_attr_%s', $name), %opts);
}

=head2 szg_value

Construct a new L<Syzygy::Value> instance.

    my $val = szg_value('datetime', Datetime->now->substract(days => 5));

=cut

sub szg_value {
    return Syzygy::Object::Model->new_value(@_);
}

=head2 szg_object_ref

Construct a new L<Syzygy::Value> using the
L<Syzygy::ValueType::ObjectRef> type.

    # $val->value will be a Syzygy::Reference::Instance
    my $val = szg_object_ref('case', '1728ab08-ba50-40b1-a4cc-4ff8ab78234e');

=cut

sub szg_object_ref {
    return Syzygy::Object::Model->new_value(
        object_ref => Syzygy::Object::Reference->new(
            type => shift,
            id => shift
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
