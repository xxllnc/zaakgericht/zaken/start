package Zaaksysteem::InstanceConfig::Source::Redis;
use namespace::autoclean;

use BTTW::Tools;
use Config::General;
use IO::Socket::SSL qw(SSL_VERIFY_PEER);
use Moose;
use Redis;

with 'Zaaksysteem::InstanceConfig::SourceInterface';

=head1 NAME

Zaaksysteem::InstanceConfig::Source::Redis - Redis implementation of
L<Zaaksysteem::InstanceConfig::SourceInterface>.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 server

Redis server hostname to connect to.

=cut

has server => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

=head2 ssl

Boolean, indicating whether a TLS connection should be used.

=cut

has ssl => (
    is => 'ro',
    isa => 'Bool',
    required => 0,
    default => 0,
);

=head2 auth_token

Token to authenticate the client against the specified Redis server.

=cut

has auth_token => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_auth_token'
);

=head2 name

Named identifier of the client, this is useful when listing connected clients
on the server. Defaults to C<Zaaksysteem>.

=cut

has name => (
    is => 'ro',
    isa => 'Str',
    default => 'Zaaksysteem'
);

=head2 instance_key_format

Defines a instance key C<fmt>, to be used when looking up specific instance
configurations

=cut

has instance_key_format => (
    is => 'ro',
    isa => 'Str',
    default => 'saas:instance:%s'
);

=head2 redis_client

Reference to a connected L<Redis> instance.

=cut

has redis_client => (
    is => 'rw',
    isa => 'Redis',
    lazy => 1,
    builder => 'build_redis_client'
);

=head1 CONSTRUCTOR

Upon construction of this client the connected-ness of the server is tested
with a PING request. If the assertion fails, an exception of type
C<instance_config/redis/connection_assertion_failed> will be thrown.

=cut

sub BUILD {
    my $self = shift;

    $self->assert_connection;
}

=head1 METHODS

=head2 assert_connection

Performs a connection-alive check using Redis a C<PING> request.

Throws a C<instance_config/redis/connection_assertion_failed> exception if
no valid response is received.

=cut

sub assert_connection {
    my $self = shift;

    my $response = $self->redis_client->ping;

    unless (defined $response && $response eq 'PONG') {
        throw(
            'instance_config/redis/connection_assertion_failed',
            'Could nog PING Redis server, assuming not connected'
        );
    }

    return;
}

=head2 find_instance_config_by_hostname

Implements required L<Zaaksysteem::InstanceConfig::SourceInterface|interface>.

Performs a Redis C<GET> request for a given hostname.

=cut

sub find_instance_config_by_hostname {
    my $self = shift;
    my $hostname = shift;

    my $key = sprintf($self->instance_key_format, $hostname);

    my $response = $self->redis_client->get($key);

    unless (defined $response) {
        throw('instance_config/redis/instance_lookup_failed', sprintf(
            'Empty response from Redis for instance configuration lookup with key %s',
            $key
        ));
    }

    my $config = Config::General->new(
        -String => $response,
        -ForceArray => 1
    );

    my %parsed_config = $config->getall;

    return $parsed_config{instance};
}

=head2 all_instance_hosts

Implements required L<Zaaksysteem::InterfaceConfig::SourceInterface|interface>.

Performs a Redis C<SMEMBERS> request for all hostnames.

=cut

sub all_instance_hosts {
    my $self = shift;

    return $self->redis_client->smembers('saas:instances');
}

=head2 build_redis_client

=cut

sub build_redis_client {
    my $self = shift;

    my %args = (
        server => $self->server,
        name => $self->name,
        reconnect => 2,
        ssl => $self->ssl,
        $self->ssl
            ? (SSL_verify_mode => SSL_VERIFY_PEER)
            : (),
    );

    if ($self->has_auth_token) {
        $args{ password } = $self->auth_token;
    }

    return Redis->new(%args);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
