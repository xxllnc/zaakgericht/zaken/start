use Test::More;
use Test::Exception;

use BTTW::Tools qw(date_filter assert_date);
use DateTime;

ok !defined date_filter(), 'no arguments returns undef';
is date_filter('19871010'), '19871010', '\d{8} dates parsed';
is date_filter('1987-10-10'), '19871010', 'YMD dates parsed';
is date_filter('1987-10-10T10:10:10Z'), '19871010', 'YMD-HMSZ datetimes parsed';
ok !defined date_filter('1987-10-10T10:10:10'), 'YMD-HMS datetime does not parse';
is date_filter(DateTime->new(year => 1987, month => 10, day => 10)), '19871010',
    'DateTime instance dates parsed';

my $date = assert_date('08-11-1979');
isa_ok($date, 'DateTime');
is("$date", "1979-11-08T00:00:00");

$date = assert_date($date);
$date = assert_date('1979-11-08');

throws_ok(
    sub {
        assert_date('1979-31-12');
    },
    qr/assert_date: Incorrect datumformaat/,
    "assert date dies correctly",
);

done_testing;
