package Syzygy::Test::Object::ValueTypes::Array;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Object::ValueTypes::Array;

sub test_object_value_type_text {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::Array->new }
        'array value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::Array',
        'array value type constructor retval';

    is $type->name, 'array', 'array value type has expected name';

    my $array = $type->new_value([]);

    is scalar @{ $array->value }, 0, 'empty value preserved after constructing';

    my @random_values = (1, 'abc', undef, $array);

    my $second = $type->new_value(\@random_values);

    is_deeply $second->value, \@random_values, 'array contents preserved';

    $random_values[1] = 'xyz';

    is_deeply $second->value, \@random_values, 'array stored by reference';

    is_deeply $type->deflate_value($second), \@random_values,
        'array value deflation is simple array';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
