package Syzygy::Test::Object::ValueTypes::UUID;

use Test::Class::Moose;
use Test::Fatal;

use BTTW::Tools::RandomData qw(generate_uuid_v4);
use Syzygy::Object::ValueTypes::String;
use Syzygy::Object::ValueTypes::UUID;

sub test_object_value_type_uuid {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::UUID->new }
        'uuid value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::UUID',
        'uuid value type constructor retval';

    is $type->name, 'uuid', 'uuid value type has expected name';

    my $a_uuid = generate_uuid_v4;
    my $b_uuid = generate_uuid_v4;

    my $a = $type->new_value($a_uuid);
    my $b = $type->new_value($b_uuid);

    ok $type->equal($a, $a), 'a == a: positive equality';
    ok $type->not_equal($a, $b), 'a != b: positive inequality';
    ok !$type->equal($a, $b), 'a == b: negative equality';
    ok !$type->not_equal($b, $b), 'b != b: negative inequality';

    is_deeply $type->deflate_value($a), $a_uuid,
        'type value deflation returned plain string';
}

sub test_object_value_type_uuid_coercions {
    my $type = Syzygy::Object::ValueTypes::UUID->new;

    my $string = Syzygy::Object::ValueTypes::String->new->new_value(
        generate_uuid_v4
    );

    my $coerced = $type->new_value($string);

    is $coerced->type_name, 'uuid', 'coerced value has expected type';

    is $coerced->value, $string->value, 'coerced value preserves content';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
