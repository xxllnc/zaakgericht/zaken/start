// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import zsModalModule from '../../../shared/ui/zsModal';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import template from './index.html';
import resourceModule from '../../../shared/api/resource';
import seamlessImmutable from 'seamless-immutable';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import omitBy from 'lodash/omitBy';
import './styles.scss';

module.exports = angular
  .module('Zaaksysteem.intern.about.route', [
    angularUiRouterModule,
    zsModalModule,
    auxiliaryRouteModule,
    resourceModule,
    snackbarServiceModule,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      $stateProvider.state('about', {
        url: '/over',
        auxiliary: true,
        controllerAs: 'vm',
        resolve: {
          versionInfo: [
            '$rootScope',
            'resource',
            '$q',
            'snackbarService',
            ($rootScope, resource, $q, snackbarService) => {
              let versionResource = resource(
                { url: '/api/v1/general/meta' },
                { scope: $rootScope }
              ).reduce((requestOptions, data) => {
                return data || seamlessImmutable([]);
              });

              return versionResource
                .asPromise()
                .then(() => versionResource)
                .catch((err) => {
                  snackbarService.error(
                    'De versieinformatie kon niet worden geladen.'
                  );

                  return $q.reject(err);
                });
            },
          ],
        },
        onActivate: [
          '$http',
          '$state',
          '$timeout',
          '$rootScope',
          '$window',
          '$document',
          '$compile',
          'zsModal',
          'versionInfo',
          (
            $http,
            $state,
            $timeout,
            $rootScope,
            $window,
            $document,
            $compile,
            zsModal,
            versionInfo
          ) => {
            let openModal = () => {
              let modal,
                unregister,
                scope = $rootScope.$new(true),
                cleanedInfo = omitBy(
                  versionInfo.data(),
                  (item) => item.instance.label === 'Customer information'
                );

              scope.getInfo = () => cleanedInfo;

              modal = zsModal({
                el: $compile(angular.element(template))(scope),
                title: 'Over xxllnc Zaken',
                classes: 'about-modal center-modal',
              });

              modal.onClose(() => {
                $state.go('^');
                return true;
              });

              modal.open();

              unregister = $rootScope.$on('$stateChangeStart', () => {
                $window.requestAnimationFrame(() => {
                  $rootScope.$evalAsync(() => {
                    modal.close().then(() => {
                      scope.$destroy();
                    });

                    unregister();
                  });
                });
              });
            };

            $window.requestAnimationFrame(() => {
              $rootScope.$evalAsync(openModal);
            });
          },
        ],
      });
    },
  ]).name;
