// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import seamlessImmutable from 'seamless-immutable';

export default (resource, scope) => {
  return resource(
    () => {
      return {
        url: '/api/v1/subject/role',
        params: {
          rows_per_page: 100,
        },
      };
    },
    { scope }
  ).reduce((requestOptions, data) => {
    return data || seamlessImmutable([]);
  });
};
