// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from '../../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.scss';

module.exports = angular
  .module('zsCaseRelationView', [angularUiRouterModule, reactIframeModule])
  .directive('zsCaseRelationView', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          caseData: '&',
          reloadCaseResource: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this;

            ctrl.getStartUrl = () => {
              return `/external-components/exposed/case/${ctrl.caseUuid()}/relations`;
            };

            scope.$on('$destroy', () => {
              ctrl.reloadCaseResource();
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
