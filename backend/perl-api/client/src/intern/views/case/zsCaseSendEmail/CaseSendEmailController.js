// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';
import email from '../forms/email';
import ensureArray from './../../../../shared/util/array/ensureArray';
import getPreviewObjectFromValues from '../library/getPreviewObjectFromValues';

export default class CaseSendEmailController {
  static get $inject() {
    return [
      '$scope',
      '$http',
      'resource',
      'composedReducer',
      'vormValidator',
      'snackbarService',
    ];
  }

  constructor(
    scope,
    $http,
    resource,
    composedReducer,
    vormValidator,
    snackbarService
  ) {
    const ctrl = this;
    let values = seamlessImmutable({});
    let previewData = seamlessImmutable({});
    let previewActive = false;

    const formReducer = composedReducer(
      { scope },
      ctrl.templates,
      ctrl.requestor
    ).reduce((templates, requestor) => {
      const subjectTypeV2 = {
        natuurlijk_persoon: 'person',
        bedrijf: 'organization',
        medewerker: 'employee',
      };
      const requestorName = get(requestor, 'instance.name', 'Onbekend');
      const subjectType = get(requestor, 'instance.subject_type');
      const uuid = get(requestor, 'instance.uuid');
      const requestorLink = `/main/contact-view/${subjectTypeV2[subjectType]}/${uuid}`;
      const emailTemplateData = ctrl.emailTemplateData();

      if (templates && requestorName) {
        return email({
          templates: seamlessImmutable(templates).asMutable({ deep: true }),
          presetRequestor:
            get(requestor, 'instance.preset_client', 'Nee') === 'Ja',
          requestorName,
          requestorLink,
          customHtmlTemplateName: emailTemplateData
            ? emailTemplateData.label
            : null,
        });
      }

      return null;
    });

    formReducer.onUpdate(() => {
      let form = formReducer.data();

      if (form) {
        values = values.merge(seamlessImmutable(form.getDefaults())).merge([
          values,
          {
            requestor_address: get(ctrl.requestor(), 'instance.email'),
          },
        ]);
      }
    });

    const roleReducer = resource('/api/v1/subject/role', { scope }).reduce(
      (options, data) => {
        if (data) {
          return data.asMutable().map((item) => {
            const { label } = item.instance;

            return {
              label,
              value: label,
            };
          });
        }

        return null;
      }
    );

    const fieldReducer = composedReducer(
      { scope },
      formReducer,
      roleReducer
    ).reduce((form, roleOptions) => {
      return form ? form.fields(roleOptions) : [];
    });

    const validityReducer = composedReducer(
      { scope },
      fieldReducer,
      () => values
    ).reduce(vormValidator);

    const getPreviewFromValues = getPreviewObjectFromValues(scope);

    ctrl.getValues = () => values;

    ctrl.handleChange = (name, value) => {
      values = formReducer.data().processChange(name, value, values);
    };

    ctrl.getValidity = () => get(validityReducer.data(), 'validations');

    ctrl.isDisabled = () => !get(validityReducer.data(), 'valid', false);

    ctrl.getFields = fieldReducer.data;

    ctrl.getPreviewData = () => previewData;

    ctrl.getPreviewActive = () => previewActive;

    ctrl.togglePreview = () => {
      previewData = previewData.merge(getPreviewFromValues(values));
      previewActive = !previewActive;
    };

    ctrl.handleSubmit = () => {
      const data = {
        recipient_type: values.recipient_type,
        betrokkene_role: values.betrokkene_role,
        log_error: 0,
        notificatie_cc: values.recipient_cc,
        notificatie_bcc: values.recipient_bcc,
        notificatie_onderwerp: values.email_subject,
        notificatie_bericht: values.email_content,
        update: 1,
        no_redirect: 1,
        mailtype: values.template ? 'bibliotheek_notificatie' : 'specific_mail',
      };
      let promise;

      if (values.recipient_type === 'behandelaar') {
        const coworkerIds = ensureArray(values.behandelaar).map((entry) => ({
          id: entry.data.id,
        }));

        data.medewerker_betrokkene_id = coworkerIds;
      } else if (values.recipient_type === 'overig') {
        data.notificatie_email = values.recipient_address;
      }

      promise = $http({
        url: `/zaak/${ctrl.caseId()}/send_email`,
        method: 'POST',
        data,
      });

      snackbarService.wait('Uw e-mail wordt verstuurd.', {
        promise,
        then: () => 'Uw e-mail is verstuurd.',
        catch: () => 'De e-mail kon niet worden verstuurd.',
      });

      ctrl.onEmailSend({ $promise: promise });
    };
  }
}
