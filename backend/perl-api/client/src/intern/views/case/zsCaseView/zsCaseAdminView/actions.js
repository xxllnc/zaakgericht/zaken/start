// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import propCheck from './../../../../../shared/util/propCheck';
import identity from 'lodash/identity';
import assign from 'lodash/assign';
import get from 'lodash/get';
import pickBy from 'lodash/pickBy';
import first from 'lodash/head';
import keys from 'lodash/keys';
import map from 'lodash/map';
import snackbarServiceModule from './../../../../../shared/ui/zsSnackbar/snackbarService';
import {
  assertCaseAllocationData,
  setCaseAllocation,
} from './../../../../../shared/case/caseActions/allocation.js';

module.exports = angular
  .module('zsCaseAdminActions', [snackbarServiceModule, angularUiRouterModule])
  .factory('zsCaseAdminActions', [
    '$q',
    '$state',
    'snackbarService',
    'dateFilter',
    ($q, $state, snackbarService, dateFilter) => {
      let simpleDate = (time) => dateFilter(time, 'dd-MM-yyyy');

      return [
        {
          type: 'case/allocate',
          request: (mutationData) => {
            assertCaseAllocationData(mutationData);
            return setCaseAllocation(mutationData);
          },
          reduce: (data /*, mutationData*/) => {
            return data;
          },
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            const redirect = pickBy(
              {
                case:
                  mutationData.allocationType === 'assign_to_self' &&
                  $state.current.name.indexOf('home') === 0,
                home:
                  mutationData.allocationType !== 'assign_to_self' &&
                  $state.current.name.indexOf('case.') === 0,
              },
              identity
            );
            const redirectTo = first(keys(redirect));
            const redirectMsg =
              redirectTo === 'case'
                ? '. U wordt doorverwezen naar de zaak.'
                : redirectTo === 'home'
                ? '. U wordt doorverwezen naar het dashboard.'
                : '';

            return snackbarService
              .wait('De toewijzing wordt gewijzigd', {
                promise,
                then: () => {
                  return `${
                    mutationData.allocationType === 'assign_to_self'
                      ? 'De zaak is aan u toegewezen'
                      : 'De toewijzing is gewijzigd'
                  }${redirectMsg}`;
                },
                catch: (data) => {
                  return get(data, 'data.json.auth_error') !== true
                    ? 'De toewijzing kon niet worden gewijzigd. Neem contact op met uw beheerder voor meer informatie.'
                    : 'Toewijzing niet toegestaan; behandelaar heeft onvoldoende rechten.';
                },
              })
              .then(() => {
                if (redirectTo) {
                  return redirectTo === 'case'
                    ? $state.go('case', { caseId: mutationData.caseId })
                    : $state.go('home');
                }
                return $q.when(false);
              });
          },
        },
        {
          type: 'case/suspend',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                reason: propCheck.string,
                termType: propCheck.oneOf(['indeterminate', 'determinate']),
                termAmount:
                  mutationData.termType === 'indeterminate'
                    ? propCheck.any.optional
                    : propCheck.any,
                termAmountType:
                  mutationData.termType === 'indeterminate'
                    ? propCheck.any.optional
                    : propCheck.oneOf([
                        'calendar_days',
                        'weeks',
                        'work_days',
                        'fixed_date',
                      ]),
              }),
              mutationData
            );

            let params = {
              selected_case_ids: mutationData.caseId,
              selection: 'one_case',
              reden: mutationData.reason,
              schedule_resume: mutationData.termType === 'determinate' ? 1 : 0,
              commit: 1,
            };

            if (mutationData.termType === 'determinate') {
              params.suspension_term_type = {
                calendar_days: 'kalenderdagen',
                weeks: 'weken',
                work_days: 'werkdagen',
                fixed_date: 'einddatum',
              }[mutationData.termAmountType];

              if (mutationData.termAmountType === 'fixed_date') {
                params.suspension_term_amount = simpleDate(
                  mutationData.termAmount
                );
              } else {
                params.suspension_term_amount = mutationData.termAmount;
              }
            }

            return {
              url: `/zaak/${mutationData.caseId}/update/opschorten`,
              data: params,
              method: 'POST',
            };
          },
          reduce: (data /*, mutationData*/) => data,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('De zaak wordt opgeschort', {
              promise,
              then: () => 'De zaak is opgeschort',
              catch: () =>
                'De zaak kon niet worden opgeschort. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/resolve_prematurely',
          request: (mutationData) => {
            let data;

            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                reason: propCheck.string,
                result_id: propCheck.string,
              }),
              mutationData
            );

            data = {
              reden: mutationData.reason,
              system_kenmerk_resultaat_id: mutationData.result_id,
              update: 1,
            };

            return {
              url: `/zaak/${mutationData.caseId}/update/afhandelen`,
              data,
            };
          },
          reduce: (data) => data,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait(
              'De zaak wordt vroegtijdig afgehandeld',
              {
                promise,
                then: () => 'De zaak is vroegtijdig afgehandeld',
                catch: () =>
                  'De zaak kon niet vroegtijdig worden afgehandeld. Neem contact op met uw beheerder voor meer informatie.',
              }
            );
          },
        },
        {
          type: 'case/prolong',
          request: (mutationData) => {
            let params;

            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                prolongationType: propCheck.oneOf(['fixedDate', 'changeTerm']),
                reason: propCheck.string,
                prolongationDate:
                  mutationData.prolongationType === 'fixedData'
                    ? propCheck.any
                    : propCheck.any.optional,
                termType:
                  mutationData.prolongationType === 'fixedDate'
                    ? propCheck.any.optional
                    : propCheck.string,
                termAmount:
                  mutationData.prolongationType === 'fixedDate'
                    ? propCheck.any.optional
                    : propCheck.any,
              }),
              mutationData
            );

            params = {
              selected_case_ids: mutationData.caseId,
              selection: 'one_case',
              prolongation_type: mutationData.prolongationType,
              reden: mutationData.reason,
              commit: 1,
            };

            if (mutationData.prolongationType === 'fixedDate') {
              params.datum = simpleDate(mutationData.prolongationDate);
            } else {
              params.term_type = {
                calendar_days: 'kalenderdagen',
                weeks: 'weken',
                work_days: 'werkdagen',
              }[mutationData.termType];
              params.amount = mutationData.termAmount;
            }

            return {
              url: `/zaak/${mutationData.caseId}/update/verlengen`,
              method: 'POST',
              data: params,
            };
          },
          reduce: (data) => data,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('De zaak wordt verlengd', {
              promise,
              then: () => 'De zaak is verlengd',
              catch: () =>
                'De zaak kon niet worden verlengd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/resume',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                reason: propCheck.string,
                stalledSince: propCheck.any,
                stalledUntil: propCheck.any,
              }),
              mutationData
            );

            return {
              url: `/zaak/${mutationData.caseId}/update/resume`,
              data: {
                selected_case_ids: mutationData.caseId,
                selection: 'one_case',
                no_redirect: 1,
                commit: 1,
                reden: mutationData.reason,
                stalled_since: simpleDate(mutationData.stalledSince),
                stalled_until: simpleDate(mutationData.stalledUntil),
              },
            };
          },
          reduce: (data) => data,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('De zaak wordt hervat', {
              promise,
              then: () => 'De zaak is hervat',
              catch: () =>
                'De zaak kon niet worden hervat. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/relate',
          request: (mutationData) => {
            let params;

            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                relatedCaseId: propCheck.number,
              }),
              mutationData
            );

            params = {
              selected_case_ids: mutationData.caseId,
              selection: 'one_case',
              commit: 1,
              zaaknr: mutationData.relatedCaseId,
            };

            return {
              url: `/zaak/${mutationData.caseId}/update/relatie`,
              params,
              method: 'GET',
            };
          },
          reduce: (data) => data,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait(
              `Zaak ${mutationData.relatedCaseId} wordt gerelateerd aan de huidige zaak`,
              {
                promise,
                then: () =>
                  `Zaak ${mutationData.relatedCaseId} is gerelateerd aan de huidige zaak`,
                catch: () =>
                  `Zaak ${mutationData.relatedCaseId} kon niet worden gerelateerd aan de huidige zaak. Neem contact op met uw beheerder voor meer informatie.`,
              }
            );
          },
        },
        {
          type: 'case/copy',
          request: (mutationData) => {
            let data;

            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
              }),
              mutationData
            );

            data = {
              confirmed: 1,
              no_redirect: 1,
            };

            return {
              url: `/zaak/duplicate/${mutationData.caseId}`,
              data,
            };
          },
          reduce: identity,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Zaak wordt gekopiëerd', {
              promise,
              then: (response) => {
                let caseId = get(response, 'data.result[0].instance.number');

                return {
                  message: 'Zaak is gekopiëerd',
                  actions: caseId
                    ? [
                        {
                          type: 'link',
                          label: `Zaak ${caseId} openen`,
                          link: $state.href('case', { caseId }),
                        },
                      ]
                    : [],
                };
              },
              catch: () =>
                'Zaak kon niet worden gekopiëerd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/set_setting',
          request: (mutationData) => {
            let data;

            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                settings: propCheck.object,
              }),
              mutationData
            );

            data = assign(
              {
                selected_case_ids: mutationData.caseId,
                no_redirect: 1,
                selection: 'one_case',
                commit: 1,
              },
              mutationData.settings
            );

            return {
              url: `/zaak/${mutationData.caseId}/update/set_settings`,
              data,
            };
          },
          reduce: identity,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Beheeractie wordt uitgevoerd', {
              promise,
              then: () => 'Beheeractie is succesvol uitgevoerd',
              catch: () =>
                'Beheeractie kon niet worden uitgevoerd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/manage_action',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
                settings: propCheck.object,
              }),
              mutationData
            );

            return {
              url: `/api/v0/case/${mutationData.caseId}/update`,
              data: mutationData.settings,
            };
          },
          reduce: identity,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Beheeractie wordt uitgevoerd', {
              promise,
              then: () => 'Beheeractie is succesvol uitgevoerd',
              catch: () =>
                'Beheeractie kon niet worden uitgevoerd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/set_settings',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                settings: propCheck.object,
                type: propCheck.string,
              }),
              mutationData
            );

            return {
              url: `/api/v2/cm/case/${mutationData.type}`,
              data: mutationData.settings,
            };
          },
          reduce: identity,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Beheeractie wordt uitgevoerd', {
              promise,
              then: () => 'Beheeractie is succesvol uitgevoerd',
              catch: () =>
                'Beheeractie kon niet worden uitgevoerd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/update_acls',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                caseReference: propCheck.string,
                acls: propCheck.arrayOf(
                  propCheck.shape({
                    position: propCheck.shape({
                      unit: propCheck.string,
                      role: propCheck.string,
                    }),
                    capabilities: propCheck.arrayOf(
                      propCheck.shape({
                        name: propCheck.string,
                        selected: propCheck.bool.optional,
                      })
                    ),
                  })
                ),
                cascadeTo: propCheck.objectOf(propCheck.bool).optional,
              }),
              mutationData
            );

            return {
              url: `/api/v1/case/${mutationData.caseReference}/acl/update`,
              data: {
                values: mutationData.acls.map((acl) => {
                  return {
                    capabilities: map(
                      acl.capabilities.filter((cap) => cap.selected),
                      'name'
                    ),
                    entity_id: `${acl.position.unit}|${acl.position.role}`,
                    entity_type: 'position',
                    scope: 'instance',
                  };
                }),
                cascade_to: mutationData.cascadeTo,
              },
            };
          },
          reduce: identity,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Rechten worden gewijzigd', {
              promise,
              then: () => 'Rechten zijn succesvol gewijzigd',
              catch: () =>
                'Rechten konden niet worden gewijzigd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/set_payment_status',
          request: (mutationData) => {
            return {
              url: `/api/v1/case/${mutationData.caseReference}/update`,
              data: {
                payment_info: {
                  payment_status: mutationData.paymentStatus,
                },
              },
            };
          },
          reduce: identity,
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Betaalstatus wordt aangepast', {
              promise,
              then: () => 'Betaalstatus is aangepast',
              catch: () =>
                'Het aanpassen van de betaalstatus is mislukt. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
      ];
    },
  ])
  .run([
    'zsCaseAdminActions',
    'mutationService',
    (zsCaseAdminActions, mutationService) => {
      zsCaseAdminActions.forEach((action) => {
        mutationService.register(action);
      });
    },
  ]).name;
