// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormFieldsetModule from './../../../../../shared/vorm/vormFieldset';
import vormInvokeModule from './../../../../../shared/vorm/vormInvoke';
import inputModule from './../../../../../shared/vorm/types/input';
import radioModule from './../../../../../shared/vorm/types/radio';
import formModule from './../../../../../shared/vorm/types/form';
import selectModule from './../../../../../shared/vorm/types/select';
import vormRightPickerModule from './../../../../../shared/zs/vorm/vormRightPicker';
import vormRolePickerModule from './../../../../../shared/zs/vorm/vormRolePicker';
import vormObjectSuggestModule from './../../../../../shared/object/vormObjectSuggest';
import vormValidatorModule from './../../../../../shared/vorm/util/vormValidator';
import caseAttrTemplateCompilerModule from './../../../../../shared/case/caseAttrTemplateCompiler';
import zsCaseAdminAttributeListModule from './zsCaseAdminAttributeList';
import caseActions from './../../../../../shared/case/caseActions';
import roleServiceModule from '../../../../../shared/user/roleService';
import zsModalModule from './../../../../../shared/ui/zsModal';
import actionsModule from './actions';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import get from 'lodash/get';
import find from 'lodash/find';
import includes from 'lodash/includes';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

module.exports = angular
  .module('zsCaseAdminView', [
    composedReducerModule,
    vormFieldsetModule,
    vormInvokeModule,
    actionsModule,
    inputModule,
    radioModule,
    formModule,
    selectModule,
    vormRolePickerModule,
    vormObjectSuggestModule,
    vormValidatorModule,
    caseAttrTemplateCompilerModule,
    zsCaseAdminAttributeListModule,
    vormRightPickerModule,
    zsModalModule,
    roleServiceModule,
  ])
  .directive('zsCaseAdminView', [
    '$state',
    '$compile',
    '$rootScope',
    'composedReducer',
    'vormValidator',
    'caseAttrTemplateCompiler',
    'vormInvoke',
    'zsModal',
    'roleService',
    'sessionService',
    'dateFilter',
    (
      $state,
      $compile,
      $rootScope,
      composedReducer,
      vormValidator,
      caseAttrTemplateCompiler,
      vormInvoke,
      zsModal,
      roleService,
      sessionService,
      dateFilter
    ) => {
      let sessionResource = sessionService.createResource($rootScope, {
        cache: { every: 15 * 60 * 1000 },
      });
      let session = sessionResource.data().instance;
      let roleResource = roleService.createResource($rootScope);
      let compiler = caseAttrTemplateCompiler.clone();

      compiler.registerType('case-admin-attribute-list', {
        control: angular.element(
          `<zs-case-admin-attribute-list
							ng-model
							compiler="vm.compiler()"
						>
						</zs-case-admin-attribute-list>`
        ),
      });

      return {
        restrict: 'E',
        template,
        scope: {
          action: '&',
          caseResource: '&',
          casetypeResource: '&',
          aclResource: '&',
          user: '&',
          onSubmit: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              submitting = false,
              values = seamlessImmutable({}).merge(),
              needsCasetype = includes(
                [
                  'fase',
                  'afhandelen',
                  'kenmerken',
                  'object-relateren',
                  'resultaat',
                ],
                ctrl.action().name
              ),
              needsAcls = ctrl.action().name === 'rechten',
              defaultsReducer,
              valueReducer,
              actionReducer,
              fieldReducer,
              validityReducer,
              messageReducer,
              args;

            args = [ctrl.action(), ctrl.caseResource(), ctrl.user()];

            if (needsCasetype) {
              args = args.concat(ctrl.casetypeResource());
            }

            if (needsAcls) {
              args = args.concat(ctrl.aclResource());
            }

            actionReducer = composedReducer({ scope }, ...args).reduce(
              (action, caseObj, user, casetypeOrAcls) => {
                let casetype = needsCasetype ? casetypeOrAcls : null,
                  acls = needsAcls ? casetypeOrAcls : [],
                  updatedAction = find(
                    caseActions(
                      { caseObj, casetype, user, $state, acls },
                      dateFilter
                    ),
                    { name: action.name }
                  );

                return updatedAction;
              }
            );

            defaultsReducer = composedReducer({ scope }, actionReducer).reduce(
              (action) => {
                let defaults = seamlessImmutable(
                  mapValues(keyBy(action.fields, 'name'), 'defaults')
                );

                return defaults;
              }
            );

            valueReducer = composedReducer(
              { scope },
              defaultsReducer,
              () => values
            ).reduce((defaults, vals) => defaults.merge(vals));

            fieldReducer = composedReducer({ scope }, actionReducer).reduce(
              (action) => {
                let fields = get(action, 'fields', []);

                return seamlessImmutable(fields).asMutable({ deep: true });
              }
            );

            validityReducer = composedReducer(
              { scope },
              valueReducer,
              fieldReducer
            ).reduce((vals, fields) => {
              let validation = vormValidator(fields, vals);

              return validation;
            });

            messageReducer = composedReducer(
              { scope },
              actionReducer,
              valueReducer
            ).reduce((action, vals) => {
              let messages = [];

              if (!action) {
                messages = messages.concat({
                  name: 'error',
                  icon: 'alert-circle',
                  classes: {
                    error: true,
                  },
                  label:
                    'De actie kon niet worden geladen. Mogelijk heeft u niet voldoende rechten om deze uit te voeren.',
                });
              } else {
                messages = messages.concat(
                  vormInvoke(action.messages, { $values: vals }) || []
                );
              }

              return messages;
            });

            ctrl.getFields = fieldReducer.data;

            ctrl.isFormValid = () =>
              get(validityReducer.data(), 'valid', false);

            ctrl.getValidity = validityReducer.data;

            ctrl.getSubmitLabel = composedReducer(
              { scope },
              ctrl.action
            ).reduce((action) => action.verb || action.label).data;

            ctrl.isViewOnly = composedReducer({ scope }, ctrl.action).reduce(
              (action) => action.isViewOnly
            ).data;

            ctrl.handleSubmit = () => {
              let action = actionReducer.data(),
                mutation = action.mutate(ctrl.getValues()),
                promise,
                opts =
                  typeof action.options === 'function'
                    ? action.options(ctrl.getValues())
                    : action.options;

              const continueSubmit = () => {
                if (!opts) {
                  opts = {};
                }

                submitting = true;

                promise = ctrl
                  .caseResource()
                  .mutate(mutation.type, mutation.data)
                  .asPromise()
                  .finally(() => {
                    submitting = false;
                  });

                return ctrl.onSubmit({
                  $promise: promise,
                  $reload: !!opts.reloadRoute,
                  $willRedirect: !!opts.willRedirect,
                });
              };

              if (
                action.name === 'toewijzing' &&
                mutation.data.allocationType === 'org-unit' &&
                session.configurable.assignment_department_check
              ) {
                const department = roleResource
                  .data()
                  .find((ou) => ou.org_unit_id === mutation.data.orgUnitId);
                const role = department.roles.find(
                  (ro) => ro.role_id === mutation.data.roleId
                );
                fetch(
                  `/api/v2/cm/case/allocation/check?case_uuid=${
                    ctrl.caseResource().data().reference
                  }&department_uuid=${department.org_unit_uuid}&role_uuid=${
                    role.role_uuid
                  }`
                )
                  .then((res) => res.json())
                  .then((res) => {
                    if (
                      res &&
                      res.data &&
                      res.data.attributes.check_result === false
                    ) {
                      const modalScope = $rootScope.$new(true);

                      modalScope.vm = {
                        values: {},
                        onConfirm: () => {
                          modal.close();
                          continueSubmit();
                        },
                        onCancel: () => {
                          modal.close();
                        },
                      };

                      const modal = zsModal({
                        title: 'Toewijzen',
                        el: $compile(`
                          <div class="modal-warning">
                            <zs-icon icon-type="alert"></zs-icon>
                            <span>De combinatie van afdeling & rol heeft geen toegang tot deze zaak.</span>
                            <div>Weet u zeker dat u door wilt gaan?</div>
                            <div class="button-container">
                              <button ng-click="vm.onCancel()" class="btn btn-secondary btn-flat">Annuleren</button>
                              <button ng-click="vm.onConfirm()" class="btn btn-primary btn-flat">Doorgaan</button>
                            </div>
                          </div>`)(modalScope),
                      });

                      modal.open((el) => {
                        el.style.zIndex = 100;
                      });

                      return;
                    } else {
                      continueSubmit();
                      return;
                    }
                  });
              } else {
                continueSubmit();
              }
            };

            ctrl.getValues = valueReducer.data;

            ctrl.handleChange = (name, value) => {
              let action = actionReducer.data();

              values = values.merge({ [name]: value });

              if (action.processChange) {
                values = action.processChange(name, value, ctrl.getValues());
              }
            };

            ctrl.getAction = actionReducer.data;

            ctrl.hasWarning = () =>
              actionReducer.state() === 'resolved' && !ctrl.getAction();

            ctrl.isSubmitting = () => submitting;

            ctrl.isLoading = () => {
              return actionReducer.state() === 'pending';
            };

            ctrl.getCompiler = () => compiler;

            ctrl.getMessages = messageReducer.data;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
