// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsModalModule from './../../../../../../shared/ui/zsModal';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import snackbarServiceModule from './../../../../../../shared/ui/zsSnackbar/snackbarService';
import reactIframeModule from './../../../../../../shared/ui/zsReactIframe';
import shortid from 'shortid';
import seamlessImmutable from 'seamless-immutable';
import template from './template.html';
import './styles.scss';

module.exports = angular
  .module('zsCaseCustomObject', [
    zsModalModule,
    reactIframeModule,
    composedReducerModule,
    snackbarServiceModule,
  ])
  .directive('zsCaseCustomObject', [
    '$compile',
    'zsModal',
    'composedReducer',
    'snackbarService',
    ($compile, zsModal, composedReducer, snackbarService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          caseData: '&',
          disabled: '&',
          required: '&',
          reloadCustomObjects: '&',
          customObjects: '&',
          objectTypeUuid: '&',
          objectLabel: '@',
          objectActionLabel: '=',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            const ctrl = this;
            let modal;
            let modalScope;
            let customObjectReducer;

            let closeModal = () => {
              modal.close();
              modalScope.$destroy();
              modal = modalScope = null;
            };

            customObjectReducer = composedReducer(
              { scope },
              ctrl.customObjects
            ).reduce((customObjects) => {
              return seamlessImmutable(customObjects || []).map(
                (customObject) => {
                  const {
                    attributes: { title, version_independent_uuid, status },
                  } = customObject;

                  return {
                    $id: shortid(),
                    versionIndependentUuid: version_independent_uuid,
                    label: title,
                    active: status === 'active',
                  };
                }
              );
            });

            ctrl.getCustomObjects = customObjectReducer.data;

            ctrl.openModal = (objectUuid) => {
              const isUpdateMode = Boolean(objectUuid);
              const iframeUrl = isUpdateMode
                ? `/external-components/exposed/case/${ctrl.caseUuid()}/object/update/${objectUuid}?objectTypeUuid=${ctrl.objectTypeUuid()}`
                : `/external-components/exposed/case/${ctrl.caseUuid()}/object/create/?objectTypeUuid=${ctrl.objectTypeUuid()}`;

              modalScope = scope.$new();

              modalScope.caseData = ctrl.caseData();

              modalScope.route = () => iframeUrl;

              const snackbarTextDictionary = {
                created: 'Het object is aangemaakt.',
                updated: 'Het object is bewerkt.',
                deactivated: 'Het object is uitgeschakeld.',
              };

              modalScope.onMessage = (message) => {
                const snackText = snackbarTextDictionary[message.data.result];
                // this is using the uuid specific for this version of the object
                // the version_indepedent_uuid is not known yet when creating a new object
                const link = `/main/object/${message.data.uuid}`;

                if (message && message.type === 'handleObjectSuccess') {
                  ctrl.reloadCustomObjects();
                  closeModal();
                  scope.$apply();
                  snackbarService.info(snackText, {
                    actions: [
                      {
                        type: 'link',
                        label: 'Object openen',
                        link,
                      },
                    ],
                  });
                }
              };

              modal = zsModal({
                title: isUpdateMode ? 'Object bewerken' : 'Object aanmaken',
                fullWidth: true,
                el: $compile(
                  `<zs-react-iframe
                  iframe-src="route()"
                  on-message="onMessage(data)"
                  case-data="caseData"
                  data-height="600"
                ></zs-react-iframe>`
                )(modalScope),
              });

              modal.open();
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
