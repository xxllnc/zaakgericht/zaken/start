// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from './../../../../../../shared/api/resource';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import zsIconModule from './../../../../../../shared/ui/zsIcon';
import zsModalModule from './../../../../../../shared/ui/zsModal';
import zsCaseObjectMutationFormModule from './zsCaseObjectMutationForm';
import zsConfirmModule from './../../../../../../shared/ui/zsConfirm';
import first from 'lodash/head';
import get from 'lodash/get';
import omit from 'lodash/omit';
import assign from 'lodash/assign';
import pickBy from 'lodash/pickBy';
import find from 'lodash/find';
import isArray from 'lodash/isArray';
import startsWith from 'lodash/startsWith';
import keyBy from 'lodash/keyBy';
import mapKeys from 'lodash/mapKeys';
import shortid from 'shortid';
import seamlessImmutable from 'seamless-immutable';
import template from './template.html';
import './styles.scss';

module.exports = angular
  .module('zsCaseObjectMutationList', [
    resourceModule,
    composedReducerModule,
    zsIconModule,
    zsModalModule,
    zsCaseObjectMutationFormModule,
    zsConfirmModule,
  ])
  .directive('zsCaseObjectMutationList', [
    '$compile',
    'resource',
    'composedReducer',
    'zsModal',
    'zsConfirm',
    ($compile, resource, composedReducer, zsModal, zsConfirm) => {
      return {
        restrict: 'E',
        template,
        scope: {
          mutationsLoading: '&',
          capabilities: '&',
          objectId: '&',
          objectLabel: '@',
          onMutationCreate: '&',
          onMutationRemove: '&',
          onMutationUpdate: '&',
          mutations: '&',
          disabled: '&',
          defaults: '&',
          ruleResult: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              mutationReducer,
              objectTypeResource = resource(
                {
                  url: '/api/object/search',
                  params: {
                    zql: `SELECT {} FROM type WHERE object.uuid = "${ctrl.objectId()}"`,
                  },
                },
                { scope }
              ),
              modal,
              modalScope;

            let closeModal = () => {
              modal.close();

              modalScope.$destroy();

              modal = modalScope = null;
            };

            let getLabel = (values) => {
              let match = objectTypeResource
                  .data()
                  .values.title_template.match(/^\[\[(.*)\]\]$/),
                label = (match && values[match[1]]) || '';

              return isArray(label) ? first(label) : label;
            };

            let createMutation = (type, values, object) => {
              return ctrl.onMutationCreate({
                $type: type,
                $values:
                  type === 'create' || type === 'update'
                    ? omit(values, '$object')
                    : null,
                $objectId: get(object, 'id'),
                $label: getLabel(values),
              });
            };

            let updateMutation = (mutation, values) => {
              let type = mutation.type;

              return ctrl.onMutationUpdate({
                $mutation: find(ctrl.mutations(), { id: mutation.id }),
                $values: type === 'create' || type === 'update' ? values : null,
                $label: getLabel(values),
              });
            };

            let openModal = (type, mutation) => {
              const updateMode = Boolean(mutation);
              modalScope = scope.$new();

              modalScope.handleSubmit = (values, object) => {
                let promise;

                if (updateMode) {
                  promise = updateMutation(mutation, values);
                } else {
                  promise = createMutation(type, values, object);
                }

                modal.hide();

                promise
                  .then(() => {
                    closeModal();
                  })
                  .catch(() => {
                    modal.show();
                  });
              };

              if (updateMode) {
                modalScope.defaults = assign(
                  mapKeys(
                    pickBy(mutation.values, (value, key) =>
                      startsWith(key, 'attribute.')
                    ),
                    (value, key) => key.replace('attribute.', '')
                  )
                );

                modalScope.objectId = mutation.object_uuid;
              } else if (type === 'create' || type === 'update') {
                modalScope.defaults = ctrl.defaults();
              }

              modal = zsModal({
                title: ctrl.capabilities()[type].label,
                el: $compile(
                  `<zs-case-object-mutation-form
                  form-mode="${updateMode ? 'update' : 'create'}"
                  object-type-resource="vm.getObjectTypeResource()"
                  object-id="objectId"
                  object-label="'${ctrl.objectLabel}'"
                  mutation-type="'${type}'"
                  mutation-verb="Mutatie ${mutation ? 'wijzigen' : 'toevoegen'}"
                  on-submit="handleSubmit($values, $object)"
                  data-defaults="defaults"
                  on-cancel="vm.handleCancel()"
                  rule-result="vm.ruleResult()"
                ></zs-case-object-mutation-form>`
                )(modalScope),
              });

              modal.onClose(() => {
                closeModal();

                return false;
              });

              modal.open();
            };

            mutationReducer = composedReducer({ scope }, ctrl.mutations).reduce(
              (mutations) => {
                let labels = {
                  create: ['Aan te maken', 'Aangemaakt'],
                  update: ['Te wijzigen', 'Gewijzigd'],
                  relate: ['Te relateren', 'Gerelateerd'],
                  delete: ['Te verwijderen', 'Verwijderd'],
                };

                return seamlessImmutable(mutations || []).map((mutation) => {
                  let state = mutation.state,
                    label = labels[mutation.type],
                    disabled = ctrl.disabled() || mutation.state !== 'pending';

                  if (state === 'executed') {
                    label = label[1];
                  } else if (state === 'pending') {
                    label = label[0];
                  } else if (state === 'failed') {
                    label = `Niet ${label[1].toLowerCase()}`;
                  }

                  return mutation.merge({
                    $id: shortid(),
                    stateLabel: label,
                    disabled,
                    classes: assign(
                      {
                        complete: mutation.complete,
                        disabled,
                        fulfilled: state !== 'pending',
                      },
                      keyBy(
                        'executed failed pending'.split(' '),
                        (key) => state === key
                      )
                    ),
                  });
                });
              }
            );

            objectTypeResource.reduce((requestOptions, data) => {
              return first(data);
            });

            ctrl.getMutations = mutationReducer.data;

            ctrl.isLoading = () => ctrl.mutationsLoading();

            ctrl.getObjectTypeResource = () => objectTypeResource;

            ctrl.handleCancel = () => {
              modal.close();
            };

            ctrl.handleClick = (type) => {
              openModal(type);
            };

            ctrl.handleEditClick = (mutation) => {
              openModal(mutation.type, mutation);
            };

            ctrl.handleRemoveClick = (mutation) => {
              zsConfirm(
                'Weet u zeker dat u deze mutatie wilt verwijderen?',
                'Verwijderen'
              ).then(() => {
                ctrl.onMutationRemove({ $mutation: mutation });
              });
            };

            scope.$on('$destroy', () => {
              if (modal) {
                modal.close();
              }
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
