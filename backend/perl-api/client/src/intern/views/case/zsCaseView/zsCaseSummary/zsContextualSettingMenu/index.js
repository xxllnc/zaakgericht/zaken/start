// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from '../../../../../../shared/ui/zsDropdownMenu/controller';
import template from '../../../../../../shared/ui/zsDropdownMenu/template.html';
import angularUiRouter from 'angular-ui-router';
import viewRegistrarModule from '../../../../../../shared/util/route/viewRegistrar';
import getActiveStates from '../../../../../../shared/util/route/getActiveStates';
import find from 'lodash/find';

module.exports = angular
  .module('zsContextualSettingMenu', [angularUiRouter, viewRegistrarModule])
  .directive('zsContextualSettingMenu', [
    '$rootScope',
    '$compile',
    '$document',
    '$animate',
    '$state',
    'viewRegistrar',
    ($rootScope, $compile, $document, $animate, $state, viewRegistrar) => {
      let wrapper = angular.element(`<div>${template}</div>`),
        containerTpl = wrapper.find('ul');

      containerTpl.removeAttr('ng-if');

      containerTpl = containerTpl[0].outerHTML;

      wrapper.find('ul').remove();
      wrapper.find('li').remove();

      return {
        template: wrapper[0].innerHTML,
        restrict: 'E',
        scope: {
          onClear: '&',
          onReset: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let body = angular.element(document.body);
            let ctrl = this,
              positionOptions = {
                attachment: 'top right',
                target: 'top right',
                reference: body.find('.case-body'),
                updateOn: 'default watch',
                customPositioning: true,
              },
              content,
              fullScreenDiv;

            ctrl.name = 'Zaakacties';

            let destroyContent = () => {
              if (content) {
                content.scope().$destroy();

                $animate.leave(content);

                content = null;
              }

              fullScreenDiv.remove();
            };

            let setContent = () => {
              let states = getActiveStates($state.$current),
                routeWithSettings,
                isoScope = scope.$new();

              if (content) {
                destroyContent();
              }

              content = angular.element(containerTpl);

              routeWithSettings = find(
                states.concat().reverse(),
                (state) => state.settings !== undefined
              );

              if (routeWithSettings) {
                let view = viewRegistrar.getView(routeWithSettings.name),
                  viewController = view.controller();

                isoScope.viewController = viewController;

                isoScope.close = ctrl.closeMenu;

                content.append(angular.element(routeWithSettings.settings));
              }

              $compile(content)(isoScope);

              $animate.enter(content, element, element.find('button'));

              // custom menu positioning

              const baseSpacing = 10;

              const button = element[0].querySelector('button');
              const buttonRect = button.getBoundingClientRect();
              const buttonTop = buttonRect.top;
              const buttonRight = buttonRect.right;

              const viewportHeight = window.innerHeight;
              const topBarHeight = 56;
              const topSpacing = topBarHeight + baseSpacing;
              const bottomSpacing = baseSpacing;
              const spaceDownward = viewportHeight - buttonTop - bottomSpacing;
              const spaceTotal = viewportHeight - topSpacing - bottomSpacing;

              const viewportWidth = window.innerWidth;
              const listWidth = 240;
              const list = element[0].querySelector('ul');

              list.style.left = `${buttonRight + baseSpacing}px`;

              setTimeout(() => {
                const listRect = list.getBoundingClientRect();
                const listHeight = listRect.height;
                let listTop;
                let listMaxHeight;

                if (spaceDownward > listHeight) {
                  listTop = buttonTop;
                } else if (spaceTotal > listHeight) {
                  listTop = viewportHeight - bottomSpacing - listHeight;
                } else {
                  listTop = topSpacing;
                  listMaxHeight = viewportHeight - topSpacing - bottomSpacing;
                }

                list.style.maxHeight = `${listMaxHeight}px`;
                list.style.top = `${listTop}px`;

                let listLeft;

                if (viewportWidth < buttonRight + baseSpacing + listWidth) {
                  listLeft = viewportWidth - listWidth;
                }

                list.style.left = `${listLeft}px`;
              }, 0);
            };

            ctrl.options = () => null;

            ctrl.buttonIcon = 'dots-vertical';

            ctrl.buttonLabel = 'Zaakacties';

            ctrl.onOpen = () => {
              setContent();

              fullScreenDiv = angular.element('<div class="full-screen-div"/>');
              fullScreenDiv.on('click', () => {
                fullScreenDiv.remove();
              });

              body.append(fullScreenDiv);
            };

            ctrl.onClose = () => {
              destroyContent();
            };

            ctrl.positionOptions = () => positionOptions;

            controller.call(ctrl, $document, scope, element, $animate);

            $rootScope.$on('$viewContentLoaded', () => {
              scope.$evalAsync(() => {
                if (ctrl.isMenuOpen()) {
                  setContent();
                }
              });
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
