// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import ContactInformationController from './controller';
import template from './template.html';
import './styles.scss';

const controller = ['$sce', ContactInformationController];

module.exports = angular
  .module('zsContactInformationView', [])
  .component('zsContactInformationView', {
    bindings: {
      subject: '&',
    },
    controller,
    template,
  }).name;
