// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from './controller';
import template from './template.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import './styles.scss';

controller.$inject = ['$scope', '$sce', 'composedReducer'];

module.exports = angular
  .module('zsContactSummary', [composedReducerModule])
  .component('zsContactSummary', {
    bindings: {
      subject: '&',
      isCollapsed: '&',
    },
    template,
    controller,
  }).name;
