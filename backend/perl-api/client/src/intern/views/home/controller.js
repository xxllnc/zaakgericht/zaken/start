// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-gridster/src/angular-gridster.js';
import find from 'lodash/find';
import assign from 'lodash/assign';
import pick from 'lodash/pick';
import map from 'lodash/map';
import invoke from 'lodash/invokeMap';
import get from 'lodash/get';
import once from 'lodash/once';
import shortid from 'shortid';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import zsDashboardWidgetModule from './zsDashboard/zsDashboardWidget';
import zsDashboardWidgetCreateModule from './zsDashboard/zsDashboardWidget/types/zsDashboardWidgetCreate';
import zsDashboardResetModule from './zsDashboard/zsDashboardWidget/types/zsDashboardReset';
import zsDashboardGridsterModule from './zsDashboard/zsDashboardGridster';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../shared/util/rwdService';
import actionsModule from './actions';
import { doWidgetsFit } from './doWidgetsFit';
import './dashboard.scss';

module.exports = angular
  .module('DashboardController', [
    snackbarServiceModule,
    actionsModule,
    zsDashboardWidgetModule,
    zsDashboardWidgetCreateModule,
    zsDashboardResetModule,
    zsDashboardGridsterModule,
    rwdServiceModule,
    composedReducerModule,
    'gridster',
  ])
  .controller('DashboardController', [
    '$timeout',
    '$element',
    '$scope',
    '$http',
    'widgetResource',
    'settingsResource',
    'snackbarService',
    'rwdService',
    'composedReducer',
    function (
      $timeout,
      $element,
      $scope,
      $http,
      widgetResource,
      settingsResource,
      snackbarService,
      rwdService,
      composedReducer
    ) {
      const mediaQueryNameSmall = 'small-and-down';
      const mediaQueryNameMedium = 'small-medium-and-down';
      const columns = 10;
      const { smallScreen, smallMediumScreen } = rwdService.getMagicNumbers();
      const ctrl = this;
      const defaultWidgetData = {
        size_x: 5,
        size_y: 10,
        row: null,
        column: null,
        widget: 'create',
        data: {
          json_data: 'valid',
        },
      };

      /**
       * ZS-FIXME: We (ab)use the `rwdService` to dynamically update the `mobileModeEnabled`
       * configuration option of `angular-gridster`. That's wasteful and *should* be unnecessary,
       * since `angular-gridster` does that itself.
       * As it turns out, `angular-gridster` is FUBAR and creates an infinite loop when the
       * breakpoint applies and a scrollbar becomes necessary.
       * Open issues from 2014, 2015 and 2016:
       * https://github.com/ManifestWebDesign/angular-gridster/issues/169
       * https://github.com/ManifestWebDesign/angular-gridster/issues/301
       * https://github.com/ManifestWebDesign/angular-gridster/issues/464
       *
       * Heads up: do *not* use a different rwdService breakpoint for compact mode
       */
      const isMobileModeEnabled = () =>
        rwdService.isActive(mediaQueryNameMedium);

      widgetResource.reduce((requestOptions, data) =>
        map(data || [], (widget, index) =>
          assign(
            {
              id: widget.reference || index,
            },
            widget.instance
          )
        )
      );

      const widgetReducer = composedReducer(
        {
          scope: $scope,
        },
        widgetResource,
        () => rwdService.getActiveViews()
      ).reduce((widgets) => {
        const compactTreshold = rwdService.isActive(mediaQueryNameSmall)
          ? columns
          : Math.ceil((smallScreen / smallMediumScreen) * columns);

        return widgets.map((widget) => {
          const compact = widget.size_x <= compactTreshold;

          // don't create new object or the widget library will
          // think it's a new one and animate it
          return assign(widget, {
            compact,
          });
        });
      });

      ctrl.isDisabled = () => {
        const settings = settingsResource.data();
        const reference = 'disable_dashboard_customization';
        const setting = find(settings, { reference });
        const disabled = get(setting, 'instance.value') === '1';

        return disabled;
      };

      ctrl.onItemAdd = [];

      ctrl.isEmpty = () =>
        widgetResource.state() === 'resolved' &&
        widgetResource.data().length === 0;

      /**
       * New widgets are placed at a free location on the dashboard (left to right, top to bottom).
       * A widget is placed at { 0, 0 } if that is a free location or when the dashboard is full
       * so no widget should be placed if it's at { 0, 0 } while that location is full.
       *
       * @param {Object} data
       */
      ctrl.createWidget = (data) => {
        const newWidgetAtZeroZero = data.row === 0 && data.column === 0;
        const freeWidgetSpace = widgetResource
          .data()
          .every(
            (widget) =>
              widget.column >= data.size_x || widget.row >= data.size_y
          );

        if (newWidgetAtZeroZero && !freeWidgetSpace) {
          snackbarService.error(
            'Het dashboard is vol. Verwijder en/of verklein een widget en probeer het dan opnieuw.'
          );

          return;
        }

        widgetResource.mutate('create_widget', data);
        $timeout(
          () => {
            $element[0].scrollIntoView(false);
          },
          0,
          false
        );
      };

      ctrl.deleteWidget = (uuid) => {
        widgetResource.mutate('delete_widget', {
          uuid,
        });
      };

      ctrl.updateWidgets = (event) => {
        // every click results in a call to update the widgets,
        // which is not great, but also not terrible,
        // unless this click is done on a widget with an iframe,
        // which then reinitialises itself or something
        // which results in the call to delete being cancelled
        const isClickOnCloseButton = event.target.classList.contains(
          'mdi-close'
        );

        if (isClickOnCloseButton) {
          return;
        }

        const updates = widgetResource.data().map((item) =>
          assign(
            {
              uuid: item.id,
            },
            pick(item, 'row', 'column', 'size_x', 'size_y', 'data')
          )
        );

        if (!doWidgetsFit(updates)) {
          snackbarService.error(
            'Het dashboard is vol. Widgets vallen over elkaar heen en kunnen niet worden opgeslagen. Verwijder en/of verklein een widget.'
          );

          return;
        }

        widgetResource.mutate('bulk_update', {
          updates,
        });
      };

      ctrl.getWidgets = widgetReducer.data;

      ctrl.getSettings = settingsResource.data;

      ctrl.handleWidgetDataChange = (id, data) => {
        const item = find(widgetResource.data(), { id });

        widgetResource.mutate(
          'update_widget',
          assign(
            {
              uuid: item.id,
            },
            pick(item, 'row', 'column', 'size_x', 'size_y', 'data'),
            {
              data,
            }
          )
        );
      };

      ctrl.handleWidgetCreate = (widgetType) => {
        const limit = widgetResource.limit() || 10;

        if (widgetResource.data().length >= limit) {
          snackbarService.error(
            `U heeft al het maximum van ${widgetResource.limit()} bereikt. Verwijder een widget en probeer het dan opnieuw.`
          );

          return;
        }

        const data = assign({
          size_x: defaultWidgetData.size_x,
          size_y: defaultWidgetData.size_y,
          data: {},
          widget: widgetType,
        });

        invoke(ctrl.onItemAdd, 'call', null, data);
        ctrl.createWidget(data);
      };

      ctrl.remove = (widget) => {
        ctrl.deleteWidget(widget.id);
      };

      ctrl.resetWidgets = () =>
        snackbarService.wait(
          'Het dashboard wordt terug gezet naar de standaardinstelling',
          {
            promise: $http({
              url: '/api/v1/dashboard/widget/set_default',
              method: 'POST',
            }).then(() => widgetResource.reload()),
            then: () =>
              'Het dashboard is terug gezet naar de standaardinstelling',
            catch: () =>
              'Het is niet gelukt om het dashboard terug te zetten naar de standaardinstelling',
          }
        );

      ctrl.customItemMap = {
        sizeX: 'widget.size_x',
        sizeY: 'widget.size_y',
        row: 'widget.row',
        col: 'widget.column',
        minSizeX: 'widget.minSizeX',
        maxSizeX: 'widget.maxSizeX',
        minSizeY: 'widget.minSizeY',
        maxSizeY: 'widget.maxSizeY',
      };

      ctrl.gridsterOpts = {
        columns,
        margins: [20, 20],
        swapping: false,
        rowHeight: 60,
        maxRows: 50,

        // ZS-INFO: mobileBreakPoint is not actually used
        mobileBreakPoint: smallMediumScreen,
        // ZS-INFO: mobileModeEnabled is dynamically monkey-patched in a watcher
        mobileModeEnabled: false,

        resizable: {
          enabled: !ctrl.isDisabled(),
          handles: ['se', 'sw', 'ne', 'nw'],
          stop: ctrl.updateWidgets,
        },
        draggable: {
          handle: ['.widget-header', '.widget-drag-handle'],
          enabled: !ctrl.isDisabled(),
          stop: ctrl.updateWidgets,
        },
      };

      $scope.$on('$destroy', () => {
        widgetResource.destroy();
        settingsResource.destroy();
      });

      $scope.$watch(isMobileModeEnabled, (isMobile) => {
        ctrl.gridsterOpts = assign({}, ctrl.gridsterOpts, {
          mobileModeEnabled: isMobile,
        });
      });

      $scope.$on(
        'gridster-resized',
        once(() => {
          // because angular-gridster uses a timeout, sometimes
          // the colWidth is calculated before the scrollbars appear
          // so we have to trigger an update by invalidating the options
          // object
          assign(ctrl.gridsterOpts, {
            zsUpdate: shortid(),
          });
        })
      );
    },
  ]).name;
