// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { doWidgetsFit } from './doWidgetsFit';

const widgets = [
  { column: 0, row: 0, size_x: 10, size_y: 10 },
  { column: 20, row: 0, size_x: 10, size_y: 10 },
  { column: 0, row: 20, size_x: 10, size_y: 10 },
  { column: 20, row: 20, size_x: 10, size_y: 10 },
];

const scenarios = [
  {
    scenario: 'widget that touches left-side',
    expected: true,
    widget: { column: 10, row: 0, size_x: 5, size_y: 10 },
  },
  {
    scenario: 'widget that touches right-side',
    expected: true,
    widget: { column: 15, row: 0, size_x: 5, size_y: 10 },
  },
  {
    scenario: 'widget that touches top-side',
    expected: true,
    widget: { column: 0, row: 10, size_x: 10, size_y: 5 },
  },
  {
    scenario: 'widget that touches bottom-side',
    expected: true,
    widget: { column: 0, row: 15, size_x: 10, size_y: 5 },
  },
  {
    scenario: 'widget that touches all-corners',
    expected: true,
    widget: { column: 10, row: 10, size_x: 10, size_y: 10 },
  },
  {
    scenario: 'widget that overlaps top-left corner',
    expected: false,
    widget: { column: 5, row: 5, size_x: 10, size_y: 10 },
  },
  {
    scenario: 'widget that overlaps top-right corner',
    expected: false,
    widget: { column: 15, row: 5, size_x: 10, size_y: 10 },
  },
  {
    scenario: 'widget that overlaps bottom-left corner',
    expected: false,
    widget: { column: 5, row: 15, size_x: 10, size_y: 10 },
  },
  {
    scenario: 'widget that overlaps bottom-right corner',
    expected: false,
    widget: { column: 15, row: 15, size_x: 10, size_y: 10 },
  },
  {
    scenario: 'widget that overlaps exactly',
    expected: false,
    widget: { column: 0, row: 0, size_x: 10, size_y: 10 },
  },
  {
    scenario: 'widget that overlaps completely',
    expected: false,
    widget: { column: 2, row: 2, size_x: 6, size_y: 6 },
  },
  {
    scenario: 'widget that overlaps multiple widgets',
    expected: false,
    widget: { column: 5, row: 5, size_x: 20, size_y: 20 },
  },
];

describe('The `doWidgetsFit` function', () => {
  scenarios.forEach(({ scenario, expected, widget }) => {
    test(scenario, () => {
      expect(doWidgetsFit(widgets.concat(widget))).toBe(expected);
    });
  });
});
