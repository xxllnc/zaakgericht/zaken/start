// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import resourceModule from './../../../shared/api/resource';
import template from './index.html';
import sessionServiceModule from '../../../shared/user/sessionService';
import isArray from 'lodash/isArray';

module.exports = angular
  .module('Zaaksysteem.intern.home.route', [
    angularUiRouterModule,
    ocLazyLoadModule,
    resourceModule,
    snackbarServiceModule,
    sessionServiceModule,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      $stateProvider.state('home', {
        url: '',
        template,
        controller: 'DashboardController',
        controllerAs: 'vm',
        resolve: {
          // add a timeout to make sure the view switch animation is visible
          // even if all dependencies are available from cache
          timeout: [
            '$timeout',
            ($timeout) => {
              return $timeout(angular.noop, 50);
            },
          ],
          widgetResource: [
            '$rootScope',
            '$q',
            'resource',
            'snackbarService',
            ($rootScope, $q, resource, snackbarService) => {
              let widgetResource = resource('/api/v1/dashboard/widget', {
                scope: $rootScope,
              }).reduce((requestOptions, data) => {
                // prevent dashboard from crashing if result is string
                return isArray(data) ? data : [];
              });

              return widgetResource
                .asPromise()
                .then(() => widgetResource)
                .catch((error) => {
                  snackbarService.error(
                    'De configuratie van het dashboard kon niet geladen worden. Neem contact op met uw beheerder voor meer informatie.'
                  );

                  return $q.reject(error);
                });
            },
          ],
          module: [
            '$rootScope',
            '$ocLazyLoad',
            '$q',
            ($rootScope, $ocLazyLoad, $q) => {
              return $q((resolve, reject) => {
                require(['./controller'], () => {
                  let load = () => {
                    $ocLazyLoad
                      .load({
                        name: 'DashboardController',
                      })
                      .then(resolve)
                      .catch(reject);
                  };

                  $rootScope.$evalAsync(load);
                });
              });
            },
          ],
        },
        title: [
          'user',
          (userResource) => {
            return userResource.data().instance.account.instance.company;
          },
        ],
        actions: [
          {
            name: 'create_widget',
            label: 'Widget aanmaken',
            iconClass: 'plus-box',
            template:
              '<zs-dashboard-widget-create on-widget-create="viewController.handleWidgetCreate($widgetType);close()"></zs-dashboard-widget-create>',
            when: [
              'viewController',
              (viewController) => !viewController.isDisabled(),
            ],
          },
          {
            name: 'reset_dashboard',
            label: 'Dashboard herstellen',
            iconClass: 'autorenew',
            template:
              '<zs-dashboard-reset reset-widgets="viewController.resetWidgets();close()"></zs-dashboard-reset>',
            when: [
              'viewController',
              (viewController) => !viewController.isDisabled(),
            ],
          },
        ],
      });
    },
  ]).name;
