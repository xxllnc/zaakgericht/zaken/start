// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';

module.exports = angular
  .module('zsDashboardReset', [])
  .directive('zsDashboardReset', [
    () => {
      return {
        restrict: 'E',
        scope: {
          resetWidgets: '&',
        },
        template,
        bindToController: true,
        controller: [function () {}],
        controllerAs: 'zsDashboardReset',
      };
    },
  ]).name;
