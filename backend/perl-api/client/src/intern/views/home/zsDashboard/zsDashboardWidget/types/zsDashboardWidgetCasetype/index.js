// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import angularDragulaModule from 'angular-dragula';
import template from './../zsDashboardWidgetFavorite/template.html';
import controller from './../zsDashboardWidgetFavorite/controller';
import zsObjectSuggestModule from './../../../../../../../shared/object/zsObjectSuggest';
import snackbarServiceModule from './../../../../../../../shared/ui/zsSnackbar/snackbarService';
import actionsModule from './../zsDashboardWidgetFavorite/actions';
import contextualActionServiceModule from './../../../../../../../shared/ui/zsContextualActionMenu/contextualActionService';
import find from 'lodash/find';
import './../zsDashboardWidgetFavorite/styles.scss';

module.exports = angular
  .module(
    'Zaaksysteem.intern.home.zsDashboard.zsDashboardWidget.zsDashboardWidgetCasetype',
    [
      angularDragulaModule(angular),
      angularUiRouter,
      zsObjectSuggestModule,
      snackbarServiceModule,
      actionsModule,
      contextualActionServiceModule,
    ]
  )
  .directive('zsDashboardWidgetCasetype', [
    'contextualActionService',
    (contextualActionService) => {
      let tpl = template.replace(
        /zsDashboardWidgetFavorite/g,
        'zsDashboardWidgetCasetype'
      );

      return {
        restrict: 'E',
        scope: {
          id: '&',
          widgetTitle: '&',
          placeholder: '&',
        },
        template: tpl,
        bindToController: true,
        controller: [
          'resource',
          'dragulaService',
          'snackbarService',
          '$scope',
          function (resource, dragulaService, snackbarService, $scope) {
            let ctrl = this;

            ctrl.type = () => 'casetype';

            ctrl.widgetTitle({
              $getter: () => 'Favoriete zaaktypen',
            });

            ctrl.placeholder = 'Zoek een zaaktype';

            controller.call(
              ctrl,
              resource,
              dragulaService,
              snackbarService,
              $scope
            );

            ctrl.favorites.reduce((requestOptions, data) => {
              return data.map((favorite) => {
                return favorite.merge({
                  click: () => {
                    contextualActionService.openAction(
                      find(contextualActionService.getAvailableActions(), {
                        name: 'zaak',
                      }),
                      {
                        casetypeId: favorite.reference_id,
                      }
                    );
                  },
                });
              });
            });
          },
        ],
        controllerAs: 'zsDashboardWidgetCasetype',
      };
    },
  ]).name;
