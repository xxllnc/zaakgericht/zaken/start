// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import template from './template.html';

module.exports = angular
  .module('zsDashboardWidgetExternalUrlBlocked', [])
  .directive('zsDashboardWidgetExternalUrlBlocked', [
    () => {
      return {
        restrict: 'E',
        scope: {
          widgetData: '&',
          widgetTitle: '&',
          widgetLoading: '&',
          onDataChange: '&',
          whitelist: '&',
        },
        template,
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.widgetTitle({
              $getter: () => get(ctrl.widgetData(), 'title') || '',
            });

            return ctrl;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
