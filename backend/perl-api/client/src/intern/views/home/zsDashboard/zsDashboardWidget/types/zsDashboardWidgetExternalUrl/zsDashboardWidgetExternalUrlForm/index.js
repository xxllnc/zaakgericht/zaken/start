// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import seamlessImmutable from 'seamless-immutable';
import get from 'lodash/get';
import vormFieldsetModule from './../../../../../../../../shared/vorm/vormFieldset';
import externalUrlForm from './form/externalUrlForm';
import template from './template.html';

module.exports = angular
  .module('zsDashboardWidgetExternalUrlForm', [vormFieldsetModule])
  .directive('zsDashboardWidgetExternalUrlForm', [
    '$timeout',
    'vormValidator',
    'composedReducer',
    ($timeout, vormValidator, composedReducer) => {
      return {
        restrict: 'E',
        scope: {
          onUrlEnter: '&',
          widgetTitle: '&',
        },
        template,
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope) {
            let ctrl = this;
            let values = seamlessImmutable({});
            let form = externalUrlForm();
            let locals = seamlessImmutable({
              $values: values,
            });
            let fields = form.fields();

            const validityReducer = composedReducer(
              { scope },
              () => values,
              () => locals
            ).reduce((vals, loc) => {
              return vormValidator(fields, vals, null, loc);
            });

            ctrl.getFields = () => fields;

            ctrl.getValues = () => values;

            ctrl.handleChange = (name, value) => {
              values = form.processChange(name, value, values);
              locals = locals.merge(
                {
                  $values: values,
                },
                { deep: true }
              );
            };

            ctrl.getValidity = () => get(validityReducer.data(), 'validations');

            ctrl.isSaveable = () => get(validityReducer.data(), 'valid');

            ctrl.handleFormSubmit = () => {
              ctrl.onUrlEnter({
                $data: values,
              });
            };

            ctrl.widgetTitle({
              $getter: () => 'Configureer de widget',
            });

            return ctrl;
          },
        ],
        controllerAs: 'zsDashboardWidgetExternalUrlForm',
      };
    },
  ]).name;
