// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import pick from 'lodash/pick';
import propCheck from './../../../../../../../shared/util/propCheck';
import { mockObject } from './../../../../../../../shared/object/mock';

const BASE = '/api/v1/dashboard/favourite';

module.exports = angular
  .module(
    'intern.home.dashboard.zsDashboard.zsDashboardWidget.zsDashboardWidgetFavorite.actions',
    []
  )
  .constant('zsDashboardWidgetFavoriteActions', [
    {
      type: 'add_favorite',
      request: (mutationData) => {
        propCheck.throw(
          propCheck.shape({
            reference_type: propCheck.string,
            reference_id: propCheck.oneOfType([
              propCheck.string,
              propCheck.number,
            ]),
            label: propCheck.string,
          }),
          mutationData
        );
        return {
          url: `${BASE}/${mutationData.reference_type}/create`,
          data: pick(mutationData, 'reference_id'),
        };
      },
      reduce: (data, mutationData) => {
        return data.concat(
          mockObject({
            type: 'favourite',
            values: mutationData,
          })
        );
      },
    },
    {
      type: 'remove_favorite',
      request: (mutationData) => {
        propCheck.throw(
          propCheck.shape({
            uuid: propCheck.string,
            reference_type: propCheck.string,
          }),
          mutationData
        );

        return {
          url: `${BASE}/${mutationData.reference_type}/${mutationData.uuid}/delete`,
        };
      },
      reduce: (data, mutationData) => {
        return data.filter((item) => item.reference !== mutationData.uuid);
      },
    },
    {
      type: 'update_favorite',
      request: (mutationData) => {
        propCheck.throw(
          propCheck.shape({
            uuid: propCheck.string,
            reference_type: propCheck.string,
            order: propCheck.number.optional,
          }),
          mutationData
        );

        return {
          url: `${BASE}/${mutationData.reference_type}/${mutationData.uuid}/update`,
          data: pick(mutationData, 'order', 'reference_id', 'reference_type'),
        };
      },
      reduce: (data, mutationData) => {
        return data.map((item) => {
          let favorite = item;

          if (favorite.reference === mutationData.uuid) {
            favorite = favorite.merge(
              { instance: { order: mutationData.order } },
              { deep: true }
            );
          }

          return favorite;
        });
      },
    },
  ])
  .run([
    'zsDashboardWidgetFavoriteActions',
    'mutationService',
    (zsDashboardWidgetFavoriteActions, mutationService) => {
      zsDashboardWidgetFavoriteActions.forEach((action) => {
        mutationService.register(action);
      });
    },
  ]).name;
