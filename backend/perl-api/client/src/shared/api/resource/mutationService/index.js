// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import immutable from 'seamless-immutable';
import propCheck from './../../../util/propCheck';
import cacherModule from './../../cacher';
import apiModule from './../../';
import cachedCollectionModule from './../../../util/cachedCollection';
import appUnloadModule from './../../../util/appUnload';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import pull from 'lodash/pull';
import filter from 'lodash/filter';
import difference from 'lodash/difference';
import map from 'lodash/map';
import reject from 'lodash/reject';
import identity from 'lodash/identity';
import assign from 'lodash/assign';
import find from 'lodash/find';
import partition from 'lodash/partition';
import invoke from 'lodash/invokeMap';
import get from 'lodash/get';
import includes from 'lodash/includes';
import without from 'lodash/without';
import isEqual from 'lodash/isEqual';
import shortid from 'shortid';

let requestCheck = propCheck.shape({
  url: propCheck.string,
  params: propCheck.object.optional,
});

module.exports = angular
  .module('shared.api.mutation', [
    apiModule,
    cacherModule,
    cachedCollectionModule,
    appUnloadModule,
    snackbarServiceModule,
  ])
  .factory('mutationService', [
    '$window',
    '$timeout',
    '$q',
    '$interval',
    'api',
    'apiCacher',
    'cachedCollection',
    'appUnload',
    'zsStorage',
    'snackbarService',
    (
      $window,
      $timeout,
      $q,
      $interval,
      api,
      apiCacher,
      cachedCollection,
      appUnload,
      zsStorage,
      snackbarService
    ) => {
      const REQUEST_MESSAGE_TYPE = 'ownedMutationsRequest',
        RESPONSE_MESSAGE_TYPE = 'ownedMutationsResponse';

      let mutationService = {},
        mutations = cachedCollection.array('resourceMutations'),
        ownedMutationIds = [],
        waiting = [],
        actions = {},
        runningRequests = {},
        removeListeners = [],
        addListeners = [],
        messageListeners = [],
        promises = [],
        syncPromise;

      let syncMutations = (ids) => {
        let val = mutations.value(),
          chain,
          mutationsToSync = ids
            .map((id) => {
              return find(val, { id });
            })
            .filter(identity)
            .filter((mutation) => {
              return (
                !runningRequests[mutation.id] && !includes(waiting, mutation.id)
              );
            });

        chain = syncPromise = mutationsToSync.reduce((prev, mutation) => {
          let action = actions[mutation.type],
            mutationPromise;

          waiting = waiting.concat(mutation.id);

          mutationPromise = prev
            .catch(() => {})
            .then(() => {
              // TODO: what happens when an action is no longer available for a certain type?

              let promise,
                { success = identity, error } = action,
                opts,
                defaultErrMsg =
                  'Wijziging kon niet worden opgeslagen. Neem contact op met uw beheerder voor meer informatie.';

              waiting = without(waiting, mutation.id);

              if (!error && typeof action.wait !== 'function') {
                error = (err) => {
                  snackbarService.error(defaultErrMsg);
                  return $q.reject(err);
                };
              }

              let removeMutation = (isError = false, args) => {
                let promisesToResolve;

                delete runningRequests[mutation.id];
                mutations.replace(
                  reject(mutations.value(), { id: mutation.id })
                );

                [promisesToResolve, promises] = partition(promises, {
                  id: mutation.id,
                });

                promisesToResolve.forEach((p) => {
                  if (isError) {
                    p.deferred.reject(args);
                  } else {
                    p.deferred.resolve(args);
                  }
                });

                return isError ? $q.reject(args) : $q.resolve(args);
              };

              try {
                opts = assign(
                  { method: 'POST' },
                  mutation.request,
                  action.request(mutation.data)
                );

                promise = api(opts);

                runningRequests[mutation.id] = promise;
              } catch (err) {
                promise = $q.reject(err);
              }

              promise = promise
                .then((response) => {
                  return $q
                    .when(
                      get(action, 'options.reloadOnComplete')
                        ? api(mutation.request)
                        : response
                    )
                    .then((resp) => {
                      // store data from final response,
                      // resolve with original response

                      apiCacher.store(mutation.request, resp.data);

                      removeMutation(false, response);
                      return $q.when(response);
                    });
                })
                .catch((err) => {
                  return removeMutation(true, err);
                })
                .then(success)
                .catch(error)
                .catch((err) => {
                  console.error(err);
                  return $q.reject(err);
                });

              return promise;
            });

          if (typeof action.wait === 'function') {
            mutationPromise = action.wait(mutation.data, mutationPromise);
          }

          return mutationPromise;
        }, syncPromise || $q.when());

        return chain;
      };

      let handleMutationsAdd = (ids) => {
        invoke(addListeners, 'call', null, ids);
      };

      let handleMutationsRemove = (ids) => {
        ids.forEach((id) => {
          if (runningRequests[id]) {
            runningRequests[id].abortRequestAndIgnore();
            delete runningRequests[id];
          }

          pull(ownedMutationIds, id);
        });

        invoke(removeListeners, 'call', null, ids);
      };

      let handleMutationUpdate = (prev, current) => {
        let prevIds = map(prev, 'id'),
          currentIds = map(current, 'id'),
          removed = difference(currentIds, prevIds),
          added = difference(prevIds, currentIds);

        if (removed.length) {
          handleMutationsRemove(removed);
        }

        if (added.length) {
          handleMutationsAdd(added);
        }
      };

      let onMessage = (event) => {
        invoke(messageListeners, 'call', null, event);
      };

      let handleMessage = (event) => {
        if (event.origin === $window.location.origin) {
          let message = event.message;

          if (message && message.type === REQUEST_MESSAGE_TYPE) {
            event.source.postMessage(
              {
                type: RESPONSE_MESSAGE_TYPE,
                data: ownedMutationIds,
              },
              $window.location.origin
            );
          }
        }
      };

      mutationService.add = (values) => {
        let mutation,
          asPromise = () => {
            let deferred = $q.defer();

            promises.push({ id: mutation.id, deferred });

            return deferred.promise;
          };

        propCheck.throw(
          propCheck.shape({
            request: requestCheck,
            data: propCheck.object.optional,
            type: propCheck.string,
          }),
          values
        );

        if (!actions[values.type]) {
          throw new Error(
            `Cannot add mutation: no handler was found for ${values.type}`
          );
        }

        mutation = immutable(
          assign({ id: shortid(), created: Date.now(), asPromise }, values)
        );

        ownedMutationIds.push(mutation.id);

        mutations.replace(mutations.value().concat(mutation));

        syncMutations([mutation.id]);

        return mutation;
      };

      mutationService.remove = (id) => {
        propCheck.throw(propCheck.string, id);

        mutations.replace(reject(mutations.value(), { id }));
      };

      mutationService.filter = (requestOptions) => {
        propCheck.throw(requestCheck, requestOptions);

        return filter(mutations.value(), (mutation) => {
          return (
            mutation.request.url === requestOptions.url &&
            isEqual(mutation.request.params, requestOptions.params)
          );
        });
      };

      mutationService.exec = (requestOptions, data) => {
        propCheck.throw(
          propCheck.shape({
            requestOptions: requestCheck,
            data: propCheck.any.optional,
          }),
          { requestOptions, data }
        );

        return mutationService
          .filter(requestOptions)
          .reduce(
            (value, mutation) =>
              actions[mutation.type].reduce(value, mutation.data),
            data
          );
      };

      mutationService.register = (action) => {
        propCheck.throw(
          propCheck.shape({
            type: propCheck.string,
            reduce: propCheck.func,
            request: propCheck.func,
            success: propCheck.func.optional,
            error: propCheck.func.optional,
            options: propCheck.object.optional,
          }),
          action
        );

        if (actions[action.type]) {
          throw new Error(
            `Cannot register action for ${action.type}. Action already exists`
          );
        }

        actions[action.type] = action;

        return action;
      };

      mutationService.getAction = (type) => actions[type];

      mutationService.list = () => mutations.value();

      mutationService.clear = () => {
        mutations.replace([]);
      };

      mutationService.onRemove = removeListeners;
      mutationService.onAdd = addListeners;

      // TODO: not working as of yet,
      // fix it w/ better cross-tab communication
      // $interval(flush, FLUSH_INTERVAL);

      mutations.onUpdate.push(handleMutationUpdate);

      $window.addEventListener('message', onMessage);

      messageListeners.push(handleMessage);

      appUnload.onUnload(() => {
        messageListeners.length = 0;
        $window.removeEventListener('message', onMessage);
      });

      $window.onunload = () => {
        // make sure all owned mutations are cleared on unload

        mutations.replace(
          reject(mutations.value, (m) => {
            return includes(ownedMutationIds, m.id);
          })
        );

        zsStorage.flush();
      };

      return mutationService;
    },
  ]).name;
