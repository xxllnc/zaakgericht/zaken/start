// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import assign from 'lodash/assign';
import get from 'lodash/get';
import find from 'lodash/find';
import last from 'lodash/last';
import merge from 'lodash/merge';
import seamlessImmutable from 'seamless-immutable';
import attrToVorm from './../../zs/vorm/attrToVorm';

export default (fields, requestor, ctrl) => {
  const idToNameMapping = mapValues(keyBy(fields, 'id'), 'magic_string');

  return keyBy(
    seamlessImmutable(fields)
      .asMutable({ deep: true })
      .filter((attr) => !attr.is_group)
      .map((attr) => {
        let field = attrToVorm(attr, {
          include: {
            dateRange: idToNameMapping,
          },
        });

        if (field.template === 'calendar') {
          field = merge(field, {
            data: {
              provider: {
                requestor: requestor.instance.old_subject_identifier,
              },
            },
          });
        } else if (field.template === 'text_block') {
          field = merge(field, {
            name: `text_block_${field.id}`,
            hideLabel: true,
          });
        } else if (field.type === 'geolatlon') {
          field = merge(field, {
            data: {
              onFeaturesSelect: (features) => {
                let attributeId = get(
                  attr,
                  'properties.map_wms_feature_attribute_id'
                );
                let attribute = find(fields, {
                  catalogue_id: attributeId,
                });
                let val;

                if (attribute) {
                  if (features.length) {
                    val = last(features);
                  } else {
                    val = null;
                  }

                  ctrl.handleChange(attribute.magic_string, val);
                }
              },
            },
          });
        } else if (field.type === 'appointment') {
          field = merge(field, {
            data: {
              provider: {
                requestor: requestor.reference,
                appointmentInterfaceUuid:
                  attr.properties.appointment_interface_uuid,
                locationId: attr.properties.location_id,
                productId: attr.properties.product_id,
              },
            },
          });
        } else if (field.type === 'geojson') {
          field = merge(field, {
            data: {
              mapSettings: ctrl.mapSettings,
              context: {
                type: 'CaseRegistrationForm',
                data: {
                  magic_string: attr.magic_string,
                  caseType: ctrl.casetype.instance,
                },
              },
            },
          });
        } else if (field.type === 'address_v2') {
          field = merge(field, {
            data: {
              mapSettings: ctrl.mapSettings,
              context: {
                type: 'CaseRegistrationForm',
                data: {
                  magic_string: attr.magic_string,
                  caseType: ctrl.casetype.instance,
                },
              },
              onFeaturesSelect: (features) => {
                let attributeId = get(
                  attr,
                  'properties.map_wms_feature_attribute_id'
                );
                let attribute = find(fields, {
                  catalogue_id: attributeId,
                });
                let val;

                if (attribute) {
                  if (features) {
                    val = features;
                  } else {
                    val = null;
                  }

                  ctrl.handleChange(attribute.magic_string, val);
                }
              },
            },
          });
        } else if (field.data && field.data.options) {
          field = assign(field, {
            data: assign(field.data, {
              options: field.data.options.filter((opt) => opt.active),
            }),
          });
        }

        return field;
      })
      .map((field) => {
        let mapped = field;

        switch (field.$attribute.type) {
          case 'file':
            mapped = merge(field, {
              data: {
                target: '/api/v2/file/create_file',
                transform: [
                  '$file',
                  '$data',
                  ($file, $data) => ({
                    filename: $file.name,
                    reference: $data.data.id,
                  }),
                ],
              },
            });
            break;
        }

        return mapped;
      }),
    'name'
  );
};
