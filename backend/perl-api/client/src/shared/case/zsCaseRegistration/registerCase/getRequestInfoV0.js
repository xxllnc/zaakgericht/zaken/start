// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import identity from 'lodash/identity';
import formatAsApiSubject from './helpersV0/formatAsApiSubject';
import getAllocationRoute from './helpersV0/getAllocationRoute';
import getAssignee from './helpersV0/getAssignee';
import getRelations from './helpersV0/getRelations';
import getFiles from './helpersV0/getFiles';
import getRelatedSubjects from './helpersV0/getRelatedSubjects';
import getContactDetails from './helpersV0/getContactDetails';

const getRequestInfoV0 = (
  ctrl,
  vals,
  intakeShowContactInfo,
  apiValues,
  shouldRedirectToCase,
  parentCaseId,
  relationType,
  subjectsFromParent
) => {
  const {
    casetype,
    channelOfContact,
    documents,
    recipient,
    requestor,
    customObject,
    user,
  } = ctrl;
  const { $allocation, $confidentiality, $files, $related_subjects } = vals;

  const assignee = getAssignee($allocation, user);
  const relatedSubjects = getRelatedSubjects(
    $related_subjects,
    subjectsFromParent
  );
  const subjects = relatedSubjects.concat(assignee).filter(identity);

  let data = {
    casetype_id: casetype.reference,
    confidentiality: $confidentiality,
    contact_details: getContactDetails(vals, intakeShowContactInfo),
    open: get($allocation, 'type') === 'me',
    requestor: formatAsApiSubject(requestor),
    route: getAllocationRoute($allocation, user),
    files: getFiles($files, documents),
    recipient: formatAsApiSubject(recipient),
    source: channelOfContact,
    subjects,
    values: apiValues,
    relations: getRelations(parentCaseId, relationType),
    custom_objects: customObject ? [customObject.id] : undefined,
  };

  // when you register a case and do not assign it to yourself, you will be redirected to the dashboard
  // registering a case via v0 is slow, so we use the delayed registration and redirect right away
  const url = shouldRedirectToCase
    ? '/api/v0/case/create'
    : '/api/v1/case/create_delayed';

  return {
    url,
    method: 'POST',
    data,
  };
};

export default getRequestInfoV0;
