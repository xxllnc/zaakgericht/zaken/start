// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import caseAttrTemplateCompilerModule from './../../caseAttrTemplateCompiler';
import vormRelatedSubjectModule from './vormRelatedSubject';
import vormAllocationPickerModule from './../../../zs/vorm/vormAllocationPicker';

module.exports = angular
  .module('registrationFormCompiler', [
    caseAttrTemplateCompilerModule,
    vormRelatedSubjectModule,
    vormAllocationPickerModule,
  ])
  .factory('registrationFormCompiler', [
    'caseAttrTemplateCompiler',
    'vormRelatedSubject',
    (caseAttrTemplateCompiler, vormRelatedSubject) => {
      let compiler = caseAttrTemplateCompiler.clone();

      compiler.registerType('related-subject', vormRelatedSubject);

      return compiler;
    },
  ]).name;
