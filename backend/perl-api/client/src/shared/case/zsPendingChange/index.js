// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import zsConfirmModule from './../../ui/zsConfirm';
import './styles.scss';

module.exports = angular
  .module('zsPendingChange', [zsConfirmModule])
  .directive('zsPendingChange', [
    'zsConfirm',
    'dateFilter',
    (zsConfirm, dateFilter) => {
      return {
        restrict: 'E',
        template,
        scope: {
          change: '&',
          disabled: '&',
          onAccept: '&',
          onReject: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.getRequestorName = () => ctrl.change().created_by_name;

            ctrl.getValue = () => {
              const change = ctrl.change();
              const { type, value } = change;

              if (type === 'date') {
                let date;

                if (
                  typeof value === 'string' &&
                  value.length >= 8 &&
                  value.length <= 10
                ) {
                  // external change suggestion (1-1-2001 / 31-12-2001)
                  const [day, month, year] = value.split('-');

                  date = new Date(year, month - 1, day);
                } else {
                  // internal change suggestion
                  date = new Date(value);
                }

                return dateFilter(date, 'dd-MM-yyyy');
              } else {
                return value;
              }
            };

            ctrl.getReason = () => ctrl.change().reason;

            ctrl.accept = () => {
              ctrl.onAccept({ $change: ctrl.change() });
            };

            ctrl.reject = () => {
              zsConfirm(
                'Weet u zeker dat u deze wijzigingsaanvraag wilt afwijzen? Let op, als behandelaar bent u verantwoordelijk om dit te melden aan de aanvrager.',
                'Afwijzen'
              ).then(() => {
                ctrl.onReject({ $change: ctrl.change() });
              });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
