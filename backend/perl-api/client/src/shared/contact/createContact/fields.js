// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';

export default (input) => {
  let values = get(input, 'vals'),
    countries = get(input, 'countries'),
    customerCountryCode = get(input, 'customerCountryCode'),
    entities = get(input, 'entities');

  return {
    fields: [
      {
        name: 'betrokkene_type',
        label: 'Type betrokkene',
        template: 'radio',
        data: {
          options: [
            {
              value: 'natuurlijk_persoon',
              label: 'Persoon',
            },
            {
              value: 'bedrijf',
              label: 'Organisatie',
            },
          ],
        },
        required: true,
      },
    ],
    citFields: [
      {
        name: 'np-burgerservicenummer',
        label: 'BSN',
        template: 'text',
        required: false,
        data: {},
      },
      {
        name: 'np-persoonsnummer',
        label: 'ID Nummer',
        template: 'text',
        required: false,
        data: {},
        when: [
          '$values',
          (vals) =>
            customerCountryCode === '5107' && vals['np-landcode'] === '5107',
        ],
      },
      {
        name: 'np-voornamen',
        label: 'Voornamen',
        template: 'text',
        required: false,
      },
      {
        name: 'np-voorvoegsel',
        label: 'Voorvoegsel',
        template: 'text',
        required: false,
      },
      {
        name: 'np-geslachtsnaam',
        label: 'Achternaam',
        template: 'text',
        required: true,
      },
      {
        name: 'np-adellijke_titel',
        label: 'Adellijke titel',
        template: 'text',
        required: false,
      },
      {
        name: 'np-geslachtsaanduiding',
        label: 'Geslacht',
        template: 'radio',
        data: {
          options: [
            {
              value: 'M',
              label: 'Man',
            },
            {
              value: 'V',
              label: 'Vrouw',
            },
            {
              value: 'X',
              label: 'Anders',
            },
          ],
        },
        required: false,
      },
      {
        name: 'np-landcode',
        label: 'Land',
        template: 'select',
        data: {
          options: countries,
        },
        required: true,
      },
      {
        name: 'npc-telefoonnummer',
        label: 'Telefoonnummer',
        template: 'text',
        required: false,
      },
      {
        name: 'npc-mobiel',
        label: 'Telefoonnummer (mobiel)',
        template: 'text',
        required: false,
      },
      {
        name: 'npc-email',
        label: 'Emailadres',
        template: 'text',
        required: false,
      },
    ],
    persDomesticAddrFields: [
      {
        name: 'briefadres',
        label: 'Briefadres',
        template: 'checkbox',
        data: {
          checkboxLabel: 'Ja',
        },
        required: false,
      },
      {
        name: 'np-straatnaam',
        label: 'Straat',
        template: 'text',
        required: () => !get(values, 'briefadres', false),
        data: {
          fieldDisabled:
            get(values, 'np-Disabled', true) && get(values, 'fieldDisabled'),
        },
      },
      {
        name: 'np-huisnummer',
        label: 'Huisnummer',
        template: 'text',
        required: () => !get(values, 'briefadres', false),
      },
      {
        name: 'np-huisnummertoevoeging',
        label: 'Huisnummertoevoeging',
        template: 'text',
        required: false,
      },
      {
        name: 'np-postcode',
        label: 'Postcode',
        template: 'text',
        required: () => !get(values, 'briefadres', false),
        when: ['$values', () => customerCountryCode != '5107'],
      },
      {
        name: 'np-woonplaats',
        label: 'Woonplaats',
        template: 'text',
        required: () => !get(values, 'briefadres', false),
        data: {
          fieldDisabled:
            get(values, 'np-Disabled', true) && get(values, 'fieldDisabled'),
        },
      },
      {
        name: 'np-in_gemeente',
        label: 'Binnengemeentelijk',
        template: 'checkbox',
        data: {
          checkboxLabel: 'Ja',
        },
        required: false,
      },
    ],
    persForeignAddrFields: [
      {
        name: 'np-adres_buitenland1',
        label: 'Adresregel 1',
        template: 'text',
        required: true,
      },
      {
        name: 'np-adres_buitenland2',
        label: 'Adresregel 2',
        template: 'text',
        required: false,
      },
      {
        name: 'np-adres_buitenland3',
        label: 'Adresregel 3',
        template: 'text',
        required: false,
      },
    ],
    corrFields: [
      {
        name: 'np-correspondentie_straatnaam',
        label: 'Briefadres straat',
        template: 'text',
        required: true,
        data: {
          fieldDisabled:
            get(values, 'np-correspondentie_Disabled', true) &&
            get(values, 'fieldDisabled'),
        },
      },
      {
        name: 'np-correspondentie_huisnummer',
        label: 'Briefadres huisnummer',
        template: 'text',
        required: true,
      },
      {
        name: 'np-correspondentie_huisnummertoevoeging',
        label: 'Briefadres huisnummer toevoeging',
        template: 'text',
        required: false,
      },
      {
        name: 'np-correspondentie_postcode',
        label: 'Briefadres postcode',
        template: 'text',
        required: true,
        when: ['$values', () => customerCountryCode != '5107'],
      },
      {
        name: 'np-correspondentie_woonplaats',
        label: 'Briefadres woonplaats',
        template: 'text',
        required: true,
        data: {
          fieldDisabled:
            get(values, 'np-correspondentie_Disabled', true) &&
            get(values, 'fieldDisabled'),
        },
      },
    ],
    orgFields: [
      {
        name: 'vestiging_landcode',
        label: 'Vestiging land',
        template: 'select',
        data: {
          options: countries,
        },
        required: true,
      },
      {
        name: 'rechtsvorm',
        label: 'Rechtsvorm',
        template: 'select',
        data: {
          options: entities,
        },
        required: true,
        when: [
          '$values',
          (vals) => vals.vestiging_landcode === customerCountryCode,
        ],
      },
      {
        name: 'dossiernummer',
        label: 'KVK-nummer',
        template: 'text',
        required: false,
        when: [
          '$values',
          (vals) =>
            vals.vestiging_landcode === customerCountryCode &&
            !vals.vestigingsnummer,
        ],
      },
      {
        name: 'dossiernummer',
        label: 'KVK-nummer',
        template: 'text',
        required: true,
        when: [
          '$values',
          (vals) =>
            vals.vestiging_landcode === customerCountryCode &&
            Boolean(vals.vestigingsnummer),
        ],
      },
      {
        name: 'vestigingsnummer',
        label: 'Vestigingsnummer',
        template: 'text',
        required: false,
        when: [
          '$values',
          (vals) => vals.vestiging_landcode === customerCountryCode,
        ],
      },
      {
        name: 'handelsnaam',
        label: 'Handelsnaam',
        template: 'text',
        required: true,
      },
      {
        name: 'npc-telefoonnummer',
        label: 'Telefoonnummer',
        template: 'text',
        required: false,
      },
      {
        name: 'npc-mobiel',
        label: 'Telefoonnummer (mobiel)',
        template: 'text',
        required: false,
      },
      {
        name: 'npc-email',
        label: 'Emailadres',
        template: 'text',
        required: false,
      },
      {
        name: 'org-briefadres',
        label: 'Correspondentie adres',
        template: 'checkbox',
        data: {
          checkboxLabel: 'Ja',
        },
        required: false,
      },
    ],
    orgDomesticAddrFields: [
      {
        name: 'vestiging_postcode',
        label: 'Vestiging postcode',
        template: 'text',
        required: true,
        when: ['$values', () => customerCountryCode != '5107'],
      },
      {
        name: 'vestiging_huisnummer',
        label: 'Vestiging huisnummer',
        template: 'text',
        required: true,
      },
      {
        name: 'vestiging_huisletter',
        label: 'Vestiging huisletter',
        template: 'text',
        required: false,
      },
      {
        name: 'vestiging_huisnummertoevoeging',
        label: 'Vestiging toevoeging',
        template: 'text',
        required: false,
      },
      {
        name: 'vestiging_straatnaam',
        label: 'Vestiging straat',
        template: 'text',
        required: true,
        data: {
          fieldDisabled:
            get(values, 'vestiging_Disabled', true) &&
            get(values, 'fieldDisabled'),
        },
      },
      {
        name: 'vestiging_woonplaats',
        label: 'Vestiging woonplaats',
        template: 'text',
        required: true,
        data: {
          fieldDisabled:
            get(values, 'vestiging_Disabled', true) &&
            get(values, 'fieldDisabled'),
        },
      },
    ],
    orgForeignAddrFields: [
      {
        name: 'vestiging_adres_buitenland1',
        label: 'Vestiging Adresregel 1',
        template: 'text',
        required: true,
      },
      {
        name: 'vestiging_adres_buitenland2',
        label: 'Vestiging Adresregel 2',
        template: 'text',
        required: false,
      },
      {
        name: 'vestiging_adres_buitenland3',
        label: 'Vestiging Adresregel 3',
        template: 'text',
        required: false,
      },
    ],
    corrAddrFields: [
      {
        name: 'correspondentie_landcode',
        label: 'Correspondentie land',
        template: 'select',
        data: {
          options: countries,
        },
        required: true,
      },
    ],
    corrDomesticAddrFields: [
      {
        name: 'correspondentie_postcode',
        label: 'Correspondentie postcode',
        template: 'text',
        required: true,
        when: ['$values', () => customerCountryCode != '5107'],
      },
      {
        name: 'correspondentie_huisnummer',
        label: 'Correspondentie huisnummer',
        template: 'text',
        required: true,
      },
      {
        name: 'correspondentie_huisletter',
        label: 'Correspondentie huisletter',
        template: 'text',
        required: false,
      },
      {
        name: 'correspondentie_huisnummertoevoeging',
        label: 'Correspondentie toevoeging',
        template: 'text',
        required: false,
      },
      {
        name: 'correspondentie_straatnaam',
        label: 'Correspondentie straat',
        template: 'text',
        required: true,
        data: {
          fieldDisabled:
            get(values, 'vestiging_Disabled', true) &&
            get(values, 'fieldDisabled'),
        },
      },
      {
        name: 'correspondentie_woonplaats',
        label: 'Correspondentie woonplaats',
        template: 'text',
        required: true,
        data: {
          fieldDisabled:
            get(values, 'vestiging_Disabled', true) &&
            get(values, 'fieldDisabled'),
        },
      },
    ],
    corrForeignAddrFields: [
      {
        name: 'correspondentie_adres_buitenland1',
        label: 'Correspondentie Adresregel 1',
        template: 'text',
        required: true,
      },
      {
        name: 'correspondentie_adres_buitenland2',
        label: 'Correspondentie Adresregel 2',
        template: 'text',
        required: false,
      },
      {
        name: 'correspondentie_adres_buitenland3',
        label: 'Correspondentie Adresregel 3',
        template: 'text',
        required: false,
      },
    ],
  };
};
