// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

// ZS-TODO: this is a copy of /frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/dateOrText.js
// Do **not** depend on more `/frontend` code in `/intern` (or the other way round).
module.exports = angular
  .module('Zaaksysteem.dateOrTextFilter', [])
  .factory('dateOrTextFilter', [
    '$filter',
    function dateOrTextFilterFactory($filter) {
      const dateFilter = $filter('date');

      return function dateOrTextFilter(string) {
        const date = new Date(string);

        if (!isNaN(date.getTime())) {
          return dateFilter.apply(this, arguments);
        }

        return string;
      };
    },
  ]).name;
