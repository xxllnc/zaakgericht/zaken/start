// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import rwdServiceModule from './../../util/rwdService';
import viewTitleModule from './../../util/route/viewTitle';
import zsDropdownMenu from '../../ui/zsDropdownMenu';
import zsSideMenuModule from './../zsSideMenu';
import zsSpotEnlighter from '../../ui/zsSpotEnlighter';
import zsUiViewProgressModule from './../../ui/zsNProgress/zsUiViewProgress';
import controller from './ZsTopBarController';
import template from './template.html';
import './topbar.scss';

module.exports = angular
  .module('shared.navigation.zsTopBar', [
    angularUiRouterModule,
    rwdServiceModule,
    viewTitleModule,
    zsDropdownMenu,
    zsSideMenuModule,
    zsSpotEnlighter,
    zsUiViewProgressModule,
  ])
  .component('zsTopBar', {
    bindings: {
      allowSearch: '&',
      company: '&',
      development: '&',
      instanceId: '&',
      onClose: '&',
      onOpen: '&',
      useLocation: '&',
      user: '&',
    },
    controller,
    template,
  }).name;
