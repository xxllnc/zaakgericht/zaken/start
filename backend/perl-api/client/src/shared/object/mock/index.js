// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import shortid from 'shortid';
import assign from 'lodash/assign';
import propCheck from './../../util/propCheck';

let converters = {
  v1: (source, version) => {
    let reference, label, instance, type;

    switch (version) {
      case 'v0':
        reference = source.id;
        label = source.label;
        instance = source.values;
        type = source.type;
        break;
    }

    return {
      reference,
      label,
      instance,
      type,
    };
  },
  v0: (source, version) => {
    let id, values, label, type;

    switch (version) {
      case 'v1':
        id = source.reference;
        label = source.label;
        values = source.instance;
        type = source.type;
        break;
    }

    return {
      id,
      label,
      values,
      type,
    };
  },
};

export const mockObject = (preferredVersion, preferredParameters) => {
  let obj,
    id,
    type,
    label,
    values,
    version = preferredVersion,
    parameters = preferredParameters;

  if (typeof version !== 'string') {
    parameters = version;
    version = 'v1';
  }

  propCheck.throw(
    propCheck.shape({
      version: propCheck.string,
      parameters: propCheck.shape({
        type: propCheck.string,
        values: propCheck.object,
        id: propCheck.string.optional,
      }),
    }),
    { version, parameters }
  );

  id = parameters.id || shortid();
  type = parameters.type;
  label = parameters.label;
  values = assign({}, parameters.values);

  switch (version) {
    default:
      throw new Error(
        `Cannot create mock object for unknown version ${version}`
      );

    case 'v1':
      obj = {
        type,
        reference: id,
        instance: values,
      };
      break;

    case 'v0':
      obj = {
        type,
        id,
        values,
        label,
      };
      break;
  }

  return obj;
};
export const convertObject = (source, fromVersion, toVersion) => {
  propCheck.throw(
    propCheck.shape({
      source: propCheck.object,
      fromVersion: propCheck.string,
      toVersion: propCheck.string,
    }),
    { source, fromVersion, toVersion }
  );

  let converter = converters[toVersion];

  if (!converter) {
    throw new Error(
      `Couldn't convert object: no converter found for ${toVersion}`
    );
  }

  return converter(source, fromVersion);
};
