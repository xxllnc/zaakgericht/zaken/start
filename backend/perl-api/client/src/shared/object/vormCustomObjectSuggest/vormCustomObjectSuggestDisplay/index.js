// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import zsIconModule from './../../../ui/zsIcon';
import zsModalModule from './../../../../shared/ui/zsModal';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import composedReducerModule from '../../../api/resource/composedReducer';

module.exports = angular
  .module('vormCustomObjectSuggestDisplay', [
    zsIconModule,
    zsModalModule,
    composedReducerModule,
    snackbarServiceModule,
  ])
  .directive('vormCustomObjectSuggestDisplay', [
    '$compile',
    ($compile) => {
      return {
        restrict: 'E',
        template,
        scope: {
          templateData: '&',
          delegate: '&',
          object: '&',
          formatter: '&',
          icon: '@',
          caseData: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$http',
          'snackbarService',
          'zsModal',
          function ($scope, $http, snackbarService, zsModal) {
            let ctrl = this,
              modal,
              modalScope;
            const delegate = ctrl.delegate();
            const templateData = ctrl.templateData();

            ctrl.getObjectUrl = () => {
              const object = ctrl.object();
              const objectUuid = object.id;

              return `/main/object/${objectUuid}`;
            };

            ctrl.getLabel = () => {
              const object = ctrl.object();

              return ctrl.formatter()
                ? ctrl.formatter()(object) || ''
                : get(object, 'label') || '';
            };

            ctrl.canEdit = () => templateData.allowEdit;

            ctrl.displayFullWidth = () => !delegate.clearable;

            //modal

            let closeModal = () => {
              modal.close();
              modalScope.$destroy();
              modal = modalScope = null;
            };

            ctrl.openModal = () => {
              const caseUuid = templateData.caseUuid;
              const object = ctrl.object();
              const objectUuid = object.id;
              const attributeId = templateData.attributeId;
              const objectTypeUuid = templateData.queryOptions.objectTypeUuid;

              const iframeUrl = `/external-components/exposed/case/${caseUuid}/object/update/${objectUuid}?objectTypeUuid=${objectTypeUuid}&attributeId=${attributeId}`;

              modalScope = $scope.$new();

              modalScope.route = () => iframeUrl;

              modalScope.caseData = ctrl.caseData();

              modalScope.onMessage = (message) => {
                if (message && message.type === 'handleObjectSuccess') {
                  $http({
                    url: `/api/v2/cm/custom_object/get_custom_object?uuid=${message.data.uuid}`,
                    method: 'GET',
                  }).then((response) => {
                    // use the version_independent_uuid to always link to the most recent version
                    const id =
                      response.data.data.attributes.version_independent_uuid;
                    const newObject = {
                      data: undefined,
                      description: response.data.data.attributes.subtitle,
                      id,
                      label: response.data.data.meta.summary,
                      link: `/object/${id}`,
                      name: id,
                      type: 'custom_object',
                    };

                    delegate.value = newObject;
                    delegate.onChange();

                    closeModal();

                    const snackbarText = {
                      created: 'Het object is aangemaakt.',
                      updated: 'Het object is bewerkt.',
                      deactivated: 'Het object is uitgeschakeld.',
                    }[message.data.result];

                    snackbarService.info(snackbarText, {
                      actions: [
                        {
                          type: 'link',
                          label: 'Object openen',
                          link: `/main/object/${id}`,
                        },
                      ],
                    });
                  });
                }
              };

              modal = zsModal({
                title: `${templateData.objectTypeName} bewerken`,
                fullWidth: true,
                onClose: closeModal,
                el: $compile(
                  `<zs-react-iframe
                  iframe-src="route()"
                  on-message="onMessage(data)"
                  data-height="600"
                  case-data="caseData"
                ></zs-react-iframe>`
                )(modalScope),
              });

              modal.open();
            };
          },
        ],
        controllerAs: 'ctrl',
      };
    },
  ]).name;
