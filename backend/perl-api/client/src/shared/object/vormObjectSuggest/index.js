// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

import vormTemplateServiceModule from './../../vorm/vormTemplateService';
import vormObjectSuggestDisplayModule from './vormObjectSuggestDisplay';
import vormObjectSuggestModelModule from './vormObjectSuggestModel';

import './vorm-object.scss';

module.exports = angular
  .module('vormObjectSuggest', [
    vormTemplateServiceModule,
    vormObjectSuggestModelModule,
    vormObjectSuggestDisplayModule,
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('object-suggest', {
        control: angular.element(
          `<vorm-object-suggest-model
								ng-model
								data-placeholder="{{vm.invokeData('placeholder')}}"
							>
							</vorm-object-suggest-model>`
        ),
        display: angular.element(
          `<vorm-object-suggest-display
								data-object="delegate.value"
								data-formatter="vm.templateData().display"
								data-icon="{{vm.templateData().icon}}"
								class="list-item-flex-text suggestion-display-item-text"
							>
							</vorm-object-suggest-display>`
        ),
        defaults: {
          editMode: 'empty',
        },
      });
    },
  ]).name;
