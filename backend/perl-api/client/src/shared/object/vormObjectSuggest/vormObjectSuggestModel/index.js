// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import zsObjectSuggestModule from './../../zsObjectSuggest';
import vormObjectSuggestDisplayModule from './../vormObjectSuggestDisplay';
import composedReducerModule from './../../../api/resource/composedReducer';
import get from 'lodash/get';

module.exports = angular
  .module('vormObjectSuggestModel', [
    zsObjectSuggestModule,
    vormObjectSuggestDisplayModule,
    composedReducerModule,
  ])
  .directive('vormObjectSuggestModel', [
    'vormInvoke',
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          delegate: '&',
          inputId: '&',
          placeholder: '@',
        },
        require: ['vormObjectSuggestModel', '^vormField', 'ngModel'],
        bindToController: true,
        controller: [
          '$scope',
          'sessionService',
          'composedReducer',
          function ($scope, sessionService, composedReducer) {
            let ctrl = this,
              vormField,
              ngModel;

            ctrl.link = (controllers) => {
              [vormField, ngModel] = controllers;
            };

            let sessionResource = sessionService.createResource($scope);

            let sessionReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce((session) => (session ? session.instance : {}));

            ctrl.getSession = () => sessionReducer.data();

            ctrl.getObjectType = () => {
              return vormField ? vormField.invokeData('objectType') : null;
            };

            ctrl.getQueryOptions = () => {
              return vormField ? vormField.invokeData('queryOptions') : null;
            };

            ctrl.handleSuggest = ($object) => {
              let formatter = get(vormField.templateData(), 'format'),
                object = $object;

              if (formatter) {
                object = formatter(object);
              }

              ngModel.$setViewValue(object, 'click');
            };

            ctrl.clearObject = () => {
              vormField.clearDelegate(ctrl.delegate());
            };

            ctrl.getObject = () => ngModel.$modelValue;
          },
        ],
        controllerAs: 'vormObjectSuggestModel',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ]).name;
