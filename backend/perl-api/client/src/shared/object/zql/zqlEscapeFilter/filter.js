// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (input) =>
  typeof input !== 'string'
    ? input
    : `"${input.replace(/(["\\])/g, '\\$1').replace(/;/g, '%3B')}"`;
