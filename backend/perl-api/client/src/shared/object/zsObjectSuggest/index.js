// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import objectSuggestionServiceModule from './objectSuggestionService';
import resourceModule from './../../api/resource';
import zsSpinnerModule from './../../ui/zsSpinner';
import zsSuggestionListModule from './../../ui/zsSuggestionList';
import zsPositionFixedModule from './../../ui/zsPositionFixed';
import zsTooltipModule from './../../ui/zsTooltip';
import zsObjectSuggestAdvancedSearchModule from './zsObjectSuggestAdvancedSearch';
import get from 'lodash/get';
import './suggest.scss';

module.exports = angular
  .module('shared.object.zsObjectSuggest', [
    objectSuggestionServiceModule,
    resourceModule,
    zsSpinnerModule,
    zsSuggestionListModule,
    zsPositionFixedModule,
    zsTooltipModule,
    zsObjectSuggestAdvancedSearchModule,
  ])
  .directive('zsObjectSuggest', [
    '$window',
    '$timeout',
    '$compile',
    '$animate',
    'resource',
    'objectSuggestionService',
    (
      $window,
      $timeout,
      $compile,
      $animate,
      resource,
      objectSuggestionService
    ) => {
      return {
        restrict: 'E',
        template,
        scope: {
          type: '&',
          queryOptions: '&',
          session: '&',
          onSelect: '&',
          onKeyUp: '&',
          placeholder: '@',
          isFixed: '&',
          inputId: '&',
          replaceQueryText: '@',
        },
        bindToController: true,
        controller: [
          '$document',
          '$scope',
          '$element',
          function ($document, $scope, $element) {
            let ctrl = this,
              suggestionResource,
              open = false,
              input = $element.find('input'),
              modelOptions = {
                updateOn: 'default blur',
                debounce: {
                  default: 250,
                  blur: 0,
                },
              };

            let getRequestOptions = () => {
              let opts = open
                ? objectSuggestionService.getRequestOptions(
                    ctrl.query,
                    ctrl.type(),
                    ctrl.session(),
                    ctrl.queryOptions()
                  )
                : null;

              return opts;
            };

            let contains = (child) => $element[0].contains(child);

            ctrl.getSuggestions = () => suggestionResource.data();

            ctrl.handleSelect = (suggestion) => {
              const isBag =
                suggestion.type === 'bag' ||
                suggestion.type === 'bag-street' ||
                suggestion.type === 'bag_address' ||
                suggestion.type === 'bag_street';

              if (suggestion && isBag) {
                fetch(
                  `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?${new URLSearchParams(
                    { id: suggestion.id }
                  )}`
                )
                  .then((r) => r.json())
                  .then((resp) => {
                    const val = resp.response.docs[0];
                    const id =
                      suggestion.type === 'bag_address'
                        ? `nummeraanduiding-${val.nummeraanduiding_id}`
                        : `openbareruimte-${val.openbareruimte_id}`;

                    ctrl.onSelect({
                      $object: {
                        label: val.weergavenaam,
                        id,
                        name: id,
                        type: suggestion.type,
                      },
                    });
                  });
              } else {
                ctrl.onSelect({
                  $object: suggestion,
                });
              }

              open = false;

              ctrl.query = ctrl.replaceQueryText ? suggestion.label : '';
            };

            ctrl.open = () => {
              open = true;
            };

            ctrl.close = () => {
              open = false;
            };

            ctrl.isOpen = () => !!(open && getRequestOptions());

            ctrl.getStateLabel = () => {
              let label = '';

              if (
                ctrl.query &&
                suggestionResource.state() === 'resolved' &&
                get(suggestionResource.data(), 'length', 0) === 0
              ) {
                label = 'Geen resultaten gevonden';
              } else if (suggestionResource.state() === 'rejected') {
                label = 'Resultaten konden niet worden opgehaald.';
              }

              return label;
            };

            ctrl.getModelOptions = () => modelOptions;

            ctrl.isLoading = () => suggestionResource.state() === 'pending';

            ctrl.getInputDelegate = () => ({ input });

            ctrl.hasAdvancedSearch = () => {
              return (
                ctrl.type() === 'natuurlijk_persoon' ||
                ctrl.type() === 'bedrijf' ||
                ctrl.type() === 'casetype'
              );
            };

            ctrl.handleIconClick = () => {
              $timeout(
                () => {
                  $element.find('input')[0].focus();
                },
                0,
                false
              );
            };

            ctrl.handleAdvancedSearchClick = () => {
              let popupScope = $scope.$new(true),
                el,
                destroyUnwatcher;

              let close = () => {
                destroyUnwatcher();
                popupScope.$destroy();

                return $animate.leave(el).then(() => {
                  el.remove();
                });
              };

              destroyUnwatcher = $scope.$on('$destroy', () => {
                close();
              });

              popupScope.onSelect = (obj) => {
                close().then(() => {
                  ctrl.onSelect({
                    $object: obj,
                  });
                });
              };

              popupScope.onClose = () => {
                close();
              };

              el = $compile(
                `<zs-object-suggest-advanced-search
								data-type="'${ctrl.type()}'"
								on-close="onClose()"
								on-select="onSelect($object)"
								zs-trap-keyboard-focus="true"
							>
							</zs-object-suggest-advanced-search>`
              )(popupScope);

              $animate.enter(el, $element);
            };

            $element.find('input').bind('focusin', (event) => {
              if (!contains(event.relatedTarget) && !open) {
                $scope.$apply(ctrl.open);
              }
            });

            $document.bind('mousedown', (event) => {
              if (open && !contains(event.target)) {
                $scope.$apply(ctrl.close);
              }
            });

            $element.find('input').bind('keyup', () => {
              ctrl.onKeyUp({ $query: input[0].value });
            });

            $element.find('input').bind('click input focusin', () => {
              if (!open) {
                $scope.$apply(ctrl.open);
              }
            });

            ctrl.query = '';

            suggestionResource = resource(getRequestOptions, {
              scope: $scope,
              cache: { disabled: true },
            }).reduce((requestOptions, data) =>
              objectSuggestionService.reduce(data, ctrl.type(), ctrl.session())
            );
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
