// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';
import mapKeys from 'lodash/mapKeys';
import omit from 'lodash/omit';
import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';
import isRequired from './common';

export default () => {
  return {
    request: (values) => {
      return {
        method: 'POST',
        url: !values.remote
          ? '/api/v1/subject'
          : '/api/v1/subject/remote_search/',
        data: {
          query: {
            match: assign(
              {
                subject_type: 'company',
              },
              mapKeys(
                omit(pickBy(values, identity), 'remote'),
                (value, key) => `subject.${key}`
              )
            ),
          },
        },
      };
    },
    fields: [
      {
        name: 'remote',
        label: '',
        template: 'checkbox',
        data: {
          checkboxLabel: 'Extern bevragen',
        },
      },
      {
        name: 'rsin',
        label: 'RSIN',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'company',
              'coc_number',
              'coc_location_number',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]);
          },
        ],
      },
      {
        name: 'coc_number',
        label: 'KVK nummer',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'company',
              'rsin',
              'coc_location_number',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]);
          },
        ],
      },
      {
        name: 'coc_location_number',
        label: 'Vestigingsnummer',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'company',
              'coc_number',
              'rsin',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]);
          },
        ],
      },
      {
        name: 'company',
        label: 'Handelsnaam',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'coc_number',
              'rsin',
              'coc_location_number',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]);
          },
        ],
      },
      {
        name: 'address_residence.street',
        label: 'Straat',
        template: 'text',
      },
      {
        name: 'address_residence.zipcode',
        label: 'Postcode',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'coc_number',
              'rsin',
              'coc_location_number',
              'company',
            ]);
          },
        ],
      },
      {
        name: 'address_residence.street_number',
        label: 'Huisnummer',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'coc_number',
              'rsin',
              'coc_location_number',
              'company',
            ]);
          },
        ],
      },
      {
        name: 'address_residence.street_number_letter',
        label: 'Huisletter',
        template: 'text',
      },
      {
        name: 'address_residence.street_number_suffix',
        label: 'Huisnummer toevoeging',
        template: 'text',
      },
      {
        name: 'address_residence.city',
        label: 'Plaats',
        template: 'text',
      },
    ],
    columns: [
      {
        id: 'coc_number',
        label: 'KVK-nummer',
      },
      {
        id: 'coc_location_number',
        label: 'Vestigingsnummer',
      },
      {
        id: 'company',
        label: 'Handelsnaam',
      },
      {
        id: 'address',
        label: 'Adres',
        valuePath: 'address',
        template: '<span>{{::item.address}}</span>',
      },
    ],
  };
};
