// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';
import get from 'lodash/get';
import identity from 'lodash/identity';
import includes from 'lodash/includes';
import mapKeys from 'lodash/mapKeys';
import omit from 'lodash/omit';
import pickBy from 'lodash/pickBy';
import isRequired from './common.js';

/**
 *
 * Check if a person can be looked up via the StUF broker.
 * The user isn't allowed to search if the search is only on the webform
 * or if said user hasn't got the capability to search in remote systems
 *
 * @param moduleInstance
 * @param userInstance
 * @return {boolean}
 *
 **/

function allowStUFSearch(module, user) {
  const interfaceConfig = get(module, 'interface_config');

  if (
    Boolean(get(interfaceConfig, 'gbav_search')) === false &&
    Boolean(get(interfaceConfig, 'local_search')) === false
  ) {
    return false;
  }

  if (Boolean(get(interfaceConfig, 'search_extern_webform_only')) === true) {
    return false;
  }

  if (
    Boolean(get(interfaceConfig, 'gbav_search_role_restriction')) === true &&
    Boolean(
      includes(user.logged_in_user.capabilities, 'contact_search_extern')
    ) === false
  ) {
    return false;
  }
  return true;
}

/**
 *
 * Get the name of the stuf configuration module
 *
 * @param moduleInstance
 * @return {string}
 *
 **/

function getModuleName(module) {
  return module.interface_config.search_form_title || module.name;
}

/**
 *
 * Get all the available modules for stuf
 * Returns null when nothing can be searched
 * Returns an array when things can be searched
 *
 * @param moduleInstance
 * @param user
 * @return {*} yourAnswer
 *
 **/

function getAvailableModules(modules, user) {
  if (modules.length === 0) {
    return null;
  }

  const availableModules = modules
    .map((module) => {
      if (allowStUFSearch(module.instance, user)) {
        return module.instance;
      }
      return null;
    })
    .filter(identity);

  if (availableModules.length === 0) {
    return null;
  }
  return availableModules;
}

/**
 *
 * Get the display information for the internal search options
 * Returns a null when nothing can be found and a hash otherwise
 *
 * @param moduleInstance
 * @param userInstance
 * @return {*} yourAnswer
 *
 **/

function getRemoteFields(modules, user) {
  const availableModules = getAvailableModules(modules, user);

  if (!availableModules) {
    return null;
  } else if (availableModules.length === 1) {
    const label = getModuleName(availableModules[0]);
    return {
      name: 'remote',
      template: 'checkbox',
      label: '',
      data: {
        checkboxLabel: `Zoeken in ${label}`,
      },
    };
  } else {
    return {
      name: 'remote',
      template: 'select',
      label: 'Bron',
      data: {
        notSelectedLabel: 'Intern',
        options: availableModules.map((module) => {
          return {
            value: module.id,
            label: getModuleName(module),
          };
        }),
      },
    };
  }
}

export default (
  locals = {
    modules: [],
    user: [],
  }
) => {
  const modules = locals.modules;
  const user = locals.user.instance;

  const remoteField = getRemoteFields(modules, user);
  const countryCode = user ? user.account.instance.country_code : null;

  return {
    format: (values) => {
      if (modules.length === 1) {
        return values.merge({
          remote: values.remote ? modules[0].instance.id : null,
        });
      }

      return values;
    },
    parse: (values) => {
      if (modules.length === 1) {
        return values.merge({ remote: !!values.remote });
      }

      return values;
    },
    request: (values) => {
      const dateOfBirthSet = get(values, 'date_of_birth');
      let processedValues;

      if (dateOfBirthSet) {
        const date_of_birth = new Date(dateOfBirthSet.asMutable().setHours(12))
          .toISOString()
          .replace(/T.*/, '');

        processedValues = assign({}, values, {
          date_of_birth,
        });
      }

      return {
        method: 'POST',
        url: !values.remote
          ? '/api/v1/subject'
          : `/api/v1/subject/remote_search/${values.remote}`,
        data: {
          query: {
            match: assign(
              {
                subject_type: 'person',
              },
              mapKeys(
                omit(
                  pickBy(dateOfBirthSet ? processedValues : values, identity),
                  'remote'
                ),
                (value, key) => `subject.${key}`
              )
            ),
          },
        },
      };
    },
    fields: [
      ...(remoteField ? [remoteField] : []),
      {
        name: 'personal_number',
        label: 'BSN',
        template: 'text',
        required: [
          '$values',
          (vals) =>
            isRequired(vals, [
              'date_of_birth',
              'persoonsnummer',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]),
        ],
      },
      {
        name: 'persoonsnummer',
        label: 'ID Nummer',
        template: 'text',
        required: [
          '$values',
          (vals) =>
            isRequired(vals, [
              'personal_number',
              'date_of_birth',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]),
        ],
        when: [() => countryCode === '5107'],
      },
      {
        name: 'date_of_birth',
        label: 'Geboortedatum',
        template: 'date',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'personal_number',
              'persoonsnummer',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]);
          },
        ],
      },
      {
        name: 'prefix',
        label: 'Voorvoegsel',
        template: 'text',
      },
      {
        name: 'family_name',
        label: 'Achternaam',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'personal_number',
              'persoonsnummer',
              'address_residence.zipcode',
              'address_residence.street_number',
            ]);
          },
        ],
      },
      {
        name: 'address_residence.zipcode',
        label: 'Postcode',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'personal_number',
              'persoonsnummer',
              'date_of_birth',
            ]);
          },
        ],
      },
      {
        name: 'address_residence.street_number',
        label: 'Huisnummer',
        template: 'text',
        required: [
          '$values',
          (vals) => {
            return isRequired(vals, [
              'personal_number',
              'persoonsnummer',
              'date_of_birth',
            ]);
          },
        ],
      },
      {
        name: 'address_residence.street_number_suffix',
        label: 'Huisnummertoevoeging',
        template: 'text',
      },
      {
        name: 'address_residence.street_number_letter',
        label: 'Huisnummerletter',
        template: 'text',
      },
    ],
    columns: [
      {
        id: 'gender',
        template: `<zs-icon
							icon-type="alert-circle"
							ng-show="item.messages"
							zs-tooltip="{{::item.messages || ''}}"
						></zs-icon>`,
        label: '',
      },
      {
        id: 'first_names',
        label: 'Voornamen',
      },
      {
        id: 'family_name',
        label: 'Achternaam',
      },
      {
        id: 'date_of_birth',
        template:
          '<span>{{::item.instance.subject.instance.date_of_birth | date:"dd-MM-yyyy"}}</span>',
        label: 'Geboortedatum',
      },
      {
        id: 'address',
        label: 'Adres',
        valuePath: 'address',
        template: '<span>{{::item.address}}</span>',
      },
    ],
  };
};
