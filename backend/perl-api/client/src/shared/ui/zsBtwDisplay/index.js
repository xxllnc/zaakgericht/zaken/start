// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from './controller';
import template from './template.html';

controller.$inject = ['currencyFilter'];

module.exports = angular.module('zsBtwDisplay', []).component('zsBtwDisplay', {
  bindings: {
    value: '&',
    btwType: '&',
  },
  controller,
  template,
}).name;
