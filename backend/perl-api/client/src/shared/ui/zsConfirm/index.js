// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsModalModule from './../zsModal';
import template from './template.html';

module.exports = angular
  .module('zsConfirm', [zsModalModule])
  .factory('zsConfirm', [
    '$interpolate',
    '$rootScope',
    '$compile',
    '$q',
    'zsModal',
    ($interpolate, $rootScope, $compile, $q, zsModal) => {
      let tpl = template;

      /* here be dragons */
      if ($interpolate.startSymbol() !== '{{') {
        tpl = tpl
          .replace(/{{/g, $interpolate.startSymbol())
          .replace(/}}/g, $interpolate.endSymbol());
      }

      return (label, verb = 'Bevestigen', options) => {
        return $q((resolve, reject) => {
          let scope = $rootScope.$new(true);

          let modal = zsModal({
            title: 'Zaak weigeren bevestigen',
            el: $compile(tpl)(scope),
            classes: 'confirm',
          });

          let cleanup = () => {
            scope.$destroy();
          };

          scope.vm = {
            label,
            verb,
            options,
            values: {},
            onConfirm: () => {
              modal.close();
              cleanup();
              resolve(scope.vm.values);
            },
            onCancel: () => {
              modal.close();
              cleanup();
              reject();
            },
          };

          modal.onClose(() => {
            cleanup();
            reject();
          });

          modal.open();
        });
      };
    },
  ]).name;
