// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import controller from './controller';
import zsPositionFixedModule from './../zsPositionFixed';
import zsIcon from './../zsIcon';
import capabilitiesModule from '../../util/capabilities';
import './dropdown-menu.scss';

module.exports = angular
  .module('shared.ui.zsDropdownMenu', [
    zsIcon,
    zsPositionFixedModule,
    capabilitiesModule,
  ])
  .directive('zsDropdownMenu', [
    '$animate',
    '$document',
    'capabilities',
    ($animate, $document, capabilities) => {
      return {
        restrict: 'E',
        template,
        scope: {
          autoClose: '=',
          options: '&',
          positionOptions: '&',
          buttonIcon: '@',
          buttonIconOptions: '&',
          buttonLabel: '@',
          buttonStyle: '&',
          mode: '&',
          isFixed: '&',
          onOpen: '&',
          onClose: '&',
          name: '@',
          emptyLabel: '@',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            controller.call(
              this,
              $document,
              $scope,
              $element,
              $animate,
              capabilities()
            );
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
