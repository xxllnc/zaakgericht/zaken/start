// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from './KeyboardNavigableListController';

module.exports = angular
  .module('shared.ui.zsKeyboardNavigableList', [])
  .directive('zsKeyboardNavigableList', () => ({
    restrict: 'A',
    scope: {
      keyInputDelegate: '&',
      highlightableItems: '&',
      onKeyCommit: '&',
      onKeyHighlight: '&',
      commitOnTab: '&',
    },
    bindToController: true,
    controllerAs: 'zsKeyboardNavigableList',
    controller,
  })).name;
