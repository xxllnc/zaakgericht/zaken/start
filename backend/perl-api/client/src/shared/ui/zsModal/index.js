// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

import assign from 'lodash/assign';
import defaults from 'lodash/defaults';
import each from 'lodash/each';
import uniqueId from 'lodash/uniqueId';

import propCheck from './../../util/propCheck';

import template from './template.html';
import './styles.scss';

module.exports = angular.module('zsModal', []).factory('zsModal', [
  '$interpolate',
  '$rootScope',
  '$compile',
  '$timeout',
  '$animate',
  '$document',
  '$window',
  (
    $interpolate,
    $rootScope,
    $compile,
    $timeout,
    $animate,
    $document,
    $window
  ) => {
    const KEYCODE_ESCAPE = 27;
    let tpl = template;
    let compiler;

    if ($interpolate.startSymbol() !== '{{') {
      tpl = tpl
        .replace(/{{/g, $interpolate.startSymbol())
        .replace(/}}/g, $interpolate.endSymbol());
    }

    compiler = $compile(tpl);

    return (preferredOptions) => {
      let options = defaults(preferredOptions, {
        title: '',
        parent: $document.find('body'),
        position: () => {
          return {
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          };
        },
        fullWidth: false,
        classes: '',
        repositionOnScroll: false,
      });
      let scope;
      let modalEl;
      let modal;
      let closeListeners = [];
      let isClosed = false;
      let onClose = options.onClose;

      let attemptClose = () => {
        if (!isClosed && closeListeners.filter((fn) => fn()).length === 0) {
          modal.close();

          if (onClose) {
            onClose();
          }
        }
      };

      let onKeyUp = (event) => {
        if (event.keyCode === KEYCODE_ESCAPE) {
          event.stopPropagation();
          scope.$evalAsync(attemptClose);
        }
      };

      let resetDimensions = () => {
        modalEl.css(options.position());
      };

      propCheck.throw(
        propCheck.shape({
          title: propCheck.string.optional,
        }),
        options
      );

      scope = $rootScope.$new(true);

      if (options.before) {
        options = assign({}, options, {
          before: null,
          after: null,
          parent: options.before.parent(),
        });
      }

      modal = {
        open: (fn) => {
          fn && fn(modalEl[0]);

          if (options.openFrom) {
            let bounds = options.openFrom[0].getBoundingClientRect();
            let viewport = {
              width: $window.innerWidth,
              height: $window.innerHeight,
            };
            let dimensions = {
              top: `${bounds.top}px`,
              left: `${bounds.left}px`,
              right: `${viewport.width - bounds.right}px`,
              bottom: `${viewport.height - bounds.bottom}px`,
            };

            modalEl.css(dimensions);
            $timeout(resetDimensions, 0, false);
          } else if (options.fullWidth) {
            modalEl.children()[0].style.width = '100%';
            modalEl.children()[0].style.maxWidth = '850px';
          }

          $document[0].addEventListener('keyup', onKeyUp, true);

          return $animate.enter(modalEl, options.parent, options.after, {
            addClass: 'modal-open',
          });
        },
        close: () => {
          isClosed = true;

          return $animate
            .leave(modalEl, { removeClass: 'modal-open' })
            .then(() => {
              $document[0].removeEventListener('keyup', onKeyUp, true);
              scope.$destroy();
            });
        },
        hide: () => {
          return $animate.addClass(modalEl, 'ng-hide');
        },
        show: () => {
          return $animate.removeClass(modalEl, 'ng-hide');
        },
        onClose: (fn) => {
          closeListeners = closeListeners.concat(fn);
        },
      };

      scope.title = options.title;
      scope.downloadUrl = options.downloadUrl;
      scope.onCloseClick = attemptClose;
      scope.id = uniqueId('modal');

      if (options.repositionOnScroll) {
        $window.addEventListener('scroll', resetDimensions);

        modal.onClose(() => {
          $window.removeEventListener('scroll', resetDimensions);
        });
      }

      compiler(scope, (el) => {
        modalEl = el;

        each(options.classes.split(' '), (cl) => {
          modalEl.addClass(cl);
        });

        modalEl.find('zs-transclude').replaceWith(options.el);
      });

      return modal;
    };
  },
]).name;
