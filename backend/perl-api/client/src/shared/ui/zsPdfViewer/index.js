// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import snackbarServiceModule from './../zsSnackbar/snackbarService';
import get from 'lodash/get';
import './styles.scss';

module.exports = angular
  .module('zsPdfViewer', [snackbarServiceModule])
  .directive('zsPdfViewer', [
    '$rootScope',
    '$q',
    'snackbarService',
    ($rootScope, $q, snackbarService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          url: '@',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let ctrl = this,
              loadingLib,
              loadingDoc,
              pdfDocument,
              viewer;

            let loadUrl = (url) => {
              loadingDoc = true;

              // eslint-disable-next-line
              PDFJS.getDocument({
                url,
                // Firefox doesn't pass an accept header,
                // causing the service worker to intercept it
                // as a text/html request
                httpHeaders: {
                  Accept: '*/*',
                  'ZS-PDF-Viewer': '1',
                },
              }).then(
                (pdfDoc) => {
                  loadingDoc = false;

                  pdfDocument = pdfDoc;

                  return viewer.setDocument(pdfDoc).then(() => {
                    viewer.currentScaleValue = 'page-width';
                  });
                },
                (err) => {
                  console.error(err);

                  loadingDoc = false;

                  snackbarService.error(
                    'Het document kon niet worden weergegeven. Neem contact op met uw beheerder voor meer informatie.'
                  );
                }
              );
            };

            loadingLib = true;

            new Promise((rez) => {
              const s1 = document.createElement('script');
              const s2 = document.createElement('script');
              let loadend = false;

              s1.src =
                'https://cdn.jsdelivr.net/npm/pdfjs-dist@1.4.83/build/pdf.combined.js';
              s2.src =
                'https://cdn.jsdelivr.net/npm/pdfjs-dist@1.4.83/web/pdf_viewer.js';

              s1.addEventListener('load', () => {
                loadend && rez();
                loadend = true;
              });

              s2.addEventListener('load', () => {
                loadend && rez();
                loadend = true;
              });

              document.body.appendChild(s1);
              document.body.appendChild(s2);
            })
              .then(() => {
                let container = element[0].querySelector('.pdf-viewer-canvas');

                // eslint-disable-next-line
                viewer = new PDFJS.PDFViewer({
                  container,
                });

                viewer.currentScaleValue = 'page-width';

                if (!scope.$$destroyed) {
                  loadUrl(ctrl.url);
                }
              })
              .catch((err) => {
                console.error(err);

                snackbarService.error(
                  'De PDF viewer kon niet worden geopend. Neem contact op met uw beheerder voor meer informatie.'
                );
              })
              .finally(() => {
                loadingLib = false;
              });

            ctrl.getTotalPages = () => {
              get(pdfDocument, 'numPages');
            };

            ctrl.getCurrentPage = () => {
              get(viewer, 'currentPage');
            };

            ctrl.isLoading = () => loadingLib || loadingDoc;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
