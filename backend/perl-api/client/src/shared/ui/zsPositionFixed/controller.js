// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import adjustEngine from 'adjust-engine';
import defaultsDeep from 'lodash/defaultsDeep';
import flatten from 'lodash/flatten';
import includes from 'lodash/includes';

export default (scope, element, document, $animate, preferredOptions = {}) => {
  const customPositioning = preferredOptions.customPositioning;

  const win = document[0].defaultView;
  const options = defaultsDeep({}, preferredOptions, {
    attachment: 'top left',
    target: 'bottom left',
    flip: true,
    updateOn: 'default',
  });
  const updateOn = flatten(
    options.updateOn.split(' ').map((prop) => {
      return prop === 'default' ? ['scroll', 'resize'] : prop;
    })
  );
  const adjuster = adjustEngine(options);
  let enabled = false;
  let unwatch;

  const position = () => {
    const reference = options.reference;
    const referenceRect = reference[0].getBoundingClientRect();

    if (options.autosize) {
      element.css('width', `${referenceRect.width}px`);
    }

    const elRect = element[0].getBoundingClientRect();
    const pos = adjuster(elRect, referenceRect, {
      left: 0,
      top: 0,
      right: win.innerWidth,
      bottom: win.innerHeight,
    });

    if (element[0].offsetParent) {
      const offsetRect = element[0].offsetParent.getBoundingClientRect();

      pos.top -= offsetRect.top;
      pos.left -= offsetRect.left;
    }

    element.css({
      'max-height': customPositioning ? undefined : '300px',
      top: `${pos.top}px`,
      left: `${pos.left}px`,
    });
  };

  const onResize = () => {
    position();
  };

  const onScroll = () => {
    position();
  };

  return {
    enable: () => {
      if (!enabled && !customPositioning) {
        if (includes(updateOn, 'resize')) {
          win.addEventListener('resize', onResize);
        }

        if (includes(updateOn, 'scroll')) {
          document[0].addEventListener('scroll', onScroll, true);
        }

        if (includes(updateOn, 'watch')) {
          unwatch = scope.$watch(() => {
            position();
          });
        }

        if (includes(updateOn, 'animate')) {
          // we need to wait for $animate.addClass, or else the
          // element still has display: none set which means
          // we're unable to position it correctly
          const keys = 'addClass removeClass enter remove'.split(' ');
          const handleAnimEvent = () => {
            keys.forEach((key) => {
              $animate.off(key, element, handleAnimEvent);
            });

            position();
          };

          keys.forEach((key) => {
            $animate.on(key, element, handleAnimEvent);
          });
        }

        position();
        enabled = true;
      }
    },
    disable: () => {
      if (enabled) {
        win.removeEventListener('resize', onResize);
        document[0].removeEventListener('scroll', onScroll, true);

        if (unwatch) {
          unwatch();
          unwatch = null;
        }

        enabled = false;
      }
    },
    position,
    isEnabled: () => enabled,
  };
};
