// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import './styles.scss';

module.exports = angular
  .module('zsReactIframe', [])
  .directive('zsReactIframe', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          iframeSrc: '&',
          onLocationChange: '&',
          onMessage: '&',
          caseData: '&',
          height: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this;

            const caseData = ctrl.caseData();

            ctrl.getHeight = () => ctrl.height() || '100%';

            ctrl.onMessageListener = (event) => {
              if (event.data && event.data.type === 'locationChange') {
                let iframeUrl = event.data.data;
                if (ctrl.onLocationChange) {
                  ctrl.onLocationChange({ data: iframeUrl });
                }
              }

              if (
                caseData &&
                event.data &&
                event.data.type === 'fetchCaseData'
              ) {
                const iframe = document.getElementById('react-iframe');

                iframe.contentWindow.postMessage({
                  type: 'postCaseData',
                  data: caseData,
                });
              }

              ctrl.onMessage && ctrl.onMessage({ data: event.data });
            };

            window.addEventListener('message', ctrl.onMessageListener);

            scope.$on('$destroy', () =>
              window.removeEventListener('message', ctrl.onMessageListener)
            );
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
