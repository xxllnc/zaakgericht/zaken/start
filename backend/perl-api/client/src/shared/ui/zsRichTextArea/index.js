// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import composedReducerModule from './../../api/resource/composedReducer';
import shortid from 'shortid';
import seamlessImmutable from 'seamless-immutable';
import merge from 'lodash/merge';
import isUndefined from 'lodash/isUndefined';
import omitBy from 'lodash/omitBy';
import includes from 'lodash/includes';
import without from 'lodash/without';
import template from './template.html';
import './styles.scss';

module.exports = angular
  .module('zsRichTextArea', [composedReducerModule])
  .directive('zsRichTextArea', [
    '$rootScope',
    '$sce',
    '$q',
    '$document',
    'composedReducer',
    '$timeout',
    ($rootScope, $sce, $q, $document, composedReducer, $timeout) => {
      let load = () => {
        return $q((resolve /*, reject*/) => {
          require(['quill'], (...rest) => {
            $rootScope.$evalAsync(() => resolve(rest));
          });
        });
      };

      return {
        restrict: 'E',
        template,
        scope: {
          formats: '&',
          onChange: '&',
          html: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let ctrl = this,
              editor,
              id = shortid().replace(/^\d+/, ''),
              optionReducer,
              formats = 'bold italic strike underline header link bullet list paste fullscreen'.split(
                ' '
              ),
              settings = seamlessImmutable({ pastePlain: true }),
              updateScheduled,
              loaded = false,
              loading = false,
              sanitizedHtml,
              tempWatcher;

            let scheduleUpdateOnBlur = () => {
              if (!updateScheduled) {
                updateScheduled = true;

                $document[0].activeElement.addEventListener('blur', () => {
                  updateScheduled = false;
                  ctrl.setEditorHtml(ctrl.html());
                });
              }
            };

            optionReducer = composedReducer(
              { scope },
              ctrl.formats,
              () => settings
            ).reduce((availableFormats = formats /*, currentSettings*/) => {
              let opts = [
                {
                  name: 'bold',
                  type: 'toggle',
                  label: 'Dikgedrukt',
                  icon: 'format-bold',
                  qlFormat: {
                    name: 'bold',
                    value: 'bold',
                  },
                },
                {
                  name: 'italic',
                  type: 'toggle',
                  label: 'Cursief',
                  icon: 'format-italic',
                  qlFormat: {
                    name: 'italic',
                    value: 'italic',
                  },
                },
                {
                  name: 'strike',
                  type: 'toggle',
                  label: 'Doorgestreept',
                  icon: 'format-strikethrough',
                  qlFormat: {
                    name: 'strike',
                    value: 'strike',
                  },
                },
                {
                  name: 'bullet',
                  type: 'click',
                  label: 'Lijst',
                  icon: 'format-list-bulleted',
                  qlFormat: {
                    name: 'list',
                    value: 'bullet',
                  },
                },
                {
                  name: 'list',
                  type: 'click',
                  label: 'Genummerde lijst',
                  icon: 'format-list-numbers',
                  qlFormat: {
                    name: 'list',
                    value: 'ordered',
                  },
                },
                {
                  name: 'align',
                  type: 'dropdown',
                  label: 'Uitlijning',
                  data: {
                    options: ['left', 'right', 'center', 'justify'].map(
                      (align) => {
                        let label = {
                          left: 'Links',
                          right: 'Rechts',
                          center: 'Centreren',
                          justify: 'Uitvullen',
                        }[align];

                        return {
                          name: align,
                          iconClass: `format-align-${align}`,
                          label,
                          click: () => {
                            editor.focus();

                            editor.format('align', align, 'user');
                          },
                        };
                      }
                    ),
                  },
                  icon: 'format-align-left',
                },
                {
                  name: 'header',
                  type: 'dropdown',
                  label: 'Grootte',
                  data: {
                    options: [0, 1, 2, 3, 4, 5, 6].map((heading, index) => {
                      let label = [
                        'Normaal',
                        'Kop 1',
                        'Kop 2',
                        'Kop 3',
                        'Kop 4',
                        'Kop 5',
                        'Kop 6',
                      ][index];

                      return {
                        name: heading,
                        label,
                        click: () => {
                          // editor needs focus, else range is null

                          editor.focus();

                          editor.format('header', heading, 'user');
                        },
                      };
                    }),
                  },
                  icon: 'format-size',
                },
                {
                  name: 'link',
                  type: 'toggle',
                  label: 'Link',
                  icon: 'link',
                  qlFormat: {
                    name: 'link',
                    value: 'link',
                  },
                },
                {
                  name: 'fullscreen',
                  type: 'click',
                  qlFormat: null,
                  label: settings.fullscreen
                    ? 'Normale weergave'
                    : 'Volledig scherm',
                  click: () => {
                    settings = settings.merge({
                      fullscreen: !settings.fullscreen,
                    });
                  },
                  icon: settings.fullscreen ? 'fullscreen-exit' : 'fullscreen',
                },
              ];

              return opts
                .filter(
                  (option) =>
                    !availableFormats || includes(availableFormats, option.name)
                )
                .map((option) => {
                  let opt,
                    buttonClasses,
                    value,
                    classes = {};

                  if (option.qlFormat !== undefined) {
                    if (option.qlFormat) {
                      buttonClasses = {
                        [`ql-${option.qlFormat.name}`]: true,
                      };
                      value = option.qlFormat.value;
                    }
                  } else {
                    buttonClasses = {
                      [`ql-${option.name}`]: true,
                    };
                  }

                  if (
                    !includes(
                      ['link', 'bullet', 'list', 'fullscreen', 'help'],
                      option.name
                    )
                  ) {
                    classes = omitBy(
                      merge({}, classes, { unsupported: true }),
                      isUndefined
                    );
                  }

                  opt = omitBy(
                    merge(
                      { id: shortid(), buttonClasses, value, classes },
                      option
                    ),
                    isUndefined
                  );

                  return opt;
                });
            });

            ctrl.getEditor = () => editor;

            ctrl.getEditorHtml = () => {
              let innerHtml = editor.container.children[0].innerHTML;

              // Quill sometimes sets empty tags which causes issues if you paste in text
              //   or when you clickityclick the styling buttons too much too quickly
              // Removing them solves the issue. It's not the prettiest solution,
              //   but it's fine for an angular implementation that will be refactored soon
              const tags = ['strong', 'em', 's'];
              const elements = tags.map((tag) => `<${tag}></${tag}>`);
              const regExp = new RegExp(elements.join('|', 'gi'));

              let contents = innerHtml.replace(
                '<span class="ql-cursor">﻿</span>',
                ''
              );

              tags.forEach(() => {
                contents = contents.replace(regExp, '');
              });

              // ZS-13764: Quill sets empty tags in the editor on init and when empty.
              // This clears those statements.
              // Also see issue https://github.com/quilljs/quill/issues/1235
              if (
                contents === '<p><br></p>' ||
                contents === '<p><br /></p>' ||
                contents === '<p><br/></p>'
              ) {
                return null;
              }
              return contents;
            };

            ctrl.setEditorHtml = (html) => {
              // pasteHTML will be deprecated in version 2.0
              // of Quill, so this should be refactored later
              editor.pasteHTML(html);

              if (html === null) {
                editor.setContents([{ insert: '\n' }]);
              }
            };

            ctrl.triggerChange = () => {
              ctrl.onChange({ $html: ctrl.getEditorHtml() });
            };

            ctrl.getUuid = () => id;

            ctrl.getToolbarOptions = optionReducer.data;

            ctrl.handlePasteToggle = () => {
              settings = settings.merge({ pastePlain: !settings.pastePlain });
            };

            ctrl.isFullscreen = () => !!settings.fullscreen;

            ctrl.getSanitizedHtml = () => sanitizedHtml;

            ctrl.isLoaded = () => loaded;

            ctrl.isLoading = () => loading;

            let loadEditor = () => {
              loading = true;

              load()
                .then((libs) => {
                  let [Quill] = libs;

                  let Parchment = Quill.import('parchment');

                  Quill.register(
                    new Parchment.Attributor.Style('align', 'text-align', {
                      scope: Parchment.Scope.BLOCK,
                    }),
                    true
                  );

                  editor = new Quill(`#${id}-editor`, {
                    bounds: element[0],
                    theme: 'snow',
                    formats: without(
                      ctrl.formats() ? ctrl.formats() : formats,
                      'paste',
                      'fullscreen'
                    ),
                    modules: {
                      toolbar: {
                        container: `#${id}-toolbar`,
                      },
                    },
                  });
                  // Disable TAB key by removing it from the keyboard mapping
                  let keyboard = editor.getModule('keyboard');
                  delete keyboard.bindings[9];

                  // We need to disable and re-enable the editor as IE11 doesn't like it when you click in the editor before the editor is fully loaded.
                  editor.disable();

                  $timeout(() => {
                    editor.enable();
                  }, 0);

                  // Until Quill gets better support for customizing tooltips, we must use the standard theme ('snow')
                  // as it has built in support for link tooltips. However, Snow overwrites the toolbar buttons with svg
                  // icons. The following undo's that change. It is hacky and should be refactored.
                  let buttons = editor.container.parentElement
                    .getElementsByClassName('ql-toolbar')[0]
                    .getElementsByTagName('button');

                  for (let i = 0; i < buttons.length; i++) {
                    let button = buttons[i],
                      className = button.getAttribute('class').toLowerCase();

                    if (className.indexOf('bold') > -1) {
                      button.innerHTML = '<i class="mdi mdi-format-bold"></i>';
                    } else if (className.indexOf('italic') > -1) {
                      button.innerHTML =
                        '<i class="mdi mdi-format-italic"></i>';
                    } else if (className.indexOf('underline') > -1) {
                      button.innerHTML =
                        '<i class="mdi mdi-format-underline"></i>';
                    } else if (className.indexOf('strike') > -1) {
                      button.innerHTML =
                        '<i class="mdi mdi-format-strikethrough"></i>';
                    } else if (
                      className.indexOf('list') > -1 &&
                      button.value === 'ordered'
                    ) {
                      button.innerHTML =
                        '<i class="mdi mdi-format-list-numbers"></i>';
                    } else if (
                      className.indexOf('list') > -1 &&
                      button.value === 'bullet'
                    ) {
                      button.innerHTML =
                        '<i class="mdi mdi-format-list-bulleted"></i>';
                    } else if (className.indexOf('link') > -1) {
                      button.innerHTML = '<i class="mdi mdi-link"></i>';
                    }
                  }

                  if (tempWatcher) {
                    tempWatcher();
                  }

                  scope.$watch(ctrl.html, (html) => {
                    // when there is a difference between the html that is supplied between the backend
                    // and whatever is actually *in* the editor.
                    if (ctrl.getEditorHtml() !== html) {
                      // prevent field from being updated when focused
                      if (element[0].contains($document[0].activeElement)) {
                        scheduleUpdateOnBlur();
                      } else {
                        ctrl.setEditorHtml(html);
                      }
                    }
                  });

                  editor.on('text-change', (delta, oldDelta, source) => {
                    if (source === 'user') {
                      ctrl.triggerChange();
                    }
                  });

                  let onKeyUp = (event) => {
                    if (event.keyCode === 27 && settings.fullscreen) {
                      scope.$evalAsync(() => {
                        settings = settings.merge({ fullscreen: false });
                      });
                    }
                  };

                  $document.bind('keyup', onKeyUp);
                })
                .catch((err) => {
                  console.error(err);
                })
                .finally(() => {
                  loading = false;

                  loaded = true;
                });
            };

            loadEditor();
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
