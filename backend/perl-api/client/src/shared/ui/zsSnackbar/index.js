// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import snackbarServiceModule from './snackbarService';
import composedReducerModule from './../../api/resource/composedReducer';
import zsSpinnerModule from './../zsSpinner';
import assign from 'lodash/assign';
import template from './template.html';
import './snackbar.scss';

module.exports = angular
  .module('shared.ui.zsSnackbar', [
    snackbarServiceModule,
    zsSpinnerModule,
    composedReducerModule,
  ])
  .directive('zsSnackbar', [
    '$sce',
    'composedReducer',
    'snackbarService',
    ($sce, composedReducer, snackbarService) => {
      return {
        restrict: 'E',
        template,
        scope: {},
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              expanded = false;

            ctrl.getSnacks = composedReducer(
              { scope },
              snackbarService.getSnacks
            ).reduce((snacks) => {
              let parsedSnacks = snacks
                .asMutable({ deep: true })
                .map((snack) => {
                  return assign({}, snack, {
                    message: $sce.trustAsHtml(snack.message),
                  });
                });

              return parsedSnacks;
            }).data;

            ctrl.handleCloseClick = (snack, $event) => {
              snackbarService.remove(snack);
              $event.stopPropagation();
            };

            ctrl.getSnackStyle = (snack, index) => {
              let scaleX,
                bottom,
                opacity,
                style,
                l = ctrl.getSnacks().length;

              if (!expanded) {
                scaleX = 1 - 0.01 * (l - 1 - index);
                bottom = (l - index - 1) * 5;
                opacity = (index / (l - 1)) * 0.75 + 0.25 || 1;

                style = {
                  transform: `scaleX(${scaleX})`,
                  bottom: `${bottom}px`,
                  opacity,
                };
              }

              return style;
            };

            ctrl.expand = () => {
              expanded = true;
            };

            ctrl.contract = () => {
              expanded = false;
            };

            ctrl.handleActionClick = (snack, action) => {
              if (action.click) {
                action.click();
              }
              snackbarService.remove(snack);
            };

            ctrl.isExpanded = () => expanded;
          },
        ],
        controllerAs: 'zsSnackbar',
      };
    },
  ]).name;
