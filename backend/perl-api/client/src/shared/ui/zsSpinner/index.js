// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';

module.exports = angular
  .module('shared.ui.zsSpinner', [])
  .directive('zsSpinner', [
    () => {
      return {
        restrict: 'E',
        scope: {
          isLoading: '&',
        },
        template,
      };
    },
  ]).name;
