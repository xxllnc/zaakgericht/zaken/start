// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';

import actionsModule from './../../../intern/zsIntern/zsActiveSubject/actions';
import auxiliaryRouteModule from './../../util/route/auxiliaryRoute';
import composedReducerModule from './../../api/resource/composedReducer';
import contextualActionServiceModule from './../zsContextualActionMenu/contextualActionService';
import externalSearchServiceModule from './externalSearchService';
import mutationServiceModule from './../../api/resource/mutationService';
import objectSuggestionServiceModule from './../../object/zsObjectSuggest/objectSuggestionService';
import resourceModule from './../../api/resource';
import savedSearchesServiceModule from './savedSearchesService';
import zsDropdownMenuModule from './../zsDropdownMenu';
import zsSpinnerModule from './../zsSpinner';
import zsSpotEnlighterSuggestionListModule from './zsSpotEnlighterSuggestionList';
import zsTooltipModule from './../zsTooltip';

import controller from './SpotEnlighterController';
import template from './template.html';
import './spotenlighter.scss';

// we use zsUniversalSearch to prevent naming clashes w/ legacy zsSpotEnlighter
module.exports = angular
  .module('zsUniversalSearch', [
    angularUiRouterModule,
    auxiliaryRouteModule,
    savedSearchesServiceModule,
    objectSuggestionServiceModule,
    zsSpotEnlighterSuggestionListModule,
    zsSpinnerModule,
    composedReducerModule,
    resourceModule,
    zsDropdownMenuModule,
    mutationServiceModule,
    zsTooltipModule,
    actionsModule,
    contextualActionServiceModule,
    externalSearchServiceModule,
  ])
  .component('zsUniversalSearch', {
    bindings: {
      isActive: '&',
      useLocation: '&',
      user: '&',
      onOpen: '&',
      onClose: '&',
    },
    controller,
    template,
  }).name;
