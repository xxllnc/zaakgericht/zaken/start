// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import uniqueId from 'lodash/uniqueId';

export default class SuggestionListController {
  static get $inject() {
    return ['$scope', '$element'];
  }

  constructor($scope) {
    let ctrl = this;
    let loading = false;
    let suggestions = [];
    let highlighted;
    let listId = uniqueId('suggestion-list');
    let listDescriptionId = `${listId}-description`;

    let select = (suggestion, event) => {
      ctrl.onSelect({
        $suggestion: suggestion,
        $event: event,
      });
    };

    ctrl.isLoading = () => loading;

    ctrl.getSuggestions = () => suggestions;

    ctrl.getKeyInputDelegate = ctrl.keyInputDelegate;

    ctrl.handleKeyCommit = (suggestion, event) => {
      select(suggestion, event);
    };

    ctrl.handleHighlight = (suggestion) => {
      highlighted = suggestion;
      ctrl
        .getKeyInputDelegate()
        .input.attr('aria-activedescendant', suggestion ? suggestion.id : '');
    };

    ctrl.handleSuggestionClick = (suggestion, event) => {
      select(suggestion, event);
    };

    ctrl.isHighlighted = (suggestion) => suggestion === highlighted;

    ctrl.listId = listId;

    let setAriaLabels = () => {
      const input = ctrl.getKeyInputDelegate().input;
      if (!input.attr('aria-haspopup')) {
        input.attr('aria-haspopup', true);
      }

      if (!input.attr('aria-autocomplete')) {
        input.attr('aria-autocomplete', 'list');
      }

      if (!input.attr('aria-controls')) {
        input.attr('aria-controls', listId);
      }

      if (!input.attr('aria-describedby')) {
        input.attr('aria-describedby', listDescriptionId);
      }
    };

    setAriaLabels();

    $scope.$watch(ctrl.suggestions, (result) => {
      loading = false;

      if (result && result.then && typeof result.then === 'function') {
        loading = true;

        result
          .then((data) => {
            suggestions = data;
          })
          .catch(() => {
            suggestions = [];
          })
          .finally(() => {
            loading = false;
          });
      } else {
        suggestions = result;
      }
    });
  }
}
