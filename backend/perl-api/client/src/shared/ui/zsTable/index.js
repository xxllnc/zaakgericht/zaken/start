// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import zsTableBodyModule from './zsTableBody';
import zsTableHeaderModule from './zsTableHeader';
import './tables.scss';

module.exports = angular
  .module('zsTable', [zsTableBodyModule, zsTableHeaderModule])
  .directive('zsTable', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          columns: '&',
          rows: '&',
          onColumnClick: '&',
          onRowClick: '&',
          useHref: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.handleColumnClick = (columnId) => {
              ctrl.onColumnClick({ $columnId: columnId });
            };
          },
        ],
        controllerAs: 'zsTable',
      };
    },
  ]).name;
