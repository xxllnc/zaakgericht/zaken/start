// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';

module.exports = angular
  .module('zsTableHeader', [])
  .directive('zsTableHeader', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          columns: '&',
          onColumnClick: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.handleColumnClick = (column) => {
              ctrl.onColumnClick({ $columnId: column.id });
            };

            ctrl.getSortingLabel = (column) => {
              const isSorted = Boolean(column.iconType);
              const allowsSorting = column.sort;

              let label = column.ariaLabel;

              if (isSorted) {
                const descending = column.iconType === 'chevron-down';
                const currentSort = descending ? 'Aflopend' : 'Oplopend';
                const oppositeSort = !descending ? 'aflopend' : 'oplopend';

                label += `. ${currentSort} gesorteerd. Klik om ${oppositeSort} te sorteren.`;
              } else if (allowsSorting) {
                label += '. Klik om aflopend te sorteren.';
              }

              return label;
            };
          },
        ],
        controllerAs: 'zsTableHeader',
      };
    },
  ]).name;
