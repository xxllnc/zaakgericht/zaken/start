// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import getIconFromEventType from './../../zs/eventTypes/getIconFromEventType';
import getEventGroupFromEventType from './../../zs/eventTypes/getEventGroupFromEventType';
import zsTimelineEventModule from './zsTimelineEvent';
import resourceModule from './../../api/resource';
import composedReducerModule from './../../api/resource/composedReducer';
import snackbarServiceModule from '../zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import get from 'lodash/get';
import assign from 'lodash/assign';
import isArray from 'lodash/isArray';
import getFullName from '../../util/subject/getFullName';
import template from './template.html';
import './styles.scss';

module.exports = angular
  .module('shared.ui.zsTimeline', [
    zsTimelineEventModule,
    composedReducerModule,
    resourceModule,
    snackbarServiceModule,
  ])
  .component('zsTimeline', {
    bindings: {
      title: '&',
      timelineReference: '&',
      eventTypes: '&',
      enableFilter: '&',
      maxItems: '&',
      loadMore: '&',
      noItemsMessage: '@',
    },
    controller: [
      '$scope',
      '$http',
      '$window',
      '$compile',
      '$document',
      '$element',
      '$timeout',
      'resource',
      'composedReducer',
      'snackbarService',
      'dateFilter',
      function (
        scope,
        $http,
        $window,
        $compile,
        $document,
        $element,
        $timeout,
        resource,
        composedReducer,
        snackbarService,
        dateFilter
      ) {
        let ctrl = this,
          eventResource,
          eventReducer,
          filterReducer,
          filterOptionsReducer,
          searchOpen = false,
          selectedFilterOptions = seamlessImmutable({
            case: false,
            contactmoment: false,
            note: false,
            document: false,
            object: false,
            other: false,
          }),
          defaultMaxItems = 20, // When no ctrl.maxItems prop is set, this is the number we shop by default.
          increaseStep = 20;

        ctrl.getTitle = () => ctrl.title();

        ctrl.getNoItemsFoundMessage = () => {
          return ctrl.noItemsMessage
            ? ctrl.noItemsMessage
            : 'Geen timeline items gevonden';
        };

        let createElasticsearchPartialFromEventTypes = (eventTypes) => {
          let convertedEventTypes = !isArray(eventTypes)
            ? [eventTypes]
            : eventTypes;

          return convertedEventTypes.reduce(
            (acc, cur) => `${acc}&query:match:event_type=${cur}`,
            ''
          );
        };

        let getMaxItems = () => defaultMaxItems;

        eventResource = resource(
          () => {
            let eventTypePartial = ctrl.eventTypes()
                ? createElasticsearchPartialFromEventTypes(ctrl.eventTypes())
                : '',
              searchPartial =
                ctrl.searchQuery !== undefined
                  ? `&query:match:keyword=${ctrl.searchQuery}`
                  : '';

            return {
              url: `/api/v1/eventlog?query:match:timeline=${ctrl.timelineReference()}${eventTypePartial}${searchPartial}`,
              params: {
                rows_per_page: ctrl.maxItems()
                  ? ctrl.maxItems()
                  : getMaxItems(),
              },
            };
          },
          { scope }
        ).reduce((requestOptions, data) => {
          return data ? data : seamlessImmutable([]);
        });

        eventReducer = composedReducer({ scope }, eventResource).reduce(
          (events) => {
            return events
              ? events.map((event) => {
                  let iconStyle = getIconFromEventType(
                    get(event, 'instance.event_type')
                  );

                  return {
                    id: get(event, 'instance.event_id'),
                    type: get(event, 'instance.event_type'),
                    icon: iconStyle.icon,
                    color: iconStyle.color,
                    date: dateFilter(
                      get(event, 'instance.date_created'),
                      'dd-MM-yyyy H:mm'
                    ),
                    subject: getFullName(event.instance.created_by),
                    channel: get(event, 'instance.event_data.contact_channel')
                      ? `Contactmoment via ${event.instance.event_data.contact_channel}`
                      : 'Contactmoment',
                    link: `/intern/contact/${get(
                      event,
                      'instance.created_by.instance.subject.reference'
                    )}`,
                    title: get(event, 'instance.event_data.human_readable'),
                    content: get(event, 'instance.event_data.content'),
                  };
                })
              : [];
          }
        );

        filterReducer = composedReducer(
          { scope },
          eventReducer,
          () => selectedFilterOptions
        ).reduce((events, filters) => {
          let keys = Object.keys(filters),
            activeKeys = keys.filter((key) => filters[key]);

          return activeKeys.length
            ? events.filter((event) => {
                return activeKeys.some(
                  (val) =>
                    val.indexOf(getEventGroupFromEventType(event.type)) !== -1
                );
              })
            : events;
        });

        ctrl.getEvents = filterReducer.data;

        filterReducer.subscribe((items) => {
          if (items && items.length) {
            $element.removeClass('empty');
          } else {
            $element.addClass('empty');
          }
        });

        ctrl.isLoading = () => eventReducer.state() === 'pending';

        ctrl.isFilterEnabled = () => ctrl.enableFilter();

        ctrl.toggleSearch = () => {
          searchOpen = !searchOpen;

          if (searchOpen) {
            $timeout(
              () => {
                $element.find('input')[0].focus();
              },
              0,
              false
            );
          } else {
            ctrl.searchQuery = '';
          }
        };

        ctrl.handleSearchKeyUp = (event) => {
          switch (event.keyCode) {
            case 27:
              searchOpen = false;
              ctrl.searchQuery = '';
              break;
          }
        };

        ctrl.isSearchOpen = () => searchOpen;

        filterOptionsReducer = composedReducer(
          { scope },
          () => selectedFilterOptions
        ).reduce((selected) => {
          return [
            {
              name: 'case',
              label: 'Zaken',
            },
            {
              name: 'contactmoment',
              label: 'Contactmomenten',
            },
            {
              name: 'note',
              label: 'Notities',
            },
            {
              name: 'document',
              label: 'Documenten',
            },
            {
              name: 'object',
              label: 'Objecten',
            },
            {
              name: 'other',
              label: 'Overig',
            },
          ].map((item) => {
            return assign({}, item, {
              iconClass: selected[item.name]
                ? 'checkbox-marked'
                : 'checkbox-blank-outline',
              click: () => {
                ctrl.selectFilter(item.name);
              },
            });
          });
        });

        ctrl.getFilterOptions = filterOptionsReducer.data;

        ctrl.selectFilter = (filter) => {
          let value = get(selectedFilterOptions, filter, false);

          selectedFilterOptions = selectedFilterOptions.merge({
            [filter]: !value,
          });
        };

        ctrl.handleSearchQuery = () => eventResource.reload();

        let increaseMaxItems = () => {
          defaultMaxItems = defaultMaxItems + increaseStep;
        };

        ctrl.loadMore = () => {
          increaseMaxItems();
          eventResource.reload();
        };

        ctrl.isMoreAvailable = () =>
          !!(eventResource.totalRows() > defaultMaxItems);

        ctrl.downloadEventLog = () => {
          let URL,
            blobUrl,
            anchor,
            eventTypePartial = ctrl.eventTypes()
              ? createElasticsearchPartialFromEventTypes(ctrl.eventTypes())
              : '',
            searchPartial =
              ctrl.searchQuery !== undefined
                ? `&query:match:keyword=${ctrl.searchQuery}`
                : '';

          event.preventDefault();

          snackbarService.wait('Het logboek wordt gedownload', {
            promise: $http({
              url: `/api/v1/eventlog/download?query:match:timeline=${ctrl.timelineReference()}${eventTypePartial}${searchPartial}`,
              responseType: 'blob', // We use 'blob' because we know this approach to work on iOS devices
            }).then((response) => {
              let now = new Date(),
                days = now.getDate() < 10 ? `0${now.getDate()}` : now.getDate(),
                months =
                  now.getMonth() + 1 < 10
                    ? `0${now.getMonth() + 1}`
                    : now.getMonth() + 1,
                years = now.getFullYear(),
                hours = now.getHours(),
                minutes = now.getMinutes(),
                dateString = `${days}-${months}-${years}_${hours}h${minutes}m`;

              URL = 'URL' in $window ? $window.URL : $window.webkitURL;

              blobUrl = URL.createObjectURL(response.data, {
                type: 'text/bin',
              });
              anchor = angular.element('<a></a>');

              anchor.attr('href', blobUrl);
              anchor.attr(
                'download',
                `zs_logboek_${dateString}_${ctrl.timelineReference()}`
              );

              $document.find('body').append(anchor);

              anchor[0].click();

              $timeout(() => {
                anchor.remove();
                URL.revokeObjectURL(blobUrl);
              }, 300);
            }),
            then: () => '',
            catch: () =>
              'Het logboek kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.',
          });
        };

        scope.$on('$destroy', () => {
          [eventResource].forEach((res) => {
            res.destroy();
          });
        });
      },
    ],
    template,
  }).name;
