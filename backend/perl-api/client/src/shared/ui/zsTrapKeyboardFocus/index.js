// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import uniqueId from 'lodash/uniqueId';
import findIndex from 'lodash/findIndex';

const TAB = 9;

module.exports = angular
  .module('zsTrapKeyboardFocus', [])
  .directive('zsTrapKeyboardFocus', [
    '$document',
    ($document) => {
      let subscribers = [];

      const addSubscriber = (id, fn) => {
        subscribers.push({ id, fn });
      };

      const removeSubscriber = (id) => {
        const index = findIndex(subscribers, (item) => item.id === id);
        if (index > -1) {
          subscribers.splice(index, 1);
        }
      };

      const notifyLastSubscriber = (event) => {
        if (event.keyCode === TAB && subscribers.length > 0) {
          subscribers[subscribers.length - 1].fn(event);
        }
      };

      $document.bind('keydown', notifyLastSubscriber);

      return {
        restrict: 'A',
        link(scope, $element, attr) {
          const element = $element[0];
          const id = uniqueId('trap-keyboard-focus-');
          const lastFocusableElement = (elements) =>
            elements[elements.length - 1];

          const firstFocusableElement = (elements) => elements[0];

          const handleForwardTab = (event, elements) => {
            if (
              document.activeElement === lastFocusableElement(elements) ||
              !element.contains(document.activeElement)
            ) {
              event.preventDefault();
              firstFocusableElement(elements).focus();
            }
          };

          const handleBackwardTab = (event, elements) => {
            if (
              document.activeElement === firstFocusableElement(elements) ||
              !element.contains(document.activeElement)
            ) {
              event.preventDefault();
              lastFocusableElement(elements).focus();
            }
          };

          const getFocusableChildren = () => {
            const focusableElements = element.querySelectorAll(
              'a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled]), [tabindex]:not([tabindex="-1"])'
            );

            return Array.prototype.slice
              .call(focusableElements)
              .filter((element) => element.offsetWidth > 0);
          };

          const keyDownHandler = (event) => {
            const focusableElements = getFocusableChildren();

            if (focusableElements.length > 0) {
              return event.shiftKey
                ? handleBackwardTab(event, focusableElements)
                : handleForwardTab(event, focusableElements);
            }
          };

          scope.$watch(attr.zsTrapKeyboardFocus, (enabled) => {
            return enabled
              ? addSubscriber(id, keyDownHandler)
              : removeSubscriber(id);
          });

          scope.$on('$destroy', () => {
            removeSubscriber(id);
          });
        },
      };
    },
  ]).name;
