// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import propCheck from './../../util/propCheck';
import mutationServiceModule from './../../api/resource/mutationService';
import each from 'lodash/each';

module.exports = angular
  .module('userSettingsActions', [mutationServiceModule])
  .factory('userSettingsActions', [
    () => {
      let actions = [
        {
          type: 'set_setting',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                key: propCheck.string,
                value: propCheck.any,
              }),
              mutationData
            );

            let { key, value } = mutationData;

            return {
              url: '/api/user/settings',
              data: {
                [key]: value,
              },
            };
          },
          reduce: (data, mutationData) => {
            return data.map((item) => {
              return item.merge(
                {
                  [mutationData.key]: mutationData.value,
                },
                { deep: true }
              );
            });
          },
        },
      ];

      return actions;
    },
  ])
  .run([
    'mutationService',
    'userSettingsActions',
    (mutationService, userSettingsActions) => {
      each(userSettingsActions, (action) => {
        mutationService.register(action);
      });
    },
  ]).name;
