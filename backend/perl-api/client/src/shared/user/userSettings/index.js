// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from './../../api/resource';
import actionsModule from './actions';
import get from 'lodash/get';
import toPath from 'lodash/toPath';
import set from 'lodash/set';
import first from 'lodash/head';
import assign from 'lodash/assign';

module.exports = angular
  .module('userSettings', [resourceModule, actionsModule])
  .factory('userSettings', [
    '$rootScope',
    'resource',
    ($rootScope, resource) => {
      let createResource = (scope) => {
        return resource('/api/user/settings', {
          scope,
          cache: {
            // only fetch once every five minutes
            every: 1000 * 60 * 5,
          },
        });
      };

      let settings = createResource($rootScope);

      return {
        set: (key, value) => {
          let path = toPath(key),
            data = assign({}, first(settings.data()), set({}, key, value)),
            actualKey = first(path),
            actualValue = get(data, actualKey);

          settings.mutate('set_setting', {
            key: actualKey,
            value: actualValue,
          });
        },
        get: (key) => {
          return get(settings.data(), `[0]${key}`);
        },
        createResource,
      };
    },
  ]).name;
