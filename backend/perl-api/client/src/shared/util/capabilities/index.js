// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

module.exports = angular.module('capabilities', []).factory('capabilities', [
  '$window',
  ($window) => {
    let capabilities = {
      touch: !!('ontouchstart' in $window),
    };

    return () => capabilities;
  },
]).name;
