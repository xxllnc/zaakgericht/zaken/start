// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import propCheck from './../propCheck';
import get from 'lodash/get';

module.exports = angular.module('newRelic', []).factory('newRelic', [
  '$window',
  ($window) => {
    return {
      log: (name, data) => {
        let interaction;

        propCheck.throw(
          propCheck.shape({
            name: propCheck.string,
            data: propCheck.object.optional,
          }),
          {
            name,
            data,
          }
        );

        if (typeof get($window, 'newrelic.interaction') !== 'function') {
          return;
        }

        interaction = $window.newrelic.interaction().setName(name);

        for (let key in data) {
          interaction.setAttribute(key, escape(data[key]));
        }

        interaction.save();

        interaction.end();
      },
    };
  },
]).name;
