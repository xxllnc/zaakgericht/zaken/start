// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const pathExpression = /^\/[^/]/;

/**
 * Rudimentary origin test for client-side API calls.
 * This does not actually compare origins but probes for its absence.
 *
 * @param {strong} url
 *   Absolute path to API endpoint under the same origin
 * @return {boolean}
 */
export const hasSameOrigin = (url) => pathExpression.test(url);
