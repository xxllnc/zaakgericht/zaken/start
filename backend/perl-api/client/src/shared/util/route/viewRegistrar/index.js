// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import getViewName from './../getViewName';
import without from 'lodash/without';
import findLast from 'lodash/findLast';

module.exports = angular
  .module('viewRegistrar', [])
  .factory('viewRegistrar', [
    () => {
      // sometimes a new view with the same name is loaded
      // before the other one is destroyed,
      // so we need to remove it before the new one is registered

      let registrations = [];

      return {
        addView: (name, element) => {
          let registration = { name, element };

          registrations = registrations.concat(registration);

          return () => {
            registrations = without(registrations, registration);
          };
        },
        getView: (name) => {
          let registration = findLast(registrations, { name });

          return registration ? registration.element : null;
        },
      };
    },
  ])
  .directive('uiView', [
    '$interpolate',
    'viewRegistrar',
    ($interpolate, viewRegistrar) => {
      return (scope, element, attrs) => {
        let name = getViewName($interpolate, scope, element, attrs),
          removeFn;

        removeFn = viewRegistrar.addView(name.replace('@', ''), element);

        scope.$on('$destroy', removeFn);
      };
    },
  ]).name;
