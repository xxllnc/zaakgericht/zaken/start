// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsFileUploadModule from './../../../ui/zsFileUpload';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormFileEditModule from './vormFileEdit';
import vormFileDisplayModule from './vormFileDisplay';

module.exports = angular
  .module('vorm.types.file', [
    vormTemplateServiceModule,
    zsFileUploadModule,
    vormFileEditModule,
    vormFileDisplayModule,
  ])
  .run([
    'vormTemplateService',
    function (vormTemplateService) {
      vormTemplateService.registerType('file', {
        control: angular.element(
          `<vorm-file-edit
									ng-model
								></vorm-file-edit>`
        ),
        display: angular.element(
          `<vorm-file-display
									data-file="delegate.value"
									data-formatter="vm.templateData().display"
								></vorm-file-display>`
        ),
        defaults: {
          editMode: 'empty',
        },
      });
    },
  ]).name;
