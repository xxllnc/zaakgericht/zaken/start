// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import zsFileUploadModule from './../../../../ui/zsFileUpload';
import vormInvokeModule from './../../../vormInvoke';
import snackbarServiceModule from './../../../../ui/zsSnackbar/snackbarService';
import get from 'lodash/get';

module.exports = angular
  .module('vormFileEdit', [
    zsFileUploadModule,
    vormInvokeModule,
    snackbarServiceModule,
  ])
  .directive('vormFileEdit', [
    'snackbarService',
    'vormInvoke',
    (snackbarService, vormInvoke) => {
      return {
        restrict: 'E',
        template,
        require: ['vormFileEdit', 'ngModel'],
        scope: {
          delegate: '&',
          templateData: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              ngModel;

            ctrl.link = (...rest) => {
              [ngModel] = rest;

              ngModel.$isEmpty = () => !ngModel.$modelValue;
            };

            ctrl.handleFileComplete = (file, data) => {
              let reducer = get(ctrl.templateData(), 'transform'),
                obj;

              if (!reducer) {
                reducer = () => ({ filename: file.name });
              }

              obj = vormInvoke(reducer, {
                $file: file,
                $data: data,
              });

              ngModel.$setViewValue(obj, 'click');
            };

            ctrl.hasFile = () => !ngModel.$isEmpty();

            ctrl.invokeData = (key, locals) => {
              let result = vormInvoke(get(ctrl.templateData(), key), locals);

              return result;
            };

            ctrl.handleError = (error) => {
              let message = get(
                error,
                'data.result[0].messages[0]',
                'Bestand kon niet worden geuploadet. Neem contact op met uw beheerder voor meer informatie.'
              );

              snackbarService.error(message);
            };
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(...controllers);
        },
      };
    },
  ]).name;
