// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import './styles.scss';

module.exports = angular
  .module('vorm.types.title', [vormTemplateServiceModule])
  .directive('vormTitle', [
    () => {
      return {
        template: `<h2 ng-click="vm.toggle()" ng-class="{'active': vm.handleClick()}">
          {{vm.label()}}
          <zs-icon icon-type="menu-down"></zs-icon>
        </h2>`,
        scope: {
          label: '&',
        },
        require: ['vormTitle', 'ngModel'],
        bindToController: true,
        controller: [
          '$scope',
          function () {
            let ctrl = this,
              clicked = false,
              ngModel;

            ctrl.link = (...rest) => {
              [ngModel] = rest;
            };

            ctrl.toggle = () => {
              ngModel.$setViewValue(!ngModel.$modelValue);
              clicked = !clicked;
            };

            ctrl.handleClick = () => {
              return clicked;
            };
          },
        ],
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(...controllers);
        },
        controllerAs: 'vm',
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('title', {
        control: angular.element(
          `<vorm-title
            ng-model
            label="vm.label"
          ></vorm-title>`
        ),
      });
    },
  ]).name;
