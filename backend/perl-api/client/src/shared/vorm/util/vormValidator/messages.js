// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  email: 'Er is geen geldig e-mailadres ingevuld (Voorbeeld: naam@example.com)',
  number:
    'Er is geen valide getal ingevuld. Alleen cijfers zijn toegestaan, tot een maximum van 18. (Voorbeeld: 12345678)',
  valuta: 'Er is geen bedrag ingevuld. (Voorbeeld: 123,45)',
  url:
    'Er is geen valide web adres ingevuld. (Voorbeeld: http://www.voorbeeld.nl of www.voorbeeld.nl)',
  bankaccount:
    'Er is geen geldig IBAN-rekeningnummer ingevuld (Voorbeeld: NL02ABNA0123456789)',
  minDate: [
    '$minDate',
    ($minDate) => `De eerst mogelijke datum is ${$minDate}.`,
  ],
  maxDate: [
    '$maxDate',
    ($maxDate) => `De laatst mogelijke datum is ${$maxDate}.`,
  ],
  required: 'Dit veld is verplicht.',
  requiredOrInvalid: 'Dit veld is verplicht of onjuist ingevuld.',
};
