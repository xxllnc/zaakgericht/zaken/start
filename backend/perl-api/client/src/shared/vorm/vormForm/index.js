// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormFieldsetModule from './../vormFieldset';
import vormValidatorModule from './../util/vormValidator';
import vormInvokeModule from './../vormInvoke';
import seamlessImmutable from 'seamless-immutable';
import template from './template.html';

module.exports = angular
  .module('vormForm', [
    vormFieldsetModule,
    vormValidatorModule,
    vormInvokeModule,
  ])
  .directive('vormForm', [
    '$q',
    'vormValidator',
    'vormInvoke',
    ($q, vormValidator, vormInvoke) => {
      return {
        restrict: 'E',
        template,
        scope: {
          defaults: '&',
          fields: '&',
          onChange: '&',
          actions: '&',
          compiler: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              values = seamlessImmutable({}).merge(ctrl.defaults() || {}),
              validity,
              submitting = false;

            let setValidity = () => {
              validity = vormValidator(ctrl.fields(), values);
            };

            ctrl.getValues = () => values;

            ctrl.handleChange = (name, value) => {
              $q.when(
                ctrl.onChange({
                  $name: name,
                  $value: value,
                  $values: values.merge({ [name]: value }),
                })
              ).then((vals) => {
                if (vals !== undefined) {
                  values = vals;
                } else {
                  values = values.merge({ [name]: value });
                }

                setValidity();
              });
            };

            ctrl.isButtonDisabled = () => !!validity.valid || submitting;

            ctrl.handleActionClick = (action, event) => {
              submitting = true;

              $q.when(
                vormInvoke(action.click, { $values: values, $event: event })
              ).finally(() => {
                submitting = false;
              });
            };

            setValidity();
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
