// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsIconModule from './../../ui/zsIcon';
import compiler from './compiler';

module.exports = angular
  .module('vorm.vormTemplateService', [zsIconModule])
  .factory('vormTemplateService', [
    '$interpolate',
    '$compile',
    ($interpolate, $compile) => {
      return compiler($compile, $interpolate);
    },
  ]).name;
