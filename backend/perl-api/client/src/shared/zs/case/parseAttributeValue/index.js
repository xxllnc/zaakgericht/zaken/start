// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import map from 'lodash/map';
import identity from 'lodash/identity';
import { stringToNumber } from '../../../util/number';

export default function parseAttributeValues(attribute, source) {
  let value = source;

  if (value === null) {
    return value;
  }

  switch (attribute.type) {
    case 'bag_adres':
    case 'bag_straat_adres':
    case 'bag_openbareruimte':
      return value ? get(value, 'bag_id') : null;
    case 'bag_adressen':
    case 'bag_straat_adressen':
    case 'bag_openbareruimtes':
      return map(value, (val) => {
        return val ? get(val, 'bag_id') : null;
      }).filter(identity);
    case 'date':
      value = value || value === 0 ? new Date(value) : null;

      if (!value || isNaN(value.getTime())) {
        return null;
      }

      return `${value.getDate()}-${
        value.getMonth() + 1
      }-${value.getFullYear()}`;
    case 'numeric':
      if (!Array.isArray(value)) {
        value = [value];
      }

      return value;
    case 'valuta':
    case 'valutaex':
    case 'valutaex21':
    case 'valutaex6':
    case 'valutain':
    case 'valutain21':
    case 'valutain6':
      return stringToNumber(value);
    case 'relationship': {
      const mapValue = (val) => ({
        type: 'relationship',
        value: val.id,
        specifics: {
          metadata: {
            summary: val.label,
            description: val.description,
          },
          relationship_type: val.type,
        },
      });

      if (attribute.multiple_values) {
        if (!value[0]) return null;

        return {
          type: 'relationship',
          value: value.map(mapValue),
          specifics: null,
        };
      }

      return mapValue(value);
    }
    default:
      return value;
  }
}
