// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: [
    { holiday: 'goodfriday', institutionTypes: ['government'] },
    { holiday: 'easter' },
    { holiday: 'easter2' },
    { holiday: 'ascension' },
    { holiday: 'pentecost' },
    { holiday: 'pentecost2' },
  ],
};
