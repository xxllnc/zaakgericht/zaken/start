// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const getSpecialHolidays = (definition, year) =>
  definition.specialHolidays.reduce((acc, holidayFunction) => {
    const holidays = holidayFunction(year);

    return [...acc, ...holidays];
  }, []);

const getHolidaysOfYear = (definition, year, institutionType) => {
  const standardHolidays = definition.holidays;
  const specialHolidays = getSpecialHolidays(definition, year);
  const holidays = [...standardHolidays, ...specialHolidays];

  return holidays.reduce((acc, holiday) => {
    const {
      day,
      month,
      yearStart = 0,
      yearEnd = 9999,
      // 2, 7, 12, 17... has interval 5 and offset 2
      interval = 1,
      offset = 0,
      institutionTypes,
    } = holiday;

    const isWithinRange = year >= yearStart && year < yearEnd;

    const yearWithOffset = year + offset;
    const isOnInterval = yearWithOffset % interval === 0;

    const isCorrectType =
      !institutionTypes || institutionTypes.includes(institutionType);

    if (isWithinRange && isOnInterval && isCorrectType) {
      const date = `${year}-${month}-${day}`;

      return [...acc, date];
    } else {
      return acc;
    }
  }, []);
};

// for example:
// with baseDateYear = 2000 and workDaysPerYear = 260
// and we want to know the 100th workday from a date X,
// we need to only know which holidays there are in 2000 and 2001
// so for each year we generate:
//   - the standard holidays (always the same date)
//   - the special holidays (different date every year)
export const generateHolidays = (
  baseDate,
  workDays,
  workDaysAction,
  definition,
  institutionType
) => {
  const weekend = definition.weekend;
  const workDaysPerYear = 52 * (7 - weekend.length);
  const maxYearsNeeded = Math.ceil(workDays / workDaysPerYear) + 1;
  const baseDateYear = baseDate.getFullYear();

  let holidays = [];

  const startingYear =
    workDaysAction === 'add' ? baseDateYear : baseDateYear - maxYearsNeeded + 1;
  const endYear =
    workDaysAction === 'add' ? baseDateYear + maxYearsNeeded : baseDateYear + 1;

  for (let year = startingYear; year < endYear; year++) {
    holidays = [
      ...holidays,
      ...getHolidaysOfYear(definition, year, institutionType),
    ];
  }

  return holidays;
};
