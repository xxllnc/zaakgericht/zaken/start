// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import angular from 'angular';
import calculateDate from '.';

module.exports = angular
  .module('zsCalculateDate', [])
  .factory('zsCalculateDate', [() => calculateDate]).name;
