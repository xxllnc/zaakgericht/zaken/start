// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import matchCondition from './matchCondition';
import some from 'lodash/some';
import every from 'lodash/every';

export default (rule, values, hidden) => {
  let conditions = rule.conditions.conditions,
    type = rule.conditions.type,
    func = type === 'and' ? every : some,
    matches = false;

  if (conditions.length === 0) {
    matches = true;
  } else {
    matches = func(conditions, (condition) =>
      matchCondition(condition, values || {}, hidden || {})
    );
  }

  return matches;
};
