// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isArray from 'lodash/isArray';
import mapValues from 'lodash/mapValues';
import emailRegex from 'email-regex';
import isEmpty from './../../../vorm/util/isEmpty';
import isValidUrl from './../../../vorm/util/isValidUrl';
import { isValidCurrency } from './../../../vorm/util/isValidCurrency';
import { isValidNumeric } from './../../../vorm/util/isValidNumeric';
import iban from 'iban';

export default (attributes, values, messages = {}) => {
  return mapValues(attributes, (attr) => {
    let required = attr.required;
    let type = attr.type;
    let value =
      attr.type === 'custom_object'
        ? values[`custom_object.${attr.label.toLowerCase()}`]
        : values[attr.magic_string];

    if (!isArray(value)) {
      value = [value];
    } else if (value.length === 0) {
      value = [null];
    }

    return value.map((val, index) => {
      let empty = isEmpty(val),
        validation = {};

      if (empty && required && index === 0) {
        validation.required = messages.required || true;
      }

      switch (type) {
        case 'bankaccount':
          if (!empty && !iban.isValid(val)) {
            validation.bankaccount = messages.bankaccount || true;
          }
          break;

        case 'email':
          if (!empty && !emailRegex({ exact: true }).test(val)) {
            validation.email = messages.email || true;
          }
          break;

        case 'numeric':
          if (!empty && !isValidNumeric(val)) {
            validation.number = messages.number || true;
          }
          break;

        case 'valuta':
        case 'valutaex':
        case 'valutaex21':
        case 'valutaex6':
        case 'valutain':
        case 'valutain21':
        case 'valutain6':
          if (!empty && !isValidCurrency(val)) {
            validation.valuta = messages.valuta || true;
          }
          break;

        case 'url':
          if (!empty && !isValidUrl(val)) {
            validation.url = messages.url || true;
          }
          break;
      }

      return Object.keys(validation).length > 0 ? validation : null;
    });
  });
};
