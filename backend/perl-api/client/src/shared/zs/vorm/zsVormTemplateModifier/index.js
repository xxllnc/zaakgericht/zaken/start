// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import './vorm.scss';
import vormTemplateServiceModule from './../../../vorm/vormTemplateService';

module.exports = angular
  .module('zsVormTemplateModifier', [vormTemplateServiceModule])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.modifyWrapper((el) => {
        let wrapper = angular.element('<div></div>'),
          metaWrapper,
          controlWrapper,
          label,
          description,
          controlList,
          addButton;

        wrapper.append(el);

        metaWrapper = angular.element(
          `<div class="form-field-meta" ng-if="!vm.hideLabel()">
						</div>`
        );

        controlWrapper = angular.element(
          `<div class="form-field-control-wrapper">
						</div>`
        );

        label = wrapper.find('label').eq(0);

        // TODO: remove HTML support as soon as customers are able to
        // update their libraries to remove HTML from titles etc

        // Functionality to display HTML in a safer way is now implemented
        // as part of Epic ZS-13868. HTML support here can be removed in July 2017.

        label.html('');

        label.append(
          `<span id="{{vm.getInputId()}}-label" ng-bind-html="vm.label">
						</span>`
        );

        label.append(
          `<span
							class="form-field-required"
							ng-show="vm.required()&&vm.label"
							zs-tooltip="Dit veld is verplicht"
							zs-tooltip-options="{ attachment: 'left middle', target: 'right middle', offset: { x: 5, y: 0 } }"
						>*</span>`
        );

        label.append(
          `<zs-icon
							icon-type="share-variant"
							ng-show="vm.pip()"
							zs-tooltip="Dit veld wordt getoond op de PIP"
							zs-tooltip-options="{ attachment: 'left middle', target: 'right middle', offset: { x: 5, y: 0 } }"
							class="form-field-pip"
						></zs-icon>`
        );

        description = angular.element(
          `<div
							class="form-field-description"
							ng-show="vm.description"
							tabindex="0"
							zs-tooltip="{{vm.description}}"
							zs-tooltip-options="{ attachment: 'left middle', target: 'right middle', offset: { x: 0, y: 0 }, sticky: true }"
						>
							<zs-icon icon-type="help"></zs-icon>
						</div>`
        );

        addButton = angular.element(
          wrapper[0].querySelector('.vorm-field-add-button')
        );

        controlList = wrapper.find('ul').eq(0);

        metaWrapper.append(description);
        metaWrapper.append(label);

        wrapper.append(metaWrapper);

        controlWrapper.append(controlList);
        controlWrapper.append(addButton);

        wrapper.append(controlWrapper);

        label.addClass('form-field-label');

        wrapper.find('ul').eq(0).addClass('form-field-value-list');

        return wrapper.children();
      });
    },
  ]).name;
