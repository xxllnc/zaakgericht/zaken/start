BEGIN;

ALTER TABLE transaction ADD COLUMN processed BOOLEAN default 'f';

COMMIT;
