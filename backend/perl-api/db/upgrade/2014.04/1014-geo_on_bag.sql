BEGIN;

ALTER TABLE bag_nummeraanduiding ADD COLUMN gps_lat_lon point;
ALTER TABLE bag_openbareruimte ADD COLUMN gps_lat_lon point;

COMMIT;