BEGIN;

ALTER TABLE bibliotheek_kenmerken DROP CONSTRAINT bibliotheek_kenmerken_value_type_check;
ALTER TABLE bibliotheek_kenmerken ADD CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK (value_type IN ('text_uc', 'checkbox', 'richtext', 'date', 'file', 'bag_straat_adres', 'email', 'valutaex', 'bag_openbareruimte', 'text', 'bag_openbareruimtes', 'url', 'valuta', 'option', 'bag_adres', 'select', 'valutain6', 'valutaex6', 'valutaex21', 'image_from_url', 'bag_adressen', 'valutain', 'calendar', 'calendar_supersaas', 'bag_straat_adressen', 'googlemaps', 'numeric', 'valutain21', 'textarea', 'bankaccount', 'subject'));

COMMIT;
