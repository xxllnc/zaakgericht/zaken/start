BEGIN;

ALTER TABLE bedrijf ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();
ALTER TABLE natuurlijk_persoon ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();

COMMIT;
