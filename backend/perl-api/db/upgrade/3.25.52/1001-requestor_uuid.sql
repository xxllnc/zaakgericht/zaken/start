BEGIN;

WITH zaak_aanvragers AS (
    SELECT zaak.id zaak_id, bedrijf.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.aanvrager = zaak_betrokkenen.id)
    INNER JOIN bedrijf ON (bedrijf.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'bedrijf'
        AND bedrijf.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('requestor', (
        SELECT subject_uuid::text
        FROM zaak_aanvragers
        WHERE object_data.object_id = zaak_aanvragers.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_aanvragers
        );

WITH zaak_coordinators AS (
    SELECT zaak.id zaak_id, bedrijf.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.coordinator = zaak_betrokkenen.id)
    INNER JOIN bedrijf ON (bedrijf.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'bedrijf'
        AND bedrijf.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('coordinator', (
        SELECT subject_uuid::text
        FROM zaak_coordinators
        WHERE object_data.object_id = zaak_coordinators.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_coordinators
        );

WITH zaak_assignees AS (
    SELECT zaak.id zaak_id, bedrijf.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.behandelaar = zaak_betrokkenen.id)
    INNER JOIN bedrijf ON (bedrijf.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'bedrijf'
        AND bedrijf.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('assignee', (
        SELECT subject_uuid::text
        FROM zaak_assignees
        WHERE object_data.object_id = zaak_assignees.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_assignees
        );

WITH zaak_aanvragers AS (
    SELECT zaak.id zaak_id, subject.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.aanvrager = zaak_betrokkenen.id)
    INNER JOIN subject ON (subject.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'medewerker'
        AND subject.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('requestor', (
        SELECT subject_uuid::text
        FROM zaak_aanvragers
        WHERE object_data.object_id = zaak_aanvragers.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_aanvragers
        );

WITH zaak_coordinators AS (
    SELECT zaak.id zaak_id, subject.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.coordinator = zaak_betrokkenen.id)
    INNER JOIN subject ON (subject.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'medewerker'
        AND subject.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('coordinator', (
        SELECT subject_uuid::text
        FROM zaak_coordinators
        WHERE object_data.object_id = zaak_coordinators.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_coordinators
        );

WITH zaak_assignees AS (
    SELECT zaak.id zaak_id, subject.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.behandelaar = zaak_betrokkenen.id)
    INNER JOIN subject ON (subject.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'medewerker'
        AND subject.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('assignee', (
        SELECT subject_uuid::text
        FROM zaak_assignees
        WHERE object_data.object_id = zaak_assignees.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_assignees
        );

WITH zaak_aanvragers AS (
    SELECT zaak.id zaak_id, natuurlijk_persoon.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.aanvrager = zaak_betrokkenen.id)
    INNER JOIN natuurlijk_persoon ON (natuurlijk_persoon.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'natuurlijk_persoon'
        AND natuurlijk_persoon.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('requestor', (
        SELECT subject_uuid::text
        FROM zaak_aanvragers
        WHERE object_data.object_id = zaak_aanvragers.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_aanvragers
        );

WITH zaak_coordinators AS (
    SELECT zaak.id zaak_id, natuurlijk_persoon.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.coordinator = zaak_betrokkenen.id)
    INNER JOIN natuurlijk_persoon ON (natuurlijk_persoon.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'natuurlijk_persoon'
        AND natuurlijk_persoon.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('coordinator', (
        SELECT subject_uuid::text
        FROM zaak_coordinators
        WHERE object_data.object_id = zaak_coordinators.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_coordinators
        );

WITH zaak_assignees AS (
    SELECT zaak.id zaak_id, natuurlijk_persoon.uuid subject_uuid
    FROM zaak
    INNER JOIN zaak_betrokkenen ON (zaak.behandelaar = zaak_betrokkenen.id)
    INNER JOIN natuurlijk_persoon ON (natuurlijk_persoon.id = zaak_betrokkenen.gegevens_magazijn_id)
    WHERE zaak_betrokkenen.betrokkene_type = 'natuurlijk_persoon'
        AND natuurlijk_persoon.uuid IS NOT NULL
) UPDATE object_data
    SET index_hstore = index_hstore || hstore('assignee', (
        SELECT subject_uuid::text
        FROM zaak_assignees
        WHERE object_data.object_id = zaak_assignees.zaak_id
    ))
    WHERE object_class = 'case'
        AND object_id IN (
            SELECT zaak_id FROM zaak_assignees
        );

COMMIT;
