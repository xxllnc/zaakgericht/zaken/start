BEGIN;

/* Migrate date_fromcurrentdate by overriding properties with a known date limit */
UPDATE zaaktype_kenmerken SET properties = '{"date_limit":{"start":{"num":"0","during":"post","reference":"current","active":"1","term":"days"},"end":{"during":"post","reference":"current","num":"","term":"days"}}}' WHERE bibliotheek_kenmerken_id IN (SELECT id FROM bibliotheek_kenmerken WHERE value_type = 'date') AND date_fromcurrentdate = 1 AND zaak_status_id IN (SELECT id FROM zaaktype_status WHERE status = 1);

COMMIT;
