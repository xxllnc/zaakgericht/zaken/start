BEGIN;

    ALTER TABLE zaak DROP CONSTRAINT IF EXISTS zaak_object_data_uuid_fkey;
    ALTER TABLE zaak ADD CONSTRAINT zaak_object_data_uuid_fkey
        FOREIGN KEY (uuid)
        REFERENCES object_data(uuid)
        ON DELETE SET NULL;

COMMIT;
