BEGIN;

    UPDATE bibliotheek_notificaties bn
        SET
            created = (
                SELECT created
                FROM logging
                WHERE event_type = 'template/email/create'
                  AND component = 'notificatie'
                  AND component_id = bn.id
                ORDER BY created ASC
                LIMIT 1
            ),
            last_modified = (
                SELECT created
                FROM logging
                WHERE event_type = 'template/email/update'
                  AND component = 'notificatie'
                  AND component_id = bn.id
                ORDER BY created DESC
                LIMIT 1
            )
        WHERE created IS NULL AND last_modified IS NULL;

    UPDATE bibliotheek_sjablonen bs
        SET
            created = (
                SELECT created
                FROM logging
                WHERE event_type = 'template/create'
                  AND component = 'sjabloon'
                  AND component_id = bs.id
                ORDER BY created ASC
                LIMIT 1
            ),
            last_modified = (
                SELECT created
                FROM logging
                WHERE event_type = 'template/update'
                  AND component = 'sjabloon'
                  AND component_id = bs.id
                ORDER BY created DESC
                LIMIT 1
            )
        WHERE created IS NULL AND last_modified IS NULL;

    UPDATE contact_data cd
        SET
            created = (
                SELECT created
                FROM logging
                WHERE event_type = 'subject/update_contact_data'
                  AND component = 'betrokkene'
                  AND component_id = cd.gegevens_magazijn_id
                ORDER BY created ASC
                LIMIT 1
            ),
            last_modified = (
                SELECT created
                FROM logging
                WHERE event_type = 'subject/update_contact_data'
                  AND component = 'betrokkene'
                  AND component_id = cd.gegevens_magazijn_id
                ORDER BY created DESC
                LIMIT 1
            )
        WHERE created IS NULL AND last_modified IS NULL;

    UPDATE zaaktype zt
        SET
            created = (
                SELECT created
                FROM logging
                WHERE event_type = 'casetype/mutation'
                  AND component = 'zaaktype'
                  AND component_id = zt.id
                ORDER BY created ASC
                LIMIT 1
            ),
            last_modified = (
                SELECT created
                FROM logging
                WHERE event_type = 'casetype/mutation'
                  AND component = 'zaaktype'
                  AND component_id = zt.id
                ORDER BY created DESC
                LIMIT 1
            )
        WHERE created IS NULL and last_modified IS NULL;

COMMIT;
