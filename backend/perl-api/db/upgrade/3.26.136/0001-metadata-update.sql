
BEGIN;

    ALTER TABLE file_metadata ALTER COLUMN trust_level TYPE text,
        ALTER COLUMN origin TYPE text,
        ALTER COLUMN document_category TYPE text;

COMMIT;

BEGIN;
    -- This one probably exists on the target DB's already so seperate
    -- transaction
    ALTER TABLE file_metadata ADD COLUMN creation_date date;
COMMIT;
