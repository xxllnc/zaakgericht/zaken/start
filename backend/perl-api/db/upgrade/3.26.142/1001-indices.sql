BEGIN;
    CREATE INDEX IF NOT EXISTS contact_data_gegevens_magazijn_id_idx ON contact_data(gegevens_magazijn_id);
    CREATE INDEX IF NOT EXISTS contact_data_betrokkene_type_idx ON contact_data(betrokkene_type);

    CREATE INDEX IF NOT EXISTS zaaktype_status_zaaktype_node_status_idx ON zaaktype_status(zaaktype_node_id, status);
COMMIT;
