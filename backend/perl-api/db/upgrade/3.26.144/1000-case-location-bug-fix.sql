BEGIN;

    update queue set status = 'pending' where status = 'failed' and type = 'create_case';

COMMIT;
