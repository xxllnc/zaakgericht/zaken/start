BEGIN;

    INSERT INTO rights (name, description) VALUES
        (
            'user',
            'Be a user in the system'
        ),
        (
            'dashboard',
            'Be able to view a dashboard'
        ),
        (
            'case_read',
            'Be able to read cases'
        ),
        (
            'cases_read_own',
            'Be able to view cases that are created by oneself'
        ),
        (
            'case_add',
            'Be able to add cases'
        ),
        (
            'case_edit',
            'Be able to edit cases'
        ),
        (
            'case_manage',
            'Be able to manage cases'
        ),
        (
            'view_sensitive_contact_data',
            'Be able to view GDPR datas'
        );

COMMIT;
