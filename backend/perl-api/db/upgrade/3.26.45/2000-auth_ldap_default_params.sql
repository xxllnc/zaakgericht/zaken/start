BEGIN;

-- Update existing authldap configs so that user logins are enabled by default.
UPDATE interface SET interface_config = interface_config::jsonb || '{"enable_user_login":"1"}'::jsonb WHERE module = 'authldap';

COMMIT;
