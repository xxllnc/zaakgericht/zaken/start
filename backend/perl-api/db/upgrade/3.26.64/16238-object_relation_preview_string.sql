BEGIN;

    ALTER TABLE object_relation ADD COLUMN object_preview TEXT;

COMMIT;
