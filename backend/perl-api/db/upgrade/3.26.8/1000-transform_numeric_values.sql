BEGIN;

    update zaak_kenmerk set value = regexp_replace(value, ',', '.') where id in (
        select zk.id from zaak_kenmerk zk join bibliotheek_kenmerken bk on bk.id = zk.bibliotheek_kenmerken_id and bk.value_type in ('valuta', 'numeric') where value LIKE '%,%'
    );

COMMIT;

