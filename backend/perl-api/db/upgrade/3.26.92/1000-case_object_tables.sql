BEGIN;

    CREATE TABLE case_property (
        id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
        name TEXT NOT NULL,
        type TEXT NOT NULL,
        namespace TEXT NOT NULL,
        value JSONB,
        value_v0 JSONB,
        case_id INT NOT NULL REFERENCES zaak(id) ON DELETE CASCADE,
        object_id UUID NOT NULL REFERENCES object_data(uuid) ON DELETE CASCADE
    );

    -- Conceivably we can encounter properties with values from only one side
    -- This would be undesireable, but in the current workflow of filling this
    -- table, only the value_v0 column will get a defined value.
    ALTER TABLE case_property ADD CONSTRAINT case_property_min_value_count CHECK (
        (value IS NOT NULL) OR
        (value_v0 IS NOT NULL)
    );

    -- HASH indexes for column equivalency queries
    CREATE INDEX case_property_name_idx
              ON case_property
           USING btree (name);

    CREATE INDEX case_property_type_idx
              ON case_property
           USING btree (type);

    -- Create referent-inverted indexes for {case,object}_id retrievals
    CREATE INDEX case_property_case_id_idx
              ON case_property
           USING btree (case_id);

    CREATE INDEX case_property_object_id_idx
              ON case_property
           USING btree (object_id);

    -- Unique index ensuring properties show up only once in the table per
    -- object per namespace. This index DOES NOT ensure that object_id and
    -- case_id refer to the same object, and MUST be enforced in software.
    CREATE UNIQUE INDEX case_property_name_ref_idx
                     ON case_property
                  USING btree (name, namespace, object_id, case_id);
    
    COMMENT ON COLUMN case_property.name IS 'unified magic_string/v1 attribute/v0 property';
    COMMENT ON COLUMN case_property.value IS 'syzygy-style value blob; {"value_type_name":"text","value":"myval","meta":"data"}';
    COMMENT ON COLUMN case_property.value_v0 IS 'object_data-style value blob; {"human_label":"Aanvrager KvK-nummer","human_value":"123456789","value":":123456789","name":"case.requestor.coc","attribute_type":"text"}';
    COMMENT ON COLUMN case_property.type IS 'de-normalized type of the /value/ of the property in the context of the referent object';
    COMMENT ON CONSTRAINT case_property_min_value_count ON case_property IS 'ensure at least one value is set';
    COMMENT ON INDEX case_property_name_ref_idx IS 'enforce uniqueness of param<->object rows, multiple values not allowed';

COMMIT;
