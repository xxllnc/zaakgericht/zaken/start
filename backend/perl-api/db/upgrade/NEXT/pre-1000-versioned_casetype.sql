-- sql file containing all changes related to ctm v2
BEGIN;

-- first drop the view and all functions (also old used function names)
drop view if exists public.view_case_type_version_v2;
drop function if EXISTS get_results_as_json(zaaktype_statusid int);
drop function if EXISTS get_results_as_json(zaaktype_nodeid int);
DROP FUNCTION IF EXISTS get_child_casetypes_as_json(mother_casetype_uuid uuid);
DROP FUNCTION IF EXISTS get_kenmerken_for_phase_as_json(zaaktype_status_id int);
DROP FUNCTION IF EXISTS get_tasks_as_json(zaaktype_statusid int);
DROP FUNCTION IF EXISTS get_templates_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS get_messages_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS get_cases_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS get_cases_attribute_mapping_uuid_as_json(zaaktype_relatie_id int);
DROP FUNCTION IF EXISTS get_phases_as_json(zaaktype_nodeid int);
DROP FUNCTION IF EXISTS get_subject_type_by_uuid(subject_uuid uuid, OUT subject_type text);
DROP FUNCTION IF EXISTS get_casetype_version_authorization(zaaktypenode_id int);

DROP function if EXISTS ctmv2_get_results_as_json(zaaktype_nodeid int);
DROP FUNCTION IF EXISTS ctmv2_get_child_casetypes_as_json(mother_casetype_uuid uuid);
DROP FUNCTION IF EXISTS ctmv2_get_kenmerken_for_phase_as_json(zaaktype_status_id int);
DROP FUNCTION IF EXISTS ctmv2_get_tasks_as_json(zaaktype_statusid int);
DROP FUNCTION IF EXISTS ctmv2_get_templates_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS ctmv2_get_messages_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS ctmv2_get_casetype_cases_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS ctmv2_get_cases_attribute_mapping_uuid_as_json(zaaktype_relatie_id int);
DROP FUNCTION IF EXISTS ctmv2_get_involved_subjects_as_json(zaak_statusid int);
DROP FUNCTION IF EXISTS ctmv2_get_phases_as_json(zaaktype_nodeid int);
DROP FUNCTION IF EXISTS ctmv2_get_casetype_version_authorization(zaaktypenode_id int);


--  create functions and view
CREATE FUNCTION public.get_subject_type_by_uuid(subject_uuid uuid, OUT subject_type text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      found uuid;
    BEGIN
          SELECT INTO found uuid FROM natuurlijk_persoon where uuid = subject_uuid;

          IF found IS NOT NULL
          THEN
            subject_type := 'person';
          END IF;

          SELECT INTO found uuid FROM bedrijf where uuid = subject_uuid;
          IF found is not null THEN
            subject_type := 'organization';
          END IF;

          SELECT INTO found uuid FROM subject where uuid = subject_uuid
            AND subject.subject_type = 'employee';
          IF found is not null THEN
            subject_type := 'employee';
          END IF;

        RETURN;
    END $$;

CREATE OR REPLACE FUNCTION ctmv2_get_results_as_json(zaaktype_nodeid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(row_to_json(results)) results
      from (
        select label as name,
               resultaat as result_type,
               standaard_keuze as is_default,
               selectielijst as selection_list,
               properties::jsonb->>'selectielijst_nummer' as selection_list_number,
               selectielijst_brondatum as selection_list_source_date,
               selectielijst_einddatum  as selection_list_end_date,
               trigger_archival as archival_trigger,
               archiefnominatie as archival_nomination,
               bewaartermijn as retention_period,
               ingang as archival_nomination_valuation,
               "comments" as comments,
               properties::jsonb->>'procestype_nummer' as processtype_number,
               properties::jsonb->>'procestype_naam' as processtype_name,
               properties::jsonb->>'procestype_omschrijving' as processtype_description,
               properties::jsonb->>'procestype_toelichting' as processtype_explanation,
               properties::jsonb->>'procestype_object' as processtype_object,
               NULLIF(properties::jsonb->>'procestype_generiek', '') as procestype_generic, 
               NULLIF(properties::jsonb->>'herkomst', '') as origin,
               NULLIF(properties::jsonb->>'procestermijn', '') as process_period
            from zaaktype_resultaten zr
            where zr.zaaktype_node_id = zaaktype_nodeid
        ) results;
END;
$$;

CREATE OR REPLACE FUNCTION ctmv2_get_casetype_version_authorization(zaaktypenode_id int)
RETURNS json
IMMUTABLE
LANGUAGE plpgsql
AS $$
begin
  return (
    select json_agg(auth) as auth
    from (
      select json_build_object(
        'role_uuid', r.uuid,
        'department_uuid', g.uuid,
        'confidential', za.confidential,
        'rights', json_agg(distinct(za.recht))) as auth
      from zaaktype_authorisation za
      join roles r on r.id = za.role_id
      join groups g on g.id = za.ou_id
      where za.zaaktype_node_id = zaaktypenode_id
      group by za.zaaktype_node_id, za.confidential, r.uuid, g.uuid
    ) auth
  );
END;
$$;

CREATE OR REPLACE FUNCTION ctmv2_get_child_casetypes_as_json(mother_casetype_uuid uuid)
RETURNS json
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
  enabled bool;
  settings json;
  child json;
  children json;
  num_phases_mother int;
  child_casetype_v2 json;
begin
	--  Child casetype settings for v2 have a different structure compared to v1. When retrieving these settings
	--  a translation can be done from v1 -> v2. But the otherway is not possible. New setting structure will be
    --- stored in the field named `child_casetype_settings`. Once stored in v2 structure the new settings are retrieved.
    select properties::json->'child_casetype_settings' into child_casetype_v2 from zaaktype_node where uuid = mother_casetype_uuid;
    if child_casetype_v2 is not null then
      return child_casetype_v2;
    end if;

    select text_to_bool(properties::json->>'is_casetype_mother'), (properties::json->>'child_casetypes')::json
    into enabled, settings from zaaktype_node where uuid = mother_casetype_uuid;
    children := '[]';
    if enabled then
      --  only process settings if enabled    
      select count(zs.id) into num_phases_mother from zaaktype_status zs join zaaktype_node ztn on ztn.id = zs.zaaktype_node_id where ztn.uuid = mother_casetype_uuid;
      for child in select * from json_array_elements(settings) loop
	    -- loop over all child settings and convert it to json_object
	    children := children::jsonb || json_build_object(
	        'enabled', child::json->'import',
	        'casetype', json_build_object(
              'uuid', (select uuid from zaaktype where id = (child::json->'casetype'->>'id')::integer),
              'name', (child::json->'casetype'->'title')
            ),
            'settings', json_build_object(
              'relations', text_to_bool(child::json->>'betrokkenen'),
              'webform', text_to_bool(child::json->>'actions'),
              'registrationform', text_to_bool(child::json->>'actions'),
              'case', text_to_bool(child::json->>'actions'),
              'notifications', text_to_bool(child::json->>'actions'),
              'api', text_to_bool(child::json->>'actions'),
              'allocations', text_to_bool(child::json->>'allocation'),
              'first_phase', text_to_bool(child::json->>'first_phase'),
              'middle_phases', case when text_to_bool(child::json->>'middle_phases') then (select json_agg(generate_series) as num from generate_series(2,num_phases_mother - 1)) else '[]' end,
              'last_phase', text_to_bool(child::json->>'last_phase'),
              'results', text_to_bool(child::json->>'resultaten'),
              'relations', text_to_bool(child::json->>'betrokkenen'),
              'authorizations', text_to_bool(child::json->>'authorisaties')
            )
        )::jsonb;
      end loop;
    end if;
  	return json_build_object('enabled', enabled, 'child_casetypes', children);
END;
$$;

CREATE OR REPLACE FUNCTION ctmv2_get_kenmerken_for_phase_as_json(zaaktype_status_id int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(attributes_for_phase) from (
    with kenmerk_permissions as (
        with kenmerk_permissions_records as (
            select kenmerk_permissions_records.id,
            (kenmerk_permissions_records.permissions->>'role_id')::int as role_id,
            (kenmerk_permissions_records.permissions->>'org_unit_id')::int as group_id
            from (
                select id, JSONB_ARRAY_ELEMENTS(required_permissions::jsonb->'selectedUnits') as permissions
                from zaaktype_kenmerken zk
            ) as kenmerk_permissions_records
        )
        select zk.id, json_agg(
            case when g.uuid is null then '{}' else json_build_object(
                'department_uuid', g.uuid,
                'department_name', g.name,
                'role_uuid', r.uuid,
                'role_name', r.name
            ) end
        ) as permissions
        from zaaktype_kenmerken zk
        left join kenmerk_permissions_records kp on kp.id = zk.id
        join groups g on g.id = kp.group_id
        join roles r on r.id = kp.role_id
        where zk.required_permissions::jsonb->'selectedUnits' is not null 
        and zk.required_permissions::jsonb->'selectedUnits' != '[]'
        group by zk.id
    )
    select bk.uuid as uuid,
        coalesce(zk.is_group::bool, False) as is_group,
        coalesce(zk.referential::bool, False) as referential,
        coalesce(zk.value_mandatory::bool, False) as mandatory,
        coalesce(zk.is_systeemkenmerk::bool, False) as system_attribute,
        zk.label as title,
        zk.help as help_intern,
        zk.help_extern as help_extern,
        zk.label_multiple,
        coalesce(zk.bag_zaakadres::bool, False) as use_as_case_address,
        bk.naam as attribute_name,
        bk.magic_string as attribute_magic_string,
        bk.relationship_type as relationship_type,
        coalesce(bk.type_multiple::bool, False) as is_multiple,
        text_to_bool(zk.pip::text) as publish_pip,
        text_to_bool(zk.pip_can_change::text) as pip_can_change,
        text_to_bool(zk.properties::json->>'skip_change_approval'::text)as skip_change_approval,
        text_to_bool(zk.properties::json->'custom_object'->'create'->>'enabled'::text)as create_custom_object_enabled,
        zk.properties::json->'custom_object'->'create'->>'label'::text as create_custom_object_label,
        zk.properties::json->'custom_object'->'attributes' as create_custom_object_attribute_mapping,
        zk.properties::json->>'relationship_subject_role' as relationship_subject_role,
        text_to_bool(zk.properties::json->>'show_on_map'::text)as show_on_map,
        zk.properties::json->>'map_wms_layer_id' as map_wms_layer_id,
        (select naam from bibliotheek_kenmerken where id = 
            case when zk.properties::json->>'map_wms_feature_attribute_id' ~ E'^\\d+$' then cast(zk.properties::json->>'map_wms_feature_attribute_id' as int) else null end) as map_wms_feature_attribute_label,
        (select uuid from bibliotheek_kenmerken where id = 
            case when zk.properties::json->>'map_wms_feature_attribute_id' ~ E'^\\d+$' then cast(zk.properties::json->>'map_wms_feature_attribute_id' as int) else null end) as map_wms_feature_attribute_id,
        text_to_bool(zk.properties::json->>'map_case_location'::text) as map_case_location,
        case when text_to_bool(zk.properties::json->'date_limit'->'start'->>'active') then 
            json_build_object(
                'value', zk.properties::json->'date_limit'->'start'->>'num',
                'active', text_to_bool(zk.properties::json->'date_limit'->'start'->>'active'),
                'term', zk.properties::json->'date_limit'->'start'->>'term',
                'during', zk.properties::json->'date_limit'->'start'->>'during',
                'reference', case when zk.properties::json->'date_limit'->'start'->>'reference' ~ '^[0-9]+$' then 
                (select inner_bk.uuid from bibliotheek_kenmerken inner_bk join zaaktype_kenmerken inner_ztk on inner_bk.id = inner_ztk.bibliotheek_kenmerken_id where inner_ztk.id = (zk.properties::json->'date_limit'->'start'->>'reference')::int)::text else 'currentDate' end
            ) else null end as start_date_limitation,
        case when text_to_bool(zk.properties::json->'date_limit'->'end'->>'active') then 
            json_build_object(
                'value', zk.properties::json->'date_limit'->'end'->>'num',
                'active', text_to_bool(zk.properties::json->'date_limit'->'end'->>'active'),
                'term', zk.properties::json->'date_limit'->'end'->>'term',
                'during', zk.properties::json->'date_limit'->'end'->>'during',
                'reference', case when zk.properties::json->'date_limit'->'end'->>'reference' ~ '^[0-9]+$' then 
                (select inner_bk.uuid from bibliotheek_kenmerken inner_bk join zaaktype_kenmerken inner_ztk on inner_bk.id = inner_ztk.bibliotheek_kenmerken_id where inner_ztk.id = (zk.properties::json->'date_limit'->'end'->>'reference')::int)::text else 'currentDate' end
            ) else null end as end_date_limitation,
       coalesce(bk.value_type,
           case when bk.value_type is null and zk.is_group then 'group' else null end,
           case when bk.value_type is null and not zk.is_group then 'textblock' else null end
       ) as attribute_type,
       kp.permissions
    from zaaktype_kenmerken zk left join bibliotheek_kenmerken bk on zk.bibliotheek_kenmerken_id = bk.id
    left join kenmerk_permissions kp on kp.id = zk.id
    where zk.zaak_status_id = zaaktype_status_id order by zk.id asc) attributes_for_phase;
END;
$$;

CREATE OR REPLACE FUNCTION ctmv2_get_tasks_as_json(zaaktype_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select coalesce(json_agg(ztsci."label"), '[]'::json) from zaaktype_status_checklist_item ztsci where ztsci.casetype_status_id = zaaktype_statusid;
END;
$$;


CREATE OR REPLACE FUNCTION ctmv2_get_templates_as_json(zaak_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(templates) from (
        select bs.uuid as catalog_template_uuid,
               bs.naam as catalog_template_name,
               zs.label as label,
               text_to_bool(zs.automatisch_genereren::text) as auto_generate,
               zs.help as description,
               zs.custom_filename as custom_filename,
               zs.allow_edit as allow_edit,
               coalesce(zs.target_format, 'odt') as target_format,
               bk.uuid as document_attribute_uuid,
               bk.naam as document_attribute_name
        from zaaktype_sjablonen zs
        join bibliotheek_sjablonen bs on zs.bibliotheek_sjablonen_id = bs.id 
        left join bibliotheek_kenmerken bk on bk.id = zs.bibliotheek_kenmerken_id
        where zaak_status_id = zaak_statusid
        order by zs.id) templates;
END;
$$;


CREATE OR REPLACE FUNCTION ctmv2_get_messages_as_json(zaak_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(messages) from (
select bn.uuid as catalog_message_uuid, 
       bn.label as catalog_message_label,
       zn.rcpt as receiver,
       zn.cc as cc,
       zn.bcc as bcc,
       text_to_bool(zn.automatic::text) as send_automatic, -- 1 or null
       zn.email as to,
       zn.betrokkene_role as person_involved_role,
       legacy_subject_id_as_json(zn.behandelaar) as subject
       from zaaktype_notificatie zn join bibliotheek_notificaties bn on zn.bibliotheek_notificaties_id = bn.id
       where zn.zaak_status_id = zaak_statusid
       order by zn.id) messages;
END;
$$;

CREATE OR REPLACE FUNCTION ctmv2_get_cases_attribute_mapping_uuid_as_json(zaaktype_relatie_id int)
RETURNS table(j jsonb)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
	return QUERY select jsonb_object_agg(bk_key.uuid, bk_value.uuid)
       from zaaktype_relatie zr, each(zr.copy_selected_attributes) 
       left join bibliotheek_kenmerken bk_key on bk_key.magic_string = key and bk_key.deleted is null
       left join bibliotheek_kenmerken bk_value on bk_value.magic_string = value and bk_value.deleted is null
       where zr.id = zaaktype_relatie_id;
END;
$$; 


CREATE OR REPLACE FUNCTION ctmv2_get_casetype_cases_as_json(zaak_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(cases) from (
       select zt.uuid as related_casetype,
	       -- aanvragen
	       ztr.eigenaar_type as requestor_type, 
	       legacy_subject_id_as_json(ztr.eigenaar_id) as requestor,
	       ztr.relatie_type as kind,
           case when ztr.relatie_type = 'vervolgzaak_datum' then ztr.start_delay else null end as start_after_fixeddate,
           case when ztr.relatie_type = 'vervolgzaak' then ztr.start_delay else null end as start_after,
	       ztr.status as automatic,
	       -- behandelen
	       case when ztr.ou_id is null or ztr.role_id is null then null else json_build_object(
	                'department_uuid', g.uuid,
	                'department_name', g.name,
	                'role_uuid', r.uuid,
	                'role_name', r.name
	            ) end as allocation,
	       ztr.required as handle_in_phase,
	       text_to_bool(ztr.automatisch_behandelen::text) as assign_automatic,
	       -- copy
	       ztr.copy_subject_role as copy_subject_roles_enabled,
	       ztr.subject_role as copy_subject_roles,
	       text_to_bool(ztr.kopieren_kenmerken::text) as copy_all_attributes,
	       ctmv2_get_cases_attribute_mapping_uuid_as_json(ztr.id) as copy_attributes_mapping,
	       ztr.copy_related_cases as copy_case_relations,
	       ztr.copy_related_objects as copy_object_relations,
	       -- overig
	       ztr.show_in_pip as show_on_pip,
	       ztr.pip_label as pip_label,
	       text_to_bool(ztr.betrokkene_role_set::text) as add_subjects_involved,       
	       text_to_bool(ztr.betrokkene_authorized::text) as subjects_involved_authorized_for_case,
	       text_to_bool(ztr.betrokkene_notify::text) as subjects_involved_email_notifcation,
	       ztr.betrokkene_role as subjects_involved_role,
	       legacy_subject_id_as_json(ztr.betrokkene_id) as subject_involved,
	       ztr.betrokkene_prefix as subject_involved_magic_string,
	       ztr.creation_style as creation_style,
           ztr.eigenaar_role as owner_role
	    from zaaktype_relatie ztr
	    left join groups g on g.id = ztr.ou_id 
	    left join roles r on r.id = ztr.role_id
	    join zaaktype zt on zt.id = ztr.relatie_zaaktype_id
	    where ztr.zaaktype_status_id = zaak_statusid
	    order by ztr.id
    ) cases;
END;
$$; 

CREATE OR REPLACE FUNCTION ctmv2_get_involved_subjects_as_json(zaak_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(cases) from (
       select legacy_subject_id_as_json(ztb.betrokkene_identifier) as involved_subject,
	       ztb.rol as role, 
	       ztb.magic_string_prefix as magic_string,
	       ztb.gemachtigd as authorized_for_case,
           ztb.notify as notify_by_email
	    from zaaktype_standaard_betrokkenen ztb
	    where ztb.zaak_status_id = zaak_statusid
	    order by ztb.id
    ) cases;
END;
$$; 

CREATE OR REPLACE FUNCTION ctmv2_get_phases_as_json(zaaktype_nodeid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(phases) from (select zts.status as phase_id,
        zts.naam as milestone_name,
        zts.fase as phase_name,
        zts.created,
        zts.last_modified,
        zts.termijn as term_in_days,
        case when g.uuid is null or r.uuid is null then null else
        json_build_object(
            'enabled', case when zts.status = 1 then true else text_to_bool(zts.role_set::text) end,
            'department_uuid', g.uuid,
            'department_name', g.name,
            'role_uuid', r.uuid,
            'role_name', r.name
        ) end as assignment,
        ctmv2_get_kenmerken_for_phase_as_json(zts.id) as custom_fields,
        ctmv2_get_tasks_as_json(zts.id) as tasks,
        ctmv2_get_templates_as_json(zts.id) as templates,
        ctmv2_get_messages_as_json(zts.id) as email_templates,
        ctmv2_get_casetype_cases_as_json(zts.id) as cases,
        ctmv2_get_involved_subjects_as_json(zts.id) as involved_subjects
        from zaaktype_status zts
        left join roles r on r.id = zts.role_id
        left join groups g on g.id = zts.ou_id
        where zts.zaaktype_node_id = zaaktype_nodeid
        order by zts.status) phases;
END;
$$;

CREATE OR REPLACE VIEW public.view_case_type_version_v2
AS 
select
    ztn.uuid as uuid,
    zt.uuid as casetype_uuid,
    zt.active as active,
    case when bc.uuid is null then null else json_build_object(
        'uuid', bc.uuid,
        'name', bc.naam
    ) end as catalog_folder,
    json_build_object(
        'name', ztn.titel,
        'identification', ztn.code,
        'tags', ztn.zaaktype_trefwoorden,
        'description', ztn.zaaktype_omschrijving,
        'case_summary', ztd.extra_informatie,
        'case_public_summary', ztd.extra_informatie_extern,
        'legal_period', case when (ztd.afhandeltermijn is null and ztd.afhandeltermijn_type is null) then null else json_build_object(
            'value', ztd.afhandeltermijn,
            'type', ztd.afhandeltermijn_type
        ) end,
        'service_period',  case when (ztd.servicenorm is null and ztd.servicenorm_type is null) then null else json_build_object(
            'value', ztd.servicenorm,
            'type', ztd.servicenorm_type
        ) end
    ) as general_attributes,
    json_build_object(
        'process_description', ztd.procesbeschrijving,
        'initiator_type', ztd.handelingsinitiator,
        'motivation', ztn.properties::json->>'aanleiding',
        'purpose', ztn.properties::json->>'doel',
        'archive_classification_code', ztn.properties::json->>'archiefclassicatiecode',
        'designation_of_confidentiality', case when (ztn.properties::json->>'vertrouwelijkheidsaanduiding' = '-') then null else ztn.properties::json->>'vertrouwelijkheidsaanduiding' end,
        'responsible_subject', ztn.properties::json->>'verantwoordelijke',
        'responsible_relationship', ztn.properties::json->>'verantwoordingsrelatie',
        'possibility_for_objection_and_appeal', text_to_bool(ztn.properties::json->>'beroep_mogelijk'),
        'publication', text_to_bool(ztn.properties::json->>'publicatie'),
        'publication_text', ztn.properties::json->>'publicatietekst',
        'bag', text_to_bool(ztn.properties::json->>'bag'),
        'lex_silencio_positivo', text_to_bool(ztn.properties::json->>'lex_silencio_positivo'),
        'may_postpone', text_to_bool(ztn.properties::json->>'opschorten_mogelijk'),
        'may_extend', text_to_bool(ztn.properties::json->>'verlenging_mogelijk'),
        'extension_period', case when ztn.properties::json->>'verlengingstermijn'~E'^\\d+$' then (ztn.properties::json->>'verlengingstermijn')::integer else null end,
        'adjourn_period', case when ztn.properties::json->>'verdagingstermijn'~E'^\\d+$' then (ztn.properties::json->>'verdagingstermijn')::integer else null end,
        'penalty_law', text_to_bool(ztn.properties::json->>'wet_dwangsom'),
        'wkpb_applies', text_to_bool(ztn.properties::json->>'wkpb'),
        'e_webform', ztn.properties::json->>'e_formulier',
        'legal_basis', ztd.grondslag,
        'local_basis', ztn.properties::json->>'lokale_grondslag',
        'gdpr', json_build_object(
            'enabled', text_to_bool(ztn.properties::json->'gdpr'->>'enabled'),
            'kind', json_build_object(
                --  'basic_details', case when ztn.properties::json->'gdpr'->'personal'->>'basic_details' = 'on' is true then true else false end,
            	'basic_details', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'basic_details'),
                'personal_id_number', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'personal_id_number'),
                'income', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'income'),
                'race_or_ethniticy', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'race_or_ethniticy'),
                'political_views', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'political_views'),
                'religion', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'religion'),
                'membership_union', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'membership_union'),
                'genetic_or_biometric_data', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'genetic_or_biometric_data'),
                'health', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'health'),
                'sexual_identity', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'sexual_identity'),
                'criminal_record', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'criminal_record'),
                'offspring', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'offspring')
            ),
            'source', json_build_object(
                'public_source', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'public_source'),
                'registration', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'registration'),
                'partner', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'partner'),
                'sender', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'sender')
            ),
            'processing_type', ztn.properties::json->'gdpr'->>'processing_type',
            'process_foreign_country', text_to_bool(ztn.properties::json->'gdpr'->>'process_foreign_country'),
            'process_foreign_country_reason', ztn.properties::json->'gdpr'->>'process_foreign_country_reason',
            'processing_legal', ztn.properties::json->'gdpr'->>'processing_legal',
            'processing_legal_reason', ztn.properties::json->'gdpr'->>'processing_legal_reason'
        )
    ) as documentation,
    json_build_object(
        'trigger', ztn.trigger,
        'allowed_requestor_types', (select json_agg(zb.betrokkene_type) from zaaktype_betrokkenen zb where zb.zaaktype_node_id = ztn.id),
        'preset_requestor', legacy_subject_id_as_json(ztd.preset_client),
        'api_preset_assignee', legacy_subject_id_as_json(ztn.properties::json->>'preset_owner_identifier'),
        'address_requestor_use_as_correspondence', coalesce (ztn.adres_aanvrager::bool, false),
        'address_requestor_use_as_case_address', coalesce(ztn.adres_andere_locatie::bool, false),
        'address_requestor_show_on_map', coalesce(ztn.adres_geojson::bool, false)
    ) as relations,
    json_build_object(
        'public_confirmation_title', ztn.properties::json->>'public_confirmation_title',
        'public_confirmation_message', ztn.properties::json->>'public_confirmation_message',
        'case_location_message', ztn.properties::json->>'case_location_message',
        'pip_view_message', ztn.properties::json->>'pip_view_message',
        'actions', json_build_object(
            'enable_webform', coalesce(ztn.webform_toegang::bool, false),
            'create_delayed', coalesce((ztn.properties::json->>'delayed')::bool, false),
            'address_check', coalesce((ztn.properties::json->>'case_location_check')::bool, false),
            'reuse_casedata', coalesce(ztn.aanvrager_hergebruik::bool, false),
            'enable_online_payment', coalesce(ztn.online_betaling::bool, false),
            'enable_manual_payment', coalesce((ztn.properties::json->>'offline_betaling')::bool, false),
            'email_required', coalesce(ztn.contact_info_email_required::bool, false),
            'phone_required', coalesce(ztn.contact_info_phone_required::bool, false),
            'mobile_required', coalesce(ztn.contact_info_mobile_phone_required::bool, false),
            'disable_captcha', coalesce((ztn.properties::json->>'no_captcha')::bool, false),
            'generate_pdf_end_webform', coalesce((ztn.properties::json->>'pdf_registration_form')::bool, false)
        ),
        'price', json_build_object(
            'web', ztd.pdc_tarief,
            'frontdesk', ztn.properties::json->>'pdc_tarief_balie',
            'phone', ztn.properties::json->>'pdc_tarief_telefoon',
            'email', ztn.properties::json->>'pdc_tarief_email',
            'assignee', ztn.properties::json->>'pdc_tarief_behandelaar',
            'post', ztn.properties::json->>'pdc_tarief_post',
            'chat', ztn.properties::json->>'pdc_tarief_chat',
            'other', ztn.properties::json->>'pdc_tarief_overige'
        )
    ) as webform,
     json_build_object(
        -- Toewijzing aanpassing bij interne intake
        'allow_assigning_to_self', coalesce(ztn.automatisch_behandelen::bool, false),
        'allow_assigning', coalesce(ztn.toewijzing_zaakintake::bool, false),
        -- Intern form
        'show_confidentionality', coalesce((ztn.properties::json->>'confidentiality')::bool, false),
        'show_contact_details', coalesce(ztn.contact_info_intake::bool, false),
        'allow_add_relations', coalesce(ztn.extra_relaties_in_aanvraag::bool, false)
     ) as registrationform,
    json_build_object(
        -- Zaakdossier
        'disable_pip_for_requestor', coalesce(ztn.prevent_pip::bool, false),
        'lock_registration_phase', coalesce((ztn.properties::json->>'lock_registration_phase')::bool, false),
        'queue_coworker_changes', coalesce((ztn.properties::json->>'queue_coworker_changes')::bool, false),
        'allow_external_task_assignment', coalesce((ztn.properties::json->>'allow_external_task_assignment')::bool, false),
        'default_document_folders', (ztn.properties::json->>'default_directories')::json,
        'default_html_email_template', ztn.properties::json->>'default_html_email_template'
    ) as case_dossier,
    json_build_object(
        -- API instellingen
        'api_can_transition', coalesce((ztn.properties::json->>'api_can_transition')::bool, false),
        'notifications', json_build_object(
            'external_notify_on_new_case', coalesce((ztn.properties::json->>'notify_on_new_case')::bool, false),
            'external_notify_on_new_document', coalesce((ztn.properties::json->>'notify_on_new_document')::bool, false),
            'external_notify_on_new_message', coalesce((ztn.properties::json->>'notify_on_new_message')::bool, false),
            'external_notify_on_exceed_term', coalesce((ztn.properties::json->>'notify_on_exceed_term')::bool, false),
            'external_notify_on_allocate_case', coalesce((ztn.properties::json->>'notify_on_allocate_case')::bool, false),
            'external_notify_on_phase_transition', coalesce((ztn.properties::json->>'notify_on_phase_transition')::bool, false),
            'external_notify_on_task_change', coalesce((ztn.properties::json->>'notify_on_task_change')::bool, false),
            'external_notify_on_label_change', coalesce((ztn.properties::json->>'notify_on_label_change')::bool, false),
            'external_notify_on_subject_change', coalesce((ztn.properties::json->>'notify_on_subject_change')::bool, false)
        )
    ) as api,
    coalesce(ctmv2_get_casetype_version_authorization(ztn.id), '[]'::json) as authorization,
    ctmv2_get_child_casetypes_as_json(ztn.uuid) as child_casetype_settings,
    ctmv2_get_phases_as_json(ztn.id) as phases,
    ctmv2_get_results_as_json(ztn.id) as results
from zaaktype_node ztn
join zaaktype zt on ztn.zaaktype_id = zt.id
join zaaktype_definitie ztd on ztn.zaaktype_definitie_id = ztd.id
left join bibliotheek_categorie bc on zt.bibliotheek_categorie_id = bc.id;


COMMIT;
