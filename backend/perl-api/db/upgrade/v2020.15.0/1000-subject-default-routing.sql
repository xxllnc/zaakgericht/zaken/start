BEGIN;

  update subject set role_ids = '{}' where role_ids is null;
  update subject set group_ids = '{}' where group_ids is null;

  alter table subject alter column role_ids set default '{}';
  alter table subject alter column group_ids set default '{}';

  alter table subject alter column role_ids set not null;
  alter table subject alter column group_ids set not null;

COMMIT;
