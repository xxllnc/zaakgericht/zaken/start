BEGIN;
-- DROP TABLE IF EXISTS custom_object_relationship;
CREATE TABLE custom_object_relationship (
    id SERIAL PRIMARY KEY NOT NULL,
    relationship_type text NOT NULL,
    relationship_magic_string_prefix text,
    custom_object_id int references custom_object (id) NOT NULL,
    custom_object_version_id int references custom_object_version (id),
    related_document_id int references file (id) NULL,
    related_case_id int references zaak (id) NULL,
    related_custom_object_id int references custom_object (id) NULL,
    related_uuid uuid NOT NULL,
    CONSTRAINT custom_object_relationship_at_least_one_relationship CHECK (
        num_nonnulls(
            related_document_id,
            related_case_id,
            related_custom_object_id
        ) = 1
    )
);
ALTER TABLE file
ADD COLUMN skip_intake boolean not null default FALSE;
DROP TABLE custom_object_case_relation;
COMMIT;