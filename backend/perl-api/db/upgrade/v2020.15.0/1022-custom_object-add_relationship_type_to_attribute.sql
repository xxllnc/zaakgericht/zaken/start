BEGIN;
ALTER TABLE bibliotheek_kenmerken DROP CONSTRAINT bibliotheek_kenmerken_value_type_check;
ALTER TABLE bibliotheek_kenmerken
ADD CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK (
        value_type = ANY (
            ARRAY ['text_uc'::text, 'checkbox'::text, 'richtext'::text, 'date'::text, 'file'::text, 'bag_straat_adres'::text, 'email'::text, 'valutaex'::text, 'bag_openbareruimte'::text, 'text'::text, 'bag_openbareruimtes'::text, 'url'::text, 'valuta'::text, 'option'::text, 'bag_adres'::text, 'select'::text, 'valutain6'::text, 'valutaex6'::text, 'valutaex21'::text, 'image_from_url'::text, 'bag_adressen'::text, 'valutain'::text, 'calendar'::text, 'calendar_supersaas'::text, 'bag_straat_adressen'::text, 'googlemaps'::text, 'numeric'::text, 'valutain21'::text, 'textarea'::text, 'bankaccount'::text, 'subject'::text, 'geolatlon'::text, 'appointment'::text, 'geojson'::text, 'relationship'::text]
        )
    );
ALTER TABLE bibliotheek_kenmerken
ADD COLUMN relationship_type TEXT;
ALTER TABLE bibliotheek_kenmerken
ADD COLUMN relationship_name TEXT;
ALTER TABLE bibliotheek_kenmerken
ADD COLUMN relationship_uuid uuid;
COMMIT;