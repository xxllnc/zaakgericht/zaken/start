
BEGIN;

  ALTER TABLE zaak ADD COLUMN current_deadline  JSONB default '{}'::jsonb NOT NULL;
  ALTER TABLE zaak ADD COLUMN deadline_timeline JSONB default '[]'::jsonb NOT NULL;

  UPDATE zaak z
  SET current_deadline = zm.current_deadline,
      deadline_timeline = zm.deadline_timeline
  FROM zaak_meta zm
  WHERE z.id = zm.zaak_id;

  ALTER TABLE zaak_meta DROP column current_deadline;
  ALTER TABLE zaak_meta DROP column deadline_timeline;
  ALTER TABLE zaak_meta DROP column last_modified;

  ALTER TABLE zaak_meta DROP CONSTRAINT zaak_meta_uniq_zaak_id;
  ALTER TABLE zaak_meta ALTER COLUMN zaak_id DROP NOT NULL;

COMMIT;
