BEGIN;

  CREATE OR REPLACE FUNCTION set_leading_qitem() RETURNS TRIGGER
  LANGUAGE 'plpgsql' AS $$
  DECLARE
    counter int;
    case_uuid uuid;
    case_id int;
    json_no jsonb;
    json_uuid jsonb;
  BEGIN

    IF NEW.type = 'touch_case'
    THEN
      case_uuid := NEW.data::jsonb->>'case_object_id';
      case_id := NEW.data::jsonb->>'case_number';
      json_no := '{}'::jsonb;
      json_uuid := '{}'::jsonb;

      if case_id is not null
      then
        select into case_uuid uuid from zaak where id = case_id;
      else
        select into case_id id from zaak where uuid = case_uuid;
      end if;

      json_no   := json_build_object('case_number',    case_id::text);
      json_uuid := json_build_object('case_object_id', case_uuid);

      UPDATE queue set status = 'cancelled' WHERE type = 'touch_case'
        AND priority < NEW.priority
        AND status in ('pending', 'waiting')
        AND (
          data::jsonb @> json_no
          OR
          data::jsonb @> json_uuid
        );

      SELECT INTO counter count(id) FROM queue WHERE type = 'touch_case'
        AND priority >= NEW.priority
        AND status in ('pending', 'waiting')
        AND (
          data::jsonb @> json_no
          OR
          data::jsonb @> json_uuid
        );

      IF counter > 0
      THEN
        NEW.status := 'cancelled';
      END IF;

    END IF;

    return NEW;

  END $$;

  DROP TRIGGER IF EXISTS insert_leading_qitem ON queue;
  CREATE TRIGGER insert_leading_qitem
  BEFORE INSERT ON queue
  FOR EACH ROW
  EXECUTE PROCEDURE set_leading_qitem();

COMMIT;

