BEGIN;

    UPDATE interface
       SET interface_config = interface_config::jsonb || '{ "map_application":"internal" }'
     WHERE module = 'map' and interface_config::jsonb @> '{"map_application":"builtin"}'::jsonb;

COMMIT;
