BEGIN;

  CREATE OR REPLACE FUNCTION subject_employee_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    properties jsonb;
    preview text;
    positions jsonb;

  BEGIN
      properties := (subject->'properties')::jsonb;

      SELECT INTO json subject_json(subject, 'employee');

      SELECT INTO positions position_matrix(
        COALESCE((subject->'group_ids')::int[], '{}'::int[]),
        COALESCE((subject->'role_ids')::int[], '{}'::int[])
      );

      SELECT INTO json json || jsonb_build_object(
        'instance', json_build_object(
          'username', subject->'username',
          'initials', properties->'initials',
          'first_names', properties->'givenname',
          'surname', properties->'surname',
          'display_name', properties->'displayname',
          'email_address', properties->'mail',
          'phone_number', properties->'telephonenumber',
          'settings', (subject->'settings')::jsonb,
          'positions', positions,
          'date_modified', (subject->'last_modified')::timestamp with time zone,
          'date_created', null
        )
      );
      RETURN;
  END;
  $$;

COMMIT;

