BEGIN;


  UPDATE zaak_meta zm SET index_hstore = od.index_hstore, text_vector=od.text_vector FROM object_data od WHERE od.object_id = zm.zaak_id AND od.object_class = 'case';

COMMIT;


