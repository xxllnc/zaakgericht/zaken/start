BEGIN;
  CREATE OR REPLACE FUNCTION position_matrix(
    IN groups int[],
    IN roles int[],
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    role_id int;
    group_id int;

    gr jsonb;
    role json;

    pos jsonb;
  BEGIN

      pos := '[]'::jsonb; -- || does nothing when value is NULL

      FOREACH group_id IN ARRAY groups
      LOOP
        SELECT INTO gr v1_json FROM groups WHERE id = group_id;
        FOREACH role_id IN ARRAY roles
        LOOP
          SELECT INTO role v1_json FROM roles WHERE id = role_id;
          SELECT INTO pos pos || jsonb_build_object(
            'preview', 'position(unsynched)',
            'reference', NULL,
            'type', 'position/route',
            'instance', json_build_object(
              'date_modified', NOW(),
              'date_created', NOW(),
              'group', gr,
              'role', role
            )
          );
        END LOOP;
      END LOOP;

      SELECT INTO json json_build_object(
        'type', 'set',
        'reference', null,
        'instance', json_build_object(
          'pager', null,
          'rows', pos
        )
      );
  END;
  $$;

COMMIT;
