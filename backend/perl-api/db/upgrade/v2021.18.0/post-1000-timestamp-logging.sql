
BEGIN;

  UPDATE
    logging
  SET
    event_data = event_data::jsonb || jsonb_build_object('timestamp', created::timestamp(0))
  WHERE
    event_type = 'case/close' and (event_data::jsonb->>'timestamp')::timestamp(0) > created::timestamp
    ;

COMMIT;
