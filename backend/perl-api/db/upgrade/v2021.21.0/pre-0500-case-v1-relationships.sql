
BEGIN;
  DROP VIEW IF EXISTS case_relationship_view CASCADE;

  DROP VIEW IF EXISTS view_case_relationship CASCADE;

  CREATE VIEW view_case_relationship
  AS
   SELECT cr.case_id_a AS case_id,
      cr.case_id_b AS relation_id,
      cr.type_a AS type,
      z.uuid AS relation_uuid,
      cr.order_seq_b as order_seq
     FROM case_relation cr
       JOIN zaak z ON z.id = cr.case_id_b AND z.deleted IS NULL
  UNION
   SELECT cr.case_id_b AS case_id,
      cr.case_id_a AS relation_id,
      cr.type_b AS type,
      z.uuid AS relation_uuid,
      cr.order_seq_a as order_seq
     FROM case_relation cr
       JOIN zaak z ON z.id = cr.case_id_a AND z.deleted IS NULL
  UNION
   SELECT parent.id AS case_id,
      child.id AS relation_id,
      'parent'::character varying AS type,
      child.uuid AS relation_uuid,
      0 as order_seq
     FROM zaak parent
       JOIN zaak child ON child.pid = parent.id
    WHERE child.deleted IS NULL AND parent.deleted IS NULL
  UNION
   SELECT child.id AS case_id,
      parent.id AS relation_id,
      'child'::character varying AS type,
      parent.uuid AS relation_uuid,
      0 as order_seq
     FROM zaak parent
       JOIN zaak child ON child.pid = parent.id
    WHERE child.deleted IS NULL AND parent.deleted IS NULL;

  CREATE VIEW view_case_relationship_v1_json AS
    SELECT case_id, type, jsonb_agg(json_build_object('type', 'case', 'reference', relation_uuid)) as relationship
    FROM
      view_case_relationship
    GROUP BY case_id, type
    ;

  CREATE VIEW case_v1 AS
  SELECT
      c.number,
      c.id,
      c.number_parent,
      c.number_master,
      c.number_previous,
      c.subject,
      c.subject_external,
      c.status,
      c.date_created,
      c.date_modified,
      c.date_destruction,
      c.date_of_completion,
      c.date_of_registration,
      c.date_target,
      c.html_email_template,
      c.payment_status,
      c.price,
      c.channel_of_contact,
      c.archival_state,
      c.confidentiality,
      c.stalled_since,
      c.stalled_until,
      c.current_deadline,
      c.deadline_timeline,
      c.result_id,
      c.result,
      c.active_selection_list,
      c.outcome,
      c.casetype,
      c.route,
      c.suspension_rationale,
      c.premature_completion_rationale,
      zts.fase::text AS phase,
      c.relations,
      c.case_relationships,
      c.requestor,
      c.assignee,
      c.coordinator,
      c.aggregation_scope,
      c.case_location,
      c.correspondence_location,
      (
          SELECT
              COALESCE(jsonb_object_agg(ca.magic_string, ca.value) FILTER (WHERE ca.magic_string IS NOT NULL), '{}'::jsonb) AS "coalesce"
          FROM
              case_attributes_v1 ca
          WHERE
              ca.case_id = c.number) AS attributes,
      (
          SELECT
              json_build_object('preview', zts.fase, 'reference', NULL::unknown, 'type', 'case/milestone', 'instance', json_build_object('date_created', timestamp_to_perl_datetime (now()), 'date_modified', timestamp_to_perl_datetime (now()), 'phase_label', CASE WHEN zts.id IS NOT NULL THEN
                  zts.fase
              ELSE
                  zts_end.fase
              END, 'phase_sequence_number', CASE WHEN zts.id IS NOT NULL THEN
                  zts.status
              ELSE
                  zts_end.status
              END, 'milestone_label', zts_previous.naam, 'milestone_sequence_number', zts_previous.status, 'last_sequence_number', zts_end.status)) AS json_build_object
          FROM
              casetype_end_status zts_end
          WHERE
              c.zaaktype_node_id = zts_end.zaaktype_node_id) AS milestone
  FROM (
      SELECT
          z.id AS number,
          z.uuid AS id,
          z.pid AS number_parent,
          z.number_master,
          z.vervolg_van AS number_previous,
          z.onderwerp AS subject,
          z.onderwerp_extern AS subject_external,
          z.status,
          z.created AS date_created,
          z.last_modified AS date_modified,
          z.vernietigingsdatum AS date_destruction,
          z.afhandeldatum AS date_of_completion,
          z.registratiedatum AS date_of_registration,
          z.streefafhandeldatum AS date_target,
          z.html_email_template,
          z.payment_status,
          z.dutch_price AS price,
          z.contactkanaal AS channel_of_contact,
          z.archival_state,
          z.zaaktype_node_id,
          z.milestone AS raw_milestone,
          get_confidential_mapping (z.confidentiality) AS confidentiality,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.stalled_since
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_since,
          CASE WHEN z.status = 'stalled'::text THEN
              z.stalled_until
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_until,
          zm.current_deadline,
          zm.deadline_timeline,
          ztr.id AS result_id,
          ztr.resultaat AS result,
          ztr.selectielijst AS active_selection_list,
          CASE WHEN ztr.id IS NOT NULL THEN
              json_build_object('reference', NULL::unknown, 'type', 'case/result', 'preview', CASE WHEN ztr.label IS NOT NULL THEN
                      ztr.label
                  ELSE
                      ztr.resultaat
                  END, 'instance', json_build_object('date_created', timestamp_to_perl_datetime (ztr.created::timestamp with time zone), 'date_modified', timestamp_to_perl_datetime (ztr.last_modified::timestamp with time zone), 'archival_type', ztr.archiefnominatie, 'dossier_type', ztr.dossiertype, 'name', CASE WHEN ztr.label IS NOT NULL THEN
                          ztr.label
                      ELSE
                          ztr.resultaat
                      END, 'result', ztr.resultaat, 'retention_period', ztr.bewaartermijn, 'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                          NULL
                      ELSE
                          ztr.selectielijst
                      END, 'selection_list_start', ztr.selectielijst_brondatum, 'selection_list_end', ztr.selectielijst_einddatum))
          ELSE
              NULL
          END AS outcome,
          json_build_object('preview', ct_ref.title, 'reference', ct_ref.uuid, 'instance', json_build_object('version', ct_ref.version, 'name', ct_ref.title), 'type', 'casetype') AS casetype,
          json_build_object('preview', (gr.name || ', '::text) || role.name, 'reference', NULL::unknown, 'type', 'case/route', 'instance', json_build_object('date_created', timestamp_to_perl_datetime (now()), 'date_modified', timestamp_to_perl_datetime (now()), 'group', gr.v1_json, 'role', role.v1_json)) AS route,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.opschorten
          ELSE
              NULL::character varying
          END AS suspension_rationale,
          CASE WHEN z.status = 'resolved'::text THEN
              zm.afhandeling
          ELSE
              NULL::character varying
          END AS premature_completion_rationale,
          json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb))) AS relations,
          json_build_object(
            'parent', parent.relationship->0,

            'continuation', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(continuation.relationship, '[]'::jsonb)
              )
            ),
            'child', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(children.relationship, '[]'::jsonb)
              )
            ),
            'plain', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(crp.relationship, '[]'::jsonb)
              )
            )
          ) AS case_relationships,

          z.requestor_v1_json AS requestor,
          z.assignee_v1_json AS assignee,
          z.coordinator_v1_json AS coordinator,
          'Dossier'::text AS aggregation_scope,
          NULL::text AS case_location,
          NULL::text AS correspondence_location
      FROM
          zaak z
      LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
      LEFT JOIN casetype_v1_reference ct_ref ON z.zaaktype_node_id = ct_ref.casetype_node_id
      LEFT JOIN zaaktype_resultaten ztr ON z.resultaat_id = ztr.id
      LEFT JOIN GROUPS gr ON z.route_ou = gr.id
      LEFT JOIN roles ROLE ON z.route_role = role.id
      LEFT JOIN view_case_relationship_v1_json crp ON z.id = crp.case_id
          AND crp.type = 'plain'
      LEFT JOIN view_case_relationship_v1_json continuation ON z.id = continuation.case_id
          AND continuation.type = 'initiator'
      LEFT JOIN view_case_relationship_v1_json children ON z.id = children.case_id
          AND children.type = 'parent'
      LEFT JOIN view_case_relationship_v1_json parent ON z.id = parent.case_id
          AND parent.type = 'child'
  WHERE
      z.deleted IS NULL) AS c
      LEFT JOIN zaaktype_status zts ON c.zaaktype_node_id = zts.zaaktype_node_id
          AND zts.status = (c.raw_milestone + 1)
      LEFT JOIN zaaktype_status zts_previous ON c.zaaktype_node_id = zts_previous.zaaktype_node_id
          AND zts_previous.status = c.raw_milestone;

COMMIT;

