BEGIN;

  CREATE OR REPLACE FUNCTION attribute_value_to_v0(
    IN value text[],
    IN value_type text,
    IN value_mvp boolean,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

  BEGIN

    value_json := null;
    SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

    IF length = 0 AND value_type = 'file'
    THEN
      value_json := '[]'::jsonb;
      RETURN;
    ELSIF length = 0
    THEN
      RETURN;
    END IF;

    IF value_type = 'date'
    THEN
      SELECT INTO value_json to_jsonb(attribute_date_value_to_text(value[1]));
      RETURN;
    END IF;

    IF value_type LIKE 'bag_%'
    THEN

      SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);

      IF value_type IN ('bag_adres', 'bag_openbareruimte', 'bag_straat_adres')
      THEN
        SELECT INTO value_json value_json->0;
      END IF;

      RETURN;
    END IF;

    IF value_type = 'file'
    THEN

      SELECT INTO value_json attribute_file_value_to_v0_jsonb(value);
      RETURN;

    END IF;

    IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
    THEN

        IF value_mvp = true
        THEN
          value_json := '[]'::jsonb;
          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || to_jsonb(m::jsonb);
          END LOOP;
        ELSE
          SELECT INTO value_json to_jsonb(value[1]::jsonb);
        END IF;
        RETURN;
    END IF;

    IF value_type IN ('checkbox', 'select')
    THEN
      SELECT INTO value_json to_jsonb(value);
      RETURN;
    END IF;

    IF value_type = 'valuta'
    THEN
      value_json := value[1]::numeric;
      RETURN;
    END IF;

    IF value_mvp = true
    THEN
      SELECT INTO value_json to_jsonb(value);
    ELSE
      SELECT INTO value_json to_jsonb(value[1]);
    END IF;


    RETURN;
  END;
  $$;

COMMIT;

