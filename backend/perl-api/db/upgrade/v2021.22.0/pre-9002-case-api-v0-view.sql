BEGIN;

  DROP VIEW IF EXISTS case_v0;

  CREATE VIEW case_v0 AS
  SELECT
    z.uuid AS id,
    z.id AS object_id,

    jsonb_build_object(
      'class_uuid', zt.uuid,
      'pending_changes', zm.pending_changes
    ) as case,

    -- zs::attributes and friends
    jsonb_build_object(
      'case.route_ou', z.route_ou,
      'case.route_role', z.route_role,

      'case.channel_of_contact', z.contactkanaal,
      'case.html_email_template', z.html_email_template,

      'case.current_deadline', zm.current_deadline,
      'case.date_destruction', timestamp_to_perl_datetime(z.vernietigingsdatum),
      'case.date_of_completion', timestamp_to_perl_datetime(z.afhandeldatum),
      'case.date_of_registration', timestamp_to_perl_datetime(z.registratiedatum),
      'case.date_target', CASE WHEN z.status = 'stalled' THEN 'Opgeschort' ELSE timestamp_to_perl_datetime(z.streefafhandeldatum) END,
      'case.deadline_timeline', zm.deadline_timeline,
      'case.stalled_since', timestamp_to_perl_datetime(zm.stalled_since),
      'case.stalled_until', timestamp_to_perl_datetime(z.stalled_until),
      'case.suspension_rationale', zm.opschorten,
      'date_created', timestamp_to_perl_datetime(z.created),
      'date_modified', timestamp_to_perl_datetime(z.last_modified),
      'case.urgency', z.urgency,
      'case.startdate', to_char(z.registratiedatum::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY'),
      'case.progress_days', get_date_progress_from_case(hstore(z)),
      'case.progress_status', z.status_percentage,
      'case.days_left', z.streefafhandeldatum::date - COALESCE(z.afhandeldatum::date, NOW()::date),
      'case.lead_time_real', z.leadtime,
      'case.destructable', is_destructable(z.vernietigingsdatum),


      'case.number_status', z.milestone,
      'case.milestone', CASE WHEN zts_next.id IS NOT NULL THEN zts.naam ELSE null END,
      'case.phase', CASE WHEN zts_next.id IS NOT NULL THEN zts_next.fase ELSE null END,

      'case.status', z.status,

      'case.subject', z.onderwerp,
      'case.subject_external', z.onderwerp_extern,

      'case.archival_state', z.archival_state,
      'case.confidentiality', get_confidential_mapping(z.confidentiality),

      'case.payment_status', get_payment_status_mapping(z.payment_status),
      'case.price', z.dutch_price,

      -- documents
      'case.documents', COALESCE((SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', ') FROM file f
        WHERE f.case_id = z.id and f.date_deleted is null and f.active_version = true), ''),

      -- attributes with files (used in e-mail tab for JS)
      'case.case_documents', COALESCE((SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', ') FROM file case_documents
        JOIN file_case_document fcd
          ON fcd.case_id = case_documents.case_id and case_documents.id = fcd.file_id
        WHERE case_documents.case_id = z.id), ''),

      -- cached values which potentially block phase transitions
      'case.num_unaccepted_updates', zm.unaccepted_attribute_update_count,
      'case.num_unread_communication', zm.unread_communication_count,
      'case.num_unaccepted_files', zm.unaccepted_files_count,

      -- static values
      'case.aggregation_scope', 'Dossier'
    )
    -- Case/casetype result blob
    || jsonb_build_object(
      'case.result', z.resultaat,
      'case.result_description', ztr.label,
      'case.result_explanation', ztr.comments,
      'case.result_id', ztr.id,
      'case.result_origin', ztr.properties::jsonb->'herkomst',
      'case.result_process_term', ztr.properties::jsonb->'procestermijn',
      'case.result_process_type_description', ztr.properties::jsonb->'procestype_omschrijving',
      'case.result_process_type_explanation', ztr.properties::jsonb->'procestype_toelichting',
      'case.result_process_type_generic', ztr.properties::jsonb->'procestype_generiek',
      'case.result_process_type_name', ztr.properties::jsonb->'procestype_naam',
      'case.result_process_type_number', ztr.properties::jsonb->'procestype_nummer',
      'case.result_process_type_object', ztr.properties::jsonb->'procestype_object',
      'case.result_selection_list_number', ztr.properties::jsonb->'selectielijst_nummer',
      'case.retention_period_source_date', ztr.ingang,
      'case.type_of_archiving', ztr.archiefnominatie,
      'case.period_of_preservation', rpt.label,
      'case.period_of_preservation_active', CASE WHEN ztr.trigger_archival = true THEN 'Ja' ELSE 'Nee' END,
      'case.type_of_archiving', ztr.archiefnominatie,
      -- TODO: Pick one
      'case.selection_list', ztr.selectielijst,
      'case.active_selection_list', ztr.selectielijst
    )
    -- ztd stuff
    || jsonb_build_object(
      'case.lead_time_legal', ztd.afhandeltermijn,
      'case.lead_time_service', ztd.servicenorm,
      'case.principle_national', ztd.grondslag
    )
    -- Case.number shizzle
    || jsonb_build_object(
      'case.custom_number', CASE WHEN char_length(z.prefix) > 0 THEN CONCAT(z.prefix, '-', z.id) ELSE z.id::text END,
      'case.number', z.id,
      'case.number_master', z.number_master
    )
    -- case.casetype stuff
    || jsonb_build_object(
      'case.casetype', zt.uuid,
      'case.casetype.id', zt.id,
      'case.casetype.generic_category', bc.naam,
      'case.casetype.initiator_type', ztd.handelingsinitiator,
      'case.casetype.price.web', ztd.pdc_tarief,
      'case.casetype.process_description', ztd.procesbeschrijving,
      'case.casetype.publicity', ztd.openbaarheid,
      -- incorrectly named
      'case.casetype.department', gr.name,
      'case.casetype.route_role', ro.name
    ) || ztn.v0_json
    -- relations
    || jsonb_build_object(
      'case.parent_uuid', COALESCE(parent.uuid::text, '')
    )
    || jsonb_build_object(
    -- assignee
      'assignee', z.assignee_v1_json->>'reference',
      'case.assignee', z.assignee_v1_json->>'preview',
      'case.assignee.uuid', z.assignee_v1_json->>'reference',
      'case.assignee.email', z.assignee_v1_json->'instance'->'subject'->'instance'->>'email_address',
      'case.assignee.initials', z.assignee_v1_json->'instance'->'subject'->'instance'->>'initials',
      'case.assignee.last_name', z.assignee_v1_json->'instance'->'subject'->'instance'->>'surname',
      'case.assignee.first_names', z.assignee_v1_json->'instance'->'subject'->'instance'->>'first_names',
      'case.assignee.phone_number', z.assignee_v1_json->'instance'->'subject'->'instance'->>'phone_number',
      'case.assignee.id', (split_part(z.assignee_v1_json->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.assignee.department', gassign.name,
      'case.assignee.title', ''
    )
    || jsonb_build_object(
    -- coordinator
      'coordinator', z.coordinator_v1_json->'reference',
      'case.coordinator', z.coordinator_v1_json->'preview',
      'case.coordinator.uuid', z.coordinator_v1_json->'reference',
      'case.coordinator.email', z.coordinator_v1_json->'instance'->'subject'->'instance'->>'email_address',
      'case.coordinator.phone_number', z.coordinator_v1_json->'instance'->'subject'->'instance'->>'phone_number',
      'case.coordinator.id', (split_part(z.coordinator_v1_json->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.coordinator.title', ''
    )
    -- requestor
    || case_subject_as_v0_json(hstore(requestor), 'requestor', false)
    || case_subject_as_v0_json(hstore(requestor), 'requestor', true)
    || jsonb_build_object(
      'case.requestor.preset_client', CASE WHEN z.preset_client = true THEN 'Ja' ELSE 'Nee' END
    )
    || case_subject_as_v0_json(hstore(recipient), 'recipient', false)
    || case_subject_as_v0_json(hstore(recipient), 'recipient', true)
    -- case location
    || case_location_as_v0_json(hstore(case_location))
    -- case relationships
    || jsonb_build_object(
      'case.related_cases', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
              FROM view_case_relationship rel
              WHERE z.id = rel.case_id
              AND type NOT IN ('parent', 'child')
              ORDER BY rel.order_seq
            ) a
          ), ''),
      'case.relations', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
              FROM view_case_relationship rel
              WHERE z.id = rel.case_id
              ORDER BY rel.order_seq
            ) a
          ), ''),
        'case.relations_complete', CASE WHEN zm.relations_complete = true THEN 'Ja' ELSE 'Nee' END
    )
    -- The actual attributes
    ||
    (
      SELECT COALESCE (
        jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value ::jsonb)
        FILTER (WHERE ca.magic_string is not null),
        '{}'::jsonb) as attributes
      FROM
        case_attributes_v0 ca
      WHERE
      ca.case_id = z.id
    )

    as values,

    -- static values
    'case' as object_type,
    '{}'::text[] as related_objects

  FROM zaak z
  JOIN
    zaak_meta zm
  ON
    z.id = zm.zaak_id
  LEFT JOIN
    zaaktype zt
  ON
    z.zaaktype_id = zt.id
  LEFT JOIN
    zaaktype_node ztn
  ON
    (z.zaaktype_node_id = ztn.id AND zt.id = ztn.zaaktype_id)
  LEFT JOIN
    zaaktype_resultaten ztr
  ON
    z.resultaat_id = ztr.id
  LEFT JOIN
    bibliotheek_categorie bc
  ON
    zt.bibliotheek_categorie_id = bc.id
  LEFT JOIN
    zaaktype_definitie ztd
  ON
    ztn.zaaktype_definitie_id = ztd.id
  LEFT JOIN
    groups gr
  ON
    z.route_ou = gr.id
  LEFT JOIN
    subject assignee
  ON
    (z.assignee_v1_json->>'reference')::uuid = assignee.uuid
  LEFT JOIN groups gassign
  ON
    assignee.group_ids[1] = gassign.id
  LEFT JOIN
    subject coordinator
  ON
    (z.coordinator_v1_json->>'reference')::uuid = coordinator.uuid
  LEFT JOIN
    roles ro
  ON
    z.route_role = ro.id
  LEFT JOIN
    zaak parent
  ON
    z.pid = parent.id
  LEFT JOIN
    zaak_betrokkenen requestor
  ON
    (z.aanvrager = requestor.id and z.id = requestor.zaak_id)
  LEFT JOIN
    zaak_betrokkenen recipient
  ON
    (z.id = recipient.zaak_id AND recipient.rol = 'Ontvanger')
  LEFT JOIN
    result_preservation_terms rpt
  ON
    ztr.bewaartermijn = rpt.code
  LEFT JOIN
    zaak_bag case_location
  ON
    (case_location.id = z.locatie_zaak and z.id = case_location.zaak_id)
  LEFT JOIN
    zaaktype_status zts_next
  ON
    ( z.zaaktype_node_id = zts_next.zaaktype_node_id
      AND zts_next.status = z.milestone + 1)
  LEFT JOIN
    zaaktype_status zts
  ON
    ( z.zaaktype_node_id = zts.zaaktype_node_id
      AND zts.status = z.milestone)
  ;

COMMIT;
