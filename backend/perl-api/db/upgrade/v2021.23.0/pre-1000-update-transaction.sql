
-- Optional run this, or don't. But it fixes the layout
BEGIN;
  update transaction set preview_data = concat('[', preview_data, ']') where preview_data like '{%' and preview_data != '{}';
COMMIT;
