BEGIN;

  UPDATE interface SET interface_config = interface_config::jsonb || jsonb_build_object('timeout', '25')
    WHERE module = 'xential'
    AND (interface_config::jsonb->>'timeout')::int < 25;

COMMIT;

