
BEGIN;
  DROP VIEW IF EXISTS case_attributes_v1 CASCADE;
  DROP VIEW IF EXISTS case_attributes_v0 CASCADE;

  CREATE VIEW case_attributes_v1 AS
    SELECT
      case_id,
      magic_string,
      library_id,
      attribute_value_to_jsonb(value, value_type) as value
    FROM
      case_attributes
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        appointment_attribute_value_to_jsonb(value, reference) as value
      FROM
        case_attributes_appointments
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        case_documents_cache.value_v1 AS value
      FROM case_documents_cache
  ;

  CREATE VIEW case_attributes_v0 AS
    SELECT
      case_id,
      magic_string,
      library_id,
      attribute_value_to_v0(value, value_type, mvp) as value
    FROM
      case_attributes
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        to_jsonb(reference::text)
      FROM
        case_attributes_appointments
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        case_documents_cache.value_v0 AS value
      FROM case_documents_cache
  ;

  CREATE VIEW case_v0 AS
  SELECT
    z.uuid AS id,
    z.id AS object_id,

    jsonb_build_object(
      'class_uuid', zt.uuid,
      'pending_changes', zm.pending_changes
    ) as case,

    -- zs::attributes and friends
    jsonb_build_object(
      'case.route_ou', z.route_ou,
      'case.route_role', z.route_role,

      'case.channel_of_contact', z.contactkanaal,
      'case.html_email_template', z.html_email_template,

      'case.current_deadline', zm.current_deadline,
      'case.date_destruction', timestamp_to_perl_datetime(z.vernietigingsdatum),
      'case.date_of_completion', timestamp_to_perl_datetime(z.afhandeldatum),
      'case.date_of_registration', timestamp_to_perl_datetime(z.registratiedatum),
      'case.date_target', CASE WHEN z.status = 'stalled' THEN 'Opgeschort' ELSE timestamp_to_perl_datetime(z.streefafhandeldatum) END,
      'case.deadline_timeline', zm.deadline_timeline,
      'case.stalled_since', timestamp_to_perl_datetime(zm.stalled_since),
      'case.stalled_until', timestamp_to_perl_datetime(z.stalled_until),
      'case.suspension_rationale', zm.opschorten,
      'date_created', timestamp_to_perl_datetime(z.created),
      'date_modified', timestamp_to_perl_datetime(z.last_modified),
      'case.urgency', z.urgency,
      'case.startdate', to_char(z.registratiedatum::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY'),
      'case.progress_days', get_date_progress_from_case(hstore(z)),
      'case.progress_status', z.status_percentage,
      'case.days_left', z.streefafhandeldatum::date - COALESCE(z.afhandeldatum::date, NOW()::date),
      'case.lead_time_real', z.leadtime,
      'case.destructable', is_destructable(z.vernietigingsdatum),


      'case.number_status', z.milestone,
      'case.milestone', CASE WHEN zts_next.id IS NOT NULL THEN zts.naam ELSE null END,
      'case.phase', CASE WHEN zts_next.id IS NOT NULL THEN zts_next.fase ELSE null END,

      'case.status', z.status,

      'case.subject', z.onderwerp,
      'case.subject_external', z.onderwerp_extern,

      'case.archival_state', z.archival_state,
      'case.confidentiality', get_confidential_mapping(z.confidentiality),

      'case.payment_status', get_payment_status_mapping(z.payment_status),
      'case.price', z.dutch_price,

      -- documents
      'case.documents', COALESCE((SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', ') FROM file f
        WHERE f.case_id = z.id and f.date_deleted is null and f.active_version = true), ''),

      -- attributes with files (used in e-mail tab for JS)
      'case.case_documents', COALESCE((SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', ') FROM file case_documents
        JOIN file_case_document fcd
          ON fcd.case_id = case_documents.case_id and case_documents.id = fcd.file_id
        WHERE case_documents.case_id = z.id), ''),

      -- cached values which potentially block phase transitions
      'case.num_unaccepted_updates', zm.unaccepted_attribute_update_count,
      'case.num_unread_communication', zm.unread_communication_count,
      'case.num_unaccepted_files', zm.unaccepted_files_count,

      -- static values
      'case.aggregation_scope', 'Dossier'
    )
    -- Case/casetype result blob
    || jsonb_build_object(
      'case.result', z.resultaat,
      'case.result_description', ztr.label,
      'case.result_explanation', ztr.comments,
      'case.result_id', ztr.id,
      'case.result_origin', ztr.properties::jsonb->'herkomst',
      'case.result_process_term', ztr.properties::jsonb->'procestermijn',
      'case.result_process_type_description', ztr.properties::jsonb->'procestype_omschrijving',
      'case.result_process_type_explanation', ztr.properties::jsonb->'procestype_toelichting',
      'case.result_process_type_generic', ztr.properties::jsonb->'procestype_generiek',
      'case.result_process_type_name', ztr.properties::jsonb->'procestype_naam',
      'case.result_process_type_number', ztr.properties::jsonb->'procestype_nummer',
      'case.result_process_type_object', ztr.properties::jsonb->'procestype_object',
      'case.result_selection_list_number', ztr.properties::jsonb->'selectielijst_nummer',
      'case.retention_period_source_date', ztr.ingang,
      'case.type_of_archiving', ztr.archiefnominatie,
      'case.period_of_preservation', rpt.label,
      'case.period_of_preservation_active', CASE WHEN ztr.trigger_archival = true THEN 'Ja' ELSE 'Nee' END,
      'case.type_of_archiving', ztr.archiefnominatie,
      -- TODO: Pick one
      'case.selection_list', ztr.selectielijst,
      'case.active_selection_list', ztr.selectielijst
    )
    -- ztd stuff
    || jsonb_build_object(
      'case.lead_time_legal', ztd.afhandeltermijn,
      'case.lead_time_service', ztd.servicenorm,
      'case.principle_national', ztd.grondslag
    )
    -- Case.number shizzle
    || jsonb_build_object(
      'case.custom_number', CASE WHEN char_length(z.prefix) > 0 THEN CONCAT(z.prefix, '-', z.id) ELSE z.id::text END,
      'case.number', z.id,
      'case.number_master', z.number_master
    )
    -- case.casetype stuff
    || jsonb_build_object(
      'case.casetype', zt.uuid,
      'case.casetype.id', zt.id,
      'case.casetype.generic_category', bc.naam,
      'case.casetype.initiator_type', ztd.handelingsinitiator,
      'case.casetype.price.web', ztd.pdc_tarief,
      'case.casetype.process_description', ztd.procesbeschrijving,
      'case.casetype.publicity', ztd.openbaarheid,
      -- incorrectly named
      'case.casetype.department', gr.name,
      'case.casetype.route_role', ro.name
    ) || ztn.v0_json
    -- relations
    || jsonb_build_object(
      'case.parent_uuid', COALESCE(parent.uuid::text, '')
    )
    || jsonb_build_object(
    -- assignee
      'assignee', z.assignee_v1_json->>'reference',
      'case.assignee', z.assignee_v1_json->>'preview',
      'case.assignee.uuid', z.assignee_v1_json->>'reference',
      'case.assignee.email', z.assignee_v1_json->'instance'->'subject'->'instance'->>'email_address',
      'case.assignee.initials', z.assignee_v1_json->'instance'->'subject'->'instance'->>'initials',
      'case.assignee.last_name', z.assignee_v1_json->'instance'->'subject'->'instance'->>'surname',
      'case.assignee.first_names', z.assignee_v1_json->'instance'->'subject'->'instance'->>'first_names',
      'case.assignee.phone_number', z.assignee_v1_json->'instance'->'subject'->'instance'->>'phone_number',
      'case.assignee.id', (split_part(z.assignee_v1_json->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.assignee.department', gassign.name,
      'case.assignee.title', ''
    )
    || jsonb_build_object(
    -- coordinator
      'coordinator', z.coordinator_v1_json->'reference',
      'case.coordinator', z.coordinator_v1_json->'preview',
      'case.coordinator.uuid', z.coordinator_v1_json->'reference',
      'case.coordinator.email', z.coordinator_v1_json->'instance'->'subject'->'instance'->>'email_address',
      'case.coordinator.phone_number', z.coordinator_v1_json->'instance'->'subject'->'instance'->>'phone_number',
      'case.coordinator.id', (split_part(z.coordinator_v1_json->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.coordinator.title', ''
    )
    -- requestor
    || case_subject_as_v0_json(hstore(requestor), 'requestor', false)
    || case_subject_as_v0_json(hstore(requestor), 'requestor', true)
    || jsonb_build_object(
      'case.requestor.preset_client', CASE WHEN z.preset_client = true THEN 'Ja' ELSE 'Nee' END
    )
    || case_subject_as_v0_json(hstore(recipient), 'recipient', false)
    || case_subject_as_v0_json(hstore(recipient), 'recipient', true)
    -- case location
    || case_location_as_v0_json(hstore(case_location))
    -- case relationships
    || jsonb_build_object(
      'case.related_cases', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
              FROM view_case_relationship rel
              WHERE z.id = rel.case_id
              AND type NOT IN ('parent', 'child')
              ORDER BY rel.order_seq
            ) a
          ), ''),
      'case.relations', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
              FROM view_case_relationship rel
              WHERE z.id = rel.case_id
              ORDER BY rel.order_seq
            ) a
          ), ''),
        'case.relations_complete', CASE WHEN zm.relations_complete = true THEN 'Ja' ELSE 'Nee' END
    )
    -- The actual attributes
    ||
    (
      SELECT COALESCE (
        jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value ::jsonb)
        FILTER (WHERE ca.magic_string is not null),
        '{}'::jsonb) as attributes
      FROM
        case_attributes_v0 ca
      WHERE
      ca.case_id = z.id
    )

    as values,

    -- static values
    'case' as object_type,
    '{}'::text[] as related_objects

  FROM zaak z
  JOIN
    zaak_meta zm
  ON
    z.id = zm.zaak_id
  LEFT JOIN
    zaaktype zt
  ON
    z.zaaktype_id = zt.id
  LEFT JOIN
    zaaktype_node ztn
  ON
    (z.zaaktype_node_id = ztn.id AND zt.id = ztn.zaaktype_id)
  LEFT JOIN
    zaaktype_resultaten ztr
  ON
    z.resultaat_id = ztr.id
  LEFT JOIN
    bibliotheek_categorie bc
  ON
    zt.bibliotheek_categorie_id = bc.id
  LEFT JOIN
    zaaktype_definitie ztd
  ON
    ztn.zaaktype_definitie_id = ztd.id
  LEFT JOIN
    groups gr
  ON
    z.route_ou = gr.id
  LEFT JOIN
    subject assignee
  ON
    (z.assignee_v1_json->>'reference')::uuid = assignee.uuid
  LEFT JOIN groups gassign
  ON
    assignee.group_ids[1] = gassign.id
  LEFT JOIN
    subject coordinator
  ON
    (z.coordinator_v1_json->>'reference')::uuid = coordinator.uuid
  LEFT JOIN
    roles ro
  ON
    z.route_role = ro.id
  LEFT JOIN
    zaak parent
  ON
    z.pid = parent.id
  LEFT JOIN
    zaak_betrokkenen requestor
  ON
    (z.aanvrager = requestor.id and z.id = requestor.zaak_id)
  LEFT JOIN
    zaak_betrokkenen recipient
  ON
    (z.id = recipient.zaak_id AND recipient.rol = 'Ontvanger' AND recipient.deleted IS NULL)
  LEFT JOIN
    result_preservation_terms rpt
  ON
    ztr.bewaartermijn = rpt.code
  LEFT JOIN
    zaak_bag case_location
  ON
    (case_location.id = z.locatie_zaak and z.id = case_location.zaak_id)
  LEFT JOIN
    zaaktype_status zts_next
  ON
    ( z.zaaktype_node_id = zts_next.zaaktype_node_id
      AND zts_next.status = z.milestone + 1)
  LEFT JOIN
    zaaktype_status zts
  ON
    ( z.zaaktype_node_id = zts.zaaktype_node_id
      AND zts.status = z.milestone)
  ;

  CREATE VIEW case_v1 AS
  SELECT
      c.number,
      c.id,
      c.number_parent,
      c.number_master,
      c.number_previous,
      c.subject,
      c.subject_external,
      c.status,
      c.date_created,
      c.date_modified,
      c.date_destruction,
      c.date_of_completion,
      c.date_of_registration,
      c.date_target,
      c.html_email_template,
      c.payment_status,
      c.price,
      c.channel_of_contact,
      c.archival_state,
      c.confidentiality,
      c.stalled_since,
      c.stalled_until,
      c.current_deadline,
      c.deadline_timeline,
      c.result_id,
      c.result,
      c.active_selection_list,
      c.outcome,
      c.casetype,
      c.route,
      c.suspension_rationale,
      c.premature_completion_rationale,
      zts.fase::text AS phase,
      c.relations,
      c.case_relationships,
      c.requestor,
      c.assignee,
      c.coordinator,
      c.aggregation_scope,
      c.case_location,
      c.correspondence_location,
      (
          SELECT
              COALESCE(jsonb_object_agg(ca.magic_string, ca.value) FILTER (WHERE ca.magic_string IS NOT NULL), '{}'::jsonb) AS "coalesce"
          FROM
              case_attributes_v1 ca
          WHERE
              ca.case_id = c.number) AS attributes,
      (
          SELECT
              json_build_object('preview', zts.fase, 'reference', NULL::unknown, 'type', 'case/milestone', 'instance', json_build_object('date_created', timestamp_to_perl_datetime (now()), 'date_modified', timestamp_to_perl_datetime (now()), 'phase_label', CASE WHEN zts.id IS NOT NULL THEN
                  zts.fase
              ELSE
                  zts_end.fase
              END, 'phase_sequence_number', CASE WHEN zts.id IS NOT NULL THEN
                  zts.status
              ELSE
                  zts_end.status
              END, 'milestone_label', zts_previous.naam, 'milestone_sequence_number', zts_previous.status, 'last_sequence_number', zts_end.status)) AS json_build_object
          FROM
              casetype_end_status zts_end
          WHERE
              c.zaaktype_node_id = zts_end.zaaktype_node_id) AS milestone
  FROM (
      SELECT
          z.id AS number,
          z.uuid AS id,
          z.pid AS number_parent,
          z.number_master,
          z.vervolg_van AS number_previous,
          z.onderwerp AS subject,
          z.onderwerp_extern AS subject_external,
          z.status,
          z.created AS date_created,
          z.last_modified AS date_modified,
          z.vernietigingsdatum AS date_destruction,
          z.afhandeldatum AS date_of_completion,
          z.registratiedatum AS date_of_registration,
          z.streefafhandeldatum AS date_target,
          z.html_email_template,
          z.payment_status,
          z.dutch_price AS price,
          z.contactkanaal AS channel_of_contact,
          z.archival_state,
          z.zaaktype_node_id,
          z.milestone AS raw_milestone,
          get_confidential_mapping (z.confidentiality) AS confidentiality,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.stalled_since
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_since,
          CASE WHEN z.status = 'stalled'::text THEN
              z.stalled_until
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_until,
          zm.current_deadline,
          zm.deadline_timeline,
          ztr.id AS result_id,
          ztr.resultaat AS result,
          ztr.selectielijst AS active_selection_list,
          CASE WHEN ztr.id IS NOT NULL THEN
              json_build_object('reference', NULL::unknown, 'type', 'case/result', 'preview', CASE WHEN ztr.label IS NOT NULL THEN
                      ztr.label
                  ELSE
                      ztr.resultaat
                  END, 'instance', json_build_object('date_created', timestamp_to_perl_datetime (ztr.created::timestamp with time zone), 'date_modified', timestamp_to_perl_datetime (ztr.last_modified::timestamp with time zone), 'archival_type', ztr.archiefnominatie, 'dossier_type', ztr.dossiertype, 'name', CASE WHEN ztr.label IS NOT NULL THEN
                          ztr.label
                      ELSE
                          ztr.resultaat
                      END, 'result', ztr.resultaat, 'retention_period', ztr.bewaartermijn, 'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                          NULL
                      ELSE
                          ztr.selectielijst
                      END, 'selection_list_start', ztr.selectielijst_brondatum, 'selection_list_end', ztr.selectielijst_einddatum))
          ELSE
              NULL
          END AS outcome,
          json_build_object('preview', ct_ref.title, 'reference', ct_ref.uuid, 'instance', json_build_object('version', ct_ref.version, 'name', ct_ref.title), 'type', 'casetype') AS casetype,
          json_build_object('preview', (gr.name || ', '::text) || role.name, 'reference', NULL::unknown, 'type', 'case/route', 'instance', json_build_object('date_created', timestamp_to_perl_datetime (now()), 'date_modified', timestamp_to_perl_datetime (now()), 'group', gr.v1_json, 'role', role.v1_json)) AS route,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.opschorten
          ELSE
              NULL::character varying
          END AS suspension_rationale,
          CASE WHEN z.status = 'resolved'::text THEN
              zm.afhandeling
          ELSE
              NULL::character varying
          END AS premature_completion_rationale,
          json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb))) AS relations,
          json_build_object(
            'parent', parent.relationship->0,

            'continuation', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(continuation.relationship, '[]'::jsonb)
              )
            ),
            'child', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(children.relationship, '[]'::jsonb)
              )
            ),
            'plain', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(crp.relationship, '[]'::jsonb)
              )
            )
          ) AS case_relationships,

          z.requestor_v1_json AS requestor,
          z.assignee_v1_json AS assignee,
          z.coordinator_v1_json AS coordinator,
          'Dossier'::text AS aggregation_scope,
          NULL::text AS case_location,
          NULL::text AS correspondence_location
      FROM
          zaak z
      LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
      LEFT JOIN casetype_v1_reference ct_ref ON z.zaaktype_node_id = ct_ref.casetype_node_id
      LEFT JOIN zaaktype_resultaten ztr ON z.resultaat_id = ztr.id
      LEFT JOIN GROUPS gr ON z.route_ou = gr.id
      LEFT JOIN roles ROLE ON z.route_role = role.id
      LEFT JOIN view_case_relationship_v1_json crp ON z.id = crp.case_id
          AND crp.type = 'plain'
      LEFT JOIN view_case_relationship_v1_json continuation ON z.id = continuation.case_id
          AND continuation.type = 'initiator'
      LEFT JOIN view_case_relationship_v1_json children ON z.id = children.case_id
          AND children.type = 'parent'
      LEFT JOIN view_case_relationship_v1_json parent ON z.id = parent.case_id
          AND parent.type = 'child'
  WHERE
      z.deleted IS NULL) AS c
      LEFT JOIN zaaktype_status zts ON c.zaaktype_node_id = zts.zaaktype_node_id
          AND zts.status = (c.raw_milestone + 1)
      LEFT JOIN zaaktype_status zts_previous ON c.zaaktype_node_id = zts_previous.zaaktype_node_id
          AND zts_previous.status = c.raw_milestone;

COMMIT;
