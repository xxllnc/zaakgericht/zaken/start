
BEGIN;

  ALTER TABLE public.thread_message_external DROP CONSTRAINT external_message_type;
  ALTER TABLE public.thread_message_external ADD CONSTRAINT external_message_type CHECK ("type" IN ('pip', 'email', 'postex'));

COMMIT;
