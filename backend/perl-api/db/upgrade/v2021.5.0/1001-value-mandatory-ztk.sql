BEGIN;

  ALTER TABLE zaaktype_kenmerken
  ADD COLUMN mandatory BOOLEAN default false NOT NULL;
  UPDATE zaaktype_kenmerken SET mandatory = true where value_mandatory = 1;
  ALTER TABLE zaaktype_kenmerken DROP COLUMN value_mandatory;
  ALTER TABLE zaaktype_kenmerken RENAME COLUMN mandatory TO value_mandatory;

COMMIT;
