BEGIN;

  ALTER TABLE zaak ADD COLUMN number_master int;

  ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_number_master_fk FOREIGN KEY (number_master) REFERENCES public.zaak(id);

COMMIT;
