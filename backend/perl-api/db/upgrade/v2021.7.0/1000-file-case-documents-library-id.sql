
BEGIN;

  ALTER TABLE file_case_document ADD COLUMN bibliotheek_kenmerken_id INT REFERENCES bibliotheek_kenmerken(id);

  UPDATE file_case_document fcd SET bibliotheek_kenmerken_id = bk.id
  FROM zaaktype_kenmerken ztk
  JOIN bibliotheek_kenmerken bk
    ON ztk.bibliotheek_kenmerken_id = bk.id
    WHERE fcd.case_document_id = ztk.id
  ;

  ALTER TABLE file_case_document ALTER COLUMN bibliotheek_kenmerken_id SET NOT NULL;

  ALTER TABLE file_case_document ADD COLUMN case_id INT REFERENCES zaak(id);

  UPDATE file_case_document fcd SET case_id = z.id
  FROM file f
  JOIN zaak z
    ON z.id = f.case_id
    WHERE fcd.file_id = f.id
  ;

COMMIT;

BEGIN;
  ALTER TABLE file_case_document ALTER COLUMN case_id SET NOT NULL;
COMMIT;

