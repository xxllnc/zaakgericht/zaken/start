BEGIN;

  ALTER TABLE zaaktype_node ALTER COLUMN contact_info_intake SET DEFAULT false;

  UPDATE zaaktype_node set contact_info_intake = FALSE WHERE contact_info_intake IS NULL;

  ALTER TABLE zaaktype_node ALTER COLUMN contact_info_intake SET NOT NULL;

COMMIT;
