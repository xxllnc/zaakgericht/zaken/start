
BEGIN;

  TRUNCATE TABLE queue_no_partition;

  INSERT INTO public.queue_no_partition
    SELECT * FROM queue;

  DROP TABLE queue;
  ALTER TABLE queue_no_partition RENAME TO queue;
  ALTER TABLE queue ADD CHECK (status IN ('pending', 'running', 'finished', 'failed', 'waiting', 'cancelled', 'postponed'));

COMMIT;
