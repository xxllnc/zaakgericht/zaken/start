BEGIN;
ALTER TABLE zaaktype_resultaten
    ADD COLUMN uuid UUID NOT NULL UNIQUE DEFAULT uuid_generate_v4 ();
COMMIT;

