
BEGIN;

update zaak_meta zm
  set index_hstore =
    zm.index_hstore ||
    hstore('case.case_location.nummeraanduiding', od.index_hstore->'case.case_location.nummeraanduiding') ||
    hstore('case.case_location.ligplaats', od.index_hstore->'case.case_location.ligplaats') ||
    hstore('case.case_location.openbareruimte', od.index_hstore->'case.case_location.openbareruimte') ||
    hstore('case.case_location.pand', od.index_hstore->'case.case_location.pand') ||
    hstore('case.case_location.verblijfsobject', od.index_hstore->'case.case_location.verblijfsobject') ||
    hstore('case.case_location.woonplaats', od.index_hstore->'case.case_location.woonplaats')
  FROM object_data od
  WHERE od.object_class = 'case'
  AND od.object_id = zm.zaak_id
  AND (
    od.index_hstore->'case.case_location.nummeraanduiding' is not null
    OR
    od.index_hstore->'case.case_location.openbareruimte' is not null
    OR
    od.index_hstore->'case.case_location.ligplaats' is not null
    OR
    od.index_hstore->'case.case_location.pand' is not null
    OR
    od.index_hstore->'case.case_location.verblijfsobject' is not null
    OR
    od.index_hstore->'case.case_location.woonplaats' is not null
  );


COMMIT;
