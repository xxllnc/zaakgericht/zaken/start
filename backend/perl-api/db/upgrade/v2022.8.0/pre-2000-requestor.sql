BEGIN;

CREATE OR REPLACE FUNCTION public.person_as_v0_json(
	np hstore,
	type text,
	OUT json jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
  DECLARE
    key text;
    gender text;
    salutation text;
    salutation1 text;
    salutation2 text;

    address_inactive text;
    address_active text;
    house_number text;

    investigation boolean;
    active boolean;
    tmp text;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    investigation := false;

    FOREACH tmp IN ARRAY ARRAY['persoon', 'huwelijk', 'overlijden', 'verblijfplaats']
    LOOP
      tmp := CONCAT('onderzoek_', tmp);
      IF (np->tmp)::boolean = true THEN
        investigation := true;
        EXIT;
      END IF;
    END LOOP;

    gender := LOWER(np->'geslachtsaanduiding');

    IF gender = 'm'
    THEN
      gender := 'man';
      salutation := 'meneer';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSIF gender = 'v'
    THEN
      gender := 'vrouw';
      salutation := 'mevrouw';
      salutation1 := 'mevrouw';
      salutation2 := 'mevrouw';
    ELSE
      gender := '';
      salutation := '';
      salutation1 := '';
      salutation2 := '';
    END IF;

    house_number := complete_house_number(
      np->'street_number',
      np->'street_number_letter',
      np->'street_number_suffix'
    );

    SELECT INTO json jsonb_build_object(

      /* This is so wrong on so many levels:
       *
       * This needs to be NULL if the country_code = 6030 This can only
       * be used when address_type = 'W'. Yet, we allow 'B' and
       * incorrect data. For now, respect the old incorrect API.
       */
      CONCAT(key, 'foreign_residence_address_line1'), np->'foreign_address_line_1',
      CONCAT(key, 'foreign_residence_address_line2'), np->'foreign_address_line_2',
      CONCAT(key, 'foreign_residence_address_line3'), np->'foreign_address_line_3',

      /* This is also wrong.
       *
       * This needs to be NULL if the country_code != 6030
       */
      CONCAT(key, 'street'), np->'street',
      CONCAT(key, 'zipcode'), np->'zipcode',
      CONCAT(key, 'place_of_residence'), np->'city',
      CONCAT(key, 'house_number'), house_number
    );

    IF np->'address_type' = 'W' THEN
      address_active = 'residence';
      address_inactive = 'correspondence';
    ELSE
      address_inactive = 'residence';
      address_active = 'correspondence';
    END IF;

    SELECT INTO json json || jsonb_build_object(
      CONCAT(key, address_active, '_house_number'), house_number,
      CONCAT(key, address_active, '_place_of_residence'), np->'city',
      CONCAT(key, address_active, '_street'), np->'street',
      CONCAT(key, address_active, '_zipcode'), np->'zipcode',

      CONCAT(key, address_inactive, '_house_number'), '',
      CONCAT(key, address_inactive, '_place_of_residence'), null,
      CONCAT(key, address_inactive, '_street'), null,
      CONCAT(key, address_inactive, '_zipcode'), null
    );

    -- active does not exists in the gm_ table of NP
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;
    IF np->'active' IS NOT NULL
    THEN
      active := (np->'active')::boolean;
    END IF;

    IF np->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, np->'uuid',
      CONCAT(key, 'type'), 'Natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'natuurlijk_persoon',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,
      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), np->'uuid',
      CONCAT(key, 'a_number'), np->'a_nummer',
      CONCAT(key, 'bsn'), lpad(np->'burgerservicenummer', 9, '0'),

      CONCAT(key, 'family_name'), np->'geslachtsnaam',
      CONCAT(key, 'first_names'), np->'voornamen',
      CONCAT(key, 'surname'), np->'naamgebruik',
      CONCAT(key, 'full_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'display_name'), get_display_name_for_person(np),
      CONCAT(key, 'gender'), gender,
      CONCAT(key, 'salutation'), salutation,
      CONCAT(key, 'salutation1'), salutation1,
      CONCAT(key, 'salutation2'), salutation2,

      CONCAT(key, 'initials'), np->'voorletters',
      CONCAT(key, 'is_secret'), NULLIF(BTRIM(np->'indicatie_geheim'), '')::INT::BOOLEAN,
      CONCAT(key, 'naamgebruik'), np->'naamgebruik',
      CONCAT(key, 'name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'noble_title'), np->'adellijke_titel',
      CONCAT(key, 'place_of_birth'), np->'geboorteplaats',
      CONCAT(key, 'preferred_contact_channel'), np->'preferred_contact_channel',

      CONCAT(key, 'investigation'), investigation,
      CONCAT(key, 'surname_prefix'), np->'voorvoegsel',

      CONCAT(key, 'country_of_birth'), np->'geboorteland',
      CONCAT(key, 'country_of_residence'), np->'country',
      CONCAT(key, 'date_of_birth'), (np->'geboortedatum')::date,
      CONCAT(key, 'date_of_death'), (np->'datum_overlijden')::date,
      CONCAT(key, 'date_of_divorce'), (np->'datum_huwelijk_ontbinding')::date,
      CONCAT(key, 'date_of_marriage'), (np->'datum_huwelijk')::date,

      CONCAT(key, 'email'), np->'email_address',
      CONCAT(key, 'mobile_number'), np->'mobile',
      CONCAT(key, 'phone_number'), np->'phone_number',
      CONCAT(key, 'has_correspondence_address'), CASE WHEN np->'address_type' = 'W' THEN false ELSE true END,

      -- Not relevant for NP
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'password'), null
    );

  END;
  
$BODY$;

COMMIT;
