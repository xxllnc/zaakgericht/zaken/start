BEGIN;

 ALTER TABLE saved_search ADD COLUMN date_created TIMESTAMP WITH TIME ZONE;

 ALTER TABLE saved_search ADD COLUMN date_updated TIMESTAMP WITH TIME ZONE;

 ALTER TABLE saved_search ADD COLUMN updated_by integer REFERENCES subject ("id");

COMMIT;