BEGIN;

  -- Stuf we no longer care about
  DROP FUNCTION IF EXISTS bag_attribute_value_to_jsonb(text[], text, int);
  DROP TRIGGER IF EXISTS insert_leading_qitem ON "queue";
  DROP FUNCTION IF EXISTS set_leading_qitem;

  -- These views should not exist, if they do, remove them
  DROP VIEW IF EXISTS case_relationship_json_view;
  DROP VIEW IF EXISTS case_relationship_view;
  DROP VIEW IF EXISTS case_number_master;
  DROP VIEW IF EXISTS case_plain_relationship_view;
  DROP VIEW IF EXISTS case_v1_subjects;

  -- The big views
  DROP VIEW IF EXISTS case_acl;
  DROP VIEW IF EXISTS case_v1 CASCADE;
  DROP VIEW IF EXISTS case_v0 CASCADE;
  DROP VIEW IF EXISTS view_case_v2 CASCADE;

  -- dependent views
  DROP VIEW IF EXISTS view_case_relationship CASCADE;
  DROP VIEW IF EXISTS case_attributes_appointments CASCADE;
  DROP VIEW IF EXISTS case_attributes;
  DROP VIEW IF EXISTS case_documents CASCADE;
  DROP VIEW IF EXISTS case_documents_view;

  ALTER SEQUENCE zaak_id_seq AS BIGINT;
  ALTER TABLE zaak ALTER id TYPE BIGINT;

  DROP TRIGGER IF EXISTS update_case_relations_complete ON "zaak";
  CREATE OR REPLACE FUNCTION update_case_relations_complete_for_case() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    DECLARE
      open_case bigint;
      complete boolean;

    BEGIN

      -- Only for cases which have a parent
      IF NEW.pid IS NULL
      THEN
        RETURN NEW;
      END IF;

      complete := true;

      IF OLD.pid IS NULL AND NEW.status != 'resolved'
      THEN
        complete := false;
      ELSIF OLD.status = 'resolved' and NEW.status != 'resolved'
      THEN
        complete := false;
      ELSIF OLD.status != 'resolved' and NEW.status = 'resolved'
      THEN
        SELECT INTO open_case id FROM zaak WHERE status != 'resolved' AND pid = NEW.pid LIMIT 1;
        IF open_case IS NOT NULL
        THEN
          complete := false;
        END IF;
      END IF;

      UPDATE zaak_meta SET relations_complete = complete WHERE zaak_id = NEW.pid;

      RETURN NEW;

    END;
  $$;

  ALTER TABLE zaak ALTER pid TYPE BIGINT;
  ALTER TABLE zaak ALTER number_master TYPE BIGINT;
  ALTER TABLE zaak ALTER vervolg_van TYPE BIGINT;

  CREATE TRIGGER update_case_relations_complete
  AFTER INSERT OR UPDATE
  OF pid, status
  ON zaak
  FOR EACH ROW
  EXECUTE PROCEDURE update_case_relations_complete_for_case ();

  ALTER TABLE zaak_authorisation ALTER zaak_id TYPE BIGINT;
  ALTER TABLE zaak_betrokkenen ALTER zaak_id TYPE BIGINT;
  ALTER TABLE zaak_kenmerk ALTER zaak_id TYPE BIGINT;
  ALTER TABLE zaak_meta ALTER zaak_id TYPE BIGINT;
  ALTER TABLE zaak_bag ALTER zaak_id TYPE BIGINT;
  ALTER TABLE logging ALTER zaak_id TYPE BIGINT;
  ALTER TABLE zaak_subcase ALTER relation_zaak_id TYPE BIGINT;
  ALTER TABLE zaak_subcase ALTER zaak_id TYPE BIGINT;

  ALTER TABLE case_relation ALTER case_id_a TYPE BIGINT;
  ALTER TABLE case_relation ALTER case_id_b TYPE BIGINT;

  ALTER TABLE case_action ALTER case_id TYPE BIGINT;
  ALTER TABLE case_documents_cache ALTER case_id TYPE BIGINT;
  ALTER TABLE file ALTER case_id TYPE BIGINT;
  ALTER TABLE file_case_document ALTER case_id TYPE BIGINT;
  ALTER TABLE checklist ALTER case_id TYPE BIGINT;
  ALTER TABLE custom_object_relationship ALTER related_case_id TYPE BIGINT;
  ALTER TABLE contactmoment ALTER case_id TYPE BIGINT;
  ALTER TABLE directory ALTER case_id TYPE BIGINT;
  ALTER TABLE scheduled_jobs ALTER case_id TYPE BIGINT;

  DROP TRIGGER IF EXISTS update_zaak_meta_count ON thread;
  ALTER TABLE thread ALTER case_id TYPE BIGINT;
  CREATE TRIGGER update_zaak_meta_count
      AFTER INSERT OR UPDATE OF case_id, unread_employee_count ON thread
      FOR EACH ROW
      WHEN (NEW.case_id IS NOT NULL)
      EXECUTE PROCEDURE zaak_meta_update_unread_count();

  CREATE VIEW case_acl AS
      SELECT
          z.id AS case_id,
          z.uuid AS case_uuid,
          cam.key AS permission,
          s.id AS subject_id,
          s.uuid AS subject_uuid,
          z.zaaktype_id AS casetype_id
      FROM
          zaak z
          JOIN zaaktype_authorisation za ON za.zaaktype_id = z.zaaktype_id
              AND za.confidential = z.confidential
          JOIN (subject_position_matrix spm
              JOIN subject s ON spm.subject_id = s.id) ON spm.role_id = za.role_id
              AND spm.group_id = za.ou_id
          JOIN case_authorisation_map cam ON za.recht = cam.legacy_key
      UNION ALL
      SELECT
          z.id AS case_id,
          z.uuid AS case_uuid,
          za.capability AS permission,
          s.id AS subject_id,
          s.uuid AS subject_uuid,
          z.zaaktype_id AS casetype_id
      FROM
          zaak z
          JOIN zaak_authorisation za ON za.zaak_id = z.id
              AND za.entity_type = 'position'::text
          JOIN (subject_position_matrix spm
              JOIN subject s ON spm.subject_id = s.id) ON spm."position" = za.entity_id
      UNION ALL
      SELECT
          z.id AS case_id,
          z.uuid AS case_uuid,
          za.capability AS permission,
          s.id AS subject_id,
          s.uuid AS subject_uuid,
          z.zaaktype_id AS casetype_id
      FROM
          zaak z
          JOIN zaak_authorisation za ON za.zaak_id = z.id
              AND za.entity_type = 'user'::text
          JOIN subject s ON s.username::text = za.entity_id;

  CREATE VIEW case_documents AS
   SELECT z.id AS case_id,
    ARRAY( SELECT f.id::character varying AS id
           FROM file_case_document fcd
           JOIN file f
           ON f.id = fcd.file_id
           WHERE fcd.case_id = z.id and bk.bibliotheek_kenmerken_id = fcd.bibliotheek_kenmerken_id)::text[] AS value,
    bk.magic_string,
    bk.bibliotheek_kenmerken_id AS library_id
   FROM zaak z
     JOIN zaaktype_document_kenmerken_map bk
     ON z.zaaktype_node_id = bk.zaaktype_node_id;

    CREATE VIEW case_attributes_appointments AS
    SELECT
      z.id AS case_id,
      COALESCE(od.properties, '{}') as value,
      bk.magic_string as magic_string,
      bk.id as library_id,
      od.uuid as reference
    FROM zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON
      (bk.id = ztk.bibliotheek_kenmerken_id AND bk.value_type = 'appointment')
    LEFT JOIN
      zaak_kenmerk zk
    ON
      (z.id = zk.zaak_id AND zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id)
    LEFT JOIN
      object_data od
    ON
      zk.value[1]::uuid = od.uuid
    GROUP BY 1,2,3,4,5;

    CREATE VIEW case_attributes AS
    SELECT
      z.id AS case_id,
      COALESCE(zk.value, '{}') as value,
      bk.magic_string as magic_string,
      bk.id as library_id,
      bk.value_type as value_type,
      bk.type_multiple as mvp
    FROM zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON
      (bk.id = ztk.bibliotheek_kenmerken_id AND bk.value_type NOT IN ('file', 'appointment'))
    LEFT JOIN
      zaak_kenmerk zk
    ON
      (z.id = zk.zaak_id AND zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id)
    GROUP BY 1,2,3,4,5;


  CREATE VIEW case_attributes_v1 AS
    SELECT
      case_id,
      magic_string,
      library_id,
      attribute_value_to_jsonb(value, value_type) as value
    FROM
      case_attributes
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        appointment_attribute_value_to_jsonb(value, reference) as value
      FROM
        case_attributes_appointments
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        case_documents_cache.value_v1 AS value
      FROM case_documents_cache
  ;

  CREATE VIEW case_attributes_v0 AS
    SELECT
      case_id,
      magic_string,
      library_id,
      attribute_value_to_v0(value, value_type, mvp) as value
    FROM
      case_attributes
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        to_jsonb(reference::text)
      FROM
        case_attributes_appointments
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        case_documents_cache.value_v0 AS value
      FROM case_documents_cache;

  CREATE VIEW case_v0 AS
  SELECT

    -- header info
    z.uuid AS id,
    z.id AS object_id,

    -- expensive bits
    build_case_v0_values(
      hstore(z),
      hstore(zm),
      hstore(zt),
      hstore(ztr),
      hstore(zts),
      hstore(zts_next),
      hstore(rpt),
      hstore(ztd),
      hstore(gr),
      hstore(ro),
      hstore(case_location),
      hstore(parent),
      hstore(requestor),
      hstore(recipient),
      bc.naam,
      gassign.name,
      ztn.v0_json
    ) as values,

    build_case_v0_case(hstore(zt), hstore(zm)) as case,

    -- static values
    '{}'::text[] as related_objects,
    'case' as object_type
  FROM zaak z
  JOIN
    zaak_meta zm
  ON
    z.id = zm.zaak_id
  JOIN
    zaaktype zt
  ON
    z.zaaktype_id = zt.id
  INNER JOIN
    zaaktype_node ztn
  ON
    z.zaaktype_node_id = ztn.id
  LEFT JOIN
    zaaktype_resultaten ztr
  ON
    z.resultaat_id = ztr.id
  LEFT JOIN
    zaaktype_status zts
  ON
    ( z.zaaktype_node_id = zts.zaaktype_node_id
      AND zts.status = z.milestone)
  LEFT JOIN
    zaaktype_status zts_next
  ON
    ( z.zaaktype_node_id = zts_next.zaaktype_node_id
      AND zts_next.status = z.milestone + 1)
  LEFT JOIN
    result_preservation_terms rpt
  ON
    ztr.bewaartermijn = rpt.code
  LEFT JOIN
    bibliotheek_categorie bc
  ON
    zt.bibliotheek_categorie_id = bc.id
  LEFT JOIN
    zaaktype_definitie ztd
  ON
    ztn.zaaktype_definitie_id = ztd.id
  LEFT JOIN
    groups gr
  ON
    z.route_ou = gr.id
  LEFT JOIN
    roles ro
  ON
    z.route_role = ro.id
  LEFT JOIN
    zaak_bag case_location
  ON
    (case_location.id = z.locatie_zaak and z.id = case_location.zaak_id)
  LEFT JOIN
    subject assignee
  ON
    z.behandelaar_gm_id = assignee.id
  LEFT JOIN groups gassign
  ON
    assignee.group_ids[1] = gassign.id
  LEFT JOIN
    zaak parent
  ON
    z.pid = parent.id
  LEFT JOIN
    zaak_betrokkenen requestor
  ON
    (z.aanvrager = requestor.id and z.id = requestor.zaak_id and requestor.deleted IS NULL)
  LEFT JOIN zaak_betrokkenen recipient ON recipient.id = (
      select id from zaak_betrokkenen zb
      where z.id = zb.zaak_id AND zb.rol = 'Ontvanger' AND zb.deleted IS NULL
      order by id asc limit 1
  )

  WHERE z.deleted IS NULL;

  CREATE VIEW view_case_relationship
  AS
   SELECT cr.case_id_a AS case_id,
      cr.case_id_b AS relation_id,
      cr.type_a AS type,
      z.uuid AS relation_uuid,
      cr.order_seq_b as order_seq
     FROM case_relation cr
       JOIN zaak z ON z.id = cr.case_id_b AND z.deleted IS NULL
  UNION
   SELECT cr.case_id_b AS case_id,
      cr.case_id_a AS relation_id,
      cr.type_b AS type,
      z.uuid AS relation_uuid,
      cr.order_seq_a as order_seq
     FROM case_relation cr
       JOIN zaak z ON z.id = cr.case_id_a AND z.deleted IS NULL
  UNION
   SELECT parent.id AS case_id,
      child.id AS relation_id,
      'parent'::character varying AS type,
      child.uuid AS relation_uuid,
      0 as order_seq
     FROM zaak parent
       JOIN zaak child ON child.pid = parent.id
    WHERE child.deleted IS NULL AND parent.deleted IS NULL
  UNION
   SELECT child.id AS case_id,
      parent.id AS relation_id,
      'child'::character varying AS type,
      parent.uuid AS relation_uuid,
      0 as order_seq
     FROM zaak parent
       JOIN zaak child ON child.pid = parent.id
    WHERE child.deleted IS NULL AND parent.deleted IS NULL;

  CREATE VIEW view_case_relationship_v1_json AS
  SELECT view_case_relationship.case_id,
    view_case_relationship.type,
    jsonb_agg(json_build_object('type', 'case', 'reference', view_case_relationship.relation_uuid) order by view_case_relationship.order_seq) AS relationship
   FROM view_case_relationship
  GROUP BY view_case_relationship.case_id, view_case_relationship.type
  ;


  CREATE VIEW case_v1 AS
  SELECT
      c.number,
      c.id,
      c.number_parent,
      c.number_master,
      c.number_previous,
      c.subject,
      c.subject_external,
      c.status,
      c.date_created,
      c.date_modified,
      c.date_destruction,
      c.date_of_completion,
      c.date_of_registration,
      c.date_target,
      c.html_email_template,
      c.payment_status,
      c.price,
      c.channel_of_contact,
      c.archival_state,
      c.confidentiality,
      c.stalled_since,
      c.stalled_until,
      c.current_deadline,
      c.deadline_timeline,
      c.result_id,
      c.result,
      c.active_selection_list,
      c.outcome,
      c.casetype,
      c.route,
      c.suspension_rationale,
      c.premature_completion_rationale,
      zts.fase::text AS phase,
      c.relations,
      c.case_relationships,
      c.requestor,
      c.assignee,
      c.coordinator,
      c.aggregation_scope,
      c.case_location,
      c.correspondence_location,
      (
          SELECT
              COALESCE(jsonb_object_agg(ca.magic_string, ca.value) FILTER (WHERE ca.magic_string IS NOT NULL), '{}'::jsonb) AS "coalesce"
          FROM
              case_attributes_v1 ca
          WHERE
              ca.case_id = c.number) AS attributes,
      (
          SELECT
              json_build_object('preview', zts.fase, 'reference', NULL::unknown, 'type', 'case/milestone', 'instance', json_build_object('date_created', timestamp_to_perl_datetime (now()), 'date_modified', timestamp_to_perl_datetime (now()), 'phase_label', CASE WHEN zts.id IS NOT NULL THEN
                  zts.fase
              ELSE
                  zts_end.fase
              END, 'phase_sequence_number', CASE WHEN zts.id IS NOT NULL THEN
                  zts.status
              ELSE
                  zts_end.status
              END, 'milestone_label', zts_previous.naam, 'milestone_sequence_number', zts_previous.status, 'last_sequence_number', zts_end.status)) AS json_build_object
          FROM
              casetype_end_status zts_end
          WHERE
              c.zaaktype_node_id = zts_end.zaaktype_node_id) AS milestone
  FROM (
      SELECT
          z.id AS number,
          z.uuid AS id,
          z.pid AS number_parent,
          z.number_master,
          z.vervolg_van AS number_previous,
          z.onderwerp AS subject,
          z.onderwerp_extern AS subject_external,
          z.status,
          z.created AS date_created,
          z.last_modified AS date_modified,
          z.vernietigingsdatum AS date_destruction,
          z.afhandeldatum AS date_of_completion,
          z.registratiedatum AS date_of_registration,
          z.streefafhandeldatum AS date_target,
          z.html_email_template,
          z.payment_status,
          z.dutch_price AS price,
          z.contactkanaal AS channel_of_contact,
          z.archival_state,
          z.zaaktype_node_id,
          z.milestone AS raw_milestone,
          get_confidential_mapping (z.confidentiality) AS confidentiality,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.stalled_since
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_since,
          CASE WHEN z.status = 'stalled'::text THEN
              z.stalled_until
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_until,
          zm.current_deadline,
          zm.deadline_timeline,
          ztr.id AS result_id,
          ztr.resultaat AS result,
          ztr.selectielijst AS active_selection_list,
          CASE WHEN ztr.id IS NOT NULL THEN
              json_build_object('reference', NULL::unknown, 'type', 'case/result', 'preview', CASE WHEN ztr.label IS NOT NULL THEN
                      ztr.label
                  ELSE
                      ztr.resultaat
                  END, 'instance', json_build_object('date_created', timestamp_to_perl_datetime (ztr.created::timestamp with time zone), 'date_modified', timestamp_to_perl_datetime (ztr.last_modified::timestamp with time zone), 'archival_type', ztr.archiefnominatie, 'dossier_type', ztr.dossiertype, 'name', CASE WHEN ztr.label IS NOT NULL THEN
                          ztr.label
                      ELSE
                          ztr.resultaat
                      END, 'result', ztr.resultaat, 'retention_period', ztr.bewaartermijn, 'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                          NULL
                      ELSE
                          ztr.selectielijst
                      END, 'selection_list_start', ztr.selectielijst_brondatum, 'selection_list_end', ztr.selectielijst_einddatum))
          ELSE
              NULL
          END AS outcome,
          json_build_object('preview', ct_ref.title, 'reference', ct_ref.uuid, 'instance', json_build_object('version', ct_ref.version, 'name', ct_ref.title), 'type', 'casetype') AS casetype,
          json_build_object('preview', (gr.name || ', '::text) || role.name, 'reference', NULL::unknown, 'type', 'case/route', 'instance', json_build_object('date_created', timestamp_to_perl_datetime (now()), 'date_modified', timestamp_to_perl_datetime (now()), 'group', gr.v1_json, 'role', role.v1_json)) AS route,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.opschorten
          ELSE
              NULL::character varying
          END AS suspension_rationale,
          CASE WHEN z.status = 'resolved'::text THEN
              zm.afhandeling
          ELSE
              NULL::character varying
          END AS premature_completion_rationale,
          json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb))) AS relations,
          json_build_object(
            'parent', parent.relationship->0,

            'continuation', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(continuation.relationship, '[]'::jsonb)
              )
            ),
            'child', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(children.relationship, '[]'::jsonb)
              )
            ),
            'plain', json_build_object(
              'type', 'set',
              'instance', json_build_object(
                'rows', COALESCE(crp.relationship, '[]'::jsonb)
              )
            )
          ) AS "case_relationships",

          z.requestor_v1_json AS requestor,
          z.assignee_v1_json AS assignee,
          z.coordinator_v1_json AS coordinator,
          'Dossier'::text AS aggregation_scope,
          NULL::text AS case_location,
          NULL::text AS correspondence_location
      FROM
          zaak z
      LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
      LEFT JOIN casetype_v1_reference ct_ref ON z.zaaktype_node_id = ct_ref.casetype_node_id
      LEFT JOIN zaaktype_resultaten ztr ON z.resultaat_id = ztr.id
      LEFT JOIN GROUPS gr ON z.route_ou = gr.id
      LEFT JOIN roles ROLE ON z.route_role = role.id
      LEFT JOIN view_case_relationship_v1_json crp ON z.id = crp.case_id
          AND crp.type = 'plain'
      LEFT JOIN view_case_relationship_v1_json continuation ON z.id = continuation.case_id
          AND continuation.type = 'initiator'
      LEFT JOIN view_case_relationship_v1_json children ON z.id = children.case_id
          AND children.type = 'parent'
      LEFT JOIN view_case_relationship_v1_json parent ON z.id = parent.case_id
          AND parent.type = 'child'
  WHERE
      z.deleted IS NULL) AS c
      LEFT JOIN zaaktype_status zts ON c.zaaktype_node_id = zts.zaaktype_node_id
          AND zts.status = (c.raw_milestone + 1)
      LEFT JOIN zaaktype_status zts_previous ON c.zaaktype_node_id = zts_previous.zaaktype_node_id
          AND zts_previous.status = c.raw_milestone;

  CREATE OR REPLACE VIEW public.view_case_v2
   AS
   SELECT z.id,
      z.uuid,
      z.onderwerp,
      z.route_ou,
      z.route_role,
      z.vernietigingsdatum,
      z.archival_state,
      z.status,
      z.contactkanaal,
      z.created,
      z.registratiedatum,
      z.streefafhandeldatum,
      z.afhandeldatum,
      z.stalled_until,
      z.milestone,
      z.last_modified,
      z.behandelaar_gm_id,
      z.coordinator_gm_id,
      z.aanvrager,
      z.aanvraag_trigger,
      z.onderwerp_extern,
      z.resultaat,
      z.resultaat_id,
      z.payment_amount,
      z.behandelaar,
      z.coordinator,
      z.urgency,
      z.urgency_date_medium,
      z.urgency_date_high,
      z.preset_client,
      z.prefix,
      z.number_master,
      z.html_email_template,
      z.payment_status,
      z.dutch_price AS price,
      z.zaaktype_node_id,
      z.pid AS number_parent,
      z.vervolg_van AS number_previous,
      z.leadtime AS lead_time_real,
      z.aanvrager_gm_id,
      z.aanvrager_type,
      z.zaaktype_id,
      z.deleted,
      z.confidentiality,
      zm.current_deadline,
      zm.deadline_timeline,
      ztr.id AS result_id,
      ztr.uuid AS result_uuid,
      ztr.resultaat AS result,
      ztr.selectielijst AS active_selection_list,
          CASE
              WHEN z.status = 'stalled'::text THEN zm.opschorten
              ELSE NULL::character varying
          END AS suspension_rationale,
          CASE
              WHEN z.status = 'resolved'::text THEN zm.afhandeling
              ELSE NULL::character varying
          END AS premature_completion_rationale,
      'Dossier'::text AS aggregation_scope,
      ztr.archiefnominatie AS type_of_archiving,
      rpt.label AS period_of_preservation,
      ztr.label AS result_description,
      ztr.comments AS result_explanation,
      ztr.properties::jsonb -> 'selectielijst_nummer'::text AS result_selection_list_number,
      ztr.properties::jsonb -> 'procestype_nummer'::text AS result_process_type_number,
      ztr.properties::jsonb -> 'procestype_naam'::text AS result_process_type_name,
      ztr.properties::jsonb -> 'procestype_omschrijving'::text AS result_process_type_description,
      ztr.properties::jsonb -> 'procestype_toelichting'::text AS result_process_type_explanation,
      ztr.properties::jsonb -> 'procestype_object'::text AS result_process_type_object,
      ztr.properties::jsonb -> 'procestype_generiek'::text AS result_process_type_generic,
      ztr.properties::jsonb -> 'herkomst'::text AS result_origin,
      ztr.properties::jsonb -> 'procestermijn'::text AS result_process_term,
          CASE
              WHEN z.status = 'stalled'::text THEN NULL::integer
              ELSE z.streefafhandeldatum::date - COALESCE(z.afhandeldatum::date, now()::date)
          END AS days_left,
      get_date_progress_from_case(hstore(z.*)) AS progress_days,
      ARRAY( SELECT json_build_object('naam', bibliotheek_kenmerken.naam, 'magic_string', bibliotheek_kenmerken.magic_string, 'value', zaak_kenmerk.value, 'type', bibliotheek_kenmerken.value_type,'is_multiple',bibliotheek_kenmerken.type_multiple) AS json_build_object
             FROM zaak_kenmerk
               JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id
            WHERE zaak_kenmerk.zaak_id = z.id) AS custom_fields,
      ARRAY( SELECT json_build_object('status', zs.status, 'type', ca.type, 'data', ca.data, 'automatic', ca.automatic) AS json_build_object
             FROM case_action ca
               JOIN zaaktype_status zs ON zs.id = ca.casetype_status_id
            WHERE ca.case_id = z.id) AS case_actions,
      is_destructable(z.vernietigingsdatum::timestamp with time zone) AS destructable,
      COALESCE(( SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', '::text) AS string_agg
             FROM file f
            WHERE f.case_id = z.id AND f.date_deleted IS NULL AND f.active_version = true), ''::text) AS documents,
      COALESCE(( SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', '::text) AS string_agg
             FROM file case_documents
               JOIN file_case_document fcd ON fcd.case_id = case_documents.case_id AND case_documents.id = fcd.file_id
            WHERE case_documents.case_id = z.id), ''::text) AS case_documents,
      json_build_object('uuid', ro.uuid, 'name', ro.name, 'description', ro.description, 'parent_uuid', gr.uuid, 'parent_name', gr.name) AS case_role,
      json_build_object('opschorten', zm.opschorten, 'afhandeling', zm.afhandeling, 'stalled_since', zm.stalled_since, 'unaccepted_files_count', zm.unaccepted_files_count, 'unaccepted_attribute_update_count', zm.unaccepted_attribute_update_count, 'unread_communication_count', zm.unread_communication_count) AS case_meta,
      json_build_object('id', zts.id, 'zaaktype_node_id', zts.zaaktype_node_id, 'status', zts.status, 'status_type', zts.status_type, 'naam', zts.naam, 'created', zts.created, 'last_modified', zts.last_modified, 'ou_id', zts.ou_id, 'role_id', zts.role_id, 'checklist', zts.checklist, 'fase', zts.fase, 'role_set', zts.role_set) AS case_status,
      ( SELECT json_build_object('uuid', grp.uuid, 'name', grp.name, 'description', grp.description, 'parent_uuid', grp_alias.uuid, 'parent_name', grp_alias.name) AS json_build_object
             FROM groups grp
               LEFT JOIN groups grp_alias ON array_length(grp.path, 1) > 1 AND grp.path[array_upper(grp.path, 1) - 1] = grp_alias.id
            WHERE z.route_ou = grp.id) AS case_department,
      ARRAY( SELECT json_build_object('betrokkene_type', zb.betrokkene_type, 'type', vc.type, 'subject_id', zb.subject_id, 'rol', zb.rol, 'magic_string_prefix', zb.magic_string_prefix, 'display_name', vc.display_name) AS json_build_object
             FROM zaak_betrokkenen zb
               JOIN view_contacts vc ON vc.uuid = zb.subject_id
            WHERE zb.zaak_id = z.id AND zb.deleted IS NULL AND zb.id <> z.aanvrager AND zb.id <> z.behandelaar AND zb.id <> z.coordinator) AS case_subjects,
      ( SELECT json_build_object('uuid', vc.uuid, 'type', vc.type, 'display_name', vc.display_name) AS json_build_object
             FROM view_contacts vc
            WHERE vc.id = z.aanvrager_gm_id AND vc.legacy_type = z.aanvrager_type::text) AS requestor_obj,
      ( SELECT json_build_object('uuid', vc.uuid, 'type', vc.type, 'display_name', vc.display_name) AS json_build_object
             FROM view_contacts vc
            WHERE vc.id = z.behandelaar_gm_id AND vc.type = 'employee'::text) AS assignee_obj,
      ( SELECT json_build_object('uuid', vc.uuid, 'type', vc.type, 'display_name', vc.display_name) AS json_build_object
             FROM view_contacts vc
            WHERE vc.id = z.coordinator_gm_id AND vc.type = 'employee'::text) AS coordinator_obj,
      json_build_object('uuid', ztn.uuid, 'name', ztn.titel) AS case_type_version,
      json_build_object('uuid', zt.uuid) AS case_type,
      ARRAY( SELECT json_build_object('naam', bibliotheek_kenmerken.naam, 'magic_string', bibliotheek_kenmerken.magic_string, 'type', bibliotheek_kenmerken.value_type,'is_multiple',bibliotheek_kenmerken.type_multiple, 'value', ARRAY( SELECT json_build_object('md5', filestore.md5, 'size', filestore.size, 'uuid', filestore.uuid, 'filename', file.name || file.extension::text, 'mimetype', filestore.mimetype, 'is_archivable', filestore.is_archivable, 'original_name', filestore.original_name, 'thumbnail_uuid', filestore.thumbnail_uuid) AS json_build_object
                     FROM file_case_document
                       JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = z.id
                       JOIN filestore ON file.filestore_id = filestore.id
                    WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id)) AS json_build_object
             FROM zaaktype_kenmerken
               JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = 'file'::text
               JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id
            WHERE zaaktype_node.id = z.zaaktype_node_id) AS file_custom_fields
     FROM zaak z
       JOIN zaaktype_node ztn ON ztn.id = z.zaaktype_node_id
       JOIN zaaktype zt ON zt.id = z.zaaktype_id
       LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
       LEFT JOIN zaaktype_resultaten ztr ON z.resultaat_id = ztr.id
       LEFT JOIN result_preservation_terms rpt ON ztr.bewaartermijn = rpt.code
       LEFT JOIN zaaktype_status zts ON z.zaaktype_node_id = zts.zaaktype_node_id AND zts.status = (z.milestone + 1)
       LEFT JOIN roles ro ON z.route_role = ro.id
       LEFT JOIN groups gr ON ro.parent_group_id = gr.id
    WHERE z.deleted IS NULL;

  CREATE OR REPLACE FUNCTION update_phase_term_in_zaak_meta(
    IN case_id bigint,
    IN orig timestamp,
    IN becomes timestamp
  )
  RETURNS void
  LANGUAGE plpgsql
  AS $$
  DECLARE
    diff int;
    cur jsonb;
    keys int;
  BEGIN

    select into cur current_deadline from zaak_meta where zaak_id = case_id;
    -- Python creates a case with null values ignoring the default *sigh*
    IF cur IS NULL
    THEN
      RETURN;
    END IF;

    -- We can't update the whole lot if we don't have the correct data
    -- structure. We need 5, says, start, current, phase_id and phase_no
    select into keys array_length(array_agg(k), 1) from jsonb_object_keys(cur) as k;
    IF keys IS NULL OR keys < 5
    THEN
      RETURN;
    END IF;

    diff := ((cur->>'days')::int + date_part('day', (becomes - orig)::interval))::int;
    -- If we have positive days, we make that the new terms
    -- Otherwise, we exceeded our terms already and a negative just doesn't
    -- make sense. So we make it equal to the current days, meaning, it needs
    -- to be dealt with straight away
    IF diff > (cur->>'current')::int
    THEN
      UPDATE zaak_meta SET current_deadline = jsonb_set(current_deadline, '{days}', diff::text::jsonb) where zaak_id = case_id;
    ELSE
      UPDATE zaak_meta SET current_deadline = jsonb_set(current_deadline, '{days}', (current_deadline->>'current')::jsonb) where zaak_id = case_id;
    END IF;

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION build_case_v0_values(
    z hstore,
    zm hstore,
    zt hstore,
    ztr hstore,
    zts hstore,
    zts_next hstore,
    rpt hstore,
    ztd hstore,
    gr hstore,
    ro hstore,
    case_location hstore,
    parent hstore,
    requestor hstore,
    recipient hstore,
    bc text,
    gassign text,
    ztn jsonb
  )
  RETURNS JSONB
  IMMUTABLE
  LANGUAGE plpgsql
  AS $$
  DECLARE
    case_v0 jsonb;
    mapped text;

    ztr_properties jsonb;
    assignee jsonb;
    coordinator jsonb;

  BEGIN

    ztr_properties := (ztr->'properties')::jsonb;
    assignee := (z->'assignee_v1_json')::jsonb;
    coordinator := (z->'coordinator_v1_json')::jsonb;

--    -- zs::attributes and friends
    return jsonb_build_object(
      'date_created', timestamp_to_perl_datetime(z->'created'),
      'date_modified', timestamp_to_perl_datetime(z->'last_modified'),

      'case.route_ou', (z->'route_ou')::int,
      'case.route_role', (z->'route_role')::int,

      'case.channel_of_contact', z->'contactkanaal',
      'case.html_email_template', z->'html_email_template',

      'case.current_deadline', (zm->'current_deadline')::jsonb,
      'case.deadline_timeline', (zm->'deadline_timeline')::jsonb,

      'case.date_destruction', timestamp_to_perl_datetime(z->'vernietigingsdatum'),
      'case.date_of_completion', timestamp_to_perl_datetime(z->'afhandeldatum'),
      'case.date_of_registration', timestamp_to_perl_datetime(z->'registratiedatum'),
      'case.date_of_registration_full', to_char((z->'registratiedatum')::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY HH24:MI:SS'),
      'case.date_of_completion_full', to_char((z->'afhandeldatum')::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY HH24:MI:SS'),

      'case.date_target', CASE WHEN z->'status' = 'stalled' THEN 'Opgeschort' ELSE timestamp_to_perl_datetime(z->'streefafhandeldatum') END,

      'case.stalled_since', timestamp_to_perl_datetime(zm->'stalled_since'),
      'case.stalled_until', timestamp_to_perl_datetime(z->'stalled_until'),

      'case.suspension_rationale', zm->'opschorten',

      'case.urgency', z->'urgency',

      'case.startdate', to_char((z->'registratiedatum')::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY'),

      'case.progress_days', get_date_progress_from_case(z),
      'case.progress_status', (z->'status_percentage')::numeric,

      'case.days_left', CASE WHEN z->'status' = 'stalled' THEN NULL ELSE (z->'streefafhandeldatum')::date - COALESCE((z->'afhandeldatum')::date, NOW()::date) END,

      'case.lead_time_real', z->'leadtime',

      'case.destructable', is_destructable((z->'vernietigingsdatum')::date),

      'case.number_status', (z->'milestone')::int,
      'case.milestone', CASE WHEN zts_next->'id' IS NOT NULL THEN zts->'naam' ELSE null END,
      'case.phase', CASE WHEN zts_next->'id' IS NOT NULL THEN zts_next->'fase' ELSE null END,

      'case.status', z->'status',
      'case.premature_completion_rationale', CASE WHEN z->'status' = 'resolved' THEN zm->'afhandeling' ELSE NULL END,

      'case.subject', z->'onderwerp',
      'case.subject_external', z->'onderwerp_extern',

      'case.archival_state', z->'archival_state',
      'case.confidentiality', get_confidential_mapping(z->'confidentiality'),

      'case.payment_status', get_payment_status_mapping(z->'payment_status'),
      'case.price', z->'dutch_price',

      -- documents
      'case.documents', COALESCE((SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', ') FROM file f
        WHERE f.case_id = (z->'id')::bigint and f.date_deleted is null and f.active_version = true), ''),

      -- attributes with files (used in e-mail tab for JS)
      'case.case_documents', COALESCE((SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', ') FROM file case_documents
        JOIN file_case_document fcd
          ON fcd.case_id = case_documents.case_id and case_documents.id = fcd.file_id
        WHERE case_documents.case_id = (z->'id')::bigint), ''),

      -- cached values which potentially block phase transitions
      'case.num_unaccepted_updates', (zm->'unaccepted_attribute_update_count')::int,
      'case.num_unread_communication', (zm->'unread_communication_count')::int,
      'case.num_unaccepted_files', (zm->'unaccepted_files_count')::int,

--      -- static values
      'case.aggregation_scope', 'Dossier'
    )
    -- Case/casetype result blob
    || jsonb_build_object(
      'case.result', z->'resultaat',
      'case.result_description', ztr->'label',
      'case.result_explanation', ztr->'comments',
      'case.result_id', (ztr->'id')::int,
      'case.result_origin', ztr_properties->'herkomst',
      'case.result_process_term', ztr_properties->'procestermijn',
      'case.result_process_type_description', ztr_properties->'procestype_omschrijving',
      'case.result_process_type_explanation', ztr_properties->'procestype_toelichting',
      'case.result_process_type_generic', ztr_properties->'procestype_generiek',
      'case.result_process_type_name', ztr_properties->'procestype_naam',
      'case.result_process_type_number', ztr_properties->'procestype_nummer',
      'case.result_process_type_object', ztr_properties->'procestype_object',
      'case.result_selection_list_number', ztr_properties->'selectielijst_nummer',
      'case.retention_period_source_date', ztr->'ingang',
      'case.type_of_archiving', ztr->'archiefnominatie',
      'case.period_of_preservation', rpt->'label',
      'case.period_of_preservation_active', CASE WHEN (ztr->'trigger_archival')::boolean = true THEN 'Ja' ELSE 'Nee' END,
      'case.type_of_archiving', ztr->'archiefnominatie',
      -- TODO: Pick one
      'case.selection_list', ztr->'selectielijst',
      'case.active_selection_list', ztr->'selectielijst'
    )
    -- ztd stuff
    || jsonb_build_object(
      'case.lead_time_legal', ztd->'afhandeltermijn',
      'case.lead_time_service', ztd->'servicenorm',
      'case.principle_national', ztd->'grondslag'
    )
    -- Case.number shizzle
    || jsonb_build_object(
      'case.custom_number', CASE WHEN char_length(z->'prefix') > 0 THEN CONCAT(z->'prefix', '-', z->'id') ELSE z->'id'::text END,
      'case.number', (z->'id')::bigint,
      'case.number_master', (z->'number_master')::bigint,
      'case.number_parent', (z->'pid')::bigint
    )
    -- case.casetype stuff
    || jsonb_build_object(
      'case.casetype', zt->'uuid',
      'case.casetype.id', zt->'id',
      'case.casetype.generic_category', bc,
      'case.casetype.initiator_type', ztd->'handelingsinitiator',
      'case.casetype.price.web', ztd->'pdc_tarief',
      'case.casetype.process_description', ztd->'procesbeschrijving',
      'case.casetype.publicity', ztd->'openbaarheid',
      -- incorrectly named
      'case.casetype.department', gr->'name',
      'case.casetype.route_role', ro->'name'
    ) || ztn
    -- relations
    || jsonb_build_object(
      'case.parent_uuid', COALESCE(parent->'uuid', '')
    )
    || jsonb_build_object(
    -- assignee
      'assignee', assignee->>'reference',
      'case.assignee', assignee->>'preview',
      'case.assignee.uuid', assignee->>'reference',
      'case.assignee.email', assignee->'instance'->'subject'->'instance'->>'email_address',
      'case.assignee.initials', assignee->'instance'->'subject'->'instance'->>'initials',
      'case.assignee.last_name', assignee->'instance'->'subject'->'instance'->>'surname',
      'case.assignee.first_names', assignee->'instance'->'subject'->'instance'->>'first_names',
      'case.assignee.phone_number', assignee->'instance'->'subject'->'instance'->>'phone_number',
      'case.assignee.id', (split_part(assignee->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.assignee.department', gassign,
      'case.assignee.title', ''
    )
    || jsonb_build_object(
    -- coordinator
      'coordinator', coordinator->'reference',
      'case.coordinator', coordinator->'preview',
      'case.coordinator.uuid', coordinator->'reference',
      'case.coordinator.email', coordinator->'instance'->'subject'->'instance'->>'email_address',
      'case.coordinator.phone_number', coordinator->'instance'->'subject'->'instance'->>'phone_number',
      'case.coordinator.id', (split_part(coordinator->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.coordinator.title', ''
    )
    -- requestor
    || case_subject_as_v0_json(requestor, 'requestor', false)
    || case_subject_as_v0_json(requestor, 'requestor', true)

    || jsonb_build_object(
      'case.requestor.preset_client', CASE WHEN (z->'preset_client')::boolean = true THEN 'Ja' ELSE 'Nee' END
    )

    || case_subject_as_v0_json(recipient, 'recipient', false)
    || case_subject_as_v0_json(recipient, 'recipient', true)
    -- case location
    || case_location_as_v0_json(case_location)
    -- case relationships
    || jsonb_build_object(
      'case.related_cases', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
             FROM view_case_relationship rel
              WHERE (z->'id')::bigint = rel.case_id
              AND type NOT IN ('parent', 'child')
              ORDER BY rel.order_seq
            ) a
          ), ''),
      'case.relations', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
              FROM view_case_relationship rel
              WHERE (z->'id')::bigint = rel.case_id
              ORDER BY rel.order_seq
            ) a
          ), ''),
        'case.relations_complete', CASE WHEN (zm->'relations_complete')::boolean = true THEN 'Ja' ELSE 'Nee' END
    )
    -- The actual attributes
    ||
    (
      SELECT COALESCE (
        jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value ::jsonb)
        FILTER (WHERE ca.magic_string is not null),
        '{}'::jsonb) as attributes
      FROM
        case_attributes_v0 ca
      WHERE
      ca.case_id = (z->'id')::bigint
    );

  END;
  $$;


  DROP FUNCTION IF EXISTS create_document_cache_for_case(int);
  CREATE OR REPLACE FUNCTION create_document_cache_for_case(
    IN id bigint
  ) RETURNS void
  LANGUAGE plpgsql
  AS $$
  BEGIN
      INSERT INTO case_documents_cache (case_id, value, magic_string, library_id)
        SELECT case_id, value, magic_string, library_id FROM case_documents
          WHERE case_id = id;
  END;
  $$;

  DROP FUNCTION IF EXISTS create_document_cache_for_case(int, int);
  CREATE OR REPLACE FUNCTION update_document_cache_for_case(
     IN id bigint,
     IN lib_id int
  ) RETURNS void
  LANGUAGE plpgsql
  AS $$
  BEGIN
    UPDATE case_documents_cache me
      SET value = subquery.value
      FROM ( SELECT * FROM case_documents WHERE case_id = id AND library_id = lib_id) as    subquery
        WHERE me.case_id = id and me.library_id = lib_id;
  END;
  $$;

COMMIT;
