BEGIN;

CREATE TEMP TABLE omf AS
SELECT
  om.id,
  om.object_uuid,
  om.lock_object_uuid,
  z.id as zaak_id,
  z.behandelaar_gm_id as subject_id,
  ca.data::jsonb
FROM
  object_mutation om
JOIN zaak z
ON om.lock_object_uuid = z.uuid
JOIN case_action ca
ON z.id = ca.case_id and ca.type = 'object_mutation' and ca.automatic = true
WHERE
  lock_object_uuid IS NOT NULL
  AND
  executed = true
;

INSERT INTO queue (type, label, priority, metadata, data, object_id)
 SELECT
    'mutate_object',
    'Devops: Reinsert object mutation',
    900,
    -- metadata
    json_build_object(
        'subject_id', omf.subject_id,
        'target', 'backend'
      ),
    omf.data,
    omf.lock_object_uuid
  FROM omf
  ;

UPDATE object_mutation SET executed = false
  WHERE id IN (SELECT id FROM omf);

COMMIT;

