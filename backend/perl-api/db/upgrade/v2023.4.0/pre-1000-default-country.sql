BEGIN;

  ALTER TABLE adres ALTER COLUMN landcode DROP DEFAULT;
  ALTER TABLE natuurlijk_persoon ALTER COLUMN landcode DROP DEFAULT;
  ALTER TABLE bedrijf ALTER COLUMN vestiging_landcode DROP DEFAULT;
  ALTER TABLE bedrijf ALTER COLUMN correspondentie_landcode DROP DEFAULT;

  CREATE OR REPLACE FUNCTION set_default_landcode() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
  BEGIN

    IF TG_OP IN ('INSERT', 'UPDATE') AND NEW.landcode IS NULL
    THEN
      SELECT INTO NEW.landcode COALESCE(value, '6030') FROM config WHERE parameter = 'customer_info_country_code';
    END IF;

    RETURN NEW;

  END;
  $$;

  CREATE OR REPLACE FUNCTION set_default_landcode_for_company() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
  DECLARE

    update_landcode_vestiging boolean;
    update_landcode_correspondentie boolean;
    landcode text;

  BEGIN

    update_landcode_vestiging   := false;
    update_landcode_correspondentie   := false;

    IF TG_OP IN ('INSERT', 'UPDATE') AND NEW.vestiging_landcode IS NULL
    THEN
      update_landcode_vestiging := true;
    END IF;

    IF TG_OP IN ('INSERT', 'UPDATE') AND NEW.correspondentie_landcode IS NULL
    THEN
      update_landcode_correspondentie := true;
    END IF;

    IF update_landcode_vestiging = true OR update_landcode_correspondentie = true
    THEN
      SELECT INTO landcode COALESCE(value, '6030') FROM config WHERE parameter = 'customer_info_country_code';

      IF update_landcode_vestiging = true
      THEN
        NEW.vestiging_landcode := landcode;
      END IF;

      IF update_landcode_correspondentie = true
      THEN
        NEW.correspondentie_landcode := landcode;
      END IF;
    END IF;


    RETURN NEW;

  END;
  $$;

  DROP TRIGGER IF EXISTS set_default_landcode on adres;
  DROP TRIGGER IF EXISTS set_default_landcode on natuurlijk_persoon;
  DROP TRIGGER IF EXISTS set_default_landcode on bedrijf;

  CREATE TRIGGER set_default_landcode BEFORE INSERT OR UPDATE ON adres FOR EACH ROW EXECUTE FUNCTION set_default_landcode();
  CREATE TRIGGER set_default_landcode BEFORE INSERT OR UPDATE ON natuurlijk_persoon FOR EACH ROW EXECUTE FUNCTION set_default_landcode();
  CREATE TRIGGER set_default_landcode BEFORE INSERT OR UPDATE ON bedrijf FOR EACH ROW EXECUTE FUNCTION set_default_landcode_for_company();

COMMIT;
