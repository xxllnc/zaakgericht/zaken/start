DROP INDEX IF EXISTS thread_last_message_cache_created_idx;
    
CREATE INDEX CONCURRENTLY thread_last_message_cache_created_idx ON thread(
    (CAST(last_message_cache AS json)->>'created')
);
