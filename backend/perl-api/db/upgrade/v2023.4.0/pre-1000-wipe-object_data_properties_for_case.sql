-- this is a copy of db/upgrade/v2022.7.0/post-9999-wipe-data.sql.disabled, but not disabled now

BEGIN;

  CREATE OR REPLACE FUNCTION cleanup_object_data() RETURNS void LANGUAGE plpgsql AS $$    
  DECLARE
  BEGIN
    UPDATE
      object_data
    SET
      properties = '{}',
      index_hstore = null,
      text_vector = null
    WHERE
      uuid in (select uuid from object_data where object_class = 'case' and properties != '{}' limit 10000);
  END;
    $$;
COMMIT;