
BEGIN;

  UPDATE interface set interface_config = interface_config::jsonb || '{"binding":"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"}' where interface_config::jsonb->>'saml_type' = 'digid' and interface_config::jsonb->>'binding' != 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST';

  UPDATE interface set interface_config = interface_config::jsonb || '{"binding":"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact"}' where interface_config::jsonb->>'saml_type' = 'eherkenning' and interface_config::jsonb->>'binding' != 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact';

  UPDATE interface set interface_config = interface_config::jsonb || '{"signature_algorithm":"sha256"}' where interface_config::jsonb->>'saml_type' = 'eidas' and interface_config::jsonb->>'signature_algorithm' = 'sha265';

  UPDATE interface set interface_config = interface_config::jsonb || '{"binding":"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"}' where interface_config::jsonb->>'saml_type' = 'minimal' and interface_config::jsonb->>'binding' = 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect';

COMMIT;
