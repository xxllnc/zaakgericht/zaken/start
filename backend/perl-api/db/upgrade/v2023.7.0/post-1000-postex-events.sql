BEGIN;

  update logging set event_type = 'case/postex/send' where event_type = 'case/send_postex';

  -- The following only exists on development enviroments
  update logging set event_type = 'case/postex/delivered' where event_type in ('case/postex_mail_delivery','case/postex_maildelivery', 'case/postex/delivery');
  update logging set event_type = 'case/postex/returned' where event_type = 'case/postex_mail_returned';
  update logging set event_type = 'case/postex/mail_read' where event_type in('case/postex_notification_read', 'case/postex_mail_read', 'case/postex/notification_read');

  update logging set created_by_name_cache = 'Postex' where event_type like 'case/postex/%' and created_by_name_cache is null;

COMMIT;
