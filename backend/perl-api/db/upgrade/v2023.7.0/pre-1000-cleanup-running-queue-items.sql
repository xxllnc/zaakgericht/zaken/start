BEGIN;

   CREATE OR REPLACE FUNCTION cleanup_running_queue_items() RETURNS void LANGUAGE plpgsql AS $$    
   DECLARE
   BEGIN
        update queue set status='pending' where status='running' and object_id is not null and date_started > (now() - '1 day'::interval) and date_started < (now() - '00:15:00'::interval);
   END;
   $$;
COMMIT;