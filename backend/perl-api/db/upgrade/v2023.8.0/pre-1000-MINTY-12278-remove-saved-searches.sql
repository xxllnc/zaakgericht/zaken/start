BEGIN;

DELETE FROM saved_search;
DELETE FROM saved_search_labels;
DELETE FROM saved_search_label_mapping;
DELETE FROM saved_search_permission;

COMMIT;