BEGIN;

ALTER TABLE subject ALTER last_modified SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);

ALTER TABLE object_data ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE "file" ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE zaak_meta ALTER last_modified SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE custom_object_type_version ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE custom_object_type_version ALTER last_modified SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE custom_object_version ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE custom_object_version ALTER last_modified SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE export_queue ALTER expires SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE file_derivative ALTER date_generated SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE filestore ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE object_mutation ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE object_subscription ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE subject_login_history ALTER date_attempt SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE thread_message_attachment_derivative ALTER date_generated SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE "transaction" ALTER date_created SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);
ALTER TABLE transaction_record ALTER date_executed  SET DEFAULT timezone('UTC', CURRENT_TIMESTAMP);


CREATE OR REPLACE FUNCTION insert_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    BEGIN
        IF NEW.created is NULL
        THEN
            NEW.created = timezone('UTC', CURRENT_TIMESTAMP);
        END IF;
        IF NEW.last_modified is NULL
        THEN
            NEW.last_modified = timezone('UTC', CURRENT_TIMESTAMP);
        END IF;
        RETURN NEW;
    END;
$$;

CREATE OR REPLACE FUNCTION update_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    BEGIN
        IF NEW.last_modified is NULL
        THEN
            NEW.last_modified = timezone('UTC', CURRENT_TIMESTAMP);
        END IF;
        RETURN NEW;
    END;
$$;

CREATE OR REPLACE FUNCTION get_date_progress_from_case(zaak hstore) RETURNS text IMMUTABLE LANGUAGE plpgsql AS $$
DECLARE
    completion_date timestamp with time zone;
    completion_epoch bigint;
    start_epoch bigint;
    target_epoch bigint;

    current_difference bigint;
    max_difference bigint;

    perc bigint;
BEGIN

    IF zaak->'status' = 'stalled'
    THEN
        RETURN '';
    END IF;

    IF zaak->'afhandeldatum' IS NOT NULL
    THEN
        completion_date := (zaak->'afhandeldatum')::timestamp without time zone;
    ELSE
        SELECT INTO completion_date timezone('UTC', CURRENT_TIMESTAMP)::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date);
    SELECT INTO start_epoch date_part('epoch', (zaak->'registratiedatum')::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', (zaak->'streefafhandeldatum')::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    perc := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));

    RETURN perc::text;
END;
$$;

CREATE OR REPLACE FUNCTION update_requestor(
    IN gm_id int,
    IN type text
) RETURNS void LANGUAGE plpgsql AS $$
    BEGIN

    UPDATE zaak SET
        "requestor_v1_json" = NULL,
        "last_modified" = timezone('UTC', CURRENT_TIMESTAMP)
    WHERE
        "status" in ('open', 'new', 'stalled')
        AND "aanvrager" IN (
            SELECT "id" from zaak_betrokkenen
            WHERE "gegevens_magazijn_id" = "gm_id"
            AND "betrokkene_type" = "type"
        );

    END;
$$;

COMMIT;