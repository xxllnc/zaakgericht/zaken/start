BEGIN;

DROP TABLE IF EXISTS style_configuration;

CREATE TABLE public.style_configuration
(
    id int GENERATED ALWAYS AS IDENTITY,
    uuid uuid NOT NULL UNIQUE,
    tenant text NOT NULL,
    name text NOT NULL,
    content text,
    last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT timezone('UTC', CURRENT_TIMESTAMP),
    PRIMARY KEY(id),
    UNIQUE(tenant, name)
);


COMMIT;