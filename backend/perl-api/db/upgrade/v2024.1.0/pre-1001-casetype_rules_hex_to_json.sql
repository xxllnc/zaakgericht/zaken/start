BEGIN;

-- some records in the column zaaktype_regel.settings are stored in a hex format.
-- this update statement converts it to UTF-8 (json)
update zaaktype_regel set settings = convert_from(decode(replace(settings, '^JSON|||hex|^', ''), 'hex'), 'UTF-8') where settings like '^JSON|||hex|^%';

END;