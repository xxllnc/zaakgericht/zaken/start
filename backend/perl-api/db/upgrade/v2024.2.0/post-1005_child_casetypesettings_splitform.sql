BEGIN;

CREATE OR REPLACE FUNCTION get_child_casetypes_as_json(mother_casetype_uuid uuid)
RETURNS json
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
  enabled bool;
  settings json;
  child json;
  children json;
  num_phases_mother int;
  child_casetype_v2 json;
begin
	--  Child casetype settings for v2 have a different structure compared to v1. When retrieving these settings
	--  a translation can be done from v1 -> v2. But the otherway is not possible. New setting structure will be
    --- stored in the field named `child_casetype_settings`. Once stored in v2 structure the new settings are retrieved.
    select properties::json->'child_casetype_settings' into child_casetype_v2 from zaaktype_node where uuid = mother_casetype_uuid;
    if child_casetype_v2 is not null then
      return child_casetype_v2;
    end if;

    select text_to_bool(properties::json->>'is_casetype_mother'), (properties::json->>'child_casetypes')::json
    into enabled, settings from zaaktype_node where uuid = mother_casetype_uuid;
    children := '[]';
    if enabled then
      --  only process settings if enabled    
      select count(zs.id) into num_phases_mother from zaaktype_status zs join zaaktype_node ztn on ztn.id = zs.zaaktype_node_id where ztn.uuid = mother_casetype_uuid;
      for child in select * from json_array_elements(settings) loop
	    -- loop over all child settings and convert it to json_object
	    children := children::jsonb || json_build_object(
	        'enabled', child::json->'import',
	        'casetype', json_build_object(
              'uuid', (select uuid from zaaktype where id = (child::json->'casetype'->>'id')::integer),
              'name', (child::json->'casetype'->'title')
            ),
            'settings', json_build_object(
              'relations', text_to_bool(child::json->>'betrokkenen'),
              'webform', text_to_bool(child::json->>'actions'),
              'registrationform', text_to_bool(child::json->>'actions'),
              'case', text_to_bool(child::json->>'actions'),
              'notifications', text_to_bool(child::json->>'actions'),
              'api', text_to_bool(child::json->>'actions'),
              'allocations', text_to_bool(child::json->>'allocation'),
              'first_phase', text_to_bool(child::json->>'first_phase'),
              'middle_phases', case when text_to_bool(child::json->>'middle_phases') then (select json_agg(generate_series) as num from generate_series(2,num_phases_mother - 1)) else '[]' end,
              'last_phase', text_to_bool(child::json->>'last_phase'),
              'results', text_to_bool(child::json->>'resultaten'),
              'relations', text_to_bool(child::json->>'betrokkenen'),
              'authorizations', text_to_bool(child::json->>'authorisaties')
            )
        )::jsonb;
      end loop;
    end if;
  	return json_build_object('enabled', enabled, 'child_casetypes', children);
END;
$$;

drop view if exists public.view_case_type_version_v2;
CREATE OR REPLACE VIEW public.view_case_type_version_v2
AS 
select
    ztn.uuid as uuid,
    zt.uuid as casetype_uuid,
    zt.active as active,
    case when bc.uuid is null then null else json_build_object(
        'uuid', bc.uuid,
        'name', bc.naam
    ) end as catalog_folder,
    json_build_object(
        'name', ztn.titel,
        'identification', ztn.code,
        'tags', ztn.zaaktype_trefwoorden,
        'description', ztn.zaaktype_omschrijving,
        'case_summary', ztd.extra_informatie,
        'case_public_summary', ztd.extra_informatie_extern,
        'legal_period', case when (ztd.afhandeltermijn is null and ztd.afhandeltermijn_type is null) then null else json_build_object(
            'value', ztd.afhandeltermijn,
            'type', ztd.afhandeltermijn_type
        ) end,
        'service_period',  case when (ztd.servicenorm is null and ztd.servicenorm_type is null) then null else json_build_object(
            'value', ztd.servicenorm,
            'type', ztd.servicenorm_type
        ) end
    ) as general_attributes,
    json_build_object(
        'process_description', ztd.procesbeschrijving,
        'initiator_type', ztd.handelingsinitiator,
        'motivation', ztn.properties::json->>'aanleiding',
        'purpose', ztn.properties::json->>'doel',
        'archive_classification_code', ztn.properties::json->>'archiefclassicatiecode',
        'designation_of_confidentiality', case when (ztn.properties::json->>'vertrouwelijkheidsaanduiding' = '-') then null else ztn.properties::json->>'vertrouwelijkheidsaanduiding' end,
        'responsible_subject', ztn.properties::json->>'verantwoordelijke',
        'responsible_relationship', ztn.properties::json->>'verantwoordingsrelatie',
        'possibility_for_objection_and_appeal', text_to_bool(ztn.properties::json->>'beroep_mogelijk'),
        'publication', text_to_bool(ztn.properties::json->>'publicatie'),
        'publication_text', ztn.properties::json->>'publicatietekst',
        'bag', text_to_bool(ztn.properties::json->>'bag'),
        'lex_silencio_positivo', text_to_bool(ztn.properties::json->>'lex_silencio_positivo'),
        'may_postpone', text_to_bool(ztn.properties::json->>'opschorten_mogelijk'),
        'may_extend', text_to_bool(ztn.properties::json->>'verlenging_mogelijk'),
        'extension_period', case when ztn.properties::json->>'verlengingstermijn'~E'^\\d+$' then (ztn.properties::json->>'verlengingstermijn')::integer else null end,
        'adjourn_period', case when ztn.properties::json->>'verdagingstermijn'~E'^\\d+$' then (ztn.properties::json->>'verdagingstermijn')::integer else null end,
        'penalty_law', text_to_bool(ztn.properties::json->>'wet_dwangsom'),
        'wkpb_applies', text_to_bool(ztn.properties::json->>'wkpb'),
        'e_webform', ztn.properties::json->>'e_formulier',
        'legal_basis', ztd.grondslag,
        'local_basis', ztn.properties::json->>'lokale_grondslag',
        'gdpr', json_build_object(
            'enabled', text_to_bool(ztn.properties::json->'gdpr'->>'enabled'),
            'kind', json_build_object(
                --  'basic_details', case when ztn.properties::json->'gdpr'->'personal'->>'basic_details' = 'on' is true then true else false end,
            	'basic_details', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'basic_details'),
                'personal_id_number', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'personal_id_number'),
                'income', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'income'),
                'race_or_ethniticy', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'race_or_ethniticy'),
                'political_views', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'political_views'),
                'religion', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'religion'),
                'membership_union', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'membership_union'),
                'genetic_or_biometric_data', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'genetic_or_biometric_data'),
                'health', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'health'),
                'sexual_identity', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'sexual_identity'),
                'criminal_record', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'criminal_record'),
                'offspring', text_to_bool(ztn.properties::json->'gdpr'->'personal'->>'offspring')
            ),
            'source', json_build_object(
                'public_source', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'public_source'),
                'registration', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'registration'),
                'partner', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'partner'),
                'sender', text_to_bool(ztn.properties::json->'gdpr'->'source_personal'->>'sender')
            ),
            'processing_type', ztn.properties::json->'gdpr'->>'processing_type',
            'process_foreign_country', text_to_bool(ztn.properties::json->'gdpr'->>'process_foreign_country'),
            'process_foreign_country_reason', ztn.properties::json->'gdpr'->>'process_foreign_country_reason',
            'processing_legal', ztn.properties::json->'gdpr'->>'processing_legal',
            'processing_legal_reason', ztn.properties::json->'gdpr'->>'processing_legal_reason'
        )
    ) as documentation,
    json_build_object(
        'trigger', ztn.trigger,
        'allowed_requestor_types', (select json_agg(zb.betrokkene_type) from zaaktype_betrokkenen zb where zb.zaaktype_node_id = ztn.id),
        'preset_requestor', legacy_subject_id_as_json(ztd.preset_client),
        'api_preset_assignee', legacy_subject_id_as_json(ztn.properties::json->>'preset_owner_identifier'),
        'address_requestor_use_as_correspondence', coalesce (ztn.adres_aanvrager::bool, false),
        'address_requestor_use_as_case_address', coalesce(ztn.adres_andere_locatie::bool, false),
        'address_requestor_show_on_map', coalesce(ztn.adres_geojson::bool, false)
    ) as relations,
    json_build_object(
        'public_confirmation_title', ztn.properties::json->>'public_confirmation_title',
        'public_confirmation_message', ztn.properties::json->>'public_confirmation_message',
        'case_location_message', ztn.properties::json->>'case_location_message',
        'pip_view_message', ztn.properties::json->>'pip_view_message',
        'actions', json_build_object(
            'enable_webform', coalesce(ztn.webform_toegang::bool, false),
            'create_delayed', coalesce((ztn.properties::json->>'delayed')::bool, false),
            'address_check', coalesce((ztn.properties::json->>'case_location_check')::bool, false),
            'reuse_casedata', coalesce(ztn.aanvrager_hergebruik::bool, false),
            'enable_online_payment', coalesce(ztn.online_betaling::bool, false),
            'enable_manual_payment', coalesce((ztn.properties::json->>'offline_betaling')::bool, false),
            'email_required', coalesce(ztn.contact_info_email_required::bool, false),
            'phone_required', coalesce(ztn.contact_info_phone_required::bool, false),
            'mobile_required', coalesce(ztn.contact_info_mobile_phone_required::bool, false),
            'disable_captcha', coalesce((ztn.properties::json->>'no_captcha')::bool, false)
        ),
        'price', json_build_object(
            'web', text_to_valuta(ztd.pdc_tarief),
            'frontdesk', text_to_valuta(ztn.properties::json->>'pdc_tarief_balie'),
            'phone', text_to_valuta(ztn.properties::json->>'pdc_tarief_telefoon'),
            'email', text_to_valuta(ztn.properties::json->>'pdc_tarief_email'),
            'assignee', text_to_valuta(ztn.properties::json->>'pdc_tarief_behandelaar'),
            'post', text_to_valuta(ztn.properties::json->>'pdc_tarief_post')
        )
    ) as webform,
     json_build_object(
        -- Toewijzing aanpassing bij interne intake
        'allow_assigning_to_self', coalesce(ztn.automatisch_behandelen::bool, false),
        'allow_assigning', coalesce(ztn.toewijzing_zaakintake::bool, false),
        -- Intern form
        'show_confidentionality', coalesce((ztn.properties::json->>'confidentiality')::bool, false),
        'show_contact_details', coalesce(ztn.contact_info_intake::bool, false),
        'allow_add_relations', coalesce(ztn.extra_relaties_in_aanvraag::bool, false)
     ) as registrationform,
    json_build_object(
        -- Zaakdossier
        'disable_pip_for_requestor', coalesce(ztn.prevent_pip::bool, false),
        'lock_registration_phase', coalesce((ztn.properties::json->>'lock_registration_phase')::bool, false),
        'queue_coworker_changes', coalesce((ztn.properties::json->>'queue_coworker_changes')::bool, false),
        'allow_external_task_assignment', coalesce((ztn.properties::json->>'allow_external_task_assignment')::bool, false),
        'default_document_folders', (ztn.properties::json->>'default_directories')::json,
        'default_html_email_template', ztn.properties::json->>'default_html_email_template'
    ) as case_dossier,
    json_build_object(
        'external_notify_on_new_case', coalesce((ztn.properties::json->>'notify_on_new_case')::bool, false),
        'external_notify_on_new_document', coalesce((ztn.properties::json->>'notify_on_new_document')::bool, false),
        'external_notify_on_new_message', coalesce((ztn.properties::json->>'notify_on_new_message')::bool, false),
        'external_notify_on_exceed_term', coalesce((ztn.properties::json->>'notify_on_exceed_term')::bool, false),
        'external_notify_on_allocate_case', coalesce((ztn.properties::json->>'notify_on_allocate_case')::bool, false),
        'external_notify_on_phase_transition', coalesce((ztn.properties::json->>'notify_on_phase_transition')::bool, false),
        'external_notify_on_task_change', coalesce((ztn.properties::json->>'notify_on_task_change')::bool, false),
        'external_notify_on_label_change', coalesce((ztn.properties::json->>'notify_on_label_change')::bool, false),
        'external_notify_on_subject_change', coalesce((ztn.properties::json->>'notify_on_subject_change')::bool, false)
    ) as notifications,
    json_build_object(
        -- API instellingen
        'api_can_transition', coalesce((ztn.properties::json->>'api_can_transition')::bool, false)
    ) as api,
    coalesce(get_casetype_version_authorization(ztn.id), '[]'::json) as authorization,
    get_child_casetypes_as_json(ztn.uuid) as child_casetype_settings,
    get_phases_as_json(ztn.id) as phases
from zaaktype_node ztn
join zaaktype zt on ztn.zaaktype_id = zt.id
join zaaktype_definitie ztd on ztn.zaaktype_definitie_id = ztd.id
left join bibliotheek_categorie bc on zt.bibliotheek_categorie_id = bc.id;

COMMIT;