BEGIN;

    DELETE FROM custom_object_relationship 
    USING zaak
    WHERE zaak.id = custom_object_relationship.related_case_id
    AND zaak.status = 'deleted'
    AND zaak.deleted IS NOT NULL;

    DELETE FROM custom_object_relationship 
    USING custom_object, custom_object_version
    WHERE custom_object_relationship.related_custom_object_id = custom_object.id
    AND custom_object.custom_object_version_id = custom_object_version.id
    AND custom_object_version.date_deleted IS NOT NULL;

COMMIT;