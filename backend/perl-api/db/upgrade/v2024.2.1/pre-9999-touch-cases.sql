BEGIN;
  INSERT INTO queue (
    type,
    label,
    priority,
    metadata,
    data
  )
  SELECT
    'touch_case',
    'devops: Fix date search index for cases',
    950,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', z.id
    )
  FROM
    zaak z
  WHERE
    z.status != 'deleted'
  AND EXISTS (
    SELECT attr.id FROM zaak_kenmerk attr
    JOIN bibliotheek_kenmerken bk ON attr.bibliotheek_kenmerken_id = bk.id AND bk.value_type = 'date'
    WHERE attr.zaak_id = z.id
  );
COMMIT;

