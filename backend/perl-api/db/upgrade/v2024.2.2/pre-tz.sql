BEGIN;
  CREATE OR REPLACE FUNCTION hstore_to_timestamp (
    IN date_field VARCHAR
  )
  RETURNS TIMESTAMP
  LANGUAGE plpgsql
  IMMUTABLE
  AS $$
  DECLARE
    tzdata text;
    landcode text;
    ts timestamp;
  BEGIN

    SELECT
      INTO landcode COALESCE (
        config.value,
        '6030'
      )
    FROM
      config
    WHERE
      parameter = 'customer_info_country_code';
    IF landcode = '5107' THEN
      tzdata := 'America/Curacao';
    ELSE
      tzdata := 'Europe/Amsterdam';
    END IF;

    SELECT INTO ts
      timezone(tzdata, to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS'))::TIMESTAMP;
    RETURN ts;
  END $$;
COMMIT;

