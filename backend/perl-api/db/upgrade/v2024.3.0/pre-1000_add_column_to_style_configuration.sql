BEGIN;

    ALTER TABLE style_configuration ADD COLUMN mimetype VARCHAR(160) NOT NULL;

COMMIT;