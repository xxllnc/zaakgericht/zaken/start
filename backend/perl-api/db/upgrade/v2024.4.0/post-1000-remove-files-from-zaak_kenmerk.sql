BEGIN;

  CREATE TABLE zaak_kenmerk_files_202405 AS (
    SELECT zk.* from zaak_kenmerk zk join bibliotheek_kenmerken bk on bk.id = zk.bibliotheek_kenmerken_id and bk.value_type = 'file'
  );
  
  delete from zaak_kenmerk where id in (
    select zk.id from zaak_kenmerk zk join bibliotheek_kenmerken bk on bk.id = zk.bibliotheek_kenmerken_id and bk.value_type = 'file'
  );

COMMIT;
