BEGIN;

    CREATE INDEX IF NOT EXISTS idx_requestor_coc_location_number ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->>'coc_location_number') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'coc_location_number';

    CREATE INDEX IF NOT EXISTS idx_requestor_coc_number ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->>'coc_number') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'coc_number';

    CREATE INDEX IF NOT EXISTS idx_requestor_correspondence_zipcode ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->'address_correspondence'->'instance'->>'zipcode') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'address_correspondence';

    CREATE INDEX IF NOT EXISTS idx_requestor_correspondence_street ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->'address_correspondence'->'instance'->>'street') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'address_correspondence';

    CREATE INDEX IF NOT EXISTS idx_requestor_correspondence_city ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->'address_correspondence'->'instance'->>'city') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'address_correspondence';

    CREATE INDEX IF NOT EXISTS idx_requestor_residence_street ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->'address_residence'->'instance'->>'city') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'address_residence';

    CREATE INDEX IF NOT EXISTS idx_requestor_residence_city ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->'address_residence'->'instance'->>'street') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'address_residence';

    CREATE INDEX IF NOT EXISTS idx_requestor_residence_zipcode ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->'address_residence'->'instance'->>'zipcode') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'address_residence';

    CREATE INDEX IF NOT EXISTS idx_requestor_is_secret ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->>'is_secret') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'is_secret';

    CREATE INDEX IF NOT EXISTS idx_requestor_noble_title ON zaak USING GIN ((requestor_v1_json->'instance'->'subject'->'instance'->>'noble_title') gin_trgm_ops) WHERE requestor_v1_json->'instance'->'subject'->'instance' ? 'noble_title';
    
COMMIT;