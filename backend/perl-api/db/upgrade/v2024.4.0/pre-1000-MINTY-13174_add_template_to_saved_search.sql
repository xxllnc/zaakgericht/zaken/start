BEGIN;

ALTER TABLE saved_search ADD COLUMN template TEXT DEFAULT 'standard';

COMMIT;
