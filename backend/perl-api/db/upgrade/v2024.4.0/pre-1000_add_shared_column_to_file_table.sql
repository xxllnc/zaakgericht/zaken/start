BEGIN;

-- Shared indicates shared lock by ms-office

ALTER TABLE file ADD COLUMN shared BOOLEAN;

COMMIT;