CREATE INDEX CONCURRENTLY subject_login_history_date_attempt_subject_id_success_idx
    ON public.subject_login_history (date_attempt, subject_id, success);
