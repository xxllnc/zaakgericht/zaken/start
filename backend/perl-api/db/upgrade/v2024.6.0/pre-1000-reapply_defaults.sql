begin;

-- Drop & recreate index is safe.
DROP INDEX IF EXISTS bibliotheek_category_unique_name_idx;
CREATE UNIQUE INDEX bibliotheek_category_unique_name_idx ON public.bibliotheek_categorie USING btree (naam, COALESCE(pid, '-1'::integer));

DROP INDEX IF EXISTS object_acl_entry_unique_idx;
CREATE UNIQUE INDEX object_acl_entry_unique_idx ON public.object_acl_entry USING btree (object_uuid, entity_type, entity_id, capability, scope, COALESCE(groupname, ''::text));

DROP INDEX IF EXISTS zaak_meta_case_assignee_id_idx;
CREATE INDEX zaak_meta_case_assignee_id_idx ON public.zaak_meta USING btree ((((index_hstore -> 'case.assignee.id'::text))::numeric)) WITH (fillfactor='80');

DROP INDEX IF EXISTS zaak_meta_hstore_to_timestamp_date_idx;
CREATE INDEX zaak_meta_hstore_to_timestamp_date_idx ON public.zaak_meta USING btree (((hstore_to_timestamp(((index_hstore -> 'case.date_of_completion'::text))::character varying))::date)) WITH (fillfactor='80');

DROP INDEX IF EXISTS zaak_meta_hstore_to_timestamp_idx;
CREATE INDEX zaak_meta_hstore_to_timestamp_idx ON public.zaak_meta USING btree (hstore_to_timestamp(((index_hstore -> 'case.date_of_completion'::text))::character varying)) WITH (fillfactor='80');

DROP INDEX IF EXISTS zaak_meta_unread_communications_idx;
CREATE INDEX zaak_meta_unread_communications_idx ON public.zaak_meta USING btree (unread_communication_count) WITH (fillfactor='90');

-- There might be remnants of old indexes with bad names:
DROP INDEX IF EXISTS bibliotheek_kenmerken_magic_string_unique_id;
DROP INDEX IF EXISTS bibliotheek_kenmerken_magic_string;
DROP INDEX IF EXISTS bibliotheek_kenmerken_magic_string_unique_idx;
CREATE UNIQUE INDEX bibliotheek_kenmerken_magic_string_unique_idx ON public.bibliotheek_kenmerken USING btree (magic_string) WHERE (deleted IS NULL);

-- Timestamps should be UTC
ALTER TABLE queue ALTER COLUMN date_created SET DEFAULT timezone('UTC'::text, statement_timestamp());

-- Some databases are missing the constraint
ALTER TABLE scheduled_jobs DROP CONSTRAINT IF EXISTS scheduled_jobs_task_check;
ALTER TABLE scheduled_jobs ADD CONSTRAINT scheduled_jobs_task_check CHECK (((task)::text ~ '^(case/update_kenmerk|case/mail)$'::text));

-- Setting NOT NULL when a field is already NOT NULL is fine
-- And some databases are missing these:
ALTER TABLE zaak_meta ALTER COLUMN zaak_id SET NOT NULL;
ALTER TABLE zaak ALTER COLUMN confidentiality SET NOT NULL;
ALTER TABLE zaaktype_status ALTER COLUMN termijn SET NOT NULL;

commit;