CREATE INDEX CONCURRENTLY IF NOT EXISTS file_search_terms_idx ON file USING gin (search_term gin_trgm_ops);
