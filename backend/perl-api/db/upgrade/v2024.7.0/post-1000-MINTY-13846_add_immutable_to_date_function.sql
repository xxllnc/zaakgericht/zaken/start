BEGIN;

    CREATE OR REPLACE FUNCTION immutable_to_date(input_text text, format_text text) 
    RETURNS date AS $$
    DECLARE
        year int;
        month int;
        day int;
        result_date date;
    BEGIN

        IF format_text = 'DD-MM-YYYY' THEN
            day := CAST(substring(input_text FROM 1 FOR 2) AS int);
            month := CAST(substring(input_text FROM 4 FOR 2) AS int);
            year := CAST(substring(input_text FROM 7 FOR 4) AS int);
            result_date := (year || '-' || LPAD(month::text, 2, '0') || '-' || LPAD(day::text, 2, '0'))::date;
            
        ELSIF format_text = 'DD-M-YYYY' THEN
            day := CAST(substring(input_text FROM 1 FOR 2) AS int);
            month := CAST(substring(input_text FROM 4 FOR 1) AS int);
            year := CAST(substring(input_text FROM 6 FOR 4) AS int);
            result_date := (year || '-' || LPAD(month::text, 2, '0') || '-' || LPAD(day::text, 2, '0'))::date;

        ELSE
            RAISE EXCEPTION 'Unsupported date format: %', format_text;
        END IF;

        RETURN result_date;
    END;
    $$ LANGUAGE plpgsql IMMUTABLE;


    CREATE OR REPLACE FUNCTION immutable_text_to_date(text) 
    RETURNS date AS $$
    BEGIN
        RETURN $1::date;
    END;
    $$ LANGUAGE plpgsql IMMUTABLE;


    CREATE OR REPLACE FUNCTION immutable_array_to_string(input_array anyarray) 
    RETURNS text AS $$
    DECLARE
        result_text text := '';
        element text;
        i integer;
    BEGIN
        -- Loop through the array elements
        FOR i IN array_lower(input_array, 1)..array_upper(input_array, 1) LOOP
            -- Cast the array element to text
            element := input_array[i]::text;

            -- Concatenate the element to the result_text with delimiter
            IF i > array_lower(input_array, 1) THEN
                result_text := result_text || ', ';
            END IF;
            result_text := result_text || element;
        END LOOP;

        RETURN result_text;
    END;
    $$ LANGUAGE plpgsql IMMUTABLE;

COMMIT;