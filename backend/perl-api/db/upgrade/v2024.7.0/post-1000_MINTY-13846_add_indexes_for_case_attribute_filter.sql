BEGIN;

    DROP INDEX IF EXISTS zaak_kenmerk_text_idx;
    CREATE INDEX zaak_kenmerk_text_idx  ON zaak_kenmerk USING GIN (immutable_array_to_string(value) gin_trgm_ops) WHERE value_type IN ('text', 'textarea');

    DROP INDEX IF EXISTS zaak_kenmerk_adres_v2_idx;
    CREATE INDEX zaak_kenmerk_adres_v2_idx ON zaak_kenmerk USING BTREE ((CAST(value[1] AS JSONB) -> 'bag' ->> 'id')) WHERE value_type = 'address_v2' AND CAST(value[1] AS JSONB) -> 'bag' ? 'id';

    DROP INDEX IF EXISTS zaak_kenmerk_bag_adres_v2_idx;
    CREATE INDEX zaak_kenmerk_bag_adres_v2_idx ON zaak_kenmerk USING GIN (CAST(value[1] AS TEXT)  gin_trgm_ops) WHERE value_type IN ('bag_openbareruimte', 'bag_straat_adres', 'bag_adres');

    DROP INDEX IF EXISTS zaak_kenmerk_bag_adres_v2_idx;
    CREATE INDEX  zaak_kenmerk_bag_adressen_v2_idx ON zaak_kenmerk USING BTREE (CAST(value[1] AS TEXT)) WHERE value_type IN ('bag_openbareruimtes', 'bag_straat_adressen', 'bag_adressen');

    DROP INDEX IF EXISTS zaak_kenmerk_option_select_idx;
    CREATE INDEX zaak_kenmerk_select_idx ON zaak_kenmerk USING GIN (CAST(value[1] AS TEXT)  gin_trgm_ops) WHERE value_type = 'option';
    
    DROP INDEX IF EXISTS zaak_kenmerk_option_idx;
    CREATE INDEX zaak_kenmerk_option_idx ON zaak_kenmerk USING BTREE (CAST(value[1] AS TEXT)) WHERE value_type = 'select';
    
    DROP INDEX IF EXISTS zaak_kenmerk_relationship_subject_idx;
    CREATE INDEX zaak_kenmerk_relationship_subject_idx ON zaak_kenmerk USING GIN ((CAST(value[1] AS JSONB) ->> 'value') gin_trgm_ops) WHERE value_type = 'relationship_subject' AND CAST(value[1] AS JSONB) ? 'value';

    DROP INDEX IF EXISTS zaak_kenmerk_email_idx;
    CREATE INDEX zaak_kenmerk_email_idx ON zaak_kenmerk (lower(CAST(value[1] AS TEXT))) WHERE value_type = 'email';

    DROP INDEX IF EXISTS zaak_kenmerk_valuta_idx;
    CREATE INDEX zaak_kenmerk_valuta_idx ON zaak_kenmerk USING BTREE (CAST(value[1] AS TEXT)) WHERE value_type = 'bankaccount';

    DROP INDEX IF EXISTS zaak_kenmerk_rich_text_idx;
    CREATE INDEX zaak_kenmerk_rich_text_idx ON zaak_kenmerk (attribute_richtext_strip_html_tags(value)) WHERE value_type = 'richtext';

    DROP INDEX IF EXISTS zaak_kenmerk_to_date_idx;
    CREATE INDEX zaak_kenmerk_to_date_idx ON zaak_kenmerk (CAST(immutable_to_date(value[1], 'DD-MM-YYYY') AS DATE)) WHERE value_type = 'date' AND value[1] ~ '^\d{2}-\d{2}-\d{4}$';
    
    DROP INDEX IF EXISTS zaak_kenmerk_date_idx;
    CREATE INDEX zaak_kenmerk_date_idx ON zaak_kenmerk (immutable_text_to_date(value[1])) WHERE value_type = 'date' AND value[1] ~ '^\d{4}-\d{2}-\d{2}$';

COMMIT;