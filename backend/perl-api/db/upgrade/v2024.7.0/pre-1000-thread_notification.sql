BEGIN;

    ALTER TABLE thread ADD is_notification BOOLEAN NOT NULL DEFAULT FALSE;
    
    -- Trick to allow only one "notification" thread per case:
    -- - Transform all `is_notification: False` to NULL
    -- - NULLs are all different and don't count for uniqueness :)
    CREATE UNIQUE INDEX ON thread(case_id, NULLIF(is_notification, FALSE)) NULLS DISTINCT;

COMMIT;
