BEGIN;

  DROP TABLE IF EXISTS zaaktype_archive_settings;
  DROP TABLE IF EXISTS case_archive_settings;
  DROP TABLE IF EXISTS archive_settings_base;

  CREATE TABLE zaaktype_archive_settings (
    id SERIAL PRIMARY KEY NOT NULL,
    uuid UUID NOT NULL DEFAULT uuid_generate_v4(),
    casetype_node_id integer NOT NULL UNIQUE,
    name text [],
    classification_code text [],
    classification_description text [],
    classification_source text [],
    classification_date text [],
    description text [],
    location text [],
    dekking_in_time text [],
    dekking_in_geo text [],
    language text,
    user_rights text [],
    user_rights_description text [],
    user_rights_date_period text [],
    confidentiality text [],
    confidentiality_description text [],
    confidentiality_date_period text [],
    form_genre text [],
    form_publication text [],
    structure text [],
    generic_metadata text [],
    FOREIGN KEY (casetype_node_id) REFERENCES zaaktype_node(id)
  );


  CREATE TABLE case_archive_settings (
    id SERIAL PRIMARY KEY NOT NULL,
    uuid UUID NOT NULL DEFAULT uuid_generate_v4(),
    case_id integer NOT NULL UNIQUE,
    name text [] NOT NULL,
    classification_code text [] NOT NULL,
    classification_description text [] NOT NULL,
    classification_source text [] NOT NULL,
    classification_date text [] NOT NULL,
    description text [],
    location text [] NOT NULL,
    dekking_in_time text [] NOT NULL,
    dekking_in_geo text [],
    language text NOT NULL,
    user_rights text [],
    user_rights_description text [],
    user_rights_date_period text [],
    confidentiality text [],
    confidentiality_description text [],
    confidentiality_date_period text [],
    form_genre text [],
    form_publication text [],
    structure text [],
    generic_metadata text [],
    FOREIGN KEY (case_id) REFERENCES zaak(id)
  );

COMMIT;
