BEGIN;

CREATE OR REPLACE FUNCTION text_to_valuta(value text)
RETURNS decimal
IMMUTABLE
LANGUAGE plpgsql
AS $$
begin
  -- REGEXP_REPLACE is applied to change values to the correct format
  -- first all , are replaced by a .
  -- after that all characters (except numbers and .) will be wiped
  -- in case only characters are supplied, we null will be the result
  -- this will only fail for input with more then 2 , characters.
  return case when value in (null, '') then null else cast(nullif(REGEXP_REPLACE(REGEXP_REPLACE(value, '[,]', '.', 'g'),'[^0-9\.]', '', 'g'), '') as decimal) end;
END;
$$;

COMMIT;
