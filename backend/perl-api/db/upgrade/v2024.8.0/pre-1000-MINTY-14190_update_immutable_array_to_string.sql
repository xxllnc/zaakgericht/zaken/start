BEGIN;

    CREATE OR REPLACE FUNCTION immutable_array_to_string(input_array anyarray) 
    RETURNS text AS $$
    DECLARE
        result_text text := '';
        element text;
        i integer;
    BEGIN
        -- Handle empty array case
        IF array_lower(input_array, 1) IS NULL THEN
            RETURN '';
        END IF;
        -- Loop through the array elements
        FOR i IN array_lower(input_array, 1)..array_upper(input_array, 1) LOOP
            -- Cast the array element to text
            element := input_array[i]::text;

            -- Concatenate the element to the result_text with delimiter
            IF i > array_lower(input_array, 1) THEN
                result_text := result_text || ', ';
            END IF;
            result_text := result_text || element;
        END LOOP;

        RETURN result_text;
    END;
    $$ LANGUAGE plpgsql IMMUTABLE;

COMMIT;