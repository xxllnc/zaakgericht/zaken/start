BEGIN;

  ALTER TABLE zaak DROP column current_deadline;
  ALTER TABLE zaak DROP column deadline_timeline;

COMMIT;