BEGIN;

  INSERT INTO municipality_code (
    dutch_code,
    label,
    alternative_name,
    uuid,
    historical
  ) VALUES (
    6001,
    'Stroomopwaarts',
    NULL,
    'a7ad239f-09ac-4dc9-a4d5-47f229ed3798',
    false
  );

COMMIT;
