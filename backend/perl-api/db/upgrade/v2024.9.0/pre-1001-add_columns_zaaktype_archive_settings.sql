BEGIN;

  ALTER TABLE zaaktype_archive_settings
  ADD COLUMN archive_type text,
  ADD COLUMN classification_source_mdto text [],
  ADD COLUMN description_mdto text [],
  ADD COLUMN coverage_in_time_mdto text [],
  ADD COLUMN coverage_in_geo_mdto text [],
  ADD COLUMN language_mdto text,
  ADD COLUMN additional_metadata text [],
  ADD COLUMN aggregation_glossary text [],
  ADD COLUMN coverage_in_time_begin_date text [],
  ADD COLUMN coverage_in_time_end_date text [],
  ADD COLUMN coverage_in_place text [],
  ADD COLUMN event_glossary text [],
  ADD COLUMN event_in_time text [],
  ADD COLUMN event_actor_name text [],
  ADD COLUMN event_result text [],
  ADD COLUMN valuation_glossary text [],
  ADD COLUMN retention_period_glossary text [],
  ADD COLUMN information_category_glossary text [],
  ADD COLUMN related_information_object_glossary text [],
  ADD COLUMN related_information_object_actor text [],
  ADD COLUMN archive_maker text [],
  ADD COLUMN subject_glossary text [],
  ADD COLUMN restriction_use_label text [],
  ADD COLUMN restriction_use_code text [],
  ADD COLUMN restriction_use_glossary text [],
  ADD COLUMN restriction_use_description text [],
  ADD COLUMN restriction_use_documents_name text [],
  ADD COLUMN restriction_use_term_label text [],
  ADD COLUMN restriction_use_term_code text [],
  ADD COLUMN restriction_use_term_glossary text [];

  ALTER TABLE case_archive_settings
  ADD COLUMN archive_type text,
  ADD COLUMN classification_source_mdto text [],
  ADD COLUMN description_mdto text [],
  ADD COLUMN coverage_in_time_mdto text [],
  ADD COLUMN coverage_in_geo_mdto text [],
  ADD COLUMN language_mdto text,
  ADD COLUMN additional_metadata text [],
  ADD COLUMN aggregation_glossary text [],
  ADD COLUMN coverage_in_time_begin_date text [],
  ADD COLUMN coverage_in_time_end_date text [],
  ADD COLUMN coverage_in_place text [],
  ADD COLUMN event_glossary text [],
  ADD COLUMN event_in_time text [],
  ADD COLUMN event_actor_name text [],
  ADD COLUMN event_result text [],
  ADD COLUMN valuation_glossary text [],
  ADD COLUMN retention_period_glossary text [],
  ADD COLUMN information_category_glossary text [],
  ADD COLUMN related_information_object_glossary text [],
  ADD COLUMN related_information_object_actor text [],
  ADD COLUMN archive_maker text [],
  ADD COLUMN subject_glossary text [],
  ADD COLUMN restriction_use_label text [],
  ADD COLUMN restriction_use_code text [],
  ADD COLUMN restriction_use_glossary text [],
  ADD COLUMN restriction_use_description text [],
  ADD COLUMN restriction_use_documents_name text [],
  ADD COLUMN restriction_use_term_label text [],
  ADD COLUMN restriction_use_term_code text [],
  ADD COLUMN restriction_use_term_glossary text [];

  ALTER TABLE zaaktype_archive_settings RENAME COLUMN classification_source TO classification_source_tmlo;
  ALTER TABLE zaaktype_archive_settings RENAME COLUMN description TO description_tmlo;
  ALTER TABLE zaaktype_archive_settings RENAME COLUMN dekking_in_time TO coverage_in_time_tmlo;
  ALTER TABLE zaaktype_archive_settings RENAME COLUMN dekking_in_geo TO coverage_in_geo_tmlo;
  ALTER TABLE zaaktype_archive_settings RENAME COLUMN language TO language_tmlo;
  ALTER TABLE zaaktype_archive_settings RENAME COLUMN generic_metadata TO generic_metadata_tmlo;

  ALTER TABLE case_archive_settings RENAME COLUMN classification_source TO classification_source_tmlo;
  ALTER TABLE case_archive_settings RENAME COLUMN description TO description_tmlo;
  ALTER TABLE case_archive_settings RENAME COLUMN dekking_in_time TO coverage_in_time_tmlo;
  ALTER TABLE case_archive_settings RENAME COLUMN dekking_in_geo TO coverage_in_geo_tmlo;
  ALTER TABLE case_archive_settings RENAME COLUMN language TO language_tmlo;
  ALTER TABLE case_archive_settings RENAME COLUMN generic_metadata TO generic_metadata_tmlo;
COMMIT;
