BEGIN;

	ALTER TABLE roles DROP COLUMN IF EXISTS permissions;

	ALTER TABLE roles ADD COLUMN IF NOT EXISTS permissions text ARRAY NULL;

	UPDATE roles
	SET permissions = ARRAY['contact_edit_subset', 'contact_search', 'dashboard', 'documenten_intake_subject', 'gebruiker', 'search', 'zaak_afdeling', 'zaak_eigen']
	WHERE name = 'Behandelaar';

	UPDATE roles
	SET permissions = ARRAY['admin', 'beheer', 'beheer_gegevens_admin', 'beheer_plugin_admin', 'beheer_zaaktype_admin', 'case_allocation', 'contact_nieuw', 'contact_search', 'contact_search_extern', 'dashboard', 'documenten_intake_all', 'documenten_intake_subject', 'gebruiker', 'message_intake', 'owner_signatures', 'plugin_mgmt', 'search', 'useradmin', 'vernietigingslijst', 'view_sensitive_data', 'zaak_afdeling', 'zaak_beheer', 'zaak_create_skip_required', 'zaak_eigen']
	WHERE name = 'Administrator';

	UPDATE roles
	SET permissions = ARRAY['admin', 'beheer', 'beheer_gegevens_admin', 'beheer_plugin_admin', 'beheer_zaaktype_admin', 'case_allocation', 'contact_nieuw', 'contact_search', 'contact_search_extern', 'dashboard', 'documenten_intake_all', 'documenten_intake_subject', 'gebruiker', 'message_intake', 'owner_signatures', 'plugin_mgmt', 'search', 'useradmin', 'vernietigingslijst', 'view_sensitive_data', 'zaak_afdeling', 'zaak_beheer', 'zaak_create_skip_required', 'zaak_eigen']
	WHERE name = 'Zaaksysteembeheerder';

	UPDATE roles
	SET permissions = ARRAY['gebruiker', 'documenten_intake_subject', 'dashboard', 'zaak_eigen', 'zaak_afdeling', 'search', 'contact_search', 'zaak_create_skip_required', 'useradmin', 'beheer_zaaktype_admin']
	WHERE name = 'Gebruikersbeheerder';

	UPDATE roles
	SET permissions = ARRAY['beheer_zaaktype_admin', 'case_allocation', 'contact_search', 'dashboard', 'gebruiker', 'objecttype_admin', 'search', 'view_sensitive_data', 'zaak_afdeling', 'zaak_beheer', 'zaak_create_skip_required', 'zaak_eigen']
	WHERE name = 'Zaaktypebeheerder';

	UPDATE roles
	SET permissions = ARRAY['case_allocation', 'contact_search', 'dashboard', 'gebruiker', 'search', 'view_sensitive_data', 'zaak_afdeling', 'zaak_beheer', 'zaak_create_skip_required', 'zaak_eigen']
	WHERE name = 'Zaakbeheerder';

	UPDATE roles
	SET permissions = ARRAY['contact_nieuw']
	WHERE name = 'Contactbeheerder';

	UPDATE roles
	SET permissions = ARRAY['beheer_gegevens_admin', 'woz_objects']
	WHERE name = 'Basisregistratiebeheerder';

	UPDATE roles
	SET permissions = ARRAY['contact_search', 'dashboard', 'gebruiker', 'plugin_mgmt', 'search', 'zaak_afdeling', 'zaak_eigen']
	WHERE name = 'Wethouder';

	UPDATE roles
	SET permissions = ARRAY['contact_search', 'dashboard', 'gebruiker', 'plugin_mgmt', 'search', 'zaak_afdeling', 'zaak_eigen']
	WHERE name = 'Directielid';

	UPDATE roles
	SET permissions = ARRAY['contact_search', 'dashboard', 'gebruiker', 'plugin_mgmt', 'search', 'zaak_afdeling', 'zaak_eigen']
	WHERE name = 'Afdelingshoofd';

	UPDATE roles
	SET permissions = ARRAY['contact_search', 'dashboard', 'gebruiker', 'plugin_mgmt', 'zaak_afdeling', 'zaak_eigen', 'zaak_intake']
	WHERE name = 'Kcc-medewerker';

	UPDATE roles
	SET permissions = ARRAY['case_allocation', 'contact_search', 'dashboard', 'gebruiker', 'plugin_mgmt', 'search', 'zaak_afdeling', 'zaak_eigen', 'zaak_intake']
	WHERE name = 'Zaakverdeler';

	UPDATE roles
	SET permissions = ARRAY['contact_search', 'contact_search_extern']
	WHERE name = 'BRP externe bevrager';

	UPDATE roles
	SET permissions = ARRAY['gebruiker']
	WHERE name = 'App gebruiker';

	UPDATE roles
	SET permissions = ARRAY['contact_search', 'dashboard', 'documenten_intake_all', 'documenten_intake_subject', 'gebruiker', 'search', 'zaak_create_skip_required', 'zaak_eigen']
	WHERE name = 'Documentintaker';

	UPDATE roles
	SET permissions = ARRAY['view_sensitive_contact_data']
	WHERE name = 'Persoonsverwerker';

	UPDATE roles
	SET permissions = ARRAY['message_intake']
	WHERE name = 'Klantcontacter';

COMMIT;
