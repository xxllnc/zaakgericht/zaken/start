
BEGIN;



 CREATE OR REPLACE FUNCTION update_message_count_on_delete()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $$
  BEGIN
    UPDATE thread SET message_count = (select count(*) from thread_message where thread_id =  OLD.thread_id) where id = OLD.thread_id;
    RETURN OLD;
  END;
  $$;	

	
CREATE OR REPLACE FUNCTION update_message_count()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $$
  BEGIN
    UPDATE thread SET message_count = (select count(*) from thread_message where thread_id =  NEW.thread_id) where id = NEW.thread_id;
    RETURN NEW;
  END;
  $$;

CREATE OR REPLACE TRIGGER  thread_message_message_count_on_delete
	AFTER
	DELETE
    ON
    thread_message FOR EACH ROW 
	EXECUTE PROCEDURE update_message_count_on_delete();


CREATE OR REPLACE  TRIGGER  thread_message_message_count 
	AFTER
	INSERT
    ON
    thread_message FOR EACH ROW 
	EXECUTE PROCEDURE update_message_count();

COMMIT;