
BEGIN;
  ALTER TABLE zaaktype_node ADD COLUMN title_seo TEXT GENERATED ALWAYS AS (
    lower(regexp_replace(titel, '[^a-z0-9]+','-', 'gi'))
  ) STORED;
  ALTER TABLE zaaktype_node ADD COLUMN v1_json JSONB NOT NULL DEFAULT '{}'::jsonb;

  CREATE OR REPLACE FUNCTION subject_json_by_legacy_id(
    IN legacy text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    type text;
    lid  int;
    subject hstore;
  BEGIN

    IF legacy IS NULL OR legacy = ''
    THEN
      RETURN;
    END IF;

    lid := (split_part(legacy, '-', 3))::int;
    type := (split_part(legacy, '-', 2))::text;

    IF type = 'natuurlijk_persoon'
    THEN
      SELECT INTO subject hstore(np.*) FROM natuurlijk_persoon np
        WHERE np.id = lid AND ( deleted_on IS NULL OR active = true);
      SELECT INTO json subject_person_json(hstore(subject));
    ELSIF type = 'bedrijf'
    THEN
      SELECT INTO subject hstore(b.*) FROM bedrijf b
        WHERE b.id = lid AND ( deleted_on IS NULL OR active = true);
      SELECT INTO json subject_company_json(hstore(subject));
    ELSE
      RAISE EXCEPTION 'Unknown preset client type type %', type;
    END IF;

  END;
  $$;

  CREATE OR REPLACE FUNCTION public_uri_path_for_casetypes(
    IN id int,
    IN title text,
    IN subject_types text[],
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN

    json := '{}';
    IF 'preset_client' = ANY ( subject_types )
    THEN
      json := jsonb_set(json, '{unknown}', to_jsonb(concat('/aanvragen/', id, '/onbekend/', title)));
    END IF;

    IF 'niet_natuurlijk_persoon' = ANY (subject_types) OR 'niet_natuurlijk_persoon_na' = ANY (subject_types)
    THEN
      json := jsonb_set(json, '{organisation}', to_jsonb(concat('/aanvragen/', id, '/organisatie/', title)));
    END IF;

    IF 'natuurlijk_persoon' = ANY (subject_types) OR 'natuurlijk_persoon_na' = ANY (subject_types)
    THEN
      json := jsonb_set(json, '{person}', to_jsonb(concat('/aanvragen/', id, '/persoon/', title)));
    END IF;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_boolean(
    IN ding text,
    OUT bool text
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
      bool := 'Nee';

      IF ding = 1::text
      THEN
        bool := 'Ja';
      END IF;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_properties(
    IN orig jsonb,
    IN ztd hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    gdpr jsonb;
    _key text;
    _value text;
    t jsonb;
  BEGIN

    json := jsonb_build_object(
      'extension', casetype_v1_boolean(orig->>'verlenging_mogelijk'),
      'lex_silencio_positivo', casetype_v1_boolean(orig->>'lex_silencio_positivo'),
      'principle_local', orig->>'lokale_grondslag',
      'supervisor', orig->>'verantwoordelijke',
      'supervisor_relation', orig->>'verantwoordingsrelatie',
      'suspension',casetype_v1_boolean(orig->>'opschorten_mogelijk'),
      'objection_and_appeal', casetype_v1_boolean(orig->>'beroep_mogelijk'),
      'publication', casetype_v1_boolean(orig->>'publicatie'),
      'text_for_publication', orig->>'publicatietekst',
      'designation_of_confidentiality', orig->'vertrouwelijkheidsaanduiding',
      'wkpb', casetype_v1_boolean(orig->>'wkpb'),
      'goal', orig->>'doel',
      'archive_classification_code', COALESCE(orig->>'archiefclassificatiecode', orig->>'archiefclassicatiecode'),
      'e_formulier', orig->>'e_formulier',
      'motivation', orig->>'aanleiding',
      'penalty', orig->'wet_dwangsom',
      'lead_time_service', CASE WHEN ztd->'servicenorm_type' IS NOT NULL THEN
        jsonb_build_object(ztd->'servicenorm_type', ztd->'servicenorm')
      ELSE null END,
      'lead_time_legal', CASE WHEN ztd->'afhandeltermijn_type' IS NOT NULL THEN
      jsonb_build_object(ztd->'afhandeltermijn_type', ztd->'afhandeltermijn')
      ELSE null END
    );

    IF jsonb_path_exists(orig, '$.gdpr') = true
    THEN
      gdpr := '{}';

      t := orig->'gdpr'->'personal';
      FOR _key, _value IN SELECT * FROM jsonb_each_text(t)
      LOOP
        IF _value = 'on'
        THEN
          t :=jsonb_set(t, ARRAY[_key], to_jsonb(true));
        ELSE
          t :=jsonb_set(t, ARRAY[_key], to_jsonb(false));
        END IF;
      END LOOP;
      gdpr := jsonb_set(gdpr, '{personal}', t, TRUE);

      t := orig->'gdpr'->'source_personal';
      FOR _key, _value IN SELECT * FROM jsonb_each_text(t)
      LOOP
        IF _value = 'on'
        THEN
          t :=jsonb_set(t, ARRAY[_key], to_jsonb(true));
        ELSE
          t :=jsonb_set(t, ARRAY[_key], to_jsonb(false));
        END IF;
      END LOOP;
      gdpr := jsonb_set(gdpr, '{source_personal}', t, TRUE);

      gdpr := gdpr || jsonb_build_object(
        'enabled', CASE WHEN orig->'gdpr'->>'enabled' = 'Ja' THEN true ELSE false END,
        'process_foreign_country', CASE WHEN orig->'gdpr'->>'process_foreign_country' = 'Ja' THEN true ELSE false END
      );
      t := orig->'gdpr';

      FOR _key, _value IN SELECT * FROM jsonb_each_text(t)
      LOOP
        IF NOT jsonb_path_exists(gdpr, ('$.' || _key)::jsonpath)
        THEN
          gdpr := jsonb_insert(gdpr, ARRAY[_key], to_jsonb(_value));
        END IF;

      END LOOP;

      json := jsonb_set(json, '{gdpr}', gdpr);
    END IF;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_settings(
    IN ztn hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    properties jsonb;
  BEGIN

    properties := (ztn->'properties')::jsonb;

    json := jsonb_build_object(
      'show_confidentiality_on_create', COALESCE((properties->>'confidentiality')::boolean, false),
      'public_registration_form', COALESCE((ztn->'webform_toegang')::boolean, false),
      'allow_subjects_on_create', COALESCE((ztn->'extra_relaties_in_aanvraag')::boolean, false),
      'offline_payment', COALESCE((properties->>'offline_betaling')::boolean, false),
      'allow_take_on_create', COALESCE((ztn->'automatisch_behandelen')::boolean, false),
      'allow_assign_on_create', COALESCE((ztn->'toewijzing_zaakintake')::boolean, false),
      'allow_reuse_previous_registration' ,COALESCE((ztn->'aanvrager_hergebruik')::boolean, false),
      'contact_info_email_required',COALESCE((ztn->'contact_info_email_required')::boolean, false),
      'allow_external_task_assignment', COALESCE((properties->>'allow_external_task_assignment')::boolean, false),
      'contact_info_mobile_phone_required',COALESCE((ztn->'contact_info_mobile_phone_required')::boolean, false),
      'contact_info_phone_required',COALESCE((ztn->'contact_info_phone_required')::boolean, false),
      'online_payment',COALESCE((ztn->'online_betaling')::boolean, false),
      'intake_show_contact_info',COALESCE((ztn->'contact_info_intake')::boolean, false)
    );

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_result_to_json(
    IN r hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    h hstore;
    p jsonb;
  BEGIN

    p := (r->'properties')::jsonb;
    json := jsonb_build_object(
      'archive_procedure', r->'ingang',
      'comments', r->'comments',
      'external_reference', r->'external_reference',
      'label', r->'label',
      'period_of_preservation', (r->'bewaartermijn')::int,
      'process_term', NULLIF(p->>'procestermijn', ''),
      'process_type_description', NULLIF(p->>'procestype_naam', ''),
      'process_type_explanation', NULLIF(p->>'procestype_omschrijving',''),
      'process_type_generic', NULLIF(p->>'procestype_generiek', ''),
      'process_type_number', NULLIF(p->>'procestype_nummer', ''),
      'process_type_object', NULLIF(p->>'procestype_object',''),
      'process_type_name', NULLIF(p->>'procestype_naam',''),
      'origin', NULLIF(p->>'herkomst',''),
      'resultaat_id', (r->'id')::int,
      'selection_list', r->'selectielijst',
      'selection_list_number', NULLIF(p->>'selectielijst_nummer', ''),
      'standard_choice', (r->'standaard_keuze')::boolean,
      'trigger_archival', (r->'trigger_archival')::boolean,
      'type', r->'resultaat',
      'type_of_archiving', r->'archiefnominatie',
      'type_of_dossier', r->'dossier_type,'
    );

    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_results(
    IN zid int,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    r record;
    j jsonb;
  BEGIN

    json := '[]'::jsonb;
    FOR r IN
      SELECT *
      FROM zaaktype_resultaten
      WHERE zaaktype_node_id = zid
      ORDER BY id
    LOOP
      SELECT INTO j casetype_v1_result_to_json(hstore(r));
      json := json || j;
    END LOOP;
    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_phase_to_json(
    IN r hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    h hstore;
    p jsonb;
    gr_v1 jsonb;
    role_v1 jsonb;
  BEGIN

    select into gr_v1 v1_json from groups where id = (r->'ou_id')::int;
    select into role_v1 v1_json from roles where id = (r->'role_id')::int;

    json := jsonb_build_object(
      'id', (r->'id')::int,
      'seq', (r->'status')::int,
      'locked', CASE WHEN (r->'locked')::boolean AND (r->'status')::int = 1 THEN true ELSE false END,
      'name', r->'fase',
      'deadline', COALESCE((r->'termijn')::int, 0),
      'route', jsonb_build_object(
        'group', gr_v1,
        'role', role_v1,
        'set', COALESCE((r->'role_set')::boolean, false)
      ),
      'fields', casetype_v1_attributes((r->'id')::int, (r->'zaaktype_node_id')::int)
    );

    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_phases(
    IN zid int,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    r record;
    j jsonb;
  BEGIN

    json := '[]'::jsonb;
    FOR r IN
      SELECT zts.*,
      COALESCE((ztn.properties::jsonb->>'lock_registration_phase')::boolean, false) as locked
      FROM zaaktype_status zts
      JOIN zaaktype_node ztn
      ON zts.zaaktype_node_id = ztn.id
      WHERE ztn.id = zid
      ORDER BY zts.status
    LOOP
      SELECT INTO j casetype_v1_phase_to_json(hstore(r));
      if j is not null
      then
        json := json || j;
      end if;
    END LOOP;
    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_permissions(
    IN perm text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    j jsonb;
    p jsonb;
    i jsonb;
    v1_group jsonb;
    v1_role jsonb;

  BEGIN

    json := '[]'::jsonb;

    IF perm IS NULL OR perm = ''
    THEN
      RETURN;
    END IF;

    j := perm::jsonb->'selectedUnits';
    IF j IS NULL OR j::text = '[]'
    THEN
      RETURN;
    END IF;

    FOR i IN SELECT * FROM jsonb_array_elements(j)
    LOOP
      SELECT INTO v1_group v1_json FROM groups WHERE id = (i->>'org_unit_id')::int;
      select into v1_role v1_json FROM roles WHERE id = (i->>'role_id')::int;

      -- transform group and role here
      v1_group := jsonb_set(v1_group, '{instance,id}', to_jsonb((v1_group->'instance'->>'group_id')::int));
      v1_role := jsonb_set(v1_role, '{instance,id}', to_jsonb((v1_role->'instance'->>'role_id')::int));
      v1_group := v1_group #- '{instance,group_id}';
      v1_role := v1_role #- '{instance,role_id}';

      json := json || jsonb_build_object('group', v1_group, 'role', v1_role);
    END LOOP;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_attribute_to_json(
    IN r hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    h hstore;
    p jsonb;
    _key text;
    _value text;
    opt jsonb;
    bkv record;
  BEGIN

    IF (r->'is_group')::boolean = true
    THEN
      json := jsonb_build_object(
          'required', false,
          'label_multiple', null,
          'is_group', true,
          'help', r->'help',
          'values', null,
          'multiple_values', null,
          'type', null,
          'id', (r->'id')::int,
          'label', r->'label'
        );
        RETURN;
    ELSE

      IF r->'object_id' IS NOT NULL
      THEN
        SELECT INTO bkv * FROM object_data where uuid = (r->'object_id')::uuid
          AND object_class = 'type';

        p := (r->'object_metadata')::jsonb;

        json := jsonb_build_object(
          'is_group', false,
          'values', null,
          'type', 'object',
          'catalogue_id', (r->'bibliotheek_kenmerken_id')::int,
          'required', (r->'value_mandatory')::boolean,
          'help', r->'help',
          'id', (r->'id')::int,
          'label', r->'label',
          'object_type_prefix', (bkv.properties)::jsonb->'values'->'prefix'->>'value',
          'object_metadata', jsonb_build_object(
            'create_object_action_label', NULLIF(p->>'create_object_action_label', ''),
            'update_object_action_label', NULLIF(p->>'update_object_action_label', ''),
            'relate_object_action_label', NULLIF(p->>'relate_object_action_label', ''),
            'delete_object_action_label', NULLIF(p->>'delete_object_action_label', ''),
            'create_object', text_to_bool(p->>'create_object'),
            'update_object', text_to_bool(p->>'text_to_bool'),
            'relate_object', text_to_bool(p->>'relate_object'),
            'delete_object', text_to_bool(p->>'delete_object')
          ),
          'label_multiple', r->'label_multiple',
          'multiple_values', (r->'type_multiple')::boolean
        );

      ELSIF r->'custom_object_uuid' IS NOT NULL
      THEN
        -- This isn't a valid representation anymore. Custom objects have
        -- become a relationship type. Casetypes which display this are broken.
        json := jsonb_build_object(
          'is_group', false,
          'values', null,
          'type', 'custom_object',
          'catalogue_id', (r->'bibliotheek_kenmerken_id')::int,
          'required', (r->'value_mandatory')::boolean,
          'help', r->'help',
          'id', (r->'id')::int,
          'label', r->'label',
          'custom_object_uuid', r->'custom_object_uuid',
          'authorisation', null,
          'create_object_action_label', (r->'object_metadata')::jsonb->'create_object_action_label'
        );

      ELSE

        p := (r->'properties')::jsonb;
        p := p || jsonb_build_object('show_on_map', text_to_bool(p->>'show_on_map'::text));

        IF p->'map_case_location' IS NOT NULL
        THEN
          p := p || jsonb_build_object('map_case_location', text_to_bool(p->>'map_case_location'::text));
        END IF;

        IF p->>'map_wms_feature_attribute_id' IS NOT NULL AND p->>'map_wms_feature_attribute_id' ~ E'^\\d+$'
        THEN
          p := p || jsonb_build_object('map_wms_feature_attribute_id', (p->>'map_wms_feature_attribute_id')::int);
        END IF;

        FOR _key, _value IN SELECT * FROM jsonb_each_text((r->'orig_properties')::jsonb)
        LOOP
            p :=jsonb_set(p, ARRAY[_key], to_jsonb(_value));
        END LOOP;

        opt := '[]'::jsonb;
        IF r->'value_type' = ANY ( ARRAY['option', 'select', 'checkbox'] )
        THEN
          FOR bkv IN
            SELECT * FROM bibliotheek_kenmerken_values WHERE bibliotheek_kenmerken_id = (r->'bibliotheek_kenmerken_id')::int
            ORDER BY sort_order
          LOOP
            opt := opt || jsonb_build_object(
              'id', bkv.id,
              'value', bkv.value,
              'active', CASE WHEN bkv.active = true THEN 1 ELSE 0 END,
              'sort_order', bkv.sort_order
            );
          END LOOP;
        END IF;

        IF r->'value_type' = 'relationship'
        THEN
          p := p || jsonb_build_object(
            'relationship_type', r->'relationship_type',
            'relationship_name', r->'relationship_name',
            'relationship_uuid', r->'relationship_uuid'
          );
        END IF;

        IF p->'date_limit' IS NOT NULL
        THEN

          IF p->'date_limit'->'start'->>'reference' = 'current'
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'start', 'reference'], to_jsonb('now'::text));
          END IF;

          IF p->'date_limit'->'start'->>'num' = ''
          THEN
            p := jsonb_set(p, array['date_limit', 'start', 'num'], 'null');
          END IF;
          IF p->'date_limit'->'end'->>'num' = ''
          THEN
            p := jsonb_set(p, array['date_limit', 'end', 'num'], 'null');
          END IF;

          IF p->'date_limit'->'start'->>'num' IS NOT NULL AND p->'date_limit'->'start'->>'num' != ''
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'start', 'num'], to_jsonb((p->'date_limit'->'start'->>'num')::int));
          END IF;

          IF p->'date_limit'->'start'->>'active' IS NOT NULL
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'start', 'active'], to_jsonb((p->'date_limit'->'start'->>'active')::boolean));
          END IF;

          IF p->'date_limit'->'end'->>'reference' = 'current'
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'end', 'reference'], to_jsonb('now'::text));
          END IF;

          IF p->'date_limit'->'end'->>'num' IS NOT NULL
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'end', 'num'], to_jsonb((p->'date_limit'->'end'->>'num')::int));
          END IF;

          IF p->'date_limit'->'start'->>'active' IS NULL
          THEN
            p := jsonb_set(p, array['date_limit', 'start', 'active'], to_jsonb(false));
          END IF;
          IF p->'date_limit'->'end'->>'active' IS NULL
          THEN
            p := jsonb_set(p, array['date_limit', 'end', 'active'], to_jsonb(false));
          END IF;

          IF p->'date_limit'->'end'->>'active' IS NOT NULL
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'end', 'active'], to_jsonb((p->'date_limit'->'end'->>'active')::boolean));
          END IF;

        END IF;

        IF r->'label' = ''
        THEN
          r := r || 'label=>null'::hstore;
        END IF;

        IF p->>'text_content' IS NOT NULL
        THEN
          json := jsonb_build_object(
            'help', r->'help',
            'id', (r->'id')::int,
            'is_group', false,
            'properties', p,
            'label', COALESCE(r->'label', r->'original_label'),
            'label_multiple', r->'label_multiple',
            'multiple_values', null,
            'required', (r->'value_mandatory')::boolean,
            'type', 'text_block',
            'values', null
          );
          ELSE
            json := jsonb_build_object(
                'catalogue_id', CASE WHEN r->'bibliotheek_kenmerken_id' IS NOT NULL THEN
                  (r->'bibliotheek_kenmerken_id')::int
                ELSE null END,
                'default_value', r->'value_default',
                'help', r->'help',
                'id', (r->'id')::int,
                'is_group', false,
                'is_system', (r->'is_systeemkenmerk')::boolean,
                'label', COALESCE(r->'label', r->'original_label'),
                'label_multiple', r->'label_multiple',
                'limit_values', CASE WHEN (r->'type_multiple')::boolean = true THEN -1 ELSE 1 END,
                'magic_string', r->'magic_string',
                'multiple_values', (r->'type_multiple')::boolean,
                'original_label', r->'original_label',
                'permissions', casetype_v1_permissions(r->'required_permissions'),
                'pip', COALESCE((r->'pip')::boolean, false),
                'properties', p,
                'publish_public', COALESCE((r->'publish_public')::boolean, false),
                'referential', (r->'referential')::boolean,
                'required', (r->'value_mandatory')::boolean,
                'values', opt,
                'type', r->'value_type'
              );
        END IF;
      END IF;


    END IF;

    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION casetype_v1_attributes(
    IN kid int,
    IN zid int,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    r record;
    j jsonb;
  BEGIN

    json := '[]'::jsonb;

    FOR r IN
      SELECT
        ztk.*,
        bk.value_type,
        bk.magic_string,
        bk.value_default,
        bk.naam as original_label,
        bk.type_multiple,
        bk.properties as orig_properties,
        bk.relationship_type,
        bk.relationship_name,
        bk.relationship_uuid
      FROM zaaktype_kenmerken ztk
      LEFT JOIN bibliotheek_kenmerken bk
      ON ztk.bibliotheek_kenmerken_id = bk.id
      WHERE zaaktype_node_id = zid
      AND ztk.zaak_status_id = kid
      ORDER BY id
    LOOP
      SELECT INTO j casetype_v1_attribute_to_json(hstore(r));

      IF j IS NOT NULL
      THEN
        json := json || j;
      END IF;

    END LOOP;
    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION zaaktype_node_as_v1(
    IN ztn hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    properties JSONB;
    ztn_id int;
    zt_id int;
    zt record;
    ztd record;
    subject_types text[];
    preset_client jsonb;
    subject jsonb;
    subject_instance jsonb;
  BEGIN

    zt_id := (ztn->'zaaktype_id')::int;
    ztn_id := (ztn->'id')::int;

    select into zt * from zaaktype where id = zt_id;
    select into ztd * from zaaktype_definitie where id = (ztn->'zaaktype_definitie_id')::int;

    select into subject_types array_agg(ztb.betrokkene_type) from zaaktype_betrokkenen ztb where zaaktype_node_id = ztn_id;

    properties := (ztn->'properties')::jsonb;

    -- The the generated column is set AFTER the trigger is done, so we take
    -- the logic and run with it
    ztn['title_seo'] := lower(regexp_replace(ztn->'titel', '[^a-z0-9]+','-', 'gi'));

    select into subject subject_json_by_legacy_id(ztd.preset_client);
    IF subject IS NOT NULL
    THEN

      subject_instance := jsonb_build_object(
          'subject', subject,
          'subject_type', subject->>'type',
          'display_name', get_subject_display_name_by_uuid((subject->>'reference')::uuid),
          'old_subject_identifier', ztd.preset_client
      );

      preset_client := jsonb_build_object(
        'type', 'subject',
        'preview', subject_instance->>'display_name',
        'reference', subject->>'reference',
        'instance', subject_instance
      );

    END IF;

    json := jsonb_build_object(
      'preview', ztn->'titel',
      'reference', zt.uuid,
      'type', 'casetype',
      'instance', jsonb_build_object(
        'id', zt.uuid,
        'legacy', jsonb_build_object(
          -- TODO: strip version from content
          'version', ztn->'version',
          'zaaktype_node_id', ztn_id,
          'zaaktype_id', zt.id
        ),
        'offline', CASE WHEN zt.active = true
        THEN
          false
        ELSE
          true
        END,
        'title', ztn->'titel',
        'trigger', ztn->'trigger',
        'preset_client', preset_client,
        'properties', casetype_v1_properties(properties, hstore(ztd)),
        'settings', casetype_v1_settings(ztn),
        'results', casetype_v1_results(ztn_id),
        'queue_coworker_changes', coalesce((properties->>'queue_coworker_changes')::bool, false),
        'sources', '{ behandelaar, balie, telefoon, post, email, webformulier, sociale media, externe applicatie }'::text[],
        'phases', casetype_v1_phases(ztn_id),
        'public_url_path', public_uri_path_for_casetypes(
          zt.id,
          ztn->'title_seo',
          subject_types
        ),
        'subject_types', subject_types
      )
    );


  RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION zaaktype_node_v1_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    BEGIN
      SELECT INTO NEW.v1_json zaaktype_node_as_v1(hstore(NEW));
      RETURN NEW;
    END;
  $$;

  CREATE TRIGGER update_v1_json
  BEFORE INSERT
  OR UPDATE
  ON zaaktype_node
  FOR EACH ROW
  EXECUTE PROCEDURE zaaktype_node_v1_json();

COMMIT;



