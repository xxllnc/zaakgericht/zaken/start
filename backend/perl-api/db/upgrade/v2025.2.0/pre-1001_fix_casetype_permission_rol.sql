BEGIN;

  CREATE OR REPLACE FUNCTION casetype_v1_permissions(
    IN perm text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    j jsonb;
    p jsonb;
    i jsonb;
    v1_group jsonb;
    v1_role jsonb;

  BEGIN

    json := '[]'::jsonb;

    IF perm IS NULL OR perm = ''
    THEN
      RETURN;
    END IF;

    j := perm::jsonb->'selectedUnits';
    IF j IS NULL OR j::text = '[]'
    THEN
      RETURN;
    END IF;

    FOR i IN SELECT * FROM jsonb_array_elements(j)
    LOOP
      SELECT INTO v1_group v1_json FROM groups WHERE id = (i->>'org_unit_id')::int;
      select into v1_role v1_json FROM roles WHERE id = (i->>'role_id')::int;

      -- transform group and role here
      v1_group := jsonb_set(v1_group, '{instance,id}', to_jsonb((v1_group->'instance'->>'group_id')::int));
      v1_role := jsonb_set(v1_role, '{instance,id}', to_jsonb((v1_role->'instance'->>'role_id')::int));

      IF v1_group IS NOT NULL
      THEN
        v1_group := v1_group #- '{instance,group_id}';
        v1_group := (v1_group || '{"reference": null}'::jsonb);
      ELSE
        v1_group := jsonb_build_object(
          'instance', jsonb_build_object(
            'name', 'Deleted group',
            'id', 0,
            'invalid', TRUE  
          ),
          'type', 'group',
          'reference', null
        );
      END IF;

      IF v1_role IS NOT NULL
      THEN
        v1_role := v1_role #- '{instance,role_id}';
        v1_role := (v1_role || '{"reference": null}'::jsonb);
      ELSE
        v1_role := jsonb_build_object(
          'instance', jsonb_build_object(
            'name', 'Deleted role',
            'system_role', FALSE,
            'id', 0,
            'invalid', TRUE  
          ),
          'type', 'role',
          'reference', null
        );
      END IF;

      json := json || jsonb_build_object('group', v1_group, 'role', v1_role);
    END LOOP;

  END;
  $$;

COMMIT;
