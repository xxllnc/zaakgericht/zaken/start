BEGIN;

DROP INDEX IF EXISTS transaction_error_count_found_idx;
CREATE INDEX IF NOT EXISTS transaction_error_count_found_idx ON public.transaction(error_count) WHERE ((error_count > 0) AND (date_deleted IS null));

COMMIT;