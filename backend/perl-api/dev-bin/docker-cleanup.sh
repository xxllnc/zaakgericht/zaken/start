#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."

CURRENT_BACKEND_REVISION=$(head -n1 "${DIR}/docker/Dockerfile.backend" | awk -F: '{print $2}')

OLD_IMAGES=$(docker images --format '{{.Tag}} {{.ID}}' registry.gitlab.com/zaaksysteem/zaaksysteem-perl| grep -v "$CURRENT_BACKEND_REVISION")
if [ -n "$OLD_IMAGES" ]; then
    # Only delete the image ID and not the tag, otherwise docker rmi
    # complains about none-existing images
    docker rmi $(echo $OLD_IMAGES| awk '{print $NF}');
fi

docker image prune
