#!/bin/sh

hostname="dev.zaaksysteem.nl"

basedir=${1:-/etc/nginx/ssl}

cd $basedir

deploy_openssl_conf() {

    size=0;
    [ -e openssl.cnf ] && size=$(stat --format %s -- openssl.cnf)
    [ $size -ne 0 ] && return 0;

    cat > openssl.cnf <<EOT
[ req ]
default_bits            = 2048
distinguished_name      = req_distinguished_name

[ req_distinguished_name ]
commonName                      = Common Name

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true
keyUsage = critical,digitalSignature,cRLSign,keyCertSign

[ server_cert ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:false
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth,clientAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = $hostname
DNS.2 = *.$hostname
EOT

}
check_valid_cert() {
    [ ! -e "$1" ] && return 1
    openssl x509 -checkend 86400 -noout -in "$1"
}

generate_ca_key_cert() {
    set -e

    ca_days=3650

    openssl genrsa -out ca.key 2048
    openssl req \
        -config $basedir/openssl.cnf \
        -extensions v3_ca \
        -new \
        -x509 \
        -days $ca_days \
        -sha256 \
        -key ca.key \
        -out ca.crt \
        -subj "/CN=Zaaksysteem Development CA"
    set +e
}

generate_server_key_cert() {
    set -e

    days=$(shuf -i 45-90 -n1)
    serial=$(shuf -i 1979-1180000 -n1)

    openssl genrsa -out server.key 2048
    openssl req \
        -config $basedir/openssl.cnf \
        -extensions server_cert \
        -new \
        -subj "/CN=$hostname" \
        -key server.key \
        -out server.csr
    openssl x509 \
        -extfile $basedir/openssl.cnf \
        -extensions server_cert \
        -req \
        -days $days \
        -sha256 \
        -in server.csr \
        -CA ca.crt \
        -CAkey ca.key \
        -set_serial $serial \
        -out server-only.crt
    set +e
    cat server-only.crt ca.crt > server.crt
}

deploy_openssl_conf

check_valid_cert "ca.crt"

if [ $? -ne 0 ]
then
    rm -f {server,server-only,ca}.{crt,key,csr}
    generate_ca_key_cert
fi

check_valid_cert "server-only.crt"

[ $? -ne 0 ] && generate_server_key_cert

cat ca.crt /etc/ssl/certs/ca-certificates.crt > bundle.crt
