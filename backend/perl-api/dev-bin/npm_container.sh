#!/usr/bin/env bash

PATH=$PATH:/usr/local/bin

set -e

CONTAINER_BUILD_DIR="/opt/zaaksysteem"

cd ${CONTAINER_BUILD_DIR}/client
echo Step 1/4: Installing '/client' dependencies
npm install --omit=optional

cd ${CONTAINER_BUILD_DIR}/frontend
echo Step 2/4: Installing '/frontend' dependencies
npm install --omit=optional

cd ${CONTAINER_BUILD_DIR}/client
echo Step 3/4: Building '/client'
npm run build

cd ${CONTAINER_BUILD_DIR}/frontend
echo Step 4/4: Building '/frontend'
npm run build
