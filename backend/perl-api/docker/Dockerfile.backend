ARG IMAGE=registry.gitlab.com/xxllnc/zaakgericht/zaken/start/backend/perl-base:master-latest
ARG BUILD_TIMESTAMP=unknown
FROM ${IMAGE} AS production

USER zaaksysteem
ENV ZAAKSYSTEEM_HOME /opt/zaaksysteem
ENV PERL5LIB /opt/zaaksysteem/lib:$PERL5LIB
# We do this on container start time
#EXPOSE 9083
COPY backend/perl-api/docker/inc/backend/entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["perl", "/opt/zaaksysteem/script/zaaksysteem_fastcgi.pl", "-l", ":9083"]

USER root

RUN apt-get update \
    && apt-get install -y --no-install-recommends sudo git \
    && apt-get autoremove --purge -yqq \
    && cpanm https://github.com/waterkip/perl-Net-SAML2.git@zs-13313 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

COPY backend/perl-api/docker/inc/sudo/* /etc/sudoers.d/
COPY backend/shared/certs/*.crt /usr/local/share/ca-certificates

RUN chmod 0400 /etc/sudoers.d/*

RUN update-ca-certificates

WORKDIR /opt/zaaksysteem

COPY backend/perl-api/script  script
COPY backend/perl-api/share   share
COPY backend/perl-api/bin     bin
COPY backend/perl-api/dev-bin dev-bin
COPY backend/perl-api/xt      xt
COPY backend/perl-api/t       t
COPY backend/perl-api/root    root
COPY backend/perl-api/lib     lib

COPY etc/csp.json /etc/zaaksysteem/csp.json

RUN perl "-MExtUtils::Manifest=mkmanifest" -e mkmanifest; \
    egrep "\.(pod|pm)" MANIFEST | egrep -v '^(MANIFEST.pod)' > MANIFEST.pod
RUN echo ${BUILD_TIMESTAMP} > /build_timestamp.backend

USER zaaksysteem

FROM production AS development

USER root
# Some things break more easily than others
RUN cpanm -n MooseX::Daemonize \
    && cpanm DBIx::Class::Schema::Loader Catalyst::Helper
RUN apt-get update \
    && apt-get install -y --no-install-recommends libxml2-utils vim-tiny \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

USER zaaksysteem
