ARG BUILD_TIMESTAMP=unknown
# Force rebuild for perl 5.38.2
FROM perl:stable AS build

ENV DEBIAN_FRONTEND=noninteractive \
  NO_NETWORK_TESTING=1

COPY backend/perl-api/dev-bin/cpanm /usr/local/bin/docker-cpanm

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends ca-certificates-java \
  && apt-get install -y --no-install-recommends \
      desktop-file-utils \
      gcc \
      git \
      ca-certificates \
      gpg \
      libc6-dev \
      libexpat1-dev \
      libmagic-dev \
      libpq-dev \
      libssl-dev \
      libssl3 \
      openssl \
      zlib1g-dev \
      libxml2-dev \
      locales \
      poppler-utils \
      shared-mime-info \
      unzip \
      uuid-dev \
      xmlsec1 \
      curl \
      pdftk \
  && localedef -i nl_NL -c -f UTF-8 \
  -A /usr/share/locale/locale.alias nl_NL.UTF-8

RUN docker-cpanm IPC::System::Simple
RUN docker-cpanm File::ShareDir::Install
# IO::Socket::SSL borks when Net::SSLeay isn't installed
RUN docker-cpanm Net::SSLeay~'>=1.46'
RUN docker-cpanm LWP::Protocol::https
RUN docker-cpanm DBIx::Class

# CPAN modules with issues
RUN docker-cpanm Catalyst::Runtime
# Class::C3 installed because there is some dependency resolution thing
# with Catalyst::Plugin::Params::Profile in the cpanfile.
RUN docker-cpanm Class::C3
# 2018-01-11: Compile::WSDL11 is a dependency of Catalyst::Controller::SOAP
# but it has not been declared in the META.json. we potentionally could end up
# that the dependecy tree does not notice it and starts building C::C::SOAP
# before Compile::WSDL11
# https://rt.cpan.org/Public/Bug/Display.html?id=95327
RUN docker-cpanm XML::Compile::WSDL11
# Make sure CGI::Simple is installed at a version which deals with it
# dependencies
RUN docker-cpanm CGI::Simple~'>=1.14'
RUN docker-cpanm Class::Accessor::Fast
# Manual addition of a module that isn't indexed on CPAN and creates
# issues when you build via a local mirror, dep of Net::OpenStack::Swift
RUN curl -q \
  http://mirror.nl.leaseweb.net/CPAN/authors/id/M/MK/MKODERER/Sys-CPU-0.52.tar.gz \
  --output Sys-CPU-0.52.tar.gz \
  && echo "34305423e86cfca9a631b6f91217f90f  Sys-CPU-0.52.tar.gz" \
  | md5sum -c - \
  && docker-cpanm Sys-CPU-0.52.tar.gz
# FluentD logger
RUN docker-cpanm Data::MessagePack::Stream@1.04
# ... things?
RUN docker-cpanm Catalyst::Plugin::Cache::HTTP::Preempt

RUN docker-cpanm https://gitlab.com/waterkip/file-archivableformats.git@v1.5.3
# Install dependencies from the local build tree
COPY ["backend/p5-bttw-tools", "/opt/dependencies/p5-bttw-tools/"]
RUN docker-cpanm --installdeps /opt/dependencies/p5-bttw-tools/ \
  && docker-cpanm /opt/dependencies/p5-bttw-tools/


# CPAN modules via cpanm
COPY backend/perl-api/cpanfile .
RUN docker-cpanm --installdeps .

FROM perl:stable-slim AS production
ENV DEBIAN_FRONTEND=noninteractive \
  NO_NETWORK_TESTING=1

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends ca-certificates-java \
  && apt-get --no-install-recommends -y install \
     ca-certificates \
     curl \
     desktop-file-utils \
     gpg \
     libexpat1 \
     libmagic1 \
     libpq5 \
     libssl3 \
     locales \
     openssl \
     pdftk \
     poppler-utils \
     shared-mime-info \
     unzip \
     uuid-runtime \
     pandoc \
  && localedef -i nl_NL -c -f UTF-8 \
  -A /usr/share/locale/locale.alias nl_NL.UTF-8 \
  && apt-get autoremove --purge -yqq\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm \
  && groupadd -g 1001 zaaksysteem \
  && useradd -ms /bin/bash -u 1001 -g 1001 zaaksysteem -d /opt/zaaksysteem \
  && chown -R zaaksysteem /opt/zaaksysteem

COPY --from=build /usr/local/lib/perl5/ /usr/local/lib/perl5/
COPY --from=build /usr/local/bin /usr/local/bin

RUN echo ${BUILD_TIMESTAMP} > /build_timestamp.perl

USER zaaksysteem
WORKDIR /opt/zaaksysteem
