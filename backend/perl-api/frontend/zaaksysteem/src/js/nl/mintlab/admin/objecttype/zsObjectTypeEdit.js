// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.admin').directive('zsObjectTypeEdit', [
    function () {
      return {
        controller: [
          '$scope',
          function ($scope) {
            var ctrl = this;

            ctrl.getObjectValues = $scope.getObjectValues;

            ctrl.getObjectTypeId = function () {
              return $scope.objectTypeId;
            };
          },
        ],
      };
    },
  ]);
})();
