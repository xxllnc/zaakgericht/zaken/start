// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';

  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.AppointmentV2Controller', [
      '$scope',
      '$element',
      'smartHttp',
      'translationService',
      function ($scope, $element, smartHttp, translationService) {
        $scope.getLabel = function (value) {
          return value.label;
        };

        window.top.addEventListener('message', (event) => {
          if ($scope.endpoint.includes(event.origin)) {
            if (event.data && event.data.type === 'appointmentSubmit') {
              $scope.appointment = event.data.data || null;
              $scope.closePopup();
              $scope.title = event.data.data
                ? $scope.updateButtonText
                : $scope.createButtonText;

              var fieldIdFull = 'kenmerk_id_' + $scope.fieldId;
              var data = {
                [fieldIdFull]: event.data.data,
                toelichting: '',
                update: fieldIdFull,
              };

              if ($scope.isPip) {
                smartHttp
                  .connect({
                    method: 'POST',
                    url:
                      '/pip/zaak/' +
                      $scope.caseId +
                      '/request_attribute_update/' +
                      $scope.fieldId,
                    data: data,
                  })
                  .error(function (/*response*/) {
                    $scope.$emit('systemMessage', {
                      type: 'error',
                      content: translationService.get(
                        'Er ging iets fout bij het opslaan van uw wijziging. Probeer het later opnieuw'
                      ),
                    });
                  });
              }
            }
          }
        });

        $scope.init = function (
          appointment,
          endpoint,
          createButtonText,
          updateButtonText
        ) {
          if (!$scope.appointment) {
            $scope.appointment = null;
          }

          $scope.endpoint = endpoint;
          $scope.createButtonText = createButtonText;
          $scope.updateButtonText = updateButtonText;
          $scope.title = appointment
            ? $scope.updateButtonText
            : $scope.createButtonText;
          $scope.fieldId = $scope.field_id.split('_')[1];
          $scope.isPip = window.location.pathname.split('/')[1] === 'pip';
          $scope.caseId = $scope.case_id;
        };

        $scope.initIframe = function (endpoint) {
          var iframe = document.getElementById('appointment-iframe');

          iframe.src = $scope.endpoint;
          iframe.style.width = '100%';
          iframe.style.height = 'calc(100% - 5px)';
          iframe.style.minHeight = '550px';
          iframe.style.minWidth = '400px';
          iframe.title = 'zsAppointment';
          iframe.allow = 'fullscreen; geolocation';
          iframe.addEventListener('load', () => {
            iframe.contentWindow.postMessage(
              {
                type: 'appointmentInit',
                data: {
                  id: ($scope.appointment || {}).id,
                },
              },
              '*'
            );
          });
        };
      },
    ]);
})();
