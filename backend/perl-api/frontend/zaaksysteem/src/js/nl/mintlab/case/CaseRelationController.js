// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.CaseRelationController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        var indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
          arrayMove = window.zsFetch('nl.mintlab.utils.collection.arrayMove'),
          safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

        $scope.relations = [];

        function parseData(data) {
          var relations = data.result || [],
            i,
            l,
            rel;

          for (i = 0, l = relations.length; i < l; ++i) {
            rel = relations[i];
            rel.order = i;
          }

          $scope.relations = relations;
        }

        $scope.reloadData = function () {
          smartHttp
            .connect({
              method: 'GET',
              url: 'zaak/' + $scope.caseId + '/relations',
            })
            .success(function onSuccess(data) {
              parseData(data);
            })
            .error(function onError() {});
        };

        $scope.initRelations = function () {
          $scope.reloadData();
        };

        $scope.relate = function (caseObj) {
          var caseId = caseObj.id,
            relation;

          relation = {
            case: caseObj,
          };

          $scope.relations.push(relation);

          smartHttp
            .connect({
              method: 'POST',
              url: 'zaak/' + $scope.caseId + '/relations/add',
              data: {
                case_id: caseId,
              },
            })
            .success(function onSuccess(data) {
              var result = data.result[0];
              for (var key in result) {
                relation[key] = result[key];
              }
            })
            .error(function onError(/*data*/) {
              var index = indexOf($scope.relations, relation);
              if (index !== -1) {
                $scope.relations.splice(index, 1);
              }
            });
        };

        $scope.unrelate = function (relation) {
          var relationId = relation.id,
            index = indexOf($scope.relations, relation);

          if (index !== -1) {
            $scope.relations.splice(index, 1);
          }

          smartHttp
            .connect({
              method: 'POST',
              url: 'zaak/' + $scope.caseId + '/relations/remove',
              data: {
                relation_id: relationId,
              },
            })
            .success(function onSuccess(/*data*/) {})
            .error(function onError(/*data*/) {
              $scope.relations.push(relation);
            });
        };

        $scope.$on('zs.sort.update', function (event, data, before) {
          safeApply($scope, function () {
            var relation,
              to = before
                ? _.findIndex($scope.relations, { id: before.id })
                : $scope.relations.length - 1;

            relation = _.find($scope.relations, { id: data.id });

            arrayMove($scope.relations, relation, to);
          });
        });

        $scope.$on('zs.sort.commit', function (event, data) {
          safeApply($scope, function () {
            var index = _.findIndex($scope.relations, { id: data.id }),
              after = $scope.relations[index - 1],
              afterId = after ? after.id : null;

            smartHttp
              .connect({
                url: 'zaak/' + $scope.caseId + '/relations/move',
                method: 'POST',
                data: {
                  relation_id: data.id,
                  after: afterId,
                },
              })
              .error(function (/*data*/) {
                smartHttp.emitDefaultError();
              });
          });
        });
      },
    ]);
})();
