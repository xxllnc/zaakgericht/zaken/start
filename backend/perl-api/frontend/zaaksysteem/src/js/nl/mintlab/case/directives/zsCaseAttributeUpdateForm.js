// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseAttributeUpdateForm', [
    'smartHttp',
    'translationService',
    function (smartHttp, translationService) {
      return {
        scope: false,
        link: function (scope, element /*, attrs*/) {
          function getFormData() {
            // TODO: remove $ from controller
            var values = $(element)
                .find('input, textarea, select')
                .serializeArray(),
              data = {};

            _.each(values, function (item) {
              var name = item.name;
              if (data[name] !== undefined && !angular.isArray(data[name])) {
                data[name] = [data[name]];
              }
              if (data[name]) {
                data[name].push(item.value);
              } else if (scope.attributeType === 'valuta') {
                data[name] = item.value
                  .replace(/,/g, '.')
                  .replace('€', '')
                  .trim();
              } else {
                data[name] = item.value;
              }
            });

            return data;
          }

          scope.submitChange = function () {
            var base_url = scope.pip ? '/pip/zaak/' : '/api/case/';

            smartHttp
              .connect({
                method: 'POST',
                url:
                  base_url +
                  scope.caseId +
                  '/request_attribute_update/' +
                  scope.bibliotheekId,
                data: getFormData(),
              })
              .success(function (response) {
                scope.fieldTemplate =
                  response.result[0].attribute_value_as_html;
                scope.editMode = false;
                scope.isPending = true;
              })
              .error(function (/*response*/) {
                scope.editMode = false;
                scope.$emit('systemMessage', {
                  type: 'error',
                  content: translationService.get(
                    'Er ging iets fout bij het opslaan van uw wijziging. Probeer het later opnieuw'
                  ),
                });
              });

            return false;
          };
        },
      };
    },
  ]);
})();
