// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case.relation').directive('zsPlannedCaseAdd', [
    'objectService',
    'objectRelationService',
    'plannedCaseService',
    function (objectService, objectRelationService, plannedCaseService) {
      return {
        require: ['zsPlannedCaseAdd', '^zsCaseView'],
        controller: [
          function () {
            var ctrl = this,
              zsCaseView;

            ctrl.link = function (controllers) {
              zsCaseView = controllers[0];
            };

            ctrl.handleEditSubmit = function ($values) {
              var caseObj = zsCaseView.getCase(),
                casetype = $values.casetype,
                params = _.pick($values, [
                  'interval_period',
                  'next_run',
                  'interval_value',
                  'runs_left',
                  'copy_relations',
                ]);

              plannedCaseService.addPlannedCase(caseObj, casetype, params);
            };

            ctrl.object = objectService.createObject('scheduled_job');

            return ctrl;
          },
        ],
        controllerAs: 'plannedCaseAdd',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
