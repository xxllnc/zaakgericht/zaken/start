// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').service('formService', [
    'bagService',
    function (bagService) {
      var formsByName = {},
        attributeMap = {},
        bagStreetLabel = bagService.getStreetLabel(),
        bagLabel = bagService.getLabel(),
        parsers = {},
        formatters = {};

      attributeMap = {
        url: {
          type: 'url',
        },
        text: {
          type: 'text',
        },
        richtext: {
          type: 'richtext',
        },
        email: {
          type: 'email',
        },
        image_from_url: {
          type: 'url',
        },
        text_uc: {
          type: 'text',
          data: {
            transform: 'uppercase',
          },
        },
        numeric: {
          type: 'number',
        },
        valuta: {
          type: 'price',
        },
        valutain: {
          type: 'price',
        },
        valutaex: {
          type: 'price',
        },
        valutain6: {
          type: 'price',
        },
        valutain21: {
          type: 'price',
        },
        valutaex21: {
          type: 'price',
        },
        date: {
          type: 'date',
        },
        'date-range': {
          type: 'date-range',
          data: {
            options: [
              {
                label: 'Alles',
                value: 'all',
              },
              {
                label: 'Afgelopen maand',
                value: 'last_month',
              },
              {
                label: 'Deze maand',
                value: 'this_month',
              },
              {
                label: 'Afgelopen week',
                value: 'last_week',
              },
              {
                label: 'Deze week',
                value: 'this_week',
              },
              {
                label: 'Specifieke periode',
                value: 'custom_range',
              },
            ],
          },
        },
        googlemaps: {
          // 'type': 'map'
          type: 'spot-enlighter',
          data: {
            restrict: 'bag',
            resolve: 'id',
            multi: false,
            label: bagLabel,
          },
        },
        geolatlon: {
          type: 'map',
        },
        textarea: {
          type: 'textarea',
        },
        option: {
          type: 'radio',
        },
        select: {
          type: 'select',
        },
        checkbox: {
          type: 'checkbox-list',
        },
        file: {
          type: 'file',
        },
        calendar: {
          type: 'appointment',
        },
        calendar_supersaas: {
          type: 'appointment',
        },
        bag_straat_adres: {
          type: 'spot-enlighter',
          data: {
            restrict: 'bag',
            resolve: 'id',
            multi: false,
            label: bagLabel,
          },
        },
        bag_straat_adressen: {
          type: 'spot-enlighter',
          data: {
            restrict: 'bag',
            resolve: 'id',
            multi: true,
            label: bagLabel,
          },
        },
        bag_adres: {
          type: 'spot-enlighter',
          data: {
            restrict: 'bag',
            resolve: 'id',
            multi: false,
            label: bagLabel,
          },
        },
        bag_adressen: {
          type: 'spot-enlighter',
          data: {
            restrict: 'bag',
            resolve: 'id',
            multi: true,
            label: bagLabel,
          },
        },
        bag_openbareruimte: {
          type: 'spot-enlighter',
          data: {
            restrict: 'bag-street',
            resolve: 'id',
            multi: false,
            label: bagStreetLabel,
          },
        },
        bag_openbareruimtes: {
          type: 'spot-enlighter',
          data: {
            restrict: 'bag-street',
            resolve: 'id',
            multi: true,
            label: bagStreetLabel,
          },
        },
        subject: {
          type: 'spot-enlighter',
          data: {
            restrict: 'contact',
            resolve: 'id',
            multi: true,
            label: 'decorated_name||handelsnaam',
          },
        },
        bankaccount: {
          type: 'bankaccount',
        },
      };

      parsers.date = function (value) {
        var parsed = null,
          date = new Date(value ? value.from : undefined);

        if (!isNaN(date.getTime())) {
          parsed = date.toISOString();
        }

        return parsed;
      };

      formatters.date = function (value) {
        var formatted = null,
          date = new Date(value);

        if (value !== null && value !== undefined && !isNaN(date.getTime())) {
          formatted = {
            from: new Date(
              date.getFullYear(),
              date.getMonth(),
              date.getDate(),
              0
            ).getTime(),
            to:
              new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate() + 1,
                0
              ).getTime() - 1,
          };
        }

        return formatted;
      };

      formatters.select = function (value) {
        var formatted = _.isArray(value) ? value[0] : value;

        return formatted;
      };

      formatters.file = function (value) {
        var formatted = !_.isArray(value) ? [value] : value;

        formatted = _.map(formatted, function (file) {
          var clone = angular.copy(file);

          clone.original_name = clone.original_name || clone.filename;

          return clone;
        });

        return formatted;
      };

      formatters.number = function (value) {
        var formatted = value;

        if (typeof value === 'string') {
          formatted = value === '' ? value : Number(value);
        }

        return formatted;
      };

      return {
        register: function (form) {
          formsByName[form.getName()] = form;
        },
        unregister: function (form) {
          delete formsByName[form.getName()];
        },
        get: function (name) {
          return formsByName[name];
        },
        getFormFieldForAttributeType: function (attrType) {
          var type = attributeMap[attrType] || attributeMap.text;
          return _.cloneDeep(type);
        },
        formatValue: function (type, value) {
          var formatter = formatters[type],
            val = value;

          if (formatter) {
            val = formatter(value);
          }

          return val;
        },
        parseValue: function (type, value) {
          var parser = parsers[type],
            val = value;

          if (parser) {
            val = parser(value);
          }

          return val;
        },
      };
    },
  ]);
})();
