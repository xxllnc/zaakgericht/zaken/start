// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.FileController', [
      '$scope',
      '$window',
      'smartHttp',
      'translationService',
      'snackbarService',
      function (
        $scope,
        $window,
        smartHttp,
        translationService,
        snackbarService
      ) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
          docStatuses;

        $scope.preview = false;
        $scope.collapsed = true;
        $scope.update = null;
        $scope.editMode = false;

        $scope.validTrustLevels = [
          'Openbaar',
          'Beperkt openbaar',
          'Intern',
          'Zaakvertrouwelijk',
          'Vertrouwelijk',
          'Confidentieel',
          'Geheim',
          'Zeer geheim',
        ];

        $scope.validOrigins = ['Inkomend', 'Uitgaand', 'Intern'];

        docStatuses = [
          {
            value: 'original',
            label: 'Origineel',
          },
          {
            value: 'copy',
            label: 'Kopie',
          },
          {
            value: 'replaced',
            label: 'Vervangen',
          },
          {
            value: 'converted',
            label: 'Geconverteerd',
          },
        ];

        $scope.getDocumentStatuses = function () {
          return docStatuses;
        };

        $scope.toggleCollapse = function (event) {
          $scope.collapsed = !$scope.collapsed;
          if (event) {
            event.stopPropagation();
          }
        };

        $scope.onFileNameClick = function (event) {
          var isIntake = $scope.isIntake,
            url = isIntake
              ? 'zaak/intake/' + $scope.entity.id + '/download/pdf?inline=1'
              : 'zaak/' +
                $scope.caseId +
                '/document/' +
                $scope.entity.id +
                '/download/pdf?inline=1';

          if ($scope.pip) {
            url = 'pip/' + url;
          }

          $window.open(smartHttp.getUrl(url), '_blank');
          event.stopPropagation();
        };

        $scope.onRemoveCaseDocClick = function (caseDoc, event) {
          $scope.removeCaseDoc([$scope.entity], caseDoc);
          event.stopPropagation();
        };

        $scope.saveAttr = function (key) {
          var data = {
            file_id: $scope.entity.id,
          };

          if (key.indexOf('metadata_id') === 0) {
            data.metadata = $scope.update.metadata_id;
          } else {
            data[key] = $scope.update[key];
          }

          $scope.entity.updating = true;

          smartHttp
            .connect({
              url: 'file/update',
              method: 'POST',
              data: data,
              blocking: false,
            })
            .success(function (data) {
              var fileData = data.result ? data.result[0] : null;
              $scope.entity.updateWith(fileData);
              if (key === 'is_revision') {
                $scope.entity.is_revision = $scope.update.is_revision;
              }
            })
            .error(function (/*data*/) {
              $scope.update = $scope.entity.clone();
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Er ging iets fout bij het opslaan van de eigenschappen. Probeer het later opnieuw.'
                ),
              });
            })
            .then(function () {
              $scope.entity.updating = false;
            });
        };

        $scope.setEditMode = function (editMode) {
          $scope.editMode = editMode;
        };

        $scope.onPropertyFormButtonClick = function (event) {
          event.stopPropagation();
        };

        // TODO(dario): refactor common methods in StoredEntityController
        $scope.$on('editsave', function (event /*, key, value*/) {
          event.stopPropagation();
        });

        $scope.$on('editcancel', function (event /*, key, value*/) {
          event.stopPropagation();
        });

        $scope.$on('drop', function (event, data /*, mimetype*/) {
          safeApply($scope, function () {
            var caseDoc = data.caseDoc,
              clearPrevious = data.clearPrevious;

            if (clearPrevious) {
              $scope.moveCaseDoc(clearPrevious, $scope.entity, caseDoc);
            } else {
              $scope.assignCaseDoc([$scope.entity], caseDoc);
            }
            event.stopPropagation();
          });
        });

        $scope.$on('startdrag', function (event, element, mimetype) {
          if (
            mimetype !== 'zs/file' ||
            element[0].className.indexOf('disabled') !== -1
          ) {
            event.stopPropagation();
          }
        });

        $scope.isDisabled = function (entity) {
          if (
            entity.lock_subject_id !== null &&
            entity.lock_subject_id !== $scope.userId &&
            !entity.shared
          ) {
            return true;
          }
          return false;
        };

        $scope.isConfidential = function (entity) {
          if (entity.getEntityType() !== 'file') {
            return false;
          }

          if (entity.confidential === 1) {
            return true;
          }

          return false;
        };

        $scope.$watch('entity', function () {
          if ($scope.entity && $scope.entity.is_revision === undefined) {
            $scope.entity.is_revision = $scope.entity.is_duplicate_of ? 1 : 0;
          }
          $scope.update = $scope.entity ? $scope.entity.clone() : null;
        });
      },
    ]);
})();
