// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.docs').directive('zsCaseDocumentImageUploader', [
    '$interpolate',
    function ($interpolate) {
      return {
        restrict: 'E',
        scope: {
          title: '@',
          id: '@',
          onUpload: '&',
        },
        template: (
          '<button title="{{title}}" class="toolbar-button {{id | lowercase}}" data-ng-click="caseDocumentImageUploader.browse()"></button>' +
          '<form id="imageForm">' +
          '<input type="file" id="imageLoader" accept="image/*"/>' +
          '</form>'
        )
          .replace(/{{/g, $interpolate.startSymbol())
          .replace(/}}/g, $interpolate.endSymbol()),
        priority: 1,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            var ctrl = this,
              reader = new FileReader(),
              fileType,
              content;

            $element.find('form').css('display', 'none');

            ctrl.browse = function () {
              var input = $element.find('input')[0];
              input.addEventListener(
                'change',
                function (e) {
                  var file = e.target.files[0];
                  fileType = file.type;
                  reader.addEventListener('load', onUploadDone);
                  reader.addEventListener('error', onUploadError);
                  reader.readAsDataURL(file);
                },
                false
              );
              $element.find('form')[0].reset();
              input.click();
            };

            function onUploadDone() {
              var hiddenImage = new Image();
              content = reader.result;
              hiddenImage.src = content;
              hiddenImage.addEventListener('load', function () {
                $scope.onUpload({
                  $fileType: fileType,
                  $imageData: content.substring(content.indexOf(',') + 1),
                  $imageWidth: hiddenImage.width,
                  $imageHeight: hiddenImage.height,
                });
              });
            }
            function onUploadError() {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: 'Het is niet mogelijk dit plaatje laden.',
              });
            }

            return ctrl;
          },
        ],
        controllerAs: 'caseDocumentImageUploader',
      };
    },
  ]);
})();
