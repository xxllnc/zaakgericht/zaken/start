// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.docs').directive('zsCaseDocumentViewer', [
    '$http',
    function ($http) {
      function isPdfSupported() {
        var isIos = /iPad|iPhone|iPod/.test(navigator.userAgent);

        return navigator.mimeTypes['application/pdf'] || !isIos;
      }

      return {
        restrict: 'E',
        scope: {
          caseId: '&',
          fileId: '&',
          extension: '&',
          isIntake: '&',
        },
        template:
          '<iframe id="document-viewer" data-ng-src="{{caseDocumentViewer.getUrl()}}" width="400" height="300" allowfullscreen webkitallowfullscreen></iframe>',
        controller: [
          '$scope',
          '$element',
          '$document',
          function ($scope, $element, $document) {
            var ctrl = this,
              //support old model view for IE11 (case document tab, document intake)
              iframe = $document[0].getElementById('document-viewer'),
              usePdfJs = isPdfSupported() === false;

            if (usePdfJs) {
              iframe.addEventListener('load', function () {
                // this loadevent is needed so we can inject css.
                var iframeDoc = iframe.contentWindow.document,
                  link = iframeDoc.createElement('link');
                link.rel = 'stylesheet';
                link.type = 'text/css';
                link.href = '/css/pdfjs-custom.css';
                iframeDoc.getElementsByTagName('head')[0].appendChild(link);
              });
            }

            ctrl.getDownloadUrl = function () {
              var url;

              if ($scope.isIntake()) {
                url = '/zaak/intake/' + $scope.fileId() + '/download';
              } else {
                url =
                  '/zaak/' +
                  $scope.caseId() +
                  '/document/' +
                  $scope.fileId() +
                  '/download';
              }

              return url;
            };

            //this is used to display the PDF version of the file
            ctrl.getUrl = function () {
              var url,
                prefix = usePdfJs
                  ? '/pdf.js-with-viewer/web/viewer.html?file='
                  : '',
                url = prefix + ctrl.getDownloadUrl() + '/pdf?inline=1';

              return url;
            };

            return ctrl;
          },
        ],
        controllerAs: 'caseDocumentViewer',
      };
    },
  ]);
})();
