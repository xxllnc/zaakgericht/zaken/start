// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem')
    .controller('nl.mintlab.object.view.ObjectRelationController', [
      '$scope',
      'objectService',
      'translationService',
      function ($scope, objectService, translationService) {
        var ctrl = this,
          object = $scope.getObject();

        function hasRelation(obj, type) {
          var exists = !!_.find(objectService.getRelations(object), {
            related_object_type: type,
            related_object_id: objectService.getObjectId(obj),
          });
          return exists;
        }

        function isObject(obj, type) {
          return (
            $scope.type.id === type &&
            objectService.getObjectId(obj) === objectService.getObjectId(object)
          );
        }

        ctrl.addRelation = function (obj, type) {
          if (hasRelation(obj, type)) {
            $scope.$emit('systemMessage', {
              content: translationService.get(
                'U kunt een object slechts één keer toevoegen'
              ),
              type: 'error',
            });
          } else if (isObject(obj, type)) {
            $scope.$emit('systemMessage', {
              content: translationService.get(
                'U kunt niet het object relateren aan zichzelf'
              ),
              type: 'error',
            });
          } else {
            objectService.addRelation(object, obj, type);
          }
        };

        ctrl.removeRelation = function (obj, type) {
          objectService.removeRelation(object, obj, type);
        };

        $scope.$on('zs.spotenlighter.object.select', function (event, object) {
          if (object) {
            ctrl.addRelation(object, $scope.type.id);
            event.targetScope.obj = null;
          }
        });

        return ctrl;
      },
    ]);
})();
