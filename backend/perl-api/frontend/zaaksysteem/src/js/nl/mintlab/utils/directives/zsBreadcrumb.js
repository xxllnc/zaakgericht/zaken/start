// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsBreadcrumb', [
    '$parse',
    function ($parse) {
      return {
        controller: [
          '$element',
          '$scope',
          '$attrs',
          function ($element, $scope, $attrs) {
            var ctrl = this,
              parser = $parse($attrs.zsBreadcrumb),
              callback = $parse($attrs.zsBreadcrumbSelect);

            ctrl.select = function (value) {
              callback($scope, {
                $value: value,
              });
            };

            ctrl.isSelected = function (value) {
              return parser($scope) === value;
            };

            $element.addClass('breadcrumb');

            return ctrl;
          },
        ],
      };
    },
  ]);
})();
