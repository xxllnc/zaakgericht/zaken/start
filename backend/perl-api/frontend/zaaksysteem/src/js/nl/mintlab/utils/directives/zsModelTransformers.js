// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.directives').directive('zsModelTransformers', [
    '$parse',
    'formValidatorService',
    function ($parse, formValidatorService) {
      var filters = {
        date: {
          iso: {
            parser: function (value) {
              var date = new Date(value);
              return !isNaN(date.getTime()) ? date.toISOString() : undefined;
            },
            formatter: function (value) {
              var date = !value && value !== 0 ? null : new Date(value);
              return date ? date.getTime() : undefined;
            },
          },
        },
        ip: {
          v4: {
            parser: function (value) {
              return formValidatorService.validate('ipv4', value);
            },
            formatter: function (value) {
              return formValidatorService.validate('ipv4', value);
            },
          },
        },
      };

      return {
        require: ['ngModel'],
        link: function (scope, element, attrs, controllers) {
          var ngModel = controllers[0],
            transformers;

          transformers =
            scope.$eval(attrs.zsModelTransformers) || attrs.zsModelTransformers;

          if (transformers && !_.isArray(transformers)) {
            transformers = [transformers];
          }

          _(transformers)
            .map(function (transformer) {
              var name;

              if (typeof transformer === 'string') {
                name = transformer;
                transformer = angular.copy($parse(transformer)(filters));
                transformer.name = name;
              }

              return transformer;
            })
            .filter(_.identity)
            .value()
            .forEach(function (transformer) {
              ngModel.$parsers.unshift(function (val) {
                var value = transformer.parser(val);
                ngModel.$setValidity(
                  transformer.name,
                  ngModel.$isEmpty(val) || value !== undefined
                );
                return transformer.parser(value);
              });
              ngModel.$formatters.unshift(function (val) {
                var value = transformer.formatter(val);
                ngModel.$setValidity(
                  transformer.name,
                  ngModel.$isEmpty(val) || value !== undefined
                );
                return value;
              });
            });
        },
      };
    },
  ]);
})();
