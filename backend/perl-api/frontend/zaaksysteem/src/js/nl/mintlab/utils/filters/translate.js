// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.locale').filter('translate', [
    'translationService',
    function (translationService) {
      return function (/*from*/) {
        return translationService.get.apply(translationService, arguments);
      };
    },
  ]);
})();
