package Authen::SASL::Perl::XOAUTH2;
use Zaaksysteem::Moose;
use MooseX::NonMoose;

extends qw(Authen::SASL::Perl);

# We want to be the first (supported) auth method to try
sub _order {
    return 1;
}

# secflags is not used by XOAUTH2
sub _secflags {
    return 0;
}

sub mechanism {
    if (defined $_[1]) {
        # If Net::SMTP calls us with a parameter, it's trying to negotiate a SASL
        # mechanism. That doesn't work with XOAUTH2.
        throw("sasl/xoauth2_not_supported", "XOAUTH2 not supported by host");
    };
    return 'XOAUTH2';
} ;

sub client_start {
    my $self = shift;
    $self->{error} = undef;
    $self->{need_step} = 1;

    return "";
}

sub client_step {
    my $self = shift ;

    return sprintf(
        "user=%s\001auth=Bearer %s\001\001",
        $self->_call("user"),
        $self->_call("access_token"),
    );
}

1 ;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
