package Zaaksysteem::API::v1::Serializer::Reader::Controlpanel;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS ZAAKSYSTEEM_CSS_TEMPLATES];

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Controlpanel - Read ObjectData rows of type: controlpanel/instance/host

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::Controlpanel->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    my $reader_method = 'read_' . $object->object_class;

    if ($class->can($reader_method)) {
        return sub { $class->$reader_method(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }

=head2 read_controlpanel

Reader for controlpanel object

=cut

sig read_controlpanel => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_controlpanel {
    my ($class, $serializer, $object) = @_;

    my ($instances, $naw);
    if ($serializer->{v1_serializer_options}->{controlpanel_full}) {
        my $resultset       = $object->result_source->schema->resultset('ObjectData');
        my @relationships   = $resultset->search(
            {
                object_class => ['instance', 'naw'],
                %{ 
                    $resultset->map_search_args({
                        owner   => $object->get_object_attribute('owner')->value,
                    }) 
                },
            }
        )->all;

        $instances = [];
        for my $relationship (@relationships) {
            if ($relationship->object_class eq 'instance') {
                push(@$instances, $class->read_instance($serializer, $relationship));
            } elsif ($relationship->object_class eq 'naw') {
                $naw = $class->read_naw($serializer, $relationship);
            }
        }
    }

    return {
        type => 'controlpanel',
        reference => $object->id,
        instance => {
            id                      => $object->id,
            available_templates     => ZAAKSYSTEEM_CSS_TEMPLATES,
            (map { my $val = $class->parse_value($serializer, $object->get_object_attribute($_)->value); $_ => (defined $val ? $val : undef) }
                qw(owner template customer_type shortname domain read_only)),

            (map { my $val = $class->parse_value($serializer, $object->get_object_attribute($_)->value); $val += 0; $_ => (defined $val ? $val : undef) }
                qw(stat_diskspace allowed_diskspace allowed_instances)),

            ($instances ? (instances => $instances, naw => $naw) : ()),
        }
    };
}

=head2 read_instance

Reader for controlpanel/instance object

=cut

sig read_instance => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_instance {
    my ($class, $serializer, $objectdata, $options) = @_;
    $options ||= {};

    my @hosts;
    unless($options->{from_host}) {
        my @relationships = $objectdata->result_source->schema->resultset('ObjectRelationships')->search(
            {
                '-or'   => [
                    { object1_uuid => $objectdata->id, object2_type => 'host' },
                    { object2_uuid => $objectdata->id, object1_type => 'host' },
                ],
            },
            {
                prefetch    => ['object1_uuid','object2_uuid'],
            }
        )->all;

        for my $relationship (@relationships) {
            my $host_data   = ($relationship->object1_type eq 'host' ? $relationship->object1_uuid : $relationship->object2_uuid);
            push(@hosts, $class->read_host($serializer, $host_data, { from_instance => 1 }));
        }
    }

    return {
        type => 'instance',
        reference => $objectdata->id,
        instance => {
            id                  => $objectdata->id,
            !$options->{from_host} ? (
                hosts               => {
                    type    => 'set',
                    rows    => \@hosts,
                }
            ) : (),
            status              => $class->_read_instance_status($objectdata),
            map { $objectdata->has_object_attribute($_) ? ($_ => $class->parse_value($serializer, $objectdata->get_object_attribute($_)->value)) : ($_ => undef) }
                qw(
                    owner
                    fqdn
                    label
                    otap
                    template
                    password
                    protected
                    api_domain
                    services_domain
                    mail
                    delete_on
                    provisioned_on
                    network_acl
                    database_provisioned
                    filestore_provisioned
                    filestore
                    database
                    database_host
                    customer_type
                    fallback_url
                    disabled
                    freeform_reference
                    software_version
                    stat_diskspace
                    maintenance
                    nginx_upstream_configuration
                    extra_data
                    oidc_organization_id
                 ),
        }
    };
}

sub _read_instance_status {
    my $class       = shift;
    my $objectdata  = shift;

    my $status      = 'active';

    if (
        !$objectdata->get_object_attribute('provisioned_on') ||
        !$objectdata->get_object_attribute('provisioned_on')->value ||
        $objectdata->get_object_attribute('provisioned_on')->value > DateTime->now
    ) {
        $status     = 'processing';
    }


    if (
        $objectdata->get_object_attribute('disabled') &&
        $objectdata->get_object_attribute('disabled')->value
    ) {
        $status     = 'disabled';
    }

    if (
        $objectdata->get_object_attribute('delete_on') &&
        $objectdata->get_object_attribute('delete_on')->value &&
        $objectdata->get_object_attribute('delete_on')->value < DateTime->now
    ) {
        $status     = 'deleted';
    }

    return $status;
}

=head2 parse_value

TODO: Fix me

=cut

sub parse_value {
    my $class       = shift;
    my $serializer  = shift;
    my $value       = shift;

    ### Blessed objects
    return $serializer->read($value) if blessed($value) && !JSON::is_bool($value);

    ### Integer values
    return $value;
}

=head2 read_host

Reader for controlpanel/host object

=cut

sig read_host => 'Zaaksysteem::API::v1::Serializer, Object, ?HashRef => HashRef';

sub read_host {
    my ($class, $serializer, $objectdata, $options) = @_;
    $options //= {};

    my $instance_data;
    if (!$options->{from_instance}) {
        my @relationships = $objectdata->result_source->schema->resultset('ObjectRelationships')->search(
            {
                '-or'   => [
                    { object1_uuid => $objectdata->id, object2_type => 'instance' },
                    { object2_uuid => $objectdata->id, object1_type => 'instance' },
                ],
            },
            {
                prefetch    => ['object1_uuid','object2_uuid'],
            }
        )->all;

        for my $relationship (@relationships) {
            $instance_data = ($relationship->object1_type eq 'instance' ? $relationship->object1_uuid : $relationship->object2_uuid);
            $instance_data = $class->read_instance($serializer, $instance_data, { from_host => 1 });
        }
    }

    # Old objects in the DB don't have the attributes and since we're
    # not dealing with objects which instantiate with an empty value..
    my %attrs = $class->_get_object_attributes($serializer, $objectdata, qw(owner fqdn label ssl_key ssl_cert));
    try {
        $attrs{template} = $class->parse_value($serializer, $objectdata->get_object_attribute('template')->value);
    }
    catch {
        $attrs{template} = undef;
    };

    return {
        type      => 'host',
        reference => $objectdata->id,
        instance  => {
            id => $objectdata->id,
            !$options->{from_instance}
                ? (instance => $instance_data)
                : (),
            %attrs,
        }
    };
}


sig read_naw => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_naw {
    my ($self, $serializer, $object) = @_;

    return {
        type => 'naw',
        reference => $object->id,
        instance => {
            $self->_get_object_attributes(
                $serializer, $object,
                qw(owner naam straatnaam huisnummer huisnummer_letter huisnummer_toevoeging postcode woonplaats telefoonnummer email latitude longitude)
            ),
        }
    };
}

sub _get_object_attributes {
    my ($self, $serializer, $object, @attribute_list) = @_;
    return map { $_ => $self->parse_value($serializer, $object->get_object_attribute($_)->value) } @attribute_list;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
