package Zaaksysteem::API::v1::Serializer::Reader::Interface;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Interface - Reads C<Interface> configuration modules

=head1 DESCRIPTION

This serializer encodes various Map Objects into a valid APIv1 JSON structure.

=head1 OBJECT TYPES

The following object types will be converted via this reader:

=head1 METHODS

=head2 dispatch_map

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::Model::DB::Interface' => sub { __PACKAGE__->read_interface(@_) },
    );
}

=head2 read_interface

Return a serializeable data structure representing an interface (configuration)

=cut

sub read_interface {
    my ($class, $serializer, $object) = @_;

    my $config;
    if ($object->module_object->can('get_public_interface_config')) {
        $config = $object->module_object->get_public_interface_config($object,
            $serializer->{v1_serializer_options},
        )
    }
    else {
        $config = $object->get_interface_config;

        unless ($serializer->{v1_serializer_options}{is_admin}) {
            delete $config->{$_} for (@{ $object->module_object->{sensitive_config_fields} }, "notes")
        }
    }
    return {
        type => 'interface',
        reference => $object->uuid,
        instance => {
            (map { $_ => $object->$_ } qw/id name module/),
            casetype_id => scalar($object->get_casetype_id),
            interface_config => $config,
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
