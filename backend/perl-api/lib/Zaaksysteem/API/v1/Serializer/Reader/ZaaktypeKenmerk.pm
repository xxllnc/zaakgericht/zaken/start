package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk;
use Moose;

use BTTW::Tools;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk - A Zaaktype kenmerk reader

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ZaaktypeKenmerken' }

=head2 read

=cut

sig read => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read {
    my ($class, $serializer, $kenmerk) = @_;

    my $rv      = {
        is_group            => $class->to_bool($kenmerk->is_group),
        help                => $kenmerk->help,
        label               => $kenmerk->label,
        required            => $class->to_bool($kenmerk->value_mandatory),
        type                => undef,
        id                  => int($kenmerk->id),
        label_multiple      => $kenmerk->label_multiple,
        multiple_values     => undef,
        values              => undef,
    };

    my $properties = $kenmerk->properties;
    if ($properties->{date_limit}) {
        foreach my $key (qw(start end)) {
            # You don't neeed to have a start or an end
            next unless exists $properties->{date_limit}{$key};
            my $days = $properties->{ date_limit }{ $key }{ num };

            # 0 days is allowed
            if (!defined $days || !length($days)) {
                $serializer->log->warn(sprintf(
                    'Invalid date-constraints in casetype attribute "%s" (%d): %s',
                    $kenmerk->label,
                    $kenmerk->id,
                    (defined $days ? $days : '<undefined>')
                ));
                $days = 0;
            }

            $properties->{date_limit}{$key}{active} = $properties->{date_limit}{$key}{active} ? \1 : \0;
            $properties->{date_limit}{$key}{num}    = $days ? int($days) : undef;
            $properties->{date_limit}{$key}{reference} = $properties->{date_limit}{$key}{reference} eq 'current' ? 'now' : $properties->{date_limit}{$key}{reference};
        }
    }

    if (exists $properties->{ map_case_location }) {
        $properties->{ map_case_location } = $properties->{ map_case_location } ? \1 : \0;
    }

    $properties->{ show_on_map } = $properties->{ show_on_map } ? \1 : \0;


    my $attrid= $properties->{ map_wms_feature_attribute_id };
    if (defined $attrid && length $attrid) {
        $properties->{ map_wms_feature_attribute_id } = int($attrid);
    }

    if (my $attr = $kenmerk->bibliotheek_kenmerken_id) {
        for my $key (keys %{ $attr->properties }) {
            $properties->{$key} = $attr->properties->{$key};
        }

        $rv = {
            %$rv,
            type                => $attr->value_type,
            catalogue_id        => $attr->id,
            magic_string        => $attr->magic_string,
            limit_values        => ($attr->type_multiple ? -1 : 1),
            multiple_values     => $class->to_bool($attr->type_multiple),
            values              => $attr->options,
            permissions         => $kenmerk->format_required_permissions($serializer),
            label               => ($rv->{label} || $attr->naam),
            original_label      => $attr->naam,
            is_system           => $class->to_bool($kenmerk->is_systeemkenmerk),
            referential         => $class->to_bool($kenmerk->referential),
            pip                 => $class->to_bool($kenmerk->pip),
            publish_public      => $class->to_bool($kenmerk->publish_public),
            properties          => $properties,
            default_value       => $attr->value_default
        };
        if ($attr->value_type eq 'relationship') {
            foreach (qw(type name uuid)) {
                my $getter =  "relationship_" . $_;
                $rv->{properties}{$getter} = $attr->$getter;
            }
        }
    }
    elsif($properties->{text_content}) {
        $rv = {
            %$rv,
            type       => 'text_block',
            properties => $properties,
        };
    }
    elsif(my $object_type_id = $kenmerk->object_id) {
        my $prefix = $object_type_id->get_object_attribute('prefix');

        my $metadata = $kenmerk->object_metadata;

        for my $crud (qw[create update relate delete]) {
            # Force true/false in our serialization
            my $flag = sprintf('%s_object', $crud);
            $metadata->{ $flag } = $metadata->{ $flag } ? \1 : \0;

            # Map empty strings to undefined values
            my $label = sprintf('%s_action_label', $flag);
            $metadata->{ $label } = undef unless length $metadata->{ $label };
        }

        $rv = {
            %$rv,
            type               => 'object',
            object_id          => $kenmerk->get_column('object_id'),
            object_type_prefix => $prefix ? $prefix->value : undef,
            object_metadata    => $kenmerk->object_metadata,
        };
    }
    elsif (my $object = $kenmerk->custom_object_uuid) {
        my $metadata = $kenmerk->object_metadata;

        $rv = {
            %$rv,
            type               => 'custom_object',
            custom_object_uuid => $kenmerk->get_column('custom_object_uuid'),
            authorisation      => $object->authorization_definition,
            create_object_action_label => $metadata->{create_object_action_label},
        };
    }

    return $rv;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
