package Zaaksysteem::AppointmentProvider::QmaticV6;
use Moose;
use namespace::autoclean;

with qw(
    Zaaksysteem::AppointmentProvider::Roles::TestConnection
    Zaaksysteem::AppointmentProvider
    MooseX::Log::Log4perl
);

=head1 NAME

Zaaksysteem::AppointmentProvider::QmaticV6 - Appointment provider plugin for te Qmatic v6 API

=head1 DESCRIPTION

Adds support for using the newer v6 API of the L<Qmatic|http://www.qmatic.com/>
application for appointments.

=cut

use JSON::XS;
use URI;
use LWP::UserAgent;
use MooseX::Types::URI qw(Uri);
use BTTW::Tools;
use Zaaksysteem::Tools::SysinModules qw(:certificates);
use Zaaksysteem::Types qw(Boolean);
use Zaaksysteem::ZAPI::Form::Field;

=head1 ATTRIBUTES

=head2 qmaticv6_endpoint

The HTTPS URL the Qmatic application can be reached at.

=cut

has qmaticv6_endpoint => (
    is       => 'rw',
    isa      => Uri,
    required => 1,
    coerce   => 1,
);

=head2 qmaticv6_ca_certificate_use_system

Boolean value, indicating whether the "system" CA store should be used, or the
one configured in the C<qmaticv6_ca_certificate> field.

=cut

has qmaticv6_ca_certificate_use_system => (
    is       => 'ro',
    isa      => Boolean,
    required => 1,
);

=head2 qmaticv6_ca_certificate

CA certificate to use for verification when connecting to qmaticv6.

=cut

has qmaticv6_ca_certificate => (
    is       => 'ro',
    required => 1,
);

=head2 ua

L<LWP::UserAgent> instance used to connect to the Qmatic API. By default, a
new instance is created, with trace logging hooks for request and response.

=cut

has ua => (
    is       => 'rw',
    isa      => 'LWP::UserAgent',
    default  => sub {
        my $self = shift;
        
        my $ca_file = $self->qmaticv6_ca_certificate;
        my $ua = LWP::UserAgent->new(
            ssl_opts => {
                verify_hostname => 1,
                $ca_file
                    ? (SSL_ca_file => "$ca_file")
                    : (SSL_ca_path => '/etc/ssl/certs'),
            },
        );

        if ($self->log->is_trace) {
            $ua->add_handler("request_send",  sub { $self->log->trace(shift->dump); return; });
            $ua->add_handler("response_done", sub { $self->log->trace(shift->dump); return; });
        }

        return $ua;
    },
    lazy     => 1,
    required => 0,
);

=head2 json

The L<JSON::XS> object used to encode/decode API content.

=cut

has json => (
    is      => 'rw',
    lazy    => 1,
    default => sub { return JSON::XS->new() },
);

=head1 METHODS

=head2 shortname

Always returns the string C<qmaticv6>.

=cut

sub shortname { "qmaticv6" }

=head2 label

Always returns the string C<Qmatic v6>.

=cut

sub label { "Qmatic v6" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_qmaticv6_endpoint',
        type        => 'text',
        label       => 'Qmatic API endpoint',
        required    => 1,
        description => '<p>Qmatic Orchestra Calendar Public Appointment API URL.</p>',
        data        => { pattern => '^https:\/\/.+' },
    ),
    ca_certificate(name => 'qmaticv6_ca_certificate'),
);

=head2 configuration_items

Returns a list of configuration items (L<Zaaksysteem::ZAPI::Form::Field>
instances) that are necessary to configure the appointment interface to
use the Qmatic v6 API.

=cut

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

=head2 get_location_list

Retrieve a list of locations ("branches" in Qmatic API speak) appointments can be planned at.

=cut

sub get_location_list {
    my ($self) = @_;

    my %location_data = $self->_qmatic_http_request('GET' => 'branches');

    my $locations = $self->json->decode($location_data{response}->decoded_content);

    return [
        map {
            {
                id    => $_->{publicId},
                label => $_->{name},
            }
        } @{ $locations->{branchList} }
    ];
}

=head2 get_product_list

Retrieve a list of products ("services" in Qmatic API speak) for a given location.

Adds the duration (in minutes) to the id, because other calls in the process
don't return the appointment duration again, and we need it to generate
appointment end times.

=cut

sub get_product_list {
    my ($self, $branch_id) = @_;

    my %product_data = $self->_qmatic_http_request(
        'GET' => sprintf(
            'branches/%s/services',
            $branch_id
        ),
    );

    my $products = $self->json->decode($product_data{response}->decoded_content);

    return [
        map {
            {
                id    => join("/", $_->{publicId}, $_->{duration}),
                label => sprintf("%s (%s min)", $_->{name}, $_->{duration}),
            }
        } @{ $products->{serviceList} }
    ];
}

=head2 get_dates

Return a list of open appointment dates for a given service/branch combination.

=cut

sub get_dates {
    my ($self, $service_id_combined, $branch_id) = @_;

    my ($service_id, $duration) = split /\//, $service_id_combined;

    my %dates_data = $self->_qmatic_http_request(
        'GET' => sprintf(
            'branches/%s/services/%s/dates',
            $branch_id,
            $service_id,
        ),
    );
    my $dates = $self->json->decode($dates_data{response}->decoded_content);

    return [
        map {
            my $date = DateTime::Format::ISO8601->parse_datetime($_);
            $date->ymd;
        } @{ $dates->{dates} }
    ];
}

=head2 get_timeslots

Retrieve a list of available timeslots for a given branch/service/date/time
combination.

=cut

sub get_timeslots {
    my ($self, $date, $service_id_combined, $branch_id) = @_;

    my ($service_id, $duration) = split /\//, $service_id_combined;

    my %timeslots_data = $self->_qmatic_http_request(
        'GET' => sprintf(
            'branches/%s/services/%s/dates/%s/times',
            $branch_id,
            $service_id,
            $date->ymd,
        ),
    );
    my $timeslots = $self->json->decode($timeslots_data{response}->decoded_content);

    my @rv;
    for my $time (@{ $timeslots->{times} }) {
        my ($hour, $minute) = ($time =~ /^([0-9]{2}):([0-9]{2})$/);

        my $date_localtime = $date->set_time_zone('Europe/Amsterdam');

        my $start = $date_localtime->clone->set(hour => $hour, minute => $minute);
        my $end = $start->clone()->add(minutes => $duration);

        push @rv, {
            start_time => $start->set_time_zone('UTC')->iso8601 . 'Z',
            end_time   => $end->set_time_zone('UTC')->iso8601 . 'Z',
            plugin_data => {
                branch_id  => $branch_id,
                service_id => $service_id,
                date       => $date->ymd,
                time       => $time,
            },
        };
    }

    return \@rv;
}

=head2 book_appointment

Book a new appointment in the Qmatic system.

=cut

sub book_appointment {
    my ($self, $appointment_data, $requestor) = @_;

    for (qw(branch_id service_id date time)) {
        throw("appointment_provider/qmaticv6/invalid_data", "Invalid appointment: missing field '$_'")
            unless exists $appointment_data->{plugin_data}{$_};
    }

    my $subject = $requestor->subject;

    my %appt_data = $self->_qmatic_http_request(
        'POST' => sprintf(
            'branches/%s/services/%s/dates/%s/times/%s/book',
            $appointment_data->{plugin_data}{branch_id},
            $appointment_data->{plugin_data}{service_id},
            $appointment_data->{plugin_data}{date},
            $appointment_data->{plugin_data}{time},
        ),
        $self->json->encode(
            {
                title => "Afspraak",
                customer => {
                    firstName  => $subject->first_names,
                    lastName   => $subject->surname,
                    email      => $subject->email_address,
                    phone      => $subject->phone_number,
                    externalId => $requestor->id,
                },
                notes => "Afspraak via Zaaksysteem"
            }
        ),
    );
    my $appt = $self->json->decode($appt_data{response}->decoded_content);

    $appointment_data->{plugin_data}{appointment_id} = $appt->{publicId};

    my $appointment = Zaaksysteem::Object::Types::Appointment->new(
        %$appointment_data,
        plugin_type => $self->shortname,
    );

    return $appointment;
}

=head2 delete_appointment

Remove an existing appointment from the Qmatic v6 system.

=cut

sub delete_appointment {
    my ($self, $appointment) = @_;

    my %delete_data = $self->_qmatic_http_request(
        'DELETE' => sprintf(
            'appointments/%s',
            $appointment->plugin_data->{appointment_id},
        ),
    );

    return {
        success => \1,
    };
}

=head2 _test_host_port

Return the configured endpoint's host and port.

Required by L<Zaaksysteem::AppointmentProvider::Roles::TestConnection>.

=cut

sub _test_host_port {
    my $self = shift;
    my $uri = URI->new($self->qmaticv6_endpoint);
    return ($uri->host, $uri->port);
}

=head2 _test_certificate

Return the configured CA certificate. 

Required by L<Zaaksysteem::AppointmentProvider::Roles::TestConnection>.

=cut

sub _test_certificate {
    my $self = shift;

    return (
        ca => $self->qmaticv6_ca_certificate,
    );
}

=head2 _qmatic_http_request

A wrapper to handle calling of the Qmatic v6 API.

=cut

sub _qmatic_http_request {
    my ($self, $method, $url_part, $params) = @_;

    my $url = URI->new($self->qmaticv6_endpoint . '/' . $url_part);

    my $post_content;
    if ($method eq 'POST') {
        $post_content = $params;
    }
    else {
        $url->query_form($params);
    }

    my $request_method = HTTP::Request::Common->can($method);

    my $request = $request_method->(
        $url,
        ($method eq 'POST')
            ? (
                "Accept"       => "application/json",
                "Content-Type" => "application/json",
                "Content" => $post_content,
              )
            : ()
    );
    my $response = $self->ua->request($request);

    if ($response->is_success) {
        return (
            request  => $request,
            response => $response,
        );
    }

    throw(
        "appointmentprovider/qmaticv6/http_error",
        "Qmatic v6 - HTTP error: " . $response->code,
        {
            request  => $request,
            response => $response,
        }
    );
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
