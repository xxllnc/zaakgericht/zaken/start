package Zaaksysteem::Auth::Credential::API;

use Moose;

use Digest::MD5    ();

use BTTW::Tools;
use Zaaksysteem::Constants;

extends 'Zaaksysteem::Auth::Credential::LDAP';

=head1 NAME

Zaaksysteem::Auth::Credential::API - HTTP-Digest and Header credential module, for external API communication

=head1 SYNOPSIS

    ### Within a different module
    my $user     = $c->authenticate({ interface_id => API_INTERFACE_ID }, 'api');

=head1 DESCRIPTION

Will login this person when the supplied credentials through HTTP::Digest match the information
in the API_INTERFACE or when the supplied API-Key header is valid.

=head2 authenticate

Arguments: $c, $realm, $authinfo

Return value: $c->user

    my $user     = $c->authenticate({ interface_id => API_INTERFACE_ID }, 'api');

Internal method, called from catalyst when calling authenticatie on the context C< $c > object.

Will throw an error when no api_key and/or username are set in the interface configuration.

=cut

sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $iface = $authinfo->{interface};

    unless ($iface) {
         $c->log->error('auth/credential/api/no_interface: ' .
             'No interface supplied'
         );
         return;
    }

    my $username = $iface->get_interface_config->{medewerker}{username};
    my $api_key  = $iface->get_interface_config->{api_key};

    if (!$username) {
         $c->log->error('auth/credential/api/no_username: ' .
             'No username set in module settings'
         );
         return;
    }

    my $auth_module    = $realm->{config}{interface};
    my $auth_interface = $c->model('DB::Interface')->find_by_module_name($auth_module);

    my $user = $realm->find_user($c, {
        username => $username,
        source   => $auth_interface->id
    });

    if (!$user) {
        $c->log->error(
            'auth/credential/api/no_user_object: ' .
            'No user object found by find_user for username: ' . $username
        );
        return;
    }
    elsif (!$user->is_active) {
        $c->log->error(
            'auth/credential/api/inactive_user: ' .
            'User setup in api is inactive: ' . $username
        );
        return;
    }

    my $authenticated;
    my $header_key = $c->req->header('API-Key');

    if ($header_key) {
        if ($header_key eq $api_key) {
            $authenticated = 1;
        }
    } else {
        $authenticated = $self->_authenticate_digest(
            $c, $realm,
            {
                user        => $username,
                password    => $api_key,
            }
        );
    }

    if ($authenticated) {
        # HTTP method is validated by Controller::Page before we get here
        my $method = lc($c->req->method);
        $c->assert_not_rate_limited("legacy:apiv1:$method", "interface-" . $auth_interface->id);

        $user->external_api_objects(['product']);
        $user->is_external_api(1);

        return $user;
    }

    $c->stash->{ api_unauthorized_response } = 1;

    if (!$header_key) {
        $self->authorization_required_response($c, $realm, $authinfo);
    } else {
        $c->res->status(401);
        $c->res->content_type('text/plain');
        $c->res->body('Authorization required.');
    }

    # Allow end actions to recognize us, we're special :D
    $c->detach;
}

=head1 INTERNAL METHODS

=head2 _authenticate_digest

Arguments: $c, $realm, $auth_info

Return value: $user OR C<undef>

    $user = $self->_authenticate_digest(
        $c, $realm,
        {
            user        => 'admin',
            password    => 'ijkdsi89w42983472304'
        }
    );

Checks the authentication of the api requestor

=cut

sub _authenticate_digest {
    my ( $self, $c, $realm, $auth_info ) = @_;

    $c->log->debug('Checking http digest authentication.') if $c->debug;

    my $headers       = $c->req->headers;
    my @authorization = $headers->header('Authorization');
    foreach my $authorization (@authorization) {
        next unless $authorization =~ m{^Digest};
        my %res = map {
            my @key_val = split /=/, $_, 2;
            $key_val[0] = lc $key_val[0];
            $key_val[1] =~ s{"}{}g;    # remove the quotes
            @key_val;
        } split /,\s?/, substr( $authorization, 7 );    #7 == length "Digest "

        my $opaque = $res{opaque};
        my $nonce  = $self->get_digest_authorization_nonce( $c, __PACKAGE__ . '::opaque:' . $opaque );
        next unless $nonce;

        my $uri         = $c->request->uri->path_query;
        my $algorithm   = $res{algorithm} || 'MD5';
        my $nonce_count = '0x' . $res{nc};

        my $check = ($uri eq $res{uri})
          && ( exists $res{username} )
          && ( exists $res{qop} )
          && ( exists $res{cnonce} )
          && ( exists $res{nc} )
          && $algorithm eq $nonce->algorithm
          && hex($nonce_count) > hex( $nonce->nonce_count )
          && $res{nonce} eq $nonce->nonce;    # TODO: set Stale instead

        unless ($check) {
            $c->log->warn('Digest authentication failed. Bad request.');
            $c->res->status(400);             # bad request
            Carp::confess $Catalyst::DETACH;
        }

        my $username = $res{username};
        my $user_obj = $auth_info->{user};

        unless ($user_obj) {    # no user, no authentication
            $c->log->debug("Unable to locate user matching user info provided") if $c->debug;
            return;
        }

        if ($username ne $user_obj) {
            $c->log->debug("Invalid user $username supplied, $user_obj expected") if $c->debug;
            return;
        }

        # everything looks good, let's check the response
        # calculate H(A2) as per spec
        my $ctx = Digest::MD5->new;
        $ctx->add( join( ':', $c->request->method, $res{uri} ) );
        if ( $res{qop} eq 'auth-int' ) {
            my $digest =
              Digest::MD5::md5_hex( $c->request->body );    # not sure here
            $ctx->add( ':', $digest );
        }
        my $A2_digest = $ctx->hexdigest;

        # the idea of the for loop:
        # if we do not want to store the plain password in our user store,
        # we can store md5_hex("$username:$realm:$password") instead
        for my $r ( 0 .. 1 ) {
            # calculate H(A1) as per spec
            my $A1_digest = $r ? $auth_info->{password} : do {
                $ctx = Digest::MD5->new;
                $ctx->add( join( ':', $username, $realm->name, $auth_info->{password} ) );
                $ctx->hexdigest;
            };
            if ( $nonce->algorithm eq 'MD5-sess' ) {
                $ctx = Digest::MD5->new;
                $ctx->add( join( ':', $A1_digest, $res{nonce}, $res{cnonce} ) );
                $A1_digest = $ctx->hexdigest;
            }

            my $digest_in = join( ':',
                    $A1_digest, $res{nonce},
                    $res{qop} ? ( $res{nc}, $res{cnonce}, $res{qop} ) : (),
                    $A2_digest );
            my $rq_digest = Digest::MD5::md5_hex($digest_in);
            $nonce->nonce_count($nonce_count);
            my $key = __PACKAGE__ . '::opaque:' . $nonce->opaque;
            $self->store_digest_authorization_nonce( $c, $key, $nonce );
            if ($rq_digest eq $res{response}) {
                return $user_obj;
            }
        }
    }

    return;
}

sub authorization_required_response {
    my ( $self, $c, $realm, $auth_info ) = @_;

    $c->res->status(401);
    $c->res->content_type('text/plain');
    $c->res->body('Authorization required.');

    # *DONT* short circuit

    unless ($self->_create_digest_auth_response($c, $auth_info)) {
        die 'Could not build authorization required response. '
        . 'Did you configure a valid authentication http type: '
        . 'basic, digest, any';
    }

    return;
}
sub _add_authentication_header {
    my ( $c, $header ) = @_;
    $c->response->headers->push_header( 'WWW-Authenticate' => $header );
    return;
}

sub _create_digest_auth_response {
    my ( $self, $c, $opts ) = @_;

    $c->log->debug('Creating digest auth response');
    if ( my $digest = $self->_build_digest_auth_header( $c, $opts ) ) {
        _add_authentication_header( $c, $digest );
        return 1;
    }

    return;
}


sub _build_auth_header_realm {
    my ( $self, $c, $opts ) = @_;
    if ( my $realm_name = qprintable($opts->{realm} ? $opts->{realm} : $self->realm->name) ) {
        $realm_name = qq{"$realm_name"} unless $realm_name =~ /^"/;
        return 'realm=' . $realm_name;
    }
    return;
}

sub _build_auth_header_domain {
    my ( $self, $c, $opts ) = @_;
    if ( my $domain = $opts->{domain} ) {
        Catalyst::Exception->throw("domain must be an array reference")
          unless ref($domain) && ref($domain) eq "ARRAY";

        my @uris =
          $self->use_uri_for
          ? ( map { $c->uri_for($_) } @$domain )
          : ( map { URI::Escape::uri_escape($_) } @$domain );

        return qq{domain="@uris"};
    }
    return;
}

sub _build_auth_header_common {
    my ( $self, $c, $opts ) = @_;
    return (
        $self->_build_auth_header_realm($c, $opts),
        $self->_build_auth_header_domain($c, $opts),
    );
}

sub _build_basic_auth_header {
    my ( $self, $c, $opts ) = @_;
    return _join_auth_header_parts( Basic => $self->_build_auth_header_common( $c, $opts ) );
}

sub _build_digest_auth_header {
    my ( $self, $c, $opts ) = @_;

    my $nonce = $self->_digest_auth_nonce($c, $opts);

    my $key = __PACKAGE__ . '::opaque:' . $nonce->opaque;

    $self->store_digest_authorization_nonce( $c, $key, $nonce );

    return _join_auth_header_parts( Digest =>
        $self->_build_auth_header_common($c, $opts),
        map { sprintf '%s="%s"', $_, $nonce->$_ } qw(
            qop
            nonce
            opaque
            algorithm
        ),
    );
}

sub _digest_auth_nonce {
    my ( $self, $c, $opts ) = @_;

    my $package = __PACKAGE__ . '::Nonce';

    my $nonce   = $package->new;

    if ( my $algorithm = $opts->{algorithm}) {
        $nonce->algorithm( $algorithm );
    }

    return $nonce;
}

sub _join_auth_header_parts {
    my ( $type, @parts ) = @_;
    return "$type " . join(", ", @parts );
}

sub _check_cache {
    my $c = shift;

    die "A cache is needed for http digest authentication."
      unless $c->can('cache');
    return;
}

sub get_digest_authorization_nonce {
    my ( $self, $c, $key ) = @_;

    _check_cache($c);
    return $c->cache->get( $key );
}

sub store_digest_authorization_nonce {
    my ( $self, $c, $key, $nonce ) = @_;

    _check_cache($c);
    return $c->cache->set( $key, $nonce );
}

### Because String::Escape dies in combination with moose

my %Printable = (
        ( map { chr($_), unpack('H2', chr($_)) } (0..255) ),
        ( "\\"=>'\\', "\r"=>'r', "\n"=>'n', "\t"=>'t', ),
        ( map { $_ => $_ } ( '"' ) )
);

sub printable ($) {
        local $_ = ( defined $_[0] ? $_[0] : '' );
        s/([\r\n\t\"\\\x00-\x1f\x7F-\xFF])/ '\\' . $Printable{$1} /gsxe;
        return $_;
}

sub quote_non_words ($) {
        ( ! length $_[0] or $_[0] =~ /[^\w\_\-\/\.\:\#]/ ) ? '"'.$_[0].'"' : $_[0]
}

sub qprintable ($) { quote_non_words printable $_[0] }

package Zaaksysteem::Auth::Credential::API::Nonce;

use strict;
use base qw[ Class::Accessor::Fast ];
use Data::UUID ();

our $VERSION = '0.02';

__PACKAGE__->mk_accessors(qw[ nonce nonce_count qop opaque algorithm ]);

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->nonce( Data::UUID->new->create_b64 );
    $self->opaque( Data::UUID->new->create_b64 );
    $self->qop('auth,auth-int');
    $self->nonce_count('0x0');
    $self->algorithm('MD5');

    return $self;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 AUTHORS

Most parts of this file are copied from L<Catalyst::Authentication::Credential::HTTP>. Big thanks
to all the authors.

=head1 COPYRIGHT and LICENSE

Most parts of this file are copied from L<Catalyst::Authentication::Credential::HTTP>. These parts
are distributed under the same license as the module they are copied from.

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 authorization_required_response

TODO: Fix the POD

=cut

=head2 get_digest_authorization_nonce

TODO: Fix the POD

=cut

=head2 printable

TODO: Fix the POD

=cut

=head2 qprintable

TODO: Fix the POD

=cut

=head2 quote_non_words

TODO: Fix the POD

=cut

=head2 store_digest_authorization_nonce

TODO: Fix the POD

=cut

