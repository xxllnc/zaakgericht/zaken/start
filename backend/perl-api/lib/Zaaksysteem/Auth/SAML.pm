package Zaaksysteem::Auth::SAML;
use Zaaksysteem::Moose;

use Crypt::OpenSSL::Random qw(random_pseudo_bytes);

with qw(Zaaksysteem::Roles::Session);

=head1 NAME

Zaaksysteem::Auth::SAML - A SAML model for Zaaksysteem

=cut

# 15 minute expire time
has '+expire' => (default => 300);

sub type {
    return 'relaystate';
}

sub _generate_token {
    my $self = shift;
    return unpack 'H*', random_pseudo_bytes(32);
}

around 'start_session' => sub {
    my ($orig, $self, $session_id) = @_;

    my $token = $self->_generate_token;
    $self->$orig($token, $session_id);
    return $token;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
