package Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet;
use Zaaksysteem::Moose;

use Zaaksysteem::Profiles qw(
    PROFILE_NATUURLIJK_PERSOON
    AUTHENTICATED_PARAM
    PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS
);

extends
    qw(Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::GenericResultSet);


has schema => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;
        return $self->result_source->schema;
    },
    handles => [qw(country_code)],
);

with qw(
    Zaaksysteem::BR::Subject::ResultSet::Person
    Zaaksysteem::Roles::GovernmentID
);


=head1 NAME

Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet - Natuurlijk Persoon ResultSet

=head1 SYNOPSIS

    ### Within a different module
    my $instance    = $np->create_natuurlijk_persoon(
        {

        }
    )

=head1 DESCRIPTION

This object handles the searching and creation of NatuurlijkPersonen in zaaksysteem (Gegevensmagazijn Side).
This means that this is the place to insert a natuurlijk persoon.

=head1 METHODS

=head2 create_natuurlijk_persoon

Arguments: \%PARAMS [, \%OPTIONS]

Return value: $ROW_NATUURLIJK_PERSOON

TODO:
authenticated
authenticated_by

voorletters: from 10 to 20

=cut

define_profile 'create_natuurlijk_persoon' => (
    %{ PROFILE_NATUURLIJK_PERSOON() }
);

sub create_natuurlijk_persoon {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;
    my $options         = shift || {};

    ### Remove the damn voorloopnul
    if ($params->{burgerservicenummer}) {
        $params->{burgerservicenummer} = int($params->{burgerservicenummer});
    }

    my %db_params;

    if ($options->{authenticated}) {
        throw(
            'betrokkene/natuurlijkpersoon/create_natuurlijk_persoon/invalid_authenticed_param',
            'Authenticated must match regex'
        ) unless $options->{authenticated} =~ AUTHENTICATED_PARAM;

        $db_params{authenticatedby} = $options->{authenticated};
        $db_params{authenticated}   = 1;
    }
    elsif ($params->{burgerservicenummer}) {
        $self->_assert_non_existent_bsn($params->{burgerservicenummer}, { authenticated => [ 0, 1 ] });
    }

    my %address_params;
    for my $param (keys %{ $params }) {
        if (grep { $param eq $_ } @{ PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS() }) {
            $address_params{$param} = $params->{$param};
        } else {
            $db_params{$param}      = $params->{$param};
        }
    }

    # TODO: Revoke this logic in the create.
    #       Creating isn't updating.
    #
    # If we already have a natuurlijk persoon, try to update that
    # person.
    my $gov_id;
    if ($self->country_code eq '5107') {
        $gov_id = $params->{persoonssnummer};

    }
    else {
        $gov_id = $params->{burgerservicenummer};
    }
    my $np = $gov_id ? $self->find_by_gov_id($gov_id) : undef;

    if ($np) {
        my $address = $np->adres_id;

        foreach (@{$address->get_columns}) {
            $address_params{$_} //= '';
        }
        $address->update(\%address_params);

        foreach (@{$self->get_columns}) {
            $db_params{$_} //= '';
        }
        $np->update(\%db_params);
    }
    else {
        $self->result_source->schema->txn_do(sub {
            my $address = $self->schema->resultset('Adres')
                ->create(\%address_params);

            $np = $self->create({ %db_params, adres_id => $address, });

            $address->natuurlijk_persoon_id($np->id);
            $address->update;
        });
    }
    return $np;

}

sub _assert_non_existent_bsn {
    my ($self, $bsn, $options) = @_;

    my $np = $self->find_by_gov_id($bsn, $options);
    if ($np) {
        throw("natuurlijk_persoon/exists",
            "Natuurlijk Persoon with BSN $bsn found in Zaaksysteem",
        );
    }
    return 1;
}

=head2 get_by_gov_id

Get an active natuurlijk persoon based on the BSN.

First checks for an authenticated contact, if that's not found, the search is
repeated for an unauthenticated contact.

Dies if nothing can be found.

=cut

sub get_by_gov_id {
    my ($self, $bsn, $resurrect) = @_;

    my $np = $self->find_by_gov_id($bsn);
    if (!$np) {
        $np = $self->find_by_gov_id(
            $bsn,
            { authenticated => 0 }
        );
    }

    return $np if $np;

    if ($resurrect) {
        $np = $self->find_by_gov_id(
            $bsn,
            { deleted_on => { '!=' => undef } },
            # Only get the latest deleted person and get only one people
            # person, otherwise the find will fail
            { order_by => { -desc => 'deleted_on' }, rows => 1 }
        );
        if ($np) {
            $np->enable_natuurlijk_persoon;
            return $np->discard_changes;
        }
    }

    throw("natuurlijk_persoon/BSN/not_found", "Unable to find Natuurlijk Persoon with BSN: $bsn");
}

=head2 find_by_gov_id

Tries to find an active natuurlijk persoon based on the BSN. By default, only
authenticated contacts are returned.

Contrary to L<get_by_gov_id> this function returns undef if nothing can be found.

=cut

sig find_by_gov_id => 'Int,?HashRef,?HashRef';

sub find_by_gov_id {
    my ($self, $bsn, $options, $params) = @_;

    my $rs = $self->search_rs(
        {
            authenticated => 1,
            deleted_on    => undef,
            %{ $options // {} },
        },
        $params
    );

    $bsn = $self->format_government_id($bsn);
    if ($self->schema->country_code eq '5107') {
        $rs = $rs->search_rs({ 'NULLIF(me.persoonsnummer,\'\')::integer' => $bsn, });
    }
    else {
        $rs = $rs->search_rs({ 'NULLIF(me.burgerservicenummer,\'\')::integer' => int($bsn), });
    }

    my @entries = $rs->all;
    my $c       = @entries;

    if ($c > 1) {
        throw(
            "natuurlijk_persoon/duplicate/bsn",
            sprintf("Multiple entries (%d) found for government ID %s", $c, $bsn)
        );
    }

    return $entries[0];
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
