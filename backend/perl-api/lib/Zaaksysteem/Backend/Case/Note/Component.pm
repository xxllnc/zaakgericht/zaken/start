package Zaaksysteem::Backend::Case::Note::Component;
use Zaaksysteem::Moose;

BEGIN { extends 'DBIx::Class::Row'; }


sub as_object_ref {
    my $self = shift;

    return {
        instance => {
            date_created  => $self->date_created,
            date_modified => $self->date_modified,
            content       => $self->value,
            owner_id      => $self->subject_id->uuid,
        },
        type      => 'case/note',
        reference => $self->uuid,
        preview   => substr($self->value, 0, 100),
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
