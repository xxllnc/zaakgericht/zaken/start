package Zaaksysteem::Backend::FileMetadata::Component;

use Moose;

=head1 NAME

Zaaksysteem::Backend::FileMetadata::Component

=cut

use BTTW::Tools;
use Zaaksysteem::Object::Types::Document::Metadata;

extends 'DBIx::Class';

my @optional = qw(
    description
    origin
    origin_date
    pronom_format
    appearance
    structure
    creation_date
    document_source
);

=head2 as_object

Return the component as a
L<Zaaksysteem::Object::Types::Document::Metadata> object

=cut

sub as_object {
    my $self = shift;

    my %args = (
        trust_level => $self->trust_level,
    );

    foreach (@optional) {
        $args{$_} = $self->$_ if defined $self->$_;
    }

    if (defined $self->document_category) {
        $args{category} = $self->document_category;
    }
    if (defined $self->document_source) {
        $args{document_source} = $self->document_source;
    }

    my $file = $self->files->first;
    if ($file) {
        $args{date_created} = $file->date_created;
        $args{date_modified} = $file->date_modified;
    }

    return Zaaksysteem::Object::Types::Document::Metadata->new(%args);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
