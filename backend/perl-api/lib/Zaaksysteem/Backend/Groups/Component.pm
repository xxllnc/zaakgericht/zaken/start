package Zaaksysteem::Backend::Groups::Component;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Types qw(NonEmptyStr SequenceNumber);
use Zaaksysteem::Object::Types::Group;

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::Backend::Subject::Component - Implement specific behaviors for
Subject rows.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 parent_id

The parent_id of this group

=cut

has parent_id => (
    is      => 'ro',
    lazy    => 1,
    default => sub { 
        my $self    = shift;

        ### Clone path, because of pop below
        my $path    = [ @{ $self->path } ];

        ### Root, no parent
        return unless ref $path && scalar(@$path) > 1;

        ### Get second last id from path
        my $id          = pop @$path;
        my $parent_id   = pop @$path;

        return $parent_id;
    }
);

=head2 object

Holds a reference to a L<Zaaksysteem::Object::Types::Group> instance, which
encodes this row in the L<Zaaksysteem::Object> style.

=cut

has object => (
    is => 'ro',
    isa => 'Zaaksysteem::Object::Types::Group',
    lazy => 1,
    builder => 'as_object'
);

=head2 subjects

Arguments: none

    my $subjects_rs     = $groups->subjects;

Will retrieve subjects who belong to this group

=cut

has subjects => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        $self->result_source->schema->resultset('Subject')->search(
            {
                $self->id => \'= ANY (group_ids)',
            },
        );
    }
);

=head1 METHODS

=head2 as_object

Return the group component as an object

=cut

sub as_object {
    my $self = shift;

    return Zaaksysteem::Object::Types::Group->new(
        group_id    => $self->id,
        name        => $self->name,
        description => $self->description // '',
        id          => $self->uuid,
    );
}

=head2 update_group

Argumants: \%params

Return value: $UPDATED_ROW

    $newrow = $row->update_group(
        {
            name            => 'Backstoffice',
            description     => 'Backstoffice',
        }
    );

=cut

define_profile 'update_group' => (
    optional            => {
        name                => NonEmptyStr,
        description         => NonEmptyStr,
        parent_group_id     => SequenceNumber,
    },
);

sub update_group {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;

    my %old = ();
    my %new = ();
    for my $key (qw/name description/) {
        $old{$key} = $self->$key;
        next if $old{key} eq $params->{$key};
        $new{$key} = $params->{$key};
    }

    return $self unless %new;
    $self->update(\%new);
    $self->result_source->schema->resultset('Logging')->trigger(
        'ou/update', {
            component => 'user',
            data => { old => \%old, new => \%new }
        }
    );
    return $self->discard_changes;
}

=head2 security_identity

Implements a duck-typed L<Zaaksysteem::Object::SecurityIdentity> for the group
instance. Returns a hashable list C<group, $id>.

=cut

sub security_identity {
    return (group => shift->id);
}

=head2 set_deleted

Arguments: none

    $role->set_deleted;

    ### Not needed:
    # $group->update

Only way to proper delete a group

=cut

sub set_deleted {
    my $self        = shift;

    ### Loop over subjects to remove role_ids
    $self->result_source->schema->txn_do(
        sub {
            my $rs = $self->subjects;
            while (my $subject = $rs->next) {
                next unless grep({ $_ == $self->id } @{ $subject->group_ids });

                my @group_ids = grep({ $_ != $self->id } @{ $subject->group_ids });
                $subject->group_ids(\@group_ids);
                $subject->update;
            }

            $self->delete;
        }
    );
}

=head2 TO_JSON

Implements the automagic serialization to JSON via L<JSON/encode>.

=cut

sub TO_JSON {
    my $self    = shift;

    return {
        $self->get_columns,
        parent_id => $self->parent_id,
        table     => 'Groups',
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
