package Zaaksysteem::Backend::Message::ResultSet;

use Moose;

use BTTW::Tools;

extends 'Zaaksysteem::Backend::ResultSet';

=head2 message_create

Add a message entry to the database.

=head3 Arguments

=over

=item case_id [required]

=item event_type [required]

=item message [required]

=item subject_id [optional]

=back

=head3 Returns

A newly created Sysin::Message object.

=cut


Params::Profile->register_profile(
    method  => 'message_create',
    profile => {
        required => [qw/
            event_type
            message
        /],
        optional => [qw/
            subject_id
            case_id
            data
            log
        /]
    }
);

sub message_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    my $log = $opts->{log}
        || $self->result_source->schema->resultset('Logging')->trigger(
        $opts->{event_type},
        {
            component => 'zaak',
            $opts->{case_id} ? (zaak_id => $opts->{case_id}) : (),
            data => {
                $opts->{case_id} ? (case_id => $opts->{case_id}) : (),
                content => $opts->{message},
                %{ $opts->{data} || {} }
            }
        }
        );

    return $self->create({
        message => $opts->{message},
        subject_id => $opts->{subject_id},
        logging_id => $log->id
    });
}

=head2 with_related_json

=over 4

=item Arguments: none

=item Return Value: L<DBIx::Class::ResultSet>

=back

    my $rs = $self->search()->with_related_json();

This call will make sure the actual DB Query will contain certain joins and other tricks to include
all the necessary data in one call.

=cut

sub with_related_json {
    my $self            = shift;

    my @related_querys  = (
        [ 'aanvrager_name' => \"CASE WHEN logging.zaak_id IS NOT NULL THEN (select naam from zaak_betrokkenen where zaak_betrokkenen.id = maybezaak.aanvrager) ELSE '' END" ],
        [ 'case_type_title' => \"CASE WHEN logging.zaak_id IS NOT NULL THEN (select titel from zaaktype_node where maybezaak.zaaktype_node_id = zaaktype_node.id) ELSE '' END" ],
    );

    return $self->search(
        undef,
        {
            prefetch    => [
                {logging => 'maybezaak' },
            ],
            '+select'   => [
                map { $_->[1] } @related_querys,
            ],
            '+as'       => [
                map { $_->[0] } @related_querys,
            ]
        }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

