package Zaaksysteem::Backend::Rules::Rule::Action::ChangeHtmlMailTemplate;
use Zaaksysteem::Moose;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action::ChangeHtmlMailTemplate - Change the HTML template use of a case

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action will change the confidentiality of a case

=head1 ATTRIBUTES


=head2 attribute_name

The attribute name for this action is not required. See also
L<Zaaksysteem::Backend::Rules::Rule::Action>

=cut

has '+attribute_name' => ( isa => 'Any', required => 0 );

has value => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has '+_data_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { return [qw/value/]; }
);

=head2 integrity_verified

For now return always return true

=cut

sub _populate_validation_results {
    my ($self, $result_object, $params) = @_;

    $result_object->changes->{'case.html_email_template'} = $self->value;
    return $result_object;
}

sub integrity_verified { return 1; }

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
