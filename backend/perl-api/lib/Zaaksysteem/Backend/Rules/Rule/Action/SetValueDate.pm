package Zaaksysteem::Backend::Rules::Rule::Action::SetValueDate;

use Zaaksysteem::Moose;
use Zaaksysteem::Types qw(ValidExtendedFormulaAttribute);
use Zaaksysteem::Backend::Tools::WorkingDays qw(add_working_days subtract_working_days);

with qw(
    Zaaksysteem::Backend::Rules::Rule::Action
    Zaaksysteem::Backend::Rules::Rule::AttributesValidation
);

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action::SetValueDate - Set value on date
attributes

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action set a dynamically calculated value on an attribute

=head1 ATTRIBUTES

=head2 attribute_name

Holds a symbolic reference (magic string) to the target attribute for which
this action should set a value.

=cut

has attribute_src => (
    is => 'rw',
    isa => ValidExtendedFormulaAttribute,
    required => 1
);

has math => (
    is => 'rw',
    isa=> 'HashRef',
);

=head2 _data_attributes

Array of attributes to show in data.

=cut

has _data_attributes => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub {
        return [qw(source_attribute target_attribute math)]
    }
);

=head1 METHODS

=head2 source_attribute

Mapper for _data_attributes API, this exposes the internal attribute name under
a different name in the TO_JSON of the rule

=cut

sub source_attribute {
    return shift->attribute_src;
}

=head2 source_attribute

Mapper for _data_attributes API, this exposes the internal attribute name under
a different name in the TO_JSON of the rule

=cut

sub target_attribute {
    return shift->attribute_name;
}

=head2 integrity_verified

Hook for the rule verification checks. See
L<Zaaksysteem::Backend::Rules::Rule::Action> for the interface description.

=cut

sub integrity_verified { my $self = shift;  return $self->math ? 1 : 0 };

=head3 Exceptions

=over 4

=item rules/rule/action/set_value/invalid_values

Thrown when validation is unable to find a value to validate.

=back

=cut

=head2 _populate_validation_results

Populates a validation result object with the local value.

=cut

sig _populate_validation_results => 'Object, HashRef';

sub _populate_validation_results {
    my ($self, $result_object, $params) = @_;

    my $date = $params->{$self->attribute_src}[0];
    return unless $date;

    my $rv = assert_date($date);
    $rv->set_time_zone('floating');

    foreach (qw(years months weeks days)) {
        my $amount = $self->math->{$_ . "_amount"};
        next unless $amount;
        my $action = $self->math->{$_ . "_action"};
        $rv->$action($_ => $amount, end_of_month => 'limit');
    }

    if (my $days = $self->math->{'working_days_amount'}) {
        my $action = $self->math->{'working_days_action'};

        my %args = (datetime => $rv, working_days => $days);
        $rv
            = $action eq 'add'
            ? add_working_days(\%args)
            : subtract_working_days(\%args);

    }

    $rv = $rv->strftime('%d-%m-%Y');

    $params->{ $self->attribute_name } = [ $rv ];
    $result_object->changes->{ $self->attribute_name } = $rv;
    $result_object->changeable_attributes->{$self->attribute_name} = 0;

    return $result_object;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules>, L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
