package Zaaksysteem::Backend::Rules::Serializer;
use Moose;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Backend::Rules::Serializer - A tool to encode/decode the rules.

=head1 DESCRIPTION

The old rules are stored in a non-readable serialized manner which is hard to debug and change.
This module tries to make the transition to actually store the data in the DB as JSON like we do with other bits.

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Rules::Serializer;
    my $s = Zaaksysteem::Backend::Rules::Serializer->new();

    my $hash_ref = $s->decode($data_from_db);
    my $json     = $s->encode($hash_ref);

    my $rule     = $s->decode_to_rule($data_from_db);

=cut

use JSON::XS;
use List::Util qw(any);
use BTTW::Tools;

=head1 ATTRIBUTES

=head2 json

A L<JSON::XS> object

=cut

has json => (
    is      => 'ro',
    isa     => 'JSON::XS',
    default => sub {
        return JSON::XS->new->convert_blessed(1);
    },
    lazy => 1,
);

=head2 schema

A L<Zaaksysteem::Schema> object, required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head1 METHODS

=head2 decode

Decode the rule to hash object

=cut

sub decode {
    my ($self, $data) = @_;

    return unless defined $data;

    my $rule = $self->_decode($data);

    if ($self->log->is_trace) {
        $self->log->trace(dump_terse($rule));
    }
    return $rule;
}

=head2 sanitize_rule

Apply sanitization on rules and its attributes.


=cut

sig sanitize_rule => 'HashRef';

sub sanitize_rule {
    my ($self, $rule) = @_;

    foreach (keys %$rule) {
        next unless ($_ =~ /^((?:ander|actie)_\d+)$/);

        my $actie = $1;

        if ($rule->{$actie} eq 'toewijzing') {
            foreach (qw(role_id ou_id)) {
                if (ref $rule->{$_}) {
                    # One cannot have multiple roles/groups
                    $rule->{$_} = $rule->{$_}[0];
                }
            }
            next;
        }

        my $attribute_type = 'none';

        my $id = $rule->{ $actie . "_kenmerk" } // '';

        if ($id =~ /^\d+$/) {
            try {
                $attribute_type = $self->_find_attribute($id)->value_type;
                # We have several valuta types, for rules we just need
                # the general type
                $attribute_type =~ s/valuta.*/valuta/g;
            } catch {
                $attribute_type = 'unknown';
            };
        }
        elsif ($id eq 'price') {
            $attribute_type = 'valuta';
        }
        elsif ($id eq 'case_result') {
            $attribute_type = 'result';
        }

        $rule->{ $actie . "_attribute_type" } = $attribute_type;

        if (any { $rule->{$actie} eq $_ } qw(vul_waarde_in set_value_magic_string)) {
            my $key   = $actie . "_value";
            my $value = $rule->{$key};

            if ($attribute_type eq 'valuta') {
                $rule->{$key} = _fix_value($value // '');
            }
            elsif ($attribute_type eq 'numeric') {
                $rule->{$key} = int($value || 0);
            }
        }
    }

    return $rule;
}

sub _find_attribute {
    my $self = shift;
    my $id   = shift;

    my $attr = $self->schema->resultset('BibliotheekKenmerken')->find($id);
    return $attr if $attr;

    throw('rules/_find_attribute', "Unable to find attribute with id $id");
}

sub _fix_value {
    my $value = shift;
    if ($value =~ /^\d+,\d+$/) {
        $value =~ s/,/./;
    }
    return $value;
}

sub _decode {
    my ($self, $data) = @_;

    my $e = try {
        return $self->json->utf8(0)->decode($data);
    }
    catch {
        $self->log->warn($_);
    };
    return $e;
}

=head2 decode_to_rule

Decode the data to rule

=cut

sub decode_to_rule {
    my $self    = shift;
    my $decoded = $self->decode(@_);
    if (!defined $decoded) {
        return $decoded;
    }
    return { map { $_ => $decoded->{$_} } keys %$decoded };
}

=head2 encode

Encode the data to a JSON string

=cut

sub encode {
    my ($self, $data) = @_;
    $data //= {};
    return $self->json->utf8(0)->encode($data);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
