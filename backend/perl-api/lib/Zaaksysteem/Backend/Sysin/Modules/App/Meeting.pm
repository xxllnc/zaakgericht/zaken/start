package Zaaksysteem::Backend::Sysin::Modules::App::Meeting;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::APP::Meeting - Interface to configure a Meeting App in zaaksysteem

=head1 DESCRIPTION

This module allows the configuration of a Meeting App on zaaksysteem.nl

=cut

use BTTW::Tools;

use constant INTERFACE_ID => 'app_meeting';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API Sleutel',
        required    => 1,
        description => 'Geef de API key op van deze koppeling.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_access',
        type => 'select',
        label => 'Toegangsniveau',
        required => 1,
        description => 'Het toegangsniveau bepaalt of de API gebruiker alleen gegevens mag raadplegen, of mogelijk ook mag wijzigen. Deze instelling werkt globaal over de API, dus zelfs als de ingestelde medewerker schrijfrechten heeft op een bepaald object, maar deze instelling op "Raadplegen" staat, zal de API gebruiker niet kunnen schrijven naar het Zaaksysteem.',
        data => {
            options => [
                { value => 'ro', label => 'Raadplegen' },
                { value => 'rw', label => 'Behandelen' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_short_name',
        type        => 'text',
        label       => 'Korte naam (enkel kleine letters en cijfers, geen spaties of slashes)',
        description => 'Voer een verkorte titel van de app in, zoals "management" of "directie"',
        required    => 1
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_title_normal',
        type        => 'text',
        label       => 'Header titel (desktop / tablet)',
        description => 'Voer een titel in voor de app die wordt getoond op tablets en desktops.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_title_small',
        type        => 'text',
        label       => 'Header titel (mobiel)',
        description => 'Voer een titel in voor de app die wordt getoond op mobiele apparaten.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_header_bgimage',
        type        => 'text',
        label       => 'Header plaatje',
        description => 'Voer een achtergrondplaatje in. Dit wordt getoond in de header van de app. Let op: het plaatje moet via https geserveerd worden.',
        data        => { pattern => '^https:\/\/.+' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_header_bgcolor',
        type        => 'text',
        label       => 'Header kleur',
        description => 'Voer een hexadecimale kleur in die wordt gebruikt als achtergrondkleur voor de header van de app. Bijvoorbeeld #ff0000 voor rood.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_accent_color',
        type        => 'text',
        label       => 'Accent kleur',
        description => 'Voer een hexadecimale kleur in die wordt gebruikt voor de titels van vergaderingen. Bijvoorbeeld #ff0000 voor rood.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_header_loader_color',
        type        => 'text',
        label       => 'Laadbalk kleur',
        description => 'Voer een hexadecimale kleur in die wordt gebruikt voor de laadbalk. Bijvoorbeeld #ff0000 voor rood. Als u deze leeg laat wordt de accent kleur gebruikt.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_links',
        type => 'multiple',
        label => 'Header links',
        description => 'Geef aan welke links binnen een uitklapmenu in de header van de de app getoond moeten worden.',
        data => {
            fields => [
                {
                    name => 'label',
                    type => 'text',
                    label => 'Label',
                    description => 'Wat u hier invult wordt weergegeven in het menu binnen de app.'
                },
                {
                    name => 'link',
                    type => 'text',
                    label => 'Link URL',
                    description => 'Voer een url naar de site in. Vergeet niet http / https / protocol van uw keuze op te nemen.'
                }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_casetype_meeting',
        type => 'spot-enlighter',
        label => 'Zaaktype vergaderingen',
        required => 1,
        description => 'Geef de zaaktypen op welke worden gebruikt als vergaderingen',
        data    => {
            'restrict'      => 'casetype',
            'placeholder'   => 'Type uw zoekterm',
            'label'         => 'zaaktype_node_id.titel',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_meeting_publication_filter_attribute',
        type        => 'spot-enlighter',
        label       => 'Publicatiekenmerk voor vergaderingen',
        description => 'Als u controle wil hebben over welke vergaderingen in de app zichtbaar zijn en welke niet, selecteer hier dan een kenmerk uit uw vergaderzaaktype waarmee u dit wilt beinvloeden. Als dit leeg is worden alle vergaderingen getoond in de app.',
        data    => {
            'restrict'      => 'attributes',
            'placeholder'   => 'Type uw zoekterm',
            'label'         => 'object.column_name',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_meeting_publication_filter_attribute_value',
        type        => 'text',
        label       => 'Publicatiekenmerkwaarde voor vergaderingen',
        description => 'Selecteer hier de waarde dat het kenmerk binnen een zaak moet hebben om de vergadering te tonen binnen de app.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_casetype_voorstel_item',
        type => 'spot-enlighter',
        label => 'Zaaktype voorstellen',
        required => 1,
        description => 'Geef de zaaktypen op welke worden gebruikt als voorstellen',
        data    => {
            'multi'         => 10,
            'restrict'      => 'casetype',
            'placeholder'   => 'Type uw zoekterm',
            'label'         => 'zaaktype_node_id.titel',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_proposal_publication_filter_attribute',
        type        => 'spot-enlighter',
        label       => 'Publicatiekenmerk voor voorstellen',
        description => 'Als u controle wil hebben over welke voorstellen in de app zichtbaar zijn en welke niet, selecteer hier dan een kenmerk uit uw voorstelzaaktype waarmee u dit wilt beinvloeden. Als dit leeg is worden alle voorstellen getoond in de app.',
        data    => {
            'restrict'      => 'attributes',
            'placeholder'   => 'Type uw zoekterm',
            'label'         => 'object.column_name',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_proposal_publication_filter_attribute_value',
        type        => 'text',
        label       => 'Publicatiekenmerkwaarde voor voorstellen',
        description => 'Selecteer hier de waarde dat het kenmerk binnen een zaak moet hebben om het voorstel te tonen binnen de app.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_magic_strings_accept',
        type => 'spot-enlighter',
        label => 'Voorstel acceptatie kenmerken',
        required => 0,
        when => 'interface_access == "rw"',
        description => 'Wanneer het toegangsniveau van deze koppeling op "Behandelen" is ingesteld,'
            . ' dan is het mogelijk om voorstellen te accorderen. Dat kan bijvoorbeeld via verschillende "akkoord wethouder X" velden.'
            . ' Wanneer deze velden als "enkelvoudige keuze" of "keuzelijst" zijn ingericht, dan kunnen deze velden beschikbaar worden gemaakt'
            . ' aan de app door de velden hierin op te nemen.' ,
        data    => {
            'multi'         => 10,
            'restrict'      => 'attributes',
            'placeholder'   => 'Type uw zoekterm',
            'label'         => 'object.column_name',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_magic_strings_comment',
        type => 'spot-enlighter',
        label => 'Voorstel toelichting kenmerken',
        required => 0,
        when => 'interface_access == "rw"',
        description => 'Wanneer het toegangsniveau van deze koppeling op "Behandelen" is ingesteld,'
            . ' dan is het mogelijk om voorstellen te accorderen. Daarnaast is het mogelijk om een extra toelichting te geven.'
            . ' Dat kan bijvoorbeeld via verschillende "toelichting wethouder X" velden.'
            . ' Wanneer deze velden als "textveld" zijn ingericht, dan kunnen deze velden beschikbaar worden gemaakt'
            . ' aan de app door de velden hierin op te nemen.' ,
        data    => {
            'multi'         => 10,
            'restrict'      => 'attributes',
            'placeholder'   => 'Type uw zoekterm',
            'label'         => 'object.column_name',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_defaultview',
        type => 'select',
        label => 'Standaard weergave bij openen app vanuit groepenlijst',
        required => 1,
        description => 'Als de app wordt geopend vanaf de groepenlijst (www.uw_omgeving.nl/vergadering) krijgt de gebruiker '
            . ' standaard een bepaalde weergave te zien: voorstellen die zijn gerelateerd aan vergaderingen, of een lijst met'
            . ' alle voorstellen onafhankelijk van of ze gerelateerd zijn aan een vergadering.'
            . ' Met deze instelling stelt u in welke weergave uw gebruikers standaard bij binnenkomst te zien krijgen. '
            . ' Gebruikers kunnen nadat ze de app hebben opgestart nog steeds zelf schakelen tussen beide weergaven.',
        data => {
            options => [
                { value => 'per_meeting', label => 'Gekoppeld per vergadering' },
                { value => 'not_per_meeting', label => 'Niet gekoppeld per vergadering' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_app_uri',
        type => 'display',
        label => 'Locatie APP',
        description => 'De link naar de applicatie',
        required => 0,
        data => {
            template => '<a href="<[field.value]>/" target="_blank"><[field.value]>/</a>'
        }
    ),
];

has ['attribute_list_meeting', 'attribute_list_voorstel'] => (
    is          => 'ro',
    isa         => 'ArrayRef',
    default     => sub { return {}; },
);

has configuration_key => (
    is      => 'ro',
    isa     => 'Str',
    default => 'app_enabled_meeting',
);

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    module_type                   => ['apiv1', 'app', 'meeting'],
    label                         => 'App - VergaderAPP',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 1,
    attribute_list                => [],
    attribute_list_meeting => [
        {
            label           => 'Vergadering - Onderwerp',
            external_name   => 'vergaderingtitel',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Vergadering - Datum',
            external_name   => 'vergaderingdatum',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Vergadering - Tijd',
            external_name   => 'vergaderingtijd',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Vergadering - Locatie',
            external_name   => 'vergaderinglocatie',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Vergadering - Voorzitter',
            external_name   => 'vergaderingvoorzitter',
            attribute_type  => 'magic_string',
        },
    ],
    attribute_list_voorstel => [
        {
            label           => 'Voorstel - Zaaknummer',
            external_name   => 'voorstelzaaknummer',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Zaaktype',
            external_name   => 'voorstelzaaktype',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Onderwerp',
            external_name   => 'voorstelonderwerp',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Portefeuillehouder',
            external_name   => 'voorstelportefeuillehouder',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Samenvatting',
            external_name   => 'voorstelsamenvatting',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Vertrouwelijkheid',
            external_name   => 'voorstelvertrouwelijkheid',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Agenderingswijze',
            external_name   => 'voorstelagenderingswijze',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Conceptbesluit college',
            external_name   => 'voorstelconceptbesluitcollege',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Conceptbesluit raad',
            external_name   => 'voorstelconceptbesluitraad',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Definitief besluit college',
            external_name   => 'voorsteldefinitiefbesluitcollege',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Definitief besluit Raad',
            external_name   => 'voorsteldefinitiefbesluitraad',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Afdeling',
            external_name   => 'voorstelafdeling',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Aanvrager',
            external_name   => 'voorstelaanvrager',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Datum Afdeling',
            external_name   => 'voorsteldatumafdeling',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Datum Management',
            external_name   => 'voorsteldatummanagement',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Datum Directie',
            external_name   => 'voorsteldatumdirectie',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Datum College',
            external_name   => 'voorsteldatumcollege',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Datum Commissie',
            external_name   => 'voorsteldatumcommissie',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Datum Raad',
            external_name   => 'voorsteldatumraad',
            attribute_type  => 'magic_string',
        },
        {
            label           => 'Voorstel - Resultaat',
            external_name   => 'voorstelresultaat',
            attribute_type  => 'magic_string',
            include_system  => 1
        },
        {
            label           => 'Voorstel - Opmerkingen',
            external_name   => 'voorstelopmerkingen',
            attribute_type  => 'magic_string',
        },
    ],
    interface_update_callback => sub {
        my $module      = shift;
        my $interface   = shift;

        my $config      = $interface->get_interface_config;

        my @voorstellen = map { $_->{id} } @{ $config->{casetype_voorstel_item} };

        my @meetings    = ($config->{casetype_meeting}{id});

        $config->{casetype_voorstel_item_uuids} = [];
        $config->{casetype_meeting_uuids}       = [];

        ### Make sure UUIDs are saved, and cache them.
        if ($config->{casetype_voorstel_item} || $config->{casetype_meeting}) {
            my $zts = $interface->result_source->schema->resultset('Zaaktype')->search(
                {
                    id  => { -in => [grep {defined} (@voorstellen, @meetings)] }
                }
            );

            while (my $zt = $zts->next) {
                push(@{ $config->{casetype_voorstel_item_uuids} }, $zt->_object->uuid) if grep({ $zt->id eq $_ } @voorstellen);
                push(@{ $config->{casetype_meeting_uuids} }, $zt->_object->uuid) if grep({ $zt->id eq $_ } @meetings);
            }

            $interface->update_interface_config($config);
            $interface->update;
        }

        return;
    },
    trigger_definition => {
    },
    test_interface  => 0,
    text_templates  => {
        attributes  => qq/
            Uw koppeling maakt gebruik van attributen, welke weer
            gekoppeld kunnen worden aan velden binnen het zaaksysteem.
            Door op onderstaande knop te drukken kunt u de standaard velden koppelen
            aan kenmerken uit uw zaaktypen. Gebruik het vinkje om aan te geven dat u dit
            veld naast het detailoverzicht tevens wilt tonen in de lijstweergave van de APP.
        /,
    }
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

has 'api_supported_scope' => (
    is      => 'ro',
    default => sub {
        return ['case']
    }
);

has 'api_full_access' => (
    is      => 'ro',
    default => 1,
);

=head2 get_public_interface_config

Returns a subset of the interface configuration

=cut

sub get_public_interface_config {
    my $self        = shift;
    my $config      = shift->get_interface_config;

    # Convert zaaktypes to ids
    $config->{casetype_voorstel_item}   = [ map { $_->{id} } @{ $config->{casetype_voorstel_item} } ];
    $config->{casetype_meeting}         = [ $config->{casetype_meeting}->{id} ];

    return $config;
}


=head2 _restrict_resultset

Restricts resultset to the setup casetype ids

=cut

sub _restrict_resultset {
    my ($self, $interface) = @_;

    my $config              = $interface->get_interface_config;

    ### Check if this API is allowed, commercially.
    unless ($interface->result_source->schema->resultset('Config')->get($self->configuration_key)) {
        throw('api/v1/app/meetingapp/no_subscription', 'You have no subscription for usage of this app "MEETINGAPP", please contact Mintlab');
    }

    unless ($config->{casetype_voorstel_item} && $config->{casetype_meeting} && @{ $config->{casetype_voorstel_item} } ) {
        throw('api/v1/app/meetingapp/no_casetypes_set', 'There are no casetypes set in interface configuration');
    }

    my $voorstellen        = [ map { $_->{id} } @{ $config->{casetype_voorstel_item} } ];
    my $meetings           = [ $config->{casetype_meeting}->{id} ];

    return {
        '-or' => [
            {'case.casetype.id' => { '-in' => $voorstellen } },
            {'case.casetype.id' => { '-in' => $meetings } },
        ]
    };
}


=head1 METHODS

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    my $iface       = $opts->{entry};
    my $iface_cfg   = $iface->get_interface_config;

    $form->load_values({
        interface_app_uri => $opts->{ base_url } . 'vergadering/' . $iface_cfg->{short_name},
        interface_app_voorstellen_uri => $opts->{ base_url } . 'vergadering/' . $iface_cfg->{short_name} . '/voorstellen'
    });

    return $form;
};

=head2 dynamic_attribute_list

Generate attribute list with configured casetype ids

=cut

sub dynamic_attribute_list {
    my $self      = shift;
    my $interface = shift;

    my $interface_config    = $interface->get_interface_config;

    ### The below line needs some explanation. We need to supply a casetype, this way the autocomplete
    ### will look for attributes available in the given casetype. Because we applied the rule: every
    ### casetype must have the same attribute naming, we just take the first casetype.
    ### CAVEAT: when the first casetype doesn't have all the magic string, you will not find them
    my $meeting_casetype  = $interface_config->{casetype_meeting}{id};
    my $proposal_casetype = $interface_config->{casetype_voorstel_item}[0]{id};

    $self->log->debug(
        sprintf(
            "Meeting ID %d, proposal ID %d",
            $meeting_casetype, $proposal_casetype
        )
    );

    my @rv;
    for my $attr (@{ $self->attribute_list_meeting }) {
        push(
            @rv,
            {
                %$attr,
                optional        => 1,
                case_type_id    => $meeting_casetype,
            },
        )
    }

    for my $attr (@{ $self->attribute_list_voorstel }) {
        push(
            @rv,
            {
                %$attr,
                optional        => 1,
                case_type_id    => $proposal_casetype,
            },
        )
    }

    # "Voorstel - Bijlage" attributes are special: they multiply so there are always more of them to add magicstrings into :)
    my $max_attachment_id = 0;
    my $empty             = 0;

    my $qr = qr/^voorstelbijlage([1-9][0-9]*)$/;

    my @bijlages = grep { $_->{external_name} =~ /$qr/ } @{ $interface_config->{attribute_mapping} };

    for my $attribute (@bijlages) {
        my $attachment_id;
        if ($attribute->{external_name} =~ /$qr/) {
            $attachment_id = $1;
        }
        $attribute->{case_type_id} = $proposal_casetype;

        $max_attachment_id = $attachment_id if $attachment_id > $max_attachment_id;

        if (!$attribute->{internal_name} || !$attribute->{internal_name}{searchable_object_id}) {
            $empty++;
        }

        push @rv, $attribute;
    }

    if ($empty < 3) {
        for (1..5) {
            my $num = ++$max_attachment_id;
            $self->log->debug("Adding attachment fields: $num");
            push @rv, {
                label           => "Voorstel - Bijlage label $num",
                external_name   => "voorstelbijlage$num",
                attribute_type  => 'magic_string',
                optional        => 1,
                checked         => 0,
                case_type_id    => $proposal_casetype,
            };
        }
    }

    return \@rv;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
