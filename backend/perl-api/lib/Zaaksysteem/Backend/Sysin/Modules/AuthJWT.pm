package Zaaksysteem::Backend::Sysin::Modules::AuthJWT;
use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

my $MODULE_SETTINGS = {
    name                    => 'authjwt',
    label                   => 'Azure OAuth2',
    sensitive_config_fields => [qw(oauth_secret)],
    interface_config        => [
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_application_id',
            type        => 'text',
            label       => 'Application ID',
            data        => { placeholder => '1234567890' },
            required    => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_tenant_id',
            type        => 'text',
            label       => 'Tenant ID',
            required    => 1,
            data        => { placeholder => '1234567890' },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_resource_id',
            type        => 'text',
            label       => 'Resource ID',
            required    => 0,
            data        => { placeholder => '1234567890' },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_oauth_secret',
            type     => 'password',
            label    => 'Client secret',
            required => 0,
            data        => { placeholder => 'f6djWUeiD3dt06cIwhMtTWFJ5ePG8A0B' },
        ),
    ],
    direction                     => 'outgoing',
    retry_on_error                => 0,
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,

};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{$MODULE_SETTINGS});
};


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
