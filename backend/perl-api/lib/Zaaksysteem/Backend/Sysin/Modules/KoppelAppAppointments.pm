package Zaaksysteem::Backend::Sysin::Modules::KoppelAppAppointments;

use Moose;

use BTTW::Tools;

use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::KoppelAppAppointments

=cut

use constant INTERFACE_ID => 'koppelapp_appointments';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint',
        type => 'text',
        label => 'Trigger URL',
        description => 'Voer hier een URL in waar het Zaaksysteem de afsprakeninterface kan openen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_create_button_label',
        type => 'text',
        label => 'Aanmaak Knoptekst',
        description => 'Voer hier de knoptekst in die voor de knop in de zaken gebruikt dient te worden bij het aanmaken van een afspraak',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_update_button_label',
        type => 'text',
        label => 'Update Knoptekst',
        description => 'Voer hier de knoptekst in die voor de knop in de zaken gebruikt dient te worden bij het updaten van een afspraak',
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Koppel.app Afspraken',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    module_type                   => ['apiv1', 'api'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface  => 1,
    test_definition => {
        description => 'Hier kunt u de verbinding met de externe interface testen.',

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'test_connection',
                method => 'test_connection',
                description => 'Zaaksysteem zal proberen verbinding naar de externe partij op te zetten.'
            }
        ]
    },
    trigger_definition => {
        getEndpoint => {
            method => 'getEndpoint',
            update => 1
        },
        getCreateButtonText => {
            method => 'getCreateButtonText',
            update => 1
        },
        getUpdateButtonText => {
            method => 'getUpdateButtonText',
            update => 1
        },
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

=head2 getEndpoint

Method to get the endpoint

=cut

sub getEndpoint {
    my ($self, $params, $interface) = @_;
    my $endpoint = $interface->get_interface_config->{endpoint};

    return $endpoint;
}

=head2 getCreateButtonText

Method to get the getCreateButtonText

=cut

sub getCreateButtonText {
    my ($self, $params, $interface) = @_;
    my $create_button_label = $interface->get_interface_config->{create_button_label};

    return $create_button_label;
}

=head2 getUpdateButtonText

Method to get the getUpdateButtonText

=cut

sub getUpdateButtonText {
    my ($self, $params, $interface) = @_;
    my $update_button_label = $interface->get_interface_config->{update_button_label};

    return $update_button_label;
}

=head2 test_connection

This test case tests a configured trigger endpoint for connectability and
SSL certificate checks.

=cut

sub test_connection {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.endpoint');

    unless ($url) {
        throw('api/v1/trigger/test_connection', 'Geen trigger URL geconfigureerd.');
    }

    return $self->test_host_port($url);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
