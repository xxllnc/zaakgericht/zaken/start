package Zaaksysteem::Backend::Sysin::Modules::Postex;

use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with 'Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Postex - Postex integration module

=head1 DESCRIPTION

This sysin module declares the Zaaksysteem interface for interactions with
the L<www.postex.com|http://www.postex.com> REST API.

=cut

use URI;
use Zaaksysteem::Constants::Users qw(:all);
use Zaaksysteem::External::Postex;
use Zaaksysteem::CommunicationTab;
use Zaaksysteem::ZAPI::Error;
use Zaaksysteem::ZAPI::Form::Field;
use JSON::XS qw(decode_json);

=head1 CONSTANTS

=head2 INTERFACE_CONFIG_FIELDS

Collection of L<Zaaksysteem::ZAPI::Form> objects used in building the UI for
this module.

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_base_uri',
        type        => 'text',
        label       => 'Postex API Endpoint',
        description => 'API Endpoint URL van de Postex service',
        required    => 1,
        data        => { placeholder => 'https://api.demo.postex.com' }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_generator_id',
        type        => 'text',
        label       => 'Postex generator ID',
        description => 'Generator ID',
        required    => 1,
        data        => { placeholder => '12345' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'Postex API Key',
        description => 'API Key van de Postex service',
        data        => { placeholder => 'very secret key' },
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_callback_endpoint',
        type        => 'display',
        label       => 'Postex callback endpoint',
        description => 'Endpoint van Zaaksysteem waartegen Postex API calls naar opstuurt',
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    ),
];

=head2 INTERFACE_DESCRIPTION

Module description / pointers for the UI.

=cut

use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
    <a href="https://www.postex.com/" hreflang="nl-NL" target="_blank">
    Postex</a> is een service waarmee documenten verzonden kunnen worden.
</p>

EOD

=head2 INTERFACE_TRIGGERS

Set of triggers available in the module.

=cut

use constant INTERFACE_TRIGGERS => [
    {
        method => 'receive_event',
        update => 1,
        public => 1
    },
    {
        method            => 'send_message',
        api               => 1,
        api_allowed_users => REGULAR,
        public            => 1,
        update            => 1,
    },
];

=head2 MODULE_SETTINGS

Sysin module parameters, used during instantiation to configure the module.

=cut

use constant MODULE_SETTINGS => {
    name                          => 'postex',
    label                         => 'Postex',
    description                   => INTERFACE_DESCRIPTION,
    interface_config              => INTERFACE_CONFIG_FIELDS,
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    trigger_definition            => {
        map { $_->{method} => $_ } @{ INTERFACE_TRIGGERS() }
    },
    sensitive_config_fields => ['api_key'],
};

=head1 METHODS

=cut

around BUILDARGS => sub {
    my ($orig, $class) = @_;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 receive_event

    $module->receive_event({ parameters => 'value' }, $interface);

Receive event trigger of the interface, processes the JSON send to the
interface endpoint

=cut

sub receive_event {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_process_receive_event',
                %$params,
            },
            direction               => 'incoming',
            external_transaction_id => $params->{packageId} // 'unknown',
            input_data =>
                JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    if ($transaction->error_count) {
        return Zaaksysteem::ZAPI::Error->new(
            type => 'general/error',
            messages => [
                "Error in transaction: " . $transaction->id,
                $transaction->preview_data->[0]{preview_string},
            ]
        );
    }
    return [
        {
            transaction_id => $transaction->id,
            message        => $transaction->preview_data->[0]{preview_string},
        }
    ];
}


=head2 send_message

    $module->send_message({ parameters => 'value' }, $interface);

Send message trigger of the interface, processes the JSON send to the
interface endpoint

=cut

sub send_message {
    my ($self, $params, $interface) = @_;

    my $req_params = $params->{request_params};
    my $json_params = JSON::XS->new->utf8(0)->canonical->pretty->encode(
        $req_params
    );

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_process_send_message',
                %$req_params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => $json_params,
        }
    );

    my $message = 'Bericht aangeleverd aan Postex';
    if (! $transaction->success_count) {
        $message = 'Fout tijdens versturen naar Postex';
    }

    return {
        type      => 'result',
        reference => $transaction->uuid,
        preview   => $message,
        instance  => {
            ok      => $transaction->success_count ? \1 : \0,
            message => $message,
            type    => 'sysin/postex/send_message',
        },
    };
}
=head1 PRIVATE METHODS

=head2 _load_values_into_form_object

Hooks into interface initialization routine to inject default values for
specified config fields.

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # don't clobber @_ further than original args

    my $form = $self->$orig(@_);
    my $interface = $opts->{ entry };

    $form->load_values({
        interface_callback_endpoint => sprintf(
            '%ssysin/interface/%d/trigger/receive_event',
            $opts->{ base_url },
            $interface->id,
        )
    });

    return $form;
};

=head2 _process_send_message

Send document to Postex
Triggers case logging mechanism for case/send_postex

=cut

sub _process_send_message {
    my ($self, $record) = @_;
    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);
    my $case_id = $params->{case_id};

    try {
        $interface->model->send_documents_to_postex(
            transaction        => $transaction,
            transaction_record => $record,
            case_id            => $case_id,
            recipient_type     => $params->{recipient_type},
            recipient_role     => $params->{recipient_role},
            file_attachments   => $params->{file_attachments},
            sender_uuid        => $params->{sender_uuid},
            subject            => $params->{subject},
            body               => $params->{body},
        );
    }
    catch {
        $transaction->preview_data({ preview_string => "$_" });
        $record->preview_string("$_");

        throw(
            'sysin/modules/postex/communication_problem',
            "Bericht kon niet worden aangeleverd: $_",
            { fatal => 1 }
        );
    };

    my $msg = "Postex bericht voor zaak $case_id is correct verwerkt";
    $transaction->preview_data({ preview_string => $msg });
    $record->preview_string($msg);
}

sub _process_receive_event {
    my $self = shift;
    my $record = shift;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    try {
        my $case = $interface->model->process_postex_status_update(%$params);
        my $str  = sprintf("Updated case %d with Postex feedback", $case->id);

        $transaction->preview_data([{ preview_string => $str }]);
        $record->preview_string($str);
    }
    catch {
        my $msg = "$_";
        $self->log->info($msg);
        $transaction->error_fatal(1);
        $transaction->error_message($msg);
        $transaction->preview_data([{ preview_string => $msg }]);

        $record->is_error(1);
        $record->last_error($msg);
    };

    return;
}

sub _get_model {
    my ($self, $opts) = @_;

    my $config = $opts->{interface}->get_interface_config;

    my $communication = Zaaksysteem::CommunicationTab->new(
        schema => $opts->{interface}->result_source->schema,
    );

    return Zaaksysteem::External::Postex->new(
        secret        => $config->{api_key},
        generator_id  => $config->{api_generator_id},
        base_uri      => $config->{api_base_uri},
        communication => $communication,
        schema        => $opts->{interface}->result_source->schema,
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
