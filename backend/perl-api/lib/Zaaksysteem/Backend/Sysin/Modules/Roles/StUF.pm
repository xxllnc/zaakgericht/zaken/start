package Zaaksysteem::Backend::Sysin::Modules::Roles::StUF;

use Moose::Role;

with 'Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate';

use Data::Dumper;
use Zaaksysteem::StUF;
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head2 config_interface

An interface component (database object) for the configuration interface to be
used.

=cut

has config_interface => (
    is       => 'rw',
    required => 0,
    lazy     => 1,
    default  => sub {
        my $self = shift;

        my $config_interface_id = $self->process_stash->{config_interface_id};

        if (!$config_interface_id) {
            my $params = $self->process_stash->{transaction}->get_processor_params;
            if (ref $params) {
                $config_interface_id =
                  $params->{params}{config_interface_id}
                  // $params->{config_interface_id}
            }
        }

        my $schema = $self->process_stash->{transaction}->result_source->schema;
        if ($config_interface_id) {
            my $config_interface = $schema->resultset('Interface')->search_active(
                { id => $config_interface_id }
            )->first;

            return $config_interface if $config_interface;
        }
        else {
            my $rs =
              $schema->resultset('Interface')
              ->search_active({ module => 'stufconfig' });
            if ($rs->count == 1) {
                return $rs->first;
            }
        }


        my $msg  = "StUF configuration interface not found or not active";
        throw("stuf/config_interface_not_found", $msg);
    },
);

sub stuf_throw {
    my $self                = shift;
    my ($object, $type, $message, $extra_options)    = @_;

    throw('stuf_throw', 'Missing required arguments') unless $object && $type && $message;

    my $stufmessage = $object->fout(
        {
            reference_id            => (
                $self->process_stash->{transaction}
                    ? $self->process_stash->{transaction}->id
                    : 'T' . time()
            ),
            date            => DateTime->now(),
            code            => 'StUF003',
            plek            => 'client',
            omschrijving    => $type .': ' . $message,
        }
    );

    $extra_options           ||= {};

    throw(
        $type,
        $message, {
            transaction_output  => $stufmessage,
            %{ $extra_options }
        }
    );
}

sub is_active {
    my $self            = shift;
    my $interface       = shift;

    return unless $self->config_interface->active;
    return unless $interface->active;
    return 1;
}

sub get_config_from_config_iface {
    my $self            = shift;

    return $self->config_interface->get_interface_config;
}

=head2 _get_certificates

    %result = $self->_get_certificates($config_interface);

    # Returns:
    (
        SSL_ca_file     => '/tmp/cert.ca',
        SSL_cert_file   => '/tmp/cert.crt',
        SSL_key_file    => '/tmp/key.key'
    )

=cut

sub _get_certificates {
    my $self                = shift;
    my ($interface, $type)  = @_;
    my $schema              = $interface->result_source->schema;

    my $ca_file;
    if ($interface->jpath('$.ca_cert[0].id')) {
        $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.ca_cert[0].id'))->get_path;
    }

    if ($interface->jpath('$.client_cert[0].id')) {
        my $client_file         = $schema->resultset('Filestore')->find($interface->jpath('$.client_cert[0].id'))->get_path;

        return (
            $ca_file ? (SSL_ca_file => $ca_file) : ('verify_hostname'   => 0),
            SSL_cert_file => $client_file,
            SSL_key_file  => $client_file,
        );
    } else {
        my $config          = $schema->catalyst_config;

        return (
            $ca_file ? (SSL_ca_file => $ca_file) : ('verify_hostname'   => 0),
            SSL_cert_file       => $config->{services}{ssl_crt},
            SSL_key_file        => $config->{services}{ssl_key}
        );
    }

}

=head2 $module->_get_stuf_version($interface)

Return value: $STUF_VERSION

Returns the configured version in the interface for this StUF module. Defaults to C<0204>. As
first parameter, please supply the current interface object, e.g. C<< transaction->interface_id >>

B<Possible return values>

=over 4

=item * 0204

Version 0204

=item * 0310

Version 0310 of BG (or 0301 StUF)

=back

=cut

sub _get_stuf_version {
    my $self            = shift;
    my $interface       = shift;

    my $version         = '0204';

    if ($interface->get_interface_config->{stuf_version}) {
        $version        = $interface->get_interface_config->{stuf_version};
    } elsif ($self->can('stuf_version')) {
        $version        = $self->stuf_version;
    }

    return $version;
}


=head2 _process_get_next_row

Single xml mode only

=cut

sub _process_get_next_row {
    my $self        = shift;

    my $xml;

    if (my $params = $self->process_stash->{transaction}->get_processor_params) {
        return if ($self->process_stash->{once});

        $self->process_stash->{once} = 1;

        #return ($params, $params);
        # Not sure why we dump this data somewhere
        return ($params, Data::Dumper::Dumper($params));
    }

    if ($self->process_stash->{transaction}->input_file) {
        if (!$self->process_stash->{stuf}->{entries}) {
            my $xmlobj  = XML::LibXML->load_xml(
                'location'  => $self->process_stash->{transaction}->input_file->get_path()
            );

            ### Find first kennisgevingsBericht
            my @nodes   = $xmlobj->findnodes("//*[local-name()='kennisgevingsBericht']");

            my @entries;
            for my $node (@nodes) {
                push(@entries, $node->toString());
            }

            $self->process_stash->{stuf}->{entries} = \@entries;
        }

        return unless $self->process_stash->{stuf}->{entries};

        my $pointer     = $self->process_stash->{stuf}->{pointer};

        if ($pointer) {
            $xml        = $self->process_stash->{stuf}->{entries}->[
                $pointer++
            ];
        } else {
            $pointer = 0;

            $xml        = $self->process_stash->{stuf}->{entries}->[
                $pointer++
            ];
        }

        $self->process_stash->{stuf}->{pointer} = $pointer;

    } else {

        return if $self->process_stash->{stuf}->{xml};

        return unless $self
                    ->process_stash
                    ->{transaction}
                    ->input_data;

        $xml            = $self->process_stash->{stuf}->{xml} = $self
                        ->process_stash
                        ->{transaction}
                        ->input_data;
    }

    return unless $xml;

    if ($self->process_stash->{config_interface_id} && $self->can('config_interface')) {
        my $config_interface_id = $self->process_stash->{config_interface_id};
        my $schema = $self->process_stash->{transaction}->result_source->schema;

        my $config_interface = $schema->resultset('Interface')->search_active(
            { id => $config_interface_id }
        )->first;

        unless ($config_interface) {
            throw(
                "stuf/config_interface_not_found",
                sprintf(
                    "StUF configuration interface %d not found or not active.",
                    $config_interface_id
                )
            );
        }

        $self->config_interface($config_interface);
    }

    my $stuf_config = $self->get_config_from_config_iface;

    my $interface   = $self
                    ->process_stash
                    ->{transaction}
                    ->interface_id;

    my $object      = Zaaksysteem::StUF->from_xml(
        $xml,
        {
            cache => ($self->process_stash->{transaction}->result_source->schema->default_resultset_attributes->{'stuf_cache'} ||= {}),
            version => $self->_get_stuf_version($interface)
        }
    );

    if ($object && $object->stuurgegevens->referentienummer) {
        $self->process_stash->{transaction}->external_transaction_id(
            $object->stuurgegevens->referentienummer
        );
        $self->process_stash->{transaction}->update;
    }

    $self->process_stash->{stuf}->{xml} = $xml || 1;

    return ($object, $xml);
}

=head2 $module->_process_selector

Return value: subref to module processor

=cut

sub _process_selector {
    my $self            = shift;
    my $object          = shift;
    my $record          = shift;

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf/process/inactive',
        'Interface is inactive, StUF messages cannot be processed',
    ) unless $self->is_active($record->transaction_id->interface_id);


    if (my $params = $record->transaction_id->get_processor_params) {
        if ($params->{processor}) {
            return $self->can($params->{processor});
        }
    }

    if (lc($object->stuurgegevens->berichtsoort) eq 'lk01') {
        return $self->can('_process_kennisgeving');
    }

    throw(
        'sysin/modules/prs/process_selector',
        'No support for this message built in zaaksysteem:' . burp($object->stuurgegevens)
    );
}

=head2 $module->_process_kennisgeving($transaction_record, $rowobject)

Return value: $STRING_RESPONSE_XML

Processes the StUF XML "row" (message).

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would probably a HASHREF row, but in
this case, it is a L<Zaaksysteem::StUF::PRS> object.

=back

=cut

sub _process_kennisgeving {
    my $self            = shift;
    my $record          = shift;
    my $object          = shift;

    $self->_validate_stuf_message($object);

    $self->process_stash->{row}->{mutations} = [];

    my $error;
    try {
        $self->_stuf_process_mutations($record, $object);
    } catch {
        $error = 1;
        $record->is_error(1);


        my $msg = qq{Message could not be processed by Zaaksysteem.
We DID send an acknowledge to make sure other messages will be processed.

Error message: };
        # Always send a stuf answer
        # but let's keep the error in the transaction log somehwere

        my $stuf_msg;
        my $stuf_type;
        if (blessed($_) && $_->can('message')) {
            $msg .= $_->message;
            $stuf_msg = $_->message;
            if ($_->can('type')) {
                $stuf_type = $_->type;
            }
        }
        else {
            $msg .= "$_";
        }

        $stuf_type //= 'unknown/error';
        $stuf_msg //= "$_";

        my $transaction = $record->transaction_id;

        my $xml = $object->fout(
            {
                omschrijving => $stuf_type,
                details      => $stuf_msg,
                reference_id => $transaction->id,
                date         => DateTime->now(),
                plek         => 'server',
                code         => 'StUF058',
            }
        );

        $self->log->info(sprintf("Error processing transaction '%d': %s", $transaction->id, $_));

        $record->transaction_id->error_message($msg);
        $record->transaction_id->update;
        $record->output($xml);

    };

    if (!$error) {
        ### We did not die yet, response with bevestiging
        $record->output(
            $object->acknowledge(
                {
                    date            => $record->transaction_id->date_created,
                    reference_id    => $record->get_column('transaction_id'),
                }
            )->to_xml
        );
    }

    $record->update;
    return 1;
}

sub _stuf_process_mutations {
    my $self            = shift;
    my $record          = shift;
    my $object          = shift;

    my $mutatiesoort    = $object->mutatiesoort;

    ### Create
    if ($mutatiesoort eq 'T') {
        ### Sleutel already exists?
        my $existing = $self->find_entry_from_subscription(
            $record,
            $object,
            $self->stuf_subscription_table
        );

        if (!$existing) {
            my $entry  = $self->stuf_create_entry($record, $object);
            ### XXX TODO
            ### OPTIONAL: when indicatorOvername = V (verplicht), we set a subscription
            ### when it is not already set
            $self->_add_subscription_for_entry($record, $object, $entry) if $entry;
        } else {
            $mutatiesoort = 'W';
        }
    }

    ### Update (or Correction)
    if ($mutatiesoort eq 'W' || $mutatiesoort eq 'C') {
        $self->stuf_update_entry($record, $object);
    }

    ### Delete
    if ($mutatiesoort eq 'V') {
        $self->get_entry_from_subscription(
            $record,
            $object,
            $self->stuf_subscription_table
        );

        $self->stuf_delete_entry($record, $object);
    }
}

sub _get_object_subscription_from_object {
    my ($self, $record, $object, $local_table, $config_module_id) = @_;

    my $config_module = $self->config_interface;
    my $params              = $object->as_params;
    my $external_id         = $params->{ $config_module->module_object->get_primary_key($config_module) };
    my $local_id            = $params->{sleutelOntvangend};

    my %args = (
            interface_id => $record->transaction_id->get_column('interface_id'),
            local_table  => $local_table,
            # If the other side specifies OUR key, use it; otherwise, use their key.
            $local_id
                ? (id => $local_id)
                : (external_id => $external_id),
            config_interface_id => $config_module->id,
            date_deleted        => undef,
    );

    my $rs = $record->result_source->schema->resultset('ObjectSubscription')->search(\%args);
    my @found = $rs->all;

    if (@found == 1) {
        my $subscription = $found[0];
        $subscription->update({ external_id => $external_id });
        return $subscription->discard_changes;
    }
    elsif (@found > 1) {
        throw(
            "object_subscription/multiple_found",
            sprintf(
                "Multiple subscriptions found with %s/%s",
                $local_id // '<sleutelOntvangend was leeg>', $external_id
            )
        );
    }

    # Not found at all - look at deleted stuf
    $args{date_deleted} = { '!=' => undef };

    $rs = $record->result_source->schema->resultset('ObjectSubscription')
             ->search(\%args, { order_by => { '-desc' => 'id' }, rows => 1 });

    my $os = $rs->first;
    if ($os) {
        $os->update({ external_id => $external_id });
        return $os;
    }
    return;

}

=head2 find_entry_from_subscription

Find an entry based on the object subscription. Dies in case of errors.

=cut

sub find_entry_from_subscription {
    my ($self, $record, $object, $local_table, $object_subscription) = @_;
    $object_subscription //= $self->_get_object_subscription_from_object($record, $object, $local_table);

    return if !$object_subscription;
    return $self->_get_entry($object_subscription);
}

sub _get_entry {
    my ($self, $os, $object) = @_;
    my $entry = $os->result_source->schema->resultset($os->local_table)->find($os->local_id);

    return $entry if $entry;

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf/process/no_np_entry_found',
        sprint(
            "No existing entry '%d' found for this subject in our database %s",
            $os->local_id, $os->table
        )
    );

    return;
}

=head2 get_entry_from_subcription

Get an entry based on the object subscriptions. Dies in case of errors
and when nothing can be found. See also C<find_entry_from_subscription>

=cut

sub get_entry_from_subscription {
    my ($self, $record, $object, $local_table, $object_subscription) = @_;

    $object_subscription //= $self->_get_object_subscription_from_object($record, $object, $local_table);
    return $self->_get_entry($object_subscription) if $object_subscription;

    throw("object_subscription/none_found", "Unable to to find object subscription for $local_table");
}


=head2 _process_disable_subscription

Arguments:

=cut

sub _process_disable_subscription {
    my $self                        = shift;
    my ($record,$object)            = @_;

    my $p_params = $record->transaction_id->get_processor_params->{params};

    my $interface = $record->transaction_id->interface_id;

    my $schema = $interface->result_source->schema;

    my $subscription = $self->_get_object_subscription_by_id(
        $schema,
        $p_params->{subscription_id}
    );

    my $config_module = $subscription->config_interface_id;
    my $module_cfg = $self->get_config_from_config_iface;

    my $return_output = '';
    if (
        $config_module->module_object->can_connect_to_other_sbus($config_module)
    ) {
        my $transaction = $record->transaction_id;

        my $call_options        = $self->_load_call_options_from_config(
            $transaction->interface_id,
            {
                reference           => $transaction->id,
                datetime            => DateTime->now(),
                follow_subscription => 1,
                calltype            => 'async',
                set_subscription    => 1,
            }
        );

        my ($rv, $trace)        = $self->stuf0204->disable_subscription(
            {
                external_id     => $p_params->{external_id},
                subscription_id => $p_params->{subscription_id},
                entity_type     => uc($self->stuf_object_type),
            },
            $call_options
        );

        $transaction->external_transaction_id($transaction->id);
        $transaction->direction('outgoing');
        $transaction->update;

        unless ($rv) {
            throw(
                'sysin/modules/stuf' . lc($self->stuf_object_type) . '/disable_natuurlijkpersoon_subscription/error',
                'Failed disabling natuurlijk_persoon_subscription: ' . Data::Dumper::Dumper($trace)
            );
        }

        $record->input($trace->request->content);
        $return_output = $trace->response->content;

    } else {
        $return_output = 'Subscription removed, but no interaction started with "StUF Makelaar"'
                .': synchronization-mode is not "hybrid" or "question". '
                . $p_params->{external_id} . '" must be manually removed';
    }

    $self->stuf_delete_entry($record, $object, $subscription);
    $record->output($return_output);

    return $return_output;
}


=head2 $module->_add_subscription_for_entry($transaction_record, $rowobject, $natuurlijk_persoon)

Return value: $BOOL_SUCCES

Generates a L<Zaaksysteem::Backend::Sysin::ObjectSubscription::Component> row, to
subscribe our data record with a datarecord from our interface

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would probably a HASHREF row, but in
this case, it is a L<Zaaksysteem::StUF::PRS> object.

=item $natuurlijk_persoon

The L<Zaaksysteem::DB::Component::NatuurlijkPersoon> row to enable a subscription on.

=back

=cut

sub _add_subscription_for_entry {
    my $self                = shift;
    my ($record, $object, $entry)   = @_;

    my $interface = $record->transaction_id->interface_id;

    my $config_module = $self->config_interface;

    my $args = {
        interface_id => $interface->id,
        local_table  => $entry->result_source->source_name,
        local_id     => $entry->id,
        external_id  => $object->as_params->{
            $config_module->module_object->get_primary_key(
                $config_module)
        },
        config_interface_id => $config_module->id,
    };

    my $rs = $record->result_source->schema->resultset('ObjectSubscription');

    my $subscription = $rs->search($args)->first // $rs->create($args);

    $subscription->object_preview(
        $record->preview_string
    );

    $subscription->update;
}

=head2 $module->_remove_subscription_from_entry($transaction_record, $rowobject, $natuurlijk_persoon)

Return value: $BOOL_SUCCES

Sets the C<date_deleted> on a L<Zaaksysteem::Backend::Sysin::ObjectSubscription::Component>
row, to unsubscribe our data record.

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would probably a HASHREF row, but in
this case, it is a L<Zaaksysteem::StUF::PRS> object.

=item $natuurlijk_persoon

The L<Zaaksysteem::DB::Component::NatuurlijkPersoon> row to disable a subscription on.

=back

=cut

sub _remove_subscription_from_entry {
    my $self                = shift;
    my ($record, $object, $entry)   = @_;

    my $config_module = $self->config_interface;

    ### NatuurlijkPersoon
    my $subscription        = $record
                            ->result_source
                            ->schema
                            ->resultset('ObjectSubscription')
                            ->search(
                                {
                                    interface_id    => $record
                                                    ->transaction_id
                                                    ->interface_id
                                                    ->id,
                                    local_table     => $entry->result_source->source_name,
                                    local_id        => $entry->id,
                                    config_interface_id => $config_module->id,
                                }
                            )->first;

    if ($subscription) {
        $subscription->date_deleted(DateTime->now());
        $subscription->update;
    }
}

=head2 $module->_validate_stuf_message($transaction_record)

Return value: $BOOL_SUCCES

Validates the given StUF message for a correct C<entiteittype> and C<mutatiesoort>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=back

=cut

sub _validate_stuf_message {
    my $self            = shift;
    my $object          = shift;

    # $self->stuf_throw(
    #     $object,
    #     'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/wrong_entiteittype',
    #     'Invalid entiteittype for processor: ' . $object->entiteittype
    # ) unless ($object->entiteittype eq $self->stuf_object_type);

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/unknown_mutatiesoort',
        'Cannot determine what action to run because of missing mutatiesoort'
    ) unless (
        $object->mutatiesoort
    );

}

=head2 $module->disable_subscription($object_subscription)

Return value: $BOOL_SUCCESS

=cut

sub disable_subscription {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    my $subscription    = $params->{subscription_id};
    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/disable_subscription',
        'No subscription id supplied',
    ) unless $subscription;

    if(UNIVERSAL::isa($subscription, 'DBIx::Class')) {
        $self->config_interface($subscription->config_interface_id);
    }
    else {
        $subscription = $self->_get_object_subscription_by_id(
            $interface->result_source->schema,
            $subscription
        );
    }

    $interface->process(
        {
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => 'Disable subscription: ' . $subscription->external_id,
            processor_params        => {
                processor                       => '_process_disable_subscription',
                params                          => {
                    subscription_id         => $subscription->id,
                    external_id             => $subscription->external_id,
                    config_interface_id     => $self->config_interface->id,
                }
            },
        }
    );
}

sub _get_object_subscription_by_id {
    my ($self, $schema, $id) = @_;

    throw('stuf/send_subcription/no_id', 'No subcription ID given')
        unless $id;

    my $os = $schema->resultset('ObjectSubscription')
        ->search_rs({ id => $id, date_deleted => undef })->first;

    if ($os) {
        $self->config_interface($os->config_interface_id);
        return $os;
    }

    throw(
        'stuf/send_subcription/no_subscription',
        "No subcription with ID $id was found"
    );

}

sub _load_call_options_from_config {
    my $self            = shift;
    my $interface       = shift;
    my $options         = shift || {};

    my $module_cfg;
    if ($interface->name eq 'STUFCONFIG') {
        $module_cfg = $interface->get_interface_config;
    }
    else {
        $module_cfg = $self->get_config_from_config_iface;
    }

    my $rv              = {
        sender                  => $module_cfg->{mk_sender},
        sender_organization     => $module_cfg->{mk_sender_organization},
        sender_administration   => $module_cfg->{mk_sender_administration},

        receiver                => (
            $options->{via_gbav}
                ? $module_cfg->{gbav_applicatie}
                : $module_cfg->{mk_ontvanger}
        ),
        reference               => $options->{reference},
        datetime                => $options->{datetime}->strftime('%Y%m%d%H%M%S00'),
        dispatch                => {},
        key_type                => ($module_cfg->{stuf_supplier} =~ /^pink(v3)?$/ ? 'gegevens' : 'ontvangend'),
        gemeentecode            => $module_cfg->{gemeentecode}
    };

    ### We need another receiver voor "afnemerindicaties"
    $rv->{receiver}     = $module_cfg->{mk_ontvanger_afnemer} if $options->{set_subscription};
    $rv->{supplier}     = $module_cfg->{stuf_supplier};

    if ($module_cfg->{mk_spoof}) {
        $self->log->trace("StUF Spoof mode activated");

        if ($options->{calltype} && $options->{calltype} eq 'pink_gbav') {
            $rv->{dispatch}{transport_hook} = sub {
                my $request  = shift;
                my $pink = Zaaksysteem::XML::Compile
                    ->xml_compile
                    ->add_class('Zaaksysteem::XML::Pink::Instance')
                    ->pink;

                return $pink->spoof_answer($request);
            };
        }
        else {
            $rv->{dispatch}{transport_hook} = sub {
                my $request  = shift;
                my $stuf0204 = Zaaksysteem::XML::Compile
                    ->xml_compile
                    ->add_class('Zaaksysteem::StUF::0204::Instance')
                    ->stuf0204;

                return $stuf0204->spoof_answer($request);
            };
        }

        return $rv;
    }

    my %transport_opts = (
        timeout     => 30,
    );

    if ($options->{calltype} && $options->{calltype} eq 'async') {
        $transport_opts{address}    = $module_cfg->{mk_async_url};
    } elsif ($options->{calltype} && $options->{calltype} eq 'pink_gbav') {
        $transport_opts{address}    = $module_cfg->{gbav_pink_url};
    } else {
        $transport_opts{address}    = $module_cfg->{mk_sync_url};
    }

    my $transport   = XML::Compile::Transport::SOAPHTTP->new(%transport_opts);

    my %certs;
    if ($interface->name eq 'STUFCONFIG') {
        %certs = $self->_get_certificates($interface);
    }
    else {
        %certs = $self->_get_certificates($self->config_interface);
    }

    $transport->userAgent->ssl_opts(
        %certs,
    );

    $rv->{dispatch}->{transport} = $transport;
    $rv->{dispatch}->{endpoint} = $transport_opts{address};

    return $rv;
}



1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_config_from_config_iface

TODO: Fix the POD

=cut

=head2 is_active

TODO: Fix the POD

=cut

=head2 stuf_throw

TODO: Fix the POD

=cut

