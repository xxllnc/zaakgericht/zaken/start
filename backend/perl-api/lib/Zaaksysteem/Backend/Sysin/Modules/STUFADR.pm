package Zaaksysteem::Backend::Sysin::Modules::STUFADR;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;

use BTTW::Tools;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUFADRAOA
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufadr';

use constant INTERFACE_CONFIG_FIELDS    => [];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling ADR',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
        # search          => {
        #     method  => 'search_nnp',
        #     #update  => 1,
        # },
        # import      => {
        #     method  => 'import_nnp',
        #     #update  => 1,
        # },
    },
    # has_attributes                  => 1,
    # attribute_list                  => [
    #     {
    #         external_name   => 'PRS.a-nummer',
    #         internal_name   => 'a_nummer',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.bsn-nummer',
    #         internal_name   => 'burgerservicenummer',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voornamen',
    #         internal_name   => 'voornamen',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorletters',
    #         internal_name   => 'voorletters',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorvoegselGeslachtsnaam',
    #         internal_name   => 'voorvoegsel',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsnaam',
    #         internal_name   => 'geslachtsnaam',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geboortedatum',
    #         internal_name   => 'geboortedatum',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsaanduiding',
    #         internal_name   => 'geslachtsaanduiding',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.datumOverlijden',
    #         internal_name   => 'datum_overlijden',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.indicatieGeheim',
    #         internal_name   => 'indicatie_geheim',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.aanduidingNaamgebruik',
    #         internal_name   => 'aanduiding_naamgebruik',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    # ]
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'ADR'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'BagNummeraanduiding'
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

1;

__END__

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFPRS - STUFPRS engine for StUF PRS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/431-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 TRIGGERS

=head2 search($params)

=cut

=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_NATUURLIJK_PERSOON

Creates a new L<Zaaksysteem::DB::Component::NatuurlijkPersoon> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut




=head1 INTERNAL METHODDS

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

