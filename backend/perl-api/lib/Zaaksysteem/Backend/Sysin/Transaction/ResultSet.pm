package Zaaksysteem::Backend::Sysin::Transaction::ResultSet;

use Moose;
use JSON;

use BTTW::Tools;
use Zaaksysteem::Backend::Sysin::Modules;

extends 'Zaaksysteem::Backend::ResultSet';

with 'Zaaksysteem::Search::TSVectorResultSet';

=head2 text_vector_column

Added support for vector searching, by including Role
L<Zaaksysteem::Search::TSVectorResultSet> and this "attribute"

=cut

sub text_vector_column { 'text_vector' }

has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            'me.date_deleted' => undef,
        }
    }
);

=head2 transaction_create

Add a transaction entry to the database.

=head3 Arguments

=over

=item interface_id [required]

=item external_transaction_id [required]

The identifier in the originating system. (The calling party)

=item automated_retry_count [optional]

Set the number of automated retries already done. Defaults to none.

=item input_data && input_file [require_one]

Either input_data (raw XML, for example) or input_file (a CSV-file) is required.

=item date_last_retry

Last time a retry attempt was done.

=item date_next_retry

When the transaction-runner needs to try executing the request again. Defaults to now() so it will be executed for the first time.

=back

=head3 Returns

A newly created Sysin::Transaction object.

=cut

Params::Profile->register_profile(
    method  => 'transaction_create',
    profile => {
        required => [qw/
            interface_id
            external_transaction_id
        /],
        optional => [qw/
            automated_retry_count
            input_data
            input_file
            date_last_retry
            date_next_retry

            processor_params
            direction
        /],
        constraint_methods => {
            interface_id            => qr/\d+/,
            automated_retry_count  => qr/\d+/,
            interface_id           => sub {
                my ($dfv, $val)     = @_;

                my $schema          = $dfv->get_input_data->{schema};

                return (
                    $schema->resultset('Interface')->find($val)
                        ? 1
                        : 0
                );
            },
        },
        require_some => {
            input_data_or_file => [1, qw/input_data input_file/],
        },
    }
);

sub transaction_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    if (not exists $opts->{uuid}) {
        $opts->{uuid} = Data::UUID->new->create_str();
    }

    return $self->create($opts);
}

=head2 search_filtered

Does a search on transactions with a given filter. Input is as you would pass to
DBIx::Class.

B<Filters>

=cut

Params::Profile->register_profile(
    method  => 'search_filtered',
    profile => {
        required => [],
        optional => [qw/
            records.is_error
            interface_id
            me.date_deleted
            me.processed
            freeform_filter
        /],
        constraint_methods => {
            is_error => qr/(0|1)/,
        },
        defaults => {
            'me.date_deleted' => undef,
        }
    }
);

sub search_filtered {
    my $self    = shift;
    my $options = assert_profile(shift || {})->valid;

    if ($options->{'records.is_error'}) {
        $options->{'-and'} = [
            { 'me.error_count' => { '!=' => undef } },
            { 'me.error_count' => { '>' => 0 } },
        ];

        delete($options->{'records.is_error'});
    }

    my $filter;
    if ($options->{freeform_filter}) {
        $filter  = $options->{freeform_filter};
        delete($options->{freeform_filter});
    }

    my $search = $self->search(
        $options,
        {
            prefetch    => ['interface','input_file'],
            order_by    => {-desc => ['me.date_created']},
        }
    );

    if ($filter) {
        return $search->search_text_vector($filter);
    }

    return wantarray ? $search->all : $search;
}

=head2 process_pending

Does a search on transactions for failed records with a date_next_retry in the future
and reprocesses them

=cut

sub process_pending {
    my $self            = shift;

    # process transactions which are not yet processed and have a date in the past
    # and error_fatal is not set.
    #
    # OR
    #
    # Every transaction with a error count above 0 and no error_fatal and a next_retry
    # in the future
    my $dt = $self->result_source->schema->format_datetime_object(DateTime->now);

    my $rs              = $self->search(
        {
            date_next_retry     => {
                '<'    => $dt,
                '!='   => undef ,
            },
            error_fatal         => [
                { '='    => undef },
            ],
            date_deleted        => undef,
        }
    );

    while (my $transaction = $rs->next) {
        eval {
            $transaction->process;
        };

        if ($@) {
            warn('backend/sysin/transaction/component/process_pending/error: ' . $@);
        }
    }

    return 1;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

