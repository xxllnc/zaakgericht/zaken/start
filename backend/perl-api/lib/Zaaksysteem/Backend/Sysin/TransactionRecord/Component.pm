package Zaaksysteem::Backend::Sysin::TransactionRecord::Component;

use Moose;

extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::Backend::Sysin::TransactionRecord::Component - Transaction Component

=head1 SYNOPSIS

=head1 DESCRIPTION

These methods provides transaction specific actions for our CRUD interface

=head1 ATTRIBUTES

=head2 _json_data

ISA: HashRef

Defines the json_data for this row.

=cut

has '_json_data'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;

        no strict 'refs';
        no warnings 'redefine';
        *DateTime::TO_JSON          = sub { shift->iso8601 . 'Z' };
        use strict;

        my $transaction = $self->transaction;
        my $pub_info = {
            id              => $self->id,
            uuid            => $self->id,
            transaction_id   => $transaction->id,
            transaction_uuid => $transaction->uuid,
            input           => $self->input,
            output          => $self->output,
            is_error        => $self->is_error,
            date_executed   => $self->date_executed,
            date_deleted    => $self->date_deleted,
            preview_string  => $self->preview_string,
            transaction_record_to_object => $self->transaction_record_to_objects->single,
        };

        return $pub_info;
    },
);

=head2 transaction_record_delete()

Will mark this record as deleted

=cut

sub transaction_record_delete {
    my $self = shift;
    $self->update({
        date_deleted => DateTime->now,
    });
    return 1;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

