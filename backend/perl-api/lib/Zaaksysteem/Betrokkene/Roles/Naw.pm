package Zaaksysteem::Betrokkene::Roles::Naw;
use Moose::Role;

=head1 NAME

Zaaksysteem::Betrokkene::Roles::Naw - A role for betrokkene address functions

=head1 METHODS

=head2 get_volledig_huisnummer_from_adres

Make the house number human readable

=cut

requires qw(
    get_full_residence_address
    get_full_correspondence_address
    volledige_naam
);

sub get_volledig_huisnummer_from_adres {
    my ($self, $adres) = @_;

    return '' if (!$adres);
    return _join_huisnummer(
        $adres->huisnummer,
        $adres->huisletter,
        $adres->huisnummertoevoeging
    )
}

sub get_volledig_huisnummer_from_bedrijf {
    my ($self, $type) = @_;

    if (($type //'') eq 'residence') {
        return _join_huisnummer(
            $self->vestiging_huisnummer,
            $self->vestiging_huisletter,
            $self->vestiging_huisnummertoevoeging
        )
    }
    else {
        return _join_huisnummer(
            $self->correspondentie_huisnummer,
            $self->correspondentie_huisletter,
            $self->correspondentie_huisnummertoevoeging
        )
    }
}

sub _join_huisnummer {
    my ($number, $char, $postfix) = @_;

    # Can't have a house number without a number
    return '' unless defined $number;
    $number .= length($char // '')    ? " $char" : '';
    $number .= length($postfix // '') ? " - $postfix" : '';
    return $number;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
