package Zaaksysteem::CommunicationTab;
use Zaaksysteem::Moose;

with qw(
    Zaaksysteem::Moose::Role::Schema
);

=head1 NAME

Zaaksysteem::CommunicationTab - Communication tab model

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::CommunicationTab;

=cut

use BTTW::Tools::RandomData qw(generate_uuid_v4);

has _thread => (
    is       => 'ro',
    isa      => 'Defined',
    builder  => '_build_thread_rs',
    lazy     => 1,
    init_arg => undef,
);

has _thread_message_external => (
    is       => 'ro',
    isa      => 'Defined',
    builder  => '_build_thread_message_external_rs',
    lazy     => 1,
    init_arg => undef,
);

has _thread_message => (
    is       => 'ro',
    isa      => 'Defined',
    builder  => '_build_thread_message_rs',
    lazy     => 1,
    init_arg => undef,
);

has _thread_message_attachment => (
    is       => 'ro',
    isa      => 'Defined',
    builder  => '_build_thread_message_attachment_rs',
    lazy     => 1,
    init_arg => undef,
);

has _filestore => (
    is       => 'ro',
    isa      => 'Defined',
    builder  => '_build_filestore',
    lazy     => 1,
    init_arg => undef,
);

sub _build_thread_rs {
    my $self = shift;
    return $self->build_resultset('Thread');
}

sub _build_thread_message_external_rs {
    my $self = shift;
    return $self->build_resultset('ThreadMessageExternal');
}

sub _build_thread_message_rs {
    my $self = shift;
    return $self->build_resultset('ThreadMessage');
}

sub _build_thread_message_attachment_rs {
    my $self = shift;
    return $self->build_resultset('ThreadMessageAttachment');
}

sub _build_filestore {
    my $self = shift;
    return $self->build_resultset('Filestore');
}

define_profile create_thread => (
    required => {
        thread_type  => 'Str',
        message_type => 'Str',
        subject      => 'Str',
        slug         => 'Str',
        created_by   => 'Zaaksysteem::Object::Types::Subject',
    },
    optional => {
        created               => 'DateTime',
        unread_pip_count      => 'Int',
        unread_employee_count => 'Int',
        attachment_count      => 'Int',
        contact               => 'Zaaksysteem::Object::Types::Subject',
        case                  => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    defaults => { created => sub { DateTime->now() }, }
);

sub create_slug {
    my $self = shift;
    return substr(shift // '', 0, 180);
}

sub create_thread {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my %thread = (thread_type => $args->{thread_type},);
    foreach (qw(unread_pip_count unread_employee_count attachment_count)) {
        next unless exists $args->{$_};
        $thread{$_} = $args->{$_};
    }

    if (my $case = $args->{case}) {
        $thread{case_id} = $case->id;
    }

    if (my $contact = $args->{contact}) {
        $thread{contact_uuid}        = $contact->id;
        $thread{contact_displayname} = $contact->display_name;
    }

    my %cache = (
        message_type => $args->{message_type},
        slug         => $self->create_slug($args->{slug}),
        subject      => $args->{subject},

        created_name => $args->{created_by}->display_name,
        created => $self->schema->format_datetime_object($args->{created}),
    );

    $thread{last_message_cache} = \%cache;

    return $self->_thread->create(\%thread);
}

define_profile create_thread_message_external => (
    required => {
        type      => 'Str',
        content   => 'Str',
        subject   => 'Str',
        direction => 'Str',
    },
    optional => {
        source_file      => 'Defined',    # Filestore
        read_pip         => 'DateTime',
        read_employee    => 'DateTime',
        attachment_count => 'Int',
        participants     => 'Defined',
        subtype          => 'Str',
    },
);

sub create_thread_message_external {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    if (my $file = delete $args->{source_file}) {
        $args->{source_file_id} = $file->id;
    }

    foreach (qw(read_pip read_employee)) {
        next unless exists $args->{$_};
        $args->{$_} = $self->schema->format_datetime_object($args->{$_});
    }

    return $self->_thread_message_external->create($args);
}

define_profile create_thread_message => (
    required => {
        thread           => 'Defined',
        external_message => 'Defined',
        created_by       => 'Zaaksysteem::Object::Types::Subject'
    },
    optional => {
        uuid               => 'Defined',
        type               => 'Str',
        slug               => 'Str',
        external_reference => 'Str',
    },
    defaults => { uuid => sub { generate_uuid_v4() }, }
);

sub create_thread_message {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $thread     = $args->{thread};
    my $message    = $args->{external_message};
    my $created_by = $args->{created_by};

    my $slug = $self->create_slug($args->{slug} // $message->content);

    return $self->_thread_message->create({
        thread_id                  => $thread->id,
        thread_message_external_id => $message->id,
        type                       => $args->{type} // $thread->thread_type,
        message_slug               => $slug,

        created_by_uuid        => $created_by->id,
        created_by_displayname => $created_by->display_name,
        $args->{external_reference}
            ? (external_reference => $args->{external_reference})
            : (),

        uuid => $args->{uuid},
    });
}

define_profile add_attachment_to_message => (
    required => {
        filestore      => 'Defined',
        thread_message => 'Defined',
    },
    optional => { filename => 'Str', },
);

sub add_attachment_to_message {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return $self->_thread_message_attachment->create(
        {
            filestore_id => $args->{filestore}->id,
            filename => $args->{filename} // $args->{filestore}->original_name,
            thread_message_id => $args->{thread_message}->id,
        }
    );
}

define_profile add_message_source => (
    required => {
        ext            => 'Str',
        source         => 'Str',
        thread_message => 'Defined',
    },
);

sub add_message_source {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    # write message_source to tempfile
    my $temp_file = File::Temp->new();
    print $temp_file $args->{source};
    $temp_file->close();

    my $name = sprintf(
        "message-%s.%s",
        $args->{thread_message}->uuid,
        $args->{ext}
    );

    my $filestore = $self->schema->resultset('Filestore')->filestore_create(
        {
            original_name    => $name,
            file_path        => $temp_file->filename,
            ignore_extension => 1,
        }
    );

    my $external_thread_message = $args->{thread_message}->thread_message_external_id;
    $external_thread_message->update({source_file_id => $filestore->id});
    return $filestore;
}

__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
