package Zaaksysteem::Constants::SAML;

use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::SAML - Constants for SAML profiles

=head1 DESCRIPTION

This module defines constants for SAML interfaces which can be used in other
bits and pieces of Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::Constants::SAML qw(:SAML_TYPES);
    use Zaaksysteem::Constants::SAML qw(:all);

=cut

use Exporter qw[import];


our @EXPORT_OK = qw(
   SAML_TYPE_LOGIUS
   SAML_TYPE_KPN_LO
   SAML_TYPE_EIDAS
   SAML_TYPE_ADFS
   SAML_TYPE_SPOOF
   SAML_TYPE_MINIMAL
   SAML_AUTHORISATIONS
);

our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
    SAML_TYPES => [grep { $_ =~ /^SAML_TYPE_/ } @EXPORT_OK],
);

=head2 SAML_TYPE_LOGIUS

This the main (only?) provider for DigiD services.

=head2 SAML_TYPE_KPN_LO

This is our currently supported supplier for eHerkenning services.

=head2 SAML_TYPE_EIDAS

SAML dialect for eIDAS authentication.

=head2 SAML_TYPE_ADFS

The identifier for Microsoft's AD Federation Services

=head2 SAML_TYPE_MINIMAL

Minimal SAML instance, only given attribute is the login name as login identifier

=cut

use constant SAML_TYPE_LOGIUS  => 'digid';
use constant SAML_TYPE_KPN_LO  => 'eherkenning';
use constant SAML_TYPE_EIDAS   => 'eidas';
use constant SAML_TYPE_ADFS    => 'adfs';
use constant SAML_TYPE_SPOOF   => 'spoof';
use constant SAML_TYPE_MINIMAL => 'minimal';

=head2 SAML_AUTHORISATIONS

A mapping between all the SAML authorisations Zaaksysteem supports for a given
IdP.

=cut

use constant SAML_AUTHORISATIONS => [
    {
        value => '',
        label => 'Geen betrouwbaarheidsniveau',
    },
    {
        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified',
        label => 'eH:Niveau 1 (unspecified)'
    },
    {
        value =>
            'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
        label => 'eH:Niveau 2 / DigiD:Niveau 10 (PasswordProtectedTransport)'
    },
    {
        value =>
            'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorUnregistered',
        label => 'eH:Niveau 2+ (MobileTwoFactorUnregistered)'
    },
    {
        value =>
            'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract',
        label => 'eH:Niveau 3 / DigiD:Niveau 20 (MobileTwoFactorContract)',
    },
    {
        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:Smartcard',
        label => 'DigiD:Niveau 25 (Smartcard)'
    },
    {
        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI',
        label => 'eH:Niveau 4 / DigiD:Niveau 30 (SmartcardPKI)'
    },

    # https://afsprakenstelsel.etoegang.nl/display/as/Level+of+assurance
    {
        value => 'urn:etoegang:core:assurance-class:loa1',
        label => 'eIDAS Non existent / eToegang Level of Assurance 1'
    },
    {
        value => 'urn:etoegang:core:assurance-class:loa2',
        label => 'eIDAS Low / eToegang Level of Assurance 2'
    },
    {
        value => 'urn:etoegang:core:assurance-class:loa2plus',
        label => 'eIDAS Low / eToegang Level of Assurance 2+'
    },
    {
        value => 'urn:etoegang:core:assurance-class:loa3',
        label => 'eIDAS Substantial / eToegang Level of Assurance 3'
    },
    {
        value => 'urn:etoegang:core:assurance-class:loa4',
        label => 'eIDAS High / eToegang Level of Assurance 4'
    },
];

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
