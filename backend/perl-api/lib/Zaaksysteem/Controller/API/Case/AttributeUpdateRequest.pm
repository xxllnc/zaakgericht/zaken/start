package Zaaksysteem::Controller::API::Case::AttributeUpdateRequest;
use Moose;
use BTTW::Tools;

BEGIN { extends 'Catalyst::Controller::REST'; }

__PACKAGE__->config(
    'stash_key' => 'rest',
    'map'       => {
        'application/json'   => 'JSON',
        'text/x-json'        => 'JSON',

        # Explicitly disabled: this library uses XML::Simple which
        # has XML Entity Expansion vulnerabilities
        'text/xml'           => undef,
    },
    'compliance_mode' => 0,
);

# this sets up the other verbs
sub approve       : Local : ActionClass('REST') {}
sub reject        : Local : ActionClass('REST') {}
sub phaserequests : Local : ActionClass('REST') {}


sub phaserequests_GET {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($params->{case_id});
    $c->assert_any_zaak_permission('zaak_edit');

    my $phase_id = $params->{phase_id} or die "need phase_id";

    my $result = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerken->search_update_field_tasks({
        phase_id => $phase_id,
        case     => $c->stash->{zaak}
    });

    # response to the frontend
    $c->detach('response', [200, {result => [scalar keys %$result ] }]);
}


sub approve_POST {
    my ($self, $c) = @_;

    $c->forward('handle_update', ['approve']);
}


sub reject_POST {
    my ($self, $c) = @_;

    $c->forward('handle_update', ['reject']);
}


sub handle_update : Private {
    my ($self, $c, $action) = @_;

    my $params = $c->req->params;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($params->{case_id});
    $c->assert_any_zaak_permission('zaak_edit');

    # find the queue of changes to this field
    my $scheduled_jobs = $c->model('DB::ScheduledJobs')->search_update_field_tasks({
        case_id   => $params->{case_id},
        kenmerken => [$params->{bibliotheek_kenmerken_id}]
    });

    my @recent = $scheduled_jobs->only_most_recent_field_update;

    throw('api/case/attributeupdaterequest', "expected only one result")
        unless scalar @recent == 1;

    my ($most_recent) = @recent;

    # When fields are used multiple times in a casetype, unexpected results
    # may happen here. the cure would be identify the phase, which means
    # a database change at the field storage level. zaak_kenmerk must start
    # working with a zaaktype_kenmerken_id instead of a bibliotheek_kenmerken_id.
    # for now i'm operating in the oblivious assumption that a field will only
    # be used once per casetype.
    my $kenmerk = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerkens->search({
        bibliotheek_kenmerken_id => $most_recent->parameters->{bibliotheek_kenmerken_id}
    })->first;

    $c->assert_field_permission($kenmerk);

    # then invalidate that one and all the others
    $scheduled_jobs->set_deleted;

    $self->process_update($most_recent, $action);
    $self->log_request_handling($c, $kenmerk, $most_recent, $action);

    # response to the frontend
    $c->detach('response', [200, { result => [{
        type    => 'case/approve/success',
        message => 'Update request has been handled succesfully'
    }] }]);
}


=head2 process_update

citizen may send multiple updates, this process the last one,
ignore the rest

note: move to scheduled_jobs resultset?

=cut

sub process_update {
    my ($self, $most_recent, $action) = @_;

    # either approve or reject
    throw('api/case/attributeupdaterequest', "unsupported action")
        unless $action =~ m/^(approve|reject)$/;

    $most_recent->apply_roles->$action;

    return $most_recent;
}


sub log_request_handling {
    my ($self, $c, $kenmerk, $scheduled_job, $action) = @_;

    my $parameters = $scheduled_job->parameters;

    my $label = $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam;

    my $new_values = $kenmerk->bibliotheek_kenmerken_id->value_type =~ m|^bag| ?
        $c->model('Gegevens::Bag')->humanize($parameters->{value}) :
        $parameters->{value};

    $c->model('DB::Logging')->trigger(
        'case/update/piprequest', {
            component => 'zaak',
            zaak_id   => $parameters->{case_id},
            data => {
                action      => $action,
                kenmerk     => $label,
                new_values  => $new_values,
                toelichting => $parameters->{reason}
            }
        }
    );
}



sub response : Private {
    my ($self, $c, $status, $entity) = @_;

    $c->forward('/execute_post_request_actions');

    $c->response->status($status);
    $self->_set_entity($c, $entity);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 approve

TODO: Fix the POD

=cut

=head2 approve_POST

TODO: Fix the POD

=cut

=head2 handle_update

TODO: Fix the POD

=cut

=head2 log_request_handling

TODO: Fix the POD

=cut

=head2 phaserequests

TODO: Fix the POD

=cut

=head2 phaserequests_GET

TODO: Fix the POD

=cut

=head2 reject

TODO: Fix the POD

=cut

=head2 reject_POST

TODO: Fix the POD

=cut

=head2 response

TODO: Fix the POD

=cut

