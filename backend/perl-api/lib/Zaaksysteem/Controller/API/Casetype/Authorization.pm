package Zaaksysteem::Controller::API::Casetype::Authorization;

use Moose;

use JSON qw[decode_json];

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub get : Local : Args: ZAPI {
    my ($self, $c, $casetype_id) = @_;

    # redundant security check, the first line of defense must be Page.pm
    throw ('api/casetype/authorization', 'Access violation') unless $c->user_exists;

    $c->stash->{zapi} = [$self->get_authorizations($c, $casetype_id)];
}


sub post : Local : ZAPI {
    my ($self, $c, $casetype_id) = @_;

    # redundant security check, the first line of defense must be Page.pm
    throw ('api/casetype/authorization', 'Access violation')
        unless $c->user_exists;

    throw ('api/casetype/authorization/method_not_post', 'only POST requests allowed')
        unless $c->req->method eq 'POST';

    my $authorizations;
    eval {
        $authorizations = decode_json($c->req->params->{authorizations});
    };
    if ($@) {
        throw ('api/casetype/authorization', 'incorrect json format input: ' . $@);
    }

    # input validation
    throw ('api/casetype/authorization/incorrect_input', "incorrect authorizations, must be arrayref")
        unless $authorizations && ref $authorizations && ref $authorizations eq 'ARRAY';

    my $casetype = $c->load_session_casetype($casetype_id);

    # the whole index deal is legacy, deal with it :)
    my $index = 0;
    $casetype->{authorisaties} = { map {
        assert_valid_entry($_);
        $index += 1;
        $index => $_
    } @$authorizations };

    $c->stash->{zapi} = [$casetype->{authorisaties}];
}


sub assert_valid_entry {
    my ($entry) = @_;

    map {
        throw('api/casetype/authorization', sprintf(
            'Incorrect fields, each authorization must have ou_id, role_id, recht, confidential. got: %s',
            join(', ', keys %{ $entry })
        )) unless exists $entry->{$_}
    } qw/confidential ou_id role_id recht/;
}

sub get_authorizations {
    my ($self, $c, $casetype_id) = @_;

    my $casetype = $c->load_session_casetype($casetype_id);

    # Sometimes, ou_id and role_id come out as integers (on the JSON end),
    # which confuses the frontend JS code. By stringifying them here, they should
    # be treated as strings by the JSON encoder.
    map {{
        recht        => $_->{recht},
        ou_id        => "$_->{ou_id}",
        role_id      => "$_->{role_id}",
        confidential => $_->{confidential} + 0, # Force it to be an integer
    }} values %{ $casetype->{authorisaties} };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 assert_valid_entry

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 get_authorizations

TODO: Fix the POD

=cut

=head2 post

TODO: Fix the POD

=cut
