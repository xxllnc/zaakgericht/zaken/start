package Zaaksysteem::Controller::API::Mail;

use Moose;
use namespace::autoclean;

use Email::Simple;
use File::Basename;
use File::Temp;
use Mail::Track;
use Try::Tiny;
use List::Util qw(any);
use Zaaksysteem::Email::ZTT;

BEGIN { extends 'Zaaksysteem::Controller' }

with qw(MooseX::Log::Log4perl);

=head2 base

Base of the mail intake controller. Loads a posted "message" into memory.

=cut

sub base : Chained('/api/base') : PathPart('mail') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $message = $c->req->upload('message');

    if ($message) {
        $self->log->trace("Found email message in uploads");
        $c->stash->{message} = $message->slurp;
    }
    elsif ($c->req->params->{message}) {
        $self->log->trace("Found email message in params");
        $c->stash->{message} = $c->req->params->{message};
    }
}

=head2 intake

E-mail intake, accepts the MIME message as plain text and processes it
depending on the interfaces which are configured.

=cut

sub intake : Chained('base') : PathPart('intake') : DisableACL {
    my ($self, $c) = @_;

    my $message = $c->stash->{message};
    die('No message found') unless $message;

    my $mailfile = _save_message_as_file($c->model('DB::File'), $message);
    my $mailfile_id = $mailfile->id;

    my $outgoing = $c->model('DB::Interface')->search_active({module => 'emailconfiguration'})->first;

    my $is_reply = 0;

    if ($outgoing) {
        $c->log->trace("Processing mail using the outgoing email configuration");
        $c->stash->{interface} = $outgoing;
        my $config = $outgoing->get_interface_config;
        my $prefix = $config->{subject};

        my $regexp = qr/\[\Q$prefix\E (?:(?:[0-9]+ )?[a-zA-Z0-9]+|[1-9][0-9]*-[0-9a-f]{6})\]/i;

        my $email = Email::Simple->new($message);

        if ($email->header('Subject') =~ $regexp) {
            $is_reply = 1;
        }
    }

    my $interfaces = $c->model('DB::Interface')->search_active({module => 'emailintake'});
    if (!$is_reply && $interfaces->count > 0) {
        my $mt = Mail::Track->new();
        my $msg = $mt->parse($message);

        my @mail_addresses = map { lc($_->address) } Email::Address->parse($msg->to),
            Email::Address->parse($msg->cc);

        my $interface;
        while ($interface = $interfaces->next) {
            my $api_user = lc($interface->get_interface_config->{api_user});
            if (any { $api_user eq $_ } @mail_addresses) {
                $c->stash->{interface} = $interface;
                return $self->_process_mail($c, $mailfile_id, $msg->subject);
            }
        }

        my $logmsg = "No e-mail interface found for e-mail addresses: "
            . join(", ", @mail_addresses);
        $self->log->info($logmsg);
    }

    my $filestore_uuid = $mailfile->filestore->uuid;

    # Delete the "file" record, Python doesn't want or need it.
    $mailfile->delete;

    $c->model('Event')->publish_incoming_email_event(
        $c->model('DB')->schema,
        $filestore_uuid
    );

    $c->res->content_type('application/json');
    $c->res->body("{}");
    return 1;
}

sub generate_preview : Chained('/api/base') : PathPart('mail/preview') : Args(0) {
    my ($self, $c) = @_;

    my %params = %{$c->req->params};
    my $case;
    if (my $id = delete $params{case_id}) {
        $case = $c->model('Zaken')->find($id);
    }
    elsif (my $uuid = delete $params{case_uuid}) {
        $case = $c->model('Zaken')->search({'me.uuid' => $uuid})->first;
    }

    if ($case) {
        # Solve this via the lookup?
        #$case->assert_write_permission($c->user);
        return if $case->is_afgehandeld;
    }

    my $mailer = Zaaksysteem::Email::ZTT->new(
        $case ? ( case => $case ) : (),
        betrokkene_model => $c->model('Betrokkene'),
    );

    my $preview = $mailer->generate_preview(%params);
    $c->stash->{ json } = $preview;
    $c->detach('Zaaksysteem::View::JSON');
    return;
}

sub _process_mail {
    my ($self, $c, $mailfile, $subject) = @_;

    # Mails are now transactionable, so no need for postfix to queue the mail.
    # It will only generate tons of new transactions for the exact same e-mail.
    try {
        $c->stash->{interface}->process_trigger(
            'process_mail',
            { message => $mailfile, subject => $subject },
            { zaak => $c->model('Zaak') }
        );
    }
    catch {
        $self->log->error("Error processing e-mail due to error: $_");
    };

    $c->res->body('ok');
    return 1;
}

=head2 intake_process

Old skool e-mail intake process. Not based on any interface. Just grabs the attachments and adds them to the document queue.

=cut

sub intake_process : Local {
    my ($self, $c) = @_;

    die('No message found') unless $c->req->params->{message};
    eval {
        $c->forward('/api/mail/intake/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
        $c->res->body($@);
    }
    else {
        $c->res->body('ok');
    }
}

=head2 input

Deprecated function. Have a look at L<http://wiki.zaaksysteem.nl> for more information about mail handling by Zaaksysteem.

=cut

sub input : Local {
    my ($self, $c) = @_;
    $c->res->body('Unsupported mail handling, please have a look at http://wiki.zaaksysteem.nl for supported mail handling.');
}

sub _save_message_as_file {
    my $rs  = shift;
    my $msg = shift;
    my $fh = File::Temp->new(UNLINK => 1, SUFFIX => '.eml');
    my $path = $fh->filename;
    print $fh $msg;
    close($fh);
    my ($filename, undef, $ext) = fileparse($path, '\.[^.]*');

    my $admin = $rs->result_source->schema->resultset('Subject')->search(
        {
            username => 'admin',
            subject_type => 'employee',
        }
    )->first;

    my $mailfile = $rs->file_create({
        name       => $filename . $ext,
        file_path  => $path,
        ignore_extension => 1,
        db_params  => {
            created_by => "betrokkene-medewerker-" . $admin->id,
            created_by => 'admin',
            queue      => 0,
        },
    });
    unlink($path);
    return $mailfile;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
