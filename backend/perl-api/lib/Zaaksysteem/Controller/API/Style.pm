package Zaaksysteem::Controller::API::Style;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

my @template_names = ("mor", "meeting");

define_profile base => (
    required => ["accent_color"],
    optional => ["header_loader_color"],
    constraint_methods => {
        accent_color        => qr/^#[0-9a-fA-F]{6}$/,
        header_loader_color => qr/^#[0-9a-fA-F]{6}$/,
    },
);

sub base : Chained('/') : PathPart('api/style') : Args(1) {
    my ($self, $c, $template_name) = @_;

    my $params = assert_profile($c->req->params)->valid;

    if (!grep { $template_name eq $_ } @template_names) {
        throw(
            "style/invalid_template",
            "Invalid template name specified.",
        )
    }

    $c->stash->{accent_color} = $params->{accent_color};
    $c->stash->{header_loader_color} = 
        $params->{header_loader_color} // '#000000';

    $c->res->content_type('text/css');
    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = "style/${template_name}.css"
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

