package Zaaksysteem::Controller::API::v1::App;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

### WARNING: Every action in this controller is ACCESS: Public, no logged in user required. Handle (custom)
### authentication in subroutines.

=head1 NAME

Zaaksysteem::Controller::API::V1::App - Custom App configuration profiles

=head1 DESCRIPTION

This is the controller API class for C<api/v1/app>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::App>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Sysin::Interface>

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ResultSet;

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('app');
    $self->add_api_context_permission('extern', 'public_access');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/app> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('app') : CaptureArgs(0) { }

=head2 instance_base

Reserves the C</api/v1/app/[APP_NAME]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $appname) = @_;

    unless ($appname =~ m/^[a-z0-9\-\_]+$/) {
        throw('api/v1/app/invalide_appname', 'Invalid appname given');
    }

    ### Check for valid interface_id and existing of interface
    my $apps                = $c->model('DB::Interface')->search_active({ module => $appname })->search_rs;
    my $app                 = $apps->first;


    if (!$app) {
        throw('api/v1/app/not_found', sprintf(
            'No application found by given name: "%s"',
            $appname
        ));
    }

    unless (grep { $_ eq 'app' } @{ $app->module_object->module_type }) {
        ### Not good, we do not want to expose more information than needed
        throw(
            'api/v1/app/not_an_app_interface',
            'Not a valid app name given'
        );
    }

    ### When multiple configurations are possible, return a set
    if ($app->module_object->allow_multiple_configurations) {
        $c->stash->{app_interface} = Zaaksysteem::API::v1::ResultSet->new(
            iterator => $apps
        )->init_paging($c->request);
    } else {
        $c->stash->{app_interface} = $app;
    }
}

=head2 get

=head3 URL Path

C</api/v1/app/[APP_NAME]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{app_interface};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
