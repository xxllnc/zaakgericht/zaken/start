package Zaaksysteem::Controller::API::v1::EventLog;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::EventLog - APIv1 controller for event logs

=head1 DESCRIPTION

Show the event log for the whole of Zaaksysteem, or just for an object or view.

=cut

use Archive::Tar::Stream;
use BTTW::Tools;
use Zaaksysteem::API::v1::ResultSet;
use Zaaksysteem::Tie::CallingHandle;
use Zaaksysteem::EventLog::Model;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/eventlog> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('eventlog') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission([qw(admin useradmin)]);
}

=head2 list

List all the events found in Zaaksysteem. This is basicaly the "Logboek"
in ZS.  When used with ES Query params you can look for specific entries:

=head3 URL Path

C</api/v1/eventlog>

=head3 Parameters

=over 4

=item match:timeline=UUID

Get the timeline related to object, eg the timeline of a user. Which
entries were made on its behalf and which changes in which cases were
made.

=item match:object=UUID

Get the timeline of the object. Only show items related to the actual
object itself.

=item match:case_id=ID

Get the timeline of the case with the specified id

=back

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    my $rs = $c->model('EventLog')->search($c->req->params);

    my $set = Zaaksysteem::API::v1::ResultSet->new(iterator => $rs);

    $c->stash->{set} = $set;

    $self->list_set($c);
}

=head2 download

Similar to the list call, but instead of returning JSON objects you get
to download all the events. When used with ES Query params you can look
for specific entries:

=head3 URL Path

C</api/v1/eventlog/download>

=head3 Parameters

=over 4

=item match:timeline=UUID

Get the timeline related to object, eg the timeline of a user. Which
entries were made on its behalf and which changes in which cases were
made.

=item match:object=UUID

Get the timeline of the object. Only show items related to the actual
object itself.

=item match:case_id=ID

Get the timeline of the case with the specified id.

If both this and "timeline" are specified, the filters will be ANDed together.

=back

=cut

sub download : Chained('base') : PathPart('download') : Args(0) : RO {
    my ($self, $c) = @_;

    my $filename = sprintf("zs-%s.tar", DateTime->now()->iso8601,);

    $c->res->content_type('application/x-tar');
    $c->res->header('Content-Disposition',
        qq[attachment; filename="$filename"]);

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); },);

    my $params = $c->req->params();

    $c->model('EventLog')->export_with_fh($handle, $params);

    $c->stash->{file_download} = 1;
}

sub download_via_export_queue : Chained('base') : PathPart('export') : Args(0)
{
    my ($self, $c) = @_;

    my $params = $c->req->params();

    my $queue = $c->model('Queue');
    my $item = $queue->create_item(
        {
            label   => "Export log book",
            type    => "logbook_export",
            subject => $c->user,

            # Make sure the user ID is there, because the ignore existing will
            # otherwise prevent two users to export the logbook
            data => {
                user_id       => $c->user->id,
                export_params => $params,
            },
            metadata => {
                request_id => $c->get_zs_session_id,
                hostname   => $c->get_server_hostname
            },
        }
    );
    $queue->queue_item($item) if $item;

    $c->stash->{result} = { success => \1};

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
