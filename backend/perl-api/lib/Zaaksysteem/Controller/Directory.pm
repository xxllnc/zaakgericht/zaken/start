package Zaaksysteem::Controller::Directory;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Directory - Frontend library for the Zaaksysteem
file structure.

=cut

=head2 create

Create a new directory.

=head3 Arguments

=over

=item name [required]

=item case_id [required]

=back

=head3 Returns

A JSON structure containing file properties.

=cut

sub create : JSON : Local {
    my ($self, $c) = @_;

    my $result =$c->model('DB::Directory')->directory_create({
        %{$c->req->params}
    });
    $c->stash->{json} = $result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 update

Create a new directory.

=head3 Arguments

=over

=item name [required]

=item directory_id [required]

Set to 0 for no directory.

=back

=head3 Returns

A JSON structure containing the directory properties.

=cut

sub update : JSON : Local {
    my ($self, $c) = @_;

    my %params = %{$c->req->params()};

    my $dir_id = $params{directory_id};
    my ($directory) = $c->model('DB::Directory')->search({
        id => $dir_id,
    });
    if (!$directory) {
        die "Directory with ID $dir_id not found";
    }
    my $result = $directory->update_properties({
        %params,
    });

    $c->stash->{json} = $result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 search_dir

Retrieve a directory and its properties, based on a directory_id.

Can yield multiple results.

=head3 Arguments

=over

=item * the case id to search for

=back

=head3 Returns

A JSON structure containing one or more results detailing the directory
properties.

=cut

sub search_dir : JSON : Chained('/') : PathPart('directory/search/directory_id') : CaptureArgs(1) {
    my ($self, $c, $search_value) = @_;

    my @result = $c->model('DB::Directory')->search({id => $search_value});

    $c->stash->{json} = \@result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 search_dir_by_caseid

Retrieve a directory and its properties, based on a case_id.

Can yield multiple results.

=head3 Arguments

=over

=item * the case id to search for

=back

=head3 Returns

A JSON structure containing one or more results detailing the directory
properties.

=cut

sub search_dir_by_caseid : JSON : Chained('/') : PathPart('directory/search/case_id') : CaptureArgs(1) {
    my ($self, $c, $search_value) = @_;

    my @result = $c->model('DB::Directory')->search({case_id => $search_value});

    $c->stash->{json} = \@result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 delete

Delete a directory.

=head3 Arguments

=over

=item directory_id [required]

=back

=head3 Returns

True upon succes.

=cut

sub delete : JSON : Local {
    my ($self, $c) = @_;

    my $result = $c->model('DB::Directory')->find(
        $c->req->param('directory_id')
    );

    $result->delete_tree($c->user->as_object);

    $c->stash->{json} = { success => 1 };

    $c->forward('Zaaksysteem::View::JSON');
}

=head2 help

Returns a HTMLified version of the POD in this library.

=cut

sub help : Local {
    my ($self, $c) = @_;

    require Pod::Simple::HTML;
    my $p = Pod::Simple::HTML->new;
    $p->output_string(\$c->stash->{pod_output});
    $p->parse_file('lib/Zaaksysteem/Controller/Directory.pm');
    $c->stash->{template} = 'pod.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

