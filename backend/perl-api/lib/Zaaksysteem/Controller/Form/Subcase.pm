package Zaaksysteem::Controller::Form::Subcase;
use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' };

=head1 NAME

Zaaksysteem::Controller::Form::Subcase - Create subcases filled with data from another case

=head1 DESCRIPTION

Endpoint to create sub cases with prefilled data from an originating case

=head1 ACTIONS

=head2 subcase_form_base

=cut


sub subcase_form_base : Chained('/') : PathPart('form/subcase') : CaptureArgs(0) {
    my ($self, $c) = @_;

    if (!$c->session->{pip}) {
        $c->log_detach(
            error          => "Non-PIP user requesting PIP login form",
            human_readable => "U bent niet ingelogd op de Persoonlijke Internet Pagina",
        );
    }

    my $betrokkene_id = $c->session->{pip}{ztc_aanvrager} //
        $c->session->{_zaak_create}{ztc_aanvrager_id};

    $c->stash->{betrokkene_id} = $betrokkene_id;
}

=head2 create_case_by_casetype

=head3 URI

    C</form/subcase/CASE_ID/ID/NAME>

=head3 DESCRIPTION

Allows direction creation of a subcase via a clickable link for PIP
users.  It redirects to the case creation form with prefilled data, the
casetype etc. If the casetype is not I<extern> or I<internextern> you
cannot create a case via this endpoint.

=cut

sub assert_case {
    my ($self, $c, $id) = @_;

    if ($id !~ /^[0-9]+$/) {
        $c->log_detach(
            error          => "Case ID is not a number",
            human_readable => "Ongeldig zaaknummer opgegeven",
        );
    }
    my $case = $c->model('DB::Zaak')->find($id);
    return $case if $case && !$case->deleted;

    $c->log_detach(
        error          => "Case ID is not found",
        human_readable => "Ongeldig zaaknummer opgegeven",
    );
}

sub create_case_by_casetype : Chained('subcase_form_base'): PathPart('') : Args(4) {
    my ($self, $c, $case_id, $casetype_id, $btype, $name) = @_;

    my $case = $self->assert_case($c, $case_id);

    my $model = $c->model('Subcase');
    my $relation;
    try {
        my $rs = $model->get_subcase_from_case($case, $casetype_id);
        if ($relation = $rs->first) {
            $model->assert_casetype($casetype_id, $btype);
        }
        else {
            # This should only go wrong when people mold and fold the
            # URI in the PIP
            throw("case/subcase/casetype/invalid",
                "Unable to create case with requested casetype_id");
        }
    }
    catch {
        $c->log_detach(
            error => "$_",
            human_readable =>
                "Het opgegeven zaaktype is niet geschikt voor het aanmaken vanuit deze bron"
        );
    };

    my $concept_model = $c->model("ConceptCase");
    my $betrokkene    = $c->stash->{betrokkene_id};

    my $concept = $concept_model->find_concept_case(
        $casetype_id,
        $betrokkene,
    );

    $c->session->{subcase}{type} = $relation->get_column('relatie_type');
    $c->session->{subcase}{id}   = $case->id;

    if ($concept) {
        my $data = $concept->zaak_gegevens();
        $data->{zaak_relatie_id} = $case->id;
        $data->{type_zaak}       = $relation->get_column('relatie_type');
        $concept_model->submit_concept_case($data);
    }
    else {
        my $data = $model->duplicate_case_data(
            case            => $case,
            relation        => $relation,
            betrokkene_id   => $betrokkene,
            betrokkene_type => $btype,
        );
        $concept_model->submit_concept_case($data);
    }

    $c->res->redirect(
        $c->uri_for(sprintf('/aanvragen/%d/%s/afronden', $casetype_id, $btype))
    );
    $c->detach;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
