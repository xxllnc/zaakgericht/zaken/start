package Zaaksysteem::Controller::Zaak::Checklist;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Zaak::Checklist - Catalyst Controller

=head1 DESCRIPTION

This controller handles all API interacties for checklists on cases

=head1 METHODS

=cut

sub base : JSON : Chained('/zaak/base') : PathPart('checklist') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $checklists = $c->stash->{ zaak }->checklists;

    $c->stash->{ checklists } = $checklists;
}

=head2 single_checklist (I<action>, I<internal>)

Internal chaining action that abstracts the retrieval of the checklist being
accessed. Uses the C<milestone> parameter to get the checklist, or defaults
to the checklist of the current phase of the case. If the case is already
closed and no C<milestone> parameter is give, it will generate an error.

=head3 Parameters

=over 4

=item milestone

Sequence number for the phase in which the checklist exists.

=back

=cut

define_profile single_checklist => (
    optional => [ qw[milestone] ],
    constraint_methods => {
        milestone => qr[^\d+$]
    }
);

sub single_checklist : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    my $fase;

    if($c->req->param('milestone')) {
        $fase = $c->stash->{ zaak }->fasen->search({ status => $c->req->param('milestone') })->first;
    } else {
        $fase = $c->stash->{ zaak }->volgende_fase;
    }

    unless($fase) {
        throw('checklist/invalid_phase', "No milestone specified and no milestone could be defaulted because the case is closed.");
    }

    my $checklist = $c->stash->{ checklists }->search({ case_milestone => $fase->status })->first;

    unless($checklist) {
        throw('checklist/not_found', "No checklist could be found, this should never happen. Has the checklist-migration script been run? Otherwise this is probably a fault during case/create, which must've died before generating the default checklists");
    }

    $c->stash->{ checklist } = $checklist;
}

=head2 view (I<action>)

This action hydrates the specified checklist in a JSON view.

=head3 URL

B</zaak/I<case_id>/checklist/view>

=head3 Parameters

=over 4

=item milestone

I<inherited from 'single_checklist' action>

=back

=cut

sub view : Chained('single_checklist') : PathPart('view') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ json } = $c->stash->{ checklist }->checklist_items({ }, { order_by => 'sequence' });
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 all (I<action>)

This action hydrates all checklists associated with the case.

=head3 URL

B</zaak/I<case_id>/checklist/all>

=head3 Example response body

    {
       "next" : null,
       "comment" : "This is a single DBIx::Class::Row, it has no paging.",
       "prev" : null,
       "at" : null,
       "num_rows" : 1,
       "rows" : null,
       "result" : [
          {
             "milestone" : 2,
             "id" : 1829,
             "items" : [
                {
                   "checked" : false,
                   "user_defined" : false,
                   "label" : "Dingess",
                   "id" : 667
                },
                {
                   "checked" : false,
                   "user_defined" : false,
                   "label" : "Dinges",
                   "id" : 666
                },
                {
                   "checked" : false,
                   "user_defined" : true,
                   "label" : "Herptesties",
                   "id" : 669
                }
             ]
          }
       ]
    }

=cut

sub all : Chained('base') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ json } = $c->stash->{ checklists };
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 checklist_writable (I<action>, I<internal>)

This internal (no public URL) action verifies that the case is, in fact, not closed.

=cut

sub checklist_writable : Chained('single_checklist') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    if($c->stash->{ zaak }->is_afgehandeld) {
        throw('checklist/not_writable', "Case is closed, checklists are read-only now, can't update.");
    }
}

=head2 update (I<action>)

This action allows for updating items on a checklist with a specific state.

=head3 URL

B</zaak/I<case_id>/checklist/update>

=head3 Parameters

=over 4

=item data

This JSON parameter should contain a hash with keys being C<checklist_item>
keys associated with true/false values for their state.

    {
        "data": {
            "123": true,
            "321": false
        }
    }

=item milestone

I<inherited from 'single_checklist' action>

=back

=cut

define_profile update => (
    required => [qw[data]]
);

sub update : Chained('checklist_writable') : PathPart('update') : Args(0) : Method(POST) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    my $checklist = $c->stash->{ checklist };

    my $data = $c->req->param('data') // {};

    for my $key (keys %{ $data }) {
        my $item = $checklist->checklist_items->find($key);

        $item->state($c->req->param('data')->{ $key });
        $item->update;

        $c->model('DB::Logging')->trigger('case/checklist/item/update', {
            component => 'zaak',
            zaak_id => $c->stash->{ zaak }->id,
            data => {
                case_id => $c->stash->{ zaak }->id,
                checklist_id => $checklist->id,
                checklist_item_name => $item->label,
                checklist_item_value => $item->state ? 'afgevinkt' : 'niet afgevinkt'
            }
        });
    }

    $c->stash->{ json } = $checklist->checklist_items;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 add_item (I<action>)

This action allows the user to add checklist items to a checklist.

=head3 URL

B</zaak/I<case_id>/checklist/add_item>

=head3 Parameters

=over 4

=item label

This parameter defines the label for the item to be added to the checklist.

    {
        "label": "Check this to complete the milestone"
    }

=item milestone

I<inherited from 'single_checklist' action>

=back

=cut

define_profile add_item => (
    required => [ qw[label] ]
);

sub add_item : Chained('checklist_writable') : PathPart('add_item') : Args(0) : Method(POST) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    my $checklist = $c->stash->{ checklist };

    my $item = $checklist->checklist_items->new_result({
        label => $c->req->param('label'),
        user_defined => 1
    });

    $item->insert;

    $c->model('DB::Logging')->trigger('case/checklist/item/create', {
        component => 'zaak',
        zaak_id => $c->stash->{ zaak }->id,
        data => {
            checklist_id => $checklist->id,
            checklist_item_id => $item->id,
            case_id => $c->stash->{ zaak }->id,
            label => $item->label
        }
    });

    $c->stash->{ json } = $item;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 remove_item (I<action>)

A call that allows the removal of user defined checklist items.
Will generate an error if an attempt is made at deleting an item
that was not orignally defined by a user.

=head3 URL

B</zaak/I<case_id>/checklist/remove_item>

=head3 Parameters

=over 4

=item item_id

This paremeter specifies which item is to be deleted from the checklist.

    {
        "item_id": 123
    }

=item milestone

I<inherited from 'single_checklist' action>

=back

=cut

define_profile remove_item => (
    required => [ qw[item_id] ],
    constraint_methods => {
        item_id => qr[^\d+$]
    }
);

sub remove_item : Chained('checklist_writable') : PathPart('remove_item') : Args(0) : Method(POST) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    my $checklist = $c->stash->{ checklist };
    my $item = $checklist->checklist_items->find($c->req->param('item_id'));

    unless($item) {
        throw('checklist/item/not_found', "No such checklist could be found, incorrect checklist_item_id supplied?");
    }

    unless($item->user_defined) {
        throw('checklist/item/system_defined', "Checklist item is system-defined, won't delete.");
    }

    $item->delete;

    $c->model('DB::Logging')->trigger('case/checklist/item/remove', {
        component => 'zaak',
        zaak_id => $c->stash->{ zaak }->id,
        data => {
            checklist_item_id => $item->id,
            checklist_id => $checklist->id,
            case_id => $c->stash->{ zaak }->id,
            label => $item->label
        }
    });

    $c->stash->{ json } = $checklist->checklist_items;
    $c->forward('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

