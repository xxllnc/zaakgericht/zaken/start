package Zaaksysteem::Controller::Zaak::Mail;

use Moose;

use Email::MIME;
use Email::Valid;
use IO::All;

use Zaaksysteem::ZTT;
use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
/;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 send_automatic_create_notifications

Send notifications that are on automatic send or
that are selected using the 'schedule_mail' rule.

Notifications are identified by their index in the phase.
It would be preferably to identify them by a systemwide unique id,
but this will require tricky changes in ZaaktypeBeheer rule management.

So first a list with the notifications and their settings for this phase is obtained:

Index   |   Notification  | Automatic   |  Schedule_by_rule  |  Action
1               Object         1                   0            Send now
2               Object         1                   1            Schedule
3               Object         0                   1            Schedule
4               Object         0                   0            -
..              ..              ..

Then the rules for the phase are executed and the results are added to this table
Then based on the settings for 'Automatic' and 'Schedule_by_rule' are compared and a
decision is made. A rule takes precedence over Automatic behaviour. It is not recommended
to use these settings simultaneously, however it will take considerable work in the
Zaaktypebeheer interface to inform the user about this pitfall.

=cut

sub send_automatic_create_notifications : Private {
    my ($self, $c) = @_;

    my $zaak = $c->stash->{zaak};

    my $registration_phase = $zaak->zaaktype_node_id->zaaktype_statussen->search({
        status => 1
    })->first;

    my $notifications = $registration_phase->notifications();

    my $scheduled_mails = $c->stash->{zaak}->reschedule_phase_notifications({
        status          => 1,
        notifications   => $notifications,
    });

    my $rules = $zaak->execute_rules({ status => 1 });

    my %transition_notifications;

    if(exists $rules->{ schedule_mail }) {
        for my $index (keys %{ $rules->{ schedule_mail } }) {
            next unless($rules->{ schedule_mail }{ $index }{ datum_bibliotheek_kenmerken_id } eq 'phase_transition');

            $transition_notifications{ $index }++;
        }
    }

    my $notification_index = 0;

    # this query has already been ran above
    $notifications->reset();

    while(my $notification = $notifications->next()) {
        $notification_index++;

        next if $scheduled_mails->{$notification_index};
        if($notification->automatic || $transition_notifications{ $notification_index }) {

            my $mailer = Zaaksysteem::Backend::Email::Case->new(
                case => $c->stash->{zaak}
            );

            my $recipient = $zaak->notification_recipient({
                recipient_type  => $notification->rcpt,
                behandelaar     => $notification->behandelaar,
                email           => $notification->email,
            });

            my $body = $mailer->send_case_notification({
                recipient    => $recipient,
                notification => $notification->bibliotheek_notificaties_id,
            }) if $recipient;
        }
    }
}

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaak::Mail in Zaak::Mail.');
}

sub preview : Private {
    my ($self, $c, $zaak) = @_;

    $c->stash->{mailconcept} = $c->view('TT')->render(
        $c,
        'tpl/zaak_v1/nl_NL/email/status_next.tt',
        {
            nowrapper => 1,
            %{ $c->stash }
        }
    );
}

sub registratie : Private {
    my ($self, $c) = @_;

    eval {
        $c->stash->{mailconcept} = $c->view('TT')->render(
            $c,
            'tpl/zaak_v1/nl_NL/email/registratie.tt',
            {
                nowrapper => 1,
                %{ $c->stash }
            }
        );

        $c->forward('aanvrager', [
            'Uw zaak is geregistreerd bij de ' .  $c->config->{gemeente}->{naam_lang}
        ]);
    };

    if ($@) {
        $c->log->debug('Something went wrong by sending this email: '
            . $@
        );
    }
}



sub aanvrager : Local {
    my ( $self, $c, $subject ) = @_;

    my $body    = $c->stash->{mailconcept} || $c->req->params->{'mailconcept'};
    if (!$subject) {
        $subject = $c->req->params->{'mailsubject'} || 'Status gewijzigd';
    }

    return unless (
        $body &&
        $c->stash->{zaak} &&
        $c->stash->{zaak}->kenmerk->aanvrager_email
    );

    eval {

        $body = $self->parse_special_vars($c, $body);

        $c->stash->{email} = {
            from    => $c->config->{gemeente}->{zaak_email},
            to      => $c->stash->{zaak}->kenmerk->aanvrager_email,
            subject => '[' . uc( $c->config->{gemeente}->{naam_kort}) . ' Zaak #' . $c->stash->{zaak}->nr . '] ' . $subject ,
            body    => $body,
        };

        $c->forward( $c->view('Email') );
    };

    if (!$@) {
        # Record email
        my %add_args = (
            zaakstatus   => $c->stash->{'zaak'}->kenmerk->status,
            filename     => $c->stash->{zaak}->kenmerk->aanvrager_email,
            documenttype => 'mail',
            category     => '',
            subject      => '[' . uc( $c->config->{gemeente}->{naam_kort}) . ' Zaak #' . $c->stash->{zaak}->nr . '] ' . $subject,
            message      => $body,
            rcpt         => $c->stash->{zaak}->kenmerk->aanvrager_email,
        );

        $c->stash->{zaak}->documents->add(
            \%add_args
        )
   } else {
       $c->log->debug(
           'Something went wrong sending email: ' . $@
        );
    }
}

sub parse_special_vars {
    my ( $self, $c, $body ) = @_;

    # Why this?
    if ((ref($body) eq 'ARRAY' && !@$body) || !$body) {
        return $body;
    }

    # Make sure to add the magic_strings hash for PIP feedback. (Amongst things)
    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context($c->stash->{ magic_strings }) if exists $c->stash->{ magic_strings };
    $ztt->add_context($c->stash->{ zaak });

    return $ztt->process_template($body)->string;
}


sub document : Private {
    my ( $self, $c, $body, $onderwerp ) = @_;

    $c->stash->{email} = {
        from    => $c->config->{gemeente}->{zaak_email},
        to      => $self->parse_special_vars($c, $c->stash->{rcpt}),
        subject => $onderwerp,
        body    => $body,
    };

    $c->forward( $c->view('Email') );
}

sub set_error_missing {
    my ($self, $dv, $field) = @_;

    $dv->{success} = 0;
    $dv->{invalid} //= [];
    push @{ $dv->{missing} }, $field;
}

sub set_error_invalid {
    my ($self, $dv, $field) = @_;

    $dv->{success} = 0;
    $dv->{invalid} //= [];
    push @{ $dv->{invalid} }, $field;
}

=head2 send_email

From zaakbehandeling, send email

=cut

sub send_email : Chained('/zaak/base') : PathPart('send_email') : Args() {
    my ($self, $c, $zaaktype_notificatie_id) = @_;

    my $params = $c->req->params;

    $c->stash->{nowrapper} = 1;

    for my $destination (qw(notificatie_email notificatie_cc notificatie_bcc)) {
        if (defined $params->{ $destination }) {
            $params->{ $destination } = $self->parse_special_vars($c, $params->{ $destination });
        }
    }

    if($params->{do_validation}) {
        my $dv = {
            success => 1,
            msgs    => {
                notificatie_email => 'Geef een geldig e-mailadres of een magic string die daarnaar verwijst'
            }
        };

        if($params->{recipient_type} eq 'overig') {

            if($params->{notificatie_email}) {
                eval {
                    my @raw_emails = split(/\s*[,;]\s*/, $params->{notificatie_email});

                    push @raw_emails, split(/\s*[,;]\s*/, $params->{notificatie_cc})
                        if (defined $params->{notificatie_cc});
                    push @raw_emails, split(/\s*[,;]\s*/, $params->{notificatie_bcc})
                        if (defined $params->{notificatie_bcc});

                    my @emails = grep { length($_) } @raw_emails;

                    foreach my $address (@emails) {
                        die "invalid e-mail address: $address" unless Email::Valid->address($address);
                    }
                };

                if($@) {
                    if ($@ =~ /body/i) {
                        $self->set_error_invalid($dv, 'notificatie_body');
                        $dv->{msgs}->{notificatie_body} = 'E-mailsjabloon is leeg';
                    }

                    if ($@ =~ /subject/i) {
                        $self->set_error_invalid($dv, 'notificatie_subject');
                    }

                    if ($@ =~ /invalid e-mail/) {
                        $self->set_error_invalid($dv, 'notificatie_email');
                    }

                    $c->log->warn('Error e-mail:' . $@);
                }
            } else {
                $self->set_error_missing($dv, 'notificatie_email');
            }

        } elsif($params->{recipient_type} eq 'medewerker') {
            $self->set_error_missing($dv, 'medewerker_betrokkene_id') unless $params->{medewerker_betrokkene_id};
        }
        elsif($params->{recipient_type} eq 'betrokkene') {
            $self->set_error_missing($dv, 'betrokkene_role') unless $params->{betrokkene_role};
        }

        if($params->{mailtype} eq 'specific_mail') {
            $self->set_error_missing($dv, 'notificatie_onderwerp') unless $params->{notificatie_onderwerp};
            $self->set_error_missing($dv, 'notificatie_bericht')   unless $params->{notificatie_bericht};
        }
        $c->zcvalidate($dv);
        $c->detach();

    } elsif($params->{update}) {

        $c->forward('send_validated_email');

    } else {

        if($zaaktype_notificatie_id) {
            $c->stash->{zaaktype_notificatie} = $c->model('DB::ZaaktypeNotificatie')->find($zaaktype_notificatie_id);
            $c->stash->{template}  = 'zaak/send_casetype_email.tt';
        } else {
            $c->stash->{template}  = 'zaak/send_email.tt';
        }
    }

}


=head2 send_validated_email

Assuming a validated request (hence 'validated'), do the actual
preparation and sending. This handles a manual mail from the plus
button on the case view.

=cut

sub send_validated_email : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $mailer = Zaaksysteem::Backend::Email::Case->new(
        case => $c->stash->{zaak}
    );

    my $recipient = $c->stash->{zaak}->notification_recipient({
        recipient_type  => $params->{recipient_type},
        behandelaar     => $params->{medewerker_betrokkene_id},
        email           => $params->{notificatie_email},
        betrokkene_role => $params->{betrokkene_role},
    });

    my $body;

    $params->{log_error} //= 1;
    my $error;

    if(
        $params->{zaaktype_notificatie_id} &&
        $params->{mailtype} eq 'bibliotheek_notificatie'
    ) {
        my $zaaktype_notificatie = $c->model('DB::ZaaktypeNotificatie')->find($params->{zaaktype_notificatie_id});

        my @zaaktype_kenmerken_ids = map { $_->{case_document_ids } }
            grep { $_->{selected} } @{ $zaaktype_notificatie->case_document_attachments };

        eval {
            $body = $mailer->send_case_notification({
                notification => $zaaktype_notificatie->bibliotheek_notificaties_id,
                recipient    => $recipient,
                attachments  => \@zaaktype_kenmerken_ids,
                cc           => $params->{notificatie_cc},
                bcc          => $params->{notificatie_bcc},
            }) if $recipient;
        };
        $error = $@;
    } else {
        try {
            $c->log->debug(sprintf(
                "Sending email from %s->%s",
                __PACKAGE__,
                'send_validated_email',
            ));
            $body = $mailer->send_from_case(
                {
                    recipient  => $recipient,
                    body       => $params->{notificatie_bericht},
                    subject    => $params->{notificatie_onderwerp},
                    cc         => $params->{notificatie_cc},
                    bcc        => $params->{notificatie_bcc},
                    log_error  => $params->{log_error},
                    request_id => $c->stash->{request_id},
                }
            ) if $recipient;
        } catch {
            $error = $_;
            $c->log->info("Error sending email: $error");
        };
    }

    my $msg;
    if ($body) {
        $msg = "E-mail verstuurd naar $recipient";
    }
    else {
        $msg = "Er is iets mis gegaan bij het versturen van e-mail naar $recipient";
        if ($error && !$params->{log_error} && eval { $error->isa('Throwable::Error') }) {
            $msg = $error->message;
        }
    }

    $c->push_flash_message($msg);
    $c->res->redirect($c->uri_for("/zaak/" . $c->stash->{zaak}->id));
}


sub download_email_pdf : Chained('/zaak/base') : PathPart('download_email_pdf') : Args(1) {
    my ($self, $c, $event_id) = @_;

    my $event = $c->model('DB::Logging')->find($event_id);
    my $event_data = $event->event_data;

    my $name = $c->stash->{zaak}->id . " " . $event_data->{subject} . ".pdf";

    my $pdf = $event->export_as_pdf();

    $c->response->body($pdf);
    $c->res->headers->content_length(length $pdf);
    $c->res->headers->content_type('application/octet-stream');
    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');
    $c->res->header('Content-Disposition' => sprintf('attachment; filename="%s"', $name));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 aanvrager

TODO: Fix the POD

=cut

=head2 document

TODO: Fix the POD

=cut

=head2 download_email_pdf

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 parse_special_vars

TODO: Fix the POD

=cut

=head2 preview

TODO: Fix the POD

=cut

=head2 registratie

TODO: Fix the POD

=cut

=head2 set_error_invalid

TODO: Fix the POD

=cut

=head2 set_error_missing

TODO: Fix the POD

=cut
