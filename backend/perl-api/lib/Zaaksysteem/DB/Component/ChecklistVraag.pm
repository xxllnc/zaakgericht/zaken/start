package Zaaksysteem::DB::Component::ChecklistVraag;

use strict;
use warnings;

use Data::Dumper;

use base qw/DBIx::Class/;

sub added_columns {
    return [qw/
        mogelijkheden
    /];
}

sub mogelijkheden {
    my $self    = shift;

    if ($self->checklist_mogelijkhedens) {
        return $self->checklist_mogelijkhedens->_retrieve_as_session;
    }
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 added_columns

TODO: Fix the POD

=cut

=head2 mogelijkheden

TODO: Fix the POD

=cut

