package Zaaksysteem::DB::Component::GmNatuurlijkPersoon;

use strict;
use warnings;

use base qw(
    Zaaksysteem::DB::Component::GenericNatuurlijkPersoon
);

sub update {
    my ($self) = shift;

    $self->set_columns(
        {
            voorletters => $self->_build_voorletters,
            naamgebruik => $self->achternaam,
        }
    );
    $self->next::method(@_);
}


1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

