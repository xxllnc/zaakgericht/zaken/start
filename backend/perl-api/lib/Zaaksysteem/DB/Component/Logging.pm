package Zaaksysteem::DB::Component::Logging;
use base qw/DBIx::Class::Row/;
use Moose;

use Moose::Util qw/apply_all_roles/;
use JSON;
use DateTime;
use DateTime::Format::ISO8601;

use BTTW::Tools;

with qw(
    MooseX::Log::Log4perl
);

sub new {
    shift->next::method(@_)->_apply_roles;
}

sub inflate_result {
    shift->next::method(@_)->_apply_roles;
}

has _skip_roles => (
    is      => 'ro',
    isa     => 'HashRef',
    builder => '_build_skip_roles',
);

sub _build_skip_roles {
    return {
        map { $_ => 1 } qw(
            Zaaksysteem::DB::Component::Logging::Admin
            Zaaksysteem::DB::Component::Logging::Admin::Catalog
            Zaaksysteem::DB::Component::Logging::Api
            Zaaksysteem::DB::Component::Logging::Api::V1
            Zaaksysteem::DB::Component::Logging::Api::V1::Update::Attributes
            Zaaksysteem::DB::Component::Logging::Api::V1::Update::Documents
            Zaaksysteem::DB::Component::Logging::Api::V1::Update::Documents
            Zaaksysteem::DB::Component::Logging::Attribute
            Zaaksysteem::DB::Component::Logging::Auth
            Zaaksysteem::DB::Component::Logging::Auth
            Zaaksysteem::DB::Component::Logging::Auth::Alternative
            Zaaksysteem::DB::Component::Logging::Case
            Zaaksysteem::DB::Component::Logging::Case::Attribute
            Zaaksysteem::DB::Component::Logging::Case::Checklist
            Zaaksysteem::DB::Component::Logging::Case::Checklist::Item
            Zaaksysteem::DB::Component::Logging::Case::Communication
            Zaaksysteem::DB::Component::Logging::Case::Creation
            Zaaksysteem::DB::Component::Logging::Case::Document::Metadata
            Zaaksysteem::DB::Component::Logging::Case::Note
            Zaaksysteem::DB::Component::Logging::Case::Note
            Zaaksysteem::DB::Component::Logging::Case::Paused
            Zaaksysteem::DB::Component::Logging::Case::Payment
            Zaaksysteem::DB::Component::Logging::Case::Pip
            Zaaksysteem::DB::Component::Logging::Case::Relation
            Zaaksysteem::DB::Component::Logging::Case::Remove
            Zaaksysteem::DB::Component::Logging::Case::Remove::Relation
            Zaaksysteem::DB::Component::Logging::Case::Subject
            Zaaksysteem::DB::Component::Logging::Case::Task
            Zaaksysteem::DB::Component::Logging::Case::Thread
            Zaaksysteem::DB::Component::Logging::Case::Thread
            Zaaksysteem::DB::Component::Logging::Case::Update
            Zaaksysteem::DB::Component::Logging::CaseRelation
            Zaaksysteem::DB::Component::Logging::CaseRelation::Create
            Zaaksysteem::DB::Component::Logging::Casetype
            Zaaksysteem::DB::Component::Logging::Casetype::SetActiveVersion
            Zaaksysteem::DB::Component::Logging::Casetype::Update
            Zaaksysteem::DB::Component::Logging::Contactmoment
            Zaaksysteem::DB::Component::Logging::Email
            Zaaksysteem::DB::Component::Logging::File
            Zaaksysteem::DB::Component::Logging::File::Trash
            Zaaksysteem::DB::Component::Logging::Folder
            Zaaksysteem::DB::Component::Logging::InternalNote
            Zaaksysteem::DB::Component::Logging::NatuurlijkPersoon
            Zaaksysteem::DB::Component::Logging::Object::Relation::Add
            Zaaksysteem::DB::Component::Logging::Ou
            Zaaksysteem::DB::Component::Logging::Role
            Zaaksysteem::DB::Component::Logging::Stuf::RemoteSearch
            Zaaksysteem::DB::Component::Logging::Subject
            Zaaksysteem::DB::Component::Logging::Subject::Company
            Zaaksysteem::DB::Component::Logging::Subject::Contactmoment
            Zaaksysteem::DB::Component::Logging::Subject::Message
            Zaaksysteem::DB::Component::Logging::Subject::Note
            Zaaksysteem::DB::Component::Logging::Sysin
            Zaaksysteem::DB::Component::Logging::Sysin::Interface
            Zaaksysteem::DB::Component::Logging::Template
            Zaaksysteem::DB::Component::Logging::Template::Email
            Zaaksysteem::DB::Component::Logging::User
            Zaaksysteem::DB::Component::Logging::Xential
            Zaaksysteem::DB::Component::Logging::Subject::Internalnote
        )
    };
}

sub _apply_roles {
    my ($self) = @_;

    unless ($self->event_type) {
        my $data = $self->infer_event_type;

        $self->event_data($data) if $data;
    }

    if ($self->event_type) {
        apply_all_roles($self, 'Zaaksysteem::DB::Component::Logging::Event');

        # Apply all possible roles to this event that we can find
        # Unroll the event_type, append event_type parts to the module we're looking for
        # Example: case/checklist/item/update

        # First iteration: try to apply ZS::DB::C::Logging::Case
        # Second iteration:             ZS::DB::C::Logging::Case::Checklist
        # Third iteration:              ZS::DB::C::Logging::Case::Checklist::Item
        # Final iteration:              ZS::DB::C::Logging::Case::Checklist::Item::Update

        # The reason for this is so we get some inheritence-like behavior in our roles.
        my @type = split('/', $self->event_type);
        my @subtype_iterator;

        while(scalar(@type)) {
            push(@subtype_iterator, shift @type);

            my $role = $self->inflect_event_type(join('/', @subtype_iterator));

            try {
                # Skip roles which we know do not exists
                return if $self->_skip_roles->{$role};

                apply_all_roles($self, $role);
            }
            catch {
                $self->log->trace("Error applying role $role: " . $_->message);
            };
        }

        $self->_add_magic_attributes;
    }

    return $self;
}

sub infer_event_type {
    my $self = shift;

    if (!defined $self->onderwerp) {
        $self->log->warn("No logging possible for event type: " . ($self->event_type // "No event type"));
        return;
    }

    if($self->onderwerp =~ 'Zaak \((\d+)\) aangemaakt') {
        $self->event_type('case/create');

        return {
            case_id => $self->get_column('zaak_id'),
            casetype_id => $self->zaak_id->get_column('zaaktype_id')
        };
    }

    if($self->onderwerp =~ 'Zaak gesloten op (.*)') {
        $self->event_type('case/close');

        return {
            case_id => $self->get_column('zaak_id'),
            close_date => $1
        }
    }

    if($self->onderwerp =~ 'Kenmerk "(.*)" gewijzigd naar: "(.*)"') {
        $self->event_type('case/attribute/update');

        my $attribute = $self
                        ->result_source
                        ->schema
                        ->resultset('ZaaktypeKenmerken')
                        ->search({
                            'bibliotheek_kenmerken_id.naam' => { 'ilike' => $1 },
                            'me.zaaktype_node_id' => $self->zaak_id->get_column('zaaktype_node_id')
                        },
                        {
                            join    => 'bibliotheek_kenmerken_id'
                        }
                    )->first;

        return {
            case_id => $self->get_column('zaak_id'),
            attribute_id => $attribute ? $attribute->get_column('bibliotheek_kenmerken_id') : undef,
            attribute_value => $2
        };
    }

    if($self->onderwerp =~ 'Document "(.*)" \[(\d+)\] succesvol aangemaakt') {
        $self->event_type('case/document/create');

        return {
            case_id => $self->get_column('zaak_id'),
            document_filename => $1,
            document_id => $self->component_id
        };
    }

    if($self->onderwerp =~ 'Vernietigingsdatum gewijzigd: (.*)') {
        $self->event_type('case/update/purge_date');

        return {
            case_id => $self->get_column('zaak_id'),
            purge_date => $1
        };
    }

    if($self->onderwerp =~ 'Streefafhandeldatum voor zaak: "(.*)" gewijzigd naar: (.*)') {
        $self->event_type('case/update/target_date');

        return {
            case_id => $self->get_column('zaak_id'),
            target_date => $2
        };
    }

    if($self->onderwerp =~ 'registratiedatum voor zaak: "(.*)" gewijzigd naar: (.*)') {
        $self->event_type('case/update/registration_date');

        return {
            case_id => $self->get_column('zaak_id'),
            registration_date => $2
        };
    }

    if($self->onderwerp =~ 'Status gewijzigd naar: (.*)') {
        $self->event_type('case/update/status');

        return {
            case_id => $self->get_column('zaak_id'),
            status => $1
        };
    }

    if($self->onderwerp =~ 'Betrokkene "(.*)" gewijzigd naar: "(.*)"') {
        $self->event_type('case/relation/update');

        return {
            case_id => $self->get_column('zaak_id'),
            subject_relation => $1,
            subject_name => $2
        };
    }

    if($self->onderwerp =~ 'Betrokkene: "(.*)" toegevoegd aan zaak, relatie: (.*)') {
        $self->event_type('case/relation/update');

        return {
            case_id => $self->get_column('zaak_id'),
            subject_relation => $1,
            subject_name => $2
        };
    }

    if($self->onderwerp =~ 'Kenmerk "(.*)" verwijderd ivm verbergen door regels.') {
        $self->event_type('case/attribute/remove');

        return {
            case_id => $self->get_column('zaak_id'),
            attribute_id => $self->component_id
        };
    }

    if($self->onderwerp =~ 'Sjabloon \((.*)\) toegevoegd') {
        $self->event_type('case/template/add');

        return {
            case_id => $self->get_column('zaak_id'),
            template_name => $1
        };
    }

    if($self->onderwerp =~ 'Sjabloon "(.*)" \[(\d+)\] succesvol gegenereerd') {
        $self->event_type('case/template/render');

        return {
            case_id => $self->get_column('zaak_id'),
            document_filename => $1,
            document_id => $2
        };
    }

    if($self->onderwerp =~ 'Antwoord voor vraag: "(.*)" gewijzigd naar "(.*)"') {
        $self->event_type('case/checklist/item/update');

        return {
            case_id => $self->get_column('zaak_id'),
            checklist_item_name => $1,
            checklist_item_value => $2
        }
    }

    if($self->onderwerp =~ 'Notitie toegevoegd') {
        $self->event_type('case/note/create');

        return {
            content => $self->bericht
        };
    }

    $self->log->trace(sprintf("The subject '%s' does not match anything, requires an extension", $self->onderwerp));

    return;
}

# C3 method resolution and this class' structure require we claim this sub
# so we can propagate the call to the *instance's* parent class. Left out,
# this insert will be handled through a seperate, undesired, path.
sub insert { shift->next::method(@_) }

sub update {
    my $self    = shift;

    if ($self->can('data')) {
        $self->event_data($self->data);
    }

    $self->next::method( @_ );
}

=head2 upsert_by_recent

Inserts or updates a log row. Uses a 10 minute interval to decide if row
was inserted recently, and identifies the row based on our current values
and the provided columns to check.

=cut

sub upsert_by_recent {
    my ($self, @columns) = @_;

    my $source = $self->result_source;

    my %id_conditions = map { $_ => $self->get_column($_) } @columns;

    $id_conditions{ last_modified } //= {
        '>=' => $source->schema->format_datetime_object(
            DateTime->now->subtract(minutes => 10)
        )
    };

    my $previous = $self->result_source->resultset->search_rs(\%id_conditions, {
        order_by => { -desc => 'last_modified' }
    })->first;

    return $self->insert unless defined $previous;

    $previous->set_columns({ $self->get_columns });

    return $previous->update;
}

sub creator {
    my $self = shift;

    return defined $self->created_by_name_cache
        ? $self->created_by_name_cache
        : $self->_get_name_of_id('created_by');
}

sub _get_name_of_id {
    my ($self, $col_name) = @_;
    my $val = $self->get_column($col_name);
    return '<onbekend>' if !defined $val;

    my $st = $self->result_source->schema->storage;
    my $name = $st->dbh_do(sub {
        my ($storage, $dbh, @args) = @_;

        my $sth = $dbh->prepare('select get_subject_by_legacy_id(?)');
        $sth->execute($val);
        my @result = $sth->fetchrow_array or die $sth->errstr;
        my $uuid = $result[0];
        return '<onbekend>' unless defined $uuid;

        $sth = $dbh->prepare('select get_subject_display_name_by_uuid(?)');
        $sth->execute($uuid);
        @result = $sth->fetchrow_array;
        return defined $result[0] ? $result[0] : '<onbekend>';
    });
    return $name;
}

sub TO_JSON {
    my $self = shift;

    return {
        id             => $self->id,
        event_type     => $self->event_type,
        event_category => $self->event_category,
        case_id        => $self->get_column('zaak_id'),
        timestamp      => $self->created_localized->datetime,
        description    => $self->get_column('onderwerp') // $self->onderwerp,
        created_by     => $self->creator // '<onbekend>',
    };

}

my %csv_data_mapping = (
    component             => 'Component',
    zaak_id               => 'Zaak',
    onderwerp             => 'Onderwerp',
    created               => 'Aangemaakt',
    created_by_name_cache => 'Aangemaakt door',
    created_for           => 'Aangemaakt voor',
    event_type            => 'Type',
);

my @csv_order = qw(
    created
    event_type
    component
    zaak_id
    onderwerp
    created_by_name_cache
    created_for
);

sub csv_header {
    my $self = shift;
    return [ map { $csv_data_mapping{$_} } @csv_order ];
}

sub csv_data {
    my $self = shift;

    my @data;

    foreach (@csv_order) {
        if ($_ eq 'onderwerp') {
            push(@data, $self->onderwerp);
            next;
        }

        if ($_ eq 'created_for') {
            push(@data, $self->_get_name_of_id($_));
            next;
        }

        if ($_ eq 'created_by_name_cache') {
            push(@data, $self->creator);
            next;
        }

        if ($_ eq 'created') {
            push(@data, $self->created_localized->strftime('%d-%m-%Y %H:%M'));
            next;
        }

        push(@data, $self->get_column($_));
    }
    return \@data;
}

# Default category is always 'event'
sub event_category { 'event' }

# Convenience getter for arbitrairy resultsets
sub rs { shift->result_source->schema->resultset(shift); }

# Nor should we be filtered
sub _should_filter { return 1; }

=head2 created_localized

Creates a localized timestamp from the created timestamp.
If the event data contains a timestamp, that timestamp is also localized.

=cut

sub created_localized {
    my $self = shift;

    if ($self->can('data') && ref $self->data eq 'HASH' && $self->data->{timestamp}) {
        my $dt = DateTime::Format::ISO8601->parse_datetime($self->data->{timestamp});
        $self->data->{timestamp} = $self->_create_localized($dt)->datetime;
    }

    return $self->_create_localized($self->created // $self->last_modified);
}

=head2 _create_localized

Private helper function to create localized timezones.

=head3 TODO

Perhaps the timezone should be in a configuration, so you can pick and choose, and/or based on the timezone of the client.
In any case it has feature request written all over it.

=cut

sub _create_localized {
    my ($self, $dt) = @_;
    return $dt->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam');
}


=head2 delete_messages

When cleaning up cases we need to get rid of the logging as well. When a
message is created it references the logging. So we need to clean that up
first.

=cut

sub delete_messages {
    my $self = shift;

    $self->messages->delete_all;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 creator

TODO: Fix the POD

=cut

=head2 event_category

TODO: Fix the POD

=cut

=head2 infer_event_type

TODO: Fix the POD

=cut

=head2 inflate_result

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 loglevel

TODO: Fix the POD

=cut

=head2 new

TODO: Fix the POD

=cut

=head2 rs

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

