package Zaaksysteem::DB::Component::Logging::Auth::Alternative::Mail;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head2 subject

Returns a L<Zaaksysteem::Model::DB::Subject> object

=cut

has subject => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        $self->rs('Subject')->find($self->component_id);
    }
);

=head2 onderwerp

Pretty print the subject

=cut

sub onderwerp {
    my $self = shift;

    my $subject = $self->subject;
    my $data    = $self->data;

    if ($data->{error}) {
        if ($subject) {
            return sprintf("Mail '%s' niet verstuurd naar '%s': %s", $data->{template}, $subject->username, $data->{error});
        }
        else {
            return sprintf("Mail '%s' niet verstuurd: %s", $data->{template}, $data->{error});
        }
    }

    if ($subject) {
        return sprintf("Mail '%s' verstuurd naar '%s <%s>'", $data->{template}, $subject->username, $data->{context}{email});
    }
    else {
        return sprintf("Mail '%s' verstuurd naar <%s>", $data->{template}, $data->{context}{email})
    }
}

sub event_category { 'system'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
