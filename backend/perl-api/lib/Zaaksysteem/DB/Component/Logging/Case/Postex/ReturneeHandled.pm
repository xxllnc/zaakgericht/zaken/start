package Zaaksysteem::DB::Component::Logging::Case::Postex::ReturneeHandled;

use Moose::Role;

with qw(
    Zaaksysteem::Moose::Role::LoggingSubject
    Zaaksysteem::Moose::Role::LoggingPostex
);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Postex::ReturneeHandled - 
Postex delivery logging component

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;
    my $data = $self->data;

    return sprintf(
        'Postex stukken retour afzender afgehandeld op %s',
        $self->format_postex_time($data->{deliveryInfo}{eventTimestamp})
    );
}

=head2 event_category

defines the category, used by the frontend in the timeline, to display the right
icons etc.

=cut

sub event_category { 'contactmoment'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
