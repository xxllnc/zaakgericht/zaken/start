package Zaaksysteem::DB::Component::Logging::Case::Postex::Scheduled;

use Moose::Role;

with qw(
    Zaaksysteem::Moose::Role::LoggingSubject
    Zaaksysteem::Moose::Role::LoggingPostex
);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Postex::Scheduled -
Postex delivery logging component

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;
    my $data = $self->data;

    if (($data->{deliveryInfo}{deliveryMethod} // '') eq 'Hardcopy') {
        return sprintf(
            'Aangeboden aan postbezorging op %s, verwachte bezorgdatum %s',
            $self->format_postex_time($data->{deliveryInfo}{eventTimestamp}),
            $self->format_postex_time($data->{deliveryInfo}{plannedDeliveryDate}),
        );
    }
    return sprintf(
        'Email ingepland op %s, verwachte verzenddatum %s',
        $self->format_postex_time($data->{deliveryInfo}{eventTimestamp}),
        $self->format_postex_time($data->{deliveryInfo}{plannedDeliveryDate}),
    );
}

=head2 event_category

defines the category, used by the frontend in the timeline, to display the right
icons etc.

=cut

sub event_category { 'contactmoment'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
