package Zaaksysteem::DB::Component::Logging::Case::Resume;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


use DateTime::Format::Strptime;
use BTTW::Tools;

sub onderwerp {
    my $self = shift;

    my $onderwerp = sprintf(
        'Zaak %d hervat: %s',
        $self->data->{ case_id },
        $self->data->{ reason },
    );

    if($self->data->{ stalled_since } && $self->data->{ stalled_until }) {
        my $stalled_since_formatted = parse_and_format_datetime($self->data->{ stalled_since });
        my $stalled_until_formatted = parse_and_format_datetime($self->data->{ stalled_until });

        $onderwerp .= ". Opgeschort van " . $stalled_since_formatted .
            " tot " . $stalled_until_formatted;
    }

    return $onderwerp;
}

sub parse_and_format_datetime {
    my $date = shift;

    my $to_dt_iso = get_datetime_parser("%Y-%m-%d");
    my $to_dt_eu = get_datetime_parser("%d-%m-%Y");

    my $parsed_date = $to_dt_iso->parse_datetime($date) // $to_dt_eu->parse_datetime($date);
    
    return "Onbekende datum '$date'" unless defined $parsed_date;

    return $parsed_date->strftime('%d-%m-%Y');
}

sub get_datetime_parser {
    my $date_format = shift;

    return DateTime::Format::Strptime->new(
        pattern   => $date_format,
        locale    => 'nl_NL',
        time_zone => "Europe/Amsterdam",
    );
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

