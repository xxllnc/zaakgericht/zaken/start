package Zaaksysteem::DB::Component::Logging::Case::SendMijnoverheidMessage::Error;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use JSON;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::SendMijnoverheidMessage::Error - Log message formatter

=head1 SYNOPSIS

This module is automatically used for "Logging" database entries of type
"case/send_mijnoverheid_message/error".

=head1 METHODS

=head2 onderwerp

Returns the subject of the log (as displayed to)

=cut

sub onderwerp {
    my $self = shift;
    return 'Bericht niet verstuurd naar MijnOverheid ' . $self->data->{destination};
}

=head2 TO_JSON

Returns a JSON representation of the log entry, with the content properly formatted.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{content} = "";

    if ($self->data->{ subject }) {
        $data->{ content } = "Onderwerp: " . $self->data->{subject} . "\n\n";
    }

    $data->{content} .= $self->data->{message};

    $data->{expanded} = JSON::false;

    return $data;
};

=head2 event_category

Returns the category of this log message (used for display purposes)

=cut

sub event_category { 'contactmoment'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
