package Zaaksysteem::DB::Component::Logging::Case::Update::CompletionDate;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head2 onderwerp

Human-readable subject for this log entry.

=cut

sub onderwerp {
    my $self = shift;

    sprintf('Afhandeldatum voor zaak: %d gewijzigd naar: %s',
        $self->data->{ case_id },
        $self->data->{ target_date }
    );
}

=head2 event_category

Category of this type of event. Always "case-mutation".

=cut

sub event_category { 'case-mutation'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
