package Zaaksysteem::DB::Component::Logging::Kcc::Call;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use Data::Dumper;
use JSON;

sub onderwerp {
    my ($self) = @_;

    return sprintf("KCC Registratie: %s naar toestel %s", $self->data->{ phonenumber }, $self->data->{ extension });
}

sub _add_magic_attributes {
    shift->meta->add_attribute('contact' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        my ($contact) = $self->result_source->schema->resultset('ContactData')->find_by_phonenumber(
            $self->data->{ phonenumber }
        );

        return $contact;
    }));
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ contact } = $self->contact;
    $data->{ accepted } = exists $self->data->{ accepted } ? ($self->data->{ accepted } ? JSON::true : JSON::false) : JSON::null;

    return $data;
};

sub _should_filter {
    my $self = shift;

    return
        (!exists $self->data->{ accepted }) &&
        (defined $self->contact) &&
        $self->data->{ extension } eq 201; # hack voor vianen presentatie
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

