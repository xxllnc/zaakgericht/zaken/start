package Zaaksysteem::DB::Component::Logging::Role::Create;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    my $role = $data->{role} // $data->{name};
    my $parent = $data->{parent};

    my $msg = "Rol '$role' toegevoegd";
    if ($parent) {
        my $group = $self->result_source->schema->resultset('Groups')->find($parent);
        $msg = sprintf("$msg aan afdeling '%s'",
            $group ? $group->name : "Onbekende afdeling $parent")
    }
    return $msg;

}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

