package Zaaksysteem::DB::Component::Logging::Subject::RemoteSearch;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


use BTTW::Tools qw(dump_terse);

=head2 onderwerp

Display the log entry in the log for a remote search

=cut

sub onderwerp {
    my $self    = shift;

    my $data = $self->data;
    my $type = delete $data->{params}{subject_type};
    if ($type eq 'person') {
        return sprintf(
            "Externe zoekopdracht 'Natuurlijk Persoon' uitgevoerd: %s",
            dump_terse($data->{params}),
        );
    }
    # Catch all for all the rest
    else {
        return sprintf(
            'Externe zoekopdracht %s uitgevoerd: %s',
            $type,
            dump_terse($data->{params}),
        );
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
