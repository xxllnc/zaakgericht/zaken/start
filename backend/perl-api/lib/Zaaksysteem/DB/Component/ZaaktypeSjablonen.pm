package Zaaksysteem::DB::Component::ZaaktypeSjablonen;

use strict;
use warnings;

use base qw/DBIx::Class/;

=head1 METHODS

=head2 added_columns

Returns virtual columns to the component

=cut

sub added_columns {
    return [qw/
        naam
        display_name
        catalog_name
    /];
}

=head2 naam

Use the C<custom_filename> or use the name of the template itself as a name.
The former wins over the latter.

=cut

sub naam {
    my $self = shift;

    if (defined(my $name = $self->get_column('custom_filename'))) {
        return $name;
    }
    return $self->display_name;
}

sub display_name {
    my $self = shift;

    if (defined(my $name = $self->get_column('label'))) {
        return $name;
    }
    return $self->catalog_name;
}

sub catalog_name {
    my $self = shift;

    if ($self->bibliotheek_sjablonen_id) {
        return $self->bibliotheek_sjablonen_id->naam;
    }
    return;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
