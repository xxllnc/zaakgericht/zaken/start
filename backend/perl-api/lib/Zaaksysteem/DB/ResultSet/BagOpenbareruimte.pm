package Zaaksysteem::DB::ResultSet::BagOpenbareruimte;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN {
    extends 'Zaaksysteem::DB::ResultSet::BagGeneral';
}

with 'Zaaksysteem::Search::ScoredResultSet';

=head1 NAME

Zaaksysteem::DB::ResultSet::BagOpenbareruimte - Additional BAG search logic

=head1 METHODS

=head2 search_address

This method returns a resultset of ranked rows that best match the given
address.

    my $rs = $c->model('DB::BagOpenbareruimte')->search_address(
        Zaaksysteem::Object::Types::Address->new(...)
    );

This method uses L<Zaaksysteem::Search::ScoredResultSet>, and defines 2
scoring levels (+1, +10).

=cut

sig search_address => 'Zaaksysteem::Object::Types::Address';

sub search_address {
    my $self = shift;
    my $address = shift;

    return $self->scored_search($address, {
        join => 'woonplaats'
    });
}

=head2 _build_score_map

Implements scoring map for L<Zaaksysteem::Search::ScoredResultSet>.

=cut

sub _build_score_map {
    my $self = shift;
    my $address = shift;

    return [
        {
            'woonplaats.naam' => $address->city,
        },
        {
            'me.naam' => $address->street
        }
    ];
}

1;

__END__
=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
