package Zaaksysteem::DB::ResultSet::Parkeergebied;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;
use Clone qw/clone/;

extends 'DBIx::Class::ResultSet';


sub parkeergebieden {
    my $self = shift;

    my $rs = $self->search(undef, {
        select  => { distinct => 'parkeergebied' },
        as      => 'parkeergebied',
        order_by => {'-asc' => 'parkeergebied'},
    });

    my $results = [];
    while(my $row = $rs->next) {
        push @$results, $row->parkeergebied;
    }

    return $results;
}

sub find_parkeergebied {
    my ($self, $options) = @_;

    my $arguments = {};
    if($options->{huisnummer} && $options->{postcode}) {
        $arguments = {
            postcode => { ilike => $options->{postcode} },
            huisnummer => $options->{huisnummer},
        };
    } elsif($options->{woonplaats} && $options->{straatnaam}) {
        $arguments = {
            woonplaats => { ilike => $options->{woonplaats} },
            straatnaam => { ilike => $options->{straatnaam} }
        };
    } else {
        return [];
    }

    if($options->{huisnummertoevoeging}) {
        $arguments->{huisnummertoevoeging} = { ilike => $options->{huisnummertoevoeging} };
    }

    if($options->{huisletter}) {
        $arguments->{huisletter} = { ilike => $options->{huisletter} };
    }

    return $self->search($arguments)->parkeergebieden;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 find_parkeergebied

TODO: Fix the POD

=cut

=head2 parkeergebieden

TODO: Fix the POD

=cut

