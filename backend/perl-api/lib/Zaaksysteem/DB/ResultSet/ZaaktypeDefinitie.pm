package Zaaksysteem::DB::ResultSet::ZaaktypeDefinitie;

use strict;
use warnings;

use Moose;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

# COMPLETE: 100%
use constant    PROFILE => {
    required        => [qw/
        openbaarheid
        handelingsinitiator
        grondslag
        afhandeltermijn
        servicenorm
        selectielijst
    /],
    optional        => [qw/
        afhandeltermijn_type
        servicenorm_type
        procesbeschrijving
        pdc_voorwaarden
        pdc_description
        pdc_meenemen
        pdc_tarief
        webform_authenticatie
        webform_toegang
        preset_client
    /],
    constraint_methods => {
        preset_client => sub {
            my $dfv = shift;
            my $val = shift;

            $val ne 'betrokkene--';
        }
    },
    msgs => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile, 1);
}

1;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

