package Zaaksysteem::Datastore::Model;
use Zaaksysteem::Moose;

with 'Zaaksysteem::Moose::Role::Schema';

use Encode qw(encode);
use File::Temp;
use List::Util qw(any none);
use Text::CSV_XS;
use Zaaksysteem::Constants qw/STUF_SUBSCRIPTION_VIEWS/;
use Zaaksysteem::Datastore::NatuurlijkPersoon;
use Zaaksysteem::Datastore::Bedrijf;
use Zaaksysteem::Types::Datastore qw(DatastoreType);

with 'MooseX::Log::Log4perl';

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

=head1 METHODS

=cut

=head1 ATTRIBUTES

=cut

has type => (
    is       => 'ro',
    isa      => DatastoreType,
    required => 1,
);

has resultset => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_resultset_for_type',
    init_arg => undef,
);

has object_subscription => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_object_subscription',
    init_arg => undef,
);

has csv => (
    is => 'ro',
    isa => 'Text::CSV_XS',
    lazy => 1,
    builder => '_build_csv',
);

has config_class => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        if ($self->type eq 'Organisatie') {
            return 'Bedrijf';
        }
        return $self->type =~ m|^Bag| ? 'Bag' : $self->type;
    },
    init_arg => undef,
);

has formatters => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_build_formatters_for_config_class',
    init_arg => undef,
);


sub _build_csv {
    my $self = shift;
    return Text::CSV_XS->new({
        quote_char          => '"',
        escape_char         => '"',
        sep_char            => ',',
        eol                 => "\n",
        binary              => 1,
        allow_loose_quotes  => 1,
        allow_loose_escapes => 1,
        allow_whitespace    => 1,
        always_quote        => 1,
    });
}

sub _build_resultset_for_type {
    my $self = shift;
    if ($self->type eq 'Organisatie') {
        return $self->build_resultset('Bedrijf');
    }
    return $self->build_resultset($self->type);
}

sub _build_object_subsciption {
    my $self = shift;
    return $self->build_resultset('ObjectSubscription');
}

sub _build_formatters_for_config_class {
    my $self = shift;

    my %dates = (
        NatuurlijkPersoon => [
            qw(
                geboortedatum
                import_datum
                deleted_on
                datum_overlijden
                datum_huwelijk
                datum_huwelijk_ontbinding
                )
        ],
        Bedrijf => [qw(import_datum deleted_on)],
    );

    return {}
        if none { $_ eq $self->config_class } qw(NatuurlijkPersoon Bedrijf);

    my $date_formatter = sub {
        my $value = shift;

        return $value && $value->strftime('%d-%m-%Y %H:%M:%S');
        return $value;
    };

    my %formatters = map { $_ => $date_formatter }
            @{ $dates{ $self->config_class } };

    if ($self->config_class eq 'NatuurlijkPersoon') {
        $formatters{burgerservicenummer} = sub {
            my $val = shift;
            return '' unless $val;
            return sprintf("%09d", int($val));
        };
        $formatters{gemeentecode} = sub {
            my $val = shift;
            return '' unless $val;
            return sprintf("%04d", int($val));
        };
    }
    else {
        $formatters{dossiernummer} = sub {
            my $val = shift;
            return '' unless $val;
            return sprintf("%08d", int($val));
        };
        $formatters{vestigingsnummer} = sub {
            my $val = shift;
            return '' unless $val;
            return sprintf("%012d", int($val));
        };
    }

    $formatters{pending} = sub {
        my $val = shift;
        return $val ? 'Ja' : 'Nee';
    };

    return \%formatters;
}

=head2 get_columns

Get the columns for the datastore type

=cut

sub get_columns {
    my ($self) = @_;

    my @columns = grep {
               $_ ne 'search_index'
            && $_ ne 'search_order'
            && $_ ne 'search_term'
            && $_ ne 'searchable_id'
            && $_ ne 'object_type'
            && $_ ne 'uuid'
            && $_ ne 'deleted_on'
            && $_ ne 'adres_id'    ### Special natuurlijkpersoon case
    } $self->resultset->result_source->columns;

    # Object subscription isn't an actual column
    my @generic_columns;
    push @generic_columns,
        Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id       => 'object_subscription',
        label    => 'afnemerindicatie',
        template => '<[item.object_subscription.external_id]>',
        when     => 'item.object_subscription'
        );
    push @generic_columns,
        Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id       => 'pending',
        label    => 'Wachtrij',
        template => '<[item.pending == 1 ? "Ja" : "Nee"]>',
        when     => 'item.pending'
        );

    if ($self->config_class eq 'NatuurlijkPersoon') {
        push @generic_columns,
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id       => 'me.uuid',
            label    => 'UUID',
            resolve  => 'uuid',
            template => '<a target="_top" href="/main/contact-view/person/<[item.uuid]>/data"><[item.uuid]></a>'
            );
    }
    elsif ($self->config_class eq 'Bedrijf') {
        push @generic_columns,
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id       => 'me.uuid',
            label    => 'UUID',
            resolve  => 'uuid',
            template => '<a target="_top" href="/main/contact-view/organization/<[item.uuid]>/data"><[item.uuid]></a>'
            );
    }

    # Process columns
    for my $col (@columns) {
        push @generic_columns,
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id      => 'me.' . $col,
            label   => $col,
            resolve => $col,
            $col eq 'id' && $self->config_class ne 'Bedrijf'
            ? (
                template =>
                    sprintf(
                    '<a href="/beheer/object/search/%s/<[item.id]>"><[item.id]></a>',
                    $self->type),
                )
            : (),
            );
    }

    if ($self->type eq 'NatuurlijkPersoon') {
        my @adr_columns = grep {
                   $_ ne 'search_index'
                && $_ ne 'search_term'
                && $_ ne 'searchable_id'
                && $_ ne 'object_type'
                && $_ ne 'deleted_on'
                && $_ ne 'natuurlijk_persoon_id'
        } $self->build_resultset('Adres')->result_source->columns;

        for my $col (@adr_columns) {
            push @generic_columns,
                Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id      => 'adres_id.' . $col,
                label   => $col,
                resolve => 'adres_id.' . $col,
                );
        }
    }
    return @generic_columns;
}

=head2 export

Export the data to CSV

=cut

sub export {
    my ($self, $export_params) = @_;

    my @raw_columns = $self->get_columns();

    my @columns = map({ $_->resolve } grep({ $_->resolve } @raw_columns));

    my @csv;
    push(@csv, ['afnemerindicatie', @columns]);

    my $rs    = $self->get_data_for_class($export_params);
    my $count = 0;
    while (my $r = $rs->next) {
        $count++;
        my @line;
        if ($r->can('subscription_id') && $r->subscription_id) {
            if ($r->subscription_id->date_deleted) {
                push(@line, '');
            }
            else {
                push(@line, $r->subscription_id->external_id);
            }
        }
        else {
            push(@line, '');
        }
        push(@line, $self->format_fields($r, \@columns));
        push @csv, \@line;
    }

    my $fh = $self->_create_csv(@csv);
    return $fh;
}

sub _create_csv {
    my ($self, @data) = @_;

    my $fh = File::Temp->new();
    $self->csv->say($fh, $_) for @data;
    close($fh);
    return $fh;
}

sub _add_freeform_to_search {
    my ($self, $freeform) = @_;

    return unless defined $freeform;
    return if $self->type eq 'NatuurlijkPersoon';
    return $freeform unless $self->type =~ /^Bag/;

    my @keywords = map { "%$_%" } split m[\s+], $freeform;
    return unless @keywords;    # empty search;

    if ($self->type eq 'BagOpenbareruimte') {
        return [
            { 'me.identificatie' => { ilike => \@keywords } },
            { 'me.naam'          => { ilike => \@keywords } },
            { 'woonplaats.naam'  => { ilike => \@keywords } }
        ];
    }

    return [
        { 'me.identificatie'    => { ilike => \@keywords } },
        { 'woonplaats.naam'     => { ilike => \@keywords } },
        { 'openbareruimte.naam' => { ilike => \@keywords } }
    ];
}

sub get_data_for_class {
    my ($self, $user_provided_search) = @_;
    my $search_opts = { prefetch => [], order_by => { -desc => 'me.id' } };

    my $search = {};

    my $freeform = $self->_add_freeform_to_search(
        $user_provided_search->{freeform_filter});

    if ($self->config_class eq 'NatuurlijkPersoon') {
        $search->{'me.deleted_on'} = undef;
        push(@{ $search_opts->{prefetch} }, 'adres_id');
    }
    elsif ($self->config_class eq 'Bedrijf') {
        $search->{'me.deleted_on'} = undef;
        $search->{search_term}     = { ilike => "%$freeform%" }
            if defined $freeform;
    }
    elsif ($self->type eq 'BagOpenbareruimte') {
        push(@{ $search_opts->{prefetch} }, 'woonplaats');
        $search->{'-or'} = $freeform if defined $freeform;
    }
    elsif ($self->type eq 'BagNummeraanduiding') {
        push(@{ $search_opts->{prefetch} },
            { openbareruimte => 'woonplaats' });
        $search->{'-or'} = $freeform if defined $freeform;
    }
    elsif (any { $self->type eq $_ } qw(BagLigplaats BagStandplaats)) {
        push(
            @{ $search_opts->{prefetch} },
            { hoofdadres => { openbareruimte => 'woonplaats' } }
        );
        $search->{'-or'} = $freeform if defined $freeform;
    }

    if ($self->resultset->result_source->has_relationship('subscription_id')) {
        push(@{ $search_opts->{prefetch} }, 'subscription_id');
    }

    if (any { $self->config_class eq $_ } qw(NatuurlijkPersoon Bedrijf)) {
        my $class = "Zaaksysteem::Datastore::" . $self->config_class;
        my $gm = $class->new(schema => $self->schema);
        return $gm->search($search, $search_opts,
            %{ $user_provided_search // {} });
    }

    return $self->resultset->search_rs({ %{$search}, }, $search_opts);
}


=head2 format_fields

If a field is a designated date field, format as date. Otherwise return string
value.

=cut

sub format_fields {
    my ($self, $row, $columns) = @_;

    my $formatters = $self->formatters;

    my @fields;
    for my $colname (@{$columns}) {

        next unless $colname;
        my $value;

        my $fmt = $formatters->{$colname};
        if ($colname =~ /\./) {
            my ($table, $colname) = $colname =~ /^(.+)\.(.+)$/;

            $fmt = $formatters->{$colname};
            if ($row->$table) {
                $value = $fmt ? $fmt->($row->$table->$colname) : $row->$table->get_column($colname);
            }
            else {
                if ($self->log->is_trace) {
                    $self->log->trace(
                        sprintf(
                            "%s.%s is not set because % is empty for %s",
                            $table, $colname, $table, $self->type
                        )
                    );
                }
                $value = "";
            }
        }
        else {
            $value = $fmt ? $fmt->($row->$colname) : $row->get_column($colname);
        }

        $value = encode('UTF-8', $value, 1) if defined $value;
        push(@fields, $value);
    }

    return @fields;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
