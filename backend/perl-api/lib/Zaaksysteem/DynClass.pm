package Zaaksysteem::DynClass;

use Moose::Role;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::DynClass - Dynamic loading of classes

=head1 SYNOPSIS

    package Your::ResultSet::Package

    with 'Zaaksysteem::DynClass';

    has '+_dynamic_classes => (
        default => sub {
            return [
                {
                    class       => 'My::Package::Row',
                    attributes  => {
                        id              => {
                            is          => 'ro',
                            isa         => UUID,
                            writer      => '_set_id',
                        },
                        label           => {
                            is          => 'ro',
                            writer      => '_set_label',
                        },
                    },
                    methods     => {
                        deflate_to_settings     => sub {
                            my $self        = shift;

                            return {
                                id                  => $self->id,
                                order               => $self->order,
                                reference_id        => $self->reference_id,
                                reference_type      => $self->reference_type,
                            };
                        },
                    },
                    moose           => 1,
                }
            ];
        }
    );

    sub BUILD {
        my $self    = shift;

        $self->_create_dynamic_classes;
    }

    # After BUILD, you would be able to invoke the class like: 
    # my $o = My::Package::Row->new(id => 1, label => "test");
    # and call $o->deflate_to_settings afterwards.


=head1 DESCRIPTION

This role implements the ability to generate packages from a given configuration hash. Especially usefull
when you would need a simple package containing a set of Moose attributes. By setting the attribute
C<_dynamic_classes> in your main class, and consuming this role, you are able to generate one or more simple
(or complicated) packages. See Synopsis for more information

=head1 ATTRIBUTES

=head2 _dynamic_classes

    [
        {
            class       => 'My::Package::Row',
            attributes  => {
                id              => {
                    is          => 'ro',
                    isa         => UUID,
                    writer      => '_set_id',
                },
                label           => {
                    is          => 'ro',
                    writer      => '_set_label',
                },
            },
            methods     => {
                deflate_to_settings     => sub {
                    my $self        = shift;

                    return {
                        id                  => $self->id,
                        order               => $self->order,
                        reference_id        => $self->reference_id,
                        reference_type      => $self->reference_type,
                    };
                },
            },
            moose           => 1,
        }
    ]

Attribute containing a list of classes which you would like to generate dynamically.

=cut

has '_dynamic_classes' => (
    is       => 'ro',
    isa      => 'ArrayRef',
    required => 1,
);

=head2 _create_dynamic_classes

Creates dynamic classes from the settings from attribute C<_dynamic_classes>.

=cut

my $_created_classes = {};

sub _create_dynamic_classes {
    my $self        = shift;

    for my $class (@{ $self->_dynamic_classes }) {
        next if $_created_classes->{$class->{class}};

        my $meta = Moose::Meta::Class->create(
            $class->{class},
            superclasses => [
                (
                    $class->{moose}
                        ? __PACKAGE__ . '::_Moose'
                        : ()
                ),
                @{ $class->{superclasses} // [] }
            ],
        );

        for my $attrname (keys %{ $class->{attributes} }) {
            $meta->add_attribute(
                $attrname,
                %{ $class->{attributes}->{$attrname} }   
            );
        }

        for my $methname (keys %{ $class->{methods} }) {
            $meta->add_method(
                $methname => $class->{methods}->{$methname}
            );
        }

        $meta->make_immutable;

        $_created_classes->{$class->{class}} = 1;
    }
};

=head2 validate_dynamic_class

Arguments: $CLASS_NAME, \%PARAMS

Return value: $TRUE or Exception

    $class->validate_dynamic_class('My::Class::Name', { bsn => "224234a23", name => "Frits" });

Runs a C<assert_profile> on the given classname and params.

=cut

sig validate_dynamic_class => 'Str, HashRef';

sub validate_dynamic_class {
    my $self        = shift;
    my $class       = shift;
    my $params      = shift || {};

    assert_profile($params, profile => $self->_gen_dynamic_profile($class));
}

=head2 _gen_dynamic_profile

Arguments: $CLASSNAME

Return value: \%assert_profile_readable_profile

    $class->_gen_dynamic_profile('My::Class::Name');

Returns an assert_profile readable hashref

=cut

sub _gen_dynamic_profile {
    my $self        = shift;
    my $class       = shift;
    my ($classdata, $profile) = (undef, { required => {}, optional => {} });

    throw('dynclass/cannot_find_class', "Cannot find class $class in _dynamic_classes")
        unless (($classdata) = grep { $_->{class} eq $class } @{ $self->_dynamic_classes });

    for my $attrname (keys %{ $classdata->{attributes} }) {
        my $attrdata    = $classdata->{attributes}->{$attrname};
        my $isa         = $self->_get_isa($attrdata->{isa});

        if ($attrdata->{required}) {
            $profile->{required}->{$attrname} = $isa;
        } else {
            $profile->{optional}->{$attrname} = $isa;
        }
    }

    return $profile;
}

=head2 _get_isa

Arguments: $isa

Return value: $isa or 'Any'

Gets the isa for validation from the object attribute.

=cut

sub _get_isa {
    my $self        = shift;
    my $isa         = shift;

    return 'Any' unless $isa;
    return 'Any' if $isa eq 'ArrayRef';

    return $isa;
}

=head1 INTERNAL PACKAGES

=head2 Zaaksysteem::DynClass::_Moose

Superclass containing one "use" statement: "use Moose".

=cut

package Zaaksysteem::DynClass::_Moose;

use Moose;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
