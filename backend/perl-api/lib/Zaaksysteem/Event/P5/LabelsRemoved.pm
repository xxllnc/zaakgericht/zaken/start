package Zaaksysteem::Event::P5::LabelsRemoved;
use Moose;

with 'Zaaksysteem::Event::EventInterface', 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Event::P5::LabelsRemoved - "Labels removed" event handler

=cut

sub event_type { 'LabelsRemoved' }

sub emit_event {
    my $self = shift;

    $self->queue->emit_case_event({
        case => $self->case,
        event_name => 'LabelsRemoved',
        description => 'Labels removed',
        changes => $self->event->{ changes },
        user_uuid  => $self->event->{ user_uuid },
        entity_id   => $self->event->{ entity_id },
        entity_type => 'document',
    });

    return $self->new_ack_message('Event processed: dispatched as case event');
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
