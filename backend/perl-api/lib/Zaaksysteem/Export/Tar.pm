package Zaaksysteem::Export::Tar;
use Moose::Role;

use IO::Scalar;

has tar => (
    is       => 'ro',
    isa      => 'Archive::Tar::Stream',
    required => 1,
);

sub add_scalar_to_export {
    my ($self, $filename, $scalar) = @_;

    my $fh = IO::Scalar->new(\$scalar);
    $self->tar->AddFile($filename, length($scalar), $fh, mode => 0644);
    return 1;
}

sub add_fh_to_export {
    my ($self, $filename, $fh) = @_;

    $self->tar->AddFile($filename, -s $fh, $fh, mode => 0644);
    return 1;
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Foo;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
