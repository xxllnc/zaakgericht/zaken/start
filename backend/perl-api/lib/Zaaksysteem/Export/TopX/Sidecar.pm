package Zaaksysteem::Export::TopX::Sidecar;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Export::TopX::Sidecar - Export cases in TopX/Sidecar format

=head1 DESCRIPTION

Export TopX to a side car structure

=head1 SYNOPSIS

    use Zaaksysteem::Export::TopX::Sidecar;

    my $model = Zaaksysteem::Export::TopX::Sidecar->new(
      schema  => $self->schema,
      user    => $user,
      objects => $results,
      tar     => $tar,
      $mapping ? (attribute_mapping => $mapping) : (),
    );

    $model->export();

=cut

with 'Zaaksysteem::Export::TopX::Base';

use DateTime;
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use XML::LibXML;
use Zaaksysteem::XML::Compile;
use File::Spec::Functions qw(catfile);

has '+identification' => (required => 1);

has target_system => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_target_system',
);

has records => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
    writer  => "_set_record",
);


sub _get_case_path {
    my $self = shift;
    my $case = shift;
    my @misc = @_;

    my $case_id = $case->get_column('id');
    my $casetype = $case->get_classification_code // $case->get_casetype_title;
    return catfile($self->identification, $casetype, $case_id, @misc);
}

sub _export_case_xml {
    my ($self, $case, $xml) = @_;

    my $filename = $self->_get_case_path($case, $case->get_column('id') . ".xml");
    $self->add_scalar_to_export($filename, $xml);
    $self->_set_record(1) unless $self->records;
    return 1;
}

sub _export_case_document_xml {
    my ($self, $case, $file, $xml) = @_;

    my $id = $file->id;
    my $uuid = $file->get_column('uuid');
    my $basedir = $self->_get_case_path($case, $id);

    my $ext = $file->extension;

    my $filename = catfile($basedir, "$uuid.metadata");
    $self->add_scalar_to_export($filename, $xml);

    my $name = catfile($basedir, "$uuid$ext");
    $self->add_fh_to_export($name, $file->filestore->get_path);
    return 1;
}

after export => sub {
    my $self = shift;
    return unless $self->records;
    my $xml  = $self->_create_archive_xml;

    my $id = $self->identification;
    my $filename = catfile($id, "$id.metadata");
    $self->add_scalar_to_export($filename, $xml);
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
