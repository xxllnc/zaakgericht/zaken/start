package Zaaksysteem::External::Postex;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::External::Postex - A Postex integration module

=head1 DESCRIPTION

Integrate L<Postex> into Zaaksysteem. This module is the glue
layer between any module that wants to use Postex into
Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::External::Postex;

    my $model = Zaaksysteem::External::Postex->new(
        secret       => 'my-api-key',
        endpoint     => 'https://some.postex.nl/api',
        generator_id => '123456789',
        schema       => $zaaksysteem_schema,
    );


=cut

use feature      qw(state);
use MIME::Base64 qw(encode_base64);
use WebService::Postex;
use Zaaksysteem::Types qw(UUID);
use JSON::XS;
use DateTime::Format::ISO8601;
use Carp qw(croak);

with qw(
    Zaaksysteem::Moose::Role::Schema
);

=head1 ATTRIBUTES

=cut

has postex => (
    is       => 'ro',
    isa      => 'WebService::Postex',
    required => 1,
    handles  => [qw(generation_rest_upload)],
);

has communication => (
    is        => 'ro',
    isa       => 'Zaaksysteem::CommunicationTab',
    predicate => 'has_communication',
);

has json => (
    is => 'ro',
    isa => 'JSON::XS',
    lazy => 1,
    builder => '_build_json',
);

sub _build_json {
    return JSON::XS->new()->utf8(1);
}

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;
    my %args = @_;

    my $postex = WebService::Postex->new(
        secret       => delete $args{secret},
        base_uri     => delete $args{base_uri},
        generator_id => delete $args{generator_id},
    );

    return $self->$orig(%args, postex => $postex);
};

=head2 get_transaction

Process a Postex status update in the system

=cut


sub get_transaction {
    my $self = shift;
    my $uuid = shift;

    my $t = $self->schema->resultset('Transaction')
        ->search_rs({ uuid => $uuid})->first;
    return $t if $t;
    die "Unable to find transaction with UUID $uuid";
}

=head2 get_transaction_records

Process a Postex status update in the system

=cut

sub get_transaction_records {
    my $self        = shift;
    my $transaction = shift;
    my $refs        = shift;

    # We want a list context, but it does not exist yet
    $refs = [ $refs ] unless ref $refs;

    my @records = $transaction->transaction_records->search_rs(
        { uuid => { -in => $refs } })->all;

    return @records if scalar @records == scalar @{$refs};
    die "Unable to find all records, from the same transaction";
}

sub _get_files_from_records {
    my $self = shift;
    my @records = @_;

    my @file_ids;
    foreach (@records) {
        my $json = $self->json->decode($_->input());
        push(@file_ids, $json->{file}{id});
    }

    return $self->schema->resultset('File')
        ->search_rs({ id => { -in => \@file_ids } })->all;
}

sub _get_recipient_from_record {
    my $self = shift;
    my $record = shift;

    my $recipient = $self->json->decode($record->input())->{recipient};
    return unless $recipient->{type} eq 'OCR';
    return $self->schema->betrokkene_model->get(
        {
            intern => 1,
            type   => $recipient->{type}
        },
        $recipient->{id}
    );
}

sub _get_case_from_transaction {
    my $self = shift;
    my $transaction = shift;
    my $case_id = $transaction->processor_params->{case_id};
    return $self->schema->resultset('Zaak')->get_case_by_id($case_id);
}

sub _get_sender_from_transaction {
    my $self        = shift;
    my $transaction = shift;
    return $self->_get_sender_by_uuid(
        $transaction->processor_params->{sender_uuid});
}

sub _get_subject_from_transaction {
    my $self        = shift;
    my $transaction = shift;
    return $transaction->processor_params->{subject} // '<geen onderwerp>';
}

sub _get_body_from_transaction {
    my $self        = shift;
    my $transaction = shift;
    return $transaction->processor_params->{body} // '<geen bericht>';
}

sub _get_external_thread_message {
    my $self = shift;
    my $reference = shift;
    my $case_id = shift;

    my $msg = $self->_find_thread_message($reference, $case_id);
    return $msg if $msg;
    die "Unable to find message for reference $reference with case id $case_id";
}

sub _find_thread_message {
    my $self      = shift;
    my $reference = shift;
    my $case_id   = shift;

    my $rs = $self->schema->resultset('ThreadMessage')->search_rs(
        {
            'thread_message_external_id.type' => 'postex',
            external_reference                => $reference,
            'thread_id.case_id'               => $case_id,
        },
        { join => [qw(thread_message_external_id thread_id)] }
    );

    my $msg = $rs->first;
    return $msg if $msg;
    return;
}

sub process_postex_status_update {
    my $self = shift;
    my %postex = @_;

    foreach (qw(mailPackId recordReference transactionReference)) {
        next if defined $postex{mailPackInfo}{$_};
        croak "Unable to process Postex status update, $_ is missing";
    }

    state @processors = qw(
        _process_mailpack_scheduled
        _process_mailpack_delivered
        _process_email_read
        _process_mailpack_returned
        _process_generic_event
    );

    foreach my $processor (@processors) {
        my $rv = $self->$processor(%postex);
        return $rv if $rv;
    }
    die sprintf("Unable to process event type %s",
        $postex{deliveryInfo}{event});
}

sub _is_eventtype {
    my $self = shift;
    my $postex = shift;
    my $event = shift;
    return 1 if $postex->{deliveryInfo}{event} eq $event;
    return 0;
}

sub _add_logging {
    my $self = shift;
    my $case = shift;
    my $type = shift;
    my $data = shift;

    return $case->trigger_logging(
        $type,
        {
            created_by_name_cache => "Postex",
            component             => 'zaak',
            data                  => {
                zaak_id => $case->id,
                %{$data},
            }
        }
    );
}


sub _process_mailpack_scheduled {
    my $self = shift;
    my %postex = @_;

    return unless $self->_is_eventtype(\%postex, "MAILPACK_SCHEDULED");

    my $transaction = $self->_get_transaction_from_postex_call(
        $postex{mailPackInfo}{transactionReference});
    my $case = $self->_get_case_from_transaction($transaction);

    my $sender = $self->_get_sender_from_transaction($transaction);

    my @records = $self->get_transaction_records($transaction,
        $postex{mailPackInfo}{recordReference});

    die "Unable to process MAILPACK_SCHEDULED, no records to be found"
        unless @records;

    my @files = $self->_get_files_from_records(@records);
    my $recipient = $self->_get_recipient_from_record($records[0]);

    my $subtype = ($postex{deliveryInfo}{deliveryMethod} // '') eq 'Hardcopy'
        ? 'mail'
        : 'email';

    # Postex events are unfortunatly not based on the mailpack but on the
    # document level. Which means that every delivery method is suddenly a
    # multi event thing. Which means we need to do all kinds of business logic
    # to replicate what postex does internally. They need to send ONE event for
    # the delivered status with all the required data so we don't need to apply
    # all kinds of logic. But here we are.. :/
    my $external_id = $postex{mailPackInfo}{mailPackId};
    my $thread = $self->_find_thread_message($external_id, $case->id);

    if (!$thread) {
        $self->_add_to_communication_tab(
            sender => $sender->as_object,
            $recipient ? (recipient => $recipient->gm_extern_np->as_object)
                : (),
            subject   => $self->_get_subject_from_transaction($transaction),
            body      => $self->_get_body_from_transaction($transaction),
            files     => \@files,
            case      => $case,
            subtype   => $subtype,
            reference => $external_id,
        );

        $self->_add_logging(
            $case,
            'case/postex/scheduled',
            \%postex,
        );

        return $case;
    }

    my $count = $thread->thread_message_attachments->search_rs(
        {
            filestore_id =>
                { -in => [map { $_->get_column('filestore_id') } @files] }
        }
    );

    die sprintf("Unable to replay event for %s with record reference %s",
        $external_id, $postex{mailPackInfo}{recordReference})
        if $count->count;

    my $tab  = $self->communication;
    foreach my $file (@files) {
        $tab->add_attachment_to_message(
            thread_message => $thread,
            filestore      => $file->filestore_id,
            filename       => $file->filename,
        );
    }

    return $case;
}

sub _process_mailpack_delivered {
    my $self = shift;
    my %postex = @_;

    return unless $self->_is_eventtype(\%postex, "MAILPACK_DELIVERED");

    my $case = $self->get_case_from_transaction_reference(
        $postex{mailPackInfo}{transactionReference}
    );

    my $msg = $self->_get_external_thread_message(
        $postex{mailPackInfo}{mailPackId},
        $case->id
    );

    my $time = $self->_parse_time($postex{deliveryInfo}{eventTimestamp});

    $msg->update(
        {
            message_date  => $time,
            last_modified => '\NOW()',
        }
    );

    $self->_add_logging(
        $case,
        'case/postex/delivered',
        \%postex,
    );

    return $case;
}

sub _parse_time {
  my $self = shift;
  my $time = shift;

  my $dt = DateTime::Format::ISO8601->parse_datetime($time);
  $dt->set_time_zone('UTC');
  return $self->schema->format_datetime_object($dt);
}


sub _process_email_read {
    my $self = shift;
    my %postex = @_;

    return unless $self->_is_eventtype(\%postex, "EMAIL_READ");

    my $case = $self->get_case_from_transaction_reference(
        $postex{mailPackInfo}{transactionReference}
    );

    my $msg = $self->_get_external_thread_message(
        $postex{mailPackInfo}{mailPackId},
        $case->id
    );

    my $ext_message = $msg->thread_message_external_id;

    # No need to update twice
    return $case if $ext_message->read_pip;

    my $time = $self->_parse_time($postex{deliveryInfo}{eventTimestamp});

    $ext_message->update({ read_pip => $time});

    my $thread = $msg->thread_id;
    my $count = $thread->unread_pip_count;

    $count-- if $count > 0;
    $thread->update({unread_pip_count => $count});

    $self->_add_logging(
        $case,
        'case/postex/email_read',
        \%postex
    );

    return $case;
}

sub _process_mailpack_returned {
    my $self = shift;
    my %postex = @_;

    return unless $self->_is_eventtype(\%postex, "MAILPACK_RETURNED");

    my $case = $self->get_case_from_transaction_reference(
        $postex{mailPackInfo}{transactionReference}
    );

    my $msg = $self->_get_external_thread_message(
        $postex{mailPackInfo}{mailPackId},
        $case->id
    );

    # No need to update twice
    my $ext_message = $msg->thread_message_external_id;
    return $case if $ext_message->failure_reason;

    $msg->thread_message_external_id->update(
        { failure_reason => "Bericht kon niet worden afgeleverd" });

    my $log = $self->_add_logging(
        $case,
        'case/postex/returned',
        \%postex
    );

    $case->create_message_for_behandelaar(
        message    => 'Postex bericht retour',
        event_type => 'case/postex/returned',
        log        => $log,
    );

    return $case;
}

sub get_case_from_transaction_reference {
    my $self = shift;
    my $reference = shift;

    my $transaction = $self->_get_transaction_from_postex_call($reference);
    return $self->_get_case_from_transaction($transaction);
}

sub _process_generic_event {
    my $self = shift;
    my %postex = @_;

    my $case = $self->get_case_from_transaction_reference(
        $postex{mailPackInfo}{transactionReference}
    );

    my $msg = $self->_get_external_thread_message(
        $postex{mailPackInfo}{mailPackId},
        $case->id
    );

    my $log_type;
    if ($self->_is_eventtype(\%postex, "DOCUMENT_VIEWED")) {
        $log_type = 'case/postex/document_viewed';
    }
    elsif ($self->_is_eventtype(\%postex, "TRANSACTION_PAGE_VISITED")) {
        $log_type = 'case/postex/transaction_page_visited';
    }
    elsif ($self->_is_eventtype(\%postex, "RETURNEE_HANDLED")) {
        $log_type = 'case/postex/returnee_handled';
    }

    return unless $log_type;

    my $logging = $self->_add_logging(
        $case,
        $log_type,
        \%postex
    );

    if ($self->_is_eventtype(\%postex, "RETURNEE_HANDLED")) {
        $case->create_message_for_behandelaar(
            message    => 'Postex retour afgehandeld',
            event_type => $log_type,
            log        => $logging,
        );
    }

    return $case;
}

sub _get_transaction_from_postex_call {
    my $self = shift;
    my $reference = shift;

    my $transaction = $self->get_transaction($reference);
    die "Unable to process JSON, original record was in error\n"
        if $transaction->is_error;
    return $transaction;
}


=head2 get_case_recipients

Get the recipients by type of a case. In case the type is OCR there are not
recipients as Postex deals with that via OCR

=cut

sub get_case_recipients {
    my ($self, $case, $type, $role) = @_;

    # When OCR is used Postex decides who the recipient is based on OCR.
    return if $type eq 'ocr';
    return $case->get_case_recipients($type, $role);
}

=head2 send_documents_to_postex

Send documents to Postex. This functions deals with all the logic so the caller
doesn't have to.

=cut

define_profile send_documents_to_postex => (
    required => {
        case_id            => 'Int',
        sender_uuid        => UUID,
        recipient_type     => 'Str',
        transaction        => 'Zaaksysteem::Model::DB::Transaction',
        transaction_record => 'Zaaksysteem::Model::DB::TransactionRecord',
        file_attachments   => 'Defined',
    },
    optional => { recipient_role => 'Str', subject => 'Str', body => 'Str' },
);

sub _get_sender_by_uuid {
    my $self = shift;
    my $uuid = shift;
    my $sender = $self->schema->resultset('Subject')
        ->search_rs({ uuid => $uuid }, { rows => 1 })->single;
    return $sender if $sender;
    die "Unable to find sender by UUID $uuid";
}

sub send_documents_to_postex {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $case_id     = $args->{case_id};
    my $schema      = $self->schema;
    my $record      = $args->{transaction_record};
    my $transaction = $args->{transaction};

    # The UUID get's lost
    $transaction->discard_changes;

    my @recipients;
    my $sender;
    my $case;
    my @files;

    try {

        $case = $schema->resultset('Zaak')->get_open_case_by_id($case_id);
        $sender = $self->_get_sender_by_uuid($args->{sender_uuid});

        @recipients = $self->get_case_recipients(
            $case,
            $args->{recipient_type},
            $args->{recipient_role}
        );

        @files = $self->get_files($case, $args->{file_attachments});

        if (!@recipients) {
            $self->upload_files_for_recipient(
                transaction => $transaction,
                case        => $case,
                sender      => $sender,
                files       => \@files,
                subject     => $args->{subject},
                body        => $args->{body},
            );
            return;
        }

        foreach (@recipients) {
            $self->upload_files_for_recipient(
                transaction => $transaction,
                case        => $case,
                sender      => $sender,
                files       => \@files,
                subject     => $args->{subject},
                body        => $args->{body},
                recipient   => $_,
            );
        }
    }
    catch {
        $self->log->info("$_");
        die
            "Postex bericht voor zaak $case_id kon niet correct verwerkt worden";
    };


    return;
}


=head2 upload_files_for_recipient

Call to upload all files for a recipient to Postex

=cut

define_profile upload_files_for_recipient => (
    required => {
        transaction => 'Zaaksysteem::Model::DB::Transaction',
        case        => 'Zaaksysteem::Zaken::ComponentZaak',
        sender      => 'Defined',
        files       => 'Defined',
    },
    optional => {
        recipient => 'Defined',
        subject   => 'Str',
        body      => 'Str'
    },
);

sub upload_files_for_recipient {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my @files = @{ delete $args->{files} };
    foreach (@files) {
        my $record = $self->_upload_file_for_recipient(%$args, file => $_);
        if ($record->is_error) {
            die $record->output;
        }
    }

    return;
}


sub _upload_file_for_recipient {
    my $self = shift;
    my %args = @_;

    my $file = $args{file};

    my $record = $args{transaction}->new_transaction_record(
        preview_string => sprintf(
            "Upload file '%s' for %s",
            $file->filename,
            $args{recipient} ? $args{recipient}->display_name : "OCR"
        ),
    );

    my $result;

    try {
        $result = $self->upload_file_to_postex(
            case               => $args{case},
            sender             => $args{sender},
            subject            => $args{subject},
            body               => $args{body},
            file               => $file,
            recipient          => $args{recipient},
            transaction        => $args{transaction},
            transaction_record => $record,
        );
    }
    catch {
        $self->log->info("$_");
        $record->is_error(1);
        $result = $_;
    }
    finally {

        $record->input($self->json->encode($result->{input}));
        $record->output($self->json->encode($result->{output}));
        $record->update();
    };

    return $record;

}


=head2 upload_file_to_postex

Implements all the logic to create a Postex document request. Returns a
payload hashref containing the messagestructure that can be delivered to Postex

=cut

define_profile upload_file_to_postex => (
    required => {
        case   => 'Zaaksysteem::Zaken::ComponentZaak',
        file   => 'Defined',
        sender => 'Defined',
    },
    optional => {
        subject            => 'Str',
        body               => 'Str',
        recipient          => 'Defined',
        transaction        => 'Zaaksysteem::Model::DB::Transaction',
        transaction_record => 'Defined',
    },
    dependency_groups =>
        { transaction_group => [qw(transaction transaction_record)] }
);

sub upload_file_to_postex {
    my $self    = shift;
    my $options = assert_profile({@_})->valid;

    my $case      = $options->{case};
    my $file      = $options->{file};
    my $recipient = $options->{recipient};

    my %payload = (
        case    => $self->_build_case_metadata($case),
        message => {
            subject => $options->{subject},
            body    => $options->{body}
        },
        files => $self->_build_files_metadata($file),
        $options->{transaction}
        ? (
            transaction => {
                reference => $options->{transaction}->uuid,
                record    =>
                    { reference => $options->{transaction_record}->uuid, }
            }
            )
        : ()
    );


    return $self->_upload_file_to_postex(
        case      => $case,
        sender    => $options->{sender},
        payload   => \%payload,
        recipient => $options->{recipient},
        file      => $options->{file},
    );
}


sub _upload_file_to_postex {
    my $self = shift;
    my %args = @_;

    my $case      = $args{case};
    my $sender    = $args{sender};
    my $recipient = $args{recipient};
    my %payload   = %{ $args{payload} };
    my $file      = $args{file};

    my %recipient_log_data;
    if ($recipient) {
        $payload{recipient} = $self->_build_recipient_data($recipient);
        %recipient_log_data = (
            display_name => $recipient->display_name,
            address      => $recipient->is_briefadres
            ? $recipient->get_full_correspondence_address
            : $recipient->get_full_residence_address,
        );
    }
    else {
        %recipient_log_data = (
            display_name => 'OCR',
            address      => 'OCR',
        );
    }

    my $result = $self->generation_rest_upload(%payload);

    my $subject = $payload{message}{subject} // '<geen onderwerp opgegeven>';
    my $body    = $payload{message}{body}    // '<geen inhoud opgegeven>';

    $case->trigger_logging(
        'case/postex/send',
        {
            component => 'zaak',
            data      => {
                zaak_id    => $case->id,
                ptx_result => $result,
                subject    => $subject,
                body       => $body,
                filenames  => [map { $_->{name} } @{ $payload{files} }],
                recipient  => \%recipient_log_data,
            }
        }
    );

    my $input;
    $input->{file} = { id => $file->id, name => $file->filename };

    if ($recipient) {
        my $type;
        if (
            $recipient->isa(
                'Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon')
            )
        {
            $type = 'natuurlijk_persoon';
        }
        elsif ($recipient->isa('Zaaksysteem::Betrokkene::Object::Bedrijf')) {
            $type = 'bedrijf';
        }

        $input->{recipient} = {
            type => $type,
            id   => $recipient->id,
        };
    }

    $input->{sender} = $sender->id;
    $input->{recipient} //= { type => 'OCR' };

    return { input => $input, output => $result };
}

sub get_files {
    my ($self, $case, $ids) = @_;

    my @files = $case->search_active_files($ids)->all();
    return @files if @files;

    throw("postex/files/missing", "No files attached to Postex");

}

sub _add_to_communication_tab {
    my ($self, %params) = @_;

    return unless $self->has_communication;

    my $tab  = $self->communication;
    my $type = 'postex';

    my $sender    = $params{sender};
    my $recipient = $params{recipient};

    my $thread = $tab->create_thread(
        case         => $params{case},
        thread_type  => 'external',
        message_type => $type,
        subject      => $params{subject},
        slug         => $params{body},
        created_by   => $sender,
        $recipient ? (contact => $recipient) : (),
    );

    my $external = $tab->create_thread_message_external(
        type         => $type,
        subtype      => $params{subtype},
        content      => $params{body},
        subject      => $params{subject},
        direction    => 'outgoing',
        participants => [
            {
                role         => 'to',
                display_name => $recipient
                ? $recipient->display_name
                : 'OCR - Zie bijlage(s)',
                address => 'devnull@postex.com',
            }
        ],
    );

    my $msg = $tab->create_thread_message(
        thread           => $thread,
        external_message => $external,
        created_by       => $sender,
        slug             => $params{body},
        external_reference => $params{reference},
    );

    foreach my $file (@{ $params{files} }) {
        $tab->add_attachment_to_message(
            thread_message => $msg,
            filestore      => $file->filestore_id,
            filename       => $file->filename,
        );
    }
    return $msg;

}

=head2 _build_case_metadata

Build hash with information about a case

=cut

sub _build_case_metadata {
    my ($self, $case) = @_;
    return {
        id              => $case->id,
        case_type_id    => $case->get_column('zaaktype_id'),
        case_type_title => $case->get_casetype_title,
    };
}

=head2 _build_recipient_data_np

Build recipient metadata for NatuurlijkPersoon

=cut

sub _build_recipient_data_np {
    my ($self, $aanvrager) = @_;

    my $adres
        = $aanvrager->is_briefadres
        ? $aanvrager->correspondentieadres
        : $aanvrager->verblijfsadres;

    return {
        type            => 'natuurlijk_persoon',
        bsn             => $aanvrager->bsn,
        display_name    => $aanvrager->display_name,
        voorletters     => $aanvrager->voorletters,
        naam            => $aanvrager->achternaam,
        adellijke_titel => $aanvrager->adellijke_titel,
        email           => $aanvrager->email,
        adres           => $self->_build_address(
            $aanvrager->is_briefadres ? 'correspondentie' : 'verblijf', $adres
        ),
        voorkeurskanaal => $aanvrager->preferred_contact_channel,
    };
}

sub _build_address {
    my ($self, $type, $address) = @_;
    return {
        type                 => $type,
        straatnaam           => $address->straatnaam,
        postcode             => $address->postcode,
        woonplaats           => $address->woonplaats,
        huisnummertoevoeging => $address->huisnummertoevoeging,
        huisletter           => $address->huisletter,
        huisnummer           => $address->huisnummer,
        adres_buitenland1    => $address->adres_buitenland1,
        adres_buitenland2    => $address->adres_buitenland2,
        adres_buitenland3    => $address->adres_buitenland3,
        land                 => $self->_landnaam($address->landcode)
    };
}

=head2 _landnaam

Convert country code integer to Dutch label

=cut

sub _landnaam {
    my ($self, $landcode) = @_;
    return Zaaksysteem::Object::Types::CountryCode->new_from_dutch_code(
        $landcode)->label;
}


=head2 _build_recipient_data_bedrijf

Build recipient metadata for Bedrijf

=cut

sub _build_recipient_data_bedrijf {
    my ($self, $aanvrager) = @_;

    my $type    = $aanvrager->is_briefadres ? "correspondenctie" : "vestiging";
    my $address = $self->_build_address($type, $aanvrager);

    return {
        type        => 'bedrijf',
        handelsnaam => $aanvrager->handelsnaam,
        email       => $aanvrager->email,
        adres       => {
            'tav' => $aanvrager->contact_naam,
            %$address,
        },
        voorkeurskanaal => $aanvrager->preferred_contact_channel,
    };
}


=head2 _build_files_metadata

Takes a case and list of file ids. Encodes the files into base64 (in memory)
and returns an array containing a hash per file entry

Assumes there is enough memory to read a file into memory
Dies if no files are selected

=cut

sub _build_files_metadata {
    my ($self, @files) = @_;

    my @metadata;
    foreach my $file (@files) {

        my $content = encode_base64($file->filestore->content(), '');

        push(
            @metadata,
            {
                data      => $content,
                name      => $file->name,
                extension => $file->extension,
                version   => $file->version,
                md5       => $file->filestore->md5,
                mimetype  => $file->filestore->mimetype,
            }
        );
    }

    return \@metadata;
}


=head2 _build_recipient_data

Builds recipient and address information block meatdata.  Delegates for
NatuurlijkPersoon and Bedrijf. Otherwise die with unsupported message.

=cut

sub _build_recipient_data {
    my ($self, $recipient) = @_;

    if ($recipient->isa('Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon'))
    {
        return $self->_build_recipient_data_np($recipient->current);
    }

    if ($recipient->isa('Zaaksysteem::Betrokkene::Object::Bedrijf')) {
        return $self->_build_recipient_data_bedrijf($recipient->current);
    }

    throw('postex/subject/unsupported_type',
        "Unsupported betrokkene_type: " . $recipient->btype);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
