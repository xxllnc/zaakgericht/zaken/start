=head1 NAME

Zaaksysteem::Manual::API::V1::Controlpanel::Instance - Controlpanel instance retrieval and creation

=head1 Description

This API-document describes the usage of our JSON Controlpanel Instance API. Via the Controlpanel Instance API it
is possible to retrieve, create and edit instances (zaaksystemen) below a customer controlpanel.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/controlpanel/UUID/instance

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Retrieve data

=head2 get

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance/c25d2daf-0e00-4fa5-8bc7-b61fff072234

Retrieving an object from our database is as simple as calling the URL C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance/[UUID]>. You will get
the response in the C<result> parameter. It will only contain one single result, and as always, the C<type>
will tell you what kind of object you received. The C<instance> property will contain the contents of this
object.

B<Example call>

  curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance/c25d2daf-0e00-4fa5-8bc7-b61fff072234

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-8fa423-39fb41",
   "development" : false,
   "result" : {
      "instance" : {
         "api_domain" : "test.api.zaaksysteem.nl",
         "database_provisioned" : null,
         "delete_on" : null,
         "filestore_provisioned" : null,
         "fqdn" : "test.zaaksysteem.nl",
         "hosts" : {
            "rows" : [],
            "type" : "set"
         },
         "id" : "96240fb2-d3b0-4273-b153-e343e01e03d4",
         "label" : "Testomgeving",
         "mail" : null,
         "network_acl" : null,
         "otap" : "production",
         "owner" : "betrokkene-bedrijf-358",
         "password" : "{SSHA}jdkOiHPi02OQwwLUkWlH1D45aHqe8gu2",
         "protected" : true,
         "provisioned_on" : null,
         "template" : "mintlab",
         "freeform_reference": "case-53535",
         "software_version": "production",
         "oidc_organization_id" : null
      },
      "reference" : "96240fb2-d3b0-4273-b153-e343e01e03d4",
      "type" : "instance"
   },
   "status_code" : 200
}

=end javascript

=head2 list

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/controlpanel/CONTROLPANEL_UUID/instance> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Example call>

 curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance/

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-8fa423-e5f43d",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : "https://testsuite/api/v1/controlpanel/9488b6e8-1120-41fe-9fb7-3cb314c7c307/instance?page=2",
            "page" : 1,
            "pages" : 2,
            "prev" : null,
            "rows" : 10,
            "total_rows" : 12
         },
         "rows" : [
            {
               "instance" : {
                  "api_domain" : "test12.api.zaaksysteem.nl",
                  "database_provisioned" : null,
                  "delete_on" : null,
                  "filestore_provisioned" : null,
                  "fqdn" : "test12.zaaksysteem.nl",
                  "hosts" : {
                     "rows" : [],
                     "type" : "set"
                  },
                  "id" : "8a640e96-25fc-43fa-82f0-27e9a18f4a9c",
                  "label" : "Testomgeving 12",
                  "mail" : null,
                  "network_acl" : null,
                  "otap" : "production",
                  "owner" : "betrokkene-bedrijf-366",
                  "password" : null,
                  "protected" : null,
                  "provisioned_on" : null,
                  "template" : "mintlab",
                  "freeform_reference": "case-53535",
                  "software_version": "production",
                  "oidc_organization_id" : null
               },
               "reference" : "8a640e96-25fc-43fa-82f0-27e9a18f4a9c",
               "type" : "instance"
            },
            {
               "instance" : {
                  "api_domain" : "test11.api.zaaksysteem.nl",
                  "database_provisioned" : null,
                  "delete_on" : null,
                  "filestore_provisioned" : null,
                  "fqdn" : "test11.zaaksysteem.nl",
                  "hosts" : {
                     "rows" : [],
                     "type" : "set"
                  },
                  "id" : "0483dc3c-e5b8-42c0-81b4-6e36f2051a88",
                  "label" : "Testomgeving 11",
                  "mail" : null,
                  "network_acl" : null,
                  "otap" : "production",
                  "owner" : "betrokkene-bedrijf-366",
                  "password" : null,
                  "protected" : null,
                  "provisioned_on" : null,
                  "template" : "mintlab",
                  "freeform_reference": "case-53535",
                  "software_version": "production",
                  "oidc_organization_id" : null
               },
               "reference" : "0483dc3c-e5b8-42c0-81b4-6e36f2051a88",
               "type" : "instance"
            }
            [...]
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

=head2 create

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance/create

It is possible to create a controlpanel instance in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item fqdn [required]

B<TYPE>: Fully qualified hostname

The primary hostname this instance will run under

=item label [optional]

B<TYPE>: String

A user defined label for this instance, e.g. "Testomgeving"

=item otap [required]

B<TYPE>: Enum (production accept)

The version to run the software on, e.g. production or acccept

=item mail [optional]

B<TYPE>: Boolean

Whether this host accepts mail or not

=item network_acl [optional]

The network acl list which are allowed to access this host

=item template [optional]

B<TYPE>: Str

The template to use for this zaaksysteem, e.g. mintlab

=item protected [optional]

B<TYPE>: Boolean

Set this to a protected site, no changes can be made on this instance by PIP users

=item api_domain [optional]

The domain the API is working on.

=item password [optional]

B<TYPE>: Plaintext password

The password to use for the user C<beheerder> of this newly created zaaksysteem

=item freeform_reference [optional]

B<TYPE>: Text

The free to use reference field for this instance. Used by website to refer to a case.

=item software_version [optional]

B<TYPE>: ENUM[production]

The software version to run this instance on. For now, only production is available

=item oidc_organization_id [optional]

B<TYPE>: Str

The oidc organization id field for this instance.

=back

B<Example call>

 curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance/create

B<Request JSON JSON>

=begin javascript

{
   "fqdn" : "test1.zaaksysteem.nl",
   "label" : "Testomgeving 1",
   "otap" : "production"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-8fa423-fd44b0",
   "development" : false,
   "result" : {
      "instance" : {
         "api_domain" : "test1.api.zaaksysteem.nl",
         "database_provisioned" : null,
         "delete_on" : null,
         "filestore_provisioned" : null,
         "fqdn" : "test1.zaaksysteem.nl",
         "hosts" : {
            "rows" : [],
            "type" : "set"
         },
         "id" : "c67919fe-ec65-48f2-bc3b-5238cf23a72d",
         "label" : "Testomgeving 1",
         "mail" : null,
         "network_acl" : null,
         "otap" : "production",
         "owner" : "betrokkene-bedrijf-366",
         "password" : null,
         "protected" : null,
         "provisioned_on" : null,
         "template" : "mintlab",
         "oidc_organization_id" : null
      },
      "reference" : "c67919fe-ec65-48f2-bc3b-5238cf23a72d",
      "type" : "instance"
   },
   "status_code" : 200
}

=end javascript

=head2 update

   /api/v1/controlpanel/UUID_CONTROLPANEL/instance/UUID_INSTANCE/update

It is possible to update a controlpanel instance in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item fqdn [required]

B<TYPE>: Fully qualified hostname

The primary hostname this instance will run under

=item label [optional]

B<TYPE>: String

A user defined label for this instance, e.g. "Testomgeving"

=item otap [required]

B<TYPE>: Enum (production accept)

The version to run the software on, e.g. production or acccept

=item mail [optional]

B<TYPE>: Boolean

Whether this host accepts mail or not

=item network_acl [optional]

The network acl list which are allowed to access this host

=item template [optional]

B<TYPE>: Str

The template to use for this zaaksysteem, e.g. mintlab

=item protected [optional]

B<TYPE>: Boolean

Set this to a protected site, no changes can be made on this instance by PIP users

=item api_domain [optional]

The domain the API is working on.

=back

B<Example call>

 curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/instance/9205e6b9-bfed-4f4f-aaa8-aef39bf2e790/update

B<Request JSON JSON>

=begin javascript

{
   "fqdn" : "test.zaaksysteem.nl",
   "label" : "Testomgeving",
   "otap" : "production"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-8fa423-de4a72",
   "development" : false,
   "result" : {
      "instance" : {
         "api_domain" : "test.api.zaaksysteem.nl",
         "database_provisioned" : null,
         "delete_on" : null,
         "filestore_provisioned" : null,
         "fqdn" : "test.zaaksysteem.nl",
         "hosts" : {
            "rows" : [],
            "type" : "set"
         },
         "id" : "594f092f-32b6-41ee-bc91-f03e2c6068c3",
         "label" : "Testomgeving",
         "mail" : null,
         "network_acl" : null,
         "otap" : "production",
         "owner" : "betrokkene-bedrijf-367",
         "password" : null,
         "protected" : null,
         "provisioned_on" : null,
         "template" : "mintlab",
         "oidc_organization_id" : null
      },
      "reference" : "594f092f-32b6-41ee-bc91-f03e2c6068c3",
      "type" : "instance"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 Instance

Most of the calls in this document return an instance of type C<instance>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item id

B<TYPE>: UUID

The unique identifier of this controlpanelinstance. You can use this identifier to request more information about the
related objects of this object, like the download API call.

=item fqdn

B<TYPE>: Fully qualified hostname

The primary hostname this instance will run under

=item label

B<TYPE>: String

A user defined label for this instance, e.g. "Testomgeving"

=item otap

B<TYPE>: Enum (production accept)

The version to run the software on, e.g. production or acccept

=item mail

B<TYPE>: Boolean

Whether this host accepts mail or not

=item network_acl

The network acl list which are allowed to access this host

=item template

B<TYPE>: Str

The template to use for this zaaksysteem, e.g. mintlab

=item protected

B<TYPE>: Boolean

Set this to a protected site, no changes can be made on this instance by PIP users

=item api_domain

B<TYPE>: FQDN

The domain the API is working on.

=item password

B<TYPE>: SSHA hashed password

The hashed password for the primary user

=item owner

B<TYPE>: Identifier of betrokkene (betrokkene-bedrijf-ID)

The owner of this controlpanel, the customer where these panels belong to.

=item status

B<TYPE>: Enum (active processing disabled deleted)

Tells whether the instance is in active, in progress, disabled or deleted

=item oidc_organization_id

B<TYPE>: Str

The oidc organization id.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Controlpanel::Instance>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
