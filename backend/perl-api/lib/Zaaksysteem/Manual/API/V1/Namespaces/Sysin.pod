=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Sysin - Sysin API reference
documentation

=head1 NAMESPACE URL

    /api/v1/sysin

=head1 DESCRIPTION

This page documents the endpoints within the C<sysin> API namespace.

=head1 ENDPOINTS

=head2 Interface endpoints

=head3 C<GET /get_all>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<interface|Zaaksysteem::Manual::API::V1::Types::Interface> instances.

=head3 C<GET /[interface:id]>

Returns a L<interface|Zaaksysteem::Manual::API::V1::Types::Interface>
instance.

=head3 C<POST /[interface:id]/trigger/[trigger:name]>

<undefined>

=head3 C<GET /get_by_module_name/[interface:module]>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<interface|Zaaksysteem::Manual::API::V1::Types::Interface> instances.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
