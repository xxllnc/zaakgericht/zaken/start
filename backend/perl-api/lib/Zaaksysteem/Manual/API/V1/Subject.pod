=head1 NAME

Zaaksysteem::Manual::API::V1::Subject - Creating and listing subjects.

=head1 Description

This API-document describes the usage of our JSON Subject API. Via the Subject API it is possible
to create, list and retrieve subjects of types C<company>, C<person> and C<employee>

=head2 API

This document is based on our version 1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 Subject Types

A subject type can be one of the following:

=over

=item employee

An employee, or a user which can login to B<zaaksysteem.nl>. This API allows you to C<retrieve> subjects
of this type

=item company

A company, or a user which can login via eHerkenning to their PIP. This API allows you to C<create> and C<retrieve>
subjects of this type

=item person

A person, or a user which can login via DigiD to their PIP. This API allows you to C<create> and C<retrieve>
subjects of this type

=back

=head2 URL

The base URL for this API is:

    /api/v1/subject

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for creates, updates and removal requests.

=head1 Retrieve data

=head2 get

Retrieving aa subject from our database is as simple as calling the URL C</api/v1/subject/[UUID]>. You will get
the response in the C<result> parameter. It will only contain one single result. The C<instance> property will contain the contents of this
subject, the C<subject_type> within the instance will tell you what kind of subject you retrieved (see L<SUBJECT TYPES>).

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/subject/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "vagrant-61b728-4fb097",
   "development" : false,
   "result" : {
      "instance" : {
         "authentication" : {
            "instance" : {
               "username" : "dedon",
            },
            "reference" : null,
            "type" : "authentication"
         },
         "date_created" : "2016-04-05T14:10:09Z",
         "date_modified" : "2016-04-05T14:10:09Z",
         "display_name" : "EB de TestpartnernaamGOOD",
         "external_subscription" : {
            "instance" : {
               "date_created" : "2016-04-05T14:10:09Z",
               "date_modified" : "2016-04-05T14:10:09Z",
               "external_identifier" : "155",
               "interface_uuid" : "130c158b-cdff-4896-b031-c70c1e8310e6",
               "internal_identifier" : 120
            },
            "reference" : null,
            "type" : "external_subscription"
         },
         "old_subject_identifier" : "betrokkene-natuurlijk_persoon-297",
         "subject" : {
            "instance" : {
               "address_correspondence" : null,
               "address_residence" : {
                  "instance" : {
                     "city" : "Fkpnirkyq",
                     "country" : {
                        "instance" : {
                           "alpha_one" : null,
                           "alpha_two" : null,
                           "alpha_three" : null,
                           "code" : null,
                           "dutch_code" : "6030",
                           "label" : "Nederland"
                        },
                        "reference" : null,
                        "type" : "country_code"
                     },
                     "date_created" : "2016-04-05T14:10:09Z",
                     "date_modified" : "2016-04-05T14:10:09Z",
                     "foreign_address_line1" : null,
                     "foreign_address_line2" : null,
                     "foreign_address_line3" : null,
                     "municipality" : null,
                     "street" : "Oqeptmbnrmchmgzyv",
                     "street_number" : 55,
                     "street_number_letter" : "U",
                     "street_number_suffix" : "YG",
                     "zipcode" : "1897BX"
                  },
                  "reference" : null,
                  "type" : "address"
               },
               "date_created" : "2016-04-05T14:10:09Z",
               "date_modified" : "2016-04-05T14:10:09Z",
               "date_of_birth" : "1980-05-19T00:00:00Z",
               "date_of_death" : null,
               "family_name" : "Vos",
               "first_names" : "Siem",
               "gender" : "M",
               "initials" : "EB",
               "is_local_resident" : false,
               "is_secret" : false,
               "partner" : {
                  "instance" : {
                     "address_correspondence" : null,
                     "address_residence" : null,
                     "date_created" : "2016-04-05T14:10:09Z",
                     "date_modified" : "2016-04-05T14:10:09Z",
                     "date_of_birth" : null,
                     "date_of_death" : null,
                     "family_name" : "TestpartnernaamGOOD",
                     "first_names" : null,
                     "gender" : null,
                     "initials" : null,
                     "is_local_resident" : null,
                     "is_secret" : null,
                     "partner" : null,
                     "personal_number" : "074081469",
                     "personal_number_a" : "9482472496",
                     "place_of_birth" : null,
                     "prefix" : "de",
                     "surname" : "de TestpartnernaamGOOD",
                     "use_of_name" : null
                  },
                  "reference" : null,
                  "type" : "person"
               },
               "personal_number" : "229886929",
               "personal_number_a" : "1335393036",
               "place_of_birth" : null,
               "prefix" : null,
               "surname" : "de TestpartnernaamGOOD",
               "use_of_name" : "P"
            },
            "reference" : "39d27eba-ae3a-4d92-9854-b118fc271b3d",
            "type" : "person"
         },
         "subject_type" : "person"
      },
      "reference" : "39d27eba-ae3a-4d92-9854-b118fc271b3d",
      "type" : "subject"
   },
   "status_code" : 200
}

=end javascript

=head2 list

Retrieving a list of subjects from our database is as simple as calling the URL C</api/v1/subject]>. You will get
the response in the C<result> parameter. It will only contain one single result. The C<instance> property will contain the contents of this
subject, the C<subject_type> within the instance will tell you what kind of subject you retrieved (see L<SUBJECT TYPES>).

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/subject

B<Properties>

Use our JSON query DSL for querying our subject.

B<Request JSON>

An empty request body suffices

=begin javascript

{
  "query": {
    "match": {
      "subject_type": "person",
      "subject.geslachtsnaam": "Ootjers",
      "subject.address_residence.street": "teststraat"
    }
  }
}

=end javascript

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-8cfb5d-45d0cd",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "authentication" : null,
                  "date_created" : "2016-04-07T10:08:56Z",
                  "date_modified" : "2016-04-07T10:08:56Z",
                  "display_name" : "F. de Boer",
                  "external_subscription" : null,
                  "old_subject_identifier" : "betrokkene-medewerker-32",
                  "subject" : {
                     "instance" : {
                        "address_correspondence" : null,
                        "address_residence" : null,
                        "date_created" : "2016-04-07T10:08:56Z",
                        "date_modified" : "2016-04-07T10:08:56Z",
                        "display_name" : "F. de Boer",
                        "first_names" : "Fritsie",
                        "gender" : null,
                        "initials" : "F",
                        "surname" : "de Boer"
                     },
                     "reference" : "e9bde7ee-ea30-4999-822a-b6ee97b42dab",
                     "type" : "employee"
                  },
                  "subject_type" : "employee"
               },
               "reference" : "e9bde7ee-ea30-4999-822a-b6ee97b42dab",
               "type" : "subject"
            },
            {
               "instance" : {
                  "authentication" : null,
                  "date_created" : "2016-04-07T10:08:56Z",
                  "date_modified" : "2016-04-07T10:08:56Z",
                  "display_name" : "F. de Boer",
                  "external_subscription" : null,
                  "old_subject_identifier" : "betrokkene-medewerker-33",
                  "subject" : {
                     "instance" : {
                        "address_correspondence" : null,
                        "address_residence" : null,
                        "date_created" : "2016-04-07T10:08:56Z",
                        "date_modified" : "2016-04-07T10:08:56Z",
                        "display_name" : "W. de Jan",
                        "first_names" : "Wim",
                        "gender" : null,
                        "initials" : "F",
                        "surname" : "de Jan"
                     },
                     "reference" : "e9bde7ee-ea30-4999-822a-b6ee97b42dac",
                     "type" : "employee"
                  },
                  "subject_type" : "employee"
               },
               "reference" : "e9bde7ee-ea30-4999-822a-b6ee97b42dab",
               "type" : "subject"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

=head2 create

   /api/v1/subject/create

Creating a subject requires a JSON body containing at least a C<subject> object and a <subject_type>. Via this
API you are able to create subjects of the two types C<person> and C<company>. Make sure you use the C<POST> method
for posting data to this endpoint. The response is identical to the response of the C<api/v1/subject/UUID> call.

B<Example call>

 curl --anyauth -H "Content-Type: application/json" --data @file_with_request_json --digest -u "username:password" https://localhost/api/v1/subject/create

B<Request JSON JSON>

=begin javascript

{
     "subject_type": "person",
     "authentication": {
         "username": "dedon",
         "password": "N0tA3asy1ToCr@ck",
     },
     "subject": {
          "address_residence": {
               "city": "Ozcmowpqp",
               "country": {
                    "dutch_code": "6030",
                    "label": "Nederland"
               },
               "street": "Ekqoinmixaxvnwdxv",
               "street_number": "25",
               "street_number_letter": "F",
               "street_number_suffix": "KY",
               "zipcode": "1105TV"
          },
          "date_of_birth": "1992-05-13T00:00:00Z",
          "family_name": "Muler",
          "first_names": "Frits",
          "gender": "M",
          "initials": "EN",
          "partner": {
               "family_name": "TestpartnernaamGOOD",
               "personal_number": "074081469",
               "personal_number_a": "9482472496",
               "prefix": "de",
          },
          "personal_number": "229886929",
          "personal_number_a": "1986532732",
          "surname": "de TestpartnernaamGOOD",
          "use_of_name": "P"
     }
}

=end javascript

B<Response JSON JSON>: See B<get>

=head2 import

  /api/v1/subject/import

Imports a C<subject> from a remote using a subset of the query language used
by Elastic Search.

At the moment, only importing of companies is supported, using the following
fields:

=over

=item * C<subject_type>

Always C<company> (other subject types are not yet supported by this API)

=item * C<subject.coc_number>

The KvK number of the company being searched for.

=item * C<subject.coc_location_number>

The KvK "vestigingsnummer" of the company being searched for.

Specifying this field is optional if the company only has one "vestiging".

=back

B<Example call>

  curl \
    -H "Content-Type: application/json" \
    --data @file_with_request_json \
    --digest \
    -u "username:password" \
    https://localhost/api/v1/subject/import

B<Request body JSON>

=begin javascript

{
  "query": {
    "match":{
      "subject_type": "company",
      "subject.coc_number": "90001966",
      "subject.coc_location_number": "990000246858"
    }
  }
}

=end javascript

B<Response body JSON>

See L</get>.

If no subject is found, or multiple subjects are found matching the specified
search criteria, an exception will be thrown.

If there is no active KvK API interface, an exception will also be thrown.

=head1 Objects

=head2 Subject

Most of the calls in this document return an instance of type C<subject>. Which is a container for holding
the various subject types. These types contain references to the C<authentication>, C<address> and C<external_subscription>
objects.

Below a description of our root C<subject> object with links to the related objects.

=over 4

=item subject_type

B<Type>: ENUM (person, company, employee)

=item subject

B<Type>: Related object

This contains one of the following related objects:

L<Zaaksysteem::Object::Types::Person>

L<Zaaksysteem::Object::Types::Company>

L<Zaaksysteem::Object::Types::Employee>

=item external_subscription

B<Type>: L<Zaaksysteem::Object::Types::ExternalSubscription>

Not every subject from within Zaaksysteem.nl. Which means there is another party (a CRM, BRP System, etc) in
play with their own reference to this subject.

By keeping track of these references between the two, we will be able to send various hooks to these systems.

=item display_name

B<Type>: Text

A human readable name for this subject. For companies this would be the name of the company, for persons this
would be their initials, prefix and surname.

=item old_subject_identifier

B<Type>: betrokkene-IDENTIFIER-INTEGER (IDENTIFIER: one of C<natuurlijk_persoon>,C<bedrijf> or C<medewerker>)

Before we worked with uuids in our application, this were the unique identifier for the various subjects. Please
stop using this information unless you know what you are doing.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Subject>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
