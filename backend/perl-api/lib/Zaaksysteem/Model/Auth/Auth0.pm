package Zaaksysteem::Model::Auth::Auth0;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Auth::Auth0::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Auth::Auth0 - Per-request
L<Zaaksysteem::Auth::Auth0::Model> factory.


=head1 SYNOPSIS

    my $model = $c->model('Auth::Auth0');

=cut


=head1 METHODS

=head2 prepare_arguments

Prepares the instantiation arguments for the model instance.

=cut

sub prepare_arguments {
    my ($self, $c, @args) = @_;

    return {
        redis => $c->model('Redis'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
