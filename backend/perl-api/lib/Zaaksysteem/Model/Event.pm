package Zaaksysteem::Model::Event;
use Moose;
use namespace::autoclean;

use BTTW::Tools;

extends 'Catalyst::Model::Factory';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Event::Model',
    constructor => 'new',
);

=head1 NAME

Zaaksysteem::Model::Event - Catalyst model factory for broadcasting events
for the new Python-based system.

=head1 DESCRIPTION

Implements a L<Catalyst::Model::Factory> for L<Zaaksysteem::Event::Model>.

=head1 SYNOPSIS

    my $model = $c->model('Event');

=head1 METHODS

=head2 prepare_arguments

Implements argument preparation for model instantiation. This preparation will
search Zaaksysteem for configured event integrations.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    my $exchange = $c->config->{'Model::Queue'}{event_exchange};

    if (!$exchange) {
        $c->log->warn(
            "Falling back to configured Model::Queue->exchange; please configure 'event_exchange' in Model::Queue block"
        );

        $exchange = $c->config->{'Model::Queue'}{exchange} // 'amq.topic';
    }

    return {
        channel  => ($c->config->{'Model::Queue'}{channel} // 1),
        exchange => $exchange,
        amqp_factory => sub { return $c->model('AMQP') },
    };
}

sub mangle_arguments {
    my ($self, $args) = @_;

    return %$args;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
