package Zaaksysteem::Object::Constants;

use warnings;
use strict;
use utf8;

use Exporter qw[import];

use Zaaksysteem::Object::Process;
use BTTW::Tools;

our @EXPORT = qw[
    OBJECT_TYPES
    ATTRIBUTE_TYPES
    SEARCH_OBJECT_TYPE_BLACKLIST
];

our @EXPORT_OK = qw[
    OBJECT_TYPE_NAME_MAP
    DEFAULT_ENVIRONMENT_VARIABLES
];

=head1 NAME

Zaaksysteem::Object::Constants - Constants for the "Object"

=head1 CONSTANTS

=head2 OBJECT_TYPES

A hash of database-level object types, mapped to the role(s) that should be
applied to the L<Zaaksysteem::Object::Data::Component> object.

=over 4

=item case

L<Zaaksysteem::Object::Data::Roles::Case>

=item type

L<Zaaksysteem::Object::Data::Roles::Type>

=item product

L<Zaaksysteem::Object::Data::Roles::Product>

=item saved_search

L<Zaaksysteem::Object::Data::Roles::SavedSearch>

=back

=cut

use constant OBJECT_TYPES    => {
    case            => 'Zaaksysteem::Backend::Object::Data::Roles::Case',
    type            => 'Zaaksysteem::Backend::Object::Data::Roles::Type',
    casetype        => 'Zaaksysteem::Backend::Object::Data::Roles::Casetype',
    saved_search    => 'Zaaksysteem::Backend::Object::Data::Roles::SavedSearch',
};

=head2 ATTRIBUTE_TYPES

A hash of database-level attribute types, mapped to the packages that
implement type-specific behaviors.

=over 4

=item bag

L<Zaaksysteem::Object::Attribute::BAG>

=item boolean

L<Zaaksysteem::Object::Attribute::Boolean>

=item timestamp

L<Zaaksysteem::Object::Attribute::Timestamp>

=item number, integer

L<Zaaksysteem::Object::Attribute::Number>

=item object

L<Zaaksysteem::Object::Attribute::Object>

=item timestamp_or_text

L<Zaaksysteem::Object::Attribute::TimestampOrText>

=back

=cut

use constant ATTRIBUTE_TYPES => {
    appointment             => [qw[]],
    bag                     => [qw[BAG]],
    boolean                 => [qw[Boolean]],
    casetype                => [qw[]],
    category                => [qw[]],
    checkbox                => [qw[Checkbox]],
    hidden                  => [qw[]],
    integer                 => [qw[Number]],
    number                  => [qw[Number]],
    object                  => [qw[Object]],
    'org-unit'              => [qw[]],
    radio                   => [qw[]],
    richtext                => [qw[]],
    textarea                => [qw[]],
    text                    => [qw[]],
    email                   => [qw[]],
    bankaccount             => [qw[]],
    bag_straat_adres        => [qw[]],
    bag_straat_adressen     => [qw[]],
    bag_adres               => [qw[]],
    bag_adressen            => [qw[]],
    bag_openbareruimte      => [qw[]],
    bag_openbareruimtes     => [qw[]],
    valuta                  => [qw[Number]],
    valutain                => [qw[Number]],
    valutaex                => [qw[Number]],
    valutain6               => [qw[Number]],
    valutaex6               => [qw[Number]],
    valutain21              => [qw[Number]],
    valutaex21              => [qw[Number]],
    subject                 => [qw[Subject]],
    select                  => [qw[]],
    url                     => [qw[]],
    image_from_url          => [qw[]],
    text_uc                 => [qw[]],
    numeric                 => [qw[]],
    date                    => [qw[Timestamp]],
    timestamp_or_text       => [qw[TimestampOrText]],
    timestamp               => [qw[Timestamp]],
    file                    => [qw[Filestore]],
    datestamp               => [qw[Datestamp]],
    googlemaps              => [qw[]],
    geolatlon               => [qw[]],
    option                  => [qw[]],
    calendar                => [qw[]],
    calendar_supersaas      => [qw[]],
    'object-attribute-list' => [qw[]],
    deadline                => [qw[Deadline]],
    confidentiality         => [qw[]],
    payment                 => [qw[]],
};

=head2 SEARCH_OBJECT_TYPE_BLACKLIST

This constant exports a list of object type prefixes that should not be
included in autocomplete or spot-enlighter resultsets.

=over 4

=item case

Cases are a special case in object search.

=item casetype

Casetypes are management objects, and won't be seached for by the primary
users.

=item customer_d

CustomerD objects are system-internal.

=item scheduled_job

ScheduledJob objects are system-internal.

=item type

Types are management objects, and won't be searched for by the primary users.

=back

=cut

use constant SEARCH_OBJECT_TYPE_BLACKLIST => [qw[
    case
    casetype
    customer_d
    scheduled_job
    saved_search
    type
]];

=head2 OBJECT_TYPE_NAME_MAP

Mapping of object type names to their human-readable equivalents.

=cut

use constant OBJECT_TYPE_NAME_MAP => {
    'scheduled_job' => 'Geplande zaak',
    'casetype'      => 'Zaaktype',
    'case'          => 'Zaak',
};

use constant DEFAULT_ENVIRONMENT_VARIABLES => {
    stack => [],

    set => Zaaksysteem::Object::Process->new(
        description => 'Set object attribute value',
        environment_profile => {
            object => 'Zaaksysteem::Object::Reference',
            name => 'Str',
            value => 'Any'
        },
        implementation => sub {
            my ($self, $env) = @_;

            my $object = $env->get('object')->_instance;

            my $attr = $object->object_attribute($env->get('name'));

            unless ($attr) {
                throw('object/process/set/invalid_attribute_name', sprintf(
                    'Attribute "%s" does not exist for "%s"',
                    $env->get('name'),
                    $object
                ));
            }

            $attr->set_value($object, $env->get('value'));

            push @{ $env->get('stack') }, $self->action('save_object', {
                object => $object,
                env => $env
            });

            push @{ $env->get('stack') }, $self->action('log_event', {
                type => 'object/attribute/set_value',
                data => {
                    attribute_name => $env->get('name'),
                    attribute_value => $env->get('value')
                }
            });

            return;
        }
    ),

    get => Zaaksysteem::Object::Process->new(
        description => 'Get object attribute value',
        log_call => 0,
        environment_profile => {
            object => 'Zaaksysteem::Object::Reference',
            name => 'Str'
        },
        implementation => sub {
            my ($self, $env) = @_;

            my $object = $env->get('object')->_instance;
            my $attr = $object->object_attribute($env->get('name'));

            unless ($attr) {
                throw('object/process/get/invalid_attribute_name', sprintf(
                    'Attribute "%s" does not exist for "%s"',
                    $env->get('name'),
                    $object
                ));
            }

            return $attr->get_value($object);
        }
    ),

    is_set => Zaaksysteem::Object::Process->new(
        description => 'Test object attribute value is set',
        log_call => 0,
        environment_profile => {
            object => 'Zaaksysteem::Object::Reference',
            name => 'Str'
        },
        implementation => sub {
            my ($self, $env) = @_;

            my $object = $env->get('object')->_instance;
            my $attr = $object->object_attribute($env->get('name'));

            unless ($attr) {
                throw('object/process/is_set/invalid_attribute_name', sprintf(
                    'Attribute "%s" does not exist for "%s"',
                    $env->get('name'),
                    $object
                ));
            }

            return $attr->has_value($object);
        }
    ),

    unset => Zaaksysteem::Object::Process->new(
        description => 'Unset object attribute value',
        environment_profile => {
            object => 'Zaaksysteem::Object::Reference',
            name => 'Str'
        },
        implementation => sub {
            my ($self, $env) = @_;

            my $object = $env->get('object')->_instance;
            my $attr = $object->object_attribute($env->get('name'));

            unless ($attr) {
                throw('object/process/unset/invalid_attribute_name', sprintf(
                    'Attribute "%s" does not exist for "%s"',
                    $env->get('name'),
                    $object
                ));
            }

            push @{ $env->get('stack') }, $self->action('save_object'),
            push @{ $env->get('stack') }, $self->action('log_event', {
                type => 'object/attribute/unset_value',
                data => {
                    attribute_name => $env->get('name'),
                }
            });

            return $attr->clear_value($object);
        }
    ),

    log => Zaaksysteem::Object::Process->new(
        description => 'Log message',
        log_call => 0,
        environment_profile => {
            level => 'Str',
            message => 'Str'
        },
        implementation => sub {
            my ($self, $env) = @_;

            my $logger = $self->log->can($env->get('level'));

            unless ($logger) {
                $logger->info(sprintf(
                    'Process attempted logging to invalid loglevel (%s): %s',
                    $env->get('level'),
                    $env->get('message')
                ));
            }

            $logger->($env->get('message'));

            return;
        }
    )
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

