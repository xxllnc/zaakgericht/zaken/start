package Zaaksysteem::Object::Mutation::Model;

use Moose;
use namespace::autoclean;

with qw[
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::Object::Mutation::Model - Interaction model for the case object
mutations.

=head1 SYNOPSIS

    my @mutations = $model->search({ object_type => 'plantenbak' });

=cut

use BTTW::Tools;

=head1 ATTRIBUTES

=head2 mutation_resultset

Reference to the table of current mutations for a given context.

The instantiating code is responsible for setting the context.

=cut

has mutation_resultset => (
    is => 'ro',
    isa => 'DBIx::Class::ResultSet',
    required => 1
);

=head2 logging_resultset

Reference to the logging table, containing C<case/object/[object_type]>
events.

The instantiating code is responsible for setting the context of this
resultset.

=cut

has logging_resultset => (
    is => 'ro',
    isa => 'DBIx::Class::ResultSet',
    required => 1
);

=head2 object_model

Reference to the object model, for the purpose of validating mutations.

=cut

has object_model => (
    is => 'ro',
    isa => 'Zaaksysteem::Object::Model',
    required => 1
);

=head2 ztt_cache

Cache blob for the ZTT evaluation of mutation labels.

=cut

has ztt_cache => (
    is => 'ro',
    isa => 'HashRef',
    default => sub { return {}; }
);

=head2 type_cache

Cache map for object types for evaluation of mutation labels.

=cut

has type_cache => (
    is => 'ro',
    isa => 'HashRef[Zaaksysteem::Object::Types::Type]',
    default => sub { return {}; },
    traits => [qw[Hash]],
    handles => {
        is_type_cached => 'exists',
        set_type_cache => 'set',
        get_type_cache => 'get'
    }
);

=head1 METHODS

=head2 search

Search for case-contextualized mutations (current and historic) by merging
the mutations and logging resultsets and composing mutation 'rows'.

    my @mutations = $model->search({ object_type => 'plantenbak' });

=cut

define_profile search => (
    optional => {
        object_type => 'Str'
    }
);

sub search {
    my $self = shift;
    my $args = assert_profile(shift || {})->valid;

    my @mutations;

    my $mutation_rs = $self->mutation_resultset->search($args, {
        order_by => 'date_created'
    });

    my $om = $self->object_model;

    while (my $mutation = $mutation_rs->next) {
        push @mutations, $mutation;

        # Exception implies invalid/incomplete state of mutation.
        my $object = try {
            $om->validate_mutation($mutation)
        };

        if (defined $object) {
            $mutation->complete(1);
            $mutation->label($object->TO_STRING);
        } else {
            # We try our best to compose a sensible title for the
            # invalid/incomplete mutation.
            my $type = $om->search('type', {
                prefix => $mutation->object_type
            })->next;

            my $name;

            if ($type->has_title_template) {
                my $ztt = Zaaksysteem::ZTT->new(cache => $self->ztt_cache);

                # Add every attribute<->value we have up to this point
                $ztt->add_context(
                    $om->translate_object_data_attributes(
                        $mutation->values
                    )
                );

                my $tpl = $ztt->process_template($type->title_template);

                $name = $tpl->string;
            }

            # final fallback, just the type name for a label...
            unless ($name) {
                $name = $type->name;
            }

            $mutation->complete(0);
            $mutation->label($name);
        }
    }

    my $logging_args = { event_type => { ILIKE => 'case/object/%' } };
    
    if ($args->{ object_type }) {
        $logging_args = {
            -and => [
                $logging_args,
                \[
                    "event_data::json->'object_type' == ?",
                    [ {}, $args->{ object_type } ]
                ]
            ]
        };
    }

    my $logging_rs = $self->logging_resultset->search($logging_args, {
        order_by => { -desc => 'created' }
    });

    while (my $event = $logging_rs->next) {
        my $event_data = $event->event_data;

        my $mutation_type = (split m[/], $event->event_type)[-1];
        my %values = map {
            $_->{ field } => $_->{ new_value }
        } @{ $event_data->{ changes } };

        my $mutation = $mutation_rs->new_result({
            object_uuid => $event->get_column('object_uuid'),
            lock_object_uuid => undef,
            object_type => $event_data->{ object_type },
            type => $mutation_type,
            executed => 1,
            values => \%values
        });

        $mutation->read_only(1);
        $mutation->complete(1);
        $mutation->label($event_data->{ object_label });

        push @mutations, $mutation;
    }

    return @mutations;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
