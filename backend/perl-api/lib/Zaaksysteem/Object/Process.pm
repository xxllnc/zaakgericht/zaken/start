package Zaaksysteem::Object::Process;

use Moose;

use Moose::Util::TypeConstraints qw[find_type_constraint class_type];

use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Process - Base class for process abstraction on objects

=head1 DESCRIPTION

This class provides a data wrapper for abstracted object processes.

=head1 ATTRIBUTES

=head2 description

Holds a short description of the process's task. It can be used for arbitrairy
modules that interact with processes and supply feedback to the user about
processes being evaluated.

=cut

has description => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

=head2 log_call

Defines whether evaluating the process should generate a log event. This is
so that idempotent or inconsequential processes don't spam the log.

=cut

has log_call => (
    is => 'ro',
    isa => 'Bool',
    required => 1,
    default => 1
);

=head2 environment_profile

Holds a variable-type mapping to be used for validating the environment
provided when evaluating the process.

=cut

has environment_profile => (
    is => 'ro',
    isa => 'HashRef',
    traits => [qw[Hash]],
    required => 1,
    default => sub { return {}; },
    handles => {
        set_variable_type => 'set',
        get_variable_type => 'get',
        env_variable_types => 'kv'
    }
);

=head2 implementation

Holds a CodeRef which executes the process' logic.

=cut

has implementation => (
    is => 'ro',
    isa => 'CodeRef',
    traits => [qw[Code]],
    required => 1,
    handles => {
        eval => 'execute_method'
    }
);

=head1 METHODS

=head2 eval

Evaluate the process in an L<environment|Zaaksysteem::Object::Environment>.

    my $result = $process->eval($env);

=cut

sig eval => 'Zaaksysteem::Object::Environment';

before eval => sub {
    my ($self, $env) = @_;

    return unless $self->log_call;

    $env->process('log', {
        level => 'info',
        message => sprintf('Process "%s" started', $self->description)
    });
};

before eval => sub {
    my ($self, $env) = @_;

    my @errors;

    for ($self->env_variable_types) {
        my ($name, $type) = @{ $_ };

        unless ($env->exists($name)) {
            push @errors, {
                name => $name,
                missing => 1,
                message => sprintf(
                    'Environment variable "%s" is expected but missing',
                    $name
                )
            };

            next;
        }

        my $constraint = find_type_constraint($type) || class_type($type);

        my $value = $env->get($name);

        unless ($constraint->check($value)) {
            push @errors, {
                name => $name,
                invalid => 1,
                message => sprintf(
                    'Environment variable "%s" is invalid: %s',
                    $name,
                    $constraint->get_message($value)
                )
            };
        }
    }

    return 1 unless scalar @errors;

    my @invalid = map { $_->{ name } } grep { $_->{ invalid } } @errors;
    my @missing = map { $_->{ name } } grep { $_->{ missing } } @errors;

    throw(
        'object/process/environment_validation_failure',
        sprintf(
            'Process environment validation failure(s): %s%s',
            join(', ', map { "$_: missing" } @missing),
            join(', ', map { "$_: invalid" } @invalid)
        ),
        \@errors
    );
};

=head2 action

Convenience method for constructing a side-effect-like action within a
process.

    my $action = $process->action('name', $arg1, $arg2);

=cut

sig action => 'Str, @Any';

sub action {
    my $self = shift;

    return { name => shift, args => \@_ };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
