package Zaaksysteem::Object::Queue::Model::ObjectMutation;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::ObjectMutation - Object mutation queue
item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 mutate_object

=cut

sig mutate_object => 'Zaaksysteem::Backend::Object::Queue::Component';

sub mutate_object {
    my $self = shift;
    my $item = shift;

    my $model = $self->object_model;
    my $case = $item->object_data;

    my $mutations = $case->object_mutation_lock_object_uuids->search({
        object_type => $item->data->{ object_type_prefix }
    });

    for my $mutation ($mutations->all) {
        try {
            my $data = $model->mutate($mutation);

            $mutation->delete;

            $data->{ object_type_name } = $item->data->{ object_type_name };

            my $event = $case->get_source_object->trigger_logging(
                sprintf('case/object/%s', $mutation->type),
                {
                    object_uuid => delete $data->{ object_uuid },
                    data => $data
                }
            );

            $self->log->info(sprintf(
                'Processed object mutation "%s"', $mutation->stringify
            ));
        } catch {
            $self->log->error(sprintf(
                'Caught exception while processing object mutation "%s": %s',
                $mutation->stringify,
                $_
            ));

            $mutation->update({ executed => 1 });
        }
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
