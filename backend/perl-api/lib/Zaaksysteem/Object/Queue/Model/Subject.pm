package Zaaksysteem::Object::Queue::Model::Subject;

use Moose::Role;

requires qw(build_resultset);

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Subject - Subject queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 update_cases_of_deceased

    $self->update_cases_of_deceased(
        $schema->resultset('Queue')->first
    );

Will touch all related cases of given queue item. This way, cases of deceased persons will be updated
with the proper "decease" data

=cut

sig update_cases_of_deceased => 'Zaaksysteem::Backend::Object::Queue::Component';

sub update_cases_of_deceased {
    my $self = shift;
    my $item = shift;

    my $natuurlijk_persoon_id   = $item->data->{natuurlijk_persoon_id};

    my $cases = $self->_retrieve_cases_for_aanvrager(
        $item->result_source->schema, 
        {
            natuurlijk_persoon_id   => $natuurlijk_persoon_id,
        }
    );

    $self->log->debug('Touching cases for deceased person with natuurlijk_persoon_id: ' . $natuurlijk_persoon_id);
    $self->_touch_cases($cases);

    return 1;
}

sub export_login_attemtps {
    my ($self, $item) = @_;

    my $data = $item->data;

    my $user = $self->find_subject($data->{subject_id});

    my $rs = $self->schema->resultset('SubjectLoginHistory');
    $rs = $rs->search_rs(
        { subject_id => $data->{download_subject_id} },
        {
            order_by => { -desc => 'date_attempt' },
            columns  => [
                qw(
                    success
                    method
                    date_attempt
                    )
            ]
        }
    );

    my $format = $data->{format} // 'csv';

    my $formatting = Zaaksysteem::Export::CSV->new(
        user         => $user,
        objects      => $rs,
        format       => $format,
        attr_order   => [qw(method date_attempt success)],
        column_order => [qw(Logintype Datum Ingelogd)],
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $hostname = $item->hostname // $self->base_uri;
    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $hostname),
    );

    my $filename = $export->generate_filename('login-historie',
        $format eq 'calc' ? 'ods' : $format,
    );

    $export->add_as_export($fh, $filename);
}

sub update_contact_details_for_cases {
    my ($self, $item) = @_;

    my $uuid = $item->data->{uuid};

    my $found = $self->build_resultset('NatuurlijkPersoon')->search_rs(
        { uuid => $uuid }
    )->first;

    $found //= $self->build_resultset('Bedrijf')->search_rs(
        { uuid => $uuid }
    )->first;

    if (!$found) {
        throw('subject/update_contact_details_for_cases/unknown_uuid',
            "$uuid not found");
    }

    $found->update_contact_details_for_cases();
}

sub update_assignee_or_coordinator_on_case {
    my $self = shift;
    my $item = shift;

    my $subject_id = $item->subject_id;

    my $cases = $self->build_resultset("Zaak")->search_rs(
        {
            deleted => undef,
            status  => { -in => [qw(open new stalled)] },
            -and    => {
                -or => [
                    { behandelaar_gm_id => $subject_id },
                    { coordinator_gm_id => $subject_id }
                ]
            },
        }
    );

    while (my $case = $cases->next) {
        my %update = ();

        if (($case->get_column('behandelaar_gm_id') // '') == $subject_id) {
            $update{assignee_v1_json} = undef;
        }
        if ($case->get_column('coordinator_gm_id') == $subject_id) {
            $update{coordinator_v1_json} = undef;
        }

        $update{last_modified} = $case->last_modified;
        $case->update(\%update);
    }
}

=head1 PRIVATE METHODS

=head2 _retrieve_cases_for_aanvrager

    my $cases = $self->_retrieve_cases_for_aanvrager(
        $schema,
        {
            natuurlijk_persoon_id   => 12984,
        }
    );

    my $first_case = $cases->first;

Will retrieve a list of cases from schema C<Zaak>. 

=cut

sig _retrieve_cases_for_aanvrager => 'Any, HashRef';

sub _retrieve_cases_for_aanvrager {
    my ($self, $schema, $params) = @_;

    my $cases = $schema->resultset('Zaak')->search(
        {
            'aanvrager.gegevens_magazijn_id'  => $params->{natuurlijk_persoon_id},
            'aanvrager.betrokkene_type'       => 'natuurlijk_persoon',
        },
        {
            join    => 'aanvrager'
        }
    );

    return $cases;
}

=head2 _touch_cases

    my $cases = $self->_touch_cases(
        $schema->resultset('Zaak')->search
    );

Will touch every case in given iterator

=cut

sub _touch_cases {
    my ($self, $cases) = @_;

    while (my $case = $cases->next) {
        $case->touch;
    }

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
