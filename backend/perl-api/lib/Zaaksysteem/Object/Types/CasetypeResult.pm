package Zaaksysteem::Object::Types::CasetypeResult;

use Moose;

use Zaaksysteem::Constants qw[ZAAKSYSTEEM_OPTIONS];
use Zaaksysteem::Types qw[JSONBoolean];

use Moose::Util::TypeConstraints qw[enum];

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::CasetypeResult - Data wrapper for casetype 'result'
objects.

=head1 ATTRIBUTES

=head2 casetype_result_id

Reference to the underlying/old C<zaaktype_resultaten> row.

=cut

has casetype_result_id => (
    is => 'rw',
    isa => 'Int',
    type => 'integer',
    label => 'Zaaktyperesultaat ID',
    traits => [qw[OA]]
);

=head2 label

User-defined label for the specific result. Usually an adjective.

=cut

has label => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Label',
    traits => [qw[OA]]
);

=head2 generic_result_type

L<Zaaksysteem::Constants> defines a list of built-in result types. This
attribute links to one of those entries.

=cut

has generic_result_type => (
    is => 'rw',
    isa => enum(ZAAKSYSTEEM_OPTIONS->{ RESULTAATTYPEN }),
    type => 'text',
    label => 'Resultaat',
    traits => [qw[OA]],
    required => 1
);

=head2 retention_period

Integer defining the retention period, which is the amount of days after the
object has transitioned from the last phase and is resolved as an active
process. After this period, the object may be archived/destroyed/transferred.

=cut

has retention_period => (
    is => 'rw',
    isa => 'Int',
    type => 'integer',
    label => 'Bewaartermijn',
    traits => [qw[OA]],
);

=head2 retention_period_source_date

References a built-in constant with options that define when the retention
period starts, in context of the object receiving the result.

The retention period could, for instance, be initiated when the object
expires.

=cut

has retention_period_source_date => (
    is => 'rw',
    isa => enum(ZAAKSYSTEEM_OPTIONS->{ RESULTAATINGANGEN }),
    type => 'text',
    label => 'Brondatum archiefprocedure',
    traits => [qw[OA]],
);

=head2 trigger_archival

While normally every object that has a result is eventually processed for
archival, destruction, or transferal, sometimes this is not desirable. Setting
this attribute to C<false> will prevent the object with this result from being
processed as such.

=cut

has trigger_archival => (
    is => 'rw',
    isa => JSONBoolean,
    type => 'boolean',
    label => 'Bewaartermijn actief',
    coerce => 1,
    traits => [qw[OA]],
    default => 1
);

=head2 archival_type

References L<Zaaksysteem::Constants/ZAAKSYSTEEM_OPTIONS> for a list of types
of actions that should be done after the retention period of the object has
expired.

For example, the type C<bewaren> ('save', or 'archive') implies that the
object should be stored in a seperate long-term archive.

=cut

has archival_type => (
    is => 'rw',
    isa => enum([ values %{ ZAAKSYSTEEM_OPTIONS->{ ARCHIEFNOMINATIE_OPTIONS } } ]),
    type => 'text',
    label => 'Archiefnominatie (waardering)',
    traits => [qw[OA]],
);

=head2 dossier_type

Type of dossier.

=cut

has dossier_type => (
    is => 'rw',
    isa => enum(ZAAKSYSTEEM_OPTIONS->{ DOSSIERTYPE }),
    label => 'Dossiertype',
    traits => [qw[OA]],
);

=head2 selection_list

Process-level textual reference for the rationale behind the result in the
context of the object being resolved.

=cut

has selection_list => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Selectielijst',
    traits => [qw[OA]],
);

=head2 selection_list_start_date

Start date of the selection list.

=cut

has selection_list_start_date => (
    is => 'rw',
    isa => 'DateTime',
    type => 'date',
    label => 'Selectielijst brondatum',
    traits => [qw[OA]]
);

=head2 selection_list_end_date

End date of the selection list.

=cut

has selection_list_end_date => (
    is => 'rw',
    isa => 'DateTime',
    type => 'date',
    label => 'Selectielijst einddatum',
    traits => [qw[OA]]
);

=head2 external_reference

API-level identifier for external systems integrations.

=cut

has external_reference => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Externe referentie',
    traits => [qw[OA]]
);

=head2 explanation

Process-level text explaining, or commenting on, the result within the process
context.

=cut

has explanation => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Toelichting',
    traits => [qw[OA]]
);

=head2 selection_list_number

Number of the selection-list.

=cut

has selection_list_number => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Selectielijst-nummer',
    traits => [qw[OA]],
);

=head2 process_type_number

Number of the selection-list category.

=cut

has process_type_number => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Procestype-nummer',
    traits => [qw[OA]],
);

=head2 process_type_name

Name of the process type

=cut

has process_type_name => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Procestype-naam',
    traits => [qw[OA]],
);

=head2 process_type_description

Description of the process type

=cut

has process_type_description => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Procestype-omschrijving',
    traits => [qw[OA]],
);

=head2 process_type_explanation

Explanation of the process type

=cut

has process_type_explanation => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Procestype-toelichting',
    traits => [qw[OA]],
);

=head2 process_type_generic

Type of process (generic, specific)

=cut

has process_type_generic => (
    is => 'rw',
    isa => enum(ZAAKSYSTEEM_OPTIONS->{ PROCESTYPE_GENERIEK }),
    type => 'text',
    label => 'Procestype-generiek',
    traits => [qw[OA]],
);

=head2 process_type_object

Object the is process is related to.

=cut

has process_type_object => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Procestype-object',
    traits => [qw[OA]],
);

=head2 origin

Origin of the result.

=cut

has origin => (
    is => 'rw',
    isa => enum(ZAAKSYSTEEM_OPTIONS->{ HERKOMST }),
    type => 'text',
    label => 'Herkomst',
    traits => [qw[OA]],
);

=head2 process_term

Process term.

=cut

has process_term => (
    is => 'rw',
    isa => enum(ZAAKSYSTEEM_OPTIONS->{ PROCESTERMIJN }),
    type => 'text',
    label => 'Procestermijn',
    traits => [qw[OA]],
);

=head2 standard_choice

Use this resulttype if more of the same type are available

=cut

has standard_choice => (
    is => 'rw',
    isa => JSONBoolean,
    type => 'boolean',
    label => 'Standaard keuze',
    coerce => 1,
    traits => [qw[OA]],
    default => 0
);

=head1 METHODS

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING> to return there result instance's
L</label>.

=cut

override TO_STRING => sub {
    my $self = shift;

    return $self->label;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
