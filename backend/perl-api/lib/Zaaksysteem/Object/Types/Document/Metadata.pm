package Zaaksysteem::Object::Types::Document::Metadata;

use Moose;

extends 'Zaaksysteem::Object';
use Zaaksysteem::Types qw(Datestamp);

=head1 NAME

Zaaksysteem::Object::Types::Document::Metadata - A document metadata object type

=head1 DESCRIPTION

=cut

use BTTW::Tools;

use Zaaksysteem::Types qw[NonEmptyStr];

=head1 ATTRIBUTES

=head2 description

A description of the document

=cut

has description => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Description',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_description',
);

=head2 trust_level

The trust level of a document

=cut

has trust_level => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Trust level',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_trust_level',
);

=head2 origin

The origin of the document

=cut

has origin => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Origin',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_origin',
);

=head2 origin_date

The date of the origin

=cut

has origin_date => (
    is       => 'rw',
    isa      => Datestamp,
    label    => 'Origin date',
    traits   => [qw[OA]],
    required => 0,
    coerce   => 1,
    predicate => 'has_origin_date',
);

=head2 category

The category of the document

=cut

has category => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Category',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_category',
);

=head2 pronom_format

The PRONOM format of the file. For more information about PRONOM have a look at
L<https://en.wikipedia.org/wiki/PRONOM> or L<http://www.nationalarchives.gov.uk/PRONOM/Default.aspx>.

=cut

has pronom_format => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'PRONOM format',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_pronom_format',
);

=head2 status

The status of the document

=cut

has status => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Status',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_status',
);


=head2 appearance

TMLO requirement

=cut

has appearance => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Appreance',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_appearance',
);

=head2 structure

TMLO requirement

=cut

has structure => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Structure',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_structure',
);

=head2 creation_date

The creation date of the document

=cut

has creation_date => (
    is       => 'rw',
    isa      => Datestamp,
    label    => 'Creation date',
    traits   => [qw[OA]],
    required => 0,
    coerce   => 1,
    predicate => 'has_creation_date',
);

=head2 document_source

    Source who created document

=cut

has document_source => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Document Source',
    traits   => [qw[OA]],
    required => 0,
    predicate => 'has_document_source',
);




override type => sub {
    my $self = shift;
    return 'document/metadata';
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
