package Zaaksysteem::Object::Types::Instance;
use Moose;
use namespace::autoclean;
extends 'Zaaksysteem::Object';
with qw(
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::Object::Roles::Security
);

=head1 NAME

Zaaksysteem::Object::Types::Instance - Built-in object type implementing
a class for customer.d configuration entries.

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Zaaksysteem::Types qw[
    Betrokkene
    CustomerTemplate
    CustomerType
    FQDN
    HashedPassword
    JSONBoolean
    JSONNum
    NonEmptyStr
    Otap
    SoftwareVersions
    Timestamp
    ZSNetworkACLs
];

=head1 ATTRIBUTES

=head2 owner

The owner of the object

=cut

has owner => (
    is       => 'rw',
    isa      => Betrokkene,
    traits   => [qw[OA]],
    label    => 'The owner of the object type',
    required => 1,
);

=head2 customer_type

A type of customer, copied from controlpanel

=cut

has customer_type => (
    is       => 'rw',
    isa      => CustomerType,
    traits   => [qw(OA)],
    label    => 'Type klant',
    required => 1,
);

=head2 fqdn

Fully qualified domainname

=cut

has fqdn => (
    is       => 'rw',
    isa      => FQDN,
    traits   => [qw[OA]],
    label    => 'Fully Qualified Domain Name',
    unique   => 1,
    required => 1,
);

=head2 label

Human-readxiaible name that identifies the customer.d entry

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw[OA]],
    label    => 'Label',
    required => 1,
);

=head2 deleted

Boolean to indicate that this customer config may be deleted from the platform, defaults to 'false'

=cut

has deleted => (
    is      => 'rw',
    isa     => JSONBoolean,
    traits  => [qw[OA]],
    label   => 'Deleted',
    default => 0,
    coerce  => 1,
);

=head2 disabled

Boolean to indicate that this customer config is disabled, defaults to C<false>

=cut

has disabled => (
    is      => 'rw',
    isa     => JSONBoolean,
    traits  => [qw[OA]],
    label   => 'Disabled',
    default => 0,
    coerce  => 1,
);

=head2 delete_on

DateTime value to trigger the actual deletion of the object from the technical platform

=cut

has delete_on => (
    is      => 'rw',
    isa     => Timestamp,
    coerce  => 1,
    traits  => [qw[OA]],
    label   => 'Delete on',
    clearer => '_clear_delete_on',
);

=head2 provisioned_on

DateTime value of the last provisioning action

=cut

has provisioned_on => (
    is      => 'rw',
    isa     => Timestamp,
    coerce  => 1,
    traits  => [qw[OA]],
    label   => 'Provisioned on',
    clearer => '_clear_provisioned_on',
);

=head2 network_acl

An arrayref for network ACL's, or Toegangsbeheer in Dutch.

=cut

has network_acl => (
    is      => 'rw',
    isa     => ZSNetworkACLs,
    traits  => [qw[OA]],
    label   => 'Toegangsbeheer',
    clearer => '_clear_network_acl',
);

=head2 database

An predefined name for the database

=cut

has database => (
    is     => 'rw',
    isa    => NonEmptyStr,
    traits => [qw[OA]],
    label  => 'Databasename',
);

=head2 database_host

Hostname where the database is located

=cut

has database_host => (
    is     => 'rw',
    isa    => NonEmptyStr,
    traits => [qw[OA]],
    label  => 'Database host',
);

=head2 nginx_upstream_configuration

Dedicated configuration for the upstream of nginx

=cut

has nginx_upstream_configuration => (
    is     => 'rw',
    isa    => 'HashRef[Str]',
    traits => [qw[OA]],
    label  => 'Nginx upstream configuration',
);

=head2 maintenance

Set instance in maintenance

=cut

has maintenance => (
    is     => 'rw',
    isa    => JSONBoolean,
    traits => [qw[OA]],
    label  => 'Maintenance',
    coerce => 1,
);

=head2 filestore

An predefined name for the filestore

=cut

has filestore => (
    is     => 'rw',
    isa    => NonEmptyStr,
    traits => [qw[OA]],
    label  => 'Filestore base dir',
);

=head2 otap

The OTAP stage

=cut

has otap => (
    is     => 'rw',
    isa    => Otap,
    traits => [qw[OA]],
    label  => 'OTAP',
);

=head2 software_version

The software version for this instance

=cut

has software_version => (
    is      => 'rw',
    isa     => SoftwareVersions,
    traits  => [qw[OA]],
    label   => 'Softwareversion',
    default => 'master'
);

=head2 protected

Set the environment to a protected state so it cannot be editted by someone in the PIP
=cut

has protected => (
    is     => 'rw',
    isa    => JSONBoolean,
    traits => [qw[OA]],
    label  => 'Protected environment',
    coerce => 1,
);

=head2 password

Set the password of this instance

=cut

has password => (
    is      => 'rw',
    isa     => HashedPassword,
    traits  => [qw[OA]],
    label   => 'Password for user "beheerder"',
    clearer => '_clear_password',
);


=head2 api_domain

The domain name which will be configured for this host to receive API calls

=cut

has api_domain => (
    is     => 'rw',
    isa    => FQDN,
    traits => [qw[OA]],
    label =>
        'Domain which is configured to receive API calls for the instance',
    clearer => '_clear_api_domain',
);

=head2 services_domain

The domain name which will be configured for this host to receive Service calls

=cut

has services_domain => (
    is     => 'rw',
    isa    => FQDN,
    traits => [qw[OA]],
    label =>
        'Domain which is configured to receive Service calls for the instance',
    clearer => '_clear_services_domain',
);

=head2 mail

Boolean value to enable mail or not for the host.

=cut

has mail => (
    is     => 'rw',
    isa    => JSONBoolean,
    traits => [qw[OA]],
    label  => 'Will the host receive mail',
    coerce => 1,
);

=head2 freeform_reference

A freeform reference. Currently in use to save the case id for our zaaksysteem-saas instance

=cut

has freeform_reference => (
    is      => 'rw',
    isa     => 'Str',
    traits  => [qw(OA)],
    label   => 'Freeform reference',
    clearer => '_clear_freeform_reference',
);

=head2 stat_diskspace

The current amount of diskspace in use in GBs

=cut

has stat_diskspace => (
    is     => 'rw',
    isa    => JSONNum,
    traits => [qw(OA)],
    label  => 'Current amount of diskspace in use',
    coerce => 1,
);

=head2 template

Default template

=cut

has template => (
    is       => 'rw',
    isa      => CustomerTemplate,
    traits   => [qw(OA)],
    label    => 'Template',
    required => 0,
);

has fallback_url => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Fall back URL',
    required => 0,
    default  => 'http://www.mintlab.nl',
);

has database_provisioned => (
    is      => 'rw',
    isa     => Timestamp,
    coerce  => 1,
    traits  => [qw(OA)],
    label   => "Provisioned subpart",
    clearer => '_clear_database_provisioned',
);

has filestore_provisioned => (
    is      => 'rw',
    isa     => Timestamp,
    coerce  => 1,
    traits  => [qw(OA)],
    label   => "Provisioned subpart",
    clearer => '_clear_filestore_provisioned',
);

=head2 extra_data

Extra (structured) data that can be used by the provisioner.

=cut

has extra_data => (
    is      => 'rw',
    isa     => 'HashRef',
    traits  => [qw(OA)],
    label   => "Extra data for the provisioner",
    default => sub { {} },
);

=head2 oidc_organization_id

Set the OIDC organization id of this instance.

=cut

has oidc_organization_id => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'OIDC organization id',
    required => 0,
    clearer => '_clear_oidc_organization_id',
);

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new(
        fqdn          => 'foo.bar.example.net',
        label         => 'new from empty',
        owner         => 'betrokkene-bedrijf-1',
        template      => 'mintlab',
        customer_type => 'government',
    );

}

=head2 relatable_types

Returns a list of types that can be related to a Instance object

=cut

sub relatable_types {
    return qw(controlpanel host);
}

=head2 add_to_customer

Shortcut method for adding a relationship with a CustomerConfig object.

Dies when you want to set a relationship if there is already an existing relationship with another CustomerConfig object.

=cut

sub add_to_customer {
    my ($self, $customer_config) = @_;

    my $existing_relation = $self->relations();
    if (@$existing_relation > 0) {
        return 1
            if $existing_relation->[-1]->related_object_id eq
            $customer_config->id;
        throw('/customerd/set_parent',
            'Existing relationship, unable to set new relationship');
    }

    return $self->relate(
        $customer_config,
        relationship_name_a => 'child',
        relationship_name_b => 'mother',
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
