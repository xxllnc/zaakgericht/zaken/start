package Zaaksysteem::Roles::v2::Relationships::Subject;
use Moose::Role;


use JSON::XS qw(encode_json);
use Encode qw(decode_utf8);

requires '_build_v2_hash';

sub as_v2_json_relationship {
    my ($self) = @_;

    my %args = $self->_build_v2_hash;

    return decode_utf8(
        encode_json(
            {
                type      => "relationship",
                value     => $args{uuid},
                specifics => {
                    metadata => {
                        summary     => $args{display_name},
                        description => $args{username},
                    },
                    relationship_type => $args{type}
                },
            }
        )
    );
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
