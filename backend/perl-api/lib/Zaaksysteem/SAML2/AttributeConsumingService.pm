package Zaaksysteem::SAML2::AttributeConsumingService;
use Moose;

# This file overrides some things that have been fixed upstream in
# Net::SAML2::AttributeConsumingService but not yet released.
#
# When a newer Net::SAML2 than 0.79 is released, this file can be removed.

extends 'Net::SAML2::AttributeConsumingService';

sub to_xml {
    my $self = shift;

    die "Unable to create attribute consuming service, we require attributes"
      unless @{ $self->attributes };

    my $xml = $self->_xml_gen();

    return $xml->AttributeConsumingService(
        $self->namespace,
        {
            index     => $self->index,
            isDefault => $self->default ? 'true' : 'false',
        },
        $xml->ServiceName($self->namespace, { 'xml:lang' => 'en' }, $self->service_name),
        $self->_has_service_description ? $xml->ServiceDescription($self->namespace, { 'xml:lang' => 'en' }, $self->service_description) : (),
        map { $_->to_xml } @{ $self->attributes },
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2022, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
