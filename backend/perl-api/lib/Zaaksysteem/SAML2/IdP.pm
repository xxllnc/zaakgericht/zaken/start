package Zaaksysteem::SAML2::IdP;

use Moose;

extends 'Net::SAML2::IdP';

use BTTW::Tools;
use BTTW::Tools::UA;
use URI;
use XML::XPath::Node::Element;
use XML::XPath::Node::Text;
use XML::XPath;

has 'interface' => (
    is  => 'rw',
    isa => 'Zaaksysteem::Model::DB::Interface',
);

# When using a Swift storage backend, we do get a filehandle which we cannot allow
# to go out of scope during this session. We place cert_path in _cert_fh for persistence.
has _cacert_fh => ( is => 'rw' );

=head1 SAML2::IdP

This class wraps L<Net::SAML2::IdP> for a bit nicer integration with
L<Zaaksysteem>. In here you can do some better/fancier validation of the
object's initialization, and extend the default constructors with subs
that hook into Zaaksysteem's behavior.

=head1 Example usage

In the context of Zaaksysteem, we are the SP, not the IdP in the SAML process.
This means that we merely use this IdP package to model the authenticating side
(So DigID, E-Herkenning, Google, whoever). This class retrieves the metadata
and allows you to retrieve a base URL for doing useful things with the IdP.

The following snippet gets the base URL for single-sign-on.

    my $idp = Zaaksysteem::SAML2::IdP->new_from_url(
        url => 'https://url.to/idp/metadata',
        cacert => '/path/to/ssl/certificate/that/signs/idp/messages'
    );

    my $sso_base_url = $idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect');

You can then use this base URL to generate a full HTTP Redirect url that can
be used to redirect the user to the IdP servers for authentication.

=head2 Common pitfalls

This object requires you supply a PEM certificate(-chain) that is being used to
sign the messages coming from the IdP. B<That doesn't need to be the same
CA-certificate your SP certificate and key were signed under!>

=head1 CONSTRUCTORS

=head2 new_from_interface

This convenience constructor makes it easy to generate an IdP interface object
from a L<Zaaksysteem::Backend::Sysin::Interface::Component> object (or deriving
modules).

=head3 Parameters

=over 4

=item interface

Required argument, assumed and validated to be an instance of
L<Zaaksysteem::Model::DB::Interface>

=back

=cut

define_profile new_from_interface => (
    required => {
        interface => 'Zaaksysteem::Model::DB::Interface'
    }
);

sub new_from_interface {
    my ($class, %args) = @_;

    my $interface = assert_profile(\%args)->valid->{ interface };

    my $schema = $interface->result_source->schema;
    my $ca_file = $interface->jpath('$.idp_ca[0].id');
    my $ca_path;
    if ($ca_file) {
        $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.idp_ca[0].id'));
        unless($ca_file) {
            throw('saml2/idp/invalid_ca_certificate', 'CA Certificate configured for IdP not found in filestore.');
        }
        $ca_path = $ca_file->get_path;
    }

    my $url = $interface->jpath('$.idp_metadata');

    if ($url) {
        return $class->new_from_url(
            url => URI->new($url),
            ($ca_path)
            ? (
                ref $ca_path ? (_cacert_fh => $ca_path) : (),
                cacert => "$ca_path",
                )
            : (),
            interface => $interface
        );
    }

    my $metadata_filestore_id = $interface->jpath('$.idp_metadata_filename[0].id');
    if ($metadata_filestore_id) {
        my $metadata = $schema->resultset('Filestore')->find($metadata_filestore_id);

        unless (defined $metadata) {
            throw('saml2/idp/cached_metadata_not_found', sprintf(
                'Attempted to load SAML IdP Metadata from filestore (id: %d), none found',
                $metadata_filestore_id
            ));
        }

        my $self = $class->new_from_xml(
            xml        => $metadata->content,
            $ca_path ? ( cacert     => "$ca_path" ) : (),
        );

        $self->_cacert_fh($ca_path) if $ca_path;
        $self->interface($interface);

        return $self;
    }

    throw('saml2/idp/no_metadata_given', 'No metadata URL given or metadata filename uploaded');
}

=head2 new_from_url

Overriding constructor that does the same as C<new_from_url> from
L<Net::SAML2::IdP> but doesn't die, and throws proper exceptions on failure.

This method also verifies the existence of a C<NameIDFormat> element in the
metadata XML being retrieved from the IdP, and injects a default value with
L</patch_missing_nameid_format>.

=head3 Parameters

=over 4

=item url

String value of the URL where the IdP's metadata can be found.

=item cacert

A file containing the certificate that was used to sign the metadata returned
by the IdP. This can be a different CA certificate than the one used to sign
the SP's certificates.

=back

=cut

around new_from_url => sub {
    my $orig = shift;
    my $self = shift;
    my %args = @_;

    my $ua = new_user_agent(ca_cert => $args{cacert});

    my $idp = $self->$orig(@_, ua => $ua);

    $idp->_cacert_fh($args{ _cacert_fh }) if $args{_cacert_fh};
    $idp->interface($args{ interface });

    return $idp;
};

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

