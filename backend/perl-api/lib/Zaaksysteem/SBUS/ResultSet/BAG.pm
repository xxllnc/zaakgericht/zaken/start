package Zaaksysteem::SBUS::ResultSet::BAG;

use Moose;

extends qw/Zaaksysteem::SBUS::ResultSet::GenericImport/;

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;
use Zaaksysteem::SBUS::Logging::Object;
use Zaaksysteem::SBUS::Logging::Objecten;

use Data::Dumper;

use constant IMPORT_KERNGEGEVENS   => [qw/
    identificatie
/];

use constant IMPORT_KERNGEGEVEN_LABEL   => 'identificatie';

use constant BAG_PROFILE            => {
    'ADR'       => {
        required    => [qw/
            identificatie
            huisnummer
            status
            inonderzoek
            begindatum

            openbareruimte_identificatie
            woonplaats_identificatie
        /],
        optional    => [qw/
            huisletter
            huisnummertoevoeging
            postcode

            correctie
            documentnummer
            documentdatum
            type
            einddatum
            officieel

            openbareruimte_naam
            woonplaats_naam
            gebruiksobject_identificatie
            verblijfsobject_identificatie
            verblijfsobject_begindatum
            verblijfsobject_einddatum
            verblijfsobject_officieel
            verblijfsobject_oppervlakte
            verblijfsobject_status
            verblijfsobject_gebruiksdoel
            verblijfsobject_statuscode
            verblijfsobject_inonderzoek
            verblijfsobject_documentdatum
            verblijfsobject_documentnummer
            verblijfsobject_correctie
        /],
        constraint_methods  => {
            verblijfsobject_gebruiksdoel    => sub {
                my $dfv = shift;

                my $val     = $dfv
                            ->get_filtered_data
                            ->{verblijfsobject_gebruiksdoel};


                ### Should be arrayref
                return 1 if UNIVERSAL::isa($val, 'ARRAY');
                return;
            }
        },
        defaults    => {
            'correctie'                 => 'N',
            'officieel'                 => 'N',
            'documentnummer'            => '',
            'documentdatum'             => '',
            'type'                      => 'verblijfsobject',
            openbareruimte_begindatum   => '20071113000000',
            verblijfsobject_begindatum  => '20071113000000',
            verblijfsobject_officieel   => 'N',
            verblijfsobject_oppervlakte => '0',
            verblijfsobject_inonderzoek => 'N',
            verblijfsobject_documentdatum => '',
            verblijfsobject_documentnummer => '',
            verblijfsobject_correctie   => 'N',
            verblijfsobject_gebruiksdoel => sub { [91] },
            verblijfsobject_status      => sub {
                my $dfv     = shift;

                if (
                    my $statuscode  = $dfv
                                    ->get_filtered_data
                                    ->{verblijfsobject_statuscode}
                ) {
                    if (
                        my $status      = STUF_STATUS_CODES
                                        ->{ int($statuscode) }
                    ) {
                        return $status;
                    }
                }

                return 'Verblijfsobject in gebruik',
            }
        },
        field_filters   => {
            'begindatum'    => sub {
                ### Todo, postfix with zero's
                return shift;
            },
            'einddatum'     => sub {
                ### Todo, postfix with zero's
                return shift;
            },
        }
    },
    'R02'       => {
        required    => [qw/
            identificatie
            naam
            woonplaats_identificatie
        /],
        optional    => [qw/
            inonderzoek
            correctie
            documentnummer
            documentdatum
            type
            einddatum
            status
            begindatum
            officieel
        /],
        defaults    => {
            'correctie'         => 'N',
            'documentnummer'    => '',
            'documentdatum'     => '',
            'type'              => 'Weg',
            'inonderzoek'       => 'N',
            'officieel'         => 'N',
            'status'            => 'Naamgeving uitgegeven',
            'begindatum'        => '19000101000000',
        },
        field_filters   => {
            'begindatum'    => sub {
                ### Todo, postfix with zero's
                return shift;
            },
            'einddatum'     => sub {
                ### Todo, postfix with zero's
                return shift;
            },
        }
    },
    'R03'       => {
        required    => [qw/
            identificatie
            naam
        /],
        optional    => [qw/
            inonderzoek
            correctie
            documentnummer
            documentdatum
            einddatum
            status
            begindatum
            officieel
        /],
        defaults    => {
            'correctie'         => 'N',
            'documentnummer'    => '',
            'documentdatum'     => '',
            'inonderzoek'       => 'N',
            'officieel'         => 'N',
            'status'            => 'Woonplaats aangewezen',
            'begindatum'        => '19000101000000',
        },
        field_filters   => {
            'begindatum'    => sub {
                ### Todo, postfix with zero's
                return shift;
            },
            'einddatum'     => sub {
                ### Todo, postfix with zero's
                return shift;
            },
        }
    },
};

use constant BAG_MINIMAL_GEBRUIKSOBJECT_COLUMNS => [qw/
    begindatum
    einddatum
    officieel
    status
    inonderzoek
    documentdatum
    documentnummer
    correctie
/];

use constant BAG_DEFAULT_BEGINDATUM => '00000000000000';
use constant BAG_DEFAULT_STATUS     => 'Onbekend';


has '_import_kerngegevens'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return IMPORT_KERNGEGEVENS;
    }
);

has '_import_kerngegeven_label'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return IMPORT_KERNGEGEVEN_LABEL;
    }
);

has '_import_objecttype'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    default     => sub {
        return SBUS_LOGOBJECT_PRS;
    }
);

has '_import_entry_profile' => (
    'is'        => 'rw',
    'lazy'      => 1,
    default     => sub {
        return {};
    }
);

around 'import_entry'   => sub {
    my $orig                = shift;
    my $class               = shift;
    my ($params)            = @_;

    $class->_import_entry_profile(
        BAG_PROFILE->{ $params->{create}->{ object_type } }
    );

    $class->_import_objecttype(
        $params->{create}->{object_type}
    );

    $class->$orig(@_);
};

sub _delete_real_entry {
    my ($self, $params, $options) = @_;

    my $record = $self->_get_kern_record($params, @_);

    ### record not found in the first place, not in ZS
    return unless $record;

    $record->delete;
}

sub _import_real_entry {
    my $self                        = shift;
    my ($input_params, $options)    = @_;
    my ($record);

    ### Clone and delete gebruiksobject_id, use this for below relatie
    my $params = {};

    ### Nummeraanduiding
    $params->{ $_ } = $input_params->{ $_ } for qw/
        identificatie
        huisnummer
        status
        inonderzoek
        huisletter
        huisnummertoevoeging
        postcode
        correctie
        documentnummer
        documentdatum
        type
        einddatum
        officieel
        begindatum
    /;

    $params->{'openbareruimte'} = $input_params->{
        'openbareruimte_identificatie'
    };

    $params->{'woonplaats'}     = $input_params->{
        'woonplaats_identificatie'
    };

    if (uc($options->{mutatie_type}) =~ /W|V/) {
        $record = $self->_get_kern_record($params, @_);

        unless ($record) {
            warn(
                $self->_import_objecttype . '-Entry niet gevonden in database: ' .
                $params->{ $self->_import_kerngegeven_label }
            );
            return;
        }
    }

    ### Detect changes
    $self->_detect_changes($params, $options, $record);

    if ($record) {
        $record->update($params);
    } else {
        $record = $self->update_or_create($params);
    }

    ### In case of gebruiksobject_id (ADR), update verblijfsobject,ligplaats etc
    $self->_import_real_entry_relatie($record, @_);

    return $record;
}

sub _import_real_entry_relatie_woonplaats {
    my $self                                = shift;
    my ($record, $input_params, $options)   = @_;

    return if (
        $self->result_source
            ->schema
            ->resultset('BagWoonplaats')
            ->find({
                identificatie   => $input_params->{woonplaats_identificatie}
            })
    );

    $self->result_source
        ->schema
        ->resultset('BagWoonplaats')
        ->create(
            {
                identificatie   => $input_params->{woonplaats_identificatie},
                naam            => $input_params->{woonplaats_naam},
                begindatum      => '20071113000000',
                officieel       => 'N',
                inonderzoek     => 'N',
                status          => 'Woonplaats aangewezen',
                correctie       => 'N',
                documentdatum   => '',
                documentnummer  => '',
            }
        );
}

sub _import_real_entry_relatie_verblijfsobject {
    my $self                                = shift;
    my ($record, $input_params, $options)   = @_;

    return unless $input_params->{verblijfsobject_identificatie};

    my ($soort_locatie)     = $input_params
                            ->{verblijfsobject_identificatie}
                            =~ /^\d{5}(\d{1})/;

    my $vo_record;

    my $c_params = {
        map {
            my $key = $_;
            $key =~ s/^verblijfsobject_//;
            $key => $input_params->{ $_ }
        } grep({
            defined($input_params->{ $_ }) &&
            $_ =~ /^verblijfsobject_/
        } keys %{ $input_params })
    };

    $c_params->{hoofdadres} = $record->identificatie;

    my $working_table   = 'BagVerblijfsobject';
    my $type            = 'verblijfsobject';
    if ($soort_locatie == 2) {
        delete($c_params->{oppervlakte});
        $working_table  = 'BagLigplaats';
        $type           = 'ligplaats';
        $c_params->{status} = 'Ligplaats in gebruik';
    } elsif ($soort_locatie == 3) {
        delete($c_params->{oppervlakte});
        $working_table  = 'BagStandplaats';
        $type           = 'standplaats';
        $c_params->{status} = 'Standplaats in gebruik';
    }

    delete($c_params->{gebruiksdoel});
    delete($c_params->{statuscode});

    $input_params->{type} = $type;

    my $ok;
    if (
        (
            $vo_record = $self->result_source
            ->schema
            ->resultset($working_table)
            ->find({
                identificatie   => $input_params
                                    ->{verblijfsobject_identificatie}
            })
        )
    ) {
        $ok = $vo_record->update( $c_params );
    } else {
        $c_params->{status} ||= 'Verblijfsobject in gebruik';

        $ok = $vo_record = $self->result_source
                ->schema
                ->resultset($working_table)
                ->create( $c_params );
    }

    if ($ok) {
        $record->type($type);
        $record->update;

        if ($type eq 'verblijfsobject') {
            $self->_import_real_entry_relatie_gebruiksdoel(@_);
        }
    }
}

sub _import_real_entry_relatie_gebruiksdoel {
    my $self                                = shift;
    my ($record, $input_params, $options)   = @_;

    return unless (
        defined($input_params->{type}) &&
        $input_params->{type} &&
        $input_params->{type} eq 'verblijfsobject' &&
        defined($input_params->{gebruiksobject_identificatie}) &&
        $input_params->{gebruiksobject_identificatie}
    );

    ### Delete gebruiksdoelen
    $self->result_source
        ->schema
        ->resultset('BagVerblijfsobjectGebruiksdoel')->search({
            identificatie   => $input_params->{gebruiksobject_identificatie},
        })->delete;

    for my $gebruiksdoel (@{ $input_params->{verblijfsobject_gebruiksdoel} }) {
        $gebruiksdoel = STUF_GEBRUIKSDOEL_CODES->{ int($gebruiksdoel) };

        $self->result_source
            ->schema
            ->resultset('BagVerblijfsobjectGebruiksdoel')
            ->update_or_create({
                identificatie   => $input_params->{gebruiksobject_identificatie},
                begindatum      => BAG_DEFAULT_BEGINDATUM,
                gebruiksdoel    => $gebruiksdoel,
                correctie       => 'N',
            });
    }
}


sub _import_real_entry_relatie_openbareruimte {
    my $self                                = shift;
    my ($record, $input_params, $options)   = @_;

    return if (
        $self->result_source
            ->schema
            ->resultset('BagOpenbareruimte')
            ->find({
                identificatie   => $input_params->{openbareruimte_identificatie}
            })
    );

    $self->result_source
        ->schema
        ->resultset('BagOpenbareruimte')
        ->create(
            {
                identificatie   => $input_params->{openbareruimte_identificatie},
                naam            => $input_params->{openbareruimte_naam},
                begindatum      => '20071113000000',
                officieel       => 'N',
                woonplaats      => $input_params->{woonplaats_identificatie},
                type            => 'Weg',
                inonderzoek     => 'N',
                status          => 'Naamgeving uitgegeven',
                correctie       => 'N',
                documentdatum   => '',
                documentnummer  => '',
            }
        );
}

sub _import_real_entry_relatie {
    my $self                                = shift;
    my ($record, $input_params, $options)   = @_;

    if ($self->_import_objecttype eq 'ADR') {
        $self->_import_real_entry_relatie_woonplaats        (@_);
        $self->_import_real_entry_relatie_openbareruimte    (@_);
        $self->_import_real_entry_relatie_verblijfsobject   (@_);
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BAG_DEFAULT_BEGINDATUM

TODO: Fix the POD

=cut

=head2 BAG_DEFAULT_STATUS

TODO: Fix the POD

=cut

=head2 BAG_MINIMAL_GEBRUIKSOBJECT_COLUMNS

TODO: Fix the POD

=cut

=head2 BAG_PROFILE

TODO: Fix the POD

=cut

=head2 IMPORT_KERNGEGEVENS

TODO: Fix the POD

=cut

=head2 IMPORT_KERNGEGEVEN_LABEL

TODO: Fix the POD

=cut

=head2 SBUS_LOGOBJECT_PRS

TODO: Fix the POD

=cut

=head2 STUF_GEBRUIKSDOEL_CODES

TODO: Fix the POD

=cut

=head2 STUF_STATUS_CODES

TODO: Fix the POD

=cut

