use utf8;
package Zaaksysteem::Schema::BackupMinty8010ZaaktypeKenmerk;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BackupMinty8010ZaaktypeKenmerk

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<backup_minty8010_zaaktype_kenmerk>

=cut

__PACKAGE__->table("backup_minty8010_zaaktype_kenmerk");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 nr

  data_type: 'bigint'
  is_nullable: 1

=head2 elem

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 1 },
  "nr",
  { data_type => "bigint", is_nullable => 1 },
  "elem",
  { data_type => "text", is_nullable => 1 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<backup_minty8010_zaaktype_kenmerk_id_nr_idx>

=over 4

=item * L</id>

=item * L</nr>

=back

=cut

__PACKAGE__->add_unique_constraint("backup_minty8010_zaaktype_kenmerk_id_nr_idx", ["id", "nr"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-11-24 14:46:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MWtKnffXNhp/ci5e7GwGWA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
