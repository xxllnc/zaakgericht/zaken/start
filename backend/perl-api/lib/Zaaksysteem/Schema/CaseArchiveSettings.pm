use utf8;
package Zaaksysteem::Schema::CaseArchiveSettings;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseArchiveSettings

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<case_archive_settings>

=cut

__PACKAGE__->table("case_archive_settings");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'case_archive_settings_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 name

  data_type: 'text[]'
  is_nullable: 0

=head2 classification_code

  data_type: 'text[]'
  is_nullable: 0

=head2 classification_description

  data_type: 'text[]'
  is_nullable: 0

=head2 classification_source_tmlo

  data_type: 'text[]'
  is_nullable: 0

=head2 classification_date

  data_type: 'text[]'
  is_nullable: 0

=head2 description_tmlo

  data_type: 'text[]'
  is_nullable: 1

=head2 location

  data_type: 'text[]'
  is_nullable: 0

=head2 coverage_in_time_tmlo

  data_type: 'text[]'
  is_nullable: 0

=head2 coverage_in_geo_tmlo

  data_type: 'text[]'
  is_nullable: 1

=head2 language_tmlo

  data_type: 'text'
  is_nullable: 0

=head2 user_rights

  data_type: 'text[]'
  is_nullable: 1

=head2 user_rights_description

  data_type: 'text[]'
  is_nullable: 1

=head2 user_rights_date_period

  data_type: 'text[]'
  is_nullable: 1

=head2 confidentiality

  data_type: 'text[]'
  is_nullable: 1

=head2 confidentiality_description

  data_type: 'text[]'
  is_nullable: 1

=head2 confidentiality_date_period

  data_type: 'text[]'
  is_nullable: 1

=head2 form_genre

  data_type: 'text[]'
  is_nullable: 1

=head2 form_publication

  data_type: 'text[]'
  is_nullable: 1

=head2 structure

  data_type: 'text[]'
  is_nullable: 1

=head2 generic_metadata_tmlo

  data_type: 'text[]'
  is_nullable: 1

=head2 archive_type

  data_type: 'text'
  is_nullable: 1

=head2 classification_source_mdto

  data_type: 'text[]'
  is_nullable: 1

=head2 description_mdto

  data_type: 'text[]'
  is_nullable: 1

=head2 coverage_in_time_mdto

  data_type: 'text[]'
  is_nullable: 1

=head2 coverage_in_geo_mdto

  data_type: 'text[]'
  is_nullable: 1

=head2 language_mdto

  data_type: 'text'
  is_nullable: 1

=head2 additional_metadata

  data_type: 'text[]'
  is_nullable: 1

=head2 aggregation_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 coverage_in_time_begin_date

  data_type: 'text[]'
  is_nullable: 1

=head2 coverage_in_time_end_date

  data_type: 'text[]'
  is_nullable: 1

=head2 coverage_in_place

  data_type: 'text[]'
  is_nullable: 1

=head2 event_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 event_in_time

  data_type: 'text[]'
  is_nullable: 1

=head2 event_actor_name

  data_type: 'text[]'
  is_nullable: 1

=head2 event_result

  data_type: 'text[]'
  is_nullable: 1

=head2 valuation_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 retention_period_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 information_category_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 related_information_object_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 related_information_object_actor

  data_type: 'text[]'
  is_nullable: 1

=head2 archive_maker

  data_type: 'text[]'
  is_nullable: 1

=head2 subject_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_label

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_code

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_glossary

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_description

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_documents_name

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_term_label

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_term_code

  data_type: 'text[]'
  is_nullable: 1

=head2 restriction_use_term_glossary

  data_type: 'text[]'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "case_archive_settings_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "name",
  { data_type => "text[]", is_nullable => 0 },
  "classification_code",
  { data_type => "text[]", is_nullable => 0 },
  "classification_description",
  { data_type => "text[]", is_nullable => 0 },
  "classification_source_tmlo",
  { data_type => "text[]", is_nullable => 0 },
  "classification_date",
  { data_type => "text[]", is_nullable => 0 },
  "description_tmlo",
  { data_type => "text[]", is_nullable => 1 },
  "location",
  { data_type => "text[]", is_nullable => 0 },
  "coverage_in_time_tmlo",
  { data_type => "text[]", is_nullable => 0 },
  "coverage_in_geo_tmlo",
  { data_type => "text[]", is_nullable => 1 },
  "language_tmlo",
  { data_type => "text", is_nullable => 0 },
  "user_rights",
  { data_type => "text[]", is_nullable => 1 },
  "user_rights_description",
  { data_type => "text[]", is_nullable => 1 },
  "user_rights_date_period",
  { data_type => "text[]", is_nullable => 1 },
  "confidentiality",
  { data_type => "text[]", is_nullable => 1 },
  "confidentiality_description",
  { data_type => "text[]", is_nullable => 1 },
  "confidentiality_date_period",
  { data_type => "text[]", is_nullable => 1 },
  "form_genre",
  { data_type => "text[]", is_nullable => 1 },
  "form_publication",
  { data_type => "text[]", is_nullable => 1 },
  "structure",
  { data_type => "text[]", is_nullable => 1 },
  "generic_metadata_tmlo",
  { data_type => "text[]", is_nullable => 1 },
  "archive_type",
  { data_type => "text", is_nullable => 1 },
  "classification_source_mdto",
  { data_type => "text[]", is_nullable => 1 },
  "description_mdto",
  { data_type => "text[]", is_nullable => 1 },
  "coverage_in_time_mdto",
  { data_type => "text[]", is_nullable => 1 },
  "coverage_in_geo_mdto",
  { data_type => "text[]", is_nullable => 1 },
  "language_mdto",
  { data_type => "text", is_nullable => 1 },
  "additional_metadata",
  { data_type => "text[]", is_nullable => 1 },
  "aggregation_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "coverage_in_time_begin_date",
  { data_type => "text[]", is_nullable => 1 },
  "coverage_in_time_end_date",
  { data_type => "text[]", is_nullable => 1 },
  "coverage_in_place",
  { data_type => "text[]", is_nullable => 1 },
  "event_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "event_in_time",
  { data_type => "text[]", is_nullable => 1 },
  "event_actor_name",
  { data_type => "text[]", is_nullable => 1 },
  "event_result",
  { data_type => "text[]", is_nullable => 1 },
  "valuation_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "retention_period_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "information_category_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "related_information_object_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "related_information_object_actor",
  { data_type => "text[]", is_nullable => 1 },
  "archive_maker",
  { data_type => "text[]", is_nullable => 1 },
  "subject_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_label",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_code",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_glossary",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_description",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_documents_name",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_term_label",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_term_code",
  { data_type => "text[]", is_nullable => 1 },
  "restriction_use_term_glossary",
  { data_type => "text[]", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<case_archive_settings_case_id_key>

=over 4

=item * L</case_id>

=back

=cut

__PACKAGE__->add_unique_constraint("case_archive_settings_case_id_key", ["case_id"]);

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-11-27 21:57:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YidJWaNEelCNeCWfURGLsg

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
