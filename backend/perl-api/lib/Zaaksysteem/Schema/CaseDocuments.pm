use utf8;
package Zaaksysteem::Schema::CaseDocuments;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseDocuments

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_documents>

=cut

__PACKAGE__->table("case_documents");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.id AS case_id,\n    (ARRAY( SELECT (f.id)::character varying AS id\n           FROM (file_case_document fcd\n             JOIN file f ON ((f.id = fcd.file_id)))\n          WHERE ((fcd.case_id = z.id) AND (bk.bibliotheek_kenmerken_id = fcd.bibliotheek_kenmerken_id))))::text[] AS value,\n    bk.magic_string,\n    bk.bibliotheek_kenmerken_id AS library_id\n   FROM (zaak z\n     JOIN zaaktype_document_kenmerken_map bk ON ((z.zaaktype_node_id = bk.zaaktype_node_id)))");

=head1 ACCESSORS

=head2 case_id

  data_type: 'bigint'
  is_nullable: 1

=head2 value

  data_type: 'text[]'
  is_nullable: 1

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 library_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "bigint", is_nullable => 1 },
  "value",
  { data_type => "text[]", is_nullable => 1 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "library_id",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-02-24 00:07:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:986uKAWoc0ofEri7c/X2vA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
