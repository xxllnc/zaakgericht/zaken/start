use utf8;
package Zaaksysteem::Schema::CaseV1;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseV1

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_v1>

=cut

__PACKAGE__->table("case_v1");
__PACKAGE__->result_source_instance->view_definition(" SELECT c.number,\n    c.id,\n    c.number_parent,\n    c.number_master,\n    c.number_previous,\n    c.subject,\n    c.subject_external,\n    c.status,\n    c.date_created,\n    c.date_modified,\n    c.date_destruction,\n    c.date_of_completion,\n    c.date_of_registration,\n    c.date_target,\n    c.html_email_template,\n    c.payment_status,\n    c.price,\n    c.channel_of_contact,\n    c.archival_state,\n    c.confidentiality,\n    c.stalled_since,\n    c.stalled_until,\n    c.current_deadline,\n    c.deadline_timeline,\n    c.result_id,\n    c.result,\n    c.active_selection_list,\n    c.outcome,\n    c.casetype,\n    c.route,\n    c.suspension_rationale,\n    c.premature_completion_rationale,\n    (zts.fase)::text AS phase,\n    c.relations,\n    c.case_relationships,\n    c.requestor,\n    c.assignee,\n    c.coordinator,\n    c.aggregation_scope,\n    c.case_location,\n    c.correspondence_location,\n    ( SELECT COALESCE(jsonb_object_agg(ca.magic_string, ca.value) FILTER (WHERE (ca.magic_string IS NOT NULL)), '{}'::jsonb) AS \"coalesce\"\n           FROM case_attributes_v1 ca\n          WHERE (ca.case_id = c.number)) AS attributes,\n    ( SELECT json_build_object('preview', zts.fase, 'reference', NULL::unknown, 'type', 'case/milestone', 'instance', json_build_object('date_created', timestamp_to_perl_datetime(now()), 'date_modified', timestamp_to_perl_datetime(now()), 'phase_label',\n                CASE\n                    WHEN (zts.id IS NOT NULL) THEN zts.fase\n                    ELSE zts_end.fase\n                END, 'phase_sequence_number',\n                CASE\n                    WHEN (zts.id IS NOT NULL) THEN zts.status\n                    ELSE zts_end.status\n                END, 'milestone_label', zts_previous.naam, 'milestone_sequence_number', zts_previous.status, 'last_sequence_number', zts_end.status)) AS json_build_object\n           FROM casetype_end_status zts_end\n          WHERE (c.zaaktype_node_id = zts_end.zaaktype_node_id)) AS milestone\n   FROM ((( SELECT z.id AS number,\n            z.uuid AS id,\n            z.pid AS number_parent,\n            z.number_master,\n            z.vervolg_van AS number_previous,\n            z.onderwerp AS subject,\n            z.onderwerp_extern AS subject_external,\n            z.status,\n            z.created AS date_created,\n            z.last_modified AS date_modified,\n            z.vernietigingsdatum AS date_destruction,\n            z.afhandeldatum AS date_of_completion,\n            z.registratiedatum AS date_of_registration,\n            z.streefafhandeldatum AS date_target,\n            z.html_email_template,\n            z.payment_status,\n            z.dutch_price AS price,\n            z.contactkanaal AS channel_of_contact,\n            z.archival_state,\n            z.zaaktype_node_id,\n            z.milestone AS raw_milestone,\n            get_confidential_mapping(z.confidentiality) AS confidentiality,\n                CASE\n                    WHEN (z.status = 'stalled'::text) THEN zm.stalled_since\n                    ELSE NULL::timestamp without time zone\n                END AS stalled_since,\n                CASE\n                    WHEN (z.status = 'stalled'::text) THEN z.stalled_until\n                    ELSE NULL::timestamp without time zone\n                END AS stalled_until,\n            zm.current_deadline,\n            zm.deadline_timeline,\n            ztr.id AS result_id,\n            ztr.resultaat AS result,\n            ztr.selectielijst AS active_selection_list,\n                CASE\n                    WHEN (ztr.id IS NOT NULL) THEN json_build_object('reference', NULL::unknown, 'type', 'case/result', 'preview',\n                    CASE\n                        WHEN (ztr.label IS NOT NULL) THEN ztr.label\n                        ELSE ztr.resultaat\n                    END, 'instance', json_build_object('date_created', timestamp_to_perl_datetime((ztr.created)::timestamp with time zone), 'date_modified', timestamp_to_perl_datetime((ztr.last_modified)::timestamp with time zone), 'archival_type', ztr.archiefnominatie, 'dossier_type', ztr.dossiertype, 'name',\n                    CASE\n                        WHEN (ztr.label IS NOT NULL) THEN ztr.label\n                        ELSE ztr.resultaat\n                    END, 'result', ztr.resultaat, 'retention_period', ztr.bewaartermijn, 'selection_list',\n                    CASE\n                        WHEN (ztr.selectielijst = ''::text) THEN NULL::text\n                        ELSE ztr.selectielijst\n                    END, 'selection_list_start', ztr.selectielijst_brondatum, 'selection_list_end', ztr.selectielijst_einddatum))\n                    ELSE NULL::json\n                END AS outcome,\n            json_build_object('preview', ct_ref.title, 'reference', ct_ref.uuid, 'instance', json_build_object('version', ct_ref.version, 'name', ct_ref.title), 'type', 'casetype') AS casetype,\n            json_build_object('preview', ((gr.name || ', '::text) || role.name), 'reference', NULL::unknown, 'type', 'case/route', 'instance', json_build_object('date_created', timestamp_to_perl_datetime(now()), 'date_modified', timestamp_to_perl_datetime(now()), 'group', gr.v1_json, 'role', role.v1_json)) AS route,\n                CASE\n                    WHEN (z.status = 'stalled'::text) THEN zm.opschorten\n                    ELSE NULL::character varying\n                END AS suspension_rationale,\n                CASE\n                    WHEN (z.status = 'resolved'::text) THEN zm.afhandeling\n                    ELSE NULL::character varying\n                END AS premature_completion_rationale,\n            json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb))) AS relations,\n            json_build_object('parent', (parent.relationship -> 0), 'continuation', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(continuation.relationship, '[]'::jsonb))), 'child', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(children.relationship, '[]'::jsonb))), 'plain', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb)))) AS case_relationships,\n            z.requestor_v1_json AS requestor,\n            z.assignee_v1_json AS assignee,\n            z.coordinator_v1_json AS coordinator,\n            'Dossier'::text AS aggregation_scope,\n            NULL::text AS case_location,\n            NULL::text AS correspondence_location\n           FROM (((((((((zaak z\n             LEFT JOIN zaak_meta zm ON ((zm.zaak_id = z.id)))\n             LEFT JOIN casetype_v1_reference ct_ref ON ((z.zaaktype_node_id = ct_ref.casetype_node_id)))\n             LEFT JOIN zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))\n             LEFT JOIN groups gr ON ((z.route_ou = gr.id)))\n             LEFT JOIN roles role ON ((z.route_role = role.id)))\n             LEFT JOIN view_case_relationship_v1_json crp ON (((z.id = crp.case_id) AND ((crp.type)::text = 'plain'::text))))\n             LEFT JOIN view_case_relationship_v1_json continuation ON (((z.id = continuation.case_id) AND ((continuation.type)::text = 'initiator'::text))))\n             LEFT JOIN view_case_relationship_v1_json children ON (((z.id = children.case_id) AND ((children.type)::text = 'parent'::text))))\n             LEFT JOIN view_case_relationship_v1_json parent ON (((z.id = parent.case_id) AND ((parent.type)::text = 'child'::text))))\n          WHERE (z.deleted IS NULL)) c\n     LEFT JOIN zaaktype_status zts ON (((c.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = (c.raw_milestone + 1)))))\n     LEFT JOIN zaaktype_status zts_previous ON (((c.zaaktype_node_id = zts_previous.zaaktype_node_id) AND (zts_previous.status = c.raw_milestone))))");

=head1 ACCESSORS

=head2 number

  data_type: 'bigint'
  is_nullable: 1

=head2 id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 number_parent

  data_type: 'bigint'
  is_nullable: 1

=head2 number_master

  data_type: 'bigint'
  is_nullable: 1

=head2 number_previous

  data_type: 'bigint'
  is_nullable: 1

=head2 subject

  data_type: 'text'
  is_nullable: 1

=head2 subject_external

  data_type: 'text'
  is_nullable: 1

=head2 status

  data_type: 'text'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_destruction

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_of_completion

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_of_registration

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_target

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 html_email_template

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 payment_status

  data_type: 'text'
  is_nullable: 1

=head2 price

  data_type: 'text'
  is_nullable: 1

=head2 channel_of_contact

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 archival_state

  data_type: 'text'
  is_nullable: 1

=head2 confidentiality

  data_type: 'jsonb'
  is_nullable: 1

=head2 stalled_since

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 stalled_until

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 current_deadline

  data_type: 'jsonb'
  is_nullable: 1

=head2 deadline_timeline

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_id

  data_type: 'integer'
  is_nullable: 1

=head2 result

  data_type: 'text'
  is_nullable: 1

=head2 active_selection_list

  data_type: 'text'
  is_nullable: 1

=head2 outcome

  data_type: 'json'
  is_nullable: 1

=head2 casetype

  data_type: 'json'
  is_nullable: 1

=head2 route

  data_type: 'json'
  is_nullable: 1

=head2 suspension_rationale

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 premature_completion_rationale

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 phase

  data_type: 'text'
  is_nullable: 1

=head2 relations

  data_type: 'json'
  is_nullable: 1

=head2 case_relationships

  data_type: 'json'
  is_nullable: 1

=head2 requestor

  data_type: 'jsonb'
  is_nullable: 1

=head2 assignee

  data_type: 'jsonb'
  is_nullable: 1

=head2 coordinator

  data_type: 'jsonb'
  is_nullable: 1

=head2 aggregation_scope

  data_type: 'text'
  is_nullable: 1

=head2 case_location

  data_type: 'text'
  is_nullable: 1

=head2 correspondence_location

  data_type: 'text'
  is_nullable: 1

=head2 attributes

  data_type: 'jsonb'
  is_nullable: 1

=head2 milestone

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "number",
  { data_type => "bigint", is_nullable => 1 },
  "id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "number_parent",
  { data_type => "bigint", is_nullable => 1 },
  "number_master",
  { data_type => "bigint", is_nullable => 1 },
  "number_previous",
  { data_type => "bigint", is_nullable => 1 },
  "subject",
  { data_type => "text", is_nullable => 1 },
  "subject_external",
  { data_type => "text", is_nullable => 1 },
  "status",
  { data_type => "text", is_nullable => 1 },
  "date_created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_destruction",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_of_completion",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_of_registration",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_target",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "html_email_template",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "payment_status",
  { data_type => "text", is_nullable => 1 },
  "price",
  { data_type => "text", is_nullable => 1 },
  "channel_of_contact",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "archival_state",
  { data_type => "text", is_nullable => 1 },
  "confidentiality",
  { data_type => "jsonb", is_nullable => 1 },
  "stalled_since",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "stalled_until",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "current_deadline",
  { data_type => "jsonb", is_nullable => 1 },
  "deadline_timeline",
  { data_type => "jsonb", is_nullable => 1 },
  "result_id",
  { data_type => "integer", is_nullable => 1 },
  "result",
  { data_type => "text", is_nullable => 1 },
  "active_selection_list",
  { data_type => "text", is_nullable => 1 },
  "outcome",
  { data_type => "json", is_nullable => 1 },
  "casetype",
  { data_type => "json", is_nullable => 1 },
  "route",
  { data_type => "json", is_nullable => 1 },
  "suspension_rationale",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "premature_completion_rationale",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "phase",
  { data_type => "text", is_nullable => 1 },
  "relations",
  { data_type => "json", is_nullable => 1 },
  "case_relationships",
  { data_type => "json", is_nullable => 1 },
  "requestor",
  { data_type => "jsonb", is_nullable => 1 },
  "assignee",
  { data_type => "jsonb", is_nullable => 1 },
  "coordinator",
  { data_type => "jsonb", is_nullable => 1 },
  "aggregation_scope",
  { data_type => "text", is_nullable => 1 },
  "case_location",
  { data_type => "text", is_nullable => 1 },
  "correspondence_location",
  { data_type => "text", is_nullable => 1 },
  "attributes",
  { data_type => "jsonb", is_nullable => 1 },
  "milestone",
  { data_type => "json", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-02-28 15:30:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:INxEyZiECDTk+t8tF2Grag

require JSON::XS;

my @hash_fields = qw(
    assignee coordinator requestor
    attributes
    case_relationships relations
    casetype
    current_deadline
    milestone route outcome
    confidentiality
) ;

foreach (@hash_fields) {
  __PACKAGE__->inflate_column($_, {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
  });
}

__PACKAGE__->inflate_column('deadline_timeline', {
  inflate => sub { JSON::XS->new->decode(shift // '[]') },
});

__PACKAGE__->load_components(
     "+Zaaksysteem::Backend::CaseV1::Component",
     __PACKAGE__->load_components()
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
