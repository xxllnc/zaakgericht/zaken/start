use utf8;
package Zaaksysteem::Schema::CountryCodeV1View;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CountryCodeV1View

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<country_code_v1_view>

=cut

__PACKAGE__->table("country_code_v1_view");
__PACKAGE__->result_source_instance->view_definition(" SELECT r.id,\n    r.dutch_code,\n    r.uuid,\n    r.label,\n    f.unit_json AS json\n   FROM country_code r,\n    LATERAL country_code_to_json(hstore(r.*)) f(unit_json)");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 dutch_code

  data_type: 'integer'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 json

  data_type: 'jsonb'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 1 },
  "dutch_code",
  { data_type => "integer", is_nullable => 1 },
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "json",
  { data_type => "jsonb", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-05-04 16:20:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:9PwaEKnTpNuf+LoPCGfUSA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
