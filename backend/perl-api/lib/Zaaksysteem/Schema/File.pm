use utf8;
package Zaaksysteem::Schema::File;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::File

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<file>

=cut

__PACKAGE__->table("file");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  default_value: 'file'
  is_nullable: 1

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 search_order

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'file_id_seq'

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 extension

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=head2 root_file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 version

  data_type: 'integer'
  default_value: 1
  is_nullable: 1

=head2 case_id

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 metadata_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 subject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 directory_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 creation_reason

  data_type: 'text'
  is_nullable: 0

=head2 accepted

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 rejection_reason

  data_type: 'text'
  is_nullable: 1

=head2 reject_to_queue

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 is_duplicate_name

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 publish_pip

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 publish_website

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 0
  timezone: 'UTC'

=head2 created_by

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 modified_by

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted_by

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 destroyed

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 scheduled_jobs_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 intake_owner

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 active_version

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 is_duplicate_of

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 queue

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 document_status

  data_type: 'enum'
  default_value: 'original'
  extra: {custom_type_name => "documentstatus",list => ["original","copy","replaced","converted"]}
  is_nullable: 0

=head2 generator

  data_type: 'text'
  is_nullable: 1

=head2 lock_timestamp

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 lock_subject_id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 lock_subject_name

  data_type: 'text'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 confidential

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 rejected_by_display_name

  data_type: 'text'
  is_nullable: 1

=head2 intake_group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 intake_role_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 skip_intake

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 created_by_display_name

  data_type: 'text'
  is_nullable: 1

=head2 shared

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  { data_type => "text", default_value => "file", is_nullable => 1 },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "search_order",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "file_id_seq",
  },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
  "extension",
  { data_type => "varchar", is_nullable => 0, size => 10 },
  "root_file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "version",
  { data_type => "integer", default_value => 1, is_nullable => 1 },
  "case_id",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "metadata_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "subject_id",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "directory_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "creation_reason",
  { data_type => "text", is_nullable => 0 },
  "accepted",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "rejection_reason",
  { data_type => "text", is_nullable => 1 },
  "reject_to_queue",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "is_duplicate_name",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "publish_pip",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "publish_website",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
  "created_by",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "modified_by",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted_by",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "destroyed",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "scheduled_jobs_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "intake_owner",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "active_version",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "is_duplicate_of",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "queue",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "document_status",
  {
    data_type => "enum",
    default_value => "original",
    extra => {
      custom_type_name => "documentstatus",
      list => ["original", "copy", "replaced", "converted"],
    },
    is_nullable => 0,
  },
  "generator",
  { data_type => "text", is_nullable => 1 },
  "lock_timestamp",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "lock_subject_id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "lock_subject_name",
  { data_type => "text", is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "confidential",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "rejected_by_display_name",
  { data_type => "text", is_nullable => 1 },
  "intake_group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "intake_role_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "skip_intake",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "created_by_display_name",
  { data_type => "text", is_nullable => 1 },
  "shared",
  { data_type => "boolean", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 custom_object_relationships

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectRelationship>

=cut

__PACKAGE__->has_many(
  "custom_object_relationships",
  "Zaaksysteem::Schema::CustomObjectRelationship",
  { "foreign.related_document_id" => "self.id" },
  undef,
);

=head2 directory_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Directory>

=cut

__PACKAGE__->belongs_to(
  "directory_id",
  "Zaaksysteem::Schema::Directory",
  { id => "directory_id" },
);

=head2 file_annotations

Type: has_many

Related object: L<Zaaksysteem::Schema::FileAnnotation>

=cut

__PACKAGE__->has_many(
  "file_annotations",
  "Zaaksysteem::Schema::FileAnnotation",
  { "foreign.file_id" => "self.id" },
  undef,
);

=head2 file_case_documents

Type: has_many

Related object: L<Zaaksysteem::Schema::FileCaseDocument>

=cut

__PACKAGE__->has_many(
  "file_case_documents",
  "Zaaksysteem::Schema::FileCaseDocument",
  { "foreign.file_id" => "self.id" },
  undef,
);

=head2 file_derivatives

Type: has_many

Related object: L<Zaaksysteem::Schema::FileDerivative>

=cut

__PACKAGE__->has_many(
  "file_derivatives",
  "Zaaksysteem::Schema::FileDerivative",
  { "foreign.file_id" => "self.id" },
  undef,
);

=head2 file_is_duplicate_ofs

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "file_is_duplicate_ofs",
  "Zaaksysteem::Schema::File",
  { "foreign.is_duplicate_of" => "self.id" },
  undef,
);

=head2 file_root_file_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "file_root_file_ids",
  "Zaaksysteem::Schema::File",
  { "foreign.root_file_id" => "self.id" },
  undef,
);

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);

=head2 intake_group_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Groups>

=cut

__PACKAGE__->belongs_to(
  "intake_group_id",
  "Zaaksysteem::Schema::Groups",
  { id => "intake_group_id" },
);

=head2 intake_role_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Roles>

=cut

__PACKAGE__->belongs_to(
  "intake_role_id",
  "Zaaksysteem::Schema::Roles",
  { id => "intake_role_id" },
);

=head2 is_duplicate_of

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to(
  "is_duplicate_of",
  "Zaaksysteem::Schema::File",
  { id => "is_duplicate_of" },
);

=head2 metadata_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::FileMetadata>

=cut

__PACKAGE__->belongs_to(
  "metadata_id",
  "Zaaksysteem::Schema::FileMetadata",
  { id => "metadata_id" },
);

=head2 root_file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to(
  "root_file_id",
  "Zaaksysteem::Schema::File",
  { id => "root_file_id" },
);

=head2 scheduled_jobs_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ScheduledJobs>

=cut

__PACKAGE__->belongs_to(
  "scheduled_jobs_id",
  "Zaaksysteem::Schema::ScheduledJobs",
  { id => "scheduled_jobs_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-02-13 08:06:07
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lQklc1nfnUTCrWO6mbqhdg

__PACKAGE__->belongs_to("case", "Zaaksysteem::Schema::Zaak", { id => "case_id" });
__PACKAGE__->belongs_to(
  "publish_type",
  "Zaaksysteem::Schema::PublishTypes",
  { id => "publish_type_id" },
);
__PACKAGE__->belongs_to(
  "directory",
  "Zaaksysteem::Schema::Directory",
  { id => "directory_id" },
);

__PACKAGE__->belongs_to(
  "metadata",
  "Zaaksysteem::Schema::FileMetadata",
  { id => "metadata_id" },
);
__PACKAGE__->belongs_to(
  "filestore",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);
__PACKAGE__->belongs_to(
  "event",
  "Zaaksysteem::Schema::Event",
  { id => "event_id" },
);
__PACKAGE__->belongs_to(
  "root_file",
  "Zaaksysteem::Schema::File",
  { id => "root_file_id" },
);

# Cascade copy needs to be disabled to be able to simply copy row
# properties. Otherwise it will freak out at the fact that there
# are rows pointing to it and attempt to copy those as well.
__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.root_file_id" => "self.id" },
  {cascade_copy => 0},
);
__PACKAGE__->has_many(
  "file_root_file_ids",
  "Zaaksysteem::Schema::File",
  { "foreign.root_file_id" => "self.id" },
  { cascade_copy => 0 },
);
__PACKAGE__->has_many(
  "file_is_duplicate_ofs",
  "Zaaksysteem::Schema::File",
  { "foreign.is_duplicate_of" => "self.id" },
  { cascade_copy => 0 },
);

__PACKAGE__->has_many(
  "case_documents",
  "Zaaksysteem::Schema::FileCaseDocument",
  { "foreign.file_id" => "self.id" },
  {cascade_copy => 0},
);

__PACKAGE__->has_many(
  "file_derivatives",
  "Zaaksysteem::Schema::FileDerivative",
  { "foreign.file_id" => "self.id" },
  { cascade_copy => 0},
);

__PACKAGE__->belongs_to(
  "metadata_id",
  "Zaaksysteem::Schema::FileMetadata",
  { id => "metadata_id" },
  { join_type => 'LEFT', cascade_copy => 0 }
);




__PACKAGE__->resultset_class('Zaaksysteem::Backend::File::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::File::Component
    +Zaaksysteem::Helper::ToJSON
/);

__PACKAGE__->add_columns('rejection_reason',
    { %{ __PACKAGE__->column_info('rejection_reason') },
    is_serializable => 1,
});

# Until we merge DROPOUT this is the least ugly way to inflate a column
# to something by default. The eval is there because the attribute it is
# calling is only set by Catalyst. This should NOT be maintainted
# long-term.
__PACKAGE__->inflate_column('created_by', {
    inflate => sub {
        my ($self, $object) = @_;

        return _get_subject_name('created_by', $object, $self);
    },
});

use JSON::XS qw();

sub _get_subject_name {
    my ($key, $object, $value) = @_;

    return $value unless $value;

    my $result = eval {
        my $column = $key . '_properties';
        if ($object->has_column_loaded($column)) {
            my $raw = $object->get_column($column);
            if (!$raw) {
                return "Onbekende medewerker";
            }
            return JSON::XS->new->decode($raw)->{displayname};
        } else {
            return $object->result_source->schema->betrokkene_model->get({}, $value)->display_name;
        }
    };

    if ($@) {
        return $value;
    }

    return $result;
}

__PACKAGE__->inflate_column('modified_by', {
    inflate => sub {
        my ($self, $object) = @_;

        return _get_subject_name('modified_by', $object, $self);
    },
});

__PACKAGE__->inflate_column('deleted_by', {
    inflate => sub {
        my ($self, $object) = @_;

        return _get_subject_name('deleted_by', $object, $self);
    },
});

__PACKAGE__->belongs_to(
  "root_file_id",
  "Zaaksysteem::Schema::File",
  { id => "root_file_id" },
  { join_type => 'LEFT' }
);


__PACKAGE__->belongs_to(
  "directory_id",
  "Zaaksysteem::Schema::Directory",
  { id => "directory_id" },
  { join_type => 'LEFT' }
);

__PACKAGE__->has_many(
  "file_case_documents",
  "Zaaksysteem::Schema::FileCaseDocument",
  { "foreign.file_id" => "self.id",
    "foreign.case_id" => "self.case_id" },
  undef,
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
