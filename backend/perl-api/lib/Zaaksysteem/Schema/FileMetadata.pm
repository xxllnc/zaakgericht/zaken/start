use utf8;
package Zaaksysteem::Schema::FileMetadata;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::FileMetadata

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<file_metadata>

=cut

__PACKAGE__->table("file_metadata");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'file_metadata_id_seq'

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 trust_level

  data_type: 'text'
  default_value: 'Zaakvertrouwelijk'
  is_nullable: 0

=head2 origin

  data_type: 'text'
  is_nullable: 1

=head2 document_category

  data_type: 'text'
  is_nullable: 1

=head2 origin_date

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 pronom_format

  data_type: 'text'
  is_nullable: 1

=head2 appearance

  data_type: 'text'
  is_nullable: 1

=head2 structure

  data_type: 'text'
  is_nullable: 1

=head2 creation_date

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 document_source

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "file_metadata_id_seq",
  },
  "description",
  { data_type => "text", is_nullable => 1 },
  "trust_level",
  {
    data_type     => "text",
    default_value => "Zaakvertrouwelijk",
    is_nullable   => 0,
  },
  "origin",
  { data_type => "text", is_nullable => 1 },
  "document_category",
  { data_type => "text", is_nullable => 1 },
  "origin_date",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "pronom_format",
  { data_type => "text", is_nullable => 1 },
  "appearance",
  { data_type => "text", is_nullable => 1 },
  "structure",
  { data_type => "text", is_nullable => 1 },
  "creation_date",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "document_source",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->has_many(
  "bibliotheek_kenmerkens",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { "foreign.file_metadata_id" => "self.id" },
  undef,
);

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.metadata_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-07-08 12:17:27
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:d7y5Qcq+NunfbcZCQsotTA

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Helper::ToJSON
    +Zaaksysteem::Backend::FileMetadata::Component
/);

# Prevent making an entirely new file row when copying metadata
__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.metadata_id" => "self.id" },
  { cascade_copy => 0 },
);

__PACKAGE__->add_columns("+description", { is_serializable => 1});
__PACKAGE__->add_columns("+origin_date", {  timezone => "floating", floating_tz_ok => 1 });

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

