use utf8;
package Zaaksysteem::Schema::LegalEntityType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::LegalEntityType

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<legal_entity_type>

=cut

__PACKAGE__->table("legal_entity_type");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'legal_entity_type_id_seq'

=head2 code

  data_type: 'integer'
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 active

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "legal_entity_type_id_seq",
  },
  "code",
  { data_type => "integer", is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "active",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<legal_entity_type_code_key>

=over 4

=item * L</code>

=back

=cut

__PACKAGE__->add_unique_constraint("legal_entity_type_code_key", ["code"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-05-05 10:30:27
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:+HLq4wUpWCaztlFinKt9Og


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
