use utf8;
package Zaaksysteem::Schema::Queue20210501;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Queue20210501

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<queue_20210501>

=cut

__PACKAGE__->table("queue_20210501");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 status

  data_type: 'text'
  default_value: 'pending'
  is_nullable: 0

=head2 type

  data_type: 'text'
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 0

=head2 data

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: statement_timestamp()
  is_nullable: 0
  timezone: 'UTC'

=head2 date_started

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_finished

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 parent_id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 priority

  data_type: 'integer'
  default_value: 1000
  is_nullable: 0

=head2 metadata

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "status",
  { data_type => "text", default_value => "pending", is_nullable => 0 },
  "type",
  { data_type => "text", is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 0 },
  "data",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"statement_timestamp()",
    is_nullable   => 0,
    timezone      => "UTC",
  },
  "date_started",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_finished",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "parent_id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "priority",
  { data_type => "integer", default_value => 1000, is_nullable => 0 },
  "metadata",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-04-01 13:52:10
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OW8SzgxQIWylFi0ZkZqTyA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
