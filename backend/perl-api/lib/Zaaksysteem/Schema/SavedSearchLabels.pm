use utf8;
package Zaaksysteem::Schema::SavedSearchLabels;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SavedSearchLabels

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<saved_search_labels>

=cut

__PACKAGE__->table("saved_search_labels");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'saved_search_labels_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 label

  data_type: 'text'
  is_nullable: 0
  original: {data_type => "varchar"}

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "saved_search_labels_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "label",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<saved_search_labels_label_key>

=over 4

=item * L</label>

=back

=cut

__PACKAGE__->add_unique_constraint("saved_search_labels_label_key", ["label"]);

=head2 C<saved_search_labels_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("saved_search_labels_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 saved_search_label_mappings

Type: has_many

Related object: L<Zaaksysteem::Schema::SavedSearchLabelMapping>

=cut

__PACKAGE__->has_many(
  "saved_search_label_mappings",
  "Zaaksysteem::Schema::SavedSearchLabelMapping",
  { "foreign.label_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-01-09 10:27:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CfzABGiAp2p+bYWoy0yVlg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
