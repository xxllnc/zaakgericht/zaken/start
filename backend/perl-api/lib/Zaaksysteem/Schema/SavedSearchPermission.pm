use utf8;
package Zaaksysteem::Schema::SavedSearchPermission;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SavedSearchPermission

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<saved_search_permission>

=cut

__PACKAGE__->table("saved_search_permission");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 saved_search_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 role_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 permission

  data_type: 'text'
  is_nullable: 0

=head2 sort_order

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "saved_search_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "role_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "permission",
  { data_type => "text", is_nullable => 0 },
  "sort_order",
  { data_type => "integer", is_nullable => 1 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<saved_search_permission_saved_search_id_group_id_role_id_pe_key>

=over 4

=item * L</saved_search_id>

=item * L</group_id>

=item * L</role_id>

=item * L</permission>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "saved_search_permission_saved_search_id_group_id_role_id_pe_key",
  ["saved_search_id", "group_id", "role_id", "permission"],
);

=head1 RELATIONS

=head2 group_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Groups>

=cut

__PACKAGE__->belongs_to(
  "group_id",
  "Zaaksysteem::Schema::Groups",
  { id => "group_id" },
);

=head2 role_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Roles>

=cut

__PACKAGE__->belongs_to("role_id", "Zaaksysteem::Schema::Roles", { id => "role_id" });

=head2 saved_search_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::SavedSearch>

=cut

__PACKAGE__->belongs_to(
  "saved_search_id",
  "Zaaksysteem::Schema::SavedSearch",
  { id => "saved_search_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-08-19 12:43:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wTCZG1raNet7VXviQFmRUA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
