use utf8;
package Zaaksysteem::Schema::StyleConfiguration;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::StyleConfiguration

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<style_configuration>

=cut

__PACKAGE__->table("style_configuration");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 tenant

  data_type: 'text'
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 content

  data_type: 'text'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 1
  timezone: 'UTC'

=head2 extension

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=head2 mimetype

  data_type: 'varchar'
  is_nullable: 0
  size: 160

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "tenant",
  { data_type => "text", is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
  "content",
  { data_type => "text", is_nullable => 1 },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 1,
    timezone      => "UTC",
  },
  "extension",
  { data_type => "varchar", is_nullable => 0, size => 10 },
  "mimetype",
  { data_type => "varchar", is_nullable => 0, size => 160 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<style_configuration_tenant_name_key>

=over 4

=item * L</tenant>

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("style_configuration_tenant_name_key", ["tenant", "name"]);

=head2 C<style_configuration_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("style_configuration_uuid_key", ["uuid"]);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-03-07 08:12:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/d7AAkl7lrSTa0u5pv22rg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.