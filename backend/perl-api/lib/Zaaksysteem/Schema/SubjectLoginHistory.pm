use utf8;
package Zaaksysteem::Schema::SubjectLoginHistory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SubjectLoginHistory

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<subject_login_history>

=cut

__PACKAGE__->table("subject_login_history");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'subject_login_history_id_seq'

=head2 ip

  data_type: 'inet'
  is_nullable: 0

=head2 subject_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 subject_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 success

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 method

  data_type: 'text'
  is_nullable: 0

=head2 date_attempt

  data_type: 'timestamp with time zone'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 0
  timezone: 'UTC'

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "subject_login_history_id_seq",
  },
  "ip",
  { data_type => "inet", is_nullable => 0 },
  "subject_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "subject_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "success",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "method",
  { data_type => "text", is_nullable => 0 },
  "date_attempt",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 subject_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_id",
  "Zaaksysteem::Schema::Subject",
  { id => "subject_id" },
);

=head2 subject_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_uuid",
  "Zaaksysteem::Schema::Subject",
  { uuid => "subject_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-11-22 07:28:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dtM6SoEmzoluAxpv6OhFQg

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
