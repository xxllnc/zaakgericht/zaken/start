use utf8;
package Zaaksysteem::Schema::Thread;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Thread

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<thread>

=cut

__PACKAGE__->table("thread");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'thread_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 contact_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 contact_displayname

  data_type: 'text'
  is_nullable: 1

=head2 case_id

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 0
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 thread_type

  data_type: 'text'
  is_nullable: 0

=head2 last_message_cache

  data_type: 'text'
  is_nullable: 0

=head2 message_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 unread_pip_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 unread_employee_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 attachment_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_notification

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "thread_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "contact_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "contact_displayname",
  { data_type => "text", is_nullable => 1 },
  "case_id",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 0, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "thread_type",
  { data_type => "text", is_nullable => 0 },
  "last_message_cache",
  { data_type => "text", is_nullable => 0 },
  "message_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "unread_pip_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "unread_employee_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "attachment_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_notification",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<thread_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("thread_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 thread_messages

Type: has_many

Related object: L<Zaaksysteem::Schema::ThreadMessage>

=cut

__PACKAGE__->has_many(
  "thread_messages",
  "Zaaksysteem::Schema::ThreadMessage",
  { "foreign.thread_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-07-08 13:29:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KYDsfCg6RPRejFZJSP4Yfg

__PACKAGE__->inflate_column('last_message_cache', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
