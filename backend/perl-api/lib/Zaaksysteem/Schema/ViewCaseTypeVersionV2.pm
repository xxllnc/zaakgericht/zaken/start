use utf8;
package Zaaksysteem::Schema::ViewCaseTypeVersionV2;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ViewCaseTypeVersionV2

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<view_case_type_version_v2>

=cut

__PACKAGE__->table("view_case_type_version_v2");
__PACKAGE__->result_source_instance->view_definition(" SELECT ztn.uuid,\n    zt.uuid AS casetype_uuid,\n    zt.active,\n        CASE\n            WHEN (bc.uuid IS NULL) THEN NULL::json\n            ELSE json_build_object('uuid', bc.uuid, 'name', bc.naam)\n        END AS catalog_folder,\n    json_build_object('name', ztn.titel, 'identification', ztn.code, 'tags', ztn.zaaktype_trefwoorden, 'description', ztn.zaaktype_omschrijving, 'case_summary', ztd.extra_informatie, 'case_public_summary', ztd.extra_informatie_extern, 'legal_period',\n        CASE\n            WHEN ((ztd.afhandeltermijn IS NULL) AND (ztd.afhandeltermijn_type IS NULL)) THEN NULL::json\n            ELSE json_build_object('value', ztd.afhandeltermijn, 'type', ztd.afhandeltermijn_type)\n        END, 'service_period',\n        CASE\n            WHEN ((ztd.servicenorm IS NULL) AND (ztd.servicenorm_type IS NULL)) THEN NULL::json\n            ELSE json_build_object('value', ztd.servicenorm, 'type', ztd.servicenorm_type)\n        END) AS general_attributes,\n    json_build_object('process_description', ztd.procesbeschrijving, 'initiator_type', ztd.handelingsinitiator, 'motivation', ((ztn.properties)::json ->> 'aanleiding'::text), 'purpose', ((ztn.properties)::json ->> 'doel'::text), 'archive_classification_code', ((ztn.properties)::json ->> 'archiefclassicatiecode'::text), 'designation_of_confidentiality',\n        CASE\n            WHEN (((ztn.properties)::json ->> 'vertrouwelijkheidsaanduiding'::text) = '-'::text) THEN NULL::text\n            ELSE ((ztn.properties)::json ->> 'vertrouwelijkheidsaanduiding'::text)\n        END, 'responsible_subject', ((ztn.properties)::json ->> 'verantwoordelijke'::text), 'responsible_relationship', ((ztn.properties)::json ->> 'verantwoordingsrelatie'::text), 'possibility_for_objection_and_appeal', text_to_bool(((ztn.properties)::json ->> 'beroep_mogelijk'::text)), 'publication', text_to_bool(((ztn.properties)::json ->> 'publicatie'::text)), 'publication_text', ((ztn.properties)::json ->> 'publicatietekst'::text), 'bag', text_to_bool(((ztn.properties)::json ->> 'bag'::text)), 'lex_silencio_positivo', text_to_bool(((ztn.properties)::json ->> 'lex_silencio_positivo'::text)), 'may_postpone', text_to_bool(((ztn.properties)::json ->> 'opschorten_mogelijk'::text)), 'may_extend', text_to_bool(((ztn.properties)::json ->> 'verlenging_mogelijk'::text)), 'extension_period',\n        CASE\n            WHEN (((ztn.properties)::json ->> 'verlengingstermijn'::text) ~ '^\\d+\$'::text) THEN (((ztn.properties)::json ->> 'verlengingstermijn'::text))::integer\n            ELSE NULL::integer\n        END, 'adjourn_period',\n        CASE\n            WHEN (((ztn.properties)::json ->> 'verdagingstermijn'::text) ~ '^\\d+\$'::text) THEN (((ztn.properties)::json ->> 'verdagingstermijn'::text))::integer\n            ELSE NULL::integer\n        END, 'penalty_law', text_to_bool(((ztn.properties)::json ->> 'wet_dwangsom'::text)), 'wkpb_applies', text_to_bool(((ztn.properties)::json ->> 'wkpb'::text)), 'e_webform', ((ztn.properties)::json ->> 'e_formulier'::text), 'legal_basis', ztd.grondslag, 'local_basis', ((ztn.properties)::json ->> 'lokale_grondslag'::text), 'gdpr', json_build_object('enabled', text_to_bool((((ztn.properties)::json -> 'gdpr'::text) ->> 'enabled'::text)), 'kind', json_build_object('basic_details', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'basic_details'::text)), 'personal_id_number', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'personal_id_number'::text)), 'income', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'income'::text)), 'race_or_ethniticy', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'race_or_ethniticy'::text)), 'political_views', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'political_views'::text)), 'religion', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'religion'::text)), 'membership_union', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'membership_union'::text)), 'genetic_or_biometric_data', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'genetic_or_biometric_data'::text)), 'health', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'health'::text)), 'sexual_identity', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'sexual_identity'::text)), 'criminal_record', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'criminal_record'::text)), 'offspring', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'personal'::text) ->> 'offspring'::text))), 'source', json_build_object('public_source', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'source_personal'::text) ->> 'public_source'::text)), 'registration', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'source_personal'::text) ->> 'registration'::text)), 'partner', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'source_personal'::text) ->> 'partner'::text)), 'sender', text_to_bool(((((ztn.properties)::json -> 'gdpr'::text) -> 'source_personal'::text) ->> 'sender'::text))), 'processing_type', (((ztn.properties)::json -> 'gdpr'::text) ->> 'processing_type'::text), 'process_foreign_country', text_to_bool((((ztn.properties)::json -> 'gdpr'::text) ->> 'process_foreign_country'::text)), 'process_foreign_country_reason', (((ztn.properties)::json -> 'gdpr'::text) ->> 'process_foreign_country_reason'::text), 'processing_legal', (((ztn.properties)::json -> 'gdpr'::text) ->> 'processing_legal'::text), 'processing_legal_reason', (((ztn.properties)::json -> 'gdpr'::text) ->> 'processing_legal_reason'::text))) AS documentation,\n    json_build_object('trigger', ztn.trigger, 'allowed_requestor_types', ( SELECT json_agg(zb.betrokkene_type) AS json_agg\n           FROM zaaktype_betrokkenen zb\n          WHERE (zb.zaaktype_node_id = ztn.id)), 'preset_requestor', legacy_subject_id_as_json(ztd.preset_client), 'api_preset_assignee', legacy_subject_id_as_json((((ztn.properties)::json ->> 'preset_owner_identifier'::text))::character varying), 'address_requestor_use_as_correspondence', COALESCE((ztn.adres_aanvrager)::boolean, false), 'address_requestor_use_as_case_address', COALESCE((ztn.adres_andere_locatie)::boolean, false), 'address_requestor_show_on_map', COALESCE(ztn.adres_geojson, false)) AS relations,\n    json_build_object('public_confirmation_title', ((ztn.properties)::json ->> 'public_confirmation_title'::text), 'public_confirmation_message', ((ztn.properties)::json ->> 'public_confirmation_message'::text), 'case_location_message', ((ztn.properties)::json ->> 'case_location_message'::text), 'pip_view_message', ((ztn.properties)::json ->> 'pip_view_message'::text), 'actions', json_build_object('enable_webform', COALESCE((ztn.webform_toegang)::boolean, false), 'create_delayed', COALESCE((((ztn.properties)::json ->> 'delayed'::text))::boolean, false), 'address_check', COALESCE((((ztn.properties)::json ->> 'case_location_check'::text))::boolean, false), 'reuse_casedata', COALESCE((ztn.aanvrager_hergebruik)::boolean, false), 'enable_online_payment', COALESCE((ztn.online_betaling)::boolean, false), 'enable_manual_payment', COALESCE((((ztn.properties)::json ->> 'offline_betaling'::text))::boolean, false), 'email_required', COALESCE(ztn.contact_info_email_required, false), 'phone_required', COALESCE(ztn.contact_info_phone_required, false), 'mobile_required', COALESCE(ztn.contact_info_mobile_phone_required, false), 'disable_captcha', COALESCE((((ztn.properties)::json ->> 'no_captcha'::text))::boolean, false), 'generate_pdf_end_webform', COALESCE((((ztn.properties)::json ->> 'pdf_registration_form'::text))::boolean, false)), 'price', json_build_object('web', ztd.pdc_tarief, 'frontdesk', ((ztn.properties)::json ->> 'pdc_tarief_balie'::text), 'phone', ((ztn.properties)::json ->> 'pdc_tarief_telefoon'::text), 'email', ((ztn.properties)::json ->> 'pdc_tarief_email'::text), 'assignee', ((ztn.properties)::json ->> 'pdc_tarief_behandelaar'::text), 'post', ((ztn.properties)::json ->> 'pdc_tarief_post'::text), 'chat', ((ztn.properties)::json ->> 'pdc_tarief_chat'::text), 'other', ((ztn.properties)::json ->> 'pdc_tarief_overige'::text))) AS webform,\n    json_build_object('allow_assigning_to_self', COALESCE((ztn.automatisch_behandelen)::boolean, false), 'allow_assigning', COALESCE((ztn.toewijzing_zaakintake)::boolean, false), 'show_confidentionality', COALESCE((((ztn.properties)::json ->> 'confidentiality'::text))::boolean, false), 'show_contact_details', COALESCE(ztn.contact_info_intake, false), 'allow_add_relations', COALESCE(ztn.extra_relaties_in_aanvraag, false)) AS registrationform,\n    json_build_object('disable_pip_for_requestor', COALESCE(ztn.prevent_pip, false), 'lock_registration_phase', COALESCE((((ztn.properties)::json ->> 'lock_registration_phase'::text))::boolean, false), 'queue_coworker_changes', COALESCE((((ztn.properties)::json ->> 'queue_coworker_changes'::text))::boolean, false), 'allow_external_task_assignment', COALESCE((((ztn.properties)::json ->> 'allow_external_task_assignment'::text))::boolean, false), 'default_document_folders', (((ztn.properties)::json ->> 'default_directories'::text))::json, 'default_html_email_template', ((ztn.properties)::json ->> 'default_html_email_template'::text)) AS case_dossier,\n    json_build_object('api_can_transition', COALESCE((((ztn.properties)::json ->> 'api_can_transition'::text))::boolean, false), 'notifications', json_build_object('external_notify_on_new_case', COALESCE((((ztn.properties)::json ->> 'notify_on_new_case'::text))::boolean, false), 'external_notify_on_new_document', COALESCE((((ztn.properties)::json ->> 'notify_on_new_document'::text))::boolean, false), 'external_notify_on_new_message', COALESCE((((ztn.properties)::json ->> 'notify_on_new_message'::text))::boolean, false), 'external_notify_on_exceed_term', COALESCE((((ztn.properties)::json ->> 'notify_on_exceed_term'::text))::boolean, false), 'external_notify_on_allocate_case', COALESCE((((ztn.properties)::json ->> 'notify_on_allocate_case'::text))::boolean, false), 'external_notify_on_phase_transition', COALESCE((((ztn.properties)::json ->> 'notify_on_phase_transition'::text))::boolean, false), 'external_notify_on_task_change', COALESCE((((ztn.properties)::json ->> 'notify_on_task_change'::text))::boolean, false), 'external_notify_on_label_change', COALESCE((((ztn.properties)::json ->> 'notify_on_label_change'::text))::boolean, false), 'external_notify_on_subject_change', COALESCE((((ztn.properties)::json ->> 'notify_on_subject_change'::text))::boolean, false))) AS api,\n    COALESCE(ctmv2_get_casetype_version_authorization(ztn.id), '[]'::json) AS \"authorization\",\n    ctmv2_get_child_casetypes_as_json(ztn.uuid) AS child_casetype_settings,\n    ctmv2_get_phases_as_json(ztn.id) AS phases,\n    ctmv2_get_results_as_json(ztn.id) AS results\n   FROM (((zaaktype_node ztn\n     JOIN zaaktype zt ON ((ztn.zaaktype_id = zt.id)))\n     JOIN zaaktype_definitie ztd ON ((ztn.zaaktype_definitie_id = ztd.id)))\n     LEFT JOIN bibliotheek_categorie bc ON ((zt.bibliotheek_categorie_id = bc.id)))");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 casetype_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 active

  data_type: 'boolean'
  is_nullable: 1

=head2 catalog_folder

  data_type: 'json'
  is_nullable: 1

=head2 general_attributes

  data_type: 'json'
  is_nullable: 1

=head2 documentation

  data_type: 'json'
  is_nullable: 1

=head2 relations

  data_type: 'json'
  is_nullable: 1

=head2 webform

  data_type: 'json'
  is_nullable: 1

=head2 registrationform

  data_type: 'json'
  is_nullable: 1

=head2 case_dossier

  data_type: 'json'
  is_nullable: 1

=head2 api

  data_type: 'json'
  is_nullable: 1

=head2 authorization

  data_type: 'json'
  is_nullable: 1

=head2 child_casetype_settings

  data_type: 'json'
  is_nullable: 1

=head2 phases

  data_type: 'json'
  is_nullable: 1

=head2 results

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "casetype_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "active",
  { data_type => "boolean", is_nullable => 1 },
  "catalog_folder",
  { data_type => "json", is_nullable => 1 },
  "general_attributes",
  { data_type => "json", is_nullable => 1 },
  "documentation",
  { data_type => "json", is_nullable => 1 },
  "relations",
  { data_type => "json", is_nullable => 1 },
  "webform",
  { data_type => "json", is_nullable => 1 },
  "registrationform",
  { data_type => "json", is_nullable => 1 },
  "case_dossier",
  { data_type => "json", is_nullable => 1 },
  "api",
  { data_type => "json", is_nullable => 1 },
  "authorization",
  { data_type => "json", is_nullable => 1 },
  "child_casetype_settings",
  { data_type => "json", is_nullable => 1 },
  "phases",
  { data_type => "json", is_nullable => 1 },
  "results",
  { data_type => "json", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2025-02-25 10:23:32
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:spOubvLvDn+VP2FU8hTTVg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
