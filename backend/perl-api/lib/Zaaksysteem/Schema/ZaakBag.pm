use utf8;
package Zaaksysteem::Schema::ZaakBag;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaakBag

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaak_bag>

=cut

__PACKAGE__->table("zaak_bag");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_bag_id_seq'

=head2 pid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaak_id

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 bag_type

  data_type: 'enum'
  extra: {custom_type_name => "zaaksysteem_bag_types",list => ["nummeraanduiding","verblijfsobject","pand","openbareruimte"]}
  is_nullable: 1

=head2 bag_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_verblijfsobject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_openbareruimte_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_nummeraanduiding_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_pand_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_standplaats_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_ligplaats_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bag_coordinates_wsg

  data_type: 'point'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_bag_id_seq",
  },
  "pid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaak_id",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "bag_type",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "zaaksysteem_bag_types",
      list => ["nummeraanduiding", "verblijfsobject", "pand", "openbareruimte"],
    },
    is_nullable => 1,
  },
  "bag_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_verblijfsobject_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_openbareruimte_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_nummeraanduiding_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_pand_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_standplaats_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_ligplaats_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bag_coordinates_wsg",
  { data_type => "point", is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<zaak_bag_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("zaak_bag_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBag>

=cut

__PACKAGE__->belongs_to("pid", "Zaaksysteem::Schema::ZaakBag", { id => "pid" });

=head2 zaak_bags

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakBag>

=cut

__PACKAGE__->has_many(
  "zaak_bags",
  "Zaaksysteem::Schema::ZaakBag",
  { "foreign.pid" => "self.id" },
  undef,
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });

=head2 zaak_locatie_correspondenties

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_locatie_correspondenties",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.locatie_correspondentie" => "self.id" },
  undef,
);

=head2 zaak_locatie_zaaks

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_locatie_zaaks",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.locatie_zaak" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-03-21 13:23:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4U7RpZUnaL7qAHSijqsB4w

__PACKAGE__->resultset_class('Zaaksysteem::Zaken::ResultSetBag');

__PACKAGE__->load_components(
    "+Zaaksysteem::Zaken::ComponentBag",
    __PACKAGE__->load_components()
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

