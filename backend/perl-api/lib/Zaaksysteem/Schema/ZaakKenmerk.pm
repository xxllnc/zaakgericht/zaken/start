use utf8;
package Zaaksysteem::Schema::ZaakKenmerk;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaakKenmerk

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaak_kenmerk>

=cut

__PACKAGE__->table("zaak_kenmerk");

=head1 ACCESSORS

=head2 zaak_id

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_kenmerk_id_seq'

=head2 value

  data_type: 'text[]'
  is_nullable: 0

=head2 magic_string

  data_type: 'text'
  is_nullable: 0

=head2 value_type

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "zaak_id",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_kenmerk_id_seq",
  },
  "value",
  { data_type => "text[]", is_nullable => 0 },
  "magic_string",
  { data_type => "text", is_nullable => 0 },
  "value_type",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<zaak_kenmerk_bibliotheek_kenmerken_id>

=over 4

=item * L</zaak_id>

=item * L</bibliotheek_kenmerken_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "zaak_kenmerk_bibliotheek_kenmerken_id",
  ["zaak_id", "bibliotheek_kenmerken_id"],
);

=head1 RELATIONS

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-08-09 08:12:45
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YVpSR3c59kwFkOSx9i0ihw

__PACKAGE__->resultset_class('Zaaksysteem::Zaken::ResultSetZaakKenmerk');

__PACKAGE__->load_components(
    "+Zaaksysteem::Zaken::ComponentZaakKenmerk",
    __PACKAGE__->load_components()
);






# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

