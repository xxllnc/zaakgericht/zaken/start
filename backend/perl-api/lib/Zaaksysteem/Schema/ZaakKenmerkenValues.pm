package Zaaksysteem::Schema::ZaakKenmerkenValues;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaakKenmerkenValues

=cut

__PACKAGE__->table("zaak_kenmerken_values");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_kenmerken_values_id_seq'

=head2 zaak_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 value

  data_type: 'text'
  is_nullable: 1

=head2 zaak_bag_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_kenmerken_values_id_seq",
  },
  "zaak_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "value",
  { data_type => "text", is_nullable => 1 },
  "zaak_bag_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakKenmerken>

=cut

__PACKAGE__->belongs_to(
  "zaak_kenmerken_id",
  "Zaaksysteem::Schema::ZaakKenmerken",
  { id => "zaak_kenmerken_id" },
);

=head2 zaak_bag_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBag>

=cut

__PACKAGE__->belongs_to(
  "zaak_bag_id",
  "Zaaksysteem::Schema::ZaakBag",
  { id => "zaak_bag_id" },
);

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-03 09:58:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PxyW6blCTh8ojqLtaL/a0A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

