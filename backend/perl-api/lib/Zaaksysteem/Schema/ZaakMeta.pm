use utf8;
package Zaaksysteem::Schema::ZaakMeta;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaakMeta

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaak_meta>

=cut

__PACKAGE__->table("zaak_meta");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_meta_id_seq'

=head2 zaak_id

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 verlenging

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 opschorten

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 deel

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 gerelateerd

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 vervolg

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 afhandeling

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 stalled_since

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 current_deadline

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=head2 deadline_timeline

  data_type: 'jsonb'
  default_value: '[]'
  is_nullable: 0

=head2 last_modified

  data_type: 'timestamp'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 0
  timezone: 'UTC'

=head2 index_hstore

  data_type: 'hstore'
  is_nullable: 1

=head2 text_vector

  data_type: 'tsvector'
  is_nullable: 1

=head2 unread_communication_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 unaccepted_attribute_update_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 pending_changes

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=head2 unaccepted_files_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 relations_complete

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_meta_id_seq",
  },
  "zaak_id",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "verlenging",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "opschorten",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "deel",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "gerelateerd",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "vervolg",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "afhandeling",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "stalled_since",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "current_deadline",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
  "deadline_timeline",
  { data_type => "jsonb", default_value => "[]", is_nullable => 0 },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
  "index_hstore",
  { data_type => "hstore", is_nullable => 1 },
  "text_vector",
  { data_type => "tsvector", is_nullable => 1 },
  "unread_communication_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "unaccepted_attribute_update_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "pending_changes",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
  "unaccepted_files_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "relations_complete",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<zaak_meta_uniq_zaak_id>

=over 4

=item * L</zaak_id>

=back

=cut

__PACKAGE__->add_unique_constraint("zaak_meta_uniq_zaak_id", ["zaak_id"]);

=head1 RELATIONS

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-11-22 07:28:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:VWgs4nTE72PMDosiM0ziRQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::ZaakMeta::ResultSet');

__PACKAGE__->load_components(
    "+Zaaksysteem::Backend::ZaakMeta::Component",
    '+Zaaksysteem::Helper::ToJSON',
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
    "case_api_v1_view",
    "Zaaksysteem::Schema::CaseV1",
    {
        "foreign.number" => "self.zaak_id",
    },
);

require JSON::XS;

__PACKAGE__->inflate_column('current_deadline', {
  inflate => sub { JSON::XS->new->decode(shift // '{}') },
  deflate => sub { JSON::XS->new->encode(shift // {}) },
});

__PACKAGE__->inflate_column('pending_changes', {
  inflate => sub { JSON::XS->new->decode(shift // '{}') },
  deflate => sub { JSON::XS->new->encode(shift // {}) },
});


__PACKAGE__->inflate_column('deadline_timeline', {
  inflate => sub { JSON::XS->new->decode(shift // '[]') },
  deflate => sub { JSON::XS->new->encode(shift // [])},
});

__PACKAGE__->inflate_column('index_hstore', {
    inflate => sub {
        Zaaksysteem::DB::HStore::decode(shift);
    },
    deflate => sub {
        Zaaksysteem::DB::HStore::encode(shift);
    }
});

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

