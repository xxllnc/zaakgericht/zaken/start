package Zaaksysteem::Search::Case::Object;
use Zaaksysteem::Moose;

with qw(
    Zaaksysteem::Moose::Role::Schema
    Zaaksysteem::Search::Role::Base
);

=head1 NAME

Zaaksysteem::Search::Case::Object - Search case object

=head1 DESCRIPTION

A migration class for Zaaksysteem::Search::Object to remove the dependency to object data

=head1 SYNOPSIS

    use Foo;

=cut

sub _build_resultset {
    my $self = shift;
    return $self->has_base_rs ? $self->base_rs : $self->build_resultset('Zaak');
};

sub _build_zql_parser {
    my $self = shift;
    return Zaaksysteem::Search::Case::ZQL->new($self->query);
};

sub search_select {
    my $self = shift;

    my $rs = $self->resultset->search_restricted('search', $self->user);

    $rs =  $rs->search_zql($self->query);

    if ($self->is_intake) {
        $rs = $self->search_intake($rs);
    }

    if ($self->is_search_distinct) {
        $rs = $self->search_distinct($rs);
    }

    return $rs;
}

sub search_documents {
    my $self = shift;
    my $rs = $self->resultset->search_restricted('read');
    return $rs->search_zql($self->query);
}

sub search_intake {
    my ($self, $rs) = @_;

    unless ($rs) {
        $rs = $self->resultset->search_restricted('search');
        $rs = $rs->search_zql($self->query);
    }

    return $rs unless $self->is_intake;

    my @seeers = ();
    my $mine   = { 'me.status' => ['new'] };
    $mine->{'me.behandelaar_gm_id'} = $self->user->uidnumber;
    push @seeers, $mine;

    my $ou_id = $self->user->primary_group->id;

    my @role_ids = map { $_->id } @{ $self->user->roles };

    push @seeers,
        {
        '-and' => [
            {
                '-and' => [
                    { '-or' => { route_role => \@role_ids } },
                    {
                        '-or' => [
                            { 'route_ou' => $ou_id },
                            { 'route_ou' => undef },
                        ],
                    }
                ],
            },
            { 'behandelaar' => undef },
        ],
        };

    # Special case: divver, they can see all zaken without a complete role.
    if ($self->user->has_legacy_permission('zaak_route_default')) {
        push @seeers, { 'me.route_role' => undef };
    }

    my %where = (
        '-or'                 => \@seeers,
        'me.deleted'          => undef,
        'me.registratiedatum' => { '<' => \'NOW()' },
        'me.status'           => 'new',
    );

    return $rs->search_rs(\%where);
}

sub search_distinct {
    my ($self, $rs) = @_;

    if (!$self->is_search_distinct) {
        croak("Cannot search distinct on something that isn't distinct");
    }

    my $opts = $self->zql->cmd->dbixify_opts;
    my $tr   = sub { my $val = shift; $val =~ s/\$/./g; return $val };

    my @result;
    while (my $object = $rs->next) {
        push(
            @result,
            {
                count => int($object->get_column('count')),
                map { $tr->($_) => $object->get_column($_) } @{ $opts->{as} }
            }
        );
    }

    return \@result;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
