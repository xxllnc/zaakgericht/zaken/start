package Zaaksysteem::Session::Invitation::Model;

use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Session::Invitation::Model - Session invitation abstractsions

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Crypt::OpenSSL::Random qw[random_pseudo_bytes];
use Moose::Util::TypeConstraints qw[role_type];

use Zaaksysteem::Types qw[Store];
use Zaaksysteem::Object::Syntax;
use Zaaksysteem::Object::Types::Session::Invitation;

=head1 ATTRIBUTES

=head2 redis

A reference to a Zaaksyteem::Redis like Object

=cut

has redis => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Store::Redis',
    required => 1,
);

=head1 METHODS

=head2 create

Creates a new invitation and returns the new
L<instance|Zaaksysteem::Object::Types::Session::Invitation>.

    my $invitation = $model->create({
        subject => $my_user_object,
        date_expires => DateTime->now->add(days => 5),
        # optionally
        action_path => '/my/path/to/catalyst/action',
        object => $my_reference_object
    });

    notify_user(token => $invitation->token);

=cut

define_profile create => (
    required => {
        subject => 'Zaaksysteem::Backend::Subject::Component',
        date_expires => 'DateTime'
    },
    optional => {
        action_path => 'Str',
        object => role_type('Zaaksysteem::Object::Reference')
    }
);

sig create => 'HashRef';

sub create {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $token = unpack 'H*', random_pseudo_bytes(8);

    my $object = $params->{object}->_ref if exists $params->{object};

    my $session_redis = {
        subject => $params->{subject}->TO_JSON,
        $object
        ? (
            object => {
                id      => $object->id,
                type    => $object->type,
                preview => $object->TO_STRING
            }
            )
        : (),
        $params->{action_path} ? ( action_path => $params->{action_path} ) : (),
    };

    my $key = "session:invite:$token";
    $self->redis->set_json($key, $session_redis);
    $self->redis->expire_at($key, $params->{date_expires}->epoch);

    return Zaaksysteem::Object::Types::Session::Invitation->new(
        token   => $token,
        # ugh..
        subject => $params->{subject}->as_object->_ref,
        $object                ? (object      => $object)                : (),
        $params->{action_path} ? (action_path => $params->{action_path}) : (),
    );
}

=head2 validate

Validate a given token. If an invitation for the token is found, it will be
removed during this validation step. If a token was valid, the
L<invitation|Zaaksysteem::Object::Types::Session::Invitation> will be
returned.

    my $invitation = $model->validate('abc123xyz');

    die "token invalid" unless defined $invitation;

=cut

sig validate => 'Str';

sub validate {
    my $self = shift;
    my $token = shift;

    my $key = "session:invite:$token";
    my $invite = $self->redis->get_json($key);
    return unless $invite;
    $self->redis->del($key);

    my $subject = Zaaksysteem::Object::Reference::Instance->new(
        id      => $invite->{subject}{uuid},
        type    => 'subject',
        preview => $invite->{subject}{display_name},
    );

    my $object;
    if (my $o = $invite->{object}) {
        $object = Zaaksysteem::Object::Reference::Instance->new(
            id      => $o->{id},
            type    => $o->{type},
            preview => $o->{preview},
        );
    }

    return Zaaksysteem::Object::Types::Session::Invitation->new(
        subject => $subject,
        $object ? (object => $object) : (),
        token => $token,
        $invite->{action_path} ? (action_path => $invite->{action_path}) : (),
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017-2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
