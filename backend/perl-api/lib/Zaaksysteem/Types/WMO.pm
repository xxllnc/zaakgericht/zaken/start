package Zaaksysteem::Types::WMO;

use MooseX::Types -declare => [qw(
    COD172
    COD539
    COD749
    COD825
    COD828

    WMO020
    WMO756
    WMO757

    WMOaanbieder
)];

use MooseX::Types::Moose qw(Str Int Num Bool ArrayRef HashRef Item);

use DateTime::Format::ISO8601 qw[];

=head1 NAME

Zaaksysteem::Types::WMO - Custom types for Zaaksysteem and WMO

=head1 SYNOPSIS

    package MyClass;
    use Moose;
    use Zaaksysteem::Types::WMO qw(TYPE1 TYPE2);

    has attr => (
        isa => TYPE1,
        is => 'ro',
    );

=head1 DESCRIPTION

All codes can be viewed online at the following URL, appended with the code.

L<https://modellen.istandaarden.nl/preview/wmo/index.php/XXXxxx>

=head1 AVAILABLE TYPES

=head2 COD172

L<https://modellen.istandaarden.nl/preview/wmo/index.php/COD172>

=cut


subtype COD172, as Int,
    where {
        my $int = shift;
        return 1 if $int =~ /^[012346]$/;
        return 0;
    };

=head2 COD539

L<https://modellen.istandaarden.nl/preview/wmo/index.php/COD539>

=cut

subtype COD539, as Int,
    where {
        my $int = shift;
        return 1 if ($int =~ /^[1234569]$/);
        return 0;
    };

=head2 COD749

L<https://modellen.istandaarden.nl/preview/wmo/index.php/COD749>

=cut

subtype COD749, as Int,
    where {
        my $int = shift;
        return 1 if ($int =~ /^[1234]$/);
        return 0;
    };

=head2 COD825

L<https://modellen.istandaarden.nl/preview/wmo/index.php/COD825>

=cut


subtype COD825, as Int,
    where {
        my $int = shift;
        return 1 if (
            $int >= 101 && $int <= 114 ||
            $int >= 201 && $int <= 214 ||
            $int >= 301 && $int <= 309 ||
            $int >= 401 && $int <= 411 ||
            $int >= 501 && $int <= 506 ||
            $int >= 601 && $int <= 611
        );
        return 0;
    };

=head2 COD828

L<https://modellen.istandaarden.nl/preview/wmo/index.php/COD828>

=cut


subtype COD828, as Int,
    where {
        my $int = shift;
        return 1 if $int =~ /^[01239]$/;
        return 0;
    };

=head2 WMO020

L<https://modellen.istandaarden.nl/preview/wmo/index.php/WMO020>

=cut

subtype WMO020, as Int,
    where {
        my $int = shift;
        return 1 if ($int >= 1 && $int <= 18);
        return 0;
    };

=head2 WMO756

L<https://modellen.istandaarden.nl/preview/wmo/index.php/WMO756>

=cut

subtype WMO756, as Int,
    where {
        my $int = shift;
        return 1 if $int =~ /^(?:[14]|1[46]|8[23])$/;
        return 0;
    };


=head2 WMO757

L<https://modellen.istandaarden.nl/preview/wmo/index.php/WMO757>

=cut

subtype WMO757, as Int,
    where {
        my $int = shift;
        return 1 if $int =~ /^[1-6]$/;
        return 0;
    };


=head2 WMOaanbieder

=cut

subtype WMOaanbieder, as Int,
    where {
        my $int = shift;
        return 1 if $int =~ /^\d{8}$/;
        return 0;
    };

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
