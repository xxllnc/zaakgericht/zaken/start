package Zaaksysteem::ZTT::Element;
use Moose;

with qw(MooseX::Log::Log4perl);

use BTTW::Tools;

use HTML::TreeBuilder;
use DateTime::Format::DateParse;
use DateTime::Format::Strptime;
use Scalar::Util qw(blessed);
use Zaaksysteem::Geo::BAG::Model;

=head1 NAME

Zaaksysteem::ZTT::Element

=head1 ATTRIBUTES

=head2 attribute

=cut

has attribute => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::ZaaktypeKenmerken',
    predicate => 'has_attribute',
);

=head2 cleaner_skip

A hash reference, used by some L<cleaner> subs to check whether they should run
or not.

If a value is set (and true), the relevant cleaner will be skipped.

This has two handles:

=over

=item * cleaner_skip_set

Used to set a value in the skip list.

=item * cleaner_skip_get

Used to get a value from the skip list.

=back

=cut

has cleaner_skip => (
    is       => 'rw',
    isa      => 'HashRef',
    required => 0,
    default  => sub { {} },
    traits   => ['Hash'],
    handles  => {
        cleaner_skip_set => 'set',
        cleaner_skip_get => 'get',
    }
);

=head2 value

=cut

has value => (
    is => 'rw',
    predicate => 'has_value',
);

=head2 original_value

some renderes are smart enough to handle their own data, rather then a munged
arrayref.

=cut

has original_value => (
    is => 'rw',
    lazy => 1,
    default => sub { shift->value },
    predicate => 'has_original_value',
);

=head2 defined

=cut

has defined => (
    is => 'rw',
    isa => 'Bool',
    lazy => 1,
    default => sub { shift->has_value },
);

=head2 type

=cut

has type => (
    is => 'rw',
    isa => 'Str',
    default => 'plaintext',
);

=head2 title

=cut

has title => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_title',
);

=head2 strip_when_empty

This attribute indicates that the wrapping paragraph in which the magic string
is injected should be removed entirely when no printable characters where
rendered.

=cut

has strip_when_empty => (
    is => 'rw',
    isa => 'Bool',
    default => 0,
    required => 1,
);

=head2 cleaners

=cut

has cleaners => (
    is => 'ro',
    default => sub { return [

        # This sanitization check goes on top, if the attribute
        # is a file, we don't want to do any processing at all
        sub {
            my $self = shift;

            return 1 if shift eq 'file';
        },

        # If the returned value is an array, we flatten it beforehand
        sub {
            my $self = shift;
            my $type = shift;

            return if $self->cleaner_skip_get('array');

            if (ref($self->value) eq 'ARRAY' && !blessed($self->value->[0])) {
                if ($type eq 'richtext') {
                    # Richtext "sanitize" returns an array, so the logic below
                    # doesn't quite work (stringifying the array to ARRAY).
                    #
                    # This lets the richtext sanitizer below do its job:
                    $self->value(join("", @{ $self->value }));
                    return;
                }
                else {
                    my @new_values;

                    for my $val (@{ $self->value }) {
                        my $el = $self->clone();
                        $el->value($val);
                        $el->sanitize();

                        push @new_values, $el->value;
                    }

                    $self->value(join(", \n", @new_values));
                    return 1;
                }
            }

            return;
        },

        # In case of a richtext attribute, we return the HTML::Elements
        # we actually wanna look at, and let the template class figure it out
        sub {
            my $self = shift;
            return unless shift eq 'richtext';

            # preserve it for powerfull renderers
            $self->original_value($self->value);

            # Force string context
            my $html = '' . $self->value;
            $html =~ s[<br \/>][\n]g;

            my $builder = HTML::TreeBuilder->new;
            $builder->implicit_body_p_tag(1);
            $builder->p_strict(1);
            $builder->no_space_compacting(1);

            my $tree = $builder->parse($html);
            $builder->eof;

            $self->value([
                $tree->find(qw[h1 h2 h3 h4 h5 h6 p ul ol])
            ]);

            $self->type('richtext');

            return;
        },

        sub {
            my $self = shift;

            return unless shift =~ m[valuta];
            return if $self->cleaner_skip_get('valuta');

            my $value = $self->value || '';

            return unless length $value;

            $value =~ s[,][.]g;
            $value = sprintf('%01.2f', $value);
            $value =~ s[\.][,]g;

            $self->value($value);

            return;
        },

        sub {
            my $self = shift;

            return unless shift eq 'numeric';

            my $value = $self->value // '';

            return unless length $value;

            if (ref($value) eq 'ARRAY') {
                $self->value([ map { int($_) } @$value ]);
            }
            else {
                $self->value(int($value));
            }

            return;
        },

        sub {
            my $self = shift;

            return unless shift =~ m[^bag];

            my $value = $self->value // '';

            return unless $value =~ m/\w+\-\d+/;

            my $bag_model = Zaaksysteem::Geo::BAG::Model->new(schema => $self->attribute->result_source->schema);

            my @bags = map {
                my ($type, $id) = split /-/, $_;

                my $bag_object = $bag_model->get($type => $id);

                $bag_object
                    ? $bag_object->to_string
                    : "Onbekend BAG-object: '$_'"
            } split m[,\s*], $value;

            $self->value( join('; ', @bags) ) if scalar @bags;

            return;
        },

        sub {
            my $self = shift;

            return unless shift eq 'appointment';
            return unless $self->has_value;

            my $value = $self->value;

            # We need the the object model to process an appointment
            my $object_model = Zaaksysteem::Object::Model->new(
                schema => $self->attribute->result_source->schema
            );

            my $appointment = $object_model->retrieve(uuid => $value);
            if ($appointment) {
                $value = $appointment;
            }
            else {
                $self->log->info("Unable to retrieve appointment '$value'");
                $value = "Onbekende afspraak";
            }
            $self->value($value);
        },

        sub {
            my $self = shift;

            return unless shift eq 'calendar';
            return unless $self->has_value;

            my ($date, $time, $id) = split m[;], $self->value;

            return unless $id;

            my $start_date = DateTime::Format::DateParse->parse_datetime($date, 'UTC');
            $start_date->set_time_zone('Europe/Amsterdam');

            my $start_time = DateTime::Format::DateParse->parse_datetime($time, 'UTC');
            $start_time->set_time_zone('Europe/Amsterdam');

            my $date_fmt = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
            my $time_fmt = DateTime::Format::Strptime->new(pattern => '%R');

            $self->value(sprintf(
                '%s %s',
                $date_fmt->format_datetime($start_date),
                $time_fmt->format_datetime($start_time)
            ));

            return;
        },

        sub {
            my $self = shift;

            return unless shift eq 'calendar_supersaas';
            return unless $self->has_value;

            my ($start, $end, $id) = split m[;], $self->value;

            return unless $id;

            $self->value(sprintf(
                '%s - %s',
                _format_appointment_dates($start),
                _format_appointment_dates($end),
            ));
        },
    ]; }
);

=head1 CONSTRUCTOR

The constructor for this class handles one common use case, striping a
supplied undefined value. This allows instantiating code to simply supply
a value, without having to worry about the L</has_value> state.

=cut

around BUILDARGS => sub {
    my ($orig, $class, %args) = @_;

    delete $args{ value } if exists $args{ value } and not defined $args{ value };

    return $class->$orig(\%args);
};

=head1 METHODS

=head2 sanitize

This method takes the value in L</value> and sanitizes it if on of the defined
L</cleaners> knows how to handle it. The C<value> attribute will be updated,
and C<$self> will be returned.

    my $element = $element->sanitize;

=cut

sub sanitize {
    my $self = shift;

    my $value_type = 'plaintext';

    if ($self->has_attribute) {
        $value_type = $self->attribute->library_attribute->value_type;
    }

    # run all the cleaners. if one of these returns truthy, don't run the rest
    for my $code (@{ $self->cleaners }) {
        last if $code->($self, $value_type);
    }

    return $self;
}

=head2 clone

Returns a deep-copied instance of C<< $self->meta >>.

=cut

sub clone {
    my $self = shift;

    my $new_instance = $self->meta->new_object(
        type => $self->type,
        cleaners => $self->cleaners,
        strip_when_empty => $self->strip_when_empty,
        defined => $self->defined,
        cleaner_skip => $self->cleaner_skip,

        ($self->has_attribute      ? ( attribute      => $self->attribute      ) : () ),
        ($self->has_value          ? ( value          => $self->value          ) : () ),
        ($self->has_original_value ? ( original_value => $self->original_value ) : () ),
        ($self->has_title          ? ( title          => $self->title          ) : () ),
    );
}

=head1 PRIVATE METHODS

=head2 _format_appointment_dates

Formats a given datetime (string of instance-of L<DateTime>) as
C<YYYY-mm-dd HH:MM>.

=cut

sub _format_appointment_dates {
    my ($dt) = @_;

    my $fmt = DateTime::Format::Strptime->new(pattern => '%F %R');
    if (!blessed($dt)) {
        $dt = DateTime::Format::DateParse->parse_datetime($dt);
    }
    $dt->set_time_zone('Europe/Amsterdam');
    return $fmt->format_datetime($dt);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
