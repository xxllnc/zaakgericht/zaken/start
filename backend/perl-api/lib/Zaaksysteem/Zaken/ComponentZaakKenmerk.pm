package Zaaksysteem::Zaken::ComponentZaakKenmerk;
use Zaaksysteem::Moose;

use List::Util qw(any);
use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
/;

extends 'DBIx::Class';

has 'human_value'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;
        my $definition  = $self->_get_veldoptie_definition($self->value_type);

        return $self->value unless $definition->{filter};

        my @values      = $self->value;

        for my $value (@values) {


        }

        if ($self->multiple) {



        }
    },
);

has value_type => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    builder => '_build_value_type',
);

has library_attribute => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::BibliotheekKenmerken',
    lazy => 1,
    builder => '_build_library_attribute',
);

has type_definition => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    builder => '_build_type_definition',
);


sub _build_library_attribute {
    my $self = shift;
    return $self->bibliotheek_kenmerken_id;
}

sub _build_value_type {
    my $self = shift;
    return $self->library_attribute->value_type;
}

sub as_object_attribute {
    my $self = shift;

    my $attribute = Zaaksysteem::Object::Attribute->new(
        name           => sprintf('attribute.%s', $self->magic_string),
        bwcompat_name  => $self->magic_string,
        property_name  => $self->magic_string,
        label          => $self->library_attribute->get_label,
        grouping       => 'case',
        attribute_type => $self->type_definition->{object_search_type} // 'text',
        bwcompat_type  => $self->value_type,
        value          => $self->filtered_value // undef,
        object_row     => $self,
    );
    return $attribute;

}

sub get_value {
    my $self = shift;

    if (   $self->library_attribute->can_have_multiple_values
        || $self->value_type eq 'select')
    {
        # All 'select' attributes have historically been exposed as arrayed
        # values. This behavior is not correct from a design point of view, but
        # the current API was created with that feature.
        return $self->value;
    }
    return $self->value->[0];
}

sub filtered_value {
    my $self = shift;
    return $self->library_attribute->filter($self->get_value);
}

sub _build_type_definition {
    my $self    = shift;

    return $self->library_attribute->get_type_definition;
}

sub _get_multiple {
    my $self    = shift;
    return $self->type_definition->{multiple};
}

sub _kenmerk_find_or_create {
    my $self                = shift;
    my $bibliotheek_kenmerk = shift;

    tombstone('20210604', 'wesleys');

    ### Try to find existing kenmerk
    my $kenmerk;
    unless(ref($bibliotheek_kenmerk)) {
        $bibliotheek_kenmerk   = $self->result_source->schema
            ->resultset('BibliotheekKenmerken')->find(
                $bibliotheek_kenmerk
            );

        die(
            'Zaken::Kenmerken->_find_or_create: '
            . ' cannot find bibliotheek kenmerk by id'
        ) unless $bibliotheek_kenmerk;
    }


    my $kenmerken   = $self->result_source->schema
        ->resultset('ZaakKenmerken')->search(
            {
                bibliotheek_kenmerken_id    => $bibliotheek_kenmerk->id,
                zaak_id                     => $self->get_column('zaak_id'),
            }
        );

    $kenmerk = $kenmerken->first if $kenmerken->count == 1;

    ### Found kenmerk, return it (this object);
    return $kenmerk if $kenmerk;

    ### Did not find existing kenmerk, finish this row
    $self->bibliotheek_kenmerken_id($bibliotheek_kenmerk->id);
    $self->naam($bibliotheek_kenmerk->naam);
    $self->value_type($bibliotheek_kenmerk->value_type);
    $self->multiple(
        (
            $self->_get_multiple ||
            $bibliotheek_kenmerk->type_multiple
        )
    );

    return $self;

}

1; #__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

