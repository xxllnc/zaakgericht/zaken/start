package Zaaksysteem::Zaken::ResultSetBetrokkenen;

use strict;
use warnings;

use Moose;
use Data::Dumper;

extends 'DBIx::Class::ResultSet';


sub search_gerelateerd {
    my $self    = shift;

    return $self->search(
        {
            deleted             => undef,
            magic_string_prefix => { 'is not' => undef },
        },
        {
            order_by    => 'id'
        }
    );
}

sub ontvanger {
    my $self    = shift;

    return $self->search(
        {
            deleted      => undef,
            'lower(rol)' => 'ontvanger',
        },
        {
            order_by => 'id',
            rows     => 1,
        }
    )->first;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ontvanger

TODO: Fix the POD

=cut

=head2 search_gerelateerd

TODO: Fix the POD

=cut

