package Zaaksysteem::Zaken::Roles::BetrokkenenObjecten;

use Moose::Role;
use Data::Dumper;

with 'Zaaksysteem::Zaken::Betrokkenen';

use BTTW::Tools;
use JSON::XS qw(encode_json);
use List::Util qw(uniq);
use Zaaksysteem::Betrokkene;
use Zaaksysteem::Constants;
use Zaaksysteem::Event::RMQ;

with 'MooseX::Log::Log4perl';

has 'aanvrager_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return unless $self->aanvrager;

        return $self->_load_betrokkene_object(
            $self->aanvrager
        );
    }
);

has 'ontvanger_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;
        my $ontvanger   = $self->zaak_betrokkenen->ontvanger;

        return unless $ontvanger;

        return $self->_load_betrokkene_object(
            $ontvanger
        );
    }
);

has 'coordinator_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return unless $self->coordinator;

        return $self->_load_betrokkene_object(
            $self->coordinator
        );
    }
);

has betrokkene_model => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->result_source->schema->resultset('Zaak')
            ->betrokkene_model;
    }
);

=head2 behandelaar_object

Returns the current behandelaar as an object, if a behandelaar is set. Returns
undef if no behandelaar is set.

=cut

sub behandelaar_object {
    my $self = shift;

    return unless $self->behandelaar;

    return $self->_load_betrokkene_object($self->behandelaar);
}

=head2 pip_authorized_betrokkenen

Returns the list of "betrokkene"-objects related to this case that have the
pip_authorized flag set.

=cut

sub pip_authorized_betrokkenen {
    my $self = shift;

    return map {
        $self->_load_betrokkene_object($_)
    } $self->zaak_betrokkenen->search({pip_authorized => 1})->all;
}

has 'ou_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'clearer'   => 'clean_ou_object',
    'predicate' => 'has_ou_object',
    'default'   => sub {
        my $self    = shift;

        return unless $self->route_ou;

        return $self->result_source->schema->resultset('Groups')->search(
            {
                id => $self->route_ou,
            }
        )->first;
    }
);

has 'role_object' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'clearer'   => 'clean_role_object',
    'predicate' => 'has_role_object',
    'default'   => sub {
        my $self = shift;

        if (my $id = $self->route_role) {
            return $self->result_source->schema->resultset('Roles')
                ->search({ id => $id, })->first;
        }
        return;

    }
);


Params::Profile->register_profile(
    'method'    => 'betrokkene_object',
    'profile'   => {
        'optional'      => [qw/
        /],
        'require_some'  => {
            'magic_string_or_rol'   => [
                1,
                'magic_string_prefix',
                'rol',
                'betrokkene_id'
            ],
        },
    }
);

sub betrokkene_object {
    my ($self, $opts)   = @_;
    my $dv              = Params::Profile->check(params => $opts);
    my $search          = {};

    die('Parameters incorrect:' . Dumper($dv)) unless $dv->success;

    $search->{deleted} = undef;

    if ($opts->{magic_string_prefix}) {
        $search->{magic_string_prefix}  = $opts->{magic_string_prefix};
    } elsif ($opts->{rol}) {
        $search->{rol}                  = $opts->{rol};
    } else {
        $search->{id}                   = $opts->{betrokkene_id};
    }

    my $betrokkene = $self->zaak_betrokkenen->search(
        $search
    );

    return unless $betrokkene->count == 1;

    $betrokkene     = $betrokkene->first;

    if (
        $self->{_betrokkene_object_cache} &&
        $self->{_betrokkene_object_cache}->{$betrokkene->id}
    ) {
        return $self->{_betrokkene_object_cache}->{$betrokkene->id};
    }

    $self->{_betrokkene_object_cache} = {} unless
        $self->{_betrokkene_object_cache};


    return $self->{_betrokkene_object_cache}->{ $betrokkene->id }
        = $self->_load_betrokkene_object(
            $betrokkene
        );
}

=head2 get_zaak_betrokkenen

Returns a search of C<ZaakBetrokkenen>, for the supplied C<search> parameters.

=cut

sub get_zaak_betrokkenen {
    my $self = shift;

    return $self->zaak_betrokkenen->search(
        { deleted => undef }
    )->search(@_);
}

=head2 get_betrokkene_objecten

Returns an array of C<Betrokkene::Object> instances, for all C<ZaakBetrokken>
rows matched by the supplied C<search> parameters.

=cut

sub get_betrokkene_objecten {
    my $self = shift;

    my $betrokkenen_rs = $self->get_zaak_betrokkenen(@_);

    my @rv;
    while (my $betrokkene = $betrokkenen_rs->next) {
        if (!$self->{_betrokkene_object_cache}->{ $betrokkene->id }) {
            $self->{_betrokkene_object_cache}->{ $betrokkene->id } =
                $self->_load_betrokkene_object(
                    $betrokkene
                );
        }

        push @rv, $self->{_betrokkene_object_cache}->{ $betrokkene->id };
    }

    return @rv;
}

sig get_betrokkene_objecten_by_role => 'Str';

sub get_betrokkene_objecten_by_role {
    my ($self, @roles) = @_;

    @roles = map { lc($_) } @roles;

    return $self->get_betrokkene_objecten(
        { 'LOWER(rol)' => { -in => \@roles } }
    );
}

sub _set_coordinator_or_behandelaar_assertions {
    my ($self, $identifier) = @_;

    $self->assert_assignee(betrokkene_id => $identifier);

    my $bo = $self->result_source->schema->resultset('Zaak')->betrokkene_model;

    ## Set betrokkene-TYPE-ID
    my $betrokkene_ident  = $bo->set($identifier);

    ### Retrieve betrokkene ID from ident (GMID-ID)
    my ($gm_id, $betrokkene_id) = $betrokkene_ident =~ /(\d+)-(\d+)$/;
    return ($gm_id, $betrokkene_id);
}

sub set_coordinator {
    my $self        = shift;
    my $identifier  = shift;

    my $reliably = sub {
        my ($gm_id, $betrokkene_id)
            = $self->_set_coordinator_or_behandelaar_assertions($identifier);

        my $zaakbetrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')
            ->find($betrokkene_id);

        $zaakbetrokkene->update({
            "rol" => "Coordinator",
            "zaak_id" => $self->id,
        });

        $self->_betrokkene_delete($self->coordinator);
        $self->coordinator($betrokkene_id);
        $self->coordinator_gm_id($gm_id);
        $self->update;
        $self->update_object_acl;
    };

    $self->result_source->schema->storage->execute_reliably($reliably);

    return;
};

sub set_behandelaar {
    my $self        = shift;
    my $identifier  = shift;

    my $reliably = sub {
        my ($gm_id, $betrokkene_id)
            = $self->_set_coordinator_or_behandelaar_assertions($identifier);

        my $zaakbetrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')
            ->find($betrokkene_id);

        $zaakbetrokkene->update({
            "rol" => "Behandelaar",
            "zaak_id" => $self->id,
        });

        $self->_betrokkene_delete($self->behandelaar);
        $self->behandelaar($betrokkene_id);
        $self->behandelaar_gm_id($gm_id);

        $self->update;
        $self->update_object_acl;
    };

    $self->result_source->schema->storage->execute_reliably($reliably);

    # Check messages and assign them to the new behandelaar.
    my $messages = $self->result_source->schema->resultset('Message')->search(
        {
            'logging.zaak_id' => $self->id,
        },
        {
            join => 'logging'
        }
    );
    $messages->update({subject_id => $identifier});

    return;
};

sub set_aanvrager {
    my $self        = shift;
    my $identifier  = shift;

    ### Check if aanvrager is same type as identifier
    my $original_requestor = $self->aanvrager_object;

    if ($original_requestor) {
        my $btype = $original_requestor->btype;
        if ($identifier !~ /$btype/) {
            $self->log->warn("Identifier is not correct");
            return;
        }
    }

    my $bo = $self->betrokkene_model;

    my $betrokkene_ident  = $bo->set($identifier);
    if (!defined $betrokkene_ident) {
        $self->log->warn(
            "Betrokkene ident is empty: $identifier");
        return;
    }

    ### Retrieve betrokkene ID from ident (GMID-ID)
    my ($gm_id, $betrokkene_id) = $betrokkene_ident =~ /(\d+)-(\d+)$/;

    ### Delete current betrokkene
    $self->_betrokkene_delete($self->aanvrager);

    $self->aanvrager($betrokkene_id);
    $self->aanvrager_gm_id($gm_id);

    $self->_set_preset_client($identifier);

    $self->update;
    $self->discard_changes;

    $self->_betrokkene_zaak_id($self->aanvrager);
    $self->update_object_acl;

    $self->trigger_logging(
        'case/update/requestor',
        {
            data => {
                old_value => $original_requestor
                ? $original_requestor->naam
                : undef,
                new_value => $self->aanvrager_object->naam,
            }
        }
    );

    $self->publish_case_event(
        'RequestorChanged',
        undef, # no user overide
        {
            "key" => "requestor",
            "old_value" => $original_requestor->uuid,
            "new_value" => $self->aanvrager_object->uuid,
        }
    );

    return;
};

sub _betrokkene_zaak_id {
    my $self        = shift;
    my $betrokkene  = shift;

    return unless $betrokkene;

    $betrokkene->zaak_id($self->id);
    $betrokkene->update;
}

sub _betrokkene_delete {
    my $self        = shift;
    my $betrokkene  = shift;

    return unless $betrokkene;

    $betrokkene->deleted(DateTime->now());
    $betrokkene->update;
}

sub _load_betrokkene_object {
    my $self    = shift;
    my $object  = shift;

    my $searchid        = $object->id;
    my $searchintern    = 1;

    if (
        $object->betrokkene_type eq 'org_eenheid'
    ) {
        $searchid = $object->gegevens_magazijn_id;
        $searchintern = 0;
    }

    return $self->result_source->schema->resultset('Zaak')->betrokkene_model->get(
        {
            'intern'    => $searchintern,
            'type'      => $object->betrokkene_type,
        }, $searchid
    );
}

after '_handle_logging' => sub {
    my $self            = shift;

    my $changed_data    = $self->_get_latest_changes;

    my @types           = qw/behandelaar coordinator aanvrager/;

    for my $type (@types) {
        if (exists($changed_data->{$type})) {
            if (!$changed_data->{$type} && $changed_data->{_is_insert}) {
                next;
            }

            $self->trigger_logging('case/relation/update', { component => 'betrokkene', data => {
                subject_name => $self->$type ? $self->$type->naam : '&lt;Geen betrokkene&gt;',
                subject_relation => ucfirst(lc($type))
            }});
        }
    }

    return $changed_data;

};


sub is_betrokkene_compleet {
    my $self    = shift;

    return 1 if $self->behandelaar;
    return;
}


around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    if($self->is_betrokkene_compleet) {
        $advance_result->ok('owner_complete');
    } else {
        $advance_result->fail('owner_complete');
    }
    if ($self->related_roles_complete) {
        $advance_result->ok('related_roles_complete');
    }
    else {
        $advance_result->fail('related_roles_complete');
    }

    return $advance_result;
};

=head2 related_roles_complete

If the casetype has related roles for deelzaak creation, you must have them defined in the fase overgang

=cut

sub related_roles_complete {
    my $self = shift;

    my $status = $self->zaaktype_node_id->zaaktype_statussen->search_rs({status => $self->milestone + 1});

    my $case_actions = $self->case_actions->search_rs(
        {
            casetype_status_id =>
                { in => $status->get_column('id')->as_query },
            type      => 'case',
            automatic => 1,
        }
    );

    my @required_roles;
    while (my $action = $case_actions->next) {
        my $data = $action->data;
        if ($data->{eigenaar_type} eq 'betrokkene') {
            push(@required_roles, lc($data->{eigenaar_role}));
        }
    }
    @required_roles = uniq @required_roles;

    my $req_count = @required_roles;
    return 1 unless $req_count;

    my $roles = $self->zaak_betrokkenen->search_rs(
        {
            deleted => undef,
            'lower(rol)' => { in => \@required_roles },
        },
        {
            columns  => [qw(rol)],
            distinct => 1,
        },
    );

    my $role_count = $roles->count;
    $self->log->trace(
        sprintf("Found %d roles, requires %d roles", $role_count, $req_count)
    );
    return $roles->count == $req_count ? 1 : 0;
}

=head2 betrokkene_has_role

Check if the betrokkene has a role in the case already

=cut

sub betrokkene_has_role {
    my ($self, $betrokkene_id, $rol) = @_;
    # Don't add duplicated
    my ($undef, $type, $id) = split(/-/, $betrokkene_id);
    my $exists = $self->zaak_betrokkenen->search(
        {
            deleted              => undef,
            rol                  => $rol,
            betrokkene_type      => $type,
            gegevens_magazijn_id => $id,

        },
        { rows => 1 }
    )->first;
    return $exists;
}

sub add_as_assignee_with_privilege {
    my ($self, $id) = @_;

    $self->betrokkene_relateren({
        betrokkene_identifier  => $id,
        employee_authorisation => 'write',
        rol                    => 'Specifieke behandelaar',
        magic_string_prefix    => 'specifiekebehandelaar',
    });
    return 1;
}

sub add_as_coordinator_with_privilege {
    my ($self, $id) = @_;

    $self->betrokkene_relateren({
        betrokkene_identifier  => $id,
        employee_authorisation => 'write',
        rol                    => 'Specifieke coordinator',
        magic_string_prefix    => 'specifiekecoordinator',
    });
    return 1;
}

define_profile betrokkene_relateren => (%{ BETROKKENE_RELATEREN_PROFILE() });

sub betrokkene_relateren {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $magic_string_prefix =
        $self->betrokkenen_relateren_magic_string_suggestion(
            $opts
        ) or return;

    if ($self->betrokkene_has_role($opts->{betrokkene_identifier}, $opts->{rol})) {
        return;
    }

    my $schema = $self->result_source->schema;

    return try {
        $schema->txn_do(sub {

            my $bo = $schema->resultset('Zaak')->betrokkene_model;
            my $betrokkene_ident = $bo->set($opts->{betrokkene_identifier});
            my ($betrokkene_id)  = $betrokkene_ident =~ /(?:\d+)-(\d+)$/;

            ### Retrieve betrokkene_id from database and manipulate
            my $betrokkene = $schema->resultset('ZaakBetrokkenen')->find($betrokkene_id);
            if (!$betrokkene) {
                throw('betrokkene/find',
                    "No betrokkene found with id $betrokkene_ident");
            }

            $betrokkene->zaak_id($self->id);
            $betrokkene->verificatie('medewerker');
            $betrokkene->rol($opts->{rol});
            $betrokkene->magic_string_prefix($magic_string_prefix);

            $betrokkene->pip_authorized(1)
                if ($opts->{pip_authorized} // '') eq '1';

            $betrokkene->authorisation($opts->{employee_authorisation})
                if $opts->{employee_authorisation};

            $betrokkene->update;
            $self->update;

            return $self->trigger_logging(
                'case/subject/add',
                {
                    component => LOGGING_COMPONENT_ZAAK,
                    data      => {
                        case_id         => $self->id,
                        case_subject_id => $betrokkene->id,
                        subject_id      => $betrokkene_id,
                        subject_name    => $betrokkene->naam,
                        role            => $opts->{rol},
                        params          => $opts
                    }
                }
            );
        });
    }
    catch {
        $self->log->info($_);
        return;
    };
}

Params::Profile->register_profile(
    'method'    => 'betrokkenen_relateren_magic_string_suggestion',
    'profile'   => {
        'optional'      => [qw/
        /],
        'require_some'  => {
            'magic_string_or_rol'   => [
                1,
                'magic_string',
                'rol',
            ],
        },
    }
);

sub betrokkenen_relateren_magic_string_suggestion {
    my $self            = shift;
    my $opts            = shift;

    my $dv              = Params::Profile->check(params => $opts);

    die('Parameters incorrect:' . Dumper($dv)) unless $dv->success;

    ### Collect used columns in Zaaksysteem
    my @used_columns    = ();

    ### Collect used columns in this zaak
    my $betrokkenen = $self->zaak_betrokkenen->search(
        {
            'deleted' => undef,
            'magic_string_prefix'   => { 'is not'   => undef }
        }
    );

    while (my $betrokkene = $betrokkenen->next) {
        push(@used_columns, $betrokkene->magic_string_prefix . '_naam');
    }

    return BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(
        \@used_columns, $dv->valid('magic_string_prefix'), $dv->valid('rol')
    );
}

sub _set_preset_client {
    my ($self, $id) = @_;

    my $preset = $self->zaaktype_node_id->zaaktype_definitie_id->preset_client;

    if (!$preset || $preset ne $id) {
        $self->preset_client(0);
        return;
    }
    $self->preset_client(1);
}

sub load_v2_relationship_role {
    my ($self, $role) = @_;

    if ($role eq 'assignee') {
        my $subject = $self->behandelaar_gm_id;
        return $subject ? $subject->as_v2_json_relationship : '';
    }
    if ($role eq 'coordinator') {
        my $subject = $self->coordinator_gm_id;
        return $subject ? $subject->as_v2_json_relationship : '';
    }
    if ($role eq 'requestor') {
        my $subject = $self->get_requestor_object;
        return $subject->as_v2_json_relationship;
    }
    if ($role eq 'recipient') {
        my $subject = $self->get_recipient_object;
        return $subject ? $subject->as_v2_json_relationship : '';
    }

    throw(
        "case/relationships/v2/role/unsupported",
        "Unsupported role type for $role!"
    );

}

sub get_requestor_object {
    my $self = shift;

    my $table = 'NatuurlijkPersoon';
    if ($self->aanvrager_type eq 'medewerker') {
        $table = 'Subject';
    }
    elsif ($self->aanvrager_type eq 'bedrijf') {
        $table = 'Bedrijf';
    }

    return $self->result_source->schema->resultset($table)
        ->find($self->aanvrager_gm_id);
}

sub get_recipient_object {
    my $self = shift;

    my $subject = $self->zaak_betrokkenen->ontvanger;
    my $table = 'NatuurlijkPersoon';
    if ($subject->betrokkene_type eq 'medewerker') {
        $table = 'Subject';
    }
    elsif ($subject->betrokkene_type eq 'bedrijf') {
        $table = 'Bedrijf';
    }

    return $self->result_source->schema->resultset($table)
        ->find($subject->gegevens_magazijn_id);
}

=head2 get_case_recipients

Get the recipients of a case for e-mail or snail mail. This function excludes
employees.

=cut

sub get_case_recipients {
    my $self = shift;
    my $type = shift;
    my $role = shift;

    my @recipients;
    if ($type eq 'requestor') {
        @recipients = $self->aanvrager_object;
    }
    elsif ($type eq 'authorized') {
        @recipients = $self->get_betrokkene_objecten(
            {
                'pip_authorized'  => 1,
                'betrokkene_type' => { '!=' => 'medewerker' }
            }
        );
    }
    elsif ($type eq 'betrokkene') {
        @recipients = $self->get_betrokkene_objecten(
            {
                'LOWER(rol)'      => lc($role),
                'betrokkene_type' => { '!=' => 'medewerker' }
            }
        );
    }
    else {
        throw(
            "case/recipient_type/invalid",
            "Unable to determine recipient type"
        );
    }

    return @recipients if @recipients;

    throw("case/recipients/missing",
        "Unable to determine recipient(s)");

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION

TODO: Fix the POD

=cut

=head2 BETROKKENE_RELATEREN_PROFILE

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_ZAAK

TODO: Fix the POD

=cut

=head2 betrokkene_object

TODO: Fix the POD

=cut

=head2 betrokkene_relateren

TODO: Fix the POD

=cut

=head2 betrokkenen_relateren_magic_string_suggestion

TODO: Fix the POD

=cut

=head2 is_betrokkene_compleet

TODO: Fix the POD

=cut

=head2 set_aanvrager

TODO: Fix the POD

=cut

=head2 set_behandelaar

TODO: Fix the POD

=cut

=head2 set_coordinator

TODO: Fix the POD

=cut

