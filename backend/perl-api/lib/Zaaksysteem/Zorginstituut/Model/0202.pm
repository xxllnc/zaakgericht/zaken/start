package Zaaksysteem::Zorginstituut::Model::0202;
use Moose;

extends 'Zaaksysteem::Zorginstituut::Model';
with 'Zaaksysteem::Zorginstituut::Roles::StUF';

=head1 NAME

Zaaksysteem::Zorginstituut::Model::0202 - A base class for 0202 messages

=head1 DESCRIPTION

Base class for 0202 Zorginstituut messages

=head1 SYNOPSIS

    package Foo;
    extends 'Zaaksysteem::Zorginstituut::Model::0202';

    # else where
    use Foo;
    my $model = Foo->new();

    $model->log_to_case();
    $model->get_xml_from_soap();
    $model->get_stuf_du01();
    $model->can_process_du01()
    $model->send_301();

=cut

use XML::LibXML::XPathContext;
use XML::LibXML;

use BTTW::Tools;
use Zaaksysteem::Constants qw(LOGGING_COMPONENT_ZAAK RGBZ_GEMEENTECODES);

=head1 METHODS

=head2 log_to_case

Log a message to a case

=cut

#sig log_to_case => 'Zaaksysteem::Schema::Zaak, HashRef';

sub log_to_case {
    my ($self, $case, %data) = @_;

    my $event = $case->trigger_logging('case/update/zorginstituut',
        { component => LOGGING_COMPONENT_ZAAK, data => \%data });

    my $msg = sprintf(
        "%s antwoordbericht is ontvangen voor zaak %d",
        uc($data{type}),
        $case->id
    );

    $case->create_message_for_behandelaar(
        event_type => 'case/update/zorginstituut',
        message    => $msg,
        log        => $event,
    );
    return 1;
}

sub _get_xpath {
    my ($self, $xml)  = @_;

    return XML::LibXML::XPathContext->new(
        XML::LibXML->load_xml(string => $xml)
    );
}

=head2 get_xml_from_soap

Get the message from the SOAP XML.

=cut

sig get_xml_from_soap => 'Str';

sub get_xml_from_soap {
    my ($self, $xml) = @_;

    my $xc = $self->_get_xpath($xml);

    $xc->registerNs('SOAP', 'http://schemas.xmlsoap.org/soap/envelope/');

    my @nodes = $xc->findnodes('//SOAP:Body/*[1]');
    if (@nodes) {
        # We don't care about the SOAP bits, so use the SOAP body's
        return $nodes[0]->toString;
    }
    throw('zorginstituut/soap/body/missing', "SOAP body is missing");
}

=head2 get_stuf_du01

Get the actual message from the StUF Du01 message and return the
L<XML::LibXML::XPathContext> object.

=cut

sig get_stuf_du01 => 'Str';

sub get_stuf_du01 {
    my ($self, $xml) = @_;

    my $xc = $self->_get_xpath($self->get_xml_from_soap($xml));

    $xc->registerNs('s', 'http://www.egem.nl/StUF/StUF0301');
    $xc->registerNs('w',
        'http://www.stufstandaarden.nl/koppelvlak/ggk0210');

    if ($xc->findvalue('//w:envelopRetourbericht-ggk_Du01')) {
        return $xc;
    }
    return undef;
}

=head2 can_process_du01

Check if this module can/should process the Du01 message.

=cut

sig can_process_du01 => 'Str';

sub can_process_du01 {
    my $self = shift;
    my $xml  = shift;

    my $du01 = $self->get_stuf_du01($xml);
    return 0 unless $du01;

    if (my $receiver = $du01->findvalue('//s:ontvanger/s:organisatie')) {
        if (int($receiver) == int($self->municipality_code)) {
            return try {
                $self->du01_to_xml($du01);
                return 1;
            } catch {
                $self->log->debug("Unable to process du01 message, invalid message code: $_");
                return 0;
            };
        }
    }
    return 0;
}

sub _build_stuf_encoder {
    my $self = shift;

    return Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::Generator::StUF0301::GGK0210'
    )->ggk_0210;
}

=head2 send_301

Send 301 messages

=cut

define_profile send_301 => (
    required => {
        case      => 'Zaaksysteem::Schema::Zaak',
        record    => 'Zaaksysteem::Schema::TransactionRecord',
    },
);

sub _xml {
    my $self = shift;
    my $case = shift;
    my $xml  = shift;
    my $now  = shift;

    my $os = $self->create_object_subscription($case);

    my $agbcode = $self->get_agbcode_from_301($xml);

    return $self->stuf_envelope(
        'ggk0210-di01',

        verzend_applicatie       => 'Zaaksysteem',
        gemeentenaam             => $self->municipality_name,
        agb_code                 => $agbcode,
        object_subscription_uuid => $os->external_id,
        berichttype              => $self->type,
        date                     => $now,
        gemeentecode             => $self->municipality_code,
        xml                      => $xml,

        application_major_version       => '0002',
        application_minor_version       => '0020',
    );
}

sub send_301 {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $msg_type = $self->_build_message_type($opts->{case});

    my $start = $msg_type->is_start();
    my $stop  = $msg_type->is_stop();

    if (!$start && !$stop) {
        # This can be either an invalid case or no mapped attributes
        # meaning the messag cannot be send.
        throw('zorginstituut/send_301/invalid_case',
            "No message could be delivered, not a start, or a stop message");
    }

    my $now = DateTime->now();


    my %answer;
    if ($stop) {

        my $xml = $self->_xml($msg_type->case, $msg_type->stop_xml, $now);

        $answer{stop}{xml}    = $xml;
        $answer{stop}{answer} = $self->_send_301($xml);
    }

    if ($start) {

        my $xml = $self->_xml($msg_type->case, $msg_type->start_xml, $now);

        $answer{start}{xml} = $xml;
        $answer{start}{answer} = $self->_send_301($xml);
    }
    return \%answer;
}

sub _send_301 {
    my ($self, $xml) = @_;

    $xml = $self->add_soap_envelope($xml);

    my $request = $self->provider->build_request(
        endpoint => $self->provider->di01_endpoint,
        xml      => $xml,
        'soap-action' => 'http://www.stufstandaarden.nl/koppelvlak/ggk0210/ggk_Di01',
    );

    my $bv03 = $self->provider->send_request($request);
    $self->log->info($bv03);
    return $bv03;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
