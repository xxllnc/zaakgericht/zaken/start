package Zaaksysteem::Zorginstituut::Roles::301;
use Moose::Role;

with 'Zaaksysteem::Zorginstituut::Roles::SendMessage';

=head1 NAME

Zaaksysteem::Zorginstituut::Roles::301 - A role for 301 messages

=cut

use BTTW::Tools;
use DateTime::Format::Strptime;

=head1 ATTRIBUTES

=head2 attribute_data

Attribute data from a case, fetched via an interface mapping

=cut

has attribute_data => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->interface->get_mapped_attributes_from_case($self->case);
    }
);

=head2 is_start

Boolean value which indicates that the message is a C<start> message

=cut

has is_start => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->attribute_data->{aanbieder} ? 1 : 0;
    }
);

=head2 product_enddates

HashRef of which fields are product enddates

=cut

has product_enddates => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        return {
            toegewezen_product_einddatum => 'toegewezen_product_ingangsdatum',
            toegewezen_product_einddatum_1 =>
                'toegewezen_product_ingangsdatum_1',
            toegewezen_product_einddatum_2 =>
                'toegewezen_product_ingangsdatum_2',
            toegewezen_product_einddatum_3 =>
                'toegewezen_product_ingangsdatum_3',
        };
    },
);

=head2 product_endreasons

HashRef of which fields are product end reasons

=cut

has product_endreasons => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        return {
            toegewezen_product_reden_intrekking =>
                'toegewezen_product_ingangsdatum',
            toegewezen_product_reden_intrekking_1 =>
                'toegewezen_product_ingangsdatum_1',
            toegewezen_product_reden_intrekking_2 =>
                'toegewezen_product_ingangsdatum_2',
            toegewezen_product_reden_intrekking_3 =>
                'toegewezen_product_ingangsdatum_3',
        };
    },
);

=head2 dtf

A L<DateTime::Format::Strptime> object. Defaults to one with a set pattern of:
C<%d-%m-%Y>

=cut

has dtf => (
    is      => 'ro',
    isa     => 'DateTime::Format::Strptime',
    default => sub {
        my $self = shift;
        return DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
    },
);

=head2 is_stop

Boolean value which indicates that the message is a C<stop> message

=cut

has is_stop => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return unless exists $self->attribute_data->{beschikkingsnummer};

        my $stop_id = $self->attribute_data->{beschikkingsnummer}->[0];

        return if (!$stop_id);

        if ($stop_id ne $self->beschikkingsnummer) {
            return 1;
        }
        return 0;
    }
);

=head2 beschikkingsnummer

The number of the beschikking, is derived from the case ID

=cut

has beschikkingsnummer => (
    is      => 'ro',
    isa     => 'Int',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->case->id;
    }
);

=head2 get_stop_case

Get the case for the stop message

=cut

sub get_stop_case {
    my $self = shift;

    if ($self->is_stop) {
        return $self->_find_case($self->attribute_data->{beschikkingsnummer});
    }
}

=head2 encode

Encodes the message from a perl data structure to an XML message

=cut

define_profile encode => (
    required => {
        data => 'HashRef',
    },
    optional => {
        case => 'Zaaksysteem::Schema::Zaak',
    },
);

sub encode {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $data = $opts->{data};

    my $case = $opts->{case} // $self->case;

    my $np = $case->aanvrager_object;

    my $toewijzingsnummer = sprintf("%07d00", $case->id);
    my $aanbieder = $data->{aanbieder}->[0];

    my @producten;
    push(@producten,
        {
            aanbieder         => $aanbieder,
            ingangsdatum      => $data->{toegewezen_product_ingangsdatum}->[0],
            einddatum         => $data->{toegewezen_product_einddatum}->[0],
            toewijzingsdatum  => $data->{toegewezen_product_toewijzingsdatum}->[0],
            toewijzingsnummer => $toewijzingsnummer,

            omvang => {
                volume     => int($data->{toegewezen_product_omvang_volume}->[0]),
                eenheid    => $data->{toegewezen_product_omvang_eenheid}->[0],
                frequentie => $data->{toegewezen_product_omvang_frequentie}->[0],
            },

            commentaar       => $data->{toegewezen_product_commentaar},
            categorie        => $data->{toegewezen_product_categorie}->[0],
            code             => $data->{toegewezen_product_code}->[0],
            reden_intrekking => $data->{toegewezen_product_reden_intrekking}->[0],
        }
    );

    foreach (qw(1 2 3)) {
        # Assume that when there is no date set, we don't have a product
        next unless exists $data->{"toegewezen_product_ingangsdatum_$_"};
        next unless $data->{"toegewezen_product_ingangsdatum_$_"}->[0];

        push(
            @producten,
            {
                aanbieder    => $aanbieder,
                ingangsdatum => $data->{"toegewezen_product_ingangsdatum_$_"}->[0],
                einddatum    => $data->{"toegewezen_product_einddatum_$_"}->[0],
                toewijzingsdatum => $data->{toegewezen_product_toewijzingsdatum}->[0],
                toewijzingsnummer => sprintf('%09d', $toewijzingsnummer + $_),
                omvang => {
                    volume => $data->{"toegewezen_product_omvang_volume_$_"}->[0]
                    ? int($data->{"toegewezen_product_omvang_volume_$_"}->[0])
                    : undef,
                    eenheid =>
                        $data->{"toegewezen_product_omvang_eenheid_$_"}->[0],
                    frequentie =>
                        $data->{"toegewezen_product_omvang_frequentie_$_"}->[0],
                },
                commentaar => $data->{"toegewezen_product_commentaar_$_"},
                categorie  => $data->{"toegewezen_product_categorie_$_"}->[0],
                code       => $data->{"toegewezen_product_code_$_"}->[0],
                reden_intrekking => $data->{"toegewezen_product_reden_intrekking_$_"}->[0],
            }
        );
    }

    my $gemeentecode = $self->municipality_code;

    my %data = (
        $self->generate_headers(
            berichtcode   => $opts->{berichtcode},
            ontvanger     => $aanbieder,
            afzender      => $gemeentecode,
        ),
        client      => $np,
        beschikking => {
            nummer        => $case->id,
            afgiftedatum  => $data->{beschikkingsafgiftedatum}->[0],
            ingangsdatum  => $data->{beschikkingsingangsdatum}->[0],
            einddatum     => $data->{beschikkingseinddatum}->[0],
        },
        gemeentecode => $gemeentecode,
        producten    => {
            beschikt => \@producten,
            toegewezen => \@producten,
        },
        commentaar => {
            general              => $data->{algemeen_commentaar},
            toegewezen_producten => $data->{toegewezen_product_commentaar},
            beschikking          => $data->{beschikkingscommentaar},
        },
    );

    return $self->encoder->build_301(writer => \%data);
}

=head2 start_xml

Generate a start message for 301 messages

=cut

sub start_xml {
    my $self = shift;
    if (!$self->is_start) {
        throw('zorginstituut/message/start/invalid',
            "Unable to generate start");
    }

    my $data = $self->attribute_data;

    $self->_set_earliest_product_enddates($data);

    return $self->encode(
        data => $data,
    );
}

=head2 stop_xml

Generate a stop message for 301 messages

=cut

sub stop_xml {
    my $self = shift;
    if (!$self->is_stop) {
        throw('zorginstituut/message/stop/invalid',
            "Unable to generate stop");
    }

    my $data = $self->attribute_data;
    my $case = $self->get_stop_case;
    my $orig = $self->_get_data_from_old_case($case);

    $data->{aanbieder} = $orig->{aanbieder};
    $data->{beschikkingseinddatum} = $data->{stop_beschikkingseinddatum};

    $self->_set_earliest_product_enddates($data, $orig);
    $self->_set_product_end_reasons($data, $orig);

    return $self->encode(
        case => $case,
        data => $data,
    );
}

=head1 PRIVATE METHODS

=head2 _get_data_from_old_case

Get mapped attributes for a case

=cut

sub _get_data_from_old_case {
    my ($self, $case) = @_;
    return $self->interface->get_mapped_attributes_from_case($case);
}

=head2 _find_case

Find a case and die if you cannot find it

=cut

sig _find_case => 'Int';

sub _find_case {
    my ($self, $case_id) = @_;
    my $case = $self->schema->resultset('Zaak')->find($case_id);
    return $case if $case;

    throw("zorginstituut/find_case/id", "Unable to find case with ID $case_id");

}

=head2 _set_product_end_reasons

Set the product end reasons

=cut

sub _set_product_end_reasons {
    my ($self, $data, $orig) = @_;

    return unless $orig;

    my %reasons = %{ $self->product_endreasons };

    foreach (keys %reasons) {
        next if !exists $data->{ $reasons{$_} };

        if ($orig->{stop_reden_intrekking_algemeen}) {
            $data->{$_} = $orig->{stop_reden_intrekking_algemeen};
        }
        elsif ($orig->{"stop_$_"}) {
            $data->{$_} = $orig->{"stop_$_"};
        }
    }
}

=head2 _set_earliest_product_enddates

Set the earliest possible end date
And include the start date for when the enddate
exceeds the original start date

=cut

sub _set_earliest_product_enddates {
    my ($self, $data, $orig) = @_;

    my %dates = %{ $self->product_enddates };
    foreach (keys %dates) {

        if (!exists $data->{ $dates{$_} }) {
            next;
        }

        my $start = $data->{$dates{$_}};

        my $prev_stop = "stop_$_";
        if ($orig && $orig->{$prev_stop}) {
            $self->log->trace("Setting $_ to $prev_stop from original case");
            $data->{$_} = $orig->{$prev_stop};
        }

        $data->{$_} = $self->_get_earliest_date($data->{$_}, $data->{beschikkingseinddatum});

        $data->{$dates{$_}} = $self->_get_earliest_date($start, $data->{beschikkingseinddatum});
    }
}

=head2 _dtf_format

Format a string to a L<DateTime> object. If we find a ref, we assume it
is already a DateTime object as this is used in the testsuite.

=cut

sub _dtf_format {
    my ($self, $val) = @_;
    return ref $val ? $val : $self->dtf->parse_datetime($val);
}

=head2 _get_earliest_date

Get the earliest date possible from a list of dates

=cut

sub _get_earliest_date {
    my ($self, @dates) = @_;

    my @copy;
    foreach (@dates) {
        next unless defined $_;
        if (ref $_ eq 'ARRAY') {
            push(@copy, $_->[0]);
        }
        else {
            push(@copy, $_);
        }
    }
    my ($retval) = sort { $a cmp $b }
    map { $self->_dtf_format($_) }
        grep { defined $_ }
        @copy;

    return [ $retval ];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
