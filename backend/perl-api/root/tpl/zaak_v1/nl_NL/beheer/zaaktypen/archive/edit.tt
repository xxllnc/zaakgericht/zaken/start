[% USE JSON %]
[% BLOCK case_action_row %]
<div class="zaaktypebeheer-archive">
  [% IF action.defined('category') %]
  <h3 class="zaaktypebeheer-actie-header">[% action.label %]</h3>
  [% ELSE %]
  <label for="[% action.field %]">[% action.label %]</label>
  [% END %]

  [% 
    lookupkey = action.name;
    ztb_key   = 'archive_settings.' _ lookupkey;
    value     = params.archive_settings.$lookupkey
  %]

  [% IF action.type == 'header' %]
   [%# nothing %]
  [% ELSIF action.type == 'select' %]
  <select id="[% ztb_key %]"
         name="[% ztb_key %]"
         class="ui-selectmenu-text"
         >
    [% FOREACH option = action.options %]
    [% FOREACH key IN option.keys %]
    <option value="[% key %]" [% IF params.archive_settings.$lookupkey == key
                   %]selected[% END %]>[% option.$key %]</option>
    [% END; END %]
  </select>
  [% ELSIF action.type == 'multiple' %]
    [% IF value %]
      [% value = value.0 ? value : [ value ] %]
    [% END %]
  <!--
  [% value | html_entity %]
  -->
    <div
      class="tekstvelden-wrapper"
      data-ng-controller="nl.mintlab.admin.EditTextFieldController"
      data-ng-init="values = [% JSON.encode(value) | html_entity %] || []"
    >
      <ul class="tekstvelden-table">
          <li
            class="tekstveld-wrapper"
            data-ng-repeat="(index, val) in values track by $index"
          >
            <div class="tekstveld-field">
              <input
                type="text"
                data-ng-model="val"
                ng-change="editField(index, val)"
                name="[% ztb_key %]"
                value="<[val]>"
              />
            </div>
            <div class="tekstveld-remove">
              <button type="button" data-ng-click="removeField(index)">
                  <i class="icon icon-font-awesome icon-remove"></i>
              </button>
            </div>
          </li>
      </ul>
      <div data-ng-form name="add" class="tekstveld-add">
        <button type="button" ng-click="addField()" class="btn btn-secondary">
          Voeg toe
        </button>
      </div>
    </div>
  [% ELSIF action.type == 'textarea' %]
  <textarea class="zaaktypebeheer-archive" id="[% ztb_key %]"
                                           name="[% ztb_key %]" rows=5>
    [% value %]
  </textarea>
  [% ELSE %]
    <input
      type="[% action.type %]"
      id="[% ztb_key %]"
      name="[% ztb_key %]"
      value="[% value %]"
      [% IF action.subtype %] [% action.subtype %][% END %]
      [% IF action.placeholder %] [% action.placeholder %][% END %]
    />

  [% END %]
</div>
[% END %]
[%

archive_type = [{
  name => 'archive_type', 
  type => 'select',
  label => 'Elementlijst',
  options => [ { tmlo => 'TMLO'},
                { mdto => 'MDTO'}
                ],
  category => 1,
}];

archive_tmlo = [
  { name => 'name',
    type => 'multiple',
    label => 'Naam',
    category => 1,
  },
  { name => 'classification',
    type => 'header',
    label => 'Classificatie',
    category => 1,
  },
  { name => 'classification_code',
    type => 'multiple',
    label => 'Code',
  },
  { name => 'classification_description',
    type => 'multiple',
    label => 'Omschrijving',
  },
  { name => 'classification_source_tmlo',
    type => 'multiple',
    label => 'Bron',
  },
  { name => 'classification_date',
    type => 'multiple',
    label => 'Datum',
  },
  { name => 'description_tmlo',
    type => 'multiple',
    label => 'Omschrijving',
    category => 1,
  },
  { name => 'location',
    type => 'multiple',
    label => 'Plaats',
    category => 1,
  },
  { name => 'dekking',
    type => 'header',
    label => 'Dekking',
    category => 1,
  },
  { name => 'coverage_in_time_tmlo',
    type => 'multiple',
    label => 'In tijd',
  },
  { name => 'coverage_in_geo_tmlo',
    type => 'multiple',
    label => 'Geografisch gebied',
  },
  { name => 'language_tmlo',
    type => 'select',
    label => 'Taal',
    category => 1,
    options => [
      # https://iso639-3.sil.org/code_tables/macrolanguage_mappings/data
      { dut => 'dut' },
      { eng => 'eng' },
      { ger => 'ger' },
    ]
  },
  { name => 'user_rights',
    type => 'header',
    label => 'Gebruikersrechten',
    category => 1,
  },
  { name => 'user_rights_description',
    type => 'multiple',
    label => 'Omschrijving voorwaarden',
  },
  { name => 'user_rights_date_period',
    type => 'multiple',
    label => 'Datum/Periode',
  },
  { name => 'confidentiality',
    type => 'header',
    label => 'Openbaarheid',
    category => 1,
  },
  { name => 'confidentiality_description',
    type => 'multiple',
    label => 'Omschrijving beperkingen',
  },
  { name => 'confidentiality_date_period',
    type => 'multiple',
    label => 'Datum/periode',
    preview => 'YYYY-MM-DD of periode',
  },
  { name => 'form',
    type => 'header',
    label => 'Vorm',
    category => 1,
  },
  { name => 'form_genre',
    type => 'multiple',
    label => 'Redactie/genre',
  },
  { name => 'form_publication',
    type => 'multiple',
    label => 'Verschijningsvorm',
  },
  { name => 'structure',
    type => 'multiple',
    label => 'Structuur',
  },
  {
    name => 'generic_metadata_tmlo',
    type => 'multiple'
    label => 'Generieke metadata',
    category => 1
  },
]

archive_mdto = [
  { name => 'Identificatie',
    type => 'header',
    label => 'Identificatie',
    category => 1,
  },
  {
    name => 'classification_source_mdto',
    type => 'multiple',
    label => 'Bron',
  },
  { name => 'description_mdto',
    type => 'multiple',
    label => 'Omschrijving',
    category => 1,
  },
  { name => 'aggregation_level',
    type => 'header',
    label => 'Aggregatieniveau',
    category => 1,
  },
  {
    name => 'aggregation_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'location',
    type => 'header',
    label => 'Raadpleeglocatie',
    category => 1,
  },
  {
    name => 'coverage_in_geo_mdto',
    type => 'multiple',
    label => 'Naam fysieke locatie',
  },
  { name => 'dekking_time',
    type => 'header',
    label => 'DekkingInTijd',
    category => 1,
  },
  { name => 'dekking_time_label',
    type => 'header',
    label => 'Label',
    category => 2,
  },
  {
    name => 'coverage_in_time_mdto',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'coverage_in_time_begin_date',
    type => 'multiple',
    label => 'Begindatum',
    category => 2,
  },
  { name => 'coverage_in_time_end_date',
    type => 'multiple',
    label => 'Einddatum',
    category => 2,
  },
  { name => 'dekking_room',
    type => 'header',
    label => 'DekkingInRuimte',
    category => 1,
  },
  {
    name => 'coverage_in_place',
    type => 'multiple',
    label => 'Naam locatie',
  },
  { name => 'language_mdto',
    type => 'select',
    label => 'Taal',
    category => 1,
    options => [
      # https://iso639-3.sil.org/code_tables/macrolanguage_mappings/data
      { dut => 'nl' },
      { eng => 'en' },
      { ger => 'de' },
    ]
  },
  { name => 'event',
    type => 'header',
    label => 'Event',
    category => 1,
  },
  { name => 'event_label',
    type => 'header',
    label => 'Label',
    category => 2,
  },
  {
    name => 'event_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'event_in_time',
    type => 'multiple',
    label => 'InTijd',
    category => 2,
  },
  { name => 'event_actor',
    type => 'header',
    label => 'VerantwoordelijkeActor',
    category => 2,
  },
  {
    name => 'event_actor_name',
    type => 'multiple',
    label => 'Naam actor',
  },
  { name => 'event_result',
    type => 'multiple',
    label => 'Resultaat',
    category => 2,
  },
  { name => 'valuation',
    type => 'header',
    label => 'Waardering',
    category => 1,
  },
  {
    name => 'valuation_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'retention_period',
    type => 'header',
    label => 'Bewaartermijnen',
    category => 1,
  },
  {
    name => 'retention_period_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'information_category',
    type => 'header',
    label => 'Informatiecategorie',
    category => 1,
  },
  {
    name => 'information_category_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'related_information_object',
    type => 'header',
    label => 'Gerelateerd informatieobject',
    category => 1,
  },
  {
    name => 'related_information_object_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'reference_actor',
    type => 'header',
    label => 'Verwijzing',
    category => 2,
  },
  { name => 'reference_actor_name',
    type => 'header',
    label => 'Naam (informatieobject)',
    category => 3,
  },
  { name => 'archive_maker_info',
    type => 'header',
    label => 'Archiefvormer',
    category => 1,
  },
  { name => 'reference_archive_maker',
    type => 'header',
    label => 'Verwijzing',
    category => 2,
  },
  { name => 'archive_maker',
    type => 'multiple',
    label => 'Naam (informatieobject)',
  },
  { name => 'subject',
    type => 'header',
    label => 'Betrokkene',
    category => 1,
  },
  { name => 'subject_type',
    type => 'header',
    label => 'Type',
    category => 2,
  },
  {
    name => 'subject_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'restriction',
    type => 'header',
    label => 'Beperking Gebruik',
    category => 1,
  },
  { name => 'restriction_type',
    type => 'header',
    label => 'Type',
    category => 2,
  },
  {
    name => 'restriction_use_label',
    type => 'multiple',
    label => 'Label',
  },
    {
    name => 'restriction_use_code',
    type => 'multiple',
    label => 'Code',
  },
  {
    name => 'restriction_use_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'restriction_use_description',
    type => 'multiple',
    label => 'NadereBeschrijving',
    category => 2,
  },
  { name => 'restriction_documents',
    type => 'header',
    label => 'Documentatie',
    category => 2,
  },
  {
    name => 'restriction_use_documents_name',
    type => 'multiple',
    label => 'Naam (informatieobject)',
  },
  { name => 'restriction_term',
    type => 'header',
    label => 'Termijn',
    category => 2,
  },
  {
    name => 'restriction_use_term_label',
    type => 'multiple',
    label => 'Label',
  },
  {
    name => 'restriction_use_term_code',
    type => 'multiple',
    label => 'Code',
  },
  {
    name => 'restriction_use_term_glossary',
    type => 'multiple',
    label => 'Naam begrippenlijst',
  },
  { name => 'additional_metadata',
    type => 'multiple',
    label => 'Aanvullende metagegevens',
    category => 1,
  },
]

%]

  <div class="block block-header ztb-header clearfix">
  [% PROCESS beheer/zaaktypen/ztb_action_header.tt action_header = "Archief" %]
  <div class="ztb">
    <form method="POST" action="[% formaction %]" class="zvalidate use_submit ezra_zaaktypebeheer" enctype="multipart/form-data">
      <input type="hidden" name="zaaktype_update" value="1" />
        <div id="content" class="block-content">

          [% FOREACH action = archive_type ; PROCESS case_action_row ; END %]

          <div id="archive_tmlo">
            [% FOREACH action = archive_tmlo ; PROCESS case_action_row ; END %]
          </div>
          
          <div id="archive_mdto">
            [% FOREACH action = archive_mdto ; PROCESS case_action_row ; END %]
          </div>

          <div class="form-actions form-actions-sticky clearfix">
            <input type="button" name="goback" value="Vorige" class="go_back
                      button-secondary button left" />
              [% PROCESS beheer/zaaktypen/voortgang.tt %]
            <input type="submit" value="Volgende" class="button-primary button right"/> 
            <input name="destination" type="hidden" value="" />
          </div>

        </div>

      </form>
    </div>
  </div>
