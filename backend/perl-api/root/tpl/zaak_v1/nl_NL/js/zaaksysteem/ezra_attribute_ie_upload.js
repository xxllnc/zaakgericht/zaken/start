/*global document,mintloader_enabled,$,console*/

(function () {
    "use strict";

    // blatant code duplication, still better than the alternative:
    // adjusting the legacy code to fit this scenario. this piece
    // needs to be rewritten in Angular
    function upload_attribute_file_ie($element) {

        // TODO non-pip scenario
        var caseId = $('#zaak_id').attr('class'),
            action = '/pip/zaak/' + caseId + '/upload',
            $form = $element.closest('form'),
            savedAction = $form.attr('action'),
            fieldname = $element.attr('name'),
            iframeName = fieldname + '_file_upload_iframe',
            $spinner = $form.find('.spinner-groot');

        $form.attr('action', action);
        $form.attr('target', iframeName);
        $spinner.css('visibility', 'visible');

        $form.submit();
        $form.removeAttr('target');
        $.ztWaitStop();
        $spinner.css('visibility', 'hidden');
        $element.replaceWith($element.clone(true));
    }

    $(document).ready(function () {

        if (!mintloader_enabled()) {
            $('.attribute-change .mintloader').each(function () {
                $(this).on('change', 'input[type="file"]', function () {
                    upload_attribute_file_ie($(this));
                });
            });
        }
    });

}());
