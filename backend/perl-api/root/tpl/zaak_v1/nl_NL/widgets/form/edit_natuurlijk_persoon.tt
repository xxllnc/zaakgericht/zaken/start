[%IF c.config.gemeente_id == "vechtstromen" OR c.config.gemeente_id == "hoogheemraadschap-van-rijnland" OR c.config.gemeente_id == "hoogheemraadschap-van-schieland" OR  c.config.gemeente_id == "hdsr" %]
  [% customer_type_text = 'uw waterschap' %]
[% ELSE %]
  [% customer_type_text = 'uw gemeente' %]
[% END %]

<div class="form zsaction-container">

  [% country_code = c.model('DB::Config').get_customer_config().country_code() %]

    <div class="stap-tekst">

        [% IF aanvrager_bsn %]
        <div class="system-message">
            Helaas hebben wij uw burgerservicenummer met de bijhorende persoonsgegevens niet kunnen vinden.
            [% IF !betrokkene.geslachtsnaam %]
                Wij verzoeken u vriendelijk uw gegevens in te vullen.
            [% ELSE %]
                Wij hebben uw gegevens opgehaald via een zogenaamde GBA-V
                bevraging, gelieve uw gegevens hieronder te controleren op
                correctheid.
            [% END %]
            [% IF betrokkene.messages.briefadres %]
                LET OP: U maakt gebruik van uw briefadres. Mochten deze gegevens niet correct zijn, gelieve contact op te nemen met [% customer_type_text%].
            [% END %]
        </div>
        [% END %]

    <div class="form-required-fields-explanation"> * = Verplicht veld </div>

    </div>
    <div class="row">
        <input type="hidden" name="betrokkene_type" value="natuurlijk_persoon">
        <input type="hidden" name="create" value="1">
    </div>
    [% IF country_code == '5071' %]
    <div class="row">
        <div class="column large-3">
            <label for="np-persoonsnummer" class="titel">Persoonsnummer</label>
        </div>

        <div class="column large-6">
            [% IF persoonsnummer; %]
                <input
                    type="hidden"
                    name="np-persoonsnummer"
                    id="np-persoonsnummer"
                    value="[% persoonsnummer | html_entity %]"
                />
                [% persoonsnummer | html_entity %]
            [% ELSE %]
            <input
                id="np-persoonsnummer"
                type="text"
                name="np-persoonsnummer"
                class="input_large"
            />
            [% END %]
        </div>
    </div>
    [% END %]

    <div class="row">
        <div class="column large-3">
            [%
               require_bsn = " *";
               IF country_code == '6030';
               THEN;
                 IF c.session.verification_method == 'twofactor' || eidas;
                 THEN;
                      require_bsn = "";
                 END;
               ELSE;
                    require_bsn = "";
               END;
            %]
            <label for="np-burgerservicenummer" class="titel">Burgerservicenummer [% require_bsn %]</label>
        </div>

        <div class="column large-6">
            [% IF aanvrager_bsn;
                aanvrager_bsn = aanvrager_bsn FILTER format('%09d'); -%]
                <input
                    type="hidden"
                    name="np-burgerservicenummer"
                    id="np-burgerservicenummer"
                    value="[% aanvrager_bsn | html_entity %]"
                />
                [% aanvrager_bsn | html_entity %]
            [% ELSE %]
            <input
                id="np-burgerservicenummer"
                type="text"
                name="np-burgerservicenummer"
                class="input_large"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw burgerservicenummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    
    
    
    <div class="row">
        <div class="column large-3"><label>Voorletters</label></div>
        <div class="column large-6">
            [% IF betrokkene.geslachtsnaam %]
                [% betrokkene.voorletters | html_entity %]
                <input
                    type="hidden"
                    name="np-voorletters"
                    class="input_mini"
                    value="[% betrokkene.voorletters | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-voorletters"
                class="input_mini"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw voorletters in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    
    <div class="row">
        <div class="column large-3"><label>Voornamen *</label></div>
        <div class="column large-6">
            [% IF betrokkene.geslachtsnaam || prefill.surname %]
                [% (betrokkene.voornamen || prefill.first_name) | html_entity  %]
                <input
                    type="hidden"
                    name="np-voornamen"
                    class="input_mini"
                    value="[% (betrokkene.voornamen || prefill.first_name) | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-voornamen"
                autocomplete="given-name"
                class="input_large"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw voornamen in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>Tussenvoegsel</label></div>
        <div class="column large-6">
            [% IF betrokkene.geslachtsnaam %]
                [% betrokkene.voorvoegsel | html_entity %]
                <input
                    type="hidden"
                    name="np-voorvoegsel"
                    class="input_mini"
                    value="[% betrokkene.voorvoegsel | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-voorvoegsel"
                class="input_medium"
                />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw tussenvoegsel in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>Achternaam *</label></div>
        <div class="column large-6">
            [% IF betrokkene.geslachtsnaam || prefill.surname %]
                [% (betrokkene.geslachtsnaam || prefill.surname) | html_entity %]
                <input
                    type="hidden"
                    name="np-geslachtsnaam"
                    class="input_mini"
                    value="[% (betrokkene.geslachtsnaam || prefill.surname) | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-geslachtsnaam"
                class="input_large"
                autocomplete="family-name"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw achternaam in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column large-3"><label>Adellijke titel</label></div>
        <div class="column large-6">
            [% IF betrokkene.adellijke_titel %]
                [% betrokkene.adellijke_titel | html_entity %]
                <input
                    type="hidden"
                    name="np-adellijke_titel"
                    class="input_mini"
                    value="[% betrokkene.adellijke_titel | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-adellijke_titel"
                class="input_large"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw adellijke titel in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>Geslacht</label></div>
        <div class="column large-6">
            [% IF betrokkene.geslachtsnaam %]
                [% betrokkene.geslachtsaanduiding | html_entity %]
                <input
                    type="hidden"
                    name="np-geslachtsaanduiding"
                    class="input_mini"
                    value="[% betrokkene.geslachtsaanduiding | html_entity %]"
                />
            [% ELSE %]
            <div class="clearfix">
            <label><input type="radio" name="np-geslachtsaanduiding" value="M"> Man</label>
            <label><input type="radio" name="np-geslachtsaanduiding" value="V"> Vrouw</label>
            <label><input type="radio" name="np-geslachtsaanduiding" value="X"> Anders</label>
            </div>
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Kies een geslacht.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="column large-3"><label>Land *</label></div>
        <div class="column large-6">
            <select
            name="np-landcode"
            class="
            zsaction
            zsaction-when-6030-show-binnenland
            zsaction-when-6030-hide-buitenland
            zsaction-whennot-6030-show-buitenland
            zsaction-whennot-6030-hide-binnenland
            replace-select-start-zaak
            ">
            [% FOREACH betrokkene_option IN landcodes %]
                <option
                value="[% betrokkene_option.value %]"
                [% (betrokkene.landcode == betrokkene_option.value || !betrokkene.landcode && betrokkene_option.value == 6030) ? 'selected="selected"' : '' %]
                >
                    [% betrokkene_option.label %]
                </option>
            [% END %]
            </select>
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw land in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column large-3"><label><h3>Verblijfsadres</h3></label></div>
        <div class="column large-6"></div>
    </div>
    
    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Postcode *</label></div>
        <div class="column large-6">
            [% IF betrokkene.straatnaam %]
                [% betrokkene.verblijf_postcode | html_entity %]
                <input
                    type="hidden"
                    name="np-postcode"
                    class="input_mini"
                    value="[% betrokkene.verblijf_postcode | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-postcode"
                class="input_large zs-address-autocomplete zs-address-autocomplete-input"
                autocomplete="postal-code"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw postcode in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Huisnummer *</label></div>
        <div class="column large-6">
            [% IF betrokkene.verblijf_straatnaam %]
                [% betrokkene.verblijf_huisnummer | html_entity %]
                <input
                    type="hidden"
                    name="np-huisnummer"
                    class="input_mini"
                    value="[% betrokkene.verblijf_huisnummer | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-huisnummer"
                class="input_mini zs-address-autocomplete zs-address-autocomplete-input"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw huisnummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Huisnummer toevoeging</label></div>
        <div class="column large-6">
            [% IF betrokkene.verblijf_straatnaam %]
                [% betrokkene.verblijf_huisnummertoevoeging | html_entity %]
                <input
                    type="hidden"
                    name="np-huisnummertoevoeging"
                    class="input_mini"
                    value="[% betrokkene.verblijf_huisnummertoevoeging | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-huisnummertoevoeging"
                class="input_mini zs-address-autocomplete"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw huisnummertoevoeging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Straatnaam *</label></div>
        <div class="column large-6">
            [% IF betrokkene.verblijf_straatnaam %]
                [% betrokkene.verblijf_straatnaam | html_entity %]
                <input
                    type="hidden"
                    name="np-straatnaam"
                    class="input_mini"
                    value="[% betrokkene.verblijf_straatnaam | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-straatnaam"
                class="input_large zs-address-autocomplete zs-address-autocomplete-output"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul een straatnaam in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Woonplaats *</label></div>
        <div class="column large-6">
            [% IF betrokkene.verblijf_straatnaam && betrokkene.verblijf_woonplaats %]
                [% betrokkene.verblijf_woonplaats | html_entity %]
                <input
                    type="hidden"
                    name="np-woonplaats"
                    class="input_mini"
                    value="[% betrokkene.verblijf_woonplaats | html_entity %]"
                />
            [% ELSE %]
            <input
                type="text"
                name="np-woonplaats"
                class="input_large zs-address-autocomplete zs-address-autocomplete-output"
                autocomplete="address-level1"
            />
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw woonplaats in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-buitenland">
        <div class="column large-3"><label>Adresregel 1 *</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="np-adres_buitenland1"
                class="input_medium"
                value="[% betrokkene.adres_buitenland1 | html_entity %]"
                autocomplete="address-line1"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw buitenlandse adres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-buitenland">
        <div class="column large-3"><label>Adresregel 2</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="np-adres_buitenland2"
                class="input_medium"
                value="[% betrokkene.adres_buitenland2 | html_entity %]"
                autocomplete="address-line2"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw buitenlandse adres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-buitenland">
        <div class="column large-3"><label>Adresregel 3</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="np-adres_buitenland3"
                class="input_medium"
                value="[% betrokkene.adres_buitenland3 | html_entity %]"
                autocomplete="address-line3"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw buitenlandse adres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="column large-3"><label><h3>Contactgegevens</h3></label></div>
        <div class="column large-6"></div>
    </div>
    
    <div class="row">
        <div class="column large-3"><label>Telefoonnummer
            [%- IF zaaktype_node.contact_info_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-6">
            <input
                type="text"
                name="npc-telefoonnummer"
                value="[% aanvrager.telefoonnummer | html_entity %]"
                class="input_large"
                autocomplete="tel"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw telefoonnummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>Telefoonnummer (mobiel)
            [%- IF zaaktype_node.contact_info_mobile_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-6">
            <input
                type="text"
                name="npc-mobiel"
                value="[% aanvrager.mobiel | html_entity %]"
                class="input_large"
                autocomplete="tel"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw mobiele nummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>E-mailadres
            [%- IF zaaktype_node.contact_info_email_required %]*[% END -%]
            [%-
                IF c.session.verification_method == 'twofactor';
                   THEN;
                        email = two_fa_email;
                   ELSE;
                        email = aanvrager.email;
               END -%]
        </label></div>
        <div class="column large-6">
            <input
                type="text"
                name="npc-email"
                value="[% email | html_entity %]"
                class="input_large"
                autocomplete="email"
                [%- IF c.session.verification_method == 'twofactor'
                %]readonly[% END %]
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw e-mailadres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    [% PROCESS betrokkene/preferred_contact_channel.tt
        no_brp = 1
    %]

    <div class="form-required-fields-explanation"> * = Verplicht veld </div>

</div>
