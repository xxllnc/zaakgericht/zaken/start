<div class="stap-tekst">

    <p class="subnote subnote-stap-beschrijving">Controleer uw contactgegevens. Uw telefoonnummer, mobiele nummer en e-mailadres kunt u desgewenst aanvullen of aanpassen.</p>

    <div class="form-required-fields-explanation"> * = Verplicht veld </div>

</div>

[% MACRO rechtsvorm_display(rechtsvorm_code) BLOCK %]
    [% SET rechtsvormmap = ZCONSTANTS.kvk_rechtsvormen %]
    [% rechtsvormmap.${rechtsvorm_code} %]
[% END %]

[% MACRO land_display(code) BLOCK %]
    [% SET land_display = '' %]
    [% FOREACH landcode IN landcodes %]
        [% IF code == landcode.value %]
            [% land_display = landcode.label %]
        [% END %]
    [% END %]
    [% land_display %]
[% END %]

[%- IF openkvk_objects.size == 1;
    subject = openkvk_objects.0;
    bedrijf = subject.subject;
END %]

[% BLOCK class_row %]
    <div class="row">
        <div class="column large-4"><label class="titel">[% label %]</label></div>
        <div class="column large-8">[% value || '-' %]</div>
    </div>
[% END %]

[% BLOCK class_row_label %]
    <div class="row">
        <div class="column large-4"><label class="titel"><h3>[% label %]</h3></label></div>
        <div class="column large-8"></div>
    </div>
[% END %]

<div class="form aanvrager webformcontent">

    [% PROCESS class_row label = "KVK-nummer"
        value = bedrijf.coc_number || aanvrager.dossiernummer %]
    [% PROCESS class_row label = "Vestigingsnummer"
        value = bedrijf.coc_location_number || aanvrager.vestigingsnummer %]
    [% PROCESS class_row label = "Handelsnaam"
        value = bedrijf.company || aanvrager.handelsnaam %]
    [% PROCESS class_row label = "Rechtsvorm"
        value = rechtsvorm_display( aanvrager.rechtsvorm ) %]
    [% PROCESS class_row label = "Briefadres"
        value = bedrijf.is_briefadres || aanvrager.is_briefadres ? 'Ja' : 'Nee' %]

    [% PROCESS class_row_label label = "Vestigingsadres"  %]

    [% country_code = bedrijf.address_residence.country.dutch_code || aanvrager.vestiging_landcode %]
    [% PROCESS class_row label = "Vestiging land"
        value = land_display( country_code ) %]
    [% IF country_code == 6030 %]
        [% PROCESS class_row label = "Vestiging straatnaam"
            value = bedrijf.address_residence.street || aanvrager.vestiging_straatnaam %]
        [% PROCESS class_row label = "Vestiging huisnummer"
            value = bedrijf.address_residence.street_number || aanvrager.vestiging_huisnummer %]
        [% PROCESS class_row label = "Vestiging huisnummerletter"
            value = bedrijf.address_residence.street_number_letter || aanvrager.vestiging_huisletter %]
        [% PROCESS class_row label = "Vestiging huisnummertoevoeging"
            value = bedrijf.address_residence.street_number_suffix || aanvrager.vestiging_huisnummertoevoeging %]
        [% PROCESS class_row label = "Vestiging postcode"
            value = bedrijf.address_residence.zipcode || aanvrager.vestiging_postcode %]
        [% PROCESS class_row label = "Vestiging woonplaats"
            value = bedrijf.address_residence.city || aanvrager.vestiging_woonplaats %]
    [% ELSE %]
        [% PROCESS class_row label = "Vestiging adresregel 1"
            value = bedrijf.address_residence.foreign_address_line1 || aanvrager.vestiging_adres_buitenland1 %]
        [% PROCESS class_row label = "Vestiging adresregel 2"
            value = bedrijf.address_residence.foreign_address_line2 || aanvrager.vestiging_adres_buitenland2 %]
        [% PROCESS class_row label = "Vestiging adresregel 3"
            value = bedrijf.address_residence.foreign_address_line3 || aanvrager.vestiging_adres_buitenland3 %]
    [% END %]
    
    [% IF bedrijf.is_briefadres || aanvrager.is_briefadres %]

    [% PROCESS class_row_label label = "Correspondentieadres" %]

    [% country_code = bedrijf.address_correspondence.country.dutch_code || aanvrager.correspondentie_landcode %]
    [% PROCESS class_row label = "Correspondentie land"
        value = land_display( country_code ) %]
    [% IF country_code == 6030 %]
        [% PROCESS class_row label = "Correspondentie straatnaam"
            value = bedrijf.address_correspondence.street || aanvrager.correspondentie_straatnaam %]
        [% PROCESS class_row label = "Correspondentie huisnummer"
            value = bedrijf.address_correspondence.street_number || aanvrager.correspondentie_huisnummer %]
        [% PROCESS class_row label = "Correspondentie huisnummerletter"
            value = bedrijf.address_correspondence.street_number_letter || aanvrager.correspondentie_huisletter %]
        [% PROCESS class_row label = "Correspondentie huisnummertoevoeging"
            value = bedrijf.address_correspondence.street_number_suffix || aanvrager.correspondentie_huisnummertoevoeging %]
        [% PROCESS class_row label = "Correspondentie postcode"
            value = bedrijf.address_correspondence.zipcode || aanvrager.correspondentie_postcode %]
        [% PROCESS class_row label = "Correspondentie woonplaats"
            value = bedrijf.address_correspondence.city || aanvrager.correspondentie_woonplaats %]
    [% ELSE %]
        [% PROCESS class_row label = "Correspondentie adresregel 1"
            value = bedrijf.address_correspondence.foreign_address_line1 || aanvrager.correspondentie_adres_buitenland1 %]
        [% PROCESS class_row label = "Correspondentie adresregel 2"
            value = bedrijf.address_correspondence.foreign_address_line2 || aanvrager.correspondentie_adres_buitenland2 %]
        [% PROCESS class_row label = "Correspondentie adresregel 3"
            value = bedrijf.address_correspondence.foreign_address_line3 || aanvrager.correspondentie_adres_buitenland3 %]
    [% END %]

    [% END %]

    [% PROCESS class_row_label label = "Contactgegevens" %]

    <div class="row">
        <div class="column large-4"><label class="titel" for="telefoon">Telefoonnummer
            [%- IF zaaktype_node.contact_info_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-8">
            <input
                type="text"
                name="npc-telefoonnummer"
                value="[% aanvrager.telefoonnummer %]"
                class="input_large"
                id="telefoon"
                autocomplete="tel"
            />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>

    <div class="row">
        <div class="column large-4"><label class="titel" for="mobiel">Telefoonnummer (mobiel)
            [%- IF zaaktype_node.contact_info_mobile_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-8">
            <input
                type="text"
                name="npc-mobiel"
                value="[% aanvrager.mobiel %]"
                class="input_large"
                id="mobiel"
                autocomplete="tel"
            />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>

    <div class="row">
        <div class="column large-4"><label class="titel" for="email">E-mailadres
            [%- IF zaaktype_node.contact_info_email_required %]*[% END -%]
        </label></div>
        <div class="column large-8">
            <input
                type="text"
                name="npc-email"
                value="[% aanvrager.email %]"
                class="input_large"
                id="email"
                autocomplete="email"
            />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>

    [% PROCESS betrokkene/preferred_contact_channel.tt
      betrokkene = aanvrager
    %]

</div>

<div class="form-required-fields-explanation"> * = Verplicht veld </div>

