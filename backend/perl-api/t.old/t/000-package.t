#! perl
use Test::Package;

all_package_files_ok(
    skip => {
        pl => 1,
        manifest => 1,
    },
);
