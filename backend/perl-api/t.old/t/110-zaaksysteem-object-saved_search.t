#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use Zaaksysteem::Search::ZQL;

SKIP: {
    skip 'AUTUMN2015BREAK: db: Can\'t call method "vectorize_value" on an undefined value',3;

$zs->zs_transaction_ok(sub {
    # Do what the controller does
    my $obj = $schema->resultset('ObjectData')->create(
        { object_class => 'saved_search' }
    );
    isa_ok($obj, 'Zaaksysteem::Model::DB::ObjectData');
    $obj->discard_changes();

    $obj->save_search_data(
        owner => 'medewerker-666',
        query => { 'query' => { 'nested' => 1 } },
        title => 'Query title',
    );

    {
        my @attrs = @{ $obj->object_attributes };
        ok(my ($mw_attr) = grep({ $_->name eq 'owner_subject_id' } @attrs), '"owner_subject_id" attribute found');
        is($mw_attr->value,          'medewerker-666', '  Value of "owner_subject_id" is correct');
        is($mw_attr->human_value,    'medewerker-666', '  Human value of "owner_subject_id" is correct');
        is($mw_attr->attribute_type, 'text',           '  Type of "owner_subject_id" is correct');

        ok(my ($title_attr) = grep({ $_->name eq 'title' } @attrs), '"title" attribute found');
        is($title_attr->value,          'Query title', '  Value of "query" is correct');
        is($title_attr->human_value,    'Query title', '  Human value of "query" is correct');
        is($title_attr->attribute_type, 'text',        '  Type of "query" is correct');

        ok(my ($query_attr) = grep({ $_->name eq 'query' } @attrs), '"query" attribute found');
        is_deeply(
            $query_attr->value,
            { 'query' => {'nested' => 1 } },
            '  Value of "query" is correct',
        );
        is_deeply(
            $query_attr->human_value,
            { 'query' => {'nested' => 1 } },
            '  Human value of "query" is correct',
        );
        is(
            $query_attr->attribute_type,
            'object',
            '  Type of "query" is correct',
        );
    }

    # Save again, no title change
    $obj->save_search_data(
        owner => 'medewerker-665',
        query => {'another' => ['query']},
    );

    {
        my @attrs = @{ $obj->object_attributes };
        ok(my ($mw_attr) = grep({ $_->name eq 'owner_subject_id' } @attrs), '"owner_subject_id" attribute found');
        is($mw_attr->value,          'medewerker-665', '  Value of "owner_subject_id" is correct');
        is($mw_attr->human_value,    'medewerker-665', '  Human value of "owner_subject_id" is correct');
        is($mw_attr->attribute_type, 'text',           '  Type of "owner_subject_id" is correct');

        ok(my ($title_attr) = grep({ $_->name eq 'title' } @attrs), '"title" attribute found');
        is($title_attr->value,          'Query title', '  Value of "query" is correct');
        is($title_attr->human_value,    'Query title', '  Human value of "query" is correct');
        is($title_attr->attribute_type, 'text',        '  Type of "query" is correct');

        ok(my ($query_attr) = grep({ $_->name eq 'query' } @attrs), '"query" attribute found');
        is_deeply(
            $query_attr->value,
            { 'another' => ['query'] },
            '  Value of "query" is correct',
        );
        is_deeply(
            $query_attr->human_value,
            { 'another' => ['query'] },
            '  Human value of "query" is correct',
        );
        is(
            $query_attr->attribute_type,
            'object',
            '  Type of "query" is correct',
        );
    }

    # Save again, now with title change
    $obj->save_search_data(
        owner => 'medewerker-665',
        query => {'another' => ['query', '3']},
        title => 'Another title',
    );

    {
        my @attrs = @{ $obj->object_attributes };
        ok(my ($mw_attr) = grep({ $_->name eq 'owner_subject_id' } @attrs), '"owner_subject_id" attribute found');
        is($mw_attr->value,          'medewerker-665', '  Value of "owner_subject_id" is correct');
        is($mw_attr->human_value,    'medewerker-665', '  Human value of "owner_subject_id" is correct');
        is($mw_attr->attribute_type, 'text',           '  Type of "owner_subject_id" is correct');

        ok(my ($title_attr) = grep({ $_->name eq 'title' } @attrs), '"title" attribute found');
        is($title_attr->value,          'Another title', '  Value of "query" is correct');
        is($title_attr->human_value,    'Another title', '  Human value of "query" is correct');
        is($title_attr->attribute_type, 'text',        '  Type of "query" is correct');

        ok(my ($query_attr) = grep({ $_->name eq 'query' } @attrs), '"query" attribute found');
        is_deeply(
            $query_attr->value,
            { 'another' => ['query', '3'] },
            '  Value of "query" is correct',
        );
        is_deeply(
            $query_attr->human_value,
            { 'another' => ['query', '3'] },
            '  Human value of "query" is correct',
        );
        is(
            $query_attr->attribute_type,
            'object',
            '  Type of "query" is correct',
        );
    }

    # ZQL
    my $query = sprintf(
        'SELECT {} FROM saved_search WHERE object.uuid = "%s"',
        $obj->uuid,
    );
    my $zql = Zaaksysteem::Search::ZQL->new($query);
    my $rs = $zql->apply_to_resultset($schema->resultset('ObjectData'));

    is($rs->count, 1, 'One object returned');
    my $search = $rs->single;

    is($search->id, $obj->id, 'Correct object id returned using ZQL search');

}, 'Basic saved searches');

}; # END SKIP

zs_done_testing();
