#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
initialize_test_globals_ok;
use Test::Deep;

use File::Spec::Functions qw(catfile);


BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };
BEGIN { use_ok('Zaaksysteem::StUF::Body::Field') };

###
### Version 0204
###
{
    my $object      = Zaaksysteem::StUF->new(
        entiteittype    => 'PRS',
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            tijdstipBericht     => '2009040717084815',
            sectormodel         => 'BG',
            versieStUF          => '0204',
            versieSectormodel   => '0204',
            entiteittype        => 'PRS',
            berichtsoort        => 'Lk01',
            referentienummer    => '99999',
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => 'CML',
            },
            kennisgeving        => {
                mutatiesoort    => 'T'
            }
        ),
        #version         => '0310',
    );

    ok($object, 'Created StUF object, version 0204');
    #note(explain($object->stuurgegevens));

    my $stufmessage = $object->fout(
        {
            reference_id    => 33,
            date            => DateTime->now(),
            code            => 'StUF003',
            plek            => 'client',
            omschrijving    => 'Errormessage',
        }
    );

    ok($stufmessage, 'Created foutbericht, version 0204');

    like($stufmessage, qr/foutBericht/, 'Valid foutcode XML, version 0204');
}

###
### Version 0310
###

{
    my $object      = Zaaksysteem::StUF->new(
        entiteittype    => 'NPS',
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            tijdstipBericht     => '2009040717084815',
            sectormodel         => 'BG',
            versieStUF          => '0204',
            versieSectormodel   => '0204',
            entiteittype        => 'PRS',
            berichtsoort        => 'Lk01',
            referentienummer    => '99999',
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => 'CML',
            },
            kennisgeving        => {
                mutatiesoort    => 'T'
            }
        ),
        version         => '0310',
    );

    ok($object, 'Created StUF object, version 0301');
    #note(explain($object->stuurgegevens));

    my $stufmessage = $object->fout(
        {
            reference_id    => 33,
            date            => DateTime->now(),
            code            => 'StUF003',
            plek            => 'client',
            omschrijving    => 'Errormessage',
        }
    );

    ok($stufmessage, 'Created foutbericht, version 0301');

    like($stufmessage, qr/Fo\d\dBericht/, 'Valid foutcode XML, version 0204');
}


zs_done_testing;