package TestFor::Catalyst::Controller::API::V1::Controlpanel::Instance;
use base qw(ZSTest::Catalyst);

use Crypt::SaltedHash;

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::Controlpanel::Instance - Proves the boundaries of our API: Controlpanel Instance

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Controlpanel/Instance.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/controlpanel/UUID/instance> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create controlpanel instance

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/create/8989asdf-asdf-asdf89a89sa-asdf89/instance/create

B<Request>

=begin javascript

{
   "fqdn" : "test.zaaksysteem.nl",
   "label" : "Testomgeving",
   "otap" : "production"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-acb70c-76978c",
   "result" : {
      "instance" : {
         "fqdn" : "test.zaaksysteem.nl",
         "id" : "9205e6b9-bfed-4f4f-aaa8-aef39bf2e790",
         "label" : "Testomgeving",
         "otap" : "production",
         "owner" : "betrokkene-bedrijf-154"
      },
      "reference" : "9205e6b9-bfed-4f4f-aaa8-aef39bf2e790",
      "type" : "instance"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_instance_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn            => 'test.zaaksysteem.nl',
                label           => 'Testomgeving',
                otap            => 'production',
                password        => 'ThisIsATest',
                protected       => 1,
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);

        ### Check valid password hash
        my $ppr = Authen::Passphrase->from_rfc2307($instance->{password});
        ok($ppr->match('ThisIsATest'), 'Correct hashed password via API');
        ok($instance->{protected}, 'Got a protected instance');
    }, 'api/v1/controlpanel/create: create a simple controlpanel instance by fqdn');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn            => 'test',
                label           => 'Testomgeving',
                otap            => 'production',
                password        => 'ThisIsATest',
                protected       => 1,
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);

        ### Check valid password hash
        my $ppr = Authen::Passphrase->from_rfc2307($instance->{password});
        ok($ppr->match('ThisIsATest'), 'Correct hashed password via API');
        ok($instance->{protected}, 'Got a protected instance');
    }, 'api/v1/controlpanel/create: create a simple controlpanel instance by hostname');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn            => 'test.cyso',
                label           => 'Testomgeving',
                otap            => 'production',
                password        => 'ThisIsATest',
                protected       => 1,
            }
        );

        my $json;
        # my $cp_data  = $self->_get_json_as_perl($json);

        # my $instance = $self->valid_instance_object($cp_data);

        print STDERR $mech->content;

        for my $reserved_host (qw/app1.cyso test.munt munt cyso data1/) {
            $mech->post_json(
                $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
                {
                    fqdn            => $reserved_host,
                    label           => 'Testomgeving',
                    otap            => 'production',
                    password        => 'ThisIsATest',
                    protected       => 1,
                }
            );

            $json        = $mech->content;
            my $cp_data  = $self->_get_json_as_perl($json);

            is($cp_data->{result}->{instance}->{fqdn}->{validity}, 'invalid', 'Correct invalid domain: ' . $reserved_host);
        }

        for my $reserved_host (qw/app1.cyso test.munt munt cyso data1/) {
            $mech->post_json(
                $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
                {
                    fqdn            => $reserved_host . '.zaaksysteem.nl',
                    label           => 'Testomgeving',
                    otap            => 'production',
                    password        => 'ThisIsATest',
                    protected       => 1,
                }
            );

            $json        = $mech->content;
            my $cp_data  = $self->_get_json_as_perl($json);

            is($cp_data->{result}->{instance}->{fqdn}->{validity}, 'invalid', 'Correct invalid domain: ' . $reserved_host);
        }

    }, 'api/v1/controlpanel/create: prevent creating of reserved hostnames');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn            => 'Test',
                label           => 'Testomgeving',
                otap            => 'production',
                password        => 'ThisIsATest',
                protected       => 1,
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);

        ### Check valid password hash
        like($instance->{fqdn}, qr/^[a-z\.]+$/, 'Got a lowercase hostname');
    }, 'api/v1/controlpanel/create: check if system properly converts uppercase host to lowercase');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn                => 'Test',
                label               => 'Testomgeving',
                otap                => 'production',
                password            => 'ThisIsATest',
                protected           => 1,
                api_domain          => 'test.api.zaaksysteem.nl',
                services_domain     => 'test.services.zaaksysteem.nl',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);

        ### Check valid password hash
        like($instance->{fqdn}, qr/^[a-z\.]+$/, 'Got a lowercase hostname');
        is($instance->{api_domain}, 'test.api.zaaksysteem.nl', 'Got a correct API domain');
        is($instance->{services_domain}, 'test.services.zaaksysteem.nl', 'Got a correct services domain');
    }, 'api/v1/controlpanel/create: check if system sets api/services domains');
}

=head2 Update

=head3 update controlpanel instance

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/create/8989asdf-asdf-asdf89a89sa-asdf89/instance/9205e6b9-bfed-4f4f-aaa8-aef39bf2e790/update

B<Request>

=begin javascript

{
   "fqdn" : "test3.zaaksysteem.nl",
   "label" : "Testomgeving 2",
   "otap" : "accept"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ad3ea0-3aa20f",
   "result" : {
      "instance" : {
         "fqdn" : "test3.zaaksysteem.nl",
         "id" : "39aba632-14af-4c8c-bcc0-fa4e3954885e",
         "label" : "Testomgeving 2",
         "otap" : "accept",
         "owner" : "betrokkene-bedrijf-257",
         "template" : "mintlab"
      },
      "reference" : "39aba632-14af-4c8c-bcc0-fa4e3954885e",
      "type" : "instance"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_instance_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'tESt.zaaksysteem.nl',
                label            => 'Testomgeving',
                otap             => 'production',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);
        ok(!$instance->{protected}, 'Got an unprotected instance');

        ### Update the controlpanel object
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $instance->{id} . '/update',
            {
                label            => 'Testomgeving 2',
                protected        => 1,
            }
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);
        $instance = $self->valid_instance_object($cp_data, { skip_checks => { otap => 1 }});

        like($instance->{fqdn}, qr/^[a-z\.]+$/, 'Got a lowercase hostname');
        ok($instance->{protected}, 'Got a protected instance');
        is($instance->{label}, 'Testomgeving 2', 'Instance value label changed.');

    }, 'api/v1/controlpanel/create: update a simple controlpanel');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test.zaaksysteem.nl',
                label            => 'Testomgeving',
                otap             => 'production',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);
        ok(!$instance->{protected}, 'Got an unprotected instance');

        ### Update the controlpanel object
        my %arguments = (
            label            => 'Testomgeving 2',
            protected        => 1,
            api_domain       => 'test3.api.zaaksysteem.nl',
            mail             => 1,
            delete_on        => DateTime->now()->iso8601 . 'Z',
            provisioned_on   => DateTime->now()->iso8601 . 'Z',
            database_provisioned => DateTime->now()->iso8601 . 'Z',
            filestore_provisioned => DateTime->now()->iso8601 . 'Z',
            disabled         => 1,
        );

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $instance->{id} . '/update',
            \%arguments
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);
        $instance = $self->valid_instance_object($cp_data, { skip_checks => { otap => 1 }});

        ok($instance->{protected}, 'Got a protected instance');
        is($instance->{label}, 'Testomgeving 2', 'Instance value label changed.');

        ### Update the controlpanel object
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $instance->{id} . '/update',
            {
                delete_on        => '2035-04-20T13:55:04Z',
                disabled         => 0,
            }
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);
        $instance = $self->valid_instance_object($cp_data, { skip_checks => { otap => 1 }});

        for my $argument (keys %arguments) {
            my $val = $arguments{$argument};

            if ($argument =~ /delete_on|disabled/) {
                isnt($instance->{$argument}, $arguments{$argument}, 'Did not alter object value: ' . $argument);
            } else {
                is($instance->{$argument}, $arguments{$argument}, 'Did not alter object value: ' . $argument);
            }
        }

    }, 'api/v1/controlpanel/create: check for unchanged updates when keys are not set');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test.zaaksysteem.nl',
                label            => 'Testomgeving',
                otap             => 'production',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_instance_object($cp_data);
        ok(!$instance->{protected}, 'Got an unprotected instance');

        ### Update the controlpanel object
        my %arguments = (
            label            => 'Testomgeving 2',
            fqdn             => 'test.zaaksysteem.nl',
        );

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $instance->{id} . '/update',
            \%arguments
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);
        $instance = $self->valid_instance_object($cp_data, { skip_checks => { otap => 1 }});

        ok($instance->{fqdn}, 'Got valid response by setting hostname to the same as it was');
        is($instance->{fqdn}, 'test.zaaksysteem.nl', 'Got valid fqdn');

        ### Update the controlpanel object
        %arguments = (
            label            => 'Testomgeving 3',
            fqdn             => 'test2.zaaksysteem.nl',
        );

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $instance->{id} . '/update',
            \%arguments
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);
        $instance = $self->valid_instance_object($cp_data, { skip_checks => { otap => 1 }});

        is($instance->{fqdn}, 'test2.zaaksysteem.nl', 'Got valid fqdn');

    }, 'api/v1/controlpanel/create: check updating of host');
}


=head2 Get

=head3 get controlpanel instance

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/8989asdf-asdf-asdf89a89sa-asdf89/instance/9205e6b9-bfed-4f4f-aaa8-aef39bf2e790

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-acb70c-76978c",
   "result" : {
      "instance" : {
         "fqdn" : "test.zaaksysteem.nl",
         "id" : "9205e6b9-bfed-4f4f-aaa8-aef39bf2e790",
         "label" : "Testomgeving",
         "otap" : "production",
         "owner" : "betrokkene-bedrijf-154"
      },
      "reference" : "9205e6b9-bfed-4f4f-aaa8-aef39bf2e790",
      "type" : "instance"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_instance_get : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test.zaaksysteem.nl',
                label            => 'Testomgeving',
                otap             => 'production',
            }
        );

         my $json     = $mech->content;
         my $cp_data  = $self->_get_json_as_perl($json);

         my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $cp_data->{result}->{reference});
         $json       = $mech->content;

         $self->valid_instance_object($self->_get_json_as_perl($json));

         # print STDERR $json;

    }, 'api/v1/controlpanel/get: get a simple controlpanel');
}

=head2 List

=head3 get controlpanel instance listing

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-fd8417-5ad631",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "fqdn" : "test2.zaaksysteem.nl",
                  "id" : "8a7a1d51-627e-4faa-94b9-462bcf96be6d",
                  "label" : "Testomgeving 2",
                  "otap" : "production",
                  "owner" : "betrokkene-bedrijf-156"
               },
               "reference" : "8a7a1d51-627e-4faa-94b9-462bcf96be6d",
               "type" : "instance"
            },
            {
               "instance" : {
                  "fqdn" : "test.zaaksysteem.nl",
                  "id" : "65808912-9bfb-4653-a1ea-0ea7db80f2a4",
                  "label" : "Testomgeving",
                  "otap" : "production",
                  "owner" : "betrokkene-bedrijf-156"
               },
               "reference" : "65808912-9bfb-4653-a1ea-0ea7db80f2a4",
               "type" : "instance"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_instance_list : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
         my $mech     = $zs->mech;
         my $controlp = $self->_create_controlpanel;

         $mech->post_json(
             $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
             {
                 fqdn             => 'test' . $_ . '.zaaksysteem.nl',
                 label            => 'Testomgeving ' . $_,
                 otap             => 'production',
             }
         ) for (1..12);

         my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/instance');
         my $json    = $mech->content;

         my $cp_data = $self->_get_json_as_perl($json);

         is($cp_data->{result}->{type}, 'set', 'Got a set of answers');

         $self->valid_instance_object($cp_data);

         print STDERR $json;

         $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/instance?page=2');
         print STDERR $mech->content;


    }, 'api/v1/controlpanel: list all of controlpanel objects of zaaksysteem');
}

=head1 IMPLEMENTATION TESTS

=head2 cat_api_v1_controlpanel_instance_exceptions

=cut

sub cat_api_v1_controlpanel_instance_exceptions : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test1.zaaksysteem.nl',
                label            => 'Testomgeving 1',
                otap             => 'production',
            }
        );
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test1.zaaksysteem.nl',
                label            => 'Testomgeving 1',
                otap             => 'production',
            }
        );

        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        is($cp_data->{result}->{type}, 'validationexception', 'Found validation exception');
        is($cp_data->{result}->{instance}->{fqdn}->{validity}, 'invalid', 'Correct parameter exception: invalid validity');
    }, 'api/v1/controlpanel/create: exceptions');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        ### No instance
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test1.zaaksysteem.nl',
                label            => 'Testomgeving 1',
                otap             => 'production',
            }
        );

        my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/instance/123456');
        # my $r       = $mech->get('/api/v1/controlpanel/' . $cp_data->{result}->{reference});
        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        is($mech->status, '404', 'Got valid exception error');
        is($cp_data->{status_code}, '404', 'Got valid status_code');
        is($cp_data->{result}->{type}, 'exception', 'Got exception for missing interface');
        like($cp_data->{result}->{instance}->{message}, qr/The controlpanel instance object with UUID.*not be found/, 'Exception: controlpanel instance could not be found');
    });

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        ### No instance
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn             => 'test1.zaaksysteem.nl',
                label            => 'Testomgeving 1',
                otap             => 'production',
            }
        );

        my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $controlp->{id});
        # my $r       = $mech->get('/api/v1/controlpanel/' . $cp_data->{result}->{reference});
        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        is($mech->status, '404', 'Got valid exception error');
        is($cp_data->{status_code}, '404', 'Got valid status_code');
        is($cp_data->{result}->{type}, 'exception', 'Got exception for missing interface');
        like($cp_data->{result}->{instance}->{message}, qr/The controlpanel instance object with UUID.*not be found/, 'Exception: controlpanel instance could not be found');
    });
}

sub validate_json_exception {
    my $self    = shift;
    my $mech    = shift;
    my $regex   = shift;
    my $message = shift;

    my $json     = $mech->content;
    my $cp_data  = $self->_get_json_as_perl($json);

    is($mech->status, '500', 'Got valid exception error');
    is($cp_data->{status_code}, '500', 'Got valid status_code');
    like($cp_data->{result}->{instance}->{message}, $regex, $message);
}

sub valid_instance_object {
    my $self      = shift;
    my $perl      = shift;
    my $opts      = shift || {};

    my $instance = $perl->{result}->{instance};
    if ($perl->{result}->{type} eq 'set') {
        $instance = $perl->{result}->{instance}->{rows}->[0]->{instance};
    } else {
       is($perl->{result}->{type}, 'instance', 'Found controlpanel instance');
       like($perl->{result}->{reference}, qr/^\w+\-\w+\-/, 'Got a "reference", like "f87e9f29-5720-4743-9cc5-47887b342709"');
       isa_ok($perl->{result}->{instance}, 'HASH', 'Got an instance of controlpanel');
    }

    like($instance->{fqdn}, qr/^\w+\.[\w+\.]+$/, 'Got a valid value for fqdn');
    like($instance->{label}, qr/^[\w\s]+$/, 'Got a valid value for label');
    is($instance->{otap}, 'production', 'Got a valid value for otap: production') unless ($opts->{skip_checks} && $opts->{skip_checks}->{otap});
    like($instance->{owner}, qr/^betrokkene-bedrijf-\d+$/, 'Got a valid value for owner');

    return $instance;
}

sub _get_json_as_perl {
    my $self    = shift;
    my $json    = shift;

    my $jo      = JSON->new->utf8->pretty->canonical;

    return $jo->decode($json);
}

sub _create_controlpanel {
    my $self     = shift;

    $zs->create_interface_ok(
        module => 'controlpanel'
    );

    my $mech     = $zs->mech;

    my $company  = $zs->create_bedrijf_ok;

    $mech->zs_login;
    $mech->post_json(
        $mech->zs_url_base . '/api/v1/controlpanel/create',
        {
            owner            => 'betrokkene-bedrijf-' . $company->id,
            customer_type    => 'government',
        }
    );

    my $json     = $mech->content;
    my $cp_data  = $self->_get_json_as_perl($json);

    return $cp_data->{result}->{instance};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
