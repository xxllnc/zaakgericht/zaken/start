package TestFor::General::Backend::Betrokkene::NatuurlijkPersoon::ResultSet;

# ./zs_prove -v t/lib/TestFor/General/Backend/Betrokkene/NatuurlijkPersoon/Resultset.pm
use base 'ZSTest';

use Moose;
use TestSetup;

use Zaaksysteem::Backend::Subject::Naamgebruik qw[naamgebruik];

sub betrokkene_natuurlijkpersoon_create_natuurlijk_persoon : Tests {
    my $params  = {
        a_nummer                => '1234567890',
        burgerservicenummer     => '987654321',
        voornamen               => 'Tinus',
        voorletters             => 'TV',
        voorvoegsel             => 'vander',
        geslachtsnaam           => 'Testpersoon',
        geboortedatum           => '19830609',
        datum_overlijden        => '19830610',
        indicatie_geheim        => undef,
        burgerlijkestaat        => undef,
        aanduiding_naamgebruik  => 'E',
        sleutel_gegevensbeheer  => '234234234',
        sleutel_verzendend      => '23423423424234',
        postcode                => '1051JL',
        huisnummer              => '7',
        straatnaam              => 'Donker Curtiusstraat',
        woonplaats              => 'Amsterdam',
        geslachtsaanduiding     => 'M',
        functie_adres           => 'B',
        partner_geslachtsnaam   => 'Testenburg',
        partner_voorvoegsel     => 'de'
    };

    throws_ok(
        sub {
            $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
                $params,
                {
                    authenticated => 'gbaaa'
                }
            );
        },
        qr/Authenticated must match regex/,
        'Valid exception for authenticated flag'
    );

    throws_ok(
        sub {
            $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
                {
                    %$params,
                    burgerservicenummer => 'abc'
                }
            );
        },
        qr/Validation of profile failed/,
        'Valid parameter checking for call'
    );

    $zs->zs_transaction_ok(sub {
        my $entry = $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
            {
                %$params,
            }
        );

        ok(!$entry->authenticated, 'No authenticated entry created');
        ok(!$entry->authenticatedby, 'No authenticatedby param set');

        for my $key (keys %$params) {
            my $checkentry;
            if ($entry->can($key)) {
                $checkentry = $entry;
            } elsif ($entry->adres_id->can($key)) {
                $checkentry = $entry->adres_id;
            }

            next unless $checkentry;

            my $val = $params->{$key};
            if ($key =~ /geboortedatum|datum_overlijden/) {
                $val =~ s/(\d{4})(\d{2})(\d{2})/$1-$2-$3T00:00:00/;
            }

            is($val, $checkentry->$key, 'Found correct value for: ' . $key);
        }

        is $entry->naamgebruik, naamgebruik($params), 'naamgebruik cached after create';

        # Set naamgebruik to partner's name
        $entry->update({ aanduiding_naamgebruik => 'P' });

        is $entry->naamgebruik, naamgebruik({ %{ $params }, aanduiding => 'P' }),
            'naamgebruik cached after update';

    }, 'Created non authenticated person');

    $zs->zs_transaction_ok(
        sub {
            my $rs = $schema->resultset('NatuurlijkPersoon');

            my $np = $rs->find_by_gov_id($params->{burgerservicenummer});
            is($np, undef, "BSN is not found by find_by_gov_id");

            throws_ok(
                sub {
                    $rs->get_by_gov_id(
                        $params->{burgerservicenummer});
                },
                qr#natuurlijk_persoon/BSN/not_found: #, "get_by_gov_id dies on finding nothing"
            );

            my $entry = $rs->create_natuurlijk_persoon($params,
                { authenticated => 'digid' });

            ok($entry->authenticated, 'authenticated entry created');
            is($entry->authenticatedby, 'digid',
                'authenticatedby param set to digid');

            $np = $rs->find_by_gov_id($params->{burgerservicenummer});
            isnt($np, undef, "BSN is found by find_by_gov_id");

            $np = $rs->get_by_gov_id($params->{burgerservicenummer});
            ok($np, "BSN is found by get_by_gov_id");

            throws_ok(
                sub {
                    $rs->_assert_non_existent_bsn(
                        $params->{burgerservicenummer});
                },
                qr#natuurlijk_persoon/exists: #, "_assert_non_existent_bsn has found something",
            );

            throws_ok(
                sub {
                    $rs->create_natuurlijk_persoon($params,
                        { authenticated => 'digid' });
                },
                qr#natuurlijk_persoon/exists: #, "create_natuurlijk_persoon is not possible",
            );

            throws_ok(
                sub {
                    no warnings qw(once redefine);
                    local *Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet::all = sub { return (2, 2) };
                    $rs->get_by_gov_id(1234);
                },
                qr#natuurlijk_persoon/duplicate/bsn: #, "Multiple NP's found on BSN, must die"
            );


        },
        'Created an authenticated person'
    );

}


sub zs_betrokken_object_np : Tests {

    $zs->zs_transaction_ok(
        sub {

            use Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;

            my $dispatch_options = { dbic => $schema, };

            my $np_info = {
                'np-voornamen'           => 'Anton Ano',
                'np-geslachtsnaam'       => 'Zaaksysteem',
                'np-huisnummer'          => 42,
                'np-postcode'            => '1011PZ',
                'np-straatnaam'          => 'Muiderstraat',
                'np-woonplaats'          => 'Amsterdam',
                'np-geslachtsaanduiding' => 'M',
                'np-voornamen'           => 'Anton Ano',
            };

            my $np_id = Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon->new
                ->create($dispatch_options, $np_info,);
            ok($np_id, "Natuurlijk persoon aangemaakt");

            my $np = $zs->get_from_zaaksysteem(
                resultset => 'NatuurlijkPersoon',
                search    => { id => $np_id },
                options   => { rows => 1 },
            )->single;
            ok($np, "Found the DB entry");

            is($np->voorletters, "", "Geen voorletters");

            my $adres_id = $np->adres_id->id;
            ok($adres_id, "Adres ID found");

            my $adres = $zs->get_from_zaaksysteem(
                resultset => 'Adres',
                search    => { id => $adres_id },
                options   => { rows => 1 },
            )->single;
            ok($adres, "Found address data");


            $np_info->{'np-voorletters'} = "A.A";
            $np_id = Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon->new
                ->create($dispatch_options, $np_info,);

            $np = $zs->get_from_zaaksysteem(
                resultset => 'NatuurlijkPersoon',
                search    => { id => $np_id },
                options   => { rows => 1 },
            )->single;
            is($np->voorletters, "A.A", "Wel voorletters");


        },
        'Natuurlijk Persoon create tested',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

