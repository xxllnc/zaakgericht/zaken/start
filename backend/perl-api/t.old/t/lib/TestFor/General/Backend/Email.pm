package TestFor::General::Backend::Email;

# ./zs_prove -v t/lib/TestFor/General/Backend/Email.pm
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::Backend::Email;
use Zaaksysteem::Backend::Mailer;
use Zaaksysteem::Zaken::DelayedTouch;
use Mail::Track;

my ($mime_message, $opts);
no warnings qw(redefine once);
*Email::Sender::Simple::send = sub {
    my $self = shift;
    my $entity = shift;
    $opts  = shift;
    $mime_message = $entity->stringify();
};
use warnings;

sub mock_email_sender {
    my $prove_email = shift;

    my $mock = Test::MockObject->new;
    $mock->mock(
        'send',
        sub {
            my ($self, $message) = @_;
            $prove_email->($message) if $prove_email;
            return 1;
        }
    );
    return $mock;
}

sub create_casetype {

    my $zt               = $zs->create_zaaktype_predefined_ok();
    my $document_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam         => 'document1',
        magic_string => 'magic_string_document',
        value_type   => 'file',
    );
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status => $zs->get_from_zaaksysteem(
            resultset => 'ZaaktypeStatus',
            search    => {
                status           => 1,
                zaaktype_node_id => $zt->zaaktype_node_id->id
            }
            )->first,
        bibliotheek_kenmerk => $document_kenmerk,
    );

    my $bibliotheek_notificatie = $zs->create_notificatie_ok;
    my $notificatie_kenmerk
        = $zs->schema->resultset('BibliotheekNotificatieKenmerk')->create(
        {
            bibliotheek_notificatie_id => $bibliotheek_notificatie->id,
            bibliotheek_kenmerken_id   => $document_kenmerk->id
        }
        );

    return ($zt, $zt_kenmerk->id);
}

sub backend_email_mail_track : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;

    $zs->zs_transaction_ok(
        sub {
            my $body    = 'Hier is wat utf: ®´ß∞¢ –';
            my $subject = 'Subject om mee te testuleren';
            my $from    = 'test@example.com';
            my $to      = 'test@example.net';

            my $interface = $zs->create_interface_ok(
                name   => 'Meuk',
                module => 'emailconfiguration',
                interface_config => {
                    user    => 'test@example.com',
                    subject => 'ZS',
                },
                active => 1,
            );

            my $case = $zs->create_case_ok;
            $case->aanvrager_object->email($to);

            my $id = join('-', $case->id, substr($case->object_data->uuid, -6));

            my $mailer = $case->mailer($from);

            my $mime_message;
            {
                no warnings qw(redefine once);
                local *Mail::Track::Message::send = sub {
                    my $self = shift;
                    my $entity = $self->_build_entity;
                    $mime_message = $entity->stringify();
                };
                $mailer->send_from_case({
                    body      => $body,
                    recipient => $to,
                    subject   => $subject,
                });
            }

            my $mt = Mail::Track->new(
                subject_prefix_name => 'ZS',
                identifier_regex    => qr/(\d+-[a-z0-9]{6})/,
            );

            my $msg = $mt->parse($mime_message);
            is($msg->identifier, $id, "Message identifier is correct");

        },
        "Mail::Track mail sending"
    );
}; # END SKIP

}


sub backend_email : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;

    $zs->zs_transaction_ok(
        sub {

            my $recipient = 'test@mintlab.nl';
            my $body
                = 'Bericht tekst om mee te testuleren, hier is wat utf: ®´ß∞¢ –';
            my $subject = 'Subject om mee te testuleren';
            my $from    = $zs->schema->resultset('Config')->get_customer_config->{zaak_email};

            # Pass the mockup mailer a test function that verifies what is sent
            my $email_sender = mock_email_sender(
                sub {
                    my ($message) = @_;

                    is $message->{body_raw},
                        $body,
                        "Body sent correctly, encoded as utf-8";

                    my %headers = @{ $message->{header}->{headers} };

                    is $headers{Subject}, $subject, 'Subject sent correclty';

                    is $headers{To},   $recipient, "Recipient set correctly";
                    is $headers{From}, $from,      "From header set correctly";
                }
            );

            my $case = $zs->create_case_ok;
            $case->aanvrager_object->email($recipient);

            my $mailer = $case->mailer($email_sender);

            my $rs = $schema->resultset('Contactmoment')
                ->search({ case_id => $case->id });

            throws_ok(
                sub {
                    $mailer->send_from_case;
                },
                qr/Input not a HashRef/,
                "Fails when called without params"
            );

            my $arguments = {
                body      => $body,
                subject   => $subject,
                recipient => 'not_a_valid_email'
            };

            throws_ok(
                sub {
                    $mailer->send_from_case($arguments);
                },
                qr/invalid.*recipient/,
                "Fails when called with incorrect email address"
            );

            $arguments->{recipient} = $case->notification_recipient(
                { recipient_type => 'aanvrager' });

            my $sent_body = $mailer->send_from_case($arguments);

            is $rs->count, 1, "One more contactmoment record present";

            my $contactmoment = $rs->first;

            is $contactmoment->type, 'email', 'Contactmoment type is email';
            is $contactmoment->subject_id, $mailer->get_betrokkene_id,
                "Betrokkene id set";

            $schema->default_resultset_attributes->{current_user} = undef;

            is $mailer->get_betrokkene_id,
                $case->aanvrager_object->rt_setup_identifier,
                "get_betrokkene_id returns np requestor if no current user set";

            $zs->set_current_user;
            like $mailer->get_betrokkene_id, qr/^betrokkene-medewerker-\d+$/,
                "get_betrokkene_id returns medewerker if current user set";

            $recipient = $arguments->{recipient}
                = 'invalid_email;devnull@zaaksysteem.nl,devnull@zaaksysteem.nl';

            throws_ok(
                sub {
                    $mailer->send_from_case($arguments);
                },
                qr/Validation of/,
                'Fails when passed invalid recipient in list'
            );

            $recipient = $arguments->{recipient}
                = 'devnull@zaaksysteem.nl;devnull@zaaksysteem.nl,devnull@zaaksysteem.nl';
            lives_ok(
                sub {
                    $mailer->send_from_case($arguments);
                },
                'Lives when given multiple recipients separated by ; or ,'
            );


            my $ztt_context = { pip_feedback_mail => 'dit is een bericht' };
            $arguments->{additional_ztt_context} = $ztt_context;
            $mailer->send_from_case($arguments);
            is_deeply $mailer->additional_ztt_context, $ztt_context,
                "Ztt context transplanted succesfully";

            my $result = $mailer->replace_magic_strings(
                "Dit is een label: [[pip_feedback_mail]]");
            is $result, "Dit is een label: dit is een bericht",
                'Additional ztt context used in mail';
        },
        'Unit test for mailer'
    );
}; # END SKIP

}

sub backend_email_mailsize : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;
    $zs->txn_ok(
        sub {

            my $case   = $zs->create_case_ok();
            my $id     = $case->id;
            my $mailer = $case->mailer(mock_email_sender);

            my %args = (
                body      => "This is the body",
                subject   => "This is the subject",
                recipient => 'mail@example.com',
                log_error => 0,
            );

            {
                # Size matters
                no warnings qw(once redefine);
                local *Mail::Track::Message::size = sub {
                    return 42 * 1000000;
                };
                use warnings;

                $mailer->send_from_case({ %args, log_error => 1 });
                my $rs = $zs->get_from_zaaksysteem(
                    resultset => 'Logging',
                    search    => { zaak_id => $id },
                    options   => { order_by => { '-desc' => 'id' }, rows => 1 }
                );
                is($rs->count, 1, "Found one log entry");
                $rs = $rs->first;
                is($rs->event_type, "case/email", "found the correct one");
                my $regexp = qr#\QDe maximale grootte van de mail is overschreden (42.00MB / Max: 10MB)\E#;
                like(
                    $rs->onderwerp,
                    $regexp,
                    "Onderwerp matches"
                );

                throws_ok(
                    sub {
                        $mailer->send_from_case(\%args);
                    },
                    qr#\Qcase/email/size: \E$regexp#,
                    "Mail not send due to filesize limitations"
                    )
            };

            {

                no warnings qw(once redefine);
                local *Mail::Track::Message::send = sub {
                    die "We died by foo";
                };
                use warnings;

                $mailer->send_from_case({ %args, log_error => 0 });

                my $rs = $zs->get_from_zaaksysteem(
                    resultset => 'Logging',
                    search    => { zaak_id => $id },
                    options   => { order_by => { '-desc' => 'id' }, rows => 1 }
                );
                is($rs->count, 1, "Found one log entry");
                $rs = $rs->first;
                is($rs->event_type, "case/email", "found the correct one");
                like(
                    $rs->onderwerp,
                    qr#\QBericht 'This is the subject' is niet verstuurd\E: We died by foo#,
                    "Sending fails",
                );
            }



        },
        "Logging or throwing",
    );
}; # END SKIP

}


sub backend_email_attachments : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;

    $zs->zs_transaction_ok(
        sub {
            $zs->schema->default_resultset_attributes->{delayed_touch}
                = Zaaksysteem::Zaken::DelayedTouch->new;

            my ($casetype, $zaaktype_kenmerken_id) = create_casetype;
            my $case = $zs->create_case_ok(zaaktype => $casetype);
            my $mailer = $case->mailer(mock_email_sender);

            my $arguments = {
                recipient => 'test@mintlab.nl',
                body      => 'Dit is een test body',
                subject   => 'Dit is een test subject'
            };

            $mailer->send_from_case($arguments);

            my $log = $schema->resultset('Logging')->search(
                {
                    zaak_id    => $case->id,
                    event_type => 'email/send'
                }
            )->first;

            my $event_data = JSON::from_json($log->event_data);

            is $event_data->{content}, $arguments->{body},
                'Body notated as content in log';
            is $event_data->{subject}, $arguments->{subject},
                'Subject properly stored';
            is_deeply $event_data->{attachments}, [],
                'No attachments when none supplied';

            $arguments->{attachments} = [];

            # add a file to given

            my $filename = 'case_document.txt';
            $schema->resultset('File')->file_create(
                {
                    db_params => {
                        created_by => $zs->get_subject_ok,
                        case_id    => $case->id,
                        accepted   => 1,
                    },
                    case_document_ids => [$zaaktype_kenmerken_id],
                    file_path => $zs->config->{filestore_test_file_path},
                    name      => $filename,
                }
            );

            throws_ok(
                sub {
                    $mailer->_get_files($zaaktype_kenmerken_id);
                },
                qr/Systeemfout/,
                'Fails when called without array'
            );

            my @files = $mailer->_get_files([$zaaktype_kenmerken_id]);

            is scalar @files, 1, "Got one file";

            my ($file) = @files;
            is $file->name . $file->extension, $filename, "Filename is correct";

            my @log_attachments = $mailer->_log_attachments(@files);
            is scalar @log_attachments, 1, "Got one log attachment";
            my ($log_attachment) = @log_attachments;
            is $log_attachment->{filename}, $file->filename,
                "Filename logged correctly";

            my @email_attachments = $mailer->_retrieve_attachments(@files);
            is scalar @email_attachments, 1, "Got one email attachment";
            my ($email_attachment) = @email_attachments;
            isa_ok $email_attachment, 'Email::MIME', 'Got email::mime object';

        },
        'Unit test for mailer attachments'
    );
}; # END SKIP

}


sub backend_email_replace_magic_strings : Tests {
    $zs->zs_transaction_ok(
        sub {
            $zs->schema->default_resultset_attributes->{delayed_touch}
                = Zaaksysteem::Zaken::DelayedTouch->new;

            my $case = $zs->create_case_ok;

            my $mailer = $case->mailer;

            throws_ok(
                sub {
                    $mailer->replace_magic_strings;
                },
                qr/empty_body/,
                'Fails when no input supplied'
            );

            my $prefix = 'Het zaaknummer is: ';
            my $result
                = $mailer->replace_magic_strings($prefix . '[[zaaknummer]]');

            is $result, $prefix . $case->id, 'Magic string replacement works';
        },
        'Verify magic strings replacement in emails'
    );
}

sub backend_email_sender : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $from = 'test@test.nl';
            my $case = $zs->create_case_ok;
            $case->aanvrager_object->email($from);

            my $mailer = $case->mailer;

            is $mailer->sender({}),
                $schema->resultset('Config')->get_customer_config->{zaak_email},
                "Happily returns default email if called without params";

            is $mailer->sender({ sender_address => $from }), $from,
                "Returns given from";

            is $mailer->sender({ sender_address => '[[zaaknummer]]' }),
                $case->id,
                "Understands magic strings";

            is $mailer->sender(
                { sender_address => $from, sender => 'Test [[zaaknummer]]' }),
                '"Test ' . $case->id . "\" <$from>",
                "Combines fields as advertised";
        }
    );
}

sub backend_email_from_header : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $sender_address = 'test@persoon.nl';
            my $sender         = 'Tinus Testpersoon';
            my $desired        = "$sender <$sender_address>";

            # Pass the mockup mailer a test function that verifies what is sent
            my $email_sender = mock_email_sender(
                sub {
                    my ($message) = @_;

                    my %headers = @{ $message->{header}->{headers} };
                    is $headers{From}, $desired, 'From header set correclty';
                }
            );

            my $case   = $zs->create_case_ok;
            my $mailer = $case->mailer($email_sender);

            $mailer->send_from_case(
                {
                    recipient      => 'test@example.nl',
                    subject        => 'Testing the from-header',
                    body           => 'Ignore the rest',
                    sender_address => $sender_address,
                    sender         => $sender
                }
            );

        },
        'Test from header'
    );
}

sub backend_email_send_case_notification : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;
    $zs->zs_transaction_ok(
        sub {
            my $recipient = 'test@test.nl';
            my $case      = $zs->create_case_ok;

            my $notification = $zs->create_notificatie_ok;

            # Pass the mockup mailer a test function that verifies what is sent
            my $email_sender = mock_email_sender(
                sub {
                    my ($message) = @_;

                    my %headers = @{ $message->{header}->{headers} };

                    is $message->body, $notification->message, "Message set";
                    is $headers{Subject}, $notification->subject, "Subject set";
                    is $headers{To}, $recipient, "Recipient set correctly";
                }
            );

            my $mailer = $case->mailer($email_sender);

            $mailer->send_case_notification(
                {
                    notification => $notification,
                    recipient    => $recipient
                }
            );

        },
        'Test send_case_notification routine'
    );
}; # END SKIP

}

sub backend_email_cc : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;

    $zs->zs_transaction_ok(
        sub {

            my $recipient = 'test@test.nl';
            my $cc        = 'cc@test.nl';
            my $case      = $zs->create_case_ok;

            my $notification = $zs->create_notificatie_ok;

            # Pass the mockup mailer a test function that verifies what is sent
            my $email_sender = mock_email_sender(
                sub {
                    my ($message) = @_;

                    my %headers = @{ $message->{header}->{headers} };

                    is $headers{To}, $recipient, "Recipient set correctly";
                    is $headers{Cc}, $cc,        "Cc Recipient set correctly";
                }
            );

            my $mailer = $case->mailer($email_sender);

            $mailer->send_from_case(
                {
                    recipient => $recipient,
                    body      => 'Dit is een test subject',
                    subject   => 'Dit is een test body',
                    cc        => $cc
                }
            );
        }
    );
}; # END SKIP

}

# This is tested by Mail::Track, where all the CC/BCC logic is now located.
sub backend_email_bcc : Tests {
SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;

    $zs->zs_transaction_ok(
        sub {

            my $body      = 'Dit is een test subject',
            my $subject   = 'Dit is een test body',

            my $to        = 'test@test.nl';
            my $bcc       = 'bcc@test.nl';
            my $cc        = 'cc@test.nl';

            my $case      = $zs->create_case_ok;

            my $notification = $zs->create_notificatie_ok;
            my $mailer = $case->mailer(mock_email_sender);

            my $opts;
            {
                no warnings qw(redefine once);
                local *Mail::Track::Message::send = sub {
                    my $self = shift;
                    $opts = {};
                    $opts->{$_} = $self->$_ foreach qw(to cc bcc);
                };
                $mailer->send_from_case({
                    body      => $body,
                    recipient => $to,
                    cc        => $cc,
                    bcc       => $bcc,
                    subject   => $subject,
                });
                is_deeply($opts, { to => $to, cc => $cc, bcc => $bcc }, "Correct recipients");

                $mailer->send_from_case({
                    body      => $body,
                    recipient => $to,
                    cc        => $cc,
                    subject   => $subject,
                });
                is_deeply($opts, { to => $to, cc => $cc, bcc => '' }, "Correct recipients, no bcc");

                $mailer->send_from_case({
                    body      => $body,
                    recipient => $to,
                    subject   => $subject,
                });
                is_deeply($opts, { to => $to, cc => '', bcc => '' }, "Correct recipients, no bcc or cc");

                $mailer->send_from_case({
                    body      => $body,
                    recipient => $to,
                    bcc       => $bcc,
                    subject   => $subject,
                });
                is_deeply($opts, { to => $to, cc => '', bcc => $bcc }, "Correct recipients, no cc");
            }
        }
    );
}; # END SKIP

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
