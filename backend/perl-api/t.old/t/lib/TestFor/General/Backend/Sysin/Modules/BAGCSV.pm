package TestFor::General::Backend::Sysin::Modules::BAGCSV;
use base qw(ZSTest);
use TestSetup;

use Memory::Usage;

use Zaaksysteem::Backend::Sysin::Modules::BAGCSV;

=head1 NAME

TestFor::General::Backend::Sysin::Modules::BAGCSV - BAGCSV interface tester

=head1 SYNOPSYS

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/BAGCSV.pm

=cut

sub _create_interface {
    return $zs->create_interface_ok(
        module => 'bagcsv',
        name   => 'BAG Import',
        active => 1,
    );
}

sub zs_bagcsv_from_file : Tests {
    $zs->txn_ok(
        sub {

            my $interface = _create_interface();
            my $filestore = $zs->create_filestore_ok(
                file_path      => 'root/examples/csv/bag_csv.csv',
                original_name => 'bag.csv',
            );

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            my $rs = $zs->schema->resultset('BagWoonplaats')->search_rs({identificatie => 1331});
            is($rs->count, 1, "Found one woonplaats");

            my $wp = $rs->first;
            is($wp->naam, "Bussum", "Woonplaats is bussum");

        },
        "BAG CSV with an actual file"
    );

}

sub zs_bagcsv_memory : Tests {
    my $self = shift;

    if (!$ENV{ZS_DEVELOPER_MEMORY}) {
        $self->builder->skip(
            "Skipping memory function, please set ZS_DEVELOPER_MEMORY to enable memory checks"
        );

        return;
    }

    my $mu = Memory::Usage->new();
    my $counter = 0;

    $zs->zs_transaction_ok(sub {
        my $filestore = $zs->create_filestore_ok(
            filepath      => 'root/examples/csv/bag_csv.csv',
            original_name => 'bag.csv',
        );

        my $interface = _create_interface();

        my $transaction = $interface->process(
            {
                input_filestore_uuid    => $filestore->uuid,
                external_transaction_id => DateTime->now->iso8601,
            }
        );

        $mu->record('BAG Import request START') if $mu;

        for (1..10) {
            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );
            $mu->record('BAG Import request ' . ++$counter) if $mu;
        }
    });

    ### Uncomment this to get the memory usage
    $mu->dump;
}

sub zs_bagcsv_from_file_developer : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_BAG}) {
        $self->builder->skip(
            "We don't have the actual BAG CSV file from the customer"
        );
        return;
    }

    use File::Basename;

    my $path = $ENV{ZS_DEVELOPER_BAG};

    $zs->txn_ok(
        sub {

            my $interface = _create_interface();
            my $filename = basename($path);
            my $filestore = $zs->create_filestore_ok(
                file_path      => $path,
                original_name  => $filename,
            );

            my $t = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );
            ok($t->success_count, "Imported $filename");
            is($t->transaction_records->count, 5, "Got all records");

            my @woonplaats = qw(1344 1345 1346 1347);

            my $rs = $zs->schema->resultset('BagWoonplaats')->search_rs({identificatie => {-in => \@woonplaats }});
            is($rs->count, scalar @woonplaats, "Got all the BagWoonplaatsen");


        },
        "BAG CSV with an actual file: $path"
    );
}

sub zs_bagcsv : Tests {

    $zs->zs_transaction_ok(
        sub {

            my $interface = _create_interface();
            my $filestore = $zs->create_filestore_ok;

            TODO: {
                local $TODO = "Processes fine for unknown reasons";
                throws_ok(
                    sub {
                        $interface->process(
                            {
                                input_data => 'baababa',
                                external_transaction_id =>
                                    DateTime->now->iso8601,
                            }
                        );
                    },
                    qr/we only allow filestore references/,
                    'BAGCSV: Valid checking for filename only'
                );
            }

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ok($transaction->$_, 'Transaction attribute set: ' . $_) for qw/
                date_created
                date_last_retry
                external_transaction_id
                input_file
                interface_id
                /;


            is($transaction->interface_id->id,
                $interface->id, 'Transaction matches interface');

            is($transaction->input_file->id,
                $filestore->id, 'Transaction matches filestore_id');

        },
        'Tested: File and transaction processing basics'
    );


    $zs->zs_transaction_ok(
        sub {
            my $interface = _create_interface();

            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            is($transaction->transaction_records->count,
                5, 'Found 5 transaction records');

            my $first_record = $transaction->transaction_records->first;

            #note(explain({ $first_record->get_columns }));
            ok($first_record->$_, 'Transaction record attribute set: ' . $_)
                for qw/
                date_executed
                input
                output
                transaction_id
                /;

        },
        'Tested: Transaction records basic'
    );


    $zs->zs_transaction_ok(
        sub {
            my $interface = _create_interface();

            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ### Check mutations
            my $entry = $schema->resultset('BagWoonplaats')->search()->first;
            ok($entry, 'Found city');
            is($entry->identificatie, '1331',   'Correct ID on woonplaats');
            is($entry->naam,          'Bussum', 'Correct name on woonplaats');
            ok(($entry->begindatum =~ /2007\-\d{2}\-\d{2}/),
                'Correct begindatum on woonplaats');

            my $mutations
                = $schema->resultset('TransactionRecordToObject')->search(
                {
                    local_table => 'BagWoonplaats',
                    local_id    => $entry->id
                }
                );
            my $mutation = $mutations->first;

            is($mutations->count, 5, 'Found five transaction mutations');
            ok(
                $mutation->transaction_record_id,
                'Mutation has transaction_record_id'
            );
            ok($mutation->mutations, 'Mutation has detailed mutation info');


        },
        'Tested: BAGCSV woonplaats results'
    );

    $zs->zs_transaction_ok(
        sub {
            my $interface = _create_interface();

            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ### Check mutations
            my $entry = $schema->resultset('BagOpenbareruimte')
                ->search({}, { order_by => 'id' })->first;
            ok($entry, 'Found openbare ruimte');
            is($entry->identificatie, '0381300000100001',
                'Correct ID on entry');
            is($entry->naam, 'Aaltje Noordewierlaan', 'Correct name on entry');
            ok(($entry->begindatum =~ /2009\-\d{2}\-\d{2}/),
                'Correct begindatum on entry');

        },
        'Tested: BAGCSV openbareruimte results'
    );

    $zs->zs_transaction_ok(
        sub {
            my $cols = {
                identificatie        => '0381200000100500',
                huisnummer           => 1,
                huisletter           => 'A',
                huisnummertoevoeging => '1-R',
                postcode             => '1403JA',
                inonderzoek          => 'J',
                type                 => 'Verblijfsobject',
                status               => 'Naamgeving uitgegeven',
            };

            my $interface = _create_interface();

            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ### Check mutations
            my $entry = $schema->resultset('BagNummeraanduiding')
                ->search({}, { order_by => 'id' })->first;
            ok($entry, 'Found nummeraanduiding');

            is($entry->$_, $cols->{$_}, 'Correct ' . $_ . ' on entry')
                for keys %{$cols};

        },
        'Tested: BAGCSV nummeraanduiding results'
    );

    $zs->zs_transaction_ok(
        sub {
            my $cols = {
                oppervlakte => 67,
                status      => 'Verblijfsobject in gebruik',
                inonderzoek => 'N',
            };

            my $interface = _create_interface();

            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ### Check mutations
            my $entry = $schema->resultset('BagVerblijfsobject')
                ->search({}, { order_by => 'id' })->first;
            ok($entry, 'Found verblijfsobject');

            is($entry->$_, $cols->{$_}, 'Correct ' . $_ . ' on entry')
                for keys %{$cols};

        },
        'Tested: BAGCSV verblijfsobject results'
    );

    $zs->zs_transaction_ok(
        sub {
            my $cols = {
                identificatie => '0381010000020398',
                gebruiksdoel  => 'woonfunctie',
            };

            my $interface = _create_interface();

            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ### Check mutations
            my $entry = $schema->resultset('BagVerblijfsobjectGebruiksdoel')
                ->search({}, { order_by => 'id' })->first;
            ok($entry, 'Found gebruiksdoel');

            is($entry->$_, $cols->{$_}, 'Correct ' . $_ . ' on entry')
                for keys %{$cols};

        },
        'Tested: BAGCSV gebruiksdoel results'
    );

    $zs->zs_transaction_ok(
        sub {
            my $cols = {
                identificatie => '0381100000118244',
                bouwjaar      => '1992',
                inonderzoek   => 'N',
                status        => 'Pand in gebruik'
            };

            my $interface = _create_interface();
            my $filestore = $zs->create_filestore_ok;

            my $transaction = $interface->process(
                {
                    input_filestore_uuid    => $filestore->uuid,
                    external_transaction_id => DateTime->now->iso8601,
                }
            );

            ok(
                $schema->resultset('BagVerblijfsobjectPand')->search(
                    {
                        identificatie => '0381010000020398',
                        pand          => '0381100000118244',

                    }
                    )->count,
                'Found pand koppeling'
            );

            ### Check mutations
            my $entry = $schema->resultset('BagPand')
                ->search({}, { order_by => 'id' })->first;
            ok($entry, 'Found pand');

            is($entry->$_, $cols->{$_}, 'Correct ' . $_ . ' on entry')
                for keys %{$cols};

        },
        'Tested: BAGCSV gebruiksdoel results'
    );

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
