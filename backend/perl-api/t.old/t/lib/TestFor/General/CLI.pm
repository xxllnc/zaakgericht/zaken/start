package TestFor::General::CLI;
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::CLI;

sub zs_cli : Tests {

    my %args = (
        hostname => 'vagrant',
        config   => 't/data/etc/zaaksysteem.conf',
    );

    {
        my $cli = Zaaksysteem::CLI->new(%args);

        isa_ok($cli, "Zaaksysteem::CLI");
        isa_ok($cli->log, "Log::Log4perl::Logger", "Got the logger object");
        ok(!$cli->log->is_debug, "We are not in debug mode");
    }

    {
        my $cli = Zaaksysteem::CLI->new(
            %args,
            debug => 1,
            dry   => 0,
        );
        ok($cli->log->is_debug, "We are in debug mode");
    }


    {
        @ARGV = qw(--config t/data/etc/zaaksysteem.conf --hostname vagrant);
        my $cli = Zaaksysteem::CLI->init();
        isa_ok($cli, "Zaaksysteem::CLI");
        isa_ok($cli->log, "Log::Log4perl::Logger", "Got the logger object");
        ok(!$cli->log->is_debug, "We are not in debug mode");
        ok(!$cli->dry, "We are not running in dry mode");
    }

    {
        @ARGV = qw(--config t/data/etc/zaaksysteem.conf --hostname vagrant --debug -n);
        my $cli = Zaaksysteem::CLI->init();
        isa_ok($cli, "Zaaksysteem::CLI");
        isa_ok($cli->log, "Log::Log4perl::Logger", "Got the logger object");
        ok($cli->log->is_debug, "We are in debug mode");
        ok($cli->dry, "We are running in dry mode");
    }
    {
        my $cli = Zaaksysteem::CLI->new(
            %args,
            volatile => 1,
            dry      => 1,
        );
        throws_ok(sub { $cli->run }, qr#ZS/CLI/volatile: Unable to proceed#, "Volatile mode and dry running is not allowed");
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
