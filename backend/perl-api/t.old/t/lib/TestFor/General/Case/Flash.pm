package TestFor::General::Case::Flash;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Constants;


sub check_messages {
    my ($case, $status, $expected_message) = @_;

    $case->payment_status($status);
    my @messages = $case->display_flash_messages;

    is scalar @messages, 1, "One message generated";
    my ($message) = @messages;
    is $message->{type}, 'error', 'Message type is error';
    is $message->{message}, $expected_message, 'Message text correct';
}


sub case_check_messages : Tests {
    $zs->zs_transaction_ok(sub {

        my $case = $zs->create_case_ok;

        check_messages($case,
            CASE_PAYMENT_STATUS_FAILED,
            'Let op, betaling niet succesvol'
        );
    }, 'Case flash messages: failed');

    $zs->zs_transaction_ok(sub {
        my $case = $zs->create_case_ok;

        check_messages($case,
            CASE_PAYMENT_STATUS_PENDING,
            'Let op, betaling (nog) niet afgerond'
        );
    }, 'Case flash messages: pending');


    $zs->zs_transaction_ok(sub {
        my $case = $zs->create_case_ok;

        my $aanvrager = $case->aanvrager_object;
        ok $aanvrager->can('messages'), "Aanvrager supports messages";

        my $messages = $aanvrager->messages;

        is scalar keys %$messages, 0, "By default no messages";
    }, 'Case flash messages: aanvrager without messages');


    $zs->zs_transaction_ok(sub {
        my $case = $zs->create_case_ok;

        my $aanvrager = $case->aanvrager_object;
        my $gm_extern_np = $aanvrager->gm_extern_np;

        $gm_extern_np->onderzoek_persoon(1);
        $gm_extern_np->update;

        $aanvrager->is_briefadres(1);
        $aanvrager->is_overleden(1);
        $aanvrager->indicatie_geheim(1);
        $aanvrager->woonplaats('Kaas'); # change woonplaats to trigger is_verhuisd

        my @messages = $case->display_flash_messages;

        is scalar @messages, 5, "Array of messages";

        my ($message) = @messages;
        is $message->{message}, 'Betrokkene staat in onderzoek', "Message as expected";
    }, 'Case flash messages: aanvrager with all messages');

}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

