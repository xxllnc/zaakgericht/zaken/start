package Zaaksysteem::Test::API::v1::Serializer::Reader::Controlpanel;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

use Zaaksysteem::API::v1::Serializer::Reader::Controlpanel;
use Zaaksysteem::API::v1::Serializer;
use Zaaksysteem::Constants qw(ZAAKSYSTEEM_CSS_TEMPLATES);


sub test_host_reader {

    my $serializer = Zaaksysteem::API::v1::Serializer->new();
    isa_ok($serializer, 'Zaaksysteem::API::v1::Serializer');

    my $reader
        = Zaaksysteem::API::v1::Serializer::Reader::Controlpanel->new();
    isa_ok($reader, 'Zaaksysteem::API::v1::Serializer::Reader::Controlpanel');

    my %mapping = (
        owner    => mock_strict(value => "You are"),
        fqdn     => mock_strict(value => "fqdn.foo"),
        label    => mock_strict(value => "Some label"),
        ssl_key  => mock_strict(value => undef),
        ssl_cert => mock_strict(value => undef),
    );

    my $object_data = mock_strict(
        id              => 'foo',
        result_source   => {
            schema => {
                resultset => {
                    search => {
                        all => sub { return () }
                    }
                }
            }
        },
        get_object_attribute => sub {
            my $term = shift;
            return $mapping{$term} if exists $mapping{$term};
            die "unable to get $term from get_object_attribute";
        },
    );

    my $result = $reader->read_host($serializer, $object_data);
    cmp_deeply(
        $result,
        {
            instance => {
                fqdn     => 'fqdn.foo',
                id       => 'foo',
                label    => 'Some label',
                owner    => 'You are',
                ssl_cert => undef,
                ssl_key  => undef,
                instance => undef,
                template => undef,
            },
            reference => 'foo',
            type      => 'host',
        },
        "hosts serialized by its reader"
    );

    $mapping{template} = mock_strict(value => "mintlab"),

    $result = $reader->read_host($serializer, $object_data, { from_instance => 0 });

    cmp_deeply(
        $result,
        {
            instance => {
                fqdn     => 'fqdn.foo',
                id       => 'foo',
                label    => 'Some label',
                owner    => 'You are',
                ssl_cert => undef,
                ssl_key  => undef,
                template => 'mintlab',
                instance => undef,
            },
            reference => 'foo',
            type      => 'host',
        },
        "hosts serialized by its reader with options set"
    );
}

sub test_controlpanel_reader {

    my $serializer = Zaaksysteem::API::v1::Serializer->new();
    isa_ok($serializer, 'Zaaksysteem::API::v1::Serializer');

    my $reader
        = Zaaksysteem::API::v1::Serializer::Reader::Controlpanel->new();
    isa_ok($reader, 'Zaaksysteem::API::v1::Serializer::Reader::Controlpanel');

    my %mapping = (
        customer_type     => mock_strict(value => "government"),
        domain            => mock_strict(value => "fqdn.foo"),
        owner             => mock_strict(value => "betrokkene-bedrijf-42"),
        read_only         => mock_strict(value => \1),
        shortname         => mock_strict(value => "getshorty"),
        template          => mock_strict(value => "mintlab"),
        stat_diskspace    => mock_strict(value => 25),
        allowed_instances => mock_strict(value => 100),
        allowed_diskspace => mock_strict(value => 100),
    );

    my $object_data = mock_strict(
        id            => 'foo',
        result_source => {
            schema => {
                resultset => {
                    search => {
                        all => sub { return () }
                    }
                }
            }
        },
        get_object_attribute => sub {
            my $term = shift;
            return $mapping{$term} if exists $mapping{$term};
            die "unable to get $term from get_object_attribute";
        },
    );

    my $result = $reader->read_controlpanel($serializer, $object_data);

    cmp_deeply(
        $result,
        {
            instance => {
                customer_type       => "government",
                domain              => "fqdn.foo",
                owner               => "betrokkene-bedrijf-42",
                read_only           => \1,
                shortname           => "getshorty",
                template            => "mintlab",
                stat_diskspace      => 25,
                allowed_instances   => 100,
                allowed_diskspace   => 100,
                id                  => 'foo',
                available_templates => ZAAKSYSTEEM_CSS_TEMPLATES,
            },
            reference => 'foo',
            type      => 'controlpanel',
        },
        "controlpanel serialized by its reader"
    );
}

sub test_instance_reader {

    my $serializer = Zaaksysteem::API::v1::Serializer->new();
    isa_ok($serializer, 'Zaaksysteem::API::v1::Serializer');

    my $reader
        = Zaaksysteem::API::v1::Serializer::Reader::Controlpanel->new();
    isa_ok($reader, 'Zaaksysteem::API::v1::Serializer::Reader::Controlpanel');

    my $now     = DateTime->now();
    my %mapping = (
        api_domain            => mock_strict(value => 'foo.api.domain'),
        customer_type         => mock_strict(value => 'government'),
        database              => mock_strict(value => 'database-name'),
        database_host         => mock_strict(value => 'pg01.host'),
        database_provisioned  => mock_strict(value => $now),
        delete_on             => mock_strict(value => undef),
        disabled              => mock_strict(value => 0),
        fallback_url          => mock_strict(value => 'https://leanback'),
        filestore             => mock_strict(value => 'foo'),
        filestore_provisioned => mock_strict(value => $now),
        fqdn                  => mock_strict(value => 'foo.fqdn'),
        freeform_reference    => mock_strict(value => 'foofoo'),
        label                 => mock_strict(value => 'foo'),
        mail                  => mock_strict(value => \1),
        maintenance           => mock_strict(value => \1),
        network_acl           => mock_strict(value => undef),
        otap                  => mock_strict(value => 'production'),
        owner            => mock_strict(value => 'betrokkene-bedrijf-54'),
        password         => mock_strict(value => 'foo'),
        protected        => mock_strict(value => \1),
        provisioned_on   => mock_strict(value => $now),
        services_domain  => mock_strict(value => 'services.fqdn'),
        software_version => mock_strict(value => 'production'),
        stat_diskspace   => mock_strict(value => 100),
        template         => mock_strict(value => 'mintlab'),
        oidc_organization_id  => mock_strict(value => undef),
    );

    my $object_data = mock_strict(
        id              => 'foo',
        result_source   => {
            schema => {
                resultset => {
                    search => {
                        all => sub { return () }
                    }
                }
            }
        },
        has_object_attribute => sub {
            my $term = shift;
            return 1 if exists $mapping{$term};
            return 0;
        },
        get_object_attribute => sub {
            my $term = shift;
            return $mapping{$term} if exists $mapping{$term};
            die "unable to get $term from get_object_attribute";
        },
    );

    my $result = $reader->read_instance($serializer, $object_data,
        { from_host => 1 });

    cmp_deeply(
        $result,
        {
            instance => {
                id     => 'foo',
                status => 'active',

                api_domain            => 'foo.api.domain',
                customer_type         => 'government',
                database              => 'database-name',
                database_host         => 'pg01.host',
                database_provisioned  => $now . "Z",
                delete_on             => undef,
                disabled              => 0,
                fallback_url          => 'https://leanback',
                filestore             => 'foo',
                filestore_provisioned => $now . "Z",
                fqdn                  => 'foo.fqdn',
                freeform_reference    => 'foofoo',
                label                 => 'foo',
                mail                  => \1,
                maintenance           => \1,
                network_acl           => undef,
                otap                  => 'production',
                owner                 => 'betrokkene-bedrijf-54',
                password              => 'foo',
                protected             => \1,
                provisioned_on        => $now . "Z",
                services_domain       => 'services.fqdn',
                software_version      => 'production',
                stat_diskspace        => 100,
                template              => 'mintlab',

                nginx_upstream_configuration => undef,
                extra_data => undef,
                oidc_organization_id => undef,
            },
            reference => 'foo',
            type      => 'instance',
        },
        "instance serialized by its reader"
    );
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::API::v1::Serializer::Reader::Controlpanel - Test the
API/v1 reader for control panel objects

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::API::v1::Serializer::Reader::Controlpanel

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
