package Zaaksysteem::Test::Backend::Email;
use Zaaksysteem::Test;
use Zaaksysteem::Backend::Email::Case;

sub test__create_smtp_transport {
    my $case = mock_case();

    my @args;
    my $control = override(
        "Email::Sender::Transport::SMTP::new" =>
        sub {
            my $self = shift;

            @args = @_;

            return 'foo';
        },
    );

    my $zbe = Zaaksysteem::Backend::Email::Case->new(
        case => $case,
    );

    my $config = {
        smarthost_hostname => 'smtp.example.com',
        smarthost_port => '587',
        smarthost_username => 'guest',
        smarthost_password => 's3kr1t',
    };
    my $interface = undef;

    my $transport = $zbe->_create_smtp_transport($interface, $config);

    is($transport, 'foo', 'Email::Sender::Transport::SMTP "new" was called');
    cmp_deeply(
        {@args},
        {
            ssl => 'starttls',
            ssl_options => {
                # Use system-default SSL
                SSL_ca_path => '/etc/ssl/certs'
            },
            host => $config->{smarthost_hostname},
            port => $config->{smarthost_port},
            sasl_username => $config->{smarthost_username},
            sasl_password => $config->{smarthost_password},
        },
        '.. and it was called with the right arguments'
    );
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Email - Test ZS::Backend::Email

=head1 DESCRIPTION

Test email backend code

=head1 SYNOPSIS

    prove -l -v t

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


