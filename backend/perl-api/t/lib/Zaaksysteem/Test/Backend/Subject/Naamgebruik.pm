package Zaaksysteem::Test::Backend::Subject::Naamgebruik;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Subject::Naamgebruik qw(voorletters);

sub test_voorletters {

    my %names = (
        'Johnny Bravo' => 'J.B.',
        'King James the Second' => 'K.J.T.S.',
        'Me' => 'M.',
        '' => '',
    );
    foreach (keys %names) {
        is(voorletters($_), $names{$_}, "$_ becomes $names{$_}");
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Subject::Naamgebruik - Test the subject naamgebruik
module

=head1 DESCRIPTION

Tests for the subject naamgebruik module

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Subject::Naamgebruik

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
