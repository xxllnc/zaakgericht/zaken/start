package Zaaksysteem::Test::Object::Queue::Model::Case;
use Zaaksysteem::Test;

use Zaaksysteem::Backend::Object::Queue::Component;
use Zaaksysteem::Object::Queue::Model;

=head1 NAME

Zaaksysteem::Test::Object::Queue::Model::Case - Test queue items related to cases

=head1 DESCRIPTION

Test "case" queue items.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Queue::Model::Case

=head2 test_add_case_subject

Test the "add case subject" queue item handling.

=cut

sub test_add_case_subject {

    my $relateren_args;
    my $rv = {
        send_mail      => 0,
        create_subject => 0,
    };
    my $case = mock_case(
        betrokkene_relateren => sub {

            # Copy values, not references.
            $relateren_args = [@_];

            return 1;
        },
        mailer => {
            send_case_notification => sub {
                $rv->{send_mail} = 1;
                return 1;
            },
        },
        result_source => {
            schema => {
                betrokkene_model => {
                    get_by_string => {
                        can          => 1,
                        gm_extern_np => { can => 1 },
                        deleted_on => 0, email => 'arniek@example.com',
                        display_name => 'Jadda',
                    }
                }
            },
            resultset => {
                ### RS: Config
                get => 12345,
                ### RS: BibliotheekNotificaties
                find => 1,
            }
        }
    );

    my $role = mock_moose_class(
        {
            superclasses => ['Zaaksysteem::Object::Queue::Model'],
            roles        => [
                'Zaaksysteem::Object::Queue::Model::Case',
                'MooseX::Log::Log4perl'
            ],
        },
        {
            table                  => mock_one(),
            statsd                 => mock_one(),
            base_uri               => URI->new('http://localhost/'),
            instance_hostname      => 'localhost',
            message_queue_factory  => sub { },
            message_queue_exchange => 'amq.topic',
            schema                 => mock_one(),
            rs_zaak                => mock_strict(
                search_rs => { single => $case },
            ),
        }
    );


    my $item = mock_one(
        data => {
            betrokkene_identifier => 'betrokkene-id',
            magic_string_prefix   => 'magicstring',
            rol                   => 'dit is hoe ik rol',
            gemachtigd            => 1,
            notify                => 1,
        },
    );
    $role->add_case_subject($item);

    is_deeply(
        $relateren_args,
        [
            {
                betrokkene_identifier  => 'betrokkene-id',
                magic_string_prefix    => 'magicstring',
                rol                    => 'dit is hoe ik rol',
                pip_authorized         => 1,
                send_auth_confirmation => 1,
            },
        ],
    );

    ok($rv->{send_mail}, "Mail send");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
