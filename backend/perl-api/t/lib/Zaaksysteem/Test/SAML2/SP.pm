package Zaaksysteem::Test::SAML2::SP;
use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Test::XML qw(:all);
use IO::All;
use Zaaksysteem::SAML2::SP;
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use URN::OASIS::SAML2       qw(:urn :binding);
use XML::LibXML qw();

sub _get_text {
    my $fh  = shift;
    my $tmp = File::Temp->new();
    {
        $/ = undef;
        my $contents = <$fh>;
        print $tmp $contents;
    }
    close($tmp);
    return $tmp;
}

sub get_certs {
    my $crt;
    my $key;

    if ($ENV{ZS_METADATA_CRT}) {
        open my $fh, '<', $ENV{ZS_METADATA_CRT};
        $crt = _get_text($fh);
    }
    if ($ENV{ZS_METADATA_KEY}) {
        open my $fh, '<', $ENV{ZS_METADATA_KEY};
        $key = _get_text($fh);
    }

    return ($crt, $key);
}

sub generate_idp {
    my %args = @_;

    my $crt = delete $args{crt};
    my $key = delete $args{key};

    my $idp_uuid = generate_uuid_v4();

    my $idp_interface = mock_one(
        uuid                 => $idp_uuid,
        get_interface_config => sub { return {} },
        jpath                => "Some cert",
        %args,
    );

}

sub generate_sp {
    my %args = @_;

    my $crt = delete $args{crt};
    my $key = delete $args{key};

    my $sp_uuid      = generate_uuid_v4();
    my $sp_interface = mock_strict(
        uuid                 => $sp_uuid,
        get_interface_config => sub {
            return {
                contact_id => {
                    object_type => 'medewerker',
                    id          => 1,
                },
                sp_application_name => 'Zaaksysteem testsuite',
                sp_webservice       => 'https://test.zaaksysteem.nl/auth/saml',
            };
        },
        result_source => {
            schema => {
                betrokkene_model =>
                    { get => { email => 'devnull@example.com' } },
                resultset => sub {
                    my $rs = shift;
                    if ($rs eq 'Filestore') {
                        return $crt ? mock_one(get_path => $crt) : mock_one();
                    }
                    return mock_strict();
                },
            }
        },
    );
}

sub test_metadata_creation_ms_adfs_online {

    my ($crt, $key) = get_certs();

    my $sp_interface = generate_sp(key => $key, crt => $crt);

    my $idp_interface = generate_idp(
        key => $key,
        crt => $crt,

        get_interface_config => sub {
            return {
                binding =>
                    'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                disable_requested_authncontext => 1,
                idp_entity_id                  => 'myIDPEntityID',
                idp_metadata                   => 'https://login.example.com/',
                idp_metadata_filename          => undef,
                saml_request_nameid            =>
                    'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
                saml_type    => 'adfs',
                sp_metadata  => 'https://dev.zaaksysteem.nl/',
                use_nameid   => 1,
                use_saml_slo => 0,
            };
        }
    );

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        interface => $sp_interface,
        idp       => $idp_interface,
    );

    isa_ok($sp, "Zaaksysteem::SAML2::SP");

    my $override = get_override($crt);

    my $metadata = $sp->metadata;

    my $xp = _get_xpath_metadata($metadata);

    my $id = $sp_interface->uuid;
    $id =~ s/^([0-9])/_$1/;

    my $base = '/md:EntityDescriptor';

    {
        my $node = get_node($xp, $base);

        is($node->getAttribute('entityID'),
            'myIDPEntityID', "... and correct entityID");
        is($node->getAttribute('ID'), $id, "... and correct ID");
        is($node->getAttribute('xmlns:dsig'),
            URN_SIGNATURE, 'dsig namespace available')
            if $crt;

    }

    TODO: {
        local $TODO = "Does not work";
        my $xpath = "$base/md:SPSSODescriptor/md:AssertionConsumerService";
        my $node
            = get_node($xp, $xpath . '[@Binding="' . BINDING_HTTP_POST . '"]');
        is($node->getAttribute('index'),     1, "... correct index");
        is($node->getAttribute('isDefault'), "true", "... is the default");
        is(
            $node->getAttribute('Location'),
            "https://test.zaaksysteem.nl/auth/saml/consumer-post",
            "... and correct location"
        );
    }

    TODO: {
        local $TODO = "Does not work";
        my $xpath = "$base/md:SPSSODescriptor/md:AssertionConsumerService";
        my $node
            = get_node($xp,
            $xpath . '[@Binding="' . BINDING_HTTP_ARTIFACT . '"]');
        is($node->getAttribute('index'),     2, "... correct index");
        is($node->getAttribute('isDefault'), "false", "... isnt the default");
        is(
            $node->getAttribute('Location'),
            "https://test.zaaksysteem.nl/auth/saml/consumer-artifact",
            "... and correct location"
        );
    }


    #    diag $metadata;

}

sub get_override {
    my $crt = shift;
    my $override
        = override('Net::SAML2::Util::generate_id' => sub { return "Some ID" }
        );

    return $override if $crt;
    $override->override(
        'Net::SAML2::SP::_build_cert_text' => sub { return 'cert as string' });

    $override->override('XML::Sig::_load_key'       => sub { return 1 });
    $override->override('XML::Sig::_load_cert_file' => sub { return 1 });
    $override->override('XML::Sig::sign' =>
            sub {
                my $self = shift;
                my $md = shift;
                return $md;
            });

    $override->override(
        'XML::LibXML::Node::nodeName' => sub { return 'dsig:Signature' });

    return $override;
}

sub _get_xpath_metadata {
    my $xp = get_xpath(shift);
    $xp->registerNs('md', URN_METADATA);
    $xp->registerNs('ds', URN_SIGNATURE);
    return $xp;
}

sub test_metadata_creation_plain {

    my ($crt, $key) = get_certs();

    my $sp_interface  = generate_sp(key => $key, crt => $crt);
    my $idp_interface = generate_idp(key => $key, crt => $crt);

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        interface => $sp_interface,
        idp       => $idp_interface,
    );

    my $override = get_override($crt);

    my $metadata = $sp->metadata;
    my $xp       = _get_xpath_metadata($metadata);

    my $id = $sp_interface->uuid;
    $id =~ s/^([0-9])/_$1/;

    my $base = '/md:EntityDescriptor';

    {
        my $node = get_node($xp, $base);
        is($node->getAttribute('entityID'),
            'https://test.zaaksysteem.nl/auth/saml', "... and correct entityID");
        is($node->getAttribute('ID'), $id, "... and correct ID");
        is($node->getAttribute('xmlns:dsig'),
            URN_SIGNATURE, 'dsig namespace available')
            if $crt;

    }

    {
        my $node = get_node($xp, "$base/md:SPSSODescriptor");
        is($node->getAttribute('WantAssertionsSigned'),
            'true', "... with Assertions signed");
        is($node->getAttribute('AuthnRequestsSigned'),
            'true', "... and authnrequest signed");

        is($node->getAttribute('errorURL'),
            "https://test.zaaksysteem.nl/auth/saml/error", "... and correct error URI");
    }

    {
        my $node
            = get_node($xp, "$base/md:SPSSODescriptor/md:SingleLogoutService");
        is($node->getAttribute('Binding'),
            BINDING_HTTP_REDIRECT, "... with the correct binding");
        is($node->getAttribute('Location'),
            "https://test.zaaksysteem.nl/auth/saml/sls-redirect", "... and with the correct Location");
    }

    {
        my $xpath = "$base/md:SPSSODescriptor/md:AssertionConsumerService";
        my $node  = get_node($xp,
            $xpath . '[@Binding="' . BINDING_HTTP_ARTIFACT . '"]');
        is($node->getAttribute('index'),     1, "... correct index");
        is($node->getAttribute('isDefault'), "true", "... is the default");
        is(
            $node->getAttribute('Location'),
            "https://test.zaaksysteem.nl/auth/saml/consumer-artifact",
            "... and correct location"
        );
    }

    {
        my $xpath = "$base/md:SPSSODescriptor/md:AssertionConsumerService";
        my $node
            = get_node($xp, $xpath . '[@Binding="' . BINDING_HTTP_POST . '"]');
        is($node->getAttribute('index'),     2, "... correct index");
        is($node->getAttribute('isDefault'), "false", "... isnt the default");
        is($node->getAttribute('Location'),
            "https://test.zaaksysteem.nl/auth/saml/consumer-post", "... and correct location");
    }

    {
        # Org
        my $org  = "$base/md:Organization";
        my $node = get_node($xp, $org);

        my $name = get_node($xp, "$org/md:OrganizationName");
        is(
            $name->textContent,
            'Zaaksysteem testsuite',
            "... and correct org name"
        );
        my $display_name = get_node($xp, "$org/md:OrganizationDisplayName");
        is(
            $display_name->textContent,
            'Zaaksysteem testsuite',
            "... and correct display name"
        );
    }

    {
        # Contact
        my $contact = "$base/md:ContactPerson";
        my $node    = get_node($xp, "$contact");
        is($node->getAttribute('contactType'),
            'other', '... and correct contactType');

        my $company = get_node($xp, "$contact/md:Company");
        is(
            $company->textContent,
            "Zaaksysteem testsuite",
            "... and correct company name"
        );
        my $email = get_node($xp, "$contact/md:EmailAddress");
        is($email->textContent, 'devnull@example.com',
            "... and correct email");
    }

    {
        my $kd   = "$base/md:SPSSODescriptor/md:KeyDescriptor";
        my $node = get_node($xp, $kd);
        ok(!$node->getAttribute('use'), ".. and isn't just for signing");

        my $cert
            = get_node($xp, "$kd/ds:KeyInfo/ds:X509Data/ds:X509Certificate");
        my $name = get_node($xp, "$kd/ds:KeyInfo/ds:KeyName");

        if (!$crt) {
            is(
                $cert->textContent,
                "cert as string",
                "... certificate is correct"
            );
            is(
                $name->textContent,
                "e0ac5227596db90852cfa4eda6bcc622",
                "... keyname is correct"
            );
        }
        else {
            isnt(
                $cert->textContent,
                "cert as string",
                "... certificate is correct"
            );
            isnt(
                $name->textContent,
                "e0ac5227596db90852cfa4eda6bcc622",
                "... keyname is correct"
            );
        }
    }

    if ($crt) {
        my $xpath = "$base/dsig:Signature";
        my $sig   = get_node($xp, $xpath);
        get_node($xp,
            "$xpath/dsig:SignedInfo/dsig:Reference" . '[@URI="#Some ID"]');
    }

}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
