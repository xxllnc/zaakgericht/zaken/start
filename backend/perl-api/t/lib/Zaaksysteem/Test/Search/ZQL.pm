package Zaaksysteem::Test::Search::ZQL;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Search::ZQL - Test Zaaksysteem::Search::ZQL

=head1 DESCRIPTION

Tests for the ZQL query parser

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Search::ZQL

=cut

use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;

sub test_zql_simple {
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar');

    isa_ok($zql, 'Zaaksysteem::Search::ZQL');

    isa_ok($zql->cmd, 'Zaaksysteem::Search::ZQL::Command::Select', 'Command object');
    isa_ok($zql->cmd->from, 'Zaaksysteem::Search::ZQL::Literal::ObjectType', 'FROM object');
    is($zql->cmd->where, undef, 'Query without WHERE clause parsed properly');
}

sub test_zql_simple_with_where {
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = bar OR baz > 123 AND qux = "lala"');

    isa_ok($zql, 'Zaaksysteem::Search::ZQL');

    isa_ok($zql->cmd, 'Zaaksysteem::Search::ZQL::Command::Select', 'Command object');
    isa_ok($zql->cmd->from, 'Zaaksysteem::Search::ZQL::Literal::ObjectType', 'FROM object');
    
    is_deeply(
        $zql,
        bless( {
        'cmd' => bless( {
            'describe' => '',
            'distinct' => '',
            'from' => bless( {
            'value' => 'foobar'
            }, 'Zaaksysteem::Search::ZQL::Literal::ObjectType' ),
            'is_intake' => 0,
            'matching' => undef,
            'opts' => [],
            'probes' => [
            bless( {
                'value' => bless( do{\(my $o = '{}')}, 'JSON::Path' )
            }, 'Zaaksysteem::Search::ZQL::Literal::JPath' )
            ],
            'where' => bless( {
            'lterm' => bless( {
                'value' => 'foo'
            }, 'Zaaksysteem::Search::ZQL::Literal::Column' ),
            'operator' => bless( {
                'operator' => '='
            }, 'Zaaksysteem::Search::ZQL::Operator::Infix' ),
            'rterm' => bless( {
                'lterm' => bless( {
                'value' => 'bar'
                }, 'Zaaksysteem::Search::ZQL::Literal::Column' ),
                'operator' => bless( {
                'operator' => 'or'
                }, 'Zaaksysteem::Search::ZQL::Operator::Infix' ),
                'rterm' => bless( {
                'lterm' => bless( {
                    'value' => 'baz'
                }, 'Zaaksysteem::Search::ZQL::Literal::Column' ),
                'operator' => bless( {
                    'operator' => '>'
                }, 'Zaaksysteem::Search::ZQL::Operator::Infix' ),
                'rterm' => bless( {
                    'lterm' => bless( {
                    'value' => '123'
                    }, 'Zaaksysteem::Search::ZQL::Literal::Number' ),
                    'operator' => bless( {
                    'operator' => 'and'
                    }, 'Zaaksysteem::Search::ZQL::Operator::Infix' ),
                    'rterm' => bless( {
                    'lterm' => bless( {
                        'value' => 'qux'
                    }, 'Zaaksysteem::Search::ZQL::Literal::Column' ),
                    'operator' => bless( {
                        'operator' => '='
                    }, 'Zaaksysteem::Search::ZQL::Operator::Infix' ),
                    'rterm' => bless( {
                        'value' => 'lala'
                    }, 'Zaaksysteem::Search::ZQL::Literal::String' )
                    }, 'Zaaksysteem::Search::ZQL::Expression::Infix' )
                }, 'Zaaksysteem::Search::ZQL::Expression::Infix' )
                }, 'Zaaksysteem::Search::ZQL::Expression::Infix' )
            }, 'Zaaksysteem::Search::ZQL::Expression::Infix' )
            }, 'Zaaksysteem::Search::ZQL::Expression::Infix' )
        }, 'Zaaksysteem::Search::ZQL::Command::Select' ),
        'query' => 'SELECT {} FROM foobar WHERE foo = bar OR baz > 123 AND qux = "lala"'
        }, 'Zaaksysteem::Search::ZQL' ),
        "ZQL with multi-clause 'WHERE' got parsed correctly"
    );
}

sub test_zql_between {
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE date BETWEEN 2020-06-04T00:00:00Z AND 2020-06-05T00:00:00Z');

        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->term->value, 'date', 'between statement term parsed correctly');

        my $start = $zql->cmd->where->between_egt->value;
        my $end = $zql->cmd->where->between_elt->value;

        isa_ok($start, 'DateTime');
        isa_ok($end, 'DateTime');

        is($start->iso8601, '2020-06-04T00:00:00', 'between statement start date');
        is($end->iso8601, '2020-06-05T00:00:00', 'between statement end date');
        is($start->time_zone->name, 'UTC', 'between statement start date tz');
        is($end->time_zone->name, 'UTC', 'between statement end date tz');
    }
    {

        # DateTime->now becomes a fixed value so test don't suffer from a small
        # timing glitch.
        my $override = override(
            "DateTime::now" => sub {
                return DateTime->new(
                    year  => 1982,
                    month => 6,
                    day   => 5
                );
            }
        );

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE last_modified BETWEEN NOW() AND PAST("4 days")');

        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        my $start = $zql->cmd->where->between_egt;
        my $end = $zql->cmd->where->between_elt;

        isa_ok($start, 'Zaaksysteem::Search::ZQL::Literal::Function');
        isa_ok($end, 'Zaaksysteem::Search::ZQL::Literal::Function');

        is($start->value, 'NOW');
        is($end->value, 'PAST');
        is($end->arguments->value->[0]->value, '4 days');

        my $cnd = $zql->cmd->where->dbixify;

        is($cnd->operator, 'and');
        is($cnd->lterm->operator, '>=');
        is($cnd->rterm->operator, '<=');

        is($cnd->lterm->lterm->value, 'last_modified');
        is($cnd->rterm->lterm->value, 'last_modified');

        isa_ok($cnd->lterm->rterm->value, 'DateTime');
        isa_ok($cnd->rterm->rterm->value, 'DateTime');

        my $now = DateTime->now();

        is($cnd->lterm->rterm->value->iso8601, $now->iso8601);
        is($cnd->rterm->rterm->value->iso8601, $now->subtract(days => 4)->iso8601);
    }
}

sub test_zql_minus_negative {
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = 42-10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->lterm->value,       '42', "42-10 parsed correctly");
        is($zql->cmd->where->rterm->operator->operator, '-',  "42-10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->value,       '10', "42-10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = 42 - 10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->lterm->value,       '42', "42 - 10 parsed correctly");
        is($zql->cmd->where->rterm->operator->operator, '-',  "42 - 10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->value,       '10', "42 - 10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = 42 -10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->lterm->value,       '42', "42 -10 parsed correctly");
        is($zql->cmd->where->rterm->operator->operator, '-',  "42 -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->value,       '10', "42 -10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = 42--10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->lterm->value,       '42',  "42--10 parsed correctly");
        is($zql->cmd->where->rterm->operator->operator, '-',   "42--10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->value,       '-10', "42--10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = 5 + 20 + -10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->lterm->value,              '5',   "5 + 20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->operator->operator,        '+',   "5 + 20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->lterm->value,       '20',  "5 + 20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->operator->operator, '+',   "5 + 20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->rterm->value,       '-10', "5 + 20 + -10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = 5 - -20 + -10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->lterm->value,              '5',   "5 - -20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->operator->operator,        '-',   "5 - -20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->lterm->value,       '-20', "5 - -20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->operator->operator, '+',   "5 - -20 + -10 parsed correctly");
        is($zql->cmd->where->rterm->rterm->rterm->value,       '-10', "5 - -20 + -10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar WHERE foo = -10');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->where->rterm->value,       '-10',  "-10 parsed correctly");
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar MATCHING "\\\\"');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->matching->value, '\\', 'double backslash parses (becomes single backslash)');
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar MATCHING "\\\\\\\\"');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->matching->value, '\\\\', 'bare backslash parses');
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar MATCHING "\\""');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->matching->value, '"', 'escaped delimiter parses');
    }
    {
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar MATCHING ""');
        isa_ok($zql, 'Zaaksysteem::Search::ZQL');

        is($zql->cmd->matching->value, '', 'empty string parses');
    }
    {
        throws_ok {
            Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar MATCHING "\\\\\\"')
        } 'BTTW::Exception::Base', 'uneven backslashes throws exception';
    }
    {
        throws_ok {
            Zaaksysteem::Search::ZQL->new('SELECT {} FROM foobar MATCHING "\\"')
        } 'BTTW::Exception::Base', 'single backslash throws exception';
    }
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
