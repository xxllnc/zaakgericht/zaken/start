package Zaaksysteem::Test::Session::Invitation::Model;

use Zaaksysteem::Test;

use Zaaksysteem::Session::Invitation::Model;
use Zaaksysteem::Object::Reference::Instance;
use BTTW::Tools::RandomData qw(generate_uuid_v4);

use DateTime;

=head1 NAME

Zaaksysteem::Test::Session::Invitation::Model - Test session invitation
abstraction model

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Session::Invitation::Model

=cut

sub test_create_invitation {

    my $store = {};
    my $expire = {};
    my $redis = mock_one(
        'X-Mock-SelfArg' => 1,
        set_json => sub {
            my $self = shift;
            my $key = shift;
            my $value = shift;
            $store->{$key} = $value;
        },
        get_json => sub {
            my $self = shift;
            my $key = shift;
            return $store->{$key};
        },
        del => sub {
            my $self = shift;
            my $key = shift;
            delete $store->{$key};
            delete $expire->{$key};
        },
        expire_at => sub {
            my $self = shift;
            my $key  = shift;
            my $time = shift;
            $expire->{$key} = $time;
        }
    );

    my $model = Zaaksysteem::Session::Invitation::Model->new(
        redis => $redis
    );

    my %subject = (
        id => 1,
        uuid => generate_uuid_v4,
        display_name => "FOO",
    );

    my $subject = mock_one(
        TO_JSON => sub { return \%subject },
        _ref    => sub {
            return Zaaksysteem::Object::Reference::Instance->new(
                id      => $subject{uuid},
                type    => 'subject',
                preview => $subject{display_name}
            );
        }
    );

    my $date = DateTime->now->add(days => 1);
    my $invitation = $model->create(
        {
            subject      => $subject,
            date_expires => $date,
        }
    );
    isa_ok($invitation, "Zaaksysteem::Object::Types::Session::Invitation");

    is(keys %$store, 1, "Got one token stored in Redis");
    my ($key) = keys %$store;
    is(
        $key,
        "session:invite:" . $invitation->token,
        "... and is the token we have stored"
    );
    cmp_deeply(
        $store->{$key},
        { subject => \%subject },
        "... and correct information is stored"
    );

    cmp_deeply(
        $expire,
        { $key => $date->epoch },
        "... with the correct expiration date"
    );

    {
        $store = {};
        $expire = {};
        $date = DateTime->now->subtract(days => 2);

        my $invitation = $model->create(
            {
                subject      => $subject,
                object       => Zaaksysteem::Object::Reference::Instance->new(
                    id => generate_uuid_v4,
                    type => 'file',
                    preview => 'Document watcher support',
                ),
                action_path => '/path/to/action',
                date_expires => $date,
            }
        );
        isa_ok($invitation, "Zaaksysteem::Object::Types::Session::Invitation");

        is(keys %$store, 1, "Got one token stored in Redis");
        my ($key) = keys %$store;
        cmp_deeply(
            $store->{$key},
            {
                subject => \%subject,
                object  => {
                    id      => ignore(),
                    type    => 'file',
                    preview => 'Document watcher support',
                },
                action_path => '/path/to/action',
            },
            "... and correct information is stored"
        );

        cmp_deeply(
            $expire,
            { $key => $date->epoch },
            "... with the correct expiration date"
        );

        my $invite = $model->validate($invitation->token);
        isa_ok($invite, "Zaaksysteem::Object::Types::Session::Invitation");
        cmp_deeply($invite, $invitation, "We got our invite");

        cmp_deeply($store, {},
            "... validate means not stored in Redis anymore");


    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
