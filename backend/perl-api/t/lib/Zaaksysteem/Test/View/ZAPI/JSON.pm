package Zaaksysteem::Test::View::ZAPI::JSON;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

use Zaaksysteem::View::ZAPI::JSON;

sub test_functions {
    my $view      = Zaaksysteem::View::ZAPI::JSON->new();
    my @functions = qw(
        encoder
        encode
        process
        encode_json
        _prepare_results
        _strip_sensitive_data
    );

    foreach (@functions) {
        can_ok($view, $_);
    }
}

sub test_strip_sensitive_fields {

    my $view = Zaaksysteem::View::ZAPI::JSON->new();

    my $user_perm = 0;
    my $catalyst  = mock_one(
        has_legacy_permission => sub { return $user_perm; },
        stash               => sub {
            return {
                zapi_hide_mappings => {
                    'attribute.secret'   => 4,
                    'case.requestor.bsn' => 4
                }
            };
        }
    );

    my $catalyst_no_user  = mock_one(
        user        => sub { return; },
        user_exists => sub { return 0; },
        stash       => sub {
            return {
                zapi_hide_mappings => {
                    'attribute.secret'   => 4,
                    'case.requestor.bsn' => 4
                }
            };
        }
    );

    my $data = {
        object_type => 'case',
        values      => {
            'attribute.foo'      => 'bar',
            'attribute.secret'   => 'area51',
            'case.requestor.bsn' => 'something',
        }
    };

    my %copy = %$data;
    $copy{values}{'case.requestor.bsn'} = "Geen rechten";
    $copy{values}{'attribute.secret'} = "Geen rechten";

    my $rv = $view->_strip_sensitive_data($catalyst, $data);
    cmp_deeply($rv, \%copy, "Stripped secret fields");

    $rv = $view->_strip_sensitive_data($catalyst_no_user, $data);
    cmp_deeply($rv, \%copy, "Stripped secret fields when no user is logged in");

    $user_perm = 1;
    $rv = $view->_strip_sensitive_data($catalyst, $data);
    cmp_deeply($rv, $data, "Did not strip secret fields");

    $data->{object_type} = 'foo';
    $rv = $view->_strip_sensitive_data($catalyst, $data);
    cmp_deeply($rv, $data,
        "Did not strip secret fields: object type not case");

    $catalyst = mock_one(
        has_legacy_permission => 0,
        stash               => sub { return {}; }
    );
    $data->{object_type} = 'case';
    $rv = $view->_strip_sensitive_data($catalyst, $data);
    cmp_deeply($rv, $data,
        "Did not strip secret fields: we don't have anything to hide");

}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::View::ZAPI::JSON - Test ZS::View::ZAPI::JSONN

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::View::ZAPI::JSON

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
