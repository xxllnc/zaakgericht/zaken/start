package Zaaksysteem::Test::XML::RIP::Instance;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use feature qw(state);

use Zaaksysteem::Test;
use Zaaksysteem::XML::Compile;
use BTTW::Tools::RandomData qw(generate_uuid_v4);

sub test_rip_creation {

    my $rip = Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::RIP::Instance'
    )->rip;

    isa_ok($rip, "Zaaksysteem::XML::RIP::Instance");

    my $record = _create_record();

    my %data = (
        packageHeader => {
            identificatie => generate_uuid_v4(),
            datum         => '2010-10-10',
        },
        record => [ $record ],
    );

    _test_rip_creation($rip, \%data, "Minimal XML created for RIP");

    my $xml = _create_topx_records();
    my $md  = _xml_to_any($xml);

    $record = _create_record($md);

    _test_rip_creation($rip, \%data, "TopX included in metadata for RIP");

    my @records;
    foreach (1..5) {

        my $xml = _create_topx_records();
        my $md  = _xml_to_any($xml);
        push(@records, _create_record($md));
    }

    $data{record} = \@records;
    _test_rip_creation($rip, \%data,
        "Multiple TopX included in metadata for RIP");


}

sub _create_record {
    my $md = shift;

    return {
        recordHeader => {
            identificatie => generate_uuid_v4(),
            status        => 'nieuw',
        },
        metadata => $md ? $md : { schemaURI => 'https://example.com/foo.xsd' },
    };

}

sub _xml_to_any {
    my $xml = shift;

    my $dom = XML::LibXML->load_xml(string => $xml);

    my $root      = $dom->documentElement();
    my $localname = $root->localname;
    my $ns        = $root->namespaceURI();

    my %metadata = (
        schemaURI => $ns,
        "{$ns}$localname" => $root,
    );
    return \%metadata;
}

sub _create_topx_records {
    state $topx = Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::TopX::Instance')->topx;

    state $id = 1;

    my %topx_data = (
        aggregatie => {
            identificatiekenmerk => $id++,
            aggregatieniveau     => 'Record',
            naam                 => 'Foo',
            classificatie => [
                {
                    code         => 'my code',
                    omschrijving => 'my desription',
                    bron         => 'my source',
                },
                {
                    code         => 'my code 2',
                    omschrijving => 'my desription 2',
                    bron         => 'my source 2',
                }
            ]
        },
    );

    return $topx->tmlo_export('writer', \%topx_data);
}

sub _test_rip_creation {
    my ($rip, $data, $msg) = @_;

    lives_ok(
        sub {
            my $xml = $rip->tmlo_export('writer', $data);
            note $xml;
        },
        $msg
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::XML::RIP::Instance - Test ZS::Test::XML::RIP::Instance

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::XML::RIP::Instance

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
