# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pathlib import Path
from setuptools import setup

local_path_minty: str = (Path(__file__).parent.parent / "minty").as_uri()
local_path_infra_amqp: str = (
    Path(__file__).parent.parent / "minty-infra-amqp"
).as_uri()
local_path_infra_misc: str = (
    Path(__file__).parent.parent / "minty-infra-misc"
).as_uri()

setup(
    install_requires=[
        "pause",
        "python-json-logger ~= 2.0",
        f"minty @ {local_path_minty}",
        f"minty-infra-amqp @ {local_path_infra_amqp}",
        f"minty-infra-misc @ {local_path_infra_misc}",
    ]
)
