# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import json
import minty.cqrs
from timed_events import __main__ as timed_events
from typing import Any
from unittest import TestCase, mock


class MockInfraFactory:
    def __init__(
        self, instance_hostnames: list[str], infrastructures: dict[str, Any]
    ):
        self.infrastructures = infrastructures
        self.instance_hostnames = instance_hostnames

    def get_configured_instance_hostnames(self):
        return self.instance_hostnames

    def get_infrastructure(self, context: str, infrastructure_name: str):
        return self.infrastructures[infrastructure_name]

    def get_config(self, hostname: str):
        return {"instance_uuid": "test_instance", "timezone": "UTC"}


class TestTimedEvents(TestCase):
    def test_send_events(self):
        mock_amqp = mock.Mock()
        mock_redis = mock.Mock()
        mock_redis.set.return_value = 1

        infra_factory = MockInfraFactory(
            instance_hostnames=["host1", "host2"],
            infrastructures={
                "amqp": mock_amqp,
                "redis": mock_redis,
            },
        )

        timestamp = datetime.datetime.now(tz=datetime.timezone.utc).replace(
            hour=0, minute=0, second=0, microsecond=0
        )

        with mock.patch("amqpstorm.Message") as message:
            timed_events.send_events(
                infra_factory=infra_factory,  # type: ignore
                publish_to_exchange="test_exchange",
                timestamp=timestamp,
            )

        amqp_calls = message.create.call_args_list

        self.assertEqual(len(amqp_calls), 4, "Number of AMQP messages")

        self.assertEqual(
            amqp_calls[0][1]["channel"], mock_amqp, "Correct AMQP channel used"
        )
        self.assertEqual(
            json.loads(amqp_calls[0][1]["body"]),
            {
                "id": mock.ANY,
                "correlation_id": mock.ANY,
                "changes": [],
                "context": "host1",
                "created_date": mock.ANY,
                "domain": "Time",
                "entity_type": "Clock",
                "event_name": "HourPassed",
                "entity_id": str(timed_events.CLOCK_ENTITY_ID),
                "entity_data": {
                    "timestamp": timestamp.isoformat(),
                    "local_timestamp": timestamp.isoformat(),
                },
                "user_info": {
                    "type": "UserInfo",
                    "permissions": {},
                    "user_uuid": str(minty.cqrs.SYSTEM_USER_UUID),
                },
                "user_uuid": str(minty.cqrs.SYSTEM_USER_UUID),
            },
            "Event body",
        )
        self.assertEqual(
            amqp_calls[0][1]["properties"],
            {"content_type": "application/json"},
            "Content type correctly set",
        )

        self.assertEqual(
            amqp_calls[1][1]["channel"], mock_amqp, "Correct AMQP channel used"
        )
        self.assertEqual(
            json.loads(amqp_calls[1][1]["body"]),
            {
                "id": mock.ANY,
                "correlation_id": mock.ANY,
                "changes": [],
                "context": "host1",
                "created_date": mock.ANY,
                "domain": "Time",
                "entity_type": "Clock",
                "event_name": "DayPassed",
                "entity_id": str(timed_events.CLOCK_ENTITY_ID),
                "entity_data": {
                    "timestamp": timestamp.isoformat(),
                    "local_timestamp": timestamp.isoformat(),
                },
                "user_info": {
                    "type": "UserInfo",
                    "permissions": {},
                    "user_uuid": str(minty.cqrs.SYSTEM_USER_UUID),
                },
                "user_uuid": str(minty.cqrs.SYSTEM_USER_UUID),
            },
            "Event body",
        )
        self.assertEqual(
            amqp_calls[1][1]["properties"],
            {"content_type": "application/json"},
            "Content type correctly set",
        )

    def test_send_events_not_midnight(self):
        mock_amqp = mock.Mock()
        mock_redis = mock.Mock()
        mock_redis.set.return_value = 1

        infra_factory = MockInfraFactory(
            instance_hostnames=["host1", "host2"],
            infrastructures={
                "amqp": mock_amqp,
                "redis": mock_redis,
            },
        )

        timestamp = datetime.datetime.now(tz=datetime.timezone.utc).replace(
            hour=12, minute=0, second=0, microsecond=0
        )

        with mock.patch("amqpstorm.Message") as message:
            timed_events.send_events(
                infra_factory=infra_factory,  # type: ignore
                publish_to_exchange="test_exchange",
                timestamp=timestamp,
            )

        amqp_calls = message.create.call_args_list

        self.assertEqual(len(amqp_calls), 2, "Number of AMQP messages")

    def test_send_events_already_sent(self):
        mock_amqp = mock.Mock()
        mock_redis = mock.Mock()
        mock_redis.set.return_value = False

        infra_factory = MockInfraFactory(
            instance_hostnames=["host1", "host2"],
            infrastructures={
                "amqp": mock_amqp,
                "redis": mock_redis,
            },
        )

        timestamp = datetime.datetime.now(tz=datetime.timezone.utc).replace(
            hour=0, minute=0, second=0, microsecond=0
        )

        with mock.patch("amqpstorm.Message") as message:
            timed_events.send_events(
                infra_factory=infra_factory,  # type: ignore
                publish_to_exchange="test_exchange",
                timestamp=timestamp,
            )

        message.create.assert_not_called()
