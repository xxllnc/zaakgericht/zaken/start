# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import amqpstorm
import datetime
import logging
import logging.config
import minty.cqrs
import pause
import redis
import zoneinfo
from minty.infrastructure import InfrastructureFactory
from minty_infra_amqp import AMQPInfrastructure
from minty_infra_misc import RedisInfrastructure
from typing import Final, NoReturn
from uuid import UUID

CLOCK_ENTITY_ID: Final = UUID("2bed73bd-207c-4a40-82c0-cbdbe6073e45")
HOUR_EVENT_NAME: Final = "HourPassed"
DAY_EVENT_NAME: Final = "DayPassed"

AMQP_INFRA_NAME: Final = "amqp"
REDIS_INFRA_NAME: Final = "redis"

logger = logging.getLogger(__name__)


def main() -> (
    NoReturn
):  # pragma: no cover (cannot be tested because of infinite loop)
    logger.info("Starting timed-event service")
    infra_factory = InfrastructureFactory(config_file="config.conf")

    # AMQP infra, used to send events
    infra_factory.register_infrastructure(
        AMQP_INFRA_NAME, AMQPInfrastructure()
    )

    # Redis, used to store when an event was sent last (so running the script
    # is idempotent -- it won't send double events if k8s somehow starts
    # the job twice)
    infra_factory.register_infrastructure(
        REDIS_INFRA_NAME, RedisInfrastructure()
    )

    global_config = infra_factory.get_config(None)
    publish_to_exchange = global_config["amqp"]["publish_settings"]["exchange"]

    while True:
        # Attempt sending the event for the current hour first, so any missed
        # events will be broadcast after restart.
        # This will do nothing if Redis indicates that the event has already
        # been sent for that environment.

        now = datetime.datetime.now(tz=datetime.timezone.utc)
        this_hour = now.replace(
            minute=0,
            second=0,
            microsecond=0,
        )

        send_events(
            infra_factory=infra_factory,
            publish_to_exchange=publish_to_exchange,
            timestamp=this_hour,
        )

        next_hour = this_hour + datetime.timedelta(hours=1)
        pause.until(next_hour)


def create_event(
    event_name: str,
    hostname: str,
    timestamp: datetime.datetime,
    local_timestamp: datetime.datetime,
) -> minty.cqrs.Event:
    return minty.cqrs.Event.create_basic(
        domain="Time",
        context=hostname,
        entity_type="Clock",
        entity_id=CLOCK_ENTITY_ID,
        event_name=event_name,
        entity_data={
            "timestamp": timestamp.isoformat(),
            "local_timestamp": local_timestamp.isoformat(),
        },
        user_info=minty.cqrs.SYSTEM_USERINFO,
    )


def send_events(
    infra_factory: InfrastructureFactory,
    publish_to_exchange: str,
    timestamp: datetime.datetime,
) -> None:
    # Get list of contexts from Redis
    #
    # For each context:
    #  - Redis set-if-notexists "event sent" key (ttl = half interval?)
    #  - Send event (eventname)

    instances = infra_factory.get_configured_instance_hostnames()
    amqp_channel: amqpstorm.Channel = infra_factory.get_infrastructure(
        context=None,
        infrastructure_name=AMQP_INFRA_NAME,
    )
    redis_handle: redis.Redis = infra_factory.get_infrastructure(
        context=None,
        infrastructure_name=REDIS_INFRA_NAME,
    )

    redis_handle.ping()

    for instance_hostname in instances:
        logger.info(f"Sending event to {instance_hostname}")

        config = infra_factory.get_config(instance_hostname)

        instance_tz: str = config.get("timezone", "Europe/Amsterdam")
        local_timestamp = timestamp.astimezone(zoneinfo.ZoneInfo(instance_tz))

        instance_uuid = config["instance_uuid"]

        events: list[minty.cqrs.Event] = []

        res = redis_handle.set(
            f"{instance_uuid}:timed_events:{HOUR_EVENT_NAME}:hour:{timestamp.hour}:sent",
            "1",
            nx=True,
            ex=3600,  # TTL of hour so an event is sent max. once per hour
        )

        if res:
            logger.debug(f" - Sending {HOUR_EVENT_NAME} for {timestamp.hour}")

            events.append(
                create_event(
                    HOUR_EVENT_NAME,
                    instance_hostname,
                    timestamp,
                    local_timestamp,
                )
            )
        else:
            logger.debug(
                f" - Not sending {HOUR_EVENT_NAME} for {timestamp.hour} (re-run?)"
            )

        if local_timestamp.hour == 0:
            res = redis_handle.set(
                f"{instance_uuid}:timed_events:{DAY_EVENT_NAME}:hour:{timestamp.day}:sent",
                "1",
                nx=True,
                ex=86400,  # TTL of 24 hours so an event is sent max. once per day
            )
            if res:
                # Start of new day
                logger.debug(
                    f" - Sending {DAY_EVENT_NAME} for {timestamp.day}"
                )

                events.append(
                    create_event(
                        DAY_EVENT_NAME,
                        instance_hostname,
                        timestamp,
                        local_timestamp,
                    )
                )
            else:
                logger.debug(
                    f" - Not sending {DAY_EVENT_NAME} for {timestamp.day} (re-run?)"
                )

        for event in events:
            message = amqpstorm.Message.create(
                channel=amqp_channel,
                body=event.as_json(),
                properties={"content_type": "application/json"},
            )
            message.publish(
                routing_key=event.routing_key(),
                exchange=publish_to_exchange,
            )


if __name__ == "__main__":  # pragma: no cover
    logging.config.fileConfig("logging.conf")

    main()
