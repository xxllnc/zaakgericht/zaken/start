# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import codecs
import json
from minty.exceptions import Forbidden, NotFound


def _get_session_data_from_redis(request):
    redis = request.infrastructure_factory.get_infrastructure(
        context=None, infrastructure_name="redis"
    )
    session_invitation_token = request.headers.get("ZS-Auth-Token")
    instance = request.configuration["instance_uuid"]
    redis_key = f"{instance}:session:invite:{session_invitation_token}"
    return redis.get(redis_key)


def _assert_session_invitation(request) -> None:
    session_invitation_token = request.headers.get("ZS-Auth-Token")
    if not session_invitation_token:
        raise KeyError
    else:
        redis_data = _get_session_data_from_redis(request)
        if redis_data:
            return

    raise Forbidden(
        "The supplied session invitation token does not exists",
        "session_invitation_token/not_found",
    )


def _get_session_invitation_user_info(request) -> dict:
    session_data_raw = _get_session_data_from_redis(request)
    if not session_data_raw:
        raise NotFound(
            "No session invitation found", "session_invitation/not_found"
        )
    session_data_decoded = codecs.decode(session_data_raw, "base64")
    subject = json.loads(session_data_decoded)["subject"]

    user_info = {}
    user_info["user_uuid"] = subject["uuid"]
    user_info["permissions"] = subject["permissions"]

    return user_info


def includeme(config) -> None:
    config.add_request_method(
        _get_session_invitation_user_info, "get_session_invitation_user_info"
    )
    config.add_request_method(
        _assert_session_invitation, "assert_session_invitation"
    )
