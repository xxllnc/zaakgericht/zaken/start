#!/usr/bin/env perl

use warnings;
use strict;

use Module::CPANfile;

my $cpanfile = Cwd::abs_path('cpanfile');

my $file = Module::CPANfile->load;
my $prereqs = $file->prereqs;
$file->merge_meta('MYMETA.json');
