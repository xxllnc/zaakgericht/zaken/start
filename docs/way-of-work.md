# Way of work

When coding on our different projects, there is a certain etiquette we bound
to. It makes things clear for everyone. To keep it short, below a listing and
examples of our way of working.

## In family we trust

There is a lot of trust in each other when coding on our projects. We believe
we selected our teammates based on a culture in which they always thrive to
build the best solution possible. We do not need to build checks to see if a
teammate made any shortcuts he/she should not do. And even when this happens,
we trust in a system of feedback to prevent this from happening in the future.

We will see what we mean by this in the process below.

## Our process

We follow a process in which we believe makes the best software. This document
aims to give technical information, so we assume every story and bug we are
talking about below is already refined and clear enough to work with. It must
be functionally clear what exactly the question or problem is that needs to be
solved. If it is not clear from a functional point of view what exactly is
being asked, then no proper technical plan can be made.

1. **Technical plan**

    Before we start coding, we make sure we have a technical understanding of
    the feature and bug, by taking the time to discuss any thought of solutions
    with my coworker. This prevents me from writing solutions which may already
    exist somewhere in the codebase.

2. **Coding**

    We use our `start` repository when coding. Depending on the type of bug or
    feature, start from the correct branch. For bugfixes on production, make
    sure you branch of `production` or `preprod`, but in any other situation
    you better branch of `master`.

    We start by checking out the `master`, `preprod` or `production` branch,
    so your local working directory is updated to match the selected branch.

    example:

    ```bash
    git checkout master
    git pull
    ```

    Create a new local branch and swith to the new local branch
    (`git checkout -b [new branch name]`). The new branch name uses the format
    `[gitlabusername]/MINTY-[NR]-description`, where:

    - `[gitlabusername]` is the author's name (the gitlab username)
    - `MINTY-[NR]` is the corresponding (unique) issue tracker ID: the MINTY
      JIRA ticket number
    - `description` being the summary with special characters removed,
      whitespaces collapsed and replaced with an underscore, and the whole set
      to lowercase

    `git checkout -b [gitlabusername]/MINTY-[NR]-description`

    example:

    `git checkout -b michielootjers/MINTY-1234-feat-add_document_to_a_case`

    Whenever you feel confident enough that your code is ready and all your
    changes are saved to the local branch, it is time to open a merge request.

    Use the `git status` command to display the state of your local branch and
    use the `git commit -m "MINTY-[NR]: Commit message."` to record the changes
    in your local branch. The commit should always start with the JIRA ticket
    number and have a solid commmit message describing the change you've made
    to the software.

    First push the local branch to the remote repository.

    `git push -u origin [gitlabusername]/MINTY-[NR]-description`

    example:

    `git push -u origin michielootjers/MINTY-1234-feat-add_document_to_a_case`


    Git prompts you with a direct link for creating a merge request. Copy the
    link.

3. **Testing**

    Please feel free to send the changes to our `development` branch. It
    automatically deploys to our [development.zaaksysteem.nl](https://development.zaaksysteem.nl)
    environment.

    example:

    ```bash
    git checkout development
    git pull
    git merge michielootjers/MINTY-1234-feat-add_document_to_a_case
    git push
    ```

    We move our `MINTY-1234` ticket in JIRA to the next lane, our Test lane, so
    our test team gets informed and tests the changes and acceptance criteria
    in your JIRA ticket.

4. **To Merge**

    Assuming everything went well, it is time to bring your ticket to the
    branch you started with (either `production`, `preprod` or `master` from
    the previous examples).

    Make sure your branch is clean. Paste the link (from Testing) in your
    browser and use the "Create merge request" button to do just that. The
    title of the merge request must be the same as the branch name. Preferably
    are all changes squashed in one commit. After that, please make a public
    merge request to the chosen branch and ask (a teammate) for a code review.

    When a teammate starts reviewing your code, they will point out all the
    hints and tips they can give you to improve your code. It is up to you to
    learn from it and to improve your code according to this feedback. The
    reviewer will only "showstop" your code review when he/she sees an obvious
    bug or security incident or something which will really break the system.
    In any other case the reviewer will accept your merge request by :thumbsup:
    your review. This way we also comply to the 4-eyes principle in most formal
    audits this system goes through.

    All merge requests should have approval of at least one teammate who is not
    the original author before merging.
