# E2E tests

## Getting started

### Playwright setup

To run the e2e tests locally the playwright packages must to be installed and a settings.json file must to be created.

Follow these steps to start testing

> Make sure [nodejs 16 or higher with npm](https://nodejs.org/en/download/current), [npx](https://www.npmjs.com/package/npx) and [yarn](https://classic.yarnpkg.com/lang/en/docs/install) ([how_to](https://www.hostinger.com/tutorials/how-to-install-yarn)) are installed

Go to the e2etests directory and install the packages from within that directory:

```yarn
cd e2etests
yarn install --frozen-lockfile
```

To install the webdrivers run

```bash
npx playwright install chromium firefox webkit 
```

In some cases it is necessary to restrart vs or to run

```bash
npx playwright install
```

### The settings.json file with environment data

Create a settings.json file:

```powershell
cp settings.example.json settings.json
```

Then edit the settings.json file and fill in the correct values.
In the settings.json file there are multiple environments defined.
These environments correspondent with the branch names in Gitlab. In the run in a terminal and in the GitLab pipeline the environment variable `CI_COMMIT_REF_SLUG` contains the branch name.

For more information about the settings.json file see the description in the file.

### The environment variable CI_COMMIT_REF_SLUG

Testing locally through the terminal, set the environment variable `CI_COMMIT_REF_SLUG` to run on a specific environment.
For example use `export CI_COMMIT_REF_SLUG=development` to test the development environment.

### The environment variable TESTS_TAG

Also set the environment variable TESTS_TAG to select certain tests. Possible options are: select, all, db, db1, db2. For example use `export TEST_TAG=@db` to run the tests with @db in the test title.

- **select** = a selection of the tests. In GitLab used for development and master code changes commits,
- **all** = all tests excluding the filling the db tests. In GitLab used for sceduled, changed e2etest code and for preprod commits. *NB it does not run the db filling tests!*,
- **db** or **db1/db2** = for testing filling an empty db. db1/db2 can be used when running headed from the playwright test extension or terminal in visual studio code. Running so and with TESTS_TAG=db, can make the system too slow.

### The vs-code testing extention

If you want to use the vs-code testing extension, you can add `"CI_COMMIT_REF_SLUG": "development"`in the .vscode settings.json file

``` json
{
  "playwright.env": {
    "CI_COMMIT_REF_SLUG": "development",
  }
}
```

Filtering on tags can only be done for certain playwright test tags. So testing here is usually only on a subdirectory or even a separate test file.

## New playwright version

### Update playwright

Playwright is actively maintained. So new versions are released often. If you want to update the whole project:

```yarn
yarn upgrade playwright
# or update all dependencies: 
yarn upgrade-interactive --latest
```

This updates both locally your project as well as the yarn.lock.

Be carefull that you don't push the changed yarn.lock file to Gitlab unless you also update the e2e.gitlab-ci.yml file. In Gitlab the Playwright Docker container should have the same version as the in your project to be installed Playwright version.

### New Playwright Docker container version for Gitlab

Updating through yarn updates the yarn.lock and packages.json files. In order for the test to run in GitLab with the same Playwright Docker container version, you also need to manually update and commit the gitlab-ci.yml file and the dockerfile. You can find the latest available Playwright version on <https://mcr.microsoft.com/en-us/catalog?search=playwright>.

As the latest playwright version most time is ahead of the docker version, you might want to set Playwright to a specific Playwright version:

``` yarn
yarn upgrade @playwright/test@<version>
```

## Running tests

### Run tests from shell

Open a shell (`cd to e2etests dir`) and run the following command from the e2etest directory to start
the tests locally:

```npx
export CI_COMMIT_REF_SLUG=dev;
export TESTS_TAG=@all;
npx playwright test -c playwright.config.ts * --headed --grep $TESTS_TAG --project=setup --project='e2e tests on chromium' --repeat-each 1 --workers 1
```

If you want to test more quickly, add `--workers 2`. Do not do so when testing the filling of the db as these tests need to be run in a specific order.

### Run tests from vs-code

#### Run tests through the playwright extension

Make sure the playwright extension is installed. It is added as a recommended extension in this project.

Open the test explorer in vs-code and start a test by clicking on the play button in the Testing extension.

If no tests are shown, try the reload button on top. Sometimes vs-code need to be restarted.
  
### Run tests from e2etests docker container

You can run the end to end tests locally by running the following `docker compose` command

from the root of the `start` repository directly:

```bash
export CI_COMMIT_REF_SLUG=dev;
export TESTS_TAG=select;
docker compose run --rm e2etests
```

**NB** Make sure the settings.json file is created first!

**NB 2** Running the docker container overwrites authorization of the test-reports and test-screenshots directories. If you want to run in a different way again, you may need to reset owner rights. 

## Run locally

### Running the right branch as dev.zaaksysteem.nl locally

In order to test the filling of an empty db, run the system locally:

1. Create a `start/.env` file. Mark/unmark the lines depending on the branch you wish to test locally:

```bash
COMPOSE_FILE=docker-compose.yml:./etc/docker/frontend-image.yml:./etc/docker/backend-image.yml:./etc/docker/helpers-off.yml:./etc/docker/auth-off.yml:docker-compose.override.yml
FRONTEND_MONO_IMAGE_TESTS_TAG=development-latest
CONSUMER_CM_IMAGE_TESTS_TAG=development-latest
CONSUMER_COMMUNICATION_IMAGE_TESTS_TAG=development-latest
CONSUMER_LOGGING_IMAGE_TESTS_TAG=development-latest
HTTP_ADMIN_IMAGE_TESTS_TAG=development-latest
HTTP_CM_IMAGE_TESTS_TAG=development-latest
HTTP_COMMUNICATION_IMAGE_TESTS_TAG=development-latest
HTTP_DOCUMENT_IMAGE_TESTS_TAG=development-latest
HTTP_STYLE_IMAGE_TESTS_TAG=development-latest
# FRONTEND_MONO_IMAGE_TESTS_TAG=preprod-latest
# CONSUMER_CM_IMAGE_TESTS_TAG=preprod-latest
# CONSUMER_COMMUNICATION_IMAGE_TESTS_TAG=preprod-latest
# CONSUMER_LOGGING_IMAGE_TESTS_TAG=preprod-latest
# HTTP_ADMIN_IMAGE_TESTS_TAG=preprod-latest
# HTTP_CM_IMAGE_TESTS_TAG=preprod-latest
# HTTP_COMMUNICATION_IMAGE_TESTS_TAG=preprod-latest
# HTTP_DOCUMENT_IMAGE_TESTS_TAG=preprod-latest
# HTTP_STYLE_IMAGE_TESTS_TAG=preprod-latest
```

1. Create a docker-compose-override file in the root:

```bash
services:
  database:
    ports:
      - "5432:5432"
```

1. Open the right branch in vs-code, pull latest

1. Run in a shell for the development branch:

    ```bash
    export ZS_DOCKER_START=0;./bin/zs zaaksysteem sprint
    ```

   For the other branches:

    ```bash
    docker-compose up --build -d
    ```

### Resetting the db after a run

#### In a shell

``` bash
docker-compose stop database; 
docker-compose rm --force database;
docker volume rm start_dbdata15;
docker-compose up -d database
```

#### In a vs-code task

``` json
...
    {
      "label": "Reset db",
      "type": "shell",
      "command": "docker-compose stop database; docker-compose rm --force database; docker volume rm start_dbdata15; docker-compose up -d database",
      "group": "build",
      "options": {
        "cwd": "${workspaceFolder}/start"
      },
      "problemMatcher": []
    },
...
```

**NB** the easiest way to run user tasks, is to add a shortcut to the `tasks: run task` in /File/Preferences/Keyboard Shortcuts. For instance `Alt + Z + S`

### Nice to have

Running tests in the terminal can also be done through a vs-code task, that first asks for which environment and then for which tag:

```json
...
    {
      "label": "Run e2e tests in docker container",
      "type": "shell",
      "command": "read -p 'Welke omgeving [dev]: ' CI_COMMIT_REF_SLUG;echo 'env is: $CI_COMMIT_REF_SLUG';read -p 'Welke tag [@db1]: ' TESTS_TAG;export TESTS_TAG={$TESTS_TAG:-@db1};export CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG:-dev};echo Env: $CI_COMMIT_REF_SLUG, tag: $TESTS_TAG;docker compose run --rm e2etests",
      "group": "build",
      "options": {
        "cwd": "${workspaceFolder}/start"
      },
      "problemMatcher": []
    },
...
```
