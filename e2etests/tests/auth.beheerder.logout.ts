import { expect, test as logout } from '@playwright/test';
import { testSettings } from '../playwright.config';
import { locators } from '../utils';

logout('Logout beheerder', async ({ page, context }) => {
  const { baseUrl, longTimeout } = testSettings;

  await page.goto(baseUrl);
  await page.waitForLoadState('domcontentloaded');

  await expect(
    page.locator(locators.partialTextLocator('Mijn openstaande zaken'))
  ).toBeVisible({ timeout: longTimeout });

  await page.locator('[aria-label="Hoofdmenu openen"]').click();
  await page.getByRole('menuitem', { name: ' Uitloggen' }).click();

  const urlAuthLogout = new RegExp(
    // eslint-disable-next-line no-useless-escape
    `${baseUrl}auth\/(?:page|login(?:\\?referer.+)?)`
  );
  await expect(page).toHaveURL(urlAuthLogout);

  await context.close();
});
