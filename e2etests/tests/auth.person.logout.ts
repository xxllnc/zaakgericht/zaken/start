import { expect, test as logout } from '@playwright/test';
import { testSettings } from '../playwright.config';

logout('Logout person from pip', async ({ page, context }) => {
  const { baseUrl } = testSettings;

  await page.goto(`${baseUrl}pip`);
  await expect(page.getByRole('link', { name: 'Uitloggen' })).toBeVisible();

  await page.getByRole('link', { name: 'Uitloggen' }).click();
  await expect(
    page.getByRole('heading', { name: 'Inloggen als persoon' })
  ).toBeVisible();

  await page.getByRole('heading', { name: 'Inloggen als persoon' }).click();

  await context.close();
});
