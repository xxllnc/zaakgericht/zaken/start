import { expect, test as setup } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../playwright.config';
import { isUrlOpened } from '../utils';

const { baseUrl, beheerderUser, beheerderPassword, pageTitle, longTimeout } =
  testSettings;

setup('authenticate beheerder', async ({ page }) => {
  await page.goto(baseUrl);
  await isUrlOpened(page, pageTitle);

  await expect(page.getByPlaceholder('Gebruikersnaam')).toBeVisible({
    timeout: longTimeout,
  });
  let notLoggedIn = true;
  let times = 1;
  while (notLoggedIn && times < 3) {
    times++;
    await page.getByPlaceholder('Gebruikersnaam').fill(beheerderUser);
    await page.getByPlaceholder('Wachtwoord').fill(beheerderPassword);
    await page.getByRole('button', { name: 'Inloggen' }).click();
    notLoggedIn = await page.getByText(/Incorrecte/).isVisible();
  }
  await page.waitForURL(/intern/);

  await page.context().storageState({ path: authPathBeheerder });
});
