/* eslint-disable playwright/expect-expect */
/* eslint-disable playwright/no-conditional-in-test */
import { expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import {
  addInternalNote,
  addPersonContact,
  openFoundPersonAndCheckSetSup,
} from '../zs-employee/searchAddPersonContact';
import {
  BSN3,
  BSN2,
  BSN1,
  generateApplicantName,
  personDataEnvironment,
  generateApplicantNameToClick,
} from '../../testfiles';
import { locators } from '../../utils';

const { shortTimeout } = testSettings;
let isFound = false;

test.describe
  .serial(`Search and add person contacts, necessary for empty db @all @select @db1`, () => {
  test.use({ storageState: authPathBeheerder });
  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  test(`BSN2: Search dashboard quick search. When not found, add contact ${BSN1}`, async ({
    page,
  }) => {
    const BSN = BSN2;
    const contactNameToClick = generateApplicantNameToClick({
      BSN,
      withBirthdate: false,
    });
    await test.step('Search through the dashboard rightcorner searchfield', async () => {
      const contactName = generateApplicantName(BSN);
      await page.waitForLoadState('domcontentloaded');
      await page.getByPlaceholder('Zoeken in xxllnc zaken').click();
      await page.getByLabel('object-type-select menu openen').click();
      await page.getByLabel('Contacten').click();
      await page.getByPlaceholder('Zoeken in xxllnc zaken').fill(contactName);
      await page.waitForResponse(res => res.status() === 200);
      await expect(
        page.getByText(
          new RegExp(
            'Geen resultaten gevonden voor deze zoekopdracht|' +
              contactNameToClick
          )
        )
      ).toBeVisible();
      const countFound = await page.getByText(contactNameToClick).count();
      isFound = countFound > 0;
    });

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (!isFound) {
      await test.step(`Add new contact BSN ${BSN}`, async () => {
        await page.getByLabel('Terug').click();
        await addPersonContact({
          page,
          BSN: BSN,
        });
      });

      await test.step(`Set the note new contact BSN ${BSN}`, async () => {
        await addInternalNote({ page, BSN });
      });
    }

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (isFound) {
      await test.step(`Open and check/set supplementary data contact BSN ${BSN}`, async () => {
        await openFoundPersonAndCheckSetSup({
          page,
          BSN: BSN,
        });
      });
    }

    await test.step(`Return to main screen for contact BSN ${BSN}`, async () => {
      await page.getByRole('link', { name: 'Dashboard Dashboard' }).click();
    });
  });

  test(`BSN3: Search by BSN. When not found, add contact BSN ${BSN2}`, async ({
    page,
  }) => {
    const BSN = BSN3;
    isFound = false;
    await test.step('Search for BSN in contact search for BSN ${BSN2}', async () => {
      await page.getByLabel('Hoofdmenu openen').click();
      await page.getByRole('menuitem', { name: ' Contact zoeken' }).click();

      await page.locator('input[name="bsn"]').fill(BSN);
      await page.getByRole('button', { name: 'Zoeken' }).click();
      await page.waitForResponse(res => res.status() === 200);
      try {
        await expect(page.getByText('Geen resultaten gevonden.')).toBeVisible({
          timeout: shortTimeout,
        });
        await page
          .getByRole('link', { name: 'Dashboard', exact: true })
          .click();
      } catch {
        isFound = true;
      }
    });

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (!isFound) {
      await test.step(`Add new contact BSN ${BSN}`, async () => {
        await addPersonContact({
          page,
          BSN: BSN,
        });
      });

      await test.step(`Set the note new contact BSN ${BSN}`, async () => {
        await addInternalNote({ page, BSN });
      });
    }

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (isFound) {
      await test.step(`Open and check/set supplementary data contact BSN ${BSN}`, async () => {
        await openFoundPersonAndCheckSetSup({
          page,
          BSN: BSN,
          withBirthdate: false,
        });
      });
    }

    await test.step(`Return to main screen for contact BSN ${BSN}`, async () => {
      await page.getByRole('link', { name: 'Dashboard Dashboard' }).click();
    });
  });

  test(`BSN1A: Search by zipcode and house number. When not found, add contact ${BSN1}`, async ({
    page,
  }) => {
    const BSN = BSN1;
    const { residenceHouseNumber, residenceZipcode } =
      personDataEnvironment[BSN];
    isFound = false;
    await test.step('Search through the menu item contact search', async () => {
      await page.getByLabel('Hoofdmenu openen').click();
      await page.getByRole('menuitem', { name: ' Contact zoeken' }).click();

      await page.waitForLoadState('domcontentloaded');
      await page.locator('input[name="zipcode"]').click();
      await page.locator('input[name="zipcode"]').fill(residenceZipcode);
      await page.locator('input[name="houseNumber"]').click();
      await page
        .locator('input[name="houseNumber"]')
        .fill(residenceHouseNumber);
      await page.getByRole('button', { name: 'Zoeken' }).click();
      await page.waitForResponse(res => res.status() === 200);
      try {
        await expect(page.getByText('Geen resultaten gevonden.')).toBeVisible({
          timeout: shortTimeout,
        });
        await page
          .getByRole('link', { name: 'Dashboard', exact: true })
          .click();
      } catch {
        isFound = true;
      }
    });

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (!isFound) {
      await test.step(`Add new contact BSN ${BSN}`, async () => {
        await addPersonContact({
          page,
          BSN: BSN,
        });
      });

      await test.step(`Set the note new contact BSN ${BSN}`, async () => {
        await addInternalNote({ page, BSN });
      });
    }

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (isFound) {
      await test.step(`Open and check/set supplementary data contact BSN ${BSN}`, async () => {
        await openFoundPersonAndCheckSetSup({
          page,
          BSN: BSN,
          withBirthdate: false,
        });
      });
    }

    await test.step(`Return to main screen for contact BSN ${BSN}`, async () => {
      await page.getByRole('link', { name: 'Dashboard Dashboard' }).click();
    });
  });

  test(`BSN1B: Search by last name for BSN ${BSN1}`, async ({ page }) => {
    const BSN = BSN1;
    const { familyName, firstNames, birthDate } = personDataEnvironment[BSN];
    const contactNameToClick = generateApplicantNameToClick({
      BSN,
      withBirthdate: false,
    });
    isFound = false;
    await test.step('Search through the menu item contact search', async () => {
      await page.getByLabel('Hoofdmenu openen').click();
      await page.getByRole('menuitem', { name: ' Contact zoeken' }).click();

      await page.locator('input[name="familyName"]').fill(familyName);
      await page.getByRole('button', { name: 'Zoeken' }).click();
      await page.waitForResponse(res => res.status() === 200);
    });

    await test.step('if more then one contact was found for BSN ${BSN}', async () => {
      try {
        await expect(
          page.getByText(`${contactNameToClick}`).first()
        ).toBeVisible({ timeout: shortTimeout });
      } catch {
        await expect(
          page.getByText(
            'Er zijn meer dan 10 resultaten gevonden. Verklein uw zoekopdracht en probeer het opnieuw.'
          )
        ).toBeVisible();
        await page.reload({ waitUntil: 'domcontentloaded' });
        await expect(
          page.getByText(
            'Gebruik een zoekopdracht waarmee maximaal 10 resultaten gevonden kunnen worden.'
          )
        ).toBeVisible();

        await page.getByPlaceholder('dd-mm-jjjj').click();
        await page.getByPlaceholder('dd-mm-jjjj').fill(birthDate);
        await page.locator('input[name="familyName"]').click();
        await page.locator('input[name="familyName"]').fill(familyName);
        await page.getByRole('button', { name: 'Zoeken' }).click();
        await page.waitForResponse(res => res.status() === 200);
      }
    });

    await test.step('Open the found contact with BSN ${BSN}', async () => {
      await expect(
        page.getByText(`${contactNameToClick}`).first()
      ).toBeVisible();
      await page.getByText(`${contactNameToClick}`).first().click();

      // eslint-disable-next-line playwright/no-conditional-in-test
      'Ja' ===
      (await page
        .locator(locators.labelLeftTextInContact('Authentiek'))
        .innerText())
        ? await expect(page.getByText(firstNames)).toBeVisible()
        : await expect(
            page.locator(`input[name="firstNames"]`).first()
          ).toHaveValue(firstNames);
    });
  });
});
