/* eslint-disable playwright/no-networkidle */
/* eslint-disable playwright/expect-expect */
/* eslint-disable camelcase */
import { TestInfo, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from './../../playwright.config';
import { createCase } from '../../utils/createCase';
import { testrondeData } from './testrondeData';
import {
  fillAttribute,
  expectAttributeValueOnOverview,
  registerCaseAndCheckMessages,
} from '../zs-form/formFillingAndCaseRegistration';
import { executeSteps } from '../../utils/executeSteps';
import {
  BSN2,
  BSN1,
  generateApplicantName,
  generateApplicantNameToClick,
} from '../../testfiles';
import { finishMainCase } from './finishMainCase';

const { shortTimeout } = testSettings;
const mainCaseTypeName = 'Auto TESTRONDE V2';
const subCaseTypeName = 'Auto TESTRONDE Deelzaak V2';
const BSN = BSN1;
const contactBSN = BSN2;
const contactFirstnameLastname = generateApplicantName(contactBSN);
const contactInitialsLastname = generateApplicantNameToClick({
  BSN: contactBSN,
  withBirthdate: false,
});
const contactNameToClick = generateApplicantNameToClick({ BSN: contactBSN });
let caseNumberInUrl = '';

test.describe
  .serial(`Create V2 object in subcase (${mainCaseTypeName}) by employee for "Nee" @all @select`, () => {
  test.use({ storageState: authPathBeheerder });
  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  const firstPageAttributes = Object.values(testrondeData).filter(
    attribute => attribute.onPages?.toString().includes('form1')
  );
  const secondPageAttributes = Object.values(testrondeData).filter(
    attribute => attribute.onPages?.toString().includes('form2')
  );
  const objectPageAttributes = Object.values(testrondeData).filter(
    attribute =>
      attribute.inObject === true && attribute.inputType !== 'Geolocatie'
  );
  test(`Make cases and object in ${subCaseTypeName}`, async ({
    page,
  }, testInfo: TestInfo) => {
    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: mainCaseTypeName, BSN });
    });

    await executeSteps(
      attribute =>
        `Fill value for ${attribute.nameAttribute} in ${mainCaseTypeName}`,
      firstPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to next page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await expect(page.getByText('Vul ze allemaal in!')).toBeVisible();
    });

    await test.step("Set 'Nee' to K1016 field, so that rules don't trigger", async () => {
      await page
        .getByText('auto_tst_k1016_enkelvoudige_keuze')
        .scrollIntoViewIfNeeded();
      await page.getByLabel('Nee').click();
    });

    await executeSteps(
      attribute =>
        `Fill value for ${attribute.nameAttribute} in ${mainCaseTypeName}`,
      secondPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to overview page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.allocation-picker-option-button');
    });

    await test.step(`Add betrokkene ${mainCaseTypeName} for choice "Nee"`, async () => {
      await page.getByRole('button', { name: 'Betrokkene toevoegen' }).click();
      await page.getByLabel('Betrokkene*', { exact: true }).click();
      await page
        .getByRole('textbox', { name: 'BetrokkeneDit veld is verplicht' })
        .fill(contactFirstnameLastname);
      await page
        .getByRole('button', {
          name: contactNameToClick,
        })
        .click();
      await page
        .getByLabel('Rol betrokkene*')
        .selectOption('string:Gemachtigde');
      await page.getByLabel('Gemachtigd voor deze zaak*').check();
      await expect(page.getByLabel('Gemachtigd voor deze zaak*')).toBeChecked();
      await page.getByLabel('Verstuur bevestiging per email*').click();
      try {
        await expect(
          page.getByLabel('Verstuur bevestiging per email*')
        ).toBeChecked({ timeout: shortTimeout });
      } catch (error) {
        await page.getByLabel('Verstuur bevestiging per email*').click();
      }
      await expect(
        page.getByLabel('Verstuur bevestiging per email*')
      ).toBeChecked();
      await page.getByRole('button', { name: 'Opslaan' }).click();
    });

    await test.step(`Register the case ${mainCaseTypeName} for choice "Nee"`, async () => {
      caseNumberInUrl = await registerCaseAndCheckMessages({
        page,
        waitForText: 'Extra testen',
        testScriptName: testInfo.title,
      });
    });

    await test.step(`Perform task create case ${subCaseTypeName} for choice "Nee"`, async () => {
      await expect(page.getByText('Auto TESTRONDE Deelzaak V2')).toBeVisible();
      await page.getByText('Auto TESTRONDE Deelzaak V2').click();
      await expect(page.getByRole('button', { name: 'Starten' })).toBeVisible();
      await page.getByRole('button', { name: 'Starten' }).click();
    });

    await test.step(`Go to the relation page on case ${mainCaseTypeName} for choice "Nee"`, async () => {
      await page.getByRole('link', { name: 'Relaties' }).click();
    });

    await test.step(`Check adding of betrokkene for ${mainCaseTypeName} for choice "Nee"`, async () => {
      await expect(
        page
          .frameLocator('#react-iframe')
          .getByText(`Natuurlijk persoon${contactInitialsLastname}Gemachtigde`)
      ).toBeVisible();
    });

    await test.step(`Wait for case ${subCaseTypeName} for choice "Nee"`, async () => {
      let isVisible = false;
      await expect
        .poll(
          async () => {
            try {
              await expect(
                page
                  .frameLocator('#react-iframe')
                  .getByText(`${subCaseTypeName}`)
              ).toBeVisible({ timeout: shortTimeout });
              isVisible = true;
            } catch {
              await page.reload({ waitUntil: 'domcontentloaded' });
              await page.waitForLoadState('networkidle');
            }
            return isVisible;
          },
          {
            // Probe, wait 1s, probe, wait 2s, probe, wait 5s, probe, wait 5s, probe, .... Defaults to [100, 250, 500, 1000].
            // intervals: [3_000, 2_000, 2_000],
            timeout: 40_000,
          }
        )
        .toBeTruthy();
    });

    await test.step(`Open partial case ${subCaseTypeName} for choice "Nee"`, async () => {
      await page
        .frameLocator('#react-iframe')
        .getByRole('row')
        .filter({ hasText: `${subCaseTypeName}` })
        .getByRole('link')
        .click();
    });

    await test.step(`Start making object for case ${subCaseTypeName} was registered message for "Nee"`, async () => {
      await page
        .getByRole('button', {
          name: 'Object V2 aanmaken via relatiekenmerk',
        })
        .click();
      await expect(
        page
          .frameLocator('#react-iframe')
          .getByText(`${testrondeData.auto_tst_o0001_object_naam.title}`)
      ).toBeVisible();
    });

    await test.step(`Add unique timestamp for object for case ${subCaseTypeName} was registered message for "Nee"`, async () => {
      await page
        .frameLocator('#react-iframe')
        .locator('input[name="auto_tst_o0001_object_naam"]')
        .click();
      await page
        .frameLocator('#react-iframe')
        .locator('input[name="auto_tst_o0001_object_naam"]')
        .press('Control+a');
      await page
        .frameLocator('#react-iframe')
        .locator('input[name="auto_tst_o0001_object_naam"]')
        .fill(`${testrondeData.auto_tst_o0001_object_naam.fillValue}`);
    });

    await test.step(`Make object for case ${subCaseTypeName} was registered message for "Nee"`, async () => {
      await expect(
        page
          .frameLocator('#react-iframe')
          .getByRole('button', { name: 'Aanmaken' })
      ).toBeVisible();
      await page
        .frameLocator('#react-iframe')
        .getByRole('button', { name: 'Aanmaken' })
        .press('Enter');
    });

    await test.step(`Check object for case ${subCaseTypeName} for choice "Nee"`, async () => {
      await page.getByText('Object openen').click();
    });

    await executeSteps(
      attribute =>
        `Ensure value of ${attribute.nameAttribute} is displayed correctly on the object page`,
      objectPageAttributes,
      expectAttributeValueOnOverview(page, false, true)
    );

    await test.step(`Check object for case ${subCaseTypeName} for choice "Nee"`, async () => {
      await page.goBack({ waitUntil: 'domcontentloaded' });
    });

    await test.step(`Add contact ${subCaseTypeName} for choice "Nee"`, async () => {
      await page.getByLabel('auto_relatie_kenmerk_testronde_contact*').click();
      await page
        .getByLabel('auto_relatie_kenmerk_testronde_contact*')
        .fill(contactFirstnameLastname);
      await page.getByRole('button', { name: `${contactNameToClick}` }).click();
    });

    await test.step(`Go to the relation page on case ${subCaseTypeName} for choice "Nee"`, async () => {
      await page.getByRole('link', { name: 'Relaties' }).click();
    });

    await test.step(`Check adding of object for ${subCaseTypeName} for choice "Nee"`, async () => {
      await page
        .frameLocator('#react-iframe')
        .getByRole('heading', { name: 'Gerelateerde objecten v2' })
        .scrollIntoViewIfNeeded();
      let isVisible = false;
      await expect
        .poll(
          async () => {
            try {
              await expect(
                page
                  .frameLocator('#react-iframe')
                  .getByText(
                    `Auto TESTRONDE Objecttype V2Auto Testronde ${testrondeData.auto_tst_k1000_datum_release_tag.fillValue} - E2E Object V2`
                  )
              ).toBeVisible();
              isVisible = true;
            } catch {
              await page.reload({ waitUntil: 'domcontentloaded' });
              await page.waitForLoadState('networkidle');
              await page
                .frameLocator('#react-iframe')
                .getByRole('heading', { name: 'Gerelateerde objecten v2' })
                .scrollIntoViewIfNeeded();
            }
            return isVisible;
          },
          {
            // Probe, wait 1s, probe, wait 2s, probe, wait 5s, probe, wait 5s, probe, .... Defaults to [100, 250, 500, 1000].
            // intervals: [3_000, 2_000, 2_000],
            timeout: 40_000,
          }
        )
        .toBeTruthy();
    });

    await test.step(`Check adding of betrokkene for ${subCaseTypeName} for choice "Nee"`, async () => {
      await page
        .frameLocator('#react-iframe')
        .getByRole('heading', { name: 'Betrokkenen' })
        .scrollIntoViewIfNeeded();
      let isVisible = false;
      await expect
        .poll(
          async () => {
            try {
              await expect(
                page
                  .frameLocator('#react-iframe')
                  .getByText(
                    `Natuurlijk persoon${contactInitialsLastname}Mantelzorger`
                  )
              ).toBeVisible();
              isVisible = true;
            } catch {
              await page.reload({ waitUntil: 'domcontentloaded' });
              await page.waitForLoadState('networkidle');
              await page
                .frameLocator('#react-iframe')
                .getByRole('heading', { name: 'Betrokkenen' })
                .scrollIntoViewIfNeeded();
            }
            return isVisible;
          },
          {
            // Probe, wait 1s, probe, wait 2s, probe, wait 5s, probe, wait 5s, probe, .... Defaults to [100, 250, 500, 1000].
            // intervals: [3_000, 2_000, 2_000],
            timeout: 40_000,
          }
        )
        .toBeTruthy();
      await expect(
        page
          .frameLocator('#react-iframe')
          .getByText(`Natuurlijk persoon${contactInitialsLastname}Mantelzorger`)
      ).toBeVisible();
    });

    await test.step(`Return to phases page on case ${subCaseTypeName} for choice "Nee"`, async () => {
      await page.getByRole('link', { name: 'Fasen' }).click();
    });

    await test.step(`Finish partial case ${subCaseTypeName} for choice "Nee"`, async () => {
      await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
      await expect(
        page.getByText('Mijn openstaande zaken').first()
      ).toBeVisible();
    });

    await test.step(`Open main case ${mainCaseTypeName} for choice "Nee"`, async () => {
      await page
        .getByRole('row', { name: 'Zaak: ' + caseNumberInUrl })
        .first()
        .click();
    });

    await finishMainCase({ page, mainCaseTypeName });
  });
});
