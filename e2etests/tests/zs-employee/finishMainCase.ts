import { Page, expect, test } from '@playwright/test';
import { locators } from '../../utils';

interface PropsMainTestCase {
  page: Page;
  mainCaseTypeName: string;
}

export const finishMainCase = async ({
  page,
  mainCaseTypeName,
}: PropsMainTestCase) => {
  await test.step(`Finish phase 2 in main case ${mainCaseTypeName} for choice "Nee"`, async () => {
    await page.getByRole('link', { name: 'Open tab Taken' }).first().click();
    await page
      .frameLocator('#react-iframe')
      .locator('li')
      .filter({
        hasText: 'Registratiefase wijzigen en nieuwe kenme',
      })
      .locator('button[name="taskListDone"]')
      .click();
    await page.reload();
    const smallScreen = await page
      .locator('zs-case-phase-sidebar')
      .getByRole('button', { name: '' })
      .isVisible();
    // eslint-disable-next-line playwright/no-conditional-in-test
    smallScreen
      ? await page
          .locator('zs-case-phase-sidebar')
          .getByRole('button', { name: '' })
          .click()
      : '';
    await page.getByRole('button', { name: ' Fase afronden' }).click();
    await expect(
      page
        .locator(locators.snackMessage())
        .filter({ hasText: 'De fase is succesvol afgerond' })
        .first()
    ).toBeVisible();
    await page.getByRole('button', { name: 'Melding sluiten' }).click();
  });

  await test.step(`Finish main case ${mainCaseTypeName} for choice "Nee"`, async () => {
    await page.getByRole('link', { name: 'Open tab Acties' }).click();
    await page
      .locator('li')
      .filter({ hasText: 'Auto TESTRONDE Gerelateerde zaak V2' })
      .getByRole('checkbox')
      .uncheck();
    await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
    await expect(
      page
        .locator(locators.snackMessage())
        .filter({ hasText: 'De zaak is succesvol afgerond' })
        .first()
    ).toBeVisible();
    await page.getByRole('button', { name: 'Melding sluiten' }).click();
  });
};
