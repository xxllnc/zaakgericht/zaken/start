/* eslint-disable playwright/no-conditional-in-test */
import { Page, expect, test } from '@playwright/test';
import { locators } from '../../utils/locators';
import { Attribute, testFiles } from '../zs-employee/testrondeData';
import { locatorPlaceholder } from '../../utils';
import { testSettings } from '../../playwright.config';
import path from 'path';

const {
  longTimeout,
  extraLongTimeout,
  superLongTimeout,
  clickDelay,
  typeDelay,
  shortTimeout,
} = testSettings;

const addressFields = [
  'Geolocatie',
  'Adres V2',
  'Adres (dmv postcode)',
  'Adressen (dmv postcode)',
  'Straat',
  'Straten',
  'Adres (dmv straatnaam)',
  'Adressen (dmv straatnaam)',
  'Locatie met kaart',
  'Adres (Google Maps)',
];
const fieldIsInputTypeAddress = (attribute: Attribute) => {
  return addressFields.find(type => type === attribute.inputType) !== undefined;
};
//In the phase Registration of the case there can be hidden mandatory *'s
//these attributes need to be located with * behind the public name
const isInCase = (page: Page) => {
  return page.url().includes('intern/zaak') ? '*' : '';
};
const fieldName = (page: Page, attribute: Attribute) =>
  `${attribute.publicName}${isInCase(page)}`;

//auto_tst_k1020_tekstveld_caps has an explanation this also has the name in it
//therefor this attribute needs to be found with the exact text
//Exact also needs to be used for auto_tst_k1011_document
const needExact = (attribute: Attribute, page: Page) => {
  return (
    ((page.url().includes('webform') &&
      attribute.publicName === 'auto_tst_k1020_tekstveld_caps') ||
      attribute.publicName === 'auto_tst_k1011_document') ??
    false
  );
};

const chooseValueToFill = (attribute: Attribute, page: Page) => {
  return attribute.inputType === 'Datum' && page.url().includes('webform')
    ? attribute.overviewValue ?? ''
    : attribute.fillValue ?? '';
};

const textfieldCapsAndWebform = (attribute: Attribute, page: Page) => {
  return (
    attribute.fillValue !== undefined &&
    attribute.inputType === 'Tekstveld (HOOFDLETTERS)' &&
    page.url().includes('webform')
  );
};

const fillGeneral = (attribute: Attribute, page: Page) => {
  return (
    attribute.fillValue !== undefined &&
    attribute.inputType !== 'Groot tekstveld' &&
    attribute.inputType !== 'Rich text' &&
    !(
      attribute.inputType === 'Tekstveld (HOOFDLETTERS)' &&
      page.url().includes('webform')
    )
  );
};
//********************************************************* */
export const fillAttribute = (page: Page) => async (attribute: Attribute) => {
  await page.getByText(attribute.publicName).first().scrollIntoViewIfNeeded();

  //In order to fill inputtype 'Tekstveld (HOOFDLETTERS)'
  //and see the capitals occur, this field needs to be cleared
  //and filled with an empty string first. ONLY on the webform!!
  const needExactly = needExact(attribute, page);
  textfieldCapsAndWebform(attribute, page)
    ? await page
        .getByLabel(attribute.publicName, {
          exact: needExactly,
        })
        .fill('')
    : '';

  const fillValue =
    attribute.fillValue !== undefined ? chooseValueToFill(attribute, page) : '';

  textfieldCapsAndWebform(attribute, page)
    ? await page
        .getByLabel(attribute.publicName, {
          exact: needExactly,
        })
        .pressSequentially(fillValue)
    : '';

  fillGeneral(attribute, page)
    ? await page
        .getByLabel(`${fieldName(page, attribute)}`, {
          exact: needExactly,
        })
        .fill(fillValue)
    : '';

  fillValue !== '' && attribute.inputType === 'Groot tekstveld'
    ? await page.locator('textarea').fill(fillValue)
    : '';

  fillValue !== '' && attribute.inputType === 'Rich text'
    ? await page
        .locator(locators.labelLeftRichTextFatt(`${attribute.publicName}`))
        .fill(fillValue)
    : '';

  attribute.testDoc !== undefined
    ? await uploadDocument({ page, attribute })
    : '';

  attribute.checkValue !== undefined
    ? await page.getByLabel(attribute.checkValue).check()
    : '';

  attribute.addressValue !== undefined
    ? await selectAddress({ page, attribute })
    : '';

  const optionToSelect = page.url().includes('aanvragen')
    ? `string:${attribute.selectValue}`
    : attribute.selectValue ?? '';
  attribute.selectValue !== undefined
    ? await page
        .getByRole('combobox', { name: attribute.publicName })
        .selectOption(optionToSelect)
    : '';

  attribute.checkValue2 !== undefined
    ? await page.getByLabel(`${attribute.checkValue2}`).click()
    : '';

  attribute.addressValue2 !== undefined
    ? await selectAddress({ page, attribute, fieldNumber: 2 })
    : '';
};

//********************************************************* */
const selectAddress = async ({
  page,
  attribute,
  fieldNumber = 1,
}: {
  page: Page;
  attribute: Attribute;
  fieldNumber?: number;
}) => {
  await page.locator('//iframe').first().isVisible({ timeout: longTimeout });
  let addressLocator =
    (attribute.inputType === 'Locatie met kaart' ||
      attribute.inputType === 'Adres (Google Maps)') &&
    page.url().includes('intern')
      ? page.locator(
          locators.inputResultField(
            `${attribute.publicName}`,
            `${attribute.inputType}`
          )
        )
      : page.getByLabel(`${fieldName(page, attribute)}`).first();

  addressLocator =
    attribute.inputType === 'Adres V2' && page.url().includes('intern')
      ? page
          .getByLabel(`${fieldName(page, attribute)}`)
          .getByPlaceholder(`${locatorPlaceholder}`)
      : addressLocator;

  await addressLocator.click({ delay: clickDelay });
  try {
    attribute.inputType === 'Adres (Google Maps)' ||
    attribute.inputType === 'Adres V2'
      ? await expect(addressLocator).toBeFocused({ timeout: shortTimeout })
      : '';
  } catch {
    await addressLocator.click({ delay: clickDelay });
  }

  const addressToFill =
    fieldNumber === 1 ? attribute.addressValue : attribute.addressValue2;
  await addressLocator.pressSequentially(
    addressToFill !== undefined ? addressToFill.replace('\\', '') : '3311BV 60'
  );
  await page.waitForLoadState('domcontentloaded');

  const addressLocatorSelect =
    attribute.inputType === 'Adres V2' ||
    attribute.inputType === 'Locatie met kaart' ||
    attribute.inputType === 'Adres (Google Maps)'
      ? addressLocator
      : page.getByRole('textbox', { name: `${attribute.publicName}` });
  const addressToSelect =
    fieldNumber === 1
      ? attribute.overviewValue?.replace(' + −', '')
      : attribute.overviewValue2;

  await expect(
    page.getByText(
      addressToSelect !== undefined
        ? addressToSelect.replace('\\', '')
        : "Bunninkh'plein 60, 3311BV Dordrecht"
    )
  ).toBeVisible();
  await addressLocatorSelect.press('ArrowDown', { delay: typeDelay });
  await Promise.all([
    page.waitForResponse(res => res.status() === 200),
    addressLocatorSelect.press('Enter'),
  ]);
  await page.waitForLoadState('domcontentloaded');
};

//********************************************************* */
const testFileDir = 'testfiles';

const uploadDocument = async ({
  page,
  attribute,
}: {
  page: Page;
  attribute: Attribute;
}) => {
  await page.waitForLoadState('domcontentloaded');
  let numberToSet = 0;
  if (attribute.publicName.endsWith('2') && page.url().includes('intern')) {
    numberToSet = 1;
  } else if (
    attribute.publicName.endsWith('3') &&
    page.url().includes('intern')
  ) {
    numberToSet = 2;
  }
  const locator = page
    .getByRole('button', { name: /Bestand toevoegen|Kies bestand/ })
    .nth(numberToSet);
  const [fileChooser] = await Promise.all([
    page.waitForEvent('filechooser'),
    locator.click({ delay: clickDelay }),
  ]);
  await fileChooser.setFiles(
    path.join(
      __dirname,
      '..',
      '..',
      testFileDir,
      `${attribute.testDoc ?? testFiles[1]}`
    )
  );
  await expect(
    page.getByText(`${attribute.testDoc ?? testFiles[1]}`)
  ).toBeVisible();
};

//********************************************************* */
const getChoices = async (attribute: Attribute, page: Page) => {
  let checked = '';
  for (const choice of attribute.choices ?? []) {
    if (await page.getByLabel(`${choice}`).isChecked()) checked += ` ${choice}`;
  }
  return checked.substring(1);
};

const getInputValue = async ({
  page,
  attribute,
}: {
  page: Page;
  attribute: Attribute;
}) => {
  //auto_tst_k1020_tekstveld_caps on PIP has an explanation, this also has the name in it
  //therefor this attribute needs to be located with exact = true
  if (attribute.inputType === 'Afbeelding') {
    const value =
      page.url().includes('aanvragen') || page.url().includes('intern')
        ? await page.getByLabel(`${fieldName(page, attribute)}`).inputValue()
        : await page
            .locator(
              `//div[contains(@data-zs-case-webform-field-name,"${attribute.publicName}")]//img`
            )
            .getAttribute('src');
    return value;
  } else if (attribute.inputType === 'Tekstveld (HOOFDLETTERS)') {
    return await page
      .getByLabel(`${fieldName(page, attribute)}`, {
        exact: page.url().includes('webform'),
      })
      .inputValue();
  } else if (attribute.inputType === 'Rich text') {
    return await page.locator('//*[@class="ql-editor"]').innerText();
  } else if (attribute.inputType === 'Enkelvoudige keuze') {
    return await page
      .locator(
        `//*[.//text()="${attribute.publicName}"]/parent::li//input[@aria-checked="true"]/parent::label/span`
      )
      .inputValue();
  } else if (attribute.inputType === 'Groot tekstveld') {
    return await page.locator('textarea').inputValue();
  } else if (attribute.inputType === 'Valuta') {
    const amount = await page
      .getByLabel(`${fieldName(page, attribute)}`)
      .inputValue();
    return amount;
  } else if (attribute.inputType === 'Meervoudige keuze') {
    return await getChoices(attribute, page);
  } else if (attribute.inputType === 'Document') {
    return (
      await page
        .locator(
          locators.inputResultField(
            `${attribute.publicName}`,
            `${attribute.inputType}`
          )
        )
        .first()
        .innerText()
    )
      .toString()
      .trim();
  } else if (attribute.inputType === 'Adres V2') {
    return (
      await page
        .getByLabel(`${attribute.publicName}`)
        .getByPlaceholder(`${locatorPlaceholder}`)
        .inputValue()
    )
      .toString()
      .replace(/\n/g, ' ');
  } else if (fieldIsInputTypeAddress(attribute)) {
    return (
      await page
        .locator(
          locators.inputResultField(
            `${attribute.publicName}`,
            `${attribute.inputType}`,
            'getText'
          )
        )
        .allInnerTexts()
    )
      .toString()
      .replace(/\n/g, ' ');
  } else {
    return (
      await page.getByLabel(`${fieldName(page, attribute)}`).inputValue()
    ).replace('string:', '');
  }
};

//********************************************************* */
const currencyFormat = new Intl.NumberFormat('nl-NL', {
  style: 'currency',
  currency: 'EUR',
});

//********************************************************* */
export const formatAmount = (dataValue: string) => {
  const amountWithBlank = currencyFormat.format(
    Number(dataValue.replace(',', '.'))
  );
  // The amount recieved seems to have a blank, but it is not %20
  // therefor skipped here.
  return amountWithBlank.slice(0, 1) + amountWithBlank.slice(2);
};

//********************************************************* */
export const expectAttributeValueOnForm =
  (page: Page, rulesTriggered = false) =>
  async (attribute: Attribute) => {
    const readOnlyField =
      rulesTriggered && attribute.becomesReadonlyAfterRuleTrigger;

    const locatorWebformOrIntern = page.url().includes('webform')
      ? 'div.regels_fixed_value'
      : 'span.delegate-value-display';
    const locator = readOnlyField
      ? page
          .locator(locatorWebformOrIntern)
          .filter({ hasText: attribute.expectedFormValueAfterRuleTrigger })
      : page.getByText(`${attribute.publicName}`);
    await locator.scrollIntoViewIfNeeded();
    const inputValue = readOnlyField
      ? (await locator.allTextContents()).toString() ?? ''
      : (await getInputValue({ attribute, page })) ?? '';
    const inputValueCorrected =
      attribute.inputType === 'Valuta' && page.url().includes('webform')
        ? inputValue.slice(0, 1) + inputValue.slice(2)
        : inputValue?.replace(/\s+/g, ' ').replace('string:', '');

    const valueDefaultOrAfterRules =
      rulesTriggered &&
      !(attribute.inputType === 'Rich text' && page.url().includes('webform'))
        ? attribute.expectedFormValueAfterRuleTrigger ??
          attribute.valueDefault ??
          ''
        : attribute.valueDefault ?? '';
    const valueDefaultOrAfterRulesCorrected =
      page.url().includes('webform') && attribute.inputType === 'Valuta'
        ? formatAmount(valueDefaultOrAfterRules)
        : valueDefaultOrAfterRules;

    expect(`${inputValueCorrected}`).toEqual(
      `${valueDefaultOrAfterRulesCorrected}`
    );
  };

//********************************************************* */
const dateFoundFormat = (dateFound: string) => {
  const dateUniFormatArray = dateFound.split('-');
  for (let index = 0; index < 3; index++) {
    if (dateUniFormatArray[index].length === 1) {
      dateUniFormatArray[index] = '0' + dateUniFormatArray[index];
    }
  }
  return dateUniFormatArray.join('-');
};

const attributeNameOnScreen = (attribute: Attribute, objectCheck: boolean) => {
  return objectCheck ? `${attribute.title}` : `${attribute.publicName}`;
};

const valueFoundLocatorNotInObject = (
  page: Page,
  attribute: Attribute,
  objectCheck: boolean
) => {
  const imgOnWebform =
    attribute.inputType === 'Afbeelding' && page.url().includes('webform');
  const externFormOverviewChoiceList =
    page.url().includes('webform') &&
    (attribute.inputType === 'Meervoudige keuze' ||
      attribute.inputType === 'Enkelvoudige keuze');

  // rem locator(
  //     '/label[contains(.//text(),"afbeelding")]/parent::div/parent::div/parent::div//img'
  //   )
  return imgOnWebform
    ? page.locator('.field > img')
    : page.locator(
        locators.labelLeftOnForm(
          attributeNameOnScreen(attribute, objectCheck),
          page.url().includes('aanvragen') || page.url().includes('intern'),
          page.url().includes('webform'),
          externFormOverviewChoiceList
        )
      );
};

const calcLengthTextGreatTextfield = (attribute: Attribute) => {
  const length = attribute.overviewValue?.length ?? 0;
  return length > 100 ? length - 5 : length - 1;
};

const getOverviewValueGreatTextfield = (page: Page, attribute: Attribute) => {
  const length = attribute.overviewValue?.length ?? 0;
  const moreOrNot = length > 100 ? ' Meer' : '';
  return page.url().includes('webform')
    ? `${attribute.overviewValue?.trimEnd()}${moreOrNot}`
    : `${
        attribute.overviewValue?.substring(
          0,
          calcLengthTextGreatTextfield(attribute)
        ) + '... MEER'
      }`;
};

const getValueFound = async (
  page: Page,
  attribute: Attribute,
  objectCheck: boolean
) => {
  const locatorValueFound = !objectCheck
    ? valueFoundLocatorNotInObject(page, attribute, objectCheck)
    : page.locator(
        locators.objectShowField(
          attributeNameOnScreen(attribute, objectCheck),
          attribute.inputType
        )
      );
  let valueFound = '';
  if (objectCheck && attribute.inputType === 'Groot tekstveld') {
    valueFound = await locatorValueFound.first().innerHTML();
  } else if (
    !objectCheck &&
    attribute.inputType === 'Afbeelding' &&
    page.url().includes('webform')
  ) {
    valueFound = (await locatorValueFound.getAttribute('src')) ?? '';
  } else {
    valueFound = (await locatorValueFound.first().innerText()).trimEnd();
  }
  if (attribute.inputType === 'Datum') {
    //Dates on a form in PIP seem to be shown in the format 1-1-2020
    //at least when run in playwright. Zeroes are added here.
    return page.url().includes('webform')
      ? dateFoundFormat(valueFound ?? '')
      : valueFound;
  } else if (attribute.inputType === 'Valuta') {
    // The amount recieved seems to have a blank, but it is not %20
    // therefor skipped here, but only when the amount is formatted
    // as an amount
    return valueFound.startsWith('€')
      ? valueFound.slice(0, 1) + valueFound.slice(2)
      : valueFound;
  }
  return valueFound?.replace(/\s+/g, ' ').replace('string:', '');
};

const getOvvValueRuleTriggered = (
  page: Page,
  attribute: Attribute,
  ovvValue: string
) => {
  const expectedOverviewValueAfterRuleTrigger =
    attribute.expectedOverviewValueAfterRuleTrigger ?? '';
  if (expectedOverviewValueAfterRuleTrigger === '') {
    return ovvValue;
  } else if (attribute.inputType === 'Groot tekstveld') {
    const value = page.url().includes('intern')
      ? `${expectedOverviewValueAfterRuleTrigger}... MEER`
      : expectedOverviewValueAfterRuleTrigger;
    return value;
  } else if (attribute.inputType === 'Valuta') {
    const value = page.url().includes('webform')
      ? formatAmount(expectedOverviewValueAfterRuleTrigger)
      : expectedOverviewValueAfterRuleTrigger;
    return value;
  }
  return attribute.expectedOverviewValueAfterRuleTrigger;
};

//********************************************************* */
export const expectAttributeValueOnOverview =
  (page: Page, rulesTriggered = false, objectCheck = false) =>
  async (attribute: Attribute) => {
    await page
      .getByText(attributeNameOnScreen(attribute, objectCheck))
      .first()
      .scrollIntoViewIfNeeded({ timeout: extraLongTimeout });

    const valueFound = await getValueFound(page, attribute, objectCheck);

    let overviewValue1 = attribute.overviewValue;
    //Because some inputtype fields in the object are not working as they are in the original form/case
    //the value shown is not the same for Groot tekstveld fields and Valuta.
    //The overviewvalue is corrected here for that difference.
    overviewValue1 =
      objectCheck && attribute.inputType === 'Valuta'
        ? formatAmount(attribute.overviewValue ?? '')
        : overviewValue1;
    overviewValue1 =
      !objectCheck && attribute.inputType === 'Groot tekstveld'
        ? getOverviewValueGreatTextfield(page, attribute)
        : overviewValue1;
    overviewValue1 =
      attribute.inputType === 'Adres (Google Maps)' &&
      page.url().includes('webform')
        ? attribute.overviewValueExtern
        : overviewValue1;

    const overviewValue2 =
      attribute.overviewValue2 !== undefined
        ? ' ' + attribute.overviewValue2
        : '';
    const ovvValue = `${overviewValue1}${overviewValue2}`;
    const ovvValueItShouldBe =
      rulesTriggered &&
      attribute.expectedOverviewValueAfterRuleTrigger !== undefined
        ? getOvvValueRuleTriggered(page, attribute, ovvValue)
        : ovvValue;
    if (attribute.inputType == 'Adres (Google Maps)') {
      try {
        expect(valueFound).toEqual(ovvValueItShouldBe);
      } catch {
        console.log(
          `${attribute.publicName} was not correct. ValueFound: "${valueFound}" should have been "${ovvValueItShouldBe}"`
        );
      }
    } else {
      expect(valueFound).toEqual(ovvValueItShouldBe);
    }
  };

//********************************************************* */
let caseNumberInUrl = '';

interface PropsgetCaseNumber {
  textEndsWithCase: string;
  seperator?: string;
}

export const getCaseNumber = ({
  textEndsWithCase,
  seperator = ' ',
}: PropsgetCaseNumber) => {
  const textArray = textEndsWithCase.split(new RegExp(`${seperator}`));
  const caseNumber = textArray[textArray.length - 1];
  console.log(`The case number is: "${caseNumber}"`);

  return caseNumber;
};

//********************************************************* */
interface PropsRegisterCaseAndCheckMessages {
  page: Page;
  waitForText: string;
  testScriptName: string;
}

export const registerCaseAndCheckMessages = async ({
  page,
  waitForText,
  testScriptName,
}: PropsRegisterCaseAndCheckMessages) => {
  await test.step(`Register case for ${testScriptName}`, async () => {
    await page.getByRole('button', { name: 'Versturen' }).click();
  });

  await test.step(`Check case is being registered message is shown for ${testScriptName}`, async () => {
    await expect(
      page
        .locator('//div[@class="snack-message" ]')
        .filter({ hasText: 'Uw zaak wordt geregistreerd.' })
        .first()
    ).toBeVisible({ timeout: superLongTimeout });
  });
  await test.step(`Check case is being registered message is hidden for ${testScriptName}`, async () => {
    await expect(
      page
        .locator(locators.snackMessage())
        .filter({ hasText: 'Uw zaak wordt geregistreerd.' })
        .first()
    ).toBeHidden({ timeout: superLongTimeout });
  });

  await test.step(`Check case was registered message for ${testScriptName}`, async () => {
    await expect(
      page
        .locator(locators.snackMessage())
        .filter({
          hasText: 'Zaak is door u in behandeling genomen onder zaaknummer ',
        })
        .first()
    ).toBeVisible({ timeout: superLongTimeout });
    await expect(
      page
        .locator(locators.snackMessage())
        .filter({
          hasText: 'Zaak is door u in behandeling genomen onder zaaknummer ',
        })
        .first()
    ).toBeHidden({ timeout: superLongTimeout });
  });

  await test.step(`Wait for text in case to be visible for ${testScriptName}`, async () => {
    await expect(page.getByText(waitForText).first()).toBeVisible({
      timeout: longTimeout,
    });
  });

  await test.step(`Get the case number out of the url  for ${testScriptName}`, async () => {
    caseNumberInUrl = getCaseNumber({
      textEndsWithCase: page.url(),
      seperator: '/',
    });
  });
  return caseNumberInUrl;
};

//********************************************************* */
const expectedValueFilledInIs = (attribute: Attribute) => {
  //For the case phase Registration for most attributes the
  //overviewValue can be used.
  //Except voor dates, there the fill value must be used
  if (attribute.inputType === 'Datum') {
    return attribute.fillValue;
  } else if (attribute.inputType === 'Adres (Google Maps)') {
    return attribute.overviewValue?.replace(' + −', '');
  } else return attribute.overviewValue;
};

export const expectedValueFilledIn =
  (page: Page) => async (attribute: Attribute) => {
    await page
      .getByText(`${attribute.publicName}`)
      .first()
      .scrollIntoViewIfNeeded();

    const expectedValueFilledIn = expectedValueFilledInIs(attribute);
    const overviewValue2 =
      attribute.overviewValue2 !== undefined
        ? ' ' + attribute.overviewValue2
        : '';
    const expectedValueFilledInTotal = `${expectedValueFilledIn}${overviewValue2}`;

    try {
      const valueFoundInCase = await getInputValue({ attribute, page });
      expect(valueFoundInCase).toBe(expectedValueFilledInTotal);
    } catch (error) {
      const valueFoundInCase = await getInputValue({ attribute, page });
      expect(valueFoundInCase).toBe(expectedValueFilledInTotal);
    }
  };
//********************************************************* */
export const emptyValueFilledIn =
  (page: Page) => async (attribute: Attribute) => {
    await page
      .getByText(`${attribute.publicName}`)
      .first()
      .scrollIntoViewIfNeeded();

    const hasText =
      attribute.inputType === 'Adres (Google Maps)'
        ? `${attribute.publicName}* ${attribute.overviewValue?.replace(
            ' + −',
            ''
          )}`
        : `${attribute.publicName}* ${attribute.overviewValue}`;

    if (attribute.testDoc !== undefined) {
      await page
        .locator('vorm-field')
        .filter({
          hasText: `${attribute.publicName}* ${attribute.testDoc} Bestand toevo`,
        })
        .getByRole('button')
        .nth(1)
        .click();
      await page.getByRole('button', { name: 'Verwijderen' }).click();
    } else if (
      attribute.addressValue !== undefined &&
      attribute.inputType === 'Adres V2'
    ) {
      await page.getByLabel('Wissen').click();
    } else if (attribute.addressValue2 !== undefined) {
      await page
        .locator('vorm-field')
        .filter({
          hasText: `${hasText} ${attribute.overviewValue2}`,
        })
        .getByRole('button')
        .nth(1)
        .click();
      await page.getByRole('button', { name: 'Verwijderen' }).click();
      await page
        .locator('vorm-field')
        .filter({
          hasText: `${hasText} Begin met t`,
        })
        .getByRole('button')
        .first()
        .click();
      await page.getByRole('button', { name: 'Verwijderen' }).click();
    } else if (attribute.addressValue !== undefined) {
      await page
        .locator('vorm-field')
        .filter({
          hasText: `${hasText} Veld toevoege${
            attribute.inputType === 'Adres (Google Maps)' ? 'n +' : ''
          }`,
        })
        .getByLabel('Veld legen')
        .click();
      await page.getByRole('button', { name: 'Verwijderen' }).click();
    } else if (attribute.checkValue !== undefined) {
      await page.getByLabel(attribute.checkValue).uncheck();
      attribute.checkValue2 !== undefined
        ? await page.getByLabel(attribute.checkValue2).uncheck()
        : '';
    }
  };
