/* eslint-disable playwright/no-conditional-in-test */
/* eslint-disable playwright/no-networkidle */
import { Locator, Page, expect, test } from '@playwright/test';
import { Person } from '../../testfiles';
import { TestSettings } from '../../types';
import { formLoginButton, locators, useCorrespondence } from '../../utils';
import { testSettings } from '../../playwright.config';

const { environment, shortTimeout, typeDelay } = testSettings;
const clickOnAndWaitForResponse = async (
  page: Page,
  locator: Locator,
  url: RegExp,
  delay = 0
) => {
  const responsePromise = page.waitForResponse(url);
  await locator.click({ delay });
  await responsePromise;
  await page.waitForLoadState();
};

interface PropsLoginFormCheckPersonData {
  formPage: Page;
  loginFormName: string;
  person: Person;
  testSettings: TestSettings;
}

export const loginFormCheckPersonData = async ({
  formPage,
  loginFormName,
  person,
  testSettings,
}: PropsLoginFormCheckPersonData) => {
  const { baseUrl } = testSettings;

  await test.step('Login', async () => {
    await formPage.goto(`${baseUrl}form`);
    await formPage.waitForLoadState('networkidle');
    await expect(
      formPage.locator(locators.persoonLink(loginFormName))
    ).toBeVisible();

    await clickOnAndWaitForResponse(
      formPage,
      formPage.locator(locators.persoonLink(loginFormName)),
      /persoon/
    );
    await formPage.waitForLoadState('networkidle');
    await expect(
      formPage.getByRole('heading', { name: 'Inloggen met DigiD' })
    ).toBeVisible();

    await expect(formLoginButton(formPage)).toBeVisible();

    await clickOnAndWaitForResponse(
      formPage,
      formLoginButton(formPage),
      /auth/
    );
    await formPage.waitForLoadState('networkidle');

    await expect(formPage.getByLabel('BSN:')).toBeVisible();

    await formPage.getByLabel('BSN:').fill(person.BSN);

    const spoofButton = formPage.getByRole('button', { name: 'Spoof!' });
    await expect(spoofButton).toHaveText('Spoof!');

    await clickOnAndWaitForResponse(formPage, spoofButton, /auth/, 300);
    await expect(formPage.locator('h2')).toHaveText('Uw gegevens');
  });

  await test.step('Check contactgegevens tab', async () => {
    const getFieldValue = async (fieldName: string): Promise<string> => {
      const fieldLocator = formPage
        .locator(locators.labelLeftOnForm(fieldName))
        .nth(0);

      await fieldLocator.scrollIntoViewIfNeeded();
      return fieldLocator.innerText();
    };

    async function checkDutchAddress() {
      const checkedAddress: {
        city: string;
        street: string;
        zipcode: string;
        houseNumber: string;
        houseNumberSuffix: string;
        houseLetter: string;
      } = useCorrespondence(
        person.hasCorrespondenceAddress,
        person.residenceCity
      )
        ? {
            city: person.correspondenceCity,
            street: person.correspondenceStreet,
            zipcode: person.correspondenceZipcode,
            houseLetter: person.correspondenceHouseLetter,
            houseNumber: person.correspondenceHouseNumber,
            houseNumberSuffix: person.correspondenceHouseNumberSuffix,
          }
        : {
            city: person.residenceCity,
            street: person.residenceStreet,
            zipcode: person.residenceZipcode,
            houseLetter: person.residenceHouseLetter,
            houseNumber: person.residenceHouseNumber,
            houseNumberSuffix: person.residenceHouseNumberSuffix,
          };
      expect(await getFieldValue('Woonplaats')).toEqual(checkedAddress.city);
      expect(await getFieldValue('Straatnaam')).toEqual(checkedAddress.street);
      expect(await getFieldValue('Huisnummer')).toEqual(
        checkedAddress.houseNumber
      );
      expect(await getFieldValue('Huisnummertoevoeging')).toEqual(
        checkedAddress.houseNumberSuffix
      );
      expect(await getFieldValue('Huisletter')).toEqual(
        checkedAddress.houseLetter
      );
      expect(await getFieldValue('Postcode')).toEqual(checkedAddress.zipcode);
    }
    async function checkForeignAddress() {
      const checkedAddress: {
        foreignAddress1: string;
        foreignAddress2: string;
        foreignAddress3: string;
      } = {
        foreignAddress1: person.foreignAddress1,
        foreignAddress2: person.foreignAddress2,
        foreignAddress3: person.foreignAddress3,
      };
      expect(await getFieldValue('Adresregel 1:')).toEqual(
        checkedAddress.foreignAddress1
      );
      expect(await getFieldValue('Adresregel 2:')).toEqual(
        checkedAddress.foreignAddress2
      );
      expect(await getFieldValue('Adresregel 3:')).toEqual(
        checkedAddress.foreignAddress3
      );
    }

    expect(await getFieldValue('Burgerservicenummer')).toEqual(person.BSN);
    expect(await getFieldValue('Voornamen')).toEqual(person.firstNames);
    expect(await getFieldValue('Tussenvoegsel')).toEqual(person.insertions);
    environment === 'dev'
      ? expect(await getFieldValue('Achternaam')).toEqual(
          `${person.insertions} ${person.familyName}`
        )
      : expect(await getFieldValue('Achternaam')).toEqual(person.familyName);
    environment !== 'dev'
      ? expect(await getFieldValue('Adellijke titel')).toEqual(
          person.nobleTitle
        )
      : '';
    environment !== 'dev'
      ? expect(await getFieldValue('Geboortedatum')).toEqual(person.birthDate)
      : '';

    person.residenceCountry === 'Nederland' ||
    person.correspondenceCountry === 'Nederland'
      ? await checkDutchAddress()
      : await checkForeignAddress();

    await expect(formPage.getByLabel('Telefoonnummer').first()).toHaveValue(
      person.phoneNumber
    );
    await expect(formPage.getByLabel('Telefoonnummer (mobiel)')).toHaveValue(
      person.mobileNumber
    );
    await expect(formPage.getByLabel('E-mailadres')).toHaveValue(person.email);
  });

  await test.step('Goto first registration page', async () => {
    //  Click on next button to go to the first registration page
    const responsePromise = formPage.waitForResponse(
      `${baseUrl}zaak/create/webformulier/aanvrager`
    );
    const button = formPage.getByRole('button', { name: 'Volgende' });
    await button.scrollIntoViewIfNeeded();
    await button.press('Enter');
    await responsePromise;
    await formPage.waitForLoadState();
    await new Promise(rez => setTimeout(rez, shortTimeout));

    // When a previous fill in was stopped, because a test was failed, for instance
    // a screen comes forward that asks if the user wants to use the previously filled
    // in values or not
    const onCaseResetScreen = await formPage
      .getByText('Nee, ik wil alle gegevens opnieuw invullen')
      .isVisible({ timeout: shortTimeout });

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (onCaseResetScreen) {
      const nextButton = formPage.getByRole('button', { name: 'Volgende' });
      await nextButton.scrollIntoViewIfNeeded();
      await nextButton.press('Enter', { delay: typeDelay });
      await new Promise(rez => setTimeout(rez, shortTimeout));
      await formPage.waitForLoadState();
    }
  });
};
