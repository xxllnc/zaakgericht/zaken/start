import { BrowserContext, Page, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { personDataEnvironment, BSN2 } from '../../testfiles/personData';
import { searchCaseInSearchfield } from './searchCaseInZaakSysteem';
import { loginFormCheckPersonData } from './loginFormCheckPersonData';
import { fillOutFormMainTestCaseWithRules } from '../zs-form/fillOutFormMainTestCase';
import { locators } from '../../utils';
import { finishMainCase } from '../zs-employee/finishMainCase';

const { baseUrl, shortTimeout } = testSettings;
const MainTestCase = 'Auto TESTRONDE V2';
const BSN = BSN2;
const person = personDataEnvironment[BSN];

test.describe
  .serial(`Person fills with choice Ja, applies and pays form ${MainTestCase}. Employee treats case @all`, () => {
  let formContext: BrowserContext;
  let formPage: Page;

  // caseNumber is set in loginForm and it is used to check the payment
  let caseNumber = '';

  test.beforeAll(async ({ browser }) => {
    // page context as person that is logged in with DigiD
    formContext = await browser.newContext(testSettings.options);
    formPage = await formContext.newPage();
  });

  test.afterAll(async () => {
    await formContext.close();
  });

  test(`Login, fill out form, submit and pay ${MainTestCase} for choice Nee`, async () => {
    await loginFormCheckPersonData({
      formPage,
      loginFormName: MainTestCase,
      person,
      testSettings,
    });

    await fillOutFormMainTestCaseWithRules({
      formPage,
      formName: MainTestCase,
    });

    await test.step('Submit form and save caseNumber', async () => {
      await formPage.getByRole('button', { name: 'Versturen' }).press('Enter');
      let isVisible = false;
      await expect
        .poll(
          async () => {
            try {
              await expect(
                formPage.locator(locators.labelLeftOnForm('Zaaknummer'))
              ).toBeVisible({ timeout: shortTimeout });
              isVisible = true;
            } catch {
              /* empty */
            }
            return isVisible;
          },
          {
            // Probe, wait 1s, probe, wait 2s, probe, wait 5s, probe, wait 5s, probe, .... Defaults to [100, 250, 500, 1000].
            // intervals: [3_000, 2_000, 2_000],
            timeout: 40_000,
          }
        )
        .toBeTruthy();

      caseNumber = await formPage
        .locator(locators.labelLeftOnForm('Zaaknummer'))
        .innerText();

      expect(caseNumber.length).toBeGreaterThan(0);
    });

    await test.step(`Pay ${MainTestCase} for choice Ja`, async () => {
      await formPage
        .getByRole('button', { name: 'Betalen', exact: true })
        .click();
      await formPage.getByRole('button', { name: 'iDEAL' }).click();
      await formPage.getByTitle('Selecteer uw bank').selectOption('9999+TST');
      await formPage.getByRole('button', { name: 'Ga verder' }).click();
      await formPage.getByRole('button', { name: 'SUCCESS' }).click();
      await expect(
        formPage.getByRole('heading', { name: ' Uw betaling is gelukt' })
      ).toBeVisible();
    });
  });

  test.describe.serial('Check and finish case in zaaksysteem @all', () => {
    test.use({ storageState: authPathBeheerder });

    test('Go to zaaksysteem as employee', async ({ page }) => {
      await test.step('Open zaaksysteem', async () => {
        await page.goto(baseUrl);
        await expect(
          page.getByRole('button', { name: 'Hoofdmenu openen' })
        ).toBeVisible();
      });

      await searchCaseInSearchfield(page, caseNumber);

      await test.step(`Take case into treatment ${caseNumber}`, async () => {
        await page
          .getByRole('button', { name: 'In behandeling nemen', exact: true })
          .click();
      });

      await test.step(`Accept documents in case ${caseNumber}`, async () => {
        await page.getByRole('button', { name: 'Fase' }).hover();
        await page
          .getByRole('link', { name: ' Documenten in de wachtrij' })
          .click();
        await page.getByRole('button', { name: 'Accepteren' }).first().click();
        await page.waitForLoadState('domcontentloaded');
        await page.getByRole('button', { name: 'Accepteren' }).first().click();
        await page.waitForLoadState('domcontentloaded');
        await page.getByRole('button', { name: 'Accepteren' }).click();
        await page.waitForLoadState('domcontentloaded');
        await page.getByLabel('Fasen').click();
        await page.waitForLoadState('domcontentloaded');
        await expect(
          page.getByRole('link', { name: 'Open tab Taken' }).first()
        ).toBeVisible();
      });

      await finishMainCase({ page, mainCaseTypeName: MainTestCase });
    });
  });
});
