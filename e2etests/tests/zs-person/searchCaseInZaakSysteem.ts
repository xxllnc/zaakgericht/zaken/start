import { Page, expect, test } from '@playwright/test';
import { testSettings } from '../../playwright.config';

const { clickDelay } = testSettings;

export const searchCaseInZsLogboek = async (page: Page, caseNumber: string) => {
  await test.step('Search case', async () => {
    await page.getByRole('button', { name: 'Hoofdmenu openen' }).click();

    await expect(
      page.getByRole('menuitem', { name: ' Logboek' })
    ).toBeVisible();
    await page.getByRole('menuitem', { name: ' Logboek' }).click();

    await expect(page.getByPlaceholder('Zaaknummer')).toBeVisible();
    await page.getByPlaceholder('Zaaknummer').click();
    await page
      .getByPlaceholder('Zaaknummer')
      .pressSequentially(`${caseNumber.padStart(3, '0')}`);
    await expect(page.getByText('Component').first()).toBeVisible();
  });
};

export const searchCaseInSearchfield = async (
  page: Page,
  caseNumber: string
) => {
  await test.step('Search case in right searchfield', async () => {
    await page.getByLabel('Ga naar dashboard').click();
    await page.getByPlaceholder('Zoeken in xxllnc zaken').click();
    await page
      .getByPlaceholder('Zoeken in xxllnc zaken')
      .pressSequentially(`${caseNumber.padStart(4, '0')}`);
    await expect(
      page.getByText('Geen resultaten gevonden voor deze zoekopdracht')
    ).toBeHidden();
    await page
      .getByText(`${caseNumber}: `)
      .first()
      .click({ delay: clickDelay });
    await page.waitForLoadState('domcontentloaded');
    await expect(page.getByPlaceholder('Zoeken in xxllnc zaken')).toBeVisible();
  });
};
