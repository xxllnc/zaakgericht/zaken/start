import { intervalToDuration } from 'date-fns';

interface Props {
  milliseconds: number;
}

export const calcDuration = ({ milliseconds }: Props) => {
  const millisecondsLeft =
    milliseconds - Math.floor(milliseconds / 1000) * 1000;
  const duration = intervalToDuration({ start: 0, end: milliseconds });
  const durationFormatted = `${duration.minutes ?? 0}:${zeroPad(
    duration.seconds
  )}.${millisecondsLeft.toString()}`;

  return durationFormatted;
};

function zeroPad(number: number | undefined) {
  return String(number).padStart(2, '0');
}
