import { Page } from '@playwright/test';
import { testSettings } from '../playwright.config';

const { environment } = testSettings;

export const formLoginButton = (page: Page) =>
  // eslint-disable-next-line playwright/no-conditional-in-test
  environment === 'preprod'
    ? page.locator('//div[@class="prelogin-button"]')
    : environment === 'master'
      ? page.locator(
          '//div[@class="prelogin-button"]//strong[contains(.//text(),"Spoof")]'
        )
      : page
          .locator('button:has-text("spoof"), a:has-text("SAML")')
          .locator('nth=0');

export const pipLoginButton = (page: Page) =>
  page
    .locator(
      'a:has-text("Spoof"), button:has-text("spoof"), a:has-text("SAML")'
    )
    .locator('nth=0');

export const locatorPlaceholder =
  environment === 'dev'
    ? 'Zoek op adres (bv. Amsterdam-Duivendrecht, H.J.E. Wenckbachweg 90)'
    : 'Zoek op adres (bv. Amsterdam, Ellermanstraat 23)';

export const useCorrespondence = (
  hasCorrespondenceAddress: string,
  residenceAddressCity: string
) =>
  environment === 'development'
    ? !!(hasCorrespondenceAddress === 'true' && residenceAddressCity === '')
    : hasCorrespondenceAddress === 'true';
