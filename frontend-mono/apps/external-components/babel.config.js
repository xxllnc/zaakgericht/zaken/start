// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

module.exports = {
  presets: ['@babel/preset-typescript', '@babel/preset-react'],
  plugins: [],
};
