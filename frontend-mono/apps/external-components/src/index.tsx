// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

document.body.style.margin = '0';
//@ts-ignore
document.querySelector('#zs-app').style.height = '100vh';

const queryParam =
  new URLSearchParams(window.location.search).get('component') || '';

const noop = () => {};

const routes: Record<string, () => void> = {
  map: () => import('./map'),
  msedit: () => import('./MsEdit'),
};

const currentRoute = routes[queryParam] || noop;
currentRoute();

export default {};
