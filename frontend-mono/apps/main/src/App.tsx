// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { RouterProvider } from 'react-router';
import { LicenseInfo } from '@mui/x-license-pro';
import { QueryClientProvider } from '@tanstack/react-query';
import ThemeProvider from '@mintlab/ui/App/Material/ThemeProvider/ThemeProvider';
import LoginWrapper from '@zaaksysteem/common/src/components/LoginWrapper';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { HelpDialog } from '@mintlab/ui/App/Zaaksysteem/HelpDialog';
import router from './Routes';
import { queryClient } from './queryClient';
import Snackbar from './components/Snackbar';

if (process.env.MUI_LICENSE_KEY) {
  LicenseInfo.setLicenseKey(process.env.MUI_LICENSE_KEY);
}

function App() {
  const [t] = useTranslation();
  const [, addMessages] = useMessages();

  React.useEffect(() => {
    if (
      window.location.href.includes('/documents') &&
      window.location.href.includes('/external-components/exposed/case/')
    ) {
      try {
        //@ts-ignore
        const topStart = window.parent.document.body
          .querySelector('iframe')
          .getBoundingClientRect()
          .top.toFixed();
        document.body.style.height = `calc(100vh - ${topStart}px)`;
        document.body.style.setProperty('--topStart', topStart + 'px');
      } catch (err) {
        console.log(err);
      }
    }
    addMessages(
      //@ts-ignore
      t('common:snackMessages', {
        returnObjects: true,
      })
    );
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider>
        <LoginWrapper>
          <React.Suspense fallback={<Loader />}>
            <RouterProvider router={router} />
          </React.Suspense>
          <ServerErrorDialog />
          <Snackbar />
          <HelpDialog />
        </LoginWrapper>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
