// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteObject } from 'react-router-dom';
import { createBrowserRouter } from 'react-router-dom';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';

const lazifyComponents = (route: {
  path: string;
  Component: () => Promise<any>;
}): RouteObject => ({ ...route, Component: React.lazy(route.Component) });

const mainRoutes = {
  path: 'main/*',
  Component: React.lazy(() => import('@mintlab/ui/App/Zaaksysteem/Layout')),
  children: [
    {
      path: 'catalog/*',
      Component: () => import('./modules/catalog'),
    },
    {
      path: 'configuration/*',
      Component: () => import('./modules/configuration'),
    },
    {
      path: 'log/*',
      Component: () => import('./modules/log'),
    },
    {
      path: 'transactions/*',
      Component: () => import('./modules/transactions'),
    },
    {
      path: 'datastore/*',
      Component: () => import('./modules/datastore'),
    },
    {
      path: 'object-type/*',
      Component: () => import('./modules/objectTypeManagement'),
    },

    // {/* Admin modules iframing old frontend */}
    {
      path: 'integrations/*',
      Component: () => import('./modules/integrations'),
    },
    {
      path: 'users/*',
      Component: () => import('./modules/users'),
    },
    {
      path: 'case-type/*',
      Component: () => import('./modules/caseType'),
    },
    {
      path: 'case-type2/*',
      Component: () => import('./modules/caseTypeManagement'),
    },
    {
      path: 'object-type-v1/*',
      Component: () => import('./modules/objectType'),
    },
    {
      path: 'object-import/*',
      Component: () => import('./modules/objectImport'),
    },

    // {/* New modules */}
    {
      path: 'dashboard',
      Component: () => import('./modules/dashboard'),
    },
    {
      path: 'communication/*',
      Component: () => import('./modules/customerContact'),
    },
    {
      path: 'document-intake',
      Component: () => import('./modules/documentIntake'),
    },
    {
      path: 'advanced-search/*',
      Component: () => import('./modules/advancedSearch'),
    },
    {
      path: 'contact-search/*',
      Component: () => import('./modules/contactSearch'),
    },
    {
      path: 'export-files/*',
      Component: () => import('./modules/exportFiles'),
    },
    {
      path: 'contact-view/:type/:uuid/*',
      Component: () => import('./modules/contactView'),
    },
    {
      path: 'case/:caseUuid/*',
      Component: () => import('./modules/case'),
    },
    {
      path: 'object/:uuid/*',
      Component: () => import('./modules/objectView'),
    },
    //@ts-ignore
    ...(window.location.host === 'development.zaaksysteem.nl'
      ? [
          {
            path: 'styles/*',
            Component: () => import('./modules/customStyling'),
          },
        ]
      : []),

    // {/* views related to integrations */}
    {
      path: 'extern/next2know/*',
      Component: () => import('./modules/next2know'),
    },
  ].map(lazifyComponents),
};

const routesExposedToInternIframes = {
  path: 'external-components/exposed/*',
  children: [
    {
      path: 'case/:caseUuid/*',
      Component: () => import('./modules/case-components'),
    },
    {
      path: 'tasks/:widgetUuid',
      Component: () =>
        import('./modules/dashboard/components/widgets/Tasks/Tasks'),
    },
    {
      path: 'advanced-search/:widgetUuid',
      Component: () =>
        import(
          './modules/dashboard/components/widgets/AdvancedSearch/AdvancedSearch'
        ),
    },
    {
      path: 'email-template-preview',
      Component: () =>
        import('./components/EmailTemplatePreviewer/EmailTemplatePreviewer'),
    },
    {
      path: 'webodf',
      Component: () => import('./components/WebOdfEditor/WebOdfEditor'),
    },
    {
      path: 'pdfjs',
      Component: () => import('./components/PDFJSDocumentPreview'),
    },
  ].map(lazifyComponents),
};

const router = createBrowserRouter([
  {
    path: '*',
    //@ts-ignore
    children: [mainRoutes, routesExposedToInternIframes].map(
      (route: RouteObject) => ({
        ErrorBoundary: ErrorBoundary,
        ...route,
      })
    ),
  },
]);

export default router;
