// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { openSnackbar } from '@zaaksysteem/common/src/signals';
const requestCounts = new Map();

export const checkForExports = () => {
  const runningExportId = localStorage.getItem('runningExportId');
  const reqCount = requestCounts.get(runningExportId) || 0;

  if (runningExportId && reqCount < 25) {
    requestCounts.set(runningExportId, reqCount + 1);

    fetch(`/api/v2/cm/export/get_export?uuid=${runningExportId}`)
      .then(res2 => res2.json())
      .then(res2 => {
        const token =
          res2.data && res2.data.attributes && res2.data.attributes.token;

        if (token) {
          localStorage.removeItem('runningExportId');
          openSnackbar({
            message: 'snacks.zipReady',
            link: `/api/v2/cm/export/download_export_file?token=${token}`,
            download: true,
            autoHideDuration: null,
          });
        }
      });
  }

  setTimeout(checkForExports, 1000);
};
