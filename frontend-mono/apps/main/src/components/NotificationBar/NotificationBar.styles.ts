// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useNotificationBarStyles = makeStyles(
  ({ palette: { sahara }, mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      borderTop: `1px solid ${greyscale.darker}`,
    },
    notificationWrapper: {
      borderBottom: `1px solid ${greyscale.darker}`,
      backgroundColor: sahara.lightest,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    notification: {
      margin: '10px 20px',
    },
    message: {
      display: '-webkit-box',
      '-webkit-line-clamp': 5,
      '-webkit-box-orient': 'vertical',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  })
);
