// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Typography from '@mui/material/Typography';
import { webOdfLocale } from './WebOdfEditor.locale';
import { useWebodfEditorStyles } from './WebOdfEditor.style';
import WebOdfToolbar from './WebOdfToolbar';
import {
  applyOdfCss,
  closeEditor,
  convertBlobToBase64,
  startOdfSession,
  useWebodfLock,
} from './WebOdfEditor.library';

let controllers = {
  formattingController: null as any,
  sessionController: null as any,
  canvas: null as any,
};

const WebOdfEditor = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const canvasRef = React.createRef<HTMLDivElement>();
  const classes = useWebodfEditorStyles();
  const [activeFormatting, setActiveFormatting] = React.useState();
  const fileId = urlParams.get('fileId');
  const title = decodeURIComponent(urlParams.get('title') || '');

  useWebodfLock(fileId || '');

  React.useEffect(() => {
    const style = applyOdfCss();
    const script = document.createElement('script');
    document.body.appendChild(script);
    script.src = '/webodf/webodf.js';
    script.type = 'text/javascript';

    const fileUrl = `/zaak/${urlParams.get(
      'caseId'
    )}/document/${fileId}/download/odt`;
    script.addEventListener('load', () => {
      //@ts-ignore
      const odf = window.odf;
      const odfCanvas = new odf.OdfCanvas(canvasRef.current);

      fetch(fileUrl)
        .then(re => re.blob())
        .then(convertBlobToBase64)
        .then(blobUrl => {
          odfCanvas.load(
            blobUrl.replace('octet-stream', 'vnd.oasis.opendocument.text')
          );
          setTimeout(() => {
            const cont = startOdfSession(odfCanvas, setActiveFormatting);
            controllers.canvas = odfCanvas;
            controllers.sessionController = cont.sessionController;
            controllers.formattingController = cont.formattingController;
          }, 0);
        });
    });

    return () => {
      style.remove();
    };
  }, []);

  return (
    <React.Fragment>
      <div className={classes.menu}>
        <div className={classes.titleContainer}>
          <Typography variant="h2">{title}</Typography>
          <IconButton onClick={closeEditor}>
            <Icon size="small">{iconNames.close}</Icon>
          </IconButton>
        </div>
        <div className={classes.toolbarContainer}>
          <div className={classes.toolbar}>
            <I18nResourceBundle resource={webOdfLocale} namespace="WebodfMenu">
              <WebOdfToolbar
                title={title}
                activeFormatting={activeFormatting}
                controllers={controllers}
                fileId={urlParams.get('fileId') || ''}
                uuid={urlParams.get('uuid') || ''}
                caseUuid={urlParams.get('caseUuid') || ''}
              />
            </I18nResourceBundle>
          </div>
        </div>
      </div>
      <div className={classes.canvasContainer}>
        <div ref={canvasRef} />
      </div>
    </React.Fragment>
  );
};

export default WebOdfEditor;
