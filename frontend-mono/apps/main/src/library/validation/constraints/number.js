// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { hasValue } from '../value';

/**
 * @param {*} userInput
 * @return {boolean}
 */
const isValidNumber = userInput =>
  Number.isFinite(userInput) && !Number.isNaN(userInput);

/**
 * @param {*} userInput
 * @param {Object} config
 * @param {string} config.type
 * @return {boolean}
 */
const passesType = (userInput, config) => {
  if (config) {
    const { type } = config;
    if (type === 'integer') {
      return /^-?\d+$/.test(userInput);
    }
  }
  return true;
};

/**
 * @param {*} userInput
 * @param {Object} config
 * @returns {boolean}
 */
export const number = (userInput, config) => {
  if (!hasValue(userInput)) {
    return true;
  }

  return isValidNumber(userInput) && passesType(userInput, config);
};
