// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const main = {
  nl: {
    aria: {
      adminIframe: 'Beheer formulier',
    },
    validation: {
      required: 'Voer een geldige waarde in.',
      number: 'Voer een numerieke waarde in.',
      email: 'Voer een geldig e-mailadres in.',
      uri: 'Voer een geldige URL in.',
    },
    validations: {
      mixed: {
        required: 'Vul een geldige waarde in.',
      },
      string: {
        invalidMagicString:
          'Deze Magic String is al in gebruik of ongeldig. Suggestie: {{suggestion}}.',
      },
      email: {
        invalidEmailAddress:
          'Er is geen geldig e-mailadres ingevuld. Suggestie: naam@example.com',
        invalidEmailAddressOrMagicString:
          "'${value}' is geen geldig e-mailadres of magicstring. Suggestie: naam@example.com of [[ magic_string ]]",
      },
      array: {
        noFile: 'Geen bestand(en) geüpload. Voeg een bestand toe.',
        invalidFile: 'Ongeldig(e) bestand(en) geüpload. Probeer het opnieuw.',
      },
    },
    form: {
      choose: 'Maak een keuze…',
      cannotChangeLater: 'Deze waarde kan later niet meer worden gewijzigd.',
    },
  },
};

export default main;
