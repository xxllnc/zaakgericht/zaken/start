// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import { Routes, Route, useParams } from 'react-router-dom';
import { useQueryClient } from '@tanstack/react-query';
import { useStyles } from './styles/AdvancedSearch.styles';
import LeftBar from './components/LeftBar/LeftBar';
import Main from './components/Main/Main';
import { KindType, AdvancedSearchParamsType } from './AdvancedSearch.types';
import { invalidateAllQueries } from './query/library';
import ExpandCollapseButton from './components/Main/ExpandCollapseButton';
import { localStateContext } from './context/LocalStateProvider';

/* eslint complexity: [2, 10] */
const AdvancedSearch: React.FunctionComponent = () => {
  const classes = useStyles();
  const [t] = useTranslation('search');
  const params = useParams<
    keyof AdvancedSearchParamsType
  >() as AdvancedSearchParamsType;
  const { kind } = params;
  const client = useQueryClient();
  const [localState] = useContext(localStateContext);
  const expanded = localState.UIState.searchesBarExpanded;
  useTransition(
    (prevKind: KindType) => {
      if (prevKind && kind !== prevKind) {
        invalidateAllQueries(client);
      }
    },
    [kind]
  );

  return (
    <div className={classes.wrapper}>
      <div className={classes.content}>
        <LeftBar
          kind={kind}
          classes={classes}
          client={client}
          //@ts-ignore
          t={t}
          params={params}
          expanded={localState.UIState.searchesBarExpanded}
        />
        <section
          className={classNames(classes.mainContent, {
            [classes.expanded]: expanded,
          })}
        >
          <Main classes={classes} t={t as any} />
        </section>
        <ExpandCollapseButton
          classes={classes}
          t={t as any}
          expanded={localState.UIState.searchesBarExpanded}
        />
      </div>
    </div>
  );
};

export default (props: any) => (
  <Routes>
    <Route path=":mode?/*" element={<AdvancedSearch {...props} />}>
      <Route path=":identifier" />
    </Route>
  </Routes>
);
