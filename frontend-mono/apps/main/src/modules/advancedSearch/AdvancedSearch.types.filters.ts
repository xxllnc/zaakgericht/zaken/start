// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { ContactType, OperatorType } from './AdvancedSearch.types';

export type FilterTypeBaseType = { uuid: string };

export type FilterTypesTypeCombined =
  | FilterType['type']
  | CustomAttributeTypes['type'];

export type FilterCustomAttributeType = FilterTypeBaseType & {
  type: 'attributes.value';
  values: {
    additionalData?: any;
    label?: string;
    operator?: 'eq' | 'ne';
    attributeUuid: string;
    magicString: string;
  } & CustomAttributeTypes;
};

export type FilterKeywordType = FilterTypeBaseType & {
  type: 'keyword';
  operator: 'and' | 'or';
  values: ValueType<string>[] | null;
};

export type FilterSubjectType = FilterTypeBaseType & {
  type: 'attributes.subject';
  operator: 'and' | 'or';
  values: ValueType<string>[] | null;
};
export type FilterRequestorZipcode = FilterTypeBaseType & {
  type: 'relationship.requestor.zipcode';
  operator: 'or';
  values: ValueType<string>[] | null;
};
export type FilterRequestorCoCNumberType = FilterTypeBaseType & {
  type: 'relationship.requestor.coc_number';
  operator: 'or';
  values: ValueType<string>[] | null;
};
export type FilterExternalReferenceType = FilterTypeBaseType & {
  type: 'attributes.external_reference';
  operator: 'or' | 'and';
  values: ValueType<string>[] | null;
};

export type FilterCasePhaseType = FilterTypeBaseType & {
  type: 'attributes.case_phase';
  operator: 'or';
  values: ValueType<string>[] | null;
};

export type FilterParentNumberType = FilterTypeBaseType & {
  type: 'attributes.parent_number';
  operator: 'or';
  values: ValueType<number>[] | null;
};

export type FilterCasePriceType = FilterTypeBaseType & {
  type: 'attributes.case_price';
  operator: 'and' | 'or';
  values: NumberEntryType[];
};

export type FilterCustomNumberType = FilterTypeBaseType & {
  type: 'attributes.custom_number';
  operator: 'or';
  values: ValueType<number>[] | null;
};

export type FilterCaseNumberType = FilterTypeBaseType & {
  type: 'attributes.case_number';
  operator: 'or';
  values: ValueType<string>[] | null;
  options?: {
    truncate?: boolean;
  };
};

export type FilterNameType = FilterTypeBaseType & {
  type: 'name';
  values: string | null;
};

export type FilterLastModifiedType = FilterTypeBaseType & {
  type: 'attributes.last_modified';
  values: DateEntryType[] | null;
};

export type FilterRegistrationDateType = FilterTypeBaseType & {
  type: 'attributes.registration_date';
  values: DateEntryType[] | null;
};

export type FilterDestructionDateType = FilterTypeBaseType & {
  type: 'attributes.destruction_date';
  values: DateEntryType[] | null;
};

export type FilterStalledSinceType = FilterTypeBaseType & {
  type: 'attributes.stalled_since';
  values: DateEntryType[] | null;
};

export type FilterStalledUntillType = FilterTypeBaseType & {
  type: 'attributes.stalled_until';
  values: DateEntryType[] | null;
};

export type FilterTargetDateType = FilterTypeBaseType & {
  type: 'attributes.target_date';
  values: DateEntryType[] | null;
};

export type FilterCompletionDateType = FilterTypeBaseType & {
  type: 'attributes.completion_date';
  values: DateEntryType[] | null;
};

export type FilterStatusType = FilterTypeBaseType & {
  type: 'attributes.status';
  values: CustomObjectStatusType[] | CaseStatusType[] | null;
};

export type FilterArchiveStatusType = FilterTypeBaseType & {
  type: 'attributes.archive_status';
  values: ArchiveStatusType | null;
};

export type FilterTypeOfArchiving = FilterTypeBaseType & {
  type: 'attributes.type_of_archiving';
  values: TypeOfArchivingType | null;
};

export type FilterCustomObjectType = FilterTypeBaseType & {
  type: 'relationship.custom_object_type';
  values: string;
};

export type FilterArchivalStateType = FilterTypeBaseType & {
  type: 'attributes.archival_state';
  values: string | null;
};

export type FilterRequestorInvestigationType = FilterTypeBaseType & {
  type: 'relationship.requestor.investigation';
  values: string | null;
};

export type FilterRequestorIsSecretType = FilterTypeBaseType & {
  type: 'relationship.requestor.is_secret';
  values: string | null;
};

export type FilterAssigneeType = FilterTypeBaseType & {
  type: 'relationship.assignee.id';
  values: {
    label: string;
    value: string;
    type: ContactType;
  }[];
};

export type FilterRequestorType = FilterTypeBaseType & {
  type: 'relationship.requestor.id';
  values: {
    label: string;
    value: string;
    type?: ContactType;
  }[];
};

export type FilterUrgencyType = FilterTypeBaseType & {
  type: 'attributes.urgency';
  values: {
    label: string;
    value: string;
  };
};

export type FilterCoordinatorType = FilterTypeBaseType & {
  type: 'relationship.coordinator.id';
  values: {
    label: string;
    value: string;
  }[];
};

export type FilterCaseTypeType = FilterTypeBaseType & {
  type: 'relationship.case_type.id';
  values: {
    label: string;
    value: string;
  }[];
};

export type FilterPaymentStatusType = FilterTypeBaseType & {
  type: 'attributes.payment_status';
  values: string[];
};

export type FilterDepartmentRoleType = FilterTypeBaseType & {
  type: 'attributes.department_role';
  values: {
    department: {
      value: string;
      label: string;
    } | null;
    role: {
      value: string;
      label: string;
    } | null;
  }[];
};

export type FilterChannelOfContactType = FilterTypeBaseType & {
  type: 'attributes.channel_of_contact';
  values: string[];
};

export type FilterResultType = FilterTypeBaseType & {
  type: 'attributes.result';
  values: string[];
};

export type FilterCaseLocationType = FilterTypeBaseType & {
  type: 'attributes.case_location';
  values: {
    label: string;
    value: string;
    id: string;
    type: 'nummeraanduiding' | 'openbareruimte';
  }[];
};

export type FilterCorrespondenceAddressType = FilterTypeBaseType & {
  type: 'attributes.correspondence_address';
  values: {
    label: string;
    value: string;
    id: string;
    type: 'nummeraanduiding' | 'openbareruimte';
  }[];
};

export type FilterConfidentialityType = FilterTypeBaseType & {
  type: 'attributes.confidentiality';
  values: string[];
};

export type FilterHasUnacceptedChangesType = FilterTypeBaseType & {
  type: 'attributes.has_unaccepted_changes';
  values: Array<'messages' | 'files' | 'updates'>;
};

export type FilterPeriodOfPreservationActiveType = FilterTypeBaseType & {
  type: 'attributes.period_of_preservation_active';
  values: 'active' | 'not-active';
};

export type FilterRetentionPeriodSourceDate = FilterTypeBaseType & {
  type: 'attributes.retention_period_source_date';
  values: string[];
};

export type FilterCaseTypeVersionType = FilterTypeBaseType & {
  type: 'relationship.case_type.version';
  values: CaseTypeVersionValueType[];
};

export type FilterCaseTypeIdentificationType = FilterTypeBaseType & {
  type: 'relationship.case_type.identification';
  values: ValueType<string>[];
};

export type FilterCaseTypeConfidentialityType = FilterTypeBaseType & {
  type: 'relationship.case_type.confidentiality';
  values: string[];
};

export type FilterCocLocationNumberType = FilterTypeBaseType & {
  type: 'relationship.requestor.coc_location_number';
  values: ValueType<string>[];
};

export type FilterNobleTitleType = FilterTypeBaseType & {
  type: 'relationship.requestor.noble_title';
  values: ValueType<string>[];
};

export type FilterRequestorTradeNameType = FilterTypeBaseType & {
  type: 'relationship.requestor.trade_name';
  values: ValueType<string>[];
};

export type FilterRequestorGenderType = FilterTypeBaseType & {
  type: 'relationship.requestor.gender';
  values: string[];
};
export type FilterRequestorDepartmentType = FilterTypeBaseType & {
  type: 'relationship.requestor.department';
  values: ValueType<string>[];
};
export type FilterCorrespondenceZipcodeType = FilterTypeBaseType & {
  type: 'relationship.requestor.correspondence_zipcode';
  values: ValueType<string>[];
};
export type FilterCorrespondenceStreetType = FilterTypeBaseType & {
  type: 'relationship.requestor.correspondence_street';
  values: ValueType<string>[];
};
export type FilterRequestorCorrespondenceCityType = FilterTypeBaseType & {
  type: 'relationship.requestor.correspondence_city';
  values: ValueType<string>[];
};
export type FilterRequestorStreetType = FilterTypeBaseType & {
  type: 'relationship.requestor.street';
  values: ValueType<string>[];
};

export type FilterRequestorCityType = FilterTypeBaseType & {
  type: 'relationship.requestor.city';
  values: ValueType<string>[];
};

export type FilterRequestorDateOfBirthType = FilterTypeBaseType & {
  type: 'relationship.requestor.date_of_birth';
  values: DateEntryType[];
};

export type FilterRequestorDateOfMarriageType = FilterTypeBaseType & {
  type: 'relationship.requestor.date_of_marriage';
  values: DateEntryType[];
};

export type FilterRequestorDateOfDeathType = FilterTypeBaseType & {
  type: 'relationship.requestor.date_of_death';
  values: DateEntryType[];
};

export type FilterIntakeType = FilterTypeBaseType & {
  type: 'intake';
  values: boolean;
};

export type FilterObjectUUIDType = FilterTypeBaseType & {
  type: 'relationship.custom_object.id';
  values: ValueType<string>[];
};

export type CustomAttributeTypes =
  | {
      type: 'address_v2';
      values: ValueType<string>[] | null;
    }
  | {
      type: 'bag_adres';
      values: ValueType<string> | null;
    }
  | {
      type: 'bag_adressen';
      values: ValueType<string> | null;
    }
  | {
      type: 'bag_adres';
      values: ValueType<string> | null;
    }
  | {
      type: 'bag_straat_adres';
      values: ValueType<string> | null;
    }
  | {
      type: 'bag_straat_adressen';
      values: ValueType<string> | null;
    }
  | {
      type: 'bag_openbareruimte';
      values: ValueType<string> | null;
    }
  | {
      type: 'bag_openbareruimtes';
      values: ValueType<string> | null;
    }
  | {
      type: 'checkbox';
      values: string[];
    }
  | {
      type: 'option';
      values: string[];
    }
  | {
      type: 'select';
      values: string[];
    }
  | {
      type: 'relationship';
      values: string[] | ValueType<string>[] | null;
    }
  | {
      type: 'date';
      values: DateEntryType[] | null;
    }
  | {
      type: 'email';
      values: ValueType<string> | null;
    }
  | {
      type: 'numeric';
      values: ValueType<string>[] | null;
    }
  | {
      type: 'bankaccount';
      values: ValueType<string>[] | null;
    }
  | {
      type: 'valuta';
      values: NumberEntryType[];
    }
  | {
      type: 'valutaex';
      values: NumberEntryType[];
    }
  | {
      type: 'valutaex21';
      values: NumberEntryType[];
    }
  | {
      type: 'valutaex6';
      values: NumberEntryType[];
    }
  | {
      type: 'valutain';
      values: NumberEntryType[];
    }
  | {
      type: 'valutain21';
      values: NumberEntryType[];
    }
  | {
      type: 'valutain6';
      values: NumberEntryType[];
    }
  | {
      type: 'file';
      values?: undefined | null;
    }
  | {
      type: 'text';
      values: ValueType<string>[] | null;
    }
  | {
      type: 'textarea';
      values: ValueType<string>[] | null;
    }
  | {
      type: 'richtext';
      values: ValueType<string>[] | null;
    };

export type CaseTypeVersionValueType = {
  casetype_uuid: string;
  casetype_name: string;
  casetype_version_number: number;
  casetype_version_date: Date;
  casetype_version_uuid: string;
};

export type CustomObjectStatusType = 'active' | 'inactive' | 'draft';
export type CaseStatusType = 'new' | 'stalled' | 'resolved' | 'open';
export type ArchiveStatusType = 'archived' | 'to destroy' | 'to preserve';
export type TypeOfArchivingType =
  | 'Bewaren (B)'
  | 'Conversie'
  | 'Migratie'
  | 'Overbrengen (O)'
  | 'Overdracht'
  | 'Publicatie'
  | 'Vernietigen (V)'
  | 'Vernietigen'
  | 'Vervallen beperkingen openbaarheid';
export type NumberEntryType = [OperatorType, Number];

export type DateEntryTypeAbsolute = {
  type: 'absolute';
  operator: OperatorType | null;
  value: string | null; // ISO string
};

export type DateEntryTypeRelative = {
  type: 'relative';
  operator: OperatorType | null;
  value: string | null; // Duration string
};

export type DateEntryTypeRange = {
  type: 'range';
  startValue: string | null;
  endValue: string | null;
  timeSetByUser: boolean;
};

export type DateEntryType =
  | DateEntryTypeAbsolute
  | DateEntryTypeRelative
  | DateEntryTypeRange;

export type FilterType =
  | FilterCustomAttributeType
  | FilterKeywordType
  | FilterCasePhaseType
  | FilterSubjectType
  | FilterRequestorZipcode
  | FilterRequestorCoCNumberType
  | FilterExternalReferenceType
  | FilterCasePhaseType
  | FilterParentNumberType
  | FilterCasePriceType
  | FilterCustomNumberType
  | FilterCaseNumberType
  | FilterNameType
  | FilterLastModifiedType
  | FilterRegistrationDateType
  | FilterDestructionDateType
  | FilterStalledSinceType
  | FilterStalledUntillType
  | FilterTargetDateType
  | FilterCompletionDateType
  | FilterStatusType
  | FilterArchiveStatusType
  | FilterCustomObjectType
  | FilterArchivalStateType
  | FilterTypeOfArchiving
  | FilterRequestorInvestigationType
  | FilterRequestorIsSecretType
  | FilterAssigneeType
  | FilterRequestorType
  | FilterUrgencyType
  | FilterCoordinatorType
  | FilterCaseTypeType
  | FilterPaymentStatusType
  | FilterDepartmentRoleType
  | FilterChannelOfContactType
  | FilterResultType
  | FilterCaseLocationType
  | FilterCorrespondenceAddressType
  | FilterConfidentialityType
  | FilterHasUnacceptedChangesType
  | FilterPeriodOfPreservationActiveType
  | FilterRetentionPeriodSourceDate
  | FilterCaseTypeVersionType
  | FilterCaseTypeIdentificationType
  | FilterCaseTypeConfidentialityType
  | FilterCocLocationNumberType
  | FilterNobleTitleType
  | FilterRequestorTradeNameType
  | FilterRequestorGenderType
  | FilterRequestorDepartmentType
  | FilterCorrespondenceZipcodeType
  | FilterCorrespondenceStreetType
  | FilterRequestorCorrespondenceCityType
  | FilterRequestorStreetType
  | FilterRequestorCityType
  | FilterRequestorDateOfBirthType
  | FilterRequestorDateOfMarriageType
  | FilterRequestorDateOfDeathType
  | FilterIntakeType
  | FilterObjectUUIDType;

export type FiltersType = {
  operator?: 'and' | 'or';
  kindType?: any;
  filters: FilterType[];
};
