// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import type { GridColDef } from '@mui/x-data-grid-pro';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Geojson } from '@mintlab/ui/types/MapIntegration';
import { FiltersType } from './AdvancedSearch.types.filters';

export type AdvancedSearchParamsType = {
  kind: KindType;
  identifier?: IdentifierType;
  mode: ModeType;
};

export type CustomFieldType = {
  type: string;
  magicString: string;
  label: string;
  name?: string;
  uuid?: string;
  value?: any;
};

export type ColumnType = {
  uuid: string;
  type: string;
  label: string;
  source: string[];
  magicString?: string;
  visible?: boolean;
  systemColumn?: boolean;
  includes?: string[];
  columnProps?: Partial<GridColDef>;
};

export type CapabilitiesType = {
  summary?: boolean;
  assigneeControls?: boolean;
  checkboxSelection?: boolean;
};

export type AuthorizationsType = 'read' | 'readwrite' | 'admin';

export type LabelsType = string[];

export type SavedSearchType = {
  uuid?: string | null;
  name: string;
  template: 'standard' | 'archive_export';
  filters: FiltersType | null;
  permissions: PermissionType[];
  columns: ColumnType[];
  authorizations: AuthorizationsType[];
  sortColumn?: string;
  sortOrder?: 'asc' | 'desc';
  labels: LabelsType;
  metaData: {
    createdByUuid: string;
    createdByName: string;
    createdByDate: string;
    lastEditedByUuid: string;
    lastEditedByName: string;
    lastEditedByDate: string;
  } | null;
};

export type EditFormStateType = {
  type: 'user' | 'default';
  selectedObjectType?: ValueType<string> | null;
  selectedFilter?: ValueType<string> | null;
} & SavedSearchType;

export type ErrorType = {
  uuid: string;
  value: string;
};

export type ContactType = 'employee' | 'person' | 'organization';
export type RelationshipType = 'coordinator' | 'requestor' | 'assignee';

export type KindType = 'custom_object' | 'case';
export type ModeType = 'view' | 'edit' | 'new' | null;
export type ViewType = 'table' | 'map' | null;
export type IdentifierType = string;
export type APIValueContextType = 'results' | 'save';

export type PermissionType = {
  groupID: string | null;
  roleID: string | null;
  writePermission: boolean;
  saved: boolean;
};

export type SortDirectionType = 'asc' | 'desc';

export type ResultRowType = {
  columns: { [key: string]: any };
  uuid: string;
  versionIndependentUuid: string;
  geoFeatures?: Geojson[];
  [key: string]: any;
};

export type ClassesType = {
  [key: string]: any;
};

export type SnackType = {
  message: string | null;
  open: boolean;
  color?: string;
};

export type OperatorType = 'lt' | 'gt' | 'le' | 'ge' | 'eq';

export type ExportResultsRowType = {
  label: string;
  value: any;
  uniqueIdentifier: string;
}[];

export type ExportFormatType = 'CSV' | 'TSV';
export type ParseResultsModeType = 'screen' | 'export' | 'raw';

export type GetResultsReturnType = {
  data: any[];
  included?: APICaseManagement.SearchCaseResponseBody['included'];
  meta?: APICaseManagement.SearchCaseResponseBody['meta'];
  page: number;
};

export type ValidateFuncType<T = any> = (value: T) => ErrorType | undefined;

export type DateModeType = 'date' | 'datetime';

export type APISearchRowType =
  | APICaseManagement.SearchCustomObjectsResponseBody['data'][0]
  | APICaseManagement.SearchCaseResponseBody['data'][0];

export type LocalStateType = {
  UIState: {
    searchesBarExpanded: boolean;
    [key: string]: any;
  };
  [key: string]: any;
};

export type CursorCollectionType = {
  current?: string | null;
  next?: string | null;
  previous?: string | null;
};

export type SkipDirectionType = 'forward' | 'back';
