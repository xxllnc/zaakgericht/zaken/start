// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { FormikProps } from 'formik';
import { UseQueryResult } from '@tanstack/react-query';
import * as i18next from 'i18next';
import { v4 } from 'uuid';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession/useSession.types';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import {
  ModeType,
  SavedSearchType,
  EditFormStateType,
  KindType,
  ColumnType,
  CustomFieldType,
} from '../../AdvancedSearch.types';
import { flat } from '../../library/library';
import {
  getSystemAttributes,
  getRelationshipAttributes,
  getCaseAttributes,
  getCaseTypeAttributes,
  getResultAttributes,
  getCaseLocationAttributes,
} from '../../library/attributes';

type GetInitialStateArgsType = {
  mode: ModeType;
  currentQuery?: UseQueryResult<SavedSearchType, V2ServerErrorsType>;
  kind: any;
  t: i18next.TFunction;
};

/* eslint complexity: [2, 12] */
export const getInitialState = ({
  mode,
  currentQuery,
  kind,
  t,
}: GetInitialStateArgsType): EditFormStateType => {
  const newState = getNewState({ kind, t });

  if (mode === 'edit') {
    const savedSearch = currentQuery?.data as SavedSearchType;
    const {
      uuid,
      name,
      template,
      permissions,
      columns,
      sortColumn,
      sortOrder,
      labels,
      metaData,
      authorizations,
    } = savedSearch;

    // ObjectType is not treated as a filter in the form, but rather
    // as a (seperate) required field, so they get split up here.
    const sanitizedFilters = savedSearch?.filters
      ? savedSearch.filters?.filters.filter(
          el => el.type !== 'relationship.custom_object_type'
        )
      : [];
    const objectTypeFilter = savedSearch?.filters?.filters.find(
      el => el.type === 'relationship.custom_object_type'
    );

    return savedSearch
      ? {
          uuid,
          template: template || 'standard',
          name: name || '',
          type: 'user',
          authorizations,
          filters: {
            ...savedSearch.filters,
            filters: sanitizedFilters,
          },
          permissions,
          columns,
          sortColumn,
          sortOrder,
          labels,
          metaData,
          selectedObjectType: objectTypeFilter
            ? (objectTypeFilter.values as unknown as ValueType<string>)
            : null,
          selectedFilter: null,
        }
      : newState;
  } else {
    return newState;
  }
};

export const validateForm = (formik: FormikProps<EditFormStateType>) => {
  setTimeout(() => {
    formik.validateForm();
  }, 75);
};

export const isFormValid = ({ saveMutation, formik }: any) => {
  if (!formik.dirty) return false;
  if (saveMutation.isLoading) return true;
  const flattened = {};
  flat(formik.errors, flattened);
  return !Object.entries(flattened).length;
};

export const getDefaultSavedSearches = ({
  kind,
  t,
  session,
}: {
  kind: KindType;
  t: i18next.TFunction;
  session: SessionType;
}): EditFormStateType[] => {
  const newState = getNewState({ kind, t });
  if (kind === 'case') {
    return [
      {
        ...newState,
        name: t('defaultSearches.allcases'),
        type: 'default',
        uuid: 'allcases',
      },
      {
        ...newState,
        name: t('defaultSearches.mycases'),
        type: 'default',
        uuid: 'mycases',
        ...(session && {
          filters: {
            operator: 'and',
            filters: [
              {
                type: 'relationship.assignee.id',
                uuid: v4(),
                values: [
                  {
                    label: session.logged_in_user.display_name as string,
                    value: session.logged_in_user.uuid as string,
                    type: 'employee',
                  },
                ],
              } as any,
              {
                type: 'attributes.status',
                uuid: v4(),
                values: ['open'],
              },
            ],
          },
        }),
      },
      {
        ...newState,
        name: t('defaultSearches.intake'),
        type: 'default',
        uuid: 'intake',
        ...(session && {
          filters: {
            operator: 'and',
            filters: [
              {
                type: 'intake',
                uuid: v4(),
                values: true,
              },
            ],
          },
        }),
      },
    ];
  } else {
    return [];
  }
};

export const getNewState = ({
  kind,
  t,
}: {
  kind: KindType;
  t: i18next.TFunction;
}) => {
  const allColumns = getColumns(kind, null, t);
  const columns = getPrefilledColumns({
    kind,
    allColumns,
  });
  return {
    name: '',
    template: 'standard',
    type: 'user',
    uuid: null,
    filters: {
      operator: 'and',
      filters: [],
    },
    permissions: [],
    columns,
    sortColumn: '',
    sortOrder: 'desc',
    labels: [],
    metaData: null,
    authorizations: ['read', 'readwrite', 'admin'],
    selectedObjectType: null,
    selectedFilter: null,
  } as EditFormStateType;
};

export const getCategories = (kind: KindType, t?: i18next.TFunction) => {
  if (kind === 'custom_object') {
    return {
      default: [
        'versionIndependentUuid',
        'title',
        'subtitle',
        'externalReference',
        'dateCreated',
        'lastModified',
      ],
    };
  } else if (kind === 'case' && t) {
    return {
      systemAttributes: getSystemAttributes(t).map(fields => fields[0]),
      requestor: getRelationshipAttributes('requestor', t).map(
        fields => fields[0]
      ),
      assignee: getRelationshipAttributes('assignee', t).map(
        fields => fields[0]
      ),
      case: getCaseAttributes(t).map(fields => fields[0]),
      caseType: getCaseTypeAttributes(t).map(fields => fields[0]),
      caseLocation: getCaseLocationAttributes(t).map(fields => fields[0]),
      result: getResultAttributes(t).map(fields => fields[0]),
    };
  } else {
    return [];
  }
};

export const getDefaultColumns = (
  kind: KindType,
  t: i18next.TFunction
): ColumnType[] => {
  let columns: Array<string | Array<string>>[];

  if (kind === 'custom_object') {
    columns = [
      [
        'versionIndependentUuid',
        t('attributes:objectUUID'),
        ['attributes', 'version_independent_uuid'],
      ],
      ['title', t('attributes:title'), ['attributes', 'title']],
      ['subtitle', t('attributes:subtitle'), ['attributes', 'subtitle']],
      [
        'externalReference',
        t('attributes:externalReference'),
        ['attributes', 'external_reference'],
      ],
      [
        'dateCreated',
        t('attributes:dateCreated'),
        ['attributes', 'date_created'],
      ],
      [
        'lastModified',
        t('attributes:lastModified'),
        ['attributes', 'last_modified'],
      ],
    ];
  } else {
    columns = [
      ...getSystemAttributes(t),
      ...getCaseAttributes(t),
      ...getCaseTypeAttributes(t),
      ...getRelationshipAttributes('assignee', t),
      ...getRelationshipAttributes('requestor', t),
      ...getCaseLocationAttributes(t),
      ...getResultAttributes(t),
    ];
  }

  return columns.map(entry => ({
    uuid: v4(),
    type: entry[0] as string,
    label: entry[1] as string,
    source: entry[2] as Array<string>,
    visible: true,
  }));
};

export const getPrefilledColumns = ({
  kind,
  allColumns,
}: {
  kind: KindType;
  allColumns: ColumnType[];
}) => {
  const columnTypes =
    kind === 'custom_object'
      ? //@ts-ignore
        getCategories(kind).default
      : [
          'caseNumber',
          'caseStatus',
          'caseProgress',
          'caseTypeTitle',
          'relationship.requestor.summary',
          'extraInformationInternal',
          'days',
        ];

  return columnTypes.map((type: string) =>
    allColumns.find((column: ColumnType) => type === column.type)
  );
};

export const getColumns = (
  kind: KindType,
  customFields: CustomFieldType[] | null,
  t: i18next.TFunction
): ColumnType[] => {
  if (kind === 'custom_object') {
    // Custom fields columns
    const customFieldColumns =
      customFields && customFields.length
        ? customFields.map(field => ({
            uuid: v4(),
            label: field.label,
            type: field.type,
            source: ['attributes', 'custom_fields', field.magicString],
            visible: true,
          }))
        : [];

    return [...getDefaultColumns(kind, t), ...customFieldColumns];
  } else {
    return [...getDefaultColumns(kind, t)];
  }
};
