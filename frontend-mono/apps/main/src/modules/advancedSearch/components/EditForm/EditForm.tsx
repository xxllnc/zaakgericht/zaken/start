// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState, useRef } from 'react';
import { Blocker, useNavigate } from 'react-router-dom';
import * as i18next from 'i18next';
import { Formik } from 'formik';
import { QueryClient } from '@tanstack/react-query';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import {
  IdentifierType,
  KindType,
  ModeType,
  ClassesType,
} from '../../AdvancedSearch.types';
import { useSingleQuery } from '../../query/useSingle';
import { invalidateAllQueries } from '../../query/library';
import { getURLMatches } from '../../library/library';
import { useSingleSaveMutation } from '../../query/useSingle';
import { getInitialState } from './EditForm.library';
import EditFormInner from './EditFormInner';

type EditFormPropsType = {
  classes: ClassesType;
  mode: ModeType;
  identifier?: IdentifierType | null;
  kind: KindType;
  client: QueryClient;
  t: i18next.TFunction;
};

/* eslint complexity: [2, 12] */
const EditForm: FunctionComponent<EditFormPropsType> = ({
  classes,
  mode,
  identifier,
  kind,
  client,
  t,
}) => {
  const [tabValue, setTabValue] = useState<number>(0);
  const blockerRef = useRef<Blocker | null>(null);
  const [ignoreBlocking, setIgnoreBlocking] = useState<boolean>(false);
  const navigate = useNavigate();
  const urlMatches = getURLMatches();
  const handleTabChange = (event: React.ChangeEvent<any>, newValue: number) =>
    setTabValue(newValue);
  const session = useSession();
  const currentQuery = useSingleQuery({
    identifier,
    kind,
    t,
    session,
  });
  const authorizations = currentQuery?.data?.authorizations;

  const saveMutation = useSingleSaveMutation({
    kind,
    identifier,
    mode,
    authorizations,
  });
  if (
    mode === 'edit' &&
    (!authorizations || !currentQuery.isSuccess || !currentQuery.data)
  ) {
    return <Loader />;
  }
  const initialValues = getInitialState({
    mode,
    currentQuery,
    kind,
    t,
  });

  if (currentQuery.isFetching) return null;

  return (
    <Formik
      enableReinitialize={false}
      initialValues={initialValues}
      isInitialValid={false}
      onReset={() => {
        if (blockerRef?.current?.reset) blockerRef.current.reset();
      }}
      onSubmit={async (values, formik) => {
        saveMutation.mutate(values, {
          onSuccess: (data: any) => {
            invalidateAllQueries(client);
            openSnackbar({ message: t('snacks.saved'), sx: snackbarMargin });
            formik.validateForm();
            setIgnoreBlocking(true);
            if (mode === 'new') {
              setTimeout(() => {
                navigate(
                  `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}/view/${data}`
                );
              }, 0);
            }
          },
          onError: openServerError,
        });
      }}
    >
      {formik => (
        <EditFormInner
          formik={formik}
          classes={classes}
          tabValue={tabValue}
          handleTabChange={handleTabChange}
          kind={kind}
          client={client}
          setTabValue={setTabValue}
          saveMutation={saveMutation}
          authorizations={authorizations}
          mode={mode}
          t={t}
          identifier={identifier}
          blockerRef={blockerRef}
          ignoreBlocking={ignoreBlocking}
        />
      )}
    </Formik>
  );
};

export default EditForm;
