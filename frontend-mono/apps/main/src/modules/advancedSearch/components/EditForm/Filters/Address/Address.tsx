// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState } from 'react';
import { TFunction } from 'i18next';
import { Field, FieldProps } from 'formik';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import Select, { renderTagsWithIcon } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  RadioGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
} from '@mui/material';
import Radio from '@mui/material/Radio';
import { BaseSelectPropsType } from '@mintlab/ui/App/Zaaksysteem/Select/types/BaseSelectPropsType';
import { ValidateFuncType } from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';
import { useAddressChoicesQuery } from './Address.library';
import { AddressType } from './Address.types';

const TRANSLATION_BASE = 'editForm.fields.filters.fields.generic.location';

type ExtraPropsType = {
  addressType?: AddressType;
} & Pick<BaseSelectPropsType<any>, 'placeholder' | 'isClearable' | 'isMulti'>;

type AddressCmpPropsType = {
  field: FieldProps['field'];
  t: TFunction;
} & ExtraPropsType;

interface AddressPropsTypeBaseType extends FilterCommonPropsType {
  validate?: ValidateFuncType<any> | undefined;
}
type AddressPropsType = AddressPropsTypeBaseType & ExtraPropsType;

const Address: FunctionComponent<AddressPropsType> = ({
  identifier,
  name,
  t,
  validate = null,
  placeholder,
  isClearable,
  addressType = 'nummeraanduiding',
  isMulti,
  ...rest
}) => {
  return (
    <div style={{ width: '100%' }}>
      <Field
        component={AddressCmp}
        name={name}
        key={identifier}
        t={t}
        validate={validate}
        placeholder={placeholder}
        isClearable={isClearable}
        addressType={addressType}
        isMulti={isMulti}
        {...rest}
      />
    </div>
  );
};

const AddressCmp: FunctionComponent<AddressCmpPropsType> = ({
  field,
  t,
  placeholder,
  isClearable,
  addressType,
  isMulti = true,
}) => {
  const [type, setType] = useState<AddressType>(
    addressType || 'nummeraanduiding'
  );
  const selectProps = useAddressChoicesQuery({
    field,
    type,
    isMulti,
  });

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <FormControl>
          <FormLabel
            id="advanced-search-address-form-label"
            sx={{ textAlign: 'right', marginBottom: '10px' }}
          >
            {t(`${TRANSLATION_BASE}.searchIn`) as string}
          </FormLabel>
          {!addressType && (
            <RadioGroup
              row
              aria-labelledby="advanced-search-address-group-label"
              name="advanced-search-address-group-label"
              onChange={evt => setType(evt.target.value as AddressType)}
              value={type}
            >
              <FormControlLabel
                value="nummeraanduiding"
                control={<Radio />}
                label={t(`${TRANSLATION_BASE}.addressType`) as string}
                sx={{ marginRight: '4px' }}
              />
              <div style={{ marginRight: '8px' }}>
                <Tooltip
                  title={t(`${TRANSLATION_BASE}.help.nummeraanduiding`)}
                  placement="top"
                >
                  <Icon size="extraSmall" color="inherit">
                    {iconNames.help_outline}
                  </Icon>
                </Tooltip>
              </div>
              <FormControlLabel
                value="openbareruimte"
                control={<Radio />}
                label={t(`${TRANSLATION_BASE}.streetType`) as string}
                sx={{ marginRight: '4px' }}
              />
              <div style={{ marginRight: '8px' }}>
                <Tooltip
                  title={t(`${TRANSLATION_BASE}.help.openbareruimte`)}
                  placement="top"
                >
                  <Icon size="extraSmall" color="inherit">
                    {iconNames.help_outline}
                  </Icon>
                </Tooltip>
              </div>
            </RadioGroup>
          )}
        </FormControl>
      </div>
      <div>
        <Select
          {...field}
          {...selectProps}
          renderTags={renderTagsWithIcon(iconNames.place)}
          placeholder={placeholder}
          isClearable={isClearable}
          isMulti={isMulti}
        />
      </div>
    </>
  );
};

export default Address;
