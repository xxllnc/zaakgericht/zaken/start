// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import { CaseTypeFinder } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Field, FieldProps } from 'formik';
import { ErrorType } from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';

const TRANSLATION_BASE = 'editForm.fields.filters.fields.caseType.';

const CaseType: FunctionComponent<FilterCommonPropsType> = ({
  name,
  t,
  identifier,
}) => {
  return (
    <div
      style={{
        width: '100%',
      }}
    >
      <Field
        component={({ field }: FieldProps) => (
          <CaseTypeFinder
            {...field}
            includeOffline={true}
            placeholder={t(`${TRANSLATION_BASE}placeholder`)}
            multiValue={true}
            //@ts-ignore
            onChange={(event: React.ChangeEvent<any>) => {
              field.onChange({
                ...event,
                target: {
                  ...event.target,
                  name: event.target.name,
                  value: event.target.value.map((value: ValueType<string>) => ({
                    label: value.label,
                    value: value.value,
                  })),
                },
              });
            }}
          />
        )}
        name={name}
        validate={(value: any): ErrorType | undefined => {
          if (!isPopulatedArray(value)) {
            return {
              value: t(`${TRANSLATION_BASE}errorMessage`),
              uuid: identifier,
            };
          }
        }}
      />
    </div>
  );
};

export default CaseType;
