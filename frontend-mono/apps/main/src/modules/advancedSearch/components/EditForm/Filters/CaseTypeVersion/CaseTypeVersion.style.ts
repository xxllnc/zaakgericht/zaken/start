// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    flexDirection: 'column',
    display: 'flex',
    flex: 1,
  },
  addWrapper: {
    display: 'flex',
    flex: 1,
    gap: 16,
    alignItems: 'center',
    '&>:nth-child(1)': {
      flex: 1,
    },
    '&>:nth-child(2)': {
      flex: 1,
    },
  },
  entry: {},
  entryWrapper: {
    marginTop: 20,
    display: 'flex',
    gap: 10,
    alignItems: 'center',
    '&>:nth-child(1)': {
      width: 'auto',
    },
  },
}));
