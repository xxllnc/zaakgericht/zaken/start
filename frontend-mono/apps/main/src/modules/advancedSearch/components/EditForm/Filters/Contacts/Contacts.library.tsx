// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
//@ts-ignore
import { hasNoValue } from '../library/Filters.library';
import { SearchTypeType } from './Contacts';

export const getTypeChoices = (
  t: i18next.TFunction
): ValueType<SearchTypeType>[] => [
  {
    label: t('contacts.all') as string,
    value: 'all',
  },
  {
    label: t('contacts.employee') as string,
    value: 'employee',
  },
  {
    label: t('contacts.person') as string,
    value: 'person',
  },
  {
    label: t('contacts.organization') as string,
    value: 'organization',
  },
];

export const validateFunc =
  (identifier: string, translations: any) => (value: any) => {
    const isUnpopulatedArray = !isPopulatedArray(value);
    return hasNoValue(value) || isUnpopulatedArray
      ? {
          value: translations.errorMessage,
          uuid: identifier,
        }
      : undefined;
  };
