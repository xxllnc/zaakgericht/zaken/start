// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, FunctionComponent, useState } from 'react';
import { Field } from 'formik';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ErrorType, DateModeType } from '../../../../AdvancedSearch.types';
import {
  DateEntryType,
  DateEntryTypeAbsolute,
  DateEntryTypeRelative,
  DateEntryTypeRange,
} from '../../../../AdvancedSearch.types.filters';
import { FilterCommonPropsType } from '../Filters.types';
import { validateForm } from '../../EditForm.library';
import { hasNoValue } from '../library/Filters.library';
import { useStyles } from './DateEntries.style';
import AbsoluteDialog from './components/AbsoluteDialog';
import RelativeDialog from './components/RelativeDialog';
import RangeDialog from './components/RangeDialog';
import {
  hasValue,
  defaultDialogProperties,
  TRANSLATION_BASE,
} from './DateEntries.library';
import Entries from './components/Entries';
import AddDropdownMenu from './components/AddDropdownMenu';
import { DialogPropertiesType } from './DateEntries.types';

interface DateEntriesPropsType extends FilterCommonPropsType {
  mode: DateModeType;
}

const DateEntries: FunctionComponent<DateEntriesPropsType> = ({
  identifier,
  t,
  name,
  formik,
  index,
  mode,
}) => {
  const classes = useStyles();
  const helpersRef = useRef<HTMLInputElement | null>(null);
  const [dialogProperties, setDialogProperties] = useState(
    defaultDialogProperties as DialogPropertiesType
  );

  const insertOrSetFieldValue = (valuesObj: DateEntryType) => {
    if (hasValue(dialogProperties.index)) {
      formik.setFieldValue(`${name}.[${dialogProperties.index}]`, valuesObj);
    } else {
      //@ts-ignore
      helpersRef.current.insert(0, valuesObj);
    }
  };

  const isOpen = (type: DateEntryType['type']) =>
    dialogProperties?.open === true && dialogProperties?.entry?.type === type;

  return (
    <>
      <AbsoluteDialog
        mode={mode}
        open={isOpen('absolute')}
        t={t}
        entry={dialogProperties?.entry as DateEntryTypeAbsolute}
        onClose={() => setDialogProperties(defaultDialogProperties)}
        onSubmit={entry => insertOrSetFieldValue(entry)}
      />
      <RelativeDialog
        open={isOpen('relative')}
        t={t}
        entry={dialogProperties?.entry as DateEntryTypeRelative}
        onClose={() => setDialogProperties(defaultDialogProperties)}
        onSubmit={entry => insertOrSetFieldValue(entry)}
        mode={mode}
      />
      <RangeDialog
        open={isOpen('range')}
        t={t}
        entry={dialogProperties?.entry as DateEntryTypeRange}
        onClose={() => setDialogProperties(defaultDialogProperties)}
        onSubmit={entry => insertOrSetFieldValue(entry)}
        mode={mode}
      />
      <div className={classes.wrapper}>
        <AddDropdownMenu
          t={t}
          setDialogProperties={setDialogProperties}
          classes={classes}
        />
        <Field
          component={Entries}
          name={name}
          validate={(value: any): ErrorType | undefined => {
            const isUnpopulatedArray = !isPopulatedArray(value);
            return hasNoValue(value) || isUnpopulatedArray
              ? {
                  value: t(`${TRANSLATION_BASE}errors.minimumEntries`),
                  uuid: identifier,
                }
              : undefined;
          }}
          classes={classes}
          t={t}
          setDialogProperties={setDialogProperties}
          validateForm={validateForm}
          index={index}
          helpersRef={helpersRef}
          setHelpersRef={(ref: HTMLInputElement | null) => {
            if (ref) helpersRef.current = ref;
          }}
          mode={mode}
        />
      </div>
    </>
  );
};

export default DateEntries;
