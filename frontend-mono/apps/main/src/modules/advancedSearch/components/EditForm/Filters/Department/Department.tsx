// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import { FieldProps } from 'formik';
import { DepartmentFinder } from '@zaaksysteem/common/src/components/form/fields/DepartmentFinder/DepartmentFinder';
import * as i18next from 'i18next';
import { Field } from 'formik';

type DepartmentPropsType = {
  name: string;
  t: i18next.TFunction;
  identifier: string;
  isMulti: boolean;
  placeholder?: string;
  [key: string]: any;
};

const DepartmentCmp = ({
  field,
  form,
  t,
  isMulti = true,
  ...rest
}: {
  field: FieldProps['field'];
  form: FieldProps['form'];
  t: i18next.TFunction;
  isMulti?: boolean;
}) => {
  return (
    <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
      <DepartmentFinder {...field} multiValue={isMulti} {...rest} />
    </div>
  );
};

const Department: FunctionComponent<DepartmentPropsType> = ({
  name,
  t,
  identifier,
  isMulti,
  placeholder,
  ...rest
}) => {
  return (
    <Field
      component={DepartmentCmp}
      name={name}
      t={t}
      isMulti={isMulti}
      placeholder={placeholder}
      {...rest}
    />
  );
};

export default Department;
