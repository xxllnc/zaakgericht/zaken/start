// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useRef } from 'react';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import { FieldProps } from 'formik';
import Button from '@mintlab/ui/App/Material/Button';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { Field, FieldArray } from 'formik';
import { ErrorType } from '../../../../AdvancedSearch.types';
import { FilterType } from '../../../../AdvancedSearch.types.filters';
import { validateForm } from '../../EditForm.library';
import { FilterCommonPropsType } from '../Filters.types';
import DepartmentRoleEntry from './components/DepartmentRoleEntry';
import { useStyles } from './DepartmentRoles.style';

type FilterValueType = Extract<
  FilterType,
  { type: 'attributes.department_role' }
>['values'];

export const TRANSLATION_BASE = 'editForm.fields.filters.fields.departmentRole';

const DepartmentRolesCmp = ({
  field,
  form,
  t,
}: {
  field: FieldProps['field'];
  form: FieldProps['form'];
  t: i18next.TFunction;
}) => {
  const classes = useStyles();
  const { name } = field;
  const helpersRef = useRef<HTMLInputElement | null>(null);
  const entries: FilterValueType = get(form.values, field.name);

  return (
    <div className={classes.departmentRoleWrapper}>
      <Button
        name="add"
        className={classes.addButton}
        action={() => {
          //@ts-ignore
          helpersRef.current.insert(0, {
            department: null,
            role: null,
          });
        }}
      >
        + {t('new')}
      </Button>
      <FieldArray
        name={name}
        key={name}
        render={arrayHelpersEntries => {
          if (!helpersRef.current) {
            //@ts-ignore
            helpersRef.current = arrayHelpersEntries;
          }
          return (
            <div className={classes.entriesWrapper}>
              {entries && entries.length > 0
                ? entries.map((entry, entryIndex) => {
                    return (
                      <div
                        className={classes.entryWrapper}
                        key={`entry-${entryIndex}`}
                      >
                        <div className={classes.entryField}>
                          <Field
                            name={`${name}.[${entryIndex}]`}
                            key={`${name}.[${entryIndex}]`}
                            component={DepartmentRoleEntry}
                            classes={classes}
                            index={entryIndex}
                            t={t}
                          />
                        </div>
                        <div className={classes.entryDelete}>
                          <IconButton
                            onClick={() => {
                              arrayHelpersEntries.remove(entryIndex);
                              validateForm(form);
                            }}
                            disableRipple={true}
                            size="small"
                          >
                            <Icon size="small" color="inherit">
                              {iconNames.delete}
                            </Icon>
                          </IconButton>
                        </div>
                      </div>
                    );
                  })
                : null}
            </div>
          );
        }}
      />
    </div>
  );
};

const DepartmentRole: FunctionComponent<FilterCommonPropsType> = ({
  name,
  t,
  identifier,
}) => {
  return (
    <Field
      component={DepartmentRolesCmp}
      name={name}
      t={t}
      validate={(value: FilterValueType): ErrorType | undefined => {
        if (!isPopulatedArray(value)) {
          return {
            value: t(`${TRANSLATION_BASE}.errors.minEntries`),
            uuid: identifier,
          };
        } else {
          return undefined;
        }
      }}
    />
  );
};

export default DepartmentRole;
