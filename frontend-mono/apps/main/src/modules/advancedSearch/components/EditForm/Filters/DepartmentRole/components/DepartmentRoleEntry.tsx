// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { Field, FieldProps } from 'formik';
import * as i18next from 'i18next';
import { DepartmentFinder } from '@zaaksysteem/common/src/components/form/fields/DepartmentFinder/DepartmentFinder';
import RoleFinder from '@zaaksysteem/common/src/components/form/fields/RoleFinder/RoleFinder';
import { TRANSLATION_BASE } from '../DepartmentRole';

type DepartmentRolePropsType = {
  field: FieldProps['field'];
  classes: any;
  t: i18next.TFunction;
};

const DepartmentRoleEntry: React.FunctionComponent<DepartmentRolePropsType> = ({
  field,
  classes,
  t,
}) => {
  const departmentUuid = field.value?.department?.value;

  return (
    <>
      <div className={classes.entryFieldRow}>
        <div>{t(`${TRANSLATION_BASE}.department`) as string}</div>
        <div>
          <Field
            name={`${field.name}.department`}
            component={({ field }: FieldProps) => {
              return (
                <DepartmentFinder
                  {...field}
                  placeholder={t(`${TRANSLATION_BASE}.departmentPlaceholder`)}
                  isClearable={false}
                  onChange={(event: React.ChangeEvent<any>) => {
                    field.onChange({
                      target: {
                        name: event.target.name,
                        value: {
                          value: event.target.value.value,
                          label: event.target.value.label,
                        },
                      },
                    });
                  }}
                />
              );
            }}
          />
        </div>
      </div>
      <div className={classes.entryFieldRow}>
        <div>{t(`${TRANSLATION_BASE}.role`) as string}</div>
        <div>
          <Field
            name={`${field.name}.role`}
            component={({ field }: FieldProps) => {
              return (
                <RoleFinder
                  {...field}
                  isClearable={false}
                  placeholder={
                    !departmentUuid
                      ? t(`${TRANSLATION_BASE}.roleDisabledPlaceholder`)
                      : t(`${TRANSLATION_BASE}.rolePlaceholder`)
                  }
                  disabled={!departmentUuid}
                  config={{
                    parentRoleUuid: departmentUuid,
                  }}
                  onChange={(event: React.ChangeEvent<any>) => {
                    field.onChange({
                      target: {
                        name: event.target.name,
                        value: {
                          value: event.target.value.value,
                          label: event.target.value.label,
                        },
                      },
                    });
                  }}
                />
              );
            }}
          />
        </div>
      </div>
    </>
  );
};

export default DepartmentRoleEntry;
