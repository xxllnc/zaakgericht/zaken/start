// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { FilterType } from '../../../../AdvancedSearch.types.filters';

export const TRANSLATION_BASE = 'editForm.fields.filters.types.file';

const Document: FunctionComponent<{
  filter: FilterType;
  t: i18next.TFunction;
  classes: any;
}> = ({ filter, t, classes }) => {
  return (
    <div className={classes.text}>
      {
        t(`${TRANSLATION_BASE}.label`, {
          //@ts-ignore
          name: filter?.values?.magicString,
        }) as string
      }
    </div>
  );
};

export default Document;
