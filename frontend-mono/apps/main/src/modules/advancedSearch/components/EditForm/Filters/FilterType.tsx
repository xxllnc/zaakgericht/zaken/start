// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormikProps, Field, FieldProps, FieldArrayRenderProps } from 'formik';
//@ts-ignore
import objectScan from 'object-scan';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import { isCustomAttribute } from '../../../library/library';
import {
  EditFormStateType,
  ClassesType,
  ErrorType,
  KindType,
} from '../../../AdvancedSearch.types';
import { FilterType } from '../../../AdvancedSearch.types.filters';
import {
  filterTranslationKeys,
  getFieldOperatorsConfig,
  getArchiveExportTemplate,
} from '../../../library/config';
import { validateForm } from '../EditForm.library';
import {
  getOperatorOptions,
  getHelp,
  getEntryPoint,
} from './library/Filters.library';
import { getField } from './library/getField';

type FilterTypePropsType = {
  filter: FilterType;
  index: number;
  arrayHelpers: FieldArrayRenderProps;
  values: EditFormStateType;
  errors: any;
  classes: ClassesType;
  t: i18next.TFunction;
  formik: FormikProps<EditFormStateType>;
  name: string;
  kind: KindType;
};

const NotImplementedMessage: FunctionComponent<{
  t: i18next.TFunction;
}> = ({ t }) => (
  <div>{t(`editForm.fields.filters.notImplementedYet`) as string}</div>
);

/* eslint complexity: [2, 10] */
// Main selection/entrypoint for a main level filter
const FilterTypeComponent: FunctionComponent<FilterTypePropsType> = ({
  filter,
  index,
  arrayHelpers,
  values,
  errors,
  classes,
  t,
  formik,
  name,
  kind,
}) => {
  const { uuid, type } = filter;
  const template = values?.template || 'default';
  const disabled =
    template === 'archive_export' &&
    getArchiveExportTemplate()
      .map(t => t.type)
      .includes(type);
  const baseName = `${name}.[${index}]`;
  let errorObj: ErrorType | undefined;

  const errorPath = objectScan(['**', 'uuid'], {
    joined: true,
    filterFn: ({ value }: any) => value?.uuid === uuid,
  })(errors);

  if (errorPath && errorPath.length) {
    errorObj = get(errors, errorPath[0]);
  }

  const filterIsCustomAttribute = isCustomAttribute(filter);

  const getLabel = (): string => {
    if (filterIsCustomAttribute) {
      return filter.values.label || filter.values.magicString;
    } else {
      //@ts-ignore
      const translatedLabel = filterTranslationKeys[type];
      return t(
        `editForm.fields.filters.fields.${translatedLabel || type}.label`
      );
    }
  };

  const field = getField({
    t,
    name: getEntryPoint(filter, baseName),
    index,
    filter,
    formik,
    kind,
    disabled,
  });

  const getOperator = () => {
    const config = getFieldOperatorsConfig({ filter, kind });

    if (field && config) {
      const choices = getOperatorOptions(t, config.operators);
      if (config.operators.length > 1) {
        return (
          <>
            <Field
              name={`${baseName}.${config.location || 'operator'}`}
              t={t}
              component={(props: FieldProps & { choices: any }) => {
                const { choices, field } = props;
                return (
                  <Select
                    disabled={disabled}
                    {...field}
                    choices={choices}
                    nestedValue={true}
                    isClearable={false}
                    sx={{
                      width: 114,
                    }}
                    inputProps={{ readOnly: true }}
                  />
                );
              }}
              choices={choices}
            />
          </>
        );
      } else if (config.operators.length === 1) {
        return (
          <div className={classes.filterOperatorStatic}>{choices[0].label}</div>
        );
      }
    } else {
      return null;
    }
  };

  return (
    <div className={classes.filterWrapper}>
      <div className={classes.filterHeader}>
        <div>
          <Tooltip
            enterDelay={200}
            title={
              filterIsCustomAttribute
                ? t('editForm.methods.attribute')
                : t('editForm.methods.systemAttribute')
            }
          >
            <Icon size="small" color="inherit">
              {filterIsCustomAttribute
                ? iconNames.extension
                : iconNames.system_attribute}
            </Icon>
          </Tooltip>
        </div>
        <div>{getLabel()}</div>
        <div>{getOperator()}</div>
        <div>{getHelp({ filter, kind, classes, t })}</div>
        <div>
          <IconButton
            disabled={disabled}
            disableRipple={false}
            onClick={() => {
              arrayHelpers.remove(index);
              validateForm(formik);
            }}
            size="small"
            classes={{
              root: classes.deleteButton,
            }}
          >
            <Icon size="extraSmall" color="inherit">
              {iconNames.delete}
            </Icon>
          </IconButton>
        </div>
      </div>

      {field || <NotImplementedMessage t={t} />}
      {errorObj && <div className={classes.filterError}>{errorObj.value}</div>}
    </div>
  );
};

export default FilterTypeComponent;
