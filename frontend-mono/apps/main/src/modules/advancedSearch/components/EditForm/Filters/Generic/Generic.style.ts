// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    text: {
      overflowWrap: 'break-word',
      WordBreak: 'break-word',
      whiteSpace: 'normal',
      padding: 10,
    },
  };
});
