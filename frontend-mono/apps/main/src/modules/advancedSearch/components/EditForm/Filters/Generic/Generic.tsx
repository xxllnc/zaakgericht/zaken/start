// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useStyles } from './Generic.style';

export const GenericText = ({ text }: { text: string }) => {
  const classes = useStyles();
  return <div className={classes.text}>{text}</div>;
};
