// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { FormikProps, FieldInputProps } from 'formik';
import { get } from '@mintlab/kitchen-sink/source';
import { formatCurrency } from '@zaaksysteem/common/src/library/currency';
import {
  OperatorType,
  EditFormStateType,
} from '../../../../AdvancedSearch.types';
import { NumberEntryType } from '../../../../AdvancedSearch.types.filters';
import { useStyles } from './NumberEntries.style';
import { TRANSLATION_BASE } from './NumberEntries';

type NumberEntryPropsType = {
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
  index: number;
  classes: any;
  currency?: boolean;
};

const Entry: FunctionComponent<NumberEntryPropsType> = ({
  form,
  field,
  t,
  currency = false,
}) => {
  const { name } = field;
  const { values } = form;
  const entry = get(values, name) as NumberEntryType;
  const classes = useStyles();

  const operatorTranslations = t(`${TRANSLATION_BASE}.operators`, {
    returnObjects: true,
  });

  return (
    <div className={classes.entry}>{`${
      operatorTranslations[entry[0] as keyof OperatorType]
      // @ts-ignore
    } ${currency ? formatCurrency(t, entry[1]) : entry[1]}`}</div>
  );
};

export default Entry;
