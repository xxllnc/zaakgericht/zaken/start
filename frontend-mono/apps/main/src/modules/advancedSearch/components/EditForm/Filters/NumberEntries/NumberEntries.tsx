// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, FunctionComponent, useState } from 'react';
import { Field } from 'formik';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import TextField from '@mintlab/ui/App/Material/TextField';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ErrorType } from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';
import { validateForm } from '../../EditForm.library';
import { hasNoValue } from '../library/Filters.library';
import { useStyles } from './NumberEntries.style';
import Entries from './Entries';

export const TRANSLATION_BASE = 'editForm.fields.filters.types.numberEntries';

const NumberEntries: FunctionComponent<
  FilterCommonPropsType & { currency?: boolean }
> = ({ identifier, t, name, index, currency = false }) => {
  const classes = useStyles();
  const helpersRef = useRef<HTMLInputElement | null>(null);
  const [addPrice, setAddPrice] = useState<string>('');
  const [addOperator, setAddOperator] = useState<string>('eq');
  const validAdd = addPrice && Number.isFinite(Number(addPrice));

  const handleAdd = () => {
    if (!helpersRef) return;
    // @ts-ignore-next-line
    helpersRef.current.insert(0, [addOperator, Number(addPrice)]);
    setAddPrice('');
    setAddOperator('eq');
  };
  const operatorChoices: ValueType<string>[] = Object.entries(
    t(`${TRANSLATION_BASE}.operators`, {
      returnObjects: true,
    })
  ).map(([value, label]) => ({ value, label }));
  return (
    <>
      <div className={classes.wrapper}>
        <div className={classes.addWrapper}>
          <Select
            choices={operatorChoices}
            value={addOperator}
            isClearable={false}
            onChange={(event: React.ChangeEvent<any>) =>
              setAddOperator(event.target.value.value)
            }
            inputProps={{ readOnly: true }}
          />
          <TextField
            name={`numberEntries-filter-${index}`}
            value={addPrice}
            placeholder={t(`${TRANSLATION_BASE}.placeholder`)}
            onChange={(event: React.ChangeEvent<any>) =>
              setAddPrice(event.target.value)
            }
            {...(currency ? { formatType: t('common:currencyCode') } : {})}
          />
          <Button
            name="addButton"
            onClick={handleAdd}
            sx={{ minWidth: 32, height: 32 }}
            disabled={!validAdd}
          >
            +
          </Button>
        </div>
        <Field
          component={Entries}
          name={name}
          validate={(value: any): ErrorType | undefined => {
            const isUnpopulatedArray = !isPopulatedArray(value);
            return hasNoValue(value) || isUnpopulatedArray
              ? {
                  value: t(`${TRANSLATION_BASE}.errors.minEntries`),
                  uuid: identifier,
                }
              : undefined;
          }}
          classes={classes}
          t={t}
          validateForm={validateForm}
          index={index}
          helpersRef={helpersRef}
          setHelpersRef={(ref: HTMLInputElement | null) => {
            if (ref) helpersRef.current = ref;
          }}
          currency={currency}
        />
      </div>
    </>
  );
};

export default NumberEntries;
