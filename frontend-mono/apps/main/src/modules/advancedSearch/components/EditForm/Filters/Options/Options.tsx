// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useEffect } from 'react';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { FormikProps } from 'formik';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { EditFormStateType } from '../../../../AdvancedSearch.types';
import { useAttributeDetailsQuery } from '../../../../query/useAttributeDetails';
import { validateForm } from '../../EditForm.library';
import { hasNoValue } from '../library/Filters.library';
import Checkboxes from '../Checkboxes/Checkboxes';
import { ErrorType } from '../../../../AdvancedSearch.types';
import { isCustomAttribute } from '../../../../library/library';
import { FilterCommonPropsType } from '../../Filters/Filters.types';
import { FilterType } from '../../../../AdvancedSearch.types.filters';

type OptionsPropsType = FilterCommonPropsType & {
  choices?: ValueType<string>[];
};

const UseAttributeDetails = (
  filter: FilterType,
  formik: FormikProps<EditFormStateType>
) => {
  //@ts-ignore
  const { attributeUuid, type } = isCustomAttribute(filter)
    ? filter.values
    : filter;
  const attributeDetails = useAttributeDetailsQuery(attributeUuid, {
    staleTime: 0,
  });
  useEffect(() => validateForm(formik), [attributeDetails?.data]);
  if (!attributeDetails.data) return null;
  const choices = attributeDetails.data.attributes.attribute_values
    .filter((choice: any) => choice.active)
    .map(({ value }: any) => ({ value, label: value, type }));

  return {
    choices,
  };
};

const Options: FunctionComponent<OptionsPropsType> = ({
  formik,
  identifier,
  name,
  t,
  index,
  filter,
  choices,
}) => {
  const customAttribute = isCustomAttribute(filter);
  const customAttributeDetails = customAttribute
    ? UseAttributeDetails(filter, formik)
    : null;

  if (customAttribute && !customAttributeDetails) return <Loader />;

  return (
    <div style={{ width: '100%' }}>
      <Checkboxes
        choices={choices || customAttributeDetails?.choices || []}
        name={name}
        t={t}
        identifier={identifier}
        formik={formik}
        index={index}
        validate={(value: any): ErrorType | undefined => {
          return hasNoValue(value) || !isPopulatedArray(value)
            ? {
                value: t(
                  'editForm.fields.filters.fields.generic.options.errorMessageMin'
                ),
                uuid: identifier,
              }
            : undefined;
        }}
      />
    </div>
  );
};

export default Options;
