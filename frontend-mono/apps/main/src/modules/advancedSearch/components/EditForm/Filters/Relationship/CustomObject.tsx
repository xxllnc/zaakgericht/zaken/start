// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { Field } from 'formik';

type CustomObjectPropsType = {
  name: string;
  objectTypeName: string;
  t: i18next.TFunction;
  classes: any;
};

const CustomObject: FunctionComponent<CustomObjectPropsType> = ({
  name,
  objectTypeName,
  t,
  classes,
}) => (
  <div className={classes.text}>
    <Field
      name={name}
      component={() =>
        t(`editForm.fields.filters.fields.relation.filterCustomObject`, {
          objectTypeName,
        }) as string
      }
      objectTypeName={objectTypeName}
      t={t}
    />
  </div>
);

export default CustomObject;
