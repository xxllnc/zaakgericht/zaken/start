// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormikProps, FieldInputProps, FieldArrayRenderProps } from 'formik';
import { KindType, EditFormStateType } from '../../../AdvancedSearch.types';
import { OptionType } from '../Filters/library/Filters.library';

export type FiltersSelectPropsType = {
  arrayHelpersRef: FieldArrayRenderProps | null;
  formik: FormikProps<EditFormStateType>;
  t: i18next.TFunction;
  kind: KindType;
  classes: any;
  inputRef: React.RefObject<HTMLInputElement> | null;
};

export type FiltersSelectCmpPropsType = Pick<
  FiltersSelectPropsType,
  't' | 'arrayHelpersRef' | 'kind' | 'inputRef'
> & {
  name: string;
  choices: OptionType[];
  value: ValueType<string> | null;
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
};

export type OptionTypeWithDataType = {
  label: string;
  value: string;
  data?: any;
};

export type dataType = {
  attributeUuid: string;
  description: string | null;
  label: string;
  magicString: string;
  name: string;
  type: string;
};

export type SelectSystemAttributesPropsType = {
  arrayHelpers: FieldArrayRenderProps;
  name: string;
  choices: any;
  value: any;
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
  kind: KindType;
};

export type MethodType = 'systemAttributes' | 'searchAttributes';
