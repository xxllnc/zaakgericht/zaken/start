// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import MUITabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { hasAccess } from '../../library/library';
import {
  AuthorizationsType,
  ClassesType,
  ModeType,
} from '../../AdvancedSearch.types';

type EditFormTabsPropsType = {
  classes: ClassesType;
  tabValue: number;
  handleTabChange: (event: any, newValue: number) => void;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
  mode: ModeType;
};

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const EditFormTabs: FunctionComponent<EditFormTabsPropsType> = ({
  classes,
  tabValue,
  handleTabChange,
  authorizations,
  t,
  mode,
}) => {
  return (
    <MUITabs
      value={tabValue}
      onChange={handleTabChange}
      aria-label="Edit Form tabs"
      classes={{
        root: classes.tabs,
        flexContainer: classes.tabsFlexContainer,
      }}
      indicatorColor="primary"
      variant="fullWidth"
    >
      <Tab
        label={t('editForm.tabs.filters') as string}
        {...a11yProps(0)}
        classes={{
          selected: classes.selectedTab,
          root: classes.tab,
        }}
      />
      <Tab
        label={t('editForm.tabs.permissions') as string}
        {...a11yProps(1)}
        classes={{
          selected: classes.selectedTab,
          root: classes.tab,
        }}
        disabled={
          mode === 'edit' &&
          authorizations &&
          !hasAccess(authorizations, 'admin')
        }
      />

      <Tab
        label={t('editForm.tabs.columns') as string}
        {...a11yProps(2)}
        classes={{
          selected: classes.selectedTab,
          root: classes.tab,
        }}
      />
    </MUITabs>
  );
};

export default EditFormTabs;
