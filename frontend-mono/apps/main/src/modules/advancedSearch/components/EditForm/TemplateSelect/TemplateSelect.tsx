// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { FormikProps } from 'formik';
import * as i18next from 'i18next';
import { FieldInputProps } from 'formik';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { onSavedSearchTemplateChange } from '../EditForm.sideEffects';
import { EditFormStateType } from '../../../AdvancedSearch.types';
import { editFormSx } from '../../../styles/editForm';

type TemplateSelectPropsType = {
  field: FieldInputProps<any>;
  form: FormikProps<EditFormStateType>;
  t: i18next.TFunction;
};

const TemplateSelect: FunctionComponent<TemplateSelectPropsType> = ({
  field,
  form,
  t,
}) => {
  const session = useSession();
  let choices = [
    {
      label: t('template.choices.standard'),
      value: 'standard',
    },
  ];
  if (session?.active_interfaces?.includes('export_mapping_topx')) {
    choices.push({
      label: t('template.choices.archive_export'),
      value: 'archive_export',
    });
  }
  return (
    <>
      {
        <Select
          {...field}
          variant="generic"
          choices={choices}
          isClearable={false}
          loading={false}
          freeSolo={true}
          nestedValue={true}
          disabled={choices.length < 2}
          sx={editFormSx().lightSelect}
          onChange={(event: React.ChangeEvent<any>) => {
            const value = event.target.value;
            onSavedSearchTemplateChange({ formik: form, value });
            field.onChange(event);
          }}
          inputProps={{
            readOnly: true,
          }}
        />
      }
    </>
  );
};

export default TemplateSelect;
