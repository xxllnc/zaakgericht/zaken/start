// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { editFormSx } from '../../../../styles/editForm';
import { useAttributeChoicesQuery } from './AttributeFinder.library';
import {
  AttributeFinderPropsType,
  SearchResultAPIType,
} from './AttributeFinder.types';

const AttributeFinderCase: FunctionComponent<AttributeFinderPropsType> = ({
  handleSelectOnChange,
  name,
  t,
  filterOption,
  selectKey,
}) => {
  const selectProps = useAttributeChoicesQuery();
  return (
    <>
      <Select
        {...selectProps}
        variant="generic"
        name={name}
        key={selectKey}
        isClearable={false}
        onChange={(event: React.ChangeEvent<any>) => {
          const data = event.target.value.data as SearchResultAPIType['data'];
          handleSelectOnChange({
            target: {
              ...event.target,
              value: {
                ...event.target.value,
                data,
              },
            },
          });
        }}
        placeholder={t('editForm.attributeFinder.placeholder')}
        freeSolo={true}
        sx={editFormSx().lightSelect}
        {...(filterOption && { filterOption })}
      />
    </>
  );
};

export default AttributeFinderCase;
