// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { CustomFieldType, KindType } from '../../../../AdvancedSearch.types';

export type AttributeFinderPropsType = {
  handleSelectOnChange: (event: any) => void;
  name: string;
  t: i18next.TFunction;
  filterOption?: any;
  selectKey: string;
  kind: KindType;
  customFields?: CustomFieldType[];
};

export type SearchResultAPIType = APICaseManagement.EntityAttributeSearchList;
