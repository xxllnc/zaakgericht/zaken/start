// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext, FunctionComponent } from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import { useParams } from 'react-router-dom';
import { QueryClient } from '@tanstack/react-query';
import {
  KindType,
  ClassesType,
  AdvancedSearchParamsType,
} from '../../AdvancedSearch.types';
import EditForm from '../EditForm/EditForm';
import { localStateContext } from '../../context/LocalStateProvider';
import List from './List/List';

type LeftBarPropsType = {
  kind: KindType;
  classes: ClassesType;
  client: QueryClient;
  t: i18next.TFunction;
};

const LeftBar: FunctionComponent<LeftBarPropsType> = ({
  kind,
  classes,
  client,
  t,
}) => {
  const [localState] = useContext(localStateContext);
  const { mode, identifier } = useParams<
    keyof AdvancedSearchParamsType
  >() as AdvancedSearchParamsType;

  return (
    <section
      className={classNames(classes.leftBar, {
        [classes.expanded]: localState.UIState.searchesBarExpanded,
      })}
    >
      {(!mode || mode === 'view') && (
        <List t={t} classes={classes} client={client} />
      )}
      {['edit', 'new'].includes(mode as string) && (
        <EditForm
          classes={classes}
          mode={mode}
          identifier={identifier}
          kind={kind}
          client={client}
          t={t}
        />
      )}
    </section>
  );
};

export default LeftBar;
