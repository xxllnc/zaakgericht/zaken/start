// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { KindType, ClassesType } from '../../../../AdvancedSearch.types';
import SwitchV1Button from '../SwitchV1Button';
import { getURLMatches } from '../../../../library/library';

type KindChoicesPropsType = {
  kind: KindType;
  classes: ClassesType;
  t: i18next.TFunction;
};

const KindChoices: FunctionComponent<KindChoicesPropsType> = ({
  kind,
  classes,
  t,
}) => {
  const urlMatches = getURLMatches();
  return (
    <section className={classes.kindChoices}>
      <SwitchV1Button />
      <Link
        className={classNames(classes.typeChip, {
          [classes.typeChipActive]: kind === 'case',
        })}
        key={'case'}
        to={`/${urlMatches?.params.prefix}/${urlMatches?.params.module}/case`}
      >
        {t('kind.cases') as string}
      </Link>
      <Link
        className={classNames(classes.typeChip, {
          [classes.typeChipActive]: kind === 'custom_object',
        })}
        key={'custom_object'}
        to={`/${urlMatches?.params.prefix}/${urlMatches?.params.module}/custom_object`}
      >
        {t('kind.objects') as string}
      </Link>
    </section>
  );
};

export default KindChoices;
