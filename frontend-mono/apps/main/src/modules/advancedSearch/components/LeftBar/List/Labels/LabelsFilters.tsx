// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, {
  useState,
  FunctionComponent,
  Dispatch,
  SetStateAction,
} from 'react';
import * as i18next from 'i18next';
import { Autocomplete } from '@mui/material';
import TextField from '@mintlab/ui/App/Material/TextField';
import { renderTags } from '../../LeftBar.library';
import { useLabelsQuery } from '../../../../query/useLabels';
import { LabelsType } from '../../../../AdvancedSearch.types';

type LabelsFiltersPropsType = {
  classes: any;
  labels: LabelsType;
  setLabels: Dispatch<SetStateAction<LabelsType>>;
  t: i18next.TFunction;
};
const LabelsFilters: FunctionComponent<LabelsFiltersPropsType> = ({
  classes,
  labels,
  setLabels,
  t,
}) => {
  const [open, setOpen] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState('');
  const allLabels = useLabelsQuery();

  const handleOnChange = (
    event: React.ChangeEvent<any>,
    newValue: LabelsType
  ) => setLabels(newValue);

  return (
    <div className={classes.labelsFiltersWrapper}>
      <Autocomplete
        id={'labelsFilterSelect'}
        multiple={true}
        value={labels}
        options={allLabels?.data || []}
        open={open}
        onChange={handleOnChange}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        onInputChange={(event, newInputValue) => setInputValue(newInputValue)}
        inputValue={inputValue}
        renderInput={props => (
          <TextField
            variant="generic1"
            name={'textfieldName'}
            placeholder={t('labels.filterLabels')}
            {...props}
          />
        )}
        filterSelectedOptions={true}
        loading={Boolean(allLabels.isLoading)}
        classes={{
          root: classes.labelsAutoCompleteRoot,
        }}
        renderTags={renderTags({ editing: true, classes })}
      />
    </div>
  );
};

export default LabelsFilters;
