// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';

const LeftBarLoader = ({ show }: any) => {
  if (!show) return null;
  return (
    <div
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        background: 'rgba(255,255,255,0.5)',
        width: '100%',
        height: '100%',
        pointerEvents: 'none',
      }}
    >
      <Loader
        style={{
          position: 'absolute',
          top: '100px',
          left: '50px',
        }}
      />
    </div>
  );
};

export default LeftBarLoader;
