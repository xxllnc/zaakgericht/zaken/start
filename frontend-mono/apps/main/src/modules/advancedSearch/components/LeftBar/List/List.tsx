// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { QueryClient } from '@tanstack/react-query';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Typography from '@mui/material/Typography';
import { useParams, useNavigate } from 'react-router-dom';
import { Box, useTheme } from '@mui/material';
import {
  LabelsType,
  EditFormStateType,
  AdvancedSearchParamsType,
} from '../../../AdvancedSearch.types';
import { getDefaultSavedSearches } from '../../../components/EditForm/EditForm.library';
import { useSavedSearchesQuery } from '../../../query/useList';
import { getURLMatches } from '../../../library/library';
import KindChoices from './KindChoices/KindChoices';
import LabelsFilters from './Labels/LabelsFilters';
import Row from './Row';
import LeftBarLoader from './LeftBarLoader';

type ListPropsType = {
  t: i18next.TFunction;
  classes: any;
  client: QueryClient;
};

const List: FunctionComponent<ListPropsType> = ({ t, classes, client }) => {
  const { kind, identifier } = useParams<
    keyof AdvancedSearchParamsType
  >() as AdvancedSearchParamsType;
  const theme = useTheme<Theme>();
  const [labels, setLabels] = useState<LabelsType>([]);
  const navigate = useNavigate();
  const urlMatches = getURLMatches();
  const session = useSession();
  const listQuery = useSavedSearchesQuery({
    kind,
    labels,
  });
  const { data, hasNextPage, fetchNextPage } = listQuery;
  const defaultSearches = getDefaultSavedSearches({ kind, t, session }).filter(
    savedSearch => savedSearch.uuid !== 'intake'
  );

  return (
    <>
      <LeftBarLoader show={listQuery.isLoading || listQuery.isFetching} />
      <KindChoices kind={kind} classes={classes} t={t} />
      <div className={classes.newSavedSearchWrapper}>
        <div>
          <Typography variant="h6">{t('savedSearches') as string}</Typography>
        </div>
        <div>
          <Tooltip enterDelay={250} title={t('newSavedSearch')}>
            <Button
              action={() => {
                navigate(
                  `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}/new`,
                  { state: { from: 'list' } }
                );
              }}
              name="newSavedSearch"
              icon="add_circle_outline"
              sx={{ color: theme.palette.primary.main }}
            />
          </Tooltip>
        </div>
      </div>

      <LabelsFilters
        classes={classes}
        labels={labels}
        setLabels={setLabels}
        t={t}
      />

      {kind === 'case' &&
        defaultSearches.map(entry => (
          <Row
            data={entry}
            key={entry.uuid}
            classes={classes}
            client={client}
            t={t}
            identifier={identifier}
            kind={kind}
          />
        ))}

      {kind === 'case' && (
        <Box className={classes.defaultSeperator} sx={{ height: 32 }} />
      )}

      {(data?.pages || []).map((pageGroup: any) =>
        pageGroup.data.map((entry: EditFormStateType) => (
          <Row
            data={entry}
            key={entry.uuid}
            classes={classes}
            client={client}
            t={t}
            identifier={identifier}
            kind={kind}
          />
        ))
      )}
      {hasNextPage ? (
        <div className={classes.loadMore}>
          <Button
            name="loadMore"
            iconSize="small"
            action={() => fetchNextPage()}
            sx={{
              backgroundColor: theme.palette.primary.main,
              '&:hover': {
                backgroundColor: theme.palette.primary.main,
              },
              color: theme.palette.common.white,
            }}
            disabled={listQuery.isFetching}
          >
            {t('loadMore')}
          </Button>
        </div>
      ) : null}
    </>
  );
};

export default List;
