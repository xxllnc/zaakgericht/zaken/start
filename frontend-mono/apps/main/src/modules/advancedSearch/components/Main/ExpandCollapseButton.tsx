// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useContext, FunctionComponent, useState } from 'react';
import classNames from 'classnames';
import * as i18next from 'i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { localStateContext } from '../../context/LocalStateProvider';

type ExpandCollapseButtonPropsType = {
  classes: any;
  t: i18next.TFunction;
  expanded: boolean;
};

const ExpandCollapseButton: FunctionComponent<
  ExpandCollapseButtonPropsType
> = ({ classes, t, expanded }) => {
  const [localState, setLocalState] = useContext(localStateContext);
  const [open, setOpen] = useState(false);

  const handleClose = () => setOpen(false);
  const handleOpen = () => setOpen(true);

  return (
    <div
      className={classNames(classes.expandCollapseButtonWrapper, {
        [classes.expandCollapseButtonWrapperExpanded]: expanded,
      })}
    >
      <Tooltip
        title={t('expandCollapse')}
        placement="top"
        onClose={handleClose}
        onClick={handleClose}
        onOpen={handleOpen}
        open={open}
      >
        <IconButton
          onClick={() => {
            setLocalState({
              ...localState,
              UIState: {
                ...localState.UIState,
                searchesBarExpanded: !localState.UIState.searchesBarExpanded,
              },
            });
          }}
          disableRipple={true}
          size="small"
          sx={{
            width: 34,
            marginRight: '2px',
          }}
          classes={{
            root: classes.expandCollapseButton,
          }}
        >
          {
            <Icon size="small" color="inherit">
              {localState.UIState.searchesBarExpanded
                ? iconNames.chevron_right
                : iconNames.chevron_left}
            </Icon>
          }
        </IconButton>
      </Tooltip>
    </div>
  );
};

export default ExpandCollapseButton;
