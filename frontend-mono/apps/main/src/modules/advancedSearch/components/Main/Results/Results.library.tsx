// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import Button from '@mintlab/ui/App/Material/Button';
import * as i18next from 'i18next';
import fecha from 'fecha';
import classNames from 'classnames';
import { UseQueryResult } from '@tanstack/react-query';
import IconButton from '@mui/material/IconButton';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import parseISO from 'date-fns/parseISO';
import {
  REGEXP_TAGS,
  REGEXP_DATE_DAY,
} from '@zaaksysteem/common/src/constants/regexes';
import { get } from '@mintlab/kitchen-sink/source';
import { asArray } from '@mintlab/kitchen-sink/source';
import {
  GridColDef,
  GridInitialState,
  GRID_CHECKBOX_SELECTION_COL_DEF,
} from '@mui/x-data-grid-pro';
import Tooltip from '@mintlab/ui/App/Material/Tooltip/';
import { capitalize } from '@mintlab/kitchen-sink/source';
import { formatCurrency } from '@zaaksysteem/common/src/library/currency';
import { getLabel } from '../Main.library';
import { getUniqueColumnIdentifier } from '../../../library/library';
import {
  ModeType,
  SavedSearchType,
  APISearchRowType,
  ColumnType,
  ParseResultsModeType,
} from '../../../AdvancedSearch.types';
import {
  DATAGRID_KEY_PREFIX,
  CHECKBOX_COLUMN_ID,
} from '../../../library/config';
import StatusIcon from './components/StatusIcon/StatusIcon';

type LinkButtonPropsType = {
  title: string;
  url: string;
  t: i18next.TFunction;
};
export const LinkButton: FunctionComponent<LinkButtonPropsType> = ({
  title,
  url,
  t,
}) => (
  <Button
    name="openInNewWindow"
    title={t('results:openInNew')}
    action={(event: any) => {
      event.stopPropagation();
      window.open(url, '_blank');
    }}
    endIcon={<Icon size="extraSmall">{iconNames.open_in_new}</Icon>}
    variant={'text'}
    sx={{
      color: ({ palette: { primary } }: any) => primary.main,
    }}
  >
    {title}
  </Button>
);

// Parses Advanced Search columns definitions into table ready column
export const getDataGridColumns = (
  columns: ColumnType[],
  classes: any
): GridColDef[] => {
  return (columns || []).map(column => {
    const { label } = column;

    const getSizeProps = () => {
      if (column.type === 'id' || column.type === 'uuid') {
        return {
          width: 280,
        };
      } else {
        return {
          flex: 1,
        };
      }
    };
    return {
      field: getUniqueColumnIdentifier(column),
      headerName: label,
      sortable: false,
      renderCell: params => {
        const thisRowData = params.value;
        return (
          <div className={classes.cellWrapper}>
            {typeof thisRowData === 'object' ? (
              thisRowData
            ) : (
              <Tooltip title={thisRowData} enterNextDelay={750}>
                {thisRowData}
              </Tooltip>
            )}
          </div>
        );
      },
      ...getSizeProps(),
      ...(column.columnProps || {}),
    };
  });
};

export const Label = ({
  classes,
  t,
  mode,
  currentQuery,
  totalResults = 0,
}: {
  classes: any;
  t: i18next.TFunction;
  mode: ModeType;
  currentQuery: UseQueryResult<SavedSearchType>;
  totalResults: number;
}) => {
  const topLabel = getLabel({ mode, currentQuery, t });
  return (
    <span className={classes.mainTopBarName}>
      {topLabel} {totalResults ? `(${totalResults})` : ''}
    </span>
  );
};

export const getDataGridState = ({
  identifier,
  columns,
  state,
}: {
  identifier: string;

  columns?: ColumnType[];
  state?: GridInitialState;
}): GridInitialState => {
  const existingState = state || getLocalDataGridState(identifier) || {};

  /* eslint complexity: [2, 12] */
  const getColumns = () => {
    const columnVisibilityModel = (columns || []).reduce((acc, current) => {
      acc[current.type] = current.visible;
      return acc;
    }, {} as any);

    const dimensions =
      existingState?.columns?.dimensions ||
      (columns || []).reduce((acc, current) => {
        acc[current.type] = {
          width: 100,
          flex: 1,
        };
        return acc;
      }, {} as any);

    const orderedFields = [
      CHECKBOX_COLUMN_ID,
      ...(columns || []).map(column => getUniqueColumnIdentifier(column)),
    ];

    return {
      columns: {
        ...(existingState?.columns || {}),
        ...(columnVisibilityModel && { columnVisibilityModel }),
        dimensions,
        orderedFields,
      },
    };
  };

  const getFilters = () => {
    return (
      existingState?.filter || {
        filter: {
          filterModel: {
            items: [],
          },
        },
      }
    );
  };

  const getPinnedColumns = () => {
    const leftFiltered = (existingState?.pinnedColumns?.left || []).filter(
      (columnName: string) =>
        columnName !== GRID_CHECKBOX_SELECTION_COL_DEF.field
    );

    return {
      pinnedColumns: {
        ...(existingState?.pinnedColumns || {}),
        left: [GRID_CHECKBOX_SELECTION_COL_DEF.field, ...(leftFiltered || [])],
      },
    };
  };

  return {
    ...existingState,
    ...getColumns(),
    ...getFilters(),
    ...getPinnedColumns(),
  };
};

export function saveLocalDataGridState(identifier: string, state: any) {
  const key = `${DATAGRID_KEY_PREFIX}${identifier}`;
  localStorage.setItem(key, JSON.stringify(state));
}

export function getLocalDataGridState(identifier: string) {
  const item = localStorage.getItem(`${DATAGRID_KEY_PREFIX}${identifier}`);
  return item ? JSON.parse(item) : null;
}

export function deleteLocaleDataGridState(identifier: string) {
  localStorage.removeItem(`${DATAGRID_KEY_PREFIX}${identifier}`);
}

const isBoolean = (str: string) =>
  ['true', 'false'].includes(str.toString().trim());

/**
 * Currently supports rendering of these attribute types:
  Geo
  Relatie (object + subject)
  Rekeningnummer
  Datum
  E-mail
  Geocoordinaten (lat/lon)
  Numeriek
  Enkelvoudige keuze
  Keuzelijst
  Tekstveld
  Groot tekstveld
  Webadres
  Valuta
  Document
 */
/* eslint complexity: [2, 10] */

type GetResultValueType = {
  classes?: any;
  column: ColumnType;
  row: APISearchRowType;
  t: i18next.TFunction;
  parseResultsMode: ParseResultsModeType;
};

export const getResultValue = ({
  classes,
  column,
  row,
  t,
  parseResultsMode,
}: GetResultValueType) => {
  const { source, type, magicString } = column;
  const joined = source.join('.');
  const baseValue = get(row, joined, '') as any;
  const caseNumber = get(row, 'attributes.number');
  const getType = () => {
    if (['caseTypeLeadTimeLegal', 'caseTimeService'].includes(type)) {
      return 'caseTypeLeadTime';
    } else if (magicString) {
      return type;
    } else if (isPopulatedArray(source)) {
      return baseValue?.type || type;
    } else {
      return type;
    }
  };
  const mapType = getType();
  const sanitize = (value: any) =>
    value === null || value === undefined ? '' : value;
  const safeValue = (value: any) =>
    // Explicitly casting to the value to string. If the value is
    // converted by this function we do not want anything but a string
    // to be shown or exported.
    typeof value === 'object' ? JSON.stringify(value) : `${value}`;

  const actualValue = (strict = false) => {
    if (strict) return baseValue;
    return baseValue?.value && baseValue?.type
      ? sanitize(baseValue.value)
      : sanitize(baseValue);
  };

  const getDate = (value: any) => {
    if (!value) {
      return null;
    } else if (REGEXP_DATE_DAY.test(value)) {
      //@ts-ignore
      return fecha.format(fecha.parse(value, 'YYYY-MM-DD'), 'DD-MM-YYYY');
    } else if (/^\d{4}-\d{2}-\d{2}T/.test(value)) {
      return fecha.format(
        //@ts-ignore
        fecha.parse(value, 'YYYY-MM-DDTHH:mm:SS'),
        'DD-MM-YYYY HH:mm:SS'
      );
    }
    return null;
  };

  // @ts-ignore
  const getFormattedCurrency = () => formatCurrency(t, actualValue());

  const formatAddressV2 = () => actualValue()?.address?.full || '';

  const typeMapping: any = () =>
    typeResolvers[parseResultsMode] && typeResolvers[parseResultsMode][mapType]
      ? typeResolvers[parseResultsMode][mapType]
      : null;

  const getRelationships = () => {
    if (baseValue?.specifics) return asArray(baseValue);
    if (baseValue?.value && baseValue?.value?.specifics)
      return asArray(baseValue.value);
    if (isPopulatedArray(baseValue?.value?.value))
      return asArray(baseValue.value.value);
    return [];
  };

  const getDaysDiff = () => {
    const {
      attributes: { registration_date, target_completion_date },
    } = row;
    if (!target_completion_date || !registration_date) return null;

    return differenceInCalendarDays(
      parseISO(target_completion_date),
      new Date()
    );
  };

  const getChoiceValue = (fieldKey: string) => {
    return actualValue()
      ? t(
          `search:editForm.fields.filters.fields.${fieldKey}.choices.${actualValue()}`
        )
      : '';
  };

  const getFileValues = () => {
    if (type === 'file' && isPopulatedArray(baseValue.value)) {
      return asArray(baseValue.value).map((file: any) => {
        return caseNumber ? (
          <LinkButton
            title={file.filename}
            url={`/intern/zaak/${caseNumber}/documenten/`}
            t={t}
          />
        ) : (
          <span>{file.filename}</span>
        );
      });
    }
  };

  const getMultiValueValue = () => asArray(actualValue()).join(', ');
  const getDepartment = () => actualValue()?.meta?.summary || '';
  const getRoles = () =>
    asArray(actualValue())
      .map(val => val?.meta?.summary)
      .join(', ');

  const getStatusIcon = (summary: boolean = false) => (
    <div className={summary ? classes.summaryStatus : ''}>
      <StatusIcon
        status={actualValue()}
        archivalState={row.attributes.archival_state}
        destructionDate={row.attributes.destruction_date}
        unreadMessagesCount={row.attributes.unread_message_count}
        unacceptedAttributesCount={
          row.attributes.unaccepted_attribute_update_count
        }
        unacceptedFilesCount={row.attributes.unaccepted_files_count}
        t={t}
      />
    </div>
  );

  const getAssignee = () => {
    const assignee = row?.relationships?.assignee?.data.meta.summary;
    const group =
      row?.relationships?.role?.data?.relationships?.parent?.meta?.summary;

    return (
      <>
        {assignee && (
          <Tooltip title={assignee} placement="left-start" enterNextDelay={200}>
            <div className={classes.summaryIcon}>
              <Icon size="extraSmall">{iconNames.person}</Icon>
            </div>
          </Tooltip>
        )}
        {!assignee && group && (
          <Tooltip title={group} placement="left-start" enterNextDelay={200}>
            <div className={classes.summaryIcon}>
              <Icon size="extraSmall">{iconNames.group}</Icon>
            </div>
          </Tooltip>
        )}
      </>
    );
  };

  const getSummaryContents = () => {
    const subject = row.attributes?.subject;
    const requestor = row?.relationships?.requestor?.data.meta.summary;

    return (
      <div className={classNames(classes.summaryContents)}>
        <div className={classes.summaryContentsHead}>
          {row.attributes.number} -{' '}
          {row.relationships?.case_type?.data?.attributes?.name}
        </div>

        {subject ? (
          <div className={classes.summaryContentsExtra}>
            {row.attributes?.subject}
          </div>
        ) : null}

        <div className={classes.summaryContentsExtra}>
          {requestor && (
            <div className={classes.summaryIcon}>
              <Icon size="extraSmall">{iconNames.person}</Icon> {requestor}
            </div>
          )}
        </div>
      </div>
    );
  };

  const getSummaryDate = () => {
    const date = fecha.format(
      fecha.parse(row.attributes.registration_date, 'YYYY-MM-DD') as Date,
      'DD MMM YYYY'
    );
    return <div className={classes.summaryDate}>{date}</div>;
  };

  const getDays = () => {
    const daysDiff = getDaysDiff();
    if (!daysDiff || isNaN(daysDiff)) return null;
    return (
      <div className={classes.daysWrapper}>
        <span
          className={classNames(classes.days, {
            [classes.daysToday]: daysDiff === 0,
            [classes.daysUrgent]: daysDiff < 0,
            [classes.daysNonUrgent]: daysDiff > 0,
          })}
        >
          {`${daysDiff}`}
        </span>
      </div>
    );
  };

  const getAssigneeControls = () => {
    return (
      <div
        className={classNames(
          classes.summaryContentsExtra,
          classes.assigneeControls
        )}
      >
        {getAssignee()}
        <IconButton
          onClick={() => {}}
          title={'accept'}
          size="small"
          className={''}
        >
          <Icon size="small" color="bluebird">
            {iconNames.play_circle_filled}
          </Icon>
        </IconButton>
        <IconButton
          onClick={() => {}}
          title={'accept'}
          size="small"
          className={''}
        >
          <Icon size="small" color="danger">
            {iconNames.cancel}
          </Icon>
        </IconButton>
      </div>
    );
  };

  const getActions = () => null;

  /*
  // Add more functions to typeResolvers to handle specific mode/type
  // combinations. Any combination not present here will fall back to the
  // generic fallbacks (see bottom). It is also possible to create custom
  // handling for column types that are not directly tied to a field on the
  // backend. This way we can create compound column types, like a combined
  // address.
  */

  const typeResolvers: {
    [key in ParseResultsModeType]: { [type: string]: () => any };
  } = {
    screen: {
      text: () => safeValue(asArray(actualValue()).join(', ')),
      document: () =>
        asArray(actualValue()).map(document => (
          <LinkButton
            key={document.value}
            title={document.label}
            url={`/api/v2/cm/custom_object/download_file?object_uuid=${row.id}&file_uuid=${document.value}`}
            t={t}
          />
        )),
      email: () =>
        actualValue() ? (
          <LinkButton
            title={actualValue()}
            url={`mailto:${actualValue()}`}
            t={t}
          />
        ) : null,
      date: () => getDate(actualValue()) || '',
      geojson: () => (
        <LinkButton
          title={t('results:types.geojson')}
          url={`/main/object/${row?.id}/map`}
          t={t}
        />
      ),
      url: () =>
        actualValue() ? (
          <LinkButton title={actualValue()} url={actualValue()} t={t} />
        ) : null,
      address_v2: formatAddressV2,
      valuta: getFormattedCurrency,
      valutaex: getFormattedCurrency,
      valutaex21: getFormattedCurrency,
      valutaex6: getFormattedCurrency,
      valutain: getFormattedCurrency,
      valutain21: getFormattedCurrency,
      valutain6: getFormattedCurrency,
      currency: getFormattedCurrency,
      relationship: () => {
        const getRelationshipProps = (relationship: any) => {
          const { specifics, value } = relationship;
          switch (specifics?.relationship_type) {
            case 'custom_object':
              return {
                url: `/main/object/${value}/`,
                title:
                  specifics?.metadata?.summary ||
                  t('results:types.relationshipTypes.customObject'),
              };
            case 'person':
            case 'subject':
            case 'employee':
            case 'organization':
              return {
                url: `/redirect/contact_page?uuid=${value}`,
                title:
                  specifics?.metadata?.summary ||
                  t('results:types.relationshipTypes.subject'),
              };
            default:
              return {
                url: '/',
                title: t('results:types.relationshipTypes.unknown'),
              };
          }
        };

        if (!actualValue()) return null;

        return getRelationships().map((relationship: any) => {
          const props = getRelationshipProps(relationship);
          return props ? (
            <LinkButton title={props.title} url={props.url} t={t} />
          ) : null;
        });
      },
      days: () => getDays(),
      caseProgress: () => {
        if (actualValue()) {
          return (
            <div className={classes.progressWrapper}>
              <div
                className={classes.progressInner}
                style={{ width: `${actualValue()}%` }}
              >
                &nbsp;
              </div>
            </div>
          );
        }
      },
      caseStatus: () => getStatusIcon(false),
      caseTypeLeadTime: () => `${baseValue.value} ${baseValue.type}`,
      resultResult: () => getChoiceValue('result'),
      resultRetentionPeriodSourceDate: () =>
        getChoiceValue('retentionPeriodSourceDate'),
      numeric: () => getMultiValueValue(),
      file: () => getFileValues(),
      textarea: () => asArray(actualValue()).join(', '),
      richtext: () => actualValue().replace(REGEXP_TAGS, ''),
      caseContactChannel: () => capitalize(actualValue()),
      assigneeDepartment: getDepartment,
      assigneeRoles: getRoles,
      assigneeControls: getAssigneeControls,
      actions: getActions,
      summaryStatus: () => getStatusIcon(true),
      summaryContents: getSummaryContents,
      summaryDays: getDays,
      summaryDate: getSummaryDate,
      summaryAssigneeControls: getAssigneeControls,
      summaryActions: getActions,
    },
    export: {
      document: () =>
        asArray(actualValue()).map((document: any) => document.label),
      geojson: () => {
        const geoValue = actualValue();
        const features =
          geoValue && isPopulatedArray(geoValue.features)
            ? geoValue.features[0]
            : null;

        if (features && isPopulatedArray(features.geometry.coordinates)) {
          const coordinates = features.geometry.coordinates;
          if (Array.isArray(coordinates[0])) {
            return coordinates[0]
              .map((coord: any) => `(${coord.join(', ')})`)
              .join(', ');
          } else {
            return coordinates?.join(', ');
          }
        }
        return '';
      },
      address_v2: formatAddressV2,
      currency: getFormattedCurrency,
      valuta: getFormattedCurrency,
      relationship: () => {
        if (!actualValue()) return '';
        return getRelationships()
          .map(
            (relationship: any) =>
              relationship?.specifics?.metadata?.summary || ''
          )
          .join(', ');
      },
      days: () => getDaysDiff() || '',
      assigneeDepartment: getDepartment,
      assigneeRoles: getRoles,
    },
    raw: {
      relationship: () => safeValue(actualValue(true)),
      days: () => getDaysDiff() || '',
    },
  };

  if (typeMapping()) {
    //@ts-ignore
    return typeMapping()();
    // Generic fallbacks
  } else if (parseResultsMode === 'raw') {
    return safeValue(actualValue());
  } else if (getDate(actualValue())) {
    return getDate(actualValue());
  } else if (isBoolean(actualValue())) {
    return t(`common:boolean.${actualValue()}`);
  } else if (
    typeof actualValue() === 'string' ||
    actualValue() instanceof String
  ) {
    return actualValue();
  } else {
    return safeValue(actualValue());
  }
};
