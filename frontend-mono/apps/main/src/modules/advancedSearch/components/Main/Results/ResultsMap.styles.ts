// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    resultsMapWrapper: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      flexGrow: 1,
    },
  };
});
