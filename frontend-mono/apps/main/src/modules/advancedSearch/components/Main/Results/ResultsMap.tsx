// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import { Geojson } from '@mintlab/ui/types/MapIntegration';
import { useStyles } from './ResultsMap.styles';

type ResultsMapPropsType = {
  features?: Geojson[];
};

const ResultsMap: FunctionComponent<ResultsMapPropsType> = ({ features }) => {
  const classes = useStyles();

  return (
    <div className={classes.resultsMapWrapper}>
      <GeoMap
        geoFeature={{
          type: 'FeatureCollection',
          //@ts-ignore
          features,
        }}
        name="ObjectMap"
        canDrawFeatures={false}
        minHeight="100%"
        context={{ type: 'AdvancedSearch', data: null }}
      />
    </div>
  );
};

export default ResultsMap;
