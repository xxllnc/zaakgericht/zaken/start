// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  Dispatch,
  SetStateAction,
  MutableRefObject,
  useState,
  useMemo,
} from 'react';
import deepEqual from 'fast-deep-equal';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { v4 } from 'uuid';
import { createPortal } from 'react-dom';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import Button from '@mintlab/ui/App/Material/Button';
import Box from '@mui/material/Box';
import {
  DataGridPro,
  GridColDef,
  useGridApiRef,
  GridValidRowModel,
  GridRowSelectionModel,
  GridRowParams,
  MuiEvent,
  GridApiCommon,
} from '@mui/x-data-grid-pro';
import * as i18next from 'i18next';
import GRID_DEFAULT_LOCALE_TEXT from '@zaaksysteem/common/src/locale/datagrid.locale';
import { useDebouncedCallback } from 'use-debounce';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import TableSelectionControls from '@zaaksysteem/common/src/components/TableSelectionControls/TableSelectionControls';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { isUUID } from '../../../library/library';
import { TABLE_SUMMARY_BREAKPOINT } from '../../../library/config';
import {
  ResultRowType,
  KindType,
  CapabilitiesType,
  ColumnType,
} from '../../../AdvancedSearch.types';
import { CustomColumnMenu } from './ResultsTable.library';
import { getDataGridColumns, saveLocalDataGridState } from './Results.library';

type ResultsTablePropsType = {
  columns: ColumnType[];
  rows: ResultRowType[];
  t: i18next.TFunction;
  kind: KindType;
  selectionProps: useSelectionBehaviourReturnType;
  dataGridState: any;
  identifier: string;
  resetToInitialState: (apiRef: MutableRefObject<GridApiCommon>) => void;
  isLoading: boolean;
  classes: any;
  topBarRightRef: HTMLElement | null | undefined;
  capabilities: CapabilitiesType;
  setShowSummary: Dispatch<SetStateAction<boolean>>;
  showSummary: boolean;
  isFetching: boolean;
};

const DEBOUNCE_DELAY = 100;

/* eslint complexity: [2, 10] */
const ResultsTable: FunctionComponent<ResultsTablePropsType> = ({
  columns,
  rows,
  t,
  kind,
  selectionProps,
  dataGridState,
  identifier,
  resetToInitialState,
  isFetching,
  classes,
  topBarRightRef,
  showSummary,
  setShowSummary,
  capabilities,
}) => {
  const [tableWidth, setTableWidth] = useState<number>(0);
  const [gridKey, setGridKey] = useState<string>(identifier);
  const apiRef = useGridApiRef();
  const isDefaultIdentifier = isUUID(identifier) === false;
  const memoizedGridColumns: GridColDef[] = useMemo(
    () => getDataGridColumns(columns, classes),
    [showSummary, columns]
  );

  useTransition(
    (prev: number) =>
      setShowSummary(
        capabilities.summary === true && tableWidth < TABLE_SUMMARY_BREAKPOINT
      ),
    [tableWidth]
  );

  // Re-initialize the DataGrid when needed
  useTransition(
    (prevColumns: ColumnType[], prevShowSummary: boolean) => {
      if (
        (prevColumns && columns && !deepEqual(prevColumns, columns)) ||
        prevShowSummary !== showSummary
      ) {
        setGridKey(v4());
      }
    },
    [columns, showSummary]
  );

  const handleDoubleClick = (
    params: GridRowParams,
    event: MuiEvent<React.MouseEvent>
  ) => {
    event.stopPropagation();
    event.preventDefault();

    const url =
      kind === 'custom_object'
        ? `/main/object/${params.row.versionIndependentUuid}`
        : `/redirect/case?uuid=${params.row.uuid}`;
    top?.window.open(url, '_new');
  };

  const parsedRows: GridValidRowModel[] = rows.map(
    ({ columns, uuid, versionIndependentUuid }) => ({
      ...columns,
      id: uuid,
      uuid,
      versionIndependentUuid,
    })
  );

  const [debounced] = useDebouncedCallback(() => {
    if (showSummary) return;
    let state = apiRef.current.exportState({
      exportOnlyDirtyModels: true,
    });
    saveLocalDataGridState(identifier, state);
  }, DEBOUNCE_DELAY);

  const {
    pageSelected,
    everythingSelected,
    onSelectEverything,
    selectEverythingTranslations,
  } = selectionProps || {};

  const createButtonPortal = () =>
    createPortal(
      <div>
        <Tooltip title={t('resetToDefault')}>
          <Button
            name="resetSettings"
            icon="delete_sweep"
            scope={`advanced-search:reset-settings`}
            iconSize="medium"
            sx={{ padding: '4px' }}
            action={() => resetToInitialState(apiRef)}
          />
        </Tooltip>
      </div>,
      topBarRightRef as Element
    );

  const getRowHeight = React.useCallback(() => {
    return showSummary ? 'auto' : 50;
  }, [showSummary]);

  return (
    <div className={classes.resultsTableWrapper}>
      {topBarRightRef && (createButtonPortal() as any)}
      {(pageSelected || everythingSelected) && (
        <div className={classes.resultsTableButtonBar}>
          <div>
            <TableSelectionControls
              pageSelected={pageSelected}
              everythingSelected={everythingSelected}
              onSelectEverything={onSelectEverything}
              selectEverythingTranslations={selectEverythingTranslations}
            />
          </div>
        </div>
      )}
      <div className={classes.resultsTable}>
        <Box sx={{ flex: 1, position: 'relative' }}>
          <Box sx={{ position: 'absolute', inset: 0 }}>
            <DataGridPro
              key={gridKey}
              apiRef={apiRef}
              initialState={showSummary ? {} : dataGridState}
              rows={parsedRows}
              getRowHeight={getRowHeight}
              columns={memoizedGridColumns}
              pagination={false}
              sortingMode="server"
              onRowSelectionModelChange={(
                rowSelectionModel: GridRowSelectionModel
              ) =>
                selectionProps.setSelectedRows(rowSelectionModel as string[])
              }
              disableRowSelectionOnClick={true}
              localeText={GRID_DEFAULT_LOCALE_TEXT}
              onRowDoubleClick={handleDoubleClick}
              loading={isFetching || !isPopulatedArray(memoizedGridColumns)}
              onColumnOrderChange={debounced}
              onColumnResize={debounced}
              onColumnVisibilityModelChange={debounced}
              onFilterModelChange={debounced}
              onPaginationModelChange={debounced}
              onPinnedColumnsChange={debounced}
              onSortModelChange={debounced}
              onMenuClose={debounced}
              onMenuOpen={debounced}
              onPreferencePanelClose={debounced}
              onPreferencePanelOpen={debounced}
              classes={{
                columnHeaderTitle: classes.resultsTableHeaderColumn,
              }}
              onResize={event => setTableWidth(event.width)}
              checkboxSelection={
                !showSummary && capabilities.checkboxSelection === true
              }
              {...(showSummary ? { columnHeaderHeight: 0 } : {})}
              disableColumnFilter={true}
              disableColumnReorder={true}
              disableColumnMenu={isDefaultIdentifier}
              disableColumnResize={isDefaultIdentifier}
              slots={{ columnMenu: CustomColumnMenu }}
            />
          </Box>
        </Box>
      </div>
    </div>
  );
};

export default ResultsTable;
