// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(
  ({ palette: { common, error, secondary, review, elephant } }: Theme) => {
    return {
      resultsMain: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
      },
      resultsTableWrapper: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        width: '100%',
        overflow: 'scroll',
      },
      resultsTable: {
        flexGrow: 1,
        overflow: 'auto',
        display: 'flex',
      },
      resultsTableButtonBar: {
        height: 56,
        display: 'flex',
        '&>:nth-child(1)': {
          width: '70%',
          justifyContent: 'flex-start',
          alignItems: 'center',
          display: 'flex',
          margin: '2px 0px 2px 12px',
        },
        '&>:nth-child(2)': {
          display: 'flex',
          flex: 1,
          justifyContent: 'flex-end',
          '&>div': {
            width: 40,
          },
        },
      },
      resultsTableHeaderColumn: {
        '&&': {
          fontWeight: 800,
        },
      },
      cellWrapper: {
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
      },
      daysWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      days: {
        display: 'block',
        padding: 4,
        color: common.white,
        borderRadius: 6,
        fontSize: 13,
      },
      daysUrgent: {
        backgroundColor: error.dark,
      },
      daysNonUrgent: {
        backgroundColor: secondary.main,
      },
      daysToday: {
        backgroundColor: review.main,
      },
      progressWrapper: {
        width: '100%',
        marginLeft: 12,
        marginRight: 12,
        height: 16,
        borderRadius: 8,
        backgroundColor: elephant.lightest,
      },
      progressInner: {
        height: 16,
        backgroundColor: secondary.main,
        borderRadius: 8,
      },
      summaryDate: {
        alignSelf: 'flex-start',
        marginTop: 8,
      },
      summaryStatus: {
        alignSelf: 'flex-start',
        marginTop: 6,
      },
      summaryContents: {
        display: 'flex',
        flexDirection: 'column',
        gap: 6,
        margin: '6px 0px 6px 0px',
      },
      summaryContentsHead: {
        fontWeight: 500,
        fontSize: 14,
      },
      summaryContentsExtra: {
        display: 'flex',
        alignItems: 'center',
        fontWeight: 'normal',
        color: elephant.main,
        gap: 14,
      },
      summaryIcon: {
        display: 'flex',
        gap: 4,
      },
      assigneeControls: {
        gap: 0,
        '&>:nth-child(1)': {
          marginRight: 4,
        },
      },
      statusMessage: {
        margin: 20,
      },
    };
  }
);
