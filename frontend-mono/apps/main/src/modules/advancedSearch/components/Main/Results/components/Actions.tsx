// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState } from 'react';
import * as i18next from 'i18next';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
//@ts-ignore
import DropdownMenu, {
  DropdownMenuList,
  //@ts-ignore
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import Button from '@mintlab/ui/App/Material/Button';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import DeleteDialog from '@zaaksysteem/common/src/components/Jobs/Dialogs/DeleteDialog/DeleteDialog';
import { KindType } from '../../../../AdvancedSearch.types';
import { FiltersType } from '../../../../AdvancedSearch.types.filters';

type ActionsPropsType = {
  t: i18next.TFunction;
  selectedRows: useSelectionBehaviourReturnType['selectedRows'];
  everythingSelected: boolean;
  kind: KindType;
  filters?: FiltersType | null;
};

/* eslint complexity: [2, 10] */
const Actions: FunctionComponent<ActionsPropsType> = ({
  t,
  selectedRows,
  everythingSelected,
  kind,
  filters,
}) => {
  const [dialog, setDialog] = useState<any>(null);
  const session = useSession();
  const capabilities = session?.logged_in_user?.capabilities || [];

  const buttons = [
    ...(kind === 'case' &&
    capabilities.some(cap => ['admin', 'zaak_beheer'].includes(cap))
      ? [
          {
            action: () => {
              setDialog({
                type: 'delete',
              });
            },
            title: t('actions.delete'),
          },
        ]
      : []),
  ];

  if (!buttons.length) return null;

  return (
    <>
      {dialog && dialog.type === 'delete' && (
        <DeleteDialog
          selectedRows={selectedRows}
          everythingSelected={everythingSelected}
          filters={filters}
          onClose={() => setDialog(null)}
          kind={kind}
        />
      )}
      <DropdownMenu
        transformOrigin={{
          vertical: -10,
          horizontal: 'center',
        }}
        trigger={<Button name="addDropdown">Acties</Button>}
      >
        <DropdownMenuList
          items={buttons.map(({ action, title }) => ({
            action,
            label: title,
            scope: `date-entries:add-dropdown-list`,
          }))}
        />
      </DropdownMenu>
    </>
  );
};

export default Actions;
