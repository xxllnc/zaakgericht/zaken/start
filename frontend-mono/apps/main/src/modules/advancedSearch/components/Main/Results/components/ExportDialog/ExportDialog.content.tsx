// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useState,
  Dispatch,
  SetStateAction,
} from 'react';
import classNames from 'classnames';
import { GridInitialState } from '@mui/x-data-grid-pro';
import * as i18next from 'i18next';
import { QueryClient } from '@tanstack/react-query';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { openSnackbar, snackbarMargin } from '@zaaksysteem/common/src/signals';
import { mapTranslationToChoice } from '../../../../../library/library';
import { QUERY_KEY_EXPORT_RESULTS } from '../../../../../query/constants';
import {
  ExportFormatType,
  KindType,
  ParseResultsModeType,
  SavedSearchType,
} from '../../../../../AdvancedSearch.types';
import { UseResultsQueryQueryParamsType } from '../../../../../query/useResults';
import { LinearProgressWithLabel } from './ExportProgress';
import { initiateDownload, getIntro } from './ExportDialog.library';
import ExportGeneratorDefault from './generators/ExportDialog.generator.default';
import ExportGeneratorArchive from './generators/ExportDialog.generator.archive';

/* eslint complexity: [2, 12] */
type ExportDialogContentPropsType = {
  classes: any;
  t: i18next.TFunction;
  queryClient: QueryClient;
  numPages: number;
  baseQueryParams: Omit<UseResultsQueryQueryParamsType, 'page'>;
  page: number;
  everythingSelected: boolean;
  selectedRows: string[];
  dataGridState: GridInitialState;
  template: SavedSearchType['template'];
  onClose: any;
  started: number | null;
  setStarted: Dispatch<SetStateAction<number | null>>;
  kind: KindType;
};

const ExportDialogContent: FunctionComponent<ExportDialogContentPropsType> = ({
  classes,
  t,
  queryClient,
  numPages,
  baseQueryParams,
  page,
  everythingSelected,
  selectedRows,
  dataGridState,
  template,
  onClose,
  started,
  setStarted,
  kind,
}) => {
  const [format, setFormat] = useState<ExportFormatType>('CSV');
  const [parseResultsMode, setParseResultsMode] =
    useState<ParseResultsModeType>('export');
  const [exportType, setExportType] =
    useState<SavedSearchType['template']>(template);
  const [progressPercentage, setProgressPercentage] = useState<number>(0);

  const cancel = async (type: 'cancel' | 'error') => {
    queryClient.cancelQueries([QUERY_KEY_EXPORT_RESULTS]);
    openSnackbar({
      sx: { color: '#DD2D4F', ...snackbarMargin },
      message:
        type === 'cancel'
          ? t('export.messages.cancel')
          : t('export.messages.error'),
    });
    reset();
  };

  const reset = () => {
    setStarted(null);
    setProgressPercentage(0);
  };

  const exportChoices = Object.entries(
    t(`export.exportTypes`, {
      returnObjects: true,
    })
  ).map(mapTranslationToChoice);

  return (
    <React.Fragment>
      <div style={{ marginBottom: 20 }}>
        {getIntro({ type: exportType, everythingSelected, selectedRows, t })}
      </div>
      <div className={classes.exportDialogSection}>
        <div className={classes.exportDialogOptionsRow}>
          <span>{t('export.typeLabel') as string}:</span>
          <div>
            <Select
              isClearable={false}
              onChange={event => setExportType(event.target.value)}
              choices={exportChoices}
              nestedValue={true}
              value={exportType}
              inputProps={{ readOnly: true }}
              disabled={Boolean(started)}
            />
          </div>
          <span />
        </div>
        {exportType === 'standard' && (
          <>
            <div className={classes.exportDialogOptionsRow}>
              <span>{t('export.options.exportTo.label') as string}</span>
              <div>
                <Select
                  isClearable={false}
                  onChange={event => setFormat(event.target.value)}
                  choices={['CSV', 'TSV'].map((format: string) => ({
                    label: format,
                    value: format,
                  }))}
                  nestedValue={true}
                  value={format}
                  inputProps={{ readOnly: true }}
                  disabled={Boolean(started)}
                />
              </div>
              <span>
                <Tooltip
                  title={t('export.options.exportTo.help')}
                  style={{ width: 'auto' }}
                >
                  <Icon size="extraSmall" color="inherit">
                    {iconNames.info_outlined}
                  </Icon>
                </Tooltip>
              </span>
            </div>
            <div className={classes.exportDialogOptionsRow}>
              <span>{t('export.options.celData.label') as string}</span>
              <div>
                <Select
                  isClearable={false}
                  onChange={event => setParseResultsMode(event.target.value)}
                  choices={[
                    {
                      label: t(
                        'export.options.celData.choices.parse'
                      ) as string,
                      value: 'export',
                    },
                    {
                      label: t(
                        'export.options.celData.choices.dontParse'
                      ) as string,
                      value: 'raw',
                    },
                  ]}
                  nestedValue={true}
                  value={parseResultsMode}
                  inputProps={{ readOnly: true }}
                  disabled={Boolean(started)}
                />
              </div>

              <span>
                <Tooltip
                  title={t('export.options.celData.help')}
                  style={{ width: 'auto' }}
                >
                  <Icon size="extraSmall" color="inherit">
                    {iconNames.info_outlined}
                  </Icon>
                </Tooltip>
              </span>
            </div>
          </>
        )}
      </div>
      <div
        className={classNames(
          classes.exportDialogSection,
          classes.exportDialogCommands
        )}
      >
        {started && exportType === 'standard' && (
          <ExportGeneratorDefault
            key={String(started)}
            format={format}
            parseResultsMode={parseResultsMode}
            numPages={numPages}
            baseQueryParams={baseQueryParams}
            t={t}
            queryClient={queryClient}
            page={page}
            everythingSelected={everythingSelected}
            selectedRows={selectedRows}
            handleError={() => cancel('error')}
            handleProgressUpdate={setProgressPercentage}
            handleDataDone={(downloadFile: any) => {
              initiateDownload(downloadFile, format);
              setStarted(null);
            }}
            dataGridState={dataGridState}
          />
        )}
        {started && exportType === 'archive_export' && (
          <ExportGeneratorArchive
            key={String(started)}
            kind={kind}
            baseQueryParams={baseQueryParams}
            t={t}
            queryClient={queryClient}
            everythingSelected={everythingSelected}
            selectedRows={selectedRows}
            handleError={() => cancel('error')}
            handleDataDone={(jobID: string) => {
              if (jobID) {
                openSnackbar({
                  sx: { color: '#DD2D4F', ...snackbarMargin },
                  message: t('export.messages.jobStarted'),
                });
                setStarted(null);
                onClose();
              }
            }}
          />
        )}
        <div>
          {exportType === 'standard' && (
            <LinearProgressWithLabel
              disabled={!started}
              value={progressPercentage}
            />
          )}
        </div>
        <div>
          {!started && (
            <Button
              name="startBtn"
              onClick={() => {
                reset();
                setStarted(Date.now());
              }}
            >
              {t('export.buttons.start')}
            </Button>
          )}
          {started && (
            <Button
              color="warning"
              disabled={!started}
              name="cancelBtn"
              onClick={() => {
                cancel('cancel');
              }}
            >
              {t('export.buttons.cancel')}
            </Button>
          )}
        </div>
      </div>
    </React.Fragment>
  );
};

export default ExportDialogContent;
