// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useEffect, FunctionComponent } from 'react';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { QueryClient } from '@tanstack/react-query';
import { UseResultsQueryQueryParamsType } from '../../../../../../query/useResults';
import { QUERY_KEY_EXPORT_RESULTS } from '../../../../../../query/constants';
import { useArchiveExportQuery } from '../../../../../../query/useArchiveExport';
import { filtersToAPIFilters } from '../../../../../../query/useResults';
import { KindType } from '../../../../../../AdvancedSearch.types';

type ExportGeneratorPropsType = {
  key: string;
  baseQueryParams: Omit<UseResultsQueryQueryParamsType, 'page'>;
  handleDataDone: any;
  t: i18next.TFunction;
  queryClient: QueryClient;
  everythingSelected: boolean;
  selectedRows: string[];
  handleError: () => void;
  kind: KindType;
};

export type QueryPropsType = {
  filters?: APICaseManagement.CaseSearchFilterDefinition['filters'];
  case_uuids?: string[];
};

const ExportGeneratorArchive: FunctionComponent<ExportGeneratorPropsType> = ({
  baseQueryParams,
  handleDataDone,
  queryClient,
  everythingSelected,
  selectedRows,
  kind,
  t,
}) => {
  let queryProps = {} as QueryPropsType;
  if (everythingSelected) {
    queryProps.filters = filtersToAPIFilters(kind, baseQueryParams?.filters);
  } else if (isPopulatedArray(selectedRows)) {
    queryProps.case_uuids = selectedRows;
  }

  useArchiveExportQuery({
    queryProps,
    onSuccess: ({
      data,
    }: APICaseManagement.RequestArchiveExportResponseBody) => {
      handleDataDone(data?.id);
    },
    t,
  });

  useEffect(() => {
    return () => {
      queryClient.removeQueries([QUERY_KEY_EXPORT_RESULTS]);
    };
  }, []);

  return null;
};

export default ExportGeneratorArchive;
