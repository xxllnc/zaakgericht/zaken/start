// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { RelationshipType } from '../AdvancedSearch.types';

export const getResultAttributes = (t: i18next.TFunction) => [
  [
    'resultResult',
    `${t('attributes:result')}`,
    ['relationships', 'case_type_result', 'data', 'attributes', 'result'],
  ],
  [
    'resultDescription',
    `${t('attributes:subcategories.result')} ${t('attributes:description')}`,
    ['relationships', 'case_type_result', 'data', 'attributes', 'description'],
  ],
  [
    'resultOrigin',
    `${t('attributes:subcategories.result')} ${t('attributes:origin')}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'origin',
    ],
  ],
  [
    'resultResultExplanation',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:resultExplanation'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'result_explanation',
    ],
  ],
  [
    'resultStandardChoice',
    `${t('attributes:subcategories.result')} ${t('attributes:standardChoice')}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'standard_choice',
    ],
  ],
  [
    'resultRetentionPeriod',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:retentionPeriod'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'retention_period',
    ],
  ],
  [
    'resultRetentionPeriodSourceDate',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:retentionPeriodSourceDate'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'retention_period_source_date',
    ],
  ],
  [
    'resultProcessTerm',
    `${t('attributes:subcategories.result')} ${t('attributes:processTerm')}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_term',
    ],
  ],
  [
    'resultProcessTypeDescription',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:processTypeDescription'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_type_description',
    ],
  ],
  [
    'resultProcessTypeExplanation',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:processTypeExplanation'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_type_explanation',
    ],
  ],
  [
    'resultProcessTypeGeneric',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:processTypeGeneric'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_type_generic',
    ],
  ],
  [
    'resultProcessTypeName',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:processTypeName'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_type_name',
    ],
  ],
  [
    'resultProcessTypeNumber',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:processTypeNumber'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_type_number',
    ],
  ],
  [
    'resultProcessTypeObject',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:processTypeObject'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'process_type_object',
    ],
  ],
  [
    'resultSelectionList',
    `${t('attributes:subcategories.result')} ${t('attributes:selectionList')}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'selection_list',
    ],
  ],
  [
    'resultSelectionListNumber',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:selectionListNumber'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'properties',
      'selection_list_number',
    ],
  ],
  [
    'resultSelectionListStart',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:selectionListStart'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'selection_list_start',
    ],
  ],
  [
    'resultSelectionListEnd',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:selectionListEnd'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'selection_list_end',
    ],
  ],
  [
    'resultTriggerArchival',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:triggerArchival'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'trigger_archival',
    ],
  ],
  [
    'resultTypeOfArchiving',
    `${t('attributes:subcategories.result')} ${t(
      'attributes:typeOfArchiving'
    )}`,
    [
      'relationships',
      'case_type_result',
      'data',
      'attributes',
      'type_of_archiving',
    ],
  ],
];

export const getCaseAttributes = (t: i18next.TFunction) => [
  ['caseNumber', t('attributes:caseNumber'), ['attributes', 'number']],
  [
    'caseTypeTitle',
    t('attributes:caseType'),
    ['attributes', 'case_type_title'],
  ],
  [
    'caseStatus',
    `${t('attributes:subcategories.case')} ${t('attributes:status')}`,
    ['attributes', 'status'],
  ],
  [
    'registrationDate',
    `${t('attributes:subcategories.case')} ${t('attributes:registrationDate')}`,
    ['attributes', 'registration_date'],
  ],
  [
    'completionDate',
    `${t('attributes:subcategories.case')} ${t('attributes:completionDate')}`,
    ['attributes', 'completion_date'],
  ],
  [
    'destructionDate',
    `${t('attributes:subcategories.case')} ${t('attributes:destructionDate')}`,
    ['attributes', 'destruction_date'],
  ],
  ['days', `${t('attributes:days')}`, ['attributes', 'days']],
  ['caseProgress', t('attributes:progress'), ['attributes', 'progress']],
  [
    'extraInformationInternal',
    t('attributes:extraInformation'),
    ['attributes', 'subject'],
  ],
  [
    'extraInformationExternal',
    t('attributes:extraInformationExternal'),
    ['attributes', 'subject_external'],
  ],
  [
    'caseContactChannel',
    `${t('attributes:subcategories.case')} ${t('attributes:contactChannel')}`,
    ['attributes', 'contact_channel'],
  ],
];

export const getCaseLocationAttributes = (t: i18next.TFunction) => [
  [
    'caseLocationFullAddress',
    `${t('attributes:subcategories.caseLocation')} ${t(
      'attributes:fullAddress'
    )}`,
    ['relationships', 'case_location', 'data', 'attributes', 'full_address'],
  ],
  [
    'caseLocationLatLng',
    `${t('attributes:subcategories.caseLocation')} ${t(
      'attributes:coordinates'
    )}`,
    ['relationships', 'case_location', 'data', 'attributes', 'coordinates'],
  ],
];

export const getCaseTypeAttributes = (t: i18next.TFunction) => [
  [
    'caseTypeName',
    `${t('attributes:subcategories.caseType')} ${t('attributes:name')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'name'],
  ],
  [
    'caseTypeVersion',
    `${t('attributes:subcategories.caseType')} ${t('attributes:version')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'version'],
  ],
  [
    'caseTypeCreated',
    `${t('attributes:subcategories.caseType')} ${t('attributes:dateCreated')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'created'],
  ],
  [
    'caseTypeModified',
    `${t('attributes:subcategories.caseType')} ${t('attributes:lastModified')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'last_modified'],
  ],
  [
    'caseTypeDescription',
    `${t('attributes:subcategories.caseType')} ${t('attributes:description')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'description'],
  ],
  [
    'caseTypeTrigger',
    `${t('attributes:subcategories.caseType')} ${t('attributes:trigger')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'initiator_source'],
  ],
  [
    'caseTypeIdentification',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:identification'
    )}`,
    ['relationships', 'case_type', 'data', 'attributes', 'identification'],
  ],
  [
    'caseTypePublic',
    `${t('attributes:subcategories.caseType')} ${t('attributes:isPublic')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'is_public'],
  ],
  [
    'caseTypeTags',
    `${t('attributes:subcategories.caseType')} ${t('attributes:tags')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'tags'],
  ],
  [
    'caseTypeInitiatorType',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:initiatorType'
    )}`,
    ['relationships', 'case_type', 'data', 'attributes', 'initiator_type'],
  ],
  [
    'caseTypePayment',
    `${t('attributes:subcategories.caseType')} ${t('attributes:payment')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'payment'],
  ],
  [
    'caseTypeLeadTimeLegal',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:leadTimeLegal'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'terms',
      'lead_time_legal',
    ],
  ],
  [
    'caseTimeService',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:leadTimeService'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'terms',
      'lead_time_service',
    ],
  ],
  [
    'caseTypeActive',
    `${t('attributes:subcategories.caseType')} ${t('attributes:active')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'active'],
  ],
  [
    'caseTypeLegalBasis',
    `${t('attributes:subcategories.caseType')} ${t('attributes:legalBasis')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'legal_basis',
    ],
  ],
  [
    'caseTypeProcessDescription',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:processDescription'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'process_description',
    ],
  ],
  [
    'caseTypeProcessMayPostpone',
    `${t('attributes:subcategories.caseType')} ${t('attributes:mayPostpone')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'may_postpone',
    ],
  ],
  [
    'caseTypeProcessMayExtend',
    `${t('attributes:subcategories.caseType')} ${t('attributes:mayExtend')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'may_extend',
    ],
  ],
  [
    'caseTypeExtensionPeroid',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:extensionPeriod'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'extension_period',
    ],
  ],
  [
    'caseTypeAdjournPeriod',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:adjournPeriod'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'adjourn_period',
    ],
  ],
  [
    'caseTypeEWebform',
    `${t('attributes:subcategories.caseType')} ${t('attributes:eWebform')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'e_webform',
    ],
  ],
  [
    'caseTypeMotivation',
    `${t('attributes:subcategories.caseType')} ${t('attributes:motivation')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'motivation',
    ],
  ],
  [
    'caseTypePenaltyLaw',
    `${t('attributes:subcategories.caseType')} ${t('attributes:penaltyLaw')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'penalty_law',
    ],
  ],
  [
    'caseTypePurpose',
    `${t('attributes:subcategories.caseType')} ${t('attributes:purpose')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'metadata', 'purpose'],
  ],
  [
    'caseTypeArchiveClassificationCode',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:archiveClassificationCode'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'archive_classification_code',
    ],
  ],
  [
    'caseTypeDesignationOfConfidentiality',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:designationOfConfidentiality'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'designation_of_confidentiality',
    ],
  ],
  [
    'caseTypeResponsibleSubject',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:responsibleSubject'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'responsible_subject',
    ],
  ],
  [
    'caseTypeResponsibleRelationship',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:responsibleRelationship'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'responsible_relationship',
    ],
  ],
  [
    'caseTypePossibilityForObjectionAndAppeal',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:possibilityForObjectionAndAppeal'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'possibility_for_objection_and_appeal',
    ],
  ],
  [
    'caseTypePublication',
    `${t('attributes:subcategories.caseType')} ${t('attributes:publication')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'publication',
    ],
  ],
  [
    'caseTypePublicationText',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:publicationText'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'publication_text',
    ],
  ],
  [
    'caseTypeBAG',
    `${t('attributes:subcategories.caseType')} ${t('attributes:bag')}`,
    ['relationships', 'case_type', 'data', 'attributes', 'metadata', 'bag'],
  ],
  [
    'caseTypeLexSilencioPositivo',
    `${t('attributes:subcategories.caseType')} ${t(
      'attributes:lexSilencioPositivo'
    )}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'lex_silencio_positivo',
    ],
  ],
  [
    'caseTypeWKPB',
    `${t('attributes:subcategories.caseType')} ${t('attributes:WKPB')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'wkpb_applies',
    ],
  ],
  [
    'caseTypeLocalBasis',
    `${t('attributes:subcategories.caseType')} ${t('attributes:localBasis')}`,
    [
      'relationships',
      'case_type',
      'data',
      'attributes',
      'metadata',
      'local_basis',
    ],
  ],
];

export const getSystemAttributes = (t: i18next.TFunction) => [
  ['id', 'UUID', ['id']],
];

export const getRelationshipAttributes = (
  type: RelationshipType,
  t: i18next.TFunction
) => [
  [
    `relationship.${type}.summary`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:summary')}`,
    ['relationships', type, 'data', 'meta', 'summary'],
  ],
  [
    `relationship.${type}.contact_information.email`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:email'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'email',
    ],
  ],
  [
    `relationship.${type}.contact_information.mobile_number`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:mobile_number'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'mobile_number',
    ],
  ],
  [
    `relationship.${type}.contact_information.phone_number`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:phone_number'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'phone_number',
    ],
  ],

  [
    `relationship.${type}.contact_information.phone_number`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:phone_number'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'phone_number',
    ],
  ],
  [
    `relationship.${type}.contact_person.family_name`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:familyName'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_person',
      'family_name',
    ],
  ],
  [
    `relationship.${type}.contact_person.first_name`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:firstName'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_person',
      'first_name',
    ],
  ],
  [
    `relationship.${type}.contact_person.initials`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:initials'
    )}`,
    ['relationships', type, 'data', 'attributes', 'contact_person', 'initials'],
  ],
  [
    `relationship.${type}.contact_person.insertions`,
    `${t(`attributes:relationships.${type}`)} ${t(`attributes:subcategories.contact`)} ${t(
      'attributes:insertions'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_person',
      'insertions',
    ],
  ],
  [
    `relationship.${type}.contact_information.name`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:name')}`,
    ['relationships', type, 'data', 'attributes', 'name'],
  ],
  [
    `relationship.${type}.contact_information.gender`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:gender')}`,
    ['relationships', type, 'data', 'attributes', 'gender'],
  ],
  [
    `relationship.${type}.contact_information.date_of_birth`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:dateOfBirth')}`,
    ['relationships', type, 'data', 'attributes', 'date_of_birth'],
  ],
  [
    `relationship.${type}.contact_information.date_of_death`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:dateOfDeath')}`,
    ['relationships', type, 'data', 'attributes', 'date_of_death'],
  ],
  [
    `relationship.${type}.location_address.city`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:city')}`,
    ['relationships', type, 'data', 'attributes', 'location_address', 'city'],
  ],
  [
    `relationship.${type}.location_address.country`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:country')}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'country',
    ],
  ],
  [
    `relationship.${type}.location_address.street`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:street')}`,
    ['relationships', type, 'data', 'attributes', 'location_address', 'street'],
  ],
  [
    `relationship.${type}.location_address.street_number`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:streetNumber')}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'street_number',
    ],
  ],
  [
    `relationship.${type}.location_address.street_number_letter`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:streetNumberLetter')}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'street_number_letter',
    ],
  ],
  [
    `relationship.${type}.location_address.street_number_suffix`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:streetNumberSuffix')}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'street_number_suffix',
    ],
  ],
  [
    `relationship.${type}.location_address.zipcode`,
    `${t(`attributes:relationships.${type}`)} ${t(
      `attributes:subcategories.location`
    )} ${t('attributes:zipcode')}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'zipcode',
    ],
  ],
  [
    `relationship.${type}.name`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:name')}`,
    ['relationships', type, 'data', 'attributes', 'name'],
  ],
  [
    `relationship.${type}.organization_type`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:organizationType')}`,
    ['relationships', type, 'data', 'attributes', 'organization_type'],
  ],
  [
    `relationship.${type}.coc_number`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:cocNumber')}`,
    ['relationships', type, 'data', 'attributes', 'coc_number'],
  ],
  [
    `relationship.${type}.coc_location_number`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:cocLocationNumber')}`,
    ['relationships', type, 'data', 'attributes', 'coc_location_number'],
  ],
  [
    `relationship.${type}.rsin`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:rsin')}`,
    ['relationships', type, 'data', 'attributes', 'rsin'],
  ],
  [
    `relationship.${type}.correspondence_address`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:correspondenceAddress')}`,
    ['relationships', type, 'data', 'attributes', 'correspondence_address'],
  ],
  [
    `relationship.${type}.date_founded`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:dateFounded')}`,
    ['relationships', type, 'data', 'attributes', 'date_founded'],
  ],
  [
    `relationship.${type}.date_registered`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:dateRegistered')}`,
    ['relationships', type, 'data', 'attributes', 'date_registered'],
  ],
  [
    `relationship.${type}.date_ceased`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:dateCeased')}`,
    ['relationships', type, 'data', 'attributes', 'date_ceased'],
  ],
  [
    `relationship.${type}.source`,
    `${t(`attributes:relationships.${type}`)} ${t('attributes:source')}`,
    ['relationships', type, 'data', 'attributes', 'source'],
  ],
  ...(type === 'assignee'
    ? [
        [
          `${type}Department`,
          `${t(`attributes:relationships.${type}`)} ${t('attributes:department')}`,
          ['relationships', type, 'data', 'relationships', 'department'],
        ],
        [
          `${type}Roles`,
          `${t(`attributes:relationships.${type}`)} ${t('attributes:roles')}`,
          ['relationships', type, 'data', 'relationships', 'roles'],
        ],
      ]
    : []),
];
