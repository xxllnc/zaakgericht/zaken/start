// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    buttons: {
      export: 'Exporteren',
    },
    tableView: 'Tabelweergave',
    mapView: 'Kaartweergave',
    openInNew: 'Open in nieuwe tab',
    noResultsFound: 'Geen resultaten gevonden',
    resetToDefault: 'Herstel tabel naar standaardinstellingen',
    invalidSavedSearch:
      'Er is iets fout gegaan bij het laden van de zoekopdracht. Controleer de identificatiecode en probeer het opnieuw.',
    actions: {
      delete: 'Verwijderen',
    },
    types: {
      email: 'E-mailadres',
      geojson: 'Geo-json',
      url: 'Webadres',
      relationshipTypes: {
        customObject: 'Objectrelatie',
        subject: 'Contactrelatie',
        unknown: 'Onbekend',
      },
    },
    selectable: {
      select: 'Alleen de rijen op deze pagina zijn geselecteerd.',
      selectButton: 'Selecteer alle rijen',
      deselect: 'Alle rijen zijn geselecteerd.',
      deselectButton: 'Selecteer alleen de rijen op deze pagina',
      selectEverything: 'Selecteer alles',
      deselectEverything: 'Deselecteer alles',
    },
    export: {
      buttons: {
        start: 'Start export',
        cancel: 'Annuleren',
      },
      typeLabel: 'Type export',
      intro: 'U staat op het punt om uw gekozen zoekopdracht te exporteren.',
      allRows:
        'Alle rijen die voldoen aan de zoekopdracht zullen worden geëxporteerd.',
      oneRow: 'Eén rij zal worden geëxporteerd.',
      numRows: '{{numRows}} rijen zullen worden geëxporteerd.',
      waitInstructions:
        'Dit kan enige tijd in beslag nemen. Sluit dit venster niet tijdens het exporteren.',
      waitInstructionsArchiveExport:
        'Dit kan enige tijd in beslag nemen. De voortgang kan bekeken onder "$t(layout:modules.export)". Na het starten van de export zal dit venster gesloten worden.',
      title: {
        objects: 'Objecten exporteren',
        cases: 'Zaken exporteren',
      },
      exportTypes: {
        standard: 'Standaard',
        archive_export: 'SIP (Archive)',
      },
      options: {
        exportTo: {
          label: 'Exporteren naar',
          help: 'Selecteer hier of de kolommen in het exportbestand gescheiden moeten worden door een komma (CSV), of een tab (TSV).',
        },
        celData: {
          label: 'Cel-data',
          help: 'Selecteer hier of de gegevens in de cellen verwerkt moeten worden, of niet. \nBij "verwerken", zullen alle gegevens worden omgevormd naar een leesbaar formaat. Bij "niet verwerken" kunnen de gegevens formattering (zoals JSON) bevatten, zoals deze in Zaaksysteem zijn opgeslagen.',
          choices: {
            parse: 'Verwerken',
            dontParse: 'Niet verwerken',
          },
        },
      },
      messages: {
        jobStarted:
          'Het exporteren is gestart en kan bekeken worden onder "$t(layout:modules.export)".',
        cancel: 'Het exporteren is geannuleerd.',
        error:
          'Er is iets fout gegaan bij het verwerken van de gegevens. Het exporteren is geannuleerd.',
      },
    },
    pagination: {
      pageLocation: 'Pagina {{page}} van {{totalPages}}',
      next: 'Volgende pagina',
      previous: 'Vorige pagina',
      skipNext: "+{{skip}} pagina's",
      skipPrevious: "-{{skip}} pagina's",
      rowsPerPage: 'Rijen per pagina:',
    },
  },
};
