// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import fecha from 'fecha';
import { useQuery } from '@tanstack/react-query';
import * as i18next from 'i18next';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { QueryPropsType } from '../components/Main/Results/components/ExportDialog/generators/ExportDialog.generator.archive';
import { getFromQueryValues } from '../library/library';
import { QUERY_KEY_EXPORT_RESULTS } from './constants';

const startArchiveExport =
  (t: i18next.TFunction) =>
  async ({ queryKey }: any) => {
    const filters = getFromQueryValues(queryKey[1], 'filters');
    const case_uuids = getFromQueryValues(queryKey[1], 'case_uuids');
    const friendlyNameType = t('jobs:jobTypes.run_archive_export');
    const friendlyNameDate = fecha.format(
      new Date(),
      t('common:dates.dateAndTimeFormat')
    );
    let selection;

    if (filters) {
      selection = {
        filters,
        type: 'filter',
      };
    } else if (case_uuids) {
      selection = {
        id_list: case_uuids,
        type: 'id_list',
      };
    }

    const payload = {
      job_description: {
        job_type: 'run_archive_export',
        selection,
      },
      friendly_name: `${friendlyNameType} - ${friendlyNameDate}`,
    };

    let results =
      await request<APICaseManagement.RequestArchiveExportResponseBody>(
        'POST',
        '/api/v2/jobs/create_job',
        payload
      );
    return results;
  };

export const useArchiveExportQuery = ({
  queryProps,
  onSuccess,
  t,
}: {
  queryProps: QueryPropsType;
  onSuccess: any;
  t: i18next.TFunction;
}) => {
  return useQuery<
    APICaseManagement.RequestArchiveExportResponseBody,
    V2ServerErrorsType
  >([QUERY_KEY_EXPORT_RESULTS, queryProps], startArchiveExport(t), {
    enabled: true,
    onError: openServerError,
    onSuccess,
    staleTime: 0,
  });
};
