// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { APIAdmin } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { getFromQueryValues } from '../library/library';
import { QUERY_KEY_ATTRIBUTE_DETAILS } from './constants';

export const getAttributeDetails = async ({ queryKey }: any): Promise<any> => {
  const uuid = getFromQueryValues(queryKey[1], 'uuid');
  const url = buildUrl<APIAdmin.GetAttributeDetailRequestParams>(
    '/api/v2/admin/catalog/get_attribute_detail',
    {
      attribute_id: uuid,
    }
  );

  const response = await request<APIAdmin.GetAttributeDetailResponseBody>(
    'GET',
    url
  );
  return response.data;
};

export const useAttributeDetailsQuery = (
  uuid: string,
  config?: { [key: string]: any }
) => {
  return useQuery<APIAdmin.GetAttributeDetailResponseBody, V2ServerErrorsType>(
    [QUERY_KEY_ATTRIBUTE_DETAILS, { uuid }],
    getAttributeDetails,
    {
      ...config,
      enabled: true,
      onError: openServerError,
      keepPreviousData: false,
    }
  );
};
