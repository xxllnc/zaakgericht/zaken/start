// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery, useMutation } from '@tanstack/react-query';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { LabelsType } from '../AdvancedSearch.types';
import { QUERY_KEY_LABELS } from './constants';

export const getLabels = async (): Promise<any> => {
  const response =
    await request<APICaseManagement.GetSavedSearchLabelsResponseBody>(
      'GET',
      '/api/v2/cm/saved_search/get_labels'
    );
  return isPopulatedArray(response?.data)
    ? response.data
        .map(label => label.attributes.name)
        .sort((aLabel, bLabel) => aLabel.localeCompare(bLabel))
    : [];
};

export const useLabelsQuery = (config?: any) => {
  return useQuery<LabelsType, V2ServerErrorsType>(
    [QUERY_KEY_LABELS],
    getLabels,
    {
      ...config,
      enabled: true,
      onError: openServerError,
      keepPreviousData: false,
    }
  );
};

export const updateLabels = async ({
  labels,
  uuid,
}: {
  labels: LabelsType;
  uuid: string;
}): Promise<any> =>
  request<APICaseManagement.SetLabelsForSavedSearchResponseBody>(
    'POST',
    '/api/v2/cm/saved_search/set_labels',
    {
      saved_search_labels: labels,
      saved_search_uuids: [uuid],
    }
  );

export const useSingleUpdateLabelsMutation = () =>
  useMutation<any, V2ServerErrorsType, { labels: LabelsType; uuid: string }>(
    (variables: { labels: LabelsType; uuid: string }) => updateLabels(variables)
  );
