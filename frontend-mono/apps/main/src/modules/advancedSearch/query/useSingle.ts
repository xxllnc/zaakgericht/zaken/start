// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { useQuery, useMutation } from '@tanstack/react-query';
import * as i18next from 'i18next';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession/useSession.types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { asArray } from '@mintlab/kitchen-sink/source';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  SavedSearchType,
  IdentifierType,
  ModeType,
  KindType,
  AuthorizationsType,
  EditFormStateType,
  PermissionType,
} from '../AdvancedSearch.types';
import { FilterType } from '../AdvancedSearch.types.filters';
import {
  isUUID,
  getFromQueryValues,
  hasAccess,
  replaceKeysInJSON,
  getTypeOrSubType,
  isCustomAttribute,
} from '../library/library';
import {
  filtersKeyNamesReplacements,
  FIELD_TYPES_AS_SINGLE_VALUE,
  FIELD_TYPES_YES_NO_BOOLEAN,
  FIELD_TYPES_AS_OPTIONS_OBJECT,
} from '../library/config';
import { getDefaultSavedSearches } from '../components/EditForm/EditForm.library';
import { mapAPISavedSearch } from './transforming';
import { QUERY_KEY_SAVEDSEARCH } from './constants';

/* eslint complexity: [2, 20] */
export const formikPermissionsToAPIPermissions = (
  permissions: PermissionType[]
) =>
  permissions.map((permission, sort_order) => ({
    group_id: permission.groupID,
    role_id: permission.roleID,
    permission:
      permission.writePermission === true ? ['read', 'write'] : ['read'],
    sort_order,
  }));

/* eslint complexity: [2, 20] */
const formikValuesToAPIPayload = ({
  values,
  kind,
  identifier,
  mode,
  authorizations,
}: {
  values: EditFormStateType;
  kind: KindType;
  identifier?: IdentifierType | null;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
}) => {
  const processFilters = (filters: FilterType[]) => {
    return filters.map(filter => {
      let baseFilterValue = cloneWithout(filter, 'uuid');
      const compareType = getTypeOrSubType(filter);
      const customAttribute = isCustomAttribute(filter);

      if (kind === 'custom_object' && customAttribute) {
        return baseFilterValue;
      } else if (
        customAttribute &&
        FIELD_TYPES_AS_OPTIONS_OBJECT.includes(compareType)
      ) {
        baseFilterValue.values.values = baseFilterValue.values.values.map(
          (value: any) => ({ label: value, value, type: compareType })
        );
      } else if (FIELD_TYPES_AS_SINGLE_VALUE.includes(compareType)) {
        if (customAttribute) {
          baseFilterValue.values.values = asArray(
            baseFilterValue.values.values
          );
        } else {
          baseFilterValue.values = asArray(baseFilterValue.values);
        }
      } else if (FIELD_TYPES_YES_NO_BOOLEAN.includes(compareType)) {
        baseFilterValue.values = Boolean(baseFilterValue.values === 'yes');
      } else if (customAttribute && compareType === 'relationship') {
        const relationType = filter.values.additionalData?.type;
        if (relationType === 'subject') {
          const thisValue = baseFilterValue.values.values;
          baseFilterValue.values.values = thisValue.map(
            ({ label, value, type }: ValueType<string>) => ({
              label,
              value,
              type,
            })
          );
        }
      } else if (customAttribute && compareType === 'file') {
        const { label, magicString, type } = baseFilterValue.values;
        baseFilterValue.values = {
          label,
          magicString,
          type,
        };
      }

      return baseFilterValue;
    });
  };

  const uuid = mode === 'edit' ? identifier : v4();
  const {
    name,
    template,
    filters,
    permissions,
    columns,
    sortColumn,
    sortOrder,
  } = values;
  const allFilters = [...(filters?.filters || [])];
  const payload: any = {
    uuid,
    name,
    template,
  };

  if (mode === 'new') payload.kind = kind;

  // Re-assemble all filters
  if (
    kind === 'custom_object' &&
    values.selectedObjectType &&
    isUUID(values?.selectedObjectType?.value)
  ) {
    // @ts-ignore
    allFilters.push({
      type: 'relationship.custom_object_type',
      uuid: v4(),
      values: [
        {
          value: values.selectedObjectType.value as string,
          label: values.selectedObjectType.label as string,
        },
      ],
    });
  }

  if (
    mode === 'new' ||
    (mode === 'edit' && authorizations && hasAccess(authorizations, 'admin'))
  ) {
    payload.permissions = formikPermissionsToAPIPermissions(permissions);
  }

  payload.filters = {
    ...values.filters,
    kind_type: kind,
    filters: processFilters(allFilters),
  };
  payload.columns = columns;

  if (sortColumn && sortOrder) {
    payload.sort_column = sortColumn;
    payload.sort_order = sortOrder;
  }

  return replaceKeysInJSON(payload, filtersKeyNamesReplacements);
};

export const saveSavedSearch = async ({
  payload,
  mode,
}: {
  payload: any;
  mode: ModeType;
}) => {
  const url =
    mode === 'new'
      ? '/api/v2/cm/saved_search/create'
      : '/api/v2/cm/saved_search/update';

  return request<APICaseManagement.CreateSavedSearchResponseBody>('POST', url, {
    ...payload,
  })
    .then(() => payload.uuid)
    .catch((serverError: V2ServerErrorsType) => {
      throw serverError;
    });
};

export const deleteSavedSearch = async (uuid: string) => {
  const url = '/api/v2/cm/saved_search/delete';
  return request<APICaseManagement.DeleteSavedSearchResponseBody>('POST', url, {
    uuid,
  }).catch((serverError: V2ServerErrorsType) => {
    throw serverError;
  });
};

export const getSavedSearch =
  (kind: KindType, t: i18next.TFunction, session: SessionType) =>
  async ({ queryKey }: { queryKey: any }): Promise<EditFormStateType> => {
    const identifier = getFromQueryValues<string>(
      queryKey[1],
      'identifier'
    ) as string;

    if (isUUID(identifier)) {
      const url = buildUrl('/api/v2/cm/saved_search/get', {
        uuid: identifier,
      });

      const results =
        await request<APICaseManagement.GetSavedSearchResponseBody>(
          'GET',
          url
        ).catch((serverError: V2ServerErrorsType) => {
          throw serverError;
        });

      const result = results.data
        ? asArray(results.data).map(mapAPISavedSearch(kind))
        : [];

      return result[0];
    } else {
      const defaults = getDefaultSavedSearches({
        kind,
        t,
        session,
      });

      const found = defaults?.find(
        def => def.uuid === identifier
      ) as EditFormStateType;
      return found || null;
    }
  };

export const useSingleQuery = ({
  identifier,
  kind,
  t,
  session,
}: {
  identifier?: IdentifierType | null;
  kind: any;
  t: any;
  session: any;
}) => {
  return useQuery<SavedSearchType, V2ServerErrorsType>(
    [QUERY_KEY_SAVEDSEARCH, { identifier }],
    getSavedSearch(kind, t, session),
    {
      enabled: Boolean(identifier),
      onError: openServerError,
    }
  );
};

export const useSingleSaveMutation = ({
  kind,
  identifier,
  mode,
  authorizations,
}: {
  kind: KindType;
  identifier?: IdentifierType | null;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
}) => {
  return useMutation<any, V2ServerErrorsType, any>((values: any) => {
    const payload = formikValuesToAPIPayload({
      values,
      kind,
      identifier,
      mode,
      authorizations,
    });
    return saveSavedSearch({
      payload,
      mode,
    });
  });
};

export const useSingleDeleteMutation = () =>
  useMutation<any, V2ServerErrorsType, string>(uuid => deleteSavedSearch(uuid));
