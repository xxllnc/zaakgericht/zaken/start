// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const editFormStyles = ({
  elephant,
  error,
  typography,
  common,
  primary,
}: any) => {
  return {
    // Edit form - wraps around the top, middle, bottom
    editFormWrapper: {
      display: 'flex',
      height: '100%',
      flexDirection: 'column' as 'column',
    },
    editFormTitle: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      ...typography.h3,
    },
    editFormTop: {
      padding: 0,
    },
    editFormMiddle: {
      flex: 1,
      'overflow-y': 'auto',
    },
    editFormBottom: {
      flex: '0 0 auto',
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      flexDirection: 'column' as 'column',
      gap: 14,
    },
    editFormBottomErrors: {
      padding: 10,
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      margin: '0px 14px 0px 14px',
      backgroundColor: error.main,
      color: common.white,
      borderRadius: 8,
      marginLeft: 'auto',
      gap: 8,
    },
    editFormBottomButtons: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
    },
    editFormGeneralFields: {
      display: 'flex',
      flexDirection: 'column',
      gap: 16,
    },
    selfAlignTop: {
      alignSelf: 'flex-start',
    },
    editFormTabsWrapper: {
      margin: 0,
    },
    noFiltersMessage: {
      margin: 0,
    },
    templateHelp: {
      display: 'flex',
      width: 50,
      alignItems: 'center',
      paddingLeft: 14,
    },
    tabs: {
      margin: 0,
      '&&': {
        minHeight: 'auto',
        borderBottom: `2px solid ${elephant.light}`,
      },
    },
    tabsFlexContainer: {
      gap: 6,
    },
    tabsContent: {
      marginTop: 16,
    },
    selectedTab: {
      '&&&': {
        backgroundColor: primary.main,
        color: common.white,
      },
    },
    tab: {
      '&&': {
        backgroundColor: elephant.lightest,
        color: primary.main,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        textTransform: 'none',
        minHeight: 38,
        padding: 0,
      },
    },
    editFormBackBtn: {
      '&&': {
        color: primary.main,
        '&>span': {
          marginRight: 10,
        },
      },
    },
    templateFieldWrapper: {
      display: 'flex',
    },
  };
};

export const editFormSx = () => ({
  lightSelect: {
    backgroundColor: 'white',
    '& .MuiAutocomplete-inputRoot': {
      backgroundColor: 'white',
    },
    '& :hover': {
      backgroundColor: 'white',
    },
  },
});
