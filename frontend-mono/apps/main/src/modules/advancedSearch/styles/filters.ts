// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const filtersStyles = ({
  common,
  coral,
  typography,
  greyscale,
  elephant,
}: any) => {
  return {
    filtersContentWrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 16,
    },
    filtersOperator: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      gap: 10,
      '&>:nth-child(1)': {
        justifyContent: 'flex-end',
      },
      '&>:nth-child(2)': {
        width: 100,
      },
    },
    filtersSelectWrapper: {
      width: '100%',
      margin: 0,
    },
    filtersSelectCaseMethod: {
      display: 'flex',
      flexWrap: 'wrap',
      width: '100%',
      columnGap: 14,
      marginTop: 10,
      marginBottom: 26,
      justifyContent: 'flex-end',
    },
    filtersSelectCaseSelectors: {
      display: 'flex',
      width: '100%',
    },
    noFiltersLabel: {
      padding: '0px 6px 0px 6px',
    },
    filterRowWithLabel: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      marginBottom: 6,
      '&>:nth-child(1)': {
        color: greyscale.offBlack,
        fontSize: 13,
      },
    },
    filterWrapper: {
      display: 'flex',
      backgroundColor: common.white,
      border: `2px solid ${elephant.lightest}`,
      //marginBottom: 16,
      padding: 10,
      borderRadius: 8,
      flexWrap: 'wrap',
      position: 'relative',
      width: '100%',
    },
    filterError: {
      flex: '1 1 100%',
      color: coral.main,
      marginTop: 20,
      ...typography.subtitle2,
    },
    filterHeader: {
      boxShadow: `0 2px 4px -4px ${common.black}`,
      display: 'flex',
      alignItems: 'center',
      paddingBottom: 10,
      margin: '0px 0px 10px 0px',
      width: '100%',
      gap: 6,
      // Type indicator
      '&>:nth-child(1)': {
        color: elephant.dark,
      },
      // Title
      '&>:nth-child(2)': {
        flex: 1,
        ...typography.h7,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      },
      // Operators
      '&>:nth-child(3)': {
        gap: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minWidth: 'fit-content',
      },
      // Help icon
      '&>:nth-child(4)': {},
      // Action buttons
      '&>:nth-child(5)': {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minWidth: 'fit-content',
      },
    },
    filterOperatorStatic: {
      borderRadius: 8,
      border: `2px solid ${greyscale.light}`,
      padding: 10,
    },
    filterOperatorHelp: {
      alignSelf: 'baseline',
      color: elephant.dark,
    },
    filterRowWrapper: {
      display: 'flex',
    },
    listFiltersWrapper: {
      flexDirection: 'column',
      gap: 12,
    },
  };
};
