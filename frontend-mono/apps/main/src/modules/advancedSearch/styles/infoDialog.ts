// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const infoDialogStyles = () => {
  return {
    infoDialogWrapper: {
      width: 500,
      minHeight: 320,
      display: 'flex',
      flexDirection: 'column',
      padding: 10,
    },
    infoDialogRow: {
      display: 'flex',
      marginBottom: 14,
      '&>:nth-child(1)': {
        display: 'flex',
        alignItems: 'top',
        width: 200,
        padding: 4,
        fontWeight: 600,
      },
      '&>:nth-child(2)': {
        flex: 1,
        padding: 4,
      },
    },
    infoDialogLabels: {
      display: 'flex',
      gap: 6,
      flexWrap: 'wrap',
    },
  };
};
