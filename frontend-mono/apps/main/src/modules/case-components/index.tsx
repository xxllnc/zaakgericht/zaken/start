// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { CaseObjType, CaseTypeType } from './../case/Case.types';
import locale from './../case/Case.locale';
import Routes from './Routes';
import {
  buildCaseObj,
  buildCaseType,
  formatRequestor,
  formatSubject,
} from './../case/Case.library';

const CaseComponentsModule: React.FunctionComponent = () => {
  const [caseObj, setCaseObj] = useState<CaseObjType>();
  const [caseType, setCaseType] = useState<CaseTypeType>();

  useEffect(() => {
    window.addEventListener('message', event => {
      if (event.data.type === 'postCaseData') {
        const {
          caseV1,
          caseV2,
          caseTypeV1,
          caseTypeV2,
          caseRoles,
          requestorV2,
        } = event.data.data;

        const relationships = caseV2.data.relationships;
        const assignee = formatSubject(relationships.assignee);
        const coordinator = formatSubject(relationships.coordinator);
        const requestor = formatRequestor(requestorV2);
        const recipient = formatSubject(relationships.recipient);

        const caseObj = buildCaseObj(caseV2, caseV1, caseRoles, {
          assignee,
          coordinator,
          requestor,
          recipient,
        });
        const caseType = buildCaseType(caseTypeV2, caseTypeV1);

        setCaseObj(caseObj);
        setCaseType(caseType);
      }
    });

    window.top?.postMessage({ type: 'fetchCaseData' }, '*');
  }, []);

  if (!caseObj || !caseType) {
    return <Loader />;
  } else {
    return (
      <I18nResourceBundle resource={locale} namespace="case">
        <Routes caseObj={caseObj} caseType={caseType} />
      </I18nResourceBundle>
    );
  }
};

export default CaseComponentsModule;
