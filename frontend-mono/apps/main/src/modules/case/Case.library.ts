// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import fecha from 'fecha';
import { useParams } from 'react-router';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import {
  EmailTemplateType,
  RecipientTypeType,
} from '@zaaksysteem/communication-module/src/types/Context.types';
import { openServerError, openSnackbar } from '@zaaksysteem/common/src/signals';
import { NotificationType } from '../../components/NotificationBar/NotificationBar';
import {
  fetchCase,
  fetchCaseV0,
  fetchCaseV1,
  fetchCaseType,
  fetchCaseTypeV1,
  fetchSubject,
  setPaymentStatus,
  assignToSelf,
  fetchJobs,
  fetchRoles,
} from './Case.requests';
import {
  CaseV2Type,
  CaseV1Type,
  CaseTypeType,
  CaseTypeV2Type,
  CaseTypeV1Type,
  CaseRolesV1Type,
  CaseObjType,
  SystemRolesType,
  SubjectV2Type,
  SubjectType,
  JobTypeV1,
  JobType,
  DialogsType,
  CaseActionDialogType,
  PhaseType,
} from './Case.types';
import { caseActions } from './sideMenu/caseActions/Caseactions.library';
import { GET_CASE_OBJECT, GET_JOBS } from './Case.keys';

export const formatBreadcrumbLabel = (
  t: i18next.TFunction,
  caseObj: CaseObjType,
  caseType: CaseTypeType
): string =>
  `${t('common:entityType.case')} ${caseObj.number}: ${caseType.name}`;

export const formatSubject = (
  subjectV2?: SubjectV2Type
): SubjectType | undefined => {
  if (!subjectV2) return;

  const {
    data: { id, type },
    meta,
  } = subjectV2;

  return { uuid: id, name: meta?.summary, type };
};

export const formatRequestor = (
  subjectV2?: SubjectV2Type
): SubjectType | undefined => {
  if (!subjectV2) return;

  const {
    id,
    meta,
    attributes: {
      has_correspondence_address: hasCorrespondenceAddress,
      is_secret: isSecret,
      is_deceased: isDeceased,
      is_under_investigation: isUnderInvestigation,
      has_valid_address: hasValidAddress,
      coc_number: cocNumber,
      coc_location_number: cocLocationNumber,
    },
    type,
  } = subjectV2.data;

  return {
    type,
    uuid: id,
    name: meta?.summary,
    isSecret,
    hasValidAddress,
    hasCorrespondenceAddress,
    isDeceased,
    isUnderInvestigation,
    cocNumber,
    cocLocationNumber,
  };
};

const getSystemRoles = async (caseV2: CaseV2Type) => {
  const relationships = caseV2.data.relationships;

  const assignee = formatSubject(relationships.assignee);
  const coordinator = formatSubject(relationships.coordinator);
  const recipient = formatSubject(relationships.recipient);

  const { id, type } = relationships.requestor.data;
  const requestorResponse = await fetchSubject(type, id);
  const requestor = formatRequestor(requestorResponse);

  return {
    assignee,
    requestor,
    recipient,
    coordinator,
  };
};

export const formatCaseV1 = (caseV1: CaseV1Type) => ({
  // map fields require the v1 response to be passed into the context (for Wetterskip)
  caseV1,
  // needed to fetch caseTypeV1
  caseTypeUuid: caseV1.casetype.reference,
  caseTypeVersion: caseV1.casetype.instance.version,
});

export const formatCaseV2 = ({
  data: {
    id,
    meta: { authorizations },
    attributes: {
      summary,
      confidentiality,
      contact_channel: contactChannel,
      custom_fields: customFields,
      html_email_template: htmlEmailTemplateName,
      milestone,
      number,
      payment,
      requestor_is_preset_client: hasPresetClient,
      progress_status: progressStatus,
      result,
      stalled_since_date: stalledSinceDate,
      stalled_until_date: stalledUntilDate,
      status,
      registration_date: registrationDate,
      target_completion_date: targetDate,
      completion_date: completionDate,
      destruction_date: destructionDate,
      num_unaccepted_files: numUnacceptedFiles,
      num_unaccepted_updates: numUnacceptedUpdates,
      num_unread_communication: numUnreadCommunication,
    },
    relationships,
  },
}: CaseV2Type) => {
  // most actions will only be available when 1. the case is open and 2. for those with edit/manage rights
  // so we pass canEdit/canManage for convenience, and pass the rest to be used for the exceptions
  // @ts-ignore apidocs
  const caseOpen = status !== 'resolved';
  const hasEditRights = authorizations.includes('write');
  const hasManageRights = authorizations.includes('manage');
  const canEdit = hasEditRights && caseOpen;
  const canManage = hasManageRights && caseOpen;

  const phase = caseOpen ? milestone + 1 : milestone;

  return {
    caseOpen,
    hasEditRights,
    hasManageRights,
    canEdit,
    canManage,

    registrationDate,
    targetDate: targetDate || null,
    completionDate: completionDate || null,
    destructionDate: destructionDate || null,

    uuid: id,
    name: id,
    number,
    status,
    department: {
      uuid: relationships?.department?.data?.id,
      name: relationships?.department?.meta.summary,
    },
    role: {
      uuid: relationships?.role?.data.id,
      name: relationships?.role?.meta.summary,
    },
    milestone,
    phase,
    summary,
    result,
    htmlEmailTemplateName,

    caseTypeVersionUuid: relationships?.case_type_version.data.id,
    confidentiality: confidentiality || 'confidential',
    contactChannel,
    customFields,
    // not supported by v2, or even v1
    location: undefined,
    payment,
    hasPresetClient,
    progressStatus,
    stalledSinceDate,
    stalledUntilDate,

    numUnacceptedFiles,
    numUnacceptedUpdates,
    numUnreadCommunication,
  };
};

const formatCaseRoles = (caseRoles: CaseRolesV1Type) =>
  caseRoles.map(({ instance: { label } }) => ({
    label,
    value: label,
  }));

export const buildCaseObj = (
  caseV2: CaseV2Type,
  caseV1: CaseV1Type,
  caseRoles: CaseRolesV1Type,
  systemRoles?: SystemRolesType
): CaseObjType => ({
  ...formatCaseV1(caseV1),
  ...formatCaseV2(caseV2),
  caseRoles: formatCaseRoles(caseRoles),
  ...systemRoles,
});

const getFields = (caseTypeV1: CaseTypeV1Type) => {
  const phases = caseTypeV1.phases;
  const fields = phases.reduce(
    (acc: any[], phase: any) => [...acc, ...phase.fields],
    []
  );

  return fields;
};

const getObjectFields = (caseTypeV1: CaseTypeV1Type) => {
  const fields = getFields(caseTypeV1);
  const objectFields = fields.filter((field: any) => field.type === 'object');

  return objectFields;
};

export const formatCaseTypeV1 = (caseTypeV1: CaseTypeV1Type) => ({
  // v2 lacks object(v1) attributes in its phase definition (for relations tab)
  objectFields: getObjectFields(caseTypeV1),
  // v2 lacks the 'id' of an attribute in its field definition (for object form)
  fields: getFields(caseTypeV1),
  // v2 doesn't return the results (for caseAction 'resolve prematurely')
  results: caseTypeV1.results,
});

const recipientTypeDict = {
  aanvrager: 'requestor',
  behandelaar: 'colleague',
  coordinator: 'coordinator',
  betrokkene: 'role',
  gemachtigde: 'authorized',
  overig: 'other',
};

const formatEmailValue = (
  emailsAsString: string | undefined
): { value: string; label: string }[] =>
  emailsAsString
    ? emailsAsString.split(/[,;]/).map(email => {
        const value = email.trim();

        return { value, label: value };
      })
    : [];

const formatEmailTemplate = ({
  label,
  data: {
    bibliotheek_notificaties_id,
    bibliotheek_notificaties_uuid,
    rcpt,
    behandelaar,
    betrokkene_role,
    email: to,
    cc,
    bcc,
    subject,
    body,
    case_document_attachments,
    sender,
    sender_address,
  },
}: CaseTypeV2Type['phases'][0]['emails'][0]): EmailTemplateType => ({
  id: bibliotheek_notificaties_id,
  uuid: bibliotheek_notificaties_uuid,
  label,
  senderName: sender,
  senderEmail: sender_address,
  // @ts-ignore
  recipientType: recipientTypeDict[rcpt] as RecipientTypeType,
  behandelaar,
  role: betrokkene_role,
  to: formatEmailValue(to),
  cc: formatEmailValue(cc),
  bcc: formatEmailValue(bcc),
  subject,
  body,
  attachments: [],
  attributeAttachmentIds: (
    case_document_attachments as { case_document_ids: number }[]
  ).map(attr => attr.case_document_ids),
});

const formatPhase = ({
  phase,
  milestone,
  emails,
  custom_fields,
}: CaseTypeV2Type['phases'][0]): PhaseType => ({
  phase,
  milestone,
  emails: emails.map(formatEmailTemplate),
  customFields: custom_fields,
});

export const formatCaseTypeV2 = ({
  data: {
    id,
    meta: { summary },
    attributes: {
      phases,
      settings,
      terms: { lead_time_legal, lead_time_service },
      metadata: {
        legal_basis: legalBasis,
        local_basis: localBasis,
        designation_of_confidentiality: designationOfConfidentiality,
        responsible_relationship: responsibleRelationship,
        process_description: processDescription,
      },
    },
  },
}: CaseTypeV2Type) => ({
  // api doesnt match apidocs in all parts
  phases: phases.map(formatPhase),
  settings,
  name: summary,
  uuid: id,
  metaData: {
    legalBasis,
    localBasis,
    designationOfConfidentiality,
    responsibleRelationship,
    processDescription,
  },
  leadTimeLegal: {
    type: lead_time_legal.type,
    value: lead_time_legal.value,
  },
  leadTimeService: {
    type: lead_time_service.type,
    value: lead_time_service.value,
  },
});

export const buildCaseType = (
  caseTypeV2: CaseTypeV2Type,
  caseTypeV1: CaseTypeV1Type
): CaseTypeType => ({
  ...formatCaseTypeV1(caseTypeV1),
  ...formatCaseTypeV2(caseTypeV2),
});

export const useCaseObjectQuery = () => {
  const { caseUuid } = useParams();

  return useQuery<CaseObjType, V2ServerErrorsType>(
    [GET_CASE_OBJECT],
    async () => {
      const [caseV2, caseV1] = await Promise.all([
        fetchCase(caseUuid || ''),
        fetchCaseV1(caseUuid || ''),
      ]);

      const caseRoles = await fetchRoles();
      const systemRoles = await getSystemRoles(caseV2);

      const caseObj = buildCaseObj(caseV2, caseV1, caseRoles, systemRoles);

      return caseObj;
    },
    { enabled: isNaN(Number(caseUuid)), onError: openServerError }
  );
};

export const useCaseTypeQuery = (
  caseObj: CaseObjType | undefined,
  enabled: boolean
) =>
  useQuery<CaseTypeType, V2ServerErrorsType>(
    ['GET_CASE_TYPE'],
    async () => {
      const caseTypeVersionUuid = caseObj?.caseTypeVersionUuid || '';
      const caseTypeUuid = caseObj?.caseTypeUuid || '';
      const caseTypeVersion = caseObj?.caseTypeVersion || 0;

      const [caseTypeV2, caseTypeV1] = await Promise.all([
        fetchCaseType(caseTypeVersionUuid),
        fetchCaseTypeV1(caseTypeUuid, caseTypeVersion),
      ]);

      const caseType = buildCaseType(caseTypeV2, caseTypeV1);

      return caseType;
    },
    { enabled: Boolean(enabled && caseObj), onError: openServerError }
  );

export const useJobsQuery = (caseUuid: string, enabled: boolean) =>
  useQuery<JobType[], V2ServerErrorsType>(
    [GET_JOBS],
    async () => {
      const response = await fetchJobs(caseUuid);

      const jobs = response.result.instance.rows.map((job: JobTypeV1) => ({
        status: job.instance.status,
      }));

      return jobs;
    },
    { enabled, onError: openServerError }
  );

export const redirectToCaseUuid = async (caseNumber: string) => {
  const caseV0 = await fetchCaseV0(caseNumber);
  const caseUuid = caseV0.id;

  window.location.href = `/main/case/${caseUuid}`;
};

export const formatDate = (date: string | null): string | undefined => {
  if (!date) return;

  return fecha.format(new Date(date), 'DD-MM-YYYY');
};

const finishedStatuses = ['finished', 'failed', 'cancelled'];

export const getUnfinishedJobs = (jobs: JobType[]) =>
  jobs.filter(({ status }) => !finishedStatuses.includes(status));

/* eslint complexity: [2, 10] */
export const getNotifications = (
  t: i18next.TFunction,
  session: SessionType,
  caseObj: CaseObjType,
  refreshCaseObj: () => void,
  jobs: JobType[]
): NotificationType[] => {
  const paymentStatus = caseObj.payment?.status;
  const status = caseObj.status;

  const statusNotifications = [
    {
      when: status === 'stalled',
      message: caseObj.stalledUntilDate
        ? t('notification.status.stalled.definite', {
            date: formatDate(caseObj.stalledUntilDate),
          })
        : t('notification.status.stalled.indefinite'),
    },
    {
      when: status === 'resolved',
      message: t('notification.status.closed'),
    },
  ];

  const assignee = caseObj.assignee;
  const userIsAssignee = session.logged_in_user.uuid === assignee?.uuid;
  const assignCaseToSelf = () =>
    assignToSelf(caseObj.uuid).then(() => refreshCaseObj());
  const assigneeNotifications = [
    {
      // this action is available to those with read rights
      when: !caseObj.assignee && status !== 'resolved',
      message: t('notification.assignment.assignToSelf'),
      button: {
        label: t('notification.actions.assignToSelf'),
        action: assignCaseToSelf,
      },
    },
    {
      // this action is available to those with read rights
      when: status === 'new' && assignee && userIsAssignee,
      message: t('notification.assignment.acceptAssignment'),
      button: {
        label: t('notification.actions.assignToSelf'),
        action: assignCaseToSelf,
      },
    },
    {
      when: status !== 'resolved' && assignee && !userIsAssignee,
      message: t('notification.assignment.userIsNotAssignee'),
    },
  ];

  const requestor = caseObj.requestor;
  const requestorNotifications = [
    {
      when: requestor?.hasCorrespondenceAddress,
      message: t('notification.requestor.correspondenceAddress'),
    },
    {
      when: requestor?.isDeceased,
      message: t('notification.requestor.deceased'),
    },
    {
      when: requestor?.isUnderInvestigation,
      message: t('notification.requestor.investigated'),
    },
    {
      when: requestor?.hasValidAddress,
      message: t('notification.requestor.moved'),
    },
    {
      when: requestor?.isSecret,
      message: t('notification.requestor.secret'),
    },
  ];

  const paymentNotifications = [
    {
      when: paymentStatus === 'failed',
      message: t(`notification.payment.failed`),
    },
    {
      when: paymentStatus === 'offline' && status !== 'resolved',
      message: t(`notification.payment.${paymentStatus}`),
      button: {
        label: t('notification.actions.payment'),
        disabled: !caseObj.canEdit,
        action: () => {
          setPaymentStatus(caseObj.uuid, 'success').then(() =>
            refreshCaseObj()
          );
        },
      },
    },
    {
      when:
        paymentStatus === 'pending' ||
        (paymentStatus === 'offline' && status === 'resolved'),
      message: t(`notification.payment.pending`),
    },
  ];

  const unfinishedJobs = getUnfinishedJobs(jobs);
  const jobNotifications = [
    {
      when: unfinishedJobs.length > 0,
      message: t('notification.jobs.unfinished'),
      button: {
        label: t('notification.actions.seeJobs'),
        action: () => {
          openSnackbar(
            `${t('snack.jobs')}: (${unfinishedJobs.length}/${jobs.length})`
          );
          const interval = setInterval(() => {
            refreshCaseObj();

            if (unfinishedJobs.length === 0) {
              clearInterval(interval);
            }
          }, 5000);
        },
      },
    },
  ];

  return [
    ...statusNotifications,
    ...assigneeNotifications,
    ...requestorNotifications,
    ...paymentNotifications,
    ...jobNotifications,
  ]
    .filter(notification => notification.when)
    .map(({ message, button }: any) => ({
      message,
      button,
    }));
};

export const dialogIsCaseAction = (
  dialog: DialogsType
): dialog is CaseActionDialogType =>
  caseActions.some(action => action === dialog);
