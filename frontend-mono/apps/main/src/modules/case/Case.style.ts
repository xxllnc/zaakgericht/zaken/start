// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCaseStyles = makeStyles(() => ({
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column',
  },
  content: {
    flexGrow: 1,
    height: 1,
    display: 'flex',
  },
  view: {
    flexGrow: 1,
  },
}));
