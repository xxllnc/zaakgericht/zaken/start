// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import Relations from './views/relations';
import Timeline from './views/timeline';
import Communication from './views/communication';
import Phases from './views/phases';
import SideMenu from './sideMenu/SideMenu';
import { CaseObjType, CaseTypeType, DialogsType, JobType } from './Case.types';
import { useCaseStyles } from './Case.style';
import { dialogIsCaseAction, getNotifications } from './Case.library';
import AboutDialog from './sideMenu/info/AboutDialog';
import Documents from './views/documents/Documents';
import ConfidentialityDialog from './sideMenu/info/ConfidentialityDialog';
import CaseActionsDialog from './sideMenu/caseActions/CaseActionsDialog';
import CaseActionDialog from './sideMenu/caseActions/CaseActionDialog';
import { invalidateCaseJobsAndCaseObj } from './Case.keys';

export interface CasePropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  jobs: JobType[];
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  caseObj,
  caseType,
  jobs,
}) => {
  const classes = useCaseStyles();
  const [t] = useTranslation('case');
  const [dialog, setDialog] = useState<DialogsType>();
  const session = useSession();

  const closeDialog = () => setDialog(undefined);
  const refreshCaseObj = invalidateCaseJobsAndCaseObj;

  const notifications = getNotifications(
    t as any,
    session,
    caseObj,
    refreshCaseObj,
    jobs
  );

  return (
    <div className={classes.wrapper}>
      <NotificationBar notifications={notifications} />
      <div className={classes.content}>
        <SideMenu
          caseObj={caseObj}
          setDialog={setDialog}
          refreshCaseObj={refreshCaseObj}
        />
        <div className={classes.view}>
          <Routes>
            <Route
              path=""
              element={<Navigate to={'phases'} replace={true} />}
            />
            <Route
              path={'phases/*'}
              element={<Phases caseObj={caseObj} caseType={caseType} />}
            />
            <Route path="documents" element={<Documents />} />
            <Route
              path={`communication/*`}
              element={<Communication caseObj={caseObj} caseType={caseType} />}
            />
            <Route path={`timeline`} element={<Timeline caseObj={caseObj} />} />
            <Route
              path={`relations`}
              element={<Relations caseObj={caseObj} caseType={caseType} />}
            />
          </Routes>
        </div>
      </div>
      <AboutDialog
        caseObj={caseObj}
        caseType={caseType}
        onClose={closeDialog}
        open={dialog === 'about'}
      />
      <ConfidentialityDialog
        caseObj={caseObj}
        onClose={closeDialog}
        open={dialog === 'confidentiality'}
        refreshCaseObj={refreshCaseObj}
      />
      <CaseActionsDialog
        caseObj={caseObj}
        onClose={closeDialog}
        open={dialog === 'caseActions'}
        setDialog={setDialog}
      />
      {dialog && dialogIsCaseAction(dialog) && (
        <CaseActionDialog
          caseObj={caseObj}
          caseType={caseType}
          onClose={closeDialog}
          caseAction={dialog}
          refreshCaseObj={refreshCaseObj}
        />
      )}
    </div>
  );
};

export default Case;
