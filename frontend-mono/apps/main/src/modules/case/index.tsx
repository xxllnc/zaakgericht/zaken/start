// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import locale from './Case.locale';
import Case from './Case';
import {
  redirectToCaseUuid,
  formatBreadcrumbLabel,
  useJobsQuery,
  useCaseObjectQuery,
  useCaseTypeQuery,
} from './Case.library';

type CaseParamsType = {
  caseUuid: string;
};

/* eslint complexity: [2, 8] */
const CaseModule: React.FunctionComponent = () => {
  const { caseUuid } = useParams<keyof CaseParamsType>() as CaseParamsType;
  const [t] = useTranslation('case');

  const enabled = isNaN(Number(caseUuid));
  // allow users to easily navigate to /main/case/123 while the new case view is being developed

  const { data: caseObj } = useCaseObjectQuery();
  const { data: jobs } = useJobsQuery(caseUuid, enabled);
  const { data: caseType } = useCaseTypeQuery(caseObj, enabled);

  useEffect(() => {
    !enabled && redirectToCaseUuid(caseUuid);
  }, [caseObj]);

  if (!caseObj || !caseType || !jobs) {
    return <Loader />;
  }

  return (
    <>
      <Case caseObj={caseObj} caseType={caseType} jobs={jobs} />
      <TopbarTitle
        title={formatBreadcrumbLabel(t as any, caseObj, caseType)}
        description={caseObj.summary}
      />
    </>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="case">
    <CaseModule />
  </I18nResourceBundle>
);
