// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAuthorizationFieldStyle = makeStyles(
  ({ typography, mintlab }: Theme) => ({
    addButtonWrapper: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 12,
      background: 'none',
    },
    addButton: {
      ...typography.body2,
      alignItems: 'center',
      justifyContent: 'center',
      background: 'transparent',
      border: 'none',
      display: 'flex',
      '& > span': {
        marginLeft: 10,
      },
      cursor: 'pointer',
    },
    displayWrapper: {
      display: 'flex',
      flexDirection: 'column',
      rowGap: '10px',
    },
    displayItem: {
      display: 'flex',
      flexDirection: 'column',
      rowGap: '10px',
      backgroundColor: mintlab.greyscale.dark,
      padding: '10px',
      borderRadius: '4px',
    },
    displayItemCapability: {
      color: mintlab.greyscale.main,
    },
  })
);

export const useAuthorizationFormStyle = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    alignItems: 'start',
  },
  fields: { flex: 1, padding: 10 },
}));
