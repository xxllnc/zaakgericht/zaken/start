// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';

export type AuthorizationFormPropsType = {
  t: i18next.TFunction;
};

export type GetFormDefinitionType = {
  t: i18next.TFunction;
};

export type AuthorizationType = {
  department: null | string | ValueType<string>;
  role: string;
  capabilities: 'search' | 'read' | 'readwrite' | 'admin';
};
