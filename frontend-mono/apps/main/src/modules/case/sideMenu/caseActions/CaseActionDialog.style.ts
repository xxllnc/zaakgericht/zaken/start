// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCaseActionDialogStyles = makeStyles(
  ({ typography }: Theme) => ({
    wrapper: {
      width: '500px',
      fontFamily: typography.fontFamily,
      display: 'flex',
      flexDirection: 'column',
    },
  })
);
