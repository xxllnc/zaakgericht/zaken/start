// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { capitalize } from '@mintlab/kitchen-sink/source';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseObjType, CaseTypeType } from '../../Case.types';

export const caseActionIcons = {
  assign: iconNames.telegram,
  stall: iconNames.pause,
  resume: iconNames.play_arrow,
  resolvePrematurely: iconNames.work_off,
  prolong: iconNames.event,
  copy: iconNames.create_new_folder,
  setRequestor: iconNames.account_circle,
  setAssignee: iconNames.account_circle,
  setCoordinator: iconNames.account_circle,
  setDepartmentAndRole: iconNames.share,
  setRegistrationDate: iconNames.today,
  setTargetCompletionDate: iconNames.event,
  setCompletionDate: iconNames.event,
  setDestructionDate: iconNames.event,
  setPhase: iconNames.skip_previous,
  setStatus: iconNames.folder_special,
  setPaymentStatus: iconNames.euro,
  customiseAuthorizations: iconNames.account_tree,
  setResult: iconNames.ballot,
  updateFieldValue: iconNames.text_rotation_none,
  changeCaseType: iconNames.flip_camera_ios,
} as const;

const getKeys = Object.keys as <T extends object>(obj: T) => Array<keyof T>;

export const caseActions = getKeys(caseActionIcons);

export const maxDestructionTerm = 99999;

export const getDestructionTermOptions = (t: i18next.TFunction) => {
  const weeks = t('common:terms.weeks').toLowerCase();
  const months = t('common:terms.months').toLowerCase();
  const year = t('common:terms.year').toLowerCase();

  return [
    { value: 28, label: `4 ${weeks}` },
    { value: 42, label: `6 ${weeks}` },
    { value: 93, label: `3 ${months}` },
    { value: 186, label: `6 ${months}` },
    { value: 279, label: `9 ${months}` },
    { value: 365, label: `1 ${year}` },
    { value: 548, label: `1,5 ${year}` },
    { value: 730, label: `2 ${year}` },
    { value: 1095, label: `3 ${year}` },
    { value: 1460, label: `4 ${year}` },
    { value: 1825, label: `5 ${year}` },
    { value: 2190, label: `6 ${year}` },
    { value: 2555, label: `7 ${year}` },
    { value: 2920, label: `8 ${year}` },
    { value: 3285, label: `9 ${year}` },
    { value: 3650, label: `10 ${year}` },
    { value: 4015, label: `11 ${year}` },
    { value: 4380, label: `12 ${year}` },
    { value: 4745, label: `13 ${year}` },
    { value: 5110, label: `14 ${year}` },
    { value: 5475, label: `15 ${year}` },
    { value: 5840, label: `16 ${year}` },
    { value: 6935, label: `19 ${year}` },
    { value: 7300, label: `20 ${year}` },
    { value: 7665, label: `21 ${year}` },
    { value: 9125, label: `25 ${year}` },
    { value: 10950, label: `30 ${year}` },
    { value: 14600, label: `40 ${year}` },
    { value: 18250, label: `50 ${year}` },
    { value: 40150, label: `110 ${year}` },
    { value: maxDestructionTerm, label: t('save') },
  ];
};

export const getResults = (caseType: CaseTypeType) =>
  caseType.results.map((result: any) => ({
    label: result.label || capitalize(result.type),
    value: result.resultaat_id.toString(),
  }));

export const getCurrentResult = (
  caseObj: CaseObjType,
  caseType: CaseTypeType
) => caseType.results.find((result: any) => result.type === caseObj.result);

export const reverseDate = (date: string) =>
  date.split('-').reverse().join('-');

const capabilities = ['search', 'read', 'readwrite', 'admin'];
export const getCapabilities = (
  level: 'search' | 'read' | 'readwrite' | 'admin'
) => {
  const authLevel = capabilities.indexOf(level);
  const auths = capabilities.slice(0, authLevel + 1);

  return auths;
};
