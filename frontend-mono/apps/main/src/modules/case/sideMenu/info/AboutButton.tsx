// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { DialogsType } from '../../Case.types';
import { useInfoStyles } from './Info.styles';

type CaseActionsButtonPropsType = {
  setDialog: (type: DialogsType) => void;
  folded: boolean;
};

const AboutButton: React.ComponentType<CaseActionsButtonPropsType> = ({
  setDialog,
  folded,
}) => {
  const [t] = useTranslation('case');
  const classes = useInfoStyles();

  const label = t('info.about');
  const action = () => setDialog('about');

  return (
    <Tooltip className={classes.tooltip} title={label} placement="right">
      {folded ? (
        <IconButton onClick={action}>
          <Icon size="small">{iconNames.info}</Icon>
        </IconButton>
      ) : (
        <Button
          name="caseSideMenuInfo"
          sx={{ width: '85%', margin: 1 }}
          action={action}
          variant="outlined"
        >
          {label}
        </Button>
      )}
    </Tooltip>
  );
};

export default AboutButton;
