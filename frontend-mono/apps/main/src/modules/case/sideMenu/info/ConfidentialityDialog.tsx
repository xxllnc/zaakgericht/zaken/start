// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { CaseObjType } from '../../Case.types';
import { changeConfidentiality } from '../../Case.requests';
import { useConfidentialityDialogStyles } from './ConfidentialityDialog.styles';

type ConfidentialityDialogPropsType = {
  caseObj: CaseObjType;
  open: boolean;
  onClose: () => void;
  refreshCaseObj: () => void;
};

const ConfidentialityDialog: React.ComponentType<
  ConfidentialityDialogPropsType
> = ({ caseObj, open, onClose, refreshCaseObj }) => {
  const [t] = useTranslation('case');
  const classes = useConfidentialityDialogStyles();
  const dialogEl = useRef(null);

  let {
    fields,
    formik: { values },
  } = useForm({
    formDefinition: [
      {
        name: 'confidentiality',
        type: fieldTypes.RADIO_GROUP,
        value: caseObj.confidentiality,
        choices: ['public', 'internal', 'confidential'].map(conf => ({
          label: t(`confidentiality.${conf}`),
          value: conf,
        })),
      },
    ],
  });

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope="case-confidentiality-dialog"
    >
      <DialogTitle
        elevated={true}
        icon="info"
        title={t('dialogs.confidentiality.title')}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div className={classes.wrapper}>
          <span className={classes.warning}>
            {t('dialogs.confidentiality.warning')}
          </span>
          <div>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'definition', 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    compact={true}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </div>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: t('common:dialog.save'),
                onClick: () => {
                  changeConfidentiality(
                    caseObj.number,
                    //@ts-ignore
                    values.confidentiality
                  ).then(() => {
                    refreshCaseObj();
                    onClose();
                  });
                },
              },
              {
                text: t('common:dialog.cancel'),
                onClick: onClose,
              },
            ],
            'case-confidentiality-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default ConfidentialityDialog;
