// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { CaseObjType, CaseTypeType } from '../../Case.types';

export interface InfoPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
}

export const Result = () => <Icon size="small">{iconNames.check_circle}</Icon>;

export const Status = ({ status }: { status: string }) => (
  <ZsIcon size="small">{`caseStatus.${status}`}</ZsIcon>
);

export const Requestor = ({
  type,
  hasPresetClient,
}: {
  type?: SubjectTypeType;
  hasPresetClient: boolean;
}) =>
  hasPresetClient ? (
    <Icon size="small">{iconNames.account_circle}</Icon>
  ) : (
    <ZsIcon size="small">{`entityType.${type}`}</ZsIcon>
  );

export const Recipient = ({ type }: { type?: SubjectTypeType }) => (
  <ZsIcon size="small">{`entityType.${type}`}</ZsIcon>
);

export const Assignee = () => (
  <Icon size="small" color="sahara">
    {iconNames.assignement_ind}
  </Icon>
);

export const Department = () => <Icon size="small">{iconNames.share}</Icon>;

export const RegistrationDate = () => (
  <Icon size="small">{iconNames.today}</Icon>
);

export const TargetDate = () => (
  <Icon size="small">{iconNames.insert_invitation}</Icon>
);

export const CompletionDate = () => (
  <Icon size="small">{iconNames.event_available}</Icon>
);

export const Location = () => <Icon size="small">{iconNames.place}</Icon>;

export const Confidentiality = ({
  confidentiality,
}: {
  confidentiality?: string;
}) => (
  <Icon size="small">
    {confidentiality === 'confidential' ? iconNames.eye_off : iconNames.eye}
  </Icon>
);

export const PaymentStatus = ({ t }: { t: i18next.TFunction }) => {
  const currency = t('common:currency');

  //@ts-ignore
  return <Icon size="small">{iconNames[currency]}</Icon>;
};
