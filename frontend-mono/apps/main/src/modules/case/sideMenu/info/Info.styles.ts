// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useInfoStyles = makeStyles(
  ({ palette: { error, thundercloud } }: Theme) => ({
    wrapper: {
      padding: '8px 0',
    },
    tooltip: {
      display: 'flex',
      height: 48,
      justifyContent: 'center',
    },
    item: {
      width: '100%',
      padding: '8px 16px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
    label: {
      flexGrow: 1,
    },
    warning: {
      color: error.dark,
      fontWeight: 'bold',
    },
    openInNewIcon: {
      textDecoration: 'none',
      color: thundercloud.main,
      padding: 8,
    },
    iconButton: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 8,
      width: 48,
    },
  })
);
