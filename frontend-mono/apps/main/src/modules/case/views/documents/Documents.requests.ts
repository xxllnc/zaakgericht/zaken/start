// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { v4 } from 'uuid';
import { useQuery, useMutation } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError, openSnackbar } from '@zaaksysteem/common/src/signals';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import {
  GET_CASE_DOCUMENTS,
  GET_DOCUMENT_VERSIONS,
} from '../../../../sharedQueryKeys';
import { queryClient } from '../../../../queryClient';
import { useCaseObjectQuery } from '../../Case.library';
import {
  DocumentLabelType,
  DocumentsNodeType,
  DocumentsTreeType,
  DocumentUploadOptions,
} from './Documents.types';
import { selectedPendingRowIds } from './Documents.signals';

const docStatus = {
  original: 'Origineel',
  copy: 'Kopie',
  replaced: 'Vervangen',
  converted: 'Geconverteerd',
};

export const useGetVersions = (fileId: number) =>
  useQuery<
    {
      result: {
        name: string;
        file_id: number;
        version: number;
        case_id: number;
        created_by: string;
        creation_reason: string;
        date_created: string;
        date_modified: string;
        extension: string;
        log: {
          created_by: string;
          description: string;
          event_category: string;
          event_type: string;
          id: number;
          renamed_to: string;
          timestamp: string;
        }[];
      }[];
    },
    V2ServerErrorsType
  >(
    [GET_DOCUMENT_VERSIONS, fileId],
    async () => {
      return request('GET', '/file/version_info/file_id/' + fileId);
    },
    { enabled: Boolean(fileId) }
  );

export const useRestoreDocumentVersionMutation = () => {
  return useMutation<{}, V2ServerErrorsType, number>(
    ['RESTORE_VERSION'],
    async file_id => {
      return request('POST', '/file/revert_to', { file_id });
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.versionRestored');

        queryClient.invalidateQueries([GET_DOCUMENT_VERSIONS]);
        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useGetCategories = () =>
  useQuery<
    {
      result: string[];
    },
    V2ServerErrorsType
  >(
    ['GET_CATEGORIES'],
    async () => {
      return request('GET', '/file/document_categories');
    },
    { onError: openServerError }
  );

export const useUpdateDocumentInfoMutation = (
  document_uuid: string,
  onSuccess: () => void
) => {
  const documents = useDocumentsQuery();
  return useMutation<{}, V2ServerErrorsType, number>(
    ['UPDATE_INFO'],
    // eslint-disable-next-line complexity
    async (data: any) => {
      const target = documents.data?.find(doc => doc.uuid === document_uuid);
      if (
        documents.data?.find(
          doc =>
            doc.title === `${data.name}.${target?.extension}` &&
            doc.uuid !== target?.uuid
        )
      ) {
        throw new Error('serverErrors.documents/duplicate_file_name');
      }
      return request('POST', '/api/v2/document/update_document', {
        document_uuid,
        pronom_format: data.pronomFormat || '',
        appearance: data.appearance || '',
        structure: data.structure || '',
        status: data.documentStatus?.value || data.documentStatus || null,
        publish_settings: {
          pip: data.publish.includes('Pip'),
          website: data.publish.includes('Website'),
        },
        basename: data.name,
        description: data.description,
        document_category:
          data.documentCategory?.value || data.documentCategory || null,
        origin: data.origin?.value || data.origin || null,
        origin_date: data.originDate
          ? new Date(data.originDate).toISOString()
          : null,
        confidentiality: data.trustLevel?.value || data.trustLevel || null,
        document_source: data.source,
      });
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
      onError: openServerError,
    }
  );
};

export const useMoveDocumentToIntakeMutation = () => {
  return useMutation<
    {},
    V2ServerErrorsType,
    { file_id: number; rejection_reason: string }
  >(
    ['INTAKE_MOVE'],
    async ({ file_id, rejection_reason }) => {
      return request('POST', '/file/update', {
        rejection_reason: rejection_reason || 'niet opgegeven',
        reject_to_queue: '1',
        accepted: false,
        file_id,
      });
    },
    {
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentMovedToIntake');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
      onError: openServerError,
    }
  );
};

const addNestedChildren = (rows: any) => {
  rows.forEach((row: any) => {
    if (row.children?.length) {
      const level = row.hierarchy.length;

      let ids: any = [];
      let pastSelf = false;
      let finish = false;

      rows.forEach((row2: any) => {
        finish = finish || (pastSelf && row2.hierarchy.length < level + 1);
        pastSelf && !finish && ids.push(row2.id);
        pastSelf = pastSelf || row === row2;
      });

      row.children = ids;
    }
  });
};

const traverseFolder = (
  rows: DocumentsNodeType[],
  folder: any,
  parents: { title: string; id: string }[],
  caseNumber: number
) => {
  folder.children?.length &&
    folder.children.forEach((child: any) => {
      const id = child.id.toString();
      const hierarchy = [...parents, { title: child.name, id }];
      rows.push({
        type: 'folder',
        uuid: child.uuid,
        accepted: true,
        id,
        title: child.name,
        children: child.children
          .map((ch: any) => ch.id.toString())
          .concat(child.files.map((fi: any) => fi.id.toString())),
        hierarchy,
      });
      traverseFolder(rows, child, hierarchy, caseNumber);
    });
  folder.files?.length &&
    // eslint-disable-next-line complexity
    folder.files.forEach((file: any) => {
      const title = (file.name + (file.extension || '')) as string;
      const hierarchy = [...parents, { id: file.id, title }];
      const id = file.id.toString();
      const locked = Boolean(file?.locked_timestamp);

      if (locked) {
        rows.forEach(
          row =>
            hierarchy.find(parent => parent.title === row.title) &&
            (row.hasLockedFiles = true)
        );
      }

      rows.push({
        id,
        title,
        hierarchy,
        locked,
        name: file.name,
        appearance: file?.metadata_id?.appearance ?? '',
        description: file?.metadata_id?.description ?? '',
        structure: file?.metadata_id?.structure ?? '',
        documentCategory: file?.metadata_id?.document_category ?? '',
        documentStatus:
          //@ts-ignore
          docStatus[file?.document_status] ?? file?.document_status ?? '',
        trustLevel: file?.metadata_id?.trust_level ?? '',
        inTrash: Boolean(file.date_deleted),
        signable: file.signable,
        type: 'file',
        uuid: file.uuid,
        version: file.version,
        accepted: Boolean(file.accepted),
        archiveable: Boolean(file.filestore_id?.is_archivable),
        extension:
          file.extension_dotless ||
          (file.filestore_id?.original_name?.split('.') || [])[1],
        origin: file?.metadata_id?.origin ?? '',
        source: file?.metadata_id?.document_source ?? '',
        originDate: file?.metadata_id?.origin_date ?? '',
        _originDate: fecha.parse(
          file?.metadata_id?.origin_date ?? '',
          'YYYY-MM-DD'
        ),
        pronomFormat: file?.metadata_id?.pronom_format ?? '',
        appliedLabelIds: file.case_documents,
        createdAt: file.date_created
          ? fecha.format(new Date(file.date_created), 'DD-MM-YY HH:mm')
          : undefined,
        _createdAt: file.date_created ? new Date(file.date_created) : undefined,
        createdBy: file.created_by_display_name,
        published: {
          pip: Boolean(file.publish_pip),
          website: Boolean(file.publish_website),
        },
        md5: file.filestore_id?.md5,
        size: file.filestore_id?.size,
        mimetype: file.filestore_id?.mimetype,
        downloadUrl: `/zaak/${caseNumber}/document/${id}/download`,
        previewUrl: `/zaak/${caseNumber}/document/${id}/download/pdf?inline=1`,
        locallyEditable: Boolean(file.locally_editable),
      });
    });
};

const fixVersionReplacementProperty = (rows: DocumentsNodeType[]) => {
  const duplicateTitles = new Set(
    rows.map(row => row.title).filter((row, ix, xs) => xs.indexOf(row) !== ix)
  );

  rows.forEach(row => {
    if (row.title && duplicateTitles.has(row.title)) {
      const target = rows.find(
        row2 => row2.title === row.title && row2.accepted
      );
      row.overwrites = target?.uuid;
    }
  });

  return rows;
};

// export const useDocumentsQuery2 = (case_uuid: string) => {
//   return useQuery<DocumentsTreeType, V2ServerErrorsType>(
//     [GET_CASE_DOCUMENTS],
//     async () => {
//       const res =
//         await request<APIDocument.GetDirectoryEntriesForCaseResponseBody>(
//           'GET',
//           buildUrl('/api/v2/document/get_directory_entries_for_case', {
//             case_uuid,
//           })
//         );

//       return res?.data?.map(entity => {
//         return entity.attributes.entry_type === 'directory' ? {
//           id: 0, // to beimplemented
//           uuid: entity.id,
//           title: entity.attributes.name,
//           // fix for multiple nesting levels
//           children: [res.data?.filter(entry2 => entry2.relationships.parent?.data.id === entity.id).map(entry2 => entry2.id)],
//           type: 'folder',

//         } : {
//           id: entity.attributes.document_number,
//           uuid: entity.id,
//           title: entity.attributes.name + entity.attributes.extension,
//           type: 'file',
//           description: entity.attributes.description,
//           accepted: entity.attributes.accepted,
//           extension: entity.attributes.extension,
//           downloadUrl: entity.links.download,
//           previewUrl: entity.links.preview,
//           version?: entity.attributes;
//           // createdAt: entity.relationships.modified_by?.meta.display_name,
//           // createdBy: entity.relationships.modified_by?.meta.display_name,
//           // origin: entity.attributes.;
//           // originDate?: string;
//           // pronomFormat?: string;
//             // trustLevel?: string;
//             // inTrash?: boolean;

//   appliedLabelIds?: number[];
//   published?: { pip: boolean; website: boolean };
// };
//       });
//     }
//   );
// };

export const useDocumentsQuery = () => {
  const { data } = useCaseObjectQuery();

  return useQuery<DocumentsTreeType, V2ServerErrorsType>(
    [GET_CASE_DOCUMENTS],
    async () => {
      const res = await request(
        'GET',
        `/api/case/${data?.number}/directory/tree`
      );

      const rows = [] as DocumentsTreeType;

      traverseFolder(rows, res.result[0], [], data?.number || 0);
      addNestedChildren(rows);
      fixVersionReplacementProperty(rows);

      return rows;
    },
    {
      enabled: Boolean(data),
      onError: openServerError,
      refetchInterval: 10 * 60 * 1000,
      refetchOnWindowFocus: true,
    }
  );
};

export const useCaseDocumentLabelsQuery = () => {
  const { data } = useCaseObjectQuery();

  return useQuery<DocumentLabelType[], V2ServerErrorsType>(
    ['GET_CASE_LABELS'],
    async () => {
      const res = (await request(
        'GET',
        `/zaak/${data?.number}/case_documents`
      )) as {
        result: {
          id: number;
          uuid: string;
          label: string;
          bibliotheek_kenmerken_id: { naam: string };
        }[];
      };

      return res.result
        .map(label => ({
          id: label.id,
          uuid: label.uuid,
          label: label.label || label.bibliotheek_kenmerken_id.naam,
        }))
        .sort((labelA, labelB) => {
          return labelA.label.localeCompare(labelB.label);
        });
    },
    { enabled: Boolean(data), onError: openServerError }
  );
};

export const useCreateFolderMutation = () => {
  const data = useDocumentsQuery();
  return useMutation<
    any,
    V2ServerErrorsType,
    { case_id: number; name: string }
  >(
    ['CREATE_FOLDER'],
    props => {
      if (data.data?.find(obj => obj.title === props.name)) {
        throw new Error('serverErrors.documents/duplicate_folder_name');
      }
      return request('POST', '/directory/create/', props);
    },
    {
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.folderCreated');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
      onError: openServerError,
    }
  );
};

export const useRenameFolderMutation = () => {
  const data = useDocumentsQuery();

  return useMutation<
    any,
    V2ServerErrorsType,
    { directory_id: number; name: string }
  >(
    ['RENAME_FOLDER'],
    props => {
      if (data.data?.find(obj => obj.title === props.name)) {
        throw new Error('serverErrors.documents/duplicate_folder_name');
      }
      return request('POST', '/directory/update/', props);
    },
    {
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.folderRenamed');
        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
      onError: openServerError,
    }
  );
};

export const useAcceptDocumentsMutation = () => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { uuid: string; as_version_of?: string }[]
  >(
    ['ACCEPT_DOCUMENTS'],
    files => {
      return Promise.all(
        files.map(file => {
          return request('POST', '/api/v2/document/accept_document', {
            document_uuid: file.uuid,
            as_version_of: file.as_version_of || null,
          });
        })
      );
    },
    {
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentsAccepted');
        selectedPendingRowIds.value = [];

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
      onError: () => {
        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useApplyLabelsMutation = () => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { document_label_uuids: string[]; document_uuids: string[] }
  >(
    ['APPLY_LABELS'],
    props => {
      return request('POST', '/api/v2/document/apply_labels', props);
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.labelApplied');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useRemoveLabelsMutation = () => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { document_label_uuids: string[]; document_uuids: string[] }
  >(
    ['REMOVE_LABELS'],
    props => {
      return request('POST', '/api/v2/document/remove_labels', props);
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.labelRemoved');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useUploadFilesMutation = () => {
  const { data } = useCaseObjectQuery();
  let completed = 0;
  let failed = 0;

  const doUpload = (data: FormData) =>
    request('POST', '/api/v2/document/create_document', data, {
      type: 'formdata',
      cached: false,
    })
      .then(() => {
        completed++;
      })
      .catch(() => {
        failed++;
      });

  const fileToFormData =
    (replaces: string | undefined, parentUuid?: string) => (file: File) => {
      const fd = new FormData();
      fd.append('document_file', file);
      fd.append('document_uuid', v4());
      fd.append('case_uuid', data?.uuid || '');
      fd.append('case_id', data?.number?.toString() || '');
      replaces && fd.append('replaces', replaces);
      parentUuid && fd.append('directory_uuid', parentUuid);

      return fd;
    };

  return useMutation<any, V2ServerErrorsType, DocumentUploadOptions>(
    ['MOVE_ITEMS'],
    async ({ files, items, replaces, targetUuid }) => {
      if (items) {
        openSnackbar({
          message: 'case:documentTableMessages.uploadStarted',
          autoHideDuration: null,
        });
        const processEntry = async (
          entry: FileSystemEntry | null,
          parentUuid?: string
        ) => {
          if (entry?.isFile) {
            await new Promise(rez =>
              (entry as FileSystemFileEntry).file(async file => {
                if (file.name.startsWith('.')) {
                  rez(undefined);
                } else {
                  await doUpload(fileToFormData('', parentUuid)(file));

                  rez(undefined);
                }
              })
            );
          } else if (entry?.isDirectory) {
            const response = await request(
              'POST',
              '/api/v2/document/create_directory',
              {
                case_uuid: data?.uuid,
                name: entry?.name,
                parent_uuid: parentUuid || '',
              }
            );

            await new Promise(rez =>
              (entry as FileSystemDirectoryEntry)
                .createReader()
                .readEntries(async entries => {
                  for (const entry of entries) {
                    await processEntry(entry, response.data.directory_uuid);
                  }

                  rez(undefined);
                })
            );
          }
        };

        const entries = items.map(item => item.webkitGetAsEntry());

        for (const entry of entries) {
          await processEntry(entry, targetUuid);
        }
      } else if (files) {
        openSnackbar({
          message: 'case:documentTableMessages.uploadStarted',
          autoHideDuration: null,
        });
        const uploads = Array.prototype.slice
          .call(files)
          .map(fileToFormData(replaces));

        for (const upload of uploads) {
          await doUpload(upload);
        }
      }
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('');
        setTimeout(() => {
          if (completed > 1 && failed === 0) {
            openSnackbar({
              message: 'case:documentTableMessages.uploadCompleteEnum',
              interpolations: { completed },
            });
          } else if (completed && failed) {
            openSnackbar({
              message: 'case:documentTableMessages.uploadCompleteWithFails',
              interpolations: {
                completed,
                failed,
              },
            });
          } else if (failed) {
            openSnackbar('case:documentTableMessages.uploadFails');
          } else {
            openSnackbar('case:documentTableMessages.uploadCompleteBasic');
          }
        }, 0);

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useBulkDeleteMutation = () => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { id: string; type: 'folder' | 'file' }[]
  >(
    ['DELETE_ITEMS'],
    async rows => {
      const files = rows
        .filter(row => row.type === 'file')
        .map(row => Number(row.id));
      const folders = rows
        .filter(row => row.type === 'folder')
        .map(row => Number(row.id))
        .reverse();

      if (files.length) {
        await request('POST', '/api/bulk/file/update', {
          files: Object.fromEntries(
            files.map(id => [
              id,
              {
                id,
                action: 'update_properties',
                data: { deleted: true },
              },
            ])
          ),
        });
      }

      for (const directory_id of folders) {
        await request('POST', '/directory/delete/', { directory_id });
      }
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentsDeleted');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useFileRestoreMutation = () => {
  return useMutation<any, V2ServerErrorsType, string[]>(
    ['RESTORE_ITEMS'],
    async ids => {
      await request('POST', '/api/bulk/file/update', {
        files: Object.fromEntries(
          ids.map(id => [
            Number(id),
            {
              id: Number(id),
              action: 'update_properties',
              data: { deleted: false },
            },
          ])
        ),
      });
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentsRestored');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useRelocationMutation = () => {
  return useMutation<
    any,
    V2ServerErrorsType,
    {
      destination: string | null;
      additionalUuid?: string;
      rows: DocumentsNodeType[];
    }
  >(
    ['RELOCATE_ITEMS'],
    async ({ destination, additionalUuid, rows }) => {
      const onlyTopLevelItems = rows.filter(
        row =>
          !rows.find(row2 =>
            row.hierarchy
              .slice(0, row.hierarchy.length - 1)
              .find(parent => parent.title === row2.title)
          )
      );

      return request('POST', '/api/v2/document/move_to_directory', {
        destination_directory_uuid: destination,
        source_directories: onlyTopLevelItems
          .filter(row => row.type === 'folder')
          .map(row => row.uuid),
        source_documents: onlyTopLevelItems
          .filter(row => row.type === 'file')
          .map(row => row.uuid)
          .concat(additionalUuid ? [additionalUuid] : []),
      });
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.itemsRelocated');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useDeletePermanentMutation = () => {
  return useMutation<any, V2ServerErrorsType, { files: string[]; rest: any }>(
    ['DELETE_PERM'],
    async ({ files, rest }) => {
      for (const file of files) {
        await request('POST', '/file/update', {
          file_id: Number(file),
          ...rest,
        });
      }
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentsDeletedPermanently');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useCopyToCaseMutation = (caseId: number) => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { copyTo: { value: number }; rows: DocumentsNodeType[] }
  >(
    ['COPY_TO_CASE'],
    vals => {
      const folderNameToNode = new Map();
      const files = [] as string[];
      const children = [] as any[];

      vals.rows.forEach(row => {
        const keyedNodes = Array.from(folderNameToNode.keys());
        const parentName = row.hierarchy[row.hierarchy.length - 2]?.title;
        const hasParentIncludedInPayload = keyedNodes.includes(parentName);

        if (row.type === 'folder' && hasParentIncludedInPayload) {
          const node = { id: row.id, files: [], children: [] };
          folderNameToNode.set(row.title, node);
          const parentNode = folderNameToNode.get(parentName);
          parentNode.children.push(node);
        } else if (row.type === 'folder') {
          const node = { id: row.id, files: [], children: [] };
          folderNameToNode.set(row.title, node);
          children.push(node);
        } else if (row.type === 'file' && hasParentIncludedInPayload) {
          const parentNode = folderNameToNode.get(parentName);
          parentNode.files.push(row.id);
        } else {
          files.push(row.id);
        }
      });

      return request('POST', `/api/case/${caseId}/file/copy_to_case`, {
        children,
        files,
        case_id: vals.copyTo.value,
      }).then(() => {
        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
        openSnackbar({
          message: 'case:documentTableMessages.documentCopiedToCase',
          link: `/intern/zaak/${vals.copyTo.value}/documenten/v2`,
        });
      });
    },
    { onError: openServerError }
  );
};

export const useCopyToPdfMutation = () => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { caseId: number; fileId: string }
  >(
    ['COPY_TO_PDF'],
    async ({ caseId, fileId }) =>
      request('POST', `/zaak/${caseId}/document/${fileId}/copy`),
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentCopiedToPdf');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
};

export const useMergeDocumentsMutation = (onSuccess: () => void) => {
  return useMutation<
    any,
    V2ServerErrorsType,
    { fileUuids: string[]; caseId: number; caseUuid: string; filename: string }
  >(
    ['DOCUMENTS_MERGE'],
    async ({ fileUuids, caseId, caseUuid, filename }) => {
      return request('POST', `/api/case/${caseId}/file/merge_documents`, {
        case_uuid: caseUuid,
        file_uuids: fileUuids,
        filename,
      });
    },
    {
      onError: openServerError,
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentsWillBeMerged');

        // Merging happens in the background, we dont know when its supposed to finish
        const reset = () => queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
        setTimeout(reset, 5 * 1000);
        setTimeout(reset, 15 * 1000);
        setTimeout(reset, 60 * 1000);
        onSuccess();
      },
    }
  );
};

export const useRelatedCasesQuery = (enabled: boolean, case_uuid: string) =>
  useQuery<{ label: string; value: string }[], V2ServerErrorsType>(
    ['GET_RELATED'],
    async () => {
      const res = await request(
        'GET',
        buildUrl('/api/v2/cm/case/get_case_relations', {
          case_uuid,
          authorization: 'write',
          include: 'this_case,other_case',
          page: 1,
          rows_per_page: 20,
        })
      );

      return res.data.map((relatedCase: any) => {
        const rel = res.included.find(
          (summary: any) =>
            summary.id === relatedCase.relationships.other_case.data.id
        ).attributes;
        const number = rel.number;

        return {
          label: `${number} - ${rel.casetype_title} (${rel.summary})`,
          value: number,
        };
      });
    },
    { enabled, onError: openServerError }
  );

export const useMaskerModuleQuery = (enabled: boolean) =>
  useQuery<{ label: string; endpoint: string }, V2ServerErrorsType>(
    ['GET_MASKER'],
    async () => {
      const res = await request(
        'GET',
        '/api/v1/sysin/interface/get_by_module_name/koppelapp_document_masker?page=1&rows_per_page=20'
      );
      const inf = res.result.instance.rows[0].instance.interface_config;

      return { label: inf.button_label, endpoint: inf.endpoint };
    },
    { enabled, onError: openServerError }
  );

export const useSignerModuleQuery = (enabled: boolean) =>
  useQuery<{ label: string; endpoint: string }, V2ServerErrorsType>(
    ['GET_SIGNER'],
    async () => {
      const res = await request(
        'GET',
        '/api/v1/sysin/interface/get_by_module_name/koppelapp_signer?page=1&rows_per_page=20'
      );
      const inf = res.result.instance.rows[0].instance.interface_config;

      return { label: inf.button_label, endpoint: inf.endpoint };
    },
    { enabled, onError: openServerError }
  );

export const useFileIntegrityQuery = (fileId: string | number) => {
  return useQuery<{ result: number }, V2ServerErrorsType>(
    ['GET_FILE_INTEGRITY', fileId],
    async () => {
      return request('GET', '/file/verify/file_id/' + fileId);
    },
    { enabled: Boolean(fileId), onError: openServerError }
  );
};

export const requestDocumentZip = (
  case_uuid: string,
  directory_uuids: string[],
  document_uuids: string[]
) =>
  fetch('/api/v2/document/request_document_archive', {
    method: 'POST',
    body: JSON.stringify({
      case_uuid,
      zip_all: false,
      directory_uuids,
      document_uuids,
    }),
  })
    .then(res => res.json())
    .then(res => {
      openSnackbar('case:documentTableMessages.zipInProgress');
      // Look up checkForZipExports.ts for continuation of this execution flow
      localStorage.setItem(
        'runningExportId',
        res.data.export_file_uuid || res.data.id
      );
    })
    .catch(openServerError);

export const useSignDocumentMutation = () =>
  useMutation<{}, V2ServerErrorsType, number | string>(
    ['SIGN_DOCUMENT'],
    async id => {
      openSnackbar('case:documentTableMessages.signingSent');

      return request('POST', `/file/${id}/request_signature`);
    },
    {
      onError: () => {
        openServerError('case:documentTableMessages.signingFailed');
      },
      onSuccess: () => {
        openSnackbar('case:documentTableMessages.documentSigning');

        queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      },
    }
  );
