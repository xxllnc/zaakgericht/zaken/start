// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { computed, signal } from '@preact/signals-core';
import { DocumentsNodeType } from './Documents.types';

export const highlighted = signal(false);
export const documentFilter = signal('');
export const currentMode = signal(
  'documents' as 'documents' | 'bin' | 'relocating' | 'pending'
);
export const msEditOverlay = signal(false);
export const documentContext = signal(
  {} as {
    row?: DocumentsNodeType;
    opened?: boolean;
    posX?: number;
    posY?: number;
  }
);
export const rejectFile = signal({} as { title?: string; id?: string });
export const selectedPendingRowIds = signal([] as string[]);
export const selectedAcceptedRowIds = signal([] as string[]);
export const versionsDialog = signal({ opened: false, fileId: 0 });
export const previewDialog = signal({ opened: false } as {
  opened: boolean;
  title?: string;
  contentType?: string;
  downloadUrl?: string;
  url?: string;
});

export const trashBinView = computed(() => currentMode.value === 'bin');
export const relocatingView = computed(
  () => currentMode.value === 'relocating'
);
