// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import useConfirmDialog from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import { ChipButton } from '../ChipButton';
import { useDeletePermanentMutation } from '../Documents.requests';
import { useSelectedAcceptedRows } from '../Documents.library';

export const PermanentDelete = () => {
  const [t] = useTranslation('case');
  const [, , selectedRowIds] = useSelectedAcceptedRows();
  const { mutateAsync: deleteFile } = useDeletePermanentMutation();

  const [confirmDeleteDialog, openConfirmDelete] = useConfirmDialog(
    t('documentTableActions.deletePermTitle'),
    t('documentTableActions.deletePermBody'),
    () =>
      deleteFile({
        files: selectedRowIds.value,
        rest: { destroyed: true },
      })
  );

  return (
    <>
      <ChipButton
        onClick={openConfirmDelete}
        icon="delete"
        title={t('documentTableActions.permDelete')}
      />
      {confirmDeleteDialog}
    </>
  );
};
