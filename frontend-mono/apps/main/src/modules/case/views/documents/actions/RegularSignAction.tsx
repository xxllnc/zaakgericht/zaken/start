// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { ChipButton } from '../ChipButton';
import { useSelectedAcceptedRows } from '../Documents.library';
import { useSignDocumentMutation } from '../Documents.requests';

/* eslint complexity: [2, 11] */
export const RegularSignAction = () => {
  const [t] = useTranslation('case');
  const { mutate: signDocument } = useSignDocumentMutation();
  const [files, selections] = useSelectedAcceptedRows();

  return selections.single && files[0]?.signable && selections.onlyFiles ? (
    <ChipButton
      onClick={() => signDocument(files[0].id)}
      icon="signature"
      title={t('documentTableActions.regularSign')}
    />
  ) : null;
};
