// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import { FolderDialog } from '../subcomponents/FolderDialog';
import { ChipButton } from '../ChipButton';

export const RenameFolder = () => {
  const [t] = useTranslation('case');
  const [dialogOpened, setDialogOpened] = React.useState(false);

  return (
    <>
      <ChipButton
        onClick={() => setDialogOpened(true)}
        icon="edit"
        title={t('documentTableActions.renameFolder')}
      />
      {dialogOpened && (
        <FolderDialog onClose={() => setDialogOpened(false)} edit={true} />
      )}
    </>
  );
};
