// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import useConfirmDialog from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import { ChipButton } from '../ChipButton';
import { relocatingView } from '../Documents.signals';
import { useBulkDeleteMutation } from '../Documents.requests';
import { useSelectedAcceptedRows } from '../Documents.library';

export const SoftDeleteItems = () => {
  const [t] = useTranslation('case');
  const [selectedRows, selections, selectedRowIds] = useSelectedAcceptedRows();
  const { mutateAsync: deleteRows } = useBulkDeleteMutation();
  const executeDelete = () =>
    deleteRows(selectedRows).then(() => {
      selectedRowIds.value = [];
    });
  const [confirmDeleteDialog, openConfirmDelete] = useConfirmDialog(
    t('documentTableActions.deleteSoftTitle'),
    t('documentTableActions.deleteSoftBody'),
    executeDelete
  );

  return (
    <>
      <ChipButton
        icon="delete"
        title={t('documentTableActions.delete')}
        enabled={
          selections.exist &&
          !relocatingView.value &&
          !selectedRows.some(row => row.hasLockedFiles)
        }
        onClick={
          selections.hasNonEmptyFolders ? openConfirmDelete : executeDelete
        }
      />
      {confirmDeleteDialog}
    </>
  );
};
