// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { DocumentsNodeType } from '../Documents.types';

export const editFormDefinition = (
  file: DocumentsNodeType,
  documentCategories: string[],
  readonly?: boolean
) =>
  [
    {
      format: 'text',
      value: file.name,
      name: 'name',
      label: 'documentTabDialogs.editInfo.name',
      type: fieldTypes.TEXT,
      required: true,
      readOnly: Boolean(readonly),
    },
    {
      format: 'text',
      value: file.description,
      name: 'description',
      label: 'documentTabDialogs.editInfo.description',
      type: fieldTypes.TEXT,
      readOnly: Boolean(readonly),
    },
    {
      format: 'text',
      value: file.documentCategory,
      name: 'documentCategory',
      label: 'documentTabDialogs.editInfo.documentCategory',
      type: fieldTypes.SELECT,
      readOnly: Boolean(readonly),
      choices: documentCategories?.map(value => ({
        label: value,
        value,
      })),
    },
    {
      format: 'text',
      value: file.documentStatus,
      name: 'documentStatus',
      label: 'documentTabDialogs.editInfo.documentStatus',
      isClearable: false,
      readOnly: Boolean(readonly),
      type: fieldTypes.SELECT,
      choices: [
        {
          label: 'documentTabDialogs.editInfo.documentStatusOriginal',
          value: 'Origineel',
        },
        {
          label: 'documentTabDialogs.editInfo.documentStatusCopy',
          value: 'Kopie',
        },
        {
          label: 'documentTabDialogs.editInfo.documentStatusReplaced',
          value: 'Vervangen',
        },
        {
          label: 'documentTabDialogs.editInfo.documentStatusConverted',
          value: 'Geconverteerd',
        },
      ],
    },
    {
      format: 'text',
      value: file.trustLevel,
      name: 'trustLevel',
      label: 'documentTabDialogs.editInfo.trustLevel',
      type: fieldTypes.SELECT,
      isClearable: false,
      readOnly: Boolean(readonly),
      choices: [
        'Openbaar',
        'Beperkt openbaar',
        'Intern',
        'Zaakvertrouwelijk',
        'Vertrouwelijk',
        'Confidentieel',
        'Geheim',
        'Zeer geheim',
      ].map(ch => ({ label: ch, value: ch })),
    },
    {
      format: 'text',
      value: file.origin,
      name: 'origin',
      label: 'documentTabDialogs.editInfo.origin',
      type: fieldTypes.SELECT,
      clearable: true,
      readOnly: Boolean(readonly),
      choices: ['Inkomend', 'Uitgaand', 'Intern'].map(ch => ({
        label: ch,
        value: ch,
      })),
    },
    {
      format: 'text',
      value: file.source,
      name: 'source',
      label: 'documentTabDialogs.editInfo.source',
      readOnly: Boolean(readonly),
      type: fieldTypes.TEXT,
    },
    {
      format: 'text',
      value: file.originDate,
      name: 'originDate',
      label: 'documentTabDialogs.editInfo.originDate',
      type: fieldTypes.DATEPICKER,
      readOnly: Boolean(readonly),
    },
    {
      format: 'text',
      value: file.pronomFormat,
      name: 'pronomFormat',
      label: 'documentTabDialogs.editInfo.pronomFormat',
      type: fieldTypes.TEXT,
      readOnly: Boolean(readonly),
      placeholder: 'documentTabDialogs.editInfo.pronomFormatPlaceholder',
    },
    {
      format: 'text',
      value: file.appearance,
      name: 'appearance',
      label: 'documentTabDialogs.editInfo.appearance',
      type: fieldTypes.TEXT,
      readOnly: Boolean(readonly),
    },
    {
      format: 'text',
      value: file.structure,
      name: 'structure',
      label: 'documentTabDialogs.editInfo.structure',
      type: fieldTypes.TEXT,
      readOnly: Boolean(readonly),
    },
    {
      format: 'text',
      value: [
        file.published?.pip && 'Pip',
        file.published?.website && 'Website',
      ],
      name: 'publish',
      label: 'documentTabDialogs.editInfo.publish',
      type: fieldTypes.CHECKBOX_GROUP,
      readOnly: Boolean(readonly),
      choices: ['Pip', 'Website'].map(value => ({
        label: `documentTabDialogs.editInfo.publish${value}`,
        value,
      })),
    },
  ] as FormDefinition;
