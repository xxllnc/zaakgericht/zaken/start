// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Article from '@mui/icons-material/Article';
import Image from '@mui/icons-material/Image';
import File from '@mui/icons-material/InsertDriveFile';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { buildUrl } from '@mintlab/kitchen-sink/source';
/* eslint-disable complexity */

export const DocumentFileIcon = ({
  uuid,
  extension,
}: {
  uuid: string;
  extension: string;
}) => {
  const [t] = useTranslation('case');
  const [cancelled, setCancelled] = React.useState(false);
  const [fetched, setFetched] = React.useState(false);
  const [image, setImage] = React.useState({ status: 0, url: '' });

  const triggerThumbnailFetch = () => {
    setCancelled(false);
    !fetched &&
      !image.status &&
      setTimeout(() => {
        if (!cancelled) {
          fetch(
            buildUrl('/api/v2/document/thumbnail_document', {
              id: uuid,
              max_height: '395',
              max_width: '279',
            })
          )
            .then(async res => {
              const blob = await res.blob();
              return [res.status, blob] as const;
            })
            .then(([status, blob]) => {
              setImage({ status, url: URL.createObjectURL(blob) });
            });
        }
      }, 300);
    setFetched(true);
  };

  return (
    <>
      <Tooltip
        PopperProps={image.status ? {} : { sx: { display: 'none' } }}
        sx={{ display: 'flex' }}
        title={
          image.status === 404 ? (
            t('documentTableHeaders.noPreview')
          ) : (
            <img
              style={{ maxHeight: '395px', maxWidth: '279px' }}
              src={image.url}
              alt="preview"
            />
          )
        }
        onMouseEnter={() => triggerThumbnailFetch()}
        onMouseLeave={() => setCancelled(true)}
      >
        {['png', 'svg', 'jpg', 'jpeg', 'gif'].includes(extension) ? (
          <Image sx={{ color: '#c22824' }} />
        ) : ['ods', 'odf', 'odt', 'rtf', 'txt'].includes(extension) ? (
          <Article sx={{ color: '#5c5cff' }} />
        ) : ['docx', 'doc'].includes(extension) ? (
          <div style={{ width: '22px', marginLeft: '-10px', minWidth: '22px' }}>
            <div className="ms-icon ms-word-icon" />
          </div>
        ) : ['pptx'].includes(extension) ? (
          <div style={{ width: '22px', marginLeft: '-10px', minWidth: '22px' }}>
            <div className="ms-icon ms-powerpoint-icon" />
          </div>
        ) : ['xlsx', 'xls'].includes(extension) ? (
          <div style={{ width: '22px', marginLeft: '-10px', minWidth: '22px' }}>
            <div className="ms-icon ms-excel-icon" />
          </div>
        ) : ['pdf'].includes(extension) ? (
          <div style={{ width: '22px', paddingLeft: '3px', minWidth: '22px' }}>
            <div className="pdf-icon" />
          </div>
        ) : (
          <File sx={{ color: 'gray' }} />
        )}
      </Tooltip>
    </>
  );
};
