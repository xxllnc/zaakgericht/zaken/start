// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Box from '@mui/material/Box';
import Accordion from '@mui/material/Accordion/Accordion';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { AccordionDetails, Avatar, Card } from '@mui/material';
import AccordionSummary from '@mui/material/AccordionSummary/AccordionSummary';
import { useCaseObjectQuery } from '../../../Case.library';
import { TooltipButton } from '../TooltipButton';
import { versionsDialog } from '../Documents.signals';
import {
  useGetVersions,
  useRestoreDocumentVersionMutation,
} from '../Documents.requests';
import { useDocumentVersionsStyle } from './useDocumentVersionsStyle';

/* eslint complexity: [2, 7] */
export const DocumentVersionsDialog = (props: { canRestore: boolean }) => {
  const [t] = useTranslation('case');
  const { data, isLoading } = useGetVersions(versionsDialog.value.fileId);
  const { mutateAsync: restoreVersion } = useRestoreDocumentVersionMutation();
  const style = useDocumentVersionsStyle();
  const caseObject = useCaseObjectQuery();
  const onClose = () => (versionsDialog.value = { opened: false, fileId: 0 });

  return (
    <Dialog
      disableBackdropClick={true}
      open={versionsDialog.value.opened}
      onClose={onClose}
      scope="catalog-case-type-versions-dialog"
    >
      <DialogTitle
        elevated={true}
        icon="history"
        id={v4()}
        title={t('documentTabDialogs.versions.title')}
        onCloseClick={onClose}
      />
      <Box
        sx={{
          height: '600px',
          width: '500px',
          overflow: 'auto',
          overflowX: 'hidden',
          padding: '0',
        }}
      >
        {isLoading ? (
          <Box sx={{ paddingTop: '20px' }}>
            <Loader />
          </Box>
        ) : (
          data?.result
            ?.sort((verA, verB) => (verA.version < verB.version ? 1 : -1))
            .map((ver, ix) => {
              return (
                <Accordion
                  slots={{ heading: Box }}
                  key={ver.date_created}
                  sx={style.panel}
                >
                  <AccordionSummary
                    expandIcon={<Icon>{iconNames.expand_more}</Icon>}
                  >
                    <Box sx={style.summaryWrapper}>
                      <Tooltip
                        sx={{ width: 'initial' }}
                        title={t('documentTabDialogs.versions.versionTooltip', {
                          createdBy: ver.created_by,
                          createdAt: fecha.format(
                            new Date(ver.date_created || ''),
                            'D MMM YYYY HH:mm'
                          ),
                        })}
                      >
                        <Avatar sx={style.avatar}>{ver.version}</Avatar>
                      </Tooltip>
                      <Box sx={style.versionTitle}>{ver.name}</Box>

                      <TooltipButton
                        sx={{ width: 'auto' }}
                        name="downloadVersion"
                        title={t('documentTabDialogs.versions.downloadVersion')}
                        icon="download"
                        href={`/zaak/${caseObject.data?.number}/document/${ver.file_id}/download?specific_version=1`}
                        onClick={ev => {
                          ev.stopPropagation();
                        }}
                      />
                      {ix !== 0 && props.canRestore ? (
                        <TooltipButton
                          sx={{ width: 'auto' }}
                          name="versionRestore"
                          title={t(
                            'documentTabDialogs.versions.restoreVersion'
                          )}
                          icon="history"
                          onClick={ev => {
                            ev.stopPropagation();
                            restoreVersion(ver.file_id);
                          }}
                        />
                      ) : null}
                    </Box>
                  </AccordionSummary>
                  <AccordionDetails sx={ix === 0 ? style.detailsActive : {}}>
                    {ver.log.map(entry => {
                      return (
                        <Card key={entry.id} sx={style.detailsCard}>
                          <Box sx={style.logEntry}>
                            <Box sx={style.logDescription}>
                              {entry.description}
                            </Box>
                            <Box sx={style.logDate}>
                              {fecha.format(
                                new Date(entry.timestamp),
                                'D MMM YYYY HH:mm:ss'
                              )}
                            </Box>
                          </Box>
                        </Card>
                      );
                    })}
                  </AccordionDetails>
                </Accordion>
              );
            })
        )}
      </Box>
    </Dialog>
  );
};
