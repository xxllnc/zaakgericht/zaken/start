// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';

export const GoToV1Button = () => {
  const [t] = useTranslation('case');

  return (
    <Tooltip title={t('documentTableActions.goToV1')} sx={{ width: 'inherit' }}>
      <Button
        href={window.top?.location.href.split('v2')[0]}
        iconAsText="v1"
        target="_top"
        name="documentsV1"
      />
    </Tooltip>
  );
};
