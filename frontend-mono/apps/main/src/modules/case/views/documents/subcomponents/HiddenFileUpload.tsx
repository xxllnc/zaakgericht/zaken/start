// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useUploadFilesMutation } from '../Documents.requests';

export const hiddenFileUploadId = 'dehfiu';

export const HiddenFileUpload = () => {
  const { mutateAsync: uploadFiles } = useUploadFilesMutation();

  return (
    <input
      id="dehfiu"
      style={{ display: 'none' }}
      multiple={true}
      type="file"
      onChange={() =>
        uploadFiles({
          //@ts-ignore
          files: document.getElementById(hiddenFileUploadId).files,
        }).finally(() => {
          const el = document.querySelector('#' + hiddenFileUploadId);
          //@ts-ignore
          el && (el.value = '');
        })
      }
    />
  );
};
