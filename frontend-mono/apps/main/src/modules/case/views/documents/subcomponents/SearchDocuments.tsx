// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import TextField from '@mintlab/ui/App/Material/TextField';
import {
  documentFilter,
  selectedAcceptedRowIds,
  selectedPendingRowIds,
} from '../Documents.signals';

export const SearchDocuments = () => {
  const [t] = useTranslation('case');

  return (
    <TextField
      sx={{ marginRight: '7px', width: '250px!important' }}
      value={documentFilter}
      variant="generic2"
      placeholder={t('documentTableHeaders.search')}
      onChange={ev => {
        documentFilter.value = ev.target.value;
        selectedAcceptedRowIds.value = [];
        selectedPendingRowIds.value = [];
      }}
      startAdornment={
        <Icon size="small" color="inherit">
          {iconNames.search}
        </Icon>
      }
      closeAction={
        documentFilter.value ? () => (documentFilter.value = '') : undefined
      }
    />
  );
};
