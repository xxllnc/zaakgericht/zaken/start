// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Badge from '@mui/material/Badge';
import { ArrowBack, Delete } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import {
  currentMode,
  selectedAcceptedRowIds,
  trashBinView,
} from '../Documents.signals';
import { useDocumentsQuery } from '../Documents.requests';

/* eslint complexity: [2, 8] */
export const TrashbinButton = () => {
  const [t] = useTranslation('case');
  const { data } = useDocumentsQuery();
  const numberOfFilesInTrash =
    data && data?.filter(row => row.inTrash === true).length;

  if (numberOfFilesInTrash === 0 && trashBinView.value) {
    currentMode.value = 'documents';
  }

  return (
    <Tooltip
      sx={{ width: 'inherit' }}
      title={
        trashBinView.value
          ? t('documentTableHeaders.moduleName')
          : t('documentTableHeaders.trashBinView')
      }
    >
      <IconButton
        disabled={!numberOfFilesInTrash}
        name="toggleBinView"
        aria-label={t('documentTableHeaders.trashBinView')}
        onClick={() => {
          currentMode.value = trashBinView.value ? 'documents' : 'bin';
          selectedAcceptedRowIds.value = [];
        }}
      >
        <Badge
          badgeContent={!trashBinView.value ? numberOfFilesInTrash : 0}
          sx={{
            '& .MuiBadge-badge': {
              background: '#757575',
              top: '8px',
              left: '3px',
              color: 'white',
              border: '2px solid white',
            },
          }}
        />

        {trashBinView.value ? (
          <ArrowBack sx={{ fontSize: '28px' }} />
        ) : (
          <Delete sx={{ fontSize: '28px' }} />
        )}
      </IconButton>
    </Tooltip>
  );
};
