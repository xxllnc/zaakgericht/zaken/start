// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { TooltipButton } from '../TooltipButton';
import { trashBinView } from '../Documents.signals';
import { hiddenFileUploadId } from './HiddenFileUpload';

export const UploadButton = () => {
  const [t] = useTranslation('case');

  return (
    <TooltipButton
      title={t('documentTableActions.uploadFile')}
      sx={{
        height: '45px',
        marginTop: '6px',
        marginLeft: '7px',
        visibility: trashBinView.value ? 'hidden' : 'visible',
      }}
      name="documentTabUpload"
      icon="cloud_upload"
      iconSize="medium"
      onClick={() => document.getElementById(hiddenFileUploadId)?.click()}
    />
  );
};
