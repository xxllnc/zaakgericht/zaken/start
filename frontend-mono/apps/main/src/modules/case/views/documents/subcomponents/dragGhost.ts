// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const dragGhost = document.createElement('div');
dragGhost.id = 'drag-ghost';
dragGhost.style.position = 'absolute';
dragGhost.style.top = '-1000px';
document.body.appendChild(dragGhost);
