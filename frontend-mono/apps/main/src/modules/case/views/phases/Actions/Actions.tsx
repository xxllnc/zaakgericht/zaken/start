// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { CaseObjType, CaseTypeType } from '../../../Case.types';
import { useActionsStyles } from './Actions.styles';
import { ActionType, SaveActionType } from './Actions.types';
import {
  getActions,
  getTitle,
  isCheckDisabled,
  mergeAction,
  updateAction,
} from './Actions.library';
import PhaseActionsDialog from './Dialog/PhaseActionsDialog';

export interface ActionsPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  canEditSideBar: boolean;
  phaseNumber: number;
  setCheckedActionsCount: (checkedActionsCount: number) => void;
}

/* eslint complexity: [2, 7] */
const Actions: React.ComponentType<ActionsPropsType> = ({
  caseObj,
  caseType,
  canEditSideBar,
  phaseNumber,
  setCheckedActionsCount,
}) => {
  const classes = useActionsStyles();
  const [t] = useTranslation('phaseActions');
  const [actions, setActions] = useState<ActionType[]>();
  const [saving, setSaving] = useState<boolean>(false);
  const [actionToOpen, setActionToOpen] = useState<ActionType | null>(null);
  const actionsDisabled = !canEditSideBar || saving;
  const checkedActionsCount = (actions || []).filter(
    action => action.automatic
  ).length;

  const refreshActions = () => {
    setSaving(true);
    getActions(caseObj.number, phaseNumber)
      .then(refreshedActions => {
        setActions(refreshedActions);
        setSaving(false);
      })
      .catch(openServerError);
  };

  useEffect(() => {
    refreshActions();
  }, []);

  useEffect(() => {
    setCheckedActionsCount(checkedActionsCount);
  }, [checkedActionsCount]);

  if (!actions) {
    return <Loader />;
  }

  const saveAction: SaveActionType = (type, newAction) => {
    setSaving(true);
    setActions(mergeAction(actions, newAction));

    updateAction(caseObj.number, type, newAction)
      .then(updatedAction => {
        if (updatedAction) {
          // using the returned action from the commit response
          setActions(mergeAction(actions, updatedAction));
          setSaving(false);
        } else {
          // refresh, because the execute resposne does not return the action
          refreshActions();
        }
      })
      .catch((error: any) => {
        setActions(actions);
        openServerError(error);
      });
  };

  if (!actions.length) {
    return <div className={classes.placeholder}>{t('noActions')}</div>;
  } else {
    return (
      <div className={classes.wrapper}>
        {actions.map((action: ActionType, index: number) => (
          <div
            key={index}
            onClick={() => {
              if (!actionsDisabled) {
                setActionToOpen(action);
              }
            }}
            className={classNames(classes.action, {
              [classes.actionActive]: !actionsDisabled,
            })}
            role="button"
            tabIndex={0}
            onKeyDown={event => {
              if (
                event.key === 'Enter' &&
                (event.target as HTMLElement).tagName === 'DIV' &&
                !actionsDisabled
              ) {
                setActionToOpen(action);
              }
            }}
          >
            <Tooltip
              className={classes.checkboxWrapper}
              title={t('checkboxTooltip')}
              onClick={event => event.stopPropagation()}
            >
              <Checkbox
                name="phase-action-automatic"
                onChange={() => {
                  saveAction('toggleCheck', {
                    ...action,
                    automatic: !action.automatic,
                    tainted: true,
                  });
                }}
                checked={action.automatic}
                disabled={
                  actionsDisabled ||
                  isCheckDisabled(action, caseType, phaseNumber)
                }
              />
            </Tooltip>
            <Tooltip
              className={classNames(classes.titleWrapper, {
                [classes.titleWrapperDisabled]: actionsDisabled,
              })}
              title={getTitle(t as any, action)}
            >
              <>
                {action.icon}
                <span className={classes.title}>{action.label}</span>
              </>
            </Tooltip>
            {action.tainted && (
              <Tooltip className={classes.lockWrapper} title={t('lockTooltip')}>
                <IconButton
                  onClick={(event: any) => {
                    event.stopPropagation();

                    saveAction('untaint', {
                      ...action,
                      automatic: false,
                      tainted: false,
                    });
                  }}
                  disabled={actionsDisabled}
                >
                  <Icon size="extraSmall">{iconNames.lock}</Icon>
                </IconButton>
              </Tooltip>
            )}
          </div>
        ))}
        {actionToOpen && (
          <PhaseActionsDialog
            action={actionToOpen}
            caseObj={caseObj}
            caseType={caseType}
            phaseNumber={phaseNumber}
            saveAction={saveAction}
            onClose={() => setActionToOpen(null)}
            open={Boolean(actionToOpen)}
          />
        )}
      </div>
    );
  }
};

export default Actions;
