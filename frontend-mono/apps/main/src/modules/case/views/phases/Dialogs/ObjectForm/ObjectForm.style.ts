// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useObjectFormStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflowY: 'scroll',
  },
  inactiveWarning: {
    margin: 20,
  },
  scrollWrapper: {
    margin: 20,
    '&>*': {
      marginBottom: 12,
    },
  },
  actionWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row-reverse',
  },
}));
