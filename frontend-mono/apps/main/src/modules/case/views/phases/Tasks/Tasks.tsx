// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { Routes, Route, useNavigate, useParams } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { openServerError } from '@zaaksysteem/common/src/signals';
import useSession, {
  hasActiveIntegration,
} from '@zaaksysteem/common/src/hooks/useSession';
import {
  deleteTaskAction,
  getSupplyChainPartnersAction,
  getTasks,
  setTaskCompletionAction,
  updateTaskAction,
} from './Tasks.library';
import {
  TasksType,
  HandleChangeType,
  AddTaskType,
  UpdateTaskType,
  DeleteTaskType,
  SetTaskCompletionType,
  TaskType,
  RelatedContactType,
} from './Tasks.types';
import { TaskList } from './components/TaskList/TaskList';
import DetailsWrapper from './components/Details/DetailsWrapper';
import { addTaskAction, countOpenTasks } from './Tasks.library';

export interface TasksPropsType {
  caseUuid: string;
  setOpenTasksCount: (checkedActionsCount: number) => void;
  externalTaskAssignment: boolean;
  canEditSideBar: boolean;
  // iframe support
  iframe?: boolean;
  // ---
}

type TasksParamsType = {
  phaseNumber: string;
};

const Tasks: React.ComponentType<TasksPropsType> = ({
  caseUuid,
  externalTaskAssignment,
  setOpenTasksCount,
  canEditSideBar,
  // iframe support
  iframe,
  // ---
}) => {
  const { phaseNumber } = useParams<keyof TasksParamsType>() as TasksParamsType;
  const navigate = useNavigate();
  const [tasks, setTasks] = useState<TasksType>();
  const [supplyChainPartners, setSupplyChainPartners] = useState<
    RelatedContactType[]
  >([]);

  const session = useSession();
  const dsoActive = hasActiveIntegration(session, 'koppelapp_dso_swf');

  useEffect(() => {
    getTasks(caseUuid, phaseNumber, setTasks, setOpenTasksCount);
  }, [phaseNumber]);

  useEffect(() => {
    if (dsoActive) {
      getSupplyChainPartnersAction(caseUuid, setSupplyChainPartners);
    }
  }, []);

  if (!tasks) {
    return <Loader />;
  }

  const enterEditMode = (task: TaskType) => {
    navigate(`edit/${task.task_uuid}`);
  };

  const exitEditMode = () => {
    navigate('');
  };

  const handleChange: HandleChangeType = (action, tasks, newTasks) => {
    const newOpenTasks = countOpenTasks(newTasks);

    setTasks(newTasks);

    // iframe support
    if (!iframe) {
      setOpenTasksCount(newOpenTasks);
    }
    // ---

    exitEditMode();

    action()
      // iframe support
      .then(() => {
        if (iframe) {
          // refresh
          setOpenTasksCount(0);
        }
      })
      // ---
      .catch((err: any) => {
        const openTasks = countOpenTasks(tasks);

        setOpenTasksCount(openTasks);
        setTasks(tasks);
        openServerError(err);
      });
  };

  const addTask: AddTaskType = title => {
    addTaskAction(caseUuid, phaseNumber, title, tasks, handleChange);
  };

  const updateTask: UpdateTaskType = (task_uuid, values) => {
    updateTaskAction(task_uuid, values, tasks, handleChange);
  };

  const deleteTask: DeleteTaskType = task_uuid => {
    deleteTaskAction(task_uuid, tasks, handleChange);
  };

  const setTaskCompletion: SetTaskCompletionType = (task_uuid, completed) => {
    setTaskCompletionAction(task_uuid, completed, tasks, handleChange);
  };

  return (
    <Routes>
      <Route
        path=""
        element={
          <TaskList
            tasks={tasks}
            canEdit={canEditSideBar}
            addTask={addTask}
            setTaskCompletion={setTaskCompletion}
            enterEditMode={enterEditMode}
          />
        }
      />
      <Route
        path={`edit/:task_uuid`}
        element={
          <DetailsWrapper
            tasks={tasks}
            canEdit={canEditSideBar}
            externalTaskAssignment={externalTaskAssignment}
            dsoActive={dsoActive}
            updateTask={updateTask}
            deleteTask={deleteTask}
            setTaskCompletion={setTaskCompletion}
            exitEditMode={exitEditMode}
            supplyChainPartners={supplyChainPartners}
          />
        }
      />
    </Routes>
  );
};

export default Tasks;
