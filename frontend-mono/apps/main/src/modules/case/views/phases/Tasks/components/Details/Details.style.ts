// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDetailStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    overflowY: 'auto',
    padding: 13,
  },
  formControlWrapper: {
    marginBottom: 20,
  },
  scrollWrapper: {
    flex: '1 1 auto',
  },
}));
