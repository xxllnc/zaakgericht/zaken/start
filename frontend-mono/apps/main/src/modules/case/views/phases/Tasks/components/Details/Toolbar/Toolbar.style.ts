// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useToolbarStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 20,
    flex: 0,
  },
  tooltipWrapper: {
    width: 'auto',
  },
}));
