// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { Button } from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip/Tooltip';
import { useToolbarStyles } from './Toolbar.style';
import {
  TaskType,
  DeleteTaskType,
  ExitEditModeType,
} from './../../../Tasks.types';

export type ToolbarPropsType = {
  task: TaskType;
  t: i18next.TFunction;
  deleteTask: DeleteTaskType;
  exitEditMode: ExitEditModeType;
} & Pick<TaskType, 'completed'>;

/*eslint complexity: ["error", 10]*/
const Toolbar = ({
  task: { task_uuid, is_editable },
  completed,
  t,
  deleteTask,
  exitEditMode,
}: ToolbarPropsType) => {
  const classes = useToolbarStyles();

  return (
    <div className={classes.wrapper}>
      <Tooltip
        title={t('caseTasks:navigation.back')}
        classes={{
          wrapper: classes.tooltipWrapper,
        }}
      >
        <Button name="exitTaskEdit" action={exitEditMode} icon="arrow_back" />
      </Tooltip>

      {is_editable && !completed && (
        <Tooltip
          title={t('caseTasks:deleteTask')}
          classes={{
            wrapper: classes.tooltipWrapper,
          }}
        >
          <Button
            name="taskDelete"
            action={() => deleteTask(task_uuid)}
            icon="delete"
          />
        </Tooltip>
      )}
    </div>
  );
};

export default Toolbar;
