// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTaskItemAddonsStyles = makeStyles(
  ({ mintlab: { greyscale }, typography }: any) => ({
    addon: {
      ...typography.caption,
      color: greyscale.darkest,
      display: 'inline-flex',
      alignItems: 'center',
      lineHeight: 'normal',
    },
    addonIcon: {
      marginRight: 3,
    },
    bull: {
      color: greyscale.evenDarker,
      margin: '0 5px',
      fontSize: 14,
    },
  })
);
