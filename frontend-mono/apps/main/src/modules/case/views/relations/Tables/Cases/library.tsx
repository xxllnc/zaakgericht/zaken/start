// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Progress from '@mintlab/ui/App/Zaaksysteem/Progress/Progress';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { capitalize } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import { LevelType, RelationType } from '../../Relations.types';
import { CaseRelationType } from '../../Relations.types';
import { fetchCaseRelations } from '../requests';

const isOfRelationType =
  (filters: RelationType[]) => (caseObj: CaseRelationType) =>
    filters.includes(caseObj.relation_type);

type SetLevelAndSortType = (
  cases: CaseRelationType[],
  levels: LevelType
) => CaseRelationType[];

const setLevelAndSort: SetLevelAndSortType = (cases, level) =>
  cases
    .map(caseObj => ({ ...caseObj, level }))
    .sort((aa, bb) => aa.number - bb.number);

type FormAFamilyType = (
  self: CaseRelationType,
  siblings: CaseRelationType[],
  children: CaseRelationType[],
  parent?: CaseRelationType
) => CaseRelationType[];

const formAFamily: FormAFamilyType = (self, siblings, children, parent) => {
  let levels: LevelType[] = ['A', 'B'];

  if (parent) {
    parent.level = 'A';
    levels = ['B', 'C'];
  }

  const siblingsAndSelf = [...(parent ? [] : [self]), ...siblings];
  const siblingsSorted = setLevelAndSort(siblingsAndSelf, levels[0]);
  const childrenSorted = setLevelAndSort(children, levels[1]);

  return [...(parent ? [parent] : []), ...siblingsSorted, ...childrenSorted];
};

type AssembleSelfType = (
  caseObj: CaseObjType,
  caseType: CaseTypeType
) => CaseRelationType;

const assembleSelf: AssembleSelfType = (
  { uuid, assignee, number, progressStatus, status, summary, result },
  caseType
) => ({
  uuid,
  case_uuid: uuid,
  relation_uuid: '',
  name: uuid,
  relation_type: 'self',
  casetype_title: caseType.name,
  caseOpen: status !== 'resolved',
  result,
  summary,
  progressStatus,
  number,
  assignee: assignee?.name,
  sequence_number: 0,
});

type SetCasesType = (cases: CaseRelationType[]) => void;

type GetCasesType = (
  caseObj: CaseObjType,
  caseType: CaseTypeType,
  setFamily: SetCasesType,
  setRelated: SetCasesType
) => void;

export const getCases: GetCasesType = async (
  caseObj,
  caseType,
  setFamily,
  setRelated
) => {
  const self = assembleSelf(caseObj, caseType);
  const relations = await fetchCaseRelations(caseObj.uuid);
  const parent = relations.find(isOfRelationType(['parent']));
  const children = relations.filter(isOfRelationType(['child']));
  const related = relations.filter(
    isOfRelationType(['initiator', 'plain', 'continuation'])
  );
  let siblings: CaseRelationType[] = [];

  if (parent) {
    const parentCaseRelations = await fetchCaseRelations(parent.uuid);

    siblings = parentCaseRelations.filter(isOfRelationType(['child']));
  }

  const family = formAFamily(self, siblings, children, parent);

  setFamily(family);
  setRelated(related);
};

type RefreshRelatedCasesType = (
  caseUuid: string,
  setRelated: SetCasesType
) => void;

export const refreshRelatedCases: RefreshRelatedCasesType = async (
  caseUuid,
  setRelated
) => {
  const caseRelations = await fetchCaseRelations(caseUuid);
  const relatedCases = caseRelations.filter(
    isOfRelationType(['initiator', 'plain', 'continuation'])
  );

  setRelated(relatedCases);
};

const defaultColumns = [
  {
    name: 'number',
    width: 110,
    minWidth: 110,
    // eslint-disable-next-line
    cellRenderer: ({ rowData }: { rowData: CaseRelationType }) => (
      <a target="_parent" href={`/intern/zaak/${rowData.number}`}>
        {rowData.number}
      </a>
    ),
  },
  {
    name: 'progressStatus',
    width: 120,
    minWidth: 120,
    // eslint-disable-next-line
    cellRenderer: ({ rowData }: { rowData: CaseRelationType }) => (
      <Progress percentage={rowData.progressStatus * 100} />
    ),
  },
  {
    name: 'casetype_title',
    width: 200,
    flexGrow: 1,
    // eslint-disable-next-line
    cellRenderer: ({
      rowData: { casetype_title },
    }: {
      rowData: CaseRelationType;
    }) => {
      if (!casetype_title) return '-';

      return (
        <Tooltip title={casetype_title} noWrap={true}>
          <span>{casetype_title}</span>
        </Tooltip>
      );
    },
  },
  {
    name: 'summary',
    width: 400,
    flexGrow: 1,
    // eslint-disable-next-line
    cellRenderer: ({ rowData: { summary } }: { rowData: CaseRelationType }) => {
      return (
        <Tooltip title={summary} noWrap={true}>
          <span>{summary}</span>
        </Tooltip>
      );
    },
  },
  {
    name: 'assignee',
    width: 150,
    // eslint-disable-next-line
    cellRenderer: ({ rowData }: { rowData: CaseRelationType }) => {
      const value = rowData.assignee;

      if (!value) return '-';

      return (
        <Tooltip title={capitalize(value)} noWrap={true}>
          <span>{value}</span>
        </Tooltip>
      );
    },
  },
  {
    name: 'result',
    width: 150,
    // eslint-disable-next-line
    cellRenderer: ({ rowData: { result } }: { rowData: CaseRelationType }) => {
      const value = result?.result_name || result?.result;

      if (!value) return '-';

      return (
        <Tooltip title={capitalize(value)} noWrap={true}>
          <span>{capitalize(value)}</span>
        </Tooltip>
      );
    },
  },
];

type GetColumnsType = (
  t: i18next.TFunction,
  startingColumns?: ColumnType[],
  endingColumns?: ColumnType[]
) => ColumnType[];

export const getColumns: GetColumnsType = (
  t,
  startingColumns = [],
  endingColumns = []
) =>
  [...startingColumns, ...defaultColumns, ...endingColumns].map(row => ({
    ...row,
    label: t(`cases.columns.${row.name}`),
  }));

export const resequence = (rows: CaseRelationType[]) =>
  rows.map((row, index) => ({
    ...row,
    sequence_number: index + 1,
  }));
