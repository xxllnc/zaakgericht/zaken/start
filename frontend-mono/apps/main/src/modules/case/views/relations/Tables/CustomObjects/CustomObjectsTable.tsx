// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  CustomObjectType,
  CustomObjectsTablePropsType,
} from '../../Relations.types';
import { attributeHeight, useRelationsStyles } from '../../Relations.style';
import { relateCustomObject, unrelateCustomObject } from '../requests';
import {
  copyValuesToCase,
  getCustomObjects,
} from './CustomObjectsTable.library';
import { getColumns } from './CustomObjectsTable.columns';
import Dialog from './Dialog';

const CustomObjectsTable: React.ComponentType<CustomObjectsTablePropsType> = ({
  caseObj,
  canEdit,
  caseType,
}) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const client = useQueryClient();
  const { uuid: caseUuid } = caseObj;

  const [saving, setSaving] = useState(false);

  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const closeDialog = () => setIsDialogOpen(false);

  const query = 'case-custom-object-relations';
  const { data: customObjects } = useQuery<
    CustomObjectType[],
    V2ServerErrorsType
  >([query], () => getCustomObjects(caseUuid, caseType), {
    onError: openServerError,
  });

  if (!customObjects) {
    return <Loader />;
  }

  const relate = async (values: any) => {
    const { customObject, copy } = values;
    const objectUuid = customObject.value;

    await relateCustomObject([caseUuid], objectUuid, copy);

    closeDialog();
    client.invalidateQueries([query]);

    if (copy) {
      await copyValuesToCase(caseObj, caseType, objectUuid)
        .then(() => {
          openSnackbar({
            message: t('customObjects.snackbar.copy.success'),
            sx: snackbarMargin,
          });
        })
        .catch(() => {
          openSnackbar({
            message: t('customObjects.snackbar.copy.error'),
            sx: snackbarMargin,
          });
        });
    }
  };

  const unrelate = async (uuid: string) => {
    setSaving(true);

    await unrelateCustomObject([caseUuid], uuid);

    setSaving(false);
    client.invalidateQueries([query]);
  };

  const rows = customObjects;
  const columns = getColumns(classes, t as any, saving, unrelate);

  const headerHeight = 50;
  const height =
    rows.reduce((acc, row) => {
      return acc + row.customFieldNames.length * attributeHeight + 20;
    }, 0) + headerHeight;

  return (
    <div className={classes.section}>
      <Typography variant="h2" classes={{ root: classes.header }}>
        {t('customObjects.title')}
      </Typography>
      <div style={{ flex: '1 1 auto', height }}>
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
      {canEdit && (
        <div className={classes.actionFooter}>
          {canEdit && (
            <>
              <Button
                name="addPlannedCase"
                action={() => setIsDialogOpen(true)}
              >
                {t('plannedCases.add')}
              </Button>
              <Dialog
                isOpen={isDialogOpen}
                close={closeDialog}
                relate={relate}
              />
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default CustomObjectsTable;
