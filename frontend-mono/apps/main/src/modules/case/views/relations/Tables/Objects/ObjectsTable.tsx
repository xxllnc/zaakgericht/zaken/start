// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  updateFields,
  hasValue,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { ObjectType, ObjectsTablePropsType } from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import { relateObject } from '../requests';
import { getObjects, getChoices } from './library';
import { ObjectFinder, OBJECT_FINDER } from './ObjectFinder';

const ObjectsTable: React.ComponentType<ObjectsTablePropsType> = ({
  caseUuid,
  caseNumber,
  canEdit,
  caseType,
}) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const [objects, setObjects] = useState<ObjectType[]>([]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  useEffect(() => {
    getObjects(caseNumber, setObjects);
  }, []);

  const rows = objects;
  const columns = [
    {
      name: 'type',
      width: 1,
      flexGrow: 1,
    },
    {
      name: 'name',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: ObjectType }) => (
        <a target="_parent" href={`/object/${rowData.uuid}`}>
          {rowData.name}
        </a>
      ),
    },
  ].map(row => ({
    ...row,
    label: t(`objects.columns.${row.name}`),
  }));

  const objectTypeChoices = getChoices(caseType);
  const formDefinition: FormDefinition<{
    objectType: string;
    object: { value: string; label: string };
    copyValues: boolean;
  }> = [
    {
      name: 'objectType',
      type: fieldTypes.SELECT,
      value: objectTypeChoices[0],
      label: t('objects.dialog.labels.objectType'),
      choices: objectTypeChoices,
    },
    {
      name: 'object',
      type: fieldTypes.OBJECT_FINDER,
      value: null,
      required: true,
      label: t('objects.dialog.labels.object'),
    },
    {
      name: 'copyValues',
      type: fieldTypes.CHECKBOX,
      value: false,
      label: t('objects.dialog.labels.copyValues'),
      suppressLabel: true,
    },
  ];

  const rules = [
    new Rule<any>()
      .when('objectType', field => !hasValue(field))
      .then((fields: any) =>
        fields.map(
          updateFields(['object'], {
            disabled: true,
          })
        )
      )
      .else((fields: any) =>
        fields.map(
          updateFields(['object'], {
            disabled: false,
          })
        )
      ),
    new Rule<any>()
      .when('objectType', () => true)
      .then(transferDataAsConfig('objectType', 'object', 'objectTypeName')),
  ];

  return (
    <div className={classes.section}>
      <Typography variant="h2" classes={{ root: classes.header }}>
        {t('objects.title')}
      </Typography>
      <div
        style={{ flex: '1 1 auto', height: `calc(${rows.length + 1} * 53px)` }}
      >
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
      {canEdit && Boolean(objectTypeChoices.length) && (
        <div className={classes.actionFooter}>
          <Button name="relateObject" action={() => setIsDialogOpen(true)}>
            {t('objects.relateObject')}
          </Button>
          <FormDialog
            formDefinition={formDefinition}
            title={t('objects.dialog.title')}
            icon="layers"
            onClose={() => setIsDialogOpen(false)}
            scope="add-to-case"
            open={isDialogOpen}
            fieldComponents={{
              [OBJECT_FINDER]: ObjectFinder,
            }}
            onSubmit={async (formValues: any) => {
              const {
                object: { value },
                copyValues,
              } = formValues;
              const result = await relateObject(caseUuid, value, copyValues);

              if (result) {
                getObjects(caseNumber, setObjects);
                setIsDialogOpen(false);
              }
            }}
            saveLabel={t('objects.dialog.submit')}
            rules={rules}
          />
        </div>
      )}
    </div>
  );
};

export default ObjectsTable;
