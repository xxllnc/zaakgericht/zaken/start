// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { getDataFuncType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { TimelineItemType } from '@zaaksysteem/common/src/components/Timeline/Timeline.types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import fecha from 'fecha';
import { PAGE_LENGTH } from '@zaaksysteem/common/src/components/Timeline/Timeline';
import { FiltersOptionType } from '@zaaksysteem/common/src/components/Timeline/Timeline.types';

type GetDataType = (
  t: i18next.TFunction,
  {
    uuid,
  }: {
    uuid: string;
  }
) => getDataFuncType<TimelineItemType, any>;

/* eslint complexity: [2, 10] */
export const getData: GetDataType = (t, { uuid }) =>
  async function getDataAsync({
    pageNum,
    startDate,
    endDate,
    filters,
  }: {
    pageNum: number;
    startDate?: Date;
    endDate?: Date;
    filters?: FiltersOptionType[];
  }) {
    const filtersParams =
      filters && filters.length
        ? filters
            .filter(thisFilter => thisFilter.checked)
            .map(thisFilter => {
              return thisFilter.value;
            })
        : null;

    const urlParams: APICaseManagement.GetCaseEventLogsRequestParams = {
      page: pageNum,
      page_size: PAGE_LENGTH,
      case_uuid: uuid,
      ...(startDate && {
        period_start:
          fecha.format(new Date(startDate), 'YYYY-MM-DD') + 'T00:00:00+01:00',
      }),
      ...(endDate && {
        period_end:
          fecha.format(new Date(endDate), 'YYYY-MM-DD') + 'T23:59:59+01:00',
      }),
      ...(filtersParams &&
        filtersParams.length && {
          'filter[attributes.category]': filtersParams,
        }),
    };

    const url = buildUrl<APICaseManagement.GetCaseEventLogsRequestParams>(
      `/api/v2/cm/case/get_timeline`,
      urlParams
    );
    const result = (await request('GET', url).catch(
      openServerError
    )) as APICaseManagement.GetCaseEventLogsResponseBody | null;

    if (!result || !result.data) return { rows: [] };

    return {
      rows: result.data.map(row => {
        const { attributes, relationships } = row;

        const user = attributes?.user;
        const author = user && user !== 'None' ? user : t('unknown');

        return {
          id: attributes?.id || '',
          type: attributes?.type || '',
          author,
          date: new Date(attributes?.date || ''),
          description: attributes?.description || '',
          content: attributes?.content || '',
          exception: attributes?.exception || '',
          caseNumber: relationships?.case?.meta?.summary,
        };
      }),
    };
  };

export const getExportFunction =
  (uuid: string) =>
  ({
    startDate,
    endDate,
  }: {
    startDate?: string | null;
    endDate?: string | null;
  }) => {
    const start = startDate
      ? fecha.format(new Date(startDate), 'YYYY-MM-DD') + 'T00:00:00+01:00'
      : null;
    const end = endDate
      ? fecha.format(new Date(endDate), 'YYYY-MM-DD') + 'T23:59:59+01:00'
      : null;

    const url = buildUrl('/api/v2/cm/case/export_timeline', {
      case_uuid: uuid,
      ...(startDate && { period_start: start }),
      ...(endDate && { period_end: end }),
    });

    return request<APICaseManagement.ExportTimelineRequestBody>('POST', url);
  };
