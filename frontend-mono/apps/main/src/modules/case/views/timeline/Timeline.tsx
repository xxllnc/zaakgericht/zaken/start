// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import TimelineComponent from '@zaaksysteem/common/src/components/Timeline/Timeline';
import { getData, getExportFunction } from './Timeline.library';
import { useCaseTimelineStyles } from './Timeline.styles';
import { TimelineViewPropsType } from '.';

const Timeline: React.FunctionComponent<TimelineViewPropsType> = ({
  caseObj,
}) => {
  const [t] = useTranslation('caseTimeline');
  const classes = useCaseTimelineStyles();
  const getDataFunction = getData(t as any, {
    uuid: caseObj.uuid,
  });

  return (
    <div className={classes.wrapper}>
      <Button
        variant="text"
        color="primary"
        sx={{ marginBottom: '10px' }}
        name="timelineV1"
        action={() => {
          top &&
            (top.window.location.href = `/intern/zaak/${caseObj.number}/timeline/`);
        }}
      >
        Timeline V1
      </Button>
      <div className={classes.timelineWrapper}>
        <TimelineComponent
          getData={getDataFunction}
          exportFunction={getExportFunction(caseObj.uuid)}
          filtersOptions={[
            {
              label: t('common:timeline.filters.changes'),
              value: 'case_update',
              checked: false,
            },
            {
              label: t('common:timeline.filters.documents'),
              value: 'document',
              checked: false,
            },
          ]}
        />
      </div>
    </div>
  );
};

export default Timeline;
