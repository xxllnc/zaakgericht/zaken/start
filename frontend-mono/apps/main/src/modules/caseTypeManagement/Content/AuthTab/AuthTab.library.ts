// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
//@ts-ignore
import * as Papaparse from 'papaparse';
import {
  AuthTypeType,
  GetAuthSetsType,
  GetAuthsValueType,
  AuthLevelType,
  AuthSetType,
  AuthType,
  AuthCopyType,
  SetSetsType,
  SetCopyType,
  DownloadType,
  GetDepartmentsType,
  GetRolesType,
  FormatAuthsForExportType,
} from './AuthTab.types';
import { fetchDepartments, fetchRoles } from './AuthTab.requests';

const authTypes: AuthTypeType[] = ['public', 'confidential'];

export const authLevels: AuthLevelType[] = [
  'zaak_search',
  'zaak_read',
  'zaak_edit',
  'zaak_beheer',
];

export const getAuthSets: GetAuthSetsType = auths =>
  authTypes.reduce(
    (acc, authType) => ({
      ...acc,
      [authType]: authLevels.reduce(
        (acc, authLevel) => ({
          ...acc,
          [authLevel]: auths.filter(
            auth => auth.type === authType && auth.level === authLevel
          ),
        }),
        {}
      ),
    }),
    {}
  ) as AuthSetType;

export const getAuthsValue: GetAuthsValueType = authSets =>
  Object.values(authSets)
    .map(levels => Object.values(levels).flat())
    .flat();

export const addAuth = (
  sets: AuthSetType,
  setSets: SetSetsType,
  type: AuthTypeType,
  level: AuthLevelType
) => {
  const uuid = v4();

  let newSets = { ...sets };
  let newList = [...sets[type][level], { uuid, type, level, order: 0 }];
  newSets[type][level] = newList;

  setSets(newSets);

  setTimeout(() => {
    const newAuthElement = document.getElementById(uuid);
    newAuthElement?.scrollIntoView({ behavior: 'smooth', block: 'center' });
  });
};

export const updateAuth = (
  sets: AuthSetType,
  setSets: SetSetsType,
  newAuth: AuthType
) => {
  const { type, level, uuid } = newAuth;

  let newSets = { ...sets };
  let newList = sets[type][level].map(auth =>
    auth.uuid !== uuid ? auth : newAuth
  );
  newSets[type][level] = newList;

  setSets(newSets);
};

export const removeAuth = (
  sets: AuthSetType,
  setSets: SetSetsType,
  { type, level, uuid }: AuthType
) => {
  let newSets = { ...sets };
  let newList = sets[type][level].filter(auth => auth.uuid !== uuid);
  newSets[type][level] = newList;

  setSets(newSets);
};

export const copyAuths = (
  sets: AuthSetType,
  setCopy: SetCopyType,
  type: AuthTypeType
) => {
  setCopy(sets[type]);
};

export const addAuths = (
  sets: AuthSetType,
  setSets: SetSetsType,
  copy: AuthCopyType,
  type: AuthTypeType
) => {
  setSets({
    ...sets,
    [type]: authLevels.reduce(
      (acc, level) => ({
        ...acc,
        [level]: [
          // @ts-ignore
          ...sets[type][level],
          // @ts-ignore
          ...copy[level].map(auth => ({
            ...auth,
            type,
            uuid: v4(),
          })),
        ],
      }),
      {}
    ),
  });
};

export const overwriteAuths = (
  sets: AuthSetType,
  setSets: SetSetsType,
  copy: AuthCopyType,
  type: AuthTypeType
) => {
  setSets({
    ...sets,
    [type]: authLevels.reduce(
      (acc, level) => ({
        ...acc,
        [level]: [
          // @ts-ignore
          ...copy[level].map(auth => ({
            ...auth,
            type,
            uuid: v4(),
          })),
        ],
      }),
      {}
    ),
  });
};

export const removeAuths = (
  sets: AuthSetType,
  setSets: SetSetsType,
  type: AuthTypeType
) => {
  setSets({
    ...sets,
    [type]: authLevels.reduce((acc, level) => ({ ...acc, [level]: [] }), {}),
  });
};

const getDepartments: GetDepartmentsType = async () => {
  const response = await fetchDepartments();

  return response.data.map(({ id: uuid, meta }) => ({
    uuid,
    name: meta?.summary,
  }));
};
const getRoles: GetRolesType = async () => {
  const response = await fetchRoles();

  return response.data.map(({ id: uuid, meta }) => ({
    uuid,
    name: meta?.summary,
  }));
};

const formatAuthsForExport: FormatAuthsForExportType = async (t, auths) => {
  const departments = await getDepartments();
  const roles = await getRoles();

  const [typeKey, departmentKey, roleKey, levelKey] = [
    'type',
    'department',
    'role',
    'level',
  ].map(key => t(`auth.csvHeader.${key}`));

  return auths.map(({ type, department, role, level }) => ({
    [typeKey]: t(`auth.type.${type}`),
    [departmentKey]:
      departments.find(dep => dep.uuid === department)?.name || '',
    [roleKey]: roles.find(rol => rol.uuid === role)?.name || '',
    [levelKey]: t(`auth.level.${level}`),
  }));
};

export const download: DownloadType = async (t, caseType) => {
  const rows = await formatAuthsForExport(t, caseType.authorization);

  const fileName = `${t('auth.title')} - ${caseType.common.name}`;
  const file = Papaparse.unparse(rows, { delimiter: ',' });
  const blob = new Blob([file], { type: `text/csv;charset=utf-8;` });
  const link = document.createElement('a');

  if (link.download !== undefined) {
    const url = URL.createObjectURL(blob);

    link.setAttribute('href', url);
    link.setAttribute('download', fileName);
    link.style.visibility = 'hidden';

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
};
