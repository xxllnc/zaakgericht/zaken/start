// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import { AuthType } from '../../AuthTab.types';
import { useAuthStyles } from './Auth.styles';
import { getDefinition, getRules } from './Auth.formDefinition';

type AuthPropsType = {
  auth: AuthType;
  updateAuth: (newAuth: AuthType) => void;
  removeAuth: () => void;
  provider: any;
};

const Auth: React.ComponentType<AuthPropsType> = ({
  auth,
  updateAuth,
  removeAuth,
  provider,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useAuthStyles();
  const { uuid } = auth;

  const formDefinition = getDefinition(t as any);
  const rules = getRules();
  const form = useForm({
    formDefinition,
    rules,
    enableReinitialize: true,
    initialValues: auth,
  });

  const {
    fields,
    formik: { values },
  } = form;

  const { FieldComponent: Department, ...departmentProps } = fields[0];
  const { FieldComponent: Role, ...roleProps } = fields[1];

  useEffect(() => {
    const departmentChanged = auth.department !== values.department;
    const role = departmentChanged ? null : values.role;

    updateAuth({
      ...auth,
      ...values,
      role,
    });
  }, [values]);

  return (
    <div
      id={uuid}
      className={classes.wrapper}
      ref={provider.innerRef}
      {...provider.draggableProps}
    >
      <div {...provider.dragHandleProps} className={classes.handle}>
        <Icon>{iconNames.drag_indicator}</Icon>
      </div>
      <div
        key={roleProps.value as string}
        className={classes.departmentAndRole}
      >
        <Department {...departmentProps} />
        <Role {...roleProps} />
      </div>
      <div className={classes.buttonBar}>
        <Tooltip title={t('auth.action.remove')}>
          <IconButton color="inherit" onClick={removeAuth}>
            <Icon size="small">{iconNames.close}</Icon>
          </IconButton>
        </Tooltip>
      </div>
    </div>
  );
};

export default Auth;
