// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useLevelStyles = makeStyles(
  ({ palette: { common, elephant }, mintlab: { greyscale } }: Theme) => ({
    level: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      gap: 20,
      height: '100%',
    },
    auths: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: '100%',
      minWidth: 300,
      minHeight: 172,
      backgroundColor: greyscale.dark,
      borderRadius: 8,
      padding: 5,
    },
    add: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      color: greyscale.darkest,
      marginBottom: 5,
      '&>button': {
        flexGrow: 1,
        border: `1px dashed ${greyscale.darker}`,
        color: common.black,
        '&:hover': {
          border: `1px dashed ${greyscale.darkest}`,
          backgroundColor: elephant.lighter,
        },
      },
    },
  })
);
