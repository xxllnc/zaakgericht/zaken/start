// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import Button from '@mintlab/ui/App/Material/Button';
import {
  AuthLevelType,
  AuthSetType,
  AuthType,
  AuthTypeType,
  SetSetsType,
} from '../../AuthTab.types';
import Auth from '../Auth/Auth';
import { addAuth, removeAuth, updateAuth } from '../../AuthTab.library';
import { useLevelStyles } from './Level.styles';

type LevelPropsType = {
  droppableId: string;
  sets: AuthSetType;
  setSets: SetSetsType;
  type: AuthTypeType;
  level: AuthLevelType;
  auths: AuthType[];
};

const Level: React.ComponentType<LevelPropsType> = ({
  droppableId,
  sets,
  setSets,
  type,
  level,
  auths,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useLevelStyles();

  return (
    <div key={droppableId} className={classes.level}>
      <Typography variant="h5">{t(`auth.level.${level}`)}</Typography>
      {/* @ts-ignore */}
      <Droppable droppableId={droppableId}>
        {provided => (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            className={classes.auths}
          >
            {auths.map((auth, index) => (
              // @ts-ignore
              <Draggable key={auth.uuid} draggableId={auth.uuid} index={index}>
                {draggableProvided => (
                  <Auth
                    key={index}
                    auth={auth}
                    updateAuth={newAuth => updateAuth(sets, setSets, newAuth)}
                    removeAuth={() => removeAuth(sets, setSets, auth)}
                    provider={draggableProvided}
                  />
                )}
              </Draggable>
            ))}
            {provided.placeholder as any}
            <div className={classes.add}>
              <Button
                variant="outlined"
                name={`add-${type}-auth`}
                action={() =>
                  addAuth(
                    sets,
                    setSets,
                    type as AuthTypeType,
                    level as AuthLevelType
                  )
                }
              >
                <Typography variant="button">
                  {t('common:verbs.add')}
                </Typography>
              </Button>
            </div>
          </div>
        )}
      </Droppable>
    </div>
  );
};

export default Level;
