// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  getBaseSectionChoices,
  getPhaseSectionChoices,
} from './ChildrenTab.library';
import { GetDefinitionType } from './ChildrenTab.types';

export const getDefinition: GetDefinitionType = (t, phases) => {
  const baseSectionChoices = getBaseSectionChoices(t);
  const phaseSectionChoices = getPhaseSectionChoices(t, phases);

  return [
    {
      name: 'caseType',
      type: fieldTypes.CASE_TYPE_FINDER,
      value: null,
      placeholder: t('children.placeholders.caseType'),
    },
    {
      name: 'baseSections',
      type: fieldTypes.CHECKBOX_GROUP,
      value: [],
      choices: baseSectionChoices,
    },
    {
      name: 'phaseSections',
      type: fieldTypes.CHECKBOX_GROUP,
      value: [],
      choices: phaseSectionChoices,
    },
  ];
};
