// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const gap = 20;

export const childWidth = 405;

export const useChildStyles = makeStyles(
  ({ palette: { common }, mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: childWidth,
      border: `1px solid ${greyscale.darker}`,
      borderRadius: 8,
      display: 'flex',
      flexDirection: 'column',
      padding: 20,
      gap: 20,
      position: 'relative',
      '&:hover': {
        '& $buttonBar': {
          display: 'flex',
        },
      },
    },
    inactive: {
      opacity: 0.75,
    },
    switch: {
      display: 'flex',
      gap: 10,
      alignItems: 'center',
    },
    name: {
      flexGrow: 1,
    },
    settings: {
      display: 'flex',
      flexDirection: 'row',
      gap: 0,
    },
    buttonBar: {
      display: 'none',
      gap: 10,
      position: 'absolute',
      top: -40,
      right: -10,
      padding: 20,
      '&:hover': {
        display: 'flex',
      },
      '&>div>button': {
        backgroundColor: common.white,
        border: `1px solid ${greyscale.darker}`,
        '&:hover': {
          backgroundColor: greyscale.lighter,
        },
        '&:disabled': {
          backgroundColor: common.white,
        },
      },
    },
    icon: {
      backgroundColor: 'red',
    },
  })
);
