// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
//@ts-ignore
import { filterProperties } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import Switch from '@mintlab/ui/App/Material/Switch';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { ChildType } from './ChildrenTab.types';
import { useChildStyles } from './Child.styles';
import { getCanPublish } from './ChildrenTab.library';

type ChildPropsType = {
  child: ChildType;
  formDefinition: AnyFormDefinitionField[];
  updateChild: (newChild: ChildType) => void;
  copyValue: ChildType[];
  setCopyValue: (children: ChildType[]) => void;
  removeChild: (uuid: string) => void;
  publish: () => void;
};

const Child: React.ComponentType<ChildPropsType> = ({
  child,
  formDefinition,
  updateChild,
  copyValue,
  setCopyValue,
  removeChild,
  publish,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useChildStyles();
  const { uuid, active } = child;

  const initialValues = child;
  const canPublish = getCanPublish(child);

  const form = useForm({
    formDefinition,
    enableReinitialize: true,
    initialValues,
  });

  const {
    fields,
    formik: { values },
  } = form;

  useEffect(() => {
    const { caseType, baseSections, phaseSections } = values;
    updateChild({
      ...child,
      baseSections,
      phaseSections,
      caseType: caseType
        ? filterProperties(caseType, 'label', 'value')
        : caseType,
    });
  }, [values]);

  const { FieldComponent: CaseType, ...caseTypeProps } = fields[0];
  const { FieldComponent: BaseSections, ...baseSectionProps } = fields[1];
  const { FieldComponent: PhaseSections, ...phasesectionProps } = fields[2];

  return (
    <div
      id={uuid}
      className={classNames(classes.wrapper, {
        [classes.inactive]: !active,
      })}
    >
      <div className={classes.switch}>
        <Typography variant="subtitle1">{`${t(
          'children.switch'
        )}:`}</Typography>
        <Switch
          checked={Boolean(child.active)}
          onChange={() => updateChild({ ...child, active: !active })}
        />
      </div>
      <div className={classes.name}>
        <CaseType {...caseTypeProps} />
      </div>
      <div className={classes.settings}>
        <BaseSections {...baseSectionProps} />
        <PhaseSections {...phasesectionProps} />
      </div>
      <div className={classes.buttonBar}>
        <Tooltip title={t('children.icon.publish')}>
          <IconButton color="inherit" onClick={publish} disabled={!canPublish}>
            <Icon size="small">{iconNames.save}</Icon>
          </IconButton>
        </Tooltip>
        <Tooltip title={t('children.icon.copy')}>
          <IconButton color="inherit" onClick={() => setCopyValue([child])}>
            <Icon size="small">{iconNames.content_copy}</Icon>
          </IconButton>
        </Tooltip>
        <Tooltip title={t('children.icon.overwrite')}>
          <IconButton
            color="inherit"
            onClick={() => {
              updateChild({
                ...child,
                baseSections: copyValue[0].baseSections,
                phaseSections: copyValue[0].phaseSections,
              });
            }}
            disabled={copyValue.length !== 1}
          >
            <Icon size="small">{iconNames.format_indent_increase}</Icon>
          </IconButton>
        </Tooltip>
        <Tooltip title={t('children.icon.remove')}>
          <IconButton color="inherit" onClick={() => removeChild(uuid)}>
            <Icon size="small">{iconNames.close}</Icon>
          </IconButton>
        </Tooltip>
      </div>
    </div>
  );
};

export default Child;
