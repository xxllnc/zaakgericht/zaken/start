// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  CreateSectionType,
  BaseSectionType,
  GetBaseSectionChoicesType,
  GetPhaseSectionChoicesType,
  PhaseSectionType,
  GetCanPublishType,
  GetCanPublishAllType,
} from './ChildrenTab.types';

export const baseSectionsDict: BaseSectionType[] = [
  'relations',
  'registrationform',
  'webform',
  'case_dossier',
  'api',
  'authorization',
];

export const allocationsSection: PhaseSectionType = 'allocations';
export const firstPhaseSection: PhaseSectionType = 'first_phase';
export const lastPhaseSection: PhaseSectionType = 'last_phase';
export const resultSection: PhaseSectionType = 'results';

const createSection: CreateSectionType = (t, value) => ({
  label: t(`children.sections.${value}`),
  value,
});

export const getBaseSectionChoices: GetBaseSectionChoicesType = t =>
  baseSectionsDict.map(value => createSection(t, value));

export const getPhaseSectionChoices: GetPhaseSectionChoicesType = (
  t,
  phases
) => {
  const allocationsSectionChoice = createSection(t, allocationsSection);
  const resultSectionChoice = createSection(t, resultSection);

  const firstPhase = {
    label: t('children.sections.first_phase'),
    value: firstPhaseSection,
  };

  const otherPhases = phases.slice(1, -1);
  const otherPhaseChoices = otherPhases.map((phase, index) => {
    const phaseNumber = index + 2;
    const label = t('children.sections.phaseX', { count: phaseNumber });

    return {
      label: label,
      value: `${phaseNumber}`,
    };
  });

  const lastPhase = {
    label: t('children.sections.last_phase', { phaseCount: phases.length }),
    value: 'last_phase',
  };

  return [
    allocationsSectionChoice,
    firstPhase,
    ...otherPhaseChoices,
    lastPhase,
    resultSectionChoice,
  ];
};

export const getCanPublish: GetCanPublishType = child =>
  Boolean(child.caseType) &&
  (Boolean(child.baseSections.length) || Boolean(child.phaseSections.length));

export const getCanPublishAll: GetCanPublishAllType = children =>
  children.length > 0 && children.every(getCanPublish);
