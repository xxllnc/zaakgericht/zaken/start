// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  CaseTypeResponseBodyType,
  ChoiceType,
  CaseTypeRequestBodyType,
} from '../../CaseTypeManagement.types';
import { PhaseType } from '../PhasesTab/PhasesTab.types';

export type CreateSectionType = (
  t: i18next.TFunction,
  value: string
) => ChoiceType;

export type BaseSectionType =
  | 'relations'
  | 'registrationform'
  | 'webform'
  | 'case_dossier'
  | 'api'
  | 'authorization';

export type PhaseSectionType =
  | 'allocations' // only phase 1
  | 'first_phase'
  // | '2' | '3' | '4' | ...
  | 'last_phase'
  | 'results';

export type ChildType = {
  uuid: string;
  caseType?: {
    label: string;
    value: string;
  };
  active: boolean;
  baseSections: BaseSectionType[];
  phaseSections: PhaseSectionType[];
};

export type GetDefinitionType = (
  t: i18next.TFunction,
  phases: PhaseType[]
) => AnyFormDefinitionField[];

export type GetBaseSectionChoicesType = (t: i18next.TFunction) => ChoiceType[];

export type GetPhaseSectionChoicesType = (
  t: i18next.TFunction,
  phases: PhaseType[]
) => ChoiceType[];

export type GetCanPublishType = (child: ChildType) => boolean;

export type GetCanPublishAllType = (children: ChildType[]) => boolean;

type ChildCaseTypeSettingsType =
  CaseTypeResponseBodyType['data']['attributes']['child_casetype_settings'];

type SettingsType = ChildCaseTypeSettingsType['child_casetypes'][0]['settings'];

export type GetBaseSectionsType = (settings: SettingsType) => BaseSectionType[];

export type GetPhaseSectionsType = (
  settings: SettingsType,
  phases: PhaseType[]
) => PhaseSectionType[];

export type ConvertChildrenType = (
  childCaseTypeSettings: ChildCaseTypeSettingsType,
  phases: PhaseType[]
) => ChildType[];

export type RevertChildrenType = (
  children: ChildType[]
) => CaseTypeRequestBodyType['child_casetype_settings'];
