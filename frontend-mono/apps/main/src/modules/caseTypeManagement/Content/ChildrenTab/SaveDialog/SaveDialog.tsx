// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { Box } from '@mui/material';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { openSnackbar } from '@zaaksysteem/common/src/signals';
import { ChildType } from '../ChildrenTab.types';
import { CaseTypeType } from '../../../CaseTypeManagement.types';
import { publishChildren, useVerifyChildren } from './SaveDialog.library';
import { useSaveDailogStyles } from './SaveDialog.styles';

type SaveChildrenDialogPropsType = {
  open: boolean;
  onClose: () => void;
  childrenToPublish: ChildType[];
  caseType: CaseTypeType;
};

const SaveChildrenDialog: React.ComponentType<SaveChildrenDialogPropsType> = ({
  open,
  onClose,
  childrenToPublish,
  caseType,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useSaveDailogStyles();
  const dialogEl = useRef();

  const verifiedChildren = useVerifyChildren(childrenToPublish);
  const publishable = verifiedChildren.every(child => child.publishable);

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={'save-casetype-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon="save"
        title={t('children.save.dialog.title')}
        onCloseClick={onClose}
      />
      <DialogContent>
        <Box sx={{ marginBottom: 2 }}>
          {t('children.save.dialog.description')}
        </Box>
        <Box sx={{ marginBottom: 2 }}>
          {verifiedChildren.map(({ uuid, caseType, verified, publishable }) => (
            <div key={uuid} className={classes.child}>
              <div
                className={classNames(
                  classes.icon,
                  publishable ? classes.success : classes.error
                )}
              >
                {verified ? (
                  <Icon size="small" color="inherit">
                    {publishable ? iconNames.check_circle : iconNames.warning}
                  </Icon>
                ) : (
                  <Loader size={15} />
                )}
              </div>
              <span>{caseType?.label}</span>
            </div>
          ))}
        </Box>
        <Box>{t('children.save.dialog.warning')}</Box>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: t('save.publish'),
                onClick() {
                  publishChildren(t as any, verifiedChildren, caseType).then(
                    () => {
                      openSnackbar(
                        t('children.save.success', {
                          count: verifiedChildren.length,
                        })
                      );
                      onClose();
                    }
                  );
                },
                disabled: !publishable,
              },
              {
                text: t('common:verbs.cancel'),
                onClick: onClose,
              },
            ],
            'save-casetype-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default SaveChildrenDialog;
