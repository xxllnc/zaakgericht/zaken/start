// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  CaseTypeRequestBodyType,
  CaseTypeType,
} from '../../../CaseTypeManagement.types';
import {
  BaseSectionType,
  ChildType,
  PhaseSectionType,
} from '../ChildrenTab.types';

export type VerifiedChildType = ChildType & {
  verified: boolean;
  publishable: boolean;
  childCaseType?: CaseTypeType;
};

export type UseVerifyChildrenType = (
  childrenToPublish: ChildType[]
) => VerifiedChildType[];

export type VerifyChildrenType = (
  verifiedChildren: VerifiedChildType[],
  setVerifiedChildren: (children: VerifiedChildType[]) => void
) => void;

export type GetValuesToSaveType = (
  baseSections: BaseSectionType[],
  phaseSections: PhaseSectionType[],
  caseType: CaseTypeType,
  childData: NonNullable<VerifiedChildType['childCaseType']>
) => CaseTypeRequestBodyType;

export type PublishChildrenType = (
  t: i18next.TFunction,
  children: VerifiedChildType[],
  caseType: CaseTypeType
) => Promise<void>;
