// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { capitalize } from '@mintlab/kitchen-sink/source';
import {
  AllowedRequestorTypesType,
  CreateChoiceType,
  CreateChoicesType,
  DesignationOfConfidentialityValuesType,
  GetChoicesType,
  InitiatorTypeValuesType,
  LeadTimeTypeValueType,
  LeadTimeValueType,
  ProcessingLegalValuesType,
  ProcessingTypeValuesType,
} from './FormTab.types';

const leadTimeTypeValues: LeadTimeTypeValueType[] = ['term', 'set'];

const createChoice: CreateChoiceType = (value, t, fieldName) => ({
  label: t ? t(`form.choices.${fieldName}.${value}`) : capitalize(value),
  value,
});

const createChoices: CreateChoicesType = (values, t, fieldName) =>
  values.map(value => createChoice(value, t, fieldName));

export const getLeadTimeTypeChoices: GetChoicesType = t =>
  createChoices(leadTimeTypeValues, t, 'leadTimeType');

const leadTimeValues: LeadTimeValueType[] = [
  'kalenderdagen',
  'werkdagen',
  'weken',
];

export const getLeadTimeChoices: GetChoicesType = t =>
  createChoices(leadTimeValues, t, 'leadTime');

export const kindValues = [
  'basic_details',
  'personal_id_number',
  'income',
  'race_or_ethniticy',
  'political_views',
  'religion',
  'membership_union',
  'genetic_or_biometric_data',
  'health',
  'sexual_identity',
  'criminal_record',
  'offspring',
];

export const getKindChoices: GetChoicesType = t =>
  createChoices(kindValues, t, 'kind');

export const sourceValues = [
  'registration',
  'sender',
  'partner',
  'public_source',
];

export const getSourceChoices: GetChoicesType = t =>
  createChoices(sourceValues, t, 'source');

export const processingTypeValues: ProcessingTypeValuesType = [
  'Delen',
  'Muteren',
  'Raadplegen',
];

export const getProcessingTypeChoices: GetChoicesType = t =>
  createChoices(processingTypeValues);

export const processingLegalValues: ProcessingLegalValuesType = [
  'Toestemming',
  'Overeenkomst',
  'Wettelijke verplichting',
  'Publiekrechtelijke taak',
  'Vitaal belang',
  'Gerechtvaardigd belang',
];

export const getProcessingLegalChoices: GetChoicesType = () =>
  createChoices(processingLegalValues);

export const initiatorTypeValues: InitiatorTypeValuesType = [
  'aangaan',
  'aangeven',
  'aanmelden',
  'aanschrijven',
  'aanvragen',
  'afkopen',
  'afmelden',
  'indienen',
  'inschrijven',
  'melden',
  'ontvangen',
  'opstellen',
  'opzeggen',
  'registreren',
  'reserveren',
  'starten',
  'stellen',
  'uitvoeren',
  'vaststellen',
  'versturen',
  'voordragen',
  'vragen',
];

export const getInitiatorTypeChoices: GetChoicesType = () =>
  createChoices(initiatorTypeValues);

export const designationOfConfidentialityValues: DesignationOfConfidentialityValuesType =
  [
    'Openbaar',
    'Beperkt openbaar',
    'Intern',
    'Zaakvertrouwelijk',
    'Vertrouwelijk',
    'Confidentieel',
    'Geheim',
    'Zeer geheim',
  ];

export const getDesignationOfConfidentialityChoices: GetChoicesType = () =>
  createChoices(designationOfConfidentialityValues);

export const externalAllowedRequestorTypes: AllowedRequestorTypesType[] = [
  'natuurlijk_persoon',
  'natuurlijk_persoon_na',
  'niet_natuurlijk_persoon',
];

export const allowedRequestorTypes: AllowedRequestorTypesType[] = [
  ...externalAllowedRequestorTypes,
  'medewerker',
];

export const getAllowedRequestorTypesChoices: GetChoicesType = t =>
  createChoices(allowedRequestorTypes, t, 'allowedRequestorTypes');
