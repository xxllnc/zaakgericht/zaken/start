// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { GetAnyFormDefinitionType } from '../FormTab.types';

export const getApiFormDefinition: GetAnyFormDefinitionType = () => [
  {
    name: 'api',
    type: fieldTypes.TEXT,
    isLabel: true,
  },
  {
    name: 'apiCanTransition',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifications',
    type: fieldTypes.TEXT,
    isLabel: true,
  },
  {
    name: 'notifyOnNewCase',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnNewDocument',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnNewMessage',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnExceedTerm',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnAllocate',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnPhaseTransition',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnTaskChange',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnLabelChange',
    type: fieldTypes.CHECKBOX,
  },
  {
    name: 'notifyOnSubjectChange',
    type: fieldTypes.CHECKBOX,
  },
];
