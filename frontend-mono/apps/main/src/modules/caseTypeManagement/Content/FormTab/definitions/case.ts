// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { GetAnyFormDefinitionType } from '../FormTab.types';

export const getCaseFormDefinition: GetAnyFormDefinitionType = (
  t,
  htmlEmailTemplates
) => {
  return [
    {
      name: 'case_dossier',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'disablePipForRequestor',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'lockRegistrationPhase',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'queueCoworkerChanges',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'allowExternalTaskAssignement',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'defaultDocumentFolders',
      type: fieldTypes.MULTI_VALUE_TEXT,
      multiValue: true,
    },
    {
      name: 'htmlEmailTemplate',
      type: fieldTypes.FLATVALUE_SELECT,
      choices: htmlEmailTemplates,
    },
  ];
};
