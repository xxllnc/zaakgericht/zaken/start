// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  getDesignationOfConfidentialityChoices,
  getInitiatorTypeChoices,
  getKindChoices,
  getProcessingLegalChoices,
  getProcessingTypeChoices,
  getSourceChoices,
} from '../FormTab.choices';
import { GetAnyFormDefinitionType } from '../FormTab.types';

/* eslint complexity: [2, 7] */
export const getDocumentationFormDefinition: GetAnyFormDefinitionType = t => {
  const initiatorTypeChoices = getInitiatorTypeChoices(t);
  const designationOfConfidentialityChoices =
    getDesignationOfConfidentialityChoices(t);

  const kindChoices = getKindChoices(t);
  const sourceChoices = getSourceChoices(t);
  const processingTypeChoices = getProcessingTypeChoices(t);
  const processingLegalChoices = getProcessingLegalChoices(t);

  return [
    {
      name: 'documentation',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'processDescription',
      type: fieldTypes.TEXT,
    },
    {
      name: 'initiatorType',
      type: fieldTypes.FLATVALUE_SELECT,
      choices: initiatorTypeChoices,
      isClearable: false,
    },
    {
      name: 'motivation',
      type: fieldTypes.TEXT,
    },
    {
      name: 'purpose',
      type: fieldTypes.TEXT,
    },
    {
      name: 'archiveClassificationCode',
      type: fieldTypes.TEXT,
    },
    {
      name: 'designationOfConfidentiality',
      type: fieldTypes.FLATVALUE_SELECT,
      choices: designationOfConfidentialityChoices,
      isClearable: true,
    },
    {
      name: 'responsibleSubject',
      type: fieldTypes.TEXT,
    },
    {
      name: 'responsibleRelationship',
      type: fieldTypes.TEXT,
    },
    {
      name: 'possibilityForObjectionAndAppeal',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'publication',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'publicationText',
      type: fieldTypes.TEXTAREA,
    },
    {
      name: 'bag',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'lexSilencioPositivo',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'mayPostpone',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'mayExtend',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'extensionPeriod',
      type: fieldTypes.TEXT,
    },
    {
      name: 'adjournPeriod',
      type: fieldTypes.TEXT,
    },
    {
      name: 'penaltyLaw',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'wkpbApplies',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'eWebform',
      type: fieldTypes.TEXT,
    },
    {
      name: 'legalBasis',
      type: fieldTypes.TEXTAREA,
    },
    {
      name: 'localBasis',
      type: fieldTypes.TEXTAREA,
    },
    {
      name: 'avg',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'gdprEnabled',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'kind',
      type: fieldTypes.CHECKBOX_GROUP,
      choices: kindChoices,
      value: kindChoices[0],
    },
    {
      name: 'source',
      type: fieldTypes.CHECKBOX_GROUP,
      choices: sourceChoices,
      value: sourceChoices[0],
    },
    {
      name: 'processingType',
      type: fieldTypes.FLATVALUE_SELECT,
      choices: processingTypeChoices,
      value: processingTypeChoices[0],
      isClearable: false,
    },
    {
      name: 'processForeignCountry',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'processForeignCountryReason',
      type: fieldTypes.TEXTAREA,
    },
    {
      name: 'processingLegal',
      type: fieldTypes.FLATVALUE_SELECT,
      choices: processingLegalChoices,
      value: processingLegalChoices[0],
      isClearable: false,
    },
    {
      name: 'processingLegalReason',
      type: fieldTypes.TEXTAREA,
    },
  ];
};
