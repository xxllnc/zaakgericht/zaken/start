// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { getAllowedRequestorTypesChoices } from '../FormTab.choices';
import { GetAnyFormDefinitionType } from '../FormTab.types';

export const getRelationsFormDefinition: GetAnyFormDefinitionType = t => {
  const allowedRequestorTypesChoices = getAllowedRequestorTypesChoices(t);

  return [
    {
      name: 'contactRelations',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'allowedRequestorTypes',
      type: fieldTypes.CHECKBOX_GROUP,
      choices: allowedRequestorTypesChoices,
    },
    {
      name: 'presetRequestor',
      type: fieldTypes.CONTACT_FINDER,
      config: {
        subjectTypes: ['person', 'organization'],
      },
    },
    {
      name: 'presetAssignee',
      type: fieldTypes.CONTACT_FINDER,
      config: {
        subjectTypes: ['employee'],
      },
    },
    {
      name: 'requestorAddress',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'requestorAddressCase',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'requestorAddressCorrespondence',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'requestorAddressMap',
      type: fieldTypes.CHECKBOX,
    },
  ];
};
