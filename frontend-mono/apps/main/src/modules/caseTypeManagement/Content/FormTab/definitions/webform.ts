// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  setDisabled,
  setEnabled,
} from '@zaaksysteem/common/src/components/form/rules';
import {
  GenericGetRulesType,
  GetAnyFormDefinitionType,
} from '../FormTab.types';
import { externalAllowedRequestorTypes } from '../FormTab.choices';

export const getWebformFormDefinition: GetAnyFormDefinitionType = t => {
  return [
    {
      name: 'webform',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'enableWebform',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'addressCheck',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'reUseCaseData',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'emailRequired',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'phoneRequired',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'mobileRequired',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'disableCaptcha',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'generatePdfEndWebform',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'publicConfirmationTitle',
      type: fieldTypes.TEXT,
    },
    {
      name: 'publicConfirmationMessage',
      type: fieldTypes.WYSISWYG,
    },
    {
      name: 'caseLocationMessage',
      type: fieldTypes.TEXT,
    },
    {
      name: 'pipViewMessage',
      type: fieldTypes.TEXT,
    },
    {
      name: 'createDelayed',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'prices',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'enableOnlinePayment',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'enableManualPayment',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'priceWebform',
      type: fieldTypes.CURRENCY,
    },
    {
      name: 'priceFrontdesk',
      type: fieldTypes.CURRENCY,
    },
    {
      name: 'pricePhone',
      type: fieldTypes.CURRENCY,
    },
    {
      name: 'priceEmail',
      type: fieldTypes.CURRENCY,
    },
    {
      name: 'priceAssignee',
      type: fieldTypes.CURRENCY,
    },
    {
      name: 'pricePost',
      type: fieldTypes.CURRENCY,
    },
  ];
};

export const getWebformRules: GenericGetRulesType = (
  values,
  formDefinition
) => {
  const fieldNames = formDefinition.map(field => field.name);
  const webformSupported = externalAllowedRequestorTypes.some(requestorType =>
    values.relations.allowedRequestorTypes.includes(requestorType)
  );
  const fieldsToToggle = webformSupported
    ? fieldNames.filter(field => field !== 'enableWebform')
    : fieldNames;

  return [
    new Rule()
      .when(() => webformSupported)
      .then(setEnabled(['enableWebform']))
      .else(setDisabled(['enableWebform'])),
    new Rule()
      .when(fields => webformSupported && Boolean(fields.enableWebform.value))
      .then(setEnabled(fieldsToToggle))
      .else(setDisabled(fieldsToToggle)),
  ];
};
