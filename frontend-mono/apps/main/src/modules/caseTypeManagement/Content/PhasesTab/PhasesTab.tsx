// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext, useEffect, useState } from 'react';
import { Routes, Route, Navigate, useLocation } from 'react-router-dom';
import { useUpdateCaseType } from '../../CaseTypeManagement.library';
import { CaseTypeType } from '../../CaseTypeManagement.types';
import { useCaseTypeQuery } from '../../CaseTypeManagement.requests';
import { CaseTypeManagementContext } from '../../CaseTypeManagement.context';
import { usePhasesTabStyles } from './PhasesTab.styles';
import {
  PhaseType,
  UpdatePhaseType,
  UpdatePhasesType,
} from './PhasesTab.types';
import Header from './components/Header/Header';
import Phase from './components/Phase/Phase';

const PhasesTab: React.ComponentType = () => {
  const classes = usePhasesTabStyles();
  const { pathname } = useLocation();

  const updateCaseType = useUpdateCaseType();

  const { data: caseType } = useCaseTypeQuery() as { data: CaseTypeType };
  const [phases, setPhases] = useState<PhaseType[]>(caseType.phases);

  const { defaultPhase } = useContext(CaseTypeManagementContext);

  const updatePhases: UpdatePhasesType = newPhases => {
    setPhases(newPhases);
  };

  const updatePhase: UpdatePhaseType = newPhase => {
    const newPhases = phases.map(phase =>
      phase.id === newPhase.id ? newPhase : phase
    );

    updatePhases(newPhases);
  };

  useEffect(() => {
    updateCaseType({ phases });
  }, [phases]);

  const phaseNumber = Number(pathname.split('/')[5]);
  const activePhase = phases[phaseNumber - 1];

  if (phaseNumber > phases.length) {
    return <Navigate to={defaultPhase} replace={true} />;
  }

  return (
    <div className={classes.wrapper}>
      <Header
        phases={phases}
        updatePhases={updatePhases}
        phaseNumber={`${phaseNumber}`}
      />
      <Routes>
        <Route>
          <Route
            path=""
            element={<Navigate to={defaultPhase} replace={true} />}
          />
          <Route
            path=":phaseNumber/*"
            element={<Phase phase={activePhase} updatePhase={updatePhase} />}
          />
        </Route>
      </Routes>
    </div>
  );
};

export default PhasesTab;
