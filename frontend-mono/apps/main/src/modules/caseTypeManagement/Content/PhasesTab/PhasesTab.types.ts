// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CaseTypeResponseBodyType } from '../../CaseTypeManagement.types';

export type ViewModeType = 'stacked' | 'sideBySide';
export type SetPhasesStackedType = (isStacked: boolean) => void;

export type SideBarType = 'actions' | 'tasks';

export type RawPhaseType = NonNullable<
  CaseTypeResponseBodyType['data']['attributes']['phases']
>[number];
type RawFormType = NonNullable<RawPhaseType['custom_fields']>;
export type RawGroupType = Extract<
  RawFormType[number],
  { attribute_type: 'group' }
>;
export type RawTextblockType = Extract<
  RawFormType[number],
  { attribute_type: 'textblock' }
>;
export type RawAttributeType = Exclude<
  RawFormType[number],
  RawGroupType | RawTextblockType
>;
export type RawFieldType = RawTextblockType | RawAttributeType;

export type AttributeAuthorizationsType = {
  department: {
    label: string;
    value: string;
  };
  role: {
    label: string;
    value: string;
  };
};

export type AttributeTypeType = RawAttributeType['attribute_type'];

export type DateLimitType = {
  active: boolean;
  value: string;
  term: 'days' | 'weeks' | 'months' | 'years';
  during: 'voor' | 'na';
  reference: string;
};

export type DateLimitsType = {
  start: DateLimitType | null;
  end: DateLimitType | null;
};

export type ObjectMappingType = {
  label: string;
  value: string;
};

export type GroupType = {
  id: string;
  type: 'group';
  title: string;
  description: string;
  publishOnPip: boolean;
  fields: FieldType[];
};

export type TextblockType = {
  id: string;
  type: 'textblock';
  title: string;
  description: string;
  publishOnPip: boolean;
};

export type AttributeType = {
  id: string;
  uuid: string;
  type: AttributeTypeType;
  name: string;
  magicString: string;

  permanentHidden: boolean;
  referential: boolean;
  title: string;
  descriptionInternal: string;
  descriptionExternal: string;
  mandatory: boolean;
  publishOnPip: boolean;
  editOnPip: boolean;
  skipQueue: boolean;
  isMultiple?: boolean;
  labelMultiple?: string;
  caseAddress?: boolean;
  showOnMap?: boolean;
  mapLayer?: string;
  mapLayerAttribute?: {
    label: string;
    value: string;
  };
  dateLimits?: DateLimitsType;
  authorizations: AttributeAuthorizationsType[] | null;
  role?: string;
  createObjectEnabled?: boolean;
  createObjectLabel?: string;
  createObjectMapping?: ObjectMappingType[];
  relationshipType?: 'custom_object' | 'subject' | null;
};

export type FieldType = AttributeType | TextblockType;

type RuleType = {};

type DocumentType = {};
export type CaseTypeRelationType = 'child' | 'follow' | 'related';
type CaseTypeType = {
  relationType: CaseTypeRelationType;
};
type MessageType = {};
type ContactType = {};
type AssignmentType = {
  department: {
    label: string;
    value: string;
  };
  role: {
    label: string;
    value: string;
  };
  enabled: boolean;
};

type ObjectActionType = {};
export type TaskType = {
  id: string;
  value: string;
};

export type PhaseType = {
  id: string;
  name: string;
  number: number;
  milestone: string;
  term: number;
  groups: GroupType[];
  rules: RuleType[];
  documents: DocumentType[];
  messages: MessageType[];
  contacts: ContactType[];
  caseTypes: CaseTypeType[];
  objectActions: ObjectActionType[];
  assignment?: AssignmentType;
  tasks: TaskType[];
  results?: ResultType[];
};

type RawAssignmentType = RawPhaseType['assignment'];

export type ConvertAssignmentType = (
  assignment: RawAssignmentType
) => AssignmentType | undefined;
export type RevertAssignmentType = (
  assignment: AssignmentType | undefined
) => RawAssignmentType;

export type ConvertTaskType = (task: string) => TaskType;
export type RevertTaskType = (task: TaskType) => string;

export type ConvertGroupType = (group: RawGroupType) => GroupType;
export type ConvertTextblockType = (
  textblock: RawTextblockType
) => TextblockType;
export type ConvertAttributeType = (attr: RawAttributeType) => AttributeType;

export type RevertFieldType = (field: FieldType) => RawFieldType;
export type RevertGroupType = (group: GroupType) => RawGroupType;
export type RevertTextblockType = (
  textblock: TextblockType
) => RawTextblockType;
export type RevertPermissionsType = (
  auths: AttributeAuthorizationsType[] | null
) => RawAttributeType['permissions'];
export type RevertAttributeType = (attr: AttributeType) => RawAttributeType;

export type ConvertFormType = (form: RawFormType) => GroupType[];
export type RevertFormType = (groups: GroupType[]) => RawFieldType[];

export type ConvertPhasesType = (
  phases: RawPhaseType[],
  results: RawResultType[]
) => PhaseType[];
export type RevertPhasesType = (phases: PhaseType[]) => {
  phases: RawPhaseType[];
  results: RawResultType[] | null;
};

export type UpdatePhasesType = (phases: PhaseType[]) => void;
export type UpdatePhaseType = (phase: PhaseType) => void;

export type RawResultType = NonNullable<RawPhaseType['results']>[0];
type ResultTypeType = RawResultType['result_type'];
type ArchivalNominiationType = RawResultType['archival_nomination'];
type ArchivalNominiationValuationType =
  RawResultType['archival_nomination_valuation'];
type ProcessTypeGenericType = NonNullable<RawResultType['procestype_generic']>;
type OriginType = RawResultType['origin'];
type ProcessPeriodType = RawResultType['process_period'];

export type ResultType = {
  id: string;
  type: 'result';

  name: string;
  resultType: ResultTypeType;
  isDefault: boolean;

  selectionList: string;
  selectionListSourceDate: string;
  selectionListEndDate: string;
  selectionListNumber: string;

  archivalTrigger: boolean;
  archivalNomination: ArchivalNominiationType;
  retentionPeriod: string | null;
  archivalNominationValuation: ArchivalNominiationValuationType;

  comments: string;
  processtypeNumber: string;
  processtypeName: string;
  processtypeDescription: string;
  processtypeExplanation: string;
  processtypeObject: string;
  processtypeGeneric: ProcessTypeGenericType | null;
  origin: OriginType | null;
  processPeriod: ProcessPeriodType | null;
};

export type ConvertResultsType = (results: RawResultType[]) => ResultType[];
export type RevertResultsType = (results: ResultType[]) => RawResultType[];

export type UpdateResultsType = (results: ResultType[]) => void;
