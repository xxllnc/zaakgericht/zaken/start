// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { filterProperties } from '@mintlab/kitchen-sink/source';
import { CaseTypeRelationType, PhaseType } from '../../PhasesTab.types';

const setTypesOfCaseTypes = (
  caseTypes: PhaseType['caseTypes'],
  barredType: CaseTypeRelationType,
  replacementType: CaseTypeRelationType
) =>
  caseTypes.map(caseType =>
    caseType.relationType === barredType
      ? { ...caseType, relationType: replacementType }
      : caseType
  );

// set assignment to previous assignment when not set
// set caseTypes of type successor to type related
const fixMiddleItem = (item: PhaseType, previousItem?: PhaseType) => ({
  ...item,
  assignment: item.assignment ? item.assignment : previousItem?.assignment,
  caseTypes: setTypesOfCaseTypes(item.caseTypes, 'follow', 'related'),
});

// add name, milestone
// remove assignment
// set caseTypes of type child to type related
const fixLastItem = (
  item: PhaseType,
  previousLastProps: Pick<PhaseType, 'name' | 'milestone'>
) => ({
  ...item,
  ...previousLastProps,
  assignment: undefined,
  caseTypes: setTypesOfCaseTypes(item.caseTypes, 'child', 'related'),
});

/* eslint complexity: [2, 100] */
export const fixItems = (
  items: PhaseType[],
  newItems: PhaseType[],
  destinationIndex: number = 0,
  sourceIndex?: number
) => {
  const previousLastItem = items[items.length - 1];
  const previousLastProps = filterProperties(
    previousLastItem,
    'name',
    'milestone'
  );

  const removed = !destinationIndex && !sourceIndex;
  const moved = Boolean(sourceIndex);
  const added = newItems.length > items.length;

  const itemCount = newItems.length;
  const lastIndex = itemCount - 1;
  const lastItem = newItems[lastIndex];
  const secondToLastItem = newItems[lastIndex - 1];
  const destinationIsLast = itemCount === destinationIndex + 1;

  let fixedItems = newItems;

  // REMOVED
  if (removed) {
    fixedItems[lastIndex] = fixLastItem(lastItem, previousLastProps);

    // MOVED
  } else if (moved) {
    if (destinationIsLast) {
      fixedItems[lastIndex] = fixLastItem(lastItem, previousLastProps);
      fixedItems[lastIndex - 1] = fixMiddleItem(secondToLastItem);
    } else {
      const movedItem = newItems[destinationIndex];
      const movedItemPrevious = newItems[destinationIndex - 1];

      fixedItems[lastIndex] = fixLastItem(lastItem, previousLastProps);
      fixedItems[destinationIndex] = fixMiddleItem(
        movedItem,
        movedItemPrevious
      );
    }

    // ADDED
  } else if (added) {
    if (destinationIsLast) {
      fixedItems[lastIndex] = fixLastItem(lastItem, previousLastProps);
      fixedItems[lastIndex - 1] = fixMiddleItem(secondToLastItem);
    } else {
      const addedItem = newItems[destinationIndex];
      const addedItemPrevious = newItems[destinationIndex - 1];

      fixedItems[destinationIndex] = fixMiddleItem(
        addedItem,
        addedItemPrevious
      );
    }
  }

  // ALWAYS RENUMBER
  return fixedItems.map((item, index) => ({
    ...item,
    number: index + 1,
  }));
};

// Navigate to the phase after its position might have changed
// or navigate to the last phase if the phase no longer exists
export const fixNavigation = (
  navigate: any,
  phaseNumber: string,
  phases: PhaseType[],
  items: PhaseType[]
) => {
  const previousPhase = phases.find(
    ({ number }) => number === Number(phaseNumber)
  );
  const previousId = previousPhase?.id;
  const currentPhase = items.find(({ id }) => id === previousId);
  const currentNumber = currentPhase?.number;

  navigate(`${currentNumber || items.length}`);
};
