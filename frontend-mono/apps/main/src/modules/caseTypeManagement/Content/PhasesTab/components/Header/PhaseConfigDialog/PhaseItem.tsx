// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { IconButton, Typography } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { PhaseType } from '../../../PhasesTab.types';
import { usePhaseItemStyles } from './PhaseItem.styles';

type PhaseItemPropsType = {
  phase: PhaseType;
  provided?: any;
  setItemToAdd?: (number: number) => void;
  setItemToEdit?: (item: PhaseType) => void;
  removeItem?: (item: PhaseType) => void;
};

/* eslint complexity: [2, 10] */
const PhaseItem: React.ComponentType<PhaseItemPropsType> = ({
  phase,
  provided,
  setItemToAdd,
  setItemToEdit,
  removeItem,
}) => {
  const classes = usePhaseItemStyles();

  const { name, milestone, term } = phase;
  const canEdit = phase.number !== 1;

  return (
    <div
      ref={provided?.innerRef}
      {...provided?.draggableProps}
      className={classes.content}
    >
      <div {...provided?.dragHandleProps} className={classes.handle}>
        {canEdit && <Icon>{iconNames.drag_indicator}</Icon>}
      </div>
      <div className={classes.main}>
        <div className={classes.title}>
          <Typography variant="h6" classes={{ root: classes.name }}>
            {name}
          </Typography>
          {Boolean(term) && <Typography variant="body1">{term}</Typography>}
          <Icon size="extraSmall">{iconNames.trending_flat}</Icon>
          <Typography variant="body1">{milestone}</Typography>
        </div>
        {setItemToAdd && (
          <div className={classes.plusBackdrop}>
            <div className={classes.plus}>
              <IconButton
                onClick={() => setItemToAdd(phase.number + 1)}
                size="small"
              >
                <div className={classes.icon}>
                  <Icon size="extraSmall">{iconNames.add}</Icon>
                </div>
              </IconButton>
            </div>
          </div>
        )}
      </div>

      <div className={classes.icons}>
        {canEdit && setItemToEdit && (
          <IconButton onClick={() => setItemToEdit(phase)} size="small">
            <div className={classes.icon}>
              <Icon size="extraSmall">{iconNames.edit}</Icon>
            </div>
          </IconButton>
        )}
        {canEdit && removeItem && (
          <IconButton onClick={() => removeItem(phase)} size="small">
            <div className={classes.icon}>
              <Icon size="extraSmall">{iconNames.delete}</Icon>
            </div>
          </IconButton>
        )}
      </div>
    </div>
  );
};

export default PhaseItem;
