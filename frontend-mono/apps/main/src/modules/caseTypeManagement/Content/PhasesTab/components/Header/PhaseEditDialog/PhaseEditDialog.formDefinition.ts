// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { PhaseType } from '../../../PhasesTab.types';

/* eslint complexity: [2, 8] */
export const getDefinition = (
  t: i18next.TFunction,
  items: PhaseType[],
  itemToAdd?: number,
  itemToEdit?: PhaseType | null
) => {
  const isLastPhase =
    itemToEdit?.number === items.length || (itemToAdd || 0) > items.length;
  const lastItem = items[items.length - 1];

  const defaultName = isLastPhase ? lastItem.name : '';
  const defaultMilestone = isLastPhase ? lastItem.milestone : '';

  return [
    {
      name: 'name',
      type: fieldTypes.TEXT,
      value: itemToEdit?.name || defaultName,
      required: true,
      disabled: isLastPhase,
    },
    {
      name: 'milestone',
      type: fieldTypes.TEXT,
      value: itemToEdit?.milestone || defaultMilestone,
      required: true,
      disabled: isLastPhase,
    },
    {
      name: 'term',
      type: fieldTypes.NUMERIC,
      value: itemToEdit?.term || '',
    },
  ].map((field: any) => ({
    ...field,
    label: t(`phases.config.formDialog.labels.${field.name}`),
  }));
};
