// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { v4 } from 'uuid';
import { PhaseType } from '../../../PhasesTab.types';
import { getDefinition } from './PhaseEditDialog.formDefinition';

const itemBase = {
  groups: [],
  rules: [],
  caseTypes: [],
  documents: [],
  messages: [],
  contacts: [],
  objectActions: [],
  tasks: [],
};

type PhaseEditDialogPropsType = {
  items: PhaseType[];
  itemToAdd?: number;
  addItem?: (item: PhaseType) => void;
  itemToEdit?: PhaseType | null;
  editItem?: (item: PhaseType) => void;
  open: boolean;
  onClose: () => void;
};

const PhaseEditDialog: React.ComponentType<PhaseEditDialogPropsType> = ({
  items,
  itemToAdd,
  addItem,
  editItem,
  itemToEdit,
  open,
  onClose,
}) => {
  const [t] = useTranslation('caseTypeManagement');

  const action = itemToAdd ? 'create' : 'edit';
  const verb = t(`common:verbs.${action}`);
  const title = t(`phases.config.formDialog.title`, {
    verb: verb.toLowerCase(),
  });
  const scope = `phase-${action}-dialog`;
  const icon =
    action === 'edit' ? iconNames.edit : iconNames.add_circle_outline;

  const formDefinition = getDefinition(t as any, items, itemToAdd, itemToEdit);

  return (
    <FormDialog
      scope={scope}
      open={open}
      icon={icon}
      title={title}
      saveLabel={verb}
      onClose={onClose}
      formDefinition={formDefinition}
      onSubmit={async ({ name, milestone, term }: any) => {
        if (itemToAdd && addItem) {
          addItem({
            id: v4(),
            ...itemBase,
            number: itemToAdd,
            name,
            milestone,
            term,
          });
        }

        if (itemToEdit && editItem) {
          editItem({
            ...itemToEdit,
            name,
            milestone,
            term,
          });
        }

        onClose();
      }}
    />
  );
};

export default PhaseEditDialog;
