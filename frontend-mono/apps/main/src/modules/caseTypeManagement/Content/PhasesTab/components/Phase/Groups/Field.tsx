// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { GroupType, FieldType } from '../../../PhasesTab.types';
import { useGroupsStyles } from './Groups.styles';
import { OpenTextblockDialogType } from './dialogs/useTextblockDialog';
import { OpenAttributeDialogType } from './dialogs/useAttributeDialog';
import FieldInfo from './FieldInfo';

type FieldPropsType = {
  group: GroupType;
  field: FieldType;
  openTextblockDialog: OpenTextblockDialogType;
  openAttributeDialog: OpenAttributeDialogType;
  removeField: (group: GroupType, field: FieldType) => void;
  isDragging: boolean;
  provided: any;
};

const Field: React.ComponentType<FieldPropsType> = ({
  group,
  field,
  openTextblockDialog,
  openAttributeDialog,
  removeField,
  isDragging,
  provided,
}) => {
  const classes = useGroupsStyles();

  const edit =
    field.type === 'textblock'
      ? () => openTextblockDialog({ type: 'edit', group, textblock: field })
      : () => openAttributeDialog({ type: 'edit', group, field });

  const remove = () => removeField(group, field);

  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      className={classNames(classes.row, classes.fieldRow, {
        [classes.dragging]: isDragging,
      })}
    >
      <FieldInfo
        field={field}
        edit={edit}
        remove={remove}
        provided={provided}
      />
    </div>
  );
};

export default Field;
