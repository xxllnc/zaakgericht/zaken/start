// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import classNames from 'classnames';
import { CaseTypeManagementContext } from '../../../../../CaseTypeManagement.context';
import { FieldType, GroupType } from '../../../PhasesTab.types';
import { useGroupsStyles } from './Groups.styles';
import PropertyIcons from './components/PropertyIcons';
import DragHandle from './components/DragHandle';
import TypeIcon from './components/TypeIcon';
import MagicString from './components/MagicString';
import ActionButtons from './components/ActionButtons';

type FieldInfoPropsType = {
  field: GroupType | FieldType;
  edit: () => void;
  remove?: () => void;
  provided: any;
};

/* eslint complexity: [2, 100] */
const FieldInfo: React.ComponentType<FieldInfoPropsType> = ({
  field,
  edit,
  remove,
  provided,
}) => {
  const classes = useGroupsStyles();

  const { showGrower, showIcons, showTitles } = useContext(
    CaseTypeManagementContext
  );

  // @ts-ignore
  const { type, name, magicString, title } = field;
  const isAttribute = type !== 'group' && type !== 'textblock';
  const showAttributeInfo = showGrower && isAttribute;

  const rowName = name || magicString || title;
  const nameClass = classNames(classes.name, {
    [classes.bold]: type === 'group',
    [classes.italic]: type === 'textblock',
    [classes.nameLimit]: showAttributeInfo,
  });

  return (
    <>
      <DragHandle provided={provided} />
      <div className={nameClass}>{rowName}</div>
      {showAttributeInfo && (
        <div className={classes.grower}>
          <TypeIcon field={field} />
          {showTitles ? (
            <div className={classes.title}>{title || '-'}</div>
          ) : (
            <MagicString field={field} />
          )}
        </div>
      )}
      {showIcons && <PropertyIcons field={field} />}
      <ActionButtons edit={edit} remove={remove} />
    </>
  );
};

export default FieldInfo;
