// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  PhaseType,
  GroupType,
  FieldType,
  UpdatePhaseType,
  AttributeType,
  TextblockType,
  DateLimitsType,
  DateLimitType,
  UpdateResultsType,
  ResultType,
} from '../../../PhasesTab.types';
import { CaseTypeType } from '../../../../../CaseTypeManagement.types';
import { AttributeChoiceType } from './components/AttributeSearch';
import {
  GroupValuesType,
  ResultValuesType,
  TextblockValuesType,
} from './dialogs/definitions';

const overWriteById = (elements: any[], element: any) =>
  elements.map(el => (el.id === element.id ? element : el));

const removeById = (elements: any[], id: string) =>
  elements.filter(el => el.id !== id);

const updateGroup = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  newGroup: GroupType
) => {
  const newGroups = overWriteById(phase.groups, newGroup);

  updatePhase({ ...phase, groups: newGroups });
};

const updateField = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType,
  newField: FieldType
) => {
  const newFields = overWriteById(group.fields, newField);

  updateGroup(phase, updatePhase, { ...group, fields: newFields });
};

export const GROUP_ZONE = 'group_zone';
export const RESULTS_ZONE = 'result_zone';

export const onDragEnd = (
  { type, source, destination, draggableId }: any,
  phase: PhaseType,
  updatePhase: UpdatePhaseType
) => {
  if (!destination) return;

  const { groups } = phase;

  if (type === GROUP_ZONE) {
    const group = groups.find(({ id }) => id === draggableId) as GroupType;

    let newGroups = removeById(groups, draggableId);
    newGroups.splice(destination.index, 0, group);

    updatePhase({ ...phase, groups: newGroups });
  } else {
    const sourceGroup = groups.find(
      group => source.droppableId === group.id
    ) as GroupType;
    const field = sourceGroup?.fields.find(
      field => draggableId === field.id
    ) as FieldType;

    const newGroups = groups.map((group, index) => {
      const isSource = source.droppableId === group.id;
      const isDestination = destination.droppableId === group.id;

      if (isSource && isDestination) {
        let newFields = removeById(group.fields, draggableId);
        newFields.splice(destination.index, 0, field);

        return { ...group, fields: newFields };
      } else if (isSource) {
        const newFields = removeById(group.fields, draggableId);

        return { ...group, fields: newFields };
      } else if (isDestination) {
        const newFields = [...group.fields];
        newFields.splice(destination.index, 0, field);

        return { ...group, fields: newFields };
      } else {
        return group;
      }
    });

    updatePhase({ ...phase, groups: newGroups });
  }
};

export const addGroup = (
  values: GroupValuesType,
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group?: GroupType
) => {
  const newGroup: GroupType = {
    id: v4(),
    type: 'group',
    fields: [],
    ...values,
  };
  const indexOfPhase = phase.groups.findIndex(grp => grp.id === group?.id);
  const newGroups = [...phase.groups];
  newGroups.splice(indexOfPhase + 1, 0, newGroup);

  updatePhase({
    ...phase,
    groups: newGroups,
  });
};

export const editGroup = (
  values: GroupValuesType,
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType
) => {
  const newGroup = { ...group, ...values };
  const newGroups = phase.groups.map(group =>
    group.id === newGroup.id ? newGroup : group
  );

  updatePhase({ ...phase, groups: newGroups });
};

export const removeGroupInclusive = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType
) => {
  const newGroups = phase.groups.filter(grp => grp.id !== group?.id);

  updatePhase({ ...phase, groups: newGroups });
};

export const removeGroupExclusive = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType
) => {
  const indexToRemove = phase.groups.findIndex(grp => grp.id === group.id);
  const indexToMergeTo = indexToRemove - 1;
  const fieldsToMerge = phase.groups[indexToRemove].fields;

  const newGroups = phase.groups.reduce((acc, grp, index) => {
    if (index === indexToRemove) return acc;
    if (index === indexToMergeTo)
      return [...acc, { ...grp, fields: [...grp.fields, ...fieldsToMerge] }];

    return [...acc, grp];
  }, [] as GroupType[]);

  updatePhase({ ...phase, groups: newGroups });
};

export const addTextblock = (
  values: TextblockValuesType,
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType
) => {
  const newTextblock: TextblockType = {
    id: v4(),
    type: 'textblock',
    ...values,
  };

  const newFields = [...group.fields, newTextblock];

  updateGroup(phase, updatePhase, { ...group, fields: newFields });
};

export const editTextblock = (
  values: TextblockValuesType,
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType,
  textblock: TextblockType
) => {
  updateField(phase, updatePhase, group, { ...textblock, ...values });
};

export const addAttribute = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType,
  choice: AttributeChoiceType
) => {
  const newField: AttributeType = {
    id: v4(),
    uuid: choice.uuid,
    name: choice.label,
    type: choice.type,
    magicString: choice.value,
    permanentHidden: false,
    referential: false,
    title: choice.label,
    descriptionInternal: '',
    mandatory: false,
    publishOnPip: false,
    editOnPip: false,
    skipQueue: false,
    descriptionExternal: '',
    authorizations: [],
    dateLimits: {
      start: null,
      end: null,
    },
  };

  const newFields = [...group.fields, newField];

  updateGroup(phase, updatePhase, { ...group, fields: newFields });
};

const sanitizeDateLimit = (dateLimit: DateLimitType) =>
  dateLimit
    ? {
        ...dateLimit,
        active: Boolean(dateLimit?.active),
        value: Number(dateLimit?.value || 0),
      }
    : null;

const sanitizeDateLimits = ({ start, end }: DateLimitsType) => ({
  start: start ? sanitizeDateLimit(start) : null,
  end: end ? sanitizeDateLimit(end) : null,
});

export const editAttribute = (
  values: any,
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType,
  field: FieldType
) => {
  let newField = { ...field, ...values };

  if (field.type === 'date') {
    newField.dateLimits = sanitizeDateLimits(values.dateLimits);
  }

  updateField(phase, updatePhase, group, newField);
};

export const removeField = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType,
  group: GroupType,
  field: FieldType
) => {
  const newFields = removeById(group.fields, field.id);

  updateGroup(phase, updatePhase, { ...group, fields: newFields });
};

export const getShowExternalDescription: (
  caseType: CaseTypeType,
  phaseNumber: string,
  field: any
) => boolean = (caseType, phaseNumber, field) => {
  const supportPip = !caseType.case_dossier.disablePipForRequestor;
  const supportWebform = caseType.webform.enableWebform;
  const firstPhase = phaseNumber === '1';
  const publishedOnPip =
    typeof field.publishOnPip === 'boolean'
      ? field.publishOnPip
      : field.publishOnPip?.value;

  if (supportWebform && firstPhase) return true;
  if (supportPip && publishedOnPip) return true;

  return false;
};

export const addResult = (
  values: ResultValuesType,
  results: ResultType[],
  updateResults: UpdateResultsType
) => {
  const newResult: ResultType = {
    id: v4(),
    type: 'result',
    ...values,
  };
  const newResults = [...results, newResult];

  updateResults(newResults);
};

export const editResult = (
  values: ResultValuesType,
  results: ResultType[],
  updateResults: UpdateResultsType,
  result: ResultType
) => {
  const newResult = { ...result, ...values };
  const newResults = results.map(result =>
    result.id === newResult.id ? newResult : result
  );

  updateResults(newResults);
};

export const removeResult = (
  results: ResultType[],
  updateResults: UpdateResultsType,
  result: ResultType
) => {
  const newResults = removeById(results, result.id);

  updateResults(newResults);
};

export const onResultsDragEnd = (
  { type, source, destination, draggableId }: any,
  results: ResultType[],
  updateResults: UpdateResultsType
) => {
  if (!destination) return;

  if (type === RESULTS_ZONE) {
    const group = results.find(({ id }) => id === draggableId) as ResultType;

    let newResults = removeById(results, draggableId);
    newResults.splice(destination.index, 0, group);

    updateResults(newResults);
  }
};
