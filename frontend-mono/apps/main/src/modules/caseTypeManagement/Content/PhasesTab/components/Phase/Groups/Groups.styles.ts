// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const indentation = 15;

export const useGroupsStyles = makeStyles(
  ({ typography, palette: { common }, mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },
    header: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      gap: 10,
      padding: 10,
      height: 58,
    },
    placeholder: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      padding: 10,
      gap: 10,
    },
    handle: {
      height: '100%',
      padding: 5,
      display: 'flex',
      alignItems: 'center',
      color: greyscale.darker,
    },
    group: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },
    row: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    groupRow: {
      backgroundColor: greyscale.light,
    },
    fieldRow: {
      paddingLeft: indentation,
      backgroundColor: common.white,
      '&:hover': {
        backgroundColor: greyscale.light,
      },
    },
    dragging: {
      border: `1px dashed ${greyscale.darkest}`,
    },
    grower: {
      flexGrow: 1,
      display: 'flex',
      gap: 10,
      overflow: 'hidden',
    },
    fields: {
      display: 'flex',
      flexDirection: 'column',
    },
    info: {
      flexGrow: 1,
      display: 'flex',
      overflow: 'hidden',
    },
    name: {
      flexGrow: 1,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      color: common.black,
    },
    nameLimit: {
      minWidth: 250,
      maxWidth: 250,
    },
    technical: {
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row',
      gap: 10,
      overflow: 'hidden',
    },
    typeIcon: {
      minWidth: 18,
      color: greyscale.darkest,
    },
    magicStringButton: {
      overflow: 'hidden',
      color: common.black,
      background: 'none',
      border: 'none',
      padding: 0,
      font: 'inherit',
      outline: 'inherit',

      '&:hover': {
        textDecoration: 'underline',
        cursor: 'pointer',
      },
      '& $magicStringTooltip': {
        backgroundColor: 'red',
        padding: 50,
      },
      '&:active': {
        textDecoration: 'unset',
      },
    },
    magicString: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    form: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      overflow: 'hidden',
    },
    title: {
      color: common.black,
      flexGrow: 1,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    bold: {
      fontWeight: typography.fontWeightBold,
      marginRight: indentation,
    },
    italic: {
      fontStyle: 'italic',
    },
    icons: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      gap: 5,
      paddingRight: 10,
      color: greyscale.darker,
    },
    icon: {
      width: 20,
    },
    buttons: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      minWidth: 66 + 8,
    },
    addButtons: {
      borderRadius: 8,
      backgroundColor: greyscale.dark,
      marginRight: 8,
    },
    new: {
      display: 'flex',
      flexDirection: 'row-reverse',
      padding: '8px 0px 8px 25px',
      alignItems: 'center',
      backgroundColor: common.white,
    },
    searchWrapper: {
      flexGrow: 1,
      paddingRight: 10,
    },
    search: {
      maxWidth: 300,
    },
  })
);
