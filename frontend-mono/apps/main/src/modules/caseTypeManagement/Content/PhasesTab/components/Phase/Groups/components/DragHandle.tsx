// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useGroupsStyles } from '../Groups.styles';

type DragHandlePropsType = {
  provided: any;
};

const DragHandle: React.ElementType<DragHandlePropsType> = ({ provided }) => {
  const classes = useGroupsStyles();

  return (
    <div {...provided.dragHandleProps} className={classes.handle}>
      <Icon>{iconNames.drag_indicator}</Icon>
    </div>
  );
};

export default DragHandle;
