// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Switch } from '@mui/material';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { CaseTypeManagementContext } from '../../../../../../CaseTypeManagement.context';
import { PhaseType } from '../../../../PhasesTab.types';

type ModeSelectorPropsType = { phase: PhaseType };

const ModeSelector: React.ElementType<ModeSelectorPropsType> = ({ phase }) => {
  const [t] = useTranslation('caseTypeManagement');
  const { showGrower, showTitles, setShowTitles } = useContext(
    CaseTypeManagementContext
  );
  const [value, setValue] = useState<boolean>(showTitles);

  const title = showTitles
    ? t('phases.attributes.mode.toggle.showMagicStrings')
    : t('phases.attributes.mode.toggle.showTitles');

  return showGrower && Boolean(phase.groups.length) ? (
    <div>
      <Tooltip title={title}>
        <Switch
          value={value}
          onChange={() => {
            setValue(!value);
            setShowTitles(!value);
          }}
        />
      </Tooltip>
    </div>
  ) : null;
};

export default ModeSelector;
