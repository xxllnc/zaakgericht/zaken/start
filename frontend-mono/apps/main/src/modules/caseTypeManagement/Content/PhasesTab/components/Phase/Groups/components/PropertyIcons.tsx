// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint react/no-danger: 0 */

import React from 'react';
// @ts-ignore
import * as DOMPurify from 'dompurify';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { FieldType, GroupType } from '../../../../PhasesTab.types';
import { useGroupsStyles } from '../Groups.styles';
import { useUrl } from '../../../../../../CaseTypeManagement.library';
import { useCaseTypeQuery } from '../../../../../../CaseTypeManagement.requests';
import { getShowExternalDescription } from '../Groups.library';
import { CaseTypeType } from '../../../../../../CaseTypeManagement.types';

const stripTagsRegex = /<\/?[^>]+(>|$)/g;

const descriptionHasUserInput = (description: string) =>
  Boolean(description) &&
  Boolean(description.replace(stripTagsRegex, '').replace(/\n/g, ''));

type PropertyIconsPropsType = {
  field: FieldType | GroupType;
};

/* eslint complexity: [2, 14] */
const PropertyIcons: React.ComponentType<PropertyIconsPropsType> = ({
  field,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const { data: caseType } = useCaseTypeQuery();
  const { phaseNumber } = useUrl();

  let icons = [];

  if (field.type === 'group') {
    const firstDescription = DOMPurify.sanitize(field.description);

    icons = [
      {
        property: 'publishOnPip',
        icon: iconNames.eye,
        show: field.publishOnPip,
        tooltip: null,
      },
      {},
      {},
      {},
      {
        icon: iconNames.help_outline,
        tooltip: <div dangerouslySetInnerHTML={{ __html: firstDescription }} />,
        show: Boolean(firstDescription),
      },
      {},
    ];
  } else if (field.type === 'textblock') {
    const firstDescription = DOMPurify.sanitize(field.description);

    icons = [
      {
        property: 'publishOnPip',
        icon: iconNames.eye,
        show: field.publishOnPip,
      },
      {},
      {},
      {},
      {
        icon: iconNames.help_outline,
        tooltip: <div dangerouslySetInnerHTML={{ __html: firstDescription }} />,
        show: Boolean(firstDescription),
      },
      {},
    ];
  } else {
    const firstDescription = DOMPurify.sanitize(field.descriptionInternal);
    const firstDescriptionHasContent =
      descriptionHasUserInput(firstDescription);
    const secondDescription = DOMPurify.sanitize(field.descriptionExternal);
    const secondDescriptionHasContent =
      descriptionHasUserInput(secondDescription);

    icons = [
      {
        property: 'permanentHidden',
        icon: iconNames.visibility_off,
        show: field.permanentHidden,
      },
      {
        property: 'referential',
        icon: iconNames.move_down,
        show: field.referential,
      },
      {
        property: 'caseAddress',
        icon: iconNames.room,
        show: field.caseAddress,
      },
      {
        property: 'publishOnPip',
        icon: iconNames.eye,
        show: field.publishOnPip,
      },
      {
        property: 'editOnPip',
        icon: iconNames.edit,
        show: field.publishOnPip && field.editOnPip,
      },
      {
        property: 'mandatory',
        icon: iconNames.star,
        show: field.mandatory,
      },
      {
        icon: iconNames.label,
        tooltip: field.title,
        show: Boolean(field.title),
      },
      {
        icon: iconNames.help_outline,
        tooltip: <div dangerouslySetInnerHTML={{ __html: firstDescription }} />,
        show: firstDescriptionHasContent,
      },
      {
        icon: iconNames.help_outline,
        tooltip: (
          <div dangerouslySetInnerHTML={{ __html: secondDescription }} />
        ),
        show:
          secondDescriptionHasContent &&
          getShowExternalDescription(
            caseType as CaseTypeType,
            phaseNumber,
            field
          ),
      },
    ];
  }

  return (
    <div className={classes.icons}>
      {icons.map(({ property, tooltip, icon, show }, index) => (
        <div key={`${field.id}-${index}`} className={classes.icon}>
          {show && icon && (
            <Tooltip
              title={tooltip || t(`phases.attributes.labels.${property}`)}
            >
              <Icon size="extraSmall">{icon}</Icon>
            </Tooltip>
          )}
        </div>
      ))}
    </div>
  );
};

export default PropertyIcons;
