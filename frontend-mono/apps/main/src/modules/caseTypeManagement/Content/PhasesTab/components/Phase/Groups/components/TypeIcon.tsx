// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useGroupsStyles } from '../Groups.styles';
import { AttributeType } from '../../../../PhasesTab.types';

type TypeIconPropsType = {
  field: AttributeType;
};

const TypeIcon: React.ElementType<TypeIconPropsType> = ({ field }) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const { type, relationshipType } = field;

  return (
    <div className={classes.typeIcon}>
      {['option', 'select', 'checkbox'].includes(type) && (
        <Tooltip title={t(`common:attributeTypes.${type}`)}>
          <Icon size="extraSmall" color="inherit">
            {iconNames.task_alt}
          </Icon>
        </Tooltip>
      )}
      {type === 'file' && (
        <Tooltip title={t(`common:attributeTypes.${type}`)}>
          <Icon size="extraSmall" color="inherit">
            {iconNames.insert_drive_file_outlined}
          </Icon>
        </Tooltip>
      )}
      {relationshipType === 'subject' && (
        <Tooltip
          title={[
            t(`common:attributeTypes.${type}`),
            t(`common:attributeRelationshipTypes.${relationshipType}`),
          ].join(' - ')}
        >
          <Icon size="extraSmall" color="inherit">
            {iconNames.person_outline}
          </Icon>
        </Tooltip>
      )}
      {relationshipType === 'custom_object' && (
        <Tooltip
          title={[
            t(`common:attributeTypes.${type}`),
            t(`common:attributeRelationshipTypes.${relationshipType}`),
          ].join(' - ')}
        >
          <Icon size="extraSmall" color="inherit">
            {iconNames.layers_outlined}
          </Icon>
        </Tooltip>
      )}
    </div>
  );
};

export default TypeIcon;
