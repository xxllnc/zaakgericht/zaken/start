// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  PhaseType,
  UpdatePhaseType,
  GroupType,
  AttributeType,
} from '../../../../PhasesTab.types';
import {
  useCaseTypeQuery,
  useMapLayersQuery,
  useSubjectRolesQuery,
} from '../../../../../../CaseTypeManagement.requests';
import { CaseTypeType } from '../../../../../../CaseTypeManagement.types';
import { editAttribute } from '../Groups.library';
import { useUrl } from '../../../../../../CaseTypeManagement.library';
import {
  getAttributeFormDefinition,
  getAttributeRules,
  getFieldComponents,
} from './definitions';

export const FormWrapper: React.ComponentType<{
  phase: PhaseType;
  attribute: AttributeType;
  onClose: () => void;
  onSubmit: (values: any) => void;
  type: 'add' | 'edit';
}> = ({ phase, attribute, onClose, onSubmit, type }) => {
  const [t] = useTranslation('caseTypeManagement');

  const title = t(`phases.attributes.attribute.${type}`);

  const width = useWidth();
  const isCompact = ['xs', 'sm'].includes(width);

  const { phaseNumber } = useUrl();
  const { data: caseType } = useCaseTypeQuery();
  const { data: subjectRoles } = useSubjectRolesQuery();
  const { data: mapLayers } = useMapLayersQuery();

  const formDefinition = getAttributeFormDefinition(
    t as any,
    caseType as CaseTypeType,
    phase,
    subjectRoles,
    mapLayers,
    attribute
  );
  const rules = getAttributeRules(caseType as CaseTypeType, phaseNumber);
  const fieldComponents = getFieldComponents();

  let {
    fields,
    formik: { values, isValid },
  } = useForm({
    formDefinition,
    rules,
    fieldComponents,
  });

  return (
    <Dialog
      disableBackdropClick={true}
      open={true}
      onClose={onClose}
      scope={'dataStore-filters-dialog'}
      fullWidth={true}
      maxWidth="md"
    >
      <DialogTitle
        elevated={true}
        icon="settings"
        title={title}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div>
          {fields.map(
            ({ FieldComponent, key, suppressLabel, mode, ...rest }) => (
              <FormControlWrapper
                {...rest}
                key={`${rest.name}-formcontrol-wrapper`}
                compact={isCompact}
              >
                <FieldComponent
                  {...rest}
                  t={t}
                  label={suppressLabel ? '' : rest.label}
                />
              </FormControlWrapper>
            )
          )}
        </div>
      </DialogContent>
      <Divider />
      <DialogActions>
        {createDialogActions(
          [
            {
              text: t('common:dialog.save'),
              onClick: () => {
                onSubmit(values as any);
                onClose();
              },
              disabled: !isValid,
            },
            {
              text: t('common:dialog.cancel'),
              onClick: onClose,
            },
          ],
          `${type}-attribute-dialog`
        )}
      </DialogActions>
    </Dialog>
  );
};

type StateType = {
  type: 'add' | 'edit';
  group: GroupType;
  field: AttributeType;
} | null;

export type OpenAttributeDialogType = (state: StateType) => void;

export const useAttributeDialog = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType
) => {
  const [attributeState, setAttributeState] = useState<StateType>(null);

  const onClose = () => setAttributeState(null);

  if (!attributeState)
    return {
      groupDialog: null,
      openAttributeDialog: setAttributeState,
    };

  const { type, group, field } = attributeState;

  const edit = (values: any) => {
    editAttribute(values, phase, updatePhase, group, field);
  };

  const onSubmit = edit;

  const attributeDialog = (
    <FormWrapper
      phase={phase}
      attribute={field}
      onClose={onClose}
      onSubmit={onSubmit}
      type={type}
    />
  );

  return {
    attributeDialog,
    openAttributeDialog: setAttributeState,
  };
};
