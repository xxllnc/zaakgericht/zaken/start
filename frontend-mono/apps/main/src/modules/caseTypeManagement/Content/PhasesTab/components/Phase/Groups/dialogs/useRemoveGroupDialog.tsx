// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { RADIO_GROUP } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  GroupType,
  PhaseType,
  UpdatePhaseType,
} from '../../../../PhasesTab.types';
import { removeGroupExclusive, removeGroupInclusive } from '../Groups.library';

type StateType = GroupType | null;

export type OpenRemoveGroupDialogType = (state: StateType) => void;

export const useRemoveGroupDialog = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType
) => {
  const [t] = useTranslation('caseTypeManagement');
  const translation_base = 'phases.attributes.dialogs.group.remove';

  const [group, setGroup] = useState<StateType>(null);
  const open = Boolean(group);
  const onClose = () => setGroup(null);

  const values = ['with', 'without'];
  const choices = values.map(value => ({
    label: t(`${translation_base}.${value}`),
    value,
  }));

  const onSubmit = ({ clusivity }: any) => {
    if (!group) return;

    const removeGroup =
      clusivity === 'with' ? removeGroupInclusive : removeGroupExclusive;

    removeGroup(phase, updatePhase, group);

    onClose();
  };

  const isOnlyGroup = phase.groups.length === 1;

  const removeGroupDialog = open ? (
    <FormDialog
      title={t(`${translation_base}.title`)}
      icon="delete"
      open={open}
      onClose={onClose}
      formDefinition={[
        {
          name: 'clusivity',
          type: RADIO_GROUP,
          value: isOnlyGroup ? values[0] : values[1],
          label: t(`${translation_base}.label`),
          choices,
          nestedValue: true,
          disabled: isOnlyGroup,
        },
      ]}
      saveLabel={t('common:verbs.delete')}
      onSubmit={onSubmit}
    />
  ) : null;

  return {
    removeGroupDialog,
    openRemoveGroupDialog: setGroup,
  };
};
