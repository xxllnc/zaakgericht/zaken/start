// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  GroupType,
  PhaseType,
  UpdatePhaseType,
  TextblockType,
} from '../../../../PhasesTab.types';
import { addTextblock, editTextblock } from '../Groups.library';
import { getTextblockFormDefinition } from './definitions';

type StateType = {
  type: 'add' | 'edit';
  group: GroupType;
  textblock?: TextblockType;
} | null;

export type OpenTextblockDialogType = (state: StateType) => void;

/* eslint complexity: [2, 8] */
export const useTextblockDialog = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType
) => {
  const [t] = useTranslation('caseTypeManagement');

  const [textblockState, setTextblockState] = useState<StateType>(null);

  const onClose = () => setTextblockState(null);

  if (!textblockState)
    return {
      groupDialog: null,
      openTextblockDialog: setTextblockState,
    };

  const { type, group, textblock } = textblockState;

  const formDefinition = getTextblockFormDefinition(t as any, textblock);
  const onSubmit = (values: any) => {
    if (type === 'edit' && textblock) {
      editTextblock(values, phase, updatePhase, group, textblock);
    } else {
      addTextblock(values, phase, updatePhase, group);
    }
    onClose();
  };

  const textblockDialog = (
    <FormDialog
      title={t(`phases.attributes.textblock.${type}`)}
      icon={type}
      open={true}
      onClose={onClose}
      formDefinition={formDefinition}
      onSubmit={onSubmit}
    />
  );

  return {
    textblockDialog,
    openTextblockDialog: setTextblockState,
  };
};
