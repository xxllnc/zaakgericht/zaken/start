// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAuthorizationListStyles = makeStyles(
  ({ typography }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
    },
    addButtonWrapper: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: 'none',
    },
    addButton: {
      ...typography.body2,
      alignItems: 'center',
      justifyContent: 'center',
      background: 'transparent',
      border: 'none',
      display: 'flex',
      '& > span': {
        marginLeft: 10,
      },
      cursor: 'pointer',
    },
    authItem: {
      display: 'flex',
      alignItems: 'center',
      gap: 10,
    },
    fields: {
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      flex: 1,
      height: 42,
    },
  })
);
