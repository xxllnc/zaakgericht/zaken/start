// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  DEPARTMENT_FINDER,
  ROLE_FINDER,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  setDisabled,
  setEnabled,
  hasValue,
  transferDataAsConfig,
  setFieldValue,
} from '@zaaksysteem/common/src/components/form/rules';

export const getFormDefinition = (t: i18next.TFunction) => [
  {
    name: 'department',
    type: DEPARTMENT_FINDER,
    value: null,
    placeholder: t('phases.attributes.attribute.auths.department.placeholder'),
  },
  {
    name: 'role',
    type: ROLE_FINDER,
    value: null,
    placeholder: t('phases.attributes.attribute.auths.role.placeholder'),
  },
];

export const getRules = () => [
  new Rule()
    .when(() => true)
    .then(transferDataAsConfig('department', 'role', 'parentRoleUuid')),
  new Rule()
    .when('department', hasValue)
    .then(setEnabled(['role']))
    .else(setDisabled(['role']))
    .and(setFieldValue('role', null as any)),
];
