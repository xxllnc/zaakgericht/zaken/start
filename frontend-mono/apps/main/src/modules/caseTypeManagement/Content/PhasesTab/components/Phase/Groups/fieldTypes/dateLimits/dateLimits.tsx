// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { DateLimitsType } from '../../../../../PhasesTab.types';
import { useDateLimitsStyles } from './dateLimits.styles';
import DateLimit from './dateLimit';

export const DATE_LIMITS = 'DateLimit';

export const DateLimits: React.ComponentType<
  FormRendererFormField<any, any, DateLimitsType>
> = props => {
  const classes = useDateLimitsStyles();

  return (
    <div className={classes.wrapper}>
      <DateLimit {...props} namespace="start" value={props.value.start} />
      <DateLimit {...props} namespace="end" value={props.value.end} />
    </div>
  );
};
