// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { useSubForm } from '@zaaksysteem/common/src/components/form/hooks/useSubForm';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { ObjectMappingType } from '../../../../../PhasesTab.types';
import { getFormDefinition } from './getFormDefinition';
import { useObjectMappingStyles } from './ObjectMapping.styles';

const MappingForm: React.ComponentType<
  FormFieldPropsType<any, any, ObjectMappingType>
> = ({ name, value, ...props }) => {
  const classes = useObjectMappingStyles();

  const formDefinition = getFormDefinition(value);

  const { fields } = useSubForm({
    namespace: name,
    formDefinition,
    ...props,
  });

  return (
    <div className={classes.mapItem}>
      <div className={classes.fields}>
        {fields.map(
          ({ FieldComponent, key, suppressLabel, mode, ...fieldProps }) => (
            <FormControlWrapper
              {...fieldProps}
              key={`${fieldProps.name}-formcontrol-wrapper`}
            >
              <FieldComponent {...fieldProps} />
            </FormControlWrapper>
          )
        )}
      </div>
    </div>
  );
};

export default MappingForm;
