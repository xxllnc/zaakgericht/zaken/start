// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';
import {
  FieldType,
  GroupType,
  PhaseType,
  UpdatePhaseType,
  UpdateResultsType,
} from '../../../PhasesTab.types';
import { useGroupsStyles } from './Groups.styles';
import { AttributeChoiceType } from './components/AttributeSearch';
import ModeSelector from './components/ModeSelector';
import Groups from './Groups';
import Results from './Results';

export type AddAttributeType = (
  group: GroupType,
  attribute: AttributeChoiceType
) => void;

export type RemoveFieldType = (group: GroupType, field: FieldType) => void;

type GroupsPropsType = {
  phase: PhaseType;
  updatePhase: UpdatePhaseType;
};

const GroupsWrapper: React.ComponentType<GroupsPropsType> = ({
  phase,
  updatePhase,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const updateResults: UpdateResultsType = results => {
    updatePhase({ ...phase, results });
  };

  return (
    <>
      <div className={classes.header}>
        <Typography variant="h3">{t('phases.attributes.title')}</Typography>
        <ModeSelector phase={phase} />
      </div>

      <Groups phase={phase} updatePhase={updatePhase} />

      {phase.results && (
        <>
          <div className={classes.header}>
            <Typography variant="h3">{t('phases.results.title')}</Typography>
          </div>

          <Results results={phase.results} updateResults={updateResults} />
        </>
      )}
    </>
  );
};

export default GroupsWrapper;
