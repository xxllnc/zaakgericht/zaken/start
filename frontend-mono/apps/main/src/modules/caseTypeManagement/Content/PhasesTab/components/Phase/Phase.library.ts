// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SideBarType } from '../../PhasesTab.types';

export const sidebarTypes: SideBarType[] = ['actions', 'tasks'];
