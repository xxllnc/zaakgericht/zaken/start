// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { sideBarWidth } from './SideBar/SideBar.styles';

export const usePhaseStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexGrow: 1,
      justifyContent: 'space-between',
      overflowY: 'auto',
    },
    contentStacked: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      minWidth: 250,
      '&>:nth-of-type(1)': {
        borderBottom: `1px solid ${greyscale.darker}`,
      },
    },
    contentSideBySide: {
      display: 'flex',
      flexDirection: 'row',
      '&>:nth-of-type(1)': {
        borderRight: `1px solid ${greyscale.darker}`,
      },
      '&>div': {
        overflow: 'auto',
      },
    },
    panel: {
      flexGrow: 1,
      minWidth: '50%',
      paddingBottom: 100,
    },
    sidePanel: {
      width: sideBarWidth,
    },
  })
);
