// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import { PhaseType, UpdatePhaseType } from '../../PhasesTab.types';
import { CaseTypeManagementContext } from '../../../../CaseTypeManagement.context';
import { usePhaseStyles } from './Phase.styles';
import SideBar from './SideBar';
import Groups from './Groups';
import Rules from './Rules/Rules';

type PhasePropsType = {
  phase: PhaseType;
  updatePhase: UpdatePhaseType;
};

const Phase: React.ComponentType<PhasePropsType> = ({ phase, updatePhase }) => {
  const classes = usePhaseStyles();

  const { isCompact, phasesStackedMode } = useContext(
    CaseTypeManagementContext
  );

  return (
    <div className={classes.wrapper}>
      <PanelLayout sx={{ minWidth: 1 }}>
        <Panel
          className={
            isCompact || phasesStackedMode
              ? classes.contentStacked
              : classes.contentSideBySide
          }
        >
          <div className={classes.panel}>
            <Groups phase={phase} updatePhase={updatePhase} />
          </div>
          <div className={classes.panel}>
            <Rules phase={phase} updatePhase={updatePhase} />
          </div>
        </Panel>
        <Panel type="side" className={classes.sidePanel}>
          <SideBar phase={phase} updatePhase={updatePhase} />
        </Panel>
      </PanelLayout>
    </div>
  );
};

export default Phase;
