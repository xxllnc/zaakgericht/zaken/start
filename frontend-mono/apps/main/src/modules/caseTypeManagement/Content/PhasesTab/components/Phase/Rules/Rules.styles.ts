// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useRulesStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexGrow: 1,
      justifyContent: 'space-between',
      overflowY: 'auto',
    },
    header: {
      display: 'flex',
      padding: 10,
    },
  })
);
