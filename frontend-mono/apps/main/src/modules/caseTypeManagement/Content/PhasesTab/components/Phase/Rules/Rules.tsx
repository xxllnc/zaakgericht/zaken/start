// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';
import {
  FieldType,
  GroupType,
  PhaseType,
  UpdatePhaseType,
} from '../../../PhasesTab.types';
import { useRulesStyles } from './Rules.styles';

export type RemoveFieldType = (group: GroupType, field: FieldType) => void;

type RulesPropsType = {
  phase: PhaseType;
  updatePhase: UpdatePhaseType;
};

const Rules: React.ComponentType<RulesPropsType> = ({ phase, updatePhase }) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useRulesStyles();

  return (
    <>
      <div className={classes.header}>
        <Typography variant="h3">{t('phases.rules.title')}</Typography>
      </div>
      <div>rules</div>
    </>
  );
};

export default Rules;
