// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useActionsStyles } from './Actions.styles';

type ActionsPropsType = {};

const Actions: React.ComponentType<ActionsPropsType> = () => {
  const classes = useActionsStyles();

  return <div className={classes.wrapper}>actions</div>;
};

export default Actions;
