// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const sideBarWidth = 300;

export const useSideBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      width: sideBarWidth,
    },
    header: {
      borderBottom: `1px solid ${greyscale.darker}`,
    },
    content: {
      flexGrow: 1,
      overflowY: 'auto',
    },
  })
);
