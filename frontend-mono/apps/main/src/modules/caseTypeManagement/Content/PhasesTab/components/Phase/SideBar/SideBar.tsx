// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { TopMenu } from '@mintlab/ui/App/Zaaksysteem/TopMenu';
import { PhaseType } from '../../../PhasesTab.types';
import { useUrl } from '../../../../../CaseTypeManagement.library';
import Actions from './Actions/Actions';
import Tasks from './Tasks/Tasks';
import { useSideBarStyles } from './SideBar.styles';
import { useNavigationItems } from './SideBar.libary';

export type SideBarPropsType = {
  phase: PhaseType;
  updatePhase: (components: any) => void;
};

export const SideBar: React.ComponentType<SideBarPropsType> = ({
  phase,
  updatePhase,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useSideBarStyles();
  const { sideBarType } = useUrl();

  const navigationItems = useNavigationItems(t as any, phase, sideBarType);

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <TopMenu items={navigationItems} folded={false} fullWidth={true} />
      </div>
      <div className={classes.content}>
        {sideBarType === 'actions' ? (
          <Actions />
        ) : (
          <Tasks phase={phase} updatePhase={updatePhase} />
        )}
      </div>
    </div>
  );
};

export default SideBar;
