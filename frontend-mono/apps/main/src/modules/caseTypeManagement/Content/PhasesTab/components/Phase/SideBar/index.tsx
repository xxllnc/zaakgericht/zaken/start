// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { CaseTypeManagementContext } from '../../../../../CaseTypeManagement.context';
import { useUrl } from '../../../../../CaseTypeManagement.library';
import { sidebarTypes } from '../Phase.library';
import SideBar, { SideBarPropsType } from './SideBar';

const SideBarWrapper: React.ComponentType<SideBarPropsType> = props => {
  const { phaseNumber } = useUrl();
  const { defaultPhaseSideBar } = useContext(CaseTypeManagementContext);
  const phaseSideBar =
    phaseNumber === '1' ? sidebarTypes[0] : defaultPhaseSideBar;

  return (
    <Routes>
      <Route path="" element={<Navigate to={phaseSideBar} replace={true} />} />
      <Route path={':sideBarType/*'} element={<SideBar {...props} />} />
    </Routes>
  );
};

export default SideBarWrapper;
