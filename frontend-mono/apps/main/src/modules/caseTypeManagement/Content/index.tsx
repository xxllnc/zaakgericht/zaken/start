// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useUrl } from '../CaseTypeManagement.library';
import FormTab from './FormTab/FormTab';
import PhasesTab from './PhasesTab/PhasesTab';
import AuthTab from './AuthTab/AuthTab';
import ChildrenTab from './ChildrenTab/ChildrenTab';

const Content: React.ComponentType = props => {
  const { tab } = useUrl();

  if (tab === 'phases') return <PhasesTab />;
  if (tab === 'authorization') return <AuthTab />;
  if (tab === 'children') return <ChildrenTab />;

  return <FormTab />;
};

export default Content;
