// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';

type NavigationDialogPropsType = {
  valid: boolean;
  onConfirm: () => void;
  onClose: (proceed?: boolean) => void;
  open: boolean;
};

const NavigationDialog: React.ComponentType<NavigationDialogPropsType> = ({
  valid,
  onConfirm,
  onClose,
  open,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const dialogEl = useRef();
  const close = () => onClose();

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={close}
      scope={'navigate-casetype-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon="save"
        title={t('navigation.title')}
        onCloseClick={close}
      />
      <DialogContent>
        {valid ? t('navigation.content') : t('navigation.contentInvalid')}
      </DialogContent>
      <Divider />
      <DialogActions>
        {createDialogActions(
          [
            {
              text: t('navigation.continue'),
              onClick: () => onClose(true),
            },
            {
              text: t('navigation.save'),
              onClick: onConfirm,
              disabled: !valid,
            },
            {
              text: t('common:verbs.cancel'),
              onClick: close,
            },
          ],
          'navigate-casetype-dialog'
        )}
      </DialogActions>
    </Dialog>
  );
};

export default NavigationDialog;
