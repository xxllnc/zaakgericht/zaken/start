// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { CaseTypeType, TabType } from '../../CaseTypeManagement.types';
import { TabStatesType } from '../SaveDialog/SaveDialog.types';
import { useUpdateCaseType } from '../../CaseTypeManagement.library';
import { getDefinition } from './ResetDialog.formDefinition';

type ResetDialogPropsType = {
  caseType: CaseTypeType;
  initialCaseType: CaseTypeType;
  tabStates: TabStatesType;
  onClose: () => void;
  open: boolean;
};

const ResetDialog: React.ComponentType<ResetDialogPropsType> = ({
  caseType,
  initialCaseType,
  tabStates,
  onClose,
  open,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const dialogEl = useRef();
  const close = () => onClose();

  const updateCaseType = useUpdateCaseType();

  const changedTabs = Object.entries(tabStates).reduce<TabType[]>(
    (acc, [key, { changed }]) => {
      return changed ? [...acc, key as TabType] : acc;
    },
    []
  );

  const formDefinition = getDefinition(t as any, changedTabs);
  const form = useForm({
    formDefinition,
    initialValues: { tabs: changedTabs },
  });

  const {
    fields,
    formik: { values },
  } = form;

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={close}
      scope={'reset-casetype-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon="save"
        title={t('reset.title')}
        onCloseClick={close}
      />
      <DialogContent>
        {fields.map(({ FieldComponent, key, ...rest }: any) => {
          return (
            <FormControlWrapper
              {...rest}
              compact={true}
              key={`${rest.name}-formcontrol-wrapper`}
            >
              <FieldComponent {...rest} t={t} containerRef={dialogEl.current} />
            </FormControlWrapper>
          );
        })}
      </DialogContent>
      <Divider />
      <DialogActions>
        {createDialogActions(
          [
            {
              text: t('reset.title'),
              onClick: () => {
                const resetValues = (values.tabs as TabType[]).reduce(
                  (acc, tabToReset) => ({
                    ...acc,
                    [tabToReset]: initialCaseType[tabToReset],
                  }),
                  {}
                );

                updateCaseType(resetValues);
                onClose();
              },
            },
            {
              text: t('common:verbs.cancel'),
              onClick: close,
            },
          ],
          'reset-casetype-dialog'
        )}
      </DialogActions>
    </Dialog>
  );
};

export default ResetDialog;
