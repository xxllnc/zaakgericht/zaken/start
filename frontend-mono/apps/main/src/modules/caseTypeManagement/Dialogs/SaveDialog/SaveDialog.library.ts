// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import deepEqual from 'fast-deep-equal';
import { tabs } from '../../Menu/Menu.library';
import {
  CheckValidTabsType,
  CheckValidType,
  TabStatesType,
  ValidChecksType,
} from './SaveDialog.types';

const checkValidName: CheckValidType = caseType =>
  Boolean(caseType.common.name);

const checkValidLeadTime: CheckValidType = ({
  common: {
    leadTimeType,
    leadTimeLegal,
    leadTimeLegalDate,
    leadTimeService,
    leadTimeServiceDate,
  },
}) =>
  leadTimeType === 'term'
    ? Boolean(leadTimeLegal.amount) && Boolean(leadTimeService.amount)
    : Boolean(leadTimeLegalDate) && Boolean(leadTimeServiceDate);

const checkValidAuth: CheckValidType = values =>
  values.authorization.every(
    auth => auth.department && auth.role && auth.level
  );

const checkValidChildren: CheckValidType = values =>
  values.children.every(child => child.caseType);

const validChecks: ValidChecksType = {
  common: values => checkValidName(values) && checkValidLeadTime(values),
  documentation: () => true,
  relations: () => true,
  registrationform: () => true,
  webform: () => true,
  case_dossier: () => true,
  api: () => true,
  phases: () => true,
  authorization: values => checkValidAuth(values),
  children: values => checkValidChildren(values),
};

export const getTabStates: CheckValidTabsType = (caseType, initialCaseType) => {
  return tabs.reduce((acc, tab) => {
    return {
      ...acc,
      [tab]: {
        valid: validChecks[tab](caseType),
        changed: !deepEqual(caseType[tab], initialCaseType[tab]),
      },
    };
  }, {}) as TabStatesType;
};
