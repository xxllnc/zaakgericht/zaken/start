// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { CaseTypeType, TabType } from '../../CaseTypeManagement.types';

export type GetDefinitionType = (
  t: i18next.TFunction,
  changedTabs: TabType[]
) => AnyFormDefinitionField[];

export type CheckValidType = (caseType: CaseTypeType) => boolean;

export type TabStatesType = {
  [key in TabType]: {
    valid: boolean;
    changed: boolean;
  };
};

export type ValidChecksType = {
  [key in TabType]: (caseType: CaseTypeType) => boolean;
};

export type CheckValidTabsType = (
  caseType: CaseTypeType,
  initialCaseType: CaseTypeType
) => TabStatesType;
