// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { Box } from '@mui/material';
import { useUrl } from '../CaseTypeManagement.library';
import { TabStatesType } from '../Dialogs/SaveDialog/SaveDialog.types';
import { flip } from '../CaseTypeManagement.styles';
import { menuItemsConfig } from './Menu.library';

type MenuPropsType = {
  tabStates: TabStatesType;
  isCompact: boolean;
};

const Menu: React.ComponentType<MenuPropsType> = ({ tabStates, isCompact }) => {
  const navigate = useNavigate();
  const [t] = useTranslation('caseTypeManagement');
  const { tab } = useUrl();

  const menuItems = Object.entries(menuItemsConfig).map<SideMenuItemType>(
    ([name, icon]) => ({
      selected: tab === name,
      onClick: () => {
        navigate(`../${name}`);
      },
      // @ts-ignore
      invalid: !tabStates[name].valid,
      label: t(`tabs.${name}`),
      icon: (
        <Box sx={name === 'registrationform' ? flip : {}}>
          <Icon>{iconNames[icon]}</Icon>
        </Box>
      ),
    })
  );

  return <SideMenu items={menuItems} folded={isCompact} />;
};

export default Menu;
