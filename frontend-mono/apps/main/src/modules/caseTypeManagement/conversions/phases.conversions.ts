// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  ConvertFormType,
  ConvertAssignmentType,
  ConvertPhasesType,
  RevertPhasesType,
  GroupType,
  RevertAssignmentType,
  RevertFormType,
  RawFieldType,
  RevertGroupType,
  RevertAttributeType,
  RevertFieldType,
  RevertTextblockType,
  ConvertGroupType,
  ConvertTextblockType,
  ConvertAttributeType,
  RawGroupType,
  RawTextblockType,
  RawAttributeType,
  RevertPermissionsType,
  ConvertTaskType,
  RevertTaskType,
  ConvertResultsType,
  RevertResultsType,
  RawPhaseType,
} from '../Content/PhasesTab/PhasesTab.types';

export const defaultPhases: RawPhaseType[] = [
  {
    phase_name: 'Registreren',
    milestone_name: 'Geregistreerd',
    term_in_days: 0,
    custom_fields: [],
    tasks: [],
  },
  {
    phase_name: 'Afhandelen',
    milestone_name: 'Afgehandeld',
    term_in_days: 0,
    custom_fields: [],
    tasks: [],
  },
];

const convertGroup: ConvertGroupType = ({
  attribute_type,
  title,
  help_intern,
  publish_pip,
}) => ({
  id: v4(),
  type: attribute_type,
  title: title,
  description: help_intern,
  publishOnPip: publish_pip,
  fields: [],
});

const convertTextblock: ConvertTextblockType = ({
  attribute_type,
  title,
  help_intern,
  publish_pip,
}) => ({
  id: v4(),
  type: attribute_type,
  title: title,
  description: help_intern,
  publishOnPip: publish_pip,
});

const convertAttribute: ConvertAttributeType = attribute => {
  const authorizations = (attribute.permissions || []).map(
    ({ department_name, department_uuid, role_name, role_uuid }: any) => ({
      department: { label: department_name, value: department_uuid },
      role: { label: role_name, value: role_uuid },
    })
  );
  const createObjectMapping = Object.entries(
    attribute.create_custom_object_attribute_mapping || {}
  ).map(([label, value]) => ({ label, value: value as string }));

  const formattedField = {
    id: v4(),
    uuid: attribute.uuid || '',
    type: attribute.attribute_type,
    name: attribute.attribute_name,
    magicString: attribute.attribute_magic_string || '',

    permanentHidden: attribute.system_attribute,
    referential: attribute.referential,
    title: attribute.title,
    descriptionInternal: attribute.help_intern,
    mandatory: attribute.mandatory,
    publishOnPip: attribute.publish_pip,
    editOnPip: attribute.pip_can_change,
    descriptionExternal: attribute.help_extern,
    skipQueue: attribute.skip_change_approval,
    isMultiple: attribute.is_multiple,
    labelMultiple: attribute.label_multiple,
    caseAddress: Boolean(attribute.use_as_case_address),
    showOnMap: Boolean(attribute.show_on_map),
    mapLayer: attribute.map_wms_layer_id,
    mapLayerAttribute: {
      label: attribute.map_wms_feature_attribute_label,
      value: attribute.map_wms_feature_attribute_id,
    },
    dateLimits: {
      start: attribute.start_date_limitation,
      end: attribute.end_date_limitation,
    },
    authorizations,
    role: attribute.relationship_subject_role,
    createObjectEnabled: attribute.create_custom_object_enabled,
    createObjectLabel: attribute.create_custom_object_label,
    createObjectMapping,
    relationshipType: attribute.relationship_type,
  };

  return formattedField;
};

const convertFields: ConvertFormType = fields => {
  const groups = fields.reduce((acc, field) => {
    if (field.attribute_type === 'group') {
      const group = convertGroup(field as RawGroupType);
      return [...acc, group];
    } else {
      const [lastGroup, ...rest] = acc.reverse();
      const firstGroups = rest.reverse();
      let formattedField;

      if (field.attribute_type === 'textblock') {
        formattedField = convertTextblock(field as RawTextblockType);
      } else {
        formattedField = convertAttribute(field as RawAttributeType);
      }

      const fields = [...lastGroup.fields, formattedField];
      const group = { ...lastGroup, fields };

      return [...firstGroups, group];
    }
  }, [] as GroupType[]);

  return groups;
};

const convertAssignment: ConvertAssignmentType = assignment => {
  if (!assignment) return;

  const { department_name, department_uuid, role_name, role_uuid, enabled } =
    assignment;

  return {
    department: {
      label: department_name,
      value: department_uuid,
    },
    role: {
      label: role_name,
      value: role_uuid,
    },
    enabled,
  };
};

const convertTask: ConvertTaskType = task => ({
  id: v4(),
  value: task,
});

/* eslint complexity: [2, 18] */
const convertResults: ConvertResultsType = (results = []) => {
  return results.map(
    ({
      name,
      result_type,
      is_default,

      selection_list,
      selection_list_source_date,
      selection_list_end_date,
      selection_list_number,

      archival_trigger,
      archival_nomination,
      retention_period,
      archival_nomination_valuation,

      comments,
      processtype_number,
      processtype_name,
      processtype_description,
      processtype_explanation,
      processtype_object,
      procestype_generic,
      origin,
      process_period,
    }) => ({
      id: v4(),
      type: 'result',

      name: name || '',
      resultType: result_type,
      isDefault: is_default || false,

      selectionList: selection_list || '',
      selectionListSourceDate: selection_list_source_date || '',
      selectionListEndDate: selection_list_end_date || '',
      selectionListNumber: selection_list_number || '',

      archivalTrigger: archival_trigger || false,
      archivalNomination: archival_nomination,
      retentionPeriod: `${retention_period}` || null,
      archivalNominationValuation: archival_nomination_valuation,

      comments: comments || '',
      processtypeNumber: processtype_number || '',
      processtypeName: processtype_name || '',
      processtypeDescription: processtype_description || '',
      processtypeExplanation: processtype_explanation || '',
      processtypeObject: processtype_object || '',
      processtypeGeneric: procestype_generic || null,
      origin: origin || null,
      processPeriod: process_period || null,
    })
  );
};

export const convertPhases: ConvertPhasesType = (phases, results) => {
  const phaseList = phases && phases.length ? phases : defaultPhases;

  return phaseList.map(
    (
      {
        phase_name,
        milestone_name,
        term_in_days,
        assignment,
        custom_fields,
        tasks,
      },
      index
    ) => {
      const number = index + 1;
      const lastPhase = number === phases.length;

      return {
        id: v4(),
        name: phase_name,
        number,
        milestone: milestone_name,
        term: term_in_days,
        groups: convertFields(custom_fields || []),
        rules: [],
        documents: [],
        caseTypes: [],
        messages: [],
        contacts: [],
        assignment: convertAssignment(assignment),
        objectActions: [],
        tasks: (tasks || []).map(convertTask),
        results: lastPhase ? convertResults(results) : undefined,
      };
    }
  );
};

const revertGroup: RevertGroupType = ({
  title,
  publishOnPip,
  type,
  description,
}) => ({
  order: 0,
  is_group: true,
  attribute_type: type,
  title: title,
  help_intern: description,
  publish_pip: publishOnPip,
});

export const revertTextblock: RevertTextblockType = ({
  type,
  title,
  description,
  publishOnPip,
}) => ({
  order: 0,
  is_group: false,
  attribute_type: type,
  title,
  help_intern: description || '',
  publish_pip: publishOnPip,
});

export const revertPermissions: RevertPermissionsType = auths => {
  if (!auths) return [];

  return auths
    .filter(({ department, role }) => department.value && role.value)
    .map(({ department, role }) => ({
      department_name: department.label,
      department_uuid: department.value,
      role_name: role.label,
      role_uuid: role.value,
    }));
};

const revertAttribute: RevertAttributeType = field => ({
  is_group: false,
  uuid: field.uuid,
  attribute_type: field.type as any,
  title: field.title,
  attribute_name: field.name,
  attribute_magic_string: field.magicString,
  mandatory: field.mandatory,
  help_intern: field.descriptionInternal,
  help_extern: field.descriptionExternal,
  use_as_case_address: field.caseAddress,
  referential: field.referential,
  publish_pip: field.publishOnPip,
  pip_can_change: field.editOnPip,
  skip_change_approval: field.skipQueue,
  system_attribute: field.permanentHidden,
  permissions: revertPermissions(field.authorizations),
  is_multiple: field.isMultiple,
  label_multiple: field.labelMultiple,
  show_on_map: field.showOnMap,
  map_wms_layer_id: field.mapLayer,
  map_wms_feature_attribute_label: field.mapLayerAttribute?.label,
  map_wms_feature_attribute_id: field.mapLayerAttribute?.value,
  start_date_limitation: field.dateLimits?.start,
  end_date_limitation: field.dateLimits?.end,
  create_custom_object_enabled: field.createObjectEnabled,
  create_custom_object_label: field.createObjectLabel,
  create_custom_object_attribute_mapping: field.createObjectMapping?.reduce(
    (acc: any, mapping: { label: string; value: string }) => {
      acc[mapping.label] = mapping.value;
      return acc;
    },
    {}
  ),
  relationship_type: field.relationshipType,
});

export const revertField: RevertFieldType = field => {
  return field.type === 'textblock'
    ? revertTextblock(field)
    : revertAttribute(field);
};

const revertFields: RevertFormType = groups => {
  const fields = groups.reduce((acc, group) => {
    const revertedGroup = revertGroup(group);
    const revertedFields = group.fields.map(revertField);

    return [...acc, revertedGroup, ...revertedFields] as RawFieldType[];
  }, [] as RawFieldType[]);

  const fieldsWithOrder = fields.map((field, index) => ({
    ...field,
    order: index + 1,
  }));

  return fieldsWithOrder;
};

const revertAssignment: RevertAssignmentType = assignment => {
  if (!assignment) return;

  const { department, role, enabled } = assignment;

  return {
    department_name: department.label,
    department_uuid: department.value,
    role_name: role.label,
    role_uuid: role.value,
    enabled,
  };
};

const revertTask: RevertTaskType = ({ value }) => value;

export const revertPhases: RevertPhasesType = phases => {
  const revertedPhases = phases.map(
    ({ name, milestone, term, assignment, groups, tasks }) => ({
      phase_name: name,
      milestone_name: milestone,
      term_in_days: term,
      assignment: revertAssignment(assignment),
      custom_fields: revertFields(groups),
      tasks: tasks.map(revertTask),
    })
  );

  const results = phases.reverse()[0].results;
  const revertedResults = results ? revertResults(results) : null;

  return { phases: revertedPhases, results: revertedResults };
};

const revertResults: RevertResultsType = results => {
  return results.map(
    ({
      name,
      resultType,
      isDefault,

      selectionList,
      selectionListSourceDate,
      selectionListEndDate,
      selectionListNumber,

      archivalTrigger,
      archivalNomination,
      retentionPeriod,
      archivalNominationValuation,

      comments,
      processtypeNumber,
      processtypeName,
      processtypeDescription,
      processtypeExplanation,
      processtypeObject,
      processtypeGeneric,
      origin,
      processPeriod,
    }) => ({
      name,
      result_type: resultType,
      is_default: isDefault,

      selection_list: selectionList,
      selection_list_source_date: selectionListSourceDate || null,
      selection_list_end_date: selectionListEndDate || null,
      selection_list_number: selectionListNumber,

      archival_trigger: archivalTrigger,
      archival_nomination: archivalNomination,
      retention_period: Number(retentionPeriod),
      archival_nomination_valuation: archivalNominationValuation,

      comments,
      processtype_number: processtypeNumber,
      processtype_name: processtypeName,
      processtype_description: processtypeDescription,
      processtype_explanation: processtypeExplanation,
      processtype_object: processtypeObject,
      procestype_generic: processtypeGeneric || undefined,
      origin: origin || undefined,
      process_period: processPeriod || undefined,
    })
  );
};
