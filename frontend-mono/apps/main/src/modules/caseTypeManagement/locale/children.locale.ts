// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  children: {
    title: '$t(tabs.children)',
    description:
      'Instellingen van het moederzaaktype kunnen worden doorgegeven aan de kindzaaktypen door deze te publiceren.',
    save: {
      success: 'De {{count}} kinderen zijn gepubliceerd.',
      button: {
        publish: 'Alle actieve kinderen publiceren',
        noPublishableChildren:
          'Er zijn geen actieve kindzaaktypen om te publiceren en/of er zijn invalide kindzaaktypen',
      },
      dialog: {
        title: 'Kinderen publiceren',
        description:
          'Publiceer onderstaande kindzaaktypen. Wanneer middenfasen aangevinkt zijn wordt gecontroleerd of het kindzaaktype deze middenfasen heeft.',
        warning:
          'Let op: Deze actie kan enkel ongedaan gemaakt worden door vanuit de catalogus de versie van kindzaaktype(n) terug te zetten.',
      },
      inherited: 'Geërfd van zaaktype: {{motherName}}.',
    },
    switch: 'Actief',
    placeholders: {
      caseType: 'Selecteer een zaaktype…',
    },
    icon: {
      publish: 'Publiceren',
      copy: 'Onderdelen kopiëren',
      overwrite: 'Gekopieerde onderdelen overschrijven',
      remove: 'Verwijderen',
    },
    sections: {
      relations: '$t(tabs.relations)',
      registrationform: '$t(tabs.registrationform)',
      webform: '$t(tabs.webform)',
      case_dossier: '$t(tabs.case_dossier)',
      api: '$t(tabs.api)',
      authorization: '$t(tabs.authorization)',
      allocations: 'Fase 1: Toewijzing',
      first_phase: 'Fase 1',
      phaseX: 'Fase {{count}} + toewijzing',
      last_phase: 'Fase {{phaseCount}}',
      results: 'Resultaten',
    },
  },
};
