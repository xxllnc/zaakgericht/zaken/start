// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const headers = {
  common: {
    title: '$t(tabs.common)',
    description: 'Algemene instellingen van dit zaaktype.',
  },
  documentation: {
    title: '$t(tabs.documentation)',
    description:
      'Deze velden zijn ter documentatie en hebben geen invloed op de werking van de zaken.',
  },
  avg: {
    title: 'AVG',
    description:
      'Deze velden zijn ter documentatie en hebben geen invloed op de werking van de zaken.',
  },
  contactRelations: {
    title: 'Relaties met contacten',
    description:
      'Instellingen voor relaties met contacten die een zaak kan hebben.',
  },
  requestorAddress: {
    title: 'Adres van de aanvrager',
    description:
      'Instellingen voor relaties met het adres van de aanvrager die een zaak kan hebben.',
  },
  registrationForm: {
    title: '$t(tabs.registrationform)',
    description: 'Instellingen voor het (interne) registratieformulier.',
  },
  webform: {
    title: '$t(tabs.webform)',
    description: 'Instellingen voor het (externe) webformulier.',
  },
  prices: {
    title: 'Tarieven',
    description:
      'Prijzen voor dit zaaktype. Het bedrag van het webformulier wordt gebruikt voor de internetkassa.',
  },
  case_dossier: {
    title: '$t(tabs.case_dossier)',
    description: 'Instellingen voor het zaakdossier.',
  },
  api: {
    title: '$t(tabs.api)',
    description: 'De instellingen voor API toegang en handelingen.',
  },
  notifications: {
    title: 'Notificaties',
    description:
      'Instellingen voor bij welke gebeurtenis de gebruiker een notificatie dient te ontvangen.',
  },
};

const fields = {
  // common
  name: {
    label: 'Naam',
    tooltip: 'De naam van het zaaktype.',
  },
  identification: {
    label: 'Identificatie',
    tooltip:
      'De unieke code van het zaaktype. Let op, voor sommige zaaktypen is er een landelijke code beschikbaar (zie de catalogus).',
  },
  tags: {
    label: 'Trefwoorden',
    tooltip:
      'De trefwoorden waarop gezocht kan worden om het zaaktype te vinden.',
  },
  description: {
    label: 'Omschrijving of toelichting',
    tooltip:
      'De omschrijving of toelichting wordt bij het resultaat getoond wanneer gezocht wordt op het zaaktype.',
  },
  summaryInternal: {
    label: 'Extra informatie',
    tooltip:
      'Maak gebruik van [[magic_strings]] om informatie van de zaak weer te geven als Extra Informatie. Deze informatie wordt enkel intern weergegeven.',
    placeholder: '[[voorbeeld]] - [[magic_string]]',
  },
  summaryExternal: {
    label: 'Extra informatie - Extern',
    tooltip:
      'Maak gebruik van [[magic_strings]] om informatie van de zaak weer te geven als Extra Informatie. Deze informatie wordt enkel op de PIP weergegeven.',
    placeholder: '[[voorbeeld]] - [[magic_string]]',
  },
  leadTimeType: {
    label: 'Afhandeltermijn',
  },
  leadTimeLegal: {
    label: 'Doorlooptijd wettelijk',
    tooltip:
      'De periode (in kalenderdagen) waarbinnen volgens wet- en regelgeving een zaak van het zaaktype afgerond dient te zijn.',
  },
  leadTimeService: {
    label: 'Doorlooptijd service',
    tooltip:
      'De periode (in kalenderdagen) waarbinnen verwacht wordt dat een zaak van het zaaktype afgerond wordt conform de geldende servicenormen.',
  },
  leadTimeLegalDate: {
    label: 'Vaste einddatum wettelijk',
  },
  leadTimeServiceDate: {
    label: 'Vaste einddatum service',
  },

  // documentation
  processDescription: {
    label: 'Procesbeschrijving',
    placeholder: 'www.voorbeeld.nl',
  },
  initiatorType: {
    label: 'Handeling initiator',
    tooltip:
      "Werkwoord dat hoort bij de handeling die de initiator verricht bij dit zaaktype. Meestal 'aanvragen', 'indienen' of 'melden'.",
  },
  motivation: {
    label: 'Aanleiding',
    tooltip:
      'Een omschrijving van de gebeurtenis die leidt tot het starten van een zaak van dit zaaktype.',
  },
  purpose: {
    label: 'Doel',
    tooltip:
      'Een omschrijving van hetgeen beoogd is te bereiken met een zaak van dit zaaktype.',
  },
  archiveClassificationCode: {
    label: 'Archiefclassificatiecode',
    tooltip:
      'De systematische identificatie van zaakdossiers van dit zaaktype overeenkomstig logisch gestructureerde conventies, methoden en procedureregels.',
  },
  designationOfConfidentiality: {
    label: 'Vertrouwelijkheidsaanduiding',
    tooltip:
      'Aanduiding van de mate waarin zaakdossiers van zaken van dit zaaktype voor de openbaarheid bestemd zijn.',
  },
  responsibleSubject: {
    label: 'Verantwoordelijke',
    tooltip:
      'De (soort) organisatorische eenheid of (functie van) medewerker die verantwoordelijk is voor de uitvoering van zaken van het zaaktype.',
  },
  responsibleRelationship: {
    label: 'Verantwoordingsrelatie',
    tooltip:
      'De relatie tussen zaken van dit zaaktype en de beleidsmatige en/of financiële verantwoording. De zaken die worden uitgevoerd leveren een bijdrage aan / of zijn een invulling van de taken van de organisatie.',
  },
  possibilityForObjectionAndAppeal: {
    label: 'Bezwaar en beroep mogelijk',
  },
  publication: {
    label: 'Publicatie',
    tooltip:
      'Aanduiding of (het starten van) een zaak van dit zaaktype gepubliceerd moet worden.',
  },
  publicationText: {
    label: 'Publicatietekst',
    tooltip: 'De generieke tekst van de publicatie van zaken van dit zaaktype.',
  },
  bag: {
    label: 'BAG',
  },
  lexSilencioPositivo: {
    label: 'Lex silencio positivo',
  },
  mayPostpone: {
    label: 'Opschorten mogelijk',
    tooltip:
      'Aanduiding die aangeeft of zaken van dit zaaktype kunnen worden opgeschort en/of aangehouden.',
  },
  mayExtend: {
    label: 'Verlenging mogelijk',
    tooltip:
      'Aanduiding die aangeeft of de doorlooptijd behandeling van zaken van dit zaaktype kan worden verlengd.',
  },
  extensionPeriod: {
    label: 'Verlengingstermijn in dagen',
    tooltip:
      'De termijn in dagen waarmee de doorlooptijd behandeling van zaken van dit zaaktype kan worden verlengd.',
  },
  adjournPeriod: {
    label: 'Verdagingstermijn in dagen',
    tooltip:
      'De termijn in dagen waarmee de doorlooptijd behandeling van zaken van dit zaaktype kan worden opgeschort.',
  },
  penaltyLaw: {
    label: 'Wet dwangsom',
  },
  wkpbApplies: {
    label: 'WKPB',
  },
  eWebform: {
    label: 'e-Formulier',
    tooltip: 'De URL naar het formulier.',
  },
  legalBasis: {
    label: 'Wettelijke grondslag',
  },
  localBasis: {
    label: 'Lokale grondslag',
  },

  // avg
  gdprEnabled: {
    label: 'Verwerkt persoonsgegevens',
  },
  kind: {
    label: 'Soort persoonsgegevens',
  },
  source: {
    label: 'Bron persoonsgegevens',
  },
  processingType: {
    label: 'Verwerkingstype',
  },
  processForeignCountry: {
    label: 'Doorgifte buitenland',
  },
  processForeignCountryReason: {
    label: 'Toelichting doorgifte buitenland',
  },
  processingLegal: {
    label: 'Grondslag verwerking',
  },
  processingLegalReason: {
    label: 'Toelichting verwerking',
  },

  // relations
  allowedRequestorTypes: {
    label: 'Mogelijke aanvragertypes',
    tooltip:
      'Zaken kunnen geregistreerd worden voor contacten van de hier opgegeven types.',
  },
  presetRequestor: {
    label: 'Vooringevulde aanvrager',
    tooltip:
      'Het hier ingevulde contact wordt vooringevuld bij de zaakregistratie.',
    placeholder: 'Zoek een contact…',
  },
  presetAssignee: {
    label: 'Vooringevulde behandelaar',
    tooltip:
      'De hier ingevulde medewerker wordt vooringevuld bij de zaakregistratie.',
    placeholder: 'Zoek een medewerker…',
  },
  requestorAddressCase: {
    label: 'Gebruiken voor correspondentie',
  },
  requestorAddressCorrespondence: {
    label: 'Gebruiken als zaakadres',
  },
  requestorAddressMap: {
    label: 'Tonen op kaart (GeoJSON)',
  },

  // registration form
  allowAssigningToSelf: {
    label: 'Zaak automatisch in behandeling nemen',
    tooltip:
      'Aan het einde van het Registratieformulier wordt een optie weergegeven om de zaak bij registratie automatisch in behandeling te nemen. Deze optie is dan standaard aangevinkt en kan uitgevinkt worden in geval van uitzonderingen.',
  },
  allowAssigning: {
    label: 'Toewijzing tonen bij zaakintake',
    tooltip:
      'Aan het einde van het registratieformulier wordt een optie weergegeven om de zaak bij registratie aan een andere afdeling/rol toe te wijzen dan de standaard zoals ingesteld op de Fasenpagina van zaaktypebeheer. Deze optie heeft standaard de waarde van zaaktypebeheer en kan gewijzigd worden voor uitzonderingen.',
  },
  showConfidentiality: {
    label: 'Toon vertrouwelijkheid',
    tooltip:
      'Aan het einde van het registratieformulier wordt een optie weergegeven om de zaak bij registratie direct een bepaalde Vertrouwelijkheid te geven: Openbaar, Intern of Vertrouwelijk',
  },
  showContactDetails: {
    label: 'Contactgegevens tonen bij intake',
    tooltip:
      "Er wordt een extra stap aan het registratieformulier toegevoegd waar het 'e-mailadres', 'telefoonummer', en 'mobiel nummer' van de aanvrager worden weergegeven, zodat deze eventueel ingevuld of gewijzigd kunnen worden.",
  },
  allowAddRelations: {
    label: 'Mogelijkheid om relaties toe te voegen bij zaakintake',
    tooltip:
      'Aan het einde van het registratieformulier wordt een optie weergegeven om bij registratie relaties toe te voegen aan de zaak.',
  },

  // webform
  publicConfirmationTitle: {
    label: 'Titel publiek bevestigingsbericht',
    tooltip:
      'De titel die de aanvrager te zien krijgt bij een succesvolle zaakregistratie met het webformulier. Er kan ook gebruik gemaakt worden van magicstrings.',
  },
  publicConfirmationMessage: {
    label: 'Publiek bevestigingsbericht',
    tooltip:
      'Het bericht die de aanvrager te zien krijgt bij een succesvolle zaakregistratie met het webformulier.',
  },
  caseLocationMessage: {
    label: 'Tekst bij adrescontrole',
  },
  pipViewMessage: {
    label: 'PIP verwijzingsbericht',
  },
  enableWebform: {
    label: 'Aanvragen met publiek webformulier',
    tooltip: 'Het zaaktype kan online worden aangevraagd.',
  },
  createDelayed: {
    label: 'Zaak op achtergrond aanmaken',
    tooltip: 'Experimenteel',
  },
  addressCheck: {
    label: 'Adrescontrole',
  },
  reUseCaseData: {
    label: 'Zaakgegevens hergebruiken',
    tooltip:
      'Als de aanvrager eerder een zaak van dit zaaktype heeft aangevraagd wordt in de eerste stap van het registratieformulier of webformulier gevraagd of de gegevens van de vorige zaak hergebruikt moeten worden.',
  },
  enableOnlinePayment: {
    label: 'Internetkassa gebruiken',
    tooltip:
      "Er wordt een extra stap 'betaling' aan het einde van het webformulier toegevoegd. Hier wordt de prijs weergegeven en kan de klant de online betaling met Ogone initiëren. Let op: Wanneer deze optie aanstaat moet er op het tabblad relaties een 'bedrag web' ingevuld zijn, of middels regels een prijs bepaald worden.",
  },
  enableManualPayment: {
    label: 'Anders betalen toestaan',
    tooltip:
      'Bij de betaalstap (indien er gebruik gemaakt wordt van de internetkassa) krijgt de aanvrager een extra optie om niet online te betalen.',
  },
  emailRequired: {
    label: 'E-mailadres verplicht',
    tooltip:
      "Wanneer deze functionaliteit aan staat is de aanvrager verplicht een e-mailadres op te geven in de stap 'contactgegevens' van het webformulier.",
  },
  phoneRequired: {
    label: 'Telefoonnummer verplicht',
    tooltip:
      "Wanneer deze functionaliteit aan staat is de aanvrager verplicht een telefoonnummer op te geven in de stap 'contactgegevens' van het webformulier.",
  },
  mobileRequired: {
    label: 'Mobiel telefoonnummer verplicht',
    tooltip:
      "Wanneer deze functionaliteit aan staat is de aanvrager verplicht een mobiel telefoonnummer op te geven in de stap 'contactgegevens' van het webformulier.",
  },
  disableCaptcha: {
    label: 'Captcha uitschakelen bij vooringevulde aanvrager',
    tooltip:
      'Schakelt de Captcha uit bij anonieme aanvragen via vooringevulde aanvrager.',
  },
  generatePdfEndWebform: {
    label: 'PDF genereren bij einde webformulier',
    tooltip:
      'Wanneer deze functionaliteit aan staat kan de aanvrager aan het einde van het webformulier een PDF van het formulier genereren en downloaden.',
  },

  // prices
  priceWebform: {
    label: 'Webformulier',
    tooltip: 'Dit bedrag wordt gebruikt voor de internetkassa.',
  },
  priceFrontdesk: {
    label: 'Balie',
  },
  pricePhone: {
    label: 'Telefoon',
  },
  priceEmail: {
    label: 'E-mail',
  },
  priceAssignee: {
    label: 'Behandelaar',
  },
  pricePost: {
    label: 'Post',
  },

  // case
  disablePipForRequestor: {
    label: 'Zaak niet op aanvrager PIP publiceren',
    tooltip:
      'De zaken zullen niet op de PIP van de aanvrager te zien zijn. Let op: Deze functionaliteit is gebaseerd op het zaaktypeversie-ID, en niet op het zaaktype-ID. Deze functionaliteit kan niet gebruikt worden om zichtbare zaken onzichtbaar te maken of andersom.',
  },
  lockRegistrationPhase: {
    label: 'Registratiefase vergrendelen',
    tooltip:
      'Deze optie vergendelt de kenmerken in de registratiefase voor gebruikers met behandelrechten.',
  },
  queueCoworkerChanges: {
    label: "Kenmerk- en documentwijzigingen van collega's in wachtrij plaatsen",
    tooltip:
      'Deze optie staat gebruikers toe om wijzigingsvoorstellen op kenmerken te doen.',
  },
  allowExternalTaskAssignement: {
    label:
      'Taken kunnen worden toegewezen aan externe contacten (personen, organisaties)',
    tooltip:
      'Deze optie staat toe om taken toe te wijzen aan externe gebruikers.',
  },
  defaultDocumentFolders: {
    label: 'Standaard documentmappen',
    tooltip:
      'Zaken bevatten op moment van aanmaken de hier opgegeven documentmappen.',
  },
  htmlEmailTemplate: {
    label: 'Berichtopmaak',
    tooltip:
      'Het geselecteerde sjabloon wordt standaard toegepast voor alle e-mails voor zaken.',
    placeholder: 'Selecteer een berichtsjabloon…',
  },

  // api
  apiCanTransition: {
    label: 'Zaaksysteem API mag fase afronden',
  },

  // notifications
  notifyOnNewCase: {
    label: 'Nieuwe zaak',
  },
  notifyOnNewDocument: {
    label: 'Wijzigingen aan documenten',
  },
  notifyOnNewMessage: {
    label: 'Nieuwe communicatie',
  },
  notifyOnExceedTerm: {
    label: 'Termijnoverschrijding',
  },
  notifyOnAllocate: {
    label: 'Toewijzingswijziging',
  },
  notifyOnPhaseTransition: {
    label: 'Faseafhandeling',
  },
  notifyOnTaskChange: {
    label: 'Wijzigingen van taken',
  },
  notifyOnLabelChange: {
    label: 'Wijzigingen van document labels',
  },
  notifyOnSubjectChange: {
    label: 'Wijzigingen van betrokkenen',
  },
};

const choices = {
  allowedRequestorTypes: {
    natuurlijk_persoon: 'Persoon (binnengemeentelijk)',
    natuurlijk_persoon_na: 'Persoon (buitengemeentelijk)',
    niet_natuurlijk_persoon: 'Organisatie',
    medewerker: 'Medewerker',
  },
  leadTimeType: {
    term: 'Doorlooptijd',
    set: 'Vaste eindedatum',
  },
  leadTime: {
    kalenderdagen: 'Kalenderdagen',
    werkdagen: 'Werkdagen',
    weken: 'Weken',
  },
  kind: {
    basic_details: 'Basispersoonsgegevens',
    personal_id_number: 'Burgerservicenummer',
    income: 'Inkomensgegevens',
    race_or_ethniticy: 'Ras of etnische afkomst',
    political_views: 'Politieke opvattingen',
    religion: 'Religieuze of levensbeschouwende overtuiging',
    membership_union: 'Lidmaatschap van een vakbond',
    genetic_or_biometric_data: 'Genetische of biometrische gegevens',
    health: 'Gezondheid',
    sexual_identity: 'Seksuele identiteit en oriëntatie',
    criminal_record: 'Strafrechtelijke veroordelingen en strafbare feiten',
    offspring: 'Gegevens m.b.t. kind(eren)',
  },
  source: {
    registration: 'Registratie',
    sender: 'Afzender',
    partner: 'Ketenpartner',
    public_source: 'Openbare bron',
  },
};

export default {
  form: {
    headers,
    fields,
    choices,
  },
};
