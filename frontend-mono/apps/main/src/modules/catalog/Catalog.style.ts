// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const catalogStyleSheet = () => {
  return {
    contentWrapper: {
      flexGrow: 1,
      height: 'calc(100% - 75px)',
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    tableWrapper: {
      position: 'relative',
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
    },
    iconCell: {
      width: '50px',
    },
    loader: {
      position: 'absolute',
      top: '20px',
      right: '50%',
    },
    card: {
      position: 'relative',
      boxSizing: 'border-box',
      height: '100%',
    },
  };
};
