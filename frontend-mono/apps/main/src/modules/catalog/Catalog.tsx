// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import Box from '@mui/material/Box';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import AddElement from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement';
import {
  getPathToCaseType,
  getPathToCustomObjectType,
  getPathToImport,
  getPathToItem,
  getPathToNewCustomObjectType,
  getPathToObjectType,
} from './library/pathGetters';
import { catalogStyleSheet } from './Catalog.style';
import CatalogTable from './components/Table/CatalogTable';
import DetailView from './components/DetailView/DetailView';
import MoveBar from './components/MoveBar';
import ActionBar from './components/ActionBar/ActionBar';
import { CatalogItemType } from './Catalog.types';
import { useCatalogItemsQuery, useMoveItemsMutation } from './Catalog.library';
import { useEmailTemplateDialog } from './components/Dialogs/EmailTemplate/EmailTemplate';
import { useDocumentTemplateDialog } from './components/Dialogs/DocumentTemplate/DocumentTemplate';
import { useAttributeDialog } from './components/Dialogs/Attribute/Attribute';
import { useCaseTypeDialog } from './components/Dialogs/CaseType/CaseTypeDialog';
import { useFolderDialog } from './components/Dialogs/Folder/Folder';
import { useDeleteDialog } from './components/Dialogs/Delete/Delete';
import { useHistoryDialog } from './components/Dialogs/History/HistoryDialog';
import { useChangeOnlineStatusDialog } from './components/Dialogs/ChangeOnlineStatus/ChangeOnlineStatus';

/* eslint complexity: [2, 20] */
const Catalog = () => {
  const searchTerm = new URLSearchParams(window.location.search).get('query');
  const folderId = /catalog(\/)?$/.test(window.location.pathname)
    ? ''
    : window.location.pathname.split('/').reverse()[0];

  const [t] = useTranslation('catalog');
  const stylesheet = catalogStyleSheet();
  const navigate = useNavigate();
  const [movingFromId, setMovingFromId] = React.useState('');
  const [itemsForMoving, setMoving] = React.useState<CatalogItemType[]>([]);
  const [showDetailView, setShowDetailView] = React.useState(false);
  const toggleDetailView = () => setShowDetailView(!showDetailView);
  const [selectedIds, setSelectedIds] = React.useState<string[]>([]);

  const [counter, setCounter] = React.useState(0);

  const [addElementOpened, setAddElementOpened] = React.useState(false);

  const { openAttributeDialog, attributeDialog } = useAttributeDialog();
  const { openCaseTypeDialog, caseTypeDialog } = useCaseTypeDialog(folderId);
  const { openEmailTemplateDialog, emailTemplateDialog } =
    useEmailTemplateDialog(folderId);
  const { openDocumentTemplateDialog, documentTemplateDialog } =
    useDocumentTemplateDialog(folderId);
  const folderDialog = useFolderDialog(folderId);
  const { openDeleteDialog, deleteDialog } = useDeleteDialog();
  const { openHistoryDialog, historyDialog } = useHistoryDialog();
  const { openChangeOnlineStatusDialog, changeOnlineStatusDialog } =
    useChangeOnlineStatusDialog();

  const { mutate: moveItems, isLoading: movingItems } = useMoveItemsMutation(
    () => {
      setMoving([]);
      setMovingFromId('');
    }
  );

  const refreshToLoadNewHistory = () => {
    setCounter(counter + 1);
    setSelectedIds([]);
  };

  const { data, isLoading, fetchNextPage, isFetchingNextPage } =
    useCatalogItemsQuery(searchTerm || '', folderId);

  const catalogItems = data ? data.pages.map(page => page[0]).flat() : [];
  const breadcrumbs = data ? data.pages.map(page => page[1])[0] : [];
  const topLevel = !breadcrumbs.length;
  const nextPage = (data?.pages.length || 0) + 1;
  const selected = catalogItems.filter(it => selectedIds.includes(it.id));
  const selectedItemId = selectedIds[0];

  const translatedBreadcrumbs = [
    {
      label: t('title'),
      path: '/main/catalog',
      id: 'root',
    },
    ...breadcrumbs.map(br => ({
      ...br,
      label: t(br.label),
    })),
  ].map(br => ({
    ...br,
    onItemClick: preventDefaultAndCall(() => {
      window.history.pushState('', br.label, br.path);
      refreshToLoadNewHistory();
    }),
  }));

  const rows = catalogItems.map(item => ({
    ...item,
    path: getPathToItem(item.id, item.type),
    isNavigable: [
      'folder',
      'case_type',
      'object_type',
      'custom_object_type',
    ].includes(item.type),
    icon: `entityType.${item.type}`,
    id: item.id,
    selected: Boolean(selected.find(it => it.id === item.id)),
    beingMoved: itemsForMoving.some(it => it.id === item.id),
  }));

  const goToEditOrCreate = {
    caseType: (id?: string) => {
      navigate(
        getPathToCaseType(id || selectedItemId, '/bewerken', true, {
          folder_uuid: folderId,
        })
      );
    },
    objectType: (id?: string) => {
      navigate(
        getPathToObjectType(id || selectedItemId, '/bewerken', true, {
          folder_uuid: folderId,
        })
      );
    },
  };

  return (
    <Sheet>
      <TopbarTitle breadcrumbs={translatedBreadcrumbs} />
      <ActionBar
        searchTerm={searchTerm || ''}
        refresh={refreshToLoadNewHistory}
        exportCaseType={() => {
          window.open(
            window.location.origin +
              `/beheer/zaaktypen/${selectedItemId}/export`,
            '_blank'
          );
        }}
        changeOnlineStatus={() =>
          openChangeOnlineStatusDialog([selectedItemId, selected[0].active])
        }
        editCaseType={() => goToEditOrCreate.caseType(selectedItemId)}
        editObjectType={() => goToEditOrCreate.objectType(selectedItemId)}
        editCustomObjectType={() => {
          navigate(getPathToCustomObjectType(selectedItemId));
        }}
        editAttribute={() => openAttributeDialog({ id: selectedItemId })}
        editDocumentTemplate={() =>
          openDocumentTemplateDialog({
            id: selectedItemId,
          })
        }
        editEmailTemplate={() =>
          openEmailTemplateDialog({ id: selectedItemId })
        }
        editFolder={() =>
          folderDialog.open({
            id: selectedItemId,
            name: selected[0].name,
          })
        }
        duplicateCaseType={() => {
          navigate(
            getPathToCaseType(selectedItemId, '/clone', true, {
              folder_uuid: folderId,
            })
          );
          // when CTM API supports everything, replace with this:
          // openCaseTypeDialog({ id: selectedItemId });
        }}
        deleteItem={() => {
          selected[0] && openDeleteDialog(selected[0]);
        }}
        initHistory={() => openHistoryDialog(selectedItemId)}
        selectedItems={selected}
        moveItems={itemsForMoving}
        toggleDetailView={toggleDetailView}
        setMoveItems={items => {
          setSelectedIds([]);
          setMovingFromId(folderId);
          setMoving(items);
        }}
        setAddElementOpened={setAddElementOpened}
      />
      <Box sx={stylesheet.contentWrapper}>
        <Box sx={stylesheet.tableWrapper}>
          <CatalogTable
            rows={rows}
            onRowNavigate={(path, navigable, event) => {
              if (path && navigable) {
                navigate(path);
                event.preventDefault();
              }
            }}
            toggleItem={id => {
              const target = catalogItems.find(item => item.id === id);
              const update =
                target === undefined
                  ? selected
                  : selected.find(item => item.id === target.id)
                    ? selected.filter(item => item.id !== target.id)
                    : [...selected, target];
              setSelectedIds(update.map(it => it.id));
            }}
            loadMore={() => {
              const lastPageFull =
                (data?.pages[data?.pages.length - 1] || [[]])[0].length === 50;

              const alreadyFetchedNextPage = data?.pages.length === nextPage;

              return !data || (lastPageFull && !alreadyFetchedNextPage)
                ? fetchNextPage({ pageParam: nextPage })
                : Promise.resolve([]);
            }}
            loadingItems={isLoading || isFetchingNextPage}
          />
          {Boolean(itemsForMoving.length) && (
            <MoveBar
              numToMove={itemsForMoving.length}
              moveAction={() => {
                movingFromId !== folderId &&
                  moveItems({ items: itemsForMoving, folderId });
              }}
              cancelAction={() => setMoving([])}
              disabled={movingItems || Boolean(searchTerm)}
            />
          )}
        </Box>
        {showDetailView && (
          <DetailView
            currentFolderName={breadcrumbs[breadcrumbs.length - 1]?.label}
            toggleDetailView={toggleDetailView}
            items={catalogItems}
            selected={selected}
          />
        )}
      </Box>
      {attributeDialog}
      {caseTypeDialog}
      {emailTemplateDialog}
      {documentTemplateDialog}
      {folderDialog.dialog}
      {deleteDialog}
      {historyDialog}
      {changeOnlineStatusDialog}
      {addElementOpened && (
        <AddElement
          title={t('addElement.title')}
          t={t}
          hide={() => setAddElementOpened(false)}
          sections={[
            [
              {
                type: 'case_type',
                title: t('common:entityType.case_type'),
                icon: 'entityType.case_type',
                action: () => {
                  openCaseTypeDialog({});
                  setAddElementOpened(false);
                },
                disabled: topLevel,
              },
              {
                type: 'object_type',
                title: t('common:entityType.object_type'),
                icon: 'entityType.object_type',
                action: () => goToEditOrCreate.objectType('0'),
                disabled: topLevel,
              },
              {
                type: 'custom_object_type',
                icon: 'entityType.custom_object_type',
                title: t('common:entityType.custom_object_type'),
                action: () => navigate(getPathToNewCustomObjectType(folderId)),
                disabled: topLevel,
              },
              {
                type: 'email_template',
                icon: 'entityType.email_template',
                title: t('common:entityType.email_template'),
                action: () => {
                  openEmailTemplateDialog({});
                  setAddElementOpened(false);
                },
                disabled: topLevel,
              },
              {
                type: 'document_template',
                icon: 'entityType.document_template',
                title: t('common:entityType.document_template'),
                action: () => {
                  openDocumentTemplateDialog({});
                  setAddElementOpened(false);
                },
                disabled: topLevel,
              },
              {
                type: 'attribute',
                icon: 'entityType.attribute',
                title: t('common:entityType.attribute'),
                action() {
                  openAttributeDialog({});
                  setAddElementOpened(false);
                },
                disabled: topLevel,
              },
            ],
            [
              {
                type: 'folder',
                icon: 'entityType.folder',
                title: t('common:entityType.folder'),
                action() {
                  folderDialog.open({});
                  setAddElementOpened(false);
                },
                disabled: false,
              },
              {
                type: 'import',
                icon: 'actions.import',
                title: t('actions.import'),
                action() {
                  navigate(getPathToImport());
                },
                disabled: topLevel,
              },
            ],
          ]}
        />
      )}
    </Sheet>
  );
};

export default Catalog;
