// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import ActionBar from '@mintlab/ui/App/Zaaksysteem/ActionBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { capitalize } from '@mintlab/kitchen-sink/source';
import { CatalogItemType } from '../../Catalog.types';

type ActionBarPropsType = {
  searchTerm: string;
  refresh: () => void;
  selectedItems: CatalogItemType[];
  setMoveItems: (items: CatalogItemType[]) => void;
  toggleDetailView: () => void;
  moveItems: CatalogItemType[];
  editCaseType: () => void;
  editObjectType: () => void;
  editCustomObjectType: () => void;
  editAttribute: () => void;
  editDocumentTemplate: () => void;
  editEmailTemplate: () => void;
  editFolder: () => void;
  duplicateCaseType: () => void;
  deleteItem: () => void;
  initHistory: () => void;
  exportCaseType: () => void;
  changeOnlineStatus: () => void;
  setAddElementOpened: (open: boolean) => void;
};

/* eslint complexity: [2, 12] */
const ActionBarWrapper: React.ComponentType<ActionBarPropsType> = ({
  searchTerm,
  refresh,
  selectedItems,
  setMoveItems,
  toggleDetailView,
  moveItems,
  editCaseType,
  editObjectType,
  editCustomObjectType,
  editAttribute,
  editDocumentTemplate,
  editEmailTemplate,
  editFolder,
  duplicateCaseType,
  deleteItem,
  initHistory,
  exportCaseType,
  changeOnlineStatus,
  setAddElementOpened,
}) => {
  const [t] = useTranslation('catalog');
  const [value, setValue] = useState(searchTerm);

  const setSearchTerm = (keyword?: string) => {
    const query = keyword ? '?query=' + keyword : '';
    const path = window.location.pathname + query;

    window.history.pushState('', '', path);
    refresh();
  };

  const clearSearchTerm = () => {
    setValue('');
    if (window.location.search.length) {
      setSearchTerm();
    }
  };

  const selectedItem = selectedItems[0];
  const selectedItemType = selectedItem?.type;
  const selectedItemActive = selectedItem?.active;

  const singleSelected = selectedItems.length === 1;
  const selectedCaseType = singleSelected && selectedItemType === 'case_type';
  const selectedObjectType =
    singleSelected && selectedItemType === 'object_type';
  const editableType =
    singleSelected &&
    [
      'attribute',
      'email_template',
      'folder',
      'document_template',
      'custom_object_type',
    ].includes(selectedItemType);

  const actionButtons = [
    {
      icon: iconNames.add,
      action: () => setAddElementOpened(true),
      condition: !searchTerm && !moveItems.length,
      label: t('buttonBar.create'),
      name: 'buttonBarCreate',
    },
    {
      icon: iconNames.edit,
      action: editCaseType,
      condition: selectedCaseType,
      label: t('buttonBar.edit'),
      name: 'buttonBarEditCaseType',
    },
    {
      icon: iconNames.edit,
      action: editObjectType,
      condition: selectedObjectType,
      label: t('buttonBar.edit'),
      name: 'buttonBarEditObjectType',
    },
    {
      icon: iconNames.file_copy,
      action: duplicateCaseType,
      condition: selectedCaseType,
      label: t('buttonBar.duplicate'),
      name: 'buttonBarFileCopy',
    },
    {
      icon: iconNames.folder_move,
      action: () => setMoveItems(selectedItems),
      label: t('buttonBar.move'),
      condition: selectedItems.length && !moveItems.length,
      name: 'buttonBarFolderMove',
    },
    {
      icon: iconNames.edit,
      action: () => {
        const action = {
          attribute: editAttribute,
          folder: editFolder,
          document_template: editDocumentTemplate,
          email_template: editEmailTemplate,
          custom_object_type: editCustomObjectType,
        }[selectedItemType as string];

        action && action();
      },
      label: t('buttonBar.edit'),
      condition: editableType,
      name: 'buttonBarEditGeneric',
    },
    {
      icon: iconNames.history,
      action: initHistory,
      label: t('buttonBar.history'),
      condition: selectedCaseType,
      name: 'buttonBarHistory',
    },
    {
      icon: iconNames.delete,
      action: deleteItem,
      label: t('buttonBar.delete'),
      condition: singleSelected,
      name: 'buttonBarDelete',
    },
  ].filter(btn => btn.condition);

  const advancedActionButtons = [
    {
      action: exportCaseType,
      label: t('buttonBar.export'),
      condition: selectedCaseType,
      name: 'buttonBarExport',
    },
    {
      action: changeOnlineStatus,
      label: capitalize(
        t('changeOnlineStatus.menuTitle', {
          type:
            singleSelected && selectedItemActive
              ? t('changeOnlineStatus.offline')
              : t('changeOnlineStatus.online'),
        })
      ),
      condition: selectedCaseType,
      name: 'buttonBarChangeOnlineStatus',
    },
  ].filter(btn => btn.condition);

  const permanentButtons = [
    {
      icon: iconNames.info,
      action: toggleDetailView,
      label: t('buttonBar.details'),
      name: 'buttonBarInfo',
    },
  ];

  return (
    <ActionBar
      filters={[
        {
          width: 400,
          type: 'text',
          value,
          icon: iconNames.search,
          placeholder: t('search'),
          onChange: event => setValue(event.target.value),
          onKeyPress: ({ key }: any) => {
            if (key.toLowerCase() === 'enter' && value && value.length > 0) {
              setSearchTerm(value);
            }
          },
          closeAction: value || searchTerm ? clearSearchTerm : undefined,
        },
      ]}
      actions={actionButtons}
      advancedActions={advancedActionButtons}
      permanentActions={permanentButtons}
    />
  );
};

export default ActionBarWrapper;
