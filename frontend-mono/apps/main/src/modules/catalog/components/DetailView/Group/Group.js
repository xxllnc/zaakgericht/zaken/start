// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { groupStylesheet } from './Group.style';

/**
 * @reactProps {Object} classes
 * @reactProps {React.children} [children]
 * @reactProps {string} title
 * @return {ReactElement}
 */
const Group = ({ classes, children, title }) => (
  <React.Fragment>
    <div className={classes.subHeading}>{title}</div>
    {children}
  </React.Fragment>
);

export default withStyles(groupStylesheet)(Group);
