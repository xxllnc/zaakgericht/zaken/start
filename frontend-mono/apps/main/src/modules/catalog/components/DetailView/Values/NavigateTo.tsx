// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useValuesStyle } from './Values.style';

type NavigateToPropsType = {
  value: any;
  title: string;
  doNavigate?: any;
  internal?: boolean;
};

const NavigateTo = ({
  value,
  title,
  doNavigate = null,
  internal = true,
}: NavigateToPropsType) => {
  const style = useValuesStyle();

  return (
    <a
      style={style.link}
      href={value}
      title={title}
      onClick={preventDefaultAndCall(() => {
        if (internal && doNavigate) {
          doNavigate(value);
        } else {
          const newPage = window.open(value, '_blank');
          newPage && newPage.focus();
        }
      })}
    >
      {title}
      {!internal && <Icon size="extraSmall">{iconNames.open_in_new}</Icon>}
    </a>
  );
};

export default NavigateTo;
