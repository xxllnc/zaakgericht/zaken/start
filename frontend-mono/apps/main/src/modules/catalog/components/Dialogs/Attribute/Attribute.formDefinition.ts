// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  DOCUMENT_CATEGORIES,
  DOCUMENT_CONFIDENTIALITY,
  DOCUMENT_ORIGIN,
} from '@zaaksysteem/common/src/constants/document';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import ObjectTypeFinder from '@zaaksysteem/common/src/components/form/fields/ObjectTypeFinder/ObjectTypeFinder';
import * as fieldTypesCommon from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import * as fieldTypes from '../../../../../components/Form/Constants/fieldTypes';
import { ProductFinder } from './ProductFinder';
import Types from './TypesComp';
import { LocationFinder } from './LocationFinder';

const getChoiceWithTranslation =
  (t: i18next.TFunction, name: string) => (value: any) => {
    return {
      value,
      label: t(`attribute.fields.${name}.choices.${value}`),
    };
  };

type AppointmentV2InterfaceType = {
  id: string;
  attributes: {
    name: string;
  };
};

const getAppointmentV2InterfacesChoices = (
  interfaces: AppointmentV2InterfaceType[]
) =>
  interfaces.map(({ id, attributes: { name } }) => ({
    label: name,
    value: id,
  }));

const strToChoice = (str: string) => ({ label: str, value: str });

export const getFormDefinition = (
  initialValues: any,
  t: i18next.TFunction,
  appointmentId: string,
  isCreating: boolean,
  appointmentV2interfaces: AppointmentV2InterfaceType[]
): FormDefinition =>
  [
    {
      name: 'name',
      type: fieldTypes.TEXT,
      value: initialValues.name,
      required: true,
      creatable: true,
      isMulti: false,
      clearable: true,
      triggerChangeWhenBlurred: true,
      label: t('attribute.fields.name.label'),
      placeholder: t('attribute.fields.name.label'),
    },
    {
      name: 'magic_string',
      type: fieldTypesCommon.MAGIC_STRING_GENERATOR,
      required: true,
      disabled: !isCreating,
      label: t('attribute.fields.magic_string.label'),
      hint: t('form:cannotChangeLater'),
    },
    {
      name: 'public_name',
      type: fieldTypes.TEXT,
      required: false,
      label: t('attribute.fields.public_name.label'),
      placeholder: t('attribute.fields.public_name.label'),
    },
    {
      name: 'sensitive_field',
      type: fieldTypes.CHECKBOX,
      checked: false,
      required: false,
      label: t('attribute.fields.sensitive_field.label'),
    },
    {
      name: 'attribute_type',
      component: Types,
      type: '',
      required: true,
      format: 'mixed' as const,
      label: t('attribute.fields.attribute_type.label'),
      placeholder: t('form:choose'),
      hint: t('form:cannotChangeLater'),
      isClearable: false,
      dialogId: 'catalog-attribute-dialog',
      disabled: !isCreating,
      choices: [
        'bag_openbareruimte',
        'bag_openbareruimtes',
        'bag_straat_adres',
        'bag_straat_adressen',
        'bag_adres',
        'bag_adressen',
        'bankaccount',
        appointmentId ? 'appointment' : '',
        'appointment_v2',
        'calendar',
        'calendar_supersaas',
        'checkbox',
        'date',
        'email',
        'file',
        'geojson',
        'geolatlon',
        'googlemaps',
        'address_v2',
        'image_from_url',
        'numeric',
        'option',
        'richtext',
        'select',
        'subject',
        'relationship',
        'text',
        'text_uc',
        'textarea',
        'url',
        'valuta',
        'valutaex',
        'valutaex6',
        'valutaex21',
        'valutain',
        'valutain6',
        'valutain21',
      ]
        .filter(Boolean)
        .map(value => ({
          label: t(`common:attributeTypes.${value}`),
          value,
        })),
    },
    {
      name: 'relationship_type',
      type: fieldTypes.SELECT,
      required: true,
      hint: t('form:cannotChangeLater'),
      isClearable: true,
      disabled: !isCreating,
      label: t('attribute.fields.relationship_type.label'),
      placeholder: t('form:choose'),
      choices: ['case', 'custom_object', 'subject'].map(value => ({
        label: t(`common:attributeRelationshipTypes.${value}`),
        value,
      })),
    },
    {
      name: 'object_type',
      component: ObjectTypeFinder,
      type: '',
      placeholder: t('form:choose'),
      required: true,
      label: t('attribute.fields.object_type.label'),
    },
    {
      name: 'type_multiple',
      type: fieldTypes.CHECKBOX,
      required: false,
      label: t('attribute.fields.type_multiple.label'),
    },
    {
      name: 'appointment_location_id',
      component: LocationFinder,
      type: '',
      required: false,
      format: 'mixed' as const,
      label: t('attribute.fields.appointment_location_id.label'),
      placeholder: t('form:choose'),
      loadingMessage: t('common:loading'),
      choices: [],
      config: { appointmentIntegrationUuid: appointmentId },
    },
    {
      name: 'appointment_product_id',
      component: ProductFinder,
      type: '',
      placeholder: t('form:choose'),
      format: 'mixed' as const,
      required: false,
      label: t('attribute.fields.appointment_product_id.label'),
      loadingMessage: t('common:loading'),
      config: { appointmentIntegrationUuid: appointmentId },
    },
    {
      name: 'appointment_interface_uuid',
      type: fieldTypes.SELECT,
      required: true,
      isClearable: true,
      label: t('attribute.fields.appointment_interface_uuid.label'),
      placeholder: t('form:choose'),
      choices: getAppointmentV2InterfacesChoices(appointmentV2interfaces),
    },
    {
      name: 'attribute_values',
      type: fieldTypes.OPTIONS,
      required: false,
      label: t('attribute.fields.attribute_values.label'),
      config: {
        allowNew: true,
        showHideInactive: true,
      },
    },
    {
      name: 'document_category',
      type: fieldTypes.SELECT,
      required: false,
      label: t('attribute.fields.document_category.label'),
      placeholder: t('form:choose'),
      choices: DOCUMENT_CATEGORIES.map(strToChoice),
    },
    {
      name: 'document_trust_level',
      type: fieldTypes.SELECT,
      required: false,
      label: t('attribute.fields.document_trust_level.label'),
      placeholder: t('form:choose'),
      choices: DOCUMENT_CONFIDENTIALITY.map(strToChoice),
    },
    {
      name: 'document_origin',
      type: fieldTypes.SELECT,
      required: true,
      label: t('attribute.fields.document_origin.label'),
      placeholder: t('form:choose'),
      choices: DOCUMENT_ORIGIN.map(
        getChoiceWithTranslation(t, 'document_origin')
      ),
    },
    {
      name: 'document_source',
      type: fieldTypes.TEXT,
      label: t('attribute.fields.document_source.label'),
    },
    {
      name: 'help',
      type: fieldTypes.TEXT,
      required: false,
      label: t('attribute.fields.help.label'),
      isMultiline: true,
    },
    {
      name: 'value_default',
      type: fieldTypes.TEXT,
      required: false,
      label: t('attribute.fields.value_default.label'),
      isMultiline: true,
    },

    {
      name: 'commit_message',
      type: fieldTypes.TEXT,
      required: true,
      label: t('attribute.fields.commit_message.label'),
      help: t('attribute.fields.commit_message.help'),
      value: isCreating ? t('defaultCreate') : t('defaultEdit'),
    },
  ].map(field => ({ value: initialValues[field.name], ...field }));
