// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIAdmin } from '@zaaksysteem/generated/types/APIAdmin.types';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { invalidateAfterChange } from '../../../Catalog.symbols';

const GET_ATTRIBUTE = 'GET_ATTRIBUTE';

export const defaultInitialValues = {
  attribute_type: null,
  name: '',
  public_name: '',
  magic_string: '',
  type_multiple: false,
  sensitive_field: false,
  help: '',
  value_default: '',
  document_origin: null,
  document_trust_level: null,
  document_category: null,
  relationship_data: { type: null, name: null, uuid: null },
  relationship_type: null,
  object_type: null,
  attribute_values: [],
  appointment_location_id: null,
  appointment_product_id: null,
  appointment_interface_uuid: '',
  commit_message: 'Nieuw aangemaakt',
};

/* eslint complexity: [2, 8] */
/* eslint-disable getter-return */
const mangleValuesForSave = ({
  values,
  id,
  appointment_interface_uuid,
}: {
  values: any;
  id: string | null;
  appointment_interface_uuid: string | undefined;
}) => {
  const isNew = !id;
  const attribute_uuid = isNew ? v4() : id;
  const mangledValues = {} as any;

  Object.keys(values).forEach(key => {
    switch (key) {
      case 'name':
        mangledValues[key] = values[key];
        break;
      case 'document_category':
      case 'document_trust_level':
      case 'document_origin':
        mangledValues[key] = normalizeValue(values[key]);
        break;
      case 'attribute_values':
        mangledValues[key] = ((values.attribute_values as any[]) || []).map(
          (item, index) => ({
            ...cloneWithout(item, 'id', 'isNew'),
            sort_order: index,
          })
        );
        break;
      default:
        mangledValues[key] = values[key];
    }
  }, {});

  mangledValues.category_uuid = window.location.pathname.split('/catalog/')[1];
  mangledValues.appointment_interface_uuid =
    values?.appointment_interface_uuid?.value ||
    appointment_interface_uuid ||
    null;

  //add relationship data
  if (mangledValues.relationship_type) {
    mangledValues.relationship_data = {
      type: normalizeValue(mangledValues.relationship_type),
    };

    if (mangledValues.object_type) {
      mangledValues.relationship_data.name = mangledValues.object_type.label;
      mangledValues.relationship_data.uuid = mangledValues.object_type.value;
    }
  }

  return {
    fields: mangledValues,
    attribute_uuid,
    isNew,
  };
};

const fetchAppointmentIntegrationId = async () => {
  const body =
    await request<APIAdmin.GetActiveAppointmentIntegrationsResponseBody>(
      'GET',
      '/api/v2/admin/integrations/get_active_appointment_integrations',
      null,
      { type: 'json', cached: true }
    );

  return body.data[0]?.id as string | undefined;
};

const fetchAppointmentV2Interfaces = async () => {
  const body =
    await request<APIAdmin.GetActiveAppointmentV2IntegrationsResponseBody>(
      'GET',
      '/api/v2/admin/integrations/get_active_appointment_v2_integrations',
      null,
      { type: 'json', cached: true }
    );

  return body ? body.data : [];
};

const fetchAttribute = async (attribute_id: string) => {
  const body = await request<APIAdmin.GetAttributeDetailResponseBody>(
    'GET',
    buildUrl<APIAdmin.GetAttributeDetailRequestParams>(
      '/api/v2/admin/catalog/get_attribute_detail',
      { attribute_id }
    )
  );

  const values = (body ? body.data : { attributes: {} }).attributes;

  const res = {
    ...values,
    ...(values?.relationship_data?.type && {
      relationship_type: values?.relationship_data?.type,
    }),
    ...(values?.relationship_data?.uuid && {
      object_type: {
        label: values.relationship_data.name,
        value: values.relationship_data.uuid,
      },
    }),
  };

  return res;
};

export const useAttributeDialogSetup = (
  enabled: boolean,
  attributeId?: string
) =>
  useQuery<
    {
      appointmentIntegrationId: any;
      appointmentV2Interfces: any;
      attributeData: any;
    },
    V2ServerErrorsType
  >(
    [GET_ATTRIBUTE, attributeId],
    async () => {
      const promises = [
        fetchAppointmentIntegrationId(),
        fetchAppointmentV2Interfaces(),
        attributeId ? fetchAttribute(attributeId) : Promise.resolve(),
      ];

      const [appointmentIntegrationId, appointmentV2Interfces, attributeData] =
        await Promise.all(promises);

      return {
        appointmentIntegrationId,
        appointmentV2Interfces,
        attributeData,
      };
    },
    { enabled, onError: openServerError }
  );

export const useSaveAttributeMutation = (onSuccess: () => void) => {
  const client = useQueryClient();

  return useMutation<
    APIAdmin.CreateAttributeRequestBody,
    V2ServerErrorsType,
    any
  >(
    ['save_attribute'],
    data => {
      const res = mangleValuesForSave(data);

      return saveAttribute(res);
    },
    {
      onError: openServerError,
      onSuccess: () => {
        client.invalidateQueries([GET_ATTRIBUTE]);
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

const saveAttribute = (payload: ReturnType<typeof mangleValuesForSave>) => {
  const url = payload.isNew
    ? '/api/v2/admin/catalog/create_attribute'
    : '/api/v2/admin/catalog/edit_attribute';

  const data = cloneWithout(payload, 'isNew');

  return request<APIAdmin.CreateAttributeRequestBody>('POST', url, data);
};
