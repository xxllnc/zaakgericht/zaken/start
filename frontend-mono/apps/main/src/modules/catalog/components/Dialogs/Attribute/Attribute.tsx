// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import isValidMagicString from '@zaaksysteem/common/src/components/form/fields/MagicStringGenerator/isValidMagicstring';
import {
  Rule,
  valueOneOf,
  valueEquals,
  hideFields,
  showFields,
  setFieldValue,
  hasValue,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { getDialogTitle } from '../library';
import {
  defaultInitialValues,
  useAttributeDialogSetup,
  useSaveAttributeMutation,
} from './Attribute.library';
import { getFormDefinition } from './Attribute.formDefinition';

/* eslint complexity: [2, 9] */
export const useAttributeDialog = () => {
  const [openedData, setOpenedData] = React.useState<false | { id?: string }>(
    false
  );
  const close = () => setOpenedData(false);

  const [t] = useTranslation('catalog');

  const { mutateAsync, isLoading: saving } = useSaveAttributeMutation(close);

  const opened = Boolean(openedData);
  const editId = (openedData && openedData.id) || '';
  const editMode = Boolean(editId);
  const createMode = !editMode;

  const { data, isLoading: initializing } = useAttributeDialogSetup(
    opened,
    editId
  );

  const formDefinition = data
    ? getFormDefinition(
        data.attributeData || defaultInitialValues,
        t as any,
        editId,
        createMode,
        data.appointmentV2Interfces
      )
    : [];

  const title = getDialogTitle(t as any, 'attribute', editMode);

  const rules = React.useMemo(
    () => [
      new Rule()
        .when('name', () => createMode)
        .then(transferDataAsConfig('name', 'magic_string', 'rawName')),
      new Rule()
        .when('attribute_type', valueOneOf(['checkbox', 'select', 'option']))
        .then(showFields(['attribute_values']))
        .else(hideFields(['attribute_values'])),
      new Rule()
        .when('attribute_type', valueEquals('file'))
        .then(
          showFields([
            'document_category',
            'document_trust_level',
            'document_origin',
            'document_source',
          ])
        )
        .else(
          hideFields([
            'document_category',
            'document_trust_level',
            'document_origin',
            'document_source',
          ])
        ),
      new Rule()
        .when('attribute_type', valueEquals('appointment'))
        .then(showFields(['appointment_location_id', 'appointment_product_id']))
        .else(
          hideFields(['appointment_location_id', 'appointment_product_id'])
        ),
      new Rule()
        .when('appointment_location_id', hasValue)
        .then(showFields(['appointment_product_id']))
        .and(
          transferDataAsConfig(
            'appointment_location_id',
            'appointment_product_id',
            'locationUuid'
          )
        )
        .else(hideFields(['appointment_product_id'])),
      new Rule()
        .when('attribute_type', valueOneOf(['relationship']))
        .then(showFields(['relationship_type']))
        .else(hideFields(['relationship_type']))
        .and(setFieldValue('relationship_type', null)),
      new Rule()
        .when('relationship_type', valueOneOf(['custom_object']))
        .then(showFields(['object_type', 'type_multiple']))
        .else(hideFields(['object_type', 'type_multiple']))
        .and(setFieldValue('object_type', [])),
      new Rule()
        .when(
          'attribute_type',
          valueOneOf(['numeric', 'select', 'text', 'text_uc', 'textarea'])
        )
        .then(showFields(['type_multiple'])),
      new Rule()
        .when('attribute_type', valueEquals('appointment_v2'))
        .then(showFields(['appointment_interface_uuid']))
        .else(hideFields(['appointment_interface_uuid'])),
      new Rule()
        .when(
          'attribute_type',
          valueOneOf([
            'address_v2',
            'geojson',
            'appointment_v2',
            'relationship',
          ])
        )
        .then(hideFields(['value_default']))
        .else(showFields(['value_default'])),
    ],
    [createMode]
  );

  const dialog = openedData && (
    <React.Fragment>
      <FormDialog
        formDefinition={formDefinition}
        title={title}
        scope="attribute-form-component"
        validationMap={{ magic_string: isValidMagicString }}
        rules={rules}
        icon="extension"
        isInitialValid={editMode}
        initializing={initializing}
        saving={saving}
        onClose={close}
        onSubmit={values => {
          return mutateAsync({
            values,
            id: editId || null,
            appointment_interface_uuid: data?.appointmentIntegrationId,
          });
        }}
      />
    </React.Fragment>
  );

  return {
    attributeDialog: dialog,
    openAttributeDialog: setOpenedData,
  };
};
