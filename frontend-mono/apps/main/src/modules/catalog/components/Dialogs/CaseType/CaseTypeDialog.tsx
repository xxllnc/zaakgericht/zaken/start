// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { openSnackbar } from '@zaaksysteem/common/src/signals';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { v4 } from 'uuid';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { getFormDefinition } from './CaseTypeDialog.formDefinition';
import { useSaveCaseTypeMutation } from './CaseTypeDialog.library';

/* eslint complexity: [2, 9] */
export const useCaseTypeDialog = (folderId: string) => {
  const [t] = useTranslation('catalog');
  const [open, setOpen] = useState<null | { id?: string }>(null);
  const duplicating = Boolean(open?.id);
  const close = () => setOpen(null);
  const uuid = v4();

  const caseType = t('common:entityType.case_type');
  const action = duplicating
    ? t('caseTypeDialog.action.duplicate')
    : t('caseTypeDialog.action.create');

  const title = `${caseType} ${action.toLowerCase()}`;

  const { mutateAsync, isLoading: saving } = useSaveCaseTypeMutation(
    t as any,
    close,
    duplicating
  );
  const formDefinition = getFormDefinition(t as any, duplicating);

  const dialog = open && (
    <FormDialog
      formDefinition={formDefinition}
      title={title}
      scope="case-type-form-component"
      icon="extension"
      saving={saving}
      onClose={close}
      saveLabel={action}
      onSubmit={values =>
        mutateAsync({ ...values, folderId, uuid, caseTypeId: open?.id }).then(
          () => {
            const link = buildUrl(`/main/case-type/${uuid}/bewerken`, {
              return_url: `/main/catalog/${folderId}`,
            });

            const name = values.name;
            const snack = duplicating
              ? t('caseTypeDialog.snack.duplicated', { name })
              : t('caseTypeDialog.snack.created', { name });

            openSnackbar({ message: snack, link });
          }
        )
      }
    />
  );

  return {
    caseTypeDialog: dialog,
    openCaseTypeDialog: setOpen,
  };
};
