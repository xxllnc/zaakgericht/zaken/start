// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIAdmin } from '@zaaksysteem/generated/types/APIAdmin.types';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { CASE_TYPE_VERSION_HISTORY } from '../../../Catalog.symbols';

export const useGetHistory = (id: string) =>
  useQuery<APIAdmin.GetCaseTypeHistoryResponseBody, V2ServerErrorsType>(
    [CASE_TYPE_VERSION_HISTORY, id],
    () => {
      return request(
        'GET',
        buildUrl<APIAdmin.GetCaseTypeHistoryRequestParams>(
          '/api/v2/admin/catalog/get_case_type_history',
          {
            case_type_id: id,
          }
        )
      );
    },
    { enabled: Boolean(id), onError: openServerError }
  );

export const useHistoryReactivationMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    any,
    V2ServerErrorsType,
    APIAdmin.UpdateCaseTypeVersionRequestBody
  >(
    ['HISTORY_REACTIVATE'],
    payload =>
      request(
        'POST',
        '/api/v2/admin/catalog/activate_case_type_version',
        payload
      ),
    {
      onError: openServerError,
      onSuccess: () => {
        client.invalidateQueries([CASE_TYPE_VERSION_HISTORY]);
        onSuccess();
      },
    }
  );
};
