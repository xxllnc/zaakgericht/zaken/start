// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIAdmin } from '@zaaksysteem/generated';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { invalidateAfterChange } from '../../Catalog.symbols';

const GET_EMAIL_TEMPLATE = 'GET_EMAIL_TEMPLATE';
const GET_DOCUMENT_TEMPLATE = 'GET_DOCUMENT_TEMPLATE';

export const useSaveEmailTemplateMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APIAdmin.CreateAnEmailTemplateRequestBody,
    V2ServerErrorsType,
    {
      payload: APIAdmin.CreateAnEmailTemplateRequestBody;
      url: string;
    }
  >(
    ['SAVE_EMAIL_TEMPLATE'],
    payload => {
      return request<APIAdmin.CreateAnEmailTemplateRequestBody>(
        'POST',
        payload.url,
        payload.payload
      );
    },
    {
      onError: openServerError,
      onSuccess: () => {
        client.invalidateQueries([GET_EMAIL_TEMPLATE]);
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useEmailTemplateQuery = (enabled: boolean, id?: string) => {
  return useQuery<
    APIAdmin.GetEmailTemplateDetailResponseBody,
    V2ServerErrorsType
  >(
    [GET_EMAIL_TEMPLATE, id],
    async () => {
      const res = await request<APIAdmin.GetEmailTemplateDetailResponseBody>(
        'GET',
        buildUrl<APIAdmin.GetEmailTemplateDetailRequestParams>(
          '/api/v2/admin/catalog/get_email_template_detail',
          { email_template_id: id || '' }
        )
      );

      return {
        ...res,
        data: {
          ...res.data,
          attributes: {
            ...res.data.attributes,
            attachments: res.data.attributes.attachments.map(
              ({ name, uuid }: { name: string; uuid: string }) => ({
                value: uuid,
                label: name,
              })
            ),
          },
        },
      };
    },
    { enabled, onError: openServerError }
  );
};

export const useSaveDocumentTemplateMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APIAdmin.CreateDocumentTemplateRequestBody,
    V2ServerErrorsType,
    {
      payload: APIAdmin.CreateDocumentTemplateRequestBody;
      url: string;
    }
  >(
    ['SAVE_DOCUMENT_TEMPLATE'],
    payload => {
      return request<APIAdmin.CreateDocumentTemplateRequestBody>(
        'POST',
        payload.url,
        payload.payload
      );
    },
    {
      onError: openServerError,
      onSuccess: () => {
        client.invalidateQueries([GET_DOCUMENT_TEMPLATE]);
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useSaveFolderMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APIAdmin.CreateFolderRequestBody,
    V2ServerErrorsType,
    { payload: APIAdmin.CreateFolderRequestBody; url: string }
  >(
    ['SAVE_DOCUMENT_TEMPLATE'],
    payload => {
      return request<APIAdmin.CreateFolderRequestBody>(
        'POST',
        payload.url,
        payload.payload
      );
    },
    {
      onError: openServerError,
      onSuccess: () => {
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useChangeOnlineStatusMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    any,
    V2ServerErrorsType,
    APIAdmin.ChangeCaseTypeOnlineStatusRequestBody
  >(
    ['SAVE_DOCUMENT_TEMPLATE'],
    payload =>
      request(
        'POST',
        '/api/v2/admin/catalog/change_case_type_online_status',
        payload
      ),
    {
      onError: openServerError,
      onSuccess: () => {
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useDocumentTemplateQuery = (enabled: boolean, id?: string) => {
  return useQuery<
    APIAdmin.GetDocumentTemplateDetailResponseBody,
    V2ServerErrorsType
  >(
    ['GET_DOCUMENT_TEMPLATE', id],
    () =>
      request(
        'GET',
        buildUrl<APIAdmin.GetDocumentTemplateDetailRequestParams>(
          '/api/v2/admin/catalog/get_document_template_detail',
          { document_template_id: id || '' }
        )
      ),
    { enabled, onError: openServerError }
  );
};

export const fixFormDefinitionValues = (
  attributes: { [key: string]: string },
  formDefinition: FormDefinition
): FormDefinition => {
  return attributes
    ? formDefinition.map(field => ({ ...field, value: attributes[field.name] }))
    : [];
};

const caseTypeStatusValues = ['online', 'offline'];
export const getCaseTypeStatusChoices = (t: i18next.TFunction) =>
  caseTypeStatusValues.map(value => ({
    label: t(`caseTypeDialog.fields.values.status.${value}`),
    value,
  }));

export const getDialogTitle = (
  t: i18next.TFunction,
  dialogType: string,
  editMode: boolean
) => {
  return t(`${dialogType}.dialog.title`, {
    action: (editMode
      ? t('common:verbs.edit')
      : t('common:verbs.create')
    ).toLowerCase(),
  });
};
