// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';

const getReturnUrl = () =>
  `/main/catalog${window.location.href.split('main/catalog')[1] || ''}`;

export const getPathToCaseType = (
  id: number | string,
  suffix: string,
  shouldReturn: boolean,
  params = {}
) => {
  return buildUrl(`/main/case-type/${id}${suffix}`, {
    return_url: shouldReturn ? getReturnUrl() : undefined,
    ...params,
  });
};

export const getPathToObjectType = (
  id: number | string,
  suffix: string,
  shouldReturn: boolean,
  params = {}
) =>
  buildUrl(`/main/object-type-v1/${id}${suffix}`, {
    return_url: shouldReturn ? getReturnUrl() : undefined,
    ...params,
  });

export const getPathToNewCustomObjectType = (folder_uuid: string) => {
  const return_url = getReturnUrl();
  return buildUrl('/main/object-type/create', { folder_uuid, return_url });
};

export const getPathToCustomObjectType = (id: string) => {
  const return_url = getReturnUrl();

  return buildUrl(`/main/object-type/update/${id}`, { return_url });
};

export const getPathToImport = () => {
  const return_url = getReturnUrl();

  return buildUrl('/main/object-import/1', { return_url });
};

export const getPathToItem = (id: string, type = 'folder') => {
  const basePath = '/main/catalog';
  if (!id) {
    return basePath;
  }

  const paths = {
    folder: `${basePath}/${id}`,
    case_type: getPathToCaseType(id, '/bewerken', true),
    object_type: getPathToObjectType(id, '/bewerken', true),
    custom_object_type: getPathToCustomObjectType(id),
  };

  //@ts-ignore
  return paths[type];
};
