// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const changeOnlineStatus = {
  changeOnlineStatus: {
    online: 'online',
    offline: 'offline',
    reason: 'reden',
    title: 'Zaaktype {{ type }} zetten',
    menuTitle: '{{ type }} zetten',
    fields: {
      title: 'Zaaktype {{ type }} zetten',
      reason: {
        label:
          'Geef hieronder de reden op voor het {{ type }} zetten van dit zaaktype.',
        placeholder: 'Reden',
      },
    },
  },
};
