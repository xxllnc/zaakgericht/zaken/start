// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SettingsStructureType } from './Configuration.types';

const parameters = {
  emailTemplate: {
    url: '/api/v1/email_template',
    match: 'label',
  },
  group: {
    url: '/api/v1/group',
    match: 'name',
  },
  municipalities: {
    url: '/api/v1/general/municipality_code',
    match: 'name',
  },
  rolev2: {
    url: '/api/v2/cm/authorization/get_roles',
    match: 'filter[relationships.parent.id]',
  },
};

export const choicesMap = {
  allocation_notification_template_id: parameters.emailTemplate,
  case_distributor_group: parameters.group,
  case_distributor_role: parameters.rolev2,
  case_suspension_term_exceeded_notification_template_id:
    parameters.emailTemplate,
  case_term_exceeded_notification_template_id: parameters.emailTemplate,
  export_queue_email_template_id: parameters.emailTemplate,
  new_assigned_case_notification_template_id: parameters.emailTemplate,
  new_attribute_proposal_notification_template_id: parameters.emailTemplate,
  new_document_notification_template_id: parameters.emailTemplate,
  new_ext_pip_message_notification_template_id: parameters.emailTemplate,
  new_int_pip_message_notification_template_id: parameters.emailTemplate,
  new_user_template: parameters.emailTemplate,
  subject_pip_authorization_confirmation_template_id: parameters.emailTemplate,
};

export const creatables = [
  'custom_relation_roles',
  'custom_cors_hosts',
  'custom_csp_hosts',
  'dashboard_ui_xss_uri',
];

export const nonClearables = [
  'case_distributor_group',
  'case_distributor_role',
  'signature_upload_role',
];

export const settingsStructure: SettingsStructureType = [
  {
    id: 'cases',
    icon: iconNames.folder_shared,
    fieldSets: [
      {
        title: 'notifications',
        description: 'notifications',
        fields: [
          'allocation_notification_template_id',
          'new_assigned_case_notification_template_id',
          'new_document_notification_template_id',
          'new_attribute_proposal_notification_template_id',
          'new_ext_pip_message_notification_template_id',
          'new_int_pip_message_notification_template_id',
          'case_term_exceeded_notification_template_id',
          'case_suspension_term_exceeded_notification_template_id',
        ],
      },
      {
        title: 'assignCase',
        fields: ['assignment_department_check'],
      },
      {
        title: 'rejectedCases',
        description: 'rejectedCases',
        fields: ['case_distributor_group', 'case_distributor_role'],
      },
      {
        title: 'caseNumber',
        description: 'caseNumber',
        fields: ['case_number_prefix'],
      },
      {
        title: 'showObject',
        description: 'showObject',
        fields: ['show_object_v1', 'show_object_v2'],
      },
    ],
  },
  {
    id: 'users',
    icon: iconNames.supervised_user_circle,
    fieldSets: [
      {
        title: 'newUsers',
        fields: ['first_login_confirmation', 'first_login_intro'],
      },
      {
        title: 'confirmationNewUsers',
        fields: ['new_user_template'],
      },
      {
        title: 'dashboard',
        fields: ['disable_dashboard_customization'],
      },
      {
        title: 'allowedDomains',
        fields: ['dashboard_ui_xss_uri'],
      },
      {
        title: 'signatureUploadRole',
        fields: ['signature_upload_role'],
      },
    ],
  },
  {
    id: 'pip',
    icon: iconNames.web,
    fieldSets: [
      {
        title: 'pipIntro',
        fields: ['pip_login_intro'],
      },
    ],
  },
  {
    id: 'documents',
    icon: iconNames.insert_drive_file,
    fieldSets: [
      {
        title: 'documentIntake',
        fields: ['document_intake_user'],
      },
      {
        title: 'documentRecognition',
        fields: ['file_username_seperator'],
      },
      {
        title: 'editDocumentOnline',
        fields: ['edit_document_online'],
      },
    ],
  },
  {
    id: 'about',
    icon: iconNames.fingerprint,
    fieldSets: [
      {
        title: 'customerInfo',
        fields: [
          'customer_info_naam',
          'customer_info_naam_lang',
          'customer_info_naam_kort',
          'customer_info_website',
        ],
      },
      {
        title: 'customerAddress',
        fields: [
          'customer_info_adres',
          'customer_info_postcode',
          'customer_info_woonplaats',
        ],
      },
      {
        title: 'customerExtra',
        fields: ['customer_info_postbus', 'customer_info_postbus_postcode'],
      },
      {
        title: 'customerPhone',
        fields: ['customer_info_telefoonnummer', 'customer_info_faxnummer'],
      },
      {
        title: 'customerMail',
        fields: ['customer_info_email', 'customer_info_zaak_email'],
      },
      {
        title: 'customerInfoCountryCode',
        fields: ['customer_info_country_code'],
      },
      {
        title: 'customerInfoGemeente',
        fields: [
          'customer_info_gemeente_portal',
          'customer_info_gemeente_id_url',
        ],
      },
    ],
  },
  {
    id: 'premium',
    icon: iconNames.star,
    fieldSets: [
      {
        title: 'documentWatcher',
        fields: ['files_locally_editable'],
      },
      {
        title: 'editDocumentOnline',
        fields: ['edit_document_msonline'],
      },
    ],
  },
  {
    id: 'other',
    icon: iconNames.card_giftcard,
    fieldSets: [
      {
        title: 'enableBIL',
        fields: ['enable_bil'],
      },
      {
        title: 'preferredChannel',
        fields: ['contact_channel_enabled'],
      },
      {
        title: 'publicManpage',
        fields: ['public_manpage'],
      },
      {
        title: 'customRelationRoles',
        fields: ['custom_relation_roles'],
      },
      {
        title: 'location',
        fields: ['customer_info_latitude', 'customer_info_longitude'],
      },
      {
        title: 'pipAuthorization',
        fields: ['subject_pip_authorization_confirmation_template_id'],
      },
      {
        title: 'exportQueue',
        fields: ['export_queue_email_template_id'],
      },
      {
        title: 'iframeableInTeams',
        fields: ['iframeable_in_teams'],
      },
      {
        title: 'corsConfiguration',
        fields: ['custom_cors_hosts'],
      },
      {
        title: 'cspConfiguration',
        fields: ['custom_csp_hosts'],
      },
    ],
  },
  {
    id: 'deprecated',
    icon: iconNames.save,
    fieldSets: [
      {
        title: 'externalSearch',
        description: 'externalSearch',
        fields: [
          'requestor_search_extension_active',
          'requestor_search_extension_name',
          'requestor_search_extension_href',
        ],
      },
      {
        title: 'changePassword',
        fields: ['users_can_change_password'],
      },
      {
        title: 'stufZkn',
        fields: ['enable_stufzkn_simulator'],
      },
      {
        title: 'allowedTemplates',
        fields: ['allowed_templates'],
      },
    ],
  },
  {
    id: 'styles',
    icon: iconNames.palette,
    fieldSets: [],
  },
];
