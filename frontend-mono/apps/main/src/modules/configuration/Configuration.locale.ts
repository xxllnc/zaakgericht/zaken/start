// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    styles: {
      host: 'Host',
      template: 'Actief template',
      actions: 'Acties',
      templateUploaded: 'Huisstijl template geüpload',
    },
    configuration: 'Configuratie',
    categories: {
      cases: 'Zaken',
      users: 'Gebruikers',
      pip: 'PIP',
      documents: 'Documenten',
      about: 'Over',
      premium: 'Premium',
      other: 'Overig',
      deprecated: 'Uitgefaseerd',
      styles: 'Huisstijl',
    },
    titles: {
      template: 'Huisstijl template',
      notifications: 'Notificaties',
      assignCase: 'Toewijzen',
      emailTemplate: 'E-mailsjabloon titel',
      rejectedCases: 'Afgewezen zaken',
      caseNumber: 'Alternatief zaaknummer',
      newUsers: 'Nieuwe gebruikers',
      confirmationNewUsers: 'Bevestigings-e-mail nieuwe gebruikers',
      dashboard: 'Dashboard',
      allowedDomains: 'Toegestane domeinen voor externe URL dashboard widget',
      signatureUploadRole: 'Handtekening uploaden',
      pipIntro: 'Welkomsttekst PIP',
      documentIntake: 'Documentinname zonder gebruiker',
      documentRecognition: 'Documentherkenning gebruiker',
      editDocumentOnline: 'Documenten bewerken met…',
      jodConverter: 'Jodconverter',
      customerInfo: 'Informatie',
      customerAddress: 'Adresgegevens',
      customerExtra: 'Aanvullende adresgegevens',
      customerPhone: 'Telefoonnummers',
      customerMail: 'E-mail',
      customerInfoCountryCode: 'Landcode',
      documentWatcher: 'Zaaksysteem document watcher',
      preferredChannel: 'Voorkeurskanaal',
      publicManpage: 'API handleiding Zaaksysteem.nl',
      customRelationRoles: 'Extra rollen voor betrokkenen',
      location: 'Weergave vestigingslocatie',
      pipAuthorization: 'E-mailnotificatie machtiging betrokkene',
      externalSearch: 'Aanvrager externe zoekfunctie',
      changePassword: 'Gebruikerswachtwoord',
      stufZkn: 'StUF-ZKN',
      pdfAnnotations: 'Annotaties',
      wordApp: 'De MS Word app is geactiveerd voor de omgeving',
      allowedTemplates: 'Toegestane web-sjablonen',
      customerInfoGemeente: "Overige URL's",
      exportQueue: 'Export als achtergrondtaak',
      corsConfiguration: 'Whitelist CORS hosts',
      cspConfiguration: 'Whitelist CSP headers',
      iframeableInTeams: 'MS Teams Integratie',
      showObject: 'Objectenoverzichten in tabblad Relaties',
      enableBIL: 'Beter inloggen',
    },
    descriptions: {
      selectEnv:
        'Upload .zss bestanden om de stijl van de geselecteerde omgeving te wijzigen.',
      notifications:
        'Selecteer het berichtsjabloon dat standaard wordt verstuurd bij de volgende gebeurtenissen.',
      rejectedCases:
        'Stuur afgewezen zaken door naar een aangewezen afdeling en rol.',
      confirmationNewUsers: 'Bevestigingse-mail nieuwe gebruikers.',
      signatureUploadRole:
        'Selecteer wie een handtekening mag uploaden voor de e-mails.',
      externalSearch:
        'Titel en link zijn pas relevant wanneer de extra zoekknop in het aanvrager dialoog zichtbaar is.',
      caseNumber: 'Alternatief zaaknummer voor zaken',
      showObject:
        'Selecteer hier welke objectoverzichten gewenst zijn om te tonen in de zaak.',
    },
    help: {
      allocation_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij een toewijzing wijziging op een zaak.',
      new_assigned_case_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij de aanmaak van nieuwe zaken met voorbepaalde behandelaars',
      new_document_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij de upload van een nieuw document in een zaak',
      new_ext_pip_message_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij de plaatsing van een nieuw bericht op de PIP door een betrokkene',
      new_int_pip_message_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij de plaatsing van een nieuw bericht op de PIP door de behandelaar',
      case_term_exceeded_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij een nieuwe overschrijding van de termijn een zaak (van urgentie middel naar hoog)',
      new_attribute_proposal_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij de plaatsing van een nieuwe kenmerkwijziging in de zaak',
      case_suspension_term_exceeded_notification_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij een nieuwe overschrijding van de opschortingstermijn van een zaak',
      assignment_department_check:
        'Deze functionaliteit stelt de beheerders van het zaaksysteem in staat om een waarschuwingsbericht te tonen wanneer de behandelaren een zaak toewijzen aan een afdeling + rol zonder rechten.',
      case_distributor_group:
        'Wanneer een zaak wordt afgewezen, wordt deze doorgestuurd naar de geconfigureerde afdeling en rol.',
      case_distributor_role:
        'Wanneer een zaak wordt afgewezen, wordt deze doorgestuurd naar de geconfigureerde afdeling en rol.',
      case_number_prefix:
        'In dit veld kan je een voorlooptekst opgeven die voor het alternatieve zaaknummer wordt gebruikt, bijvoorbeeld ZSNL-2020',
      first_login_confirmation:
        'Deze tekst verschijnt bij een nieuwe gebruiker, wanneer hij/zij haar afdeling heeft geselecteerd.',
      first_login_intro:
        'Deze tekst verschijnt wanneer een gebruiker voor het eerst inlogt in Zaaksysteem.',
      new_user_template:
        'Deze instelling wijzigt het e-mailsjabloon welke verzonden wordt bij het accepteren van een nieuwe gebruiker.',
      disable_dashboard_customization:
        'Standaard is het mogelijk om dashboard-widgets te verplaatsen, toe te voegen en te verwijderen. Als deze instelling aan staat, is deze mogelijkheid er niet.',
      signature_upload_role:
        'Deze instelling geeft aan welke rol rechten heeft om handtekeningen te uploaden.',
      pip_login_intro:
        'Deze tekst verschijnt op de inlogpagina van de PIP. Indien er geen waarde is ingevuld wordt er geen tekst weergegeven.',
      file_username_seperator:
        'Dit is het scheidingsteken waarmee de gebruiker van de bestandsnaam onderscheiden wordt.',
      edit_document_online:
        'Activeer hier de integratie met Zoho Office om online documenten te bewerken. Er zijn kosten verbonden met het activeren van deze functionaliteit.',
      edit_document_msonline:
        'Activeer hier de integratie met Microsoft Office om online documenten te bewerken. Er zijn kosten verbonden met het activeren van deze functionaliteit.',
      document_intake_user:
        'Dit is de virtuele gebruiker waarnaar documenten ge-e-maild kunnen worden die niet direct aan een gebruiker toegewezen moeten of kunnen worden. Dit mag niet matchen met een echte user in LDAP!',
      customer_info_website: 'De URL van uw website.',
      customer_info_naam:
        'De naam van uw organisatie. Deze naam is ook te zien in de URL-balk.',
      customer_info_naam_lang:
        'Deze naam wordt onder meer gebruikt indien er een zaakregistratie heeft plaatsgevonden.',
      customer_info_naam_kort:
        'Deze naam wordt onder meer gebruikt voor e-mailfunctionaliteiten.',
      customer_info_straatnaam: 'De straatnaam van uw vestiging.',
      customer_info_adres: 'Het adres van uw vestiging.',
      customer_info_woonplaats: 'De vestigingsplaats van uw organisatie.',
      customer_info_postbus: 'De postbus van uw vestiging.',
      customer_info_postcode: 'De postcode van uw vestiging.',
      customer_info_postbus_postcode:
        'De postcode van uw postbus van uw vestiging.',
      customer_info_telefoonnummer: 'Het telefoonnummer van uw vestiging.',
      customer_info_faxnummer: 'Het faxnummer van uw vestiging.',
      customer_info_zaak_email:
        "Het e-mailadres voor uw zaakcorrespondentie. Deze optie wordt uitgefaseerd, gebruik de 'Uitgaande e-mailkoppeling in het koppelingsoverzicht'.",
      customer_info_email: 'Uw algemene e-mailadres.',
      customer_info_country_code:
        'De landcode van de organisatie dat Zaaksysteem.nl in gebruik heeft. Deze wordt gebruikt indien een klant vanuit een locatie anders dan Nederland het Zaaksysteem in gebruik heeft.',
      customer_info_gemeente_id_url:
        'Deze URL wordt gebruikt voor het aanvragen van een bedrijven-ID, zoals bijvoorbeeld het mintlabID. Deze wordt steeds minder gebruikt vanwege eHerkenning.',
      customer_info_gemeente_portal:
        'De URL van uw producten en dienstenpagina, deze wordt gebruikt indien een klant een aanvraag annuleert.',
      files_locally_editable:
        'Zet de functionaliteit voor het lokaal bewerken van documenten aan (Zaaksysteem document watcher).',
      preferredChannel:
        'Met deze functionaliteit krijgen personen en organisaties de mogelijkheid om zelf een voorkeurskanaal in te stellen. Dit kan tijdens de stappen van een webformulier of de PIP. Daarnaast heeft de behandelaar de mogelijkheid om het voorkeurskanaal te wijzigen via het contactoverzicht.',
      custom_cors_hosts:
        'Voeg hostnaam toe zodat deze worden toegestaan bijv. zaaksysteem.nl',
      custom_csp_hosts:
        'Voeg hostnaam toe zodat deze worden toegestaan bijv. zaaksysteem.nl',
      iframeable_in_teams:
        'Dit is nodig om de MS Teams-integratie goed te laten werken',
      public_manpage:
        'Deze instelling bepaalt of de /man van uw zaaksysteem publiekelijk beschikbaar is.',
      custom_relation_roles:
        'Extra namen voor rollen van betrokkenen (bovenop de ingebouwde NEN-lijst).',
      customer_info_latitude:
        'De breedtegraad van uw vestiging. Voor gebruik van de kaartfunctionaliteiten.',
      customer_info_longitude:
        'De lengtegraad van uw vestiging. Voor gebruik van de kaartfunctionaliteiten.',
      subject_pip_authorization_confirmation_template_id:
        'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij een betrokkene machtiging op een zaak.',
      requestor_search_extension_active:
        'Deze instelling schakelt de extra zoekknop in het aanvrager dialoog in.',
      requestor_search_extension_name:
        'Voer hier de knoptitel in voor een externe link die op de aanvrager zoek-popup dialog verschijnt.',
      requestor_search_extension_href:
        'Voer het webadres in voor een externe link die op de aanvrager zoek-popup dialog verschijnt.',
      users_can_change_password:
        'Dit werkt alleen voor handmatig aangemaakte gebruikers, niet voor gebruikers die via ADFS zijn aangemaakt.',
      enable_stufzkn_simulator:
        'StUF-ZKN-simulator simuleert een ZSC, en kan gebruikt worden om handmatig StUF-ZKN berichten te sturen naar een StUF-ZKN ZS.',
      pdf_annotations_public:
        'Deze instelling bepaalt welke annotaties verschijnen in de PDF-voorbeeldweergave.',
      allowed_templates: 'Sjablonen die toegestaan zijn voor /form.',
      export_queue_email_template_id:
        'E-mailsjabloon dat gebruikt wordt om de gebruiker te informeren dat het exportbestand klaar is om gedownload te worden.',
      show_object_v1: '',
      show_object_v2: '',
      enable_bil:
        'Als deze instelling aan staat, kan de leverancier inloggen met een authenticatietoken',
    },
  },
};
