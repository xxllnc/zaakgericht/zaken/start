// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IconNameType } from '@mintlab/ui/App/Material/Icon';

export type SectionIdType =
  | 'cases'
  | 'users'
  | 'pip'
  | 'documents'
  | 'about'
  | 'premium'
  | 'other'
  | 'deprecated'
  | 'styles';

type FieldNameType =
  | 'allocation_notification_template_id'
  | 'new_assigned_case_notification_template_id'
  | 'new_document_notification_template_id'
  | 'new_attribute_proposal_notification_template_id'
  | 'new_ext_pip_message_notification_template_id'
  | 'new_int_pip_message_notification_template_id'
  | 'case_term_exceeded_notification_template_id'
  | 'case_suspension_term_exceeded_notification_template_id'
  | 'assignment_department_check'
  | 'case_distributor_group'
  | 'case_distributor_role'
  | 'case_number_prefix'
  | 'show_object_v1'
  | 'show_object_v2'
  | 'first_login_confirmation'
  | 'first_login_intro'
  | 'new_user_template'
  | 'disable_dashboard_customization'
  | 'dashboard_ui_xss_uri'
  | 'signature_upload_role'
  | 'pip_login_intro'
  | 'document_intake_user'
  | 'file_username_seperator'
  | 'edit_document_online'
  | 'customer_info_naam'
  | 'customer_info_naam_lang'
  | 'customer_info_naam_kort'
  | 'customer_info_website'
  | 'customer_info_adres'
  | 'customer_info_postcode'
  | 'customer_info_woonplaats'
  | 'customer_info_postbus'
  | 'customer_info_postbus_postcode'
  | 'customer_info_telefoonnummer'
  | 'customer_info_faxnummer'
  | 'customer_info_email'
  | 'customer_info_zaak_email'
  | 'customer_info_country_code'
  | 'customer_info_gemeente_portal'
  | 'customer_info_gemeente_id_url'
  | 'files_locally_editable'
  | 'edit_document_msonline'
  | 'enable_bil'
  | 'contact_channel_enabled'
  | 'public_manpage'
  | 'custom_relation_roles'
  | 'customer_info_latitude'
  | 'customer_info_longitude'
  | 'subject_pip_authorization_confirmation_template_id'
  | 'export_queue_email_template_id'
  | 'custom_cors_hosts'
  | 'custom_csp_hosts'
  | 'requestor_search_extension_active'
  | 'requestor_search_extension_name'
  | 'requestor_search_extension_href'
  | 'users_can_change_password'
  | 'enable_stufzkn_simulator'
  | 'allowed_templates'
  | 'iframeable_in_teams';

// SETTINGS RESPONSE BODY

type ValueType = any;

type ValueTypeNameType = 'text' | 'uri' | 'boolean' | 'object_ref' | null;

type ValueTypeType = null | {
  options?: {
    format?: 'html';
    choices?: string[];
    constraints?: { name: string[] };
    style?: 'paragraph';
  };
  parent_type_name?: string;
};

type DefinitionResponseBodyType = {
  instance: {
    config_item_name: FieldNameType;
    value_type_name: ValueTypeNameType;
    value_type?: ValueTypeType;
    label: string;
    mutable: boolean;
    mvp: boolean;
  };
};

type ItemResponseBodyType = {
  reference: string;
  instance: { name: FieldNameType; value: ValueType };
};

export type ConfigResponseBodyType = {
  result: {
    instance: {
      definitions: { instance: { rows: DefinitionResponseBodyType[] } };
      items: {
        instance: { rows: ItemResponseBodyType[] };
      };
    };
  };
};

export type FetchDataType = () => Promise<
  ConfigResponseBodyType['result']['instance']
>;

// SAVING SETTINGS

export type EditFormStateType = {
  [key: string]: any;
};

export type FlattenValueType = (value: any) => string | null;

export type SubmitDataType = (data: {
  items: {
    [key: string]: { value: any };
  };
}) => Promise<any>;

// SETTINGS STRUCTURE

type FieldSetStructureType = {
  title: string;
  description?: string;
  fields: FieldNameType[];
};

type SectionStructureType = {
  id: SectionIdType;
  icon: IconNameType;
  fieldSets: FieldSetStructureType[];
};

export type SettingsStructureType = SectionStructureType[];

// FORMAT SETTINGS STRUCTURE

export type FieldTypeType =
  | 'text'
  | 'checkbox'
  | 'select'
  | 'creatable'
  | 'richtext';

type FieldOptionsType = {
  isMultiline: boolean;
  rows: number;
  disabled: boolean;
  searchConfig: {
    url: string;
    match?: string;
  };
};

export type GetFieldTypeType = (
  config_item_name: FieldNameType,
  value_type_name: ValueTypeNameType,
  value_type?: ValueTypeType
) => FieldTypeType;

type DefinitionType = {
  name: string;
  type: FieldTypeType;
  label: string;
  options: FieldOptionsType;
};

export type FormatDefinitionsType = (
  definitions: DefinitionResponseBodyType[]
) => DefinitionType[];

export type ConvertValueV1ObjectType = (value: SearchV1ResultType) => {
  label: string;
  value: string;
};

export type ConvertValueV2ObjectType = (value: SearchV2ResultType) => {
  label: string;
  value: string;
};

export type GetValueType = (name: FieldNameType, value: ValueType) => any;

export type ItemType = {
  uuid: string;
  name: string;
  value: ValueType;
};

export type FormatItemsType = (items: ItemResponseBodyType[]) => ItemType[];

export type FieldsType = {
  uuid: string;
  name: string;
  type: FieldTypeType;
  label: string;
  value: ValueType;
  options: FieldOptionsType;
};

export type CreateFieldsType = (
  definitions: DefinitionType[],
  items: ItemType[]
) => FieldsType[];

export type FieldSetType = {
  title: string;
  description?: string;
  fields: FieldsType[];
};

export type SectionType = {
  id: SectionIdType;
  icon: IconNameType;
  fieldSets: FieldSetType[];
};

export type SettingsType = SectionType[];

// SEARCH

type SearchV1ResultType = {
  reference: string;
  instance: { label?: string; name?: string };
};

export type SearchResponseBodyType = {
  result: {
    instance: {
      rows: SearchV1ResultType[];
    };
  };
};

export type SearchV1Type = (
  baseUrl: string,
  match: string,
  keyword: string | undefined
) => Promise<SearchV1ResultType[]>;

type SearchV2ResultType = {
  id: string;
  meta: {
    summary: string;
  };
};

export type SearchV2ResponseBodyType = {
  data: SearchV2ResultType[];
};

export type SearchV2Type = (
  baseUrl: string,
  match: string,
  keyword: string
) => Promise<SearchV2ResultType[]>;
