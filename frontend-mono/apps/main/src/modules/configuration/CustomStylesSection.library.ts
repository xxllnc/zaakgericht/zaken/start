// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { useQuery, useMutation } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError, openSnackbar } from '@zaaksysteem/common/src/signals';
import { queryClient } from '../../queryClient';

const GET_TENANTS = 'GET_TENANTS';
export const useGetTenantsQuery = () =>
  useQuery([GET_TENANTS], async () => {
    const res = await request('GET', '/api/v2/style/tenants').catch(
      openServerError
    );

    return (
      res
        ? res.data.map((tenant: any) => ({
            template: tenant.attributes.template,
            label: tenant.attributes.name,
          }))
        : []
    ) as {
      template: string;
      label: string;
    }[];
  });

export const useUploadStylesTemplateMutation = () =>
  useMutation(
    ['UPLOAD_STYLES', Math.random().toString()],
    async ({ file, tenant }: { file: File; tenant: string }) => {
      const fd = new FormData();
      fd.append('file', file);
      fd.append('tenant', tenant);

      return request('POST', '/api/v2/style/upload_template', fd, {
        type: 'formdata',
        cached: false,
      }).catch(openServerError);
    },
    {
      onSuccess: () => {
        openSnackbar('configuration:styles.templateUploaded');
        queryClient.invalidateQueries([GET_TENANTS]);
      },
    }
  );
