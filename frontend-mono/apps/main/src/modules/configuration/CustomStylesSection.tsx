// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Button from '@mintlab/ui/App/Material/Button';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table/Table';
import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';
import TableHead from '@mui/material/TableHead/TableHead';
import TableBody from '@mui/material/TableBody/TableBody';
import {
  useGetTenantsQuery,
  useUploadStylesTemplateMutation,
} from './CustomStylesSection.library';

const CustomStylesSection = () => {
  const { data, isLoading } = useGetTenantsQuery();
  const [t] = useTranslation('configuration');
  const [tenant, setTenant] = React.useState('');
  const { mutateAsync } = useUploadStylesTemplateMutation();
  const uploadElId = 'ctupload';

  return isLoading ? (
    <Loader />
  ) : (
    <Box sx={{ margin: '20px' }}>
      <SubHeader
        title={t('titles.template')}
        description={t('descriptions.selectEnv')}
      />
      <input
        id={uploadElId}
        onChange={ev => {
          const file = ev.target.files && ev.target.files[0];
          file &&
            mutateAsync({
              file,
              tenant,
            }).finally(() => {
              ev.target.value = '';
            });
        }}
        style={{ width: 0 }}
        type="file"
        accept=".zss"
      />
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{t('styles.host')}</TableCell>
            <TableCell>{t('styles.template')}</TableCell>
            <TableCell sx={{ paddingLeft: '31px' }}>
              {t('styles.actions')}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data?.map((el, ix) => {
            return (
              <TableRow key={ix}>
                <TableCell>{el.label}</TableCell>
                <TableCell>{el.template || '-'}</TableCell>
                <TableCell>
                  <Button
                    name="downloadStylesTemplate"
                    disabled={!el.template}
                    icon="download"
                    target="_blank"
                    href={`https://${el.label}/api/v2/style/get_content?name=${el.template}`}
                  />
                  <Button
                    name="uploadStylesTemplate"
                    icon="upload_file"
                    onClick={() => {
                      setTenant(el.label);
                      document.getElementById(uploadElId)?.click();
                    }}
                  />
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Box>
  );
};

export default CustomStylesSection;
