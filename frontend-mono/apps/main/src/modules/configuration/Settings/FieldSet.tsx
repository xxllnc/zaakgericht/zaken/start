// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { Formik, Form, Field, FormikProps } from 'formik';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import { CreatableSelect } from '@mintlab/ui/App/Zaaksysteem/Select/';
import { Wysiwyg } from '@mintlab/ui/App/Zaaksysteem/Wysiwyg';
import { FormSelect } from '@mintlab/ui/App/Zaaksysteem/Select';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import Button from '@mintlab/ui/App/Material/Button';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { searchV1, searchV2 } from '../Configuration.requests';
import { EditFormStateType, FieldsType } from '../Configuration.types';
import { FieldSetType } from './../Configuration.types';
import { useFieldSetStyles } from './FieldSet.style';
import { getInitialState } from './Settings.library';
import {
  convertValueV1Object,
  ConvertValueV2Object,
  useSettingsMutation,
} from './../Configuration.library';

const fieldTypeMap: {
  [key: string]: any;
} = {
  text: TextField,
  richtext: Wysiwyg,
  checkbox: Checkbox,
  select: FormSelect,
  creatable: CreatableSelect,
};

const FieldComponent = ({ field, form, ...props }: any) => {
  const Component = fieldTypeMap[props.type] || TextField;

  return <Component {...field} {...props} checked={Boolean(props.value)} />;
};

const SearchFieldComponent = ({
  field,
  form,
  searchConfig,
  customKeyword,
  ...props
}: any) => {
  const Component = fieldTypeMap[props.type] || TextField;
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const enabled = Boolean(input) || Boolean(customKeyword);
  const { url, match } = searchConfig;

  const { data } = useQuery<
    {
      label: string;
      value: string;
    }[],
    V2ServerErrorsType
  >(
    [`search-${field.name}`, input, customKeyword],
    async ({ queryKey: [__, keyword] }) => {
      if (customKeyword) {
        const options = await searchV2(url, match, customKeyword);

        return options.map(ConvertValueV2Object);
      } else {
        const options = await searchV1(url, match, keyword as string);

        return options.map(convertValueV1Object);
      }
    },
    { enabled, onError: openServerError }
  );

  return (
    <Component
      {...field}
      {...props}
      checked={Boolean(props.value)}
      choices={data}
      onInputChange={(ev: any, val: any, reason: any) =>
        ['input', 'clear'].includes(reason) && setInput(val)
      }
    />
  );
};

const FieldSet: React.ComponentType<FieldSetType> = ({
  title,
  description,
  fields,
}) => {
  const classes = useFieldSetStyles();
  const [t] = useTranslation('configuration');

  const width = useWidth();
  const isCompact = ['xs', 'sm'].includes(width);

  const formikRef = useRef<FormikProps<EditFormStateType> | null>(null);
  const initialValues = getInitialState(fields);

  const tooltips = t('help', { returnObjects: true }) as {
    [key: string]: string;
  };

  const { mutateAsync: save, isLoading: saving } = useSettingsMutation();

  return (
    <div className={classes.fieldSetWrapper}>
      <SubHeader
        title={t(`titles.${title}`)}
        description={description ? t(`descriptions.${description}`) : undefined}
      />
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        isInitialValid={false}
        onSubmit={save}
      >
        {formik => {
          if (!formikRef.current) formikRef.current = formik;

          return (
            // @ts-ignore
            <Form>
              <div className={classes.fieldsWrapper}>
                {fields.map(field => {
                  const { uuid, name, type, label, options } = field;
                  const tooltip = tooltips[name];
                  let customKeyword;

                  if (name === 'case_distributor_role') {
                    const groupField = fields.find(
                      field => field.name === 'case_distributor_group'
                    ) as FieldsType;

                    customKeyword = formik.values[groupField.uuid]?.value;
                  }

                  return (
                    <div
                      key={name}
                      className={classNames({
                        [classes.fieldWrapper]: !isCompact,
                        [classes.fieldWrapperCompact]: isCompact,
                      })}
                    >
                      <div className={classes.labelWrapper}>
                        <span className={classes.label}>{label}</span>
                        {tooltip && (
                          <Tooltip
                            className={classes.icon}
                            title={t(`help.${name}`)}
                          >
                            <Icon size="small">{iconNames.help_outline}</Icon>
                          </Tooltip>
                        )}
                      </div>
                      <div
                        className={classNames(classes.input, {
                          [classes.inputBackgroundColor]: type === 'richtext',
                        })}
                      >
                        <Field
                          name={uuid}
                          type={type}
                          value={formik.values[`${uuid}`]}
                          customKeyword={customKeyword}
                          component={
                            options.searchConfig
                              ? SearchFieldComponent
                              : FieldComponent
                          }
                          {...options}
                        />
                      </div>
                    </div>
                  );
                })}
              </div>
              {formik.dirty && (
                <Button
                  name={`save-config-${title}`}
                  onClick={formik.submitForm}
                  disabled={saving}
                >
                  {t('common:verbs.save')}
                </Button>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FieldSet;
