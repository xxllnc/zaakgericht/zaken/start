// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SectionIdType, SettingsType } from '../Configuration.types';
import { useSettingsStyles } from './Settings.style';
import { getFieldSets } from './Settings.library';
import FieldSet from './FieldSet';

const Settings: React.ComponentType<{
  sectionId: SectionIdType;
  settings: SettingsType;
}> = ({ sectionId, settings }) => {
  const classes = useSettingsStyles();
  const fieldSets = getFieldSets(sectionId, settings);

  return (
    <div className={classes.fieldSetsWrapper}>
      {fieldSets.map((fieldSet, index) => (
        <FieldSet {...fieldSet} key={index} />
      ))}
    </div>
  );
};

export default Settings;
