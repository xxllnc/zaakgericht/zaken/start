// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useStyles } from '../ContactSearch.styles';
import { ContactType, ResultType } from '../ContactSearch.types';
import { getColumns } from './getColumns';

export interface ResultsTablePropsType {
  type: ContactType;
  results?: ResultType[];
  setResults: (results: ResultType[] | undefined) => void;
  isCompact: boolean;
}

const ResultsTable: React.FunctionComponent<ResultsTablePropsType> = ({
  type,
  results,
  setResults,
  isCompact,
}) => {
  const classes = useStyles();
  const tableStyles = useSortableTableStyles();
  const [t] = useTranslation('contactSearch');
  const navigate = useNavigate();

  const noResults = t('results.noResults');
  const resultsFound = t('results.resultsFound', { count: results?.length });

  return !results ? (
    <div className={classes.placeholder}>{t('results.searchForResults')}</div>
  ) : (
    <div className={classes.resultsWrapper}>
      {isCompact && (
        <Button
          action={() => setResults(undefined)}
          name="clearResults"
          sx={{ width: 200, margin: 2.5 }}
          variant="outlined"
        >
          {t('results.button')}
        </Button>
      )}
      {results.length > 10 && type !== 'employee' ? (
        <div className={classes.placeholder} aria-live="polite" role="region">
          {t('results.tooManyResults')}
        </div>
      ) : (
        <div
          style={{
            flex: '1 1 auto',
            height: '100%',
            width: '100%',
          }}
        >
          <span
            className={classes.resultsAria}
            aria-live="polite"
            role="region"
          >
            {results.length ? resultsFound : noResults}
          </span>
          <SortableTable
            styles={tableStyles}
            rows={results}
            columns={getColumns(t as any, type)}
            noRowsMessage={noResults}
            onRowClick={({ rowData }: any) => {
              navigate(`/main/contact-view/${type}/${rowData.uuid}`);
            }}
          />
        </div>
      )}
    </div>
  );
};

export default ResultsTable;
