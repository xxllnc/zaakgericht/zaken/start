// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { COUNTRY_CODE_CURACAO } from '@zaaksysteem/common/src/constants/countryCodes.constants';
import Button from '@mintlab/ui/App/Material/Button';
import { useStyles } from '../ContactSearch.styles';
import { ContactType, SearchType } from '../ContactSearch.types';
import { getFormDefinition, getCanSubmit } from './formDefinitions';

export interface SearchFormPropsType {
  type: ContactType;
  search: SearchType;
  countryCode?: string;
}

const SearchForm: React.FunctionComponent<SearchFormPropsType> = ({
  type,
  search,
  countryCode,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('contactSearch');
  const seperatorLabel = t('common:or').toUpperCase();
  const supportsSedulaNumber = countryCode === COUNTRY_CODE_CURACAO;

  const formDefinition = getFormDefinition[type]({
    t: t as any,
    supportsSedulaNumber,
  });
  const validationMap = generateValidationMap(formDefinition);

  let {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition,
    validationMap,
  });
  const canSubmit = getCanSubmit(type, isValid, values);

  const ref = useRef({ values, canSubmit });
  useEffect(() => {
    ref.current = { values, canSubmit };
  }, [values, canSubmit]);

  const listener = (event: any) => {
    const isSubmitKey = ['Enter', 'NumpadEnter'].includes(event.code);
    const isTextField = event.target.type === 'text';

    if (isSubmitKey && isTextField && ref.current.canSubmit) {
      search(ref.current.values);
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', listener);

    return () => {
      document.removeEventListener('keydown', listener);
    };
  }, []);

  const required = values.excludeAuthenticated === false;

  return (
    <div className={classes.searchWrapper}>
      <SubHeader
        title={t('title', {
          type: t(`common:entityType.${type}`),
        })}
        description={t('subTitle', {
          type: t(`common:entityType.${type}`).toLowerCase(),
        })}
      />
      <div>
        {fields.map(
          (
            { FieldComponent, key, type, manualRequired, label, ...rest }: any,
            index: number
          ) =>
            rest.isLabel ? (
              required && (
                <div
                  key={`${rest.name}-${index}-formcontrol-wrapper`}
                  className={classes.separator}
                >
                  {seperatorLabel}
                </div>
              )
            ) : (
              <FormControlWrapper
                {...rest}
                label={required && manualRequired ? `${label} *` : label}
                key={`${rest.name}-formcontrol-wrapper`}
              >
                <FieldComponent {...rest} t={t} />
              </FormControlWrapper>
            )
        )}
      </div>
      <Button
        action={() => {
          search(values);
        }}
        disabled={!canSubmit}
        name="contactSearchFormSubmit"
        sx={{ width: '200px' }}
      >
        {t('search.button')}
      </Button>
    </div>
  );
};

export default SearchForm;
