// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import fecha from 'fecha';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { GetColumnsType } from '../ContactSearch.types';

const personColumns = [
  { name: 'externalSubscription', width: 40 },
  { name: 'name', width: 1, flexGrow: 1 },
  { name: 'address', width: 1, flexGrow: 1 },
  { name: 'dateOfBirth', width: 150 },
];

const organizationColumns = [
  { name: 'externalSubscription', width: 40 },
  { name: 'coc', width: 150 },
  { name: 'name', width: 1, flexGrow: 1 },
  { name: 'address', width: 1, flexGrow: 1 },
];

const employeeColumns = [
  { name: 'department', width: 200 },
  { name: 'name', width: 1, flexGrow: 1 },
];

const columnTypes = {
  person: personColumns,
  organization: organizationColumns,
  employee: employeeColumns,
};

/* eslint complexity: [2, 9] */
export const getColumns: GetColumnsType = (t, type) => {
  return columnTypes[type].map(column => ({
    ...column,
    label: t(`results.columns.${column.name}`),
    disableSort: true,
    cellRenderer: ({ dataKey, rowData }: any) => {
      switch (dataKey) {
        case 'name': {
          return (
            <Link to={`/main/contact-view/${type}/${rowData.uuid}`}>
              {rowData[dataKey]}
            </Link>
          );
        }
        case 'address': {
          const address = rowData[dataKey].map(
            (addr: string, index: number) => (
              <div key={`${addr}-${index}`}>{addr}</div>
            )
          );

          return (
            <Tooltip title={address} placement="left">
              {address}
            </Tooltip>
          );
        }
        case 'externalSubscription': {
          const icon = rowData.externalSubscription
            ? iconNames.lock
            : iconNames.lock_open;
          const tooltip = rowData.externalSubscription
            ? t('results.externalSubscription.true')
            : t('results.externalSubscription.false');

          return (
            <Tooltip title={tooltip}>
              <Icon size="extraSmall">{icon}</Icon>
            </Tooltip>
          );
        }
        case 'dateOfBirth': {
          const date = rowData.dateOfBirth;
          const dateOfBirth = date
            ? fecha.format(new Date(rowData.dateOfBirth), 'DD-MM-YYYY')
            : '-';

          return <span>{dateOfBirth}</span>;
        }
        default:
          return <span>{rowData[dataKey] || '-'}</span>;
      }
    },
  }));
};
