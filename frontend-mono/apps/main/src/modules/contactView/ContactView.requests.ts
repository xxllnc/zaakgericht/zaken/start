// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';
import { SubjectType } from './ContactView.types';

type FetchSubjectType = (
  uuid: string,
  type: SubjectTypeType
) => Promise<SubjectType>;

export const fetchSubject: FetchSubjectType = async (uuid, type) => {
  const url = buildUrl<APICaseManagement.GetContactRequestParams>(
    '/api/v2/cm/contact/get_contact',
    {
      uuid,
      type,
    }
  );
  const result = await request('GET', url);

  return result.data;
};
