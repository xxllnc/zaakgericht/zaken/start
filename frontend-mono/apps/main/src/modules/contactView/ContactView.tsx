// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import Information from './information/Information';
import Communication from './communication/Communication';
import LocationModule from './location/Location';
import Relationship from './relationship/Relationship';
import ControlPanel from './controlPanel';
import Cases from './cases/Cases';
import { ContactViewSideMenu } from './ContactViewSideMenu';
import { useStyles } from './ContactView.styles';
import ContactTimeline from './timeline';
import { SubjectType } from './ContactView.types';

export type ContactViewPropsType = {
  uuid: string;
  type: SubjectTypeType;
  subject: SubjectType;
  refreshSubject: () => void;
};

const ContactView: React.FunctionComponent<ContactViewPropsType> = ({
  uuid,
  type,
  subject,
  refreshSubject,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.contentWrapper}>
      <PanelLayout>
        <Panel type="side">
          <ContactViewSideMenu />
        </Panel>
        <Panel className={classes.content}>
          <Routes>
            <Route path="" element={<Navigate to="data" replace={true} />} />
            <Route
              path={'data/*'}
              element={
                <Information
                  subject={subject}
                  refreshSubject={refreshSubject}
                />
              }
            />
            <Route
              path={'communication/*'}
              element={<Communication uuid={uuid} />}
            />
            <Route path={`cases`} element={<Cases subject={subject} />} />
            <Route
              path={'map/*'}
              element={<LocationModule subject={subject} />}
            />
            <Route path={'relationships/*'} element={<Relationship />} />
            <Route
              path={'timeline/*'}
              element={<ContactTimeline uuid={uuid} type={type} />}
            />
            <Route
              path={`environments`}
              element={<ControlPanel uuid={uuid} />}
            />
          </Routes>
        </Panel>
      </PanelLayout>
    </div>
  );
};

export default ContactView;
