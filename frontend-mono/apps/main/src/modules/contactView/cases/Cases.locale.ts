// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    columns: {
      number: '#',
      casetype: 'Zaaktype',
      status: 'Status',
      summary: 'Extra informatie',
      roles: 'Rol',
      progress: 'Voortgang',
      daysLeft: 'Dagen',
    },
    noRowsMessage: 'Geen resultaten.',
    loadingMessage: 'Bezig met laden…',
    statusType: {
      new: 'Nieuw',
      open: 'In behandeling',
      stalled: 'Opgeschort',
      resolved: 'Afgehandeld',
      transfer: 'Over te dragen',
      destroy: 'Te vernietigen',
    },
    selectPlaceholder: 'Selecteer overzicht…',
    search: 'Zoeken…',
    casesFor: 'Zaken voor ',
    openCasesFor: 'Openstaande zaken voor ',
    authorizedCases: 'Gemachtigd voor zaken',
    involdedInCases: 'Betrokken bij zaken',
    addressCases: 'Zaken op ',
  },
};
