// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SortDirectionType } from 'react-virtualized';
import { APICaseManagement } from '@zaaksysteem/generated';

export type SelectFilterType =
  | 'owned'
  | 'ownedOpen'
  | 'authorized'
  | 'involdedIn'
  | 'shared_address';

export type CasesRowType = {
  number: number;
  casetype: string;
  status: StatusType | 'transfer' | 'destroy';
  summary?: string;
  roles: string[];
  progress: number;
  daysLeft: number;
  uuid: string;
  name: string;
};

export type rowType =
  APICaseManagement.GetContactRelatedCasesResponseBody['data'];
export type DaysLeftType =
  APICaseManagement.GetContactRelatedCasesResponseBody['data.attributes.days_left'];
export type StatusType =
  APICaseManagement.GetContactRelatedCasesResponseBody['data.attributes.status'];
export type ArchivalStatusType =
  APICaseManagement.GetContactRelatedCasesResponseBody['data.attributes.archive_status'];
export type FilterType = String | null;

export type ParamsType =
  | {
      [key: string]: string;
    }
  | undefined;

export type DataParams = {
  params?: ParamsType;
  selectFilter: SelectFilterType | null;
  textFilter: string;
  pageLength: number;
  uuid: string;
  sortBy: string | null;
  sortDirection: SortDirectionType | null;
  pageNum?: number;
};
