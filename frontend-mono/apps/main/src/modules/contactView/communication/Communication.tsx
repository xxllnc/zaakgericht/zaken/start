// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import CommunicationModule from '@zaaksysteem/communication-module/src';

const Communication = ({ uuid }: { uuid: string }) => {
  return (
    <CommunicationModule
      capabilities={{
        allowSplitScreen: true,
        canAddAttachmentToCase: true,
        canAddSourceFileToCase: true,
        canAddThreadToCase: false,
        canCreateContactMoment: true,
        canCreatePipMessage: false,
        canCreateEmail: false,
        canCreatePostex: false,
        canMarkUnread: false,
        canCreateNote: true,
        canDeleteMessage: true,
        canImportMessage: false,
        canSelectCase: true,
        canSelectContact: false,
        canFilter: true,
        canOpenPDFPreview: true,
      }}
      context="contact"
      contactUuid={uuid}
    />
  );
};

export default Communication;
