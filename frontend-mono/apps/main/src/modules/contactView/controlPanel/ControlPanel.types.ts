// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type CustomerTypeType =
  | 'commercial'
  | 'government'
  | 'lab'
  | 'development'
  | 'staging'
  | 'acceptance'
  | 'preprod'
  | 'testing'
  | 'production';

export type OtapType = 'development' | 'testing' | 'accept' | 'production';

export type SoftwareVersionType = 'master';

export type ControlPanelType = {
  uuid: string;
  template: string;
  shortname: string;
  readOnly: 0 | 1 | null;
  customerType: CustomerTypeType;
  allowedDiskspace: number;
  allowedEnvironments: number;
  availableTemplates: string[];
};

export type EnvironmentType = {
  name: string;
  uuid: string;
  label: string;
  fqdn: string;
  diskspace: number;
  hosts: { fqdn: string; uuid: string }[];
  status: 'active' | 'processing' | 'disabled';
  template: string;
  otap: OtapType;
  customerType: CustomerTypeType;
  softwareVersion: string;
  fallbackUrl: string;
  servicesDomain: string;
  apiDomain: string;
  database: string;
  databaseHost: string;
  filestore: string;
  freeformReference: string;
  protected: boolean;
  disabled: boolean;
  oidcOrganizationId: string;
};

export type HostType = {
  uuid: string;
  label: string;
  fqdn: string;
  sslCert: string;
  sslKey: string;
  template: string;
  environment?: EnvironmentType;
};

export type ControlPanelDataType = {
  id: string;
  controlPanel?: ControlPanelType;
};

export type GetControlPanelType = (
  uuid: string
) => Promise<ControlPanelDataType>;

export type SetCustomerTypeType = (
  id: string,
  customerType: CustomerTypeType
) => Promise<void>;

export type FormatEnvironmentType = (response: any) => EnvironmentType;

export type FindEnvironmentType = (
  hostUuid: string,
  environments: EnvironmentType[]
) => EnvironmentType | undefined;

export type FormatHostType = (
  response: any,
  environments: EnvironmentType[]
) => HostType;

export type DataType = {
  environments: EnvironmentType[];
  hosts: HostType[];
};

export type GetDataType = (controlPanelUuid: string) => Promise<DataType>;

export type SaveEnvironmentType = (
  controlPanel: ControlPanelType,
  values: any,
  environment: EnvironmentType | null
) => Promise<void>;

export type SaveHostType = (
  controlPanel: ControlPanelType,
  values: any,
  host: HostType | null
) => Promise<void>;
