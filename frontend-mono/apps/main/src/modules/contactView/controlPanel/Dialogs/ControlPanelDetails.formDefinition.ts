// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { ControlPanelType, EnvironmentType } from '../ControlPanel.types';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  controlPanel: ControlPanelType,
  environments: EnvironmentType[]
) => AnyFormDefinitionField[];

export const getFormDefinition: GetFormDefinitionType = (
  t,
  controlPanel,
  environments
) => {
  const allowedDiskspace = controlPanel.allowedDiskspace;
  const allowed =
    allowedDiskspace || allowedDiskspace === 0 ? allowedDiskspace : '∞';
  const usedDiskSpace = environments.reduce(
    (diskspace, environment) => diskspace + environment.diskspace,
    0
  );

  return [
    {
      name: 'template',
      value: controlPanel.template,
    },
    {
      name: 'shortname',
      value: controlPanel.shortname,
    },
    {
      name: 'customerType',
      value: t(`environments.customerTypes.${controlPanel.customerType}`),
    },
    {
      name: 'readOnly',
      value: controlPanel.readOnly ? t('common:yes') : t('common:no'),
    },
    {
      name: 'diskspace',
      value: `${usedDiskSpace} / ${allowed} GB`,
    },
  ].map(field => ({
    ...field,
    label: t(`controlPanel.details.${field.name}`),
    readOnly: true,
    type: fieldTypes.TEXT,
  }));
};
