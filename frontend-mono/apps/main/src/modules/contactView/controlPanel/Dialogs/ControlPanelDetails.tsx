// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ControlPanelType, EnvironmentType } from '../ControlPanel.types';
import { getFormDefinition } from './ControlPanelDetails.formDefinition';

type ControlPanelDetailsDialogPropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  onClose: () => void;
  open: boolean;
};

const ControlPanelDetailsDialog: React.ComponentType<
  ControlPanelDetailsDialogPropsType
> = ({ controlPanel, environments, onClose, open }) => {
  const [t] = useTranslation('controlPanel');

  return (
    <FormDialog
      saving={false}
      icon={iconNames.info}
      title={controlPanel.template}
      formDefinitionT={t as any}
      onClose={onClose}
      formDefinition={getFormDefinition(t as any, controlPanel, environments)}
      open={open}
      compact={false}
    />
  );
};

export default ControlPanelDetailsDialog;
