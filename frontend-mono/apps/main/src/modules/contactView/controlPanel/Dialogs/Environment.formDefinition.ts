// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { ControlPanelType, EnvironmentType } from '../ControlPanel.types';
import {
  customerTypes,
  otapTypes,
  softwareVersions,
} from '../ControlPanel.constants';
import { FQDN } from './Fields/FqdnField';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  controlPanel: ControlPanelType,
  environment: EnvironmentType | null
) => AnyFormDefinitionField[];

/* eslint complexity: [2, 8] */
export const getFormDefinition: GetFormDefinitionType = (
  t,
  controlPanel,
  environment
) => {
  const formDefinition = [
    {
      name: 'label',
      type: fieldTypes.TEXT,
      value: environment?.label || '',
      required: true,
      label: t('environments.fields.label'),
    },
    {
      name: 'fqdn',
      type: FQDN,
      value: environment?.fqdn?.split('.')[0] || '',
      required: true,
      label: t('environments.fields.fqdn'),
      config: {
        customerType: controlPanel.customerType,
      },
    },
    ...(environment
      ? []
      : [
          {
            name: 'customerType',
            type: fieldTypes.SELECT,
            value: controlPanel.customerType,
            required: false,
            isClearable: false,
            nestedValue: true,
            label: t('environments.fields.customerType'),
            choices: customerTypes.map(customerType => ({
              label: t(`environments.customerTypes.${customerType}`),
              value: customerType,
            })),
          },
        ]),
    {
      name: 'otap',
      type: fieldTypes.SELECT,
      value: environment?.otap || 'development',
      required: false,
      isClearable: false,
      nestedValue: true,
      label: t('environments.fields.otap'),
      choices: otapTypes.map(otap => ({
        label: t(`environments.otapTypes.${otap}`),
        value: otap,
      })),
    },
    ...(environment
      ? []
      : [
          {
            name: 'softwareVersion',
            type: fieldTypes.SELECT,
            value: 'master',
            required: false,
            disabled: true,
            nestedValue: true,
            label: t('environments.fields.softwareVersion'),
            choices: softwareVersions.map(softwareVersion => ({
              label: t(`environments.softwareVersions.${softwareVersion}`),
              value: softwareVersion,
            })),
          },
        ]),
    {
      name: 'template',
      type: fieldTypes.SELECT,
      value: environment?.template || null,
      required: false,
      nestedValue: true,
      label: t('environments.fields.template'),
      choices: controlPanel.availableTemplates.map(template => ({
        label: template,
        value: template,
      })),
      placeholder: t('environments.placeholders.template'),
    },
    {
      name: 'password',
      type: fieldTypes.TEXT,
      displayType: 'password',
      autoComplete: 'new-password',
      value: '',
      label: t('environments.fields.password'),
    },
    {
      name: 'oidcOrganizationId',
      type: fieldTypes.TEXT,
      value: environment?.oidcOrganizationId || '',
      required: false,
      label: t('environments.fields.oidcOrganizationId'),
    },
  ];

  return formDefinition;
};
