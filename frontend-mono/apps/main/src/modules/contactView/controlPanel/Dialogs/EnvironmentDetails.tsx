// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { EnvironmentType } from '../ControlPanel.types';
import { getFormDefinition } from './EnvironmentDetails.formDefinition';

type EnvironmentDetailsDialogPropsType = {
  environment: EnvironmentType;
  onClose: () => void;
  open: boolean;
};

const EnvironmentDetailsDialog: React.ComponentType<
  EnvironmentDetailsDialogPropsType
> = ({ environment, onClose, open }) => {
  const [t] = useTranslation('controlPanel');

  return (
    <FormDialog
      saving={false}
      icon={iconNames.info}
      title={environment.label}
      formDefinitionT={t as any}
      onClose={onClose}
      formDefinition={getFormDefinition(t as any, environment)}
      open={open}
      compact={false}
    />
  );
};

export default EnvironmentDetailsDialog;
