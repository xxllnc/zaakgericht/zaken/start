// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ComponentType } from 'react';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { CustomerTypeType } from '../../ControlPanel.types';
import { useStyles } from './FqdnField.style';

export const FQDN = 'fqdn';

export interface FqdnFieldPropsType
  extends FormFieldPropsType<any, { customerType: CustomerTypeType }, string> {}

const domains = {
  commercial: '.zaaksysteem.net',
  government: '.zaaksysteem.nl',
  lab: '.lab.zaaksysteem.app',
  development: '.dev.zaaksysteem.app',
  staging: '.stg.zaaksysteem.app',
  acceptance: '.acc.zaaksysteem.app',
  testing: '.tst.zaaksysteem.app',
  preprod: '.pre.zaaksysteem.app',
  production: '.prd.zaaksysteem.app',
};

export const FqdnField: ComponentType<FqdnFieldPropsType> = props => {
  const classes = useStyles();
  const { name, setFieldValue, value, error, config } = props;
  const domain = domains[config?.customerType || 'government'];

  const formDefinition = [
    {
      name: FQDN,
      type: fieldTypes.TEXT,
      value,
    },
  ];

  const { fields } = useForm({
    onChange: values => {
      const val = values['fqdn'] || '';

      // @ts-ignore
      setFieldValue(name, val);
    },
    formDefinition: formDefinition,
  });

  return (
    <div className={classes.wrapper}>
      <div className={classes.fieldWrapper}>
        {fields.map(({ FieldComponent, ...rest }) => (
          <FieldComponent key={name} error={error} {...rest} />
        ))}
      </div>
      <span className={classes.domain}>{domain}</span>
    </div>
  );
};

export default FqdnField;
