// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  ControlPanelType,
  EnvironmentType,
  HostType,
} from '../ControlPanel.types';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  host: HostType | null,
  controlPanel: ControlPanelType,
  environments: EnvironmentType[]
) => AnyFormDefinitionField[];

/* eslint complexity: [2, 8] */
export const getFormDefinition: GetFormDefinitionType = (
  t,
  host,
  controlPanel,
  environments
) => {
  const formDefinition = [
    {
      name: 'label',
      type: fieldTypes.TEXT,
      value: host?.label || '',
      required: true,
      label: t('hosts.fields.label'),
    },
    {
      name: 'fqdn',
      type: fieldTypes.TEXT,
      value: host?.fqdn || '',
      required: true,
      label: t('hosts.fields.fqdn'),
      placeholder: t('hosts.placeholders.fqdn'),
    },
    {
      name: 'sslCert',
      type: fieldTypes.TEXTAREA,
      value: host?.sslCert || '',
      required: false,
      label: t('hosts.fields.sslCert'),
    },
    {
      name: 'sslKey',
      type: fieldTypes.TEXTAREA,
      value: host?.sslKey || '',
      required: false,
      label: t('hosts.fields.sslKey'),
    },
    // can't save a template on create
    ...(host
      ? [
          {
            name: 'template',
            type: fieldTypes.SELECT,
            value: host.template || null,
            required: false,
            isClearable: true,
            nestedValue: true,
            label: t('hosts.fields.template'),
            choices: controlPanel.availableTemplates.map(template => ({
              label: template,
              value: template,
            })),
            placeholder: t('hosts.placeholders.template'),
          },
        ]
      : []),
    {
      name: 'environment',
      type: fieldTypes.SELECT,
      value: host?.environment?.uuid || null,
      required: false,
      isClearable: true,
      nestedValue: true,
      label: t('hosts.fields.environment'),
      choices: environments.map(environment => ({
        label: `${environment.label} (${environment.fqdn})`,
        value: environment.uuid,
      })),
      placeholder: t('hosts.placeholders.environment'),
    },
  ];

  return formDefinition;
};
