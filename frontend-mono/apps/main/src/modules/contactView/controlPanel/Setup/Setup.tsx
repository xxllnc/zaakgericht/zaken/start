// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { setCustomerType } from '../ControlPanel.requests';
import { customerTypes } from '../ControlPanel.constants';
import { useStyles } from '../ControlPanel.styles';

type SetupPropsType = {
  id: string;
  refetch: () => void;
};

const Setup: React.FunctionComponent<SetupPropsType> = ({ id, refetch }) => {
  const classes = useStyles();
  const [t] = useTranslation('controlPanel');

  return (
    <div className={classes.setup}>
      <span>{t('setup.intro')}</span>
      {customerTypes.map(customerType => (
        <Button
          key={customerType}
          variant="outlined"
          action={() => {
            setCustomerType(id, customerType)
              .then(refetch)
              .catch(openServerError);
          }}
          name={customerType}
          sx={{ width: 300 }}
        >
          {`${t(`environments.customerTypes.${customerType}`)} ${t('common:verbs.activate').toLowerCase()}`}
        </Button>
      ))}
    </div>
  );
};

export default Setup;
