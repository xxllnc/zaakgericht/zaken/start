// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useQuery } from '@tanstack/react-query';
import locale from './ControlPanel.locale';
import ControlPanel from './ControlPanel';
import { ControlPanelDataType } from './ControlPanel.types';
import { CONTROL_PANEL } from './ControlPanel.constants';
import Setup from './Setup/Setup';
import { getControlPanel } from './ControlPanel.requests';

type ControlPanelModulePropsType = { uuid: string };

const ControlPanelModule: React.FunctionComponent<
  ControlPanelModulePropsType
> = ({ uuid }) => {
  const { data, refetch } = useQuery<ControlPanelDataType, V2ServerErrorsType>(
    [CONTROL_PANEL, uuid],
    () => getControlPanel(uuid),
    { onError: openServerError }
  );

  if (!data) return <Loader />;

  const { id, controlPanel } = data;

  return controlPanel ? (
    <ControlPanel controlPanel={controlPanel} />
  ) : (
    <Setup id={id} refetch={refetch} />
  );
};

export default (props: ControlPanelModulePropsType) => (
  <I18nResourceBundle resource={locale} namespace="controlPanel">
    <ControlPanelModule {...props} />
  </I18nResourceBundle>
);
