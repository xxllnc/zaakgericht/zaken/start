// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { SubjectType } from '../ContactView.types';
import { useInformationStyles } from './Information.style';

export const SeperatorLabel = ({ str }: { str?: String }) => {
  const classes = useInformationStyles();
  return <div className={classes.formLabel}>{str}</div>;
};

export const getCommonTitleSuffix = (
  subject: SubjectType,
  t: i18next.TFunction
) => {
  if (subject.isActive) return;

  const type = subject.dateOfDeath ? 'passed' : 'inactive';
  const text = t(`common.titleSuffix.${type}`);

  return `(${text})`;
};
