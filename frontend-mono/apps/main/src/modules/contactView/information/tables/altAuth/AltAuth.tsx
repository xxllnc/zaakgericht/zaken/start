// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useQueryClient } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { openServerError, openSnackbar } from '@zaaksysteem/common/src/signals';
import {
  saveAltAuth,
  sendAltAuth,
  deleteAltAuth,
} from '../../Information.requests';
import { SubjectType } from '../../../ContactView.types';
import { AltAuthDataType } from '../../Information.types';
import { getFormDefinition } from './altAuth.formDefinition';

type AltAuthPropsType = {
  altAuthData: AltAuthDataType;
  setSnackOpen: any;
  subject: SubjectType;
};

const AltAuth: React.FunctionComponent<AltAuthPropsType> = ({
  altAuthData,
  setSnackOpen,
  subject,
}) => {
  const [t] = useTranslation('information');
  const [confirmOpen, setConfirmOpen] = useState<boolean>(false);
  const [busy, setBusy] = useState<boolean>(false);
  const client = useQueryClient();
  const formDefinition = getFormDefinition(t as any, altAuthData);
  const validationMap = generateValidationMap(formDefinition);

  const parseV1Errors = (errorObj: any): V2ServerErrorsType | null => {
    const results = errorObj?.result;
    const checks = [
      ['missing: kvknummer', t('altAuth.errors.kvk')],
      ['missing: vestigingsnummer', t('altAuth.errors.vestigingsnummer')],
      ['Account already exists', t('altAuth.errors.accountExists')],
    ];
    const errorTypes = [
      ['auth/alternative/username/exists', t('altAuth.errors.usernameExists')],
    ];

    const getErrorMessage = (message: any) => {
      for (var check of checks) {
        if (message.indexOf(check[0]) !== -1) return check[1];
      }
    };

    const getErrorType = (type: any) => {
      for (var errorType of errorTypes) {
        if (type.indexOf(errorType[0]) !== -1) return errorType[1];
      }
    };

    if (results.length && results[0]?.messages?.length) {
      const message = results[0].messages[0] as string;
      const errorType = results[0].type as string;
      const errorMessage =
        errorType === 'params/profile'
          ? getErrorMessage(message)
          : getErrorType(errorType);
      return errorMessage
        ? {
            response: errorObj.response,
            errors: [
              {
                detail: errorMessage,
                code: '',
              },
            ],
          }
        : null;
    }
    return null;
  };

  const {
    fields,
    formik: { isValid, values, dirty, setValues },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <>
      <ConfirmDialog
        title={t('altAuth.deleteAccount')}
        body={t('altAuth.confirmDeleteBody')}
        open={confirmOpen}
        onClose={() => setConfirmOpen(false)}
        onConfirm={async () => {
          setBusy(true);
          deleteAltAuth(altAuthData.id, subject.type)
            .then(async () => {
              client.invalidateQueries(['contactInformation']);
              setValues({
                email: '',
                username: '',
                phone: '',
                active: '0',
              });
              openSnackbar(t('altAuth.deletedMessage'));
              setConfirmOpen(false);
            })
            .catch(openServerError)
            .finally(() => {
              setBusy(false);
            });
        }}
      />
      <SubHeader
        title={t('altAuth.title')}
        description={t('altAuth.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button
          action={() =>
            saveAltAuth(altAuthData.id, values)
              .then(() => {
                client.invalidateQueries(['contactInformation']);
                setSnackOpen(true);
              })
              .catch((err: any) => {
                openServerError(parseV1Errors(err) || err);
              })
          }
          name="saveAltAuth"
          disabled={!isValid || !dirty}
        >
          {t('common:verbs.save')}
        </Button>
        <Button
          action={() =>
            sendAltAuth(altAuthData.id, values).catch(openServerError)
          }
          sx={{ marginLeft: '20px' }}
          name="sendAltAuth"
        >
          {t('altAuth.sendActivationEmail')}
        </Button>
        <Button
          action={() => setConfirmOpen(true)}
          sx={{ marginLeft: '20px' }}
          name="delete"
          disabled={altAuthData.hasAccount === false || busy}
        >
          {t('altAuth.deleteAccount')}
        </Button>
      </div>
    </>
  );
};

export default AltAuth;
