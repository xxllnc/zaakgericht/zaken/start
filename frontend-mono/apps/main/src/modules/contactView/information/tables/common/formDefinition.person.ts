// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import * as i18next from 'i18next';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
  setRequired,
  setOptional,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  COUNTRY_CODE_NETHERLANDS,
  COUNTRY_CODE_CURACAO,
} from '@zaaksysteem/common/src/constants/countryCodes.constants';
import { SubjectType } from '../../../ContactView.types';

const transformDate = (date: string) =>
  date ? fecha.format(new Date(date), 'DD-MM-YYYY') : null;

/* eslint complexity: [2, 50] */
export const getPersonFormDefinition = ({
  t,
  subject,
  canEdit,
  session,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  canEdit?: boolean;
  session: SessionType;
}): AnyFormDefinitionField[] => {
  const customerCountryCode = session.account.instance.country_code;
  const allowedToViewSensitiveContactData =
    session.logged_in_user.capabilities.includes('view_sensitive_contact_data');

  return [
    {
      name: 'generalLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
    },
    {
      name: 'firstNames',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'insertions',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'familyName',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'surname',
      type: fieldTypes.TEXT,
      readOnly: true,
    },
    {
      name: 'nobleTitle',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'gender',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t(`common.person.genderValue.Male`),
          value: 'M',
        },
        {
          label: t(`common.person.genderValue.Female`),
          value: 'F',
        },
        {
          label: t(`common.person.genderValue.Other`),
          value: 'X',
        },
      ],
      value: subject.gender ? subject.gender : null,
      readOnly: !canEdit,
    },
    {
      name: 'dateOfBirth',
      type: fieldTypes.TEXT,
      value: transformDate(subject['dateOfBirth']),
      readOnly: true,
    },
    {
      name: 'dateOfDeath',
      type: fieldTypes.TEXT,
      value: transformDate(subject['dateOfDeath']),
      readOnly: true,
    },
    {
      name: 'personalNumber',
      type: fieldTypes.SENSITIVE_DATA,
      value: subject['personalNumber'],
      readOnly: !allowedToViewSensitiveContactData,
      config: {
        uuid: subject.uuid,
        authenticated: subject.authenticated,
        type: 'personal_number',
      },
    },
    {
      name: 'sedulaNumber',
      type: fieldTypes.SENSITIVE_DATA,
      value: subject['sedulaNumber'],
      readOnly: !allowedToViewSensitiveContactData,
      config: {
        uuid: subject.uuid,
        authenticated: subject.authenticated,
        type: 'sedula_number',
      },
    },
    {
      name: 'insideMunicipality',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t('common:yes'),
          value: t('common:yes'),
        },
        {
          label: t('common:no'),
          value: t('common:no'),
        },
      ],
      value: t(
        `special.insideMunicipalityValue.${subject['insideMunicipality']}`
      ),
      readOnly: true,
    },
    {
      name: 'addressLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
    },
    {
      name: 'residenceCountry',
      type: fieldTypes.COUNTRY_FINDER,
      value: {
        label:
          subject['residenceCountry'] ||
          (customerCountryCode == COUNTRY_CODE_NETHERLANDS
            ? t('common:countries.netherlands')
            : t('common:countries.curacao')),
        value:
          subject.residenceCountryCode === null ||
          subject.residenceCountryCode === undefined
            ? customerCountryCode
            : subject.residenceCountryCode,
      },
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'residenceStreet',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'residenceHouseNumber',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'residenceHouseNumberLetter',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'residenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'residenceZipcode',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'residenceCity',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'foreignAddress1',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'foreignAddress2',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'foreignAddress3',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
    },
    {
      name: 'hasCorrespondenceAddress',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t('common:yes'),
          value: t('common:yes'),
        },
        {
          label: t('common:no'),
          value: t('common:no'),
        },
      ],
      value: t(`common:${subject['hasCorrespondenceAddress']}`),
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceCountry',
      type: fieldTypes.COUNTRY_FINDER,
      value: {
        label:
          subject['correspondenceCountry'] ||
          (customerCountryCode == COUNTRY_CODE_NETHERLANDS
            ? t('common:countries.netherlands')
            : t('common:countries.curacao')),
        value:
          subject.correspondenceCountryCode === null ||
          subject.correspondenceCountryCode === undefined
            ? customerCountryCode
            : subject.correspondenceCountryCode,
      },
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'correspondenceStreet',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceHouseNumber',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceHouseNumberLetter',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceZipcode',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceCity',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceForeignAddress1',
      label: t('common.person.foreignAddress1'),
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceForeignAddress2',
      label: t('common.person.foreignAddress2'),
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceForeignAddress3',
      label: t('common.person.foreignAddress3'),
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
  ].map(field => ({
    label: t(`common.person.${field.name}`),
    value: subject[field.name],
    ...field,
  }));
};

const residenceAddressFields = [
  'residenceStreet',
  'residenceHouseNumber',
  'residenceHouseNumberLetter',
  'residenceHouseNumberSuffix',
  'residenceZipcode',
  'residenceCity',
];

const correspondenceAddressFields = [
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceHouseNumberLetter',
  'correspondenceHouseNumberSuffix',
  'correspondenceZipcode',
  'correspondenceCity',
];

const foreignAddressFields = [
  'foreignAddress1',
  'foreignAddress2',
  'foreignAddress3',
];

const correspondenceForeignAddressFields = [
  'correspondenceForeignAddress1',
  'correspondenceForeignAddress2',
  'correspondenceForeignAddress3',
];

const residenceRequiredFields = [
  'residenceStreet',
  'residenceHouseNumber',
  'residenceZipcode',
  'residenceCity',
];

const correspondenceRequiredFields = [
  'correspondenceCountry',
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceZipcode',
  'correspondenceCity',
];

export const getPersonRules = (session: SessionType) => {
  const customerCountryCode = session.account.instance.country_code;

  return [
    //Local address
    new Rule()
      .when('residenceCountry', (field: any) => {
        const countryCode = field?.value?.value;

        return customerCountryCode == countryCode;
      })
      .then(showFields(residenceAddressFields))
      .then(hideFields(foreignAddressFields))
      .then(setRequired(residenceRequiredFields))
      .then(setOptional(['foreignAddress1']))
      .else(showFields(foreignAddressFields))
      .else(hideFields(residenceAddressFields))
      .else(setRequired(['foreignAddress1']))
      .else(setOptional(residenceRequiredFields)),
    // Sedula
    new Rule()
      .when(() => customerCountryCode == COUNTRY_CODE_CURACAO)
      .then(showFields(['sedulaNumber']))
      .else(hideFields(['sedulaNumber'])),
    new Rule()
      // No correspondence address
      .when(fields => fields.hasCorrespondenceAddress.value === 'Nee')
      .then(
        hideFields([
          ...correspondenceAddressFields,
          ...correspondenceForeignAddressFields,
          'correspondenceCountry',
        ])
      )
      .then(
        setOptional([
          ...correspondenceRequiredFields,
          ...['correspondenceForeignAddress1'],
        ])
      ),
    new Rule()
      // Dutch correspondence address
      .when((fields: any) => {
        return (
          fields.hasCorrespondenceAddress.value === 'Ja' &&
          fields.correspondenceCountry?.value?.value == customerCountryCode
        );
      })
      .then(
        showFields(['correspondenceCountry', ...correspondenceAddressFields])
      )
      .then(hideFields(correspondenceForeignAddressFields))
      .then(setRequired(correspondenceRequiredFields)),
    new Rule()
      // Foreign correspondence address
      .when((fields: any) => {
        return (
          fields.hasCorrespondenceAddress.value === 'Ja' &&
          fields.correspondenceCountry?.value?.value != customerCountryCode
        );
      })
      .then(
        showFields([
          'correspondenceCountry',
          ...correspondenceForeignAddressFields,
        ])
      )
      .then(hideFields(correspondenceAddressFields))
      .then(setRequired(['correspondenceForeignAddress1'])),
    new Rule()
      .when(() => customerCountryCode == COUNTRY_CODE_CURACAO)
      .then(hideFields(['residenceZipcode', 'correspondenceZipcode']))
      .then(setOptional(['residenceZipcode', 'correspondenceZipcode'])),
  ];
};
