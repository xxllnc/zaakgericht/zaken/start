// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { saveNotificationSettings } from '../../Information.requests';
import { getFormDefinition } from './notificationSettings.formDefinition';
import { SubjectType } from './../../../ContactView.types';

type NotificationSettingsPropsType = {
  subject: SubjectType;
};

const NotificationSettings: React.FunctionComponent<
  NotificationSettingsPropsType
> = ({ subject }) => {
  const [t] = useTranslation('information');
  const formDefinition = getFormDefinition(t as any, subject);

  const {
    fields,
    formik: { values, dirty },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      <SubHeader
        title={t('notificationSettings.title')}
        description={t('notificationSettings.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button
          action={() =>
            saveNotificationSettings(subject.uuid, values).catch(
              openServerError
            )
          }
          name="saveNotificationSettings"
          disabled={!dirty}
        >
          {t('common:verbs.save')}
        </Button>
      </div>
    </>
  );
};

export default NotificationSettings;
