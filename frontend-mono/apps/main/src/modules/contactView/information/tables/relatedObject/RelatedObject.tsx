// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@mintlab/ui/App/Material/Button';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { ObjectType, ObjectTypeType } from '../../Information.types';
import { getFormDefinition } from './relatedObject.formDefinition';

type RelatedObjectPropsType = {
  object: ObjectType;
  objectType: ObjectTypeType;
};

const RelatedObject: React.FunctionComponent<RelatedObjectPropsType> = ({
  object,
  objectType,
}) => {
  const [t] = useTranslation('information');
  const formDefinition = getFormDefinition(t as any, object, objectType);

  const {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
  });

  const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
    <Link to={`/main/object/${object.id}`} {...props} ref={ref} />
  ));

  return (
    <>
      <SubHeader
        title={t('relatedObject.title')}
        description={t('relatedObject.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button component={LinkComponent} name="openObject">
          {t('relatedObject.openObject')}
        </Button>
      </div>
    </>
  );
};

export default RelatedObject;
