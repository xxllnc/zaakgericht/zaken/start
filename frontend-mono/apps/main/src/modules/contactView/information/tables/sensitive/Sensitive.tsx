// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';

const Sensitive: React.FunctionComponent = () => {
  const [t] = useTranslation('information');

  return (
    <>
      <SubHeader
        title={t('sensitive.title')}
        description={t('sensitive.subTitle')}
      />
      <div>
        <p>{t('sensitive.placeholder')}</p>
      </div>
    </>
  );
};

export default Sensitive;
