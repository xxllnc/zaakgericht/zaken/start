// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export type FormValuesType = {
  authenticated: string;
  source: string;
  uuid: string;
};

export const getOrganizationFormDefinition = ({
  t,
  subject,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
}): AnyFormDefinitionField[] => {
  const authenticatedValue = subject.authenticated
    ? t('common:yes')
    : t('common:no');

  return [
    {
      name: 'authenticated',
      type: fieldTypes.TEXT,
      value: authenticatedValue,
    },
    {
      name: 'source',
      type: fieldTypes.TEXT,
    },
    {
      name: 'uuid',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`special.${field.name}`),
    value: subject[field.name],
    readOnly: true,
    ...field,
  }));
};
