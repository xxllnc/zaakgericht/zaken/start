// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { SubjectType } from '../../ContactView.types';

export const transformEmployee = (
  employee: SubjectType,
  settings: APICaseManagement.GetContactSettingsResponseBody['data']
) => {
  const {
    id: uuid,
    type,
    attributes: {
      source,
      status,
      first_name: firstName,
      surname,
      contact_information,
      is_active: isActive,
    },
    relationships,
  } = employee;

  const roles = relationships?.roles;
  const department = relationships?.department.meta.summary;

  const {
    attributes: { notification_settings, phone_extension: phoneExtension },
  } = settings;

  const caseSuspensionTermExceeded =
    notification_settings?.case_suspension_term_exceeded;
  const caseTermExceeded = notification_settings?.case_term_exceeded;
  const newAssignedCase = notification_settings?.new_assigned_case;
  const newAttributeProposal = notification_settings?.new_attribute_proposal;
  const newDocument = notification_settings?.new_document;
  const newExtPipMessage = notification_settings?.new_ext_pip_message;

  const signatureId = settings.relationships?.signature?.data?.id;

  const phoneNumber = contact_information?.phone_number;
  const email = contact_information?.email;

  const relatedObjectUuid = relationships?.related_custom_object?.data?.id;

  return {
    uuid,
    type,
    source,
    status,
    firstName,
    surname,
    phoneNumber,
    email,
    department,
    roles: (roles || []).map((role: any) => role.meta.summary),
    settings: {
      caseSuspensionTermExceeded,
      caseTermExceeded,
      newAssignedCase,
      newAttributeProposal,
      newDocument,
      newExtPipMessage,
      signatureId,
    },
    phoneExtension,
    isActive,
    relatedObjectUuid,
  };
};
