// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectType } from '../../ContactView.types';
import { ensureEmptyValue } from './library';

export const transformPerson = (person: SubjectType) => {
  const {
    id: uuid,
    type,
    attributes: {
      source,
      external_identifier: externalIdentifier,
      authenticated,
      noble_title: nobleTitle,
      first_names: firstNames,
      insertions,
      family_name: familyName,
      surname,
      gender,
      date_of_birth: dateOfBirth,
      date_of_death: dateOfDeath,
      inside_municipality,
      residence_address,
      correspondence_address,
      contact_information,
      is_active: isActive,
    },
    relationships,
  } = person;

  // Residence address
  const residenceCountry = residence_address?.country;
  const residenceCountryCode = residence_address?.country_code;
  const residenceStreet = residence_address?.street;
  const residenceHouseNumber = residence_address?.street_number;
  const residenceHouseNumberLetter = residence_address?.street_number_letter;
  const residenceHouseNumberSuffix = residence_address?.street_number_suffix;
  const residenceZipcode = residence_address?.zipcode;
  const residenceCity = residence_address?.city;
  const foreignAddress1 = residence_address?.address_line_1;
  const foreignAddress2 = residence_address?.address_line_2;
  const foreignAddress3 = residence_address?.address_line_3;

  // Correspondence address
  const correspondenceCountry = correspondence_address?.country;
  const correspondenceCountryCode = correspondence_address?.country_code;
  const correspondenceStreet = correspondence_address?.street;
  const correspondenceHouseNumber = correspondence_address?.street_number;
  const correspondenceHouseNumberLetter =
    correspondence_address?.street_number_letter;
  const correspondenceHouseNumberSuffix =
    correspondence_address?.street_number_suffix;
  const correspondenceZipcode = correspondence_address?.zipcode;
  const correspondenceCity = correspondence_address?.city;
  const correspondenceForeignAddress1 = correspondence_address?.address_line_1;
  const correspondenceForeignAddress2 = correspondence_address?.address_line_2;
  const correspondenceForeignAddress3 = correspondence_address?.address_line_3;

  // Other
  const hasCorrespondenceAddress = correspondence_address ? 'yes' : 'no';
  const insideMunicipality = inside_municipality ? 'yes' : 'no';
  const email = contact_information?.email;
  const mobileNumber = contact_information?.mobile_number;
  const phoneNumber = contact_information?.phone_number;
  const internalNote = contact_information?.internal_note;
  const anonymousUser = contact_information?.is_anonymous_contact;
  const preferredContactChannel =
    contact_information?.preferred_contact_channel;

  const relatedObjectUuid = relationships?.related_custom_object?.data?.id;

  return {
    uuid,
    type,
    source: ensureEmptyValue(source),
    externalIdentifier,
    authenticated,
    anonymousUser,
    nobleTitle: ensureEmptyValue(nobleTitle),
    firstNames: ensureEmptyValue(firstNames),
    insertions: ensureEmptyValue(insertions),
    familyName: ensureEmptyValue(familyName),
    surname: ensureEmptyValue(surname),
    gender,
    dateOfBirth,
    dateOfDeath,
    residenceCountry: ensureEmptyValue(residenceCountry),
    residenceCountryCode,
    insideMunicipality,
    residenceStreet: ensureEmptyValue(residenceStreet),
    residenceHouseNumber: ensureEmptyValue(residenceHouseNumber),
    residenceHouseNumberLetter: ensureEmptyValue(residenceHouseNumberLetter),
    residenceHouseNumberSuffix: ensureEmptyValue(residenceHouseNumberSuffix),
    residenceZipcode: ensureEmptyValue(residenceZipcode),
    residenceCity: ensureEmptyValue(residenceCity),
    hasCorrespondenceAddress,
    correspondenceCountry: ensureEmptyValue(correspondenceCountry),
    correspondenceCountryCode,
    correspondenceStreet: ensureEmptyValue(correspondenceStreet),
    correspondenceHouseNumber: ensureEmptyValue(correspondenceHouseNumber),
    correspondenceHouseNumberLetter: ensureEmptyValue(
      correspondenceHouseNumberLetter
    ),
    correspondenceHouseNumberSuffix: ensureEmptyValue(
      correspondenceHouseNumberSuffix
    ),
    correspondenceZipcode: ensureEmptyValue(correspondenceZipcode),
    correspondenceCity: ensureEmptyValue(correspondenceCity),
    foreignAddress1: ensureEmptyValue(foreignAddress1),
    foreignAddress2: ensureEmptyValue(foreignAddress2),
    foreignAddress3: ensureEmptyValue(foreignAddress3),
    correspondenceForeignAddress1: ensureEmptyValue(
      correspondenceForeignAddress1
    ),
    correspondenceForeignAddress2: ensureEmptyValue(
      correspondenceForeignAddress2
    ),
    correspondenceForeignAddress3: ensureEmptyValue(
      correspondenceForeignAddress3
    ),
    email: ensureEmptyValue(email),
    mobileNumber: ensureEmptyValue(mobileNumber),
    phoneNumber: ensureEmptyValue(phoneNumber),
    internalNote: ensureEmptyValue(internalNote),
    preferredContactChannel,
    relatedObjectUuid,
    isActive,
  };
};
