// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IntegrationData } from '@mintlab/ui/types/MapIntegration';
import { fetchCoordinatesForAddress } from '@zaaksysteem/common/src/components/form/fields/Address/Address.library';
import { SubjectType } from '../ContactView.types';

type FetchContactAddressType = (
  subject: SubjectType
) => Promise<IntegrationData['contact']>;

export const fetchContactAddress: FetchContactAddressType = async subject => {
  const location_address =
    subject.attributes.location_address ||
    subject.attributes.residence_address ||
    subject.attributes.correspondence_address;
  const emptyFilter = (part: any) => part;
  const address = location_address
    ? [
        [
          location_address.street,
          location_address.street_number,
          location_address.street_letter,
        ]
          .filter(emptyFilter)
          .join(' '),
        location_address.zipcode,
        location_address.city,
      ].join(', ')
    : '';

  const coordinates = address
    ? await fetchCoordinatesForAddress(address)
    : null;

  if (!coordinates) {
    return null;
  } else {
    return {
      uuid: subject.id,
      address: address,
      location_address: location_address,
      coordinates,
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Point',
              coordinates,
            },
          },
        ],
      } as const,
    };
  }
};
