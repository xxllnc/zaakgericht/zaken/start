// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import RelationsTable from '@mintlab/ui/App/Zaaksysteem/RelationsTable';
import locale from '../../objectView/ObjectView.locale';
import { useRelationshipStyles } from './Relationship.style';

export const Relationship: React.FunctionComponent = () => {
  const classes = useRelationshipStyles();
  const { uuid } = useParams() as { uuid: string };

  return (
    <div className={classes.wrapper}>
      <I18nResourceBundle resource={locale} namespace="objectView">
        <RelationsTable context="subject" uuid={uuid} type="subjects" />
        <RelationsTable context="subject" uuid={uuid} type="objects" />
      </I18nResourceBundle>
    </div>
  );
};

export default Relationship;
