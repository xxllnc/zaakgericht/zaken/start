// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import Timeline from '../../../../../../packages/common/src/components/Timeline/Timeline';
import { getData } from './library';
import { useStyles } from './styles';
import { getExportFunction } from './library';

type ContactTimelinePropsType = {
  uuid: string;
  type: SubjectTypeType;
};

const ContactTimeline: React.FunctionComponent<ContactTimelinePropsType> = ({
  uuid,
  type,
}) => {
  const classes = useStyles();
  const getDataFunction = getData({ uuid, type });

  return (
    <div className={classes.wrapper}>
      <Timeline
        getData={getDataFunction}
        exportFunction={getExportFunction(uuid, type)}
      />
    </div>
  );
};

export default ContactTimeline;
