// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { getDataFuncType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { TimelineItemType } from '@zaaksysteem/common/src/components/Timeline/Timeline.types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { PAGE_LENGTH } from '@zaaksysteem/common/src/components/Timeline/Timeline';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { openServerError } from '@zaaksysteem/common/src/signals';

type GetDataType = ({
  uuid,
  type,
}: {
  uuid: string;
  type: SubjectTypeType;
}) => getDataFuncType<TimelineItemType, any>;

/* eslint complexity: [2, 8] */
export const getData: GetDataType = ({ uuid, type }) =>
  async function getDataAsync({ pageNum, keyword, startDate, endDate }: any) {
    const urlParams: APICaseManagement.GetContactEventLogsRequestParams = {
      page: pageNum,
      page_size: PAGE_LENGTH,
      ...(uuid && { contact_uuid: uuid }),
      ...(type && { contact_type: type }),
      ...(startDate && {
        period_start:
          fecha.format(new Date(startDate), 'YYYY-MM-DD') + 'T00:00:00+01:00',
      }),
      ...(endDate && {
        period_end:
          fecha.format(new Date(endDate), 'YYYY-MM-DD') + 'T23:59:59+01:00',
      }),
    };

    const url = buildUrl<APICaseManagement.GetContactEventLogsRequestParams>(
      `/api/v2/cm/contact/get_contact_event_logs`,
      urlParams
    );
    const result =
      await request<APICaseManagement.GetContactEventLogsResponseBody>(
        'GET',
        url
      ).catch(openServerError);

    if (!result || !result.data) return { rows: [] };

    return {
      rows: result.data.map(row => {
        const { attributes, relationships } = row;

        return {
          id: attributes?.id || '',
          type: attributes?.type || '',
          author: attributes?.user || '',
          date: new Date(attributes?.date || ''),
          description: attributes?.description || '',
          caseNumber: relationships?.case?.meta?.summary,
        };
      }),
    };
  };

export const getExportFunction =
  (uuid: string, type: SubjectTypeType) =>
  ({
    startDate,
    endDate,
  }: {
    startDate?: string | null;
    endDate?: string | null;
  }) => {
    const start = startDate
      ? fecha.format(new Date(startDate), 'YYYY-MM-DD') + 'T00:00:00+01:00'
      : null;
    const end = endDate
      ? fecha.format(new Date(endDate), 'YYYY-MM-DD') + 'T23:59:59+01:00'
      : null;

    return request<any>('POST', '/api/v2/cm/contact/export_timeline', {
      uuid,
      type,
      ...(startDate && { period_start: start }),
      ...(endDate && { period_end: end }),
    });
  };
