// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import FileSaver from 'file-saver';
import JSZIP from 'jszip';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { fillCSStemplateFile } from './cssTemplate';

// Update each time and write to changelog
// If removing fields bump major
export const STYLES_VERSION = '1.0.0';

const major = (str: string) => str.split('.')[0];

const prepareTemplate = (data: FormData) => {
  const zip = new JSZIP();

  zip.file(
    'config.json',
    JSON.stringify({
      siteImprovId: data.get('siteImprovId'),
      documentTitle: data.get('documentTitle'),
      mainAddress: data.get('mainAddress'),
      version: STYLES_VERSION,
    })
  );

  const favicon = data.get('favicon') as File;
  const logoLogin = data.get('logo-login') as File;
  const logoPip = data.get('logo-pip') as File;

  favicon && zip.file('favicon.ico', favicon.arrayBuffer());
  logoLogin &&
    zip.file(
      'logo-login.' +
        (logoLogin.type
          ? logoLogin.type.split('/')[1].split('+')[0]
          : logoLogin.name.split('.')[1]),
      logoLogin.arrayBuffer()
    );
  logoPip &&
    zip.file(
      'logo-pip.' +
        (logoPip.type
          ? logoPip.type.split('/')[1].split('+')[0]
          : logoPip.name.split('.')[1]),
      logoPip.arrayBuffer()
    );

  const cssFills = cssProperties.map(cssField => {
    return [
      cssField.name,
      data.get(cssField.name)?.toString() || '',
      Boolean(cssField.imports),
    ] as const;
  });

  zip.file('stylesheet.css', fillCSStemplateFile(cssFills));
  zip.file('cssProps.json', JSON.stringify(cssFills));

  return zip;
};

export const saveTemplate = (data: FormData) => {
  const zip = prepareTemplate(data);
  zip.generateAsync({ type: 'blob' }).then(content => {
    FileSaver.saveAs(content, (data.get('name') || 'tpl') + '.zss');
  });
};

export const loadTemplate = () => {
  const input = document.querySelector('#archiveUpload');
  //@ts-ignore
  const file = input.files[0];
  const zipH = new JSZIP();

  if (file) {
    zipH.loadAsync(file).then(zip => {
      const plugInFile = (name: string) => {
        const fullName = Object.keys(zip.files).find(key =>
          key.includes(name)
        ) as string;
        const container = new DataTransfer();

        zip.files[fullName].async('blob').then(blb => {
          const type = fullName.includes('.svg')
            ? 'image/svg+xml'
            : fullName.includes('.png')
              ? 'image/png'
              : fullName.includes('.jp')
                ? 'image/jpeg'
                : 'image/x-icon';
          //@ts-ignore
          container.items.add(new File([blb], fullName, { type }));
          //@ts-ignore
          document.querySelector('#' + name).files = container.files;
        });
      };

      plugInFile('favicon');
      plugInFile('logo-login');
      plugInFile('logo-pip');

      zip.files['config.json'].async('text').then(txt => {
        const config = JSON.parse(txt);
        //@ts-ignore
        document.querySelector('#siteImprovId').value = config.siteImprovId;
        //@ts-ignore
        document.querySelector('#name').value = file.name.split('.zss')[0];

        if (config.version && major(config.version) !== major(STYLES_VERSION)) {
          if (config.version.includes('.')) {
            alert(
              `Major version of this template ${config.version} does not match current version of templating system (${STYLES_VERSION}). The edited/saved template will be missing something from this. Check the changelog and make sure to test over.`
            );
          } else {
            alert(
              `This appears to be handwritten template file (version "${config.version}"). It will VERY likely break after editing here`
            );
          }
        }
      });

      zip.files['cssProps.json'].async('text').then(txt => {
        const props = JSON.parse(txt);
        //@ts-ignore
        props.forEach(([key, value]) => {
          try {
            //@ts-ignore
            document.querySelector('#' + key).value = value;
          } catch (err) {
            console.warn(err);
          }
        });
      });
    });
  }
};

export const uploadTemplate = (data: FormData) => {
  const zip = prepareTemplate(data);

  zip.generateAsync({ type: 'blob' }).then(content => {
    const fd = new FormData();
    fd.append('file', new File([content], 'test.zss'));
    fd.append('tenant', 'development.zaaksysteem.nl');

    return request('POST', '/api/v2/style/upload_template', fd, {
      type: 'formdata',
      cached: false,
    });
  });
};

export const cssProperties = [
  {
    name: 'primary-color',
    value: '#252938',
    type: 'color',
    tooltip: 'Kleur die op veel plaatsen wordt gebruikt',
  },
  {
    name: 'font-family',
    value: "'Lato', sans-serif",
    tooltip: 'Standaardlettertype en reservelettertypen',
  },
  {
    name: 'font-link1',
    value: '',
    tooltip: 'eg. https://fonts.googleapis.com/css?family=Open+Sans:400,600',
    imports: true,
  },
  {
    name: 'header-height',
    value: '122px',
    tooltip:
      'Hoogte van de container met het logo weergegeven op PIP en webformulieren',
  },
  {
    name: 'header-padding',
    value: '22px 4px 0',
    tooltip:
      'Opvulling van de container met het logo dat wordt weergegeven op PIP en webformulieren',
  },
  {
    name: 'logo-pip-width',
    value: '326px',
    tooltip: 'Breedte van het logo getoond op PIP en webformulieren',
  },
  {
    name: 'logo-pip-height',
    value: '79px',
    tooltip: 'Hoogte van het logo getoond op PIP en webformulieren',
  },
  {
    name: 'logo-pip-margin',
    value: '0 0 0 15px',
    tooltip: 'Marge van het logo getoond op PIP en webformulieren',
  },
  {
    name: 'logo-pip-bg-size',
    value: 'auto',
    tooltip: 'contain/cover/auto',
  },
  {
    name: 'step-active-bg-color',
    value: '#f6653c',
    type: 'color',
    tooltip:
      'Achtergrond van het element dat de momenteel ingevulde stap van het formulier weergeeft',
  },
  {
    name: 'icon-question-sign-color',
    value: '#dfff1d',
    type: 'color',
    tooltip: 'Kleur voor de vraagtekens met de tooltips op webformulieren',
  },
  {
    name: 'button-primary-bg',
    value: '#f6653c',
    type: 'color',
    tooltip:
      'Achtergrond van bijvoorbeeld de knop "Volgende" op webformulieren',
  },
  {
    name: 'button-primary-color',
    value: '#ffffff',
    type: 'color',
    tooltip:
      'Letterkleur van bijvoorbeeld de knop "Volgende" op webformulieren',
  },
  {
    name: 'button-primary-bg-hover',
    value: '#f43f0b',
    type: 'color',
    tooltip:
      'Achtergrond van bijvoorbeeld knop "Volgende" op webformulieren (tijdens muisover)',
  },
  {
    name: 'anchor-color',
    value: '#252938',
    type: 'color',
    tooltip: 'Kleur van hyperlinks',
  },
  {
    name: 'anchor-color-hover',
    value: '#004e86',
    type: 'color',
    tooltip: 'Kleur van hyperlinks (tijdens muisover)',
  },
  {
    name: 'anchor-text-decoration',
    value: 'none',
    tooltip:
      'Moet er een onderstreping zijn voor de links (zet "none" of "underline")',
  },
  {
    name: 'anchor-text-decoration-hover',
    value: 'none',
    tooltip:
      'Moet er een onderstreping zijn voor de links tijdens het zweven (zet "none" of "underline")',
  },
  {
    name: 'button-id-bg-color',
    value: '#252938',
    type: 'color',
    tooltip: 'Achtergrond van de knop "inloggen met Digid"',
  },
  {
    name: 'pip-nav-background-color',
    value: '#252938',
    type: 'color',
    tooltip: 'Achtergrond van de navigatiebalk in PIP',
  },
  {
    name: 'pip-nav-border-color',
    value: '#252938',
    type: 'color',
    tooltip: 'Kleur van de rand van de navigatiebalk in PIP',
  },
  {
    name: 'pip-nav-border-color-active',
    value: 'black',
    type: 'color',
    tooltip:
      'Kleur van het lettertype van de actieve link op de PIP-navigatiebalk (actief betekent de link naar de pagina waarop u zich momenteel bevindt)',
  },
  {
    name: 'pip-nav-anchor-color',
    value: '#ffffff',
    type: 'color',
    tooltip: 'Letterkleur van de links in de PIP-navigatiebalk',
  },
  {
    name: 'pip-nav-anchor-color-active',
    value: '#ffffff',
    type: 'color',
    tooltip:
      'Letterkleur van de actieve link in de PIP-navigatiebalk (actief betekent de link naar de pagina waarop u zich momenteel bevindt)',
  },
  {
    name: 'pip-nav-bg-active',
    value: 'rgba(255,255,255,.2)',
    type: 'color',
    tooltip:
      'Achtergrond van de actieve link in de PIP-navigatiebalk (actief betekent de link naar de pagina waarop u zich momenteel bevindt)',
  },
  {
    name: 'pip-nav-bg-hover',
    value: 'rgba(255,255,255,.1)',
    type: 'color',
    tooltip:
      'Achtergrond van de links in de PIP-navigatiebalk (tijdens muisover)',
  },
  {
    name: 'pip-progress-bar-bg',
    value: '#9da4bf',
    type: 'color',
    tooltip:
      'Achtergrondkleur van de voortgangsbalk in PIP-weergave van cases (ongevuld deel van de balk)',
  },
  {
    name: 'login-logo-size-mode',
    value: 'contain',
    tooltip:
      'Veranderingen hoe het login-logo in het Beheerer-gedeelte van ZS wordt weergegeven (zet "cover" of "contain")',
  },
  {
    name: 'title-font-size',
    value: '1.75rem',
    tooltip:
      'Title verwijst naar de banner die u ziet als u naar de inlogpagina voor PIP gaat',
  },
  {
    name: 'title-bg-color',
    value: '#3b3b3b',
    type: 'color',
    tooltip:
      'Title verwijst naar de banner die u ziet als u naar de inlogpagina voor PIP gaat',
  },
  {
    name: 'title-font-weight',
    value: '400',
    tooltip:
      'Title verwijst naar de banner die u ziet als u naar de inlogpagina voor PIP gaat',
  },
  {
    name: 'title-padding',
    value: '1.25em',
    tooltip:
      'Title verwijst naar de banner die u ziet als u naar de inlogpagina voor PIP gaat',
  },
  {
    name: 'title-height',
    value: 'auto',
    tooltip:
      'Title verwijst naar de banner die u ziet als u naar de inlogpagina voor PIP gaat',
  },
  {
    name: 'title-text-color',
    value: '#ffffff',
    type: 'color',
    tooltip:
      'Title verwijst naar de banner die u ziet als u naar de inlogpagina voor PIP gaat',
  },
  {
    name: 'template-width',
    value: '960px',
    tooltip:
      'Hoe breed de webformulierpagina mag zijn (Aanbevolen om dit niet te wijzigen)',
  },
  {
    name: 'pip-nav-border-weight',
    value: '2px',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'form-navigation-margin',
    value: '0 0 1em',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading1-color',
    value: '#000',
    type: 'color',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading1-fontsize',
    value: '2em',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading1-weight',
    value: '700',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading1-font-family',
    value: "'Montserrat', sans-serif",
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading2-color',
    value: '#000',
    type: 'color',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading2-fontsize',
    value: '1.5em',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading2-weight',
    value: '700',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'heading2-font-family',
    value: "'Montserrat', sans-serif",
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'font-size',
    value: '100%',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'container-border',
    value: 'none',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'step-active-anchor-font-weight',
    value: '400',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'body-bg',
    value: '#f1f1f1',
    type: 'color',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'header-bg',
    value: '#ffffff',
    type: 'color',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'border-radius',
    value: '1px',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
  {
    name: 'button-radius',
    value: '5px',
    tooltip: 'Aanbevolen om dit niet te wijzigen',
  },
];
