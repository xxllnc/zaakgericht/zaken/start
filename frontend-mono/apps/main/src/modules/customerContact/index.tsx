// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import {
  hasCapability,
  hasSystemRole,
} from '@zaaksysteem/common/src/hooks/useSession';
import useUnauthorizedBanner from '@zaaksysteem/common/src/hooks/useUnauthorizedBanner';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import locale from './CustomerContact.locale';
import CustomerContact from './CustomerContact';

const CustomerContactModule: React.FunctionComponent = () => {
  const [t] = useTranslation('customerContact');

  const Banner = useUnauthorizedBanner(
    session =>
      hasSystemRole(session, 'Behandelaar') &&
      hasCapability(session, 'message_intake')
  );

  return Banner ? (
    Banner
  ) : (
    <Sheet>
      <CustomerContact />
      <TopbarTitle title={t('title')} />
    </Sheet>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="customerContact">
    <CustomerContactModule />
  </I18nResourceBundle>
);
