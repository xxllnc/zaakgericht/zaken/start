// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { SettingsType } from './AdvancedSearch';

export const updateWidgetPostMessage = ({
  widgetUuid,
  settings,
}: {
  widgetUuid: string;
  settings: SettingsType;
}) => {
  window.parent?.postMessage(
    {
      type: 'updateWidget',
      data: { widgetUuid, settings },
    },
    window.location.origin
  );
};

export const getWidgetData = async (
  widgetUuid: string
): Promise<SettingsType> => {
  const url = buildUrl<any>('/api/v1/dashboard/widget', {
    page: 1,
    rows_per_page: 20,
  });

  const data = await request('GET', url);
  const widget = data.result.instance.rows.find(
    (option: any) => option.reference === widgetUuid
  );

  return widget.instance.data;
};
