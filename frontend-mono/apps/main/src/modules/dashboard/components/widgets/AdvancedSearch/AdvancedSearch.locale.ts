// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    dialogTitle: 'Widget instellingen',
    fields: {
      title: {
        label: 'Widget titel',
      },
      identifier: {
        label: 'Zoekopdracht identificatiecode',
        placeholder: 'Zoekopdracht identificatiecode (UUID of kenmerk)',
      },
      kind: {
        label: 'Soort',
      },
    },
  },
};

export default locale;
