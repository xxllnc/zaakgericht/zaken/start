// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect, FunctionComponent } from 'react';
import { useParams } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { default as advancedSearchLocale } from '../../../../advancedSearch/locale/AdvancedSearch.locale';
import ResultsView from '../../../../advancedSearch/components/Main/Results/ResultsView';
import Widget from '../Widget';
import {
  IdentifierType,
  KindType,
} from '../../../../advancedSearch/AdvancedSearch.types';
import locale from './AdvancedSearch.locale';
import AdvancedSearchHeader from './components/Header';
import {
  updateWidgetPostMessage,
  getWidgetData,
} from './AdvancedSearch.library';

export type SettingsType = Partial<{
  keyword?: string;
  kind: KindType;
  identifier: IdentifierType;
  title: string;
}>;

type AdvancedSearchWidgetPropsType = {
  onClose: () => void;
};

/* eslint complexity: [2, 13] */
const AdvancedSearchWidget: FunctionComponent<
  AdvancedSearchWidgetPropsType
> = ({ onClose }) => {
  const [initialLoading, setInitialLoading] = useState(true);
  const params = useParams();
  const widgetUuid = params.widgetUuid as string;
  const [settings, setSettings] = useState<SettingsType | null>(null);
  const [t] = useTranslation();

  useEffect(() => {
    if (!settings) {
      (async function () {
        const widgetData = await getWidgetData(widgetUuid);
        setSettings(widgetData);
        setInitialLoading(false);
      })().catch();
    } else {
      setInitialLoading(false);
    }
  }, []);

  const handleSetValues = (thisVal: SettingsType) => {
    let updateValue = settings || {};
    updateValue = {
      ...updateValue,
      ...thisVal,
    } as SettingsType;
    setSettings(updateValue);
    updateWidgetPostMessage({ widgetUuid, settings: updateValue });
  };

  return initialLoading ? (
    <Loader />
  ) : (
    <Widget
      header={
        <AdvancedSearchHeader
          title={settings?.title || ''}
          onClose={onClose}
          widgetUuid={widgetUuid}
          settings={settings}
          forceOpen={
            initialLoading === false && (!settings || !settings?.identifier)
          }
          setValues={handleSetValues}
          t={t}
        />
      }
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          width: '100%',
          flex: 1,
          overflowY: 'hidden',
        }}
      >
        <div style={{ flex: 1, display: 'flex' }}>
          {settings && settings?.identifier && (
            <ResultsView
              keyword={settings?.keyword}
              kind={settings?.kind as KindType}
              identifier={settings?.identifier}
              view={'table'}
              capabilities={{
                summary: false,
                assigneeControls: false,
                checkboxSelection: false,
              }}
            />
          )}
        </div>
      </div>
    </Widget>
  );
};

export default (props: AdvancedSearchWidgetPropsType) => (
  <I18nResourceBundle
    resource={locale}
    namespace="dashboardWidgetAdvancedSearch"
  >
    <I18nResourceBundle
      resource={advancedSearchLocale}
      namespace="advancedSearch"
    >
      <AdvancedSearchWidget {...props} />
    </I18nResourceBundle>
  </I18nResourceBundle>
);
