// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SettingsType } from '../AdvancedSearch';

type ColumnsConfigDialogPropsType = {
  value: SettingsType | null;
  onClose: () => void;
  onSubmit: (val: any) => void;
  t: ReturnType<typeof useTranslation>['t'];
};

const ConfigDialog: React.ComponentType<ColumnsConfigDialogPropsType> = ({
  value,
  onClose,
  onSubmit,
  t,
}) => {
  const formDefinition = [
    {
      name: 'title',
      label: t('dashboardWidgetAdvancedSearch:fields.title.label'),
      type: fieldTypes.TEXT,
      value: value?.title || '',
      placeholder: t('dashboardWidgetAdvancedSearch:fields.title.label'),
      required: true,
    },
    {
      name: 'identifier',
      label: t('dashboardWidgetAdvancedSearch:fields.identifier.label'),
      type: fieldTypes.TEXT,
      value: value?.identifier || '',
      placeholder: t(
        'dashboardWidgetAdvancedSearch:fields.identifier.placeholder'
      ),
      required: true,
    },
    {
      name: 'kind',
      label: t('dashboardWidgetAdvancedSearch:fields.kind.label'),
      type: fieldTypes.SELECT,
      value: value?.kind || 'case',
      choices: [
        {
          label: t('advancedSearch:kind.cases'),
          value: 'case',
        },
        {
          label: t('advancedSearch:kind.objects'),
          value: 'custom_object',
        },
      ],
      isClearable: false,
      required: true,
      nestedValue: true,
    },
  ];

  return (
    <FormDialog
      formDefinition={formDefinition}
      title={t('dashboardWidgetAdvancedSearch:dialogTitle')}
      {...(Boolean(value) &&
        //@ts-ignore
        Object.entries(value).length && {
          onClose,
        })}
      saving={false}
      scope="config-dialog"
      open={true}
      onSubmit={onSubmit}
    />
  );
};

export default ConfigDialog;
