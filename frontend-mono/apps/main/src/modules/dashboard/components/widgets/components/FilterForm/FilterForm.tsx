// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useEffect } from 'react';
import { FormikProps } from 'formik';
import { useTranslation } from 'react-i18next';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Typography from '@mui/material/Typography';
import Button from '@mintlab/ui/App/Material/Button';
import { useTheme } from '@mui/material';

import { FilterType, FormValuesType } from '../../Tasks/types/Tasks.types';
import { ClassesType, useFilterFormStyles } from './FilterForm.styles';
import { getFormDefinition, getReturnValues } from './FilterForm.library';
import locale from './locale';

type FilterFormPropsType = {
  filters: null | FilterType[];
  onSetFilters: (values: FilterType[]) => void;
  setFilterForm: (addFilter: boolean) => void;
  onMount: (formik: FormikProps<any>) => any;
};

const FilterForm: React.FunctionComponent<FilterFormPropsType> = ({
  filters,
  onSetFilters,
  setFilterForm,
  onMount,
}) => {
  const classes = useFilterFormStyles();
  const [t] = useTranslation('filterForm');
  const theme = useTheme<Theme>();

  const { fields, formik } = useForm<any>({
    formDefinition: getFormDefinition({ filters, t: t as any, theme }),
    isInitialValid: false,
    onSubmit: () => {},
    onChange: () => {},
  });

  useEffect(() => {
    (onMount as any)(formik);
  }, []);

  const { values, isValid } = formik;

  const setFilters = (values: FormValuesType) => {
    onSetFilters(getReturnValues(values));
    setFilterForm(false);
  };

  return (
    <div className={classes.wrapper}>
      <div className={classes.fields}>{fields.map(renderField(classes))}</div>
      <div className={classes.bottom}>
        <Button
          action={() => setFilters(values)}
          title={t('save')}
          name="filterFormSave"
          disabled={!isValid}
        >
          {t('save')}
        </Button>
        <Button
          action={() => setFilterForm(false)}
          title={t('cancel')}
          name="filterFormCancel"
        >
          {t('cancel')}
        </Button>
      </div>
    </div>
  );
};

const renderField =
  (classes: ClassesType) => (field: FormRendererFormField) => {
    const { FieldComponent, name, label } = field;

    return (
      <div className={classes.row} key={`field-${name}`}>
        <div>
          <Typography variant="body1">{label}</Typography>
        </div>
        <FieldComponent {...field} key={`field-${name}-cmp`} />
      </div>
    );
  };

/* eslint-disable-next-line */
export default (props: FilterFormPropsType) => (
  <I18nResourceBundle resource={locale} namespace="filterForm">
    <FilterForm {...props} />
  </I18nResourceBundle>
);
