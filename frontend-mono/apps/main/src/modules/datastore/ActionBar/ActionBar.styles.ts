// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useActionBarStyles = makeStyles(
  ({ mintlab: { greyscale, radius }, typography }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      flexWrap: 'wrap',
      gap: 20,
      padding: 20,
      borderBottom: `1px solid ${greyscale.light}`,
    },
  })
);
