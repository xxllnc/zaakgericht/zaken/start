// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import {
  snackbarMargin,
  openServerError,
  openSnackbar,
} from '@zaaksysteem/common/src/signals';
import ActionBar from '@mintlab/ui/App/Zaaksysteem/ActionBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  DataType,
  DataTypeType,
  FiltersType,
  SelectedType,
  SetDataTypeType,
  SetFiltersType,
} from '../DataStore.types';
import {
  dataTypeValues,
  initiateExport,
  getActiveFilters,
  defaultFilters,
} from '../DataStore.library';
import { EverythingSelectedType } from '../../transactions/Transactions.types';
import { useActionBarStyles } from './ActionBar.styles';
import FiltersDialog from './FiltersDialog';
import BulkDialog from './BulkDialog';
import CleanUpDialog from './CleanUpDialog';

type ActionBarPropsType = {
  data?: DataType;
  dataType: DataTypeType;
  filters: FiltersType;
  selectedRows: SelectedType;
  everythingSelected: EverythingSelectedType;
  setDataType: SetDataTypeType;
  setFilters: SetFiltersType;
  refreshData: () => void;
};

const ActionBarWrapper: React.ComponentType<ActionBarPropsType> = ({
  data,
  dataType,
  filters,
  selectedRows,
  everythingSelected,
  setDataType,
  setFilters,
  refreshData,
}) => {
  const classes = useActionBarStyles();
  const [t] = useTranslation('dataStore');
  const [loading, setLoading] = useState<boolean>(false);
  const [dialogOpen, setDialogOpen] = useState<
    'filters' | 'bulk' | 'cleanUp' | null
  >(null);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const setTextFilter = (value: string) => {
    setFilters({ ...filters, freeform_filter: value, zapi_page: 0 });
  };
  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= 3) setTextFilter(value);
  }, 500);
  const filtersActive =
    dataType === 'NatuurlijkPersoon' || dataType === 'Organisatie';

  return (
    <>
      <ActionBar
        filters={[
          {
            type: 'select',
            name: 'data-type',
            value: dataType,
            choices: dataTypeValues.map((value: DataTypeType) => ({
              value,
              label: value,
            })),
            onChange: (event: any) => {
              setFilters({
                ...defaultFilters,
                freeform_filter: filters.freeform_filter,
              });
              setDataType(event.target.value?.value);
            },
            isClearable: false,
          },
          {
            type: 'text',
            value: searchTerm,
            placeholder: t('filters.fields.freeform_filter.placeholder'),
            onChange: (event: any) => {
              const value = event.target.value;
              setSearchTerm(value);
              debouncedCallback(event.target.value);
            },
            closeAction: () => {
              setSearchTerm('');
              setTextFilter('');
            },
          },
        ]}
        advancedActions={[
          {
            label: t('export.button'),
            disabled: loading,
            name: 'export',
            action: () => {
              setLoading(true);
              initiateExport(dataType, filters)
                .then(() => {
                  openSnackbar({
                    message: t('export.snack'),
                    sx: snackbarMargin,
                  });
                })
                .catch(openServerError)
                .finally(() => setLoading(false));
            },
          },
          {
            label: t('bulk.button'),
            disabled: !selectedRows.length,
            name: 'datastoreBulk',
            action: () => {
              setDialogOpen('bulk');
            },
          },
          {
            label: t('cleanUp.button'),
            disabled: !filtersActive,
            action: () => {
              setDialogOpen('cleanUp');
            },
            name: 'datastoreCleanup',
          },
        ]}
        permanentActions={[
          {
            label: t('filters.button'),
            name: 'datastoreFilters',
            disabled: !filtersActive,
            icon: iconNames.filter_alt,
            action: () => {
              setDialogOpen('filters');
            },
          },
        ]}
      />
      <div className={classes.wrapper}>
        <span>{`${t('activeFilters')}: ${
          getActiveFilters(t as any, filters, dataType).join(', ') ||
          t('common:none')
        }.`}</span>
      </div>
      {dialogOpen === 'filters' && (
        <FiltersDialog
          dataType={dataType}
          filters={filters}
          setFilters={setFilters}
          onClose={() => setDialogOpen(null)}
          open={dialogOpen === 'filters'}
        />
      )}
      <BulkDialog
        data={data}
        selectedRows={selectedRows}
        everythingSelected={everythingSelected}
        dataType={dataType}
        filters={filters}
        onClose={() => setDialogOpen(null)}
        open={dialogOpen === 'bulk'}
        refreshData={refreshData}
      />
      <CleanUpDialog
        dataType={dataType}
        onClose={() => setDialogOpen(null)}
        open={dialogOpen === 'cleanUp'}
        refreshData={refreshData}
      />
    </>
  );
};

export default ActionBarWrapper;
