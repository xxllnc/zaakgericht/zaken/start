// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  Rule,
  hideFields,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { filterKeys, filterValues, getFilterLabel } from '../DataStore.library';
import {
  DataTypeType,
  FiltersType,
  FilterValueType,
} from './../DataStore.types';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  dataType: DataTypeType,
  filters: FiltersType
) => AnyFormDefinitionField[];

export const getFiltersDialogFormDefinition: GetFormDefinitionType = (
  t,
  dataType,
  filters
) => {
  const formDefinition = filterKeys
    .filter(filterName => filterName !== 'freeform_filter')
    .map(filterName => ({
      name: filterName,
      type: fieldTypes.SELECT,
      // @ts-ignore
      value: filters[filterName],
      nestedValue: true,
      choices: filterValues.map((value: FilterValueType) => {
        const label = getFilterLabel(t, filterName, dataType, value);

        return {
          value,
          label,
        };
      }),
    }));

  return formDefinition;
};

export const getFiltersDialogRules = (dataType: DataTypeType) => [
  new Rule()
    .when(() => dataType === 'NatuurlijkPersoon')
    .then(showFields(['muncipality']))
    .else(hideFields(['muncipality'])),
];
