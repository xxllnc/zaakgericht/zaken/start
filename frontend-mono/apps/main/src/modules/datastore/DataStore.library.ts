// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  DataTypeType,
  FilterKeyType,
  FiltersType,
  FilterValueType,
  GetDateType,
  InitiateExportType,
  PerformBulkActionType,
  GetFilterLabelType,
  GetActiveFiltersType,
} from './DataStore.types';
import { fetchData, startExport, bulkAction } from './DataStore.requests';

export const dataTypeValues: DataTypeType[] = [
  'NatuurlijkPersoon',
  'Organisatie',
  'BagLigplaats',
  'BagNummeraanduiding',
  'BagOpenbareruimte',
  'BagPand',
  'BagStandplaats',
  'BagVerblijfsobject',
  'BagWoonplaats',
];

export const filterValues: FilterValueType[] = ['0', '1', '2'];

export const defaultDataType = 'NatuurlijkPersoon';

export const getContactType = (dataType: DataTypeType) =>
  dataType === 'NatuurlijkPersoon' ? 'np' : 'nnp';

export const filterKeys: FilterKeyType[] = [
  'object_subscription',
  'active',
  'cases',
  'running',
  'concepts',
  'address',
  'deceased',
  'muncipality', // [SIC]
  'freeform_filter',
];

export const defaultFilters: FiltersType = {
  freeform_filter: '',
  active: '0',
  address: '0',
  cases: '0',
  concepts: '0',
  deceased: '0',
  muncipality: '0',
  object_subscription: '0',
  running: '0',
  zapi_num_rows: 20,
  zapi_page: 0,
};

const formatLatLong = (latlong: any) => {
  const coordinates = latlong?.geometry?.coordinates;

  return coordinates ? coordinates.join(', ') : null;
};

const formatRow = (row: any) => ({
  ...row,
  name: row.uuid,
  afnemerindicatie: row?.object_subscription?.external_id,
  pending: row?.pending ? 'Ja' : 'Nee',
  vestiging_latlong: formatLatLong(row.vestiging_latlong),
  ...row.adres_id,
  id: row.id,
  // uuid is used for rowSelection
  uuid: row.id,
  realUuid: row.uuid,
});

const formatRows = (rows: any) => rows.map((row: any) => formatRow(row));

export const getData: GetDateType = async (dataType, filters) => {
  const { zapi_page, zapi_order_by, zapi_order_by_direction } = filters;
  const data = await fetchData(dataType, {
    ...filters,
    zapi_page: zapi_page + 1,
    zapi_order_by: zapi_order_by ? `me.${filters.zapi_order_by}` : undefined,
    zapi_order_by_direction: zapi_order_by_direction?.toLowerCase(),
  });
  const rows = formatRows(data.result);
  const count = data.num_rows;

  return {
    rows,
    count,
  };
};

export const initiateExport: InitiateExportType = async (dataType, filters) => {
  const response = await startExport(dataType, filters);

  const success = response.result[0].success;

  return Boolean(success);
};

export const performBulkAction: PerformBulkActionType = async ({
  type,
  selectedRows,
  everythingSelected,
  dataType,
  filters,
}) => {
  const contactType = getContactType(dataType);
  const selection_type = everythingSelected ? 'all' : 'subset';
  const response = await bulkAction(type, contactType, {
    ...filters,
    selection_type,
    selection_id: everythingSelected ? undefined : selectedRows,
  });

  return response.result[0].queue_items === '0';
};

export const getFilterLabel: GetFilterLabelType = (
  t,
  filterName,
  dataType,
  value
) => {
  switch (filterName) {
    case 'deceased': {
      return t(`filters.fields.deceased.${dataType}.${value}`);
    }
    case 'freeform_filter': {
      return t(`filters.fields.${filterName}.label`, { value });
    }
    default: {
      return t(`filters.fields.${filterName}.${value}`);
    }
  }
};

export const getActiveFilters: GetActiveFiltersType = (t, filters, dataType) =>
  filterKeys
    .filter(key => filters[key] && filters[key] !== '0')
    .map(key => getFilterLabel(t, key, dataType, filters[key]))
    .map((activeFilter, index) =>
      index === 0 ? activeFilter : activeFilter.toLowerCase()
    );
