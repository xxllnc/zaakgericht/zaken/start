// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useSession, {
  hasSystemRole,
} from '@zaaksysteem/common/src/hooks/useSession';

export const deleteDocument = async (
  body: APIDocument.DeleteDocumentRequestBody
) =>
  await request<APIDocument.DeleteDocumentResponseBody>(
    'POST',
    '/api/v2/document/delete_document',
    body
  );

export const useAssignmentChoices = (t: i18next.TFunction) => {
  const session = useSession();
  const isIntaker = hasSystemRole(session, 'Documentintaker');

  return [
    {
      label: t('assignment.assigned'),
      value: 'assigned',
    },
    ...(isIntaker
      ? [
          {
            label: t('assignment.assignedToMe'),
            value: 'assignedToMe',
          },
        ]
      : []),
    {
      label: t('assignment.unassigned'),
      value: 'unassigned',
    },
    {
      label: t('assignment.all'),
      value: 'all',
    },
  ];
};
