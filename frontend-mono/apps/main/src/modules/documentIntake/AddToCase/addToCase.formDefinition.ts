// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { DocumentItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { DOCUMENT_LABEL_SELECT } from './DocumentLabelSelect';

export type FormValuesType = {
  case_uuid: string;
  description: string;
  origin: 'Inkomend';
  document_label_uuids: { value: string; label: string }[];
  date: string | null;
  documentNames: string;
};

type InitialValuesType = {
  description?: string;
  origin?: string;
  date?: string | null;
  documentNames?: string;
  selectedDocuments: DocumentItemType[];
};

/* eslint complexity: [2, 8] */
export const getFormDefinition: (
  t: i18next.TFunction,
  initialValues: InitialValuesType,
  compact: boolean
) => FormDefinition<FormValuesType> = (t, initialValues, compact) => {
  const originChoices = [
    {
      label: t('addToCase.origin.choices.inkomend'),
      value: 'Inkomend',
    },
    {
      label: t('addToCase.origin.choices.uitgaand'),
      value: 'Uitgaand',
    },
    {
      label: t('addToCase.origin.choices.intern'),
      value: 'Intern',
    },
  ];
  return [
    {
      name: 'documentNames',
      type: fieldTypes.TEXT,
      label:
        initialValues.selectedDocuments.length > 1
          ? t('addToCase.name.labelMultiple')
          : t('addToCase.name.label'),
      placeholder: t('addToCase.name.placeholder'),
      value: initialValues.selectedDocuments
        .map(document => `${document.name}${document.extension}`)
        .join(', '),
      required: false,
      readOnly: true,
      config: {
        style: {
          wordWrap: 'break-word',
          padding: '8px 12px',
        },
      },
    },
    {
      name: 'case_uuid',
      type: fieldTypes.CASE_FINDER,
      value: '',
      required: true,
      nestedValue: true,
      label: t('addToCase.case_uuid.label'),
      isSearchable: true,
      config: { filter: { status: ['new', 'open', 'stalled'] } },
      placeholder: t('addToCase.case_uuid.placeholder'),
    },
    {
      name: 'description',
      type: fieldTypes.TEXTAREA,
      multi: true,
      value: initialValues.description,
      required: false,
      hidden: compact,
      label: t('addToCase.description.label'),
      placeholder: t('addToCase.description.placeholder'),
    },
    {
      name: 'origin',
      type: fieldTypes.SELECT,
      value:
        originChoices.find(option => option.value === initialValues.origin) ||
        originChoices[0],
      choices: originChoices,
      hidden: compact,
      label: t('addToCase.origin.label'),
    },
    {
      name: 'date',
      type: fieldTypes.DATEPICKER,
      value: initialValues.date,
      required: false,
      variant: 'inline',
      placeholder: t('documentIntake:properties.additional.date.placeholder'),
    },
    {
      name: 'document_label_uuids',
      type: DOCUMENT_LABEL_SELECT,
      value: [],
      isSearchable: false,
      required: false,
      isMulti: true,
      hidden: compact,
      label: t('addToCase.document_label_uuids.label'),
      noOptionsMessage: t('addToCase.document_label_uuids.noOptions'),
      placeholder: t('addToCase.document_label_uuids.placeholder'),
    },
  ];
};
