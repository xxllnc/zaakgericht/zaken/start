// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const addToCase = (
  body: APIDocument.AddDocumentToCaseRequestBody
): Promise<any> =>
  request<APIDocument.AddDocumentToCaseResponseBody>(
    'POST',
    '/api/v2/document/add_document_to_case',
    body
  ).then(response => {
    return response.data || [];
  });
