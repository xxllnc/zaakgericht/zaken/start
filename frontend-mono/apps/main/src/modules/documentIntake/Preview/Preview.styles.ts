// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePreviewStyles = makeStyles(() => {
  return {
    wrapper: {
      backgroundColor: 'white',
      minWidth: 100,
      minHeight: 100,
    },
    loaderWrapper: {
      paddingTop: 32,
    },
    loader: {
      margin: '0px auto',
    },
  };
});
