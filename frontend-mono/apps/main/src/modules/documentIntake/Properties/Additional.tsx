// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { useAdditionalStyles } from './Additional.styles';

type AdditionalValuesType = {
  document_number: number;
  version: number;
  mimetype: string;
  filesize: number;
  md5: string;
  status: string;
};

export const Additional: React.ComponentType<
  FormFieldPropsType<any, any, AdditionalValuesType>
> = ({ value }) => {
  const { document_number, version, mimetype, filesize, md5, status } = value;
  const classes = useAdditionalStyles();
  const [t] = useTranslation('documentIntake');

  const parseFileSize = (bytes: number) => {
    const round = (num: number) => Math.round(num * 10) / 10;

    return bytes < 1000000
      ? `${round(filesize / 1000)} kB`
      : `${round(filesize / 1000000)} MB`;
  };

  return (
    <div className={classes.wrapper}>
      <div className={classes.row}>
        <span>{t('properties.additional.documentNumber')}</span>
        <span>{document_number}</span>
      </div>
      <div className={classes.row}>
        <span>{t('properties.additional.version')}</span>
        <span>{version}</span>
      </div>
      <div className={classes.row}>
        <span>{t('properties.additional.mimetype')}</span>
        <span>{mimetype}</span>
      </div>
      <div className={classes.row}>
        <span>{t('properties.additional.filesize')}</span>
        <span>{parseFileSize(filesize)}</span>
      </div>
      <div className={classes.row}>
        <span>{t('properties.additional.md5')}</span>
        <span>{md5}</span>
      </div>
      <div className={classes.row}>
        <span>{t('properties.additional.integrity.label')}</span>
        <span>{status}</span>
      </div>
    </div>
  );
};

export default Additional;
