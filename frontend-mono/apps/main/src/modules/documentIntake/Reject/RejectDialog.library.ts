// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const rejectDocument = ({
  reason,
  uuid,
}: {
  reason: string;
  uuid: string;
}): Promise<APIDocument.RejectAssignedDocumentResponseBody> =>
  request<APIDocument.RejectAssignedDocumentResponseBody>(
    'POST',
    '/api/v2/document/reject_assigned_document',
    {
      document_uuid: uuid,
      rejection_reason: reason,
    }
  ).then(response => {
    return response.data || [];
  });
