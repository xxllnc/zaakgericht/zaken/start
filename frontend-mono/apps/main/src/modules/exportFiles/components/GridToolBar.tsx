// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarDensitySelector,
  GridToolbarExport,
  GridToolbarQuickFilter,
} from '@mui/x-data-grid-pro';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { useClientUtils } from '../hooks/useClientUtils';

const GridToolBar = ({ t, classes }: any) => {
  const [invalidate] = useClientUtils();

  return (
    <GridToolbarContainer className={classes.gridToolBarWrapper}>
      <GridToolbarColumnsButton />
      <GridToolbarFilterButton />
      <GridToolbarDensitySelector />
      <GridToolbarExport />
      <div className={classes.gridToolbar}>
        <IconButton onClick={() => invalidate()} size="small">
          <Tooltip title={t('refresh')} placement="left-start">
            <Icon size="small" color="primary">
              {iconNames.refresh}
            </Icon>
          </Tooltip>
        </IconButton>
        <GridToolbarQuickFilter />
      </div>
    </GridToolbarContainer>
  );
};

export default GridToolBar;
