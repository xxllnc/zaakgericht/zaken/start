// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { Trans } from 'react-i18next';
import {
  TEXT,
  CHECKBOX,
  SWITCH,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { APIJobs } from '@zaaksysteem/generated';
import {
  Rule,
  showFields,
  hideFields,
} from '@zaaksysteem/common/src/components/form/rules';
import * as yup from 'yup';
import { JobEntryType } from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { KindType } from '../../../advancedSearch/AdvancedSearch.types';

export type CopyDialogFormShape = {
  name: string;
  simulation: boolean;
  recalculate: boolean;
  summary: string;
  approve: boolean;
};

export const getFormDefinition = ({
  job,
  kind,
  t,
}: {
  job: JobEntryType;
  kind: KindType;
  t: i18next.TFunction;
}) => [
  {
    name: 'name',
    type: TEXT,
    required: true,
    label: t('copyDialog:fields.name.label'),
    value: `${job.name} (${t('common:copy')})`,
    placeholder: t('copyDialog:fields.name.placeholder'),
  },
  {
    name: 'simulation',
    type: SWITCH,
    value: true,
    label: t('copyDialog:fields.simulation.label'),
    suppressWrapperLabel: true,
  },
  ...(job.selection.type === 'filter'
    ? [
        {
          name: 'recalculate',
          type: CHECKBOX,
          value: false,
          label: t(`copyDialog:recalculate.label`, {
            kind: t(`copyDialog:kind.${kind}`),
          }),
          suppressWrapperLabel: true,
        },
      ]
    : []),
  {
    name: 'summary',
    type: 'summary',
    required: false,
  },
  {
    name: 'approve',
    type: CHECKBOX,
    value: false,
    label: t('copyDialog:fields.approve.label'),
    suppressWrapperLabel: true,
  },
];

export const JobSummary =
  ({
    job,
    classes,
    t,
    kind,
  }: {
    job: JobEntryType;
    classes: any;
    t: i18next.TFunction;
    kind: KindType;
  }) =>
  (params: FormFieldPropsType<CopyDialogFormShape>) => {
    const { selection, name } = job;
    const {
      values: { simulation, recalculate },
    } = params.formik;

    const introTranslation = t('copyDialog:summary.intro', {
      name,
    });
    const kindTranslation = t(`copyDialog:kind.${kind}`);
    const calcTranslation = t(
      `copyDialog:summary.recalculate.${recalculate === true ? 'yes' : 'no'}`,
      {
        kind: kindTranslation,
        type: t(`copyDialog:summary.recalculate.types.${selection.type}`, {
          kind: kindTranslation,
        }),
      }
    );

    return (
      <p className={classes.summaryOverview}>
        {introTranslation}
        <Trans
          i18nKey={`copyDialog:summary.simulation.${simulation ? 'yes' : 'no'}`}
          components={[<strong key={'simulation-translation'} />]}
        />
        {calcTranslation}
      </p>
    );
  };

export const formRules = [
  new Rule()
    .when('simulation', field => field.value === true)
    .then(hideFields(['approve']))
    .else(showFields(['approve'])),
];

export const validationMap = {
  approve: ({ t }: { t: i18next.TFunction }) => {
    const errorMsg = t('copyDialog:fields.approve.error');
    return yup.boolean().required(errorMsg).oneOf([true], errorMsg);
  },
};

export const getPayload = ({
  job,
  values,
}: {
  job: JobEntryType;
  values: CopyDialogFormShape;
}): APIJobs.CreateJobRequestBody => {
  const { name, simulation, recalculate } = values;
  const { uuid, jobType = '', selection } = job;

  const getJobType = () => {
    const base = jobType.replace('simulate_', '');
    return `${simulation ? 'simulate_' : ''}${base}` as JobEntryType['jobType'];
  };

  const getSelection = (): JobEntryType['selection'] =>
    recalculate
      ? selection
      : {
          source_job: uuid,
          type: 'source_job',
        };

  return {
    job_description: {
      job_type: getJobType(),
      selection: getSelection(),
    },
    friendly_name: name,
  };
};
