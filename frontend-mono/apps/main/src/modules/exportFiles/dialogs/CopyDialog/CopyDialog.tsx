// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { getJobKind } from '@zaaksysteem/common/src/components/Jobs/JobDialogs.library';
import { JobEntryType } from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useMutation } from '@tanstack/react-query';
import { createJob } from '@zaaksysteem/common/src/components/Jobs/Jobs.requests';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { useStyles } from './CopyDialog.style';
import { getFormDefinition, getPayload } from './CopyDialog.library';
import {
  JobSummary,
  formRules,
  validationMap,
  CopyDialogFormShape,
} from './CopyDialog.library';
import locale from './CopyDialog.locale';

type CopyDialogPropsType = {
  onClose: () => void;
  t: i18next.TFunction;
  job: JobEntryType;
  onSuccess: () => void;
};

/* eslint complexity: [2, 12] */
export const CopyDialog: FunctionComponent<CopyDialogPropsType> = React.memo(
  ({ onClose, t, job, onSuccess }) => {
    const classes = useStyles();
    const createMutation = useMutation<any, V2ServerErrorsType, any>(payload =>
      createJob(payload)
    );
    const kind = getJobKind(job.jobType as string);

    return (
      <FormDialog
        key={'copy-dialog'}
        icon="memory"
        open={true}
        title={t('copyDialog:title')}
        fieldComponents={{
          summary: JobSummary({ job, classes, t, kind }),
        }}
        formDefinition={getFormDefinition({ job, kind, t }) as any}
        rules={formRules}
        //@ts-ignore
        validationMap={validationMap}
        onClose={onClose}
        onSubmit={async (values: CopyDialogFormShape) => {
          createMutation.mutate(getPayload({ job, values }), {
            onSuccess: () => {
              openSnackbar({
                message: t('copyDialog:success'),
                sx: snackbarMargin,
              });
              if (onSuccess) onSuccess();
              onClose();
            },
            onError: openServerError,
          });
        }}
      />
    );
  }
);

export default (props: CopyDialogPropsType) => (
  <I18nResourceBundle resource={locale} namespace="copyDialog">
    <CopyDialog {...props} />
  </I18nResourceBundle>
);
