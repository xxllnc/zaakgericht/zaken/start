// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as yup from 'yup';
import fecha from 'fecha';
import { v4 } from 'uuid';
import * as i18next from 'i18next';
import {
  TEXT,
  CHECKBOX,
  CHECKBOX_GROUP,
  MULTI_VALUE_TEXT,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { JobEntryType } from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { getInitialState } from '../../../advancedSearch/components/EditForm/EditForm.library';
import { KindType } from '../../../advancedSearch/AdvancedSearch.types';
import { DATE_FORMAT } from '../../library/ExportFiles.library';

export const MAX_ROWS_NEW_SAVED_SEARCH = 500;

type NewSavedSearchFormShape = {
  abort: string;
  intro: string;
  name: string;
  basedOn: string[];
  labels: ValueType<string>[];
  open: boolean;
};

export type CombinedResultsType = {
  errors: any[];
  warnings: any[];
  successes: any[];
};

export const isValidChoice =
  ({
    combinedResults,
    t,
  }: {
    combinedResults: CombinedResultsType | null;
    t: i18next.TFunction;
  }) =>
  () => {
    if (!combinedResults) return false;
    return yup.mixed().test(
      'is-valid-total',
      t('newSavedSearchDialog:fields.basedOn.error', {
        maxRows: MAX_ROWS_NEW_SAVED_SEARCH,
      }),
      val => {
        let total = 0;
        ((val as string[]) || []).forEach(
          //@ts-ignore
          element => (total += combinedResults[element].length)
        );
        return total > 0 && total <= MAX_ROWS_NEW_SAVED_SEARCH;
      }
    );
  };

/* eslint complexity: [2, 12] */
export const getFormDefinition = ({
  combinedResults,
  kind,
  abort,
  t,
  job,
}: {
  combinedResults: CombinedResultsType | null;
  kind: KindType;
  abort: boolean;
  t: i18next.TFunction;
  job: JobEntryType;
}): FormDefinition<NewSavedSearchFormShape> => {
  const jobTypeTranslation = t(`jobs:jobTypes.${job.jobType}`);
  const FIELDS_BASE = 'newSavedSearchDialog:fields';
  const getKind = (count: number) =>
    t(`common:kind.${kind}`, {
      count,
    });

  if (abort) {
    return [
      {
        name: 'abort',
        type: TEXT,
        required: false,
        label: '',
        readOnly: true,
        value: t(`${FIELDS_BASE}.abort`),
      },
    ];
  }
  return [
    {
      name: 'intro',
      type: TEXT,
      required: false,
      label: '',
      readOnly: true,
      value: t(`${FIELDS_BASE}.intro`),
      format: 'text',
    },
    {
      name: 'name',
      type: TEXT,
      required: true,
      label: t(`${FIELDS_BASE}.name.label`),
      placeholder: t(`${FIELDS_BASE}.name.placeholder`),
      format: 'text',
      value: `${jobTypeTranslation} - ${job.name} - ${fecha.format(new Date(), DATE_FORMAT)}`,
    },
    {
      name: 'basedOn',
      type: CHECKBOX_GROUP,
      choices: [
        ...(combinedResults && isPopulatedArray(combinedResults.successes)
          ? [
              {
                label: `${t('newSavedSearchDialog:combinedResults.successes')}: ${combinedResults.successes.length} ${getKind(combinedResults.successes.length)}`,
                value: 'successes',
              },
            ]
          : []),
        ...(combinedResults && isPopulatedArray(combinedResults.warnings)
          ? [
              {
                label: `${t('newSavedSearchDialog:combinedResults.warnings')}: ${combinedResults.warnings.length} ${getKind(combinedResults.warnings.length)}`,
                value: 'warnings',
              },
            ]
          : []),
        ...(combinedResults && isPopulatedArray(combinedResults.errors)
          ? [
              {
                label: `${t('newSavedSearchDialog:combinedResults.errors')}: ${combinedResults.errors.length} ${getKind(combinedResults.errors.length)}`,
                value: 'errors',
              },
            ]
          : []),
      ],
      label: t(`${FIELDS_BASE}.basedOn.label`),
      required: true,
      isClearable: false,
      multiValue: true,
      value: [],
    },
    {
      name: 'labels',
      type: MULTI_VALUE_TEXT,
      label: t(`${FIELDS_BASE}.labels.label`),
      required: false,
      value: [
        {
          value: 'jobs',
          label: t(`${FIELDS_BASE}.labels.default`),
        },
      ],
    },
    {
      name: 'open',
      type: CHECKBOX,
      label: t(`${FIELDS_BASE}.open`),
      required: false,
      suppressWrapperLabel: true,
      value: true,
    },
  ];
};

export const getSavedSearchPayload = ({
  formValues,
  combinedResults,
  t,
  kind,
}: {
  formValues: NewSavedSearchFormShape;
  combinedResults: CombinedResultsType | null;
  t: i18next.TFunction;
  kind: KindType;
}) => {
  let savedSearch = getInitialState({
    mode: 'new',
    kind,
    t,
  });
  const { name } = formValues;

  if (!combinedResults) return;

  const values = Object.entries(combinedResults).reduce(
    (accumulator, currentValue) => {
      const [type, thisResults] = currentValue;
      //@ts-ignore
      if (formValues.basedOn.includes(type)) accumulator.push(...thisResults);
      return accumulator;
    },
    []
  );

  //@ts-ignore
  savedSearch.filters.filters = [
    {
      uuid: v4(),
      type: 'attributes.case_number',
      operator: 'or',
      options: {
        truncate: true,
      },
      values: values.map(val => ({
        label: val,
        value: val,
      })),
    },
  ];

  savedSearch.name = name;
  return savedSearch;
};
