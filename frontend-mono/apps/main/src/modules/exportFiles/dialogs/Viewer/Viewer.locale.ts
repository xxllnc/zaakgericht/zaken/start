// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Verkenner',
    columns: {
      caseID: 'Zaaknummer',
      simulation: 'Simulatie',
      remarks: 'Opmerkingen',
      errors: 'Fouten',
    },
    noContents: 'Dit bestand heeft geen inhoud.',
  },
};
