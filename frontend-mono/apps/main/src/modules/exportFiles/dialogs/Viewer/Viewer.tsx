// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useTheme } from '@mui/material';
import { DataGridPro, GridToolbar } from '@mui/x-data-grid-pro';
import {
  Dialog as UIDialog,
  DialogTitle,
} from '@mintlab/ui/App/Material/Dialog';
import {
  FieldType,
  JobEntryType,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import Button from '@mintlab/ui/App/Material/Button';
import GRID_DEFAULT_LOCALE_TEXT from '@zaaksysteem/common/src/locale/datagrid.locale';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import { useFileQuery } from '../../ExportFiles.query';
import locale from './Viewer.locale';
import { useStyles } from './Viewer.style';
import { getColumns, openURL } from './Viewer.library';

type ViewerDialogPropsType = {
  onClose: () => void;
  t: i18next.TFunction;
  fieldType: FieldType;
  job: JobEntryType;
};

/* eslint complexity: [2, 12] */
const ViewerDialog: FunctionComponent<ViewerDialogPropsType> = ({
  job,
  fieldType,
  onClose,
  t,
}) => {
  const classes = useStyles();
  const { jobType, status, uuid } = job;
  const theme = useTheme<Theme>();
  const fileQuery = useFileQuery({
    job,
    fieldType,
    parse: true,
    enabled: Boolean(uuid && status === 'finished'),
  });

  return (
    <React.Fragment>
      {/* @ts-ignore */}
      <UIDialog onClose={onClose} open={true} fullWidth={true} maxWidth={'lg'}>
        <DialogTitle
          title={t('viewer:title')}
          icon="preview"
          onCloseClick={onClose}
        />
        <Divider />
        <DialogContent className={classes.contentWrapper}>
          {fileQuery.isSuccess && !fileQuery?.data && t('viewer:noContents')}

          <div className={classes.tableWrapper}>
            <DataGridPro
              className={classes.viewerDataGrid}
              key={'results-file-viewer'}
              rows={fileQuery?.data || []}
              columns={getColumns({
                fieldType,
                t,
                jobType,
                theme,
              })}
              pagination={true}
              disableRowSelectionOnClick={true}
              localeText={GRID_DEFAULT_LOCALE_TEXT}
              loading={fileQuery.isFetching}
              onRowClick={openURL}
              slots={{
                toolbar: GridToolbar,
              }}
              slotProps={{
                toolbar: {
                  showQuickFilter: true,
                },
              }}
            />
          </div>
        </DialogContent>
        <Divider />
        <div className={classes.actionButtons}>
          <Button
            name="viewer-close"
            action={onClose}
            title={t('common:dialog.close')}
          >
            {t('verbs.close')}
          </Button>
        </div>
      </UIDialog>
    </React.Fragment>
  );
};

export default (props: ViewerDialogPropsType) => (
  <I18nResourceBundle resource={locale} namespace="viewer">
    <ViewerDialog {...props} />
  </I18nResourceBundle>
);
