// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQueryClient } from '@tanstack/react-query';
import { QUERY_KEY_JOBS } from '../library/ExportFiles.library';

export const useClientUtils = () => {
  const client = useQueryClient();

  const invalidate = () => {
    client.invalidateQueries([QUERY_KEY_JOBS]);
  };
  return [invalidate];
};
