// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  GridRowClassNameParams,
  GridRowSpacingParams,
} from '@mui/x-data-grid-pro';
import {
  AnyFileType,
  FieldType,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import { AnyEntryType } from '@zaaksysteem/common/src/components/Jobs/Jobs.types';

export const QUERY_KEY_JOBS = 'jobs';
export const QUERY_KEY_FILE = 'viewerFile';
export const DATE_FORMAT = 'D MMM YYYY H:m';
export const REFETCH_INTERVAL = 5000;

export const getRowClassName =
  (classes: any) =>
  ({ row }: GridRowClassNameParams<AnyEntryType>) =>
    row.isLastInGroup ? classes.rowLastInGroup : '';

export const getRowSpacing =
  () =>
  ({ model }: GridRowSpacingParams) => {
    return model.isLastInGroup
      ? {
          top: 0,
          bottom: 40,
        }
      : {};
  };

export const initiateDownload = (fieldType: FieldType, job: AnyEntryType) => {
  //@ts-ignore
  window.top.location = getURL({ fieldType, job });
};

export const getURL = ({
  fieldType,
  job,
}: {
  fieldType: FieldType;
  job: AnyEntryType;
}) => {
  const { type, uuid } = job;
  if (type === 'job') {
    const baseUrl =
      fieldType === 'errorFile'
        ? '/api/v2/jobs/download_errors'
        : '/api/v2/jobs/download_result';
    return buildUrl(baseUrl, {
      job_uuid: uuid,
    });
  } else if (type === 'legacy_download') {
    return `/api/v2/cm/export/download_export_file?token=${job.token}`;
  }
};

export const getCombinedResults = (
  resultRows?: AnyFileType[],
  errorRows?: AnyFileType[]
) => {
  const getId = (row: AnyFileType) => row.caseID;
  const errors = (errorRows || []).map(getId);

  const warnings = (resultRows || [])
    .filter(row => row.type === 'warning')
    .map(getId);

  const successes = (resultRows || [])
    .filter(row => row.type === 'result')
    .map(getId);

  return {
    errors,
    successes,
    warnings,
  };
};
