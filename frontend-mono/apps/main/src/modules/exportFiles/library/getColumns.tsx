// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Dispatch, SetStateAction } from 'react';
import * as i18next from 'i18next';
import formatDistance from 'date-fns/formatDistance';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  AnyEntryType,
  FieldType,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import { GridColDef, GridRenderCellParams } from '@mui/x-data-grid-pro';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import MiniProgress from '@mintlab/ui/App/Zaaksysteem/MiniProgress/MiniProgress';
import { DialogType } from '../ExportFiles.types';
import { initiateDownload } from './ExportFiles.library';

export const getColumns = ({
  t,
  classes,
  setDialog,
  theme,
  localeModule,
}: {
  t: i18next.TFunction;
  classes: any;
  setDialog: Dispatch<SetStateAction<DialogType | null>>;
  theme: Theme;
  localeModule: Locale | null;
}): GridColDef<AnyEntryType>[] => [
  {
    field: 'name',
    headerName: t('exportFiles:columns.name'),
    flex: 1,
    renderCell: ({ row }) => {
      const { name, type } = row;
      const getIconName = () => {
        if (type === 'job' && row.simulation) return iconNames.view_in_ar;
        if (type === 'legacy_download') return iconNames.insert_drive_file;
        return iconNames.memory;
      };

      const getTooltip = () => {
        if (type === 'legacy_download') {
          return t('jobs:types.legacy_download');
        } else if (type === 'job') {
          return t(`jobs:jobSimulation.${row.simulation ? 'yes' : 'no'}`);
        }
        return '';
      };

      return (
        <div className={classes.nameCell}>
          <Tooltip title={getTooltip()} placement="right">
            <Icon size="small" color="primary">
              {getIconName()}
            </Icon>
          </Tooltip>
          <div>{name}</div>
        </div>
      );
    },
  },
  {
    field: 'type',
    headerName: t('exportFiles:columns.type'),
    width: 150,
    renderCell: (row: GridRenderCellParams<AnyEntryType>) => {
      const { type } = row.row;
      if (type === 'job') {
        return t(`jobs:jobTypes.${row.row.jobType}`) as string;
      } else if (type === 'legacy_download') {
        return t('jobs:types.legacy_download') as string;
      }
      return null;
    },
  },
  {
    field: 'status',
    headerName: t('exportFiles:columns.status'),
    width: 150,
    renderCell: ({
      row: { status, progress },
    }: GridRenderCellParams<AnyEntryType>) => {
      if (['running', 'finished'].includes(status)) {
        return (
          <div style={{ width: '100%' }}>
            <MiniProgress progress={progress} showPercentage={true} />
          </div>
        );
      } else {
        return t(`exportFiles:status.${status}`) as string;
      }
    },
  },
  {
    field: 'expires',
    headerName: t('exportFiles:columns.expires'),
    renderCell: renderDateCell(localeModule),
    width: 150,
  },
  {
    field: 'resultsFile',
    headerName: '',
    sortable: false,
    width: 110,
    resizable: false,
    disableReorder: true,
    renderCell: renderFileCell({ t, setDialog, classes }),
    renderHeader: () => (
      <div className={classes.fileHeader}>
        <Icon
          size="small"
          sx={{
            color: theme.palette.grass.main,
            marginLeft: '6px',
          }}
        >
          {iconNames.check_circle}
        </Icon>
        <Icon
          size="small"
          sx={{
            color: theme.palette.review.main,
            marginLeft: '6px',
          }}
        >
          {iconNames.warning}
        </Icon>
      </div>
    ),
  },
  {
    field: 'errorFile',
    headerName: '',
    sortable: false,
    width: 110,
    resizable: false,
    disableReorder: true,
    renderCell: renderFileCell({ t, setDialog, classes }),
    renderHeader: () => (
      <div className={classes.fileHeader}>
        <Icon
          size="small"
          sx={{
            color: theme.palette.error.dark,
            marginLeft: '6px',
          }}
        >
          {iconNames.error}
        </Icon>
      </div>
    ),
  },
  {
    field: 'actions',
    headerName: t('exportFiles:columns.actions'),
    sortable: false,
    renderCell: renderActionsCell({ classes, setDialog, t }),
    width: 60,
    resizable: false,
    disableReorder: true,
  },
  {
    field: 'moreActions',
    headerName: '',
    sortable: false,
    renderCell: renderMoreActionsCell({ setDialog, t }),
    width: 60,
    resizable: false,
    disableReorder: true,
  },
];

const renderActionsCell =
  ({
    classes,
    setDialog,
    t,
  }: {
    classes: any;
    setDialog: any;
    t: i18next.TFunction;
  }) =>
  (row: GridRenderCellParams<AnyEntryType>) => {
    const {
      row: { status, uuid, name },
    } = row;

    return (
      <div className={classes.actionsCell}>
        {['running', 'pending'].includes(status) && (
          <IconButton
            onClick={() =>
              setDialog({ type: 'cancelJob', parameters: { uuid, name } })
            }
            size="small"
          >
            <Tooltip
              title={t('exportFiles:actions.cancel.buttonTitle')}
              placement="right"
            >
              <Icon size="small" color="primary">
                {iconNames.cancel}
              </Icon>
            </Tooltip>
          </IconButton>
        )}
      </div>
    );
  };

const renderMoreActionsCell =
  ({ setDialog, t }: { setDialog: any; t: i18next.TFunction }) =>
  (row: GridRenderCellParams<AnyEntryType>) => {
    const entry = row.row;
    const { status, uuid, name, type } = entry;
    let actions = [];
    const TRANSLATION_BASE = 'exportFiles:actions';

    if (
      type === 'job' &&
      ['finished'].includes(status) &&
      entry.jobType &&
      entry.jobType.indexOf('delete_case') > -1
    ) {
      actions.push({
        action: () => {
          setDialog({
            type: 'newSavedSearch',
            parameters: {
              job: row.row,
            },
          });
        },
        title: t(`${TRANSLATION_BASE}.newSavedSearch.buttonTitle`),
      });
    }
    if (
      type === 'job' &&
      ['delete_case', 'delete_case_simulation'].includes(entry.jobType) &&
      ['finished'].includes(status)
    ) {
      actions.push({
        action: () => {
          setDialog({
            type: 'copyJob',
            parameters: {
              job: row.row,
            },
          });
        },
        title: t(`${TRANSLATION_BASE}.copy.buttonTitle`),
      });
    }

    if (type === 'job' && ['cancelled', 'finished'].includes(status)) {
      actions.push({
        action: () => {
          setDialog({
            type: 'deleteJob',
            parameters: {
              uuid,
              name,
            },
          });
        },
        title: t(`${TRANSLATION_BASE}.delete.buttonTitle`),
      });
    }

    if (!actions.length) return null;

    return (
      <div>
        <DropdownMenu
          transformOrigin={{ horizontal: 'center', vertical: 'top' }}
          trigger={
            <Button
              name="savedSearchMore"
              icon="more_vert"
              scope={`more-actions:button`}
              iconSize="small"
            />
          }
        >
          <DropdownMenuList
            items={actions.map(({ action, title }) => ({
              action,
              label: title,
              scope: `more-actions:foldout-menu`,
            }))}
          />
        </DropdownMenu>
      </div>
    );
  };

const renderDateCell =
  (localeModule: Locale | null) =>
  ({ value }: GridRenderCellParams<AnyEntryType>) => {
    if (!value || !localeModule) return '';
    return formatDistance(new Date(value), new Date(), {
      addSuffix: true,
      locale: localeModule,
    });
  };

/* eslint complexity: [2, 14] */
const renderFileCell =
  ({
    t,
    setDialog,
    classes,
  }: {
    t: i18next.TFunction;
    setDialog: any;
    classes: any;
  }) =>
  (row: GridRenderCellParams<AnyEntryType>) => {
    const field = row.field as FieldType;
    const thisRow: AnyEntryType = row.row;
    const { type, status, resultsFileSize, resultsFileType } = thisRow;

    if (status !== 'finished') return null;
    if (field === 'errorFile' && type === 'legacy_download') return null;
    const hasFile = () => {
      if (type === 'legacy_download') {
        return true;
      } else {
        return (
          (field === 'resultsFile' && resultsFileSize) ||
          (field === 'errorFile' && thisRow.errorsFileSize)
        );
      }
    };

    const downloadButtonEnabled = () => Boolean(hasFile());
    const viewerButtonEnabled = () =>
      ((Boolean(type === 'job') &&
        (
          (field === 'resultsFile' && resultsFileType?.toLowerCase()) ||
          ''
        )?.indexOf('csv') > -1) ||
        field === 'errorFile') &&
      hasFile();

    return (
      <div className={classes.fileCell}>
        {viewerButtonEnabled() && (
          <IconButton
            onClick={() => {
              setDialog({
                type: 'viewer',
                parameters: {
                  job: row.row,
                  fieldType: field,
                },
              });
            }}
            size="small"
          >
            <Tooltip
              title={t('exportFiles:actions.preview.buttonTitle')}
              placement="right"
            >
              <Icon size="small" color="primary">
                {iconNames.preview}
              </Icon>
            </Tooltip>
          </IconButton>
        )}
        {downloadButtonEnabled() && (
          <IconButton
            onClick={() => {
              initiateDownload(field, row.row);
            }}
            size="small"
          >
            <Tooltip
              title={t('exportFiles:actions.download.buttonTitle')}
              placement="right"
            >
              <Icon size="small" color="primary">
                {iconNames.download}
              </Icon>
            </Tooltip>
          </IconButton>
        )}
      </div>
    );
  };
