// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    log: 'Logboek',
    export: {
      button: 'Exporteren',
      snack:
        'Exportbestand wordt gegenereerd. Er wordt na afronding een bericht verstuurd.',
    },
    filters: {
      keyword: 'Trefwoord',
      caseId: 'Zaaknummer',
      user: 'Behandelaar',
    },
    table: {
      noResults: 'Geen resultaten',
      labelRowsPerPage: 'Per pagina:',
      columns: {
        caseId: 'Zaak #',
        dateCreated: 'Datum',
        event: 'Gebeurtenis',
        component: 'Component',
        user: 'Behandelaar',
      },
      components: {
        api: 'Api',
        authentication: 'Authenticatie',
        berichtenbox: 'Berichtenbox',
        betrokkene: 'Betrokkene',
        case: 'Zaak',
        document: 'Document',
        documenten: 'Document',
        email: 'E-mail',
        export: 'Export',
        event: 'Gebeurtenis',
        interface: 'Interface',
        kenmerk: 'Kenmerk',
        notificatie: 'Notificatie',
        Sim: 'Simloket',
        sjabloon: 'Sjabloon',
        sysin: 'Systeem',
        transaction: 'Transactie',
        user: 'Gebruiker',
        woz_objects: 'WOZ object',
        zaak: 'Zaak',
        zaaktype: 'Zaaktype',
        folder: 'Map',
        object_type: 'Objecttype',
        custom_object_type: 'Objecttype',
        custom_object: 'Object',
      },
    },
  },
};
