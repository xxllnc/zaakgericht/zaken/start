// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { DataType, FiltersType } from './Log.types';
import { defaultFilters, getData } from './Log.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const Log: React.ComponentType = () => {
  const [t] = useTranslation('log');

  const [filters, setFilters] = useState<FiltersType>(defaultFilters);

  const { data, isFetching } = useQuery<DataType, V2ServerErrorsType>(
    ['data', filters],
    () => getData(filters),
    {
      staleTime: 0,
      keepPreviousData: true,
      onError: openServerError,
    }
  );

  return (
    <Sheet>
      <ActionBar filters={filters} setFilters={setFilters} />
      {data ? (
        <DataTable
          data={data}
          loading={isFetching}
          filters={filters}
          setFilters={setFilters}
        />
      ) : (
        <Loader />
      )}
      <TopbarTitle title={t('log')} />
    </Sheet>
  );
};

export default Log;
