// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useAdminBanner } from '../../library/auth';
import Log from './Log';
import locale from './Log.locale';

type LogRouteType = {};

const LogModule: React.ComponentType<LogRouteType> = () => {
  const banner = useAdminBanner.system();
  return (
    banner || (
      <I18nResourceBundle resource={locale} namespace="log">
        <Log />
      </I18nResourceBundle>
    )
  );
};

export default LogModule;
