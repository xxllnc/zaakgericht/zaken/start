// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    documentAttributes: {
      title: 'Document',
    },
    thumbnail: {
      placeholder: 'Geen voorvertoning beschikbaar',
    },
    relatedDocuments: {
      documentsInFile: 'Documenten in dossier',
      placeholder: 'Dit document is geen onderdeel van een dossier.',
    },
  },
};
