// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useNext2KnowStyles = makeStyles(
  ({ typography, mintlab: { greyscale }, palette: { common } }: Theme) => ({
    wrapper: {
      display: 'flex',
      gap: 20,
      width: '100%',
      overflow: 'auto',
    },
    documentAttributes: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
      padding: 20,
      paddingBottom: 100,
    },
    fields: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    field: {
      display: 'flex',
      gap: 20,
    },
    fieldCompact: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    label: {
      minWidth: 215,
      maxWidth: 215,
      textWrap: 'wrap',
      fontWeight: typography.fontWeightBold,
    },
    valueMultilineWrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    valueMultiline: {
      display: '-webkit-box',
      '-webkit-line-clamp': 5,
      '-webkit-box-orient': 'vertical',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    sidebar: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
      padding: 20,
      paddingBottom: 100,
      minWidth: 600,
      overflow: 'auto',
    },
    thumbnail: {
      width: '100%',
      borderRadius: 8,
      backgroundColor: greyscale.light,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      maxWidth: 560,
      border: `1px solid ${common.black}`,
    },
    placeholder: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      gap: 10,
      padding: 20,
      color: greyscale.darkest,
    },
    relatedDocuments: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    relatedDocumentsFields: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
    },
  })
);
