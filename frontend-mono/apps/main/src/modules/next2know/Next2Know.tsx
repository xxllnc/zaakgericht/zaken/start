// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { Box } from '@mui/material';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Button from '@mintlab/ui/App/Material/Button';
import { useNext2KnowStyles } from './Next2Know.styles';
import { DocumentInfoType } from './Next2Know.types';

type Next2KnowPropsType = DocumentInfoType & { documentId: string };

/* eslint complexity: [2, 8] */
const Next2Know: React.FunctionComponent<Next2KnowPropsType> = ({
  documentId,
  documentAttributes,
  thumbnail,
  relatedDocuments,
}) => {
  const classes = useNext2KnowStyles();
  const [t] = useTranslation('next2know');

  const [fullTextFolded, setFullTextFolded] = useState<boolean>(true);

  const width = useWidth();
  const isCompact = ['xs', 'sm', 'md'].includes(width);

  return (
    <PanelLayout className={classes.wrapper}>
      <Panel className={classes.documentAttributes}>
        <SubHeader title={t('documentAttributes.title')} />
        <div className={classes.fields}>
          {documentAttributes.map(({ label, name, value }) => (
            <Box
              key={name}
              className={classes.field}
              sx={{ flexDirection: isCompact ? 'column' : 'row' }}
            >
              <div className={classes.label}>{label}</div>
              {value && `${value}`.replaceAll('\n', '') ? (
                name === 'fulltext' ? (
                  <div className={classes.valueMultilineWrapper}>
                    <div>
                      <Button
                        name="fullText"
                        variant="outlined"
                        onClick={() => setFullTextFolded(!fullTextFolded)}
                      >
                        {fullTextFolded
                          ? t('common:verbs.unfold')
                          : t('common:verbs.fold')}
                      </Button>
                    </div>
                    <div
                      className={
                        fullTextFolded ? classes.valueMultiline : undefined
                      }
                    >
                      {Boolean(value) &&
                        `${value}`.split('\n').map((val, index) => (
                          <React.Fragment key={index}>
                            {val}
                            <br />
                          </React.Fragment>
                        ))}
                    </div>
                  </div>
                ) : (
                  <div>{value}</div>
                )
              ) : (
                <div>-</div>
              )}
            </Box>
          ))}
        </div>
      </Panel>
      <Panel type="side" className={classes.sidebar}>
        <div className={classes.thumbnail}>
          {thumbnail ? (
            <img
              className={classes.image}
              alt="preview"
              src={`data:image/png;base64,${thumbnail.data}`}
            />
          ) : (
            <div className={classes.placeholder}>
              <Icon size="large" color="inherit">
                {iconNames.image_not_supported}
              </Icon>
              <span>{t('thumbnail.placeholder')}</span>
            </div>
          )}
        </div>
        <div className={classes.relatedDocuments}>
          <SubHeader title={t('relatedDocuments.documentsInFile')} />
          {relatedDocuments && relatedDocuments.length > 1 ? (
            <div className={classes.relatedDocumentsFields}>
              {relatedDocuments.map(({ _id, _index, _source: { Naam } }) =>
                documentId === _id ? (
                  <div key={_id}>{Naam}</div>
                ) : (
                  <Link key={_id} to={`../${_index}/${_id}`}>
                    {Naam}
                  </Link>
                )
              )}
            </div>
          ) : (
            <div>{t('relatedDocuments.placeholder')}</div>
          )}
        </div>
      </Panel>
    </PanelLayout>
  );
};

export default Next2Know;
