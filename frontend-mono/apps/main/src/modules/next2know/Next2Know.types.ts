// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  Next2KnowType,
  HeadersType,
} from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Search/ExternalSearch/next2know/next2know.types';

// document attributes

type DocumentAttributeType = { label: string; name: string; value: string };

export type FetchDocumentAttributesParamsType = { format: 'array' };

export type DocumentAttributesResponseBodyType = {
  data: DocumentAttributeType[];
};

export type FetchDocumentAttributesType = (
  endpoint: string,
  searchIndex: string,
  documentId: string,
  headers: HeadersType
) => Promise<DocumentAttributesResponseBodyType['data']>;

// thumbnail

type ThumbnailType = any;

export type ThumbnailResponseBodyType = any;

export type FetchThumbnailType = (
  endpoint: string,
  searchIndex: string,
  documentId: string,
  headers: HeadersType
) => Promise<ThumbnailResponseBodyType>;

// related documents

type RelatedDocumentType = {
  _id: string;
  _index: string;
  _source: { Naam: string; Omschrijving: string };
};

export type FetchRelatedDocumentsParamsType = { q: string; size: number };

export type RelatedDocumentsResponseBodyType = {
  data: {
    hits: RelatedDocumentType[];
  };
};

export type FetchRelatedDocumentsType = (
  endpoint: string,
  params: FetchRelatedDocumentsParamsType,
  headers: HeadersType
) => Promise<RelatedDocumentType[] | null>;

// other

export type FindAttributeType = (
  documentAttributes: DocumentAttributeType[],
  key: string
) => string;

export type DocumentInfoType = {
  title: string;
  documentAttributes: DocumentAttributeType[];
  thumbnail: ThumbnailType | null;
  relatedDocuments: RelatedDocumentType[] | null;
};

export type GetDocumentInfoType = (
  integration: Next2KnowType,
  searchIndex: string,
  documentId: string
) => Promise<DocumentInfoType>;
