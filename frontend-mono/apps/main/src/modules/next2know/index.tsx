// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, useParams } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { openServerError } from '@zaaksysteem/common/src/signals';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { getIntegration } from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Search/ExternalSearch/next2know/next2know.library';
import Next2Know from './Next2Know';
import locale from './Next2Know.locale';
import { getDocumentInfo } from './Next2Know.library';

type Next2KnowParamsType = {
  searchIndex: string;
  documentId: string;
};

const ONE_HOUR = 60 * 60 * 1000;

const Next2KnowModule = () => {
  const { searchIndex, documentId } = useParams() as Next2KnowParamsType;

  const { data: integration } = useQuery(
    ['next2know'],
    () => getIntegration(),
    {
      onError: openServerError,
      staleTime: ONE_HOUR,
    }
  );

  const { data: documentInfo } = useQuery(
    ['next2knowDocumentInfo', documentId, integration],
    () =>
      integration
        ? getDocumentInfo(integration, searchIndex, documentId)
        : null,
    {
      onError: openServerError,
    }
  );

  if (!documentInfo) return <Loader />;

  return (
    <>
      <Next2Know documentId={documentId} {...documentInfo} />
      <TopbarTitle title={documentInfo.title} />
    </>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="next2know">
    <Sheet>
      <Routes>
        <Route path=":searchIndex/:documentId" element={<Next2KnowModule />} />
      </Routes>
    </Sheet>
  </I18nResourceBundle>
);
