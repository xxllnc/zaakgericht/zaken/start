// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { useAdminBanner } from '../../library/auth';
import { InlineFrameLoader } from '../../components/InlineFrame/InlineFrameLoader';
import locale from './locale';

const ObjectImportModule = () => {
  const banner = useAdminBanner.catalog();
  const [t] = useTranslation('objectImport');

  return (
    banner || (
      <Sheet>
        <TopbarTitle
          breadcrumbs={[
            { label: t('moduleParentName'), path: '/main/catalog' },
            { label: t('moduleName') },
          ]}
        />
        <InlineFrameLoader
          url={window.location.pathname + window.location.search}
        />
      </Sheet>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="objectImport">
    <ObjectImportModule />
  </I18nResourceBundle>
);
