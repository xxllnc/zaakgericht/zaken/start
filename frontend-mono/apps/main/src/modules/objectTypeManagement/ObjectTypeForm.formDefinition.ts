// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  CHECKBOX_GROUP,
  TEXTAREA,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  ATTRIBUTE_LIST,
  CASETYPE_RELATION_LIST,
  OBJECTTYPE_RELATION_LIST,
  TEXT,
} from '../../components/Form/Constants/fieldTypes';
import {
  GetObjectTypeFormDefinition,
  GetObjectTypeFormSteps,
} from './ObjectTypeManagement.types';

export const getObjectTypeFormDefinition: GetObjectTypeFormDefinition = t => [
  {
    name: 'custom_fields',
    type: ATTRIBUTE_LIST,
    format: 'object',
    value: [],
    required: false,
    multiValue: true,
    placeholder: t('form.fields.custom_fields.placeholder'),
  },
  {
    name: 'components_changed',
    type: CHECKBOX_GROUP,
    value: [],
    choices: [
      {
        label: t('form.fields.components_changed.choices.generic'),
        value: 'attributes',
      },
      {
        label: t('form.fields.components_changed.choices.attributes'),
        value: 'custom_fields',
      },
      {
        label: t('form.fields.components_changed.choices.relations'),
        value: 'relationships',
      },
      {
        label: t('form.fields.components_changed.choices.authorizations'),
        value: 'authorizations',
      },
    ],
    required: true,
    label: t('form.fields.components_changed.label'),
  },
  {
    name: 'changes',
    type: TEXTAREA,
    isMultiline: true,
    value: '',
    required: true,
    label: t('form.fields.changes.label'),
    help: t('form.fields.changes.help'),
    placeholder: t('form.fields.changes.placeholder'),
  },

  {
    name: 'name',
    type: TEXT,
    value: '',
    required: true,
    label: t('form.fields.name.label'),
    help: t('form.fields.name.help'),
    placeholder: t('form.fields.name.placeholder'),
  },
  {
    name: 'title',
    type: TEXT,
    value: '',
    required: true,
    label: t('form.fields.title.label'),
    help: t('form.fields.title.help'),
    placeholder: t('form.fields.title.placeholder'),
  },
  {
    name: 'subtitle',
    type: TEXT,
    value: '',
    required: false,
    label: t('form.fields.subtitle.label'),
    help: t('form.fields.subtitle.help'),
    placeholder: t('form.fields.subtitle.placeholder'),
  },
  {
    name: 'external_reference',
    type: TEXT,
    value: '',
    label: t('form.fields.external_reference.label'),
    help: t('form.fields.external_reference.help'),
    placeholder: t('form.fields.external_reference.placeholder'),
  },

  {
    name: 'caseTypeRelations',
    type: CASETYPE_RELATION_LIST,
    format: 'object',
    value: [],
    multiValue: true,
    label: t('form.fields.caseTypeRelations.label'),
    help: t('form.fields.caseTypeRelations.help'),
    placeholder: t('form.fields.custom_fields.placeholder'),
  },
  {
    name: 'objectTypeRelations',
    type: OBJECTTYPE_RELATION_LIST,
    format: 'object',
    value: [],
    multiValue: true,
    label: t('form.fields.objectTypeRelations.label'),
    help: t('form.fields.objectTypeRelations.help'),
    placeholder: t('form.fields.custom_fields.placeholder'),
  },
  {
    format: 'object',
    multiValue: true,
    minItems: 1,
    name: 'authorizations',
    type: 'AuthorizationList',
    value: [],
    required: true,
  },
];

export const getObjectTypeFormSteps: GetObjectTypeFormSteps = t => [
  {
    title: t('form.steps.general.title'),
    description: t('form.steps.general.description'),
    fieldNames: ['name', 'title', 'subtitle', 'external_reference'],
  },
  {
    title: t('form.steps.attributes.title'),
    fieldNames: ['custom_fields'],
  },
  {
    title: t('form.steps.relations.title'),
    fieldNames: ['caseTypeRelations', 'objectTypeRelations'],
  },
  {
    title: t('form.steps.rights.title'),
    fieldNames: ['authorizations'],
  },
  {
    title: t('form.steps.audit.title'),
    fieldNames: ['components_changed', 'changes'],
  },
];
