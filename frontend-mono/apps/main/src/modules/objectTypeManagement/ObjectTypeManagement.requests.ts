// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchObjectTypeType,
  ObjectTypeResponseBodyType,
  SubmitObjectTypeType,
} from './ObjectTypeManagement.types';

export const fetchObjectType: FetchObjectTypeType = async uuid => {
  const url = buildUrl(
    '/api/v2/cm/custom_object_type/get_persistent_custom_object_type',
    { uuid }
  );

  const response = await request<ObjectTypeResponseBodyType>('GET', url);

  return response;
};

export const submitObjectType: SubmitObjectTypeType = async (
  action: string,
  data: any,
  uuid: string
) => {
  const url = buildUrl(
    `/api/v2/cm/custom_object_type/${action}_custom_object_type`,
    { uuid }
  );

  await request('POST', url, data);
};
