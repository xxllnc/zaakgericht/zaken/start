// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import SideMenu from './components/Menu/Menu';
import Map from './components/Map/Map';
import Attributes from './components/Attributes/Attributes';
import Relationships from './components/Relationships/Relations';
import Timeline from './components/Timeline/Timeline';
import { DataType } from './ObjectView.types';

const ObjectView: React.FunctionComponent<DataType> = props => (
  <PanelLayout>
    <Panel type="side">
      <SideMenu />
    </Panel>
    <Panel>
      <Routes>
        <Route path="" element={<Navigate to="attributes" replace={true} />} />
        <Route path="attributes" element={<Attributes {...props} />} />
        <Route path="timeline" element={<Timeline {...props} />} />
        <Route path="map" element={<Map {...props} />} />
        <Route path="relationships" element={<Relationships {...props} />} />
      </Routes>
    </Panel>
  </PanelLayout>
);

export default ObjectView;
