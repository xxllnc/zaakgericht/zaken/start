// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

export type CustomFieldType = { type: string; value: any };

export type ObjectType = {
  uuid: string;
  registrationDate?: string;
  lastModified?: string;
  status?: 'active' | 'inactive' | 'draft';
  customFieldsValues: { [key: string]: CustomFieldType };
  title: string;
  objectTypeVersionUuid: string;
  versionIndependentUuid: string;
  relatedCasesUuids?: string[];
  authorizations: ('read' | 'readwrite' | 'admin')[];
};

export type GetObjectType = (uuid: string) => Promise<ObjectType>;

export type ObjectTypeType =
  APICaseManagement.GetCustomObjectTypeResponseBody['data']['attributes'] & {
    customFieldsDefinition: APICaseManagement.CustomObjectTypeCustomFieldDefinition[];
  };

export type GetObjectTypeType = (uuid: ObjectType) => Promise<ObjectTypeType>;

export type DataType = {
  object: ObjectType;
  objectType: ObjectTypeType;
};

export type FetchDataType = (uuid: string) => Promise<DataType>;
