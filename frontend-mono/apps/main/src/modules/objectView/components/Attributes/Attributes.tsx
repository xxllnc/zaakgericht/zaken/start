// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import useSession, {
  hasCapability,
} from '@zaaksysteem/common/src/hooks/useSession';
import { openServerError } from '@zaaksysteem/common/src/signals';
import generateCustomFieldValues from '@zaaksysteem/common/src/components/form/library/generateCustomFieldValues';
import { useQueryClient } from '@tanstack/react-query';
import { DataType } from '../../ObjectView.types';
import { activateObject, updateObjectAction } from '../../ObjectView.library';
import { OBJECT_VIEW_DATA } from '../..';
import { useAttributesStyles } from './Attributes.style';
import { getSystemAttributesFormDefinition } from './systemAttributes.formDefinition';
import Header from './Header';
import Form from './Form';
import { getCustomFormDefinition } from './Attribute.library';

export type AttributeType = {
  type: string;
  name: string;
  value: string;
  label?: string;
  description?: string;
};

type AttributesWrapperPropsType = DataType;

const AttributesWrapper: React.FunctionComponent<
  AttributesWrapperPropsType
> = props => {
  const [editing, setEditing] = useState(false);
  const toggleEditing = () => setTimeout(() => setEditing(!editing));

  return (
    <Attributes
      key={editing ? 1 : 0}
      {...props}
      editing={editing}
      toggleEditing={toggleEditing}
    />
  );
};

type AttributesPropsType = AttributesWrapperPropsType & {
  editing: boolean;
  toggleEditing: () => void;
};

/* eslint complexity: [2, 14] */
const Attributes: React.FunctionComponent<AttributesPropsType> = ({
  object,
  objectType,
  editing,
  toggleEditing,
}) => {
  const classes = useAttributesStyles();
  const [t] = useTranslation('objectView');
  const client = useQueryClient();

  const session = useSession();
  const isAdmin = objectType.authorizations.includes('admin');
  const isAllowedSystemAttributes = isAdmin || hasCapability(session, 'admin');

  const formDefinition = getCustomFormDefinition(
    t as any,
    object,
    objectType,
    editing,
    isAllowedSystemAttributes
  );
  const {
    fields,
    formik: { values },
  } = useForm({ formDefinition });

  const systemFormDefinition = getSystemAttributesFormDefinition(
    t as any,
    object
  );
  const systemForm = useForm({ formDefinition: systemFormDefinition });

  if (!object || !objectType) {
    return <Loader />;
  }

  const save = () => {
    updateObjectAction(
      object.relatedCasesUuids || [],
      object.uuid,
      generateCustomFieldValues(values, formDefinition)
    )
      .then(() => {
        client.invalidateQueries([OBJECT_VIEW_DATA]);
        toggleEditing();
      })
      .catch(openServerError);
  };

  const activate = () => {
    activateObject(
      object.relatedCasesUuids || [],
      object.uuid,
      generateCustomFieldValues(values, fields)
    )
      .then(() => {
        const newObject = { ...object, status: 'active' };

        client.setQueriesData([OBJECT_VIEW_DATA], {
          object: newObject,
          objectType,
        });
      })
      .catch(openServerError);
  };

  return (
    <div className={classes.wrapper}>
      <Header
        editing={editing}
        toggleEditing={toggleEditing}
        object={object}
        objectType={objectType}
        isAdmin={isAdmin}
        save={save}
        activate={activate}
      />
      <Form type="custom" fields={fields} />
      <Form type="system" fields={systemForm.fields} />
    </div>
  );
};

export default AttributesWrapper;
