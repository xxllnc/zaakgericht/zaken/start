// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { useAttributesStyles } from './Attributes.style';

type CreateDialogPropsType = {
  caseTypeChoices: { label: string; value: string }[];
  setCaseTypeUuid: (caseTypeUuid: string) => void;
  onClose: () => void;
  open: boolean;
};

const CreateDialog: React.ComponentType<CreateDialogPropsType> = ({
  caseTypeChoices,
  setCaseTypeUuid,
  onClose,
  open,
}) => {
  const classes = useAttributesStyles();
  const [t] = useTranslation('objectView');
  const dialogEl = useRef();

  const formDefinition = [
    {
      name: 'caseType',
      value: null,
      type: fieldTypes.SELECT,
      required: true,
      label: t('create.fields.caseType'),
      choices: caseTypeChoices,
    },
  ];

  let {
    fields,
    formik: {
      isValid,
      values: { caseType },
    },
  } = useForm({
    formDefinition,
  });

  const title = t('common:verbs.create');

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={'create-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon="delete"
        title={title}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div className={classes.dialogContent}>
          <span>{t('delete.description')}</span>
          {fields.map(({ FieldComponent, key, ...rest }: any) => {
            return (
              <FormControlWrapper
                {...rest}
                compact={true}
                key={`${rest.name}-formcontrol-wrapper`}
              >
                <FieldComponent
                  {...rest}
                  t={t}
                  containerRef={dialogEl.current}
                />
              </FormControlWrapper>
            );
          })}
        </div>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: title,
                onClick() {
                  setCaseTypeUuid(caseType.value);
                  onClose();
                },
                disabled: !isValid,
              },
              {
                text: t('common:verbs.cancel'),
                onClick: onClose,
              },
            ],
            'create-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default CreateDialog;
