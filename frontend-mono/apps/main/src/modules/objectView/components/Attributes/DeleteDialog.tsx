// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { ObjectType } from '../../ObjectView.types';
import { deleteObjectAction } from '../../ObjectView.library';
import { useAttributesStyles } from './Attributes.style';

type DeleteDialogPropsType = {
  object: ObjectType;
  onClose: () => void;
  open: boolean;
};

const DeleteDialog: React.ComponentType<DeleteDialogPropsType> = ({
  object,
  onClose,
  open,
}) => {
  const classes = useAttributesStyles();
  const [t] = useTranslation('objectView');
  const dialogEl = useRef();

  const formDefinition = [
    {
      name: 'reason',
      value: null,
      type: fieldTypes.TEXTAREA,
      required: true,
      label: t('delete.fields.reason'),
      rows: 3,
    },
  ];

  let {
    fields,
    formik: {
      isValid,
      values: { reason },
    },
  } = useForm({
    formDefinition,
  });

  const title = t('common:verbs.delete');

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={'delete-custom-object-type-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon="delete"
        title={title}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div className={classes.dialogContent}>
          <span>{t('delete.description')}</span>
          {fields.map(({ FieldComponent, key, ...rest }: any) => {
            return (
              <FormControlWrapper
                {...rest}
                compact={true}
                key={`${rest.name}-formcontrol-wrapper`}
              >
                <FieldComponent
                  {...rest}
                  t={t}
                  containerRef={dialogEl.current}
                />
              </FormControlWrapper>
            );
          })}
        </div>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: title,
                onClick() {
                  deleteObjectAction(object.uuid, reason).then(() => {
                    window.location.href = '/intern';
                  });
                },
                disabled: !isValid,
              },
              {
                text: t('common:verbs.cancel'),
                onClick: onClose,
              },
            ],
            'delete-custom-object-type-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default DeleteDialog;
