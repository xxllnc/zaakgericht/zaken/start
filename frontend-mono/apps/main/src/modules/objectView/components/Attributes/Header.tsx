// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import CaseCreateDialog from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Dialogs/CaseCreate/CaseCreateDialog';
import { ObjectType, ObjectTypeType } from '../../ObjectView.types';
import DeleteDialog from './DeleteDialog';
import { useAttributesStyles } from './Attributes.style';
import CreateDialog from './CreateDialog';

type HeaderPropsType = {
  editing: boolean;
  toggleEditing: () => void;
  object: ObjectType;
  objectType: ObjectTypeType;
  isAdmin: boolean;
  save: () => void;
  activate: () => void;
};

/* eslint complexity: [2, 14] */
const Header: React.FunctionComponent<HeaderPropsType> = ({
  editing,
  toggleEditing,
  object,
  objectType,
  isAdmin,
  save,
  activate,
}) => {
  const [t] = useTranslation('objectView');
  const classes = useAttributesStyles();

  const [creating, setCreating] = useState<boolean>(false);
  const [caseTypeUuid, setCaseTypeUuid] = useState<string>();
  const [deleting, setDeleting] = useState<boolean>(false);

  const isActive = object.status === 'active';

  const caseTypeChoices = (
    objectType?.relationship_definition?.relationships || []
  )
    .filter(relation => Boolean(relation.case_type_uuid))
    .map(caseType => ({
      label: caseType.name,
      value: caseType.case_type_uuid || '',
    }));
  const canCreate = !editing && Boolean(caseTypeChoices.length);

  return (
    <div className={classes.header}>
      <div className={classes.buttons}>
        {canCreate && (
          <Button action={() => setCreating(true)} name="createCase">
            {t('attributes.actions.create')}
          </Button>
        )}

        {!editing && !isActive && isAdmin && (
          <Button action={activate} name="activateObject">
            {t('attributes.actions.activate')}
          </Button>
        )}

        {!editing && isActive && (
          <Button
            action={() => {
              toggleEditing();
            }}
            name="editObject"
          >
            {t('attributes.actions.edit')}
          </Button>
        )}
        {editing && (
          <Button
            action={() => {
              toggleEditing();
            }}
            variant="outlined"
            name="toggleReadOnlyObject"
          >
            {t('attributes.actions.cancel')}
          </Button>
        )}
        {editing && (
          <Button action={save} name="uploadObject">
            {t('attributes.actions.save')}
          </Button>
        )}

        {isAdmin && (
          <>
            <Button
              action={() => {
                setDeleting(true);
              }}
              name="deleteObject"
            >
              {t('attributes.actions.delete')}
            </Button>
          </>
        )}
      </div>
      {creating && (
        <CreateDialog
          caseTypeChoices={caseTypeChoices}
          setCaseTypeUuid={setCaseTypeUuid}
          onClose={() => setCreating(false)}
          open={creating}
        />
      )}
      {deleting && (
        <DeleteDialog
          object={object}
          onClose={() => setDeleting(false)}
          open={deleting}
        />
      )}
      <CaseCreateDialog
        open={Boolean(caseTypeUuid)}
        onClose={() => setCaseTypeUuid(undefined)}
        savedCaseTypeUuid={caseTypeUuid}
        allowCaseTypeChange={false}
        object={{ label: object.title, value: object.uuid }}
        objectType={{
          label: objectType.title as string,
          value: objectType.version_independent_uuid as string,
        }}
      />
    </div>
  );
};

export default Header;
