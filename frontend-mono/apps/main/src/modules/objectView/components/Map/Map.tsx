// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIGeo } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { DataType } from '../../ObjectView.types';

const getObjectGeoFeatures = async (
  uuid: string
): Promise<GeoJSON.FeatureCollection | undefined> => {
  const body = (await request(
    'GET',
    buildUrl<APIGeo.GetGeoFeaturesRequestParams>(
      '/api/v2/geo/get_geo_features',
      { uuid: [uuid] }
    )
  ).catch(openServerError)) as APIGeo.GetGeoFeaturesResponseBody | null;

  return body?.data && body.data[0]
    ? {
        type: 'FeatureCollection',
        features: body.data.reduce(
          (acc, dataEntry) =>
            acc.concat(dataEntry.attributes?.geo_json.features || []),
          []
        ) as any,
      }
    : undefined;
};

const Map: React.FunctionComponent<DataType> = ({ object, objectType }) => {
  const [objectGeoFeatures, setObjectGeoFeatures] = useState<
    GeoJSON.FeatureCollection | undefined
  >(undefined);

  React.useEffect(
    () =>
      void getObjectGeoFeatures(object.versionIndependentUuid).then(
        setObjectGeoFeatures
      ),
    []
  );

  return (
    <GeoMap
      geoFeature={objectGeoFeatures || null}
      name="ObjectMap"
      canDrawFeatures={false}
      minHeight="100%"
      context={{
        type: 'ObjectMap',
        data: { object, objectType },
      }}
    />
  );
};

export default Map;
