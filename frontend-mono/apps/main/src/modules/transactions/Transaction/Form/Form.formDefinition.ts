// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { RequestDataType, TransactionType } from '../../Transactions.types';
import { formatDate } from '../../Transactions.library';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  transaction: TransactionType,
  requestData: RequestDataType
) => AnyFormDefinitionField[];

export const getFormDefinition: GetFormDefinitionType = (
  t,
  { uuid, externalId, created, direction, errorMessage },
  requestData
) =>
  [
    {
      name: 'transactionUuid',
      type: fieldTypes.TEXT,
      value: uuid,
    },
    {
      name: 'externalId',
      type: fieldTypes.TEXT,
      value: externalId === 'unknown' ? t('table.unknown') : externalId,
    },
    {
      name: 'created',
      type: fieldTypes.TEXT,
      value: formatDate(created),
    },
    {
      name: 'direction',
      type: fieldTypes.TEXT,
      value: t(`table.values.direction.${direction}`),
    },
    {
      name: 'requestData',
      type: fieldTypes.TEXTAREA,
      value: requestData,
      readOnly: true,
      maxRows: 10,
    },
    ...(errorMessage
      ? [
          {
            name: 'errorMessage',
            type: fieldTypes.TEXTAREA,
            value: errorMessage,
            readOnly: true,
            maxRows: 10,
          },
        ]
      : []),
  ].map(field => ({
    label: t(`transaction.${field.name}`),
    readOnly: true,
    ...field,
  }));
