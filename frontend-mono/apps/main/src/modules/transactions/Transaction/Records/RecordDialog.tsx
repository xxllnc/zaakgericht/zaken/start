// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { RecordType } from '../../Transactions.types';
import { useBulkDialogStyles } from './RecordDialog.style';
import { getFormDefinition } from './RecordDialog.formdefinition';

type FiltersDialogPropsType = {
  record: RecordType;
  onClose: () => void;
  open: boolean;
};

const BulkDialog: React.ComponentType<FiltersDialogPropsType> = ({
  record,
  onClose,
  open,
}) => {
  const [t] = useTranslation('transactions');
  const classes = useBulkDialogStyles();
  const dialogEl = useRef();

  const title = record.preview || record.name;

  const formDefinition = getFormDefinition(t as any, record);

  let { fields } = useForm({
    formDefinition,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'transaction-record-dialog'}
        ref={dialogEl}
        fullWidth={true}
        maxWidth={'lg'}
      >
        <DialogTitle
          elevated={true}
          icon="text_snippet"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.formWrapper}>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default BulkDialog;
