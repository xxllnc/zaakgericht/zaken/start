// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetRecordsType,
  GetRequestDataType,
  GetTransactionType,
  RecordsResponseBodyType,
} from '../Transactions.types';
import { formatTransaction } from '../Transactions/Transactions.library';
import {
  fetchTransaction,
  fetchRecords,
  fetchRequestData,
} from './Transaction.requests';

export const getTransaction: GetTransactionType = async (
  transactionUuid,
  integrations
) => {
  const response = await fetchTransaction(transactionUuid);
  const transaction = formatTransaction(response.data, integrations);

  return transaction;
};

const formatRecords = ({
  id,
  attributes: { is_error, date_executed, input, output },
  meta,
}: RecordsResponseBodyType['data'][number]) => ({
  uuid: id,
  name: id,
  preview: meta?.summary,
  result: Boolean(is_error),
  executed: date_executed,
  inputData: input,
  input,
  output,
});

export const getRecords: GetRecordsType = async transactionUuid => {
  const params = { transaction_uuid: transactionUuid };
  const response = await fetchRecords(params);
  const records = response.data.map(formatRecords);

  return records;
};

export const getRequestData: GetRequestDataType = async transactionUuid => {
  const params = { uuid: transactionUuid };
  const response = await fetchRequestData(params);
  const requestData = response.data.attributes.input_data;

  return requestData;
};
