// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchTransactionType,
  FetchRecordsType,
  TransactionParamsType,
  TransactionResponseBodyType,
  RecordsParamsType,
  RecordsResponseBodyType,
  FetchRequestDataType,
  RequestDataParamsType,
  RequestDataResponseBodyType,
} from './../Transactions.types';

export const fetchTransaction: FetchTransactionType = async transactionUuid => {
  const url = buildUrl<TransactionParamsType>(
    `/api/v2/admin/integrations/get_transaction`,
    { uuid: transactionUuid }
  );

  const response = await request<TransactionResponseBodyType>('GET', url);

  return response;
};

export const fetchRecords: FetchRecordsType = async params => {
  const url = buildUrl<RecordsParamsType>(
    `/api/v2/admin/integrations/get_transaction_records`,
    params
  );

  const response = await request<RecordsResponseBodyType>('GET', url);

  return response;
};

export const fetchRequestData: FetchRequestDataType = async params => {
  const url = buildUrl<RequestDataParamsType>(
    '/api/v2/admin/integrations/get_transaction_data',
    params
  );

  const response = await request<RequestDataResponseBodyType>('GET', url);

  return response;
};
