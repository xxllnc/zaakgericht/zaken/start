// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { IntegrationType } from '../Transactions.types';
import {
  getTransaction,
  getRecords,
  getRequestData,
} from './Transaction.library';
import Form from './Form/Form';
import Records from './Records/Records';
import { useTransactionStyles } from './Transaction.style';

type TransactionParamsType = {
  transactionUuid: string;
};

type TransactionPropsType = { integrations: IntegrationType[] };

const Transaction: React.ComponentType<TransactionPropsType> = ({
  integrations,
}) => {
  const [t] = useTranslation('transactions');
  const classes = useTransactionStyles();
  const { transactionUuid } = useParams<
    keyof TransactionParamsType
  >() as TransactionParamsType;

  const { data: transaction } = useQuery(
    ['transaction', transactionUuid],
    () => getTransaction(transactionUuid, integrations),
    {
      onError: openServerError,
    }
  );

  const { data: records } = useQuery(
    ['records', transactionUuid],
    () => getRecords(transactionUuid),
    {
      onError: openServerError,
    }
  );

  const { data: requestData } = useQuery(
    ['request_data', transactionUuid],
    () => getRequestData(transactionUuid),
    {
      onError: openServerError,
    }
  );

  if (
    !transaction ||
    !records ||
    requestData === null ||
    requestData === undefined
  ) {
    return <Loader />;
  }

  return (
    <Sheet classes={{ sheet: classes.sheet }}>
      <Form transaction={transaction} requestData={requestData} />
      <Records records={records} />
      <TopbarTitle
        breadcrumbs={[
          { label: t('transactions'), path: '/main/transactions' },
          { label: transaction.interface },
        ]}
      />
    </Sheet>
  );
};

export default Transaction;
