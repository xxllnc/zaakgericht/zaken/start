// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { sortSupportedColumnDict } from '../Transactions.library';
import { cellRenderer } from './DataTableCell';

export const supportSort = ['direction', 'externalId', 'created'];

export const getColumns = (t: i18next.TFunction, classes: any) =>
  [
    {
      name: 'status',
      width: 50,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'record',
      width: 1,
      minWidth: 300,
      flexGrow: 1,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'direction',
      width: 100,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'externalId',
      width: 270,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'nextAttempt',
      width: 200,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'created',
      width: 200,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'records',
      width: 70,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
    {
      name: 'errors',
      width: 70,
      cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
    },
  ].map(column => ({
    label: t(`transaction.${column.name}`),
    disableSort: !Object.keys(sortSupportedColumnDict).includes(column.name),
    minWidth: column.width,
    ...column,
  }));
