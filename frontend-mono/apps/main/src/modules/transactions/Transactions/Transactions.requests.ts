// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchIntegrationsType,
  FetchTransactionsType,
  IntegrationsResponseBodyType,
  PerformActionType,
  PostManualTransactionType,
  TransactionsParamsType,
  TransactionsResponseBodyType,
} from '../Transactions.types';

export const fetchIntegrations: FetchIntegrationsType = async () => {
  const url = '/api/v2/admin/integrations/get_integrations';

  const response = await request<IntegrationsResponseBodyType>('GET', url);

  return response;
};

export const fetchTransactions: FetchTransactionsType = async params => {
  const url = buildUrl<TransactionsParamsType>(
    '/api/v2/admin/integrations/get_transactions',
    params
  );

  const response = await request<TransactionsResponseBodyType>('GET', url);

  return response;
};

export const performAction: PerformActionType = async (action, data) => {
  const url = `/sysin/transaction/${action}`;

  const response = await request('POST', url, data);

  return response;
};

export const postManualTransaction: PostManualTransactionType = async data => {
  const url = `/sysin/interface/${data.interface}/manual_process`;

  const response = await request('POST', url, data);

  return response;
};
