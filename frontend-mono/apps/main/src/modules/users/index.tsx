// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { useAdminBanner } from '../../library/auth';
import { InlineFrameLoader } from '../../components/InlineFrame/InlineFrameLoader';
import locale from './locale';

const UsersModule = () => {
  const banner = useAdminBanner.users();
  const [t] = useTranslation('users');

  return (
    banner || (
      <Sheet>
        <TopbarTitle breadcrumbs={[{ label: t('moduleName') }]} />
        <InlineFrameLoader url="/main/users" />
      </Sheet>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="users">
    <UsersModule />
  </I18nResourceBundle>
);
