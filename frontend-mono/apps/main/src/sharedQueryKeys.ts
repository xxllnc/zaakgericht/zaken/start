// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const GET_CASE_DOCUMENTS = 'GET_CASE_DOCUMENTS';
export const GET_DOCUMENT_VERSIONS = 'GET_DOCUMENT_VERSIONS';
