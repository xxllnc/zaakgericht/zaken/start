// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import classNames from 'classnames';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTranslation } from 'react-i18next';
import { IntegrationType } from '../Mor.types';
import { useUrl } from '../Mor.library';
import { QUERY_CASE } from '../Mor.constants';
import { useCaseViewStyles } from './CaseView.styles';
import Header from './Header/Header';
import { getCase } from './CaseView.library';
import { CaseObjType } from './CaseView.types';
import Content from './Content/Content';

type CaseViewPropsType = {
  integration: IntegrationType;
};

const CaseView: React.ComponentType<CaseViewPropsType> = ({ integration }) => {
  const [t] = useTranslation('mor');
  const classes = useCaseViewStyles();
  const { caseNumber } = useUrl();

  const { data: caseObj, error } = useQuery<
    CaseObjType | null,
    V2ServerErrorsType
  >([QUERY_CASE, caseNumber], () =>
    caseNumber ? getCase(t as any, integration, caseNumber) : null
  );

  return (
    <div
      className={classNames(
        classes.wrapper,
        caseNumber ? classes.open : classes.closed
      )}
    >
      <Header integration={integration} caseObj={caseObj} />
      {caseObj ? <Content caseObj={caseObj} /> : <Loader />}
      {error && <ServerErrorDialog />}
    </div>
  );
};

export default CaseView;
