// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';
import { scrollToTop } from '@mintlab/ui/App/Zaaksysteem/Swiper/Swiper.library';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import classNames from 'classnames';
import { CaseObjType } from '../CaseView.types';
import {
  filterAttribute,
  performTakeCase,
  resolveCase,
} from '../CaseView.library';
import { queryClient } from '../../../App';
import { QUERY_CASE, QUERY_NEW, QUERY_OPEN } from '../../Mor.constants';
import { useContentStyles } from './Content.styles';
import Attribute from './Attribute';
import { getFormDefinition } from './Form';

type ContentPropsType = {
  caseObj: CaseObjType;
};

/* eslint complexity: [2, 11] */
const Content: React.ComponentType<ContentPropsType> = ({ caseObj }) => {
  const [t] = useTranslation('mor');
  const classes = useContentStyles();
  const navigate = useNavigate();

  const [saving, setSaving] = useState<boolean>(false);
  const [resolving, setResolving] = useState<boolean>(false);

  const { uuid, status, specialAttributes, attributes, documents } = caseObj;

  const take = () => {
    setSaving(true);
    performTakeCase(uuid)
      .then(() => {
        const newCaseObj = { ...caseObj, status: 'open' };

        queryClient.setQueriesData([QUERY_CASE], newCaseObj);
        queryClient.invalidateQueries([QUERY_OPEN]);
        queryClient.invalidateQueries([QUERY_NEW]);
      })
      .finally(() => {
        setSaving(false);
      });
  };

  const resolve = () => {
    setSaving(true);
    resolveCase(uuid, fields)
      .then(() => {
        queryClient.invalidateQueries([QUERY_OPEN]);
        scrollToTop();
        navigate('..');
      })
      .catch(() => {
        setSaving(false);
      });
  };

  const formDefinition = getFormDefinition(specialAttributes);
  const validationMap = generateValidationMap(formDefinition);

  let {
    fields,
    formik: { isValid },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <div className={classes.wrapper}>
      {status !== 'resolved' && (
        <div className={classes.button}>
          {status === 'new' && (
            <Button
              variant="outlined"
              name={'case-take'}
              action={take}
              disabled={saving}
            >
              {t('case.button.take')}
            </Button>
          )}
          {status === 'open' && !resolving && (
            <Button
              variant="outlined"
              name={'case-resolve'}
              action={() => setResolving(true)}
            >
              {t('case.button.resolve')}
            </Button>
          )}
          {resolving && (
            <Button
              variant="outlined"
              name={'case-cancel'}
              action={() => setResolving(false)}
            >
              {t('case.button.cancel')}
            </Button>
          )}
        </div>
      )}
      {status !== 'resolved' && (
        <div
          className={classNames(
            classes.form,
            resolving ? classes.formOpen : classes.formClosed
          )}
        >
          {fields.map(({ FieldComponent, mode, ...props }) => (
            <FormControlWrapper
              {...props}
              compact={true}
              key={`${props.name}-formcontrol-wrapper`}
            >
              <FieldComponent {...props} />
            </FormControlWrapper>
          ))}
          <Button
            variant="contained"
            name={'case-cancel'}
            action={resolve}
            disabled={!isValid || saving}
          >
            {t('case.button.resolve')}
          </Button>
        </div>
      )}
      {status === 'resolved' && (
        <div className={classes.specialAttributes}>
          <Typography variant="h5">
            <Icon size="small">{iconNames.done_all}</Icon>
            {t('case.done')}
          </Typography>
          {specialAttributes.filter(filterAttribute).map(attr => (
            <Attribute key={attr.magic_string} attr={attr} />
          ))}
        </div>
      )}
      <div className={classes.attributes}>
        {attributes.map(attr => (
          <Attribute key={attr.magic_string} attr={attr} />
        ))}
        {Boolean(documents.length) && (
          <div className={classes.attribute}>
            <Typography variant="h6">{t('case.attachments')}</Typography>
            {documents.map(doc => (
              <div key={doc.uuid} className={classes.document}>
                <Icon size="small">{iconNames.file_copy}</Icon>
                <a href={`/api/v1/case/${uuid}/document/${doc.uuid}/download`}>
                  {doc.name}
                </a>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Content;
