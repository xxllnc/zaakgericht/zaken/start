// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { AttributeType } from '../CaseView.types';

const fieldDict = {
  text: fieldTypes.TEXT,
  textarea: fieldTypes.TEXTAREA,
  option: fieldTypes.RADIO_GROUP,
  checkbox: fieldTypes.CHECKBOX_GROUP,
};

type GetFormDefinitionType = (
  attributes: AttributeType[]
) => AnyFormDefinitionField[];

export const getFormDefinition: GetFormDefinitionType = attributes =>
  attributes.map(({ label, value, type, magic_string, choices, required }) => ({
    name: magic_string,
    // @ts-ignore
    type: fieldDict[type],
    value,
    label,
    required,
    choices,
  }));
