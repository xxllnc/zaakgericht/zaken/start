// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ViewType } from './Mor.types';

export const views: ViewType[] = ['open', 'resolved'];

export const prefix = '/mor';

export const QUERY_OPEN = 'mor-cases-open';
export const QUERY_NEW = 'mor-cases-new';
export const QUERY_RESOLVED = 'mor-cases-resolved';
export const QUERY_STATUS = {
  open: QUERY_OPEN,
  new: QUERY_NEW,
  resolved: QUERY_RESOLVED,
};
export const QUERY_CASE = 'mor-case';
