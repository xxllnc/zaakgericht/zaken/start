// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  CaseTypeResponseBodyType,
  CasesResponseBodyType,
  DocumentsResponseBodyType,
  FetchCaseTypeType,
  FetchCasesType,
  FetchDocumentsType,
  IntegrationResponseBody,
  TakeCaseType,
  TransitionCaseType,
  UpdateCaseType,
  fetchIntegrationType,
} from './Mor.types';
import { prefix } from './Mor.constants';

export const logout = () => {
  const logoutUrl = '/auth/logout';
  const loginUrl = buildUrl('/auth/login', { referer: prefix });

  fetch(logoutUrl).then(() => {
    window.location.href = loginUrl;
  });
};

export const fetchIntegration: fetchIntegrationType = async () => {
  const response = await request<IntegrationResponseBody>(
    'GET',
    '/api/v1/app/app_mor'
  );

  return response;
};

export const fetchCases: FetchCasesType = async zql => {
  const url = buildUrl('/api/v1/case', {
    page: 1,
    rows_per_page: 20,
    zql,
  });

  const response = await request<CasesResponseBodyType>('GET', url);

  return response;
};

export const fetchCaseType: FetchCaseTypeType = async (uuid, version) => {
  const url = buildUrl(`/api/v1/casetype/${uuid}`, { version });

  const response = await request<CaseTypeResponseBodyType>('GET', url);

  return response;
};

export const fetchDocuments: FetchDocumentsType = async number => {
  const url = `/api/case/${number}/directory/tree`;

  const response = await request<DocumentsResponseBodyType>('GET', url);

  return response;
};

export const takeCase: TakeCaseType = async uuid => {
  const url = `/api/v1/case/${uuid}/take`;

  await request('POST', url);
};

export const updateCase: UpdateCaseType = async (uuid, data) => {
  const url = `/api/v1/case/${uuid}/update`;

  await request('POST', url, data);
};

export const transitionCase: TransitionCaseType = async (uuid, data) => {
  const url = `/api/v1/case/${uuid}/transition`;

  await request('POST', url, data);
};
