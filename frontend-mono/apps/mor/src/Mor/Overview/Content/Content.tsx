// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useMemo } from 'react';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { Swiper } from '@mintlab/ui/App/Zaaksysteem/Swiper';
import { IntegrationType, FilterType } from '../../Mor.types';
import { getZqls } from '../Overview.library';
import { useUrl } from '../../Mor.library';
import { views } from '../../Mor.constants';
import { SHEET_WIDTH } from '../../Mor.styles';
import { queryClient } from '../../../App';
import { QUERY_NEW, QUERY_OPEN, QUERY_RESOLVED } from '../../Mor.constants';
import Table from './Tables/Table';

type ContentPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
};

const Content: React.ComponentType<ContentPropsType> = ({
  integration,
  filters,
}) => {
  const session = useSession();
  const { view } = useUrl();

  const zqls = useMemo(() => getZqls(integration, session), []);

  const onLoadLeft = () => {
    queryClient.invalidateQueries([QUERY_OPEN]);
    queryClient.invalidateQueries([QUERY_NEW]);
  };

  const onLoadRight = () => {
    queryClient.invalidateQueries([QUERY_RESOLVED]);
  };

  return (
    <Swiper
      view={view}
      views={views}
      minWidth={SHEET_WIDTH}
      onLoadLeft={onLoadLeft}
      onLoadRight={onLoadRight}
      ContentLeft={
        <>
          <Table
            integration={integration}
            filters={filters}
            type="open"
            zqls={zqls}
          />
          <Table
            integration={integration}
            filters={filters}
            type="new"
            zqls={zqls}
          />
        </>
      }
      ContentRight={
        <>
          <Table
            integration={integration}
            filters={filters}
            type="resolved"
            zqls={zqls}
          />
        </>
      }
    />
  );
};

export default Content;
