// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTableStyles = makeStyles(
  ({ palette: { common, elephant, grass, coral } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      padding: '20px 0 40px 0',
      '&>:first-child': {
        padding: '0 20px',
      },
    },
    table: {
      position: 'relative',
      '&>*:last-child': {
        borderBottom: 0,
      },
    },
    loader: {
      position: 'absolute',
      width: '100%',
    },
    row: {
      display: 'flex',
      flexDirection: 'row',
      padding: '20px 20px',
      gap: 10,
      alignItems: 'center',
      borderBottom: `1px solid ${elephant.light}`,
      '&:hover': {
        cursor: 'pointer',
      },
    },
    info: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
      overflow: 'hidden',
    },
    subject: {
      width: '100%',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    location: {
      display: 'flex',
      flexDirection: 'row',
      gap: 5,
      marginLeft: -4,
      alignItems: 'center',
      color: elephant.dark,
    },
    urgency: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: 24,
      height: 24,
      borderRadius: 4,
      color: common.white,
      fontSize: 12,
    },
    onTime: {
      backgroundColor: grass.main,
    },
    late: {
      backgroundColor: coral.main,
    },
    placeholder: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
      padding: 20,
      margin: '10px 0',
      alignItems: 'center',
      textAlign: 'center',
      width: '100%',
      backgroundColor: elephant.lightest,
      color: elephant.dark,
      borderTop: `3px solid ${elephant.lighter}`,
      borderBottom: `3px solid ${elephant.lighter}`,
    },
  })
);
