// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { IntegrationType } from '../../Mor.types';
import { useFooterStyles } from './Footer.styles';

type FooterPropsType = {
  integration: IntegrationType;
};

const Footer: React.ComponentType<FooterPropsType> = ({ integration }) => {
  const [t] = useTranslation('mor');
  const classes = useFooterStyles();

  return integration.helpUrl ? (
    <div className={classes.wrapper}>
      <span>{t('footer.description')}</span>
      <a
        target="_blank"
        href={integration.helpUrl}
        rel="noreferrer"
        style={{
          color: integration.colorAccent,
          whiteSpace: 'nowrap',
        }}
      >
        {t('footer.helpUrl')}
      </a>
    </div>
  ) : null;
};

export default Footer;
