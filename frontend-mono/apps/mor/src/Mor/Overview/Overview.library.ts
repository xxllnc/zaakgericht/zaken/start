// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FilterCasesType, GetZqlsType } from './Overview.types';

export const getZqls: GetZqlsType = (integration, session) => {
  const {
    logged_in_user: { id },
  } = session;
  const { caseTypeSettings } = integration;
  const ids = caseTypeSettings.map(({ id }) => `"${id}"`).join(',');

  const from = 'SELECT {} FROM';

  const assignee = `(case.assignee.id = ${id})`;
  const open = `(case.status = "open")`;
  const resolved = `(case.status = "resolved")`;
  const caseType = `(case.casetype.id IN (${ids}))`;

  const ORDER = 'NUMERIC ORDER BY case.number';

  return {
    open: `${from} case WHERE ${caseType} AND ${assignee} AND ${open} ${ORDER} ASC`,
    new: `${from} case intake WHERE ${caseType} ${ORDER} ASC`,
    resolved: `${from} case WHERE ${caseType} AND ${resolved} ${ORDER} DESC`,
  };
};

export const filterCases: FilterCasesType = (cases, filters) => {
  return cases.filter(({ filterText }) =>
    filters.every(({ value }) => filterText.includes(value.toLowerCase()))
  );
};
