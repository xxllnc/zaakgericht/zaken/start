// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { IntegrationType, FilterType } from '../Mor.types';
import Content from './Content/Content';
import Footer from './Footer/Footer';
import Header from './Header/Header';
import { useOverviewStyles } from './Overview.styles';

type OverViewPropsType = {
  integration: IntegrationType;
};

const OverView: React.ComponentType<OverViewPropsType> = ({ integration }) => {
  const classes = useOverviewStyles();
  const [filters, setFilters] = useState<FilterType[]>([]);

  return (
    <div className={classes.wrapper}>
      <Header
        integration={integration}
        filters={filters}
        setFilters={setFilters}
      />
      <Content integration={integration} filters={filters} />
      <Footer integration={integration} />
    </div>
  );
};

export default OverView;
