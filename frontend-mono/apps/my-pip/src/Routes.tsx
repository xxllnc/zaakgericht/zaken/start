// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Suspense } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { useAppStyle } from './App.style';

const CommunicationModule = React.lazy(
  () => import(/* webpackChunkName: "case" */ './modules/communication')
);

const PipCaseDocuments = React.lazy(
  () =>
    import(
      /* webpackChunkName: "PipCaseDocuments" */ './components/PipCaseDocuments/PipCaseDocuments'
    )
);

export interface RoutesPropsType {
  prefix: string;
}

const PipRoutes: React.ComponentType<RoutesPropsType> = ({ prefix }) => {
  const classes = useAppStyle();

  return (
    <Suspense fallback={<Loader delay={200} />}>
      <div className={classes.app}>
        <ErrorBoundary>
          <Router>
            <Routes>
              <Route
                path={`${prefix}/communication/:userUuid/*`}
                element={<CommunicationModule />}
              />
              <Route
                path={`${prefix}/documents/:caseUuid`}
                element={
                  <PipCaseDocuments
                    capabilities={{
                      canDownload: true,
                      canSearch: true,
                    }}
                  />
                }
              />
            </Routes>
          </Router>
        </ErrorBoundary>
      </div>
    </Suspense>
  );
};

export default PipRoutes;
