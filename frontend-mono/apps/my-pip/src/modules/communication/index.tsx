// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import CommunicationModule from '@zaaksysteem/communication-module';

type CommunicationParamsType = {
  userUuid: string;
};

const Communication: React.ComponentType = () => {
  const { userUuid } = useParams<
    keyof CommunicationParamsType
  >() as CommunicationParamsType;
  const { caseUuid } = objectifyParams(location.search) as any;

  return (
    <CommunicationModule
      contactUuid={userUuid}
      caseUuid={caseUuid}
      context="pip"
      capabilities={{
        allowSplitScreen: false,
        canAddAttachmentToCase: false,
        canAddSourceFileToCase: false,
        canAddThreadToCase: false,
        canCreateContactMoment: false,
        canCreatePipMessage: true,
        canCreateEmail: false,
        canCreatePostex: false,
        canMarkUnread: false,
        canCreateNote: false,
        canDeleteMessage: false,
        canImportMessage: false,
        canSelectCase: true,
        canSelectContact: false,
        canFilter: false,
        canOpenPDFPreview: false,
      }}
    />
  );
};

export default Communication;
