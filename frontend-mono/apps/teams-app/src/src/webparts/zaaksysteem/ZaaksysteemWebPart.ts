// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import axios from 'axios';
import * as ReactDom from 'react-dom';
import * as strings from 'ZaaksysteemWebPartStrings';
import { Version } from '@microsoft/sp-core-library';
import {
  type IPropertyPaneConfiguration,
  PropertyPaneTextField,
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { IReadonlyTheme } from '@microsoft/sp-component-base';
import { SPFI } from '@pnp/sp/presets/all';
import Zaaksysteem from './components/Zaaksysteem';
import { IZaaksysteemProps } from './components/IZaaksysteemProps';

import { getSP, getGraph } from './pnpjsConfig';

export interface IZaaksysteemWebPartProps {
  description: string;
  zaaksysteemUrl: string;
  zaaksysteemId: string;
  relativeUrl: string;
  spConfig: SPFI;
  domainUrl: string;
}

export default class ZaaksysteemWebPart extends BaseClientSideWebPart<IZaaksysteemWebPartProps> {
  private _isDarkTheme: boolean = false;
  private _environmentMessage: string = '';
  private _teamId: string | undefined;
  private _channelId: string | undefined;
  private _relativeUrl: string | undefined;
  private _domainURL: string | undefined;

  public render(): void {
    //@ts-ignore
    const element: React.ReactElement<IZaaksysteemProps> = React.createElement(
      Zaaksysteem,
      {
        description: this.properties.description,
        zaaksysteemUrl: this.properties.zaaksysteemUrl,
        zaaksysteemId: this.properties.zaaksysteemId,
        //@ts-ignore
        isDarkTheme: this._isDarkTheme,
        environmentMessage: this._environmentMessage,
        teamId: this._teamId,
        channelId: this._channelId,
        hasTeamsContext: !!this.context.sdks.microsoftTeams,
        userDisplayName: this.context.pageContext.user.displayName,
        relativeUrl: this._relativeUrl,
        domainUrl: this._domainURL,
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected async onInit(): Promise<void> {
    this._environmentMessage = await this._getEnvironmentMessage();

    await super.onInit();

    getSP(this.context);
    getGraph(this.context);

    const tokenProvider =
      await this.context.aadTokenProviderFactory.getTokenProvider();
    const token = await tokenProvider.getToken('api://zaaksysteem-teams-api');
    axios.defaults.headers['Authorization'] = 'Bearer ' + token;

    if (this.context.sdks.microsoftTeams) {
      // checking that we're in Teams
      const context = this.context.sdks.microsoftTeams.teamsJs.app.getContext();
      this._applyTheme((await context).app.theme || 'default');
      this.context.sdks.microsoftTeams.teamsJs.app.registerOnThemeChangeHandler(
        this._applyTheme
      );
    }
  }

  private _applyTheme = (theme: string): void => {
    this.context.domElement.setAttribute('data-theme', theme);
    document.body.setAttribute('data-theme', theme);
  };

  private _getEnvironmentMessage(): Promise<string> {
    if (this.context.sdks.microsoftTeams) {
      // running in Teams, office.com or Outlook
      return (
        this.context.sdks.microsoftTeams.teamsJs.app
          .getContext()
          /* eslint complexity: [2, 8] */
          .then(context => {
            let environmentMessage: string = '';
            this._teamId = context.team?.groupId;
            this._channelId = context.channel?.id;
            this._relativeUrl = context.channel?.relativeUrl;
            this._domainURL = context.sharePointSite?.teamSiteDomain;
            switch (context.app.host.name) {
              case 'Office': // running in Office
                environmentMessage = this.context.isServedFromLocalhost
                  ? strings.AppLocalEnvironmentOffice
                  : strings.AppOfficeEnvironment;
                break;
              case 'Outlook': // running in Outlook
                environmentMessage = this.context.isServedFromLocalhost
                  ? strings.AppLocalEnvironmentOutlook
                  : strings.AppOutlookEnvironment;
                break;
              case 'Teams': // running in Teams
                break;
              case 'TeamsModern':
                environmentMessage = this.context.isServedFromLocalhost
                  ? strings.AppLocalEnvironmentTeams
                  : strings.AppTeamsTabEnvironment;
                break;
              default:
                environmentMessage = strings.UnknownEnvironment;
            }

            return environmentMessage;
          })
      );
    }

    return Promise.resolve(
      this.context.isServedFromLocalhost
        ? strings.AppLocalEnvironmentSharePoint
        : strings.AppSharePointEnvironment
    );
  }

  protected onThemeChanged(currentTheme: IReadonlyTheme | undefined): void {
    if (!currentTheme) {
      return;
    }

    this._isDarkTheme = !!currentTheme.isInverted;
    const { semanticColors } = currentTheme;

    if (semanticColors) {
      this.domElement.style.setProperty(
        '--bodyText',
        semanticColors.bodyText || null
      );
      this.domElement.style.setProperty('--link', semanticColors.link || null);
      this.domElement.style.setProperty(
        '--linkHovered',
        semanticColors.linkHovered || null
      );
    }
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  //@ts-ignore
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description:
              'App om bestanden te kopieren van een zaak naar een Microsoft Teams kanaal.',
          },
          groups: [
            {
              groupName: 'Zaaksysteem configuratie',
              groupFields: [
                PropertyPaneTextField('zaaksysteemUrl', {
                  label: 'Zaaksysteem URL',
                  placeholder: 'https://zaaksysteem.nl',
                }),
                PropertyPaneTextField('zaaksysteemId', {
                  label: 'Zaaknummer',
                }),
              ],
            },
          ],
        },
      ],
    };
  }
}
