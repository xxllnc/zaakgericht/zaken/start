// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
declare interface IZaaksysteemWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AppLocalEnvironmentSharePoint: string;
  AppLocalEnvironmentTeams: string;
  AppLocalEnvironmentOffice: string;
  AppLocalEnvironmentOutlook: string;
  AppSharePointEnvironment: string;
  AppTeamsTabEnvironment: string;
  AppOfficeEnvironment: string;
  AppOutlookEnvironment: string;
  UnknownEnvironment: string;
}

declare module 'ZaaksysteemWebPartStrings' {
  const strings: IZaaksysteemWebPartStrings;
  export = strings;
}
