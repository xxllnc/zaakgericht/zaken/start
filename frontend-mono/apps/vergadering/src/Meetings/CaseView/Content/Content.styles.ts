// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const gap = 30;
const attributeGap = 20;
export const createButtonTop = 10 - gap;

export const useContentStyles = makeStyles(
  ({
    palette: { common, elephant, sahara },
    mintlab: { shadows },
    typography,
  }: Theme) => ({
    wrapper: {
      width: '100%',
      height: 0,
      display: 'flex',
      flexGrow: 1,
    },
    content: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      gap,
      padding: 20,
      overflowY: 'auto',
    },
    sideBar: {
      width: 400,
      minWidth: 400,
      padding: 20,
      borderLeft: `1px solid ${elephant.light}`,
      overflowY: 'auto',
    },
    sideBarCompact: {},
    votingAttributesWrapper: {
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      alignItems: 'flex-start',
      padding: 20,
      backgroundColor: elephant.lighter,
      borderRadius: 8,
      '&>div>div>div': {
        fontSize: '1rem',
        fontWeight: typography.fontWeightBold,
        color: common.black,
      },
    },
    votingAttributes: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      gap: attributeGap,
    },
    attributes: {
      display: 'flex',
      flexDirection: 'column',
      gap: attributeGap,
      position: 'relative',
      '&>:first-child': {
        height: 48,
        display: 'flex',
        alignItems: 'flex-end',
      },
    },
    attribute: {
      display: 'flex',
      flexDirection: 'column',
      gap: 5,
    },
    attributeLabel: {
      fontSize: '0.8rem',
      color: elephant.main,
    },
    valueList: {
      margin: 0,
      paddingLeft: 20,
    },
    notes: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
    },
    note: {},
    noteLabel: {
      flexGrow: 1,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      textAlign: 'left',
      fontWeight: typography.fontWeightRegular,
    },
    noteCreateWrapper: {
      position: 'absolute',
      top: 0,
      right: 0,
    },
    noteCreate: {
      '&&': {
        padding: 10,
        borderRadius: 8,
        backgroundColor: sahara.lightest,
        boxShadow: shadows.sharp,
        '&:hover': {
          backgroundColor: sahara.lighter,
        },
        color: common.black,
      },
    },
    resolvedWarning: {
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      padding: 20,
      backgroundColor: elephant.light,
      borderRadius: 8,
      alignItems: 'center',
      boxShadow: shadows.sharp,
    },
    documents: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    documentList: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
    },
    document: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      gap: 5,
      paddingRight: 5,
      borderRadius: 8,
      backgroundColor: elephant.lighter,
      boxShadow: shadows.sharp,
      '&:hover': {
        backgroundColor: elephant.light,
      },
      cursor: 'pointer',
    },
    documentLabel: {
      width: '100%',
      textAlign: 'left',
      wordWrap: 'break-word',
      fontWeight: typography.fontWeightMedium,
      fontSize: '1rem',
    },
  })
);
