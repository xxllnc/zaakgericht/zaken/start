// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';
import { CaseObjType } from '../../types';
import { useContentStyles } from './Content.styles';
import Document from './Document';

type SideBarPropsType = {
  caseObj: CaseObjType;
  isCompact?: boolean;
};

const SideBarContent: React.ComponentType<SideBarPropsType> = ({ caseObj }) => {
  const classes = useContentStyles();
  const [t] = useTranslation('meetings');

  const { documents } = caseObj;

  return (
    <div className={classes.documents}>
      <Typography variant="h3">{t('case.documents.title')}</Typography>
      {documents.length ? (
        <div className={classes.documentList}>
          {documents.map(document => (
            <Document
              key={document.uuid}
              caseObj={caseObj}
              document={document}
            />
          ))}
        </div>
      ) : (
        <div>{t('case.documents.placeholder')}</div>
      )}
    </div>
  );
};

const SideBar: React.ComponentType<SideBarPropsType> = props => {
  const classes = useContentStyles();

  return (
    <div className={props.isCompact ? classes.sideBarCompact : classes.sideBar}>
      <SideBarContent {...props} />
    </div>
  );
};

export default SideBar;
