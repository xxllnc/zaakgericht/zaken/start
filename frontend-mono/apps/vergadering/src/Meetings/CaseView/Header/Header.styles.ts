// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useHeaderStyles = makeStyles(
  ({ mintlab: { shadows } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      alignItems: 'center',
      padding: 10,
      boxShadow: shadows.sharp,
    },
    title: {
      flexGrow: 1,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
  })
);
