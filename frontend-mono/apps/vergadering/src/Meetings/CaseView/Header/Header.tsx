// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { IntegrationType } from '../../types';
import { QUERY_CASE } from '../../constants';
import { queryClient } from '../../../App';
import { useHeaderStyles } from './Header.styles';

type HeaderPropsType = {
  integration: IntegrationType;
  title: string;
};

const Header: React.ComponentType<HeaderPropsType> = ({
  integration,
  title,
}) => {
  const classes = useHeaderStyles();
  const navigate = useNavigate();
  const {
    // @ts-ignore
    palette: { common, primary },
  } = useTheme();
  const sx = { color: common.white };

  const refresh = () => {
    queryClient.invalidateQueries([QUERY_CASE]);
  };

  return (
    <div
      style={{ backgroundColor: integration.colorHeader || primary.main }}
      className={classes.wrapper}
    >
      <IconButton onClick={() => navigate('..')} sx={sx}>
        <Icon size="small">{iconNames.arrow_back}</Icon>
      </IconButton>
      <Typography variant="h3" classes={{ root: classes.title }} sx={sx}>
        {title}
      </Typography>
      <IconButton onClick={refresh} sx={sx}>
        <Icon size="small">{iconNames.refresh}</Icon>
      </IconButton>
    </div>
  );
};

export default Header;
