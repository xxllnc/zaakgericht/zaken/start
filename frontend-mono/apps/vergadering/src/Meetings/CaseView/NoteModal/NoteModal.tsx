// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { useTheme } from '@mui/material';
import TextField from '@mintlab/ui/App/Material/TextField';
import useConfirmDialog from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import { useTranslation } from 'react-i18next';
import { queryClient } from '../../../App';
import { CaseObjType, NoteType } from '../../types';
import { createNote, deleteNote, editNote, getNotes } from '../../requests';
import { QUERY_CASE } from '../../constants';
import { useNodeModalStyles } from './NoteModal.styles';
import Header from './Header';

type NoteModalPropsType = {
  caseObj: CaseObjType;
  title: string;
  canEdit: boolean;
  noteToEdit: NoteType;
  setNoteToEdit: (note: NoteType | null) => void;
};

const NoteModal: React.ComponentType<NoteModalPropsType> = ({
  caseObj,
  title,
  canEdit,
  noteToEdit,
  setNoteToEdit,
}) => {
  const [t] = useTranslation();
  const classes = useNodeModalStyles();
  const {
    palette: { sahara },
  } = useTheme<Theme>();

  const [creating, setCreating] = useState<boolean>(false);
  const [note, setNote] = useState<NoteType>(noteToEdit);
  useEffect(() => {
    setNote(noteToEdit);
    setCreating(!noteToEdit?.uuid);
  }, [noteToEdit]);

  const close = () => setNoteToEdit(null);

  const listener = (event: any) => {
    const isCloseKey = ['Escape'].includes(event.code);

    if (isCloseKey) close();
  };

  useEffect(() => {
    document.addEventListener('keydown', listener);

    return () => {
      document.removeEventListener('keydown', listener);
    };
  }, []);

  const updateCase = (notes: NoteType[]) => {
    queryClient.setQueriesData([QUERY_CASE], { ...caseObj, notes });
  };

  const createAction = () => {
    createNote(caseObj.uuid, note?.content as string).then(() => {
      close();
      getNotes(caseObj).then(notes => updateCase(notes));
    });
  };

  const editAction = () => {
    editNote(caseObj.uuid, note as NoteType).then(() => {
      close();
      updateCase(caseObj.notes.map(ni => (ni.uuid === note?.uuid ? note : ni)));
    });
  };

  const [confirmDeleteDialog, openConfirmDelete] = useConfirmDialog(
    t('case.notes.deleteTitle'),
    t('case.notes.deleteBody'),
    () => {
      close();
      deleteNote(caseObj.uuid, note?.uuid as string).then(() => {
        updateCase(caseObj.notes.filter(ni => ni.uuid !== note?.uuid));
      });
    }
  );

  return (
    <div
      className={classNames(
        classes.wrapper,
        noteToEdit === null ? classes.closed : classes.open
      )}
    >
      {confirmDeleteDialog}
      <Header
        title={title}
        close={close}
        canEdit={canEdit}
        creating={creating}
        createAction={createAction}
        editAction={editAction}
        deleteAction={openConfirmDelete}
      />
      <TextField
        isMultiline={true}
        value={note?.content}
        placeholder={t('case.notes.placeholder')}
        onChange={event => {
          setNote({
            uuid: note?.uuid || '',
            content: event.target.value,
          });
        }}
        sx={{
          '& .MuiInputBase-root': {
            height: '100%',
            width: '100%',
            display: 'flex',
            alignItems: 'flex-start',
            padding: 2,
            backgroundColor: sahara.lightest,
            overflowY: 'auto',
            '&>*div': {
              overflowY: 'auto',
            },
            '&:hover': {
              backgroundColor: sahara.lightest,
            },
            '&:focus': {
              borderBottom: 'unset',
              border: 'unset',
            },
            '&:after': {
              border: 'none',
            },
          },
        }}
      />
    </div>
  );
};

export default NoteModal;
