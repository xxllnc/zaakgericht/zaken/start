// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Button from '@mintlab/ui/App/Material/Button';
import ButtonBar from '@mintlab/ui/App/Zaaksysteem/ButtonBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { IntegrationType } from '../types';
import { logout } from '../library';
import { useIntegrationsStyles } from './Integrations.styles';

type IntegrationsPropsType = {
  integrations: IntegrationType[];
};

const Integrations: React.ComponentType<IntegrationsPropsType> = ({
  integrations,
}) => {
  const [t] = useTranslation('meetings');
  const classes = useIntegrationsStyles();
  const navigate = useNavigate();

  const title = t('title');

  document.title = title;

  const permanentActions = [
    {
      icon: iconNames.power_settings_new,
      action: () => logout(),
      label: t('buttonBar.logout'),
      name: 'buttonBarInfo',
    },
  ];

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <Typography variant="h3">{title}</Typography>
        <ButtonBar permanentActions={permanentActions} negative />
      </div>
      <div className={classes.integrations}>
        {integrations.map(({ shortName, title }) => (
          <Button
            key={shortName}
            name={title}
            action={() => navigate(shortName)}
            variant="outlined"
          >
            {title}
          </Button>
        ))}
      </div>
    </div>
  );
};

export default Integrations;
