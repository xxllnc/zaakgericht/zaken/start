// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { TableCell } from '@mui/material';
import { useProposalListStyles } from './ProposalList.styles';

type HeaderCellPropsType = {
  toggleSort: (magicString: string) => void;
  magicString: string;
  label: string | React.ReactElement;
  sortedColumn: string;
  ascending: boolean;
};

const HeaderCell: React.ComponentType<HeaderCellPropsType> = ({
  toggleSort,
  magicString,
  label,
  sortedColumn,
  ascending,
}) => {
  const classes = useProposalListStyles();

  return (
    <TableCell
      className={classes.headerCellWrapper}
      onClick={() => toggleSort(magicString)}
    >
      <div className={classes.headerCell}>
        <div>{label}</div>
        {sortedColumn === magicString && (
          <Icon size="extraSmall">
            {ascending ? iconNames.arrow_drop_down : iconNames.arrow_drop_up}
          </Icon>
        )}
      </div>
    </TableCell>
  );
};

export default HeaderCell;
