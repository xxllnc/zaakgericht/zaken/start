// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useProposalListStyles = makeStyles(
  ({ palette: { elephant }, typography }: Theme) => ({
    placeholder: {
      display: 'flex',
      justifyContent: 'center',
      textAlign: 'center',
      width: '100%',
      padding: 40,
    },
    row: {
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: elephant.lightest,
      },
    },
    header: {
      backgroundColor: elephant.lightest,
      borderBottom: `1px solid ${elephant.light}`,
      padding: 20,
      fontWeight: typography.fontWeightMedium,
      fontSize: 16,
    },
    headerCellWrapper: {
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: elephant.lightest,
      },
    },
    headerCell: {
      display: 'flex',
      flexDirection: 'row',
      gap: 5,
    },
  })
);
