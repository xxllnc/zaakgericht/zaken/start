// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const headerHeight = 140;

export const useHeaderStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    minHeight: headerHeight,
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '10px 10px 0 10px',
    gap: 5,
    height: 50,
    '&>:first-child': {
      flexGrow: 1,
    },
  },
  image: {
    width: '100%',
    position: 'absolute',
    height: headerHeight,
    backgroundSize: 'cover',
    backgroundPosition: '50%',
    opacity: 0.25,
  },
  actions: {
    display: 'flex',
    flexDirection: 'row',
  },
  navigation: {
    display: 'flex',
  },
}));
