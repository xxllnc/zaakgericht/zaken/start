// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ViewType, GroupingType } from './types';

export const views: ViewType[] = ['open', 'resolved'];
export const groupings: GroupingType[] = ['grouped', 'ungrouped'];

export const prefix = '/vergadering';

export const QUERY_MEETINGS_OPEN = 'meetings-open';
export const QUERY_MEETINGS_RESOLVED = 'meetings-resolved';
export const QUERY_PROPOSALS = 'proposals';
export const QUERY_UNGROUPED_PROPOSALS = 'ungrouped-proposals';

export const QUERY_CASE = 'meetings-case';

export const zIndex = {
  caseView: 1,
  note: 2,
  noteHeader: 3,
};
