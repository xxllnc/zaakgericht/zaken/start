// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTranslation } from 'react-i18next';
import { getIntegrations } from './requests';
import { IntegrationType } from './types';
import Integrations from './Integrations/Integrations';
import Overview from './Overview/Overview';
import { useMeetingsModuleStyles } from './styles';
import { useUrl } from './library';
import { views } from './constants';
import CaseView from './CaseView/CaseView';

type MeetingModulePropsType = {
  integrations: IntegrationType[];
};

const MeetingModule: React.ComponentType<MeetingModulePropsType> = ({
  integrations,
}) => {
  const [t] = useTranslation('meetings');

  const { shortName } = useUrl();
  const integration = integrations.find(int => int.shortName === shortName);

  document.title = integration?.title || t('title');

  return integration ? (
    <Routes>
      <Route path="" element={<Navigate to={views[0]} replace={true} />} />
      <Route path=":view">
        <Route
          path=""
          element={
            <>
              <Overview integration={integration} />
              <CaseView integration={integration} />
            </>
          }
        />
        <Route
          path=":caseNumber"
          element={
            <>
              <Overview integration={integration} />
              <CaseView integration={integration} />
            </>
          }
        />
      </Route>
    </Routes>
  ) : (
    <Navigate to=".." />
  );
};

const MeetingsModule = () => {
  const classes = useMeetingsModuleStyles();

  const { data: integrations, error } = useQuery<
    IntegrationType[],
    V2ServerErrorsType
  >(['integrations'], () => getIntegrations());

  return (
    <Sheet classes={{ sheet: classes.sheet }}>
      {!integrations ? (
        <Loader />
      ) : (
        <Routes>
          <Route
            path=""
            element={<Integrations integrations={integrations} />}
          />
          <Route
            path=":shortName/*"
            element={<MeetingModule integrations={integrations} />}
          />
        </Routes>
      )}
      {error && <ServerErrorDialog />}
    </Sheet>
  );
};

export default MeetingsModule;
