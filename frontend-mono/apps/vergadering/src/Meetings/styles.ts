// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const SHEET_WIDTH = 1600;

//@ts-ignore
export const useMeetingsModuleStyles = makeStyles(() => ({
  sheet: {
    border: 'unset',
    maxWidth: SHEET_WIDTH,
    position: 'relative',
  },
}));
