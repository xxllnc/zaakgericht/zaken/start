// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fetch from 'node-fetch';
import { increment } from './logging.js';
import { readFileSync } from 'fs';

const cspSettings = new Map();
const bucketHost = process.env.BUCKET_HOST;

const addBucket = xs => [...xs, bucketHost].filter(Boolean).join(' ');

let str;

try {
  str = readFileSync('./csp.json');
} catch (err) {
  console.error(err);
  str = `{
  "whitelistMain": [
    "https://zaaksysteem.nl/",
    "https://*.bus.koppel.app",
    "https://www.gstatic.com/recaptcha/",
    "https://www.google.com/recaptcha/",
    "https://service.pdok.nl"
  ],
  "whitelistStyles": [
    "https://fonts.googleapis.com/css"
  ],
  "whitelistImages": [
    "https://service.pdok.nl",
    "https://geodata.nationaalgeoregister.nl",
    "https://c1-powerpoint-15.cdn.office.net",
    "https://c1-excel-15.cdn.office.net",
    "https://c1-word-view-15.cdn.office.net"
  ],
  "whitelistFrames": [
    "https://*.bus.koppel.app",
    "https://www.google.com/recaptcha/",
    "https://login.live.com/",
    "https://*.officeapps.live.com/"
  ],
  "whitelistConnect": [
    "https://service.pdok.nl",
    "https://geodata.nationaalgeoregister.nl",
    "https://api.pdok.nl"
  ]
}
`;
}

const cspWhitelist = JSON.parse(str);

const runCspRequests = () => {
  setTimeout(() => {
    cspSettings.forEach((setting, key) => {
      setTimeout(
        () => {
          increment(key);
          buildCspFetch(key)
            .then(rs => rs.json())
            .then(res => {
              const whitelistDomains = res.csp_hosts
                .filter(hostname => hostname !== '*')
                .map(hostname => 'https://' + hostname)
                .join(' ');

              cspSettings.set(key, {
                whitelistDomains,
                iframeableInTeams: Boolean(res.iframeable_in_teams),
              });
            })
            .catch(console.error);
        },
        Math.random() * 30 * 1000
      );
    });
    runCspRequests();
  }, 60 * 1000);
};

runCspRequests();

const buildCspFetch = reqHost => {
  if (process.env.API_HOSTNAME) {
    return fetch(`http://${process.env.API_HOSTNAME}/csp`, {
      headers: { Host: reqHost },
    });
  }

  return fetch(`https://${reqHost}/csp`);
};

const addBucketHost = xs => [...xs, bucketHost].filter(Boolean).join(' ');

export default (nonce, unsafe, reqHost) => {
  const currentSettings = cspSettings.get(reqHost);
  const { whitelistDomains, iframeableInTeams } = currentSettings || {
    whitelistDomains: '',
    iframeableInTeams: false,
  };

  if (!currentSettings) {
    cspSettings.set(reqHost, null);
  }

  return [
    `default-src 'self' ${addBucketHost(
      cspWhitelist.whitelistMain
    )} ${whitelistDomains}`,
    `style-src 'self' ${unsafe ? "'unsafe-inline'" : `'nonce-${nonce}'`} ${cspWhitelist.whitelistStyles.join(' ')} ${whitelistDomains}`,
    `frame-src 'self' ${addBucketHost(
      cspWhitelist.whitelistFrames
    )} ${whitelistDomains}`,
    `img-src 'self' blob: data: ${addBucketHost(
      cspWhitelist.whitelistImages
    )} ${whitelistDomains}`,
    `object-src 'none'`,
    `frame-ancestors 'self' ${iframeableInTeams === true ? 'https://teams.microsoft.com' : ''}`,
    `require-trusted-types-for 'script'`,
    `connect-src 'self' data: ${addBucket(
      cspWhitelist.whitelistConnect
    )} ${whitelistDomains}`,
    `base-uri 'self'`,
  ].join('; ');
};
