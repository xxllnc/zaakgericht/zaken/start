Contact: mailto:security@xxllnc.nl
Expires: 2025-12-12T11:38:00.000Z
Preferred-Languages: nl,en
Canonical: https://xxllnc.nl/.well-known/security.txt
Policy: https://xxllnc.nl/policies#Responsible_disclosure
