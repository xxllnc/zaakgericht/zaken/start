// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Alert from '../dialogs/Alert/Alert';

export interface ConfirmDialogPropsType {
  title: string;
  body: string | React.ReactElement;
  onConfirm: () => void;
  onClose?: () => void;
  open?: boolean;
  container?: React.ReactInstance | null;
}

const ConfirmDialog: React.FunctionComponent<ConfirmDialogPropsType> = ({
  onConfirm,
  onClose,
  title,
  body,
  open = true,
  container,
}) => {
  const [t] = useTranslation('common');

  return (
    <Alert
      primaryButton={{ text: t('dialog.ok'), action: onConfirm }}
      secondaryButton={{ text: t('dialog.cancel'), action: onClose }}
      onClose={onClose}
      title={title}
      open={open}
      container={container}
    >
      {body}
    </Alert>
  );
};

export default ConfirmDialog;
