// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDocumentExplorerStyles = makeStyles(
  ({
    typography,
    breakpoints,
    palette: { elephant, coral },
    mintlab: { greyscale },
  }: Theme) => {
    const section = {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      paddingTop: 10,
      paddingBottom: 10,
    };

    const defaultFont = {
      fontWeight: typography.fontWeightRegular,
      fontSize: 13,
    };

    return {
      sheet: {
        maxWidth: 2000,
      },
      wrapper: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      },
      topbar: {
        ...section,
        display: 'flex',
        justifyContent: 'space-between',
        padding: 10,
        gap: 10,

        [breakpoints.down('xs')]: {
          flexDirection: 'column',
        },
      },
      table: {
        flex: 1,
        color: elephant.dark,
        ...defaultFont,
      },
      toolbar: {
        ...section,
        display: 'flex',
        minHeight: 80,
        paddingLeft: 20,
        justifyContent: 'flex-end',
        padding: 10,
      },
      actionButtons: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'flex-end',
      },
      iconWrapper: {
        marginRight: 10,
      },
      nameCell: {
        display: 'flex',
        alignItems: 'center',
        '&>:first-child': {
          minWidth: 30,
          width: 30,
        },
        '&>:nth-child(2)': {
          flexGrow: 1,
        },
        '&>:nth-child(3)': {
          minWidth: 75,
          width: 75,
        },
      },
      statusIndicator: {
        color: coral.dark,
        marginLeft: 10,
        fontSize: 11,
        fontWeight: typography.fontWeightMedium,
        flexShrink: 0,
        flexGrow: 0,
      },
      extension: {
        textTransform: 'capitalize',
      },
      name: {
        fontWeight: typography.fontWeightMedium,
        color: elephant.darkest,
      },
      extensionName: {
        fontWeight: typography.fontWeightLight,
      },
      breadCrumbs: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
      },
    };
  }
);
