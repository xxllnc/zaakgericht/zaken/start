// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useRejectedStyles = makeStyles(
  ({ palette: { danger } }: Theme) => {
    return {
      wrapper: {
        color: danger.main,
      },
    };
  }
);
