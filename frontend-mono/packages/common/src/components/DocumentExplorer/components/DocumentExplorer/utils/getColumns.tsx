// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import fecha from 'fecha';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { DocumentItemType } from '../../FileExplorer/types/FileExplorerTypes';
import { ClassesType } from '../types/types';
import Rejected from '../library/Rejected/Rejected';
import { getInfo } from '../../FileExplorer/library/getInfo';

type ColumnNames = 'mimeType' | 'modified' | 'description';
type NameColumnType = {
  name: ({
    fileNameAction,
    tooltipSuffix,
    showRejected,
    showNotAccepted,
  }: {
    fileNameAction?: (
      item: DocumentItemType
    ) => ((event: React.MouseEvent) => void) | undefined;
    iconRenderer?: (
      item: DocumentItemType,
      icon: React.ReactElement<any>
    ) => any;
    tooltipSuffix?: (item: DocumentItemType) => string | undefined;
    showRejected?: boolean;
    showNotAccepted?: boolean;
  }) => ColumnType;
};
type GetColumnsReturnType = NameColumnType &
  Record<ColumnNames, () => ColumnType>;

export const getColumns = ({
  t,
  classes,
}: {
  t: i18next.TFunction;
  classes: ClassesType;
}): GetColumnsReturnType => ({
  name: ({
    fileNameAction,
    iconRenderer,
    tooltipSuffix,
    showRejected = false,
    showNotAccepted = false,
  }) => {
    return {
      label: t('DocumentExplorer:columns.name.label'),
      name: 'name',
      width: 1,
      flexGrow: 1,
      flexShrink: 0,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }) => {
        const accepted = rowData.type === 'document' && rowData.accepted;
        const rejectedReason =
          rowData.type === 'document' && rowData.rejectionReason;
        const suffix = tooltipSuffix && tooltipSuffix(rowData);
        const tooltipText = suffix
          ? [rowData.name, suffix].join('')
          : rowData.name;
        const action = fileNameAction && fileNameAction(rowData);
        const icon = (
          <ZsIcon
            size="small"
            classes={{
              wrapper: classes.iconWrapper,
            }}
          >
            {getInfo(rowData).icon}
          </ZsIcon>
        );
        const parsedIcon = iconRenderer ? iconRenderer(rowData, icon) : icon;

        const getName = () => (
          <span>
            <span className={classes.name}>
              {(rowData.name || '').replace(rowData.extension, '')}
            </span>
            <span className={classes.extensionName}>{rowData.extension}</span>
          </span>
        );

        return (
          <div className={classes.nameCell}>
            {parsedIcon}
            <Tooltip
              title={tooltipText}
              placement={'top-start'}
              enterDelay={700}
            >
              {action ? (
                <Button
                  variant="text"
                  color="primary"
                  name="actionName"
                  component="a"
                  action={action}
                  sx={{
                    color: 'black',
                    margin: 0,
                    padding: 0,
                    width: '100%',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: 'flex',
                    justifyContent: 'start',
                    '&:hover': {
                      background: 'none',
                      textDecoration: 'underline',
                    },
                  }}
                >
                  {getName()}
                </Button>
              ) : (
                <Fragment>{getName()}</Fragment>
              )}
            </Tooltip>
            {rowData.type === 'document' && showNotAccepted && !accepted && (
              <span className={classes.statusIndicator}>
                {t('DocumentExplorer:columns.name.notAccepted') as string}
              </span>
            )}
            {rowData.type === 'document' && showRejected && rejectedReason && (
              <Rejected t={t} rowData={rowData} />
            )}
          </div>
        );
      },
    };
  },
  mimeType: () => ({
    label: t('DocumentExplorer:columns.type.label'),
    name: 'mimeType',
    width: 66,
    showFromWidth: 600,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }) => (
      <div className={classNames(classes.extension)}>
        {(rowData.extension || '').replace('.', '')}
      </div>
    ),
  }),
  modified: () => ({
    label: t('DocumentExplorer:columns.modified.label'),
    name: 'modified',
    width: 118,
    showFromWidth: 650,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }) => (
      <div>
        {rowData.modified
          ? fecha.format(new Date(rowData.modified), 'DD-M-YYYY HH:mm')
          : ''}
      </div>
    ),
  }),
  description: () => ({
    label: t('DocumentExplorer:columns.description.label'),
    name: 'description',
    width: 250,
    showFromWidth: 850,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }) => (
      <div>
        <Tooltip
          title={rowData.description || ''}
          placement={'top-start'}
          enterDelay={700}
        >
          {rowData.description}
        </Tooltip>
      </div>
    ),
  }),
});
