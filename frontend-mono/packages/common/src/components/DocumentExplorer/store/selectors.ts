// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommonItemType } from '../components/FileExplorer/types/FileExplorerTypes';
import {
  StoreShapeType,
  FiltersType,
} from '../components/DocumentExplorer/types/types';

export const getItemsFilteredAndSortedByNameAndType = (
  state: StoreShapeType,
  filters: FiltersType
) => {
  const { items } = state;
  if (!items || !items.length) return items;

  const nameSort = (sortA: CommonItemType, sortB: CommonItemType) =>
    sortA.name.localeCompare(sortB.name);

  const directory = items
    .filter((item: CommonItemType) => item.type === 'directory')
    .sort(nameSort);
  const documents = items
    .filter((item: CommonItemType) => item.type === 'document')
    .sort(nameSort);

  const sorted = [...directory, ...documents];
  const filtered = sorted.filter(item =>
    Object.values(filters).every(filter => filter && filter(item))
  );

  return filtered;
};
