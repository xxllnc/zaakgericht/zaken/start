// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useReducer, useEffect } from 'react';
import * as i18next from 'i18next';
import {
  DocumentItemType,
  DirectoryItemType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import { openServerError } from '../../signals';
import {
  StoreShapeType,
  PresetLocations,
} from './components/DocumentExplorer/types/types';
import { fetchData } from './components/DocumentExplorer/utils/requests';
import { initialState, reducer } from './store/store';
import {
  setItemsAction,
  navigateAction,
  setLoadingAction,
  doSearchAction,
  toggleSelectedAction,
} from './store/actions';

type ActionsType = {
  doRefreshAction: () => void;
  doSearchAction: (search: string) => void;
  doNavigateAction: (id: string) => void;
  doFileOpenAction: (item: DocumentItemType) => void;
  doToggleSelectAction: (rowData: DirectoryItemType) => void;
  doSearchCloseAction: () => void;
};

const handleFileOpen = (item: DocumentItemType) => {
  if (item.download) window.open(item.download.url, '_blank');
};

export const useDocumentExplorer = ({
  t,
  getURL,
}: {
  t: i18next.TFunction;
  getURL: (state: StoreShapeType) => string;
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { location, search } = state;

  const loadData = async () => {
    dispatch(setLoadingAction(true));
    try {
      const data = await fetchData({
        url: getURL(state),
        t,
        location,
        search,
      });
      const { items, path } = data;
      dispatch(setItemsAction(items, search, path));
    } catch (errorObj: any) {
      openServerError(errorObj);
    }
    dispatch(setLoadingAction(false));
  };

  useEffect(() => {
    loadData();
  }, [location, search]);

  const actions: ActionsType = {
    doRefreshAction: () => loadData(),
    doSearchAction: search => {
      if (search === '') dispatch(navigateAction(PresetLocations.Home));
      dispatch(doSearchAction(search));
    },
    doNavigateAction: id => dispatch(navigateAction(id)),
    doFileOpenAction: item => handleFileOpen(item),
    doToggleSelectAction: rowData =>
      dispatch(toggleSelectedAction(rowData.uuid)),
    doSearchCloseAction: () => dispatch(navigateAction(PresetLocations.Home)),
  };

  return [state, actions] as const;
};

export default useDocumentExplorer;
