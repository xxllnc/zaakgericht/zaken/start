// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';
import { DocumentPreviewPropsType } from '../DocumentPreview.types';
import { useDocumentPreviewHeaderStyles } from './DocumentPreviewHeader.style';

interface DocumentPreviewHeaderPropsType extends DocumentPreviewPropsType {}

export const DocumentPreviewHeader: React.ComponentType<
  DocumentPreviewHeaderPropsType
> = ({ title, downloadUrl, onClose }) => {
  const classes = useDocumentPreviewHeaderStyles();
  const [t] = useTranslation('common');
  const LinkComponent = React.forwardRef<HTMLAnchorElement>(
    //@ts-ignore
    ({ children, ...props }, ref) => (
      <a
        {...props}
        ref={ref}
        href={downloadUrl}
        target="blank"
        rel="noopener noreferrer"
      >
        {children}
      </a>
    )
  );
  LinkComponent.displayName = 'DownloadLink';

  return title || downloadUrl ? (
    <header className={classes.wrapper}>
      <Typography variant="h3" classes={{ root: classes.title }}>
        {title}
      </Typography>

      {downloadUrl && (
        <Button
          name="downloadDocument"
          component={LinkComponent}
          variant="link"
        >
          {t('filePreview.downloadDocument')}
        </Button>
      )}

      {onClose && (
        <Button name="closeDocumentPreview" icon="close" action={onClose} />
      )}
    </header>
  ) : null;
};
