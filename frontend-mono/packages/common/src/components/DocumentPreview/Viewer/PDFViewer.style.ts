// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePDFViewerStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    wrapper: {
      position: 'relative',
      backgroundColor: greyscale.darkest,
      width: '100%',
      height: '100%',
      overflow: 'hidden',
    },
    pageWrapper: {
      position: 'relative',
      width: '100%',
      height: '100%',
      overflow: 'auto',
    },
    page: {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: 'transparent!important',
      '& > .react-pdf__Page__canvas': {
        margin: 10,
      },
    },
    controls: {
      position: 'absolute',
      bottom: 20,
      right: 20,
      zIndex: 1,
    },
  })
);
