// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import classNames from 'classnames';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { DocumentPreviewPropsType } from '../DocumentPreview.types';
import { ViewerControls } from './ViewerControls';
import { PageControls } from './PageControls';
import { usePDFViewerStyles } from './PDFViewer.style';
import './reset.css';

pdfjs.GlobalWorkerOptions.workerSrc = `/main/pdfjs.worker.mjs`;

export interface PDFViewerPropsType extends DocumentPreviewPropsType {
  className?: string;
}

export const PDFViewer: React.ComponentType<PDFViewerPropsType> = ({
  url,
  className,
  pagination = false,
}) => {
  const classes = usePDFViewerStyles();
  const [scale, setScale] = useState(1);
  const [page, setPage] = useState(1);
  const [rotation, setRotation] = useState(0);
  const [numPages, setNumPages] = useState(0);

  const loader = <Loader delay={200} />;

  const getSinglePage = (pageNumber: number) => (
    <Page
      scale={scale}
      rotate={rotation}
      className={classes.page}
      pageNumber={pageNumber}
      key={pageNumber}
      loading={loader}
    />
  );

  const getAllPages = () => {
    return Array(numPages)
      .fill(false)
      .map((val, index) => getSinglePage(index + 1));
  };

  const getPages = () => (pagination ? getSinglePage(page) : getAllPages());

  return (
    <div className={classNames(classes.wrapper, className && className)}>
      <ViewerControls
        className={classes.controls}
        scale={scale}
        rotation={rotation}
        onScaleChange={setScale}
        onRotationChange={setRotation}
      />
      {pagination && (
        <PageControls page={page} numPages={numPages} onPageChange={setPage} />
      )}
      <div className={classes.pageWrapper}>
        <Document
          file={url}
          onLoadSuccess={({ numPages }) => setNumPages(numPages)}
          loading={loader}
        >
          {numPages > 0 ? getPages() : null}
        </Document>
      </div>
    </div>
  );
};

export default PDFViewer;
