// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import Fab from '@mintlab/ui/App/Material/Fab/Fab';
import { useViewerControlStyles } from './ViewerControls.style';

interface ViewerControlsPropsType {
  scale: number;
  /* Defaults to 0.2 */
  scaleStep?: number;
  rotation?: number;
  className?: string;

  onScaleChange: (scale: number) => void;
  onRotationChange?: (rotation: number) => void;
}

export const ViewerControls: React.ComponentType<ViewerControlsPropsType> = ({
  onRotationChange,
  onScaleChange,
  rotation = 0,
  scale,
  className,
  scaleStep = 0.2,
}) => {
  const [t] = useTranslation('common');
  const classes = useViewerControlStyles();
  const zoomIn = () => onScaleChange(Math.min(5, scale + scaleStep));
  const zoomOut = () =>
    onScaleChange(Math.max(scaleStep * 2, scale - scaleStep));
  const rotate = () => {
    const newRotation = rotation + 90;
    onRotationChange && onRotationChange(newRotation >= 360 ? 0 : newRotation);
  };

  return (
    <div className={classNames(classes.wrapper, className)}>
      <Fab
        iconName="zoom_in"
        sx={{ marginBottom: '10px' }}
        ariaLabel={t('filePreview.zoomIn')}
        size="medium"
        action={zoomIn}
      />
      <Fab
        iconName="zoom_out"
        ariaLabel={t('filePreview.zoomOut')}
        size="medium"
        action={zoomOut}
      />
      {onRotationChange && (
        <Fab
          iconName="rotate_right"
          ariaLabel={t('filePreview.rotateRight')}
          size="small"
          action={rotate}
        />
      )}
    </div>
  );
};
