// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Alert from '../dialogs/Alert/Alert';

export interface ErrorDialogPropsType {
  errorCode: string;
  onClose?: () => void;
  title: string;
  body: string;
  primaryButtonText: string;
  open: boolean;
}

const ErrorDialog: React.FunctionComponent<ErrorDialogPropsType> = ({
  errorCode,
  onClose,
  title,
  body,
  primaryButtonText,
  open = true,
}) => (
  <Alert
    primaryButton={{ text: primaryButtonText, action: onClose }}
    key={errorCode}
    onClose={onClose}
    title={title}
    open={open}
  >
    {body}
  </Alert>
);

export default ErrorDialog;
