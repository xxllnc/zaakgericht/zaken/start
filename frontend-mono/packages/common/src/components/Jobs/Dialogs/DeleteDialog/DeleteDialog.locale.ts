// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    title: 'Start Job - Verwijderen',
    intro: {
      1: 'Dit scherm biedt de mogelijkheid om {{amount}} {{action}}. Dit zal op de achtergrond gebeuren. Na het starten zal dit venster worden gesloten. De voortgang is te bekijken in het "Exportbestanden / Jobs" scherm.',
      2: 'Er zal zoveel mogelijk van de Job worden uitgevoerd. Er zullen twee rapporten worden gegenereerd:',
      3: '1. Een succesrapport, met daar in informatie over de zaken die succesvol zijn verwijderd, inclusief zaken die één of meerdere waarschuwingen of andere opmerkingen bevatten.',
      4: '2. Een foutrapport, met daar in informatie over zaken die niet verwijderd konden worden.',
      5: 'Er wordt aangeraden om altijd een simulatie uit te voeren bij grote bulkacties. Een simulatie produceert dezelfde succes- en foutrapporten, maar voert de acties niet daadwerkelijk uit.',
      6: 'Na het uitvoeren van een simulatie, is het mogelijk de actie opnieuw uit te voeren met dezelfde resultaten, zonder simulatie.',
    },
    amountAll: 'alle {{kind}} die voldoen aan de zoekopdracht',
    amountNr: '{{nr}} {{kind}}',
    types: {
      delete: {
        success: 'Verwijder bulkactie aangemaakt',
        title: 'Bulkactie verwijderen ',
        action: 'te verwijderen',
      },
    },
    fields: {
      friendlyName: {
        placeholder: 'Omschrijving (optioneel)',
      },
      simulation: {
        label: 'Uitvoeren in simulatie-modus',
      },
      approve: {
        label:
          'Ik bevestig het uitvoeren van deze bulkactie en ben mij bewust van de gevolgen.',
        error: 'Dit veld is verplicht',
      },
    },
  },
};

export default locale;
