// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { KindType } from '../../../../../apps/main/src/modules/advancedSearch/AdvancedSearch.types';

export const getJobKind = (jobType: string): KindType => {
  const caseTypes = ['delete_case'];
  const objectTypes: any[] = [];
  if (caseTypes.includes(jobType)) return 'case';
  if (objectTypes.includes(jobType)) return 'custom_object';
  return 'case';
};
