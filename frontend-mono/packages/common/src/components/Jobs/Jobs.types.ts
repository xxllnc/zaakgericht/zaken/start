// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { APIJobs } from '@zaaksysteem/generated';
export type BaseJobEntryType = {
  id: string;
  uuid: string;
  name: string;
  progress: number;
  expires: string;
  friendlyName: string;
  resultsFileSize?: number;
  resultsFileType?: string;
  totalItemCount?: number;
  started?: string;
  isLastInGroup?: boolean;
};

export type JobEntryType = BaseJobEntryType & {
  type: 'job';
  jobType: APIJobs.GetJobsResponseBody['data'][number]['attributes']['job_description']['job_type'];
  status: APIJobs.GetJobsResponseBody['data'][number]['attributes']['status'];
  selection: APIJobs.GetJobsResponseBody['data'][number]['attributes']['job_description']['selection'];
  simulation: boolean;
  errorsFileSize?: number;
};
export type LegacyDownloadEntryType = BaseJobEntryType & {
  type: 'legacy_download';
  status: 'finished';
  token: string;
  fileUUID: string;
};

export type AnyEntryType = JobEntryType | LegacyDownloadEntryType;
export type FieldType = 'resultsFile' | 'errorFile';

// Files
export type BaseFileType = {
  id: string;
  jobType: APIJobs.GetJobsResponseBody['data'][number]['attributes']['job_description']['job_type'];
};

export type BaseCaseFileType = {
  caseID: string;
  simulation: boolean;
};

export interface GenericResultsFileType extends BaseCaseFileType, BaseFileType {
  type: 'result' | 'warning';
  remarks: string;
}

export interface GenericErrorFileType extends BaseCaseFileType, BaseFileType {
  type: 'error';
  errors: string;
}

export type AnyFileType = GenericResultsFileType | GenericErrorFileType;
