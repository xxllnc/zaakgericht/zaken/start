// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { navToLogin } from './LoginWrapper.library';

const LoginWrapper: React.ComponentType<{
  children: React.ReactNode;
}> = ({ children }) => {
  const session = useSession();

  if (!session) {
    return <Loader />;
  } else if (!session.logged_in_user) {
    navToLogin();

    return <Loader delay={200} />;
  } else {
    return <>{children}</>;
  }
};

export default LoginWrapper;
