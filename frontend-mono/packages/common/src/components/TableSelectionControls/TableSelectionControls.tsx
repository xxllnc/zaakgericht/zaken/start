// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import { useTableSelectionControlsStyles } from './TableSelectionControls.style';

/* eslint complexity: [2, 20] */
const TableSelectionControls: FunctionComponent<
  Pick<
    useSelectionBehaviourReturnType,
    | 'pageSelected'
    | 'everythingSelected'
    | 'onSelectEverything'
    | 'selectEverythingTranslations'
  >
> = ({
  pageSelected,
  everythingSelected,
  onSelectEverything,
  selectEverythingTranslations,
}) => {
  const classes = useTableSelectionControlsStyles();

  return pageSelected && selectEverythingTranslations ? (
    <div className={classes.selectAll}>
      <span>
        {everythingSelected
          ? selectEverythingTranslations.deselect
          : selectEverythingTranslations.select}
      </span>
      <Button
        name="selectAll"
        action={onSelectEverything}
        variant="outlined"
        sx={{
          height: '20px',
          paddingTop: '7px',
        }}
      >
        {everythingSelected
          ? selectEverythingTranslations.deselectButton
          : selectEverythingTranslations.selectButton}
      </Button>
    </div>
  ) : null;
};

export default TableSelectionControls;
