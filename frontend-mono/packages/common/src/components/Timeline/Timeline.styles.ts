// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => {
  return {
    wrapper: {
      padding: '0px 30px 0 30px',
      height: '100%',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    content: {
      flexGrow: 1,
    },
    item: {
      width: '100%',
      display: 'flex',
      paddingRight: 20,
    },
    itemTime: {
      width: 120,
      color: theme.palette.elephant.light,
      fontWeight: theme.typography.fontWeightRegular,
    },
    itemType: {
      display: 'flex',
      alignItems: 'center',
      width: 60,
      flexDirection: 'column',
      '&>:nth-child(1)': {
        height: 36,
      },
      '&>:nth-child(2)': {
        flex: 1,
        width: 2,
        backgroundColor: theme.palette.cloud.main,
        marginBottom: 10,
      },
    },
    itemContentList: {
      paddingLeft: 5,
    },
    itemContent: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
      '&>:nth-child(1)': {
        marginBottom: 10,
        fontWeight: theme.typography.fontWeightMedium,
      },
      '&>:nth-child(2)': {
        margin: '0px 0px 16px 0px',
      },
    },
    itemDescription: {
      padding: 20,
      backgroundColor: theme.palette.cloud.light,
      borderRadius: 10,
      marginBottom: 24,
    },
    itemContentLine: {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    },
    itemException: {
      whiteSpace: 'break-spaces',
      padding: 20,
      backgroundColor: theme.palette.cloud.light,
      borderRadius: 10,
      marginBottom: 0,
    },
    toolbarWrapper: {
      display: 'flex',
      marginBottom: 30,
      alignItems: 'center',
      '&>:nth-child(1)': {},
      '&>:nth-child(2)': {
        width: 280,
        marginLeft: 'auto',
      },
      '&>:nth-child(3)': {
        width: 60,
        textAlign: 'center',
        color: theme.palette.elephant.main,
      },
      '&>:nth-child(4)': {
        width: 280,
      },
      '&>:nth-child(5)': {
        marginLeft: 26,
        width: 80,
      },
    },
    loader: {
      position: 'absolute',
      bottom: 1,
      width: '100%',
    },
    popover: {
      ...theme.typography.body2,
    },
  };
});
