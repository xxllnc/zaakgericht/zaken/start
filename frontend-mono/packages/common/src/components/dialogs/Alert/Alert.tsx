// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '../library/createDialogActions';

type AlertPropsType = {
  open?: boolean;
  title: string;
  scope?: string;
  icon?: string;
  onClose?: () => void;
  primaryButton: {
    text: string;
    onClick?: (event: React.MouseEvent) => void;
    action?: (event: React.MouseEvent) => void;
  };
  secondaryButton?: {
    text: string;
    onClick?: (event: React.MouseEvent) => void;
    action?: (event: React.MouseEvent) => void;
  };
  container?: React.ReactInstance | null;
  children?: any;
};

export const Alert: React.ComponentType<AlertPropsType> = ({
  open = false,
  primaryButton,
  secondaryButton,
  title,
  scope,
  children,
  onClose,
  container,
}) => {
  return (
    <Dialog
      aria-label={title}
      open={open}
      disableBackdropClick={true}
      onClose={onClose}
      container={container}
    >
      <DialogTitle title={title} scope={scope} />
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        {createDialogActions(
          //@ts-ignore
          [primaryButton, secondaryButton],
          scope
        )}
      </DialogActions>
    </Dialog>
  );
};

export default Alert;
