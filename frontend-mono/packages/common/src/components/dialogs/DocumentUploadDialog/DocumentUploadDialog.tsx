// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import { FileUploadDialog } from '@zaaksysteem/common/src/components/dialogs/FileUploadDialog/FileUploadDialog';
import { openServerError } from '../../../signals';
import { createDocuments } from './requests';

export type DocumentUploadDialogPropsType = {
  onConfirm: () => void;
  onClose: () => void;
  open: boolean;
  caseUUID?: string;
  directoryUUID?: string;
  multiValue?: boolean;
};

export const DocumentUploadDialog: React.FunctionComponent<
  DocumentUploadDialogPropsType
> = ({ onConfirm, onClose, open, caseUUID, directoryUUID, multiValue }) => {
  const handleDocumentCreation = async (formValues: FormikValues) => {
    try {
      await createDocuments({
        values: formValues.files,
        caseUUID,
        directoryUUID,
      });

      onConfirm();
    } catch (errorObj: any) {
      openServerError(errorObj);
    }
  };

  return (
    <FileUploadDialog
      onConfirm={handleDocumentCreation}
      onClose={onClose}
      open={open}
      multiValue={multiValue}
    />
  );
};
