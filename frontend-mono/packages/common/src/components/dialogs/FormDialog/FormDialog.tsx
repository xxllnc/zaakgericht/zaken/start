// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { v4 as unique } from 'uuid';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  CircularProgress,
  DialogActions,
  DialogContent,
  Divider,
  useTheme,
} from '@mui/material';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  FormDefinition,
  FormValuesType,
  UseFormType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { DialogTitlePropsType } from '@mintlab/ui/App/Material/Dialog/DialogTitle/DialogTitle';
import { useForm } from '../../form/hooks/useForm';
import { useStyles } from './FormDialog.style';

export type FormDialogPropsType<Values> = Omit<
  UseFormType<Values>,
  'onSubmit'
> & {
  formDefinition: FormDefinition<Values>;
  initialValues?: Values;
  onClose?: () => void;
  title: string;
  onSubmit?: (values: FormValuesType<Values>) => Promise<any> | void;
  scope?: string;
  saveLabel?: string;
  icon?: DialogTitlePropsType['icon'];
  initializing?: boolean;
  compact?: boolean;
  saving?: boolean;
  open?: boolean;
  container?: React.ReactInstance | null;
  formDefinitionT?: i18next.TFunction;
  minHeight?: number;
  prependContents?: React.ReactNode;
  appendContents?: React.ReactNode;
  disableSubmit?: boolean;
  extraContents?: React.ReactNode;
  fullScreen?: boolean;
};

/* eslint complexity: [2, 8] */
function FormDialog<Values>({
  onSubmit,
  formDefinition,
  title,
  scope,
  saveLabel,
  icon,
  initializing = false,
  compact = true,
  saving = false,
  validationMap,
  fieldComponents,
  rules,
  isInitialValid = false,
  formDefinitionT,
  onClose,
  open = true,
  container,
  minHeight = 400,
  prependContents,
  appendContents,
  initialValues,
  disableSubmit,
  fullScreen,
  initialTouched = {},
}: FormDialogPropsType<Values>) {
  const [submittedValues, setSubmittedValues] = React.useState(null as any);
  const dialogEl = useRef(null);
  const [t] = useTranslation('common');
  const theme = useTheme<any>();
  const classes = useStyles(minHeight)(theme);

  /* eslint complexity: [2, 12] */
  const FormDialogContent = () => {
    const {
      fields,
      formik: { isValid, values },
    } = useForm({
      t: formDefinitionT,
      //@ts-ignore
      formDefinition,
      //@ts-ignore
      initialValues,
      isInitialValid,
      validationMap,
      rules: rules,
      fieldComponents,
      initialTouched,
    });

    return (
      <React.Fragment>
        <DialogContent>
          {prependContents}
          {fields.map(
            ({
              FieldComponent,
              key,
              type,
              suppressLabel = false,
              suppressWrapperLabel = false,
              ...rest
            }) => {
              const props = cloneWithout(rest, 'definition', 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  compact={compact}
                  label={suppressWrapperLabel ? false : props.label}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent
                    {...props}
                    value={
                      (submittedValues && submittedValues[props.name]) ||
                      props.value
                    }
                    t={t}
                    containerRef={dialogEl.current}
                    submit={() => {
                      if (!isValid) return;
                      //@ts-ignore
                      onSubmit(values);
                    }}
                    label={suppressLabel ? false : props.label}
                  />
                </FormControlWrapper>
              );
            }
          )}
          {appendContents}
        </DialogContent>
        {Boolean(onSubmit) && (
          <>
            <Divider />
            <DialogActions>
              {createDialogActions(
                [
                  {
                    startIcon: saving && (
                      <CircularProgress
                        style={{ marginRight: '10px' }}
                        size={15}
                      />
                    ),
                    disabled: disableSubmit === true || saving || !isValid,
                    text: saveLabel ? saveLabel : t('dialog.save'),
                    onClick() {
                      //@ts-ignore
                      const res = onSubmit(values);

                      //@ts-ignore
                      if (res instanceof Promise) {
                        setSubmittedValues(values);
                        res.finally(() => setSubmittedValues(null));
                      } else {
                        console.warn('Make sure onSubmit returns promise');
                      }
                    },
                  },
                  ...(onClose
                    ? [
                        {
                          sx: { display: saving ? 'none' : 'flex' },
                          text: t('forms.cancel'),
                          onClick: onClose,
                        },
                      ]
                    : []),
                ],
                scope || ''
              )}
            </DialogActions>
          </>
        )}
      </React.Fragment>
    );
  };

  return (
    <Dialog
      classes={{
        root: fullScreen ? classes.dialogRoot : '',
        paper: classes.dialogPaper,
      }}
      aria-label={title}
      open={open}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
      container={container}
      fullScreen={fullScreen}
    >
      <DialogTitle
        elevated={true}
        id={unique()}
        title={title}
        onCloseClick={onClose}
        scope={scope}
        {...(icon && { icon })}
        buttonProps={{ name: t('dialog.close') }}
      />
      {initializing ? (
        <Loader style={{ paddingTop: 0, margin: '60px auto' }} />
      ) : (
        <FormDialogContent />
      )}
    </Dialog>
  );
}

export default FormDialog;
