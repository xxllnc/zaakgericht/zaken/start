// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';

export const useCaseDocumentChoicesQuery = (caseUuid: string) => {
  const data = useQuery(
    ['caseDocuments', caseUuid],
    async ({ queryKey: [__, case_uuid] }) => {
      const body = await request<APIDocument.SearchDocumentResponseBody>(
        'GET',
        buildUrl<APIDocument.SearchDocumentRequestParams>(
          `/api/v2/document/search_document`,
          { case_uuid: case_uuid }
        )
      );

      return body
        ? (body.data || [])
            .map(document => ({
              value: document.relationships.file.data.id,
              label: document.attributes.name,
              number: document.meta.document_number,
              confidential:
                document.attributes.metadata?.confidentiality ===
                'Confidentieel',
            }))
            .sort((docA, docB) => docA.label.localeCompare(docB.label))
        : [];
    },
    { onError: openServerError }
  );

  return {
    loading: data.status === 'loading',
    choices: data.data || [],
  };
};
