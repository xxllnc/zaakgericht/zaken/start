// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { VisibilityOff } from '@mui/icons-material';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldComponentType } from '../../types/Form2.types';
import { useCaseDocumentFinderStyle } from './CaseDocumentFinder.style';
import { useCaseDocumentChoicesQuery } from './CaseDocumentFinder.library';

const CaseDocumentFinder: FormFieldComponentType<
  ValueType<string>[],
  { caseUuid: string; defaultDocuments: string[]; accept: string[] }
> = ({
  multiValue = true,
  config: { caseUuid, defaultDocuments, accept },
  ...restProps
}) => {
  const [temp, setTemp] = React.useState(defaultDocuments || []);
  const classes = useCaseDocumentFinderStyle();
  const selectProps = useCaseDocumentChoicesQuery(caseUuid);

  React.useEffect(() => {
    defaultDocuments &&
      restProps.onChange({
        target: {
          name: restProps.name,
          value: defaultDocuments
            .map(doc =>
              selectProps.choices.find(choice => choice.number === Number(doc))
            )
            .filter(Boolean) as ValueType<string>[],
        },
      });
  }, [selectProps.loading]);

  return (
    <div
      className={classNames(
        restProps.applyBackgroundColor && classes.withBackground
      )}
    >
      <Select
        {...restProps}
        {...selectProps}
        choices={selectProps.choices.filter(document =>
          (accept || [''])?.some(ext =>
            document.label.toLowerCase().endsWith(ext)
          )
        )}
        onChange={ev => {
          setTemp([]);
          restProps.onChange(ev);
        }}
        renderOption={(props, option, index) => {
          return (
            <li
              {...props}
              //@ts-ignore
              key={option.key || option.label + props['data-option-index']}
            >
              <span>{option.label}</span>
              {option.confidential ? (
                <VisibilityOff sx={{ opacity: 0.6, marginLeft: '10px' }} />
              ) : null}
            </li>
          );
        }}
        value={
          temp.length && selectProps.choices
            ? selectProps.choices.filter(choice =>
                temp.includes(choice.number?.toString() || '')
              )
            : restProps.value
        }
        isMulti={multiValue}
      />
    </div>
  );
};

export default CaseDocumentFinder;
