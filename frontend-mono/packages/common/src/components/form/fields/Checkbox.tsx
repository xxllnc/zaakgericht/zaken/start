// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import MaterialCheckbox from '@mintlab/ui/App/Material/Checkbox';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const Checkbox: FormFieldComponentType<string> = ({
  readOnly,
  ...restProps
}) => {
  const [t] = useTranslation('common');

  return readOnly ? (
    <ReadonlyValuesContainer
      value={restProps.value ? t('forms.checkboxReadonlyChecked') : ''}
    />
  ) : (
    <MaterialCheckbox {...restProps} />
  );
};

export default Checkbox;
