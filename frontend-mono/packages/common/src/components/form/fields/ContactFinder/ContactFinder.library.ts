// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { openServerError } from '../../../../signals';
import { ContactFinderConfigType, ContactType } from './ContactFinder.types';

type SearchContactRequestParams = APICommunication.SearchContactRequestParams;
type SearchContactResponseBody = APICommunication.SearchContactResponseBody;

export const fetchContactChoices =
  (config = defaultConfig as ContactFinderConfigType) =>
  async (input: string) => {
    let filters = {} as any;

    if (config.subjectTypes) {
      filters['filter[type]'] = config.subjectTypes.join(',');
    }

    if (config.employeeStatusActive) {
      filters['filter[employee.status.active]'] = config.employeeStatusActive;
    }

    const body = await request<SearchContactResponseBody>(
      'GET',
      buildUrl<SearchContactRequestParams>(
        `/api/v2/communication/search_contact`,
        {
          keyword: input,
          ...filters,
        }
      )
    ).catch(openServerError);

    return body
      ? body.data
          .map<ContactType | null>(contact => {
            const item = createDefaultItem(contact);
            return config.itemResolver
              ? config.itemResolver(contact, item)
              : item;
          })
          .filter((item): item is ContactType => Boolean(item))
      : [];
  };

const defaultConfig: Pick<
  ContactFinderConfigType,
  'subjectTypes' | 'itemResolver'
> = {
  subjectTypes: [],
  itemResolver: (contact, defaultItem) => defaultItem,
};

const createDefaultItem = ({
  id,
  attributes: { name, address, type },
}: APICommunication.ContactEntity): ContactType => ({
  value: id,
  key: id,
  label: name,
  type,
  ...(address ? { subLabel: address } : {}),
});
