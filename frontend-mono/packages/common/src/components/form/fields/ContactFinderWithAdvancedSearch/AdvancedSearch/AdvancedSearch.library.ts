// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { hasCapability } from '@zaaksysteem/common/src/hooks/useSession';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  fetchStufConfig,
  search,
  importContact,
} from './AdvancedSearch.requests';
import {
  ContactTypeType,
  FilterIntegrationsType,
  FormatIntegrationsType,
  GetStufConfigIntegrationsType,
  FormatResultsType,
  PerformSearchType,
  PerformImportContactType,
  ContactTypeV1Type,
  FormatGenderType,
} from './AdvancedSearch.types';

export const contactTypes: ContactTypeType[] = ['person', 'organization'];
export const contactTypesV1: ContactTypeV1Type[] = ['person', 'company'];
const transformContactTypeToV1 = (contactType: ContactTypeType) =>
  contactTypesV1[contactTypes.indexOf(contactType)];
const transformContactTypeToV2 = (contactType: ContactTypeV1Type) =>
  contactTypes[contactTypesV1.indexOf(contactType)];

export const findField = (
  fields: AnyFormDefinitionField[],
  fieldName: string
) => fields.filter((field: any) => field.name === fieldName)[0];

const filterIntegrations: FilterIntegrationsType = (rows, session) =>
  rows.filter(
    ({
      instance: {
        interface_config: {
          gbav_search,
          local_search,
          search_extern_webform_only,
          gbav_search_role_restriction,
        },
      },
    }) => {
      // the feature should be on
      if (!gbav_search && !local_search) {
        return false;
      }

      // the feature should not be restricted to the webform
      if (search_extern_webform_only) {
        return false;
      }

      // if the feature is restricted to the external search role
      // the user should have that role
      if (
        gbav_search_role_restriction &&
        !hasCapability(session, 'contact_search_extern')
      ) {
        return false;
      }

      return true;
    }
  );

const formatIntegrations: FormatIntegrationsType = rows =>
  rows.map(
    ({
      instance: {
        id,
        interface_config: { search_form_title },
      },
    }) => ({
      label: search_form_title,
      value: `${id}`,
    })
  );

export const getStufConfigIntegrations: GetStufConfigIntegrationsType =
  async session => {
    const response = await fetchStufConfig();
    const rows = response.result.instance.rows || [];
    const allowedIntegrations = filterIntegrations(rows, session);
    const integrations = formatIntegrations(allowedIntegrations);

    return integrations;
  };

const formatGender: FormatGenderType = gender => {
  if (gender === 'M') {
    return 'male';
  } else if (gender === 'V') {
    return 'female';
  } else {
    return 'perm_identity';
  }
};

const getForeignAddress = (address: any) =>
  [
    address.foreign_address_line1,
    address.foreign_address_line2,
    address.foreign_address_line3,
    address.country.instance.label,
  ]
    .filter(emptyFilter)
    .join(', ');

/* eslint complexity: [2, 7] */
const getDomesticAddress = ({
  street,
  street_number,
  street_number_letter,
  street_number_suffix,
  zipcode,
  city,
}: any) => {
  const separator =
    (street_number || street_number_letter) && street_number_suffix ? '-' : '';

  const number = `${street_number || ''}${
    street_number_letter || ''
  }${separator}${street_number_suffix || ''}`;

  return [street, number, zipcode, city].filter(Boolean).join(', ');
};

const emptyFilter = (part: any) => part;
const getFormattedAddress = (address: any) => {
  return address
    ? address.foreign_address_line1
      ? getForeignAddress(address)
      : getDomesticAddress(address)
    : '';
};

const formatResults: FormatResultsType = (rows, integrationId) =>
  rows.map(row => {
    const {
      instance: {
        display_name,
        subject_type,
        subject: {
          reference,
          instance: {
            gender,
            date_of_birth,
            date_of_death,
            is_secret,
            coc_number,
            coc_location_number,
            address_residence,
            address_correspondence,
          },
        },
      },
    } = row;

    return {
      uuid: reference || v4(),
      type: transformContactTypeToV2(subject_type),
      name: display_name,
      integrationId,
      gender: formatGender(gender),
      dateOfBirth: date_of_birth || null,
      hasCorrespondenceAddress: Boolean(address_correspondence),
      isDeceased: Boolean(date_of_death),
      isSecret: Boolean(is_secret),
      cocNumber: coc_number || null,
      cocLocationNumber: coc_location_number || null,
      address: getFormattedAddress(
        address_residence?.instance || address_correspondence?.instance || null
      ),
      rawResponse: row,
    };
  });

const fieldsMap = {
  person: {
    bsn: { name: 'subject.personal_number' },
    sedulaNumber: { name: 'subject.persoonsnummer' },
    dateOfBirth: {
      name: 'subject.date_of_birth',
      parse: (value: Date) =>
        new Date(new Date(value).setHours(12)).toISOString().replace(/T.*/, ''),
    },
    familyName: { name: 'subject.family_name' },
    prefix: { name: 'subject.prefix' },
    zipCode: { name: 'subject.address_residence.zipcode' },
    streetNumber: { name: 'subject.address_residence.street_number' },
    streetNumberLetter: {
      name: 'subject.address_residence.street_number_letter',
    },
    suffix: { name: 'subject.address_residence.street_number_suffix' },
  },
  organization: {
    rsin: { name: 'subject.rsin' },
    cocNumber: { name: 'subject.coc_number' },
    cocLocationNumber: { name: 'subject.coc_location_number' },
    tradeName: { name: 'subject.company' },
    street: { name: 'subject.address_residence.street' },
    zipCode: { name: 'subject.address_residence.zipcode' },
    streetNumber: { name: 'subject.address_residence.street_number' },
    streetNumberLetter: {
      name: 'subject.address_residence.street_number_letter',
    },
    suffix: { name: 'subject.address_residence.street_number_suffix' },
  },
};

export const performSearch: PerformSearchType = async (contactType, values) => {
  const integrationId = values.searchIn;
  const formattedData = Object.entries(values).reduce((acc, [key, value]) => {
    const fieldMap = fieldsMap[contactType];
    const isDataItem = Object.prototype.hasOwnProperty.call(fieldMap, key);

    if (!isDataItem || !value) return acc;

    // @ts-ignore
    const { name, parse } = fieldMap[key];

    return {
      ...acc,
      [name]: parse ? parse(value) : value,
    };
  }, {});
  const data = {
    query: {
      match: {
        ...formattedData,
        subject_type: transformContactTypeToV1(contactType),
      },
    },
  };
  const response = await search(data, integrationId);
  const rows = response.result.instance.rows || [];

  const results = formatResults(rows, integrationId);

  return results;
};

export const performImportContact: PerformImportContactType = async (
  contactToImport,
  contactType
) => {
  const data = {
    uuid: contactToImport.uuid,
    ...contactToImport.rawResponse,
  };

  const response = await importContact(
    data,
    contactToImport.integrationId,
    contactType
  );

  return response;
};
