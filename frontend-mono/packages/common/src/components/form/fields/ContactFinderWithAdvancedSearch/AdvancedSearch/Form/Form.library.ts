// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { COUNTRY_CODE_CURACAO } from '../../../../../../constants/countryCodes.constants';

export const getRequiredPersonFields = (countryCode: string) => [
  ['bsn'],
  ...(countryCode === COUNTRY_CODE_CURACAO ? [['sedulaNumber']] : []),
  ['dateOfBirth', 'familyName'],
  ['zipCode', 'streetNumber'],
];

export const getRequiredOrganizationFields = () => [
  ['rsin'],
  ['cocNumber'],
  ['cocLocationNumber'],
  ['tradeName'],
  ['zipCode', 'streetNumber'],
];
