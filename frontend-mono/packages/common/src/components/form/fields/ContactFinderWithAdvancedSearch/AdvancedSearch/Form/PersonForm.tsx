// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Button from '@mintlab/ui/App/Material/Button';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { StufConfigIntegrationType } from '../AdvancedSearch.types';
import { useAdvancedSearchStyles } from '../AdvancedSearch.styles';
import { getFormDefinition, getRules } from './PersonForm.formDefinition';

export type FormPropsType = {
  onSubmit: any;
  countryCode: string;
  stufConfigIntegrations: StufConfigIntegrationType[];
};

const PersonForm: React.ComponentType<FormPropsType> = ({
  onSubmit,
  countryCode,
  stufConfigIntegrations,
}) => {
  const classes = useAdvancedSearchStyles();
  const [t] = useTranslation('ContactFinderWithAdvancedSearch');

  const formDefinition = getFormDefinition(t, stufConfigIntegrations);
  const rules = getRules(countryCode, stufConfigIntegrations);

  let {
    fields,
    formik: { values, isValid },
  } = useForm({
    formDefinition,
    rules,
  });

  return (
    <div className={classes.formWrapper}>
      <div>
        {fields.map(({ FieldComponent, key, type, suppressLabel, ...rest }) => {
          const props = cloneWithout(rest, 'definition', 'mode');

          return (
            <FormControlWrapper
              {...props}
              label={suppressLabel ? false : props.label}
              key={`${props.name}-formcontrol-wrapper`}
            >
              <FieldComponent {...props} t={t} />
            </FormControlWrapper>
          );
        })}
      </div>
      <div className={classes.submit}>
        <Button
          name="search"
          variant="contained"
          disabled={!isValid}
          action={() => onSubmit(values)}
        >
          {t('search')}
        </Button>
      </div>
    </div>
  );
};

export default PersonForm;
