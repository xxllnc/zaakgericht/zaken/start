// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const locale = {
  nl: {
    advancedSearch: 'Uitgebreid zoeken',
    search: 'Zoeken',
    types: {
      person: 'Persoon',
      organization: 'Organisatie',
    },
    fields: {
      searchIn: 'Zoeken in',
      rsin: 'RSIN',
      bsn: 'BSN',
      sedulaNumber: 'ID Nummer',
      dateOfBirth: 'Geboortedatum',
      familyName: 'Achternaam',
      prefix: 'Voorvoegsel',
      street: 'Straatnaam',
      zipCode: 'Postcode',
      streetNumber: 'Huisnummer',
      streetNumberLetter: 'Huisletter',
      streetNumberSuffix: 'Huisnummertoevoeging',
      cocNumber: 'KVK-nummer',
      cocLocationNumber: 'Vestigingsnummer',
      tradeName: 'Handelsnaam',
    },
    integration: {
      searchIn: {
        intern: 'Zaaksysteem (intern)',
        extern: 'Extern',
      },
    },
    warnings: {
      isDeceased: 'Contact is overleden.',
      isSecret: 'Contact heeft geheimhouding.',
      hasCorrespondenceAddress: 'Contact heeft correspondentie-adres.',
    },
    columns: {
      name: 'Naam',
      dateOfBirth: 'Geboortedatum',
      address: 'Adres',
      cocNumber: 'KVK-nr.',
      cocLocationNumber: 'Vestigingsnr.',
      tradeName: 'Handelsnaam',
    },
    importContact: 'Importeer een contact',
    import: 'Importeren',
    confirm: 'Weet u zeker dat u {{preview}} wilt importeren?',
    noResults:
      'Geen resultaten gevonden. Verfijn de zoekopdracht en probeer opnieuw.',
    maxRows:
      'Er worden maximaal {{rows}} resultaten weergegeven. Verfijn eventueel de zoekopdracht.',
    searchIn: 'Zoeken in',
    local: 'Zaaksysteem (intern)',
    results: {
      searchFor:
        'Zoek een {{contactType}} met minimaal één van de volgende filtercombinaties',
      noResults:
        'Geen resultaten gevonden. Verfijn de zoekopdracht en probeer opnieuw.',
    },
  },
};

export default locale;
