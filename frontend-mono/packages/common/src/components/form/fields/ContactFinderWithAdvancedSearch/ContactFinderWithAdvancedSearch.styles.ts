// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useContactFinderWithAdvancedSearchStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: Theme) => {
    return {
      wrapper: {
        display: 'flex',
      },
      component: {
        backgroundColor: greyscale.light,
        width: '100%',
        borderRadius: radius.defaultFormElement,
      },
      button: {
        marginLeft: 16,
        maxWidth: 'min-content',
      },
    };
  }
);
