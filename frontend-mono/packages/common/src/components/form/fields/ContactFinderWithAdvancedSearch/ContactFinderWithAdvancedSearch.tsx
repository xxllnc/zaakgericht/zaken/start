// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import ContactFinder from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder';
import { FormFieldComponentType } from '../../types/Form2.types';
import AdvancedSearch from './AdvancedSearch';
import { useContactFinderWithAdvancedSearchStyles } from './ContactFinderWithAdvancedSearch.styles';

export type ContactFinderWithAdvancedSearchPropsType = FormFieldComponentType<
  any,
  { subjectTypes?: SubjectTypeType[] }
>;

const ContactFinderWithAdvancedSearch: ContactFinderWithAdvancedSearchPropsType =
  props => {
    const [t] = useTranslation('ContactFinderWithAdvancedSearch');
    const classes = useContactFinderWithAdvancedSearchStyles();
    const [open, setOpen] = useState(false);
    const { onChange, name, disabled, config } = props;
    const subjectTypes = config?.subjectTypes || [
      'person',
      'organization',
      'employee',
    ];

    const advancedSearchAllowed = () =>
      !disabled &&
      (subjectTypes.includes('person') ||
        subjectTypes.includes('organization'));

    const handleSelectRow = ({ label, value, type }: ValueType<String>) => {
      onChange({
        target: {
          name,
          value: {
            label,
            value,
            type,
          },
        },
      } as React.ChangeEvent<any>);
      setOpen(false);
    };

    return (
      <div className={classes.wrapper}>
        <div className={classes.component}>
          <ContactFinder {...props} />
        </div>

        <div className={classes.button}>
          <Tooltip title={t('advancedSearch')}>
            <IconButton
              onClick={() => setOpen(true)}
              color="inherit"
              disabled={!advancedSearchAllowed()}
            >
              <Icon size="small">{iconNames.import}</Icon>
            </IconButton>
          </Tooltip>
        </div>

        {open && (
          <AdvancedSearch
            onSelectRow={handleSelectRow}
            onClose={() => setOpen(false)}
            subjectTypes={subjectTypes}
          />
        )}
      </div>
    );
  };

export default ContactFinderWithAdvancedSearch;
