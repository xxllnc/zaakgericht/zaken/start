// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './ContactFinderWithAdvancedSearch.locale';
import ContactFinderWithAdvancedSearch, {
  ContactFinderWithAdvancedSearchPropsType,
} from './ContactFinderWithAdvancedSearch';

const ContactFinderWithAdvancedSearchModule: ContactFinderWithAdvancedSearchPropsType =
  props => (
    <I18nResourceBundle
      resource={locale}
      namespace="ContactFinderWithAdvancedSearch"
    >
      <ContactFinderWithAdvancedSearch {...props} />
    </I18nResourceBundle>
  );

export default ContactFinderWithAdvancedSearchModule;
