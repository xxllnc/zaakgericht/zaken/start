// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '../../types/Form2.types';
import { useCountryChoicesQuery } from './CountryFinder.library';

export const CountryFinder: FormFieldComponentType<ValueType<string>, any> = ({
  value,
  multiValue,
  styles,
  config,
  ...restProps
}) => {
  const [selectProps, emptyChoicesResult] = useCountryChoicesQuery();

  return (
    <Select
      isClearable={false}
      {...restProps}
      {...selectProps}
      value={value}
      disabled={Boolean(restProps?.disabled || emptyChoicesResult)}
      isMulti={multiValue}
    />
  );
};
