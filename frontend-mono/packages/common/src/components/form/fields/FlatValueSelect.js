// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * !!!!!!!!!!!!!!!!!!!!!!
 * THIS IS NOW DEPRECATED
 * !!!!!!!!!!!!!!!!!!!!!!
 *
 * ui/Select has built in support for providing a
 * string as a value. If the string value matches
 * the value from one of the Select's options,
 * that option will be accepted as the Select's
 * value.
 *
 * OnChange of the Select will always return
 * the full Option object. See the `ValueType` type.
 */

import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { get, asArray } from '@mintlab/kitchen-sink/source';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';

/**
Wraps Select, where the value may be provided as
either an object or a flat value.

Always outputs the selected choice as a flat value.
**/

/**
 * @param {*} props
 * @return {ReactElement}
 */
const FlatValueSelect = props => {
  const { value, choices } = props;

  const getNormalizedValue = () => {
    if (typeof value === 'object') return value;
    if (!choices || !asArray(choices).length) return value;

    const foundChoice = choices.find(choice => choice.value === value);

    return foundChoice ? foundChoice : value;
  };
  const normalizedValue = getNormalizedValue();

  const handleChange = event => {
    const { onChange } = props;

    const newEvent = {
      target: {
        ...event.target,
        value: get(event, 'target.value.value', null),
      },
    };
    setTimeout(() => onChange(newEvent), 0);
  };

  return props.readOnly ? (
    <ReadonlyValuesContainer
      value={normalizedValue?.label || normalizedValue}
    />
  ) : (
    <Select {...props} value={normalizedValue} onChange={handleChange} />
  );
};

export default FlatValueSelect;
