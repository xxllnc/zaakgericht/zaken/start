// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';

export const useObjectChoicesQuery = (objectTypeUuid: string | undefined) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const enabled = Boolean(input);

  const data = useQuery(
    ['objects', input, objectTypeUuid],
    async ({ queryKey: [__, keyword, uuid] }) => {
      const body = await request<APICaseManagement.SearchResponseBody>(
        'GET',
        buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
          keyword: keyword || '',
          type: 'custom_object' as any,
          ...(uuid
            ? { 'filter[relationships.custom_object_type.id]': uuid }
            : {}),
        })
      );

      return body
        ? (body.data || []).map(({ id = '', meta, attributes }) => ({
            value: id,
            label: meta?.summary || '',
            ...(attributes?.description && {
              subLabel: attributes.description,
            }),
          }))
        : [];
    },
    { enabled, onError: openServerError }
  );

  return {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };
};
