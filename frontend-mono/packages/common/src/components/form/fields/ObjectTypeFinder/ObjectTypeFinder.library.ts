// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { useQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '../../../../signals';

export const useObjectTypeChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const enabled = Boolean(input && input?.length > 2);

  const data = useQuery(
    ['objectType', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<APICaseManagement.SearchResponseBody>(
        'GET',
        buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
          keyword: keyword || '',
          type: 'custom_object_type' as any,
        })
      );

      return body
        ? (body.data || []).map(({ id = '', meta }) => ({
            value: id,
            label: meta?.summary || '',
          }))
        : [];
    },
    { enabled, onError: openServerError }
  );

  const [setInputDebounced] = useDebouncedCallback(
    async (val: any) => setInput(val),
    400
  );

  return {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInputDebounced(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };
};
