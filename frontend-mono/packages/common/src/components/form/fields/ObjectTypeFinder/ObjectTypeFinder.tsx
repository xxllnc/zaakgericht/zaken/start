// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '../../types/Form2.types';
import { useObjectTypeChoicesQuery } from './ObjectTypeFinder.library';

const ObjectTypeFinder: FormFieldComponentType<ValueType<string>> = props => {
  const selectProps = useObjectTypeChoicesQuery();

  return <Select {...props} {...selectProps} filterOption={() => true} />;
};

export default ObjectTypeFinder;
