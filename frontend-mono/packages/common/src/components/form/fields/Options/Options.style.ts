// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const marginBottom = '20px';

export const useOptionsStylesheet = makeStyles(
  ({ mintlab: { greyscale }, typography }: Theme) => ({
    optionsWrapper: {
      border: `1px solid ${greyscale.dark}`,
      padding: '18px 18px 0px 18px',
      borderRadius: '5px',
      display: 'flex',
      flexDirection: 'column',
    },
    addNew: {
      marginBottom,
    },
    adornment: {
      marginRight: '7px',
      '& button': {
        color: greyscale.darkest,
      },
    },
    errorLabel: {
      marginBottom,
    },
  })
);
