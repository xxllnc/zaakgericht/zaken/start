// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as unique } from 'uuid';
import InputAdornment from '@mui/material/InputAdornment';
import Button from '@mintlab/ui/App/Material/Button';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import Render from '@mintlab/ui/App/Abstract/Render';
import Typography from '@mui/material/Typography';
import ErrorLabel from '@mintlab/ui/App/Zaaksysteem/FormHelpers/library/ErrorLabel';
import { asArray } from '@mintlab/kitchen-sink/source';
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import Option from './Option';
import OptionsList from './OptionsList';
import { useOptionsStylesheet } from './Options.style';
import { ItemValueType, OptionsComponentType } from './Options.types';

/* eslint complexity: [2, 8] */
const Options: OptionsComponentType = ({
  value: valueProp,
  setFieldValue,
  name,
  config: { allowNew = false, showHideInactive = false },
}) => {
  const [t] = useTranslation('options');
  const [newItem, setNewItem] = useState('');
  const [showInactive, setShowInactive] = useState(true);
  const [error, setError] = useState(null as any);
  const value = valueProp || [];
  const classes = useOptionsStylesheet();

  const filteredItems = asArray(value).filter(filteredItem =>
    showInactive ? true : filteredItem.active
  );

  const hasInactiveItems = asArray(value).filter(
    thisItem => !thisItem.active
  ).length;

  const addItem = () => {
    if (!newItem.trim()) return;

    if (value.find(thisItem => thisItem.value === newItem)) {
      setError(t('duplicateName'));
      return;
    }

    const newItems = [
      ...value,
      {
        id: unique(),
        value: newItem,
        active: true,
        isNew: true,
      },
    ];

    setFieldValue(name, newItems);
    setNewItem('');
  };

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return;
    const newItems = reorder(result.source.index, result.destination.index);
    setFieldValue(name, newItems);
  };

  const reorder = (startIndex: number, endIndex: number) => {
    const result = Array.from(value);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const deleteItem = (item: ItemValueType) => {
    const newItems = value.filter(thisItem => item.id !== thisItem.id);
    setFieldValue(name, newItems);
  };

  const switchActive = (item: ItemValueType) => {
    const newItems = value.map(thisItem => {
      if (item.value === thisItem.value) {
        return {
          ...item,
          active: !thisItem.active,
        };
      }
      return thisItem;
    });
    setFieldValue(name, newItems);
  };

  const onKeyPress = (event: any) => {
    const { key } = event;

    setError(null);

    if (key.toLowerCase() === 'enter') {
      addItem();
    }
  };

  return (
    <div className={classes.optionsWrapper}>
      {/* @ts-ignore */}
      <DragDropContext onDragEnd={onDragEnd}>
        {/* @ts-ignore */}
        <Droppable droppableId="attribute-options-droppable">
          {(provided, snapshot) => (
            <OptionsList provided={provided} snapshot={snapshot}>
              {filteredItems.map((item, index) => (
                //@ts-ignore
                <Draggable
                  key={item.value}
                  draggableId={item.value}
                  index={index}
                >
                  {(draggableProvided, draggableSnapshot) => (
                    <Option
                      item={item}
                      //@ts-ignore
                      index={index}
                      provided={draggableProvided}
                      snapshot={draggableSnapshot}
                      onDelete={deleteItem}
                      onSwitchActive={switchActive}
                      t={t}
                    />
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </OptionsList>
          )}
        </Droppable>
      </DragDropContext>
      {allowNew ? (
        <TextField
          classes={{
            formControl: classes.addNew,
          }}
          name="attribute-options-new"
          value={newItem}
          placeholder={t('namePlaceholder')}
          onKeyPress={onKeyPress}
          onChange={({ target }: any) => setNewItem(target.value)}
          InputProps={{
            endAdornment: (
              <InputAdornment
                classes={{
                  root: classes.adornment,
                }}
                position="end"
              >
                <Button
                  name="addOption"
                  action={addItem}
                  label={t('addOption')}
                  icon="add"
                />
              </InputAdornment>
            ),
          }}
        />
      ) : null}

      <Render condition={error}>
        <ErrorLabel label={error} classes={{ root: classes.errorLabel }} />
      </Render>

      <Render
        condition={Boolean(
          showHideInactive && asArray(value).length && hasInactiveItems
        )}
      >
        <Button
          name="showOption"
          action={() => setShowInactive(!showInactive)}
          sx={{ margin: '20px 0' }}
          label={showInactive ? t('hideInactive') : t('showInactive')}
        >
          <Typography variant="subtitle2">
            {showInactive ? t('hideInactive') : t('showInactive')}
          </Typography>
        </Button>
      </Render>
    </div>
  );
};

export default Options;
