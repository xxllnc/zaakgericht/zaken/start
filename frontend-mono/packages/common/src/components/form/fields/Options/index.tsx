// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { OptionsComponentType } from './Options.types';
import locale from './Options.locale';

const Comp = React.lazy(() => import('./Options'));

export default (props: React.ComponentProps<OptionsComponentType>) => (
  <React.Suspense>
    <I18nResourceBundle resource={locale} namespace="options">
      {/* @ts-ignore */}
      <Comp {...props} />
    </I18nResourceBundle>
  </React.Suspense>
);
