// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import MaterialRadioGroup from '@mintlab/ui/App/Material/RadioGroup';
import { FormFieldPropsType } from '../types/formDefinition.types';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';

const RadioGroup: React.FunctionComponent<FormFieldPropsType> = props => {
  const { readOnly, choices, value } = props;

  if (readOnly) {
    const selectedChoice = choices?.find(choice => choice.value === value);

    return <ReadonlyValuesContainer value={selectedChoice || value} />;
  }

  //@ts-ignore
  return <MaterialRadioGroup {...props} />;
};

export default RadioGroup;
