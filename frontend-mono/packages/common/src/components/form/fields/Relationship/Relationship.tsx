// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Upload from '../Upload/Upload';
import { ObjectFinder } from '../ObjectFinder/ObjectFinder';
import ContactFinder from '../ContactFinder/ContactFinder';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyLinkContainer } from '../../library/ReadonlyValuesContainer';

export type RelationshipTypeType = 'custom_object' | 'subject' | 'document';

const relationshipDict = {
  custom_object: { Comp: ObjectFinder, config: {} },
  subject: { Comp: ContactFinder, config: {} },
  document: { Comp: Upload, config: { fileType: 'document' } },
};

const Relationship: FormFieldComponentType<
  any,
  { relationshipType: RelationshipTypeType }
> = props => {
  const relationshipType: RelationshipTypeType = props.config.relationshipType;
  const relationshipField = relationshipDict[relationshipType];
  const { value, readOnly, multiValue } = props;

  return readOnly ? (
    <ReadonlyLinkContainer value={value} type={props.config.relationshipType} />
  ) : (
    <relationshipField.Comp
      {...props}
      config={
        relationshipType === 'document'
          ? relationshipField.config
          : (props.config as any)
      }
      isMulti={multiValue}
      filterOption={() => true}
    />
  );
};

export default Relationship;
