// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// @ts-ignore
import ChoiceChips from '@mintlab/ui/App/Zaaksysteem/ChoiceChips';
import {
  TEXT,
  SELECT,
  CHECKBOX,
  CHECKBOX_GROUP,
  CASE_DOCUMENT_FINDER,
  FLATVALUE_SELECT,
  EMAIL,
  TEXTAREA,
  CHOICE_CHIP,
  MULTI_VALUE_TEXT,
  CASE_FINDER,
  CASE_TYPE_FINDER,
  DATEPICKER,
  UPLOAD,
  CONTACT_FINDER,
  DEPARTMENT_FINDER,
  ROLE_FINDER,
  RADIO_GROUP,
  GEOJSON_MAP_INPUT,
  OBJECT_FINDER,
  OBJECT_TYPE_FINDER,
  RELATIONSHIP,
  WEB_ADDRESS,
  IBAN,
  CURRENCY,
  ADDRESS_BOX,
  ADDRESS_MAP,
  OPTIONS,
  NUMERIC,
  SENSITIVE_DATA,
  PHONE_NUMBER,
  MAGIC_STRING_GENERATOR,
  CONTACT_FINDER_WITH_ADVANCED_SEARCH,
  COUNTRY_FINDER,
  WYSISWYG,
  AMOUNT_AND_TYPE,
  SWITCH,
} from '../constants/fieldTypes';
import { TextField } from './TextField';
import FlatValueSelect from './FlatValueSelect';
import Textarea from './Textarea';
import MultiValueText from './MultiValueText';
import Upload from './Upload/Upload';
import CaseDocumentFinder from './CaseDocumentFinder/CaseDocumentFinder';
import { ObjectFinder } from './ObjectFinder/ObjectFinder';
import ObjectTypeFinder from './ObjectTypeFinder/ObjectTypeFinder';
import Relationship from './Relationship/Relationship';
import CaseFinder from './CaseFinder/CaseFinder';
import { CaseTypeFinder } from './CaseTypeFinder/CaseTypeFinder';
import ContactFinder from './ContactFinder/ContactFinder';
import CheckboxGroup from './CheckboxGroup/CheckboxGroup';
import Checkbox from './Checkbox';
import { DepartmentFinder } from './DepartmentFinder/DepartmentFinder';
import RadioGroup from './RadioGroup';
import RoleFinder from './RoleFinder/RoleFinder';
import DatePicker from './DatePicker';
import AmountAndType from './AmountAndType/AmountAndType';
import { GeoJsonMapInput } from './GeoJsonMapInput';
import { WebAddressField } from './WebAddressField';
import { CurrencyField } from './CurrencyField';
import { AddressMapField } from './Address/AddressMap';
import { AddressBoxField } from './Address/AddressBox';
import Options from './Options';
import SensitiveData from './SensitiveData/SensitiveData';
import MagicStringGenerator from './MagicStringGenerator/MagicStringGenerator';
import ContactFinderWithAdvancedSearch from './ContactFinderWithAdvancedSearch';
import CountryFinder from './CountryFinder';
import { Select } from './Select';
import Wysiwyg from './../../../../../ui/App/Zaaksysteem/Wysiwyg';
import Switch from './Switch';

export const FormFields = {
  [RADIO_GROUP]: RadioGroup,
  [TEXT]: TextField,
  [NUMERIC]: TextField,
  [PHONE_NUMBER]: TextField,
  [MULTI_VALUE_TEXT]: MultiValueText,
  [SELECT]: Select,
  [UPLOAD]: Upload,
  [CASE_DOCUMENT_FINDER]: CaseDocumentFinder,
  [OBJECT_FINDER]: ObjectFinder,
  [OBJECT_TYPE_FINDER]: ObjectTypeFinder,
  [RELATIONSHIP]: Relationship,
  [FLATVALUE_SELECT]: FlatValueSelect,
  [CHECKBOX]: Checkbox,
  [CHECKBOX_GROUP]: CheckboxGroup,
  [EMAIL]: TextField,
  [IBAN]: TextField,
  [WEB_ADDRESS]: WebAddressField,
  [CURRENCY]: CurrencyField,
  [TEXTAREA]: Textarea,
  [CHOICE_CHIP]: ChoiceChips,
  [CASE_FINDER]: CaseFinder,
  [CASE_TYPE_FINDER]: CaseTypeFinder,
  [DATEPICKER]: DatePicker,
  [CONTACT_FINDER]: ContactFinder,
  [DEPARTMENT_FINDER]: DepartmentFinder,
  [ROLE_FINDER]: RoleFinder,
  [GEOJSON_MAP_INPUT]: GeoJsonMapInput,
  [ADDRESS_MAP]: AddressMapField,
  [ADDRESS_BOX]: AddressBoxField,
  [OPTIONS]: Options,
  [SENSITIVE_DATA]: SensitiveData,
  [MAGIC_STRING_GENERATOR]: MagicStringGenerator,
  [CONTACT_FINDER_WITH_ADVANCED_SEARCH]: ContactFinderWithAdvancedSearch,
  [COUNTRY_FINDER]: CountryFinder,
  [WYSISWYG]: Wysiwyg,
  [AMOUNT_AND_TYPE]: AmountAndType,
  [SWITCH]: Switch,
};
