// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useRef, useEffect, useState, useMemo } from 'react';
import { useFormik, FormikProps, FormikValues } from 'formik';
import { useDebouncedCallback } from 'use-debounce';
import { FormValuesType, UseFormType } from '../types/formDefinition.types';
import useValidation from '../validation/hooks/useValidation';
import getValuesFromDefinition from '../library/getValuesFromFormDefinition';
import useRuleEngine from '../rules/hooks/useRuleEngine';
import { translateFormDefinition } from '../library/translateFormDefinition';
import { useFormFields, UseFormFieldsReturnType } from './useFormFields';

export type UseFormReturnType<FormShape> = {
  fields: UseFormFieldsReturnType<FormShape>;
  formik: FormikProps<FormValuesType<FormShape>>;
};

export function useForm<FormShape extends FormikValues>({
  formDefinition,
  onSubmit,
  onChange,
  t,
  enableReinitialize = false,
  isInitialValid = false,
  validationMap = {},
  fieldComponents = {},
  rules = [],
  initialValues,
  initialTouched = {},
}: UseFormType<FormShape>): UseFormReturnType<FormShape> {
  const translatedFormDefinition = useMemo(
    () => (t ? translateFormDefinition(formDefinition, t) : formDefinition),
    []
  );

  const [formDefinitionFromState, setFormDefinition] = useState(
    translatedFormDefinition
  );
  const initialValuesMemo = useMemo(
    () => getValuesFromDefinition<FormShape>(formDefinitionFromState),
    []
  );

  const { schema, validate, registerValidation } = useValidation<FormShape>({
    formDefinition: formDefinitionFromState,
    validationMap: validationMap || {},
  });

  const initialValuesOnlyFromFormDefinition =
    initialValues &&
    Object.fromEntries(
      formDefinition.map(field => [field.name, initialValues[field.name]])
    );

  const formik = useFormik<FormShape>({
    validate,
    onSubmit: (values, helpers) => {
      onSubmit && onSubmit(values, helpers);
    },
    enableReinitialize,
    //@ts-ignore
    initialValues: initialValuesOnlyFromFormDefinition || initialValuesMemo,
    validateOnMount: true,
    initialTouched,
  });

  const [debouncedValidate] = useDebouncedCallback(
    () => formik.validateForm(),
    10
  );

  const [updatedFormDefinition, updatedValues] = useRuleEngine<FormShape>({
    rules,
    setFormDefinition,
    formDefinition: formDefinitionFromState,
    values: formik.values,
    setValues: formik.setValues,
    setFieldValue: formik.setFieldValue,
  });

  const fields = useFormFields<FormShape>({
    formik,
    registerValidation,
    validateForm: debouncedValidate,
    formDefinitionFields: updatedFormDefinition,
    values: updatedValues,
    customFields: fieldComponents,
  });

  const valueRef = useRef(updatedValues);

  const triggerOnChange = () => {
    if (valueRef.current !== updatedValues && onChange) {
      onChange(updatedValues);
    }

    valueRef.current = updatedValues;
    formik.validateForm();
  };

  const validateFormAfterUpdate = () => {
    debouncedValidate();
  };

  useEffect(triggerOnChange, [updatedValues]);
  useEffect(validateFormAfterUpdate, [schema]);

  return { fields, formik };
}
