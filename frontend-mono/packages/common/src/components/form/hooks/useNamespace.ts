// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { WithNamespaceType } from '../types';

export function useNamespace<FormShape = any>(namespace?: string) {
  const withNameSpace: WithNamespaceType<FormShape> = value =>
    namespace ? (`${namespace}.${value}` as keyof FormShape & string) : value;

  return { withNameSpace };
}
