// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useRef, useEffect, useState, useMemo } from 'react';
import {
  FormikTouched,
  FormikErrors,
  FormikProps,
  useFormik,
  FormikValues,
} from 'formik';
import { useDebouncedCallback } from 'use-debounce';
import {
  FormDefinition,
  AnyFormDefinitionField,
  StepsDefinition,
  UseSteppedFormType,
} from '../types/formDefinition.types';
import { FormValuesType } from '../types/formDefinition.types';
import useValidation from '../validation/hooks/useValidation';
import getValuesFromDefinition from '../library/getValuesFromFormDefinition';
import useRuleEngine from '../rules/hooks/useRuleEngine';
import { translateFormDefinition } from '../library/translateFormDefinition';
import { useFormFields, UseFormFieldsReturnType } from './useFormFields';
import { useNamespace } from './useNamespace';

export type UseSteppedFormReturnType<FormShape> = {
  fields: UseFormFieldsReturnType<FormShape>;
  formik: FormikProps<FormValuesType<FormShape>>;
} & Omit<UseFormStepsReturnType<FormShape>, 'activeFields'>;

export function useSteppedForm<FormShape extends FormikValues>({
  formDefinition,
  stepsDefinition,
  onSubmit,
  onChange,
  t,
  enableReinitialize = false,
  isInitialValid = false,
  validationMap = {},
  fieldComponents = {},
  rules = [],
}: UseSteppedFormType<FormShape>): UseSteppedFormReturnType<FormShape> {
  const translatedFormDefinition = useMemo(
    () => (t ? translateFormDefinition(formDefinition, t) : formDefinition),
    []
  );

  const [formDefinitionFromState, setFormDefinition] = useState(
    translatedFormDefinition
  );
  const initialValues = useMemo(
    () => getValuesFromDefinition<FormShape>(formDefinitionFromState),
    []
  );

  const { schema, validate, registerValidation } = useValidation<FormShape>({
    formDefinition: formDefinitionFromState,
    validationMap: validationMap || {},
  });

  const formik = useFormik<FormShape>({
    validate,
    onSubmit: onSubmit || (() => {}),
    enableReinitialize,
    initialValues,
    validateOnMount: !isInitialValid,
  });

  const [debouncedValidate] = useDebouncedCallback(
    () => formik.validateForm(),
    10
  );

  const [updatedFormDefinition, updatedValues] = useRuleEngine<FormShape>({
    rules,
    setFormDefinition,
    formDefinition: formDefinitionFromState,
    values: formik.values,
    setValues: formik.setValues,
    setFieldValue: formik.setFieldValue,
  });

  const { activeFields, ...restFormSteps } = useFormSteps<FormShape>({
    formDefinition: updatedFormDefinition,
    stepsDefinition,
    errors: formik.errors,
    touched: formik.touched,
  });

  const fields = useFormFields<FormShape>({
    formik,
    registerValidation,
    validateForm: debouncedValidate,
    formDefinitionFields: activeFields,
    values: updatedValues,
    customFields: fieldComponents,
  });

  const valueRef = useRef(updatedValues);

  const triggerOnChange = () => {
    if (valueRef.current !== updatedValues && onChange) {
      onChange(updatedValues);
    }

    valueRef.current = updatedValues;

    formik.validateForm();
  };

  const validateFormAfterUpdate = () => {
    debouncedValidate();
  };

  useEffect(triggerOnChange, [updatedValues]);
  useEffect(validateFormAfterUpdate, [schema]);

  return { fields, formik, ...restFormSteps };
}

export type FormStepType = {
  title: string;
  description?: string;
  isValid: boolean;
  isActive: boolean;
  isTouched: boolean;
};

export type UseFormStepsReturnType<FormShape> = {
  activeFields: AnyFormDefinitionField<FormShape>[];
  activeStepNumber: number;
  activeStep: FormStepType;
  steps: FormStepType[];
  hasNextStep: boolean;
  hasPreviousStep: boolean;
  handleNextStep: () => void;
  handlePreviousStep: () => void;
};

type UseFormStepsType<FormShape> = {
  formDefinition: FormDefinition<FormShape>;
  errors: FormikErrors<FormShape>;
  touched: FormikTouched<FormShape>;
  namespace?: string;
  stepsDefinition: StepsDefinition;
};

export function useFormSteps<FormShape>({
  formDefinition,
  stepsDefinition,
  errors,
  touched,
  namespace,
}: UseFormStepsType<FormShape>): UseFormStepsReturnType<FormShape> {
  const { withNameSpace } = useNamespace<FormShape>(namespace);
  const [activeStepNumber, setActiveStepNumber] = useState(1);
  const activeStepIndex = activeStepNumber - 1;

  const steppedDefinition = stepsDefinition.map(step => ({
    ...step,
    fields: step.fieldNames.map(fieldName => {
      const field = formDefinition.find(
        field => field.name === fieldName
      ) as AnyFormDefinitionField<any>;
      !field && console.warn(fieldName, 'Not found in the stepped form');

      return { ...field, name: field && withNameSpace(field.name as any) };
    }),
  }));

  const activeSteps = steppedDefinition.filter(
    ({ fields }) => fields.filter(field => !field.hidden).length > 0
  );

  const numSteps = activeSteps.length;

  const hasNextStep = activeStepNumber + 1 <= numSteps;
  const hasPreviousStep = activeStepNumber - 1 > 0;

  const handleNextStep = () => {
    if (hasNextStep) {
      setActiveStepNumber(activeStepNumber + 1);
    }
  };

  const handlePreviousStep = () => {
    if (hasPreviousStep) {
      setActiveStepNumber(activeStepNumber - 1);
    }
  };

  const steps = useMemo(
    () =>
      steppedDefinition.map<FormStepType>(
        ({ title, description, fields }, index) => {
          const isActive = index === activeStepIndex;
          const isValid = fields.every(field =>
            Object.keys(errors).every(
              key => key.startsWith(field.name) === false
            )
          );
          const isTouched = fields
            .filter(field => field.required)
            .every(field => typeof touched[field.name] !== 'undefined');

          return {
            title,
            description,
            isValid,
            isActive,
            isTouched,
          };
        }
      ),
    [formDefinition, errors, touched, activeStepIndex]
  );

  const activeStep = steps[activeStepIndex];

  return {
    activeFields: steppedDefinition[activeStepIndex].fields,
    activeStepNumber,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    activeStep,
  };
}
