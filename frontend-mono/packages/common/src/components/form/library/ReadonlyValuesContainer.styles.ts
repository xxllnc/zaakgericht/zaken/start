// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const ReadonlyLinkContainerStyleSheet = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    container: {
      display: 'flex',
      color: greyscale.black,
      textDecoration: 'none',
      '&:focus': {
        outline: '0',
      },
      '& + $container ': {
        marginTop: 8,
      },
    },
    list: {
      listStyleType: 'none',
      padding: '8px 12px',
      margin: 0,
    },
  })
);

export const iconStyleSheet = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      marginLeft: 10,
      padding: 2,
      marginTop: '-2px',
      background: greyscale.light,
      boxSizing: 'content-box',
    },
    root: {
      fill: greyscale.darker,
    },
  })
);
