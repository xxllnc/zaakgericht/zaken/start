// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormikProps, FormikErrors } from 'formik';
import { RegisterValidationForNamespaceType } from './generic.types';

export type FormFieldPropsType<
  FormShape = any,
  ValueType = any,
  Config = any,
> = {
  value: ValueType | null;
  checked: boolean;
  error?: string;
  touched: boolean;
  readOnly?: boolean;
  dateFormat?: string;
  submit?: () => void;
  validateForm: () => void;
  registerValidation: RegisterValidationForNamespaceType;
  formik: FormikProps<FormShape>;
  type: string;
  name: keyof FormShape & string;
  hidden?: boolean;
  required?: boolean;
  disabled?: boolean;
  placeholder?: string;
  createOnBlur?: boolean;
  uploadDialog?: boolean;
  accept?: string[];
  label?: string;
  config: Config;
  multiValue?: boolean;
  filter: { [key: string]: string };
  choices?: any[];
  applyBackgroundColor?: boolean;
  onClose?: () => void;
  getError?: (
    errors: FormikErrors<FormShape>,
    name: keyof FormShape & string
  ) => string | undefined;
  onChange: (event: {
    target: {
      name: string;
      value: ValueType | null;
    };
  }) => void;
  onBlur: (event: React.SyntheticEvent) => void;
  [key: string]: any;
} & Pick<
  FormikProps<FormShape>,
  'setFieldValue' | 'setFieldTouched' | 'registerField'
>;

type FlatFormValue = string | number | boolean | GeoJSON.GeoJsonObject | object;

type NestedFormValue = {
  value: FlatFormValue | object;
  label: string | React.ReactNode;
};

export type FormValueType =
  | FlatFormValue
  | NestedFormValue
  | FlatFormValue[]
  | NestedFormValue[]
  | null;

export type FormFieldComponentType<
  Value extends FormValueType,
  Config extends {} = {},
> = React.FunctionComponent<FormFieldPropsType<any, Value, Config>>;
