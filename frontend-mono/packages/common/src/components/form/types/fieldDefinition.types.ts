// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { GenericFormDefinitionFieldAttributes } from './generic.types';

export interface FormDefinitionField<
  FormShape = any,
  Config = any,
  ValueType = unknown,
> extends GenericFormDefinitionFieldAttributes<FormShape, Config, ValueType> {
  minItems?: number;
  maxItems?: number;
}

export interface FormDefinitionNumberField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format: 'number';
  value: unknown;
  min?: number;
  max?: number;
  lessThan?: number;
  moreThan?: number;
}

export interface FormDefinitionTextField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format: 'text';
  value: unknown;
  min?: number;
  max?: number;
}

export interface FormDefinitionMixedField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format: 'mixed' | 'object';
}

export interface FormDefinitionEmailField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format: 'email';
  value: unknown;
  allowMagicString?: boolean;
}

export interface FormDefinitionAmountAndTypeField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format: 'amountAndtype';
  value: unknown;
}

export interface FormDefinitionStringField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format?: '';
}

export interface FormDefinitionFileField<FormShape = any>
  extends FormDefinitionField<FormShape> {
  format: 'file';
  accept?: string[];
}

export type AnyFormDefinitionField<FormShape = any> =
  | FormDefinitionStringField<FormShape>
  | FormDefinitionNumberField<FormShape>
  | FormDefinitionTextField<FormShape>
  | FormDefinitionEmailField<FormShape>
  | FormDefinitionMixedField<FormShape>
  | FormDefinitionFileField<FormShape>;
