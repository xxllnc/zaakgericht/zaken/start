// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { asArray } from '@mintlab/kitchen-sink/source';
import {
  FormValue,
  FlatFormValue,
  FormDefinition,
} from '../../types/formDefinition.types';
import { ValidationMap } from '../createValidation';
import getFieldByName from '../../library/getFieldByName';

/**
 * Flattens a object of values, so it can be handled
 * by the default schemas.
 */

export function flattenValues<FormShape = any>(
  values: FormShape,
  formDefinition: FormDefinition<FormShape>,
  validationMap?: ValidationMap
): { [key: string]: FlatFormValue | FlatFormValue[] } {
  //@ts-ignore
  return Object.entries(values).reduce((acc, [key, value]) => {
    const field = getFieldByName<FormShape>(
      formDefinition,
      key as keyof FormShape
    );
    const { format, multiValue } = field;

    const hasCustomValidation = validationMap
      ? Boolean(validationMap[field.name])
      : false;

    const transformed = asArray(value).map(thisValue =>
      transformFormValueToFlatValue(thisValue, format, hasCustomValidation)
    );

    return {
      ...acc,
      [key]: multiValue ? transformed : transformed[0],
    };
  }, {});
}

/* eslint complexity: [2, 7] */
function transformFormValueToFlatValue(
  value: FormValue | null,
  format: string | undefined,
  hasCustomValidation: boolean
): FlatFormValue | any {
  if (hasCustomValidation || format === 'mixed' || format === 'file')
    return value;

  if (
    value &&
    typeof value === 'object' &&
    Object.prototype.hasOwnProperty.call(value, 'value')
  )
    return value.value;

  return value;
}
