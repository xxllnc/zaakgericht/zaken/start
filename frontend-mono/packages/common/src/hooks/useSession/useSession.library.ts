// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormatSessiontype } from './useSession.types';

export const formatSession: FormatSessiontype = response => {
  let formattedData = response.result.instance;

  // @ts-ignore
  formattedData.configurable.signature_upload_role =
    formattedData.configurable.signature_upload_role?.toLowerCase();

  return formattedData;
};
