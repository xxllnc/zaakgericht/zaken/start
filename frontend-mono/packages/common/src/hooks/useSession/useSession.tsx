// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { formatSession } from './useSession.library';
import { SessionResponseBodyType, SessionType } from './useSession.types';

type UseSessionType = () => SessionType;

// @ts-ignore
const useSession: UseSessionType = () => {
  const { data } = useQuery(['session'], async () => {
    const url = '/api/v1/session/current';
    const response = await request<SessionResponseBodyType>('GET', url);
    const session = formatSession(response);

    return session;
  });

  return data;
};

export default useSession;
