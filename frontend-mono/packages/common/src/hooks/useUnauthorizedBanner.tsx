// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';
import useSession, {
  SessionType,
} from '@zaaksysteem/common/src/hooks/useSession';

type DetermineAuthorizationType = (session: SessionType) => boolean | undefined;

type UseUnauthorizedBannerType = (
  determineAuthorization: DetermineAuthorizationType
) => JSX.Element | null;

const useUnauthorizedBanner: UseUnauthorizedBannerType =
  determineAuthorization => {
    const [t] = useTranslation('common');
    const session = useSession();

    const onClose = () => {
      window.location.href = '/';
    };

    if (!determineAuthorization(session)) {
      return (
        <Alert
          open={true}
          onClose={onClose}
          title={t('noAccess')}
          primaryButton={{ text: t('dialog.ok'), action: onClose }}
        >
          {t('serverErrors.status.401')}
        </Alert>
      );
    } else {
      return null;
    }
  };

export default useUnauthorizedBanner;
