// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fecha from 'fecha';
import { asArray } from '@mintlab/kitchen-sink/source';
import { addResourceBundle } from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from '../locale/common.locale';

function initFetcha(i18n: i18next.i18n) {
  fecha.default.i18n = {
    ...fecha.default.i18n,
    ...i18n.t('common:dates', { returnObjects: true }),
  };
}

export async function initI18n(options: i18next.InitOptions, use: any[]) {
  if (use) {
    asArray(use).map(item => i18next.default.use(item));
  }

  await i18next.default.init(options);

  addResourceBundle(i18next.default, 'common', locale);
  initFetcha(i18next.default);

  return i18next.default;
}
