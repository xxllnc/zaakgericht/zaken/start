// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';

type FormatCurrencyType = (t: i18next.TFunction, value?: number) => string;

export const formatCurrency: FormatCurrencyType = (t, value) =>
  value
    ? new Intl.NumberFormat(t('common:locale'), {
        style: 'currency',
        currency: t('common:currencyCode'),
      }).format(value)
    : '';
