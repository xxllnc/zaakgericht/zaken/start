// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { translateKeyGlobally } from '../translateKeyGlobally';

type TranslatedErrorMessagePropsType = {
  errorObj: V2ServerErrorsType;
};

/* eslint complexity: [2, 12] */
export const TranslatedErrorMessage: React.ComponentType<
  TranslatedErrorMessagePropsType
> = ({ errorObj }) => {
  const {
    errors,
    response: { url, status },
  } = errorObj;

  const [t] = useTranslation();
  const translateCode = (code: string) => translateKeyGlobally(t, code);

  const statusOrDefault = (
    <Fragment>
      {translateCode(`serverErrors.status.${status}`) ||
        translateCode('dialog.error.body')}
    </Fragment>
  );

  if (/api\/v2\//.test(url) || errors?.length) {
    if (!errors || !errors.length) return statusOrDefault;
    const { code, detail, title, source } = errors[0];
    const details = [detail, title].filter(entry => Boolean(entry)).join('. ');
    const sources = source
      ? Array.isArray(source)
        ? source.join(', ')
        : source
      : null;
    const message = translateCode(`serverErrors.${code}`) || statusOrDefault;

    return (
      <Fragment>
        {message}
        {details ? (
          <Fragment>
            <h4>{t('Common:dialog.error.details')}:</h4>
            <p>
              {sources ? `${sources}: ` : null} {details}
            </p>
          </Fragment>
        ) : null}
      </Fragment>
    );
  } else {
    return statusOrDefault;
  }
};
