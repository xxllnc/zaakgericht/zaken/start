// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

interface Messages {
  [key: string]: string;
}

type AddMessages = (items: Messages) => void;
type RemoveMessages = (items: Messages) => void;

let messages: Messages = {
  'case_management/custom_object/object/not_found':
    'Object niet gevonden of onvoldoende rechten.',
  'case_management/custom_object/object_type/not_found':
    'Objecttype niet gevonden of onvoldoende rechten.',
  'case_management/custom_object/object_create/unauthorised':
    'Object kan niet worden gemaakt vanwege onvoldoende rechten.',
};

const addMessages: AddMessages = items => {
  messages = {
    ...messages,
    ...items,
  };
};

const removeMessages: RemoveMessages = items => {
  const removeKeys = Object.keys(items);

  messages = Object.entries(messages)
    .filter(([key]) => removeKeys.includes(key))
    .reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key]: value,
      }),
      {}
    );
};

export default function useMessages(): [Messages, AddMessages, RemoveMessages] {
  return [messages, addMessages, removeMessages];
}
