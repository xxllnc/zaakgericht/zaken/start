// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    subcategories: {
      contact: 'Contact',
      location: 'Lokatie',
      case: 'Zaak',
      caseType: 'Zaaktype',
      caseLocation: 'Zaaklocatie',
      result: 'Resultaat',
      requestor: 'Aanvrager',
      object: 'Object',
    },
    relationships: {
      coordinator: 'Coördinator',
      assignee: 'Behandelaar',
      requestor: 'Aanvrager',
      object: 'Object',
    },
    id: 'Identificatienummer',
    objectUUID: 'Object UUID',
    identification: 'Identificatie',
    title: 'Titel',
    subtitle: 'Subtitel',
    externalReference: 'Externe referentie',
    dateCreated: 'Datum aangemaakt',
    lastModified: 'Laatst gewijzigd',
    email: 'E-mailadres',
    phoneNr: 'Telefoonnummer',
    name: 'Naam',
    summary: 'Samenvatting',
    mobile_number: 'Mobiel',
    phone_number: 'Telefoonnummer',
    gender: 'Geslacht',
    dateOfBirth: 'Geboortedatum',
    dateOfDeath: 'Overlijdensdatum',
    familyName: 'Achternaam',
    firstName: 'Voornaam',
    initials: 'Initialen',
    insertions: 'Tussenvoegsel',
    country: 'Land',
    city: 'Plaats',
    placeOfResidence: 'Woonplaats',
    street: 'Straatnaam',
    streetNumber: 'Huisnummer',
    streetNumberLetter: 'Huisletter',
    streetNumberSuffix: 'Huisnummertoevoeging',
    zipcode: 'Postcode',
    organizationType: 'Organisatietype',
    cocNumber: 'KVK-nummer',
    cocLocationNumber: 'KVK-lokatienummer',
    correspondenceAddress: 'Correspondentieadres',
    dateFounded: 'Datum opgericht',
    dateRegistered: 'Datum geregistreerd',
    dateCeased: 'Datum opgeheven',
    source: 'Bron',
    rsin: 'RSIN',
    caseNumber: 'Zaaknummer',
    completionDate: 'Afhandeldatum',
    registrationDate: 'Registratiedatum',
    destructionDate: 'Vernietigingsdatum',
    caseType: 'Zaaktype',
    status: 'Status',
    days: 'Dagen',
    progress: 'Voortgang',
    version: 'Versie',
    description: 'Omschrijving',
    trigger: 'Trigger',
    isPublic: 'Publiek',
    tags: 'Trefwoorden',
    initiatorType: 'Handelingsinitiator',
    payment: 'Tarief',
    leadTimeLegal: 'Doorlooptijd (wettelijk)',
    leadTimeService: 'Doorlooptijd (service)',
    active: 'Actief',
    legalBasis: 'Wettelijke grondslag',
    processDescription: 'Procesbeschrijving',
    mayPostpone: 'Opschorten mogelijk',
    mayExtend: 'Verlenging mogelijk',
    extensionPeriod: 'Verlengingstermijn in dagen',
    adjournPeriod: 'Verjaringstermijn in dagen',
    eWebform: 'E-Formulier',
    motivation: 'Motivatie',
    penaltyLaw: 'Wet dwangsom',
    purpose: 'Doel',
    archiveClassificationCode: 'Archief classificatiecode',
    designationOfConfidentiality: 'Vertrouwelijkheidsaanduiding',
    responsibleSubject: 'Verantwoordelijke',
    responsibleRelationship: 'Verantwoordingsrelatie',
    possibilityForObjectionAndAppeal: 'Beroep mogelijk',
    publication: 'Publicatie',
    publicationText: 'Publicatietekst',
    bag: 'Basisregistraties Adressen en Gebouwen',
    lexSilencioPositivo: 'Lex Silencio Positivo',
    WKPB: 'WKPB',
    localBasis: 'Lokale grondslag',
    processTypeDescription: 'Procestype-omschrijving',
    processTypeExplanation: 'Procestype-toelichting',
    processTypeGeneric: 'Procestype-generiek',
    processTypeName: 'Procestype-naam',
    processTypeNumber: 'Procestype-nummer',
    processTypeObject: 'Procestype-object',
    processTerm: 'Procestermijn',
    result: 'Resultaat',
    resultExplanation: 'Toelichting',
    selectionList: 'Selectielijst',
    selectionListNumber: 'Selectielijst-nummer',
    selectionListStart: 'Selectielijst brondatum',
    selectionListEnd: 'Selectielijst einddatum',
    triggerArchival: 'Activeer bewaartermijn',
    retentionPeriod: 'Bewaartermijn',
    retentionPeriodSourceDate: 'Brondatum archiefprocedure',
    standardChoice: 'Standaardkeuze',
    typeOfArchiving: 'Archiefnominatie',
    origin: 'Herkomst',
    fullAddress: 'Volledig adres',
    coordinates: 'Coördinaten',
    targetDate: 'Streefafhandeldatum',
    stalledUntil: 'Opgeschort tot',
    stalledSince: 'Opgeschort sinds',
    parentNumber: 'Hoofdzaak nummer',
    customNumber: 'Alternatief zaaknummer',
    phase: 'Fase',
    subject: 'Onderwerp',
    published: 'Gepubliceerd',
    department: 'Afdeling',
    roles: 'Rollen',
    extraInformation: 'Extra informatie',
    extraInformationExternal: 'Extra informatie extern',
    contactChannel: 'Contactkanaal',
  },
};
