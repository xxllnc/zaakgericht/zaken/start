// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { HelpDialogSignalType } from '@mintlab/ui/App/Zaaksysteem/HelpDialog';
import { signal } from '@preact/signals-react';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

// SNACKBAR

type SnackbarProps = {
  message: string;
  link?: string;
  sx?: any;
  download?: boolean;
  autoHideDuration?: number | null;
  interpolations?: Object;
};

export const snackbarMargin = { marginBottom: '45px' };

export const snackbarMessage = signal<SnackbarProps>({
  message: '',
});

export const openSnackbar = (snackbarProps: SnackbarProps | string) => {
  snackbarMessage.value =
    typeof snackbarProps === 'string'
      ? { message: snackbarProps }
      : snackbarProps;
};

// SERVER ERROR

export const serverError = signal<V2ServerErrorsType | string | null>(null);

export const openServerError = (err: V2ServerErrorsType | string) => {
  serverError.value = err;
};

// HELP DIALOG

const defaultHelpDialogState: HelpDialogSignalType = {
  title: '',
  content: '',
};

export const helpDialogData = signal<HelpDialogSignalType>(
  defaultHelpDialogState
);

export const openHelpDialog = (data: HelpDialogSignalType) => {
  helpDialogData.value = data;
};

export const closeHelpDialog = () => {
  helpDialogData.value = defaultHelpDialogState;
};

export const helpMode = signal<boolean>(false);

export const setHelpMode = (mode: boolean) => (helpMode.value = mode);
