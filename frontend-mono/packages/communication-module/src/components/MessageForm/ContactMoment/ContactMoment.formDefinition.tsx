// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CONTACT_FINDER, CASE_FINDER } from '../../../Communication.constants';
import * as CHANNELS from '../../../Communication.constants';
import { CommunicationContextType } from '../../../types/Context.types';
import { ContactMomentRequestBody } from '../MessageForm.types';

type SaveContactMomentFormValuesType = Pick<
  ContactMomentRequestBody,
  'content' | 'channel' | 'direction' | 'case_uuid'
> & { contact_uuid: { value: string } };

type GetContactMomentFormDefinitionType = (
  context: CommunicationContextType
) => FormDefinition<SaveContactMomentFormValuesType>;

export const getContactMomentFormDefinition: GetContactMomentFormDefinitionType =
  context => {
    const {
      contactName,
      contactUuid,
      caseUuid,
      capabilities: { canSelectContact, canSelectCase },
    } = context;

    const defaultValue = {
      label: contactName,
      value: contactUuid,
    };

    return [
      {
        name: 'contact_uuid',
        type: CONTACT_FINDER,
        hidden: canSelectContact === false,
        required: true,
        placeholder: 'addFields.contact',
        label: 'addFields.contact',
        ...(canSelectContact && contactName && contactUuid
          ? {
              choices: [defaultValue],
              value: defaultValue,
            }
          : {
              value: contactUuid ? { value: contactUuid } : null,
            }),
      },
      {
        name: 'case_uuid',
        type: CASE_FINDER,
        hidden: canSelectCase === false,
        value: caseUuid ? caseUuid : null,
        required: false,
        placeholder: 'addFields.case',
        label: 'addFields.case',
      },
      {
        name: 'channel',
        type: fieldTypes.CHOICE_CHIP,
        value: 'assignee',
        required: true,
        label: 'addFields.channel',
        placeholder: '',
        choices: [
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_ASSIGNEE}`,
            value: 'assignee',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_ASSIGNEE}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_MAIL}`,
            value: 'mail',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_MAIL}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_PHONE}`,
            value: 'phone',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_PHONE}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_EMAIL}`,
            value: 'email',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_EMAIL}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_POSTEX}`,
            value: 'postex',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_POSTEX}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_WEBFORM}`,
            value: 'webform',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_WEBFORM}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_SOCIALMEDIA}`,
            value: 'social_media',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_SOCIALMEDIA}`}</ZsIcon>
              );
            },
          },
          {
            label: `channels.${CHANNELS.TYPE_CHANNEL_FRONTDESK}`,
            value: 'frontdesk',
            renderIcon() {
              return (
                <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_FRONTDESK}`}</ZsIcon>
              );
            },
          },
        ],
      },
      {
        name: 'direction',
        type: fieldTypes.CHOICE_CHIP,
        value: 'incoming',
        required: true,
        label: 'addFields.direction',
        placeholder: 'addFields.direction',
        choices: [
          {
            label: 'direction.incoming',
            value: 'incoming',
          },
          {
            label: 'direction.outgoing',
            value: 'outgoing',
          },
        ],
      },
      {
        name: 'content',
        label: 'addFields.content',
        type: fieldTypes.TEXT,
        multi: true,
        value: '',
        required: true,
        placeholder: 'addFields.content',
        isMultiline: true,
        rows: 7,
      },
    ];
  };
