// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  Rule,
  valueEquals,
  showFields,
  hideFields,
  transferDataAsConfig,
  updateFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { mergeErrors } from '@zaaksysteem/common/src/components/form/library/mergeErrors';
import { CASE_SELECTOR } from '../../../Communication.constants';
import { EmailTemplateType } from '../../../types/Context.types';
import { SubjectRelationType } from '../../../types/SubjectRelations';
import { EmailIntegrationType } from '../MessageForm.types';
import {
  getSenderTypesChoices,
  getRecipientTypeChoices,
  getRequestorValue,
  getCoordinatorValue,
  getRoleChoices,
  getAuthorizedChoices,
} from './Email.library';

export type SaveEmailValuesType = {
  emailTemplate: NestedFormValue;
  thread_uuid?: string;
  case_uuid: string;
  sender_type: string;
  recipient_type: string;
  requestor: NestedFormValue[];
  colleague: NestedFormValue[];
  coordinator: NestedFormValue[];
  role: NestedFormValue[];
  authorized: NestedFormValue[];
  other: NestedFormValue[];
  htmlEmailTemplateDescription: string;
  cc: NestedFormValue[];
  bcc: NestedFormValue[];
  subject: string;
  content: string;
  attachments: NestedFormValue | NestedFormValue[];
};

type GetEmailFormDefinitionType = (
  t: i18next.TFunction,
  caseUuid?: string,
  contactUuid?: string,
  htmlEmailTemplateDescription?: string,
  emailTemplates?: EmailTemplateType[],
  subjectRelations?: SubjectRelationType[],
  emailIntegration?: EmailIntegrationType
) => FormDefinition<SaveEmailValuesType>;

/* eslint complexity: [2, 100] */
export const getEmailFormDefinition: GetEmailFormDefinitionType = (
  t,
  caseUuid,
  contactUuid,
  htmlEmailTemplateDescription,
  emailTemplates,
  subjectRelations,
  emailIntegration
) => {
  const defaultTemplateChoice = { label: t('common:none'), value: '0' };
  const senderTypes = getSenderTypesChoices(emailIntegration);
  const recipientTypes = getRecipientTypeChoices();
  const requestor = getRequestorValue(t, subjectRelations);
  const coordinator = getCoordinatorValue(t, subjectRelations);
  const roleChoices = getRoleChoices(subjectRelations);
  const authorizedChoices = getAuthorizedChoices(subjectRelations);

  const defaultDocumentsParams =
    window.top?.location.href.split('documents=')[1];
  const defaultDocuments = defaultDocumentsParams
    ? defaultDocumentsParams.split(',')
    : undefined;

  return [
    ...(emailTemplates?.length
      ? [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'emailTemplate',
            type: fieldTypes.SELECT,
            value: defaultTemplateChoice,
            required: true,
            label: 'addFields.emailTemplate',
            showLabel: true,
            isClearable: false,
            choices: [
              defaultTemplateChoice,
              ...emailTemplates.map(emailTemplate => ({
                ...emailTemplate,
                value: `${emailTemplate.id}`,
              })),
            ],
          },
        ]
      : []),
    ...(contactUuid && !caseUuid
      ? [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'case_uuid',
            type: CASE_SELECTOR,
            value: '',
            required: true,
            placeholder: 'addFields.selectCase',
            label: 'addFields.selectCase',
            isSearchable: false,
            config: {
              contactUuid,
            },
          },
        ]
      : [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'case_uuid',
            value: caseUuid,
            type: fieldTypes.TEXT,
            hidden: true,
          },
        ]),
    ...(emailTemplates?.length
      ? [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'sender_type',
            type: fieldTypes.SELECT,
            value: senderTypes[0],
            isClearable: false,
            showLabel: true,
            label: 'addFields.senderType',
            choices: senderTypes,
            applyBackgroundColor: true,
          },
        ]
      : []),
    {
      name: 'recipient_type',
      type: fieldTypes.SELECT,
      value: recipientTypes[0],
      isClearable: false,
      showLabel: true,
      hideBorder: true,
      label: 'addFields.recipientType',
      choices: recipientTypes,
      applyBackgroundColor: true,
    },
    {
      name: 'requestor',
      type: fieldTypes.SELECT,
      value: [requestor],
      applyBackgroundColor: true,
      label: 'addFields.recipientTypes.requestor',
      disabled: true,
    },
    {
      name: 'coordinator',
      type: fieldTypes.SELECT,
      value: [coordinator],
      applyBackgroundColor: true,
      label: 'addFields.recipientTypes.coordinator',
      disabled: true,
    },
    {
      name: 'colleague',
      type: fieldTypes.CONTACT_FINDER,
      value: [],
      required: true,
      multiValue: true,
      applyBackgroundColor: true,
      label: 'addFields.recipientTypes.colleague',
      placeholder: 'addFields.colleague',
      config: {
        subjectTypes: ['employee'],
        itemResolver: (contact: any, item: any) => {
          const {
            attributes: { name, email_address },
            id,
          } = contact;

          return email_address
            ? {
                ...item,
                label: name,
                subLabel: email_address,
                value: email_address,
                id,
              }
            : null;
        },
      },
    },
    {
      name: 'role',
      type: fieldTypes.SELECT,
      value: [],
      required: true,
      label: 'addFields.recipientTypes.role',
      placeholder: 'addFields.rolePlaceholder',
      applyBackgroundColor: true,
      isClearable: true,
      isMulti: true,
      choices: roleChoices,
    },
    {
      name: 'authorized',
      type: fieldTypes.SELECT,
      value: [],
      required: true,
      label: 'addFields.recipientTypes.authorized',
      placeholder: 'addFields.authorizedPlaceholder',
      applyBackgroundColor: true,
      isClearable: true,
      isMulti: true,
      choices: authorizedChoices,
    },
    {
      name: 'other',
      type: fieldTypes.MULTI_VALUE_TEXT,
      format: 'email',
      allowMagicString: true,
      value: [],
      multiValue: true,
      label: 'addFields.recipientTypes.other',
      placeholder: 'addFields.to',
      getError: mergeErrors<SaveEmailValuesType>('other'),
    },
    {
      name: 'cc',
      type: fieldTypes.MULTI_VALUE_TEXT,
      format: 'email',
      allowMagicString: true,
      value: [],
      multiValue: true,
      label: 'addFields.cc',
      placeholder: 'addFields.cc',
      getError: mergeErrors<SaveEmailValuesType>('cc'),
    },
    {
      name: 'bcc',
      type: fieldTypes.MULTI_VALUE_TEXT,
      format: 'email',
      allowMagicString: true,
      value: [],
      multiValue: true,
      label: 'addFields.bcc',
      placeholder: 'addFields.bcc',
      getError: mergeErrors<SaveEmailValuesType>('bcc'),
    },
    ...(htmlEmailTemplateDescription
      ? [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'htmlEmailTemplateDescription',
            type: fieldTypes.TEXT,
            value: htmlEmailTemplateDescription,
            readOnly: true,
          },
        ]
      : []),
    {
      name: 'subject',
      type: fieldTypes.TEXT,
      value: '',
      placeholder: 'addFields.subject',
      label: 'addFields.subject',
    },
    {
      name: 'content',
      type: fieldTypes.TEXTAREA,
      multi: true,
      value: '',
      placeholder: 'addFields.message',
      label: 'addFields.message',
      isMultiline: true,
      rows: 7,
    },
    ...(caseUuid
      ? [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'attachments',
            type: fieldTypes.CASE_DOCUMENT_FINDER,
            value: null,
            required: false,
            format: 'file',
            uploadDialog: false,
            multiValue: true,
            label: 'addFields.attachments',
            placeholder: 'addFields.attachmentsPlaceholder',
            applyBackgroundColor: true,
            config: { caseUuid, defaultDocuments },
          },
        ]
      : [
          <FormDefinitionField<SaveEmailValuesType>>{
            name: 'attachments',
            type: fieldTypes.UPLOAD,
            value: null,
            required: false,
            format: 'file',
            uploadDialog: true,
            multiValue: true,
          },
        ]),
  ];
};

const recipientFieldRules = [
  'requestor',
  'coordinator',
  'colleague',
  'role',
  'authorized',
  'other',
].map(field =>
  new Rule()
    .when('recipient_type', valueEquals(field))
    .then(showFields([field]))
    .else(hideFields([field]))
);

export const getEmailRules = (
  prevRef: any,
  subjectRelations?: SubjectRelationType[],
  emailIntegration?: EmailIntegrationType
) => [
  ...recipientFieldRules,
  new Rule()
    .when(() => true)
    .then(transferDataAsConfig('case_uuid', 'to', 'caseUuid')),
  new Rule()
    .when(() => true)
    .then(fields => {
      const emailTemplate = (
        fields.find((field: any) => field.name === 'emailTemplate') as any
      )?.value;
      const recipientTypeChoices = fields.find(
        (field: any) => field.name === 'recipient_type'
      )?.choices as any;

      if (!emailTemplate) {
        return fields;
      }

      const previousRef = prevRef.current;
      const isFirstRender = previousRef === undefined;

      const emailTemplateChanged = prevRef.current !== emailTemplate.value;
      prevRef.current = emailTemplate.value;

      if (isFirstRender || !emailTemplateChanged) {
        return fields;
      } else if (emailTemplate.value === '0') {
        const senderTypeChoices = getSenderTypesChoices(emailIntegration);

        return fields
          .map(
            updateFields(['sender_type'], {
              choices: senderTypeChoices,
              value: senderTypeChoices[0],
            })
          )
          .map(
            updateFields(['recipient_type'], {
              value: recipientTypeChoices[0],
            })
          )
          .map(updateFields(['cc', 'bcc'], { value: [] }))
          .map(updateFields(['subject', 'content'], { value: '' }))
          .map(updateFields(['attachments'], { value: null }));
      } else {
        const {
          recipientType,
          assignee,
          to,
          cc,
          bcc,
          role,
          subject,
          body,
          attachments,
        } = emailTemplate;

        const recipientTypeValue = recipientTypeChoices.find(
          (choice: any) => choice.value === recipientType
        );

        const senderTypeChoices = getSenderTypesChoices(
          emailIntegration,
          emailTemplate
        );

        const roleValue =
          subjectRelations?.filter(
            subj =>
              (subject.value || subject.role === 'aanvrager') &&
              subj.role === role?.toLowerCase()
          ) || [];

        const authorizedValue =
          subjectRelations?.filter(
            subj =>
              (subject.value || subject.role === 'aanvrager') && subj.authorized
          ) || [];

        return fields
          .map(
            updateFields(['sender_type'], {
              choices: senderTypeChoices,
              value: senderTypeChoices[0],
            })
          )
          .map(
            updateFields(['recipient_type'], {
              value: recipientTypeValue,
            })
          )
          .map(
            updateFields(
              ['colleague'],
              recipientType === 'colleague' ? { value: assignee } : {}
            )
          )
          .map(
            updateFields(
              ['role'],
              recipientType === 'role' ? { value: roleValue } : {}
            )
          )
          .map(
            updateFields(
              ['authorized'],
              recipientType === 'authorized' ? { value: authorizedValue } : {}
            )
          )
          .map(
            updateFields(
              ['other'],
              recipientType === 'other' ? { value: to } : {}
            )
          )
          .map(updateFields(['subject'], { value: subject }))
          .map(updateFields(['content'], { value: body }))
          .map(updateFields(['cc'], { value: cc }))
          .map(updateFields(['bcc'], { value: bcc }))
          .map(updateFields(['attachments'], { value: attachments }));
      }
    }),
];
