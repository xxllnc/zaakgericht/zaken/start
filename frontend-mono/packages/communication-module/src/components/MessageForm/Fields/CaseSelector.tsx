// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ReadonlyValuesContainer } from '@zaaksysteem/common/src/components/form/library/ReadonlyValuesContainer';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { useCaseChoicesForContactQuery } from './CaseSelector.library';
export type CaseSelectorConfigType = {
  contactUuid: string;
};

export interface CaseSelectorPropsType
  extends FormFieldPropsType<unknown, CaseSelectorConfigType, string> {}

const CaseSelector: React.ComponentType<CaseSelectorPropsType> = ({
  config,
  ...restProps
}) => {
  const [t] = useTranslation('communication');
  const selectProps = useCaseChoicesForContactQuery(config?.contactUuid);

  if (!config || !config.contactUuid) {
    console.error(
      `The CaseSelector component cannot be used without config.contactUuid.`
    );
    return null;
  }

  if (restProps.readOnly) {
    return (
      <ReadonlyValuesContainer
        value={
          selectProps.choices.find(choice => choice.value === restProps.value)
            ?.label || ''
        }
      />
    );
  }

  return (
    <Select
      {...restProps}
      {...selectProps}
      filterOption={restProps.isSearchable ? undefined : () => true}
      placeholder={t('addFields.caseSelector.choose')}
    />
  );
};

export default CaseSelector;
