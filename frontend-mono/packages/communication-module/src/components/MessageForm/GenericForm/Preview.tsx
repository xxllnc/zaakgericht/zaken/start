// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect, useContext } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  FormRendererFormField,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CommunicationContext } from '../../../Communication.context';
import { getPreview } from '../MessageForm.library';
import {
  HtmlEmailTemplateData,
  MessageFormComponentPropsType,
} from '../MessageForm.types';
import { useGenericFormStyle } from './GenericForm.styles';
import { renderPreviewField } from './library/renderPreviewField';
import { parseHtmlEmailTemplate } from './library/parseHtmlEmailTemplate';

interface PreviewPropsType<Values = any>
  extends Pick<MessageFormComponentPropsType, 'formName'> {
  fields: FormRendererFormField<Values>[];
  values: FormValuesType<Values>;
  htmlEmailTemplateData?: HtmlEmailTemplateData;
}

const Preview: React.ComponentType<PreviewPropsType> = ({
  formName,
  values,
  fields,
  htmlEmailTemplateData,
}) => {
  const classes = useGenericFormStyle();
  const { caseUuid = '' } = useContext(CommunicationContext);
  const [resolvedValues, setResolvedValues] = useState<FormValuesType | null>(
    null
  );

  useEffect(() => {
    async function getPreviewValues() {
      const resolvedValues = await getPreview(values, caseUuid);
      const parsedValues = htmlEmailTemplateData
        ? parseHtmlEmailTemplate(resolvedValues, htmlEmailTemplateData)
        : resolvedValues;

      setResolvedValues(parsedValues);
    }

    getPreviewValues();
  }, []);

  if (!resolvedValues) {
    return <Loader />;
  }

  const previewFields = fields.map(item => ({
    ...item,
    disabled: true,
    value: resolvedValues[item.name],
  }));

  return (
    <React.Fragment>
      {previewFields.map(
        renderPreviewField({ classes, formName, htmlEmailTemplateData })
      )}
    </React.Fragment>
  );
};

export default Preview;
