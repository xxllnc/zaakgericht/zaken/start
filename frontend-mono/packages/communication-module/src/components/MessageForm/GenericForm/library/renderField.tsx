// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { v4 as unique } from 'uuid';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { Render } from '@mintlab/ui/App/Abstract/Render';
import { MessageFormComponentPropsType } from '../../MessageForm.types';

type RenderFieldPropsType = Pick<MessageFormComponentPropsType, 'formName'> & {
  classes: any;
};

export const renderField = ({ classes, formName }: RenderFieldPropsType) => {
  return function RenderFieldComponent({
    FieldComponent,
    name,
    error,
    touched,
    value,
    showLabel,
    hideBorder = false,
    enablePreview = false,
    ...rest
  }: FormRendererFormField) {
    const id = unique();
    const props = {
      ...cloneWithout(rest, 'type', 'mode', 'classes'),
      name,
      value,
      key: `${formName}-component-${name}`,
      scope: `${formName}-component-${name}`,
      id,
      inputId: id,
    };

    return (
      <div
        className={classNames(classes.formRow, {
          [classes.formRowBottom]: !hideBorder,
        })}
        key={props.key}
      >
        <div className={classes.fieldWrapper}>
          {showLabel ? (
            <label className={classes.label} htmlFor={props.id}>
              {rest.label}
            </label>
          ) : null}
          <div className={classes.field}>
            <FieldComponent {...props} error={error} />
          </div>
        </div>
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div role="alert" className={classes.error}>
            {error}
          </div>
        </Render>
      </div>
    );
  };
};
