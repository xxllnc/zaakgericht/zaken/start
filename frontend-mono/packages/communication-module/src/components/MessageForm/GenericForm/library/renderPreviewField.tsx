// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { flattenField } from '@zaaksysteem/common/src/components/form/library/formHelpers';
import Typography from '@mui/material/Typography';
import { Render } from '@mintlab/ui/App/Abstract/Render';
import { MessageFormComponentPropsType } from '../../MessageForm.types';
import CaseSelector from '../../Fields/CaseSelector';

type RenderFieldPropsType = Pick<
  MessageFormComponentPropsType,
  'formName' | 'htmlEmailTemplateData'
> & {
  classes: any;
};

const getPlainTextValue = (value: unknown) => {
  if (!value || (Array.isArray(value) && value.length === 0)) {
    return '-';
  }

  return Array.isArray(value)
    ? value.map(item => flattenField(item, 'label')).join(', ')
    : flattenField(value, 'label');
};

export function renderPreviewField<Values>({
  classes,
  formName,
  htmlEmailTemplateData,
}: RenderFieldPropsType) {
  /* eslint complexity: [2, 7] */
  return function RenderFieldComponent({
    name,
    error,
    value,
    touched,
    hideBorder = false,
    config,
  }: FormRendererFormField<Values>) {
    const iframeId = `${formName}-${name}`;
    const resizeIframe = () => {
      const iframe = document.getElementById(iframeId);

      if (iframe && iframe.tagName === 'IFRAME') {
        iframe.style.height = `${
          (iframe as any).contentWindow.document.body.scrollHeight + 40
        }px`;
      }
    };

    return (
      <div
        key={name}
        className={classNames(classes.formRow, {
          [classes.formRowBottom]: !hideBorder,
        })}
      >
        {formName === 'email-form' &&
        htmlEmailTemplateData &&
        name === 'content' &&
        typeof value === 'string' ? (
          <iframe
            title={iframeId}
            id={iframeId}
            className={classes.previewHtml}
            srcDoc={value}
            onLoad={resizeIframe}
          />
        ) : name === 'case_uuid' ? (
          //@ts-ignore
          <CaseSelector
            config={config}
            readOnly={true}
            value={value as string}
          />
        ) : (
          <Typography variant="body1" classes={{ root: classes.formValue }}>
            {getPlainTextValue(value)}
          </Typography>
        )}
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div className={classes.error}>{error}</div>
        </Render>
      </div>
    );
  };
}
