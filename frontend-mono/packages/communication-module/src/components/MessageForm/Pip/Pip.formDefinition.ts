// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CASE_SELECTOR } from '../../../Communication.constants';
import { CommunicationContextContextType } from '../../../types/Context.types';
import { SubjectRelationType } from '../../../types/SubjectRelations';

export type SavePipValuesType = {
  case_uuid: string;
  requestor: NestedFormValue[];
  subject: string;
  content: string;
  attachments: NestedFormValue | NestedFormValue[];
};

type GetPipFormDefinitionType = (
  context: CommunicationContextContextType,
  caseUuid?: string,
  contactUuid?: string,
  subjectRelations?: SubjectRelationType[]
) => FormDefinition<SavePipValuesType>;

/* eslint complexity: [2, 8] */
export const getPipFormDefinition: GetPipFormDefinitionType = (
  context,
  caseUuid,
  contactUuid,
  subjectRelations
) => {
  const requestor = subjectRelations?.find(
    relation => relation.role === 'aanvrager'
  );

  return [
    ...(context === 'case'
      ? [
          <FormDefinitionField<SavePipValuesType>>{
            name: 'requestor',
            type: fieldTypes.SELECT,
            value: requestor,
            applyBackgroundColor: true,
            disabled: true,
          },
        ]
      : []),
    contactUuid && context === 'pip'
      ? <FormDefinitionField<SavePipValuesType>>{
          name: 'case_uuid',
          type: CASE_SELECTOR,
          value: caseUuid || '',
          required: true,
          placeholder: 'addFields.selectCase',
          label: 'addFields.selectCase',
          isSearchable: context === 'pip',
          nestedValue: true,
          config: {
            contactUuid,
          },
          readOnly: Boolean(caseUuid),
        }
      : <FormDefinitionField<SavePipValuesType>>{
          name: 'case_uuid',
          value: caseUuid,
          type: fieldTypes.TEXT,
          hidden: true,
        },

    {
      name: 'subject',
      type: fieldTypes.TEXT,
      multi: true,
      format: 'text',
      value: '',
      required: true,
      placeholder: 'addFields.subjectPlaceholder',
      label: 'addFields.subject',
    },
    {
      name: 'content',
      type: fieldTypes.TEXTAREA,
      multi: true,
      format: 'text',
      value: '',
      required: true,
      placeholder: 'addFields.messagePlaceholder',
      label: 'addFields.message',
      isMultiline: true,
      rows: 7,
    },
    ...(caseUuid && context !== 'pip'
      ? [
          <FormDefinitionField<SavePipValuesType>>{
            name: 'attachments',
            type: fieldTypes.CASE_DOCUMENT_FINDER,
            value: null,
            placeholder: 'addFields.attachmentsPlaceholder',
            multiValue: true,
            applyBackgroundColor: true,
            config: { caseUuid },
          },
        ]
      : [
          <FormDefinitionField<SavePipValuesType>>{
            name: 'attachments',
            type: fieldTypes.UPLOAD,
            value: null,
            required: false,
            format: 'file',
            uploadDialog: true,
            multiValue: true,
            label: 'addFields.attachments',
          },
        ]),
  ];
};
