// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const sendPostex = (
  values: any,
  interfaceUuid: string,
  caseNumber: number,
  userUuid: string
) => {
  return request(
    'POST',
    `/api/v1/sysin/interface/${interfaceUuid}/trigger/send_message`,
    {
      body: '',
      subject: '',
      file_attachments: values.attachments.map((val: any) => val.number),
      recipient_type: values.recipient_type.value,
      recipient_role: values.role_name.value || null,
      case_id: caseNumber,
      sender_uuid: userUuid,
    }
  );
};

export const usePostexInfoQuery = () =>
  useQuery(['POSTEX_INFO'], async () => {
    const res = await request(
      'GET',
      '/api/v1/sysin/interface/get_all?rows_per_page=250'
    );

    const postex = res.result.instance.rows.find(
      (row: any) => row.instance.module === 'postex'
    );

    return { uuid: postex.reference };
  });
