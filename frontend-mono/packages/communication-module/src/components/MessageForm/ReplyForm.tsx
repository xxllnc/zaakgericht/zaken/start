// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Email from './Email/Email';
import Pip from './Pip/Pip';

export interface ReplyFormPropsType {
  type: string;
  caseUuid: string;
  threadUuid: string;
  firstMessage: any;
  lastMessage: any;
}

const ReplyForm: React.ComponentType<ReplyFormPropsType> = ({
  type,
  caseUuid,
  threadUuid,
  firstMessage,
  lastMessage,
}) => {
  const [replyIsOpen, setReplyOpen] = useState(false);
  const [t] = useTranslation('communication');
  const closeReplyForm = () => setReplyOpen(false);

  return replyIsOpen ? (
    <div>
      {type === 'email' ? (
        <Email
          cancel={closeReplyForm}
          caseUuid={caseUuid}
          threadUuid={threadUuid}
          firstMessage={firstMessage}
          lastMessage={lastMessage}
          onSubmit={() => {
            setReplyOpen(false);
          }}
        />
      ) : (
        <Pip
          cancel={closeReplyForm}
          caseUuid={caseUuid}
          threadUuid={threadUuid}
          firstMessage={firstMessage}
        />
      )}
    </div>
  ) : (
    <Button
      name="openMessageReply"
      icon="add"
      action={() => {
        setReplyOpen(true);
      }}
    >
      {t('replyForm.openButtonLabel')}
    </Button>
  );
};

export default ReplyForm;
