// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { ThreadType } from '../Thread.types';
import { ContactMomentMessageType } from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';

type NameObjectType = { name: string };

interface ContactType {
  direction: string;
  sender: NameObjectType;
  recipient: NameObjectType;
}

type GetName = (options: ContactType) => string;

const getSenderName: GetName = ({ direction, sender, recipient }) =>
  (direction === 'incoming' ? recipient : sender).name;

const getRecipientName: GetName = ({ direction, sender, recipient }) =>
  (direction === 'incoming' ? sender : recipient).name;

type ContactMomentMessagePropsType = {
  message: ContactMomentMessageType;
  thread: ThreadType;
};

const ContactMomentMessage: React.FunctionComponent<
  ContactMomentMessagePropsType
> = ({ message, thread }) => {
  const { channel, content, direction, sender, recipient, type } = message;
  const [t] = useTranslation('communication');
  const title = t('thread.contactMoment.messageTitle', {
    sender: getSenderName({ direction, sender, recipient }),
    receiver: getRecipientName({ direction, sender, recipient }),
  });
  const info = `${t(`channels.${channel}`)} ${
    direction === null ? '' : `- ${t(`direction.${direction}`)}`
  }`;
  const icon = <ZsIcon size="small">{`channel.inverted.${channel}`}</ZsIcon>;

  return (
    <div>
      <MessageHeader
        thread={thread}
        message={message}
        title={title}
        info={info}
        icon={icon}
      />
      <MessageContent content={content} type={type} />
    </div>
  );
};

export default ContactMomentMessage;
