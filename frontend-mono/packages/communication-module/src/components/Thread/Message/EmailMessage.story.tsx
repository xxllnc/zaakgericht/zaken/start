// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { boolean, stories } from '@mintlab/ui/App/story';
import EmailMessage from './EmailMessage';
import { getMessage } from './PipMessage.story';

const getEmailMessage = () => ({
  ...getMessage(),
  participants: {
    to: [
      {
        email: 'to@test.com',
        name: 'Recipient',
      },
    ],
    from: {
      email: 'from@test.com',
      name: 'Sender',
    },
    cc: [
      {
        email: 'cc@test.com',
        name: 'RecipientCC',
      },
    ],
    bcc: [],
  },
});

stories(module, `${__dirname}/EmailMessage`, {
  Default() {
    return (
      <EmailMessage
        isUnread={boolean('Unread', false)}
        message={getEmailMessage()}
        expanded={true}
      />
    );
  },

  Unread() {
    return (
      <EmailMessage
        isUnread={true}
        message={getEmailMessage()}
        expanded={true}
      />
    );
  },

  Collapsed() {
    return (
      <EmailMessage
        isUnread={false}
        message={getEmailMessage()}
        expanded={false}
      />
    );
  },
});
