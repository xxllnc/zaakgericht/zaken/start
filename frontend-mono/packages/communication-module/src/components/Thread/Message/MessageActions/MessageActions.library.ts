// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICommunication, APIDocument } from '@zaaksysteem/generated';
import MarkAsUnreadSharedVariable from '../../../../library/MarkAsUnreadSharedVariable';

type OutputFormatType = 'original' | 'pdf';

export const postCommunicationReadCountChange = () => {
  top?.window.postMessage(
    {
      type: 'communicationReadCountChange',
    },
    '*'
  );
};

export const markMessageAsUnread = (
  messageUuids: string[],
  context: string
) => {
  MarkAsUnreadSharedVariable.value = messageUuids[0];
  const url = '/api/v2/communication/mark_messages_unread';

  const payload = {
    context: (context === 'pip' ? 'pip' : 'employee') as 'pip' | 'employee',
    message_uuids: messageUuids,
  };

  return request<APICommunication.MarkMessagesUnreadRequestBody>(
    'POST',
    url,
    payload
  );
};

export const deleteMessage = (message_uuid: string) => {
  const url = '/api/v2/communication/delete_message';

  return request<APICommunication.DeleteMessageRequestBody>('POST', url, {
    message_uuid,
  });
};

export const addSourceFileToCase = (
  message_uuid: string,
  messageSubtype: string,
  output_format?: OutputFormatType
) => {
  if (messageSubtype === 'email' || messageSubtype === 'pip') {
    const url = '/api/v2/document/create_document_from_message';
    const payload: APIDocument.CreateDocumentFromMessageRequestBody =
      output_format
        ? {
            message_uuid,
            output_format,
          }
        : { message_uuid };

    return request('POST', url, payload);
  } else {
    console.error(
      'addSourceFileToCase should be called with type email or pip',
      messageSubtype
    );
    return Promise.resolve();
  }
};
