// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useConfirmDialog } from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import Button from '@mintlab/ui/App/Material/Button';
import {
  TYPES_EXTERNAL_MESSAGE,
  GET_MESSAGES,
  GET_THREADS,
} from '../../../../Communication.constants';
import { CommunicationContext } from '../../../../Communication.context';
import { MessageType } from '../../../../types/Message.types';
import { ThreadType } from '../../Thread.types';
import {
  markMessageAsUnread,
  postCommunicationReadCountChange,
  deleteMessage,
  addSourceFileToCase,
} from './MessageActions.library';

/* eslint complexity: [2, 12]*/
export const MessageActions: React.ComponentType<{
  message: MessageType;
  thread: ThreadType;
}> = ({ message, thread }) => {
  const navigate = useNavigate();
  const { context, capabilities } = useContext(CommunicationContext);
  const client = useQueryClient();
  const { mutate: markAsUnreadMutation } = useMutation({
    mutationKey: ['markAsUnread'],
    mutationFn: () => markMessageAsUnread([message.id], context),
    onSuccess: () => {
      client.refetchQueries(GET_THREADS);
      client.invalidateQueries(GET_MESSAGES);
      postCommunicationReadCountChange();
    },
  });

  const { mutate: deleteMutation } = useMutation<any, V2ServerErrorsType>(
    () => deleteMessage(message.id),
    {
      mutationKey: ['deleteMessage'],
      onError: openServerError,
      onSuccess: () => {
        if (thread.numberOfMessages === 1) {
          client.refetchQueries(GET_THREADS);

          navigate('..');
        } else {
          client.invalidateQueries(GET_MESSAGES);
        }
      },
    }
  );

  const { mutate: addOriginalFileToCase } = useMutation<
    void,
    V2ServerErrorsType
  >(() => addSourceFileToCase(message.id, message.type, 'original'), {
    onError: openServerError,
    mutationKey: ['addOriginalSourceFileToCase'],
  });

  const { mutate: addPdfFileToCase } = useMutation<void, V2ServerErrorsType>(
    () => addSourceFileToCase(message.id, message.type, 'pdf'),
    {
      onError: openServerError,
      mutationKey: ['addPdfSourceFileToCase'],
    }
  );

  const isExternalMessage = TYPES_EXTERNAL_MESSAGE.includes(message.type);
  const [t] = useTranslation('communication');
  const [confirmDeleteDialog, confirmDelete] = useConfirmDialog(
    t('thread.deleteMessage.title'),
    t('thread.deleteMessage.body'),
    deleteMutation
  );

  const actions = [
    {
      label: t('thread.messageActions.delete'),
      action: confirmDelete,
      condition: capabilities.canDeleteMessage && message.type !== 'pip',
    },
    {
      label: t('thread.messageActions.addSourceFileToCaseOriginal'),
      action: addOriginalFileToCase,
      condition:
        isExternalMessage &&
        capabilities.canAddSourceFileToCase &&
        thread.relatedCaseActive &&
        message.hasSourceFile,
    },
    {
      label: t('thread.messageActions.addSourceFileToCasePDF'),
      action: addPdfFileToCase,
      condition: isExternalMessage && capabilities.canAddSourceFileToCase,
    },
    {
      label: t('thread.messageActions.markAsUnread'),
      action: markAsUnreadMutation,
      condition:
        isExternalMessage && !message.isUnread && capabilities.canMarkUnread,
    },
  ].filter(action => action.condition);

  return actions.length > 0 ? (
    <>
      <DropdownMenu
        trigger={
          <Button name="communicationActionMenuTrigger" icon="more_vert" />
        }
      >
        <DropdownMenuList items={actions} scope="attachment" />
      </DropdownMenu>
      {confirmDeleteDialog}
    </>
  ) : null;
};
