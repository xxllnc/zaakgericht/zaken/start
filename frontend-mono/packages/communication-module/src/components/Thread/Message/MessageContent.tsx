// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import Typography from '@mui/material/Typography';
import { AttachmentType } from '../../../types/Message.types';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from './../../../Communication.constants';
import AttachmentList from './../AttachmentList/AttachmentList';
import { useMessageContentStyle } from './MessageContent.style';

type MessageContentPropsType = {
  content: string;
  type: string;
  attachments?: AttachmentType[];
};

const MessageContent: React.FunctionComponent<MessageContentPropsType> = ({
  content,
  type,
  attachments,
}) => {
  const classes = useMessageContentStyle();
  return (
    <div
      className={classNames(classes.content, {
        [classes.background]: [TYPE_CONTACT_MOMENT, TYPE_NOTE].includes(type),
      })}
    >
      <Typography variant="body2" classes={{ root: classes.body }}>
        {content}
      </Typography>
      {attachments && <AttachmentList attachments={attachments} />}
    </div>
  );
};

export default MessageContent;
