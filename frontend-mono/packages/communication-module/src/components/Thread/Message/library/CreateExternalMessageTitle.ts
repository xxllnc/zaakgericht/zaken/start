// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import { ExternalMessageType } from '../../../../types/Message.types';

export default function CreateExternalMessageTitle(
  message: ExternalMessageType
): string {
  const { sender, participants } = message;
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [t] = useTranslation('communication');
  const createTitle = (from: string) =>
    t('thread.email.title', {
      createdByName: from,
    });

  if (participants && participants.from && participants.from[0]) {
    return createTitle(participants.from[0].email);
  } else if (sender) {
    return createTitle(sender.name);
  }

  return createTitle(t('thread.email.title'));
}
