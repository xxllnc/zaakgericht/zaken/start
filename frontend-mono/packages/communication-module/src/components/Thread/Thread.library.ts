// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { APICommunication } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { MessageType, ExternalMessageType } from '../../types/Message.types';
import {
  TYPE_PIP_MESSAGE,
  TYPE_POSTEX,
  TYPES_EXTERNAL_MESSAGE,
} from '../../Communication.constants';
import { GET_MESSAGES, GET_THREADS } from '../../Communication.constants';
import { CommunicationContextType } from '../../types/Context.types';
import {
  mapContactMomentMessage,
  mapNoteMessage,
  mapExternalMessage,
} from './messageMapFunctions';
import { postCommunicationReadCountChange } from './Message/MessageActions/MessageActions.library';

/* eslint complexity: [2, 9] */
export const getThreadReplyability = (
  messages: any,
  caseRelation: any,
  data: any,
  context: CommunicationContextType
) => {
  if (!messages || messages.length === 0 || !data) {
    return {
      isReplyable: false,
      canSendReply: false,
    };
  }

  const firstMessage = messages[0];
  const isPostex = (firstMessage as ExternalMessageType).type === TYPE_POSTEX;
  const isReplyable = TYPES_EXTERNAL_MESSAGE.includes(firstMessage.type);
  const canReplyOnMessageType =
    (firstMessage as ExternalMessageType).type === TYPE_PIP_MESSAGE
      ? context.capabilities.canCreatePipMessage
      : isPostex
        ? false
        : context.capabilities.canCreateEmail;
  const canSendReply =
    canReplyOnMessageType &&
    Boolean(caseRelation) &&
    data.caseRelation.status !== 'resolved';

  return {
    isReplyable,
    canSendReply,
  };
};

const getCaseRelationship = (
  response: APICommunication.GetMessageListResponseBody
): any => {
  return response.relationships && response.relationships.case
    ? {
        id: response.relationships.case.data.id,
        caseNumber: response.relationships.case.data.attributes.display_id,
        status: response.relationships.case.data.attributes.status,
      }
    : null;
};

const getMessages = (thread_uuid?: string) => {
  return (
    thread_uuid
      ? request<APICommunication.GetMessageListResponseBody>(
          'GET',
          buildUrl<APICommunication.GetMessageListRequestParams>(
            '/api/v2/communication/get_message_list',
            { thread_uuid }
          )
        )
      : (Promise.resolve() as unknown as Promise<APICommunication.GetMessageListResponseBody>)
  ).then(res => {
    if (res) {
      const messages: MessageType[] = res.data.map(message => {
        switch (message.type) {
          case 'contact_moment':
            return mapContactMomentMessage(message);

          case 'external_message':
            return mapExternalMessage(message);

          case 'note':
            return mapNoteMessage(message);
        }
      });

      return {
        caseRelation: getCaseRelationship(res),
        numberOfMessages: messages.length,
        caseNumber: res?.relationships?.case?.data?.attributes?.display_id,
        relatedCaseActive:
          res?.relationships?.case?.data?.attributes?.status !== 'resolved',
        messages,
      };
    }
  });
};

export const useMessagesQuery = (thread_uuid?: string) =>
  useQuery({
    enabled: Boolean(thread_uuid),
    queryKey: [...GET_MESSAGES, thread_uuid].filter(Boolean),
    queryFn: () => getMessages(thread_uuid),
    onError: openServerError,
  });

export const useMarkMessagesAsReadMutation = (context: string) => {
  const client = useQueryClient();
  return useMutation({
    //@ts-ignore
    mutationKey: ['markAsRead'],
    mutationFn: (message_uuids: string[]) => {
      return message_uuids.length
        ? request<APICommunication.MarkMessagesReadRequestBody>(
            'POST',
            '/api/v2/communication/mark_messages_read',
            {
              context: context === 'pip' ? 'pip' : 'employee',
              message_uuids,
            }
          )
        : Promise.resolve(false);
    },
    onError: openServerError,
    onSuccess: (data: any) => {
      if (data) {
        postCommunicationReadCountChange();
        client.refetchQueries(GET_THREADS);
        client.invalidateQueries(GET_MESSAGES);
      }
    },
  });
};
