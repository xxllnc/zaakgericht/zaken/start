// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useDirectionIconStyleSheet } from './DirectionIcon.style';

type DirectionIconPropsType = {
  direction: keyof ReturnType<typeof useDirectionIconStyleSheet>;
};

const DirectionIcon: React.FunctionComponent<DirectionIconPropsType> = ({
  direction,
}) => {
  const classes = useDirectionIconStyleSheet();
  return (
    <Icon size={16} classes={{ root: `${classes.root} ${classes[direction]}` }}>
      {iconNames.forward}
    </Icon>
  );
};

export default DirectionIcon;
