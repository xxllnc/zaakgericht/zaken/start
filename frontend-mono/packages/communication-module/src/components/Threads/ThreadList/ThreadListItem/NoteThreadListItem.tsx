// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import ThreadListItem, { ThreadListItemPropsType } from './ThreadListItem';
import ThreadTypeIcon from './ThreadTypeIcon/ThreadTypeIcon';

type NoteThreadListItemPropsType = {
  createdByName: string;
  type: string;
} & Omit<ThreadListItemPropsType, 'title' | 'icon'>;

const NoteThreadListItem: React.FunctionComponent<
  NoteThreadListItemPropsType
> = ({ createdByName, ...rest }) => {
  const [t] = useTranslation('communication');
  const title = t('thread.note.title', {
    createdByName,
  });

  return (
    <ThreadListItem
      {...rest}
      title={title}
      {...addScopeProp('thread', 'note')}
      icon={<ThreadTypeIcon type="entityType.inverted.note" />}
    />
  );
};

export default NoteThreadListItem;
