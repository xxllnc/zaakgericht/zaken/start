// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useInfiniteQuery } from '@tanstack/react-query';
import { APICommunication } from '@zaaksysteem/generated/types/APICommunication.types';
import { buildUrl, ENCODED_NULL_BYTE } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { CommunicationContextContextType } from '../../types/Context.types';
import {
  AnyThreadDescriptionType,
  ThreadDescriptionType,
  ThreadType,
} from '../Thread/Thread.types';
import { GET_THREADS } from '../../Communication.constants';

export type FilterType =
  | 'contact_moment'
  | 'note'
  | 'email'
  | 'pip_message'
  | 'postex';

type GetThreadListQueryParameters = {
  context: CommunicationContextContextType;
  caseUuid?: string;
  contactUuid?: string;
};

export const getFilterParameters = ({
  caseUuid,
  contactUuid,
  context,
}: GetThreadListQueryParameters): APICommunication.ThreadListRequestParams => {
  switch (context) {
    case 'case':
      return {
        filter: {
          case_uuid: caseUuid,
        },
      };

    case 'contact':
      return {
        filter: {
          contact_uuid: contactUuid,
        },
      };

    case 'pip':
      return {};

    case 'inbox':
      return {
        filter: {
          case_uuid: ENCODED_NULL_BYTE,
          contact_uuid: ENCODED_NULL_BYTE,
          message_types: ['email', 'pip_message', 'postex'],
        },
      };
  }
};

export const useGetThreadListQuery = (
  context: GetThreadListQueryParameters,
  searchTerm: string,
  filterValue?: FilterType
) =>
  useInfiniteQuery(
    [...GET_THREADS, searchTerm, filterValue],
    async props => {
      const page = props.pageParam || 1;
      const response = await request<APICommunication.ThreadListResponseBody>(
        'GET',
        buildUrl<APICommunication.ThreadListRequestParams>(
          '/api/v2/communication/get_thread_list',
          {
            ...getFilterParameters(context),
            'filter[keyword]': searchTerm,
            'filter[message_types]': filterValue ? [filterValue] : undefined,
            page,
          }
        )
      );

      return {
        data: response.data.map(mapThreadProperties),
        nextPage: page + 1,
      };
    },
    {
      getNextPageParam: lastPage => lastPage.nextPage,
      keepPreviousData: true,
    }
  );

const getCaseNumber = (thread: APICommunication.MessageThreadEntity) =>
  thread.relationships && thread.relationships.case
    ? thread.relationships.case.data.attributes.display_id
    : undefined;

export const mapThreadProperties = (
  thread: APICommunication.MessageThreadEntity
): ThreadType => {
  const {
    id,
    attributes: { thread_type, last_message },
    meta,
  } = thread;
  const { slug, created } = last_message;
  const {
    number_of_messages,
    unread_employee_count,
    unread_pip_count,
    attachment_count,
  } = meta;
  const relatedCaseActive =
    thread.relationships?.case?.data.attributes.status !== 'resolved';

  return {
    id,
    type: thread_type,
    summary: slug,
    date: new Date(created),
    numberOfMessages: number_of_messages,
    tag: '',
    caseNumber: getCaseNumber(thread),
    lastMessage: mapLastMessageType(thread),
    unread:
      window.location.href.includes('/pip/') ||
      window.location.href.includes('/my-pip/')
        ? typeof unread_pip_count !== 'undefined' && unread_pip_count > 0
        : typeof unread_employee_count !== 'undefined' &&
          unread_employee_count > 0,
    hasAttachment: attachment_count > 0,
    relatedCaseActive,
  };
};

const mapLastMessageType = (
  thread: APICommunication.MessageThreadEntity
): AnyThreadDescriptionType => {
  const lastMessage = thread.attributes.last_message;
  const genericProperties: ThreadDescriptionType = {
    createdByName: lastMessage.created_name,
  };

  switch (lastMessage.message_type) {
    case 'contact_moment':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        channel: lastMessage.channel,
        direction: lastMessage.direction,
        withName: lastMessage.recipient_name,
      };

    case 'email':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        subject: lastMessage.subject,
        failureReason: lastMessage.failure_reason,
      };

    case 'postex':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        subject: lastMessage.subject,
        failureReason: lastMessage.failure_reason,
      };

    case 'note':
      return { ...genericProperties, type: lastMessage.message_type };

    case 'pip_message':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        subject: lastMessage.subject,
      };
  }
};
