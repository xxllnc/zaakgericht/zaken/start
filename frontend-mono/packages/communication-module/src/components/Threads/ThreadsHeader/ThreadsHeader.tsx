// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import ActionBar from '@mintlab/ui/App/Zaaksysteem/ActionBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CommunicationContext } from '../../../Communication.context';
import { FilterType } from '../Threads.library';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_EMAIL,
  TYPE_NOTE,
  TYPE_POSTEX,
} from '../../../Communication.constants';
import {
  canCreate,
  getDefaultCreateTypeFromCapabilities,
} from './ThreadsHeader.library';

export interface ThreadsHeaderPropsType {
  filter?: FilterType;
  onFilterChange: (filter?: FilterType) => void;
  searchTerm: string;
  onSearchTermChange: (searchTerm: string) => void;
}

/* eslint complexity: [2, 9] */
const ThreadsHeader: React.ComponentType<ThreadsHeaderPropsType> = ({
  filter,
  searchTerm,
  onFilterChange,
  onSearchTermChange,
}) => {
  const { capabilities, setImportMessageDialogOpened } =
    React.useContext(CommunicationContext);
  const [t] = useTranslation(['communication']);
  const navigate = useNavigate();

  const addThreadPath = `new/${getDefaultCreateTypeFromCapabilities(
    capabilities
  )}`;

  const filterChoices = [
    TYPE_NOTE,
    TYPE_CONTACT_MOMENT,
    TYPE_EMAIL,
    'pip_message',
    TYPE_POSTEX,
  ].map(type => ({
    label: t(`threadTypes.${type}`),
    value: type,
  }));

  return (
    <div>
      <ActionBar
        filters={[
          {
            type: 'select' as 'select',
            value: filter,
            name: 'filter-assignment',
            isClearable: true,
            isSearchable: false,
            placeholder: t('thread.filter'),
            onChange: (ev: any) => {
              onFilterChange(ev.target.value?.value as FilterType);
            },
            choices: filterChoices,
            condition: capabilities.canFilter,
          },
          {
            value: searchTerm,
            type: 'text' as 'text',
            name: 'searchThreads',
            icon: iconNames.search,
            placeholder: t('thread.searchPlaceholder'),
            onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
              onSearchTermChange(event.currentTarget.value);
            },
            closeAction: () => onSearchTermChange(''),
            condition: true,
          },
        ].filter(action => action.condition)}
        actions={[
          {
            name: 'addThread',
            label: t('forms.new'),
            icon: iconNames.add,
            type: 'text' as 'text',
            action: () => {
              navigate(addThreadPath);
            },
            condition: canCreate(capabilities),
          },
          {
            name: 'importThread',
            action: () => setImportMessageDialogOpened(true),
            label: t('threads.actions.import'),
            icon: iconNames.publish,
            condition: capabilities.canImportMessage,
          },
        ].filter(action => action.condition)}
      />
    </div>
  );
};

export default ThreadsHeader;
