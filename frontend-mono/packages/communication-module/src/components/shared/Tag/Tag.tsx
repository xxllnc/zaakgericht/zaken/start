// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useTagStyle } from './Tag.style';

const Tag = ({ children, style }: { style?: any; children?: any }) => {
  const classes = useTagStyle();
  return <span className={classNames(classes.wrapper, style)}>{children}</span>;
};

export default Tag;
