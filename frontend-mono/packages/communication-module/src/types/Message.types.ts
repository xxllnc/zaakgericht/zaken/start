// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';

type Direction = 'incoming' | 'outgoing';

export type TypeOfCreateMessage = 'contact_moment' | 'note' | 'pip' | 'email';

export type TypeOfMessage = 'contact_moment' | 'note' | 'external_message';
export type SubTypeOfMessage = 'pip' | 'email' | 'postex';

type NameObject = {
  name: string;
  type: SubjectTypeType;
};

export type AttachmentType = {
  download?: {
    url: string;
  };
  preview?: {
    url: string;
    contentType?: string;
  };
  id: string;
  name: string;
  size: number;
};

export type ParticipantType = {
  email: string;
  name: string;
  uuid?: string;
};

export type ParticipantsType = {
  from: ParticipantType[];
  to: ParticipantType[];
  cc: ParticipantType[];
  bcc: ParticipantType[];
};

export type ExternalMessageParticipantsType = {
  from: ParticipantType[];
  to: ParticipantType[];
  cc: ParticipantType[];
  bcc: ParticipantType[];
};

export interface MessageType {
  id: string;
  type: TypeOfMessage | SubTypeOfMessage;
  content: string;
  createdDate: string;
  summary: string;
  threadUuid: string;
  isUnread?: boolean;
  failureReason?: string;
  hasSourceFile?: boolean;
  hasAttachements?: boolean;
}

export interface ContactMomentMessageType extends MessageType {
  channel: string;
  direction: Direction;
  recipient: NameObject;
  sender: NameObject;
}

export interface NoteMessageType extends MessageType {
  sender: NameObject;
}

export interface ExternalMessageType extends MessageType {
  subject: string;
  attachments: AttachmentType[];
  participants?: ExternalMessageParticipantsType;
  sender: NameObject | null;
}

export type CaseChoiceType = {
  value: string;
  label: string;
  subLabel: string | null;
};

export type ImportMessageFormFields = { file_uuid: NestedFormValue };
