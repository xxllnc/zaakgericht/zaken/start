// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const fs = require('fs');
const { promisify } = require('util');
const handle = require('./promiseHandle');

const readFileAsync = promisify(fs.readFile);

async function readSchemaFile() {
  const schemaFile = await handle(readFileAsync('schema.json', 'UTF-8'));
  const [schema, schemaError] = schemaFile;

  if (schemaError) throw Error('Error reading schema.json file from disk');

  return JSON.parse(schema);
}

module.exports = readSchemaFile;
