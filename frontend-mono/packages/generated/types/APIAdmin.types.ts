// Generated on: 3-1-2025 11:00:10 (Amsterdam time)
// Branch used: development
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Admin domain.
/* eslint-disable */
export namespace APIAdmin {
  export interface GetFolderContentsRequestParams {
    folder_id?: string;
    page: number;
    page_size: number;
    [k: string]: any;
  }

  export interface GetFolderContentsResponseBody {
    data: {
      type: 'folder_entry';
      id: string;
      attributes: {
        type:
          | 'folder'
          | 'case_type'
          | 'object_type'
          | 'attribute'
          | 'email_template'
          | 'document_template'
          | 'custom_object_type';
        name: string;
        active: boolean;
        [k: string]: any;
      };
      links: {
        self: string;
        meta?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    links?: {
      self?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      parent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      grandparent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetEntryDetailRequestParams {
    item_id: string;
    type:
      | 'folder'
      | 'case_type'
      | 'object_type'
      | 'custom_object_type'
      | 'attribute'
      | 'email_template'
      | 'document_template';
    [k: string]: any;
  }

  export interface GetEntryDetailResponseBody {
    data: {
      schema?:
        | {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'case_type';
            id: string;
            attributes: {
              name?: string;
              active?: boolean;
              api_v1_endpoint?: string;
              stuf_identification?: string;
              current_version?: number;
              last_modified?: string;
              context?: ('internal' | 'external')[];
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              used_in_object_types?: {
                id?: string;
                type?: 'object_type';
                attributes?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            links?: {
              export: string;
              version_history: string;
              registration_form_internal?: string;
            };
            [k: string]: any;
          }
        | {
            type: 'object_type';
            id: string;
            attributes: {
              name?: string;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'custom_object_type';
            id: string;
            attributes: {
              name?: string;
              title?: string;
              external_reference?: string;
              status?: string;
              version?: number;
              version_independent_uuid?: string;
              last_modified?: string;
              date_created?: string;
              [k: string]: any;
            };
            relationships: {
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'attribute';
            id: string;
            attributes: {
              name?: string;
              magic_string?: string;
              entry_type?: string;
              is_multiple?: boolean;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              used_in_object_types?: {
                id?: string;
                type?: 'object_type';
                attributes?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'email_template';
            id: string;
            attributes: {
              name?: string;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'document_template';
            id: string;
            attributes: {
              name?: string;
              download_uri?: string;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface MoveFolderEntriesResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface MoveFolderEntriesRequestBody {
    folder_enties: {
      type:
        | 'folder'
        | 'case_type'
        | 'object_type'
        | 'attribute'
        | 'email_template'
        | 'document_template';
      id: string;
      [k: string]: any;
    }[];
    destination_folder_id: string;
    [k: string]: any;
  }

  export interface ChangeCaseTypeOnlineStatusResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ChangeCaseTypeOnlineStatusRequestBody {
    active: boolean;
    case_type_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface GetAttributeDetailRequestParams {
    attribute_id: string;
    [k: string]: any;
  }

  export interface GetAttributeDetailResponseBody {
    data: {
      schema?: {
        type: string;
        id: string;
        attributes: {
          attribute_type?: string;
          name?: string;
          public_name?: string;
          magic_string?: string;
          type_multiple?: boolean;
          sensitive_field?: boolean;
          help?: string;
          value_default?: string;
          category_name?: string;
          category_uuid?: string;
          document_origin?: string;
          document_trust_level?: string;
          document_source?: string;
          document_category?: string;
          relationship_data?: {
            [k: string]: any;
          };
          attribute_values?: string[];
          /**
           * Can be any value - string, number, boolean, array or object.
           */
          appointment_location_id?: {
            [k: string]: any;
          };
          /**
           * Can be any value - string, number, boolean, array or object.
           */
          appointment_product_id?: {
            [k: string]: any;
          };
          appointment_interface_uuid?: string | null;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAttributeRequestParams {
    [k: string]: any;
  }

  export interface EditAttributeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAttributeRequestBody {
    attribute_uuid?: string;
    fields?: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAttributeRequestParams {
    [k: string]: any;
  }

  export interface CreateAttributeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAttributeRequestBody {
    attribute_uuid?: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GenerateMagicStringRequestParams {
    string_input: string;
    [k: string]: any;
  }

  export interface GenerateMagicStringResponseBody {
    data: {
      schema?: {
        magic_string: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetActiveAppointmentIntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetActiveAppointmentIntegrationsResponseBody {
    data: {
      type: 'integration';
      id: string;
      attributes: {
        name: string;
        active: boolean;
        module: string;
        legacy_id: number;
        manual_type: ('text' | 'file' | 'references')[];
      };
    }[];
    [k: string]: any;
  }

  export interface GetActiveAppointmentV2IntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetActiveAppointmentV2IntegrationsResponseBody {
    data: {
      type: 'integration';
      id: string;
      attributes: {
        name: string;
        active: boolean;
        module: string;
        legacy_id: number;
        manual_type: ('text' | 'file' | 'references')[];
      };
    }[];
    [k: string]: any;
  }

  export interface CreateFolderResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateFolderRequestBody {
    folder_uuid: string;
    name: string;
    parent_uuid: string;
    [k: string]: any;
  }

  export interface RenameFolderResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RenameFolderRequestBody {
    folder_uuid: string;
    name: string;
    [k: string]: any;
  }

  export interface SearchAttributeByNameRequestParams {
    search_string: string;
    type?: string;
    [k: string]: any;
  }

  export interface SearchAttributeByNameResponseBody {
    data: {
      type?: string;
      id?: string;
      attribute?: {
        name?: string;
        magic_string?: string;
        value_type?: string;
        is_multipe?: boolean;
        relationship_type?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  export interface GetEmailTemplateDetailRequestParams {
    email_template_id: string;
    [k: string]: any;
  }

  export interface GetEmailTemplateDetailResponseBody {
    data: {
      schema?: {
        attributes: {
          attachments?: {
            name?: string;
            uuid?: string;
            [k: string]: any;
          }[];
          category_uuid?:
            | string
            | {
                [k: string]: any;
              }
            | null;
          label?: string;
          message?: string;
          sender?: string;
          sender_address?: string;
          subject?: string;
          [k: string]: any;
        };
        id: string;
        type: 'email_template';
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAnEmailTemplateRequestParams {
    [k: string]: any;
  }

  export interface CreateAnEmailTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAnEmailTemplateRequestBody {
    email_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAnEmailTemplateRequestParams {
    [k: string]: any;
  }

  export interface EditAnEmailTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAnEmailTemplateRequestBody {
    email_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SearchCatalogRequestParams {
    keyword?: string;
    page?: number;
    page_size?: number;
    [k: string]: any;
  }

  export interface SearchCatalogResponseBody {
    data: {
      type: 'folder_entry';
      id: string;
      attributes: {
        type:
          | 'folder'
          | 'case_type'
          | 'object_type'
          | 'attribute'
          | 'email_template'
          | 'document_template'
          | 'custom_object_type';
        name: string;
        active: boolean;
        [k: string]: any;
      };
      links: {
        self: string;
        meta?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    links?: {
      self?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      parent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      grandparent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetActiveDocumentIntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetActiveDocumentIntegrationsResponseBody {
    data: {
      type: 'integration';
      id: string;
      attributes: {
        name: string;
        active: boolean;
        module: string;
        legacy_id: number;
        manual_type: ('text' | 'file' | 'references')[];
      };
    }[];
    [k: string]: any;
  }

  export interface GetIntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetIntegrationsResponseBody {
    data: {
      type: 'integration';
      id: string;
      attributes: {
        name: string;
        active: boolean;
        module: string;
        legacy_id: number;
        manual_type: ('text' | 'file' | 'references')[];
      };
    }[];
    [k: string]: any;
  }

  export interface GetTransactionRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetTransactionResponseBody {
    data: {
      type: 'transaction';
      id: string;
      attributes: {
        id: number;
        external_id: string;
        direction: 'incoming' | 'outgoing';
        created: string;
        last_retry: string | null;
        next_retry: string | null;
        retry_count: number | null;
        error_count: number;
        success_count: number;
        error_fatal: boolean;
        record_count: number;
        processed: boolean;
        error_message: string | null;
      };
      meta?: {
        summary?: string | null;
      };
      relationships: {
        integration?: {
          data?: {
            type?: 'integration';
            id?: string;
            [k: string]: any;
          };
        };
        [k: string]: any;
      };
    };
    included?: {
      type: 'integration';
      id: string;
      attributes: {
        name: string;
        active: boolean;
        module: string;
        legacy_id: number;
        manual_type: ('text' | 'file' | 'references')[];
      };
    }[];
    [k: string]: any;
  }

  export interface GetTransactionDataRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetTransactionDataResponseBody {
    data: {
      type: 'transaction_data';
      id: string;
      attributes: {
        input_data: string;
      };
    };
    [k: string]: any;
  }

  export interface GetTransactionsRequestParams {
    page: number;
    page_size: number;
    sort: 'created' | '-created';
    'filters[keyword]'?: string;
    'filters[has_errors]'?: 'only_errors' | 'has_errors' | 'no_errors';
    'filters[relationship.integration.id]'?: string[];
    [k: string]: any;
  }

  export interface GetTransactionsResponseBody {
    data: {
      type: 'transaction';
      id: string;
      attributes: {
        id: number;
        external_id: string;
        direction: 'incoming' | 'outgoing';
        created: string;
        last_retry: string | null;
        next_retry: string | null;
        retry_count: number | null;
        error_count: number;
        success_count: number;
        error_fatal: boolean;
        record_count: number;
        processed: boolean;
        error_message: string | null;
      };
      meta?: {
        summary?: string | null;
      };
      relationships: {
        integration?: {
          data?: {
            type?: 'integration';
            id?: string;
            [k: string]: any;
          };
        };
        [k: string]: any;
      };
    }[];
    included?: {
      type: 'integration';
      id: string;
      attributes: {
        name: string;
        active: boolean;
        module: string;
        legacy_id: number;
        manual_type: ('text' | 'file' | 'references')[];
      };
    }[];
    meta?: {
      total_results?: number;
    };
    [k: string]: any;
  }

  export interface GetTransactionRecordsRequestParams {
    transaction_uuid: string;
    [k: string]: any;
  }

  export interface GetTransactionRecordsResponseBody {
    data: {
      type: 'transaction_record';
      id: string;
      attributes: {
        input: string;
        output: string;
        is_error: boolean;
        date_executed: string;
        error_message: string | null;
      };
      meta?: {
        summary: string | null;
      };
      relationships: {
        integration?: {
          data?: {
            type: 'transaction';
            id: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    [k: string]: any;
  }

  export interface CreateDocumentTemplateRequestParams {
    [k: string]: any;
  }

  export interface CreateDocumentTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentTemplateRequestBody {
    document_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetDocumentTemplateDetailRequestParams {
    document_template_id: string;
    [k: string]: any;
  }

  export interface GetDocumentTemplateDetailResponseBody {
    data: {
      schema?: {
        attributes: {
          category_uuid?:
            | string
            | {
                [k: string]: any;
              }
            | null;
          name?: string;
          interface_uuid?: string;
          template_external_name?: string;
          help?: string;
          [k: string]: any;
        };
        id: string;
        type: 'document_template';
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditDocumentTemplateRequestParams {
    [k: string]: any;
  }

  export interface EditDocumentTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditDocumentTemplateRequestBody {
    document_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCaseTypeHistoryRequestParams {
    case_type_id: string;
    [k: string]: any;
  }

  export interface GetCaseTypeHistoryResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCaseTypeVersionRequestParams {
    [k: string]: any;
  }

  export interface UpdateCaseTypeVersionResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCaseTypeVersionRequestBody {
    case_type_id: string;
    version_id: string;
    reason: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteAttributeRequestParams {
    [k: string]: any;
  }

  export interface DeleteAttributeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteAttributeRequestBody {
    attribute_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteEmailTemplateRequestParams {
    [k: string]: any;
  }

  export interface DeleteEmailTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteEmailTemplateRequestBody {
    email_template_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteFolderRequestParams {
    [k: string]: any;
  }

  export interface DeleteFolderResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteFolderRequestBody {
    folder_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteCaseTypeRequestParams {
    [k: string]: any;
  }

  export interface DeleteCaseTypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCaseTypeRequestBody {
    case_type_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteObjectTypeRequestParams {
    [k: string]: any;
  }

  export interface DeleteObjectTypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteObjectTypeRequestBody {
    case_type_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteDocumentTemplateRequestParams {
    [k: string]: any;
  }

  export interface DeleteDocumentTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteDocumentTemplateRequestBody {
    document_template_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface GetVersionedCasetypeRequestParams {
    uuid?: string;
    version_uuid?: string;
    [k: string]: any;
  }

  export interface GetVersionedCasetypeResponseBody {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Indicator if the casetype is active
         */
        active: boolean;
        /**
         * Api settings for the versioned_casetype
         */
        api: {
          /**
           * Api can transition for this versioned_casetype
           */
          api_can_transition?: boolean;
          /**
           * Api notifications for the versioned_casetype
           */
          notifications?: {
            /**
             * External notification on allocation of case for this versioned_casetype
             */
            external_notify_on_allocate_case?: boolean;
            /**
             * External notification on exceed term for this versioned_casetype
             */
            external_notify_on_exceed_term?: boolean;
            /**
             * External notification on label change for this versioned_casetype
             */
            external_notify_on_label_change?: boolean;
            /**
             * External notification on new case for this versioned_casetype
             */
            external_notify_on_new_case?: boolean;
            /**
             * External notification on new document for this versioned_casetype
             */
            external_notify_on_new_document?: boolean;
            /**
             * External notification on new message for this versioned_casetype
             */
            external_notify_on_new_message?: boolean;
            /**
             * External notification on phase transition for this versioned_casetype
             */
            external_notify_on_phase_transition?: boolean;
            /**
             * External notification on subject change for this versioned_casetype
             */
            external_notify_on_subject_change?: boolean;
            /**
             * External notification on task change for this versioned_casetype
             */
            external_notify_on_task_change?: boolean;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Authorization settings for the versioned_casetype
         */
        authorization: {
          /**
           * Indicator if this authorization entry is confidential
           */
          confidential: boolean;
          /**
           * Department uuid of this authorization entry
           */
          department_uuid: string;
          /**
           * Rights configured for this authorization entry. Possible values are 'zaak_beheer', 'zaak_edit', 'zaak_read', 'zaak_search'
           */
          rights: string[];
          /**
           * Role uuid of this authorization entry
           */
          role_uuid: string;
          [k: string]: any;
        }[];
        /**
         * Case dossier inforation for the versioned_casetype
         */
        case_dossier: {
          /**
           * Allow external task assignment this versioned_casetype
           */
          allow_external_task_assignment?: boolean;
          /**
           * Default document folders for this versioned_casetype
           */
          default_document_folders?: string[];
          /**
           * Default html template for this versioned_casetype
           */
          default_html_email_template?: string;
          /**
           * Disable pip for requestor for this versioned_casetype
           */
          disable_pip_for_requestor?: boolean;
          /**
           * Lock registration phase for this versioned_casetype
           */
          lock_registration_phase?: boolean;
          /**
           * Queue co worker changes for this versioned_casetype
           */
          queue_coworker_changes?: boolean;
          [k: string]: any;
        };
        /**
         * UUID of the casetype
         */
        casetype_uuid: string;
        /**
         * Calatog folder for the versioned_casetype
         */
        catalog_folder: {
          /**
           * Name of the catalog folder
           */
          name?: string;
          /**
           * Uuid of the catalog folder
           */
          uuid: string;
          [k: string]: any;
        };
        /**
         * Describing the changes for the versioned_casetype
         */
        change_log: {
          /**
           * List of components that have been changed for {ENTITY_TYPE}
           */
          update_components: string[];
          /**
           * Description of what changes have been made for versioned_casetype
           */
          update_description?: string;
          [k: string]: any;
        };
        /**
         * Mother casetype settings for the versioned_casetype
         */
        child_casetype_settings: {
          /**
           * List of child casetypes and settings
           */
          child_casetypes: {
            /**
             * Child casetype
             */
            casetype: {
              /**
               * Name of the child casetype
               */
              name: string;
              /**
               * Uuid of the child casetype
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Flag that indicates if this child casetype is enabled
             */
            enabled: boolean;
            /**
             * settings for the child casetype
             */
            settings: {
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          /**
           * Flag if mother casetype settings are enabled
           */
          enabled?: boolean;
          [k: string]: any;
        };
        /**
         * Documentation for the versioned_casetype
         */
        documentation: {
          /**
           * Adjourn periond of the versioned_casetype
           */
          adjourn_period?: number;
          /**
           * Archive classification code of the versioned_casetype
           */
          archive_classification_code?: string;
          /**
           * BAG enabled for the versioned_casetype
           */
          bag?: boolean;
          /**
           * Designation of confidentiality of the versioned_casetype
           */
          designation_of_confidentiality?:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          /**
           * E-webform for versioned_casetype
           */
          e_webform?: string;
          /**
           * Extention period of the versioned_casetype
           */
          extension_period?: number;
          /**
           * GDPR settings for this versioned_casetype
           */
          gdpr?: {
            /**
             * GDPR enabled for the versioned_casetype
             */
            enabled: boolean;
            /**
             * Kind of GDPR
             */
            kind: {
              /**
               * GDPR basic details enabled of the versioned_casetype
               */
              basic_details: boolean;
              /**
               * GDPR criminal record enabled of the versioned_casetype
               */
              criminal_record: boolean;
              /**
               * GDPR genetic or biometric data enabled of the versioned_casetype
               */
              genetic_or_biometric_data: boolean;
              /**
               * GDPR health enabled of the versioned_casetype
               */
              health: boolean;
              /**
               * GDPR income enabled of the versioned_casetype
               */
              income: boolean;
              /**
               * GDPR membership union enabled of the versioned_casetype
               */
              membership_union: boolean;
              /**
               * GDPR  offspring enabled of the versioned_casetype
               */
              offspring: boolean;
              /**
               * GDPR id number enabled of the versioned_casetype
               */
              personal_id_number: boolean;
              /**
               * GDPR political views enabled of the versioned_casetype
               */
              political_views: boolean;
              /**
               * GDPR race or ethniticy enabled of the versioned_casetype
               */
              race_or_ethniticy: boolean;
              /**
               * GDPR religion enabled of the versioned_casetype
               */
              religion: boolean;
              /**
               * GDPR sexual identity enabled of the versioned_casetype
               */
              sexual_identity: boolean;
              [k: string]: any;
            };
            /**
             * Proccess foreign country enabled of the versioned_casetype
             */
            process_foreign_country: boolean;
            /**
             * Process foregein country reason for versioned_casetype
             */
            process_foreign_country_reason?: string;
            /**
             * Processing label for versioned_casetype
             */
            processing_legal?:
              | 'Toestemming'
              | 'Overeenkomst'
              | 'Wettelijke verplichting'
              | 'Publiekrechtelijke taak'
              | 'Vitaal belang'
              | 'Gerechtvaardigd belang';
            /**
             * Processing legal reason for versioned_casetype
             */
            processing_legal_reason?: string;
            /**
             * Processing type of versioned_casetype
             */
            processing_type?: 'Delen' | 'Muteren' | 'Raadplegen';
            /**
             * Source of GDPR
             */
            source: {
              /**
               * GDPR partner enabled of the versioned_casetype
               */
              partner: boolean;
              /**
               * GDPR public source enabled of the versioned_casetype
               */
              public_source: boolean;
              /**
               * GDPR registration enabled of the versioned_casetype
               */
              registration: boolean;
              /**
               * GDPR sender enabled of the versioned_casetype
               */
              sender: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Initiator type of the {ENTITY_TYPE}
           */
          initiator_type?:
            | 'aangaan'
            | 'aangeven'
            | 'aanmelden'
            | 'aanschrijven'
            | 'aanvragen'
            | 'afkopen'
            | 'afmelden'
            | 'indienen'
            | 'inschrijven'
            | 'melden'
            | 'ontvangen'
            | 'opstellen'
            | 'opzeggen'
            | 'registreren'
            | 'reserveren'
            | 'starten'
            | 'stellen'
            | 'uitvoeren'
            | 'vaststellen'
            | 'versturen'
            | 'voordragen'
            | 'vragen';
          /**
           * Legal basis for versioned_casetype
           */
          legal_basis?: string;
          /**
           * Lex silencio positivo enable for the versioned_casetype
           */
          lex_silencio_positivo?: boolean;
          /**
           * Local basis for versioned_casetype
           */
          local_basis?: string;
          /**
           * May extend enabled for the versioned_casetype
           */
          may_extend?: boolean;
          /**
           * May postpone enabled for the versioned_casetype
           */
          may_postpone?: boolean;
          /**
           * Motivation of the versioned_casetype
           */
          motivation?: string;
          /**
           * Penalty law of the versioned_casetype
           */
          penalty_law?: boolean;
          /**
           * Possibility for objection and_appeal for the versioned_casetype
           */
          possibility_for_objection_and_appeal?: boolean;
          /**
           * Process description of the versioned_casetype
           */
          process_description?: string;
          /**
           * Publication for the versioned_casetype
           */
          publication?: boolean;
          /**
           * Publication text for versioned_casetype
           */
          publication_text?: string;
          /**
           * Purpose of the versioned_casetype
           */
          purpose?: string;
          /**
           * Responsible relationship of the versioned_casetype
           */
          responsible_relationship?: string;
          /**
           * Resposible subject of the versioned_casetype
           */
          responsible_subject?: string;
          /**
           * WKPB applies for the versioned_casetype
           */
          wkpb_applies?: boolean;
          [k: string]: any;
        };
        /**
         * General attributes for the versioned_casetype
         */
        general_attributes: {
          /**
           * Case public summary for versioned_casetype
           */
          case_public_summary?: string;
          /**
           * Case summary for versioned_casetype
           */
          case_summary?: string;
          /**
           * Description for versioned_casetype
           */
          description?: string;
          /**
           * Identification of the versioned_casetype
           */
          identification?: string;
          /**
           * Legal period for the versioned_casetype
           */
          legal_period?: {
            /**
             * Type of monitoring period
             */
            type?: 'kalenderdagen' | 'werkdagen' | 'weken' | 'einddatum';
            /**
             * Value for the monitoring period
             */
            value?: string;
            [k: string]: any;
          };
          /**
           * Name of the versioned_casetype
           */
          name?: string;
          /**
           * Service period for the versioned_casetype
           */
          service_period?: {
            /**
             * Type of monitoring period
             */
            type?: 'kalenderdagen' | 'werkdagen' | 'weken' | 'einddatum';
            /**
             * Value for the monitoring period
             */
            value?: string;
            [k: string]: any;
          };
          /**
           * Tags for versioned_casetype
           */
          tags?: string;
          [k: string]: any;
        };
        /**
         * Phases for this versioned_casetype
         */
        phases?: {
          /**
           * Assignment settings for the phase
           */
          assignment?: {
            /**
             * Department name for this phase assignment
             */
            department_name: string;
            /**
             * Department uuid for this phase assignment
             */
            department_uuid: string;
            /**
             * Indicator if the phase assignment is enabled
             */
            enabled: boolean;
            /**
             * Role name for this phase assignment
             */
            role_name: string;
            /**
             * Role uuid for this phase assignment
             */
            role_uuid: string;
            [k: string]: any;
          };
          /**
           * Create datetime of the phase
           */
          created?: string;
          /**
           * Custom fields for the phase
           */
          custom_fields?: (
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bag_straat_adres';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bag_adres';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bag_straat_adressen';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bag_adressen';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'address_v2';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * WMS feature attribute id for this attribute
                 */
                map_wms_feature_attribute_id?: string;
                /**
                 * WMS feature attribute label for this attribute
                 */
                map_wms_feature_attribute_label?: string;
                /**
                 * WMS layer id for this attribute
                 */
                map_wms_layer_id?: string;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Show attribute on map
                 */
                show_on_map: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'appointment';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'appointment_v2';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bankaccount';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'calendar';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'calendar_supersaas';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'checkbox';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'date';
                /**
                 * End date limitation for date attribute
                 */
                end_date_limitation?: {
                  /**
                   * Active indicator for date limitation
                   */
                  active: boolean;
                  /**
                   * During for the date limitation [pre | post]
                   */
                  during: string;
                  /**
                   * Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute
                   */
                  reference: string | 'currentDate';
                  /**
                   * Unit for date limitation [days | weeks | months | years]
                   */
                  term: string;
                  /**
                   * Value for date limitation
                   */
                  value: number;
                  [k: string]: any;
                };
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Start date limitation for date attribute
                 */
                start_date_limitation?: {
                  /**
                   * Active indicator for date limitation
                   */
                  active: boolean;
                  /**
                   * During for the date limitation [pre | post]
                   */
                  during: string;
                  /**
                   * Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute
                   */
                  reference: string | 'currentDate';
                  /**
                   * Unit for date limitation [days | weeks | months | years]
                   */
                  term: string;
                  /**
                   * Value for date limitation
                   */
                  value: number;
                  [k: string]: any;
                };
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'email';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'file';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'geojson';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'googlemaps';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Show attribute on map
                 */
                show_on_map: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute Type
                 */
                attribute_type: 'group';
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'image_from_url';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'geolatlon';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Indicator if attribute must be used as case location
                 */
                map_case_location: boolean;
                /**
                 * WMS feature attribute uuid for this attribute
                 */
                map_wms_feature_attribute_id?: string;
                /**
                 * WMS feature attribute label for this attribute
                 */
                map_wms_feature_attribute_label?: string;
                /**
                 * WMS layer id for this attribute
                 */
                map_wms_layer_id?: string;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'numeric';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bag_openbareruimte';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'bag_openbareruimtes';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * Use this attribute as case address
                 */
                use_as_case_address?: boolean;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'option';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'richtext';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'select';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'subject';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'textarea';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'text';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute Type
                 */
                attribute_type: 'textblock';
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'text_uc';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'url';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valuta';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valutaex21';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valutaex6';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valutaex';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valutain21';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valutain6';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'valutain';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'relationship';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if multiple values is configured
                 */
                is_multiple?: boolean;
                /**
                 * Label for allowing multiple values
                 */
                label_multiple?: string;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Selected role for subject relation
                 */
                relationship_subject_role?: string;
                /**
                 * Indicator for the relationship type
                 */
                relationship_type?: 'subject';
                /**
                 * Show attribute on map
                 */
                show_on_map: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'relationship';
                /**
                 * Attribute mapping for object creation
                 */
                create_custom_object_attribute_mapping?: {
                  [k: string]: any;
                };
                /**
                 * Is object creation enabled
                 */
                create_custom_object_enabled: boolean;
                /**
                 * Label for object creation
                 */
                create_custom_object_label?: string;
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator for the relationship type
                 */
                relationship_type?: 'custom_object';
                /**
                 * Show attribute on map
                 */
                show_on_map: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Attribute magic string
                 */
                attribute_magic_string?: string;
                /**
                 * Attribute name
                 */
                attribute_name: string;
                /**
                 * Attribute Type
                 */
                attribute_type: 'relationship';
                /**
                 * External help text for the attribute
                 */
                help_extern: string;
                /**
                 * Internal help text for the attribute
                 */
                help_intern: string;
                /**
                 * Indicator if this is a group attribute
                 */
                is_group: boolean;
                /**
                 * Indicator if the attribute is mandatory
                 */
                mandatory: boolean;
                /**
                 * Permissions for the attribute
                 */
                permissions: {
                  /**
                   * Department name for the attribute permission
                   */
                  department_name: string;
                  /**
                   * Department uuid for the attribute permission
                   */
                  department_uuid: string;
                  /**
                   * Role name for the attribute permission
                   */
                  role_name: string;
                  /**
                   * Role uuid for the attribute permission
                   */
                  role_uuid: string;
                  [k: string]: any;
                }[];
                /**
                 * Indicator if requestor can change attribute value on PIP
                 */
                pip_can_change: boolean;
                /**
                 * Indicator if this attribute can be published on pip
                 */
                publish_pip: boolean;
                /**
                 * Indicator if the attribute is referential
                 */
                referential: boolean;
                /**
                 * Indicator for the relationship type. This case relation type is not supported anymore.
                 */
                relationship_type?: 'case';
                /**
                 * Show attribute on map
                 */
                show_on_map: boolean;
                /**
                 * Indicator if change approval is needed
                 */
                skip_change_approval: boolean;
                /**
                 * Inidcator if this is a system attribute
                 */
                system_attribute: boolean;
                /**
                 * Title for the attribute
                 */
                title: string;
                /**
                 * UUID for the attribute
                 */
                uuid: string;
                [k: string]: any;
              }
          )[];
          /**
           * Email templates for this phase
           */
          email_templates?: (
            | {
                /**
                 * Bcc
                 */
                bcc: string;
                /**
                 * Catalog Message Label
                 */
                catalog_message_label: string;
                /**
                 * Catalog Message Uuid
                 */
                catalog_message_uuid: string;
                /**
                 * Cc
                 */
                cc: string;
                /**
                 * Reciever
                 */
                reciever:
                  | 'aanvrager'
                  | 'zaak_behandelaar'
                  | 'coordinator'
                  | 'gemachtigde';
                /**
                 * Send Automatic
                 */
                send_automatic: string;
                [k: string]: any;
              }
            | {
                /**
                 * Bcc
                 */
                bcc: string;
                /**
                 * Catalog Message Label
                 */
                catalog_message_label: string;
                /**
                 * Catalog Message Uuid
                 */
                catalog_message_uuid: string;
                /**
                 * Cc
                 */
                cc: string;
                /**
                 * Reciever
                 */
                reciever: 'overig';
                /**
                 * Send Automatic
                 */
                send_automatic: string;
                /**
                 * To
                 */
                to: string;
                [k: string]: any;
              }
            | {
                /**
                 * Bcc
                 */
                bcc: string;
                /**
                 * Catalog Message Label
                 */
                catalog_message_label: string;
                /**
                 * Catalog Message Uuid
                 */
                catalog_message_uuid: string;
                /**
                 * Cc
                 */
                cc: string;
                /**
                 * Person Involved Role
                 */
                person_involved_role: string;
                /**
                 * Reciever
                 */
                reciever: 'betrokkene';
                /**
                 * Send Automatic
                 */
                send_automatic: string;
                [k: string]: any;
              }
            | {
                /**
                 * Bcc
                 */
                bcc: string;
                /**
                 * Catalog Message Label
                 */
                catalog_message_label: string;
                /**
                 * Catalog Message Uuid
                 */
                catalog_message_uuid: string;
                /**
                 * Cc
                 */
                cc: string;
                /**
                 * Reciever
                 */
                reciever: 'behandelaar';
                /**
                 * Send Automatic
                 */
                send_automatic: string;
                /**
                 * Introspectable object based on pydantic
                 */
                subject: {
                  /**
                   * Name of the subject relation
                   */
                  name: string;
                  /**
                   * Type of the subject relation
                   */
                  type?: 'employee';
                  /**
                   * Uuid of the subject relation
                   */
                  uuid: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }
          )[];
          /**
           * Last modified datetime of the phase
           */
          last_modified?: string;
          /**
           * Name of the phase milestone
           */
          milestone_name: string;
          /**
           * Name of the phase
           */
          phase_name: string;
          /**
           * Configured tasks for the phase
           */
          tasks?: string[];
          /**
           * Templates for this phase.
           */
          templates?: {
            /**
             * Allow editing the filename
             */
            allow_edit?: boolean;
            /**
             * Autogenerate the template document on phase transition
             */
            auto_generate?: boolean;
            /**
             * Name of the document template from the catalog
             */
            catalog_template_name?: string;
            /**
             * UUID of the document_tempate from the catalog
             */
            catalog_template_uuid?: string;
            /**
             * Filename for the generated document
             */
            custom_filename?: string;
            /**
             * Description of the template
             */
            description?: string;
            /**
             * Name of the attribute where the generated document will be added.
             */
            document_attribute_name?: string;
            /**
             * UUID of the attribute where the generated document will be added.
             */
            document_attribute_uuid?: string;
            /**
             * Label of the template
             */
            label?: string;
            /**
             * Target format of the template
             */
            target_format?: 'odt' | 'pdf' | 'docx';
            [k: string]: any;
          }[];
          /**
           * Term in days for milestone
           */
          term_in_days: number;
          [k: string]: any;
        }[];
        /**
         * RegistrationForm attributes for the versioned_casetype
         */
        registrationform: {
          /**
           * Allow add relations for the versioned_casetype
           */
          allow_add_relations?: boolean;
          /**
           * Allow assigning for the versioned_casetype
           */
          allow_assigning?: boolean;
          /**
           * Allow assigning to self for the versioned_casetype
           */
          allow_assigning_to_self?: boolean;
          /**
           * Show confidentionality for the versioned_casetype
           */
          show_confidentionality?: boolean;
          /**
           * Show contact details for the versioned_casetype
           */
          show_contact_details?: boolean;
          [k: string]: any;
        };
        /**
         * Relations for the versioned_casetype
         */
        relations: {
          /**
           * Show address requestor on map for this versioned_casetype
           */
          address_requestor_show_on_map?: boolean;
          /**
           * Use address of requestor as case address for this versioned_casetype
           */
          address_requestor_use_as_case_address?: boolean;
          /**
           * Use address of requestor for correspondence for the versioned_casetype
           */
          address_requestor_use_as_correspondence?: boolean;
          /**
           * List of allowed requestor types
           */
          allowed_requestor_types?: string[];
          /**
           * Api preset requestor for the versioned_casetype
           */
          api_preset_assignee?: {
            /**
             * Name of the subject relation
             */
            name: string;
            /**
             * Type of the subject relation
             */
            type?: 'employee';
            /**
             * Uuid of the subject relation
             */
            uuid: string;
            [k: string]: any;
          };
          /**
           * Preset requestor of the versioned_casetype
           */
          preset_requestor?:
            | {
                /**
                 * Name of the subject relation
                 */
                name: string;
                /**
                 * Type of the subject relation
                 */
                type?: 'person';
                /**
                 * Uuid of the subject relation
                 */
                uuid: string;
                [k: string]: any;
              }
            | {
                /**
                 * Name of the subject relation
                 */
                name: string;
                /**
                 * Type of the subject relation
                 */
                type?: 'organization';
                /**
                 * Uuid of the subject relation
                 */
                uuid: string;
                [k: string]: any;
              };
          /**
           * Trigger details for the versioned_casetype
           */
          trigger?: 'extern' | 'intern' | 'internextern';
          [k: string]: any;
        };
        /**
         * Results for the versioned_casetype
         */
        results?: {
          /**
           * Archive nomination
           */
          archival_nomination:
            | 'Bewaren (B)'
            | 'Conversie'
            | 'Migratie'
            | 'Overbrengen (O)'
            | 'Overdracht'
            | 'Publicatie'
            | 'Vernietigen (V)'
            | 'Vernietigen'
            | 'Vervallen beperkingen openbaarheid';
          /**
           * Arhive nomination valuation
           */
          archival_nomination_valuation:
            | 'vervallen'
            | 'onherroepelijk'
            | 'afhandeling'
            | 'verwerking'
            | 'geweigerd'
            | 'verleend'
            | 'geboorte'
            | 'einde-dienstverband';
          /**
           * Achival trigger is activated
           */
          archival_trigger?: boolean;
          /**
           * Comments for this result
           */
          comments?: string;
          /**
           * Is default result
           */
          is_default?: boolean;
          /**
           * Name of the result
           */
          name?: string;
          /**
           * Origin for this result
           */
          origin?:
            | 'Specifiek benoemde wet- en regelgeving'
            | 'Systeemanalyse'
            | 'Trendanalyse'
            | 'Risicoanalyse';
          /**
           * Process period for this result
           */
          process_period?: 'A' | 'B' | 'C' | 'D' | 'E';
          /**
           * Processtype description
           */
          processtype_description?: string;
          /**
           * Processtype explanation
           */
          processtype_explanation?: string;
          /**
           * Processtype name
           */
          processtype_name?: string;
          /**
           * Processtype number
           */
          processtype_number?: string;
          /**
           * Processtype object
           */
          processtype_object?: string;
          /**
           * Procestype generic
           */
          procestype_generic?: 'Generiek' | 'Specifiek';
          /**
           * Type of result
           */
          result_type:
            | 'aangegaan'
            | 'aangehouden'
            | 'aangekocht'
            | 'aangesteld'
            | 'aanvaard'
            | 'afgeboekt'
            | 'afgebroken'
            | 'afgehandeld'
            | 'afgesloten'
            | 'afgewezen'
            | 'akkoord'
            | 'akkoord met wijzigingen'
            | 'behaald'
            | 'betaald'
            | 'beëindigd'
            | 'buiten behandeling gesteld'
            | 'definitief toegekend'
            | 'geannuleerd'
            | 'gedeeltelijk gegrond'
            | 'gedeeltelijk verleend'
            | 'gedoogd'
            | 'gegrond'
            | 'gegund'
            | 'geleverd'
            | 'geweigerd'
            | 'gewijzigd'
            | 'geïnd'
            | 'handhaving uitgevoerd'
            | 'ingericht'
            | 'ingeschreven'
            | 'ingesteld'
            | 'ingetrokken'
            | 'ingewilligd'
            | 'niet aangekocht'
            | 'niet aangesteld'
            | 'niet akkoord'
            | 'niet behaald'
            | 'niet betaald'
            | 'niet doorgegaan'
            | 'niet gegund'
            | 'niet geleverd'
            | 'niet gewijzigd'
            | 'niet geïnd'
            | 'niet ingesteld'
            | 'niet ingetrokken'
            | 'niet nodig'
            | 'niet ontvankelijk'
            | 'niet opgelegd'
            | 'niet opgeleverd'
            | 'niet toegekend'
            | 'niet uitgevoerd'
            | 'niet vastgesteld'
            | 'niet verkregen'
            | 'niet verleend'
            | 'niet verstrekt'
            | 'niet verwerkt'
            | 'ongegrond'
            | 'ontvankelijk'
            | 'opgeheven'
            | 'opgelegd'
            | 'opgeleverd'
            | 'opgelost'
            | 'opgezegd'
            | 'toegekend'
            | 'toezicht uitgevoerd'
            | 'uitgevoerd'
            | 'vastgesteld'
            | 'verhuurd'
            | 'verkocht'
            | 'verkregen'
            | 'verleend'
            | 'vernietigd'
            | 'verstrekt'
            | 'verwerkt'
            | 'voorlopig toegekend'
            | 'voorlopig verleend';
          /**
           * Retention period in days.
           */
          retention_period?: number;
          /**
           * Selection list
           */
          selection_list?: string;
          /**
           * Selection list end date
           */
          selection_list_end_date?: string;
          /**
           * Selection list number
           */
          selection_list_number?: string;
          /**
           * Selection list source date
           */
          selection_list_source_date?: string;
          [k: string]: any;
        }[];
        /**
         * Internal identifier of the versioned_casetype
         */
        uuid: string;
        /**
         * WebForm attributes for the versioned_casetype
         */
        webform: {
          /**
           * Actions for the versioned_casetype
           */
          actions?: {
            /**
             * Address check enabled for the versioned_casetype
             */
            address_check?: boolean;
            /**
             * Case creation delayed enabled for the versioned_casetype
             */
            create_delayed?: boolean;
            /**
             * Disable captcha for the versioned_casetype
             */
            disable_captcha?: boolean;
            /**
             * Email required enabled for the versioned_casetype
             */
            email_required?: boolean;
            /**
             * Manual payment enabled for the versioned_casetype
             */
            enable_manual_payment?: boolean;
            /**
             * Online payment enabled for the versioned_casetype
             */
            enable_online_payment?: boolean;
            /**
             * Is webform enabled for the versioned_casetype
             */
            enable_webform?: boolean;
            /**
             * Generate PDF at end of webform for the versioned_casetype
             */
            generate_pdf_end_webform?: boolean;
            /**
             * Mobile required enabled for the versioned_casetype
             */
            mobile_required?: boolean;
            /**
             * Phone required enabled for the versioned_casetype
             */
            phone_required?: boolean;
            /**
             * Reuse case data enabled for the versioned_casetype
             */
            reuse_casedata?: boolean;
            [k: string]: any;
          };
          /**
           * Case location message for the versioned_casetype
           */
          case_location_message?: string;
          /**
           * Pip view message for the versioned_casetype
           */
          pip_view_message?: string;
          /**
           * Pricing information for the versioned_casetype
           */
          price?: {
            /**
             * Price assignee for the versioned_casetype
             */
            assignee?: string;
            /**
             * Price email for the versioned_casetype
             */
            email?: string;
            /**
             * Price frontdesk for the versioned_casetype
             */
            frontdesk?: string;
            /**
             * Price phone for the versioned_casetype
             */
            phone?: string;
            /**
             * Price post for the versioned_casetype
             */
            post?: string;
            /**
             * Price web for the versioned_casetype
             */
            web?: string;
            [k: string]: any;
          };
          /**
           * Public confirmation message for the versioned_casetype
           */
          public_confirmation_message?: string;
          /**
           * Public confirmation title for the versioned_casetype
           */
          public_confirmation_title?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'versioned_casetype';
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateVersionedCasetypeRequestParams {
    [k: string]: any;
  }

  export interface CreateVersionedCasetypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateVersionedCasetypeRequestBody {
    casetype_uuid?: string;
    casetype_version_uuid?: string;
    catalog_folder_uuid?: string;
    active: boolean;
    general_attributes?: {
      /**
       * Case public summary for versioned_casetype
       */
      case_public_summary?: string;
      /**
       * Case summary for versioned_casetype
       */
      case_summary?: string;
      /**
       * Description for versioned_casetype
       */
      description?: string;
      /**
       * Identification of the versioned_casetype
       */
      identification?: string;
      /**
       * Legal period for the versioned_casetype
       */
      legal_period?: {
        /**
         * Type of monitoring period
         */
        type?: 'kalenderdagen' | 'werkdagen' | 'weken' | 'einddatum';
        /**
         * Value for the monitoring period
         */
        value?: string;
        [k: string]: any;
      };
      /**
       * Name of the versioned_casetype
       */
      name?: string;
      /**
       * Service period for the versioned_casetype
       */
      service_period?: {
        /**
         * Type of monitoring period
         */
        type?: 'kalenderdagen' | 'werkdagen' | 'weken' | 'einddatum';
        /**
         * Value for the monitoring period
         */
        value?: string;
        [k: string]: any;
      };
      /**
       * Tags for versioned_casetype
       */
      tags?: string;
      [k: string]: any;
    };
    documentation?: {
      /**
       * Adjourn periond of the versioned_casetype
       */
      adjourn_period?: number;
      /**
       * Archive classification code of the versioned_casetype
       */
      archive_classification_code?: string;
      /**
       * BAG enabled for the versioned_casetype
       */
      bag?: boolean;
      /**
       * Designation of confidentiality of the versioned_casetype
       */
      designation_of_confidentiality?:
        | 'Openbaar'
        | 'Beperkt openbaar'
        | 'Intern'
        | 'Zaakvertrouwelijk'
        | 'Vertrouwelijk'
        | 'Confidentieel'
        | 'Geheim'
        | 'Zeer geheim';
      /**
       * E-webform for versioned_casetype
       */
      e_webform?: string;
      /**
       * Extention period of the versioned_casetype
       */
      extension_period?: number;
      /**
       * GDPR settings for this versioned_casetype
       */
      gdpr?: {
        /**
         * GDPR enabled for the versioned_casetype
         */
        enabled: boolean;
        /**
         * Kind of GDPR
         */
        kind: {
          /**
           * GDPR basic details enabled of the versioned_casetype
           */
          basic_details: boolean;
          /**
           * GDPR criminal record enabled of the versioned_casetype
           */
          criminal_record: boolean;
          /**
           * GDPR genetic or biometric data enabled of the versioned_casetype
           */
          genetic_or_biometric_data: boolean;
          /**
           * GDPR health enabled of the versioned_casetype
           */
          health: boolean;
          /**
           * GDPR income enabled of the versioned_casetype
           */
          income: boolean;
          /**
           * GDPR membership union enabled of the versioned_casetype
           */
          membership_union: boolean;
          /**
           * GDPR  offspring enabled of the versioned_casetype
           */
          offspring: boolean;
          /**
           * GDPR id number enabled of the versioned_casetype
           */
          personal_id_number: boolean;
          /**
           * GDPR political views enabled of the versioned_casetype
           */
          political_views: boolean;
          /**
           * GDPR race or ethniticy enabled of the versioned_casetype
           */
          race_or_ethniticy: boolean;
          /**
           * GDPR religion enabled of the versioned_casetype
           */
          religion: boolean;
          /**
           * GDPR sexual identity enabled of the versioned_casetype
           */
          sexual_identity: boolean;
          [k: string]: any;
        };
        /**
         * Proccess foreign country enabled of the versioned_casetype
         */
        process_foreign_country: boolean;
        /**
         * Process foregein country reason for versioned_casetype
         */
        process_foreign_country_reason?: string;
        /**
         * Processing label for versioned_casetype
         */
        processing_legal?:
          | 'Toestemming'
          | 'Overeenkomst'
          | 'Wettelijke verplichting'
          | 'Publiekrechtelijke taak'
          | 'Vitaal belang'
          | 'Gerechtvaardigd belang';
        /**
         * Processing legal reason for versioned_casetype
         */
        processing_legal_reason?: string;
        /**
         * Processing type of versioned_casetype
         */
        processing_type?: 'Delen' | 'Muteren' | 'Raadplegen';
        /**
         * Source of GDPR
         */
        source: {
          /**
           * GDPR partner enabled of the versioned_casetype
           */
          partner: boolean;
          /**
           * GDPR public source enabled of the versioned_casetype
           */
          public_source: boolean;
          /**
           * GDPR registration enabled of the versioned_casetype
           */
          registration: boolean;
          /**
           * GDPR sender enabled of the versioned_casetype
           */
          sender: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Initiator type of the {ENTITY_TYPE}
       */
      initiator_type?:
        | 'aangaan'
        | 'aangeven'
        | 'aanmelden'
        | 'aanschrijven'
        | 'aanvragen'
        | 'afkopen'
        | 'afmelden'
        | 'indienen'
        | 'inschrijven'
        | 'melden'
        | 'ontvangen'
        | 'opstellen'
        | 'opzeggen'
        | 'registreren'
        | 'reserveren'
        | 'starten'
        | 'stellen'
        | 'uitvoeren'
        | 'vaststellen'
        | 'versturen'
        | 'voordragen'
        | 'vragen';
      /**
       * Legal basis for versioned_casetype
       */
      legal_basis?: string;
      /**
       * Lex silencio positivo enable for the versioned_casetype
       */
      lex_silencio_positivo?: boolean;
      /**
       * Local basis for versioned_casetype
       */
      local_basis?: string;
      /**
       * May extend enabled for the versioned_casetype
       */
      may_extend?: boolean;
      /**
       * May postpone enabled for the versioned_casetype
       */
      may_postpone?: boolean;
      /**
       * Motivation of the versioned_casetype
       */
      motivation?: string;
      /**
       * Penalty law of the versioned_casetype
       */
      penalty_law?: boolean;
      /**
       * Possibility for objection and_appeal for the versioned_casetype
       */
      possibility_for_objection_and_appeal?: boolean;
      /**
       * Process description of the versioned_casetype
       */
      process_description?: string;
      /**
       * Publication for the versioned_casetype
       */
      publication?: boolean;
      /**
       * Publication text for versioned_casetype
       */
      publication_text?: string;
      /**
       * Purpose of the versioned_casetype
       */
      purpose?: string;
      /**
       * Responsible relationship of the versioned_casetype
       */
      responsible_relationship?: string;
      /**
       * Resposible subject of the versioned_casetype
       */
      responsible_subject?: string;
      /**
       * WKPB applies for the versioned_casetype
       */
      wkpb_applies?: boolean;
      [k: string]: any;
    };
    relations?: {
      /**
       * Show address requestor on map for this versioned_casetype
       */
      address_requestor_show_on_map?: boolean;
      /**
       * Use address of requestor as case address for this versioned_casetype
       */
      address_requestor_use_as_case_address?: boolean;
      /**
       * Use address of requestor for correspondence for the versioned_casetype
       */
      address_requestor_use_as_correspondence?: boolean;
      /**
       * List of allowed requestor types
       */
      allowed_requestor_types?: string[];
      /**
       * Api preset requestor for the versioned_casetype
       */
      api_preset_assignee?: {
        /**
         * Name of the subject relation
         */
        name: string;
        /**
         * Type of the subject relation
         */
        type?: 'employee';
        /**
         * Uuid of the subject relation
         */
        uuid: string;
        [k: string]: any;
      };
      /**
       * Preset requestor of the versioned_casetype
       */
      preset_requestor?:
        | {
            /**
             * Name of the subject relation
             */
            name: string;
            /**
             * Type of the subject relation
             */
            type?: 'person';
            /**
             * Uuid of the subject relation
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Name of the subject relation
             */
            name: string;
            /**
             * Type of the subject relation
             */
            type?: 'organization';
            /**
             * Uuid of the subject relation
             */
            uuid: string;
            [k: string]: any;
          };
      /**
       * Trigger details for the versioned_casetype
       */
      trigger?: 'extern' | 'intern' | 'internextern';
      [k: string]: any;
    };
    webform?: {
      /**
       * Actions for the versioned_casetype
       */
      actions?: {
        /**
         * Address check enabled for the versioned_casetype
         */
        address_check?: boolean;
        /**
         * Case creation delayed enabled for the versioned_casetype
         */
        create_delayed?: boolean;
        /**
         * Disable captcha for the versioned_casetype
         */
        disable_captcha?: boolean;
        /**
         * Email required enabled for the versioned_casetype
         */
        email_required?: boolean;
        /**
         * Manual payment enabled for the versioned_casetype
         */
        enable_manual_payment?: boolean;
        /**
         * Online payment enabled for the versioned_casetype
         */
        enable_online_payment?: boolean;
        /**
         * Is webform enabled for the versioned_casetype
         */
        enable_webform?: boolean;
        /**
         * Generate PDF at end of webform for the versioned_casetype
         */
        generate_pdf_end_webform?: boolean;
        /**
         * Mobile required enabled for the versioned_casetype
         */
        mobile_required?: boolean;
        /**
         * Phone required enabled for the versioned_casetype
         */
        phone_required?: boolean;
        /**
         * Reuse case data enabled for the versioned_casetype
         */
        reuse_casedata?: boolean;
        [k: string]: any;
      };
      /**
       * Case location message for the versioned_casetype
       */
      case_location_message?: string;
      /**
       * Pip view message for the versioned_casetype
       */
      pip_view_message?: string;
      /**
       * Pricing information for the versioned_casetype
       */
      price?: {
        /**
         * Price assignee for the versioned_casetype
         */
        assignee?: string;
        /**
         * Price email for the versioned_casetype
         */
        email?: string;
        /**
         * Price frontdesk for the versioned_casetype
         */
        frontdesk?: string;
        /**
         * Price phone for the versioned_casetype
         */
        phone?: string;
        /**
         * Price post for the versioned_casetype
         */
        post?: string;
        /**
         * Price web for the versioned_casetype
         */
        web?: string;
        [k: string]: any;
      };
      /**
       * Public confirmation message for the versioned_casetype
       */
      public_confirmation_message?: string;
      /**
       * Public confirmation title for the versioned_casetype
       */
      public_confirmation_title?: string;
      [k: string]: any;
    };
    registrationform?: {
      /**
       * Allow add relations for the versioned_casetype
       */
      allow_add_relations?: boolean;
      /**
       * Allow assigning for the versioned_casetype
       */
      allow_assigning?: boolean;
      /**
       * Allow assigning to self for the versioned_casetype
       */
      allow_assigning_to_self?: boolean;
      /**
       * Show confidentionality for the versioned_casetype
       */
      show_confidentionality?: boolean;
      /**
       * Show contact details for the versioned_casetype
       */
      show_contact_details?: boolean;
      [k: string]: any;
    };
    case_dossier?: {
      /**
       * Allow external task assignment this versioned_casetype
       */
      allow_external_task_assignment?: boolean;
      /**
       * Default document folders for this versioned_casetype
       */
      default_document_folders?: string[];
      /**
       * Default html template for this versioned_casetype
       */
      default_html_email_template?: string;
      /**
       * Disable pip for requestor for this versioned_casetype
       */
      disable_pip_for_requestor?: boolean;
      /**
       * Lock registration phase for this versioned_casetype
       */
      lock_registration_phase?: boolean;
      /**
       * Queue co worker changes for this versioned_casetype
       */
      queue_coworker_changes?: boolean;
      [k: string]: any;
    };
    api?: {
      /**
       * Api can transition for this versioned_casetype
       */
      api_can_transition?: boolean;
      /**
       * Api notifications for the versioned_casetype
       */
      notifications?: {
        /**
         * External notification on allocation of case for this versioned_casetype
         */
        external_notify_on_allocate_case?: boolean;
        /**
         * External notification on exceed term for this versioned_casetype
         */
        external_notify_on_exceed_term?: boolean;
        /**
         * External notification on label change for this versioned_casetype
         */
        external_notify_on_label_change?: boolean;
        /**
         * External notification on new case for this versioned_casetype
         */
        external_notify_on_new_case?: boolean;
        /**
         * External notification on new document for this versioned_casetype
         */
        external_notify_on_new_document?: boolean;
        /**
         * External notification on new message for this versioned_casetype
         */
        external_notify_on_new_message?: boolean;
        /**
         * External notification on phase transition for this versioned_casetype
         */
        external_notify_on_phase_transition?: boolean;
        /**
         * External notification on subject change for this versioned_casetype
         */
        external_notify_on_subject_change?: boolean;
        /**
         * External notification on task change for this versioned_casetype
         */
        external_notify_on_task_change?: boolean;
        [k: string]: any;
      };
      [k: string]: any;
    };
    authorization?: {
      /**
       * Indicator if this authorization entry is confidential
       */
      confidential: boolean;
      /**
       * Department uuid of this authorization entry
       */
      department_uuid: string;
      /**
       * Rights configured for this authorization entry. Possible values are 'zaak_beheer', 'zaak_edit', 'zaak_read', 'zaak_search'
       */
      rights: string[];
      /**
       * Role uuid of this authorization entry
       */
      role_uuid: string;
      [k: string]: any;
    }[];
    child_casetype_settings?: {
      /**
       * List of child casetypes and settings
       */
      child_casetypes: {
        /**
         * Child casetype
         */
        casetype: {
          /**
           * Name of the child casetype
           */
          name: string;
          /**
           * Uuid of the child casetype
           */
          uuid: string;
          [k: string]: any;
        };
        /**
         * Flag that indicates if this child casetype is enabled
         */
        enabled: boolean;
        /**
         * settings for the child casetype
         */
        settings: {
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      /**
       * Flag if mother casetype settings are enabled
       */
      enabled?: boolean;
      [k: string]: any;
    };
    change_log?: {
      /**
       * List of components that have been changed for {ENTITY_TYPE}
       */
      update_components: string[];
      /**
       * Description of what changes have been made for versioned_casetype
       */
      update_description?: string;
      [k: string]: any;
    };
    phases?: {
      /**
       * Assignment settings for the phase
       */
      assignment?: {
        /**
         * Department name for this phase assignment
         */
        department_name: string;
        /**
         * Department uuid for this phase assignment
         */
        department_uuid: string;
        /**
         * Indicator if the phase assignment is enabled
         */
        enabled: boolean;
        /**
         * Role name for this phase assignment
         */
        role_name: string;
        /**
         * Role uuid for this phase assignment
         */
        role_uuid: string;
        [k: string]: any;
      };
      /**
       * Create datetime of the phase
       */
      created?: string;
      /**
       * Custom fields for the phase
       */
      custom_fields?: (
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_straat_adres';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_adres';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_straat_adressen';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_adressen';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'address_v2';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * WMS feature attribute id for this attribute
             */
            map_wms_feature_attribute_id?: string;
            /**
             * WMS feature attribute label for this attribute
             */
            map_wms_feature_attribute_label?: string;
            /**
             * WMS layer id for this attribute
             */
            map_wms_layer_id?: string;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'appointment';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'appointment_v2';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bankaccount';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'calendar';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'calendar_supersaas';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'checkbox';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'date';
            /**
             * End date limitation for date attribute
             */
            end_date_limitation?: {
              /**
               * Active indicator for date limitation
               */
              active: boolean;
              /**
               * During for the date limitation [pre | post]
               */
              during: string;
              /**
               * Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute
               */
              reference: string | 'currentDate';
              /**
               * Unit for date limitation [days | weeks | months | years]
               */
              term: string;
              /**
               * Value for date limitation
               */
              value: number;
              [k: string]: any;
            };
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Start date limitation for date attribute
             */
            start_date_limitation?: {
              /**
               * Active indicator for date limitation
               */
              active: boolean;
              /**
               * During for the date limitation [pre | post]
               */
              during: string;
              /**
               * Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute
               */
              reference: string | 'currentDate';
              /**
               * Unit for date limitation [days | weeks | months | years]
               */
              term: string;
              /**
               * Value for date limitation
               */
              value: number;
              [k: string]: any;
            };
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'email';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'file';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'geojson';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'googlemaps';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute Type
             */
            attribute_type: 'group';
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'image_from_url';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'geolatlon';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Indicator if attribute must be used as case location
             */
            map_case_location: boolean;
            /**
             * WMS feature attribute uuid for this attribute
             */
            map_wms_feature_attribute_id?: string;
            /**
             * WMS feature attribute label for this attribute
             */
            map_wms_feature_attribute_label?: string;
            /**
             * WMS layer id for this attribute
             */
            map_wms_layer_id?: string;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'numeric';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_openbareruimte';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_openbareruimtes';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'option';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'richtext';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'select';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'subject';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'textarea';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'text';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute Type
             */
            attribute_type: 'textblock';
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'text_uc';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'url';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valuta';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutaex21';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutaex6';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutaex';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutain21';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutain6';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutain';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'relationship';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Selected role for subject relation
             */
            relationship_subject_role?: string;
            /**
             * Indicator for the relationship type
             */
            relationship_type?: 'subject';
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'relationship';
            /**
             * Attribute mapping for object creation
             */
            create_custom_object_attribute_mapping?: {
              [k: string]: any;
            };
            /**
             * Is object creation enabled
             */
            create_custom_object_enabled: boolean;
            /**
             * Label for object creation
             */
            create_custom_object_label?: string;
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator for the relationship type
             */
            relationship_type?: 'custom_object';
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'relationship';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator for the relationship type. This case relation type is not supported anymore.
             */
            relationship_type?: 'case';
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
      )[];
      /**
       * Email templates for this phase
       */
      email_templates?: (
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Reciever
             */
            reciever:
              | 'aanvrager'
              | 'zaak_behandelaar'
              | 'coordinator'
              | 'gemachtigde';
            /**
             * Send Automatic
             */
            send_automatic: string;
            [k: string]: any;
          }
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Reciever
             */
            reciever: 'overig';
            /**
             * Send Automatic
             */
            send_automatic: string;
            /**
             * To
             */
            to: string;
            [k: string]: any;
          }
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Person Involved Role
             */
            person_involved_role: string;
            /**
             * Reciever
             */
            reciever: 'betrokkene';
            /**
             * Send Automatic
             */
            send_automatic: string;
            [k: string]: any;
          }
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Reciever
             */
            reciever: 'behandelaar';
            /**
             * Send Automatic
             */
            send_automatic: string;
            /**
             * Introspectable object based on pydantic
             */
            subject: {
              /**
               * Name of the subject relation
               */
              name: string;
              /**
               * Type of the subject relation
               */
              type?: 'employee';
              /**
               * Uuid of the subject relation
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }
      )[];
      /**
       * Last modified datetime of the phase
       */
      last_modified?: string;
      /**
       * Name of the phase milestone
       */
      milestone_name: string;
      /**
       * Name of the phase
       */
      phase_name: string;
      /**
       * Configured tasks for the phase
       */
      tasks?: string[];
      /**
       * Templates for this phase.
       */
      templates?: {
        /**
         * Allow editing the filename
         */
        allow_edit?: boolean;
        /**
         * Autogenerate the template document on phase transition
         */
        auto_generate?: boolean;
        /**
         * Name of the document template from the catalog
         */
        catalog_template_name?: string;
        /**
         * UUID of the document_tempate from the catalog
         */
        catalog_template_uuid?: string;
        /**
         * Filename for the generated document
         */
        custom_filename?: string;
        /**
         * Description of the template
         */
        description?: string;
        /**
         * Name of the attribute where the generated document will be added.
         */
        document_attribute_name?: string;
        /**
         * UUID of the attribute where the generated document will be added.
         */
        document_attribute_uuid?: string;
        /**
         * Label of the template
         */
        label?: string;
        /**
         * Target format of the template
         */
        target_format?: 'odt' | 'pdf' | 'docx';
        [k: string]: any;
      }[];
      /**
       * Term in days for milestone
       */
      term_in_days: number;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  export interface UpdateVersionedCasetypeRequestParams {
    [k: string]: any;
  }

  export interface UpdateVersionedCasetypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateVersionedCasetypeRequestBody {
    casetype_uuid: string;
    casetype_version_uuid?: string;
    catalog_folder_uuid?: string;
    active: boolean;
    general_attributes?: {
      /**
       * Case public summary for versioned_casetype
       */
      case_public_summary?: string;
      /**
       * Case summary for versioned_casetype
       */
      case_summary?: string;
      /**
       * Description for versioned_casetype
       */
      description?: string;
      /**
       * Identification of the versioned_casetype
       */
      identification?: string;
      /**
       * Legal period for the versioned_casetype
       */
      legal_period?: {
        /**
         * Type of monitoring period
         */
        type?: 'kalenderdagen' | 'werkdagen' | 'weken' | 'einddatum';
        /**
         * Value for the monitoring period
         */
        value?: string;
        [k: string]: any;
      };
      /**
       * Name of the versioned_casetype
       */
      name?: string;
      /**
       * Service period for the versioned_casetype
       */
      service_period?: {
        /**
         * Type of monitoring period
         */
        type?: 'kalenderdagen' | 'werkdagen' | 'weken' | 'einddatum';
        /**
         * Value for the monitoring period
         */
        value?: string;
        [k: string]: any;
      };
      /**
       * Tags for versioned_casetype
       */
      tags?: string;
      [k: string]: any;
    };
    documentation?: {
      /**
       * Adjourn periond of the versioned_casetype
       */
      adjourn_period?: number;
      /**
       * Archive classification code of the versioned_casetype
       */
      archive_classification_code?: string;
      /**
       * BAG enabled for the versioned_casetype
       */
      bag?: boolean;
      /**
       * Designation of confidentiality of the versioned_casetype
       */
      designation_of_confidentiality?:
        | 'Openbaar'
        | 'Beperkt openbaar'
        | 'Intern'
        | 'Zaakvertrouwelijk'
        | 'Vertrouwelijk'
        | 'Confidentieel'
        | 'Geheim'
        | 'Zeer geheim';
      /**
       * E-webform for versioned_casetype
       */
      e_webform?: string;
      /**
       * Extention period of the versioned_casetype
       */
      extension_period?: number;
      /**
       * GDPR settings for this versioned_casetype
       */
      gdpr?: {
        /**
         * GDPR enabled for the versioned_casetype
         */
        enabled: boolean;
        /**
         * Kind of GDPR
         */
        kind: {
          /**
           * GDPR basic details enabled of the versioned_casetype
           */
          basic_details: boolean;
          /**
           * GDPR criminal record enabled of the versioned_casetype
           */
          criminal_record: boolean;
          /**
           * GDPR genetic or biometric data enabled of the versioned_casetype
           */
          genetic_or_biometric_data: boolean;
          /**
           * GDPR health enabled of the versioned_casetype
           */
          health: boolean;
          /**
           * GDPR income enabled of the versioned_casetype
           */
          income: boolean;
          /**
           * GDPR membership union enabled of the versioned_casetype
           */
          membership_union: boolean;
          /**
           * GDPR  offspring enabled of the versioned_casetype
           */
          offspring: boolean;
          /**
           * GDPR id number enabled of the versioned_casetype
           */
          personal_id_number: boolean;
          /**
           * GDPR political views enabled of the versioned_casetype
           */
          political_views: boolean;
          /**
           * GDPR race or ethniticy enabled of the versioned_casetype
           */
          race_or_ethniticy: boolean;
          /**
           * GDPR religion enabled of the versioned_casetype
           */
          religion: boolean;
          /**
           * GDPR sexual identity enabled of the versioned_casetype
           */
          sexual_identity: boolean;
          [k: string]: any;
        };
        /**
         * Proccess foreign country enabled of the versioned_casetype
         */
        process_foreign_country: boolean;
        /**
         * Process foregein country reason for versioned_casetype
         */
        process_foreign_country_reason?: string;
        /**
         * Processing label for versioned_casetype
         */
        processing_legal?:
          | 'Toestemming'
          | 'Overeenkomst'
          | 'Wettelijke verplichting'
          | 'Publiekrechtelijke taak'
          | 'Vitaal belang'
          | 'Gerechtvaardigd belang';
        /**
         * Processing legal reason for versioned_casetype
         */
        processing_legal_reason?: string;
        /**
         * Processing type of versioned_casetype
         */
        processing_type?: 'Delen' | 'Muteren' | 'Raadplegen';
        /**
         * Source of GDPR
         */
        source: {
          /**
           * GDPR partner enabled of the versioned_casetype
           */
          partner: boolean;
          /**
           * GDPR public source enabled of the versioned_casetype
           */
          public_source: boolean;
          /**
           * GDPR registration enabled of the versioned_casetype
           */
          registration: boolean;
          /**
           * GDPR sender enabled of the versioned_casetype
           */
          sender: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Initiator type of the {ENTITY_TYPE}
       */
      initiator_type?:
        | 'aangaan'
        | 'aangeven'
        | 'aanmelden'
        | 'aanschrijven'
        | 'aanvragen'
        | 'afkopen'
        | 'afmelden'
        | 'indienen'
        | 'inschrijven'
        | 'melden'
        | 'ontvangen'
        | 'opstellen'
        | 'opzeggen'
        | 'registreren'
        | 'reserveren'
        | 'starten'
        | 'stellen'
        | 'uitvoeren'
        | 'vaststellen'
        | 'versturen'
        | 'voordragen'
        | 'vragen';
      /**
       * Legal basis for versioned_casetype
       */
      legal_basis?: string;
      /**
       * Lex silencio positivo enable for the versioned_casetype
       */
      lex_silencio_positivo?: boolean;
      /**
       * Local basis for versioned_casetype
       */
      local_basis?: string;
      /**
       * May extend enabled for the versioned_casetype
       */
      may_extend?: boolean;
      /**
       * May postpone enabled for the versioned_casetype
       */
      may_postpone?: boolean;
      /**
       * Motivation of the versioned_casetype
       */
      motivation?: string;
      /**
       * Penalty law of the versioned_casetype
       */
      penalty_law?: boolean;
      /**
       * Possibility for objection and_appeal for the versioned_casetype
       */
      possibility_for_objection_and_appeal?: boolean;
      /**
       * Process description of the versioned_casetype
       */
      process_description?: string;
      /**
       * Publication for the versioned_casetype
       */
      publication?: boolean;
      /**
       * Publication text for versioned_casetype
       */
      publication_text?: string;
      /**
       * Purpose of the versioned_casetype
       */
      purpose?: string;
      /**
       * Responsible relationship of the versioned_casetype
       */
      responsible_relationship?: string;
      /**
       * Resposible subject of the versioned_casetype
       */
      responsible_subject?: string;
      /**
       * WKPB applies for the versioned_casetype
       */
      wkpb_applies?: boolean;
      [k: string]: any;
    };
    relations?: {
      /**
       * Show address requestor on map for this versioned_casetype
       */
      address_requestor_show_on_map?: boolean;
      /**
       * Use address of requestor as case address for this versioned_casetype
       */
      address_requestor_use_as_case_address?: boolean;
      /**
       * Use address of requestor for correspondence for the versioned_casetype
       */
      address_requestor_use_as_correspondence?: boolean;
      /**
       * List of allowed requestor types
       */
      allowed_requestor_types?: string[];
      /**
       * Api preset requestor for the versioned_casetype
       */
      api_preset_assignee?: {
        /**
         * Name of the subject relation
         */
        name: string;
        /**
         * Type of the subject relation
         */
        type?: 'employee';
        /**
         * Uuid of the subject relation
         */
        uuid: string;
        [k: string]: any;
      };
      /**
       * Preset requestor of the versioned_casetype
       */
      preset_requestor?:
        | {
            /**
             * Name of the subject relation
             */
            name: string;
            /**
             * Type of the subject relation
             */
            type?: 'person';
            /**
             * Uuid of the subject relation
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Name of the subject relation
             */
            name: string;
            /**
             * Type of the subject relation
             */
            type?: 'organization';
            /**
             * Uuid of the subject relation
             */
            uuid: string;
            [k: string]: any;
          };
      /**
       * Trigger details for the versioned_casetype
       */
      trigger?: 'extern' | 'intern' | 'internextern';
      [k: string]: any;
    };
    webform?: {
      /**
       * Actions for the versioned_casetype
       */
      actions?: {
        /**
         * Address check enabled for the versioned_casetype
         */
        address_check?: boolean;
        /**
         * Case creation delayed enabled for the versioned_casetype
         */
        create_delayed?: boolean;
        /**
         * Disable captcha for the versioned_casetype
         */
        disable_captcha?: boolean;
        /**
         * Email required enabled for the versioned_casetype
         */
        email_required?: boolean;
        /**
         * Manual payment enabled for the versioned_casetype
         */
        enable_manual_payment?: boolean;
        /**
         * Online payment enabled for the versioned_casetype
         */
        enable_online_payment?: boolean;
        /**
         * Is webform enabled for the versioned_casetype
         */
        enable_webform?: boolean;
        /**
         * Generate PDF at end of webform for the versioned_casetype
         */
        generate_pdf_end_webform?: boolean;
        /**
         * Mobile required enabled for the versioned_casetype
         */
        mobile_required?: boolean;
        /**
         * Phone required enabled for the versioned_casetype
         */
        phone_required?: boolean;
        /**
         * Reuse case data enabled for the versioned_casetype
         */
        reuse_casedata?: boolean;
        [k: string]: any;
      };
      /**
       * Case location message for the versioned_casetype
       */
      case_location_message?: string;
      /**
       * Pip view message for the versioned_casetype
       */
      pip_view_message?: string;
      /**
       * Pricing information for the versioned_casetype
       */
      price?: {
        /**
         * Price assignee for the versioned_casetype
         */
        assignee?: string;
        /**
         * Price email for the versioned_casetype
         */
        email?: string;
        /**
         * Price frontdesk for the versioned_casetype
         */
        frontdesk?: string;
        /**
         * Price phone for the versioned_casetype
         */
        phone?: string;
        /**
         * Price post for the versioned_casetype
         */
        post?: string;
        /**
         * Price web for the versioned_casetype
         */
        web?: string;
        [k: string]: any;
      };
      /**
       * Public confirmation message for the versioned_casetype
       */
      public_confirmation_message?: string;
      /**
       * Public confirmation title for the versioned_casetype
       */
      public_confirmation_title?: string;
      [k: string]: any;
    };
    registrationform?: {
      /**
       * Allow add relations for the versioned_casetype
       */
      allow_add_relations?: boolean;
      /**
       * Allow assigning for the versioned_casetype
       */
      allow_assigning?: boolean;
      /**
       * Allow assigning to self for the versioned_casetype
       */
      allow_assigning_to_self?: boolean;
      /**
       * Show confidentionality for the versioned_casetype
       */
      show_confidentionality?: boolean;
      /**
       * Show contact details for the versioned_casetype
       */
      show_contact_details?: boolean;
      [k: string]: any;
    };
    case_dossier?: {
      /**
       * Allow external task assignment this versioned_casetype
       */
      allow_external_task_assignment?: boolean;
      /**
       * Default document folders for this versioned_casetype
       */
      default_document_folders?: string[];
      /**
       * Default html template for this versioned_casetype
       */
      default_html_email_template?: string;
      /**
       * Disable pip for requestor for this versioned_casetype
       */
      disable_pip_for_requestor?: boolean;
      /**
       * Lock registration phase for this versioned_casetype
       */
      lock_registration_phase?: boolean;
      /**
       * Queue co worker changes for this versioned_casetype
       */
      queue_coworker_changes?: boolean;
      [k: string]: any;
    };
    api?: {
      /**
       * Api can transition for this versioned_casetype
       */
      api_can_transition?: boolean;
      /**
       * Api notifications for the versioned_casetype
       */
      notifications?: {
        /**
         * External notification on allocation of case for this versioned_casetype
         */
        external_notify_on_allocate_case?: boolean;
        /**
         * External notification on exceed term for this versioned_casetype
         */
        external_notify_on_exceed_term?: boolean;
        /**
         * External notification on label change for this versioned_casetype
         */
        external_notify_on_label_change?: boolean;
        /**
         * External notification on new case for this versioned_casetype
         */
        external_notify_on_new_case?: boolean;
        /**
         * External notification on new document for this versioned_casetype
         */
        external_notify_on_new_document?: boolean;
        /**
         * External notification on new message for this versioned_casetype
         */
        external_notify_on_new_message?: boolean;
        /**
         * External notification on phase transition for this versioned_casetype
         */
        external_notify_on_phase_transition?: boolean;
        /**
         * External notification on subject change for this versioned_casetype
         */
        external_notify_on_subject_change?: boolean;
        /**
         * External notification on task change for this versioned_casetype
         */
        external_notify_on_task_change?: boolean;
        [k: string]: any;
      };
      [k: string]: any;
    };
    authorization?: {
      /**
       * Indicator if this authorization entry is confidential
       */
      confidential: boolean;
      /**
       * Department uuid of this authorization entry
       */
      department_uuid: string;
      /**
       * Rights configured for this authorization entry. Possible values are 'zaak_beheer', 'zaak_edit', 'zaak_read', 'zaak_search'
       */
      rights: string[];
      /**
       * Role uuid of this authorization entry
       */
      role_uuid: string;
      [k: string]: any;
    }[];
    child_casetype_settings?: {
      /**
       * List of child casetypes and settings
       */
      child_casetypes: {
        /**
         * Child casetype
         */
        casetype: {
          /**
           * Name of the child casetype
           */
          name: string;
          /**
           * Uuid of the child casetype
           */
          uuid: string;
          [k: string]: any;
        };
        /**
         * Flag that indicates if this child casetype is enabled
         */
        enabled: boolean;
        /**
         * settings for the child casetype
         */
        settings: {
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      /**
       * Flag if mother casetype settings are enabled
       */
      enabled?: boolean;
      [k: string]: any;
    };
    change_log?: {
      /**
       * List of components that have been changed for {ENTITY_TYPE}
       */
      update_components: string[];
      /**
       * Description of what changes have been made for versioned_casetype
       */
      update_description?: string;
      [k: string]: any;
    };
    phases?: {
      /**
       * Assignment settings for the phase
       */
      assignment?: {
        /**
         * Department name for this phase assignment
         */
        department_name: string;
        /**
         * Department uuid for this phase assignment
         */
        department_uuid: string;
        /**
         * Indicator if the phase assignment is enabled
         */
        enabled: boolean;
        /**
         * Role name for this phase assignment
         */
        role_name: string;
        /**
         * Role uuid for this phase assignment
         */
        role_uuid: string;
        [k: string]: any;
      };
      /**
       * Create datetime of the phase
       */
      created?: string;
      /**
       * Custom fields for the phase
       */
      custom_fields?: (
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_straat_adres';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_adres';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_straat_adressen';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_adressen';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'address_v2';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * WMS feature attribute id for this attribute
             */
            map_wms_feature_attribute_id?: string;
            /**
             * WMS feature attribute label for this attribute
             */
            map_wms_feature_attribute_label?: string;
            /**
             * WMS layer id for this attribute
             */
            map_wms_layer_id?: string;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'appointment';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'appointment_v2';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bankaccount';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'calendar';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'calendar_supersaas';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'checkbox';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'date';
            /**
             * End date limitation for date attribute
             */
            end_date_limitation?: {
              /**
               * Active indicator for date limitation
               */
              active: boolean;
              /**
               * During for the date limitation [pre | post]
               */
              during: string;
              /**
               * Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute
               */
              reference: string | 'currentDate';
              /**
               * Unit for date limitation [days | weeks | months | years]
               */
              term: string;
              /**
               * Value for date limitation
               */
              value: number;
              [k: string]: any;
            };
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Start date limitation for date attribute
             */
            start_date_limitation?: {
              /**
               * Active indicator for date limitation
               */
              active: boolean;
              /**
               * During for the date limitation [pre | post]
               */
              during: string;
              /**
               * Reference for the date limitation. Valid values are currentDate or the UUID of another date attribute
               */
              reference: string | 'currentDate';
              /**
               * Unit for date limitation [days | weeks | months | years]
               */
              term: string;
              /**
               * Value for date limitation
               */
              value: number;
              [k: string]: any;
            };
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'email';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'file';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'geojson';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'googlemaps';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute Type
             */
            attribute_type: 'group';
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'image_from_url';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'geolatlon';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Indicator if attribute must be used as case location
             */
            map_case_location: boolean;
            /**
             * WMS feature attribute uuid for this attribute
             */
            map_wms_feature_attribute_id?: string;
            /**
             * WMS feature attribute label for this attribute
             */
            map_wms_feature_attribute_label?: string;
            /**
             * WMS layer id for this attribute
             */
            map_wms_layer_id?: string;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'numeric';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_openbareruimte';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'bag_openbareruimtes';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * Use this attribute as case address
             */
            use_as_case_address?: boolean;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'option';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'richtext';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'select';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'subject';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'textarea';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'text';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute Type
             */
            attribute_type: 'textblock';
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'text_uc';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'url';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valuta';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutaex21';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutaex6';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutaex';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutain21';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutain6';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'valutain';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'relationship';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if multiple values is configured
             */
            is_multiple?: boolean;
            /**
             * Label for allowing multiple values
             */
            label_multiple?: string;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Selected role for subject relation
             */
            relationship_subject_role?: string;
            /**
             * Indicator for the relationship type
             */
            relationship_type?: 'subject';
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'relationship';
            /**
             * Attribute mapping for object creation
             */
            create_custom_object_attribute_mapping?: {
              [k: string]: any;
            };
            /**
             * Is object creation enabled
             */
            create_custom_object_enabled: boolean;
            /**
             * Label for object creation
             */
            create_custom_object_label?: string;
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator for the relationship type
             */
            relationship_type?: 'custom_object';
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
        | {
            /**
             * Attribute magic string
             */
            attribute_magic_string?: string;
            /**
             * Attribute name
             */
            attribute_name: string;
            /**
             * Attribute Type
             */
            attribute_type: 'relationship';
            /**
             * External help text for the attribute
             */
            help_extern: string;
            /**
             * Internal help text for the attribute
             */
            help_intern: string;
            /**
             * Indicator if this is a group attribute
             */
            is_group: boolean;
            /**
             * Indicator if the attribute is mandatory
             */
            mandatory: boolean;
            /**
             * Permissions for the attribute
             */
            permissions: {
              /**
               * Department name for the attribute permission
               */
              department_name: string;
              /**
               * Department uuid for the attribute permission
               */
              department_uuid: string;
              /**
               * Role name for the attribute permission
               */
              role_name: string;
              /**
               * Role uuid for the attribute permission
               */
              role_uuid: string;
              [k: string]: any;
            }[];
            /**
             * Indicator if requestor can change attribute value on PIP
             */
            pip_can_change: boolean;
            /**
             * Indicator if this attribute can be published on pip
             */
            publish_pip: boolean;
            /**
             * Indicator if the attribute is referential
             */
            referential: boolean;
            /**
             * Indicator for the relationship type. This case relation type is not supported anymore.
             */
            relationship_type?: 'case';
            /**
             * Show attribute on map
             */
            show_on_map: boolean;
            /**
             * Indicator if change approval is needed
             */
            skip_change_approval: boolean;
            /**
             * Inidcator if this is a system attribute
             */
            system_attribute: boolean;
            /**
             * Title for the attribute
             */
            title: string;
            /**
             * UUID for the attribute
             */
            uuid: string;
            [k: string]: any;
          }
      )[];
      /**
       * Email templates for this phase
       */
      email_templates?: (
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Reciever
             */
            reciever:
              | 'aanvrager'
              | 'zaak_behandelaar'
              | 'coordinator'
              | 'gemachtigde';
            /**
             * Send Automatic
             */
            send_automatic: string;
            [k: string]: any;
          }
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Reciever
             */
            reciever: 'overig';
            /**
             * Send Automatic
             */
            send_automatic: string;
            /**
             * To
             */
            to: string;
            [k: string]: any;
          }
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Person Involved Role
             */
            person_involved_role: string;
            /**
             * Reciever
             */
            reciever: 'betrokkene';
            /**
             * Send Automatic
             */
            send_automatic: string;
            [k: string]: any;
          }
        | {
            /**
             * Bcc
             */
            bcc: string;
            /**
             * Catalog Message Label
             */
            catalog_message_label: string;
            /**
             * Catalog Message Uuid
             */
            catalog_message_uuid: string;
            /**
             * Cc
             */
            cc: string;
            /**
             * Reciever
             */
            reciever: 'behandelaar';
            /**
             * Send Automatic
             */
            send_automatic: string;
            /**
             * Introspectable object based on pydantic
             */
            subject: {
              /**
               * Name of the subject relation
               */
              name: string;
              /**
               * Type of the subject relation
               */
              type?: 'employee';
              /**
               * Uuid of the subject relation
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }
      )[];
      /**
       * Last modified datetime of the phase
       */
      last_modified?: string;
      /**
       * Name of the phase milestone
       */
      milestone_name: string;
      /**
       * Name of the phase
       */
      phase_name: string;
      /**
       * Configured tasks for the phase
       */
      tasks?: string[];
      /**
       * Templates for this phase.
       */
      templates?: {
        /**
         * Allow editing the filename
         */
        allow_edit?: boolean;
        /**
         * Autogenerate the template document on phase transition
         */
        auto_generate?: boolean;
        /**
         * Name of the document template from the catalog
         */
        catalog_template_name?: string;
        /**
         * UUID of the document_tempate from the catalog
         */
        catalog_template_uuid?: string;
        /**
         * Filename for the generated document
         */
        custom_filename?: string;
        /**
         * Description of the template
         */
        description?: string;
        /**
         * Name of the attribute where the generated document will be added.
         */
        document_attribute_name?: string;
        /**
         * UUID of the attribute where the generated document will be added.
         */
        document_attribute_uuid?: string;
        /**
         * Label of the template
         */
        label?: string;
        /**
         * Target format of the template
         */
        target_format?: 'odt' | 'pdf' | 'docx';
        [k: string]: any;
      }[];
      /**
       * Term in days for milestone
       */
      term_in_days: number;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  export interface RenameUserRequestParams {
    [k: string]: any;
  }

  export interface RenameUserResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RenameUserRequestBody {
    dry_run?: string;
    users: {
      old_username: string;
      new_username: string;
      [k: string]: any;
    }[];
    integration_uuid: string;
    [k: string]: any;
  }

  /**
   * Can be any value - string, number, boolean, array or object.
   */
  export interface AnyValue {
    [k: string]: any;
  }

  export interface AttributeDetail {
    type: 'attribute';
    id: string;
    attributes: {
      name?: string;
      magic_string?: string;
      entry_type?: string;
      is_multiple?: boolean;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      used_in_object_types?: {
        id?: string;
        type?: 'object_type';
        attributes?: {
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AttributeDetailForEdit {
    type: string;
    id: string;
    attributes: {
      attribute_type?: string;
      name?: string;
      public_name?: string;
      magic_string?: string;
      type_multiple?: boolean;
      sensitive_field?: boolean;
      help?: string;
      value_default?: string;
      category_name?: string;
      category_uuid?: string;
      document_origin?: string;
      document_trust_level?: string;
      document_source?: string;
      document_category?: string;
      relationship_data?: {
        [k: string]: any;
      };
      attribute_values?: string[];
      /**
       * Can be any value - string, number, boolean, array or object.
       */
      appointment_location_id?: {
        [k: string]: any;
      };
      /**
       * Can be any value - string, number, boolean, array or object.
       */
      appointment_product_id?: {
        [k: string]: any;
      };
      appointment_interface_uuid?: string | null;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AttributeDetailSearch {
    type?: string;
    id?: string;
    attribute?: {
      name?: string;
      magic_string?: string;
      value_type?: string;
      is_multipe?: boolean;
      relationship_type?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CaseTypeDetail {
    type: 'case_type';
    id: string;
    attributes: {
      name?: string;
      active?: boolean;
      api_v1_endpoint?: string;
      stuf_identification?: string;
      current_version?: number;
      last_modified?: string;
      context?: ('internal' | 'external')[];
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      used_in_object_types?: {
        id?: string;
        type?: 'object_type';
        attributes?: {
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    links?: {
      export: string;
      version_history: string;
      registration_form_internal?: string;
    };
    [k: string]: any;
  }

  export interface CatalogEntry {
    type: 'folder_entry';
    id: string;
    attributes: {
      type:
        | 'folder'
        | 'case_type'
        | 'object_type'
        | 'attribute'
        | 'email_template'
        | 'document_template'
        | 'custom_object_type';
      name: string;
      active: boolean;
      [k: string]: any;
    };
    links: {
      self: string;
      meta?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CatalogEntryAttributes {
    type:
      | 'folder'
      | 'case_type'
      | 'object_type'
      | 'attribute'
      | 'email_template'
      | 'document_template'
      | 'custom_object_type';
    name: string;
    active: boolean;
    [k: string]: any;
  }

  export interface CatalogEntryCaseTypeRelation {
    id?: string;
    type?: 'case_type';
    attributes?: {
      name?: string;
      version?: number;
      is_current_version?: boolean;
      active?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type CatalogEntryDetail =
    | {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'case_type';
        id: string;
        attributes: {
          name?: string;
          active?: boolean;
          api_v1_endpoint?: string;
          stuf_identification?: string;
          current_version?: number;
          last_modified?: string;
          context?: ('internal' | 'external')[];
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          used_in_object_types?: {
            id?: string;
            type?: 'object_type';
            attributes?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        links?: {
          export: string;
          version_history: string;
          registration_form_internal?: string;
        };
        [k: string]: any;
      }
    | {
        type: 'object_type';
        id: string;
        attributes: {
          name?: string;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'custom_object_type';
        id: string;
        attributes: {
          name?: string;
          title?: string;
          external_reference?: string;
          status?: string;
          version?: number;
          version_independent_uuid?: string;
          last_modified?: string;
          date_created?: string;
          [k: string]: any;
        };
        relationships: {
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'attribute';
        id: string;
        attributes: {
          name?: string;
          magic_string?: string;
          entry_type?: string;
          is_multiple?: boolean;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          used_in_object_types?: {
            id?: string;
            type?: 'object_type';
            attributes?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'email_template';
        id: string;
        attributes: {
          name?: string;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'document_template';
        id: string;
        attributes: {
          name?: string;
          download_uri?: string;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };

  export interface CatalogEntryLinks {
    self: string;
    meta?: string;
    [k: string]: any;
  }

  export interface CatalogEntryObjectTypeRelation {
    id?: string;
    type?: 'object_type';
    attributes?: {
      name?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DocumentTemplateDetail {
    type: 'document_template';
    id: string;
    attributes: {
      name?: string;
      download_uri?: string;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DocumentTemplateDetailForEdit {
    attributes: {
      category_uuid?:
        | string
        | {
            [k: string]: any;
          }
        | null;
      name?: string;
      interface_uuid?: string;
      template_external_name?: string;
      help?: string;
      [k: string]: any;
    };
    id: string;
    type: 'document_template';
    [k: string]: any;
  }

  export interface EmailTemplateDetail {
    type: 'email_template';
    id: string;
    attributes: {
      name?: string;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EmailTemplateDetailForEdit {
    attributes: {
      attachments?: {
        name?: string;
        uuid?: string;
        [k: string]: any;
      }[];
      category_uuid?:
        | string
        | {
            [k: string]: any;
          }
        | null;
      label?: string;
      message?: string;
      sender?: string;
      sender_address?: string;
      subject?: string;
      [k: string]: any;
    };
    id: string;
    type: 'email_template';
    [k: string]: any;
  }

  export interface FolderDetail {
    type: 'folder';
    id: string;
    attributes: {
      name?: string;
      [k: string]: any;
    };
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface IntegrationDetail {
    type: 'integration';
    id: string;
    attributes: {
      name: string;
      active: boolean;
      module: string;
      legacy_id: number;
      manual_type: ('text' | 'file' | 'references')[];
    };
  }

  export interface TransactionEntity {
    type: 'transaction';
    id: string;
    attributes: {
      id: number;
      external_id: string;
      direction: 'incoming' | 'outgoing';
      created: string;
      last_retry: string | null;
      next_retry: string | null;
      retry_count: number | null;
      error_count: number;
      success_count: number;
      error_fatal: boolean;
      record_count: number;
      processed: boolean;
      error_message: string | null;
    };
    meta?: {
      summary?: string | null;
    };
    relationships: {
      integration?: {
        data?: {
          type?: 'integration';
          id?: string;
          [k: string]: any;
        };
      };
      [k: string]: any;
    };
  }

  export interface TransactionDataEntity {
    type: 'transaction_data';
    id: string;
    attributes: {
      input_data: string;
    };
  }

  export interface TransactionRecordDetail {
    type: 'transaction_record';
    id: string;
    attributes: {
      input: string;
      output: string;
      is_error: boolean;
      date_executed: string;
      error_message: string | null;
    };
    meta?: {
      summary: string | null;
    };
    relationships: {
      integration?: {
        data?: {
          type: 'transaction';
          id: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
  }

  export interface MagicString {
    magic_string: string;
    [k: string]: any;
  }

  export interface MoveableCatalogEntry {
    type:
      | 'folder'
      | 'case_type'
      | 'object_type'
      | 'attribute'
      | 'email_template'
      | 'document_template';
    id: string;
    [k: string]: any;
  }

  export interface ObjectTypeDetail {
    type: 'object_type';
    id: string;
    attributes: {
      name?: string;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CustomObjectTypeDetail {
    type: 'custom_object_type';
    id: string;
    attributes: {
      name?: string;
      title?: string;
      external_reference?: string;
      status?: string;
      version?: number;
      version_independent_uuid?: string;
      last_modified?: string;
      date_created?: string;
      [k: string]: any;
    };
    relationships: {
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }
}
