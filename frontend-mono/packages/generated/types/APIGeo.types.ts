// Generated on: 29-11-2024 13:07:03 (Amsterdam time)
// Branch used: master
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Geo domain.
/* eslint-disable */
export namespace APIGeo {
  export interface GetGeoFeaturesRequestParams {
    uuid: string[];
    [k: string]: any;
  }

  export type GetGeoFeaturesResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      attributes: {
        /**
         * GeoJSON object detailing the feature
         */
        geo_json: {
          [k: string]: any;
        };
        /**
         *
         *         Geo features are retrieved based on the UUID of an entity in the system.
         *
         *         An entity can be related to other entities, and get "related geo" in that
         *         way.
         *
         *         This list indicates which UUID(s) in the query led to this geo feature
         *         entity being returned.
         *
         */
        origin: string[];
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'geo_feature';
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    meta?: {
      total_results?: number;
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface CreateGeoFeatureRequestBody {
    /**
     * UUID of the geo feature to be created.
     */
    uuid: string;
    /**
     * Geo Feature object.
     */
    geojson: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefGeoFieldTypesEnum = 'geojson' | 'address_v2' | 'relationship';

  /**
   * Introspectable object based on pydantic
   */
  export interface DefMapMagicString {
    /**
     * Label of the custom field
     */
    field_label: string;
    /**
     * Type of custom field; used to determine how to use the value
     */
    field_type: 'geojson' | 'address_v2' | 'relationship';
    /**
     * Magic string of the custom field
     */
    magic_string: string;
    [k: string]: any;
  }

  export interface EntityCase {
    /**
     * Entity representing a case as seen by the geo domain.
     *
     * It is very minimal on purpose, as the "actual" case is part of the
     * case-management domain; this only has the UUID as and a list of magic
     * strings that should be represented on the map.
     */
    data: {
      attributes: {
        /**
         * Assignee of the case
         */
        assignee: string;
        /**
         * Family for this entity is case
         */
        family: string;
        /**
         * List of magic strings of custom fields that should be shown on the map
         */
        map_magic_strings: {
          /**
           * Label of the custom field
           */
          field_label: string;
          /**
           * Type of custom field; used to determine how to use the value
           */
          field_type: 'geojson' | 'address_v2' | 'relationship';
          /**
           * Magic string of the custom field
           */
          magic_string: string;
          [k: string]: any;
        }[];
        /**
         * Requestor of the case
         */
        requestor: string;
        /**
         * UUID of requestor of the case
         */
        requestor_uuid?: string;
        /**
         * Status of the case
         */
        status: string;
        /**
         * Subtitle is the extra information for the case
         */
        subtitle: string;
        /**
         * Id of the case
         */
        title: string;
        /**
         * Type of the case
         */
        type: string;
        /**
         * Flag that indicates whether the geo-data of the requestor needs to be linked
         */
        use_geojson_address: boolean;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'case';
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseList {
    data: {
      attributes: {
        /**
         * Assignee of the case
         */
        assignee: string;
        /**
         * Family for this entity is case
         */
        family: string;
        /**
         * List of magic strings of custom fields that should be shown on the map
         */
        map_magic_strings: {
          /**
           * Label of the custom field
           */
          field_label: string;
          /**
           * Type of custom field; used to determine how to use the value
           */
          field_type: 'geojson' | 'address_v2' | 'relationship';
          /**
           * Magic string of the custom field
           */
          magic_string: string;
          [k: string]: any;
        }[];
        /**
         * Requestor of the case
         */
        requestor: string;
        /**
         * UUID of requestor of the case
         */
        requestor_uuid?: string;
        /**
         * Status of the case
         */
        status: string;
        /**
         * Subtitle is the extra information for the case
         */
        subtitle: string;
        /**
         * Id of the case
         */
        title: string;
        /**
         * Type of the case
         */
        type: string;
        /**
         * Flag that indicates whether the geo-data of the requestor needs to be linked
         */
        use_geojson_address: boolean;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'case';
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    meta?: {
      total_results?: number;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityContact {
    /**
     * Entity representing a contact as seen by the geo domain.
     *
     * It is very minimal and only contains the information needed to store in the
     * geo domain.
     */
    data: {
      attributes: {
        /**
         * Full address of the contact
         */
        address: string;
        /**
         * Display-name of the contact
         */
        name: string;
        /**
         * Status of the contact (active or inactive)
         */
        status?: string;
        /**
         * Type of contact (person, organization)
         */
        type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'contact';
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityContactList {
    data: {
      attributes: {
        /**
         * Full address of the contact
         */
        address: string;
        /**
         * Display-name of the contact
         */
        name: string;
        /**
         * Status of the contact (active or inactive)
         */
        status?: string;
        /**
         * Type of contact (person, organization)
         */
        type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'contact';
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    meta?: {
      total_results?: number;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObject {
    /**
     * Entity representing a custom object as seen by the geo domain.
     *
     * It is very minimal on purpose, as the "actual" custom object is part of the
     * case-management domain; this only has the version-independent UUID and the
     * fields that should be represented on a map.
     */
    data: {
      attributes: {
        /**
         * Family of a custom object is Object
         */
        family: string;
        /**
         * List of magic strings of custom fields that should be shown on the map
         */
        map_magic_strings: {
          /**
           * Label of the custom field
           */
          field_label: string;
          /**
           * Type of custom field; used to determine how to use the value
           */
          field_type: 'geojson' | 'address_v2' | 'relationship';
          /**
           * Magic string of the custom field
           */
          magic_string: string;
          [k: string]: any;
        }[];
        /**
         * Status of a custom object
         */
        status: string;
        /**
         * Subtitle of a custom object
         */
        subtitle: string;
        /**
         * Title of a custom object
         */
        title: string;
        /**
         * Type of a custom object
         */
        type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'custom_object';
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectList {
    data: {
      attributes: {
        /**
         * Family of a custom object is Object
         */
        family: string;
        /**
         * List of magic strings of custom fields that should be shown on the map
         */
        map_magic_strings: {
          /**
           * Label of the custom field
           */
          field_label: string;
          /**
           * Type of custom field; used to determine how to use the value
           */
          field_type: 'geojson' | 'address_v2' | 'relationship';
          /**
           * Magic string of the custom field
           */
          magic_string: string;
          [k: string]: any;
        }[];
        /**
         * Status of a custom object
         */
        status: string;
        /**
         * Subtitle of a custom object
         */
        subtitle: string;
        /**
         * Title of a custom object
         */
        title: string;
        /**
         * Type of a custom object
         */
        type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'custom_object';
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    meta?: {
      total_results?: number;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityGeoFeature {
    /**
     * Represents the geographic features in a custom object
     */
    data: {
      attributes: {
        /**
         * GeoJSON object detailing the feature
         */
        geo_json: {
          [k: string]: any;
        };
        /**
         *
         *         Geo features are retrieved based on the UUID of an entity in the system.
         *
         *         An entity can be related to other entities, and get "related geo" in that
         *         way.
         *
         *         This list indicates which UUID(s) in the query led to this geo feature
         *         entity being returned.
         *
         */
        origin: string[];
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'geo_feature';
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityGeoFeatureList {
    data: {
      attributes: {
        /**
         * GeoJSON object detailing the feature
         */
        geo_json: {
          [k: string]: any;
        };
        /**
         *
         *         Geo features are retrieved based on the UUID of an entity in the system.
         *
         *         An entity can be related to other entities, and get "related geo" in that
         *         way.
         *
         *         This list indicates which UUID(s) in the query led to this geo feature
         *         entity being returned.
         *
         */
        origin: string[];
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'geo_feature';
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    meta?: {
      total_results?: number;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityGeoFeatureRelationship {
    /**
     * Represents a relation between two objects, for the purposes of linking
     * their geo information together.
     */
    data: {
      attributes: {
        /**
         * UUID of the 'original' object or case
         */
        origin_uuid: string;
        /**
         * UUID of related object
         */
        related_uuid: string;
        /**
         * Identifier for this entity
         */
        uuid: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'geo_feature_relationship';
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityGeoFeatureRelationshipList {
    data: {
      attributes: {
        /**
         * UUID of the 'original' object or case
         */
        origin_uuid: string;
        /**
         * UUID of related object
         */
        related_uuid: string;
        /**
         * Identifier for this entity
         */
        uuid: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: 'geo_feature_relationship';
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    meta?: {
      total_results?: number;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface Entities {
    [k: string]: any;
  }
}
