# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.8.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.8.1...@mintlab/kitchen-sink@3.8.2) (2022-01-26)

**Note:** Version bump only for package @mintlab/kitchen-sink





## 3.8.1 (2020-06-30)

**Note:** Version bump only for package @mintlab/kitchen-sink





# [3.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.7.1...@mintlab/kitchen-sink@3.8.0) (2020-04-28)


### Features

* **SubForms:** MINTY-3806 Add `useSubForm` hook that enables the use of namespaced subforms ([ffcde68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ffcde68))





## [3.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.7.0...@mintlab/kitchen-sink@3.7.1) (2020-03-17)

**Note:** Version bump only for package @mintlab/kitchen-sink





# [3.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.6.2...@mintlab/kitchen-sink@3.7.0) (2020-01-10)


### Features

* **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))





## [3.6.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.6.1...@mintlab/kitchen-sink@3.6.2) (2020-01-09)


### Bug Fixes

* (CaseManagement) truncate task title ([c5d397b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d397b))





## [3.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.6.0...@mintlab/kitchen-sink@3.6.1) (2020-01-09)


### Bug Fixes

* **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))





# [3.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.5.1...@mintlab/kitchen-sink@3.6.0) (2019-10-17)


### Features

* **Communciation:** MINTY-1790 Add communication module on `/main/customer-contact` route ([c90005c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c90005c))





## [3.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.5.0...@mintlab/kitchen-sink@3.5.1) (2019-09-06)

**Note:** Version bump only for package @mintlab/kitchen-sink





# [3.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.4.0...@mintlab/kitchen-sink@3.5.0) (2019-08-29)


### Features

* **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))





# [3.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.3.2...@mintlab/kitchen-sink@3.4.0) (2019-07-31)


### Features

* **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))





## [3.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.3.1...@mintlab/kitchen-sink@3.3.2) (2019-07-25)

**Note:** Version bump only for package @mintlab/kitchen-sink





## [3.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@mintlab/kitchen-sink@3.3.0...@mintlab/kitchen-sink@3.3.1) (2019-07-23)

**Note:** Version bump only for package @mintlab/kitchen-sink





# 3.3.0 (2019-07-18)


### Features

* **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
* **KitchenSink:** MINTY-1120 Import kitchen-sink package into monorepo ([ee5f0bb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/ee5f0bb))
* **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
