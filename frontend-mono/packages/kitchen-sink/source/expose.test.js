// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const { expose } = require('./expose');

test('expose()', assert => {
  const bucket = expose({
    foo: 'bar',
  });

  const actual = Object.keys(bucket).length;
  const expected = 0;
  const message = 'sets properties that are not enumerable';

  assert.equal(actual, expected, message);
  assert.end();
});
