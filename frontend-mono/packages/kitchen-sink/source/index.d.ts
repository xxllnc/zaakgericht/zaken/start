// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

declare module '@mintlab/kitchen-sink/source' {
  function get<D, R, A = undefined>(
    data: D,
    valuePath: any,
    alternateValue?: A
  ): R | A;

  function buildUrl<P = { [key: string]: any }>(url: string, params: P): string;

  const ENCODED_NULL_BYTE: string;

  function asArray(data: any): any[];

  function toggleItem<T>(items: T[], item: T): T[];

  function capitalize(string: string): string;

  function cloneWithout(input: any, ...rest: string[]): any;

  function preventDefaultAndCall(fn: (ev?: any) => void): any;
}
