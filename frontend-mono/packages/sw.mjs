// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { precacheAndRoute } from 'workbox-precaching/precacheAndRoute.mjs';
import { clientsClaim } from 'workbox-core';

// Precaching
precacheAndRoute(self.__WB_MANIFEST);

// Force the waiting service worker to become the active service worker
self.skipWaiting();

// Ensure control over all clients
clientsClaim();

self.addEventListener('activate', event => {
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          return caches.delete(cacheName);
        })
      );
    })
  );
});
