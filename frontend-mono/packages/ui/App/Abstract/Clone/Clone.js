// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { cloneElement } from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';

/**
 * Use a *JSX* component instead of (yet) an(other) expression
 * to clone properties to a component’s first child.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Clone
 * @see /npm-mintlab-ui/documentation/consumer/manual/Clone.html
 *
 * @param {Object} props
 * @param {ReactElement} props.children
 *   Component content nodes
 * @return {ReactElement}
 */
export const Clone = props => {
  const { children } = props;
  const newProps = cloneWithout(props, 'children');

  return cloneElement(children, newProps);
};

export default Clone;
