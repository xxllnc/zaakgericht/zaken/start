// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text } from '../../story';
import Dynamic from '.';

const Heading1 = ({ children }) => <h1>{children}</h1>;

const Heading2 = ({ children }) => <h2>{children}</h2>;

stories(module, __dirname, {
  String() {
    return (
      <Dynamic component={Heading1} title="Hello, title!">
        {text('Greeting', 'Hello, Heading 1!')}
      </Dynamic>
    );
  },
  Function() {
    return (
      <Dynamic component={Heading2}>
        {text('Greeting', 'Hello, Heading2!')}
      </Dynamic>
    );
  },
});
