// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as LazyLoader } from './LazyLoader';
export * from './LazyLoader';
export default LazyLoader;
