// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import reactTriggerChange from 'react-trigger-change';
import { React, stories, object } from '../../story';
import Resource from '.';

const KNOB_ID = 'Resource';
const DELAY = 3000;
const INCREMENT = 1;

const getKnob = id => window.parent.document.getElementById(id);

function setResource(id, data) {
  const textArea = getKnob(KNOB_ID);

  if (textArea) {
    if (data) {
      textArea.value = JSON.stringify({
        [id]: {
          data,
        },
      });
      textArea.disabled = true;
    } else {
      textArea.value = JSON.stringify({
        [id]: null,
      });
      textArea.disabled = false;
    }

    reactTriggerChange(textArea);
  }
}

const delay = {
  resourceTypes: {
    foo: ['FOO RESOURCE'],
    bar: ['BAR RESOURCE'],
  },
  get(id) {
    setResource(id);
    setTimeout(function resolveResource() {
      setResource(id, delay.resourceTypes[id]);
    }, DELAY);
  },
};

const ResultList = ({ resource }) => (
  <ul>
    {resource.foo.data.map((item, index) => (
      <li key={index}>{item}</li>
    ))}
  </ul>
);

const ResultTable = ({ resource }) => (
  <tbody>
    {resource.foo.data.map((item, index) => (
      <tr key={index}>
        <th>{index + INCREMENT}</th>
        <td>{item}</td>
      </tr>
    ))}
  </tbody>
);

stories(module, __dirname, {
  'Circle Loader': function CircleLoader() {
    return (
      <Resource
        id={['foo']}
        payload={object(KNOB_ID, {
          foo: null,
        })}
        resolve={delay.get}
      >
        <ResultList />
      </Resource>
    );
  },
  'Table Body Loader': function TableBodyLoader() {
    const wrapper = ({ children }) => (
      <tbody>
        <tr>
          <td colSpan={2}>{children}</td>
        </tr>
      </tbody>
    );

    return (
      <table>
        <thead>
          <tr>
            <th>Foo</th>
            <th>Bar</th>
          </tr>
        </thead>
        <Resource
          id={['foo']}
          payload={object(KNOB_ID, {
            foo: null,
          })}
          resolve={delay.get}
          wrapper={wrapper}
        >
          <ResultTable />
        </Resource>
      </table>
    );
  },
});
