// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Resource } from './Resource';
export * from './Resource';
export default Resource;
