// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useErrorDialog } from '@zaaksysteem/common/src/hooks/useErrorDialog';
import {
  WrapperMapMessageType,
  WrapperMapEventType,
  Geojson,
  IntegrationContextType,
} from '@mintlab/ui/types/MapIntegration';
import { v4 } from 'uuid';
import { useMapConfig } from './useMapConfig';

const version = 5;
let hasNotifiedAboutError = false;

type GeoMapPropsType = {
  geoFeature: Geojson | null;
  markerPosition?: GeoJSON.Point;
  onClick?: (point: GeoJSON.Point) => void;
  onFeatureDraw?: (feature: Geojson | null) => void;
  name: string;
  canDrawFeatures?: boolean;
  minHeight?: number | string;
  context: IntegrationContextType;
  featureRequestTargetLayer?: string;
};

const useWindowListener: <K extends keyof WindowEventMap>(
  store: Record<string, (ev: WindowEventMap[K]) => any>,
  type: K,
  listener: (ev: WindowEventMap[K]) => any,
  options?: boolean | EventListenerOptions
) => (active?: boolean) => void = (store, type, fn) => {
  const [id, setId] = React.useState('');
  const [active, setActive] = React.useState(false);

  React.useEffect(() => {
    const mainId = v4();
    setId(mainId);

    const wrappedFn: typeof fn = (...args) => {
      store[mainId] && store[mainId](...args);
    };

    window.top?.addEventListener(type, wrappedFn);

    return () => {
      window.top?.removeEventListener(type, wrappedFn);
      delete store[mainId];
    };
  }, []);

  if (id && active) {
    store[id] = fn;
  }

  return (active = true) => setActive(active);
};

const store = {};

export const GeoMap: React.ComponentType<GeoMapPropsType> = props => {
  const {
    geoFeature,
    onClick,
    onFeatureDraw,
    markerPosition,
    name,
    minHeight,
    canDrawFeatures = true,
    featureRequestTargetLayer,
    context,
  } = props;
  const [activeGeoFeature, setActiveGeoFeature] = React.useState(geoFeature);
  const [ErrorDialog, openErrorDialog] = useErrorDialog();
  const [t] = useTranslation('common');
  const mapConfig = useMapConfig();
  const ref = React.useRef<HTMLIFrameElement>(null);

  React.useEffect(() => {
    if (geoFeature !== activeGeoFeature) {
      sendMessage({
        type: 'setFeature',
        version,
        name,
        value: geoFeature,
      });
    }
  }, [geoFeature]);

  React.useEffect(() => {
    sendMessage({
      type: 'setMarker',
      version,
      name,
      value: markerPosition || null,
    });
  }, [markerPosition]);

  React.useEffect(() => {
    sendMessage({
      type: 'setDrawToolbar',
      version,
      name,
      value: canDrawFeatures,
    });
  }, [canDrawFeatures]);

  const sendMessage = (message: WrapperMapMessageType) =>
    ref?.current?.contentWindow?.postMessage(message, '*');

  const handleMessage =
    // eslint-disable-next-line complexity
    (event: WrapperMapEventType) => {
      if (event?.data?.name === name) {
        if (event.data.version !== version && !hasNotifiedAboutError) {
          hasNotifiedAboutError = true;
          openErrorDialog({
            title: t('externalComponents.mapVersionMismatchTitle'),
            message: t('externalComponents.mapVersionMismatchMessage', {
              oldVersion: event.data.version,
              newVersion: version,
            }),
          });
        }

        if (event.data.type === 'featureChange' && onFeatureDraw) {
          setActiveGeoFeature(event.data.value);
          onFeatureDraw(event.data.value);
        } else if (event.data.type === 'click' && onClick) {
          onClick(event.data.value);
        } else if (event.data.type === 'getFeatureInfoResolved') {
          // Expand on this when implementing case forms
          console.log(event.data.value);
        }
      }
    };

  const activateListener = useWindowListener(store, 'message', handleMessage);

  return (
    <React.Fragment>
      {mapConfig ? (
        <iframe
          style={{ width: '100%', minHeight }}
          ref={ref}
          src={mapConfig.appUrl}
          title={name}
          allowFullScreen={true}
          allow="fullscreen; geolocation"
          frameBorder="0"
          onLoad={() => {
            sendMessage({
              type: 'init',
              name,
              version,
              value: {
                initialFeature: geoFeature,
                center: mapConfig.center,
                wmsLayers: mapConfig.wmsLayers,
                canDrawFeatures,
                canSelectLayers: true,
                featureRequestTargetLayer,
                region: mapConfig.region || 'nl',
                context,
              },
            });
            markerPosition &&
              sendMessage({
                type: 'setMarker',
                version,
                name,
                value: markerPosition,
              });
            activateListener();
          }}
        />
      ) : (
        <Loader />
      )}
      {ErrorDialog}
    </React.Fragment>
  );
};
