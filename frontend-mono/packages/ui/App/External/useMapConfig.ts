// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { cachedFetch } from '@zaaksysteem/common/src/library/request/cachedFetch';

type WMSLayer = {
  url: string;
  layers: string;
  xpath?: string | null;
  label: string;
};

type MapConfigResponseType = {
  status_code: number;
  result: {
    type: string;
    reference: null;
    instance: {
      xml_namespaces: {
        preview: string;
        instance: {
          namespace_uri: string;
          date_created: string;
          prefix: string;
          date_modified: string;
        };
        reference: null;
        type: string;
      }[];
      map_application_url: string;
      wms_layers: {
        instance: {
          url: string;
          layer_name: string;
          date_created: string;
          date_modified: string;
          feature_info_xpath: string | null;
          label: string;
          active: boolean;
        };
        preview: string;
        reference: null;
        type: string;
      }[];
      map_application: 'builtin' | 'external';
      map_region: 'nl' | 'cw';
      map_center: string;
    };
  };
  api_version: number;
  request_id: string;
  development: boolean;
};

type MapConfigType = {
  center: [number, number];
  wmsLayers: WMSLayer[];
  appUrl: string;
  region: 'nl' | 'cw';
};

const defaultAppUrl =
  window.location.origin + '/external-components/index.html?component=map';

const defaultConfig: MapConfigType = {
  center: [52, 4],
  appUrl: defaultAppUrl,
  region: 'nl',
  wmsLayers: [
    {
      layers: 'gemeenten',
      label: 'gemeenten',
      url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms',
    },
    {
      layers: 'provincies',
      label: 'provincies',
      url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms',
    },
  ],
};

export const useMapConfig = () => {
  const [config, setConfig] = React.useState<MapConfigType>();
  React.useEffect(() => {
    cachedFetch('/api/v1/map/ol_settings')
      .then(resp => resp.json())
      .then(
        ({
          result: {
            instance: {
              map_center,
              wms_layers,
              map_application_url,
              map_application,
              map_region,
            },
          },
        }: MapConfigResponseType) =>
          setConfig({
            region: map_region || 'nl',
            center: map_center.split(',').map(Number) as [number, number],
            appUrl:
              map_application === 'external'
                ? map_application_url
                : defaultAppUrl,
            wmsLayers: wms_layers
              .filter(layer => layer.instance.active)
              .map(layer => ({
                url: layer.instance.url,
                layers: layer.instance.layer_name,
                xpath: layer.instance.feature_info_xpath,
                label: layer.instance.label,
              })),
          })
      )
      .catch(() => setConfig(defaultConfig));
  }, []);

  return config;
};
