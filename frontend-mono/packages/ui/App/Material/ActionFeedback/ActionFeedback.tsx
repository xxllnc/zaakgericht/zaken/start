// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import MuiTooltip from '@mui/material/Tooltip';
import ClickAwayListener from '@mui/material/ClickAwayListener';

type ActionFeedbackPropsType = {
  children: (handleOpen: () => void) => React.ReactElement;
  title: string;
  type: 'default';
};

export const ActionFeedback = ({
  children,
  title,
}: ActionFeedbackPropsType) => {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <ClickAwayListener onClickAway={handleClose}>
      <div>
        <MuiTooltip
          PopperProps={{ disablePortal: true }}
          onClose={handleClose}
          open={open}
          title={title}
          disableFocusListener
          disableHoverListener
          disableTouchListener
        >
          {children(handleOpen)}
        </MuiTooltip>
      </div>
    </ClickAwayListener>
  );
};

export default ActionFeedback;
