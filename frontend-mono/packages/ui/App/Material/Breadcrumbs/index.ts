// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Breadcrumbs } from './Breadcrumbs';
export * from './Breadcrumbs';
export * from './Breadcrumbs.types';
export default Breadcrumbs;
