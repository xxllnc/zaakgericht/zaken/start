// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { BreadcrumbItemType } from '../Breadcrumbs.types';

/**
 * @param {Array<Object>} items
 * @param {Number} maxItems
 * @param {Number} collapse
 * @return {Array<Object>}
 */
export const getItemsToRender = (
  items: BreadcrumbItemType[],
  maxItems: number,
  collapse: number
): BreadcrumbItemType[] => {
  if (items.length >= maxItems) {
    return items.map((item, index) =>
      index + 1 === collapse ? { ...item, label: '...' } : item
    );
  }

  return items;
};
