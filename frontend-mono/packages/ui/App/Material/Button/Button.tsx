// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import IconButton from '@mui/material/IconButton';
import MuiButton from '@mui/material/Button';
import { SxProps, useTheme } from '@mui/material';
import Icon, { IconSizeType, IconNameType } from '../Icon';

type ButtonPropsType = {
  name: string;
  action?: (ev: React.MouseEvent) => any;
  onClick?: (ev: React.MouseEvent) => any;
  component?:
    | React.ReactElement
    | React.ForwardRefExoticComponent<any>
    | string;
  sx?: SxProps<Theme>;
  disabled?: boolean;
  download?: string;
  label?: string;
  scope?: string;
  icon?: IconNameType;
  iconSize?: IconSizeType;
  variant?: 'text' | 'outlined' | 'contained' | 'default' | 'link';
  className?: string;
  title?: string;
  fullWidth?: boolean;
  endIcon?: any;
  href?: string;
  target?: '_blank' | '_parent' | '_top';
  iconAsText?: string;
  iconTransform?: string;
  children?: any;
  id?: any;
  color?: any;
};

// To make a button with just text, pass text as children
// To make an icon with button effects, pass icon prop
// If you pass both pass icon prop and pass text as children the
// icon will be rendered as adornment to the button text

/* eslint complexity: [2, 12] */
export const Button: React.ComponentType<ButtonPropsType> = ({
  action,
  children = null,
  disabled = false,
  icon,
  label,
  scope,
  iconTransform = '',
  iconSize,
  variant = 'contained',
  sx = {},
  color,
  iconAsText,
  ...rest
}) => {
  const theme = useTheme<Theme>();
  const size = iconSize || (children ? 18 : 28);

  const muiVariant =
    {
      text: 'text',
      outlined: 'outlined',
      contained: 'contained',
      default: 'contained',
      link: 'text',
    }[variant] || variant;

  const muiStyleOverrides = {
    text: color ? {} : { color: 'rgba(0, 0, 0, 0.87)' },
    link: {},
    outlined: {},
    contained: {},
    default: {
      backgroundColor: theme.mintlab.greyscale.dark,
      boxShadow: 'none',
      marginRight: '14px',
      whiteSpace: 'nowrap',
      color: 'rgba(0, 0, 0, 0.87)',
      '&:hover': {
        backgroundColor: theme.mintlab.greyscale.darker,
        boxShadow: 'none',
      },
    },
  }[variant];

  const props = {
    'aria-label': label,
    disabled,
    color,
    onClick: typeof action === 'function' ? action : undefined,
    variant: muiVariant,
    sx: { ...muiStyleOverrides, ...sx },
    ...rest,
  };

  const IconElement = icon ? <Icon size={size}>{icon}</Icon> : null;

  return iconAsText || (icon && !children)
    ? // @ts-ignore
      React.createElement(IconButton, props, iconAsText || IconElement)
    : React.createElement(
        MuiButton,
        // @ts-ignore
        { ...props, startIcon: IconElement },
        children
      );
};

export default Button;
