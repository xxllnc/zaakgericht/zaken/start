// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import { DateTimePicker as NormalPicker } from '@mui/x-date-pickers/DateTimePicker';
import { StaticDateTimePicker as StaticPicker } from '@mui/x-date-pickers/StaticDateTimePicker';
import TextField from '../TextField';
import LocalizationProvider from './LocalizationProvider';

type DateTimePickerPropsType = {
  name: string;
  onChange: any;
  onClose?: any;
  value?: string | null;
  disabled?: boolean;
  outputFormat?: 'ISO' | string;
  placeholder?: string;
  displayFormat?: string;
  hideTabs?: boolean;
  okLabel?: React.ReactNode;
  cancelLabel?: React.ReactNode;
  open?: boolean;
  TextFieldComponent?: any;
  staticVariant?: boolean;
  sx?: any;
};

const DateTimePicker: React.FunctionComponent<DateTimePickerPropsType> = ({
  name,
  onChange,
  onClose,
  value,
  disabled = false,
  placeholder = '',
  outputFormat = 'ISO',
  displayFormat = 'd MMMM yyyy HH:mm',
  hideTabs = true,
  open = false,
  staticVariant = false,
  sx,
  ...rest
}) => {
  const Picker = staticVariant ? StaticPicker : NormalPicker;
  const handleClose = () => onClose && onClose();
  const handleChange = (value: string | null) => {
    let newValue: any;

    if (outputFormat === 'ISO') {
      //@ts-ignore
      const dateObj = new Date(value);
      newValue = dateObj.toISOString();
    } else {
      newValue = fecha.format(newValue, outputFormat);
    }

    onChange &&
      onChange({
        target: {
          name,
          value: newValue,
        },
      });
  };

  return (
    <LocalizationProvider>
      <Picker
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onClose={handleClose}
        inputFormat={displayFormat}
        ampm={false}
        hideTabs={hideTabs}
        open={open}
        ToolbarComponent={() => null}
        components={{ ActionBar: () => null }}
        renderInput={(props: any) => (
          //@ts-ignore
          <TextField
            {...props}
            inputProps={{ ...props.inputProps, placeholder, name }}
          />
        )}
        {...rest}
      />
    </LocalizationProvider>
  );
};

export default DateTimePicker;
