// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '../TextField';
import { DatePickerPropsType } from './DatePicker.types';
import LocalizationProvider from './LocalizationProvider';

export const PopoverDatePicker = ({
  value,
  onChange,
  name,
  clearable = true,
  disabled = false,
  displayFormat = 'dd-MM-yyyy',
  outputFormat = 'YYYY-MM-DD',
  placeholder = 'dd-mm-jjjj',
  ...rest
}: DatePickerPropsType) => {
  const handleChange = (newValue: any) => {
    onChange &&
      //@ts-ignore
      !isNaN(new Date(newValue)) &&
      onChange({
        target: {
          name: name || '',
          value: fecha.format(newValue, outputFormat),
        },
      });
  };

  return (
    <LocalizationProvider>
      <DatePicker
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onAccept={handleChange}
        inputFormat={displayFormat}
        renderInput={params => (
          //@ts-ignore
          <TextField
            {...params}
            inputProps={{ ...params.inputProps, placeholder, name }}
          />
        )}
        {...rest}
      />
    </LocalizationProvider>
  );
};

export default PopoverDatePicker;
