// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiDialog from '@mui/material/Dialog';
import { addScopeAttribute } from '../../../library/addScope';
import { useDialogStyles } from './Dialog.style';

type DialogPropsType = {
  open: boolean;
  classes?: { [key in 'root' | 'paper']: string };
  scope?: string;
  onClose?: (event: any) => void;
  ref?: any;
  disableBackdropClick?: boolean;
  disableEscapeKeyDown?: boolean;
  fullWidth?: boolean;
  fullScreen?: boolean;
  maxWidth?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl' | false | string;
  container?: React.ReactInstance | null;
  children?: any;
  sx?: any;
  PaperProps?: any;
};

export const Dialog: React.ComponentType<DialogPropsType> = React.forwardRef(
  (
    {
      open = false,
      scope,
      children,
      classes,
      container,
      onClose,
      disableBackdropClick,
      disableEscapeKeyDown,
      ...rest
    },
    ref
  ) => {
    const defaultClasses = useDialogStyles();
    const onCloseWithReason = (
      event: any,
      reason: 'escapeKeyDown' | 'backdropClick'
    ) => {
      if (!onClose) return;
      if (reason === 'escapeKeyDown' && disableEscapeKeyDown) return;
      if (reason === 'backdropClick' && disableBackdropClick) return;

      onClose(event);
    };

    const containerProps = container
      ? {
          container,
          style: { position: 'absolute' },
          BackdropProps: { style: { position: 'absolute' } },
        }
      : {};

    return (
      //@ts-ignore
      <MuiDialog
        //@ts-ignore
        ref={ref}
        open={open}
        aria-live="assertive"
        onClose={onCloseWithReason}
        classes={classes || defaultClasses}
        {...addScopeAttribute(scope, 'dialog')}
        {...containerProps}
        {...rest}
      >
        {children}
      </MuiDialog>
    );
  }
);

Dialog.displayName = 'Dialog';
