// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import createSvgIcon from '@mui/material/utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path d="M0,0h24v24H0V0z" fill="none" />
    <path
      d="M20.6,5.7h-9.2c-0.4,0-0.9-0.2-1.2-0.5l-1-1C8.8,3.9,8.4,3.7,8,3.7H3.4c-0.5,0-1,0.4-1,1v14.6c0,0.5,0.4,1,1,1
    h17.2c0.5,0,1-0.4,1-1V6.7C21.6,6.2,21.1,5.7,20.6,5.7z M17.2,13.8l-3,3c-0.2,0.2-0.4,0.2-0.6,0.1c-0.2-0.1-0.3-0.3-0.3-0.5v-1.1
    H8.5c0,0,0,0,0,0c-0.3,0-0.5-0.2-0.5-0.5V12c0-0.3,0.2-0.5,0.5-0.5h4.8v-1c0-0.2,0.1-0.4,0.3-0.5c0.2-0.1,0.4,0,0.6,0.1l3,3
    c0.1,0.1,0.2,0.2,0.2,0.4C17.4,13.6,17.3,13.7,17.2,13.8z"
    />
  </React.Fragment>,
  'FolderMove'
);
