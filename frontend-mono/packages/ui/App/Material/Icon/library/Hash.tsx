// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import createSvgIcon from '@mui/material/utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <svg viewBox="0 0 24 24">
      <line
        stroke="currentColor"
        strokeWidth="2"
        x1="4"
        x2="20"
        y1="9"
        y2="9"
      />
      <line
        stroke="currentColor"
        strokeWidth="2"
        x1="4"
        x2="20"
        y1="15"
        y2="15"
      />
      <line
        stroke="currentColor"
        strokeWidth="2"
        x1="10"
        x2="8"
        y1="3"
        y2="21"
      />
      <line
        stroke="currentColor"
        strokeWidth="2"
        x1="16"
        x2="14"
        y1="3"
        y2="21"
      />
    </svg>
  </React.Fragment>,
  'Hash'
);
