// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import createSvgIcon from '@mui/material/utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <svg
      version="1.2"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width="24"
      height="24"
    >
      <title>kenmerk-icons</title>
      <path
        id="Layer"
        fill="currentcolor"
        d="m10.5 17.8c-1.5 0-2.7 1.2-2.7 2.7v1.5h-3.8c-1.1 0-2-0.9-2-2v-3.8h1.5c1.5 0 2.7-1.2 2.7-2.7 0-1.5-1.2-2.7-2.7-2.7h-1.5v-3.8c0-1.1 0.9-2 2-2h4v-1.5c0-1.4 1.1-2.5 2.5-2.5 1.4 0 2.5 1.1 2.5 2.5v1.5h4c1.1 0 2 0.9 2 2v3.1q-0.5-0.1-1-0.1c-3.9 0-7 3.1-7 7q0 0.4 0.1 0.9-0.3-0.1-0.6-0.1z"
      />
      <path
        id="Color Fill 1"
        fill="currentcolor"
        d="m22.7 18.1c0.1 0.1 0.1 0.2 0.1 0.3l-1.1 1.9c-0.1 0.1-0.2 0.1-0.3 0.1l-1.3-0.5q-0.4 0.3-0.9 0.5l-0.2 1.4c0 0.1-0.1 0.2-0.3 0.2h-2.1c-0.1 0-0.2-0.1-0.2-0.2l-0.2-1.4q-0.5-0.2-0.9-0.5l-1.3 0.5c-0.1 0-0.3 0-0.3-0.1l-1.1-1.9c-0.1-0.1 0-0.2 0.1-0.3l1.1-0.9q-0.1-0.2-0.1-0.5 0-0.2 0.1-0.5l-1.1-0.9c-0.1 0-0.2-0.2-0.1-0.3l1.1-1.8c0-0.1 0.1-0.2 0.3-0.1l1.3 0.5q0.4-0.3 0.9-0.5l0.2-1.4c0-0.2 0.1-0.2 0.2-0.2h2.1c0.2 0 0.3 0 0.3 0.2l0.2 1.4q0.5 0.2 0.9 0.5l1.3-0.5c0.1-0.1 0.2 0 0.3 0.1l1.1 1.8c0 0.1 0 0.3-0.1 0.3l-1.1 0.9q0 0.3 0 0.5 0 0.3 0 0.5zm-3.1-1.3c0-1.1-0.9-1.9-1.9-1.9-1.1 0-1.9 0.8-1.9 1.9 0 1 0.8 1.8 1.9 1.8 1 0 1.9-0.8 1.9-1.8z"
      />
    </svg>
  </React.Fragment>,
  'SystemAttribute'
);
