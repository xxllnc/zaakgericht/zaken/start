// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStepperStyles = makeStyles(
  ({ palette: { primary, error }, mintlab: { shadows } }: Theme) => ({
    root: {
      justifyContent: 'space-between',
      borderRadius: 50,
      padding: 12,
      boxShadow: shadows.flat,
    },
    activeStep: {
      backgroundColor: primary.lightest,
      padding: 5,
      borderRadius: 20,
      outline: `2px solid ${primary.light}`,
    },
    activeStepError: {
      backgroundColor: error.light,
      padding: 5,
      borderRadius: 20,
      outline: `1px solid ${error.main}`,
    },
  })
);
