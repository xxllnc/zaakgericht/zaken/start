// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { default as MUIStepper, StepperProps } from '@mui/material/Stepper';
import Step, { StepProps } from '@mui/material/Step';
import StepLabel, { StepLabelProps } from '@mui/material/StepLabel';
import { useStepperStyles } from './Stepper.style';

export type StepType = Pick<StepProps, 'active' | 'completed' | 'disabled'> &
  Pick<StepLabelProps, 'error'> & {
    name: string;
  };

export type StepperPropsType = Omit<StepperProps, 'children'> & {
  steps: StepType[];
};

export const Stepper: React.ComponentType<StepperPropsType> = ({
  activeStep,
  steps,
}) => {
  const classes = useStepperStyles();

  return (
    <MUIStepper
      activeStep={activeStep}
      connector={<React.Fragment />}
      classes={{ root: classes.root }}
    >
      {steps.map(step => {
        return (
          <Step
            key={step.name}
            active={step.active}
            completed={step.completed}
            disabled={step.disabled}
            classes={{
              root:
                step.active && step.error
                  ? classes.activeStepError
                  : step.active
                    ? classes.activeStep
                    : undefined,
            }}
          >
            <StepLabel error={step.error}>{step.name}</StepLabel>
          </Step>
        );
      })}
    </MUIStepper>
  );
};
