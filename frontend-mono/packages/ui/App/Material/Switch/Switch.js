// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { default as MUISwitch } from '@mui/material/Switch';
import { withStyles } from '@mui/styles';
import { switchStylesheet } from './Switch.style';

export const Switch = ({
  classes,
  checked,
  onChange,
  variant = 'regular',
  ...rest
}) => {
  const isIosVariant = variant.toLowerCase() === 'ios';
  const {
    switchBase,
    checked: checkedClass,
    bar,
    icon,
    iconChecked,
    ...restClasses
  } = classes;

  return (
    <MUISwitch
      disableRipple
      classes={{
        switchBase: classNames({ [switchBase]: isIosVariant }),
        checked: classNames({ [checkedClass]: isIosVariant }),
        bar: classNames({ [bar]: isIosVariant }),
        icon: classNames({ [icon]: isIosVariant }),
        iconChecked: classNames({ [iconChecked]: isIosVariant }),
        ...restClasses,
      }}
      checked={checked}
      onChange={onChange}
      {...rest}
    />
  );
};

export default withStyles(switchStylesheet)(Switch);
