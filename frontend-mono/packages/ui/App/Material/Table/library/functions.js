// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import classNames from 'classnames';

/**
 * @param {number} index
 * @return {boolean}
 */
export function isFirstIndex(index) {
  const firstIndex = 0;

  return index === firstIndex;
}

/**
 * @param {number} index
 * @param {Array} array
 * @return {number}
 */
export function isLastIndex(index, array) {
  const offset = 1;
  const lastIndex = array.length - offset;

  return index === lastIndex;
}

/**
 * @param {string} columnName
 * @param {Object} classes
 * @return {string}
 */
const getSpecificCellClass = (columnName, classes) =>
  classes[`${columnName}Cell`] || classes.otherCells;

/**
 * @param {Array} columns
 * @param {Object} classes
 * @param {number} key
 * @return {string}
 */
export const getCellClass = (columns, classes, key) => {
  const classList = [classes.tableCell];
  const columnName = columns[key].name;

  return classNames(...classList, getSpecificCellClass(columnName, classes));
};
