// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createElement } from 'react';
import { ThemeProvider as MaterialUiThemeProvider } from '@mui/material/styles';
import { theme } from './theme';
import './fonts/roboto.css';

const ThemeProvider = ({ children, ...rest }: { children: any }) =>
  createElement(
    MaterialUiThemeProvider,
    {
      theme,
      ...rest,
    },
    children
  );

export default ThemeProvider;
