// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as DropdownIndicator } from './DropdownIndicator';
export * from './DropdownIndicator';
export default DropdownIndicator;
