// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useActionBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      gap: 20,
      padding: 20,
      borderBottom: `1px solid ${greyscale.light}`,
    },
    filters: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      alignContent: 'flex-start',
      gap: 20,
    },
  })
);
