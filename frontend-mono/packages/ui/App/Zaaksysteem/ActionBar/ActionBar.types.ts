// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IconNameType } from '@mintlab/ui/App/Material/Icon';

export type FilterType = {
  type: 'text' | 'select' | 'checkbox';
  name?: string;
  width?: number;
  placeholder?: string;
  value?: any;
  icon?: IconNameType;
  isClearable?: boolean;
  onChange: (event: any) => void;
  onClose?: (event: any) => void;
  choices?: { label: string; value: string }[];
  [key: string]: any;
};
