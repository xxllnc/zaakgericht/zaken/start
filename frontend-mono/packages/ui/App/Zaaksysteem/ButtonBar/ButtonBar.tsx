// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material/styles';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { Box } from '@mui/material';
import { useButtonBarStyles } from './ButtonBar.style';
import { ButtonType } from './ButtonBar.types';

export type ButtonBarPropsType = {
  actions?: ButtonType[];
  advancedActions?: ButtonType[];
  permanentActions?: ButtonType[];
  negative?: boolean;
};

const ButtonBar: React.FunctionComponent<ButtonBarPropsType> = ({
  actions = [],
  advancedActions = [],
  permanentActions = [],
  negative = false,
}) => {
  const classes = useButtonBarStyles();

  const {
    // @ts-ignore
    palette: { common },
  } = useTheme();

  const sxIcon = negative ? { color: common.white } : {};

  const hasActions = Boolean(actions.length);
  const hasAdvancedActions = Boolean(advancedActions.length);
  const hasPermanentActions = Boolean(permanentActions.length);
  const hasDivider = (hasActions || hasAdvancedActions) && hasPermanentActions;

  return (
    <Box className={classes.wrapper} sx={{ color: common.white }}>
      {actions.map(({ action, label, icon, name, type }, index) =>
        icon && type !== 'text' ? (
          <div key={`action-${index}`} className={classes.textButton}>
            <Tooltip key={`action-${index}`} title={label} placement="bottom">
              <Button action={action} icon={icon} name={name} sx={sxIcon} />
            </Tooltip>
          </div>
        ) : (
          <div key={`action-${index}`} className={classes.textButton}>
            <Button action={action} name={name} variant="default" icon={icon}>
              {label}
            </Button>
          </div>
        )
      )}
      {hasAdvancedActions && (
        <DropdownMenu
          trigger={
            <Button icon="more_vert" name="advancedActions" sx={sxIcon} />
          }
        >
          <DropdownMenuList items={advancedActions} />
        </DropdownMenu>
      )}
      {hasDivider && <div className={classes.divider} />}
      {permanentActions.map(({ action, label, icon, name }, index) =>
        icon ? (
          <div key={`permanentAction-${index}`} className={classes.textButton}>
            <Tooltip key={`action-${index}`} title={label} placement="bottom">
              <Button action={action} icon={icon} name={name} sx={sxIcon} />
            </Tooltip>
          </div>
        ) : (
          <div key={`permanentAction-${index}`} className={classes.textButton}>
            <Button action={action} name={name} variant="default">
              {label}
            </Button>
          </div>
        )
      )}
    </Box>
  );
};

export default ButtonBar;
