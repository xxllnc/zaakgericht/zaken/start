// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import { v4 as unique } from 'uuid';
import Menu from '@mui/material/Menu';
import { addScopeAttribute } from '../../library/addScope';
import { useDropdownMenuStyle } from './DropdownMenu.style';
export { default as DropdownMenuList } from './library/DropdownMenuList';

const DropdownMenu = ({
  transformOrigin = {
    vertical: 0,
    horizontal: 'right',
  },
  manualId,
  children,
  trigger,
  scope,
  ...rest
}: {
  scope?: string;
  transformOrigin?: any;
  children: any;
  trigger: any;
  manualId?: number;
}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const style = useDropdownMenuStyle();

  const open = Boolean(anchorEl);
  const id = (manualId || unique()).toString();
  const TriggerElement =
    trigger &&
    React.cloneElement(trigger, {
      'aria-owns': open ? id : null,
      'aria-haspopup': 'true',
      onClick: (event: any) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
      },
      ...addScopeAttribute(scope, 'dropdown-menu'),
    });

  return (
    <Fragment>
      {TriggerElement}
      <Menu
        sx={style}
        anchorEl={anchorEl}
        transformOrigin={transformOrigin}
        id={id}
        onClick={event => {
          event.stopPropagation();
          setAnchorEl(null);
        }}
        open={open}
        MenuListProps={{
          sx: {
            display: 'flex',
            flexDirection: 'column',
            padding: '8px',
            margin: '0px',
            '& button:not(:last-child)': {
              marginBottom: '6px',
            },
          },
        }}
        {...rest}
      >
        {children}
      </Menu>
    </Fragment>
  );
};

export default DropdownMenu;
