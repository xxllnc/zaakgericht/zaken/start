// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Divider from '@mui/material/Divider';
import DropdownMenuButton from './DropdownMenuButton';

const { isArray } = Array;

type ActionConditionFunctionType = () => boolean;

export type ActionType = {
  action: (event: any) => void;
  label: string;
  condition?: boolean | ActionConditionFunctionType;
  hidden?: boolean;
};

export type NestedActionType = ActionType[] | ActionType[][];

const DropdownMenuList = React.forwardRef(
  ({ items, scope }: { items: NestedActionType; scope?: string }, ref) => {
    const normalized = (
      (items as any).every((item: any) => isArray(item)) ? items : [items]
    ) as ActionType[][];

    return normalized.map((group, index) => (
      <React.Fragment key={index}>
        {Boolean(index) && index !== normalized.length && <Divider />}
        {group.map(({ label, action, condition, ...rest }, buttonIndex) => (
          <DropdownMenuButton
            ref={ref}
            key={buttonIndex}
            label={label}
            action={action}
            scope={scope}
            disabled={condition === false}
            {...rest}
          />
        ))}
      </React.Fragment>
    )) as any;
  }
);

DropdownMenuList.displayName = 'DropdownMenuList';

export default DropdownMenuList;
