// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { stories, action, number, boolean, select } from '../../story';
import FileSelect from './library/FileSelect.lazy';
import FileList from './library/FileList';
import File from './library/File';

stories(module, __dirname, {
  Default() {
    return (
      <FileSelect
        selectInstructions="Select from disk"
        dragInstructions="Drag your file(s) here"
        dropInstructions="Drop your files here..."
        orLabel="or"
        scope="story"
        onDrop={action('File select')}
        multiValue={boolean('Multiple files', false)}
        error={boolean('Error', false)}
      />
    );
  },

  SelectWithFiles() {
    const numberOfDocuments = number('Number of files', 1, {
      range: true,
      min: 0,
      max: 15,
      step: 1,
    });

    const files = Array(numberOfDocuments)
      .fill(0)
      .map((file, index) => ({
        name: `example-document-${index}.odt`,
        onDeleteClick: action(`Delete file (${index})`),
      }));

    return (
      <FileSelect
        selectInstructions="Select from disk"
        dragInstructions="Drag your file(s) here"
        dropInstructions="Drop your file(s) here..."
        orLabel="or"
        onDrop={action('File select')}
        hasFiles={files.length > 0}
        multiValue={boolean('Multiple files', false)}
        fileList={
          <FileList>
            {files.map((file, index) => (
              <File key={index} scope="story" {...file} />
            ))}
          </FileList>
        }
      />
    );
  },

  File() {
    const deletable = boolean('Deletable', true);
    const uploadProgress = number('Upload progress', 0, {
      range: true,
      min: 0,
      max: 1,
      step: 0.01,
    });
    const status = select(
      'Status',
      {
        '': '',
        pending: 'pending',
        failed: 'failed',
      },
      ''
    );

    return (
      <File
        uploadProgress={uploadProgress}
        name="example-filename.odt"
        status={status}
        onDeleteClick={deletable ? () => {} : null}
      />
    );
  },
});
