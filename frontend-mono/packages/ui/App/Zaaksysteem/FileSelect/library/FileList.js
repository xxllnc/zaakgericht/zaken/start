// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { fileListStylesheet } from './FileList.style';

const FileList = ({ children, classes }) => (
  <div className={classes.wrapper}>{children}</div>
);

export default withStyles(fileListStylesheet)(FileList);
