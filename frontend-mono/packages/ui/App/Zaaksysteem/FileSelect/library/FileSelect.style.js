// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import svg from './svg/pattern.svg';

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const fileSelectStylesheet = ({
  mintlab: { greyscale },
  palette: { error },
}) => ({
  wrapper: {
    position: 'relative',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
    backgroundImage: `url(${svg})`,
    backgroundSize: 100,
    backgroundColor: greyscale.light,
    flexDirection: 'column',
    boxSizing: 'border-box',
  },
  error: {
    '&:after': {
      content: '""',
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 2,
      backgroundColor: error.main,
    },
  },
  form: {
    textAlign: 'center',
  },
  orLabel: {
    marginTop: 10,
    marginBottom: 8,
  },
  fileList: {
    width: '100%',
  },
  file: {
    marginBottom: 8,
  },
});
