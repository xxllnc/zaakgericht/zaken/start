// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material';
import Icon from '../../../Material/Icon';

export const InfoIcon = () => {
  const theme = useTheme();

  return (
    <Icon
      size={20}
      sx={{
        color: theme.palette.grey[400],
        '&:hover': {
          color: theme.palette.primary.main,
        },
      }}
    >
      help_outline
    </Icon>
  );
};

export default InfoIcon;
