// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Box } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { GetterType, HelpDataIndexType } from './HelpDialog.types';

const defaultGetter: GetterType = (t, id) => {
  const translations = t(id, { returnObjects: true }) as any;
  const { title, content } = translations;

  if (typeof content === 'string') return { title, content };

  return {
    title,
    content: (
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          gap: 1,
        }}
      >
        {Object.entries(content).map(([key, line]: any) => (
          <span key={`${id}-${key}`}>{line}</span>
        ))}
      </Box>
    ),
  };
};

const quickSearchPreferenceGetter: GetterType = (t, id) => {
  const wrapperClass = { display: 'flex', flexDirection: 'column', gap: 2 };
  const iconRowsClass = { display: 'flex', flexDirection: 'column', gap: 1 };
  const iconRowClass = {
    display: 'flex',
    flexDirection: 'row',
    gap: 1,
    alignItems: 'center',
  };

  return {
    title: t(`${id}.title`),
    content: (
      <Box sx={wrapperClass}>
        <span>{t(`${id}.description`)}</span>
        <Box sx={iconRowsClass}>
          {[iconNames.lock_open, iconNames.lock, iconNames.no_encryption].map(
            icon => (
              <Box key={icon} sx={iconRowClass}>
                <Icon size="small">{icon}</Icon>
                <span>{t(`${id}.icon.${icon}`)}</span>
              </Box>
            )
          )}
        </Box>
      </Box>
    ),
  };
};

export const helpDataIndex: HelpDataIndexType = {
  // main menu,
  dashboard: defaultGetter,
  communication: defaultGetter,
  documentIntake: defaultGetter,
  advancedSearch: defaultGetter,
  contactSearch: defaultGetter,
  // export: defaultGetter,
  catalog: defaultGetter,
  users: defaultGetter,
  log: defaultGetter,
  transactions: defaultGetter,
  integrations: defaultGetter,
  datastore: defaultGetter,
  configuration: defaultGetter,

  // top bar
  quickSearch: defaultGetter,
  quickSearchPreference: quickSearchPreferenceGetter,
};
