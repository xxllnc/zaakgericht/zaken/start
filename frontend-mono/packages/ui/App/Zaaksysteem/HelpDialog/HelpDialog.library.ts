// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { helpDataIndex } from './HelpDialog.contents';
import { GetAngleToRotateType, GetHelpDataType } from './HelpDialog.types';

/*
  Homebrew formula to calculate the angle required
  to rotate a rectangle X more pixels into the short dimension.

    e.g. a 'landscape' rectangle of 30 height and 70 width
    will rotate such that it takes up 36 height (+3 up, +3 down)

    e.g. a 'portrait' rectangle of 70 height and 30 width
    will rotate such that it takes up 36 width (+3 left, +3 right)

  The goal here is to make sure that a long and stretched element
  visually shakes about as much as short and stumpy element
*/
export const getAngleToRotate: GetAngleToRotateType = (
  height,
  width,
  desiredDeviationInPx
) => {
  // consider the rectangle to be in 'landscape' orientation
  const [short, long] = [height, width].sort((aa, bb) => aa - bb);

  // imagine a triangle from the center of the rectangle,
  // to the center of the right side, to the top-right corner of the rectangle
  const longSide = long / 2;
  const shortSide = short / 2;
  const hypotenuse = Math.sqrt(shortSide * shortSide + longSide * longSide);

  // this triangle has an angle at the center of the rectangle
  const angleOld = Math.atan(shortSide / longSide);

  // now turn the triangle until the hypotenuse reaches the desired deviation
  const maxHeight = shortSide + desiredDeviationInPx;

  // the hypotenuse and the maxHeight now form a new triangle
  // for which we can calculate the angle
  const angleNew = Math.asin(maxHeight / hypotenuse);

  // we now subtract the angle of the new triangle from the old triangle
  // and that is how much we should rotate the rectangle
  const radian = angleNew - angleOld;
  const conversion = Math.PI / 180;
  const angleToRotate = radian / conversion;

  return angleToRotate;
};

export const getHelpData: GetHelpDataType = (t, { id, title, content }) => {
  if (id) {
    const helpDataGetter = helpDataIndex[id];
    const helpData = helpDataGetter(t, id);

    return helpData;
  } else {
    return { title: title as string, content };
  }
};
