// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    // main menu
    dashboard: {
      title: '$t(layout:modules.dashboard)',
      content:
        'Het dashboard is de hoofdpagina van elke gebruiker. Op het dashboard staan standaard de openstaande zaken (werkvoorraad), zaakintake (nieuwe zaken) en favoriete zaaktypen van de gebruiker.',
    },
    communication: {
      title: '$t(layout:modules.communication)',
      content:
        'In de algemene communicatie staan alle e-mails die naar afgehandelde zaken zijn verstuurd. Deze e-mails kunnen worden toegevoegd aan bestaande zaken of verwijderd worden.',
    },
    documentIntake: {
      title: '$t(layout:modules.documentIntake)',
      content:
        'In de documentintake staan alle documenten die naar het zaaksysteem zijn verstuurd (b.v. door de scanstraat), of documenten die uit zaken geweigerd worden. Deze documenten kunnen worden toegevoegd aan zaken of verwijderd worden. Ook kunnen nieuwe zaken gestart worden op basis van deze documenten.',
    },
    advancedSearch: {
      title: '$t(layout:modules.advancedSearch)',
      content:
        "In '$t(layout:modules.advancedSearch)' kunnen met zoekopdrachten overzichten gemaakt worden van zaaktypen, zaken en objecten. Deze overzichten kunnen vervolgens ook op het dashboard getoond worden.",
    },
    contactSearch: {
      title: '$t(layout:modules.contactSearch)',
      content:
        "In '$t(layout:modules.contactSearch)' kan gezocht worden naar personen, organisaties en medewerkers die in xxllnc Zaken geregistreerd staan.",
    },
    // export: { title: '$t(layout:modules.export)', content: '' },
    catalog: {
      title: '$t(layout:modules.catalog)',
      content:
        'In de catalogus kunnen de zaaktypen en objecttypen beheerd worden, mede door gebruik te maken van de kenmerken van de Gemma Zaaktypecatalogus (ZTC), eigen kenmerken (plus-elementen), documentsjablonen en e-mailsjablonen.',
    },
    users: {
      title: '$t(layout:modules.users)',
      content: {
        1: 'Op de gebruikerspagina kunnen de afdelingen, rollen en gebruikers beheerd worden.',
        2: 'xxllnc Zaken maakt gebruik van Role Based Services voor het inrichten van rollen en rechten binnen het systeem',
      },
    },
    log: {
      title: '$t(layout:modules.log)',
      content:
        'Het logboek bevat logging van alle wijzigingen die aangebracht worden in xxllnc Zaken.',
    },
    transactions: {
      title: '$t(layout:modules.transactions)',
      content:
        'In het transactieoverzicht staan alle transacties. Deze transacties kunnen opnieuw worden uitgevoerd of verwijderd worden.',
    },
    integrations: {
      title: '$t(layout:modules.integrations)',
      content:
        'In het configuratiescherm staan alle koppelingen die door de klant beheerd worden. Ook kunnen nieuwe koppelingen toegevoegd worden.',
    },
    datastore: {
      title: '$t(layout:modules.datastore)',
      content:
        'In het gegevensmagazijn staan alle geïmporteerde gegevensobjecten. Deze gegevensobjecten kunnen worden aangemaakt of verwijderd worden.',
    },
    configuration: {
      title: '$t(layout:modules.configuration)',
      content:
        'In de configuratie staan instellingen die voor de gehele omgeving gelden.',
    },

    // top bar
    quickSearch: {
      title: 'Algemeen zoeken',
      content: {
        1: 'Via algemeen zoeken kan door het volledige zaaksysteem gezocht worden op basis van één trefwoord van minimaal 3 karakters.',
        2: "Algemeen zoeken kan geactiveerd worden door er in te klikken of met de toetscombinatie 'Ctrl' + '/'.",
      },
    },
    quickSearchPreference: {
      title: 'Filtervoorkeur',
      description:
        "Wanneer veel met dezelfde filteroptie gewerkt wordt kan deze ingesteld worden als voorkeur. Bij het openen van het 'Algemeen zoeken' zal de filter automatisch ingesteld zijn op de ingestelde voorkeur.",
      icon: {
        lock_open: 'Er is geen voorkeur ingesteld.',
        lock: 'De huidig geselecteerde filteroptie is ingesteld als voorkeur.',
        no_encryption:
          'Er is een voorkeur ingesteld, maar het is niet de geselecteerde filteroptie.',
      },
    },
  },
};

export default locale;
