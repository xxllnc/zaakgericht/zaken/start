// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';

// enforces that either 'id' or 'title' + 'content' are supplied
type IdType = { id: HelpIdType };
type CustomType = { title: string; content: string | React.ReactNode };
type HelpDataByIdType = IdType & Partial<CustomType>;
type HelpDataCustomType = Partial<IdType> & CustomType;
export type HelpDialogSignalType = HelpDataByIdType | HelpDataCustomType;

export type HelpIdType =
  // main menu
  | 'dashboard'
  | 'communication'
  | 'documentIntake'
  | 'advancedSearch'
  | 'contactSearch'
  // | 'export'
  | 'catalog'
  | 'users'
  | 'log'
  | 'transactions'
  | 'integrations'
  | 'datastore'
  | 'configuration'

  // top bar
  | 'quickSearch'
  | 'quickSearchPreference';

export type HelpDataIndexType = { [key in HelpIdType]: GetterType };
export type GetterType = (t: i18next.TFunction, id: HelpIdType) => CustomType;

export type GetHelpDataType = (
  t: i18next.TFunction,
  input: HelpDialogSignalType
) => CustomType;

export type RectType = { height: number; width: number };
export type GetAngleToRotateType = (
  height: number,
  width: number,
  desiredDeviationInPx: number
) => number;
export type GetShakeSxType = (rect: RectType) => { [key: string]: any };
