// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { GetShakeSxType } from './HelpDialog.types';
import { getAngleToRotate } from './HelpDialog.library';

export const getWrapperSx = () => ({
  position: 'relative',
});

export const getHighlightSx = (offset: number) => ({
  position: 'relative',
  '&::after': {
    content: '""',
    position: 'absolute',
    top: offset,
    right: offset,
    bottom: offset,
    left: offset,
    borderRadius: 2,
    'pointer-events': 'none',
    border: '2px solid red',
  },
  clipPath: `inset(${offset}px round 8px)`,
});

export const getButtonOverlaySx = (isActive: boolean, offset: number) => ({
  padding: offset,
  position: 'absolute',
  top: offset,
  left: offset,
  right: offset,
  bottom: offset,
  cursor: 'pointer',
  'pointer-events': isActive ? 'auto' : 'none',
});

export const getBadgeSx = (offset: number) => ({
  position: 'absolute',
  top: offset,
  right: offset,
});

export const getShakeSx: GetShakeSxType = rect => {
  const height = Math.round(rect.height);
  const width = Math.round(rect.width);

  const shakeId = `shake-${height}-${width}`;
  const keyframes = `@keyframes ${shakeId}`;

  const desiredDeviationInPx = 3;
  const angle = getAngleToRotate(height, width, desiredDeviationInPx);

  return {
    animation: `${shakeId} 2s ease-out infinite`,
    [keyframes]: {
      '00%': { transform: `rotate(0deg)` },
      '50%': { transform: `rotate(0deg)` },
      '52%': { transform: `rotate(${angle}deg)` },
      '54%': { transform: `rotate(-${angle}deg)` },
      '56%': { transform: `rotate(${angle}deg)` },
      '58%': { transform: `rotate(-${angle}deg)` },
      '60%': { transform: `rotate(${angle}deg)` },
      '62%': { transform: `rotate(-${angle}deg)` },
      '64%': { transform: `rotate(0deg)` },
    },
  };
};
