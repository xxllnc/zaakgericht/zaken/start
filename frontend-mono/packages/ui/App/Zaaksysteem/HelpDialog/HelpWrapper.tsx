// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef, useState } from 'react';
import Badge from '@mui/material/Badge';
import { Box } from '@mui/material';
import {
  helpDialogData,
  helpMode,
  openHelpDialog,
  setHelpMode,
} from '@zaaksysteem/common/src/signals';
import {
  getHighlightSx,
  getButtonOverlaySx,
  getShakeSx,
  getBadgeSx,
  getWrapperSx,
} from './HelpWrapper.styles';
import { HelpDialogSignalType, RectType } from './HelpDialog.types';

type HelpWrapperPropsType = HelpDialogSignalType & {
  offset?: number;
  children: React.ReactNode;
};

const HelpWrapper: React.ComponentType<HelpWrapperPropsType> = ({
  id,
  title,
  content,
  offset = 0,
  children,
}) => {
  const elementRef = useRef<HTMLDivElement | null>(null);

  const data = { id, title, content } as HelpDialogSignalType;

  const { title: titleOfOpenDialog } = helpDialogData.value;
  const helpDialogIsOpen = Boolean(titleOfOpenDialog);

  const isActive = helpMode.value;
  const highlight = isActive && !helpDialogIsOpen;

  const [rect, setRect] = useState<RectType | null>();

  useEffect(() => {
    if (elementRef.current) {
      setRect(elementRef.current.getBoundingClientRect());
    }
  }, [isActive]);

  const wrapperSx = getWrapperSx();
  const badgeSx = getBadgeSx(offset);

  const highLightSx = getHighlightSx(offset);
  const shakeSx = rect ? getShakeSx(rect) : {};
  const wrappedSx = highlight ? { ...highLightSx, ...shakeSx } : {};

  const buttonOverlaySx = getButtonOverlaySx(highlight, offset);

  return (
    <Box sx={wrapperSx}>
      <Badge
        badgeContent="!"
        color="error"
        invisible={!highlight}
        sx={badgeSx}
      />
      <Box ref={elementRef} sx={wrappedSx}>
        {children}
        <Box
          sx={buttonOverlaySx}
          onClick={() => {
            setHelpMode(false);
            openHelpDialog(data);
          }}
        />
      </Box>
    </Box>
  );
};

export default HelpWrapper;
