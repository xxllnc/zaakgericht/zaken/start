// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import HelpWrapper from './HelpWrapper';

export { default as HelpDialog } from './HelpDialog';
export type { HelpDialogSignalType } from './HelpDialog.types';

export default HelpWrapper;
