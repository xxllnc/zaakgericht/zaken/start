// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useEffect, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import useSession, {
  hasActiveIntegration,
} from '@zaaksysteem/common/src/hooks/useSession';
import {
  FormatActivePhoneCallType,
  FormatContactType,
  FormatPhoneCallType,
  GetActivePhoneCallsType,
  GetKccUserStatusType,
  GetPhoneCallsType,
  KccUserStatusType,
  PerformAcceptPhoneCallType,
  PerformClearActivePhoneCallsType,
  PerformRejectPhoneCallType,
  PerformToggleKccUserStatusType,
} from './Kcc.types';
import {
  acceptPhoneCall,
  fetchActivePhoneCalls,
  fetchKccUserStatus,
  fetchPhoneCalls,
  rejectPhoneCall,
  toggleKccUserStatus,
  clearActivePhoneCalls,
  getContactV1,
  activate,
} from './Kcc.requests';
import { ACTIVE_PHONE_CALLS } from '.';

export const useKccContext = () => {
  const session = useSession();
  const hasKccIntegration = hasActiveIntegration(session, 'kcc');
  const [kccUserStatus, setKccUserStatus] = useState<KccUserStatusType>(0);
  const kccContext = { hasKccIntegration, kccUserStatus, setKccUserStatus };

  useEffect(() => {
    if (hasKccIntegration) {
      getKccUserStatus().then(setKccUserStatus);
    }
  }, []);

  return kccContext;
};

export const getKccUserStatus: GetKccUserStatusType = async () => {
  const response = await fetchKccUserStatus();

  return response.result[0].user_status;
};

export const performToggleKccUserStatus: PerformToggleKccUserStatusType =
  async kccUserStatus => {
    const toggleType = kccUserStatus ? 'disable' : 'enable';

    const response = await toggleKccUserStatus(toggleType);

    return response.result[0].user_status;
  };

const formatContact: FormatContactType = ({
  uuid,
  type,
  name,
  telephone_numbers,
  email_addresses,
  street,
  postal_code,
  city,
}) => ({
  uuid,
  type: type === 'natuurlijk_persoon' ? 'person' : 'organization',
  name,
  phoneNumbers: telephone_numbers,
  emails: email_addresses,
  address: [street, postal_code, city].join(', '),
});

const formatPhoneCall: FormatPhoneCallType = rawPhoneCall => {
  const { id } = rawPhoneCall;
  const contact = rawPhoneCall.betrokkene[0];

  return {
    id,
    contact: formatContact(contact),
  };
};

export const getPhoneCalls: GetPhoneCallsType = async () => {
  const response = await fetchPhoneCalls();

  const phoneCalls = response.result.map(rawPhoneCall =>
    formatPhoneCall(rawPhoneCall)
  );

  return phoneCalls;
};

const formatActivePhoneCall: FormatActivePhoneCallType = rawActivePhoneCall => {
  const contact = rawActivePhoneCall;

  return {
    id: null,
    contact: formatContact(contact),
  };
};

export const getActivePhoneCalls: GetActivePhoneCallsType = async () => {
  const response = await fetchActivePhoneCalls();

  const activePhoneCalls = response.result.map(rawPhoneCall =>
    formatActivePhoneCall(rawPhoneCall)
  );

  return activePhoneCalls;
};

export const performAcceptPhoneCall: PerformAcceptPhoneCallType = async id => {
  const data = { call_id: id };

  await acceptPhoneCall(data);
};

export const performRejectPhoneCall: PerformRejectPhoneCallType = async id => {
  const data = { call_id: id };

  await rejectPhoneCall(data);
};

export const performClearActivePhoneCalls: PerformClearActivePhoneCallsType =
  async () => {
    await clearActivePhoneCalls();
  };

export const useActivateContact = () => {
  const queryClient = useQueryClient();

  const activateContact = async (uuid: string) => {
    const contactV1 = await getContactV1(uuid);
    const identifier = contactV1.result.instance.old_subject_identifier;
    const data = { identifier };

    await activate(data);
  };

  const refreshPhoneCalls = () => {
    queryClient.invalidateQueries([ACTIVE_PHONE_CALLS]);
  };

  return { activateContact, refreshPhoneCalls };
};
