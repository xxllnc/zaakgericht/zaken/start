// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePhoneCallStyles = makeStyles(
  ({
    transitions,
    mintlab: { shadows },
    palette: { primary, common },
  }: Theme) => ({
    wrapper: {
      width: 400,
      boxShadow: shadows.sharp,
    },
    folded: {
      '& $contentWrapper': {
        height: 0,
        // @ts-ignore
        transition: transitions.create(['height'], {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }),
      },
    },
    unfolded: {
      '& $contentWrapper': {
        height: 150,
        // @ts-ignore
        transition: transitions.create(['height'], {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }),
      },
    },
    header: {
      boxShadow: shadows.sharp,
      padding: '10px 10px 10px 20px',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      gap: 10,
      backgroundColor: primary.main,
      color: common.white,
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8,
    },
    name: {
      flexGrow: 1,
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    },
    link: {
      color: common.white,
      '&:hover': {
        textDecoration: 'none',
      },
    },
    contentWrapper: {
      overflow: 'hidden',
      backgroundColor: common.white,
    },
    content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      padding: '0 20px',
      height: 150,
    },
    item: {
      display: 'flex',
      flexDirection: 'row',
      gap: 20,
    },
  })
);
