// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    skipToContent: 'Spring naar content',
    menu: 'Menu',
    modules: {
      styles: 'Huisstijl editor',
      dashboard: 'Dashboard',
      communication: 'Communicatie',
      documentIntake: 'Documentintake',
      advancedSearch: 'Uitgebreid zoeken',
      contactSearch: 'Contact zoeken',
      export: 'Exportbestanden',
      catalog: 'Catalogus',
      users: 'Gebruikers',
      log: 'Logboek',
      transactions: 'Transactieoverzicht',
      integrations: 'Koppelingconfiguratie',
      datastore: 'Gegevensmagazijn',
      configuration: 'Configuratie',
    },
    links: {
      help: 'Help',
      about: 'Over xxllnc Zaken',
      releaseNotes: 'Release notes',
      logout: 'Uitloggen',
    },
    new: 'Nieuw',
    dialog: {
      help: {
        title: 'Help',
        shortcuts: {
          title: 'Sneltoetsen',
          quickSearch: 'Snelzoeken openen',
          helpMode: 'Helpmodus activeren',
          helpModeClose: 'Helpmodus sluiten',
        },
        page: {
          title: 'Helppagina',
          description:
            "Raadpleeg de helppagina van xxllnc Zaken voor meer uitleg over de verschillende pagina's en functionaliteiten.",
        },
      },
      about: {
        title: 'Over xxllnc Zaken',
        text: 'xxllnc Zaken is een complete oplossing voor gemeenten om de dienstverlening te verbeteren.',
        items: {
          application: {
            label: 'Applicatie',
          },
          supplier: {
            label: 'Leverancier',
          },
          license: {
            label: 'Licentie',
          },
          startDate: {
            label: 'Startdatum',
          },
          endDate: {
            label: 'Einddatum',
            value: 'n.v.t.',
          },
          documentation: {
            label: 'Documentatie',
          },
        },
      },
      releaseNotes: {
        title: 'Release notes',
        text: 'Releasenotes zijn alle ontwikkelingen en oplossingen die een release bevat. Bekijk hier alle release notes van voorgaande en aankomende versies van xxllnc Zaken',
      },
    },
    profile: {
      profile: 'Profiel',
      buttons: {
        notifications: 'Notificaties',
        cases: 'Zaken',
        settings: 'Gegevens',
        phone: {
          deactivate: 'Afmelden',
          activate: 'Aanmelden',
        },
      },
      info: {
        name: 'Naam',
        phonenumber: 'Telefoonnummer',
        email: 'E-mail',
        department: 'Afdeling',
        roles: 'Rollen',
      },
      notifications: {
        title: 'Notificaties',
        none: 'Er zijn geen notificaties.',
        link: 'Zaak {{caseId}}',
      },
      archive: {
        single: {
          title: 'Notificatie archiveren',
          snack: 'Notificatie gearchiveerd.',
        },
        all: {
          title: 'Alle notificaties archiveren',
          text: 'Weet u zeker dat u alle notificaties wilt archiveren?',
          snack: 'Notificaties gearchiveerd.',
        },
      },
    },
    kcc: {
      pickup: 'Opnemen',
      hangup: 'Ophangen',
      fold: 'Inklappen',
      unfold: 'Uitklappen',
      createCase: 'Zaak aanmaken',
      createContactMoment: 'Contactmoment aanmaken',
      deactivate: 'Deactiveer contact',
    },
  },
};

export default locale;
