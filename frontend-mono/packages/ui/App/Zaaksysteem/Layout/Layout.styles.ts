// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useLayoutStyles = makeStyles(
  ({ typography, palette: { common } }: Theme) => ({
    wrapper: {
      fontFamily: typography.fontFamily,
      display: 'flex',
      flexDirection: 'row',
      width: '100vw',
      height: '100vh',
    },
    view: {
      flex: 1,
      overflow: 'auto',
      display: 'flex',
      flexDirection: 'column',
    },
    skip: {
      position: 'absolute',
      padding: 10,
      backgroundColor: common.white,
      top: -40,
      '&:focus': {
        top: 0,
      },
    },
    content: {
      flex: 1,
      overflow: 'auto',
    },
    phonePopup: {
      position: 'absolute',
    },
  })
);
