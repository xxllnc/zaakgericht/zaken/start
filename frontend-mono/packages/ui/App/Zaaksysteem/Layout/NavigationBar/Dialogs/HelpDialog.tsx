// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { Typography } from '@mui/material';
import Table from '@mui/material/Table/Table';
import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';
import TableBody from '@mui/material/TableBody/TableBody';
import { useHelpDialogStyles } from './HelpDialog.styles';

type HelpDialogPropsType = {
  onClose: () => void;
};

const HelpDialog: React.ComponentType<HelpDialogPropsType> = ({ onClose }) => {
  const [t] = useTranslation('layout');
  const classes = useHelpDialogStyles();
  const dialogEl = useRef();

  const title = t('dialog.help.title');
  const helpPageUrl = 'help.zaaksysteem.nl';

  const isMac = navigator.appVersion.indexOf('Mac') != -1;
  const ctrl = isMac ? 'command' : 'ctrl';

  const shortcuts = {
    quickSearch: [ctrl, '/'],
    helpMode: ['F2'],
  };

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={true}
        onClose={onClose}
        scope={'help-dialog'}
        ref={dialogEl}
        maxWidth="xs"
      >
        <DialogTitle
          elevated={true}
          icon="help"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.wrapper}>
            <div className={classes.section}>
              <Typography variant="h3">
                {t('dialog.help.page.title')}
              </Typography>
              <div id="help-page-description" className={classes.description}>
                {t('dialog.help.page.description')}
              </div>
              <a
                href={`https://${helpPageUrl}`}
                target="_blank"
                rel="noreferrer"
                aria-labelledby="help-page-description"
              >
                {helpPageUrl}
              </a>
            </div>
            <div className={classes.section}>
              <Typography variant="h3">
                {t('dialog.help.shortcuts.title')}
              </Typography>
              <Table>
                <TableBody>
                  {Object.entries(shortcuts).map(([name, keys]) => {
                    const label = t(`dialog.help.shortcuts.${name}`);
                    const ariaShortcut = keys.join(` ${t('common:and')} `);

                    return (
                      <TableRow key={name}>
                        <TableCell
                          tabIndex={0}
                          role="textbox"
                          aria-label={`${label}: ${ariaShortcut}`}
                        >
                          {label}
                        </TableCell>
                        <TableCell aria-hidden="true">
                          {keys.map((key, index) => (
                            <>
                              <span key={index} className={classes.key}>
                                {key}
                              </span>
                              {index < keys.length - 1 && <span>+</span>}
                            </>
                          ))}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default HelpDialog;
