// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Icon from '@mintlab/ui/App/Material/Icon/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ListItemPropsType } from '../NavigationBar.types';
import { useNavigationBarStyles } from '../NavigationBar.styles';

const ListItem: React.ComponentType<ListItemPropsType> = ({ open, button }) => {
  const classes = useNavigationBarStyles();
  const { label, icon } = button;

  return (
    <>
      <ListItemIcon classes={{ root: classes.icon }}>
        <Tooltip
          sx={{ display: 'flex', justifyContent: 'center' }}
          style={{ width: 'initial' }}
          disabled={open}
          title={label}
          placement="right"
        >
          <Icon size="small">{icon}</Icon>
        </Tooltip>
      </ListItemIcon>

      <div className={classes.label}>
        <ListItemText>{label}</ListItemText>
      </div>
    </>
  );
};

export default ListItem;
