// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import ListItemButton from '@mui/material/ListItemButton';
import { useNavigationBarStyles } from '../NavigationBar.styles';
import { ButtonType } from '../NavigationBar.types';
import ListItem from './ListItem';

type ListItemBtnPropsType = {
  button: ButtonType;
  open: boolean;
  currentLocation: any;
};

const ListItemBtn: React.ComponentType<ListItemBtnPropsType> = ({
  button,
  open,
  currentLocation,
}) => {
  const classes = useNavigationBarStyles();
  const { link = '', action, external } = button;
  const isCurrentPage = link && currentLocation.pathname.includes(link);
  const linkWithinApp = /^\/(admin)|(main)\//.test(link);

  return (
    <ListItemButton
      LinkComponent={linkWithinApp ? Link : 'a'}
      {...{ to: link }}
      href={link}
      aria-label={button.label}
      target={external ? '_new' : undefined}
      onClick={action}
      classes={{
        root: classNames(classes.button, {
          [classes.buttonActive]: isCurrentPage && !open,
          [classes.buttonOpenActive]: isCurrentPage && open,
        }),
      }}
    >
      <ListItem open={open} button={button} />
    </ListItemButton>
  );
};

export default ListItemBtn;
