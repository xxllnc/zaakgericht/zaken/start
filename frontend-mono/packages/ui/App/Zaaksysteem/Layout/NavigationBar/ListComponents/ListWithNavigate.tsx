// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useLocation } from 'react-router-dom';
import List from '@mui/material/List';
import HelpWrapper from '../../../HelpDialog';
import { ListComponentPropstype } from './../NavigationBar.types';
import ListItemBtn from './ListItemButton';

const ListWithNavigate: React.ComponentType<ListComponentPropstype> = ({
  open,
  buttons,
}) => {
  const currentLocation = useLocation();

  return (
    <List disablePadding>
      {/* eslint complexity: [2, 9] */}
      {buttons.map((button, index) => {
        const { helpId } = button;
        const key = `main-menu-${index}`;

        return helpId ? (
          <HelpWrapper key={key} id={helpId} offset={2}>
            <ListItemBtn
              button={button}
              open={open}
              currentLocation={currentLocation}
            />
          </HelpWrapper>
        ) : (
          <ListItemBtn
            key={key}
            button={button}
            open={open}
            currentLocation={currentLocation}
          />
        );
      })}
    </List>
  );
};

export default ListWithNavigate;
