// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { iconNames } from '@mintlab/ui/App/Material/Icon/Icon';
import { hasCapability } from '@zaaksysteem/common/src/hooks/useSession';
import { ButtonListType, GetButtonListsType } from './NavigationBar.types';

/* eslint complexity: [2, 7] */
export const getButtons: GetButtonListsType = (t, session, setDialogOpen) => {
  const isGebruiker = hasCapability(session, 'gebruiker');
  const hasDashboard = hasCapability(session, 'dashboard');
  const internAllowed = isGebruiker && hasDashboard;

  const mainButtonList: ButtonListType = [
    {
      link: '/intern',
      icon: iconNames.home,
      label: t('modules.dashboard'),
      show: internAllowed,
      // Remove this when dashboard is moved to V2
      action: () => {
        const iframe = document.querySelector<HTMLIFrameElement>('#intern');

        if (iframe) {
          iframe.src = '/intern';
        }
      },
      helpId: 'dashboard',
    },
    {
      link: '/main/communication',
      icon: iconNames.chat_bubble,
      label: t('modules.communication'),
      show: internAllowed && hasCapability(session, 'message_intake'),
      helpId: 'communication',
    },
    {
      link: '/main/document-intake',
      icon: iconNames.insert_drive_file,
      label: t('modules.documentIntake'),
      show:
        internAllowed && hasCapability(session, 'documenten_intake_subject'),
      helpId: 'documentIntake',
    },
    {
      link: '/search',
      icon: iconNames.search,
      label: t('modules.advancedSearch'),
      show: internAllowed && hasCapability(session, 'search'),
      helpId: 'advancedSearch',
    },
    {
      link: '/main/contact-search',
      icon: iconNames.person_search,
      label: t('modules.contactSearch'),
      show: internAllowed && hasCapability(session, 'contact_search'),
      helpId: 'contactSearch',
    },
    {
      link: '/main/export-files',
      icon: iconNames.download,
      label: t('modules.export'),
      show: internAllowed,
      // helpId: 'export',
    },
  ];

  const isZaaktypebeheerder = hasCapability(session, 'beheer_zaaktype_admin');
  const isGebruikersBeheerder = hasCapability(session, 'useradmin');
  const isAdmin = hasCapability(session, 'admin');

  const adminButtonList: ButtonListType = [
    {
      link: '/main/catalog',
      icon: iconNames.extension,
      label: t('modules.catalog'),
      show: isZaaktypebeheerder,
      helpId: 'catalog' as 'catalog',
    },
    {
      link: '/main/users',
      icon: iconNames.people,
      label: t('modules.users'),
      show: isGebruikersBeheerder,
      helpId: 'users' as 'users',
    },
    {
      link: '/main/log',
      icon: iconNames.terminal,
      label: t('modules.log'),
      show: isAdmin,
      helpId: 'log' as 'log',
    },
    {
      link: '/main/transactions',
      icon: iconNames.import_export,
      label: t('modules.transactions'),
      show: isAdmin,
      helpId: 'transactions' as 'transactions',
    },
    {
      link: '/main/integrations',
      icon: iconNames.link,
      label: t('modules.integrations'),
      show: isAdmin,
      helpId: 'integrations' as 'integrations',
    },
    {
      link: '/main/datastore',
      icon: iconNames.share,
      label: t('modules.datastore'),
      show: isAdmin,
      helpId: 'datastore' as 'datastore',
    },
    // {
    //   link: '/main/styles',
    //   icon: iconNames.palette,
    //   label: t('modules.styles'),
    //   show: isAdmin,
    // },
    {
      link: '/main/configuration',
      icon: iconNames.settings,
      label: t('modules.configuration'),
      show: isAdmin,
      helpId: 'configuration' as 'configuration',
    },
  ].filter(Boolean);

  const systemButtonList: ButtonListType = [
    {
      action: () => setDialogOpen('help'),
      icon: iconNames.help,
      label: t('links.help'),
      show: internAllowed,
    },
    {
      action: () => setDialogOpen('about'),
      icon: iconNames.info,
      label: t('links.about'),
      show: internAllowed,
    },
    {
      action: () => setDialogOpen('releaseNotes'),
      icon: iconNames.lightbulb,
      label: t('links.releaseNotes'),
      show: internAllowed,
    },
    {
      link: '/auth/logout',
      icon: iconNames.power_settings_new,
      label: t('links.logout'),
      show: true,
    },
  ];

  return [mainButtonList, adminButtonList, systemButtonList]
    .map(buttons => buttons.filter(button => button.show))
    .filter(buttons => buttons.length);
};
