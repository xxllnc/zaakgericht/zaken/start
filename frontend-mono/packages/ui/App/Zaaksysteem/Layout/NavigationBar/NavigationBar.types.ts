// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import { IconNameType } from '@mintlab/ui/App/Material/Icon/Icon';
import { HelpIdType } from '../../HelpDialog/HelpDialog.types';

export type DialogType = 'help' | 'about' | 'releaseNotes' | null;

export type ButtonType = {
  action?: () => void;
  link?: string;
  icon: IconNameType;
  label: string;
  show: boolean;
  external?: boolean;
  helpId?: HelpIdType;
};

export type ButtonListType = ButtonType[];

export type GetButtonListsType = (
  t: i18next.TFunction,
  session: SessionType,
  setDialogOpen: (dialogType: DialogType) => void
) => ButtonListType[];

export type ListComponentPropstype = {
  open: boolean;
  buttons: ButtonListType;
};

export type ListItemPropsType = {
  open: boolean;
  button: ButtonType;
};
