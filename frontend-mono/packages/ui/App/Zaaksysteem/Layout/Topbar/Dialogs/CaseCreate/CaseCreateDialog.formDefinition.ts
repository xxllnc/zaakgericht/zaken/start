// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { CaseTypeOptionType } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder.library';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import {
  ContactChannelType,
  ObjectType,
  SavedCaseTypeType,
} from './CaseCreateDialog.types';
import { getShouldPrefillCaseType } from './CaseCreateDialog.library';

export type CaseCreateFormValuesType = {
  documentName?: string;
  objectName?: string;
  caseType: CaseTypeOptionType;
  requestor: ValueType<string>;
  recipient: ValueType<string>;
  contactChannel: ValueType<string>;
  searchType: any;
};

export const getCaseCreateFormDefinition: ({
  t,
  prefillContactOption,
  prefillCaseTypeOption,
  contactChannel,
  documentName,
  legacyDocumentId,
  allowCaseTypeChange,
  object,
}: {
  t: i18next.TFunction;
  prefillContactOption?: ContactType | null;
  prefillCaseTypeOption?: SavedCaseTypeType | null;
  contactChannel?: ContactChannelType;
  documentName?: string;
  legacyDocumentId?: number;
  allowCaseTypeChange: boolean;
  object?: ObjectType;
}) => FormDefinition<CaseCreateFormValuesType> = ({
  t,
  prefillContactOption,
  prefillCaseTypeOption,
  contactChannel = 'behandelaar',
  documentName = '',
  legacyDocumentId,
  allowCaseTypeChange,
  object,
}) => {
  const shouldPrefillCaseType = getShouldPrefillCaseType(
    prefillContactOption,
    prefillCaseTypeOption
  );

  return [
    {
      name: 'objectName',
      label: t('object.label'),
      type: fieldTypes.TEXT,
      value: object?.label || '',
      required: false,
      readOnly: true,
      hidden: !object,
      config: {
        style: {
          wordWrap: 'break-word',
        },
      },
    },
    {
      name: 'documentName',
      label: t('document.label'),
      type: fieldTypes.TEXT,
      value: documentName || '',
      required: false,
      readOnly: true,
      hidden: !legacyDocumentId,
      config: {
        style: {
          wordWrap: 'break-word',
        },
      },
    },
    {
      name: 'caseType',
      label: t('caseType.label'),
      placeholder: t('caseType.placeholder'),
      type: fieldTypes.CASE_TYPE_FINDER,
      value: shouldPrefillCaseType ? prefillCaseTypeOption : null,
      required: true,
      config: {
        type: prefillContactOption?.type,
        prefillActive: shouldPrefillCaseType,
        supportPrefillOption: true,
      },
      disabled: !allowCaseTypeChange,
    },
    {
      name: 'requestor',
      type: fieldTypes.CONTACT_FINDER_WITH_ADVANCED_SEARCH,
      label: t('requestor.label'),
      placeholder: t('requestor.placeholder'),
      value: prefillContactOption || null,
      required: true,
      disabled: false,
    },
    {
      name: 'recipient',
      label: t('recipient.label'),
      placeholder: t('recipient.placeholder'),
      type: fieldTypes.CONTACT_FINDER,
      value: '',
      required: false,
    },
    {
      name: 'contactChannel',
      label: t('contactChannel.label'),
      placeholder: t('contactChannel.placeholder'),
      type: fieldTypes.SELECT,
      value: contactChannel,
      required: true,
      choices: [
        { value: 'behandelaar', label: 'Behandelaar' },
        { value: 'balie', label: 'Balie' },
        { value: 'telefoon', label: 'Telefoon' },
        { value: 'post', label: 'Post' },
        { value: 'email', label: 'E-mail' },
        { value: 'webformulier', label: 'Webformulier' },
        { value: 'sociale media', label: 'Sociale media' },
        { value: 'externe applicatie', label: 'Externe applicatie' },
      ],
    },
  ];
};
