// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const locale = {
  nl: {
    title: 'Zaak aanmaken',
    requestorTitle: 'Zaak aanmaken voor',
    caseType: {
      label: 'Zaaktype',
      placeholder: 'Selecteer zaaktype',
      remember: 'Onthouden',
    },
    requestor: {
      label: 'Aanvrager',
      placeholder: 'Selecteer aanvrager',
      choices: {
        person: 'Burger',
        organization: 'Organisatie',
        employee: 'Medewerker',
      },
    },
    recipient: {
      label: 'Ontvanger',
      placeholder: 'Selecteer ontvanger',
    },
    contactChannel: {
      label: 'Contactkanaal',
      placeholder: 'Selecteer contactkanaal',
    },
    document: {
      label: 'Document',
    },
    object: {
      label: 'Object',
    },
  },
};

export default locale;
