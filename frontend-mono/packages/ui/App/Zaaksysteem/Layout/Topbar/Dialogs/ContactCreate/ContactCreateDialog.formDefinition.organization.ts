// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  setOptional,
  setRequired,
  hideFields,
  showFields,
  hasValue,
} from '@zaaksysteem/common/src/components/form/rules';
import { COUNTRY_CODE_CURACAO } from '@zaaksysteem/common/src/constants/countryCodes.constants';
import {
  GetOrganizationFormDefinitionType,
  GetOrganizationRulesType,
} from './ContactCreateDialog.types';

export const getOrganizationFormDefinition: GetOrganizationFormDefinitionType =
  (t, countryCode, countryCodes, legalEntityTypes) => {
    return [
      {
        name: 'vestiging_landcode',
        type: fieldTypes.FLATVALUE_SELECT,
        value: countryCode,
        isClearable: false,
        required: true,
        choices: countryCodes,
      },
      {
        name: 'rechtsvorm',
        type: fieldTypes.FLATVALUE_SELECT,
        value: null,
        isClearable: false,
        required: true,
        choices: legalEntityTypes,
      },
      {
        name: 'dossiernummer',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'vestigingsnummer',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'handelsnaam',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'npc-telefoonnummer',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'npc-mobiel',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'npc-email',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'org-briefadres',
        type: fieldTypes.CHECKBOX,
        value: false,
        required: false,
      },
      {
        name: 'vestiging_straatnaam',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'vestiging_huisnummer',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'vestiging_huisletter',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'vestiging_huisnummertoevoeging',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'vestiging_postcode',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'vestiging_woonplaats',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'vestiging_adres_buitenland1',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'vestiging_adres_buitenland2',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'vestiging_adres_buitenland3',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'correspondentie_landcode',
        type: fieldTypes.FLATVALUE_SELECT,
        value: countryCode,
        isClearable: false,
        required: true,
        choices: countryCodes,
      },
      {
        name: 'correspondentie_straatnaam',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'correspondentie_huisnummer',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'correspondentie_huisletter',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'correspondentie_huisnummertoevoeging',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'correspondentie_postcode',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'correspondentie_woonplaats',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'correspondentie_adres_buitenland1',
        type: fieldTypes.TEXT,
        value: '',
        required: true,
      },
      {
        name: 'correspondentie_adres_buitenland2',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'correspondentie_adres_buitenland3',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
    ].map(field => ({
      label: t(`organization.label.${field.name}`),
      ...field,
    }));
  };

const companyFields = ['rechtsvorm', 'dossiernummer', 'vestigingsnummer'];

const residenceAddressFields = [
  'vestiging_straatnaam',
  'vestiging_huisnummer',
  'vestiging_huisletter',
  'vestiging_huisnummertoevoeging',
  'vestiging_postcode',
  'vestiging_woonplaats',
];

const residenceRequiredFields = [
  'vestiging_straatnaam',
  'vestiging_huisnummer',
  'vestiging_postcode',
  'vestiging_woonplaats',
];

const foreignAddressFields = [
  'vestiging_adres_buitenland1',
  'vestiging_adres_buitenland2',
  'vestiging_adres_buitenland3',
];

const correspondenceAddressFields = [
  'correspondentie_straatnaam',
  'correspondentie_huisnummer',
  'correspondentie_huisletter',
  'correspondentie_huisnummertoevoeging',
  'correspondentie_postcode',
  'correspondentie_woonplaats',
];

const correspondenceRequiredFields = [
  'correspondentie_straatnaam',
  'correspondentie_huisnummer',
  'correspondentie_postcode',
  'correspondentie_woonplaats',
];

const correspondenceForeignFields = [
  'correspondentie_adres_buitenland1',
  'correspondentie_adres_buitenland2',
  'correspondentie_adres_buitenland3',
];

export const getOrganizationRules: GetOrganizationRulesType =
  customerCountryCode => [
    new Rule()
      .when('vestigingsnummer', hasValue)
      .then(setRequired(['dossiernummer']))
      .else(setOptional(['dossiernummer'])),
    new Rule()
      .when('vestiging_landcode', (field: any) => {
        const countryCode = field?.value;

        return customerCountryCode == countryCode;
      })
      .then(showFields(companyFields))
      .then(setRequired(['rechtsvorm']))
      .then(showFields(residenceAddressFields))
      .then(setRequired(residenceRequiredFields))
      .then(hideFields(foreignAddressFields))
      .then(setOptional([foreignAddressFields[0]]))
      .else(hideFields(companyFields))
      .else(setOptional(['rechtsvorm']))
      .else(hideFields(residenceAddressFields))
      .else(setOptional(residenceRequiredFields))
      .else(showFields(foreignAddressFields))
      .else(setRequired([foreignAddressFields[0]])),
    new Rule()
      .when('org-briefadres', (field: any) => field?.value)
      .then(showFields(['correspondentie_landcode']))
      .else(hideFields(['correspondentie_landcode'])),
    new Rule()
      .when('vestiging_landcode', () => true)
      .then(hideFields(correspondenceAddressFields))
      .then(setOptional(correspondenceAddressFields))
      .then(hideFields(correspondenceForeignFields))
      .then(setOptional(correspondenceForeignFields)),
    new Rule()
      .when('correspondentie_landcode', (field: any) => {
        const countryCode = field?.value;

        return !field.hidden && customerCountryCode == countryCode;
      })
      .then(showFields(correspondenceAddressFields))
      .then(setRequired(correspondenceRequiredFields)),
    new Rule()
      .when('correspondentie_landcode', (field: any) => {
        const countryCode = field?.value;

        return !field.hidden && customerCountryCode != countryCode;
      })
      .then(showFields(correspondenceForeignFields))
      .then(setRequired([correspondenceForeignFields[0]])),
    new Rule()
      .when('vestiging_landcode', (field: any) => {
        const countryCode = field?.value;

        return countryCode === COUNTRY_CODE_CURACAO;
      })
      .then(hideFields(['vestiging_postcode']))
      .then(setOptional(['vestiging_postcode'])),
    new Rule()
      .when('correspondentie_landcode', (field: any) => {
        const countryCode = field?.value;

        return countryCode === COUNTRY_CODE_CURACAO;
      })
      .then(hideFields(['correspondentie_postcode']))
      .then(setOptional(['correspondentie_postcode'])),
  ];
