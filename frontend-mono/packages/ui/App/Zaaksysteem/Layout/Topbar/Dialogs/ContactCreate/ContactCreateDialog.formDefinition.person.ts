// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { COUNTRY_CODE_CURACAO } from '@zaaksysteem/common/src/constants/countryCodes.constants';
import {
  Rule,
  setFieldValue,
  setOptional,
  setRequired,
  hideFields,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import {
  GetPersonFormDefinitionType,
  GetPersonRulesType,
  SexChoiceType,
} from './ContactCreateDialog.types';

export const getPersonFormDefinition: GetPersonFormDefinitionType = (
  t,
  countryCode,
  countryCodes
) => {
  const sexChoices: SexChoiceType[] = [
    {
      label: t('person.sex.male'),
      value: 'M',
    },
    {
      label: t('person.sex.female'),
      value: 'V',
    },
    {
      label: t('person.sex.other'),
      value: 'X',
    },
  ];

  return [
    {
      name: 'np-burgerservicenummer',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-persoonsnummer',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },

    {
      name: 'np-voornamen',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-voorvoegsel',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-geslachtsnaam',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-adellijke_titel',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-geslachtsaanduiding',
      type: fieldTypes.RADIO_GROUP,
      value: null,
      required: false,
      choices: sexChoices,
    },
    {
      name: 'np-landcode',
      type: fieldTypes.FLATVALUE_SELECT,
      value: countryCode,
      isClearable: false,
      required: true,
      choices: countryCodes,
    },
    {
      name: 'npc-telefoonnummer',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'npc-mobiel',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'npc-email',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'briefadres',
      type: fieldTypes.CHECKBOX,
      value: false,
      required: false,
    },
    {
      name: 'np-straatnaam',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-huisnummer',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    // Not supported in v0
    // {
    //   name: 'np-huisletter',
    //   type: fieldTypes.TEXT,
    //   value: '',
    //   required: false,
    // },
    {
      name: 'np-huisnummertoevoeging',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-postcode',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-woonplaats',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-in_gemeente',
      type: fieldTypes.CHECKBOX,
      value: false,
      required: false,
    },
    {
      name: 'np-adres_buitenland1',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-adres_buitenland2',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-adres_buitenland3',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-correspondentie_straatnaam',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-correspondentie_huisnummer',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    // Not supported in v0
    // {
    //   name: 'np-correspondentie_huisletter',
    //   type: fieldTypes.TEXT,
    //   value: '',
    //   required: false,
    // },
    {
      name: 'np-correspondentie_huisnummertoevoeging',
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'np-correspondentie_postcode',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
    {
      name: 'np-correspondentie_woonplaats',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
    },
  ].map(field => ({
    label: t(`person.label.${field.name}`),
    ...field,
  }));
};

const residenceAddressFields = [
  'np-straatnaam',
  'np-huisnummer',
  // 'np-huisnummerletter',
  'np-huisnummertoevoeging',
  'np-postcode',
  'np-woonplaats',
];

const correspondenceAddressFields = [
  'np-correspondentie_straatnaam',
  'np-correspondentie_huisnummer',
  // 'np-correspondentie_huisnummerletter',
  'np-correspondentie_huisnummertoevoeging',
  'np-correspondentie_postcode',
  'np-correspondentie_woonplaats',
];

const foreignAddressFields = [
  'np-adres_buitenland1',
  'np-adres_buitenland2',
  'np-adres_buitenland3',
];

const residenceRequiredFields = [
  'np-straatnaam',
  'np-huisnummer',
  'np-zipcode',
  'np-woonplaats',
];

const correspondenceRequiredFields = [
  'np-correspondentie_straatnaam',
  'np-correspondentie_huisnummer',
  'np-correspondentie_postcode',
  'np-correspondentie_woonplaats',
];

const unsupportedByCuracao = ['np-postcode', 'np-correspondentie_postcode'];

export const getPersonRules: GetPersonRulesType = customerCountryCode => {
  return [
    new Rule()
      .when('np-landcode', (field: any) => {
        const countryCode = field?.value;

        return customerCountryCode == countryCode;
      })
      .then(showFields(residenceAddressFields))
      .then(setRequired(residenceRequiredFields))
      .then(hideFields(foreignAddressFields))
      .then(setOptional(['np-adres_buitenland1']))
      .then(showFields(['briefadres']))
      .else(showFields(foreignAddressFields))
      .else(setRequired(['np-adres_buitenland1']))
      .else(hideFields(residenceAddressFields))
      .else(setOptional(residenceRequiredFields))
      .else(hideFields(['briefadres']))
      .else(setFieldValue('briefadres', false)),
    new Rule()
      .when('briefadres', (field: any) => field?.value)
      .then(showFields(correspondenceAddressFields))
      .then(setRequired(correspondenceRequiredFields))
      .then(setOptional(residenceRequiredFields))
      .else(setRequired(residenceRequiredFields))
      .else(hideFields(correspondenceAddressFields))
      .else(setOptional(correspondenceRequiredFields)),
    new Rule()
      .when('np-landcode', (field: any) => {
        const countryCode = field?.value;

        return countryCode === COUNTRY_CODE_CURACAO;
      })
      .then(showFields(['np-persoonsnummer']))
      .then(hideFields(unsupportedByCuracao))
      .then(setOptional(unsupportedByCuracao))
      .else(hideFields(['np-persoonsnummer'])),
  ];
};
