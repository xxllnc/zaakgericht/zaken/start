// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { openServerError } from '@zaaksysteem/common/src/signals';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  getPersonFormDefinition,
  getPersonRules,
} from './ContactCreateDialog.formDefinition.person';
import {
  getOrganizationFormDefinition,
  getOrganizationRules,
} from './ContactCreateDialog.formDefinition.organization';
import { performCreateContact } from './ContactCreateDialog.library';
import {
  ContactCreateDialogPropsType,
  FieldsType,
} from './ContactCreateDialog.types';

const ContactCreateDialog: React.ComponentType<
  ContactCreateDialogPropsType
> = ({
  open,
  onClose,
  type,
  countryCodes,
  legalEntityTypes = [],
  setSnack,
  openErrorDialog,
}) => {
  const [t] = useTranslation('contactCreate');

  const session = useSession();
  const countryCode = session.account.instance.country_code;

  const formDefinition =
    type === 'person'
      ? getPersonFormDefinition(t, countryCode, countryCodes)
      : getOrganizationFormDefinition(
          t,
          countryCode,
          countryCodes,
          legalEntityTypes
        );
  const rules =
    type === 'person'
      ? getPersonRules(countryCode)
      : getOrganizationRules(countryCode);

  let {
    fields,
    formik: { isValid, resetForm },
  } = useForm({
    formDefinition,
    rules,
  });

  const handleSubmit = () => {
    performCreateContact(t, type, fields as FieldsType)
      .then(response => {
        if (response.errors?.length) {
          openErrorDialog({
            title: t('error.title'),
            message: (
              <>
                {response.errors.map((error, index) => (
                  <div key={index}>{error}</div>
                ))}
              </>
            ),
          });
        } else {
          onClose();
          setSnack({
            link: `/main/contact-view/${type}/${response.uuid}`,
          });
          resetForm();
        }
      })
      .catch(openServerError);
  };

  const entity = t(`common:entityType.${type}`);
  const verb = t('common:verbs.create').toLocaleLowerCase();
  const dialogTitle = `${entity} ${verb}`;
  const dialogIcon = type === 'person' ? 'person' : 'domain';

  return (
    <Dialog
      aria-label={dialogTitle}
      open={open}
      onClose={onClose}
      disableBackdropClick={true}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        id={'contact-create-dialog'}
        title={dialogTitle}
        icon={dialogIcon}
        onCloseClick={onClose}
        scope={'contact-create-dialog'}
      />

      <DialogContent>
        {fields.map(({ FieldComponent, key, type, suppressLabel, ...rest }) => {
          const props = cloneWithout(rest, 'definition', 'mode');

          return (
            <FormControlWrapper
              {...props}
              label={suppressLabel ? false : props.label}
              key={`${props.name}-formcontrol-wrapper`}
            >
              <FieldComponent {...props} t={t} />
            </FormControlWrapper>
          );
        })}
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                disabled: !isValid,
                text: t('common:verbs.create'),
                onClick() {
                  handleSubmit();
                },
              },
              {
                text: t('common:forms.cancel'),
                onClick: onClose,
              },
            ],
            'contact-create-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default ContactCreateDialog;
