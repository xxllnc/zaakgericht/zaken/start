// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

// COUNTRY CODES

export type CountyCodeResponseBodyType = {
  result: {
    instance: { rows: { instance: { dutch_code: number; label: string } }[] };
  };
};

export type FetchCountryCodesType = () => Promise<CountyCodeResponseBodyType>;

export type CountryCodeType = {
  value: number;
  label: string;
};

export type GetCountryCodesType = () => Promise<CountryCodeType[]>;

// LEGAL ENTITY TYPES

export type LegalEntityResponseBodyType = {
  result: {
    instance: {
      rows: { instance: { code: string; label: string; active: boolean } }[];
    };
  };
};

export type FetchLegalEntityTypesType =
  () => Promise<LegalEntityResponseBodyType>;

export type LegalEntityTypeType = {
  value: string;
  label: string;
};

export type GetLegalEntityTypesType = () => Promise<LegalEntityTypeType[]>;

// CONTACT

export type ContactTypeType = 'person' | 'organization';

type SexType = 'M' | 'V' | 'X';

export type SexChoiceType = { label: string; value: SexType };

export type PersonValuesType = {
  'np-burgerservicenummer': ValueType<string>;
  'np-persoonsnummer': ValueType<string>;
  'np-voornamen': ValueType<string>;
  'np-voorvoegsel': ValueType<string>;
  'np-geslachtsnaam': ValueType<string>;
  'np-adellijke_titel': ValueType<string>;
  'np-geslachtsaanduiding': ValueType<SexType>;
  'np-landcode': ValueType<CountryCodeType['value']>;
  'npc-telefoonnummer': ValueType<string>;
  'npc-mobiel': ValueType<string>;
  'npc-email': ValueType<string>;
  briefadres: ValueType<boolean>;
  'np-straatnaam': ValueType<string>;
  'np-huisnummer': ValueType<string>;
  'np-huisnummertoevoeging': ValueType<string>;
  'np-postcode': ValueType<string>;
  'np-woonplaats': ValueType<string>;
  'np-in_gemeente': ValueType<boolean>;
  'np-adres_buitenland1': ValueType<string>;
  'np-adres_buitenland2': ValueType<string>;
  'np-adres_buitenland3': ValueType<string>;
  'np-correspondentie_straatnaam': ValueType<string>;
  'np-correspondentie_huisnummer': ValueType<string>;
  'np-correspondentie_huisnummertoevoeging': ValueType<string>;
  'np-correspondentie_postcode': ValueType<string>;
  'np-correspondentie_woonplaats': ValueType<string>;
};

export type OrganizationValuesType = {
  vestiging_landcode: ValueType<CountryCodeType['value']>;
  rechtsvorm: ValueType<LegalEntityTypeType['value']>;
  dossiernummer: ValueType<string>;
  vestigingsnummer: ValueType<string>;
  handelsnaam: ValueType<string>;
  'npc-telefoonnummer': ValueType<string>;
  'npc-mobiel': ValueType<string>;
  'npc-email': ValueType<string>;
  'org-briefadres': ValueType<boolean>;
  vestiging_straatnaam: ValueType<string>;
  vestiging_huisnummer: ValueType<string>;
  vestiging_huisletter: ValueType<string>;
  vestiging_huisnummertoevoeging: ValueType<string>;
  vestiging_postcode: ValueType<string>;
  vestiging_woonplaats: ValueType<string>;
  vestiging_adres_buitenland1: ValueType<string>;
  vestiging_adres_buitenland2: ValueType<string>;
  vestiging_adres_buitenland3: ValueType<string>;
  correspondentie_landcode: ValueType<string>;
  correspondentie_straatnaam: ValueType<string>;
  correspondentie_huisnummer: ValueType<string>;
  correspondentie_huisletter: ValueType<string>;
  correspondentie_huisnummertoevoeging: ValueType<string>;
  correspondentie_postcode: ValueType<string>;
  correspondentie_woonplaats: ValueType<string>;
  correspondentie_adres_buitenland1: ValueType<string>;
  correspondentie_adres_buitenland2: ValueType<string>;
  correspondentie_adres_buitenland3: ValueType<string>;
};

export type ValuesType = PersonValuesType | OrganizationValuesType;

export type FieldsType = {
  hidden: boolean;
  name: string;
  value: ValueType<any>;
}[];

export type GetValuesType = (fields: FieldsType) => any;

export type FormatBooleanType = (value: boolean) => 1 | 0;

export type GetPersonFormDefinitionType = (
  t: i18next.TFunction,
  countryCode: string,
  countryCodes: CountryCodeType[]
) => AnyFormDefinitionField[];

export type GetPersonRulesType = (countryCode: string) => any;

export type GetOrganizationFormDefinitionType = (
  t: i18next.TFunction,
  countryCode: string,
  countryCodes: CountryCodeType[],
  legalEntityTypes: LegalEntityTypeType[]
) => AnyFormDefinitionField[];

export type GetOrganizationRulesType = (countryCode: string) => any;

type ErrorMessageType = string;

export type PerformCreateContactType = (
  t: i18next.TFunction,
  type: ContactTypeType,
  fields: FieldsType
) => Promise<{
  uuid?: string;
  errors?: ErrorMessageType[];
}>;

export type CreateContactResponseBodyType = {
  json: {
    success?: null | 1;
    succes?: null | 1;
    uuid: string;
    msgs: {
      [key: string]: string;
    };
  };
};

export type GetErrorMessagesType = (
  t: i18next.TFunction,
  type: ContactTypeType,
  response: CreateContactResponseBodyType
) => ErrorMessageType[];

export type CreateContactType = (
  params: any
) => Promise<CreateContactResponseBodyType>;

// PROPS

export type SnackType = {
  link: string;
} | null;
export type SetSnackType = (contact: SnackType) => void;

export type ContactCreateDialogPropsType = {
  open: boolean;
  onClose: () => void;
  type: ContactTypeType;
  countryCodes: CountryCodeType[];
  legalEntityTypes?: LegalEntityTypeType[];
  setSnack: SetSnackType;
  openErrorDialog: any;
};

export type ContactCreateDialogModulePropsType = Pick<
  ContactCreateDialogPropsType,
  'open' | 'onClose' | 'type'
>;
