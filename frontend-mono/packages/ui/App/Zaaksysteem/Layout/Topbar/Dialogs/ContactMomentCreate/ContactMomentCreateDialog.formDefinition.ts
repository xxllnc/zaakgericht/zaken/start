// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';

export type ContactMomentCreateFormValuesType = {
  contact: ValueType<string>;
  contactChannel: ValueType<string>;
  direction: ValueType<'incoming' | 'outgoing'>;
  case: ValueType<string>;
  content: ValueType<string>;
};

export const getContactMomentCreateFormDefinition: ({
  t,
  prefillContactOption,
}: {
  t: i18next.TFunction;
  prefillContactOption?: ContactType | null;
}) => FormDefinition<ContactMomentCreateFormValuesType> = ({
  t,
  prefillContactOption,
}) => {
  return [
    {
      name: 'contact',
      type: fieldTypes.CONTACT_FINDER_WITH_ADVANCED_SEARCH,
      label: t('contact.label'),
      placeholder: t('contact.placeholder'),
      value: prefillContactOption || null,
      required: true,
    },
    {
      name: 'contactChannel',
      label: t('contactChannel.label'),
      placeholder: t('contactChannel.placeholder'),
      type: fieldTypes.SELECT,
      value: { value: 'assignee', label: t('contactChannel.choices.assignee') },
      required: true,
      choices: [
        'assignee',
        'frontdesk',
        'phone',
        'mail',
        'email',
        'webform',
        'social_media',
      ].map(choice => ({
        value: choice,
        label: t(`contactChannel.choices.${choice}`),
      })),
    },
    {
      name: 'direction',
      label: t('direction.label'),
      type: fieldTypes.RADIO_GROUP,
      value: null,
      required: true,
      choices: [
        {
          label: t('direction.choices.incoming'),
          value: 'incoming',
        },
        {
          label: t('direction.choices.outgoing'),
          value: 'outgoing',
        },
      ],
    },
    {
      name: 'case',
      label: t('case.label'),
      type: fieldTypes.CASE_FINDER,
      value: null,
      required: false,
    },
    {
      name: 'content',
      label: t('content.label'),
      type: fieldTypes.TEXTAREA,
      value: null,
      required: true,
    },
  ];
};
