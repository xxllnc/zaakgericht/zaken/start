// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as unique } from 'uuid';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { ContactMomentCreateDialogInnerPropsType } from './ContactMomentCreateDialog.types';
import { getContactMomentCreateFormDefinition } from './ContactMomentCreateDialog.formDefinition';
import { createContactMoment } from './ContactMomentCreateDialog.library';

/**
 * This dialog can be opened in two configurations:
 *
 * 1) Without the 'contact' prop. In this case the user must search and find, or import the contact.
 *
 * 2) With the 'contact' prop. The contactdetails are fetched from the backend and the contact option
 * is prefilled as the value.
 *
 */
export const ContactMomentCreateDialogForm: React.ComponentType<
  ContactMomentCreateDialogInnerPropsType
> = ({ contactOption, open, onClose }) => {
  const [t] = useTranslation('contactMomentCreate');

  const dialogEl = useRef(null);

  let {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition: getContactMomentCreateFormDefinition({
      t,
      prefillContactOption: contactOption,
    }),
  });

  const handleSubmit = () => {
    createContactMoment(values as any)
      .then(onClose)
      .catch(openServerError);
  };

  return (
    <Dialog
      aria-label={t('title')}
      open={open}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        id={unique()}
        title={t('title')}
        onCloseClick={onClose}
        scope={'contact-moment-create-dialog'}
      />

      <DialogContent>
        {fields.map(({ FieldComponent, key, type, suppressLabel, ...rest }) => {
          const props = cloneWithout(rest, 'definition', 'mode');

          return (
            <FormControlWrapper
              {...props}
              label={suppressLabel ? false : props.label}
              compact={true}
              key={`${props.name}-formcontrol-wrapper`}
            >
              <FieldComponent
                {...props}
                t={t}
                containerRef={dialogEl.current}
              />
            </FormControlWrapper>
          );
        })}
      </DialogContent>
      <Fragment>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                disabled: !isValid,
                text: t('common:forms.add'),
                onClick() {
                  handleSubmit();
                  onClose();
                },
              },
              {
                text: t('common:forms.cancel'),
                onClick: onClose,
              },
            ],
            'contact-moment-create-dialog'
          )}
        </DialogActions>
      </Fragment>
    </Dialog>
  );
};
