// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { performToggleKccUserStatus } from '../../../Kcc/Kcc.library';
import { KccContext } from '../../../Layout';
import { useButtonBarstyles } from './ButtonBar.styles';

export type InfoPropsType = {
  open: number;
  setOpen: (open: number) => void;
  unreadCount?: number;
};

export const ButtonBar: React.ComponentType<InfoPropsType> = ({
  open,
  setOpen,
  unreadCount,
}) => {
  const [t] = useTranslation('layout');
  const classes = useButtonBarstyles();
  const session = useSession();
  const { uuid } = session.logged_in_user;
  const { hasKccIntegration, kccUserStatus, setKccUserStatus } =
    useContext(KccContext);

  return (
    <div className={classes.buttonBar}>
      <IconButton name="notifications" onClick={() => setOpen(2 / open)}>
        <Counter count={unreadCount} className={classes.counter}>
          <Tooltip
            title={t('profile.buttons.notifications')}
            placement="bottom"
          >
            <Icon>{iconNames.notifications}</Icon>
          </Tooltip>
        </Counter>
      </IconButton>
      <IconButton
        name="cases"
        onClick={() => {
          window.location.href = `/main/contact-view/employee/${uuid}/cases`;
        }}
      >
        <Tooltip title={t('profile.buttons.cases')} placement="bottom">
          <Icon>{iconNames.list}</Icon>
        </Tooltip>
      </IconButton>
      <IconButton
        name="settings"
        onClick={() => {
          window.location.href = `/main/contact-view/employee/${uuid}`;
        }}
      >
        <Tooltip title={t('profile.buttons.settings')} placement="bottom">
          <Icon>{iconNames.settings}</Icon>
        </Tooltip>
      </IconButton>
      {hasKccIntegration && (
        <IconButton
          name="kcc"
          onClick={() => {
            performToggleKccUserStatus(kccUserStatus).then(setKccUserStatus);
          }}
        >
          <Tooltip
            title={
              kccUserStatus
                ? t('profile.buttons.phone.deactivate')
                : t('profile.buttons.phone.activate')
            }
            placement="bottom"
          >
            <div
              className={kccUserStatus ? classes.phoneIconActive : undefined}
            >
              <Icon>
                {kccUserStatus ? iconNames.phone_in_talk : iconNames.phone}
              </Icon>
            </div>
          </Tooltip>
        </IconButton>
      )}
    </div>
  );
};

export default ButtonBar;
