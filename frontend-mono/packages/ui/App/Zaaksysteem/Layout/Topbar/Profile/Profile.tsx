// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { Backdrop } from '@mui/material';
import Typography from '@mui/material/Typography';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import { useUserStyles } from './Profile.styles';
import Info from './Info/Info';
import ProfileIcon from './ProfileIcon/ProfileIcon';
import ButtonBar from './ButtonBar/ButtonBar';
import Notifications, {
  NotificationsPropsType,
} from './Notifications/Notifications';

export type ProfilePropsType = Pick<
  NotificationsPropsType,
  'notifications' | 'setNotifications'
>;

export const Profile: React.ComponentType<ProfilePropsType> = ({
  notifications,
  setNotifications,
}) => {
  const [t] = useTranslation('layout');
  const classes = useUserStyles();
  const [open, setOpen] = useState<number>(0);

  const unreadCount = notifications?.filter(item => !item.isRead).length;

  useEffect(() => {
    const handleKeyDown = (event: any) => {
      if (event.key === 'Escape') {
        setOpen(open - 1);
      }
    };

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [open]);

  return (
    <div
      className={classNames(
        classes.wrapper,
        open ? classes.open : classes.close
      )}
    >
      <Counter count={unreadCount} className={classes.counter}>
        <ProfileIcon open={open} setOpen={setOpen} />
      </Counter>
      <Backdrop
        sx={{ color: '#fff', zIndex: theme => theme.zIndex.drawer + 1 }}
        open={open >= 1}
        onClick={event => {
          // @ts-ignore
          if (event.target.id === 'backdrop') {
            setOpen(open - 1);
          }
        }}
        id="backdrop"
      >
        <div id="profile" className={classes.panelWrapper}>
          <div className={classes.panelHeader}>
            <Typography
              variant="h3"
              color="inherit"
              classes={{ root: classes.title }}
            >
              {t('profile.profile')}
            </Typography>
            <ProfileIcon open={open} setOpen={setOpen} />
          </div>
          <div className={classes.panelContent}>
            <ButtonBar
              open={open}
              setOpen={setOpen}
              unreadCount={unreadCount}
            />
            <Info open={open} />
          </div>
        </div>
        {notifications && (
          <Notifications
            open={open === 2}
            notifications={notifications}
            setNotifications={setNotifications}
          />
        )}
      </Backdrop>
    </div>
  );
};

export default Profile;
