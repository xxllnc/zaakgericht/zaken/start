// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQueryClient, useQuery } from '@tanstack/react-query';
import Profile from './Profile';
import { getNotifications } from './Profile.library';
import { SetNotificationsType } from './Profile.types';

// 15 minutes
const staleTime = 15 * 60 * 1000;

const ProfileModule: React.ComponentType = () => {
  const queryClient = useQueryClient();
  // It might be ok not to implement ServerErrorDialog here
  const { data: notifications } = useQuery(
    ['notifications'],
    () => getNotifications(),
    { refetchOnWindowFocus: true, staleTime }
  );

  const setNotifications: SetNotificationsType = notifications => {
    queryClient.setQueriesData(['notifications'], notifications);
  };

  return (
    <Profile
      notifications={notifications}
      setNotifications={setNotifications}
    />
  );
};

export default ProfileModule;
