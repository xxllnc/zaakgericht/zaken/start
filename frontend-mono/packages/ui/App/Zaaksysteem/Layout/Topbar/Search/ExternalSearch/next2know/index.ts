// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { OverwriteType } from '../externalSearch.types';
import { getIntegration, performSearch } from './next2know.library';

const ONE_HOUR = 60 * 60 * 1000;
const errorMessage = 'Something went wrong fetching the next2know integration';

export const useNext2Know = (overwrites: OverwriteType) => {
  const { data: integration } = useQuery(
    ['next2know'],
    () => getIntegration(),
    {
      // silently fail
      onError: () => console.log(errorMessage),
      staleTime: ONE_HOUR,
    }
  );

  if (!integration) return integration;

  return {
    integration: { ...integration, ...overwrites },
    performSearch,
  };
};
