// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchIntegrationsType,
  IntegrationsResponseBodyType,
  ExternalResultParamsType,
  SearchType,
} from './next2know.types';

export const fetchIntegrations: FetchIntegrationsType = async () => {
  const url = '/api/v1/sysin/interface/get_by_module_name/next2know';

  const response = await request<IntegrationsResponseBodyType>('GET', url);

  return response;
};

export const search: SearchType = async (endpoint, params, headers) => {
  const url = buildUrl<ExternalResultParamsType>(
    `${endpoint}/documents/_all/search`,
    params
  );

  const response = await request('GET', url, undefined, undefined, headers);

  return response;
};
