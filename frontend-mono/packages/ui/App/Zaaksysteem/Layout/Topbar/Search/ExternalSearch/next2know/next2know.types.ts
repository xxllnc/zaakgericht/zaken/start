// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ForceKeywordType, ResultType } from '../../Search.types';
import { SetExternalSearchOverwriteType } from '../externalSearch.types';

// INTEGRATION

type TokenType = {
  access_token: string;
  token_type: string;
};

type IntegrationType = {
  instance: {
    interface_config: {
      endpoint: string;
      result_grouping: GroupingType;
      token: TokenType;
    };
  };
};

export type IntegrationsResponseBodyType = {
  result: {
    instance: {
      rows: IntegrationType[];
    };
  };
};

type GroupingType = 'none' | 'dossier_name';

export type Next2KnowType = {
  endpoint: string;
  grouping: GroupingType;
  token: TokenType;
};

export type FetchIntegrationsType = () => Promise<IntegrationsResponseBodyType>;

export type GetIntegrationType = () => Promise<Next2KnowType | null>;

export type FormatIntegrationType = (
  integration: IntegrationType
) => Next2KnowType;

// SEARCH / RESULTS

export type ExternalResultParamsType = {
  group_by?: string;
  q: string;
  size: number;
};

export type SingleResultType = {
  _id: string;
  _index: string;
  _source: {
    Naam: string;
    Omschrijving: string;
    Externe_identificatiekenmerken_nummer_binnen_systeem: string;
  };
};

export type GroupedResultType = {
  doc_count: number;
  key: string;
};

type HitType = SingleResultType | GroupedResultType;

export type ExternalResultResponseBodyType = {
  data: {
    hits: HitType[];
  };
};

export type HeadersType = {
  Authorization: string;
};

export type CreateHeadersType = (token: TokenType) => HeadersType;

export type SearchType = (
  endpoint: string,
  params: ExternalResultParamsType,
  headers: HeadersType
) => Promise<ExternalResultResponseBodyType>;

export type PerformSearchType = (
  t: i18next.TFunction,
  keyword: string,
  settings: Next2KnowType,
  forceKeyword: ForceKeywordType,
  setExternalSearchOverwrite: SetExternalSearchOverwriteType
) => Promise<ResultType[]>;

export type FormatSingleResultType = (hit: SingleResultType) => ResultType;
export type FormatGroupedResultType = (
  t: i18next.TFunction,
  hit: GroupedResultType,
  index: number,
  forceKeyword: ForceKeywordType,
  setExternalSearchOverwrite: SetExternalSearchOverwriteType
) => ResultType;

export type FormatResultsType = (
  t: i18next.TFunction,
  hits: HitType[],
  grouping: GroupingType,
  forceKeyword: ForceKeywordType,
  setExternalSearchOverwrite: SetExternalSearchOverwriteType
) => ResultType[];
