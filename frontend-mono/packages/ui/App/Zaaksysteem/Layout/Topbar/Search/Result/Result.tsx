// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { IconButton } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { ResultType } from '../Search.types';
import { useActivateContact } from '../../../Kcc/Kcc.library';
import { useResultStyles } from './Result.styles';

type ResultPropsType = {
  result: ResultType;
  setInactive: () => void;
};

/* eslint complexity: [2, 10] */
const Result: React.ComponentType<ResultPropsType> = ({
  result: { uuid, type, title, description, url, action, caseUuid },
  setInactive,
}) => {
  const [t] = useTranslation('search');
  const classes = useResultStyles();
  const navigate = useNavigate();

  const { activateContact, refreshPhoneCalls } = useActivateContact();

  return (
    <Link
      className={classes.wrapper}
      to={url || 'no_url'}
      onClick={(event: any) => {
        if (url?.includes('/redirect')) {
          window.location.href = url;
        }

        if (url?.includes('=doc2') && caseUuid) {
          event.preventDefault();
          fetch('/api/v2/cm/case/get_case?case_uuid=' + caseUuid)
            .then(res => res.json())
            .then(data =>
              navigate(
                `/main/intern?caseId=${data.data.attributes.number}&location=doc2`
              )
            );
        }

        if (action) {
          event.preventDefault();

          action();
        } else {
          setInactive();
        }
      }}
    >
      <div>
        {type === 'external' ? (
          <Icon size="small">{iconNames.lan}</Icon>
        ) : (
          <ZsIcon size="extraSmall">{`entityType.inverted.${type}`}</ZsIcon>
        )}
      </div>
      <div className={classes.content}>
        <span>{title}</span>
        {description && type !== 'document' && (
          <span className={classes.description}>{description}</span>
        )}
      </div>
      <div className={classes.buttonWrapper}>
        {type === 'organization' && (
          <div>
            <IconButton
              title={t('common:verbs.activate')}
              onClick={event => {
                event.stopPropagation();
                event.preventDefault();
                activateContact(uuid).then(() => {
                  setInactive();
                  refreshPhoneCalls();
                });
              }}
            >
              <Icon size="extraSmall" color="primary">
                {iconNames.play_circle_filled}
              </Icon>
            </IconButton>
          </div>
        )}
        {url && (
          <div>
            <IconButton
              title={t('common:general.openInNewWindow')}
              href={url}
              onClick={event => {
                event.stopPropagation();
                event.preventDefault();
                window.open(url, '_blank');
              }}
            >
              <Icon size="extraSmall" color="primary">
                {iconNames.open_in_new}
              </Icon>
            </IconButton>
          </div>
        )}
      </div>
    </Link>
  );
};

export default Result;
