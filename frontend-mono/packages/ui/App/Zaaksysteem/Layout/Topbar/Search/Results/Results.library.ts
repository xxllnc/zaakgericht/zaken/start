// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { GroupedResultsType, GroupResultsType } from '../Search.types';

export const groupResults: GroupResultsType = results =>
  results.reduce((acc, result) => {
    const type = result.type;
    const firstOfItsType = !Object.prototype.hasOwnProperty.call(acc, type);

    if (firstOfItsType) {
      return { ...acc, [type]: [result] };
    } else {
      const accumulatedResults = acc[type];

      return { ...acc, [type]: [...(accumulatedResults || []), result] };
    }
  }, {} as GroupedResultsType);
