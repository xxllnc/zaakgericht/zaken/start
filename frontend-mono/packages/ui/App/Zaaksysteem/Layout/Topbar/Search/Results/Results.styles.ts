// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useResultsStyles = makeStyles(() => ({
  wrapper: { display: 'flex', flexDirection: 'column', gap: 20 },
  title: {
    padding: 20,
  },
  results: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    gap: 20,
  },
}));
