// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import { catalogTypes, resultTypes } from './../Search.library';
import Result from './../Result/Result';
import { ResultType } from './../Search.types';
import { groupResults } from './Results.library';
import { useResultsStyles } from './Results.styles';

type ResultsPropsType = {
  results: ResultType[];
  setInactive: () => void;
};

/* eslint complexity: [2, 7] */
const Results: React.ComponentType<ResultsPropsType> = ({
  results,
  setInactive,
}) => {
  const classes = useResultsStyles();
  const [t] = useTranslation('search');

  const groupedResults = groupResults(results);

  return (
    <div className={classes.wrapper}>
      {[...resultTypes, ...catalogTypes].map(type => {
        // not mapping over the keys of groupedResults,
        // because this way the order of the groups can be forced

        // @ts-ignore
        const resultGroup: ResultType[] = groupedResults[type];

        if (!resultGroup) {
          return null;
        } else {
          return (
            <div key={type}>
              <div className={classes.title}>
                <Typography variant="h3">{t(`filters.${type}`)}</Typography>
              </div>
              <div className={classes.results}>
                {resultGroup.map(result => (
                  <Result
                    key={result.uuid}
                    result={result}
                    setInactive={setInactive}
                  />
                ))}
              </div>
            </div>
          );
        }
      })}
    </div>
  );
};

export default Results;
