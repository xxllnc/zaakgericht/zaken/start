// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { performExternalSearches } from './ExternalSearch/externalSearch.library';
import { search } from './Search.request';
import {
  FocusOnSearchType,
  FilterType,
  GetFilterChoicesType,
  GetUrlType,
  ConvertFilterToTypeType,
  FormatResultType,
  ResultTypeType,
  useSearchType,
  GetBuiltInSavedSearchesType,
  ResultType,
  ParamFilterTypeType,
} from './Search.types';

export const GENERIC_SEARCH = 'generic-string';

export const focusOnSearch: FocusOnSearchType = () => {
  setTimeout(() => {
    document.getElementsByName(GENERIC_SEARCH)[0].focus();
  });
};

// note: These lists determine the order of the filters and the results
export const catalogTypes = ['case_type', 'custom_object_type'];
const filters: FilterType[] = [
  'all',
  'case',
  'organization',
  'employee',
  'document',
  'saved_search',
  'custom_object',
  'object_v1',
];
const filterTypes: ParamFilterTypeType[] = [
  'case',
  'organization',
  'employee',
  'document',
  'saved_search',
  'custom_object',
  'object_v1',
];
export const resultTypes: ResultTypeType[] = [...filterTypes, 'external'];

export const getFilterChoices: GetFilterChoicesType = (
  t,
  hasCatalogPrivileges
) => {
  const supportedFilters = [
    ...filters,
    ...(hasCatalogPrivileges ? (catalogTypes as FilterType[]) : []),
  ];

  return supportedFilters.map(filter => ({
    value: filter,
    label: t(`filters.${filter}`),
  }));
};

/* eslint complexity: [2,11] */
const getUrl: GetUrlType = (uuid, type, caseUuid) => {
  switch (type) {
    case 'case': {
      return `/redirect/case?uuid=${uuid}`;
    }
    case 'organization':
    case 'employee': {
      return `/main/contact-view/${type}/${uuid}`;
    }
    case 'document': {
      return `/redirect/case?uuid=${caseUuid}&tab=document`;
    }
    case 'saved_search': {
      return `/search/${uuid}`;
    }
    case 'custom_object': {
      return `/main/object/${uuid}`;
    }
    case 'object_v1': {
      return `/object/${uuid}`;
    }
    case 'case_type': {
      return `/main/case-type/${uuid}/bewerken`;
    }
    case 'custom_object_type': {
      return `/main/object-type/update/${uuid}`;
    }
    default: {
      return '';
    }
  }
};

const formatResult: FormatResultType = ({
  id: uuid,
  meta,
  attributes: { result_type, description },
  relationships,
}) => {
  const type = result_type as ResultTypeType;
  const title = meta?.summary as string;
  const caseUuid =
    type === 'document' ? relationships?.case?.data.id : undefined;
  const url = getUrl(uuid, type, caseUuid);

  return { uuid, type, title, description, url, caseUuid };
};

const defaultSearches = [
  {
    uuid: 'all',
    type: 'saved_search',
    icon: 'saved_search',
    url: '/search/all',
  },
  {
    uuid: 'mine',
    type: 'saved_search',
    icon: 'saved_search',
    url: '/search/mine',
  },
  {
    uuid: 'myDepartment',
    type: 'saved_search',
    icon: 'saved_search',
    url: '/search/my-department',
  },
];

export const getBuiltInSavedSearches: GetBuiltInSavedSearchesType = (
  t,
  filter,
  keyword
) =>
  filter === 'all' || filter === 'saved_search'
    ? (defaultSearches
        .map(defaultSearch => ({
          ...defaultSearch,
          title: t(`savedSearch.${defaultSearch.uuid}`),
        }))
        .filter(defaultSearch => {
          const title = defaultSearch.title.toLowerCase();
          const search = keyword.toLowerCase();

          return title.includes(search);
        }) as ResultType[])
    : [];

const convertFilterToType: ConvertFilterToTypeType = (
  filter,
  hasCatalogPrivileges
) => {
  if (filter === 'all') {
    const supportedResultTypes = [
      ...filterTypes,
      ...(hasCatalogPrivileges ? (catalogTypes as ParamFilterTypeType[]) : []),
    ];

    return supportedResultTypes;
  } else {
    return [filter];
  }
};

export const useSearch: useSearchType = (
  t,
  keyword,
  filter,
  hasCatalogPrivileges,
  externalSearches,
  forceKeyword,
  setExternalSearchOverwrite
) => {
  const type = convertFilterToType(filter, hasCatalogPrivileges);

  const params = {
    keyword,
    type,
    max_results_per_type: 20,
  };

  const { data: internalResponse, isFetching: internalLoading } = useQuery(
    ['searchResultsInternal', keyword, filter],
    () => (keyword ? search(params) : null),
    { onError: openServerError }
  );
  const internalResults = internalResponse?.data.map(formatResult) || [];

  const { data: externalResults, isFetching: externalLoading } = useQuery(
    ['searchResultsExternal', keyword, filter],
    () =>
      keyword
        ? performExternalSearches(
            t,
            keyword,
            externalSearches,
            forceKeyword,
            setExternalSearchOverwrite
          )
        : null,
    { staleTime: 0 }
  );

  const results = [...internalResults, ...(externalResults || [])];
  const loading = internalLoading || externalLoading;

  return { results, loading };
};
