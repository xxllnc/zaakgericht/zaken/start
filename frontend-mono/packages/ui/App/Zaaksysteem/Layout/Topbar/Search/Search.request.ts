// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  SearchType,
  SearchRequestParamsType,
  SearchResponseBodyType,
} from './Search.types';

export const search: SearchType = async params => {
  const url = buildUrl<SearchRequestParamsType>('/api/v2/cm/search', params);

  const response = await request<SearchResponseBodyType>('GET', url);

  return response;
};
