// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSearchStyles = makeStyles(
  ({ zIndex, palette: { common }, mintlab: { greyscale } }: Theme) => ({
    placeholder: {
      width: 300,
      minWidth: 250,
    },
    wrapper: {
      position: 'absolute',
      top: 0,
      left: 0,
      height: '100vh',
      width: '100%',
      backgroundColor: common.white,
      zIndex: zIndex.modal,
    },
    sheet: {
      display: 'flex',
      flexDirection: 'column',
    },
    header: {
      display: 'flex',
      alignItems: 'center',
      margin: 'auto',
      width: '100%',
      padding: '0 20px',
      minHeight: 70,
      height: 70,
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    searchWrapper: {
      position: 'relative',
      margin: 'auto',
      width: 1000,
      paddingLeft: 20,
      display: 'flex',
      gap: 20,
    },
    search: {
      flexGrow: 1,
      minWidth: 250,
    },
    filter: {
      width: 180,
    },
    content: {
      flexGrow: 1,
      padding: 20,
      position: 'relative',
      overflowY: 'auto',
      scrollbarGutter: 'stable',
    },
    loading: {
      opacity: 0.5,
    },
    loader: {
      position: 'absolute',
      top: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    noResults: {
      margin: 'auto',
    },
    resultsWrapper: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      gap: 20,
    },
  })
);
