// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import classNames from 'classnames';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import useSession, {
  hasCapability,
} from '@zaaksysteem/common/src/hooks/useSession';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useSearchStyles } from './Search.styles';
import {
  GENERIC_SEARCH,
  focusOnSearch,
  getFilterChoices,
  useSearch,
} from './Search.library';
import { PreferenceType, FilterType, ForceKeywordType } from './Search.types';
import Results from './Results/Results';
import FilterPreference from './FilterPreference/FilterPreference';
import {
  ExternalSearchType,
  SetExternalSearchOverwriteType,
} from './ExternalSearch/externalSearch.types';

type SearchPropsType = {
  filter: FilterType;
  preference: PreferenceType;
  setPreference: (preference: PreferenceType) => void;
  setFilter: (filter: FilterType) => void;
  setInactive: () => void;
  externalSearches: ExternalSearchType[];
  resetExternalSearchOverwrites: () => void;
  setExternalSearchOverwrite: SetExternalSearchOverwriteType;
};

/* eslint complexity: [2, 12] */
const Search: React.ComponentType<SearchPropsType> = ({
  filter,
  preference,
  setPreference,
  setFilter,
  setInactive,
  externalSearches,
  resetExternalSearchOverwrites,
  setExternalSearchOverwrite,
}) => {
  const classes = useSearchStyles();
  const [t] = useTranslation('search');
  const session = useSession();
  const hasCatalogPrivileges = hasCapability(session, 'beheer_zaaktype_admin');

  const [inputValue, setInputValue] = useState<string>('');
  const [keyword, setKeyword] = useState<string>('');

  const forceKeyword: ForceKeywordType = value => {
    setInputValue(value);
    setKeyword(value);
  };

  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= 3) setKeyword(value);
  }, 500);

  React.useEffect(() => {
    const keyDownListener = (event: any) => {
      if (event.key === 'Escape') {
        setInactive();
      }
    };

    document.addEventListener('keydown', keyDownListener);

    return () => document.removeEventListener('keydown', keyDownListener);
  }, []);

  const { results, loading } = useSearch(
    t,
    keyword,
    filter,
    hasCatalogPrivileges,
    externalSearches,
    forceKeyword,
    setExternalSearchOverwrite
  );

  const placeholder = t('searchIn', { systemName: t('common:systemName') });

  return (
    <div className={classes.wrapper}>
      <Sheet classes={{ sheet: classes.sheet }}>
        <div className={classes.header}>
          <IconButton title={t('common:verbs.close')} onClick={setInactive}>
            <Icon size="small">{iconNames.arrow_back}</Icon>
          </IconButton>
          <div className={classes.searchWrapper}>
            <div className={classes.search}>
              <Select
                name={GENERIC_SEARCH}
                variant="generic"
                value={inputValue}
                isClearable={Boolean(inputValue.length)}
                isMulti={false}
                placeholder={placeholder}
                onInputChange={(ev, value) => {
                  setInputValue(value);
                  debouncedCallback(value);

                  if (!value) {
                    resetExternalSearchOverwrites();
                  }
                }}
                freeSolo={true}
                onClose={event => {
                  if (event.key === 'Escape') {
                    setInactive();
                  }
                }}
                startAdornment={
                  <Icon style={{ marginRight: '10px' }} size="small">
                    {iconNames.search}
                  </Icon>
                }
              />
            </div>
            <div className={classes.filter}>
              <Select
                name="generic-filter"
                variant="generic"
                choices={getFilterChoices(t, hasCatalogPrivileges)}
                value={filter}
                isClearable={false}
                isMulti={false}
                placeholder={placeholder}
                onChange={event => {
                  setFilter(event.target.value.value);
                  if (!inputValue) {
                    focusOnSearch();
                  }
                }}
              />
            </div>
            <FilterPreference
              preference={preference}
              setPreference={setPreference}
              filter={filter}
            />
          </div>
        </div>
        <div
          className={classNames(classes.content, {
            [classes.loading]: loading,
          })}
        >
          {results && !results.length && inputValue && (
            <div className={classes.noResults}>{t('content.noResults')}</div>
          )}
          {results && Boolean(results.length) && (
            <Results results={results} setInactive={setInactive} />
          )}
          {loading && (
            <div className={classes.loader}>
              <Loader />
            </div>
          )}
        </div>
      </Sheet>
    </div>
  );
};

export default Search;
