// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import HelpWrapper from '@mintlab/ui/App/Zaaksysteem/HelpDialog';
import Search from './Search';
import Placeholder from './SearchPlaceholder';
import locale from './Search.locale';
import { getFilterPreference } from './FilterPreference/FilterPreference.library';
import { focusOnSearch } from './Search.library';
import { PreferenceType, FilterType } from './Search.types';
import { useNext2Know } from './ExternalSearch/next2know';
import {
  ExternalSearchOverwritesType,
  ExternalSearchType,
  SetExternalSearchOverwriteType,
} from './ExternalSearch/externalSearch.types';

const SearchModule = () => {
  const [externalSearchOverwrites, setExternalSearchOverwrites] =
    useState<ExternalSearchOverwritesType>();
  const resetExternalSearchOverwrites = () =>
    setExternalSearchOverwrites(undefined);
  const setExternalSearchOverwrite: SetExternalSearchOverwriteType = (
    name,
    overwrites
  ) => {
    setExternalSearchOverwrites({
      ...externalSearchOverwrites,
      [name]: overwrites,
    });
  };

  const next2know = useNext2Know(externalSearchOverwrites?.next2know);
  const externalSearches = [next2know].filter(Boolean) as ExternalSearchType[];

  const filterPreference = getFilterPreference();
  const [preference, setPreference] =
    useState<PreferenceType>(filterPreference);
  const [filter, setFilter] = useState<FilterType | null>(null);

  const setActive = () => {
    setFilter(preference || 'case');
    focusOnSearch();
  };

  const setInactive = () => {
    setFilter(null);
    resetExternalSearchOverwrites();
  };

  return filter ? (
    <Search
      setInactive={setInactive}
      preference={preference}
      setPreference={setPreference}
      filter={filter}
      setFilter={setFilter}
      externalSearches={externalSearches}
      resetExternalSearchOverwrites={resetExternalSearchOverwrites}
      setExternalSearchOverwrite={setExternalSearchOverwrite}
    />
  ) : (
    <HelpWrapper id="quickSearch">
      <Placeholder setActive={setActive} />
    </HelpWrapper>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="search">
    <SearchModule />
  </I18nResourceBundle>
);
