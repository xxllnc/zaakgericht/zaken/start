// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const contactSelector = (state: any) => {
  const parts = state.router?.location.pathname.split('/');
  if (parts?.some((part: string) => part === 'contact-view')) {
    return {
      uuid: parts.filter((part: string) => part.length === 36)[0],
      type: parts.filter((part: string) =>
        ['employee', 'organization', 'person'].includes(part)
      )[0],
    };
  }
  return null;
};
