// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTopBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      gap: 20,
      padding: '0px 20px',
      borderBottom: `1px solid ${greyscale.darker}`,
      height: 70,
    },
    portalWrapper: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      columnGap: 20,
      overflow: 'hidden',
      minWidth: 250,
    },
    search: {
      display: 'flex',
      width: 550,
    },
  })
);
