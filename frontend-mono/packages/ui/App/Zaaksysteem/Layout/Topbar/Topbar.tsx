// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import useSession, {
  hasCapability,
} from '@zaaksysteem/common/src/hooks/useSession';
import Button from '@mintlab/ui/App/Material/Button';
import AddElement from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement';
import CaseCreateDialog from './Dialogs/CaseCreate/CaseCreateDialog';
import ContactMomentCreateDialog from './Dialogs/ContactMomentCreate/ContactMomentCreateDialog';
import ContactCreateDialogModule from './Dialogs/ContactCreate';
import { useTopBarStyles } from './Topbar.styles';
import Search from './Search';
import { getSections, getContactFromUrl } from './Topbar.library';
import Profile from './Profile';
import { DialogTypeType } from './Topbar.types';

export type TopbarPropsType = {
  topbarPortalRef: React.MutableRefObject<null> | null;
};

export const Topbar: React.ComponentType<TopbarPropsType> = ({
  topbarPortalRef,
}) => {
  const session = useSession();
  const [t] = useTranslation('layout');
  const classes = useTopBarStyles();
  const [dialogOpen, setDialogOpen] = useState<DialogTypeType>(null);
  const closeDialog = () => setDialogOpen(null);
  const contact = getContactFromUrl();

  const addElements = getSections({
    t,
    contactAllowed: hasCapability(session, 'contact_nieuw'),
    setDialogOpen,
  });

  return (
    <header className={classes.wrapper}>
      <div className={classes.portalWrapper} ref={topbarPortalRef} />
      <Search />
      <Button name="topBarAdd" icon="add" action={() => setDialogOpen('add')}>
        {t('new')}
      </Button>
      {dialogOpen === 'add' ? (
        <AddElement
          t={t}
          hide={closeDialog}
          sections={addElements}
          title={t('new')}
        />
      ) : null}

      <Profile />

      <CaseCreateDialog
        open={dialogOpen === 'case'}
        onClose={closeDialog}
        {...(contact && { contact })}
      />

      <ContactMomentCreateDialog
        open={dialogOpen === 'contactMoment'}
        onClose={closeDialog}
        {...(contact && { contact })}
      />

      <ContactCreateDialogModule
        open={dialogOpen === 'person'}
        onClose={closeDialog}
        type="person"
      />

      <ContactCreateDialogModule
        open={dialogOpen === 'organization'}
        onClose={closeDialog}
        type="organization"
      />
    </header>
  );
};

export default Topbar;
