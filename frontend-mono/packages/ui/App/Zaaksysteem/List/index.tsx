// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Suspense } from 'react';
import { Loader } from '../Loader';
import { SortableListPropsType } from './SortableList';

const LazyList = React.lazy(
  () =>
    import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.sortableeditablelist" */
      './SortableList'
    )
);

export { useListStyle } from './List.style';

export function SortableList<T>(props: SortableListPropsType<T>) {
  return (
    <Suspense fallback={<Loader />}>
      {/* 
        // @ts-ignore */}
      <LazyList {...props} />
    </Suspense>
  ) as React.ReactElement;
}
