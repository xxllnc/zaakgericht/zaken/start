// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { addScopeAttribute } from '../../../library/addScope';
import { iterator } from '../library/iterator';
import { LoaderVariantComponentType } from './LoaderVariantComponentType';
import { useCircleLoaderStyle, LENGTH } from './CircleLoader.style';

export const Circle: LoaderVariantComponentType = ({
  color,
  scope,
  className,
  size = 40,
  style = {},
}) => {
  const classes = useCircleLoaderStyle();

  return (
    <div
      style={{
        height: size,
        width: size,
        margin: `${size / 2}px auto`,
        ...style,
      }}
      className={classNames(classes.circle, className)}
      {...addScopeAttribute(scope)}
    >
      {iterator(LENGTH).map(item => (
        <div key={item} className={classes.child}>
          <span
            className={classes.dot}
            style={{
              backgroundColor: color,
            }}
          />
        </div>
      ))}
    </div>
  );
};
