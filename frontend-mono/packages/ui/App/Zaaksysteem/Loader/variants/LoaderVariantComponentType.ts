// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';

export type LoaderVariantComponentType = React.ComponentType<{
  color: string;
  scope: string;
  className?: string;
  size?: number;
  style: Object;
}>;
