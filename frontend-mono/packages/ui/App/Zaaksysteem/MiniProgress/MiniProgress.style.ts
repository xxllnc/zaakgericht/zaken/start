// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useMiniProgressStyles = makeStyles((theme: Theme) => ({
  progressWrapper: {
    display: 'flex',
    alignItems: 'baseline',
    gap: 6,
  },
  progressBarWrapper: {
    flex: 1,
    height: 16,
    borderRadius: 8,
    backgroundColor: theme.palette.elephant.lightest,
  },
  progressBarInner: {
    height: 16,
    backgroundColor: theme.palette.secondary.main,
    borderRadius: 8,
  },
}));
