// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useWidth } from '../../library/useWidth';
import { usePanelLayoutStyles } from './PanelLayout.styles';

type PanelType = {
  className?: string;
  type?: 'side' | 'fluid';
  children?: any;
  folded?: boolean;
};

export const Panel: React.ComponentType<PanelType> = ({
  type = 'fluid',
  children,
  className,
  folded,
}) => {
  const classes = usePanelLayoutStyles();

  const width = useWidth();
  const defaultFolded = ['xs', 'sm'].includes(width);
  const isCompact = folded === undefined ? defaultFolded : folded;

  return (
    <div
      className={classNames(
        classes.panel,
        classes[type],
        className && className,
        { [classes.folded]: isCompact }
      )}
    >
      {children}
    </div>
  );
};
