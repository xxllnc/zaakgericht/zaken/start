// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { usePanelLayoutStyles } from './PanelLayout.styles';

export const PanelLayout = ({ children }: any) => {
  const classes = usePanelLayoutStyles();

  return <div className={classes.wrapper}>{children}</div>;
};
