// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  fetchRelations,
  relateObjectTo,
  search,
  unrelateObjectFrom,
} from './RelationsTable.requests';
import {
  OnRowClickType,
  GetRelationsType,
  FormatRelationsType,
  MapChoiceType,
  PerformSearchType,
  PerformRelateType,
  GetSupportedToAddType,
  SupportedToAddType,
  PerformUnrelateType,
} from './RelationsTable.types';

export const onRowClick: OnRowClickType = (relation, type) => {
  let url = '';

  if (type === 'cases') {
    // @ts-ignore
    url = `/intern/zaak/${relation.nr}`;
  } else if (type === 'objects') {
    url = `/main/object/${relation.uuid}`;
  } else if (type === 'subjects') {
    // @ts-ignore
    url = `/main/contact-view/${relation.subjectType}/${relation.uuid}`;
  }

  window.open(url, '_blank');
};

/* eslint complexity: [2, 10] */
export const formatCaseRelations: FormatRelationsType = relations =>
  relations.map(row => ({
    uuid: row.id || '',
    name: row.id || '',
    nr: row.attributes?.number || 0,
    progress: (row.attributes?.progress || 0) * 100,
    status: row.attributes?.status || '',
    caseType: row.relationships?.case_type?.meta?.summary || '',
    result: row.attributes?.result || '',
    extra: row.meta?.summary || '',
    assignee: row.relationships?.assignee?.meta?.summary || '',
  }));

export const formatObjectRelations: FormatRelationsType = (relations, uuid) =>
  relations.map(
    ({
      id,
      meta,
      attributes: {
        related_to_magic_strings,
        related_from_magic_strings,
        relation_source,
      },
      relationships,
    }) => ({
      uuid: id,
      relationSource: relation_source,
      name: meta?.summary || '',
      objectType: relationships?.object_type?.meta.summary,
      relatedTo: uuid,
      relatedToMagicStrings: related_to_magic_strings,
      relatedFromMagicStrings: related_from_magic_strings,
    })
  );

export const formatSubjectRelations: FormatRelationsType = relations =>
  relations.map(row => ({
    uuid: row.id || '',
    name: row.meta?.summary || '',
    subjectType: row.attributes?.type,
    roleType: row.attributes?.roles,
  }));

const formatters = {
  cases: formatCaseRelations,
  objects: formatObjectRelations,
  subjects: formatSubjectRelations,
};

export const getRelations: GetRelationsType = async (context, uuid, type) => {
  const response = await fetchRelations(context, uuid, type);
  const formatter = formatters[type];
  const relations = formatter(response.data || [], uuid);

  return relations;
};

// not everything is supported by the API yet
const supportedToAdd: SupportedToAddType = {
  case: [
    /* 'cases', 'objects', 'subjects' */
  ],
  custom_object: ['cases', 'objects' /* , 'subjects' */],
  subject: [
    /* 'cases', 'objects' 'subjects' */
  ],
};

export const getSupportedToAdd: GetSupportedToAddType = (context, type) =>
  supportedToAdd[context] && supportedToAdd[context].includes(type);

const searchTypes = {
  cases: ['case' as 'case'],
  objects: ['custom_object' as 'custom_object'],
  subjects: [
    'person' as 'person',
    'organization' as 'organization',
    'employee' as 'employee',
  ],
};

const mapChoice: MapChoiceType = result => ({
  value: result.id,
  label: result.meta?.summary || '',
});

export const performSearch: PerformSearchType = async (keyword, type) => {
  if (!keyword || keyword.length < 3) return [];

  const searchType = [...searchTypes[type]];

  const response = await search({ keyword, type: searchType });

  return response.data.map(mapChoice);
};

export const performRelate: PerformRelateType = async (
  context,
  uuid,
  type,
  value
) => {
  if (context === 'case') {
    // await relateCaseTo(data)
  } else if (context === 'custom_object') {
    // this call should be expanded to also support 'objects' and 'subjects'
    if (type === 'cases') {
      await relateObjectTo({ custom_object_uuid: uuid, [type]: [value] });
    } else if (type === 'objects') {
      await relateObjectTo({
        custom_object_uuid: uuid,
        custom_objects: [value],
      });
    }
  }
};

export const performUnrelate: PerformUnrelateType = async (
  context,
  uuid,
  type,
  value
) => {
  if (context === 'case') {
    // await unrelateCaseTo(data)
  } else if (context === 'custom_object') {
    // this call should be expanded to also support 'objects' and 'subjects'
    if (type === 'cases') {
      await unrelateObjectFrom({ custom_object_uuid: uuid, [type]: [value] });
    } else if (type === 'objects') {
      await unrelateObjectFrom({
        custom_object_uuid: uuid,
        custom_objects: [value],
      });
    }
  }
};
