// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './RelationsTable.locale';
import RelationsTable, { RelationsTablePropsType } from './RelationsTable';

const RelationsTableModule: React.FunctionComponent<
  RelationsTablePropsType
> = props => (
  <I18nResourceBundle resource={locale} namespace="relationsTable">
    <RelationsTable {...props} />
  </I18nResourceBundle>
);

export default RelationsTableModule;
