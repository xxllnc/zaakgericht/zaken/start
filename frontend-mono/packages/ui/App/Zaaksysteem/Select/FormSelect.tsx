// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import { Box } from '@mui/material';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import TextField from '../../Material/TextField';
import { BaseSelectPropsType } from './types/BaseSelectPropsType';
import defaultFilterOptions from './library/filterOption';
import { getNormalizedValue } from './library/getNormalizedValue';

const defaultStyling = (isMulti: boolean) => ({
  '& .MuiAutocomplete-inputRoot': {
    height: isMulti ? 'auto' : 40,
    padding: '5px 10px 2px 10px',
  },
  '& .MuiAutocomplete-inputRoot.Mui-disabled': {
    backgroundColor: 'transparent',
    pointerEvents: 'none',
  },
});

const genericStyling = {
  '& .MuiFilledInput-root': {
    paddingTop: '2px',
    paddingBottom: '2px',
    paddingLeft: '20px',
    color: 'rgb(47, 51, 56)',
    fontWeight: 100,
  },
  '&&& .Mui-focused': {
    backgroundColor: 'rgb(225 237 255)',
    '& :hover': {
      backgroundColor: 'rgb(225 237 255)',
    },
  },
};

/* eslint complexity: [2, 10] */
export const FormSelect: React.ComponentType<
  BaseSelectPropsType<any> & {
    nestedValue?: boolean;
    variant?: 'generic' | 'default';
    startAdornment?: React.ReactElement;
  }
> = ({
  disabled,
  error,
  loading,
  isClearable = true,
  isMulti = false,
  value,
  choices,
  onInputChange,
  filterOption,
  name,
  onBlur,
  onChange,
  onOpen,
  onClose,
  onFocus,
  openMenuOnClick = true,
  isOptionDisabled,
  placeholder,
  renderOption,
  renderTags,
  freeSolo,
  startAdornment,
  nestedValue = false,
  sx = {},
  variant = 'default',
  filterSelectedOptions = true,
  groupBy,
  inputRef,
  inputProps = {},
}) => {
  const {
    mintlab: { greyscale },
  } = useTheme<Theme>();
  const [inputValue, setInputValue] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const norm = getNormalizedValue({ value, isMulti, choices });
  const memValue = React.useMemo(
    () => norm,
    [isMulti ? norm.map((norm: any) => norm.label).join('') : norm.label, value]
  );

  const performOpen = () => setOpen(true);
  const performClose = () => setOpen(false);

  return (
    <Autocomplete
      filterSelectedOptions={filterSelectedOptions}
      sx={{
        ...(variant === 'default' ? defaultStyling(isMulti) : genericStyling),
        ...sx,
      }}
      onOpen={onOpen || performOpen}
      onClose={onClose || performClose}
      onFocus={onFocus}
      value={memValue}
      inputValue={inputValue}
      options={choices || []}
      getOptionDisabled={isOptionDisabled}
      disableClearable={!isClearable}
      disabled={disabled}
      loading={loading}
      multiple={isMulti}
      freeSolo={freeSolo}
      openOnFocus={openMenuOnClick}
      renderTags={renderTags}
      onInputChange={(ev, val, reason) => {
        setInputValue(val);
        onInputChange && onInputChange(ev, val, reason, open, memValue);
      }}
      renderOption={
        renderOption ||
        ((props, option) => {
          return (
            <Box
              component="li"
              {...props}
              //@ts-ignore
              key={option.key || option.label + props['data-option-index']}
              sx={{
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              <Box component="span" sx={{ width: '100%' }}>
                {option.label}
              </Box>
              {option.subLabel ? (
                <Tooltip
                  title={option.subLabel}
                  enterDelay={200}
                  placement="left"
                >
                  <Box
                    component="span"
                    sx={{
                      color: greyscale.main,
                      width: '100%',
                      display: '-webkit-box',
                      '-webkit-line-clamp': '3',
                      '-webkit-box-orient': 'vertical',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                    }}
                  >
                    {option.subLabel}
                  </Box>
                </Tooltip>
              ) : null}
            </Box>
          );
        })
      }
      isOptionEqualToValue={(option, value) => {
        return option.key && value.key
          ? option.key === value.key
          : value.label === option.label;
      }}
      onChange={(ev, value) => {
        if (onChange) {
          onChange({
            ...ev,
            target: {
              ...ev.target,
              name,
              value: Array.isArray(value)
                ? value.map(val => (nestedValue ? val?.value : val))
                : nestedValue
                  ? value?.value || null
                  : value,
            },
          });
        }
        setInputValue('');
      }}
      renderInput={renderProps => {
        return (
          <TextField
            onBlur={onBlur}
            name={name}
            error={error}
            placeholder={placeholder}
            {...renderProps}
            InputProps={{
              ...renderProps.InputProps,
              disableUnderline: variant === 'generic',
              ...(startAdornment ? { startAdornment } : {}),
              ...(inputRef ? { inputRef } : {}),
            }}
            inputProps={{
              ...renderProps.inputProps,
              ...inputProps,
            }}
          />
        );
      }}
      filterOptions={(options, state) =>
        options.filter(opt =>
          (filterOption || defaultFilterOptions)(opt, state.inputValue)
        )
      }
      groupBy={groupBy}
    />
  );
};

export default FormSelect;
