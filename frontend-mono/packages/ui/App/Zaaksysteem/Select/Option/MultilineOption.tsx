// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import { useMultilineOptionStyle } from './MultilineOption.style';

const MultilineOption = ({ ExtensionComponent, subLabel, ...rest }: any) => {
  const { icon, label, url, depth } = rest;
  const classes = useMultilineOptionStyle();
  const Wrapper = url ? Link : Box;

  return (
    <div {...rest}>
      {React.createElement(
        //@ts-ignore
        Wrapper,
        {
          target: '_blank',
          rel: 'noopener noreferrer',
          href: url,
          underline: 'none',
          className: classes.optionWrapper,
        },
        <>
          {icon && <div className={classes.icon}>{icon}</div>}
          <div
            className={classes.labelWrapper}
            style={
              typeof depth === 'number' ? { marginLeft: 20 * depth + 'px' } : {}
            }
          >
            <div className={classes.optionLabel}>
              <Typography variant="body1">{label}</Typography>
            </div>
            {subLabel && (
              <div className={classes.subLabel}>
                <Typography variant="body2">{subLabel}</Typography>
              </div>
            )}
          </div>

          {ExtensionComponent && <ExtensionComponent {...rest} />}
        </>
      )}
    </div>
  );
};

export default MultilineOption;
