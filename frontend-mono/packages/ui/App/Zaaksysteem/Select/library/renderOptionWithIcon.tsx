// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import MultilineOption from '../Option/MultilineOption';

export const renderOptionWithIcon =
  (iconName: any) => (props: any, option: any) => {
    const name = typeof iconName === 'string' ? iconName : iconName(option);
    return (
      <MultilineOption
        {...props}
        {...option}
        icon={<ZsIcon size="tiny">{name}</ZsIcon>}
      />
    );
  };

export default renderOptionWithIcon;
