// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import classNames from 'classnames';
import React from 'react';
import { useSheetStyles } from './Sheet.style';

type SheetPropsType = {
  classes?: Record<string, string>;
  children: React.ReactNode;
};

const Sheet: React.FunctionComponent<SheetPropsType> = ({
  children,
  classes = {},
}) => {
  const sheetClasses = useSheetStyles();
  const background = classNames(sheetClasses.background, classes.background);
  const sheet = classNames(sheetClasses.sheet, classes.sheet);

  return (
    <div className={background}>
      <div className={sheet}>{children}</div>
    </div>
  );
};

export default Sheet;
