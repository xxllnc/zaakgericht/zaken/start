// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import List from '@mui/material/List';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import { ListItemButton } from '@mui/material';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import classNames from 'classnames';
import { useSideMenuStyles } from './SideMenu.styles';

export type SideMenuItemType = {
  label: string;
  tooltip?: string;
  ariaLabel?: string;
  component?: any;
  selected?: boolean;
  href?: string;
  onClick?: () => void;
  count?: number;
  invalid?: boolean;
  icon?: React.ReactElement;
  [key: string]: any;
};

export type SideMenuPropsType = {
  items: SideMenuItemType[];
  folded?: boolean;
};

export const SideMenu: React.ComponentType<SideMenuPropsType> = ({
  items,
  folded,
}) => {
  const classes = useSideMenuStyles();
  const width = useWidth();

  const defaultFolded = width === 'sm' || width === 'xs';
  const isFolded = folded === undefined ? defaultFolded : folded;

  return (
    <List component="nav" role="navigation">
      {items.map(
        /* eslint complexity: [2, 7] */
        ({
          component,
          icon,
          label,
          selected,
          href,
          onClick,
          tooltip,
          count,
          invalid,
        }) => {
          const tooltipTitle = tooltip || isFolded ? label : '';

          return (
            <Tooltip key={label} title={tooltipTitle} placement="right">
              <ListItemButton
                role="tab"
                href={href}
                onClick={onClick}
                component={component}
                selected={selected}
                classes={{
                  root: classes.root,
                  selected: classes.selected,
                }}
              >
                {icon && (
                  <ListItemIcon
                    classes={{
                      root: classNames({
                        [classes.iconSelected]: selected,
                      }),
                    }}
                  >
                    <Counter className={classes.counter} count={count}>
                      <div
                        className={classNames({ [classes.invalid]: invalid })}
                      >
                        {icon}
                      </div>
                    </Counter>
                  </ListItemIcon>
                )}
                <div className={classNames({ [classes.invalid]: invalid })}>
                  {label && !isFolded && <ListItemText>{label}</ListItemText>}
                </div>
              </ListItemButton>
            </Tooltip>
          );
        }
      )}
    </List>
  );
};
