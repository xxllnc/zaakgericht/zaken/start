// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Loader } from '../Loader';
import { SortableTablePropsType } from './SortableTable.types';

const Comp = React.lazy(() => import('./SortableTable'));

export * from './SortableTable.types';

export { useSortableTableStyles } from './SortableTable.style';

export { reorder } from './library/reorder';

export default (props: SortableTablePropsType) => (
  <React.Suspense fallback={<Loader />}>
    <Comp {...props} />
  </React.Suspense>
);
