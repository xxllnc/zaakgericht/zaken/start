// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { parseSize } from './parseSize';

/**
 * `Svg` component utility function.
 *
 * @param {Array} viewBox
 * @param {string} width
 * @return {Object}
 */
export function getStyleFromWidth(viewBox, width) {
  const [viewBoxWidth, viewBoxHeight] = viewBox;
  const [size, unit] = parseSize(width);
  const vertical = (viewBoxHeight / viewBoxWidth) * size;
  const value = `${vertical}${unit}`;

  if (unit === '%') {
    return {
      width,
      paddingTop: value,
    };
  }

  return {
    width,
    height: value,
  };
}
