// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable no-magic-numbers */
import { toViewBox } from './toViewBox';

/**
 * @test {toViewBox}
 */
describe('The `Svg` component’s `toViewBox` utility function', () => {
  test('can be passed a number', () => {
    expect(toViewBox(42)).toEqual([0, 0, 42, 42]);
  });

  test('can be passed an array with two numbers', () => {
    expect(toViewBox([42, 24])).toEqual([0, 0, 42, 24]);
  });

  test('can be passed an array with four numbers', () => {
    expect(toViewBox([0, 0, 42, 42])).toEqual([0, 0, 42, 42]);
  });

  test('throws if it is not passed an array or a number', () => {
    expect(() => toViewBox()).toThrow();
    expect(() => toViewBox('42')).toThrow();
    expect(() => toViewBox({})).toThrow();
  });

  test('throws if it is passed an array with one number', () => {
    expect(() => toViewBox([42])).toThrow();
  });

  test('throws if it is passed an array with three numbers', () => {
    expect(() => toViewBox([42, 42, 42])).toThrow();
  });

  test('throws if it is passed an array with more than four numbers', () => {
    expect(() => toViewBox([42, 42, 42, 42, 42])).toThrow();
  });
});
