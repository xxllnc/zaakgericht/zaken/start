// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router-dom';
import List from '@mui/material/List';
import { ListItemButton, useTheme } from '@mui/material';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Icon, { IconNameType } from '@mintlab/ui/App/Material/Icon';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useTopMenuStyles } from './TopMenu.styles';

export type TopMenuItemType = {
  selected: boolean;
  icon?: IconNameType;
  label: string;
  count?: number;
  href?: string;
  onClick?: () => void;
};

export type TopMenuPropsType = {
  items: TopMenuItemType[];
  folded?: boolean;
  fullWidth?: boolean;
};

/* eslint complexity: [2, 10] */
export const TopMenu: React.ComponentType<TopMenuPropsType> = ({
  items,
  folded,
  fullWidth = false,
}) => {
  const classes = useTopMenuStyles();
  const navigate = useNavigate();
  const width = useWidth();

  const {
    palette: { primary, common, elephant },
  } = useTheme<Theme>();

  const defaultFolded = width === 'sm' || width === 'xs';
  const isFolded = folded === undefined ? defaultFolded : folded;

  return (
    <List
      sx={{
        display: 'flex',
        justifyContent: fullWidth ? 'center' : 'flex-start',
        padding: 0,
        height: 50,
      }}
      component="nav"
      role="navigation"
    >
      {items.map(
        (
          { href, selected, icon, label, count, onClick }: TopMenuItemType,
          index: number
        ) => (
          <ListItemButton
            role="button"
            onClick={onClick ? onClick : () => navigate(href || '')}
            key={index}
            selected={selected}
            sx={{
              padding: '10px 20px',
              color: elephant.dark,
              ...(fullWidth
                ? {
                    display: 'flex',
                    justifyContent: 'center',
                  }
                : {
                    flexGrow: 0,
                    maxWidth: 300,
                  }),
              overflow: 'hidden',
              ...(selected
                ? {
                    borderBottom: `2px solid ${primary.main}`,
                    '&&': {
                      color: primary.main,
                      backgroundColor: common.white,
                    },
                  }
                : {}),
            }}
          >
            {icon && !isFolded && (
              <ListItemIcon
                sx={{
                  ...(selected
                    ? {
                        color: primary.main,
                        minWidth: 30,
                      }
                    : {
                        minWidth: 30,
                        color: elephant.dark,
                      }),
                }}
              >
                <Icon size="small">{icon}</Icon>
              </ListItemIcon>
            )}
            <Counter
              count={count}
              margin={5}
              className={selected ? classes.counter : classes.counterInactive}
            >
              <ListItemText
                classes={{ primary: classes.textWrapper }}
                sx={{
                  flex: 'none',
                  width: fullWidth ? 'unset' : '100%',
                  margin: 0,
                }}
              >
                <span className={classes.text}>{label}</span>
              </ListItemText>
            </Counter>
          </ListItemButton>
        )
      )}
    </List>
  );
};
