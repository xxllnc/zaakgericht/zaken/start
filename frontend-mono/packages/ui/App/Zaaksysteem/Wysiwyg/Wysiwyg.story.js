// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, boolean, text } from '../../story';
import Editor from '.';

const stores = {
  Default: {
    value: 'Hey this editor rocks 😀',
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store, value }) {
      const onChange = ({ target }) => {
        store.set({
          value: target.value,
        });
      };

      return (
        <div
          style={{
            width: '500px',
            maxWidth: '500px',
          }}
        >
          <Editor
            name="editor"
            value={value}
            readOnly={boolean('Read-only', false)}
            error={text('Error', '')}
            onChange={onChange}
          />
        </div>
      );
    },
  },
  stores
);
