// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Wysiwyg } from './Wysiwyg';
export * from './Wysiwyg';
export default Wysiwyg;
