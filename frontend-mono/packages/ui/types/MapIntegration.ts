// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type IntegrationData = {
  magic_string: string;
  object: {
    lastModified?: string;
    objectTypeVersionUuid?: string;
    registrationDate?: string;
    status?: string;
    title?: string;
    uuid: string;
    versionIndependentUuid?: string;
  } | null;
  objectType: {
    catalog_folder_id?: number;
    date_created?: string;
    date_deleted?: string;
    external_reference?: string;
    is_active_version?: boolean;
    last_modified?: string;
    name?: string;
    status?: string;
    title?: string;
    version?: number;
    version_independent_uuid?: string;
  } | null;
  case: {
    assignee: null;
    date_of_registration: string;
    date_target: string;
    id: string;
    parent_uuid: string;
    phase: string;
  } | null;
  caseType: {
    id: string;
    title: string;
    trigger: string;
  };
  contact: {
    uuid: string;
    address: string;
    location_address: any;
    geojson: {
      readonly type: 'FeatureCollection';
      readonly features: readonly [
        {
          readonly type: 'Feature';
          readonly properties: {};
          readonly geometry: {
            readonly type: 'Point';
            readonly coordinates: [number, number] | null;
          };
        },
      ];
    };
  } | null;
  contactType: string;
};

export type LatLng = [number, number];
export type Geojson = GeoJSON.GeoJsonObject;

export type IntegrationContextType =
  | {
      type: 'ContactMap';
      data: Pick<IntegrationData, 'contact' | 'contactType'>;
    }
  | { type: 'ObjectMap'; data: Pick<IntegrationData, 'object' | 'objectType'> }
  | {
      type: 'ObjectView';
      data: Pick<IntegrationData, 'object' | 'objectType' | 'magic_string'>;
    }
  | {
      type: 'CaseForm';
      data: Pick<IntegrationData, 'case' | 'caseType' | 'magic_string'>;
    }
  | {
      type: 'CaseObjectForm';
      data: Pick<
        IntegrationData,
        'case' | 'caseType' | 'object' | 'objectType' | 'magic_string'
      >;
    }
  | {
      type: 'CaseRegistrationForm';
      data: Pick<IntegrationData, 'caseType' | 'magic_string'>;
    }
  | { type: 'CaseMap'; data: Pick<IntegrationData, 'case' | 'caseType'> }
  | {
      type: 'WebformFormField';
      data: null;
    }
  | { type: 'AdvancedSearch'; data: null };

type CurrentVersion = 5;
type EventOfMessage<T> = { data: T };
type Message<TypeName, Value> = {
  type: TypeName;
  name: string;
  version: CurrentVersion;
  value: Value;
};

export type MapInitType = {
  initialFeature: GeoJSON.GeoJsonObject | null;
  canDrawFeatures: boolean;
  canSelectLayers: boolean;
  center: LatLng;
  context: IntegrationContextType;
  region: 'nl' | 'cw';
  featureRequestTargetLayer?: string;
  wmsLayers: {
    url: string;
    layers: string;
    label: string;
    xpath?: string | null;
  }[];
};

type InitMessageType = Message<'init', MapInitType>;
type InitEventType = EventOfMessage<InitMessageType>;

type SetFeatureMessageType = Message<
  'setFeature',
  GeoJSON.GeoJsonObject | null
>;
type SetFeatureEventType = EventOfMessage<SetFeatureMessageType>;

type SetMarkerMessageType = Message<'setMarker', GeoJSON.Point | null> & {
  xpathQuery?: boolean;
};
type SetMarkerEventType = EventOfMessage<SetMarkerMessageType>;

type SetDrawMessageType = Message<'setDrawToolbar', boolean>;
type SetDrawEventType = EventOfMessage<SetDrawMessageType>;

type FeatureChangeMessageType = Message<
  'featureChange',
  GeoJSON.GeoJsonObject | null
>;
type FeatureChangeEventType = EventOfMessage<FeatureChangeMessageType>;

type ClickMessageType = Message<'click', GeoJSON.Point>;
type ClickEventType = EventOfMessage<ClickMessageType>;

type GetFeatureInfoResolvedMessageType = Message<
  'getFeatureInfoResolved',
  string
>;
type GetFeatureInfoResolvedEventType =
  EventOfMessage<GetFeatureInfoResolvedMessageType>;

export type ExternalMapEventType =
  | InitEventType
  | SetFeatureEventType
  | SetMarkerEventType
  | SetDrawEventType;

export type ExternalMapMessageType =
  | FeatureChangeMessageType
  | GetFeatureInfoResolvedMessageType
  | ClickMessageType;

export type WrapperMapMessageType =
  | SetFeatureMessageType
  | InitMessageType
  | SetMarkerMessageType
  | SetDrawMessageType;

export type WrapperMapEventType =
  | FeatureChangeEventType
  | GetFeatureInfoResolvedEventType
  | ClickEventType;

type ZsCaseData = {
  identifier: string;
  title: string;
  address: string;
  status: string;
  family: 'case';
  type: string;
  assignee: string;
  requestor: string;
  caseNumber: number;
  deadline: number;
  parentLink: string;
};

type ZsObjectData = {
  identifier: string;
  title: string;
  status: string;
  family: 'object';
  type: string;
};

type ZsContactData = {
  title: string;
  family: 'organization' | 'person';
  status: string;
  location_description: string;
  identifier: string;
};

export type ZaaksysteemFeatureProp = {
  origin: ZsCaseData | ZsObjectData | ZsContactData;
  self: ZsCaseData | ZsObjectData | ZsContactData;
};
