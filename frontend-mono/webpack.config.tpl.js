// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable complexity */

import { resolve as _resolve, relative, dirname } from 'path';
import { config } from 'dotenv';
import { fileURLToPath } from 'url';
import { createRequire } from 'module';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { InjectManifest } from 'workbox-webpack-plugin';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import copyPublicDirectoryPlugin from './node/copyPublicDirectoryPlugin.js';
import copyClientIndexesPlugin from './node/copyClientIndexesPlugin.js';

config({
  path: '../../../.env',
});

const req = createRequire(import.meta.url);
const webpack = req('webpack');
const MEP = req('mini-css-extract-plugin');
const BundleAnalyzerPlugin = req(
  'webpack-bundle-analyzer'
).BundleAnalyzerPlugin;

const thisFileDirectory = dirname(fileURLToPath(import.meta.url));

const isEnvDevelopment =
  Boolean(process.env.PROD_REACT_ONLY) ||
  process.env.NODE_ENV === 'development';

const makeEnvVarDefinitions = xs =>
  Object.fromEntries(
    xs.map(name => [
      `process.env.${name}`,
      process.env[name] ? JSON.stringify(process.env[name]) : undefined,
    ])
  );

export default app => {
  return {
    mode: isEnvDevelopment ? 'development' : 'production',
    bail: !isEnvDevelopment,
    devtool: 'source-map',
    entry: { [app.name]: app.indexPath },
    output: {
      path: _resolve(thisFileDirectory, 'build', 'apps', app.name),
      pathinfo: isEnvDevelopment,
      filename: '[name].js',
      chunkFilename: '[name].chunk.js',
      publicPath: app.publicUrlFrag,
      devtoolModuleFilenameTemplate: isEnvDevelopment
        ? info => _resolve(info.absoluteResourcePath).replace(/\\/g, '/')
        : info =>
            relative(app.srcPath, info.absoluteResourcePath).replace(
              /\\/g,
              '/'
            ),
      globalObject: 'this',
    },
    optimization: {
      minimize: !isEnvDevelopment,
      splitChunks: {
        chunks: 'async',
        minSize: 20000,
        minRemainingSize: 0,
        minChunks: 1,
        maxAsyncRequests: 30,
        maxInitialRequests: 30,
        enforceSizeThreshold: 50000,
        cacheGroups: {
          defaultVendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
            reuseExistingChunk: true,
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true,
          },
        },
        name: isEnvDevelopment ? false : app.name,
      },
      runtimeChunk: {
        name: entrypoint => `runtime-${entrypoint.name}`,
      },
    },
    resolve: {
      modules: ['node_modules'],
      extensions: ['.js', '.ts', '.tsx', '.svg', '.css', '.png'],
      alias: {
        'react/jsx-runtime': 'react/jsx-runtime.js',
        '@zaaksysteem': _resolve(thisFileDirectory, 'packages'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(js|mjs|jsx|ts|tsx)$/,
          include: [
            app.srcPath,
            _resolve(thisFileDirectory, 'packages', 'common', 'src'),
            _resolve(thisFileDirectory, 'packages', 'ui', 'App'),
            _resolve(
              thisFileDirectory,
              'packages',
              'communication-module',
              'src'
            ),
            _resolve(thisFileDirectory, 'packages', 'kitchen-sink', 'source'),
          ],
          exclude: /(node_modules)/,
          loader: 'swc-loader',
          resolve: { fullySpecified: false },
          options: {
            jsc: {
              target: 'es2022',
              parser: {
                syntax: 'typescript',
                jsx: true,
                tsx: true,
                dynamicImport: true,
                exportDefaultFrom: true,
              },
            },
          },
        },
        {
          test: /.css$/,
          use: [MEP.loader, 'css-loader'],
          sideEffects: true,
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                mimetype: 'application/font-woff',
              },
            },
          ],
        },
        {
          test: /.svg$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                mimetype: 'image/svg+xml',
              },
            },
          ],
        },
        {
          test: /.png$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                mimetype: 'image/png',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new InjectManifest({
        swSrc: _resolve(thisFileDirectory, 'packages', 'sw.mjs'),
        swDest: _resolve(
          thisFileDirectory,
          'build',
          'apps',
          app.name,
          'service-worker.js'
        ),
      }),
      new webpack.DefinePlugin(
        makeEnvVarDefinitions([
          'APP_CONTEXT_ROOT',
          'NODE_ENV',
          'PUBLIC_URL',
          'MUI_LICENSE_KEY',
          'SKIP_SW',
        ])
      ),
      new HtmlWebpackPlugin(
        Object.assign(
          {},
          {
            inject: true,
            template: app.htmlPath,
          },
          isEnvDevelopment
            ? undefined
            : {
                minify: {
                  removeComments: true,
                  collapseWhitespace: true,
                  removeRedundantAttributes: true,
                  useShortDoctype: true,
                  removeEmptyAttributes: true,
                  removeStyleLinkTypeAttributes: true,
                  keepClosingSlash: true,
                  minifyJS: true,
                  minifyCSS: true,
                  minifyURLs: true,
                },
              }
        )
      ),
      new BundleAnalyzerPlugin({
        analyzerMode: process.env.BUNDLE_ANALYZER || 'disabled',
      }),
      new MEP(),
      new WebpackManifestPlugin({
        fileName: 'asset-manifest.json',
        publicPath: app.publicUrlFrag,
        generate: (seed, files, entrypoints) => {
          const manifestFiles = files.reduce((manifest, file) => {
            manifest[file.name] = file.path;
            return manifest;
          }, seed);
          const entrypointFiles = entrypoints[app.name].filter(
            fileName => !fileName.endsWith('.map')
          );

          return {
            files: manifestFiles,
            entrypoints: entrypointFiles,
          };
        },
      }),
      new webpack.IgnorePlugin({ resourceRegExp: /moment$/ }),
      copyPublicDirectoryPlugin(app.name),
      copyClientIndexesPlugin(),
    ].filter(Boolean),
  };
};
