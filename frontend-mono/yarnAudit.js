// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable complexity, no-empty */

import { exec } from 'child_process';

const reason =
  'This affects only part of the build script and we dont ship this to the client';

const ignoredCves = [
  { id: 'GHSA-3h5v-q93c-6h6q', reason },
  { id: 'GHSA-grv7-fg5c-xmjg', reason },
  { id: 'GHSA-8hc4-vh64-cxmj', reason },
  { id: 'GHSA-rhx6-c78j-4q9w', reason },
];

let fullMsg = '';
const proc = exec('yarn audit --json');
proc.stdout.on('data', msg => {
  fullMsg += msg.toString();
});

proc.addListener('close', () => {
  fullMsg.split('\n').forEach(msg => {
    if (msg && msg.trim()) {
      let adv;
      try {
        adv = JSON.parse(msg.trim());
      } catch (err) {
        console.log(err);
      }

      const id =
        adv &&
        adv.data &&
        adv.data.advisory &&
        adv.data.advisory.github_advisory_id;
      const severity =
        adv && adv.data && adv.data.advisory && adv.data.advisory.severity;
      const cve = ignoredCves.find(cve => cve.id === id);

      if (cve) {
        console.log('Ignoring ' + cve.id, cve.reason);
        cve.logged = true;
      } else if (
        adv.type !== 'auditSummary' &&
        ['high', 'critical'].includes(severity)
      ) {
        console.log(adv.data.advisory.github_advisory_id);
        throw new Error(
          'New high/critical vulnerabilites were introduced, run `yarn audit` manually for more info'
        );
      }
    }
  });

  ignoredCves.forEach(cve => {
    !cve.logged && console.log('No longer flagged ' + cve.id);
  });
});
