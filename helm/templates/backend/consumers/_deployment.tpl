{{/*
Deployment template
*/}}
{{- define "consumer.deployment" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- if $app.enabled -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
  labels:
    {{- include "common.labels" (list $ $app.name) | nindent 4 }}
spec:
  replicas: {{ $app.replicaCount }}
  strategy:
    {{- $app.strategy | default $.Values.global.strategy | toYaml | nindent 4 }}
  selector:
    matchLabels:
      {{- include "selectorLabels" (list $ $app.name) | nindent 6 }}
  template:
    metadata:
      {{- with $.Values.global.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "selectorLabels" (list $ $app.name) | nindent 8 }}
        {{- include "podLabels" (list $ $app) | nindent 8 }}
    spec:
      {{- with $.Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "serviceAccountName" (list $ $app.name) }}
      securityContext:
        {{- include "zaaksysteem.default_pod_security_context" $ | nindent 8 }}
      volumes:
        - name: zaaksysteem-config
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" $ }}
        - name: zaaksysteem-dkim
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" $ }}-dkim
        - name: instance-config
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" $ }}-customerd
        {{- include "statsd.volume" $ | nindent 8 }}
        {{- if $app.image.mountTmpVolume }}
        - name: tmp
          emptyDir: {}
        {{- end }}
      containers:
        - name: {{ $app.name }}
          securityContext:
            {{- include "zaaksysteem.default_security_context" $ | nindent 12 }}

          image: "{{ $app.image.repository }}:{{ $app.image.tag | default $.Chart.AppVersion }}"
          imagePullPolicy: {{ $.Values.global.image.pullPolicy }}

          command:
            {{- toYaml $app.image.command | nindent 12 }}

          env:
            - name: MQ_EXCHANGE
              value: "{{ $.Values.global.rabbitmq.event_exchange | default $.Values.global.rabbitmq.exchange }}"
            - name: API_HOSTNAME
              value: "{{ $.Values.backend.perl_api_internal.name }}:{{ $.Values.backend.perl_api_internal.service.port }}"

          volumeMounts:
            - name: zaaksysteem-config
              mountPath: /opt/{{- $app.name -}}/logging.conf
              subPath: logging.conf
            - name: zaaksysteem-config
              mountPath: /opt/{{- $app.name -}}/config.conf
              subPath: zaaksysteem.python.conf
            - name: instance-config
              mountPath: /etc/zaaksysteem/customer.d
            - name: zaaksysteem-dkim
              mountPath: /etc/zaaksysteem/dkim.key
              subPath: dkim.key
            {{- if $app.image.mountTmpVolume }}
            - mountPath: /tmp
              name: tmp
            {{- end }}

          resources:
            {{- toYaml $app.resources | nindent 12 }}

        {{- include "statsd.container" $ | nindent 8 }}

      {{- with $app.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        {{- include "zaaksysteem.affinity" (dict "name" $app.name "Release" $.Release) | nindent 8 }}
      {{- with $app.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}
{{- end }}
