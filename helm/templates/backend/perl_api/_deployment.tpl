{{ define "perl_api.deployment" }}
{{- $ := index . 0 }}
{{- $app := index . 1 }}

{{- if $app.enabled -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
  labels:
    {{- include "common.labels" (list $ $app.name) | nindent 4 }}
spec:
  replicas: {{ $app.replicaCount }}
  strategy:
    {{- $app.strategy | default $.Values.global.strategy | toYaml | nindent 4 }}
  selector:
    matchLabels:
     {{- include "selectorLabels" (list $ $app.name) | nindent 6 }}
  template:
    metadata:
      {{- with $.Values.global.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
       {{- include "selectorLabels" (list $ $app.name) | nindent 8 }}
    spec:
      {{- with $.Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "serviceAccountName" (list $ $app.serviceAccountName) }}
      securityContext:
        {{- include "zaaksysteem.default_pod_security_context" $ | nindent 8 }}

      volumes:
        - name: zaaksysteem-config
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" $ }}
        - name: zaaksysteem-dkim
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" $ }}-dkim
        - name: instance-config
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" $ }}-customerd
        - name: perlapi-config
          configMap:
            defaultMode: 0444
            name: {{ include "fullname" (list $ $app.name) }}
        - name: tmp
          emptyDir: {}
        {{- include "statsd.volume" $ | nindent 8 }}

      containers:
        - name: {{ $app.name }}
          securityContext:
            {{- include "zaaksysteem.default_security_context" . | nindent 12 }}

          image: "{{ $app.image.repository }}:{{ $app.image.tag | default $.Chart.AppVersion }}"
          imagePullPolicy: {{ $.Values.global.image.pullPolicy }}

          env:
            - name: API_HOSTNAME
              value: "{{ $.Values.backend.perl_api_internal.name }}:{{ $.Values.backend.perl_api_internal.service.port }}"
            - name: DOCUMENT_CONVERTER_URL
              value: "http://{{ $.Values.backend.document_converter.name }}:{{ $.Values.backend.document_converter.service.port }}"
            - name: OLO_SERVICE_HOST
              value: "{{ $.Values.backend.olo_syncer.name }}:{{ $.Values.backend.olo_syncer.service.port }}"
            - name: VIRUS_SCAN_SERVICE_HOST
              value: "{{ $.Values.backend.virus_scanner.name }}:{{ $.Values.backend.virus_scanner.service.port }}"
            - name: ZAAKSYSTEEM_BUNDLE_VERSION
              value: "{{ $.Values.global.bundleVersion | default $.Chart.AppVersion }}"
            - name: SMTP_SERVER
              value: "{{ $.Values.backend.smtp_relay.name }}"
            - name: SMTP_PORT
              value: "{{ $.Values.backend.smtp_relay.service.port }}"
            - name: SMTP_TLS
              value: "0"
            - name: DKIM_CURRENT_SELECTOR
              value: "{{ $.Values.global.email.dkim_selector }}"
            - name: DKIM_KEY_FILE
              value: "/etc/zaaksysteem/dkim.key"
            - name: "ZAAKSYSTEEM_INTERNAL"
              value: {{ $app.is_internal | quote }}

          command:
            - "starman"
            - "--preload-app"
            - "--host=0.0.0.0"
            - "--port={{ $app.service.port }}"
            - "--workers={{ $app.workers | default 10 }}"
            - "/opt/zaaksysteem/script/zaaksysteem.psgi"

          ports:
            - name: http
              containerPort: {{ $app.service.port }}
              protocol: TCP

          volumeMounts:
            - name: zaaksysteem-config
              mountPath: /etc/zaaksysteem/zaaksysteem.conf
              subPath: zaaksysteem.perl.conf
            - name: instance-config
              mountPath: /etc/zaaksysteem/customer.d
            - name: perlapi-config
              mountPath: /etc/zaaksysteem/log4perl.conf
              subPath: log4perl.conf
            - name: zaaksysteem-dkim
              mountPath: /etc/zaaksysteem/dkim.key
              subPath: dkim.key
            - mountPath: /tmp
              name: tmp

          readinessProbe:
            {{- toYaml $app.readinessProbe | nindent 12 }}
            httpGet:
              path: /health
              port: {{ $app.service.port }}
              httpHeaders:
                - name: Host
                  value: {{ $app.readinessProbeHostname | quote }}

          resources:
            {{- toYaml $app.resources | nindent 12 }}

          # Crude hack to wait a bit, so the ingresses catch up with the fact
          # that this pod is going away; this prevents some 502 errors.
          # Also send SIGQUIT to starman to do a graceful shutdown (K8s sends
          # SIGTERM by default, which would cause starman to do an immediate
          # shutdown, cancelling "in progress" requests, leading to another
          # group of 502 errors)
          lifecycle:
            preStop:
              exec:
                command: ["/bin/sh", "-c", "sleep 10 && kill -SIGQUIT 1 && sleep 20"]

        {{- include "statsd.container" $ | nindent 8 }}

      {{- with $app.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        {{- include "zaaksysteem.affinity" (dict "name" $app.name "Release" $.Release) | nindent 8 }}
      {{- with $app.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      terminationGracePeriodSeconds: 60
{{- end }}
{{- end }}
