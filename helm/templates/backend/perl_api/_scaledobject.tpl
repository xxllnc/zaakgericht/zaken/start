{{/*
ScaledObject template
*/}}
{{- define "perl_api.scaled_object" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- $scaling := $app.scaling | default $.Values.global.http_scaling -}}
{{- if (and $app.enabled $.Values.global.keda.enabled) -}}
apiVersion: keda.sh/v1alpha1
kind: ScaledObject
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
  namespace: {{ $.Release.Namespace }}
spec:
  scaleTargetRef:
    name: {{ include "fullname" (list $ $app.name) }}

  {{- $scaling.parameters | toYaml | nindent 2 }}
  triggers:
    - type: prometheus
      metadata:
        serverAddress: {{ $.Values.global.prometheus_address | quote }}
        query: 'sum(rate(request_count{namespace="{{ $.Release.Namespace }}",component="{{ $app.component_name }}"}[2m]))'
        threshold: {{ $scaling.requests_per_second | quote }}
{{- end }}
{{- end }}
