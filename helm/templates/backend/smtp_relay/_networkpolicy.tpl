{{- define "egress_policy.to_smarthost" -}}
- toFQDNs:
    - matchName: {{ .Values.backend.smtp_relay.smarthost.host | quote }}
  toPorts:
    - ports:
      - port: {{ .Values.backend.smtp_relay.smarthost.port | quote }}
        protocol: TCP
{{- end }}