{{- if .Values.backend.swagger_ui.enabled -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "fullname" (list . .Values.backend.swagger_ui.name) }}
  labels:
    {{- include "common.labels" (list . .Values.backend.swagger_ui.name) | nindent 4 }}
spec:
  replicas: {{ .Values.backend.swagger_ui.replicaCount }}
  strategy:
    {{- toYaml .Values.backend.swagger_ui.strategy | nindent 4 }}
  selector:
    matchLabels:
      {{- include "selectorLabels" (list . .Values.backend.swagger_ui.name) | nindent 6 }}
  template:
    metadata:
      {{- with .Values.global.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "selectorLabels" (list . .Values.backend.swagger_ui.name) | nindent 8 }}
    spec:
      {{- with .Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "serviceAccountName" (list . .Values.backend.swagger_ui.name) }}
      securityContext:
        {{- include "zaaksysteem.default_pod_security_context" . | nindent 8 }}

      containers:
        - name: {{ .Values.backend.swagger_ui.name }}
          securityContext:
            # Swagger-ui doesn't support readonly file system so override it here
            # https://github.com/swagger-api/swagger-ui/pull/7413
            readOnlyRootFilesystem: false
            capabilities:
              drop:
                - ALL

          image: "{{ .Values.backend.swagger_ui.image.repository }}:{{ .Values.backend.swagger_ui.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: Always
          env:
          - name: BASE_URL
            value: /apidocs
          - name: URLS
            value: |
              [
                  { url: "/api/v2/admin/catalog/openapi/openapi30.json", name: "API v2 - Admin" },
                  { url: "/api/v2/cm/openapi/openapi30.json", name: "API v2 - Case Management" },
                  { url: "/api/v2/communication/openapi/openapi30.json", name: "API v2 - Communication" },
                  { url: "/api/v2/document/openapi/openapi30.json", name: "API v2 - Documents" },
                  { url: "/api/v2/geo/openapi/openapi30.json", name: "API v2 - Geo" },
                  { url: "/api/v2/style/openapi/openapi30.json", name: "API v2 - Style Configuration" },
                  { url: "/api/v2/jobs/openapi/openapi30.json", name: "API v2 - Jobs" },
                  { url: "/doc/api/v1/swagger/index.yaml", name: "API v1" }
              ]
          {{- range $key, $value := .Values.extraEnv }}
          - name: {{ $key }}
            value: "{{ $value }}"
          {{- end }}

          ports:
            - name: http
              containerPort: {{ .Values.backend.swagger_ui.service.port }}
              protocol: TCP

          resources:
            {{- toYaml .Values.backend.swagger_ui.resources | nindent 12 }}

      {{- with .Values.backend.swagger_ui.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        {{- include "zaaksysteem.affinity" (dict "name" .Values.backend.swagger_ui.name "Release" $.Release) | nindent 8 }}
      {{- with .Values.backend.swagger_ui.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}