# A collection of commonly used network policies.

###############################################################################
# Ingress policies
# Define ingress policies like this ("ingress_policy.NAME_HERE")
###############################################################################
{{- define "ingress_policy.from_ingress" -}}
- fromEndpoints:
  - matchLabels:
      k8s:io.kubernetes.pod.namespace: ingress-nginx
{{- end }}

{{- define "ingress_policy.from_perl_api" -}}
- fromEndpoints:
  - matchLabels:
    {{- include "selectorLabels" (list . .Values.backend.perl_api.name) | nindent 8 }}
  - matchLabels:
    {{- include "selectorLabels" (list . .Values.backend.perl_api_internal.name) | nindent 8 }}
{{- end }}

###############################################################################
# Egress policies
# Define egress policies like this ("egress_policy.NAME_HERE")
###############################################################################
{{- define "egress_policy.to_rabbitmq" -}}
- toFQDNs:
    - matchName: {{ .Values.global.rabbitmq.host | quote }}
  toPorts:
    - ports:
      - port: {{ .Values.global.rabbitmq.port | quote }}
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_outgoing_mail" -}}
- toPorts:
    - ports:
      # SMTP (+ StartTLS)
      - port: "25"
        protocol: TCP
      # SMTPS
      - port: "465"
        protocol: TCP
      # SMTP Submission
      - port: "587"
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_incoming_mail" -}}
- toPorts:
    - ports:
      # POP3 (+ StartTLS)
      - port: "110"
        protocol: TCP
      # POP3S
      - port: "995"
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_microsoft_oauth_service" }}
- toFQDNs:
    # Microsoft's O365 OAuth service.
    - matchName: login.microsoftonline.com
    - matchName: login.microsoft.com
  toPorts:
    - ports:
      - port: "443"
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_wopi_proxy" -}}
- toFQDNs:
    - matchName: {{ .Values.global.wopi.host | quote }}
  toPorts:
    - ports:
      - port: "443"
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_https_world" -}}
- toPorts:
    - ports:
      - port: "443"
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_world" -}}
- toEntities:
  - world
{{- end }}

{{- define "egress_policy.to_pdf_generator" -}}
- toFQDNs:
    - matchName: {{ .Values.global.pdf.host | quote }}
  toPorts:
    - ports:
      - port: "443"
        protocol: TCP
{{- end }}

{{- define "egress_policy.to_auth0" -}}
- toFQDNs:
    - matchName: {{ .Values.global.oidc.domain | quote }}
  toPorts:
    - ports:
      - port: "443"
        protocol: TCP
{{- end }}

###############################################################################
# Cilium network policy base template
# 
# Include with 2 parameters:
# 
# - Global context
# - App-specific values
# 
# And put two keys in your app's "values" block:
# 
# - ingress_rules
# - egress_rules
# 
# Example:
# 
#   {{ include "default.ciliumnetworkpolicy" $ .Values.some_app }}
###############################################################################
{{- define "default.ciliumnetworkpolicy" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- $ingress_rules := $app.ingress_rules }}
{{- $egress_rules := $app.egress_rules }}
apiVersion: cilium.io/v2
kind: CiliumNetworkPolicy
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
spec:
  endpointSelector:
    matchLabels:
      {{- include "selectorLabels" (list $ $app.name) | nindent 6 }}
  {{- if $ingress_rules }}
  ingress:
    {{- range $ingress_rules }}
    {{- include (printf "ingress_policy.%s" .) $ | nindent 4 }}
    {{- end }}
  {{- end }}
  {{- if $egress_rules }}
  egress:
    {{- range $egress_rules }}
    {{- include (printf "egress_policy.%s" .) $ | nindent 4 }}
    {{- end }}
  {{- end }}
{{- end }}