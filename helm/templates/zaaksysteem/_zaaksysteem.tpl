{{/*
    minty-python pyramid configuration
*/}}

{{- define "zaaksysteem.pyramid.production.ini" -}}
[app:main]
use = egg:{{ .egg }}

pyramid.reload_templates = false
pyramid.debug_authorization = false
pyramid.debug_notfound = false
pyramid.debug_routematch = false
pyramid.default_locale_name = nl

minty_service.infrastructure.config_file = /etc/zaaksysteem/zaaksysteem.conf
session_manager = True
session_cookie_name = zaaksysteem_session

[server:main]
use = egg:waitress#main
listen = 0.0.0.0:{{ .servicePort }}

{{/*
    Max upload size is 2 GiB; max request body is slightly larger to accomodate for
    other fields in multipart/form-data.

    2 GiB (1024 * 1024 * 1024 * 2) + 2 MiB (1024 * 1024 * 2) = 2149580800
*/}}
max_request_body_size = 2149580800

trusted_proxy = *
trusted_proxy_headers = x-forwarded-for x-forwarded-host x-forwarded-proto x-forwarded-port
clear_untrusted_proxy_headers = True
log_untrusted_proxy_headers = False
{{- end }}

{{/*
    minty-python logging
*/}}

{{- define "zaaksysteem.python.logging.conf" -}}
{{- $logLevel := .logLevel -}}
[loggers]
keys = root, statsd

[handlers]
keys = custom

[formatters]
keys = json

[logger_root]
{{- if eq $logLevel "TRACE" }}
level = NOTSET
{{- else }}
level = {{ $logLevel }}
{{- end }}
handlers = custom

[logger_statsd]
level = WARNING
handlers = custom
qualname = statsd

[logger_amqpstorm]
level = WARNING

[logger_s3transfer]
level = CRITICAL

[logger_boto3]
level = CRITICAL

[logger_botocore]
level = CRITICAL

[handler_custom]
class = StreamHandler
args = (sys.stderr,)
level = NOTSET
formatter = json

[formatter_json]
format = %(asctime)s %(created)s %(filename)s %(funcName)s %(levelname)s %(levelno)s %(lineno)s %(module)s %(msecs)s %(message)s %(name)s %(pathname)s %(process)s %(processName)s %(relativeCreated)s %(thread)s %(threadName)s
class = pythonjsonlogger.jsonlogger.JsonFormatter
{{- end -}}

{{- define "zaaksysteem.customerd.conf" -}}
<{{ .hostname }}>
    logging_id    = {{ .logging_id }}
    instance_uuid = {{ .instance_id }}
    instance_hostname = {{ .hostname }}
    customer_id   = {{ .customer_id | default "xxllnc" }}
    template      = zaak_v1
    basedir       = /opt/filestore/zaaksysteem

    # ONLY enable this on the Mintlab instance.
    oidc_organization_id = {{ .oidc_organization_id | default "" }}

    # WARNING: Enabling this will enable the "Log in on other instance" feature.
    # THIS WILL ENABLE USERS TO LOG IN ON *EVERY* OTHER INSTANCE
    # ONLY enable this on the Mintlab instance.
    allow_outgoing_login_requests = {{ .allow_outgoing_login_requests | default 0 }}
    #services_ca_cert = /etc/ssl/certs/zaaksysteem.crt

    # This should only be included on the Mintlab instance as well!
    {{- if .outgoing_platform_keys }}
    <cloud_platform_keys>
        {{- range $key, $val := .outgoing_platform_keys }}
        {{ $key }} = {{ $val }}
        {{- end }}
    </cloud_platform_keys>
    {{- end }}

    <Model::DB>
        <connect_info>
            dsn             = dbi:Pg:dbname={{ .database.name }};host={{ .database.host }}
            user            = {{ .database.username }}
            password        = {{ .database.password }}
            pg_enable_utf8  = 1
        </connect_info>
        <slave>
            dsn             = dbi:Pg:dbname={{ .database_ro.name }};host={{ .database_ro.host }}
            user            = {{ .database_ro.username }}
            password        = {{ .database_ro.password }}
            pg_enable_utf8  = 1
        </slave>
    </Model::DB>

    zaaksysteemdb.url = postgresql://{{ .database.username }}:{{ .database.password }}@{{ .database.host }}:5432/{{ .database.name }}
    zaaksysteemdb_ro.url = postgresql://{{ .database_ro.username }}:{{ .database_ro.password }}@{{ .database_ro.host }}:5432/{{ .database_ro.name }}

    # For debugging database issues in Python:
    zaaksysteemdb.echo = False
    zaaksysteemdb_ro.echo = False
</{{ .hostname }}>
{{- end }}
