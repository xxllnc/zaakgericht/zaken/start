"""ClamAV replacement for development only."""

import io
import logging
import os
import socket
import struct

logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)

CLAMAV_IP = os.environ.get('CLAMAV_IP', '0.0.0.0')
CLAMAV_PORT = int(os.environ.get('CLAMAV_PORT', '3310'))
CLAMAV_PORT_STREAM = int(os.environ.get('CLAMAV_PORT_STREAM', '3311'))

EICAR = b"X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*"


class SocketIO(io.RawIOBase):
    def __init__(self, sock):
        self.sock = sock

    def read(self, sz=-1):
        if (sz == -1):
            sz = 0x7FFFFFFF
        return self.sock.recv(sz)

    def read_size(self, sz):
        data = b""

        while len(data) < sz:
            data += self.sock.recv(sz - len(data))

        return data


    def write(self, b):
        return self.sock.send(b)

    def seekable(self):
        return False


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as main_socket:
    main_socket.bind((CLAMAV_IP, CLAMAV_PORT))
    main_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    logger.info(f"Listening on {CLAMAV_IP}:{CLAMAV_PORT}")

    main_socket.listen()

    while True:
        connection_raw, address = main_socket.accept()

        logger.info(f"Accepted connection from {address}")
        connection = SocketIO(connection_raw)

        with connection:
            virus = False

            command = b''
            while next_byte := connection.read_size(1):
                command += next_byte
                if command in (b"PING", b"nINSTREAM\n"):
                    break

                # Invalid starts of commands
                if command not in b"PING" and command not in b"nINSTREAM\n":
                    break

            if command == b"PING":
                connection.write(b"PONG\n")
                continue
            elif command != b"nINSTREAM\n":
                logger.error(f"Unknown command: {command}. Closing connection.")
                connection_raw.shutdown(socket.SHUT_RDWR)
                continue

            data = b''
            while True:
                stream_size = connection.read_size(4)
                (num_bytes,) = struct.unpack('!I', stream_size)
                if num_bytes == 0:
                    break

                logger.info(f"Processing chunk of {num_bytes}")
                chunk = connection.read_size(num_bytes)

                data += chunk

            if data == EICAR:
                virus = True

            if virus:
                connection.write(b"stream: 1 FOUND\n")
            else:
                connection.write(b"stream: OK\n")

            connection_raw.shutdown(socket.SHUT_RDWR)
            connection.close()
