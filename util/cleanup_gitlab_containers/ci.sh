#! /bin/bash

set -e

# Code formatting
isort --check-only . 
black --check --exclude "env" . 
pytest .