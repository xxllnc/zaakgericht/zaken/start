loguru>=0.6.0
joblib>=1.2.0
packaging>=23.0

black>=22.1.0
ruff~=0.0
isort>=5.0
pytest>=7.2.1
