import pytest
from cleanup_gitlab_containers import determine_wich_tags_to_remove
from typing import List

release_tags_to_keep = [
    "release/v2023.2.0.0",
    "release/v2023.1.0.5",
    "release/v2023.1.0.2",
    "release/v2023.1.0.3",
    "release/v2023.1.0.4",
]
release_tags_to_remove = ["release/v2023.1.0.1", "release/v2023.1.0"]

xcp_release_tags_to_keep = [
    "2023.3.0-release",
    "2023.3.5-release",
    "2023.3.2-release",
    "2023.3.3-release",
    "2023.3.4-release",
]
xcp_release_tags_to_remove = ["2023.2.0-release", "2023.2.1-release"]

master_to_keep = [
    "0.0.333-master",
    "0.0.336-master",
    "0.0.337-master",
    "0.0.338-master",
    "0.1.0-master",
    "master-latest",
    "master-ci-270",
    "master-ci-277",
    "master-ci-327",
    "master-ci-333",
    "master-ci-336",
]
master_to_remove = [
    "0.0.277-master",
    "0.0.327-master",
    "master",
    "master-ci-101",
    "master-ci-186",
    "master-ci-216",
    "master-ci-223",
    "master-ci-75",
    "master-ci-81",
    "master-ci-99",
]

development_to_keep = [
    "2.0.333-development",
    "1.0.336-development",
    "0.0.337-development",
    "0.0.338-development",
    "0.1.0-development",
    "development-ci-270",
    "development-ci-277",
    "development-ci-327",
    "development-ci-333",
    "development-ci-336",
    "development-latest",
]
development_to_remove = [
    "0.0.277-development",
    "0.0.327-development",
    "development",
    "development-ci-101",
    "development-ci-186",
    "development-ci-216",
    "development-ci-223",
    "development-ci-75",
    "development-ci-81",
    "development-ci-99",
]

preprod_to_keep = [
    "2.0.0-preprod",
    "2.0.0.1-preprod",
    "2.1.0-preprod",
    "2.2.0-preprod",
    "3.0.0-preprod",
    "preprod-ci-6",
    "preprod-ci-7",
    "preprod-ci-8",
    "preprod-ci-9",
    "preprod-ci-10",
    "preprod-latest",
]
preprod_to_remove = [
    "1.0.277-preprod",
    "0.8.327-preprod",
    "preprod",
    "preprod-ci-1",
    "preprod-ci-2",
    "preprod-ci-3",
    "preprod-ci-4",
    "preprod-ci-5",
]

production_to_keep = [
    "3.0.0.1-production",
    "3.0.1-production",
    "3.0.1.1-production",
    "3.1.0-production",
    "3.1.0.1-production",
    "production-ci-60",
    "production-ci-70",
    "production-ci-80",
    "production-ci-90",
    "production-ci-11",
    "production-latest",
]
production_to_remove = [
    "2.0.0-production",
    "2.0.0.1-production",
    "2.1.0-production",
    "2.2.0-production",
    "3.0.0-production",
    "production-ci-6",
    "production-ci-7",
    "production-ci-8",
    "production-ci-9",
    "production-ci-10",
]

other_tags_to_remove = ["test1234", "branch_x", "prod", "123"]
invalid_tags_to_remove = [
    "0.0.3099-user-minty-11561-port-11402-to",
    "0.0.3092-user-minty-11561-port-11402-to",
]


def _is_in_list(list: List[dict], search_value) -> bool:
    for item in list:
        if item["label"] == search_value:
            return True
    return False


def test_determine_wich_tags_to_remove():
    to_be_removed = [
        *master_to_remove,
        *release_tags_to_remove,
        *xcp_release_tags_to_remove,
        *preprod_to_remove,
        *development_to_remove,
        *production_to_remove,
        *other_tags_to_remove,
        *invalid_tags_to_remove,
    ]
    to_be_kept = [
        *release_tags_to_keep,
        *xcp_release_tags_to_keep,
        *master_to_keep,
        *development_to_keep,
        *preprod_to_keep,
        *production_to_keep,
    ]

    result = determine_wich_tags_to_remove("backend/test", [*to_be_removed, *to_be_kept])

    assert len(result["tags_to_remove"]) == len(to_be_removed)
    assert len(result["tags_to_keep"]) == len(to_be_kept)


@pytest.mark.parametrize(
    "expect_to_keep, expect_to_remove",
    [
        (release_tags_to_keep, release_tags_to_remove),
        (xcp_release_tags_to_keep, xcp_release_tags_to_remove),
        (master_to_keep, master_to_remove),
        (development_to_keep, development_to_remove),
        (preprod_to_keep, preprod_to_remove),
        (production_to_keep, production_to_remove),
    ],
)
def test_determine_wich_release_tags_to_remove(expect_to_keep: list, expect_to_remove: list):
    result = determine_wich_tags_to_remove("backend/test", [*expect_to_keep, *expect_to_remove])

    tags_to_remove = result["tags_to_remove"]
    tags_to_keep = result["tags_to_keep"]

    assert len(tags_to_remove) == len(expect_to_remove)
    assert len(tags_to_keep) == len(expect_to_keep)

    for tag in expect_to_keep:
        assert _is_in_list(tags_to_keep, tag) is True
        assert _is_in_list(tags_to_remove, tag) is False
    for tag in expect_to_remove:
        assert _is_in_list(tags_to_remove, tag) is True
        assert _is_in_list(tags_to_keep, tag) is False
