from queue import Queue
from threading import Thread
import boto3
import logging
from zsapi.database import LocalDatabase
from zsapi import SCRIPT_DIR
from time import perf_counter as pc

logger = logging.getLogger(__name__)
loggers_to_silence = ["boto3", "botocore", "s3transfer", "urllib3"]
for _logger in loggers_to_silence:
    logging.getLogger(_logger).setLevel(logging.WARNING)

GOV_BUCKET = "gov-zaaksysteem-20230614092552890600000002"
LOCAL_DATABASE_PATH = SCRIPT_DIR / "info.db"


def execution_time(func):
    def time_func(*args, **kwargs):
        start = pc()
        result = func(*args, **kwargs)
        end = pc()
        exec_time = end - start
        logger.info(f"{func.__name__} took {exec_time:.2f} seconds")
        return result

    return time_func


@execution_time
def create_queue_from_local_database() -> Queue:
    object_queue = Queue()
    select_query = "SELECT uuid, version_id FROM objects WHERE filestore = 'gemeenscha_accept_e6fde6' AND deleted = 0;"
    with LocalDatabase(LOCAL_DATABASE_PATH) as local_db:
        objects_to_delete = local_db.run_query(select_query)
        object_list = []
        object_count = 0

        for object_tuple in objects_to_delete:
            object_list.append(object_tuple)
            object_count += 1
            if object_count == 1000:
                object_queue.put(object_list)
                object_list = []
                object_count = 0

        if object_list and object_count < 1000:
            object_queue.put(object_list)
    return object_queue
        

@execution_time
def delete_objects(bucket: str, deletion_list: list[tuple[str, str]]) -> None:
    s3 = boto3.resource("s3").meta.client
    deletion_dict = {
        "Objects": [
            {"Key": key, "VersionId": version_id} for key, version_id in deletion_list
        ]
    }
    s3.delete_objects(Bucket=bucket, Delete=deletion_dict)


@execution_time
def worker(worker_id: int, object_queue: Queue) -> None:
    with LocalDatabase(LOCAL_DATABASE_PATH) as local_db:
        while not object_queue.empty():
            delete_list: list[tuple[str, str]] = object_queue.get()
            object_uuids = [(item[0],) for item in delete_list]
            first_item = delete_list[0]
            last_item = delete_list[-1]
            logger.info(f"Worker-{worker_id}: Deleting {first_item} - {last_item}")
            delete_objects(GOV_BUCKET, delete_list)
            update_query = "UPDATE objects SET deleted  = 1 WHERE uuid = ?;"
            local_db.run_many(update_query, object_uuids)


@execution_time
def main():
    max_workers = 2
    object_queue = create_queue_from_local_database()
    queue_length = object_queue.qsize()
    
    worker_count = max_workers if queue_length > max_workers else queue_length

    workers: list[Thread] = []
    for worker_id in range(worker_count):
        thread = Thread(target=worker, args=(worker_id, object_queue))
        workers.append(thread)
        thread.start()

    for thread in workers:
        thread.join()

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.info("Exiting per user request")
