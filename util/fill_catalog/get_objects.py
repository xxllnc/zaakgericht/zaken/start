import threading
from zsapi import SCRIPT_DIR
from zsapi.database import LocalDatabase
import logging
import boto3
from queue import Queue
from threading import Thread
from time import perf_counter as pc
import json
import argparse
import time

logger = logging.getLogger(__name__)
loggers_to_silence = ["boto3", "botocore", "s3transfer", "urllib3"]
for _logger in loggers_to_silence:
    logging.getLogger(_logger).setLevel(logging.WARNING)

buckets = {
    "zaaksysteem-gov": "gov-zaaksysteem-20230614092552890600000002",
    "zaaksysteem-com": "com-zaaksysteem-20230616060842032000000003",
}

database_filestore_path = SCRIPT_DIR / "database_filestore_map.json"
local_info_db_path = SCRIPT_DIR / "info.db"
local_files_db_path = SCRIPT_DIR / "files.db"
create_table_query = """
CREATE TABLE IF NOT EXISTS objects (
    uuid TEXT PRIMARY KEY,
    filestore TEXT DEFAULT NULL,
    last_modified TEXT,
    version_id TEXT DEFAULT NULL
);"""
insert_query = "REPLACE INTO objects (uuid, filestore, last_modified, version_id) VALUES (?, ?, ?, ?);"

terminate = False


def execution_time(func):
    def time_func(*args, **kwargs):
        start = pc()
        result = func(*args, **kwargs)
        end = pc()
        exec_time = end - start
        logger.info(f"{func.__name__} took {exec_time:.2f} seconds")
        return result

    return time_func


@execution_time
def load_database_filestore_map() -> dict:
    with open(database_filestore_path, "r") as f:
        return json.load(f)


@execution_time
def create_objects_table() -> None:
    with LocalDatabase(local_info_db_path) as local_database:
        local_database.run_query(create_table_query)


def prefix_generator(start: int, prefix_length: int):
    n = start
    max_string = prefix_length * "f"
    limit = int(f"0x{max_string}", 0) + 1
    while n < limit:
        yield hex(n)[2:].rjust(prefix_length, "0")
        n += 1


@execution_time
def list_objects(prefix: str, bucket: str) -> list[str, str]:
    s3 = boto3.resource("s3")
    try:
        object_list = s3.meta.client.list_objects_v2(Bucket=bucket, Prefix=prefix)[
            "Contents"
        ]
    except KeyError:
        logger.info(f"{prefix} on bucket: {bucket} returned 0 results")
        return []
    results = [
        (_obj["Key"], _obj["LastModified"].strftime("%Y-%m-%dT%H:%M:%S.000000Z"))
        for _obj in object_list
    ]
    logger.info(f"prefix {prefix} on {bucket} returned {len(results)} results")
    return results


def get_version(bucket: str, results_queue: Queue, versions_queue: Queue) -> None:
    while not versions_queue.empty():
        key = versions_queue.get()
        version_id = boto3.resource("s3").meta.client.get_object(
            Bucket=bucket, Key=key
        )["VersionId"]
        if not version_id:
            logger.warning(f"Did not find version for {key} in {bucket}")
            continue
        results_queue.put((key, version_id))


@execution_time
def get_versions(bucket: str, object_keys: list[str]) -> dict:
    threads: list[Thread] = []
    object_count = len(object_keys)
    max_workers = 10
    worker_count = max_workers if object_count > max_workers else object_count

    results_queue = Queue()
    versions_queue = Queue()
    for object_key in object_keys:
        versions_queue.put(object_key)
    for _ in range(worker_count):
        if terminate:
            return {}
        thread = Thread(
            target=get_version, args=(bucket, results_queue, versions_queue)
        )
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    results = {}
    while not results_queue.empty():
        key, version_id = results_queue.get()
        results[key] = version_id

    return results


@execution_time
def delete_objects(bucket: str, deletion_list: list[tuple[str, str]]) -> None:
    s3 = boto3.resource("s3").meta.client
    deletion_dict = {
        "Objects": [
            {"Key": key, "VersionId": version_id} for key, version_id in deletion_list
        ]
    }
    s3.delete_objects(Bucket=bucket, Delete=deletion_dict)


@execution_time
def generate_prefix_queue(start: int, prefix_length: int) -> Queue:
    prefix_queue = Queue()
    for prefix in prefix_generator(start, prefix_length):
        prefix_queue.put(prefix)
    return prefix_queue


def get_filestore_for_objects(object_list: list[str]) -> dict:
    with LocalDatabase(local_files_db_path) as local_db:
        results = {}
        for key in object_list:
            select_query = f"SELECT uuid, filestore FROM files WHERE uuid = '{key}';"
            try:
                uuid, filestore = local_db.run_query(select_query)[0]
                results[uuid] = filestore
            except IndexError:
                continue
    return results


def join_filestore_versions(
    objects_filestore: dict, objects_versions: dict, object_list: list[str]
) -> list[tuple[str, str, str]]:
    results: list[tuple[str, str, str]] = []
    for key, last_modified in object_list:
        filestore = objects_filestore.get(key, "")
        version_id = objects_versions.get(key, "")
        results.append((key, filestore, last_modified, version_id))
    return results


def worker(
    worker_id: int, prefix_queue: Queue, bucket: str, timing_queue: Queue
) -> None:
    with LocalDatabase(local_info_db_path) as local_database:
        while not prefix_queue.empty() and not terminate:
            prefix_start = pc()
            prefix = prefix_queue.get()
            object_list = list_objects(prefix=prefix, bucket=bucket)
            object_count = len(object_list)
            key_list = [_obj[0] for _obj in object_list]
            objects_filestore = get_filestore_for_objects(key_list)
            objects_versions = get_versions(bucket, key_list)
            rows_to_insert = join_filestore_versions(
                objects_filestore, objects_versions, object_list
            )
            row_count = len(rows_to_insert)
            if row_count > 0:
                logger.info(
                    f"Worker-{worker_id}: Inserting {row_count} rows for prefix {prefix}"
                )
                local_database.run_many(insert_query, rows_to_insert)
            prefix_end = pc()
            time_taken = prefix_end - prefix_start
            timing_queue.put((time_taken, object_count))


@execution_time
def main():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "-p", "--prefix", type=str, help="start from this prefix"
    )
    argument_parser.add_argument(
        "-n",
        "--namespace",
        type=str,
        help="namespace of s3 bucket",
        choices=["zaaksysteem-gov", "zaaksysteem-com"],
        required=True,
    )
    arguments = argument_parser.parse_args()
    prefix = arguments.prefix
    prefix_length = len(prefix)
    start = int(f"0x{prefix}", 0)
    namespace = arguments.namespace
    create_objects_table()
    bucket = buckets[namespace]
    prefix_queue = generate_prefix_queue(start=start, prefix_length=prefix_length)
    threads: list[Thread] = []
    for worker_id in range(10):
        thread = Thread(
            target=worker, args=(worker_id, prefix_queue, bucket, timing_queue)
        )
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == "__main__":
    timing_queue = Queue()
    try:
        main()
    except KeyboardInterrupt:
        terminate = True

    max_sleep = 10
    slept = 0
    while threading.active_count() > 1:
        logger.info(
            f"Waiting for all threads to finish, slept for {slept} seconds so far {threading.active_count()} threads still active"
        )
        if slept > max_sleep:
            break
        time.sleep(1)
        slept += 1

    with LocalDatabase(local_info_db_path) as local_database:
        last_uuid = local_database.run_query(
            "select uuid from objects order by 1 desc limit 1;"
        )[0][0]
        logger.warning(
            f"Last uuid processed: {last_uuid} use prefix: {last_uuid[:4]} for continuation"
        )

    process_time_total = 0
    items_processed = 0
    while not timing_queue.empty():
        time_taken, object_count = timing_queue.get()
        process_time_total += time_taken
        items_processed += object_count

    logger.info(
        f"Processed {items_processed} items in {process_time_total:.2f} seconds, {(process_time_total / items_processed):.2f} seconds per object"
    )
