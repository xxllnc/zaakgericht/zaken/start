#! /usr/bin/env python3

import sys

ALLOWED_LICENSES = {
    "apache",
    "apache_2_0",
    "artistic_1",
    "artistic_2",
    "artistic",
    "bsd",
    "eupl_1_1",
    "eupl_1_2",
# Note: this is about _upstream_ compatibility (so, things _we_ can depend on)
# GPL v2 and v3 are not compatible in that way sadly.
#    "gpl_2",
#    "gpl_3",
    "lgpl_2",
    "lgpl_2_1",
    "lgpl",
    "mit",
    "mozilla_1_1",
    "mozilla_2",
    "perl_5",
    "unrestricted",
    # For transient dependencies that are not loaded by using our code:
    "UNUSED",
}
LICENSE_OVERRIDES = {
    # Same license as Perl itself
    # https://metacpan.org/release/RJBS/Algorithm-Diff-1.201/source/lib/Algorithm/Diff.pm#L1674
    "Algorithm-Diff": {"perl_5"},
    # Same license as Perl itself
    # https://metacpan.org/pod/Any::URI::Escape
    "Any-URI-Escape": {"perl_5"},
    # Same license as Perl itself
    # https://metacpan.org/release/MLEHMANN/AnyEvent-HTTP-2.25/source/COPYING
    "AnyEvent-HTTP": {"perl_5"},
    # Same license as Perl itself
    # https://metacpan.org/release/MLEHMANN/AnyEvent-7.17/source/COPYING
    "AnyEvent": {"perl_5"},
    # You may distribute under the terms of either the GNU General Public License or the Artistic License, as specified in the Perl README file.
    "Array-Utils": {"gpl_2", "artistic_2"},
    # GPLv2, but our code never imports this (we don't process DEC VMS passwords)
    # https://metacpan.org/pod/Authen::DecHpwd
    "Authen-DecHpwd": {"UNUSED", "gpl_2"},
    # https://metacpan.org/pod/B::Flags
    # "AL & GPL"
    "B-Flags": {"artistic_2", "gpl_2"},
    # Same license as Perl itself
    # https://metacpan.org/dist/Bit-Vector/view/Vector.pod
    "Bit-Vector": {"perl_5"},
    # Artistic or GPL
    # https://metacpan.org/release/RJBS/Cache-Cache-1.08/source/COPYING
    "Cache-Cache": {"gpl_2", "artistic_2"},
    # Same license as Perl itself
    # https://metacpan.org/release/MLEHMANN/Canary-Stability-2013/source/COPYING
    "Canary-Stability": {"perl_5"},
    # https://metacpan.org/release/DRUOSO/Catalyst-Controller-SOAP-1.25/source/README#L113
    "Catalyst-Controller-SOAP": {"perl_5"},
    # Same license as Perl itself
    # https://metacpan.org/release/MICHIEL/Catalyst-Plugin-Params-Profile-0.05/source/README
    "Catalyst-Plugin-Params-Profile": {"perl_5"},
    # Same license as Perl itself
    # https://metacpan.org/pod/Class::ISA
    "Class-ISA" : {"perl_5"},
    # Same license as Perl it self
    # https://metacpan.org/release/MLEHMANN/common-sense-3.75/source/LICENSE
    "common-sense" : {"perl_5"},
    # BSD-like
    # https://metacpan.org/release/DPARIS/Crypt-DES-2.07/source/COPYRIGHT
    "Crypt-DES" : {"bsd"},
    # Same as Perl itself
    # https://metacpan.org/pod/Crypt::MySQL
    "Crypt-MySQL": {"perl_5"},
    # LGPLv3
    # https://metacpan.org/release/LEONT/Crypt-Rijndael-1.16/source/COPYING
    "Crypt-Rijndael": {"lgpl"},
    # Same as Perl itself
    # https://metacpan.org/pod/Crypt::UnixCrypt_XS
    "Crypt-UnixCrypt_XS": {"perl_5"},
    # GPLv2 or Artistic
    # https://metacpan.org/pod/Data::CompactReadonly
    "Data-CompactReadonly": {"gpl_2", "artistic_2"},
    # Same as Perl itself
    # https://metacpan.org/pod/DateTime::Event::ICal
    "DateTime-Event-ICal": {"perl_5"},
    # "Terms of Perl itself"
    # https://metacpan.org/release/FGLOCK/DateTime-Event-Recurrence-0.19/source/LICENSE
    "DateTime-Event-Recurrence": {"perl_5"}, 
    # Same as Perl itself
    # https://metacpan.org/pod/DateTime::Set
    "DateTime-Set": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Devel::Deprecations::Environmental
    "Devel-Deprecations-Environmental": {"perl_5"},
    # The author of this package disclaims all copyrights and releases it into the public domain.
    # https://metacpan.org/pod/Digest::CRC
    "Digest-CRC": {"unrestricted"},
    # Same as Perl itself
    # https://metacpan.org/pod/Digest::MD4
    "Digest-MD4": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Digest::MD5::File
    "Digest-MD5-File": {"perl_5"},
    # BSD-like
    # https://metacpan.org/release/ETHER/FCGI-0.82/source/LICENSE
    "FCGI": {"bsd"},
    # LGPL
    # https://metacpan.org/pod/FCGI::ProcManager
    "FCGI-ProcManager": {"lgpl"},
    # Same as Perl itself
    # https://metacpan.org/pod/File::Find::Rule
    "File-Find-Rule": {"perl_5"},
    # Apache licensed
    # https://metacpan.org/pod/File::MMagic
    "File-MMagic": {"apache"},
    # Same as Perl itself
    # https://metacpan.org/pod/Getopt::Long
    "Getopt-Long": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/release/WROSS/HTML-TagFilter-1.03/source/README
    "HTML-TagFilter": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/IO::String
    "IO-String": {"perl_5"},
    # "You may use this module under the terms of the BSD, Artistic, or GPL licenses, any version."
    "IPC-Run3": {"bsd", "artistic_2", "gpl_2"},
    # Same as Perl itself
    # https://metacpan.org/release/MLEHMANN/JSON-XS-4.03/source/COPYING
    "JSON-XS": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Lingua::EN::Inflect
    "Lingua-EN-Inflect": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Lingua::EN::Words2Nums
    "Lingua-EN-Words2Nums": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Lingua::Identify
    "Lingua-Identify": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Lingua::Stem::Fr
    "Lingua-Stem-Fr": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Lingua::Stem::It
    "Lingua-Stem-It": {"perl_5"},
    # GPLv2
    # https://metacpan.org/dist/Lingua-Stem-Snowball-Da/source/Da.pm#L5
    "Lingua-Stem-Snowball-Da": {"gpl_2"},
    # Same as Perl itself
    # https://metacpan.org/pod/Linux::Proc::Net::TCP
    "Linux-Proc-Net-TCP": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Locales
    "Locales": {"perl_5"},
    # Artistic or GPLv2+
    # https://metacpan.org/release/SCHWIGON/LockFile-Simple-0.208/source/README#L9
    "LockFile-Simple": {"artistic_2", "gpl_2"},
    # Same as Perl itself
    # https://metacpan.org/pod/LWP::UserAgent::Determined
    "LWP-UserAgent-Determined": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Module::Pluggable::Fast
    "Module-Pluggable-Fast": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Module::Want
    "Module-Want": {"perl_5"},
    # Mozilla Public License V2
    # https://metacpan.org/pod/Mozilla::CA
    "Mozilla-CA": {"mozilla_2"},
    # GPLv2 or artistic
    # https://metacpan.org/pod/NetAddr::IP
    "NetAddr-IP": {"gpl_2", "artistic"},
    # Same as Perl itself
    # https://metacpan.org/release/MANU/Net-IP-1.26/source/COPYING
    "Net-IP": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Net::SCP::Expect
    "Net-SCP-Expect": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Number::Compare
    "Number-Compare": {"perl_5"},
    # GPLv2 or Artistic
    # https://metacpan.org/pod/Number::Phone
    "Number-Phone": {"artistic", "gpl_2"},
    # EUPL (Mintlab BV/zaaksysteem.nl)
    # https://metacpan.org/pod/OpenOffice::OODoc::HeadingStyles
    "OpenOffice-OODoc-HeadingStyles": {"eupl_1_1"},
    # EUPL (Mintlab BV/zaaksysteem.nl)
    # https://metacpan.org/pod/OpenOffice::OODoc::InsertDocument
    "OpenOffice-OODoc-InsertDocument": {"eupl_1_1"},
    # LGPL v2.1
    # https://metacpan.org/pod/OpenOffice::OODoc
    "OpenOffice-OODoc": {"lgpl_2_1"},
    # Same as Perl itself
    # https://metacpan.org/pod/Parallel::Fork::BossWorkerAsync
    "Parallel-Fork-BossWorkerAsync": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Params::Profile
    "Params-Profile": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Parse::FixedLength
    "Parse-FixedLength": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Pod::Coverage
    "Pod-Coverage": {"perl_5"},
    # Artistic or same as Perl itself
    # https://metacpan.org/pod/Pod::Parser
    "Pod-Parser": {"perl_5"},
    # Artistic or GPLv2
    # https://metacpan.org/pod/Scalar::Type
    "Scalar-Type": {"gpl_2", "artistic"},
    # Same as Perl itself
    # https://metacpan.org/pod/Set::Infinite
    "Set-Infinite": {"perl_5"},
    # LGPL v2
    # https://metacpan.org/pod/String::Approx
    "String-Approx": {"lgpl_2"},
    # Artistic or GPLv2
    # https://metacpan.org/pod/String::Binary::Interpolation
    "String-Binary-Interpolation": {"gpl_2", "artistic"},
    # Same as Perl itself
    # https://metacpan.org/release/KABLAMO/String-CamelSnakeKebab-0.06/source/LICENSE
    "String-CamelSnakeKebab": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Sub::Attribute
    "Sub-Attribute": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Sub::Override
    "Sub-Override": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Template::Provider::Encoding
    "Template-Provider-Encoding": {"perl_5"},
    # GPLv3 or Artistic v2
    # https://metacpan.org/pod/Template::Timer
    "Template-Timer": {"artistic_2", "gpl_3"},
    # Same as Perl itself
    # https://metacpan.org/pod/Term::ProgressBar::Simple
    "Term-ProgressBar-Simple": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Term::ReadPassword
    "Term-ReadPassword": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Text::Brew
    "Text-Brew": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/dist/Text-German/view/German.pod
    "Text-German": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Text::Ngram
    "Text-Ngram": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Tie::ToObject
    "Tie-ToObject": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/release/MLEHMANN/Types-Serialiser-1.01/source/COPYING
    "Types-Serialiser": {"perl_5"},
    # Same as Perl itself
    # https://metacpan.org/pod/Unicode::String
    "Unicode-String": {"perl_5"},
    # (c) Mintlab/Zaaksysteem/xxllnc, EUPL
    # https://metacpan.org/pod/WebService::KvKAPI
    "WebService-KvKAPI": {"eupl_1_1"},
    # (c) Mintlab/Zaaksysteem/xxllnc, EUPL
    # https://metacpan.org/pod/WebService::OverheidIO
    "WebService-OverheidIO": {"eupl_1_1"},
    # Same license as Perl itself
    # https://metacpan.org/release/MIKEWONG/XML-Dumper-0.81/source/README
    "XML-Dumper": {"perl_5"},
    # XML::SAX is dual licensed under the same terms as Perl itself.
    # https://metacpan.org/release/GRANTM/XML-SAX-1.02/source/LICENSE
    "XML-SAX": {"perl_5"},
    # This is our own code
    "Zaaksysteem-OLO-Sync": {"eupl_1_1"},
    "Zaaksysteem-Service-HTTP": {"eupl_1_1"},
    "Zaaksysteem-VirusScanner": {"eupl_1_1"},
}

errors = 0
with open(sys.argv[1], "r") as license_tsv:
    for line in license_tsv.read().splitlines():
        package, licenses_raw = line.split("\t")
        licenses = set(licenses_raw.split(","))
        
        if package in LICENSE_OVERRIDES:
            if not isinstance(LICENSE_OVERRIDES[package], dict):
                licenses = LICENSE_OVERRIDES[package]


        if not licenses & ALLOWED_LICENSES:
            print(f"Package {package} is not licensed under an allowed license: {licenses}")
            errors += 1

if errors:
    print("❌ Error verifying dependency licenses")
    sys.exit(1)

print("✅ Dependency licenses verified")
